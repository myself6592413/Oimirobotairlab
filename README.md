## Oimi robot project
Development of a mobile autonomous robot addressed to autistic children at AIRLab lab at Politecnico di Milano

## Overview
The OIMIRobot project is an initiative of the AIRLab at Politecnico di Milano. 
It aims to develop an autonomous mobile robot specifically designed for children with Autism Spectrum Disorder (ASD). 

The robot is intended to assist in the development of social skills and improve cognitive abilities trough play ad hoc games.
It is designed to be interactive and engaging, with a range of sensors and capabilities that allow it to respond to the child's behavior and interests. 
It is also intended to be customizable, so that it can be adapted to the individual needs and preferences of each child.

## Info
For more details about the whole project, look into: 
``` 
pi/OIMI/oimi_docs/oimi_project_info
```
The main folder with source code is in 
```
pi/OIMI/oimi_code/src
```

For other info regarding sensors and actuators, check:
```
$vim /pi/OIMI/oimi_docs/datasheets
```

To check aliases for terminal command to use in Raspi OS:
```
$vim /pi/.bash_aliases
```

To check list of Bluetooth commands to send look:
```
pi/OIMI/oimi_code/src/managers/bluetooth_manager
```
