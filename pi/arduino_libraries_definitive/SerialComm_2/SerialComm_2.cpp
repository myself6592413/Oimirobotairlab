/*************************************************************
* Oimi-Robot SetialComm.cpp --> see SerialComm_2.h for details
**************************************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include "SerialComm_2.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructor */
SerialComm_2::SerialComm_2(){
	startChar = '<';	      // recognizable string's start marker <
	endChar = '>';			  // recognizable string's end marker >
	receivedBytes = 0;		  // initializes to zero inputBuffer index
	currParser = 0;
	// Buffers
	bufferMsg[0] = {0}; 	  // initializes first element to zero
	// Strings
	secondMsg[0] = {0}; 	  // initializes first element to zero
	thirdMsg[0] = {0};        // initializes first element to zero	
	// Flags
	just_changed = false;
	isStillReading = false;
	okNewData = false;
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Taking info from USB */
void SerialComm_2::catchFromSerial(int modality){
/*Splitting buffer data into parts
mod 0: saves command for next instr
mod 1: stores not only the command, but also other data as the speeds e.g gamepad
mod 2: Stores command and parameter as the speeds e.g gamepad (when angular speed is not needed and not sent by raspi)
mod 3: Stores custom leds animations or speeds imposed by raspi e.g games reactions
*/
	switch(modality){
		char * strokToken;                          // where store each subpart of string temporarily
		case 0:
			strokToken = strtok(inputBuffer,",");	// strtok() breaks string str into parts, using "," as delimiter, getting only the first (or unique) part of the string
			strcpy(bufferMsg, strokToken); 		    // copies into bufferMsg
		break;
		//Messages of type = <reacthis, red, wipe> 
		case 1:									
			strokToken = strtok(inputBuffer,",");
			strcpy(bufferMsg, strokToken);

			strokToken = strtok(NULL, ",");
			strcpy(secondMsg, strokToken);

			strokToken = strtok(NULL, ",");
			strcpy(thirdMsg, strokToken);
		break;
	}
}
void SerialComm_2::acquireDataFromRaspi(){
/*
Receive data on serial shared with raspi, asap a new msg appears on serial
Serial.available() Returns the number of bytes arrived and ready to be read
Reds data already arrived and stored in the serial receive buffer (64 bytes)
Saves data into inputBuffer
Parses and stores correctly data calling one catchFromSerial
CurrParser is initialized to zero, than changed from raspi with a new msg
(For changing modality it's enough mod0, if currParser is different from mod0 is ok,
if next data discovered on serial are few than expected (by current modality) no prolem, in any case bufferMsg is captured)
*/
	if(Serial.available() > 0){ 				 // only if there are new unread data
		char x = Serial.read(); 				 // reads msg on serial
		if(x==endChar){ 					 	 // checks if received char is the keyword ">", if so the end of the string is reached
			isStillReading = false;				 // sets flag for correct reading
			okNewData = true;					 // sets flag for reply timing
			inputBuffer[receivedBytes] = 0;		 // resets inputBuffer
			catchFromSerial(currParser);		 // parses bufferMsg
		}
		if(isStillReading){						 // only if there's more to read
			inputBuffer[receivedBytes] = x;		 // puts collected data into inputBuffer
			receivedBytes ++;					 // increments buffer index for next iter
			if (receivedBytes == bufferSize)
				receivedBytes = bufferSize - 1;  // controls last element
		}
		if(x==startChar){						 // checks if received char is "<" ,so start is finded
			receivedBytes = 0;					 // initializes index for input buffer
			isStillReading = true;			     // asserts that is possible to start reading data
		}
	}
	just_changed = true; 						 // allows the identification of gamepad's commands
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Getters*/
bool SerialComm_2::getJustChanged(){
	return just_changed;
}
char* SerialComm_2::getBufferMsg(){
	return bufferMsg;
}
bool SerialComm_2::getOkNewData(){
	return okNewData;
}

int SerialComm_2::getCurrParser(){
	return currParser;
}
char* SerialComm_2::getMsgSecond(){
	return secondMsg;
}
char* SerialComm_2::getMsgThird(){
	return thirdMsg;
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Setters*/
void SerialComm_2::setCurrParser(int modality){
	currParser = modality;
}
void SerialComm_2::setOkNewData(bool data){
	okNewData = data;
}
void SerialComm_2::setJustChanged(bool change){
	just_changed = change;
}