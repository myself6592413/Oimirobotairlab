/********************************************************************
* Oimi SerialComm.h - Library for handling communication with raspi.
*
* --notes
*	Designed for Oimi Robot needs,
*		eg. receiving 3 different msgs
*			receiving 3 different floats
*
* Created by Giacomo Colombo Airlab Politecnico di Milano 2020
*********************************************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Conditional compilation */
#ifndef SerialComm_2_h
#define SerialComm_2_h
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
#define bufferSize 100
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Class */
class SerialComm_2{
	private:
		/*************************************************************************************************** Variables */
		char startChar;				    // marker always needed to begin a msg
		char endChar;		 			// marker always needed at the end of a msg
		byte receivedBytes;		     	// inputBuffer index for
		int currParser; 				// chooses how catch info on serial, can change is value according to new data read
		// Buffers
		char inputBuffer[bufferSize]; 	// principal buffer
		char bufferMsg[bufferSize]; 	// where first and main command is stored
		char secondMsg[bufferSize]; // ledstrip color command
		char thirdMsg[bufferSize];  // ledstrip animation command		
		// Flags
		bool just_changed;				// for manual movement
		bool isStillReading;			// for reading whole msg
		bool okNewData;					// if ok looks for data on serial

	public:
		/*************************************************************************************************** Constructor */
		SerialComm_2();
		/*************************************************************************************************** Methods */
		// Fundamentals
		void catchFromSerial(int modality);
		//void catchFromSerial();
		void acquireDataFromRaspi();
		// Getters
		int getCurrParser();
		bool getOkNewData();
		bool getJustChanged();
		char* getBufferMsg();
		char* getMsgSecond();
		char* getMsgThird();
		// Setters
		void setCurrParser(int modality);
		void setOkNewData(bool data);
		void setJustChanged(bool change);
		};
		#endif



