/**
*Oimi-Robot - Voltage.h for detecting lead battery status periodically
*
*/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */ 
#include "Voltage.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructor */ 
Voltage::Voltage(uint8_t analpin){
    _checkpin=analpin;

	sample_count=0;
	sum=0;
	cont=0;
	voltage=0.0f;
	battery_indicator=0;
	lastBatteryUpdate=0;
	map_res=0.0f;
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Methods */ 
float Voltage::findVoltage(){
/*
Uses 5.0 for a 5.0V ADC reference voltage, but 5.015V isthe calibrated reference voltage
Voltage is multiplied by 11 when using voltage divider that divides by 11. 
Uses 11.132 as csalibrated voltage divide

//olds ---54?? 3.61/63 ?
ma prima era ok con ?3.72 = 01112019 ricky pair ; 3.78 = oimi; 3.915 old old ???
*/	
	while (sample_count < NUM_SAMPLES) {
    	sum += analogRead(_checkpin);
    	sample_count++;
	}
	voltage = (((float)sum / (float)NUM_SAMPLES * 5.005) / 1024.0)*4.05; 

	sample_count=0;
	sum=0;
	cont++;
	return voltage;
}

float Voltage::mapfloat(float x, float in_min, float in_max, float out_min, float out_max){
	return (float)(x - in_min) * (out_max - out_min) / (float)(in_max - in_min) + out_min;
}
void Voltage::calcVoltage(){
    map_res = mapfloat(voltage,MIN_INDICATOR_VOLTAGE,MAX_INDICATOR_VOLTAGE,MIN_INDICATOR_VALUE,MAX_INDICATOR_VALUE);
    battery_indicator=constrain(map_res,MIN_INDICATOR_VALUE,MAX_INDICATOR_VALUE);
}
