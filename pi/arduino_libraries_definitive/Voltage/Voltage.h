/********************************************************************************************
* Oimi-Robot - Voltage.h, library for checking periodically the status of the Lead Battery
*
* Created by Giacomo Colombo Airlab Politecnico di Milano 2020
********************************************************************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Conditional compilation */
#ifndef Voltage_h
#define Voltage_h
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
#define NUM_SAMPLES 10
#define MAX_INDICATOR_VOLTAGE 12.5f
#define MIN_INDICATOR_VOLTAGE 11.5f
#define MAX_INDICATOR_VALUE 0.0f
#define MIN_INDICATOR_VALUE 5.0f
#define BATTERY_UPDATE_TIME 20000
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Class */
class Voltage{
	private:
		/***************************************************** Variables */
		unsigned char sample_count;
		int sum;
		int cont;
		float voltage;
		float map_res;
		int battery_indicator;
		unsigned long int lastWarning;
		unsigned long int lastBatteryUpdate;
		uint8_t _checkpin;
	public:
		/***************************************************** Constructor */
  		Voltage(uint8_t checkpin);
  		/************************************************************************************ Methods */
  		float mapfloat(float x, float in_min, float in_max, float out_min, float out_max);
  		float findVoltage();
  		void calcVoltage();
};
#endif
