/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Oimi-Robot Movements.cpp --> see Movements.h for info
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include "Movements.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructor */
Movements::Movements(OimiPid & oimiPid, Sonars_EZ1 & sonars, Adafruit_NeoPixel & strip)
: _oimiPid(oimiPid), _sonars(sonars), _strip(strip){
	// Speeds
	strafe 	= 0.0;
	forward = 0.0;
	angular = 0.0;
	actual_forward_speed = FORWARD_SLOW_SPEED;
	actual_angular_speed = ANGULAR_SLOW_SPEED;
	// Distances
	allowed_distance = 0.0;
	// Poses
	startPosX	= 0.0;
	startPosY 	= 0.0;
	startPosTh 	= 0.0;
	// Angles
	exactAngle 	= 0.0;
	// Flags
	//isLighten 		= true;
	// Custom Speeds
	customSpeed 	= 0.0;
	customDistance 	= 0.0;
	customAngle 	= 0.0;
	// Leds
	currentAnimation = "None";
	currentColor 	 = "None";
	// Timers
	previousTurnTime   = 0;
	rotationInterval   = 720;
	seekInterval 	   = 12000;
	stallInterval	   = 500;
	stallBackInterval  = 400;
	// Counters
	counter_moves = 0;
	numberTurning = 0;
	// Movements
	lastDirection 		= none;
	forbidden_movement 	= none;
	last_pose = 0;
	intervalloPrint = 20000;
	okLeds = true; //poi lo devo passare da ...o uso eeprom
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Getters*/
float Movements::getAllowedDistance(){
	return allowed_distance;
}
float Movements::getStartPosTh(){
	return startPosTh;
}
float Movements::getAngular(){
	return angular;
}
float Movements::getStrafe(){
	return strafe;
}
float Movements::getForward(){
	return forward;
}
unsigned char Movements::getActualMovement(){
	return actual_movement;
}
bool Movements::getIsMoving(){
	return isMoving;
}
bool Movements::getisSelfGoverning(){
	return isSelfGoverning;
}
bool Movements::getIsFollowing(){
	return isFollowing;
}
bool Movements::getIsControlled(){
	return isControlled;
	}
bool Movements::getIsEnded(){
	return isEnded;
}
bool Movements::getAutonomousFirst(){
	return auto_action;
}
float Movements::getCustomSpeed(){
	return customSpeed;
}
float Movements::getCustomDistance(){
	return customDistance;
}
float Movements::getCustomAngle(){
	return customAngle;
}
Verse Movements::getForbiddenMov(){
	return forbidden_movement;
}
float Movements::getActualAngularSpeed(){
	return actual_angular_speed;
}
float Movements::getActualForwardSpeed(){
	return actual_forward_speed;
}
const char* Movements::getCurrentAnimation(){
	return currentAnimation;
}
const char* Movements::getCurrentColor(){
	return currentColor;
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Setters */
void Movements::setOkLeds(bool decision){
	okLeds = decision;
}
void Movements::setIsMoving(bool decision){
	isMoving = decision;
}
void Movements::setisSelfGoverning(bool decision){ //useless??
	isSelfGoverning = decision;
}
void Movements::setIsFollowing(bool decision){ //useless??
	isSelfGoverning = decision;
}
void Movements::setIsControlled(bool decision){
	isControlled = decision;
}
void Movements::setAllowedDistance(float distance){
/* For games and reactions, set by raspi */
	allowed_distance = distance;
}
void Movements::setCustomSpeed(float speed){
/* For games and reactions, set by raspi */
	customSpeed = speed;
}
void Movements::setStrafeSpeed(float speed){
	strafe = speed;
}
void Movements::setForwardSpeed(float speed){
	forward = speed;
}
void Movements::setAngularSpeed(float speed){
	angular = speed;
}
void Movements::setCustomAngle(float  angle){
	customAngle = angle;
}
void Movements::setActualMovement(unsigned char actual){
	actual_movement = actual;
}
void Movements::setCustomDistance(float distance){
	customDistance = distance;
}
void Movements::setAutonomousFirst(bool first){
	auto_action = first;
}
void Movements::setForbiddenMov(Verse forbidden){
	forbidden_movement = forbidden;
}
void Movements::setActualAngularSpeed(float speed){
	actual_angular_speed = speed;
}
void Movements::setActualForwardSpeed(float speed){
	actual_forward_speed = speed;
}
void Movements::setCurrentColor(const char* color){
/*For led-stripe, set by raspi during games*/
	currentColor = color;
}
void Movements::setCurrentAnimation(const char* animation){
/*For led-stripe, set by raspi during games*/
	currentAnimation = animation;
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Leds */
//provo selezioni diverse animazioni anche quindi prendo come param quale animazione in char (togliendo cili però)
//va messo anche qui begin show..? dubito
void Movements::lightUpLeds(const char* color, const char* animation){
	uint32_t choosen_col;
	if((strcmp(color, "red")==0))
		choosen_col = _strip.Color(255,0,0);
	if((strcmp(color, "green")==0))
		choosen_col = _strip.Color(0,255,0);
	if((strcmp(color, "blue")==0))
		choosen_col = _strip.Color(0,0,255);
	if((strcmp(color, "yellow")==0))
		choosen_col = _strip.Color(255,255,0);

	if((strcmp(animation, "stable")==0)){
		_strip.colorStable(choosen_col);
	}
	else if((strcmp(animation, "wipe")==0)){
		_strip.colorWipe(choosen_col,40);
	}
	//_strip.colorRed(color);
	//aggiunta ultima!
	//if strcmpy...uguale a message but for messageLed
	//animation led _strip.colorWipe
	//or elseif colorRainb
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Speeds */
void Movements::calcStrafeForwardSpeeds(float module, float xp, float yp){
/*
Finds all speeds according to a vector's components and its magnitude
*/
    if(xp==0 && yp==0){
        strafe = 0.0;
        forward = 0.0;
        angular = 0.0;
    }
    else{
    	float angle = atan2(yp, xp);
        strafe = cos(angle)*module;
        forward = sin(angle)*module;
        angular = 0.0;

    	if(strafe < MIN_STRAFE && strafe > -MIN_STRAFE)	strafe = 0.0;
    	if(forward < MIN_FORWARD && forward > -MIN_FORWARD)	forward = 0.0;
    }
    if(isSelfGoverning){
    	strafe = strafe*MAX_FORWARD_AUTO;
    	forward = forward*MAX_FORWARD_AUTO;
    }
    else{ //gamepad control
		strafe = strafe*MAX_FORWARD_CONTROL;
    	forward = forward*MAX_FORWARD_CONTROL;
    }
}
void Movements::putAngularSpeed(float rotation){
	angular = rotation;
	if(angular < MIN_ANGULAR && angular > -MIN_ANGULAR)
		angular = 0;
    }
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Distances                                                     */
float Movements::chooseForSpeedsAndDistance(maxSonar distance){
/*
Sets correct speeds and choosing max allowed distance
Priority to slowers -->change for testing
*/
	int rand_ang = random(6);
	int rand_for = random(2);
	switch(rand_ang){
		case 0: actual_angular_speed = ANGULAR_MIN_SPEED;	  break;
		case 1: actual_angular_speed = ANGULAR_SLOW_SPEED;	  break;
		case 2: actual_angular_speed = ANGULAR_SLOW_SPEED;	  break;
		case 3: actual_angular_speed = ANGULAR_NORMAL_SPEED;  break;
		case 4: actual_angular_speed = ANGULAR_NORMAL_SPEED;  break;
		case 5: actual_angular_speed = ANGULAR_QUICK_SPEED;	  break;
	}
	switch(distance.status){
		case contact:
			allowed_distance = 0.0;
			actual_forward_speed = 0.0f;
		break;
		case very_close:
			actual_forward_speed = FORWARD_MIN_SPEED;
		break;
		case close:
			if (rand_for) actual_forward_speed = FORWARD_MIN_SPEED;
			else actual_forward_speed = FORWARD_SLOW_SPEED;
		break;
		case near:
			if (rand_for) actual_forward_speed = FORWARD_MIN_SPEED;
			else actual_forward_speed = FORWARD_SLOW_SPEED;
		break;
		case medium:
			rand = random(4);
			switch(rand){
			case 0:	actual_forward_speed = FORWARD_SLOW_SPEED;	  break;
			case 1:
			case 2: actual_forward_speed = FORWARD_NORMAL_SPEED;  break;
			case 3: actual_forward_speed = FORWARD_QUICK_SPEED;   break;
			}
		break;
		case great:
			rand = random(6);
			switch(rand){
			case 0:	actual_forward_speed = FORWARD_SLOW_SPEED;	  break;
			case 1:
			case 2: actual_forward_speed = FORWARD_NORMAL_SPEED;  break;
			case 3:
			case 4: actual_forward_speed = FORWARD_QUICK_SPEED;   break;
			case 5: actual_forward_speed = FORWARD_FAST_SPEED; 	  break;
			}
		break;
		case very_great:
		case far:
		case very_far:
		case big:
		case very_big:
		case super_big:
		case super_far:
			rand = random(6);
			switch(rand){
			case 0:
			case 1: actual_forward_speed = FORWARD_NORMAL_SPEED;  break;
			case 2:
			case 3: actual_forward_speed = FORWARD_QUICK_SPEED;	  break;
			case 4:
			case 5: actual_forward_speed = FORWARD_FAST_SPEED; 	  break;
			}
		break;
		case max:
		case err: //probable error in this case
			if(rand_for)
				actual_forward_speed = FORWARD_MIN_SPEED;
			else
				actual_forward_speed = FORWARD_SLOW_SPEED;
	}
	allowed_distance = distance.dist_in;
	//TOREMOVE!
	actual_forward_speed = FORWARD_SLOW_SPEED;

	return allowed_distance;
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Basic movements*/
void Movements::setupMovement(){
	_oimiPid.resetIntegTerm();
	_oimiPid.resetPoses();
	movStartTime=millis();
	startPosX = _oimiPid.getPosX();
	startPosY = _oimiPid.getPosY();
	startPosTh = _oimiPid.getPosTh();
	actual_movement = no_movement;
	setIsMoving(false);
}
void Movements::setupMovement(unsigned char action){
	_oimiPid.resetIntegTerm();
	_oimiPid.resetPoses();
	movStartTime=millis();
	startPosX = _oimiPid.getPosX();
	startPosY = _oimiPid.getPosY();
	startPosTh = _oimiPid.getPosTh();
	actual_movement = action;
	setIsMoving(true);
}
void Movements::goDirectly(float module, float xp, float yp){
	calcStrafeForwardSpeeds(module, xp, yp);
	_oimiPid.holonomicRun(strafe,forward,angular);
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Stops */
void Movements::stopMoving(){
	//counter_moves=0;
	//Serial.println("stopMoving!!!!!");
	//_strip.noColor();
//	if(okLeds && isLighten){
//		currentAnimation = "stable";
//		currentColor = "red";
//		lightUpLeds(currentColor,currentAnimation);
//		isLighten=false;
//	}
	if(!isFollowing && !isSelfGoverning){
		//Serial.print(F("!!STOP MOVING NOW!!"));
 		////Serial.print(F("!stop_complete1"));
		_oimiPid.completeStop();
		if(singleTrial){
			counter_moves = 0;
			isMoving = false;
			isEnded = true;
			actual_movement = no_movement;
			singleTrial = false;
		}
		else if(momentanous)
			counter_moves = counter_moves+1;
	}
	else if(isFollowing && isSafe){
		counter_moves=0;
		_oimiPid.completeStop();
		goingAhead = false;
		isSafe = false;
		freePath = false;
		actual_movement = following;
		_sonars.setGoingAhead(goingAhead);
	}
	else if(isFollowing){
		counter_moves = 0;
		_oimiPid.simpleStop();
		goingAhead = false;
		_sonars.setGoingAhead(goingAhead);
		follo_hurdle = false;
		actual_movement = following;
	}
	else if(isSelfGoverning){
		counter_moves = 0;
		if((!auto_hurdle)
		&&((actual_movement==turn35_l)||(actual_movement==go_left)||(actual_movement==go_right)
		||(actual_movement==turn35_r)||(actual_movement==turn_until_l)||(actual_movement==turn_until_r))){
			_oimiPid.simpleStop();
		}
		else{
 			_oimiPid.partialStop();
		}
		// for avoiding unexpected behaviours
		_oimiPid.resetIntegTerm();
		_oimiPid.setPosTh(0);
		_oimiPid.setPosX(0);
		_oimiPid.setPosY(0);
		movStopTime=millis();
		if(auto_hurdle){
			////Serial.print(F("stopMoving --enter here cause auto_hurdle = true"));
			auto_hurdle = false; //mi serve veramente??
			//wasBlocked = true;//avoid repeat old move
			actual_movement = autonomous;
		}
		else if(auto_change){
			////Serial.print(F("stopMoving --enter here cause auto_change orientation finished"));
			auto_change = false;
			actual_movement = autonomous;
		}
		else if(auto_variation){
			////Serial.print(F("stopMoving --enter here cause auto_change= true, begin"));
			while (millis() < movStopTime + 2000) {} //delay two seconds
			auto_variation = false;
			changeOrientation();
		}
		else if(auto_action){
		//dopo un turn proveniente da left/rightmove..
			//Serial.print(F("stopMoving --enter here cause auto_action = true"));
			actual_movement = go_straight;
			setAutonomousFirst(false);
		}
		else{
			//nel caso in cui...dai cazzo inizia a commentare bene tutti i casi cosi li ricordo
			//qui se vengo da gostraight from quicklyturn
			////Serial.print(F("stopMoving --enter here cause auto_action = false"));
			actual_movement = autonomous;
		}
	}
	else if(actual_movement==dance && counter_moves==19){
		counter_moves=0;
		actual_movement = no_movement;
	}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Rotations */
void Movements::turn(float angle, int direction){ //posso anche qui sistemare e ridurre un solo stopmoving!
	//Serial.println("GIRATI");
//	if(millis() >= intervalloPrint){
//		Serial.println(F("<turnmov>"));
//	okmanda = false;
//	}
	setAutonomousFirst(true);
//	if(isLighten && okLeds){
//		currentAnimation = "stable";
//		currentColor = "blue";
//		lightUpLeds(currentColor,currentAnimation);
//		isLighten=false;
//	}
	if(angle==30){
		if(abs(_oimiPid.getPosTh()) < startPosTh+PI/6){
			if(!direction){ //dir=0=false=left
				////Serial.print(F("turnAlphaleft at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				okmanda=false;
			}
			else{ //dir=1=true=right
				////Serial.print(F("turnAlpahRight at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				okmanda=false;
			}
		}
		else stopMoving();
	}
	else if(angle==35){ //new for new position of sonars
	    //Serial.println("GIRATI 35 left");
		if(abs(_oimiPid.getPosTh()) < startPosTh+PI/5){
			if(!direction){ //dir=0=false=left
				////Serial.print(F("turnAlphaleft at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				okmanda=false;
			}
			else{ //dir=1=true=right
				//Serial.println("GIRATI 35 right:");   
                ////Serial.print(F("turnAlpahRight at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				okmanda=false;
			}
		}
		else stopMoving();
	}
	else if(angle==45){
		if(abs(_oimiPid.getPosTh()) < startPosTh+PI/4){
			if(direction==false){ //dir=0=false=left
				////Serial.print(F("turnBetaLeft at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				okmanda=false;
			}
			else{ //dir=1=true=right
				////Serial.print(F("turnBetaRight at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				okmanda=false;
			}
		}
		else stopMoving();
	}
	else if(angle==60){
		if(abs(_oimiPid.getPosTh()) < startPosTh+PI/3){
			if(direction==false){ //dir=0=false=left
				////Serial.print(F("turnGammaleft at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				okmanda=false;
			}
			else{ //dir=1=true=right
				////Serial.print(F("turnGammaRight at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				okmanda=false;
			}
		}
		else stopMoving();
	}
	else if(angle==90){
		if(abs(_oimiPid.getPosTh()) <= startPosTh+PI/2){
			if(direction==false){ //dir=0=false=left
				////Serial.print(F("turn90left at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				okmanda=false;
			}
			else{ //dir=1=true=right
				////Serial.print(F("turn90right at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				okmanda=false;
			}
		}
		else stopMoving();
	}
	else if(angle==120){
		if(abs(_oimiPid.getPosTh()) < startPosTh+2*PI/3){
			if(direction==false){ //dir=0=false=left
				////Serial.print(F("turnDeltaLeft at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				okmanda=false;
		}
			else{ //dir=1=true=right
				////Serial.print(F("turnDeltaRight at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				okmanda=false;
			}
		}
		else stopMoving();
	}
	else if(angle==150){
		if(abs(_oimiPid.getPosTh()) < startPosTh+5*PI/6){
			if(direction==false){ //dir=0=false=left
				////Serial.print(F("turnBetaLeft at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				okmanda=false;
			}
			else{ //dir=1=true=right
				////Serial.print(F("turnEpsilonRight  at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				okmanda=false;
			}
		}
		else stopMoving();
	}
	else if(angle==180){
		if(abs(_oimiPid.getPosTh()) <= startPosTh+PI){
			if(direction==false){ //dir=0=false=left
				////Serial.print(F("turn90left at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				okmanda=false;
			}
			else{ //dir=1=true=right
				////Serial.print(F("turn90right at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				okmanda=false;
			}
		}
		else stopMoving();
	}
	else if(angle==360){
		if(abs(_oimiPid.getPosTh()) < startPosTh+2*PI){
			if(direction==false){ //dir=0=false=left
				////Serial.print(F("turn360left at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				okmanda=false;
			}
			else{ //dir=1=true=right
				////Serial.print(F("turn360right at speed "));
				////Serial.print(actual_angular_speed);
				_oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				okmanda=false;
			}
		}
		else stopMoving();
	}
	//guardo qui! è giusto if inside last if?
	else if(angle==2){ //turn until free
		//current_env = _sonars.getCurrentEnv();
		if(!direction){ //left
			//Serial.print("turn until left");
            if(current_env.scans[0].dist_in > 0){
				//non posso metterne due sensori per vedere se li metto belli distanti separati
				//nella nuova config.
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				if(current_env.scans[2].status > very_close)
					stopMoving();
			}
		}
		else{ //right
			if(current_env.scans[0].dist_in > 0){ //0 or 1 is the same
				//Serial.print("turn until right");
                _oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				if(current_env.scans[0].status > very_close)
					stopMoving();
			}
		}
	}
	else if(angle==3){ //turn until free
		//current_env = _sonars.getCurrentEnv();
		if(!direction){ //left
			if(current_env.scans[0].dist_in > 0){
				//non posso metterne due sensori per vedere se li metto belli distanti separati
				//nella nuova config.
				_oimiPid.nonHolonomicRun(0.0, actual_angular_speed);
				if((current_env.scans[0].status > very_close)
				&&(current_env.scans[1].status > very_close)
				&&(current_env.scans[2].status > very_close))
					stopMoving();
			}
		}
		else{ //right
			if(current_env.scans[0].dist_in > 0){ //num scan is the same, basta una
				_oimiPid.nonHolonomicRun(0.0, -actual_angular_speed);
				if((current_env.scans[0].status > very_close)
				&&(current_env.scans[1].status > very_close)
				&&(current_env.scans[2].status > very_close))
					stopMoving();
			}
		}
	}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Predefinite movements */
void Movements::goStraight(int event){
/*Advance immediately after a rotation without stop
Conter incrementation instead of stop asap detection front hurdle
Min distance +1 for left right not for front
*/
	//Serial.println("gostraight parameter vers");

//	if(okmanda){
		//Serial.println(F("<frontmov>"));
//		okmanda = false;
//	}
//	Serial.print(F("<frontmov"));
//	Serial.println(F(">"));

	//current_env = _sonars.getCurrentEnv();
	goingAhead = true;
	_sonars.setGoingAhead(goingAhead);
	//Serial.print("allowed distance is: ");
	////Serial.print(allowed_distance);
    ////Serial.print(current_env.scans[1].state);
	if(!isFollowing && !isSelfGoverning){
		if((current_env.scans[0].dist_in > 0)&&(current_env.scans[1].dist_in > 0)&&(current_env.scans[2].dist_in > 0)){
			if((current_env.scans[0].dist_in <= MIN_DISTANCE+1)||(current_env.scans[1].dist_in <= MIN_DISTANCE)||(current_env.scans[2].dist_in <= MIN_DISTANCE+1))
				stopMoving();
		}
	}
	else if(isFollowing && isSafe){
		chooseForSpeedsAndDistance(current_env.scans[1]);
		actual_forward_speed = FORWARD_SLOW_SPEED;
		//provo minore =??
		if((current_env.scans[0].dist_in > 0)&&(current_env.scans[1].dist_in > 0)&&(current_env.scans[2].dist_in > 0)){
			if(((current_env.scans[0].dist_in <= MIN_DISTANCE+1)||(current_env.scans[1].dist_in <= MIN_DISTANCE)||(current_env.scans[2].dist_in <= MIN_DISTANCE+1))
			||(_oimiPid.getPosX() > allowed_distance/15 && current_env.scans[1].status > big)
			||(_oimiPid.getPosX() > allowed_distance/11 && current_env.scans[1].status < big))
				counter_moves = event + 1;
		}
	}
	else if(isFollowing){
		actual_forward_speed = FORWARD_SLOW_SPEED;
		//provo minore =??
		if((current_env.scans[0].dist_in > 0)&&(current_env.scans[1].dist_in > 0)&&(current_env.scans[2].dist_in > 0)){
			if(((current_env.scans[0].dist_in <= MIN_DISTANCE+1)||(current_env.scans[1].dist_in <= MIN_DISTANCE)||(current_env.scans[2].dist_in <= MIN_DISTANCE+1))
			||(current_env.pat != front_case))
				counter_moves = event + 1;
		}
	}
	else if(isSelfGoverning){
		stopme=random(4);
		switch(stopme){
			case 0:
			case 1: randomStop=false; break;
			case 2:
			case 3: randomStop=true;  break;
		}
//		if(isLighten && okLeds){
//			currentAnimation = "stable";
//			currentColor = "green";
//			lightUpLeds(currentColor,currentAnimation);
//			isLighten=false;
//		}
		if(current_env.scans[0].dist_in > 0){
			if(((current_env.scans[0].dist_in <= MIN_DISTANCE+1)||(current_env.scans[1].dist_in <= MIN_DISTANCE)||(current_env.scans[2].dist_in <= MIN_DISTANCE+1))
			||(allowed_distance >= 70 and randomStop and _oimiPid.getPosX() >= allowed_distance/1.5)){
/*			if(
			(
			(current_env.scans[0].dist_in <= MIN_DISTANCE &&
			current_env.scans[1].dist_in > MIN_DISTANCE &&
			current_env.scans[1].dist_in <= VERY_CLOSE_DISTANCE
			)
			||
			(current_env.scans[2].dist_in <= MIN_DISTANCE &&
			current_env.scans[1].dist_in > MIN_DISTANCE &&
			current_env.scans[1].dist_in <= VERY_CLOSE_DISTANCE
			)
			||
			current_env.scans[1].dist_in <= MIN_DISTANCE
			)
			|| (allowed_distance >= 70 and randomStop and _oimiPid.getPosX() >= allowed_distance/1.5))
    		{*/
				//okLeds = true;
				//setIsMoving(false); errore metterlo qui
				//stopMoving();
				counter_moves = event + 1;
				if(allowed_distance >= 70 && randomStop && _oimiPid.getPosX() >= allowed_distance/1.5){
					//Serial.print("RANDOMSTOP???? HERE!!!!!!!!!!!");
					hasStopped = 1;
				}
				//Serial.print("GETPOSE -->");
				////Serial.print(_oimiPid.getPosX());
				//Serial.print(F("allowed distance is: "));
				////Serial.print(allowed_distance);
				//colora(_strip.Color(0,0,0)); //rifare con animation
			}
		}
	}
	//setIsMoving(true); //? ok here??
	//Serial.print(F("RunGoStraight at speed: "));
	////Serial.print(actual_forward_speed);
	_oimiPid.nonHolonomicRun(actual_forward_speed,0.0f);
	//if(isLighten){
	//	colora(_strip.Color(255,0,0)); //rifare con animation
	//	isLighten=false;
	//}
}
void Movements::goStraight(){
/*Movements forward until (and sometimes before) a front obstacle detection
*/
	//Serial.println(F("gostraight no param"));
//	if(okmanda){
		//Serial.print("<frontmov>");
//		okmanda = false;
//	}
	//current_env = _sonars.getCurrentEnv();
	goingAhead = true;
	_sonars.setGoingAhead(goingAhead);
	if(!isFollowing && !isSelfGoverning){
		if((current_env.scans[0].dist_in > 0)&&(current_env.scans[1].dist_in > 0)&&(current_env.scans[2].dist_in > 0)){
			if((current_env.scans[0].dist_in <= MIN_DISTANCE+1)||(current_env.scans[1].dist_in <= MIN_DISTANCE)||(current_env.scans[2].dist_in <= MIN_DISTANCE+1)){
				stopMoving();
			}
		}
	}
	else if(isFollowing && isSafe){
		chooseForSpeedsAndDistance(current_env.scans[1]);
		actual_forward_speed = FORWARD_SLOW_SPEED;
		if((current_env.scans[0].dist_in > 0)&&(current_env.scans[1].dist_in > 0)&&(current_env.scans[2].dist_in > 0)){
			if(((current_env.scans[0].dist_in <= MIN_DISTANCE+1)||(current_env.scans[1].dist_in <= MIN_DISTANCE)||(current_env.scans[2].dist_in <= MIN_DISTANCE+1))
			||(_oimiPid.getPosX() > allowed_distance/15 && current_env.scans[1].status > big)
			||(_oimiPid.getPosX() > allowed_distance/11 && current_env.scans[1].status < big)){
				stopMoving();
			}
		}
	}
	else if(isFollowing){
		actual_forward_speed = FORWARD_SLOW_SPEED;
		if((current_env.scans[0].dist_in > 0)&&(current_env.scans[1].dist_in > 0)&&(current_env.scans[2].dist_in > 0)){
			if(((current_env.scans[0].dist_in <= MIN_DISTANCE+1)||(current_env.scans[1].dist_in <= MIN_DISTANCE)||(current_env.scans[2].dist_in <= MIN_DISTANCE+1))
			||(current_env.pat != front_case))
				stopMoving();
		}
	}
	else if(isSelfGoverning){
		stopme=random(4);
		switch(stopme){
			case 0:
			case 1: randomStop=false; break;
			case 2:
			case 3: randomStop=true; break;
		}
//		if(isLighten && okLeds){
//			currentAnimation = "stable";
//			currentColor = "green";
//			lightUpLeds(currentColor,currentAnimation);
//			isLighten=false; //useless???
//		}
		if(current_env.scans[0].dist_in > 0){
/*			if(
			(
			(current_env.scans[0].dist_in <= MIN_DISTANCE &&
			current_env.scans[1].dist_in > MIN_DISTANCE &&
			current_env.scans[1].dist_in <= VERY_CLOSE_DISTANCE
			)
			||
			(current_env.scans[2].dist_in <= MIN_DISTANCE &&
			current_env.scans[1].dist_in > MIN_DISTANCE &&
			current_env.scans[1].dist_in <= VERY_CLOSE_DISTANCE
			)
			||
			current_env.scans[1].dist_in <= MIN_DISTANCE
			)
			|| (allowed_distance >= 70 and randomStop and _oimiPid.getPosX() >= allowed_distance/1.5))
    		{*/

//		if((current_env.scans[0].dist_in > 0)&&(current_env.scans[1].dist_in > 0)&&(current_env.scans[2].dist_in > 0)){
			if(((current_env.scans[0].dist_in <= MIN_DISTANCE+1)||(current_env.scans[1].dist_in <= MIN_DISTANCE)||(current_env.scans[2].dist_in <= MIN_DISTANCE+1))
			||(allowed_distance >= 70 and randomStop and _oimiPid.getPosX() >= allowed_distance/1.5)){
				if(allowed_distance >= 70 && randomStop && _oimiPid.getPosX() >= allowed_distance/1.5){
					hasStopped = 1;
				}
				okLeds=true;
				//setIsMoving(false); errore metterlo qui!
				stopMoving();
			}
		}
	}
	okLeds=false;
	//Serial.print(F("RunGoStraight at speed: "));
	////Serial.print(actual_forward_speed);
	_oimiPid.nonHolonomicRun(actual_forward_speed,0.0f);
	//okmanda = false;
}
void Movements::goBack(){
/*Movements backward in reverse until (and sometimes before) back hurdle is detected
*/
	//Serial.println(F("goback"));

//	if(okmanda){
//		Serial.println("<backmov>");
//		okmanda = false;
//	}
//	Serial.print("<backmov");
//	Serial.println(">");

	//current_env = _sonars.getCurrentEnv();
	goingAhead = false;
	_sonars.setGoingAhead(goingAhead);
	stopme=random(4);
	switch(stopme){
		case 0:
		case 1: randomStop=false; break;
		case 2:
		case 3: randomStop=true; break;
	}
//	if(isLighten && okLeds){
//		currentAnimation = "stable";
//		currentColor = "green";
//		lightUpLeds(currentColor,currentAnimation);
//		isLighten=false; //useless???
//	}
	//repetition necessary??? contact or distance?? state vs dis_in? (with < =!!)
	if((current_env.scans[0].dist_in > 0)&&(current_env.scans[1].dist_in > 0)&&(current_env.scans[2].dist_in > 0)){
		if((current_env.scans[3].status == contact) || (current_env.scans[3].dist_in <= MIN_DISTANCE) 
        || (allowed_distance >= 70 and randomStop and -(_oimiPid.getPosX()) >= allowed_distance/2)){
			if(allowed_distance >= 70 && randomStop && _oimiPid.getPosX() >= allowed_distance/1.5){
				hasStopped = 1;
			}
			//Serial.print("GETPOSE -->");
			////Serial.print(_oimiPid.getPosX());
			//Serial.print(F("allowed distance is: "));
			////Serial.print(allowed_distance);
			stopMoving();
		}
	}
//	else{
		//setIsMoving(true);
		//Serial.print(F("RunGoBack at speed: "));
		//Serial.print(actual_forward_speed);
		_oimiPid.nonHolonomicRun(-actual_forward_speed,0.0f);
		//okmanda = false;
//	}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Translations */
/*
N.B. blind spot alert!
Translate until allowed Y distance is reached,
case left,right is based on right-triangle theorems (60') on left/right sonars' outputs
case frontleft/frontright is based on sonars' outputs
case backleft/backright is based on right-triangle theorems (45') on back sonars' outputs
*/
void Movements::translateLeft(){
	////Serial.print(F("translating left"));
	//current_env = _sonars.getCurrentEnv();
	detected_dist = chooseForSpeedsAndDistance(current_env.scans[0]);
	actual_forward_speed = FORWARD_SLOW_SPEED;
	allowed_distance = detected_dist * cos(-PI/6);
	//Serial.print(F("allowed_distance is: "));
	////Serial.print(allowed_distance);
	if(current_env.scans[0].dist_in<MIN_DISTANCE || (startPosY + abs(_oimiPid.getPosY())<= allowed_distance)){
		if(isTranslating){
			calcStrafeForwardSpeeds(0.5, -1, 0);
			isTranslating = 0;
		}
		_oimiPid.holonomicRun(strafe,forward,angular);
	}
	else{
		isTranslating = 1;
		stopMoving();
	}
}
void Movements::translateRight(){
	////Serial.print(F("translating right"));
	//current_env = _sonars.getCurrentEnv();
	detected_dist = chooseForSpeedsAndDistance(current_env.scans[2]);
	actual_forward_speed = FORWARD_SLOW_SPEED;
	allowed_distance = detected_dist * cos(PI/6);
	//Serial.print(F("allowed_distance is: "));
	////Serial.print(allowed_distance);
	if((current_env.scans[0].dist_in<MIN_DISTANCE)||(startPosY + abs(_oimiPid.getPosY())<= allowed_distance)){
		if(isTranslating){
		calcStrafeForwardSpeeds(0.5, 1, 0);
		isTranslating = 0;
		}
		_oimiPid.holonomicRun(strafe,forward,angular);
	}else{
		isTranslating = 1;
	 	stopMoving();
	 }
}
void Movements::translateFrontLeft(){
	////Serial.print(F("translating front left"));
	int dist1 = chooseForSpeedsAndDistance(current_env.scans[0]);
	int dist2 = chooseForSpeedsAndDistance(current_env.scans[1]);
	////Serial.print(F("dist1 comparing:"));
	////Serial.print(dist1);
	////Serial.print(F("dist2 comparing:"));
	////Serial.print(dist2);
	allowed_distance = std::min(dist1,dist2);
	actual_forward_speed = FORWARD_SLOW_SPEED;
	////Serial.print(F("allowed_distance:"));
	////Serial.print(allowed_distance);
	//current_env = _sonars.getCurrentEnv();
	if((current_env.scans[0].dist_in<MIN_DISTANCE)||(current_env.scans[1].dist_in<MIN_DISTANCE)||(startPosY + abs(_oimiPid.getPosY())<= allowed_distance)){
		if(isTranslating){
		calcStrafeForwardSpeeds(0.5, -0.5, 0.5);
		isTranslating = 0;
		}
		_oimiPid.holonomicRun(strafe,forward,angular);
	}else{
		isTranslating = 1;
	 	stopMoving();
	 }
}
void Movements::translateFrontRight(){
	////Serial.print(F("translating front right"));
	int dist1 = chooseForSpeedsAndDistance(current_env.scans[0]);
	int dist2 = chooseForSpeedsAndDistance(current_env.scans[2]);
	////Serial.print(F("dist1 comparing:"));
	////Serial.print(dist1);
	////Serial.print(F("dist2 comparing:"));
	////Serial.print(dist2);

	allowed_distance = std::min(dist1,dist2);
	////Serial.print(F("allowed_distance:"));
	////Serial.print(allowed_distance);
	actual_forward_speed = FORWARD_SLOW_SPEED;
	//current_env = _sonars.getCurrentEnv();
	if((current_env.scans[1].dist_in<MIN_DISTANCE)||(current_env.scans[2].dist_in<MIN_DISTANCE)||(startPosY + abs(_oimiPid.getPosY())<= allowed_distance)){
		if(isTranslating){
			calcStrafeForwardSpeeds(0.5, 0.5, 0.5);
			isTranslating = 0;
		}
////Serial.print
		_oimiPid.holonomicRun(strafe,forward,angular);
	}else{
		isTranslating = 1;
		stopMoving();
	}
}
void Movements::translateBackLeft(){
	////Serial.print(F("translating back left"));
	//////Serial.print(F("allowed_distance:"));
	//////Serial.print(allowed_distance);
	//current_env = _sonars.getCurrentEnv();
	int dis1 = chooseForSpeedsAndDistance(current_env.scans[4]);
	int dis2 = chooseForSpeedsAndDistance(current_env.scans[0]);
	allowed_distance = std::min(dis1,dis2);
	actual_forward_speed = FORWARD_SLOW_SPEED;
	////Serial.print(F("dist1 comparing:"));
	////Serial.print(dis1);
	////Serial.print(F("dist2 comparing:"));
	////Serial.print(dis2);
	////Serial.print(F("allowed_distance:"));
	////Serial.print(allowed_distance);
	if((current_env.scans[0].dist_in<MIN_DISTANCE)||(current_env.scans[3].dist_in<MIN_DISTANCE)||(startPosY + abs(_oimiPid.getPosY())<= allowed_distance)){
		if(isTranslating){
		calcStrafeForwardSpeeds(0.5, -0.5, -0.5);
		isTranslating = 0;
		}
		_oimiPid.holonomicRun(strafe,forward,angular);
	}else{
		isTranslating = 1;
		stopMoving();
	}
}
void Movements::translateBackRight(){
	////Serial.print(F("translating back right"));
	//////Serial.print(F("allowed_distance:"));
	//////Serial.print(allowed_distance);
	//current_env = _sonars.getCurrentEnv();
	int dis1 = chooseForSpeedsAndDistance(current_env.scans[4]);
	int dis2 = chooseForSpeedsAndDistance(current_env.scans[2]);
	allowed_distance = std::min(dis1,dis2);
	actual_forward_speed = FORWARD_SLOW_SPEED;
	////Serial.print(F("dist1 comparing:"));
	////Serial.print(dis1);
	////Serial.print(F("dist2 comparing:"));
	////Serial.print(dis2);
	////Serial.print(F("allowed_distance:"));
	////Serial.print(allowed_distance);
	if((current_env.scans[2].dist_in<MIN_DISTANCE)||(current_env.scans[3].dist_in<MIN_DISTANCE)||(startPosY + abs(_oimiPid.getPosY())<= allowed_distance)){
		if(isTranslating){
			calcStrafeForwardSpeeds(0.5, 0.5, -0.5);
			isTranslating = 0;
		}
		_oimiPid.holonomicRun(strafe,forward,angular);
	}else{
		isTranslating = 1;

		stopMoving();
	}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Complex movements */
//void Movements::rotate(float dTh, float speedTh, float speedX, int event, bool type) {

//Rotation without stopping
//Basis used by predefinite emotional movements
//if positive with minus became negative, otherwise remains negative...
//using abs no sense in having two right??
//*/
/*	if(!type){
		if (speedTh > 0) {
			if(abs(_oimiPid.getPosTh()) < dTh && counter_moves == event)
				_oimiPid.nonHolonomicRun(speedX, -speedTh);
			else if (counter_moves == event)
				counter_moves = event + 1;
		}
		else if (speedTh < 0) {
			if(abs(_oimiPid.getPosTh()) < dTh && counter_moves == event)
				_oimiPid.nonHolonomicRun(speedX, -speedTh);
			else if (counter_moves == event)
				counter_moves = event + 1;
			}
	}
	else{
		if (speedTh > 0) {
			if((millis() < rotationTime + rotationInterval)  && counter_moves == event)
				_oimiPid.nonHolonomicRun(speedX, -speedTh);
			else if (counter_moves == event)
				counter_moves = event + 1;
		}
		else if (speedTh < 0) {
			if((millis() < rotationTime + rotationInterval) && counter_moves == event)
				_oimiPid.nonHolonomicRun(speedX, -speedTh);
			else if (counter_moves == event)
				counter_moves = event + 1;
			}
	}
}*/

//void Movements::rotate(float dTh, float speedTh, float speedX, int event, bool type) {
/*
Rotation without stopping
Basis used by predefinite emotional movements
if positive with minus became negative, otherwise remains negative...
using abs no sense in having two right??
*/
//	if(!type){
//		if(abs(_oimiPid.getPosTh()) < dTh && counter_moves == event)
//			_oimiPid.nonHolonomicRun(speedX, -speedTh);
//		else if (counter_moves == event)
//			counter_moves = event + 1;
//	}
//	else{
//		if((millis() < rotationTime + rotationInterval) && counter_moves == event)
///			_oimiPid.nonHolonomicRun(speedX, -speedTh);
//		else if (counter_moves == event)
//			counter_moves = event + 1;
//		if((millis() < rotationTime + rotationInterval) && counter_moves == event)
//			_oimiPid.nonHolonomicRun(speedX, -speedTh);
//		else if (counter_moves == event)
//			counter_moves = event + 1;
//	}
	/*
	if(type==2){
		if(freePath && counter_moves == event)
			_oimiPid.nonHolonomicRun(speedX, -speedTh);
		else if (counter_moves == event)
			counter_moves = event + 1;
	}
	*/
//}

void Movements::rotate(float dTh, float speedTh, float speedX, int event, int type) {
/*
Rotation without stopping
Basis used by predefinite emotional movements
if positive with minus became negative, otherwise remains negative...
using abs no sense in having two right??
*/
//	if(type==0){
//		Serial.print("Pose is: ");
//		Serial.println(_oimiPid.getPosTh());
//		Serial.print("countermovesis: ");
//		Serial.println(_oimiPid.getPosTh());
/*		if(abs(_oimiPid.getPosTh()) < dTh && counter_moves == event)
			_oimiPid.nonHolonomicRun(speedX, -speedTh);
		else if (counter_moves == event){
			Serial.println("fanculo1");
			counter_moves = event + 1;
		}else if(abs(_oimiPid.getPosTh()) >= dTh)
			Serial.println("fanculo2");
}*/
/*ok but wrong angle
		if(speedTh > 0){
			if(_oimiPid.getPosTh() < dTh && counter_moves == event)
				_oimiPid.nonHolonomicRun(speedX, -speedTh);
			else if (counter_moves 	== event){
				Serial.println("fanculo1");
				counter_moves = event + 1;
			}
		}
		else if(speedTh < 0){
			if(_oimiPid.getPosTh() > dTh && counter_moves == event)
				_oimiPid.nonHolonomicRun(speedX, -speedTh);
			else if (counter_moves == event){
				Serial.println("fanculo1");
				counter_moves = event + 1;
			}
		}*/
	if(!type && (isFollowing || isSelfGoverning)){
  		if(speedTh > 0){
			//Serial.println("siamo dentro nel caso 1");
	  		if((_oimiPid.getPosTh() < dTh) && counter_moves == event)
				_oimiPid.nonHolonomicRun(speedX, -speedTh);
			else if (counter_moves == event) counter_moves = event + 1;
		}
		else if(speedTh < 0){
			//Serial.println("siamo dentro nel caso 2");
			if((_oimiPid.getPosTh() > dTh)){ //&& counter_moves == event){
				_oimiPid.nonHolonomicRun(speedX, -speedTh);
				//Serial.println("ok fatto");
			}
			else if (counter_moves == event) counter_moves = event + 1;
		}
	}
///		if(abs(_oimiPid.getPosTh()) < dTh && counter_moves == event)
//			_oimiPid.nonHolonomicRun(speedX, -speedTh);
//		else if (counter_moves == event)
//			counter_moves = event + 1; }
	if(!type && !isFollowing && !isSelfGoverning){
		if(speedTh > 0){
			if((_oimiPid.getPosTh() < (dTh + last_pose)) && counter_moves == event)
				_oimiPid.nonHolonomicRun(speedX, -speedTh);
			else if (counter_moves 	== event){
				counter_moves = event + 1;
				last_pose = _oimiPid.getPosTh();
			}
		}
		else if(speedTh < 0){
			if((_oimiPid.getPosTh() > (dTh - last_pose)) && counter_moves == event)
				_oimiPid.nonHolonomicRun(speedX, -speedTh);
			else if (counter_moves == event){
				counter_moves = event + 1;
				last_pose = _oimiPid.getPosTh();
			}
		}

	}
	else if(type){
		if((millis() < rotationTime + rotationInterval) && counter_moves == event)
			_oimiPid.nonHolonomicRun(speedX, -speedTh);
		else if(counter_moves == event)
			counter_moves = event + 1;
	}
	else if(type==2){
		if((current_env.scans[0].status < close
		&& current_env.scans[1].status < close
		&& current_env.scans[2].status < close)
		&& counter_moves==event){
			_oimiPid.nonHolonomicRun(speedX, -speedTh);
			//Serial.print("ENOUGH!!!");
		}
		else if (counter_moves == event)
			counter_moves = event + 1;
	}
}

void Movements::proceed(float allowed, int side, int event) {
/*
for now only left,right translation
Basis used by predefinite emotional movements
*/
	if (side == 0) { //left
		////Serial.print("left!");
		if ((startPosY + abs(_oimiPid.getPosY()) <= allowed)  && counter_moves == event){
			//_oimiPid.runM(-1.8f,6.6f,-5.5f);
			//_oimiPid.runM(-1.5f,6.8f,-5.3f);
			calcStrafeForwardSpeeds(0.5, -1,0);
			_oimiPid.holonomicRun(strafe,forward,angular);
		}
    	else if (counter_moves == event){
    		////Serial.print(F("incremento event"));
    		counter_moves = event + 1;
    		_oimiPid.setPosY(0);
    		startPosY = 0;
    	}
    }
    else if (side == 1) { //right
		////Serial.print("right!");
		if ((startPosY + abs(_oimiPid.getPosY()) <= allowed)  && counter_moves == event){
			//_oimiPid.runM(1.8f,-6.6f,5.5f);
			//_oimiPid.runM(1.5f,-6.8f,5.3f);
			calcStrafeForwardSpeeds(0.5, 1,0);
			_oimiPid.holonomicRun(strafe,forward,angular);
	    }
	    else if (counter_moves == event){
    		////Serial.print(F("incremento event"));
    		counter_moves = event + 1;
	    	_oimiPid.setPosY(0);
	    	startPosY = 0;
		}
	}
	else if (side == 2) { //front
		////Serial.print("left!");
		if ((startPosY + abs(_oimiPid.getPosX()) <= allowed)  && counter_moves == event){
			//_oimiPid.runM(-1.8f,6.6f,-5.5;
			//_oimiPid.runM(-1.5f,6.8f,-5.3);
			calcStrafeForwardSpeeds(0.5, 0,1);
			_oimiPid.holonomicRun(strafe,forward,angular);
		}
    	else if (counter_moves == event){
    		////Serial.print(F("incremento event"));
    		counter_moves = event + 1;
    		_oimiPid.setPosX(0);
    		startPosX = 0;
    	}
    }
    else if (side == 3) { //back
		////Serial.print("right!");
		if ((startPosY + abs(_oimiPid.getPosX()) <= allowed)  && counter_moves == event){
			//_oimiPid.runM(1.8f,-6.6f,5.5f);
			//_oimiPid.runM(1.5f,-6.8f,5.3f);
			calcStrafeForwardSpeeds(0.5, 0,-1);
			_oimiPid.holonomicRun(strafe,forward,angular);
	    }
	    else if (counter_moves == event){
    		////Serial.print(F("incremento event"));
    		counter_moves = event + 1;
	    	_oimiPid.setPosX(0);
	    	startPosX = 0;
		}
	}
}
///////////no sonars control???
void Movements::goForwardUntil(float distance, float speed) {
/*Movements on for a determinated range*/
	////Serial.print(F("Go forward for"));
	////Serial.print(distance);
	if (_oimiPid.getPosX() < startPosX + distance)
  		_oimiPid.nonHolonomicRun(speedX, 0.0);
	else{
//		singleTrial = true;
		stopMoving();
	}
}

void Movements::goBackwardUntil(float distance,float speed) {
/*Movements reverse for a specific range*/
	////Serial.print(F("Go backward for"));
	////Serial.print(distance);
	if (_oimiPid.getPosX() > startPosX - distance)
		_oimiPid.nonHolonomicRun(-speed, 0.0);
	else
		stopMoving();
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Complete instructions */
//Left
void Movements::moveLeft(){
	//Serial.println(F("Doing a LEFTmove!"));
  	lastDirection = left;
	//forbidden_movement = left; li ho rimossi da tutti quanti!
	//current_env = _sonars.getCurrentEnv();
	allowed_distance = chooseForSpeedsAndDistance(current_env.scans[0]);
	//setAutonomousFirst(true); errore metterlo qui!!
	rand=random(10);
	//more often 30,until and quick
	switch(rand){
        case 0:
        case 1:
        	controlloforbidden=true;
        	actual_movement = turn35_l;	     break;
        case 2:
        case 3:
        case 4:
        	controlloforbidden=true;
			actual_movement = turn_until_l;	     break;
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
	        rotateType = 0;
	        actual_movement = go_left;
        	//actual_movement = turn35_l;	     break;
        break;
    }
}
//Front
void Movements::moveForward(){
    //serve piu che altro per settare la distance in modo che non la cambi continuamente da qui non ripassa più
  	lastDirection = front;
    //Serial.println(F("Doing a FRONTmove!"));
	//current_env=_sonars.getCurrentEnv();
	allowed_distance = chooseForSpeedsAndDistance(current_env.scans[1]);
	//Serial.print(F("allowed distance is: "));
	////Serial.print(allowed_distance);
	setAutonomousFirst(false); //useless in theory here
	actual_movement = go_straight;
}
//Right
void Movements::moveRight(){
/*

*/
  	//Serial.println(F("Doing a RIGHTmove!"));
  	lastDirection = right;
	//current_env=_sonars.getCurrentEnv();
	allowed_distance = chooseForSpeedsAndDistance(current_env.scans[2]);
	//setAutonomousFirst(true); errore here
    rand=random(10);
	//more often 30,until and quick
	switch(rand){
        case 0:
        case 1:
	        controlloforbidden=true;
			actual_movement = turn35_r;	     break;
        case 2:
        case 3:
        case 4:
        	controlloforbidden=true;
    	    actual_movement = turn_until_r;	     break;
		case 5:
        case 6:
        case 7:
        case 8:
        case 9:
			rotateType = 0;
        	actual_movement = go_right;
			//actual_movement = turn35_r;	     break;
        break;
    }
}
//Back
void Movements::moveBackward(){
/*
Choose among going backward directly or first turning 180' and then going straight
*/
	//Serial.println(F("Doing a BACKMove!"));
	//current_env=_sonars.getCurrentEnv();
	allowed_distance = chooseForSpeedsAndDistance(current_env.scans[3]);
	//Serial.print(F("allowed distance is: "));
	////Serial.print(allowed_distance);
	backchoice = random(5);
	backverse = random(2);
	backangle = random(6);
	switch(backchoice){
		case 0:
		case 1:
		case 2:
			lastDirection = none;
			setAutonomousFirst(true);
			switch (backverse){
				switch(backangle){
					case 0: actual_movement = turn180_l; break;
					case 1: actual_movement = turn180_r; break;
					case 2: actual_movement = turn150_l; break;
					case 3: actual_movement = turn150_r; break;
					case 4: actual_movement = turn_until_bl; break;
					case 5: actual_movement = turn_until_br; break;
				}
			}
		break;
		case 3:
		case 4:
			lastDirection = back;
			actual_movement = go_back;
		break;
	}
}

void Movements::goLeft(int type){
/*
Implement a different strategy with counter, avoiding stops
*/
	//Serial.println("<leftmov>");
	//Serial.println(F("GOING QUICKLEFT 30!"));
	rotate(-PI/5,  -ANGULAR_NORMAL_SPEED, 0.0, 0, type);
	//redundent no? actual_movement = go_left;
	if (counter_moves==1)  goStraight(1);
	if (counter_moves==2){
		stopMoving(); //here theres the reset of posTh
	}
}
void Movements::goRight(int type) {
/*
Implement a different strategy with counter, avoiding stops
*/
	//Serial.println("<rightmov>");
	//Serial.println(F("GOING QUICKRIGHT 30!"));
	rotate(PI/5,  ANGULAR_NORMAL_SPEED, 0.0, 0, type);
	//redundant actual_movement = go_right;
	if (counter_moves==1)  goStraight(1);
	if (counter_moves==2){
		stopMoving(); //here theres the reset of posTh
	}
}
//void Movements::goLeft(float angle){
/*
Implement a different strategy with counter, avoiding stops
left here is negative, on the contrary of turn..change at least the 0rotate() function
*/
	////Serial.print(F("GOING QUICKLEFT FOR"));
	////Serial.print(angle);
/*	bool thisType = false;
	rotate(angle,  -ANGULAR_QUICK_SPEED, 0.0, 0, thisType);
	if (counter_moves==1)  goStraight(1);
	if (counter_moves==2){
		//actual_movement = following;
		stopMoving(); //here theres the reset of posTh
	}
}*/
//void Movements::goRight(float angle){
/*
Implement a different strategy with counter, avoiding stops
*/
	//Serial.print(F("GOING QUICKRIGHT FOR "));
	////Serial.print(angle);
/*	bool thisType = false;
	rotate(angle,  ANGULAR_QUICK_SPEED, 0.0, 0, thisType);
	if (counter_moves==1)  goStraight(1);
	if (counter_moves==2){
		//actual_movement = following;
		stopMoving(); //here theres the reset of posTh
	}
}*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////
void Movements::goLeft(float angle){
/*
Implement a different strategy with counter, avoiding stops
left here is negative, on the contrary of turn..change at least the rotate() function
*/
	//Serial.println("<leftmov2>");
	////Serial.print(F("GOING QUICKLEFT FOR"));
	////Serial.print(angle);
	int thisType = false;
	rotate(angle,  -ANGULAR_NORMAL_SPEED, 0.0, 0, thisType);
	if (counter_moves==1)  goStraight(1);
	if (counter_moves==2){
		//actual_movement = following;
		stopMoving(); //here theres the reset of posTh
	}
}
void Movements::goRight(float angle){
/*
Implement a different strategy with counter, avoiding stops
*/
	//Serial.println("<rightmov2>");
	//Serial.print(F("GOING QUICKRIGHT FOR "));
	////Serial.print(angle);
	int thisType = false;
	rotate(angle,  ANGULAR_NORMAL_SPEED, 0.0, 0, thisType);
	if (counter_moves==1)  goStraight(1);
	if (counter_moves==2){
		//actual_movement = following;
		stopMoving(); //here theres the reset of posTh
	}
}

/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Autonomous Movements */
void Movements::turnDuetoHurdle(){
/*
Turn around when there are obstacles everywhere in search of a clear way,
when robot is sourrounded by obstacles, namely when all sonars detect obstacles
Next time avoiding to turn back towards blocked path, continue turning in same direction instead
Choices
	Turns 90 degrees and Turns 150 degrees to have a new position in which robot is looking at different path respect old position
	Turns until path becomes free
Avoids turning againg towards opposite direction (and thus returning in the old position), when is trying to excape from a "trap"
Chooses turn-until strategy more often
*/
	//Serial.print(F("turn cause red everywhere! "));
	if(isSelfGoverning)
		auto_hurdle = true;
	if(isFollowing)
		follo_hurdle = true;
	int choice = random(10);
	int selection_l = random(5);
	int selection_r = random(5);
	okmanda = true;
	if(!wasBlocked){
		wasBlocked = true; //error to besetted in stopmoving
		switch(choice){
			case 0: actual_movement = turn90_l;	    break;
			case 1: actual_movement = turn90_r;     break;
			case 2: actual_movement = turn150_l;    break;
			case 3: actual_movement = turn150_r;    break;
			case 4:
			case 6:
			case 8: actual_movement = turn90_l;break;
			//case 8: actual_movement = turn_until_l; break;
			case 5:
			case 7:
			case 9: actual_movement = turn90_r;break;
			//case 9: actual_movement = turn_until_r; break;
		}
		old_choice = actual_movement;
	}
	else{
		wasBlocked = false;
		if(!(old_choice & 1)){ //check if old_choice is even, looking at last bit with bitwise repr
			switch(selection_l){
				case 0: actual_movement = turn90_l;	    break;
				case 1: actual_movement = turn150_l;    break;
				case 2:
				case 3:
				case 4: actual_movement = turn90_l;		break;
				//case 4: actual_movement = turn_until_l; break;
			}
		}
		else{
			switch(selection_r){
				case 0: actual_movement = turn90_r;	    break;
				case 1: actual_movement = turn150_r;    break;
				case 2:
				case 3:
				case 4: actual_movement = turn90_r;		break;
				//case 4: actual_movement = turn_until_r; break;
			}
		}
	}
}
void Movements::changeOrientation(){
/*
Change casually direction while robot is moving autonomously
*/
	okmanda = true;
	if(auto_variation)
		stopMoving();
	else{
		auto_change = true;
		int choice = random(10);
		forbidden_movement = none; //reset
		////Serial.print(F("turn without a precise reason! "));
		////Serial.print(F("turn without a precise reason! "));
		switch(choice){
			case 0: actual_movement = turn60_l;   break;
			case 1: actual_movement = turn60_r;   break;
			case 2: actual_movement = turn90_l;   break;
			case 3: actual_movement = turn90_r;   break;
			case 4: actual_movement = turn120_l;  break;
			case 5: actual_movement = turn120_r;  break;
			case 6: actual_movement = turn150_l;  break;
			case 7: actual_movement = turn150_r;  break;
			case 8: actual_movement = turn180_l;  break;
			case 9: actual_movement = turn180_r;  break;
		}
	}
}
//void Movements::goNormally(){
/*
Movements around avoiding obstacles
Follow freer path in order of allowed according to sonars detections
Favour most free way more often
Avoiding repetition of same movement as before
Turn if all paths are blocked
la forbidden funziona sempre è l'ultimo controllo in questo caso e cambia allowed
pero allowed cambia dipende,
non sta seguendo il più free sempre ma solo più frequentemente
*/
	//current_env = _sonars.getCurrentEnv();
	//if(auto_change)
	//    set_auto_change(true);

	//_sonars.printOnlyScans();
	//Serial.print("FORBIDDENISIS");
	//Serial.println(forbidden_movement);
/*	int casual= 0;
	switch(current_env.length){
		case 0:
			turnDuetoHurdle(); break;
		case 1:
			//Serial.print("alloweeeen ");
			allowed = current_env.allowed.at(0);
			///Serial.println("directionnn!!!!");
			//Serial.println(allowed.direction);
			//Serial.println();
			//metto anche qui due to hardle x girare e nn tornare indietro se sto andando avanti e metto ostacolo prima di tornare
		    //nuovamente indietro?
//			if(allowed.direction==forbidden_movement){
//				casual=random(3);
//				switch(casual){
//					case 0:
//					case 1:	allowed = current_env.allowed.at(0);  //qui mene sbatto del forbidden  perke c'è un unica direzione disponibile
//					case 2: turnDuetoHurdle(); //per adesso dovrebbe andare bene ...cmq dipende da Sonars.allowed come mette allowed
//				}

		break;
		case 2:
			casual=random(3);
			switch(casual){
				case 0:
				case 1:
					allowed = current_env.allowed.at(0);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(1); break;
				case 2:
					allowed = current_env.allowed.at(1);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(0); break;
			}
		break;
		case 3:
			casual=random(7);
			switch(casual){
				case 0:
				case 1:allowed = current_env.allowed.at(0);
					if(allowed.direction==forbidden_movement) //posso metter un altro switch ed evito ripetizione
						allowed = current_env.allowed.at(1);
				break;
				case 2:allowed = current_env.allowed.at(0);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(2);
				break;
				case 3:allowed = current_env.allowed.at(1);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(0);
				break;
				case 4:allowed = current_env.allowed.at(1);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(2);
				break;
				case 5:allowed = current_env.allowed.at(2);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(0);
				break;
				case 6:allowed = current_env.allowed.at(2);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(1);
				break;
				}
		break;
		case 4:
			//evito allowed_4??no!
			casual=random(14);
			switch(casual){
				case 0:
				case 1:allowed = current_env.allowed.at(0);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(1);
				break;
				case 2:
				case 3:allowed = current_env.allowed.at(0);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(2);
				break;
				case 4:allowed = current_env.allowed.at(0);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(3);
				break;
				case 5:allowed = current_env.allowed.at(1);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(0);
				break;
				case 6:allowed = current_env.allowed.at(1);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(2);
				break;
				case 7:allowed = current_env.allowed.at(2);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(3);
				break;
				case 8:allowed = current_env.allowed.at(2);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(0);
				break;
				case 9:allowed = current_env.allowed.at(2);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(1);
				break;
				case 10:allowed = current_env.allowed.at(2);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(2);
				break;
				case 11:allowed = current_env.allowed.at(3);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(0);
				break;
				case 12:allowed = current_env.allowed.at(3);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(1);
				break;
				case 13:allowed = current_env.allowed.at(3);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(2);
				break;
			}
	}
	if(current_env.length > 0){
		switch(allowed.direction){
			isLighten = true;
			okmanda = true;
			case none:  break;	//never enters here, just for avoiding compile time warning
			case left:   moveLeft();   	 break;
			case front:  moveForward();  break;
			case right:  moveRight();  	 break;
			case back:   moveBackward(); break;
		}
	}
}*/

void Movements::goNormally(){
/*
Movements around avoiding obstacles
Follow freer path in order of allowed according to sonars detections
Favour most free way more often
Avoiding repetition of same movement as before
Turn if all paths are blocked
la forbidden funziona sempre è l'ultimo controllo in questo caso e cambia allowed
pero allowed cambia dipende,
non sta seguendo il più free sempre ma solo più frequentemente
*/
	//current_env = _sonars.getCurrentEnv();
	//if(auto_change)
	//    set_auto_change(true);

	//_sonars.printOnlyScans();
	//Serial.print("FORBIDDENISIS");
	//Serial.println(forbidden_movement);
    int casual= 0;
	int scegli=0;
	switch(current_env.length){
		case 0:
			turnDuetoHurdle(); break;
		case 1:
			//Serial.print("alloweeeen ");
			allowed = current_env.allowed.at(0);
			///Serial.println("directionnn!!!!");
			//Serial.println(allowed.direction);
			//Serial.println();
			//metto anche qui due to hardle x girare e nn tornare indietro se sto andando avanti e metto ostacolo prima di tornare
		    //nuovamente indietro?
//			if(allowed.direction==forbidden_movement){
//				casual=random(3);
//				switch(casual){
//					case 0:
//					case 1:	allowed = current_env.allowed.at(0);  //qui mene sbatto del forbidden  perke c'è un unica direzione disponibile
//					case 2: turnDuetoHurdle(); //per adesso dovrebbe andare bene ...cmq dipende da Sonars.allowed come mette allowed
//				}

		break;
		case 2:
			casual=random(5);
			switch(casual){
				case 0:
				case 1:
				case 2:
					allowed = current_env.allowed.at(0);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(1); break;
				case 3:
				case 4:
					allowed = current_env.allowed.at(1);
					if(allowed.direction==forbidden_movement) allowed = current_env.allowed.at(0); break;
			}
		break;
		case 3:
			scegli = random(2);
			casual=random(4);
			switch(casual){
				case 0:
				case 1:
					allowed = current_env.allowed.at(0);
					if(allowed.direction==forbidden_movement){ //posso metter un altro switch ed evito ripetizione
						switch(scegli){
					 		case 0: allowed = current_env.allowed.at(1); break;
					 		case 1: allowed = current_env.allowed.at(2); break;
						}
					}
				break;
				case 2:
					allowed = current_env.allowed.at(1);
					if(allowed.direction==forbidden_movement){ //posso metter un altro switch ed evito ripetizione
						switch(scegli){
					 		case 0: allowed = current_env.allowed.at(2); break;
					 		case 1: allowed = current_env.allowed.at(0); break;
						}
					}
				break;
				case 3:
					allowed = current_env.allowed.at(2);
					if(allowed.direction==forbidden_movement){ //posso metter un altro switch ed evito ripetizione
						switch(scegli){
					 		case 0: allowed = current_env.allowed.at(1); break;
					 		case 1: allowed = current_env.allowed.at(0); break;
						}
					}
				break;
			}
		break;
		case 4:
			casual=random(5);
			scegli =random(3);
			switch(casual){
				case 0:
				case 1:
					allowed = current_env.allowed.at(0);
					if(allowed.direction==forbidden_movement){
						switch(scegli){
					 		case 0: allowed = current_env.allowed.at(1); break;
					 		case 1: allowed = current_env.allowed.at(2); break;
					 		case 2: allowed = current_env.allowed.at(3); break;
						}
					}
				break;
				case 2:
					allowed = current_env.allowed.at(1);
					if(allowed.direction==forbidden_movement){
						switch(scegli){
					 		case 0: allowed = current_env.allowed.at(2); break;
					 		case 1: allowed = current_env.allowed.at(3); break;
					 		case 2: allowed = current_env.allowed.at(0); break;
						}
					 }
				break;
				case 3:
					allowed = current_env.allowed.at(2);
					if(allowed.direction==forbidden_movement){
						switch(scegli){
					 		case 0: allowed = current_env.allowed.at(3); break;
					 		case 1: allowed = current_env.allowed.at(1); break;
					 		case 2: allowed = current_env.allowed.at(0); break;
						}
					 }
				break;
				case 4:
					allowed = current_env.allowed.at(3);
					if(allowed.direction==forbidden_movement){
						switch(scegli){
					 		case 0: allowed = current_env.allowed.at(2); break;
					 		case 1: allowed = current_env.allowed.at(1); break;
					 		case 2: allowed = current_env.allowed.at(0); break;
						}
					 }
				break;
			}
	}
	if(current_env.length > 0){
		switch(allowed.direction){
			isLighten = true;
			okmanda = true;
			case none:  break;	//never enters here, just for avoiding compile time warning
			case left:  moveLeft();   	break;
			case front: moveForward();  break;
			case right: moveRight();  	break;
			case back:  moveBackward(); break;
		}
	}
}

void Movements::freeMe(){
/*
For going freely in the env
in 1 case among 4 change casually direction instead of following freest path
Avoid to call again ChangeOrientation
  --if it sees at least 2 free path
  --until at least 10 sec (10000 millisec) are passed from last changement
  --if it was recently stuck
*/
	//_sonars.printOnlyScans();
	//_strip.noColor();
	isSelfGoverning = true;
	//current_env = _sonars.getCurrentEnv();
	int rap=random(4);
	switch(lastDirection){
		case none: break;
		case left:
			if(controlloforbidden){
				forbidden_movement = right;
//			    forbidden_movement = none;
				controlloforbidden = false;
			}
			else forbidden_movement = none;
		break;
		case front:
			if(hasStopped){
//				forbidden_movement = none;
				forbidden_movement = front;
				hasStopped = false;
			}
//			else forbidden_movement = back;
			else forbidden_movement = none;
		break;
		case right:
			if(controlloforbidden){
				forbidden_movement = left;
//			    forbidden_movement = none;
				controlloforbidden = false;
			}
			else forbidden_movement = none;
		break;
		case back:
			if(hasStopped){
//				forbidden_movement = none;
				forbidden_movement = back;
				hasStopped = false;
			}
//			else forbidden_movement = front;
			else forbidden_movement = none;
		break;
	}
	switch(rap){
		case 0:
			if((current_env.length>2) && (!wasBlocked) && (millis() - previousTurnTime > 10000)){
				auto_variation = true;
				changeOrientation();
				previousTurnTime = millis();
			}
			else goNormally();
		break;
		case 1:
		case 2:
		case 3: goNormally(); break;
	}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Pattern movements */
/*void Movements::letsDance(){
  set	IsMoving(true);
  rotate( PI / 4,  HAPPY_ANGULAR_SPEED, 0.0f, 0);
  rotate( PI / 4 - PI / 9,  -SCARED_ANGULAR_SPEED, 0.0f, 1);
  rotate( PI / 4 , SCARED_ANGULAR_SPEED, 0.0f, 2);
  rotate( - PI / 4, -HAPPY_ANGULAR_SPEED, 0.0f, 3);
  rotate( - PI / 4 + PI / 9, SCARED_ANGULAR_SPEED, 0.0f, 4);
  rotate( - PI / 4, -SCARED_ANGULAR_SPEED, 0.0f, 5);
  rotate( PI / 4,  HAPPY_ANGULAR_SPEED, 0.0f, 6);
  rotate( PI / 4 - PI / 9,  -SCARED_ANGULAR_SPEED, 0.0f, 7);
  rotate( PI / 4 , SCARED_ANGULAR_SPEED, 0.0f, 8);
  rotate( - PI / 4, -HAPPY_ANGULAR_SPEED, 0.0f, 9);
  rotate( - PI / 4 + PI / 9, SCARED_ANGULAR_SPEED, 0.0f, 10);
  rotate( - PI / 4, -SCARED_ANGULAR_SPEED, 0.0f, 11);
  rotate( PI / 4,  HAPPY_ANGULAR_SPEED, 0.0f, 12);
  rotate( PI / 4 - PI / 9,  -SCARED_ANGULAR_SPEED, 0.0f, 13);
  rotate( PI / 4 , SCARED_ANGULAR_SPEED, 0.0f, 14);
  rotate( - PI / 4, -HAPPY_ANGULAR_SPEED, 0.0f, 15);
  rotate( - PI / 4 + PI / 9, SCARED_ANGULAR_SPEED, 0.0f, 16);
  rotate( - PI / 4, -SCARED_ANGULAR_SPEED, 0.0f, 17);
  rotate( 0.0, HAPPY_ANGULAR_SPEED, 0.0f, 18);
  ////Serial.print("movI is ");
  //////Serial.print(counter_moves);
  if (counter_moves==19) stopMoving();
}
void Movements::okAgree(){
	rotate( PI / 75.0,  SCARED_ANGULAR_SPEED, 0.0f, 0);
	rotate(- PI / 75.0,  - SCARED_ANGULAR_SPEED, 0.0f, 1);
	rotate( PI / 75.0,  SCARED_ANGULAR_SPEED, 0.0f, 2);
	rotate(- PI / 75.0,  - SCARED_ANGULAR_SPEED, 0.0f, 3);
	rotate( 0.0f, SCARED_ANGULAR_SPEED, 0.0f, 4);
	if (counter_moves == 5)
		stopMoving();
}
void Movements::koRefuse(){
	rotate( PI / 75.0,  SCARED_ANGULAR_SPEED, 0.0f, 0);
	rotate(- PI / 75.0,  - SCARED_ANGULAR_SPEED, 0.0f, 1);
	rotate( PI / 75.0,  SCARED_ANGULAR_SPEED, 0.0f, 2);
	rotate(- PI / 75.0,  - SCARED_ANGULAR_SPEED, 0.0f, 3);
	rotate( 0.0f, SCARED_ANGULAR_SPEED, 0.0f, 4);
	if (counter_moves == 5)
		stopMoving();
}
void Movements::actAngry(){
	if (counter_moves == 0) {
		_oimiPid.setKi(2.0);
		_oimiPid.setKp(1.5);
	}
	rotate( 5 * PI / 3,  ANGRY_ANGULAR_SPEED, 0.0f, 0);
	rotate(-4 * PI / 5,  - ANGRY_ANGULAR_SPEED, 0.0f, 1);
	rotate(-2 * PI / 5,  ANGRY_ANGULAR_SPEED, 0.0f, 2);
	rotate(-PI / 5,  - ANGRY_ANGULAR_SPEED, 0.0f, 3);
	rotate(0.0,  ANGRY_ANGULAR_SPEED, 0.0f, 4);
	if (counter_moves == 5) {
		_oimiPid.setDefaultKi();
		_oimiPid.setDefaultKp();
	stopMoving();
	}
}
void Movements::actSad() {
	float angle = 0.0;
	//if(newfla == 0){
	//	lightUpLeds(currentColor,currentAnimation);
	//	newfla =1;
	//}
	//lightUpLeds();
	if(isCustom) angle = customAngle;
	else angle = ANGULAR_NORMAL_SPEED;
	setIsMoving(true);
	//lightUpLeds(currentColor,currentAnimation);
	rotate( PI / 2 , angle, 0.0f, 0);
	rotate( -PI / 2 , -angle, 0.0f, 1);
	rotate( 0.0 , angle, 0.0f, 2);
	if (counter_moves==3) stopMoving();
}
void Movements::actScared(){
	float forwardSpeed = SCARED_FORWARD_SPEED; //PASS VALUE AS PARAMETER OR NOT??
	setIsMoving(true);
	rotate(+ PI / 24.0 , SCARED_ANGULAR_SPEED, forwardSpeed, event);
	rotate(- PI / 24.0 , -SCARED_ANGULAR_SPEED, forwardSpeed, event + 1);
	rotate(+ PI / 24.0 , SCARED_ANGULAR_SPEED, forwardSpeed, event + 2);
	rotate(- PI / 24.0 , -SCARED_ANGULAR_SPEED, forwardSpeed, event + 3);
	rotate(0.0 , SCARED_ANGULAR_SPEED, forwardSpeed, event + 4);
	if (counter_moves==event+5) stopMoving();
}
void Movements::actNormal(){
	//if(newfla == 0){
	//	lightUpLeds(currentColor,currentAnimation);
	//	newfla =1;
	//}
	//lightUpLeds();
	setIsMoving(true);
	//lightUpLeds(currentColor,currentAnimation);
	rotate( PI / 2 , customAngle, 0.0f, 0);
	rotate( -PI / 2 , -customAngle, 0.0f, 1);
	rotate( 0.0 , customAngle, 0.0f, 2);
	if (counter_moves==3) stopMoving();
}
void Movements::actHappy(){
	//////HANDLING CUSTOM SPEEDS OR NOT! IF?
	//lightUpLeds();
	//if(newfla == 0){
	//	lightUpLeds(currentColor,currentAnimation);
	//	newfla = 1;
	//}
	//lightUpLeds(currentColor,currentAnimation);

	//OLD_RIGHT
	//set_isMoving(true);
	//rotate( PI / 2 , customAngle, 0.0f, 0);
	//rotate( -PI / 2 , -customAngle, 0.0f, 1);
	//rotate( 0.0 , customAngle, 0.0f, 2);
	//if (counter_moves==3) stopMoving();
	setIsMoving(true);
	rotate( PI / 3 , customAngle, 1.7f, 0);
	rotate( PI / 3 , -customAngle, 1.8f, 1);
	rotate( PI / 4 , customAngle, 2.3f, 2);
	rotate( PI / 4 , -customAngle, 1.4f, 3);
	rotate( PI / 3 , customAngle, 0.4f, 4);
	rotate( PI / 3 , -customAngle, 0.4f, 5);
	rotate( PI / 4 , customAngle, 1.9f, 6);
	rotate( PI / 4 , -customAngle, 1.9f, 7);

	//rotate( (2/3)*PI / , customAngle, 0.0f, 2);
	//rotate( -PI / 7 , -customAngle, 0.0f, 3);
	//rotate( -PI / 4 , +customAngle, 0.0f, 4);
	//rotate( -PI / 5 , -customAngle, 0.0f, 5);

	rotate( 2*PI , customAngle, 0.0f, 8);

	if (counter_moves==9) stopMoving();

}
void Movements::actEnthusiast(){
	//if(newfla == 0){
	//	lightUpLeds(currentColor,currentAnimation);
	//	newfla =1;
	//}
	setIsMoving(true);
	rotate( PI / 2 , customAngle, 0.0f, 0);
	rotate( -PI / 2 , -customAngle, 0.0f, 1);
	rotate( 0.0 , customAngle, 0.0f, 2);
	if (counter_moves==3) stopMoving();
}*/
/*void Movements::odd(){
	translateRobot(allowed_distance,1,0);
	translateRobot(allowed_distance,0,1);
	translateRobot(allowed_distance,1,2);
	translateRobot(allowed_distance,0,3);
	translateRobot(allowed_distance,1,4);
	translateRobot(allowed_distance,0,5);
	translateRobot(allowed_distance,1,6);
	translateRobot(allowed_distance,0,7);
	translateRobot(allowed_distance,1,8);
	translateRobot(allowed_distance,0,9);
	translateRobot(allowed_distance,1,10);
	translateRobot(allowed_distance,0,11);
	translateRobot(allowed_distance,1,12);
	translateRobot(allowed_distance,0,13);
	translateRobot(allowed_distance,1,14);
	translateRobot(allowed_distance,0,15);
	rotate( 2*PI , customAngle, 0.0f, 8);
	if (counter_moves==17) stopMoving();
}*/
void Movements::actSad() {
/*gli devo passare angolo negativo per continuare senza stop con valore di posth che si incrementa,
gli devo passare vel negativa solo per cambiare direction
i led dipendono da nano a questo punto non da mega
*/
float angular = 0.0;
	if(isCustom) angular = customAngle;
	else angular = ANGULAR_NORMAL_SPEED;
	setIsMoving(true); //??metto qui o nel principale?
//	Serial.println("first ROTATION");
	rotate( PI / 2 , angular, 0.0, 0, 0);
//	Serial.println("second ROTATION");
	rotate( PI / 2 , -angular, 0.0, 1, 0);
//	Serial.println("third ROTATION");
	rotate( 5*PI/6 , angular, 0.0f, 2, 0);
	if (counter_moves==3){
		momentanous = true;
		stopMoving();
	}
	proceed(13,2,4);
	proceed(13,3,5);
	rotate( PI/5 , angular, 0.0f, 6, 0);
	rotate( 2*PI/3 , -angular, 0.0f, 7, 0);
	rotate( PI/4 , angular, 0.0f, 8, 0);
	if (counter_moves==9){
		singleTrial = true;
		stopMoving();
	}
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Manager movements */
void Movements::controlMe(){
	//if(isControlled){
		//_oimiPid.nonHolonomicRun(strafe,forward);
	//}
	//else {
	//	isMoving = false;
	//	_oimiPid.stop2();
	//}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Manager movements */
void Movements::followMe(){
//	Serial.print("ecco tempo solitude per nopat:");
//	Serial.println(_sonars.getSolitudeTime());
//	Serial.print("ecco tempo stall per almost blocked:");
//	Serial.println(_sonars.getStallTime());
//	Serial.print("ecco tempo backstall per back:");
//	Serial.println(_sonars.getStallBackTime());
	//_sonars.printOnlyScans();
	//_sonars.printNewIncr();
	isFollowing = true;
	//current_env = _sonars.getCurrentEnv();
		//firt cycle robot is still
	if(current_env.pat==no_case && (millis() < (_sonars.getSolitudeTime() + seekInterval))){
        Serial.println(F("<nopat>"));
		randomNumb = random(2,5);
		numberTurning = 0;
		stopMoving();
	}
	else if(current_env.pat==no_case && (millis() > (_sonars.getSolitudeTime() + seekInterval))){
		Serial.print("CAMBIOOOO");
		_oimiPid.resetIntegTerm();
		_oimiPid.setPosX(0);
		_oimiPid.setPosY(0);
		_oimiPid.setPosTh(0);
//		Serial.println(F("<changepat>"));
        //Serial.println(F("<nopat>"));
		numberTurning = 0;
		numberTurning ++;
		rotateType = 0;
		int decide = random(2);
		int look = random(3);
		int casual = random(2);
		exactAngle = 2*PI/3;
		isSafe = true;
		if(!casual)
			freePath = true;
		if (_sonars.getTooNear()){
			isSafe = true;
			_sonars.setSolitudeTime(millis());
		}
		else if(numberTurning == randomNumb){
			numberTurning = 0;
			_sonars.setSolitudeTime(millis());
			//if(look)
			//	isSafe = true;
		}
		if(!decide)
			actual_movement = go_left_2;
		else
			actual_movement = go_right_2;
	}
	else if(current_env.pat==blocked_case){
		numberTurning = 0;
		Serial.println(F("<blopat>"));
		turnDuetoHurdle();
	}
	else if(current_env.pat==almost_blocked_back_case && (millis() < (_sonars.getStallBackTime() + stallBackInterval))){
		Serial.println(F("<albapat>"));
     	stopMoving();
		isSafe = true;
		goStraight();
		//actual_movement = go_straight; //errore se vuoi fermarti nel mezzo e calcolare allowed distance...se vuoi usarlo metti flag per primo giro
	}
	else if(current_env.pat==almost_blocked_back_case && (millis() > (_sonars.getStallBackTime() + stallBackInterval))){
		Serial.print("CAMBIOOOO");
//		Serial.println(F("<albapat>"));
		isSafe = true;
   	    goStraight();
		//actual_movement = go_straight; //errore same as above
		_sonars.setStallBackTime(millis());
	}
	else if(current_env.pat==almost_blocked_case && (millis() < (_sonars.getStallTime() + stallInterval))){
		Serial.println(F("<alpat>"));
   	    stopMoving();
	}
	else if(current_env.pat==almost_blocked_case && (millis() > (_sonars.getStallTime() + stallInterval))){
		Serial.print("CAMBIOOOO");
		_oimiPid.resetIntegTerm();
		_oimiPid.setPosX(0);
		_oimiPid.setPosY(0);
		_oimiPid.setPosTh(0);
//		Serial.println(F("<alpat>"));
	    Serial.print("ecco tempo stall:");
		Serial.print(_sonars.getStallTime());
   	    rotateType = 0;
		int decide = random(2);
		int choose = random(3);
		int casual = random(2);
		if(casual)
			freePath = true;
		exactAngle = 2*PI/3;
		_sonars.setStallTime(millis());
		switch(choose){
			case 0: exactAngle = 2*PI/3; break;
			case 1: exactAngle = 5*PI/6; break;
			case 2: exactAngle = PI; 	 break;
		}
		isSafe = true;
		if(!decide)
			actual_movement = go_left_2;
		else
			actual_movement = go_right_2;
	}
	else if(current_env.pat==front_case){
		numberTurning = 0;
		Serial.println("<frontpat>");
		actual_movement = go_straight;
	}
	else if(current_env.pat==back_case){
		_oimiPid.resetIntegTerm();
		_oimiPid.setPosTh(0);
		_oimiPid.setPosX(0);
		_oimiPid.setPosY(0);
		numberTurning = 0;
		Serial.println("<backpat>");
		int decide = random(2);
		exactAngle = PI;
		rotateType = 0;
		if(!decide)
//			actual_movement = turn180_l;
			actual_movement = go_left_2;
		else
//			actual_movement = turn180_r;
			actual_movement = go_right_2;
	}
	else if(current_env.pat==left_case){
		numberTurning = 0;
		Serial.println("<leftpat>");
		rotateType = 1;
		rotationTime = millis();
		//rotationInterval = 720;
		//rotationInterval = 500; //ok with min normal speed
		actual_movement = go_left;
	}
	else if(current_env.pat==frontLeft_1_case){
		numberTurning = 0;
		//Serial.println("<fl1pat>");
		exactAngle = PI/7;
		actual_movement = go_left_2;
	}
	else if(current_env.pat==frontLeft_2_case){
		numberTurning = 0;
		//Serial.println("<fl2pat>");
		exactAngle = PI/8;
		actual_movement = go_right_2;
	}
	else if(current_env.pat==right_case){
		numberTurning = 0;
		Serial.println("<rightpat>");
		rotateType = 1;
		rotationTime = millis();
		//rotationInterval = 720; //ok with min angular speed
		//rotationInterval = 500; //ok with min normal speed
		actual_movement = go_right;
	}
	else if(current_env.pat==frontRight_1_case){
		numberTurning = 0;
		//Serial.println("<fr1pat>");
		exactAngle = PI/7;
		actual_movement = go_right_2;
	}
	else if(current_env.pat==frontRight_2_case){
		numberTurning = 0;
		//Serial.println("<fr2pat>");
		exactAngle = PI/8;
		actual_movement = go_right_2;
	}
}
//void Movements::copiasonars(int s1,int s2,int s3,int s4){
//	_sonars.putSonars(s1,s2,s3,s4);
//	_sonars.maxsonarsLoop2(0);
//}

void Movements::printa(){
	Serial.print(current_env.scans[0].dist_in);
	Serial.print(",,");
	Serial.print(current_env.scans[1].dist_in);
	Serial.print(",,");
	Serial.print(current_env.scans[2].dist_in);
	Serial.print(",,");
	Serial.println(current_env.scans[3].dist_in);
}
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Manager movements 											*/
void Movements::driveMe(){
/*
Movements driver, selects next movement instruction to performe
for main loop of oimi_sketch, according to actual_movement value
*/
    //unsigned char actual_mov = getActualMovement();
    //isMoving = true;
    //delay(250);
    switch(actual_movement){
        //controlled_movement:  controlMe(); break;???
        case no_movement:          break; //setIsMoving(false);						      break;
        case autonomous:  		   freeMe();     	  					  		  break;
        case following:			   followMe(); 	  	    	                      break;
        case go_straight:    	   goStraight();  		                      	  break;
        case go_back:         	   goBack(); 	  								  break;
        case turn30_l:     	   	   turn(30,0);                              	  break;
        case turn30_r:     	       turn(30,1);                              	  break;
        case turn35_l:     	       turn(35,0);                              	  break;
        case turn35_r:     	       turn(35,1);                              	  break;
        case turn45_l:     	       turn(45,0);                              	  break;
        case turn45_r:     	       turn(45,1);                              	  break;
        case turn60_l:     	       turn(60,0);                              	  break;
        case turn60_r:     	       turn(60,1);                              	  break;
        case turn90_l:         	   turn(90,0);                              	  break;
        case turn90_r:         	   turn(90,1);                              	  break;
        case turn120_l:     	   turn(120,0);                             	  break;
        case turn120_r:     	   turn(120,1);                             	  break;
        case turn150_l:            turn(150,0);                             	  break;
        case turn150_r:            turn(150,1);                             	  break;
        case turn180_l:        	   turn(180,0);                             	  break;
        case turn180_r:        	   turn(180,1);                             	  break;
        case turn360_l:       	   turn(360,0);                             	  break;
        case turn360_r:        	   turn(360,1);                                	  break;
        case turn_until_l:	       turn(2,0);                                     break;
        case turn_until_r:	       turn(2,1);                                     break;
        case turn_until_bl:	       turn(3,0);                                     break;
        case turn_until_br:	       turn(3,1);                                     break;
        case forward_until:        goForwardUntil(customDistance,customSpeed);    break;
		case backward_until:       goBackwardUntil(customDistance,-customSpeed);  break;
		case go_left:      		   goLeft(rotateType);  				          break;
		case go_right:		       goRight(rotateType);					          break;
		case go_left_2:            goLeft(exactAngle);					          break;
		case go_right_2:		   goRight(exactAngle);					          break;
        case translate_l:          translateLeft();								  break;
        case translate_r:      	   translateRight();							  break;
        case translate_fl:	       translateFrontLeft();						  break;
        case translate_fr:         translateFrontRight();					      break;
        //case dance:	           letsDance();								      break;
		//case refuse:             koRefuse();							          break;
		//case agree:              okAgree();								      break;
		//case angriness:          actAngry();								      break;
		case sadness:	           actSad();								      break;
	    //case scare:		       actScared();								      break;
		//case normality:		   actNormal();             		              break;
		//case happiness:		   actHappy();								  	  break;
		//case enthusiasm:	   	   actEnthusiast();								  break;
		//case control_me:	       controlMe();                                   break;
    }
}
