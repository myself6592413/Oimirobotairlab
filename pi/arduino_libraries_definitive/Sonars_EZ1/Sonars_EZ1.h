
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Oimi-Robot Sonars_EZ1_following.h library, allows usage of Maxsonars EZ1, through multiple analog chain
* (constantly looping configuration - refers to Maxbotix LV-MaxSonar EZ Datasheet)
* 
* Overloading for void maxsonarsLoop() 
* 1) No argument: simple sonars scans loop that detect distances in inch (called by Nano1 to setup sonars and start detecting distances)
* those values will be printed in the sonarserial from Nano1 to Mega
* 2) modality param: complete sonars main to create the the environemnt object, check traffic.... "distanceDetection"...
* (called by Mega to start methods that work on scans values received from Nano1) 
***
****** void maxsonarsLoop(bool scanModality) => 
		if scanModality param = 0 use the methods for accomplish "following movement"
		if scanModality = 1 use the methods of "autonomous movement"
*
* Equality class represent the type of relation between distances in terms of of membership to same Status enum ! 
* It is mainly used to find if there are more candidates with minimum (or maximum) distance to choose.
*
* { ....eq_01 = the minimum distance (e.g. very_close) at position 0 is equal to the ones at position 1 ...
* { ....eq_012 = the first three minimum distances (e.g. very_far) are similar
*
* Created by Giacomo Colombo Airlab Politecnico di Milano 2022
*
* --notes
*	maxsonar and obstacle types 
*	new analog ??? change! betteranalog library sketchbook?
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Conditional compilation */
#ifndef Sonars_EZ1_h
#define Sonars_EZ1_h
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
#include "StandardCplusplus.h"
#include <vector>
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
// Distances [inches]
#define MIN_DISTANCE 		  7
#define VERY_CLOSE_DISTANCE  12
#define CLOSE_DISTANCE 		 20
#define NEAR_DISTANCE 		 25
#define MEDIUM_DISTANCE 	 35
#define GREAT_DISTANCE 		 62
#define VERY_GREAT_DISTANCE  80
#define BIG_DISTANCE 	     90
#define VERY_BIG_DISTANCE   110
#define SUPER_BIG_DISTANCE  130
#define FAR_DISTANCE 	    160
#define VERY_FAR_DISTANCE   185
#define SUPER_FAR_DISTANCE  200
#define MAX_DISTANCE 	    230
#define ERR_DISTANCE 	    250

#define MAX_NUM_ELEM 4

/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Enumerations */
/*
Used both by following movement:
	current environment patterns for establishing a precise situation
	current environment equality between status, only in sortedScans vector (01 = sortedScans[0], sortedScans[1]...)
*/
// Status
enum Range{ contact, very_close, close, near, medium, great,
	very_great, big, very_big, super_big, far, very_far, super_far, max, err };
// Directions
enum Verse{ none, left, front, right, back };
// Pattern
enum Pattern{ start_case, no_case, blocked_case, almost_blocked_case, almost_blocked_back_case, back_case, front_case, left_case, frontLeft_1_case,
	frontLeft_2_case, right_case, frontRight_1_case, frontRight_2_case };
// Equality
enum Equality{ eq_default, eq_none, eq_01, eq_02, eq_12, eq_012 };
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Structs*/
typedef struct maxSonar{
	int dist_in;       // distance [inch]
	float dist_cm;     // distance [inch]
	Verse direction;   // among Directions
	Range status;      // among Ranges
}maxSonar;

typedef struct Obs{
	int length;						// number of free paths
	maxSonar scans[4];				// current environment situation
	maxSonar sortedScansIncr[4];	// sensors ordered in increasing order
	maxSonar sortedScansDecr[4];	// sensors ordered in decreasing order
	Pattern pat = start_case;		// actual pattern of readings
	std::vector<maxSonar> allowed;	// correct sonar to use for self-governing move
	Equality equal = eq_default;	// Type of relation of equality between distances in terms of membership (or not) of same Status enum
}Obs;

extern Obs current_env;
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Class definition */
class Sonars_EZ1{

	private:
		/****************************************************************** Variables */
		// maxsonar Pins
		uint8_t _anPin1;			 // back
		uint8_t _anPin2;			 // left
		uint8_t _anPin3;			 // front
		uint8_t _anPin4;			 // right
		uint8_t _triggerPin1;		 // pwm start new cycle of scans
		uint8_t _triggerPin2;		 // pwm start new cycle of scans
		uint8_t _triggerPin3;		 // pwm start new cycle of scans
		uint8_t _triggerPin4;		 // pwm start new cycle of scans
		// Detection
		// Indices
		int cont_contact = 0;		 // number of surrounding obstacles
		// Flags
		bool left_flag;				 //useless??? remove??
		bool right_flag; 		     //useless?? remove??
		bool stillNopat;
		bool tooNear;				// for following nopat differentiation
		bool goingAhead;
		//bool corrected_again=false;
		// Timers
		unsigned long sonarDelay;    // for delay between each cycle of scans
		unsigned long triggerDelay;	 // pause interval for trigger a new cycle of sensing
		unsigned long triggerDelay1;	 // pause interval for trigger a new cycle of sensing
		unsigned long triggerDelay2;	 // pause interval for trigger a new cycle of sensing
		unsigned long triggerDelay3;	 // pause interval for trigger a new cycle of sensing
		unsigned long triggerDelay4;	 // pause interval for trigger a new cycle of sensing
		unsigned long solitudeTime;    // when no_case pattern has started
		unsigned long stallTime;     // when almost_blocked_case pattern has started
		unsigned long stallBackTime;     // when almost_blocked_case pattern has started
		// Ausiliary
		Pattern previousPattern;
		float follo_angle;
		int left4[3],front4[3],right4[3],back4[3];
		int back_sonar,left_sonar,front_sonar,right_sonar;
		//int*  ordered;
		//int leftSonar[3];
		//int frontSonar[3];
		//int rightSonar[3];
		//int backSonar[3];          // for sorting for Mode calc

	public:
		/****************************************************************************************************************************************************************************** Constructors */
		Sonars_EZ1();
		Sonars_EZ1(uint8_t _anPin1, uint8_t _anPin2, uint8_t _anPin3, uint8_t _anPin4, uint8_t _triggerPin1, uint8_t _triggerPin2, uint8_t _triggerPin3, uint8_t _triggerPin4);
		/****************************************************************************************************************************************************************************** Methods */
		// Principals
		int readSensor(uint8_t anPin1);  	// detects distance for a specif maxsonar
		void setupAllSonars();				 	// sets up pin every new cycle of detections
		void runAllSonars();			 	// detects distances for all chain of maxsonars
		void putSonars(int distance1, int distance2, int distance3, int distance4);  // copy current env
		// Getters
		int getSonarLeft_current();			     // left distance from obstacles  [inch]
		int getSonarRight_current();			     // left distance from obstacles  [inch]
		int getSonarFront_current();			     // left distance from obstacles  [inch]
		int getSonarBack_current();			     // left distance from obstacles  [inch]
		int getSonarLeft();			     // left distance from obstacles  [inch]
		int getSonarFront();			 // front distance from obstacles [inch]
		int getSonarRight();			 //	right distance from obstacles [inch]
		int  getSonarBack();			 // back distance from obstacles  [inch]
		Obs getCurrentEnv();			 // return complete sorrounding situation
		Pattern getPreviousPattern();            // following?? useless
		bool getLeftFlag();   	         // following?? useless
		bool getRightFlag(); 			 // following?? useless
		bool getGoingAhead();
		int getNumContacts();		     // returns how many sonars have absolute min output
		unsigned long getSolitudeTime();
		unsigned long getStallTime();
		unsigned long getStallBackTime();
		bool getTooNear();
		// Setters
		void setLeftFlag(bool left);              	// following??
		void setRightFlag(bool right);           	// following??
		void setSolitudeTime(unsigned long time);
		void setStallTime(unsigned long time);
		void setStallBackTime(unsigned long time);
		// Distances
		void leftDetection();					  	// determines current_env.scans, left maxSonar obj
		void frontDetection();					  	// determines current_env.scans, front maxSonar obj
		void rightDetection();					  	// determines current_env.scans, right maxSonar obj
		void backDetection();					  	// determines current_env.scans, back maxSonar obj
		void distanceDetection(int index); 		   	// sets vars of current_env obj for a specified sensor
		void detectTraffic();
		void createAllowedDirections();			  	// defines vector of allowed direction
		void checkContacts();					  	// calcs number of times an obstacle is found
		// Calcs
		void convertTocm();						  									// from inches to cm
		bool checkSameElement(int array[], int size);								// check if there at least a duplicate inside a vector
		int* sortSamples(int old_array[], int new_array[], int size, bool order); 	// sorts samples from min to max (order=0) and viceversa
		// Filters
		int medianFilter(int array[], int size);
		int modeFilter(int array[], int size);
		int Filter(int array[], int size);
		// Following
		void verifyEquality();					  	
		void findPattern();							
		void createSortedScans(bool order);
		void setGoingAhead(bool goingForw);
		void setPreviousPattern(Pattern prevPattern);
		// Principal
		void maxsonarsLoop();			   			// simple sonars scans loop (used by Nano1 to setup sonars and start detecting distances)
		void maxsonarsLoop(bool scanModality);	   	// complete sonars main (called by Mega to start methods that work on scans values received from Nano1) distanceDetection
        void test_sonarsLoop_simple();              // for test only
		// Debugging
		void printOnlyScans2();
		void printOnlyScans();
		void printAllScans();
		void printFollow();
		void printNewIncr();
		void printNewDecr();
};
#endif
