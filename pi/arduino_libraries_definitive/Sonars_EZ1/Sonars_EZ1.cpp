/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Oimi-Robot --> look at Sonars_EZ1.h for precise info
*
* notes
*
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Includes */
#include "Sonars_EZ1.h"
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructor */
Sonars_EZ1::Sonars_EZ1(){
	current_env.allowed.reserve(4);
}

Sonars_EZ1::Sonars_EZ1(uint8_t an_1, uint8_t an_2, uint8_t an_3, uint8_t an_4, uint8_t trigger_1,  uint8_t trigger_2,  uint8_t trigger_3,  uint8_t trigger_4){
    _anPin1 = an_1;
	_anPin2 = an_2;
	_anPin3 = an_3;
	_anPin4 = an_4;
	_triggerPin1 = trigger_1;
	_triggerPin2 = trigger_2;
	_triggerPin3 = trigger_3;
	_triggerPin4 = trigger_4;
	pinMode(_triggerPin1, OUTPUT);
	pinMode(_triggerPin2, OUTPUT);
	pinMode(_triggerPin3, OUTPUT);
	pinMode(_triggerPin4, OUTPUT);
	solitudeTime = 0;
	current_env.pat = no_case;
	current_env.allowed.reserve(4);
	triggerDelay = 0;
	triggerDelay1 = 0;
	triggerDelay2 = 0;
	triggerDelay3 = 0;
	triggerDelay4 = 0;
	//prevFollowing = none; ??non serve giusto?
}
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Principal functions */

//void Sonars_EZ1::setupAllSonars(){
/*
Raises for 40 microseconds triggers PWM pins,
one for each sensors at every new cicle of scans
*/
/*
	if(millis() >= triggerDelay1 + 40){
		digitalWrite(_triggerPin1,HIGH);
		triggerDelay1 = millis();
	}
	else if(millis() < triggerDelay1 + 40)
		digitalWrite(_triggerPin1,LOW);
	if(millis() >=  triggerDelay2 + 40){
		digitalWrite(_triggerPin2,HIGH);
		triggerDelay2 = millis();
	}
	else if(millis() < triggerDelay2 + 40)
		digitalWrite(_triggerPin2,LOW);
	if(millis() >= triggerDelay3 + 40){
		digitalWrite(_triggerPin3,HIGH);
		triggerDelay3 = millis();
	}
	else if(millis() < triggerDelay3 + 40)
		digitalWrite(_triggerPin3,LOW);
	if(millis() >= triggerDelay4 + 40){
		digitalWrite(_triggerPin4,HIGH);
		triggerDelay4 = millis();
	}
	else if(millis() < triggerDelay4 + 40)
		digitalWrite(_triggerPin4,LOW);
}*/
void Sonars_EZ1::setupAllSonars(){
/*
Raises for 40 microseconds triggers PWM pins,
one for each sensors at every new cicle of scans
*/
	digitalWrite(_triggerPin1,HIGH);
	triggerDelay = millis();
	while(millis() < triggerDelay + 40) {}
	digitalWrite(_triggerPin1,LOW);
	digitalWrite(_triggerPin2,HIGH);
	triggerDelay = millis();
	while(millis() < triggerDelay + 40) {}
	digitalWrite(_triggerPin2,LOW);
	digitalWrite(_triggerPin3,HIGH);
	triggerDelay = millis();
	while(millis() < triggerDelay + 40) {}
	digitalWrite(_triggerPin3,LOW);
	digitalWrite(_triggerPin4,HIGH);
	triggerDelay = millis();
	while(millis() < triggerDelay + 40) {}
	digitalWrite(_triggerPin4,LOW);
}


int Sonars_EZ1::readSensor(uint8_t anPin){
/*
Discovers ultrasound distance for a specific sonar
Scale factor is (Vcc/512) per inches
5V supply yields ~9.8mV/in
Analog pin goes from 0 to 1024, division by 2 to get the actual measurement in inches
*/
	int distance = analogRead(anPin)/2;
	return distance;
}
void Sonars_EZ1::runAllSonars(){
/*
Detects distance for all sonars simultaneosly NEW!!! with Nano1.
*/
	back_sonar  = analogRead(_anPin1)/2; //old_back_pin12
	left_sonar  = analogRead(_anPin2)/2; //old_left_pin8
	front_sonar = analogRead(_anPin3)/2; //old_front_pin11
	right_sonar = analogRead(_anPin4)/2; //old_right_pin9
}
void Sonars_EZ1::putSonars(int s1,int s2,int s3,int s4){
/*
Settle the environment scans NEW!!! with Nano1.
*/
	current_env.scans[0].dist_in = s1; //nano left_pin 0
	current_env.scans[1].dist_in = s2; //nano front_pin 1
	current_env.scans[2].dist_in = s3; //nano right_pin 2
	current_env.scans[3].dist_in = s4; //nano back_pin 3

}
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Getters */
bool Sonars_EZ1::getGoingAhead(){
	return goingAhead;
}
unsigned long Sonars_EZ1::getSolitudeTime(){
	return solitudeTime;
}
unsigned long Sonars_EZ1::getStallTime(){
	return stallTime;
}
unsigned long Sonars_EZ1::getStallBackTime(){
	return stallBackTime;
}
int Sonars_EZ1::getSonarLeft(){
	return left_sonar;
}
int Sonars_EZ1::getSonarRight(){
	return right_sonar;
}
int Sonars_EZ1::getSonarFront(){
	return front_sonar;
}
int Sonars_EZ1::getSonarBack(){
	return back_sonar;
}
int Sonars_EZ1::getSonarLeft_current(){
	return current_env.scans[0].dist_in;
}
int Sonars_EZ1::getSonarRight_current(){
	return current_env.scans[2].dist_in;
}
int Sonars_EZ1::getSonarFront_current(){
	return current_env.scans[1].dist_in;
}
int Sonars_EZ1::getSonarBack_current(){
	return current_env.scans[3].dist_in;

}
Pattern Sonars_EZ1::getPreviousPattern(){
    return previousPattern;
}
int Sonars_EZ1::getNumContacts(){
    return cont_contact;
}
//Pattern Sonars_EZ1::getPrevPat(){
//    return prev_pat;
//}
Obs Sonars_EZ1::getCurrentEnv(){
	return current_env;
}
bool Sonars_EZ1::getTooNear(){
	return tooNear;
}

/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Setters */
void Sonars_EZ1::setGoingAhead(bool goingForw){
	goingAhead = goingForw;
}
void Sonars_EZ1::setPreviousPattern(Pattern direction){
	previousPattern = direction;
}
void Sonars_EZ1::setLeftFlag(bool left){
	left_flag=left;
}
void Sonars_EZ1::setRightFlag(bool right){
	right_flag=right;
}
void Sonars_EZ1::setSolitudeTime(unsigned long time){
	solitudeTime = time;
}
void Sonars_EZ1::setStallTime(unsigned long time){
	stallTime = time;
}
void Sonars_EZ1::setStallBackTime(unsigned long time){
	stallBackTime = time;
}
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Distance detection */
void Sonars_EZ1::distanceDetection(int i){
/*
Sets complete status situation of current environment for a specified sensor
*/
	if(current_env.scans[i].dist_in<=MIN_DISTANCE)
		current_env.scans[i].status=contact;
	else if((current_env.scans[i].dist_in > MIN_DISTANCE)&&(current_env.scans[i].dist_in<=VERY_CLOSE_DISTANCE))
		current_env.scans[i].status=very_close;
	else if((current_env.scans[i].dist_in > VERY_CLOSE_DISTANCE)&&(current_env.scans[i].dist_in<=CLOSE_DISTANCE))
		current_env.scans[i].status=close;
	else if((current_env.scans[i].dist_in > CLOSE_DISTANCE)&&(current_env.scans[i].dist_in<=NEAR_DISTANCE))
		current_env.scans[i].status=near;
	else if((current_env.scans[i].dist_in > NEAR_DISTANCE)&&(current_env.scans[i].dist_in<=MEDIUM_DISTANCE))
		current_env.scans[i].status=medium;
	else if((current_env.scans[i].dist_in > MEDIUM_DISTANCE)&&(current_env.scans[i].dist_in<=GREAT_DISTANCE))
		current_env.scans[i].status=great;
	else if((current_env.scans[i].dist_in > GREAT_DISTANCE)&&(current_env.scans[i].dist_in<=VERY_GREAT_DISTANCE))
		current_env.scans[i].status= very_great;
	else if((current_env.scans[i].dist_in > VERY_GREAT_DISTANCE)&&(current_env.scans[i].dist_in<=BIG_DISTANCE))
		current_env.scans[i].status= big;
	else if((current_env.scans[i].dist_in > BIG_DISTANCE)&&(current_env.scans[i].dist_in<=VERY_BIG_DISTANCE))
		current_env.scans[i].status= very_big;
	else if((current_env.scans[i].dist_in > VERY_BIG_DISTANCE)&&(current_env.scans[i].dist_in<=SUPER_BIG_DISTANCE))
		current_env.scans[i].status= super_big;
	else if((current_env.scans[i].dist_in > SUPER_BIG_DISTANCE)&&(current_env.scans[i].dist_in<=FAR_DISTANCE))
		current_env.scans[i].status= far;
	else if((current_env.scans[i].dist_in > FAR_DISTANCE)&&(current_env.scans[i].dist_in<=VERY_FAR_DISTANCE))
	   current_env.scans[i].status=very_far;
	else if((current_env.scans[i].dist_in > VERY_FAR_DISTANCE)&&(current_env.scans[i].dist_in<=SUPER_FAR_DISTANCE))
        current_env.scans[i].status=super_far;
	else if((current_env.scans[i].dist_in > SUPER_FAR_DISTANCE)&&(current_env.scans[i].dist_in<=MAX_DISTANCE))
        current_env.scans[i].status=max;
	else if((current_env.scans[i].dist_in > MAX_DISTANCE)&&(current_env.scans[i].dist_in<=ERR_DISTANCE))
        current_env.scans[i].status=max;
	else if(current_env.scans[i].dist_in >= ERR_DISTANCE)
        current_env.scans[i].status=err;
}
void Sonars_EZ1::detectTraffic(){
/*
Concrete detection of current environment status
*/
	current_env.scans[0].direction = left;
	current_env.scans[1].direction = front;
	current_env.scans[2].direction = right;
	current_env.scans[3].direction = back;

	//leftDetection();
	//frontDetection();
	//rightDetection();
	//backDetection();

    //for future CHANGE (only next line)
    distanceDetection(0);
    distanceDetection(1);
    distanceDetection(2);
    distanceDetection(3);

}
void Sonars_EZ1::checkContacts(){
/*
Calculates number of times an obstacle is found
Calls detectTraffic
*/
	cont_contact=0;
	detectTraffic();
	for(int k=0;k<4;k++)
		if(current_env.scans[k].status==contact)
			cont_contact = cont_contact+1;
}
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Filters */
void Sonars_EZ1::convertTocm(){
/*
Trasform measurement unit from inch to cm
EZ1 min value is 5/6/7 (according to exact analog voltage on pin)
Reduce respectively to 0 and 1 value 6 and 7
Error disambiguations when value is exactly 7
--> See EZ1 maxbotix datasheet for details
*/
	if (current_env.scans[0].dist_in <= 4 ||current_env.scans[0].dist_in == 5)
		current_env.scans[0].dist_cm = 0;
	else if(current_env.scans[0].dist_in == 6)
		current_env.scans[0].dist_cm = 0.5;
	else if(current_env.scans[0].dist_in == 7)
		current_env.scans[0].dist_cm = 2;
	else
		current_env.scans[0].dist_cm = float(current_env.scans[1].dist_in) * 2.54;

	if (current_env.scans[1].dist_in <= 4 ||current_env.scans[1].dist_in == 5)
		current_env.scans[1].dist_cm = 0.0;
	else if(current_env.scans[1].dist_in == 6)
		current_env.scans[1].dist_cm = 0.5;
	else if(current_env.scans[1].dist_in == 7)
		current_env.scans[1].dist_cm = 2;
	else
		current_env.scans[1].dist_cm = float(current_env.scans[1].dist_in) * 2.54;

	if (current_env.scans[2].dist_in <= 4 ||current_env.scans[2].dist_in == 5)
		current_env.scans[2].dist_cm = 0;
	else if(current_env.scans[2].dist_in == 6)
		current_env.scans[2].dist_cm = 0.5;
	else if(current_env.scans[2].dist_in == 7)
		current_env.scans[2].dist_cm = 2;
	else
		current_env.scans[2].dist_cm = float(current_env.scans[2].dist_in) * 2.54;

	if (current_env.scans[3].dist_in <= 4 ||current_env.scans[3].dist_in == 5)
		current_env.scans[3].dist_cm = 0;
	else if(current_env.scans[3].dist_in == 6)
		current_env.scans[3].dist_cm = 0.5;
	else if(current_env.scans[3].dist_in == 7)
		current_env.scans[3].dist_cm = 2;
	else
		current_env.scans[3].dist_cm = float(current_env.scans[3].dist_in) * 2.54;

}

int* Sonars_EZ1::sortSamples(int old_array[], int new_array[], int size, bool order){
    int* p1 = old_array;
    int* p2 = new_array;
    for(int i = 0; i < size; i++)
    	*(p2+i) = *(p1+i);
    for(int i = 1; i < size; i++){
	    float j = new_array[i];
	    int k;
	    if(!order){ //ascending order
	    	for (k = i - 1; (k >= 0) && (j < new_array[k]); k--)
	    		new_array[k + 1] = new_array[k];
	    }
	    else{ //descending order
	    	for (k = i - 1; (k >= 0) && (j > new_array[k]); k--)
	    		new_array[k + 1] = new_array[k];
	    }
	    new_array[k + 1] = j;
    }
    return new_array;
}
void Sonars_EZ1::createSortedScans(bool order){
/*
Fill sortedScans with maxSonar scan attribute, comparing dist_ins
It is used in the case of "following" movement for defining patterns
*/
	bool already = false;
	int arraytemp[4];
	int size = *(&arraytemp + 1) - arraytemp;
	int ordered[4];
	arraytemp[0] = current_env.scans[0].dist_in;
	arraytemp[1] = current_env.scans[1].dist_in;
	arraytemp[2] = current_env.scans[2].dist_in;
	arraytemp[3] = current_env.scans[3].dist_in;
	if(!order)//ascending order --> following
		sortSamples(arraytemp, ordered, size, false);
	else //descending order --> autonomous
		sortSamples(arraytemp, ordered, size, true);
	//toremove print!!?
//	Serial.println("SIMPLY ORDERED");
//	Serial.print(ordered[0]);
//	Serial.print(",");
//	Serial.print(ordered[1]);
//	Serial.print(",");
//	Serial.print(ordered[2]);
//	Serial.print(",");
//	Serial.println(ordered[3]);
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			if(ordered[i] == current_env.scans[j].dist_in){
				if(!order){
					if(!already){
						current_env.sortedScansIncr[i].dist_in = current_env.scans[j].dist_in;
						current_env.sortedScansIncr[i].dist_cm = current_env.scans[j].dist_cm;
						current_env.sortedScansIncr[i].direction = current_env.scans[j].direction;
						current_env.sortedScansIncr[i].status = current_env.scans[j].status;
						already = true;
					}
					else{
						current_env.sortedScansIncr[i].dist_in = current_env.scans[j].dist_in;
						current_env.sortedScansIncr[i].dist_cm = current_env.scans[j].dist_cm;
						current_env.sortedScansIncr[i].direction = current_env.scans[j].direction;
						current_env.sortedScansIncr[i].status = current_env.scans[j].status;
						already = false;
						break;
					}
				}
				else{
					if(!already){
						current_env.sortedScansDecr[i].dist_in = current_env.scans[j].dist_in;
						current_env.sortedScansDecr[i].dist_cm = current_env.scans[j].dist_cm;
						current_env.sortedScansDecr[i].direction = current_env.scans[j].direction;
						current_env.sortedScansDecr[i].status = current_env.scans[j].status;
						already = true;
					}
					else{
						current_env.sortedScansDecr[i].dist_in = current_env.scans[j].dist_in;
						current_env.sortedScansDecr[i].dist_cm = current_env.scans[j].dist_cm;
						current_env.sortedScansDecr[i].direction = current_env.scans[j].direction;
						current_env.sortedScansDecr[i].status = current_env.scans[j].status;
						already = false;
						break;
					}
				}
			}
		}
	}
}

void Sonars_EZ1::createAllowedDirections(){
/*
Sorts detections according to farest one exploting all possible cases,
setting the vector of allowed maxSonar obj.
Used in "autonomous movement"
Defines the length, that's the number of available paths
Creates allowed vector putting 0.0 distance in cm that is going to be created later
*/
	current_env.allowed.clear();
	createSortedScans(true);
	switch(cont_contact){
		case 0:
			current_env.length=4;
    		current_env.allowed.push_back({current_env.sortedScansDecr[0].dist_in, 0.0, current_env.sortedScansDecr[0].direction, current_env.sortedScansDecr[0].status});
			current_env.allowed.push_back({current_env.sortedScansDecr[1].dist_in, 0.0, current_env.sortedScansDecr[1].direction, current_env.sortedScansDecr[1].status});
			current_env.allowed.push_back({current_env.sortedScansDecr[2].dist_in, 0.0, current_env.sortedScansDecr[2].direction, current_env.sortedScansDecr[2].status});
			current_env.allowed.push_back({current_env.sortedScansDecr[3].dist_in, 0.0, current_env.sortedScansDecr[3].direction, current_env.sortedScansDecr[3].status});
		break;
		case 1:
			current_env.length=3;
    		//minimum distance [3] is contact
    		current_env.allowed.push_back({current_env.sortedScansDecr[0].dist_in, 0.0, current_env.sortedScansDecr[0].direction, current_env.sortedScansDecr[0].status});
			current_env.allowed.push_back({current_env.sortedScansDecr[1].dist_in, 0.0, current_env.sortedScansDecr[1].direction, current_env.sortedScansDecr[1].status});
			current_env.allowed.push_back({current_env.sortedScansDecr[2].dist_in, 0.0, current_env.sortedScansDecr[2].direction, current_env.sortedScansDecr[2].status});
		break;
		case 2:
			current_env.length=2;
    		//minimum distance [2]and[3] are contact
    		current_env.allowed.push_back({current_env.sortedScansDecr[0].dist_in, 0.0, current_env.sortedScansDecr[0].direction, current_env.sortedScansDecr[0].status});
			current_env.allowed.push_back({current_env.sortedScansDecr[1].dist_in, 0.0, current_env.sortedScansDecr[1].direction, current_env.sortedScansDecr[1].status});
		break;
		case 3:
			current_env.length=1;
    		current_env.allowed.push_back({current_env.sortedScansDecr[0].dist_in, 0.0, current_env.sortedScansDecr[0].direction, current_env.sortedScansDecr[0].status});
		break;
		case 4: current_env.length=0;
		break;
	}
	/*Serial.print("Sona--> ");
	Serial.print(current_env.scans[0].dist_in);
	Serial.print(",");
	Serial.print(current_env.scans[1].dist_in);
	Serial.print(",");
	Serial.print(current_env.scans[2].dist_in);
	Serial.print(",");
	Serial.println(current_env.scans[3].dist_in);*/
}

void Sonars_EZ1::verifyEquality(){
/*
Sets similarity of the scans in terms of membership (or not) of same status
Improves precision in finding the pattern
?? redundancy with 02 012?*
*/
	if((current_env.sortedScansIncr[0].status == current_env.sortedScansIncr[1].status)&&(current_env.sortedScansIncr[0].status != current_env.sortedScansIncr[2].status))
		current_env.equal=eq_01;
	else if((current_env.sortedScansIncr[0].status == current_env.sortedScansIncr[2].status)&&(current_env.sortedScansIncr[0].status != current_env.sortedScansIncr[1].status))
		current_env.equal=eq_02; //redundancy try to remove !?? is already ordered *
	else if((current_env.sortedScansIncr[0].status != current_env.sortedScansIncr[1].status)&&(current_env.sortedScansIncr[0].status != current_env.sortedScansIncr[2].status)
	&&(current_env.sortedScansIncr[1].status == current_env.sortedScansIncr[2].status))
		current_env.equal=eq_12;
	else if((current_env.sortedScansIncr[0].status == current_env.sortedScansIncr[1].status)&&(current_env.sortedScansIncr[0].status == current_env.sortedScansIncr[2].status))
		current_env.equal=eq_012;
	else if((current_env.sortedScansIncr[0].status != current_env.sortedScansIncr[1].status)&&(current_env.sortedScansIncr[0].status != current_env.sortedScansIncr[2].status)
	&&(current_env.sortedScansIncr[1].status != current_env.sortedScansIncr[2].status))
		current_env.equal=eq_none;
}
void Sonars_EZ1::findPattern(){
/*
*/
	previousPattern = current_env.pat;
	if(current_env.scans[0].dist_in<=5) //start condition when all sonars values are still zero (Arduino Nano-Mega communication delay)
		current_env.pat = no_case;
    else{
    	if(cont_contact==4)
			current_env.pat = blocked_case; //move 90 degrees trying to be free
    	//almost pat
    	else if (current_env.pat == almost_blocked_case && (((cont_contact == 3 || cont_contact == 2)
    	||(current_env.scans[0].status < close && current_env.scans[1].status < close && current_env.scans[2].status < close)) && current_env.sortedScansIncr[3].direction == back))
    		current_env.pat = almost_blocked_case;
    	else if (current_env.pat != almost_blocked_case && (((cont_contact == 3 || cont_contact == 2)
    	||(current_env.scans[0].status <= close && current_env.scans[1].status <= close && current_env.scans[2].status <= close)) && current_env.sortedScansIncr[3].direction == back)){
       		current_env.pat = almost_blocked_case;
    		stallTime = millis();
    	}
    	//almost back
    	else if (current_env.pat != almost_blocked_back_case && (cont_contact == 1 || (current_env.scans[0].status > close && current_env.scans[1].status > close && current_env.scans[2].status > close && current_env.scans[3].status <= close))
    	&& current_env.sortedScansIncr[0].direction == back){
    		current_env.pat = almost_blocked_back_case;
    		stallBackTime = millis();
    	}
    	else if (current_env.pat == almost_blocked_back_case && (cont_contact == 1 || (current_env.scans[0].status > close && current_env.scans[1].status > close && current_env.scans[2].status > close && current_env.scans[3].status <= close))
    	&& current_env.sortedScansIncr[0].direction == back){
    		current_env.pat = almost_blocked_back_case;
    	}
		//no_case
    	else if (current_env.pat == no_case && (cont_contact>0 || (cont_contact==0 && ((current_env.sortedScansIncr[0].status > great || current_env.sortedScansIncr[0].status < close))))) {
    		current_env.pat = no_case;
    		if(current_env.sortedScansIncr[0].status < close)
    			tooNear = true; //serve per girare subito se sei troppo vicino anziché aspettare tot time si sposta subito ed esce dalla situazione di stallo
    		else
    			tooNear = false;
    	}
    	else if (current_env.pat == almost_blocked_case && (cont_contact>0 || (cont_contact==0 && ((current_env.sortedScansIncr[0].status > big || current_env.sortedScansIncr[0].status < close)))))
    		current_env.pat = almost_blocked_case;
    	else if (goingAhead && ((cont_contact>0 || (cont_contact==0 && (((current_env.sortedScansIncr[0].status > far || current_env.sortedScansIncr[0].status < close))
    	||(current_env.sortedScansIncr[1].status > far || current_env.sortedScansIncr[1].status < close)))))) {
    		current_env.pat = no_case;
		    if(current_env.sortedScansIncr[0].status < close)
    			tooNear = true;
    		else
    			tooNear = false;
    	}
    	else if (current_env.pat != no_case && (cont_contact>0 || (cont_contact==0 && ((current_env.sortedScansIncr[0].status > great || current_env.sortedScansIncr[0].status < close))))){
    		current_env.pat = no_case;
    		solitudeTime = millis();
    		if(current_env.sortedScansIncr[0].status < close)
    			tooNear = true;
    		else tooNear = false;
    	}
    	//front pat
		else if(current_env.sortedScansIncr[0].direction==left && current_env.sortedScansIncr[1].direction==front
		&& ((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <= 9))
			current_env.pat = front_case;
//		else if(current_env.sortedScansIncr[0].direction==left && current_env.sortedScansIncr[1].direction==front
//		&& (((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) > 5)&&((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <=15)))
//			current_env.pat = frontLeft_1_case;
		else if(current_env.sortedScansIncr[0].direction==front && current_env.sortedScansIncr[1].direction==left
		&& ((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <= 9))
				current_env.pat = front_case;
//		else if(current_env.sortedScansIncr[0].direction==front && current_env.sortedScansIncr[1].direction==left
//		&&(((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) > 5)&&((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <=15)))
//				current_env.pat = frontLeft_1_case;
//		else if(current_env.sortedScansIncr[0].direction==front && current_env.sortedScansIncr[1].direction==right
//		&& (((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) > 5)&&((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <=15)))
//				current_env.pat = frontRight_1_case;
		else if(current_env.sortedScansIncr[0].direction==right && current_env.sortedScansIncr[1].direction==front
		&&((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <= 9))
			current_env.pat = front_case;
//		else if(current_env.sortedScansIncr[0].direction==right && current_env.sortedScansIncr[1].direction==front
//		&& (((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) > 5)&&((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <=15)))
//			current_env.pat = frontRight_1_case;

		else if(((current_env.sortedScansIncr[0].direction==left && current_env.sortedScansIncr[1].direction==right)
		&& ((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <= 9))
		||((current_env.sortedScansIncr[0].direction==right && current_env.sortedScansIncr[1].direction==left)
		&& ((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <= 9)))
			current_env.pat = front_case;
		//pat left pat right
		else if(((current_env.sortedScansIncr[0].direction==left && current_env.sortedScansIncr[1].direction==right)
		&& ((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <= 15)
		&& current_env.pat == right_case))
			current_env.pat = right_case;
		else if(((current_env.sortedScansIncr[0].direction==right && current_env.sortedScansIncr[1].direction==left)
		&& ((current_env.sortedScansIncr[1].dist_in - current_env.sortedScansIncr[0].dist_in) <= 15)
		&& current_env.pat == left_case))
			current_env.pat = left_case;
		//common pat
		else if(current_env.sortedScansIncr[0].direction==back && current_env.pat  == no_case)
			current_env.pat = back_case;
		else if(current_env.sortedScansIncr[0].direction==back && current_env.pat != no_case && current_env.sortedScansIncr[1].direction==left)
			current_env.pat = left_case;
		else if(current_env.sortedScansIncr[0].direction==back && current_env.pat != no_case && current_env.sortedScansIncr[1].direction==front)
			current_env.pat = front_case;
		else if(current_env.sortedScansIncr[0].direction==back && current_env.pat != no_case && current_env.sortedScansIncr[1].direction==right)
			current_env.pat = right_case;

		else if(current_env.sortedScansIncr[0].direction==front)
			current_env.pat = front_case;
		else if(current_env.sortedScansIncr[0].direction==left)
			current_env.pat = left_case;
		else if(current_env.sortedScansIncr[0].direction==right)
			current_env.pat = right_case;
	}
}
bool Sonars_EZ1::checkSameElement(int array[], int size){
	for(int i = 0; i < size; i++)
		for (int j = i+1; j < size; j++)
			if (array[i] == array[j])
				return true;
	return false;
}
int Sonars_EZ1::Filter(int array[], int size){
	int result;
	if(checkSameElement(array,size))
		result = modeFilter(array,size);
	else
		result = medianFilter(array,size);
	return result;
}
int Sonars_EZ1::medianFilter(int array[], int size){
	//array is already sorted here !
	int median = array[size/2];
	return median;
}
int Sonars_EZ1::modeFilter(int array[], int size){
    long mode = array[0];
    uint8_t mode_count = 1;
    uint8_t count = 1;
    for(int i = 1; i < size; i++){
        if(array[i] == array[i - 1])
            count++;
        else
            count = 1;
        if(array[i] == mode)
            mode_count++;
        else if(count == mode_count){
            mode_count = count;
            mode = array[i];
        }
    }
    return mode;
}
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Debugging */
 void Sonars_EZ1::printOnlyScans(){
	Serial.print(F("--> "));
	Serial.print(current_env.scans[0].dist_in);
	Serial.print(F(", "));
	Serial.print(current_env.scans[1].dist_in);
	Serial.print(F(", "));
	Serial.print(current_env.scans[2].dist_in);
	Serial.print(F(", "));
	Serial.println(current_env.scans[3].dist_in);

}
void Sonars_EZ1::printOnlyScans2(){
	Serial.print(F("<scans: "));
	Serial.print(current_env.scans[0].dist_in);
	Serial.print(F(", "));
	Serial.print(current_env.scans[1].dist_in);
	Serial.print(F(", "));
	Serial.print(current_env.scans[2].dist_in);
	Serial.print(F(", "));
	Serial.print(current_env.scans[3].dist_in);
	Serial.println(F(">"));
}
void Sonars_EZ1::printAllScans(){
	switch(current_env.length){
		case 0:
			Serial.print(F("no_allowed"));
			Serial.print(F("LEN: "));
			Serial.println(current_env.length);
		break;
		case 1:
			Serial.print(F("LEN: "));
			Serial.println(current_env.length);
			Serial.println(F("single allowed"));
			Serial.print(current_env.allowed[0].dist_in);
			Serial.print(F(","));
			Serial.print(current_env.allowed[0].direction);
			Serial.print(F(","));
			Serial.println(current_env.allowed[0].status);
		break;
		case 2:
			Serial.print(F("LEN: "));
			Serial.println (current_env.length);
			Serial.println(F("two allowed"));
			Serial.print(current_env.allowed[0].dist_in);
			Serial.print(F(","));
			Serial.print(current_env.allowed[0].direction);
			Serial.print(F(","));
			Serial.println(current_env.allowed[0].status);
			Serial.print(current_env.allowed[1].dist_in);
			Serial.print(F(","));
			Serial.print(current_env.allowed[1].direction);
			Serial.print(F(","));
			Serial.println(current_env.allowed[1].status);
		break;
		case 3:
			Serial.print(F("LEN: "));
			Serial.println (current_env.length);
			Serial.println(F("three allowed"));
			Serial.print(current_env.allowed[0].dist_in);
			Serial.print(F(", "));
			Serial.print(current_env.allowed[0].direction);
			Serial.print(F(", "));
			Serial.println(current_env.allowed[0].status);
			Serial.print(current_env.allowed[1].dist_in);
			Serial.print(F(", "));
			Serial.print(current_env.allowed[1].direction);
			Serial.print(F(", "));
			Serial.println(current_env.allowed[1].status);
			Serial.print(current_env.allowed[2].dist_in);
			Serial.print(F(", "));
			Serial.print(current_env.allowed[2].direction);
			Serial.print(F(", "));
			Serial.println(current_env.allowed[2].status);
		break;
		case 4:
			Serial.print(F("LEN: "));
			Serial.println (current_env.length);
			Serial.println(F("ALL allowed"));
			Serial.print(current_env.allowed[0].dist_in);
			Serial.print(F(","));
			Serial.print(current_env.allowed[0].direction);
			Serial.print(F(","));
			Serial.println(current_env.allowed[0].status);
			Serial.print(current_env.allowed[1].dist_in);
			Serial.print(F(","));
			Serial.print(current_env.allowed[1].direction);
			Serial.print(F(","));
			Serial.println(current_env.allowed[1].status);
			Serial.print(current_env.allowed[2].dist_in);
			Serial.print(F(","));
			Serial.print(current_env.allowed[2].direction);
			Serial.print(F(","));
			Serial.println(current_env.allowed[2].status);
			Serial.print(current_env.allowed[3].dist_in);
			Serial.print(F(","));
			Serial.print(current_env.allowed[3].direction);
			Serial.print(F(","));
			Serial.println(current_env.allowed[3].status);
		break;
	}
}
/*
void Sonars_EZ1::printFollow(){
	Serial.println(F(" ---> SCANS NORMALI!"));
	Serial.print(temp[0]);
	Serial.print(F("; "));
	Serial.print(temp[1]);
	Serial.print(F("; "));
	Serial.print(temp[2]);
	Serial.print(F("; "));
	Serial.println(temp[3]);
	Serial.println(F(" BEFORE SORT!"));
	Serial.println(F("LEFT4"));
	Serial.print(left4[0]);
	Serial.print(F(", "));
	Serial.print(left4[1]);
	Serial.print(F(", "));
	Serial.println(left4[2]);
	Serial.println(F("FRONT4"));
	Serial.print(front4[0]);
	Serial.print(F(", "));
	Serial.print(front4[1]);
	Serial.print(F(", "));
	Serial.println(front4[2]);
	Serial.println(F("RIGHT4"));
	Serial.print(right4[0]);
	Serial.print(F(", "));
	Serial.print(right4[1]);
	Serial.print(F(", "));
	Serial.println(rightSonar[2]);
	Serial.println(F(" BACK4"));
	Serial.print(back4[0]);
	Serial.print(F(", "));
	Serial.print(back4[1]);
	Serial.print(F(", "));
	Serial.print(back4[2]);
	Serial.println(F(" "));
	Serial.println(F("ORDERED!"));
	Serial.println(F("LEFTsonar"));
	Serial.print(leftSonar[0]);
	Serial.print(F(", "));
	Serial.print(leftSonar[1]);
	Serial.print(F(", "));
	Serial.println(leftSonar[2]);
	Serial.println(F("FRONTsonar"));
	Serial.print(frontSonar[0]);
	Serial.print(F(", "));
	Serial.print(frontSonar[1]);
	Serial.print(F(", "));
	Serial.println(frontSonar[2]);
	Serial.println(F("RIGHTsonar"));
	Serial.print(rightSonar[0]);
	Serial.print(F(", "));
	Serial.print(rightSonar[1]);
	Serial.print(F(", "));
	Serial.println(rightSonar[2]);
	Serial.println(F("BACKsonar"));
	Serial.print(backSonar[0]);
	Serial.print(F(", "));
	Serial.print(backSonar[1]);
	Serial.print(F(", "));
	Serial.print(backSonar[2]);
	Serial.println(F(" "));
	//Serial.println(";");
	//Serial.print("dist to follow=");
	//Serial.println(current_env.dist_to_follow.direction);
	//Serial.print("cont_contact: ");
	//Serial.println(cont_contact);
	Serial.print("numero contatti! ");
	Serial.println(getNumContacts());
	Serial.print("PATTERN == ");
	switch(current_env.pat){
		case 0: Serial.println("blocked_case"); break;
		case 1: Serial.println("no_case"); break;
		case 2: Serial.println("back_case"); break;
		case 3: Serial.println("front_case"); break;
		case 4: Serial.println("left_case"); break;
		case 5: Serial.println("frontLeft_1_case"); break;
		case 6: Serial.println("frontLeft_2_case"); break;
		case 7: Serial.println("right_case"); break;
		case 8: Serial.println("frontRight_1_case"); break;
		case 9: Serial.println("frontRight_2_case"); break;
	}
	Serial.println("++++FINALE CURR SCANS");
	Serial.print(current_env.scans[0].dist_in);
	Serial.print(", ");
	Serial.print(current_env.scans[0].dist_cm);
	Serial.print(", ");
	// rifai singolo stampa
	switch(current_env.scans[0].status){
		case 0: Serial.println("contact"); break;
		case 1: Serial.println("very_close"); break;
		case 2: Serial.println("close"); break;
		case 3: Serial.println("near"); break;
		case 4: Serial.println("medium"); break;
		case 5: Serial.println("great"); break;
		case 6: Serial.println("very_great"); break;
		case 7: Serial.println("big"); break;
		case 8: Serial.println("very_big"); break;
		case 9: Serial.println("super_big"); break;
		case 10: Serial.println("far"); break;
		case 11: Serial.println("very_far"); break;
		case 12: Serial.println("super_far"); break;
		case 13: Serial.println("max"); break;
		case 14: Serial.println("err"); break;
	}
	Serial.print(";;; ");
	Serial.print(current_env.scans[1].dist_in);
	Serial.print(", ");
	Serial.print(current_env.scans[1].dist_cm);
	Serial.print(", ");
	switch(current_env.scans[1].status){
		case 0: Serial.println("contact"); break;
		case 1: Serial.println("very_close"); break;
		case 2: Serial.println("close"); break;
		case 3: Serial.println("near"); break;
		case 4: Serial.println("medium"); break;
		case 5: Serial.println("great"); break;
		case 6: Serial.println("very_great"); break;
		case 7: Serial.println("big"); break;
		case 8: Serial.println("very_big"); break;
		case 9: Serial.println("super_big"); break;
		case 10: Serial.println("far"); break;
		case 11: Serial.println("very_far"); break;
		case 12: Serial.println("super_far"); break;
		case 13: Serial.println("max"); break;
		case 14: Serial.println("err"); break;
	}
	Serial.print(";;; ");
	Serial.print(current_env.scans[2].dist_in);
	Serial.print(", ");
	Serial.print(current_env.scans[2].dist_cm);
	Serial.print(", ");
	switch(current_env.scans[2].status){
		case 0: Serial.println("contact"); break;
		case 1: Serial.println("very_close"); break;
		case 2: Serial.println("close"); break;
		case 3: Serial.println("near"); break;
		case 4: Serial.println("medium"); break;
		case 5: Serial.println("great"); break;
		case 6: Serial.println("very_great"); break;
		case 7: Serial.println("big"); break;
		case 8: Serial.println("very_big"); break;
		case 9: Serial.println("super_big"); break;
		case 10: Serial.println("far"); break;
		case 11: Serial.println("very_far"); break;
		case 12: Serial.println("super_far"); break;
		case 13: Serial.println("max"); break;
		case 14: Serial.println("err"); break;
	}
	Serial.print(";;; ");
	Serial.print(current_env.scans[3].dist_in);
	Serial.print(", ");
	Serial.print(current_env.scans[3].dist_cm);
	Serial.print(", ");
	switch(current_env.scans[3].status){
		case 0: Serial.println("contact"); break;
		case 1: Serial.println("very_close"); break;
		case 2: Serial.println("close"); break;
		case 3: Serial.println("near"); break;
		case 4: Serial.println("medium"); break;
		case 5: Serial.println("great"); break;
		case 6: Serial.println("very_great"); break;
		case 7: Serial.println("big"); break;
		case 8: Serial.println("very_big"); break;
		case 9: Serial.println("super_big"); break;
		case 10: Serial.println("far"); break;
		case 11: Serial.println("very_far"); break;
		case 12: Serial.println("super_far"); break;
		case 13: Serial.println("max"); break;
		case 14: Serial.println("err"); break;
	}
	Serial.print(";;; ");
	Serial.println("ORDERED SORT");
	Serial.print(current_env.sortedScansIncr[0].dist_in);
	Serial.print(", ");
	Serial.print(current_env.sortedScansIncr[1].dist_in);
	Serial.print(", ");
	Serial.print(current_env.sortedScansIncr[2].dist_in);
	Serial.print(", ");
	Serial.println(current_env.sortedScansIncr[3].dist_in);
	Serial.println(F(" "));
}*/
void Sonars_EZ1::printNewDecr(){
	Serial.println("SORTED Decreasing order");
	Serial.print(current_env.sortedScansDecr[0].dist_in);
	Serial.print(",");
	Serial.print(current_env.sortedScansDecr[1].dist_in);
	Serial.print(",");
	Serial.print(current_env.sortedScansDecr[2].dist_in);
	Serial.print(",");
	Serial.println(current_env.sortedScansDecr[3].dist_in);
	Serial.println("ALLOWED");
	for(unsigned int q=0;q<current_env.allowed.size();q++){
		Serial.print(current_env.allowed[q].dist_in);
		Serial.print(",");
	}
}
void Sonars_EZ1::printNewIncr(){
	Serial.println("SORTED Increasing order");
	Serial.print(current_env.sortedScansIncr[0].dist_in);
	Serial.print(",");
	Serial.print(current_env.sortedScansIncr[1].dist_in);
	Serial.print(",");
	Serial.print(current_env.sortedScansIncr[2].dist_in);
	Serial.print(",");
	Serial.println(current_env.sortedScansIncr[3].dist_in);
}
/* *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Main */
void Sonars_EZ1::maxsonarsLoop(){
//	sonarDelay = millis();
	setupAllSonars();
	runAllSonars();
//	while (millis() < sonarDelay + 250){}
//	setupAllSonars();

//	if(millis() >= sonarDelay + 250){
//		runAllSonars();
//		sonarDelay = millis(); //sonarDelay += 250;
//	}
}
void Sonars_EZ1::maxsonarsLoop(bool scanModality){
/*
Gather used by oimi_definitive used after when current_environment is setted...?
*/
	if(scanModality==1){
		checkContacts();
		createAllowedDirections();
		convertTocm();
		//printOnlyScans();
	}
	if(scanModality==0){
		checkContacts();
		createSortedScans(false);
		verifyEquality();
		findPattern();
		convertTocm();
		//printOnlyScans();
	}
}

void Sonars_EZ1::test_sonarsLoop_simple(){
	sonarDelay = millis();
	setupAllSonars();
	runAllSonars();
    while (millis() < sonarDelay + 250){}
}