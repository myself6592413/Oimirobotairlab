#ifndef StreamPrint_h
#define StreamPrint_h

#include <Arduino.h>
#include <avr/pgmspace.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

extern void StreamPrint_progmem(Print &out,PGM_P format,...);

#define Serialprint(format, ...) StreamPrint_progmem(Serial,PSTR(format),##__VA_ARGS__)
#define Streamprint(stream,format, ...) StreamPrint_progmem(stream,PSTR(format),##__VA_ARGS__)

#endif
