/*************************************************************
* Oimi-Robot SetialComm.cpp --> see SerialComm.h for details
**************************************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include "SerialComm.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructor */
SerialComm::SerialComm(){
	startChar = '<';	      // recognizable string's start marker <
	endChar = '>';			  // recognizable string's end marker >
	receivedBytes = 0;		  // initializes to zero inputBuffer index
	currParser = 0; 		  // chooses how catch info on serial, can change is value according to new data read
	// Buffers
	bufferMsg[0] = {0}; 	  // initializes first element to zero
	// Strings
	secondMsg[0] = {0}; 	  // initializes first element to zero
	thirdMsg[0] = {0};        // initializes first element to zero
	// Floats
	firstFloatVar = 0.0;
	secondFloatVar = 0.0;
	thirdFloatVar = 0.0;
	// Timers
	replyinterval = 50;       // ms
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Taking info from USB */
void SerialComm::catchFromSerial(int modality){
/*Splitting buffer data into parts
mod 0: saves command for next instr, single word (Messages of type = <switchMod3>)
mod 1: stores not only the command, but also other data as the speeds e.g gamepad, 4 words  (Messages of type = <simplygo, 20.0, 10.0, 0.0> ) (strafe, forward, angular)
mod 2: Stores command and parameter as the speeds e.g gamepad (when angular speed is not needed and not sent by raspi), 3 words (Messages sample = <simplygo2, 20.0, 10.0> )
mod 3: Stores custom leds animations or speeds imposed by raspi e.g games reactions...5 words (Messages of type = <happy, red, wipe, 28, 0> )  useless for now! 
	when LEDs were placed to Nanos boards.
	---------> remove red animation!!! handled by Nano!
*/
	switch(modality){
		char * strokToken;                          // where store each subpart of string temporarily
		case 0:
			strokToken = strtok(inputBuffer,",");	// strtok() breaks string str into parts, using "," as delimiter, getting only the first (or unique) part of the string
			strcpy(bufferMsg, strokToken); 		    // copies into bufferMsg
		break;
		case 1:										//allows the identification of gamepad's commands
			strokToken = strtok(inputBuffer,",");	// gets the first part of the string
			strcpy(bufferMsg, strokToken); 		    // copies into bufferMsg as before

			strokToken = strtok(NULL, ",");			// gets the second part of the string
			firstFloatVar = atof(strokToken);	    // sets strafe speed, Parsing the C string str, interpreting its content as a floating point number
													// ..and returns its value as a double.
			strokToken = strtok(NULL, ",");			// gets the third part of the string
			secondFloatVar = atof(strokToken);		// sets forward speed, converting the string token to a float 

			strokToken = strtok(NULL, ",");			// gets the fourth and last section of the input msg
			thirdFloatVar = atof(strokToken);		// sets angular speed, converting the string token to a float 
		break;
		case 2:
			strokToken = strtok(inputBuffer,",");
			strcpy(bufferMsg, strokToken);

			strokToken = strtok(NULL, ",");
			firstFloatVar = atof(strokToken);

			strokToken = strtok(NULL, ",");
			secondFloatVar = atof(strokToken);
		break;
		//Messages of type = <happy, red, wipe, 28, 0> 
		case 3:										
			strokToken = strtok(inputBuffer,",");
			strcpy(bufferMsg, strokToken);

			strokToken = strtok(NULL, ",");
			strcpy(secondMsg, strokToken);

			strokToken = strtok(NULL, ",");
			strcpy(thirdMsg, strokToken);

			strokToken = strtok(NULL, ",");
			firstFloatVar = atof(strokToken);

			strokToken = strtok(NULL, ",");
			secondFloatVar = atof(strokToken);
		break;
	}
}
void SerialComm::acquireDataFromRaspi(int modality) {
/*
Receive data on serial shared with raspi, asap a new msg appears on serial
Serial.available() Returns the number of bytes arrived and ready to be read
Reds data already arrived and stored in the serial receive buffer (64 bytes)
Saves data into inputBuffer
Parses and stores correctly data calling one catchFromSerial
CurrParser is initialized to zero, than changed from raspi with a new msg
(For changing modality it's enough mod0, if currParser is different from mod0 is ok,
if next data discovered on serial are few than expected (by current modality) no prolem, in any case bufferMsg is captured)
*/
	if(Serial.available() > 0){ 				 // only if there are new unread data
		char x = Serial.read(); 				 // reads msg on serial
		if(x==endChar){ 					 	 // checks if received char is the keyword ">", if so the end of the string is reached
			isStillReading = false;				 // sets flag for correct reading
			okNewData = true;					 // sets flag for reply timing
			inputBuffer[receivedBytes] = 0;		 // resets inputBuffer
			catchFromSerial(currParser);		 // parses bufferMsg
		}
		if(isStillReading){						 // only if there's more to read
			inputBuffer[receivedBytes] = x;		 // puts collected data into inputBuffer
			receivedBytes ++;					 // increments buffer index for next iter
			if (receivedBytes == bufferSize)
				receivedBytes = bufferSize - 1;  // controls last element
		}
		if(x==startChar){						 // checks if received char is "<" ,so start is finded
			receivedBytes = 0;					 // initializes index for input buffer
			isStillReading = true;			     // asserts that is possible to start reading data
		}
	}
	just_changed = true; 						 // other msg cannot be received until the request is completed
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Getters*/
bool SerialComm::getJustChanged(){
	return just_changed;
}
char* SerialComm::getBufferMsg(){
	return bufferMsg;
}
char* SerialComm::getMsgSecond(){
	return secondMsg;
}
char* SerialComm::getMsgThird(){
	return thirdMsg;
}
float SerialComm::getFirstFloatVar(){
	return firstFloatVar;
}
float SerialComm::getSecondFloatVar(){
	return secondFloatVar;
}
float SerialComm::getThirdFloatVar(){
	return thirdFloatVar;
}
int SerialComm::getFirstIntVar(){
	return firstIntVar;
}
int SerialComm::getCurrParser(){
	return currParser;
}
bool SerialComm::getOkNewData(){
	return okNewData;
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Setters*/
void SerialComm::setCurrParser(int modality){
	currParser = modality;
}
void SerialComm::setOkNewData(bool data){
	okNewData = data;
}
void SerialComm::setJustChanged(bool change){
	just_changed = change;
}
