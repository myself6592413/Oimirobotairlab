/********************************************************************
* Oimi SerialComm.h - Library for handling communication with raspi.
*
* --notes
*	Designed for Oimi Robot needs,
*		eg. receiving 3 different msgs
*			receiving 3 different floats
*
* Created by Giacomo Colombo Airlab Politecnico di Milano 2020
*********************************************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Conditional compilation */
#ifndef SerialComm_h
#define SerialComm_h
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
#define bufferSize 200
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Class */
class SerialComm{
	private:
		/*************************************************************************************************** Variables */
		char startChar;				    // marker always needed to begin a msg
		char endChar;		 			// marker always needed at the end of a msg
		byte receivedBytes;		     	// inputBuffer index for
		int currParser; 				// chooses how catch info on serial, can change is value according to new data read
		// Buffers
		char inputBuffer[bufferSize]; 	// principal buffer
		char bufferMsg[bufferSize]; // where first and main command is stored
		char secondMsg[bufferSize]; // ledstrip color command
		char thirdMsg[bufferSize];  // ledstrip animation command
		// Variables
		int firstIntVar;
		float firstFloatVar;
		float secondFloatVar;
		float thirdFloatVar;
		// Timers
		unsigned long replyinterval;	// response pause
		// Flags
		bool just_changed;				// for manual movement
		bool isStillReading;			// for reading whole msg
		bool okNewData;					// if ok looks for data on serial

	public:
		/*************************************************************************************************** Constructor */
		SerialComm();
		/*************************************************************************************************** Methods */
		// Fundamentals
		void catchFromSerial(int modality);
		void acquireDataFromRaspi(int modality); //modality??? useless
		// Getters
		int getFirstIntVar();
		int getCurrParser();
		bool getOkNewData();
		bool getJustChanged();
		char* getBufferMsg();
		char* getMsgSecond();
		char* getMsgThird();
		float getFirstFloatVar();
		float getSecondFloatVar();
		float getThirdFloatVar();
		// Setters
		void setCurrParser(int modality);
		void setOkNewData(bool data);
		void setJustChanged(bool change);
};
#endif



