/**********************************************
* Oimi-Robot - look at CytronDriverC10.h for info
***********************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include "CytronDriver_C10.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructor */
CytronDriver_C10::CytronDriver_C10(){}

CytronDriver_C10::CytronDriver_C10(unsigned char MDIR, unsigned char MPWM)
{
	_MDIR = MDIR;
	_MPWM = MPWM;
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Motors */
void CytronDriver_C10::initDriver()
/*
Prepares driver to be used 
Configuring the specified pin to behave as an output
*/
{
	pinMode(_MDIR,OUTPUT);  
	pinMode(_MPWM,OUTPUT);
}

void CytronDriver_C10::setDirection(bool direction){
/*
Changes direction of a motor
Setting the outputs voltage (in the specified pin ):
	LOW for counterclockwise
	HIGH for clockwise  
*/
	if(!direction)
		digitalWrite(_MDIR, LOW);
	else
		digitalWrite(_MDIR, HIGH);
}

void CytronDriver_C10::setSpeed(int speed){ 
/*
Changes speed verse if given speed is negative
Set the correct speed writing an analog value on proper motor's pin 
Actually Actuates a single motor 
Speed is between [-255 , 255]
*/
	bool direction = 0;
	if(speed < 0){
		direction = 1;
		speed = -speed;
	}
	if(speed > 255)
		speed = 255;
	setDirection(direction);
	analogWrite(_MPWM, speed);
}


