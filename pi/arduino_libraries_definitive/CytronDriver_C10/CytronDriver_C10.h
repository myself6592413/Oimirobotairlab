/**************************************************************************************************************
* Oimi-Robot - Library for using cytron MDC10 motor drivers
* 
* --notes
*	one driver for each motors
*	one motors controlled at the time by one class instance 
*	according to HW wires setup (red channel B,black channel A)...dir 0 must be LOW + dir == 1 must be HIGH
*	
* Created by Giacomo Colombo Airlab Politecnico di Milano 2020 */
/*************************************************************************************************************/

/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Conditional compilation */
#ifndef CytronDriver_C10_h
#define CytronDriver_C10_h
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Class */
class CytronDriver_C10{
	private:
		/***************************************************************** Variables */
		unsigned char _MDIR;  //pwm signal input for speed control --> pins 9,11,10       
		unsigned char _MPWM;  //input for direction control --> pins 6,5,4  	
	public:
		/***************************************************************** Constructor */
		CytronDriver_C10();
		CytronDriver_C10(unsigned char MDIR, unsigned char MPWM);
		/***************************************************************** Functions */
		void initDriver();  // configures driver pins
		void setDirection(bool dir); // changes rotation direction of a motor 
		void setSpeed(int speed);    // powers up the motor with correct speed 
};
#endif
