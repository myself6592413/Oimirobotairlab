/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Oimi Movements_following.h - Library for wrapping movement methods
*
* --notes
*
*
*
* Created by Giacomo Colombo, Airlab Politecnico di Milano 2020
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Conditional compilation */
#ifndef Movements_h
#define Movements_h
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
#include "StandardCplusplus.h"
#include <vector>
#include "Adafruit_NeoPixel.h"
#include "OimiPid.h"
#include "Encoder.h"
#include "CytronDriver_C10.h"
#include "Sonars_EZ1.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
// Geometries 															
#define RADIUS 16.0											   			// robot radius [cm]
#define PERIOD 2.0 * (float) PI * RADIUS / FORWARD_SLOW_SPEED  			// period of a complete round [s]
#define PERIOD_MS PERIOD * 1000							       			// period [ms]
// Speeds [ms/s] ; [rad/s]
#define FORWARD_MIN_SPEED 15.0											// default speed
#define FORWARD_SLOW_SPEED 20.0
#define FORWARD_NORMAL_SPEED 24.0
#define FORWARD_QUICK_SPEED 28.0
#define FORWARD_FAST_SPEED 40.0
#define FORWARD_MAX_SPEED 33.0
#define ANGULAR_MIN_SPEED FORWARD_MIN_SPEED / RADIUS
#define ANGULAR_SLOW_SPEED FORWARD_SLOW_SPEED / RADIUS
#define ANGULAR_NORMAL_SPEED FORWARD_NORMAL_SPEED / RADIUS / 2
#define ANGULAR_QUICK_SPEED FORWARD_QUICK_SPEED / RADIUS / 2
#define ANGULAR_FAST_SPEED FORWARD_FAST_SPEED / RADIUS / 3	    		// divided by 3 for not exceede max angular speed
#define ANGULAR_MAX_SPEED FORWARD_MAX_SPEED / RADIUS / 3        		// divided by 3 for not exceede max angular speed
// Patterns' speeds
#define ANGRY_FORWARD_SPEED 53.0
#define SCARED_FORWARD_SPEED 47.0
#define SAD_FORWARD_SPEED  16.0
#define HAPPY_FORWARD_SPEED 39.0
#define ANGRY_ANGULAR_SPEED 2.0
#define SCARED_ANGULAR_SPEED 1.0
#define SAD_ANGULAR_SPEED  1.0
#define HAPPY_ANGULAR_SPEED 1.5

#define MIN_STRAFE 0.01													// limits
// Joystick's speeds
#define MIN_STRAFE 0.01													// limits
#define MIN_FORWARD 0.01
#define MIN_ANGULAR 0.1
#define MAX_FORWARD_CONTROL 55
#define MAX_ANGULAR_CONTROL 1.5
#define MAX_FORWARD_AUTO 60
#define MAX_ANGULAR_AUTO 2
#define TURBO 0.5														// multiplier for increasing speed
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Enumerations */
/*
Enums are placed outside of the Movement class, for sparing size of other files that are using them.
For increasing intuitivity, Kind of movement is divided into different enums, according to their semantic meaning, 
so each enum needs to start from correct list's number, instead of beginning from zero as usual. 
During execution is treated as unsigned char anyway.
*/
enum BasicMove{
	no_movement,
	go_straight,
	go_back
};
enum SimpleTurnMove{
	turn30_l 	 	= 3,
	turn30_r 	 	= 4,
	turn35_l        = 5,
	turn35_r        = 6,
	turn45_l 	 	= 7,
	turn45_r 	 	= 8,
	turn60_l 	 	= 9,
	turn60_r 	 	= 10,
	turn90_l 	 	= 11,
	turn90_r 	 	= 12,
	turn120_l 	 	= 13,
	turn120_r 	 	= 14,
	turn150_l  	 	= 15,
	turn150_r 	 	= 16,
	turn180_l 		= 17,
	turn180_r 	 	= 18,
	turn360_l 	 	= 19,
	turn360_r 	 	= 20,
	turn_until_l 	= 21,
	turn_until_r 	= 22,
	turn_until_bl   = 23,
	turn_until_br   = 24
};
enum FancyTurnMove{
	go_left 		= 25,
	go_right 		= 26,
	go_right_2		= 27,
	go_left_2		= 28
};
enum TranslateMove{
	translate_l  	= 29,
	translate_r  	= 30,
	translate_fl 	= 31,
	translate_fr 	= 32,
	translate_bl 	= 33,
	translate_br 	= 34,
	forward_until	= 35,
	backward_until	= 36
	
};
enum ComplexMove{
	autonomous 	    = 37,
	following 		= 38,
	change          = 39
};
enum PatternMove{
	refuse 		 	= 40,
	agree 		 	= 41,
	dance 		 	= 42,
	scare 	 	 	= 43,
	angriness    	= 44,
	sadness		 	= 45,
	normality	 	= 46,
	happiness	 	= 47,
	enthusiasm	 	= 48,
	go_straight_1   = 49,
	anger   		= 50
};
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Class */
class Movements{

	private:
		/********************************************************************************************************************* Variables */
		// Enums
		unsigned char actual_movement;					//current movement in execution, updated by driveMe() manager
		// Speeds
		float actual_forward_speed;    					// used by autonomous and complex movements
		float actual_angular_speed;    					// used by autonomous and complex movements
		float strafe,forward,angular;  					// for manual movement
		// Kinematics
		float dTh, speedTh, speedX;
		// Distances
		float allowed_distance;		   					// used by autonomous movement
		float detected_dist;           					// used by translations
		// Positions
		float resp_pos;
		Range resp_ce;
		float startPosX;
	 	float startPosY;
  		float startPosTh;
		// Angles
		float exactAngle;								// turning according to a fixed angle
		// Flags movements
		int just_called;								//for informing that driveMe is called the first time
		bool primo_giro;
		bool adessostampi;
		bool letLedsOn;
		bool beginning;
		bool momentanous;
		bool singleTrial;
		bool okPrint;
		bool hasStopped;								// true when robot change path randomly (during autonomous move), useful for setting forbidden movement
		bool isMoving;                 					// alwayas active when robot is actually moving
		bool isSelfGoverning;             				// always on when movement is autonomous, not only when actual_mov = autonomous, but also when actual_mov is another one during auto situation
		bool isFollowing;              					// always on when is following...
		bool isControlled;             					// states if the type of movement is controlled by gamepad
		bool isTranslating;            					// for settting correct speeds only first time during a translation
		bool isCustom;                 					// states when use custom speeds
		bool isSafe;									// active when is possible to move forward despite standard constraints in effect
		bool isEnded;				   					// true when a complete action is performed, for pausing Arduino
		bool randomStop; 			   					// while goStraight/goBack randomStop 6//change forbidden if is active
		bool wasBlocked;			   					// states that last state was blocked everywhere by obstacles
		bool freePath;									// allows turning until a free front path is available for goLeft()/goRight()
		bool goingAhead;
		// Flags autonomous
		bool auto_action;         						// allow stopMoving to set next actual_movement after rotation
		bool auto_hurdle; 		   						// when there are obstacles everywhere during autonomous
		bool auto_change;        						// when path is changed casually during autonomous
		bool auto_variation1;        					// for delay jump from stopmoving to changeorientation ok need 2 flag
		bool auto_variation2;        					// for delay jump from stopmoving to changeorientation ok need 2 flag
		bool controlloforbidden;
		// Flags following
		bool follo_hurdle; 		   						// when there are obstacles everywhere during following
		// Flags Leds....useless now that i use nano???
		bool okLeds;              						// true if is time to switch the led
		bool isLighten;                 				// active when has been decided to move with leds on
		// Counters
		int counter_moves; 			   					// useful in complex movements
		int numberTurning;								// useful in following
		int event;					   					// used in rotation
		// Following Movement
		int rotateType;									// for basic rotation, is false when rotation depends on angles, true when depends on time
		// Random behaviours
		int stopme; 				   					// used by goStraight()
		int rand; 					   					// used by complete instructions
		int old_choice; 			   					// used by turnDuetoHurdle()
		int backchoice; 			   					// used by goBack()
		int backangle; 				   					// used by goBack()
		int backverse; 				   					// used by goBack()
		int randomNumb;									// used by FollowMe()
		int cont_cancel;
		// Timers
		unsigned long movStopTime, movStartTime;
		unsigned long loopTime, startTime;
		unsigned long followTime;
		unsigned long seekInterval;
		unsigned long stallInterval;
		unsigned long stallBackInterval;
  		unsigned long previousTurnTime;
		unsigned long rotationTime;
		unsigned long rotationInterval;
  		unsigned long intervalloPrint;
  		unsigned long pause;
  		unsigned long interval;
  		unsigned long translateTime;
  		// Objects
		OimiPid & _oimiPid;			  					// for handling odometry and kinematics
		Sonars_EZ1 & _sonars; 							// for handling obstacles detection 
														//mi serve cmq per following per ora a meno che non metto come extern pure i timers      					
		Adafruit_NeoPixel & _strip;			    		// for handling ledstripe animation during movements
		// Structs
		//Obs current_env; 			   					// current environemnt situation around the robot 
		maxSonar allowed; 	  		   					// actual sonar output
		// Customs Speeds (those received from raspi)
		float customSpeed;
		float customDistance;
		float customAngle;
		// Directions
		Verse forbidden_movement;   					// for avoiding repeating again last pattern sometimes (e.g in autonomous after casual stop)
		Verse lastDirection;							// autonomous last movement performed, useful for setting forbidden movement in different ways
		Verse originDirection1;							// following
		Verse originDirection2;							// following
		// Leds
		const char* currentAnimation;					// ongoing
		const char* currentColor;						// ongoing color of leds
		float last_pose;
		/******************************************************************************************************************************************** Methods */
		// Key movements
		void proceed(float distance,int side, int counter);								// std straight navigation (used with arbitrary number of fixed steps)
		void rotate(float dth, float speedTh, float speedX, int counter, int type);		// std rotation (used with arbitrary number of fixed steps)
		// Principal movements
		void turn(float angle, int direction);					// all kind of rotations possible
		void goStraight();									  	// forward navigation with stop
		void goStraight(int index);							  	// forward navigation without stop
		void goBack();                                        	// reverse movement
		void goLeft(int type);                          		// left movement (no stop)
		void goLeft(float angle);
		void goRight(int type);                         		// right movement (no stop)
		void goRight(float angle);
		void goBackwardUntil(float distace,float speed);        // backward fixed action
		void goForwardUntil(float distance,float speed);        // forward fixed action
		// Translation actions
		void translateLeft();
		void translateLeft2(int event);
		void translateRight();
		void translateFrontLeft();
		void translateFrontRight();
		void translateBackLeft();
		void translateBackRight();
		// Autonomous actions
		void goDirectly(float module, float xp, float yp);      // manual omnidirectional navigation
		void goNormally();                                     	// standard navigation
		void moveLeft();                                        // complete instructions used by freeMe()
		void moveRight();
		void moveForward();
		void moveBackward();
		void changeOrientation();                               // changes casually way during autonomous movement
		void turnDuetoHurdle(); 							    // rotates finding a free path
		// Pattern movements
		void letsDance();
		void okAgree();
		void koRefuse();
		void actAngry();
		void actSad();
		void actScared();
		void actNormal();
		void actHappy();
		void actEnthusiast();

	public:
		/****************************************************************************************************************************** Constructors */
		Movements(OimiPid & oimiPid, Sonars_EZ1 & sonars, Adafruit_NeoPixel & strip);
		/****************************************************************************************************************************** Methods */
		// Getters
		bool getisSelfGoverning();
        bool getIsFollowing();
		bool getIsControlled();
		bool getAutonomousFirst();
		bool getAutonomousRed();
		bool getAutonomousRedCasual();
        bool getIsMoving();
        bool getIsEnded();
		Verse getForbiddenMov();
		float getAllowedDistance();
		float getStrafe();
		float getForward();
		float getAngular();
		float getActualAngularSpeed();
		float getActualForwardSpeed();
		float getStartPosX();
		float getStartPosY();
		float getStartPosTh();
		float getCustomDistance();
		float getCustomSpeed();
		float getCustomAngle();
		bool getGoingAhead();
		unsigned char getActualMovement();
		const char* getCurrentAnimation();
		const char* getCurrentColor();
		// Setters
		void setJustCalled(int option);
		void setOkLeds(bool decision);
		void setOkPrint(bool decision);
		void setIsEnded(bool decision);
		void setIsMoving(bool decision);
		void setisSelfGoverning(bool decision);
		void setIsFollowing(bool decision);
		void setIsControlled(bool decision);
		void setIsTranslating(bool decision);
		void setAutonomousFirst(bool first);
		void setAutonomousRed(bool red);
		void setActualMovement(unsigned char speed);
		void setActualAngularSpeed(float speed);
		void setActualForwardSpeed(float speed);
		void setStrafeSpeed(float speed);
		void setForwardSpeed(float speed);
		void setAngularSpeed(float speed);
		void setAllowedDistance(float dis);
		void setCurrentAnimation(const char* animation);                 // for ledstripe, set by raspi
		void setCurrentColor(const char* color);
		void setCustomAngle(float angle);                                // for games and reactions, set by raspi
		void setCustomDistance(float distance);
		void setCustomSpeed(float speed);
		void setForbiddenMov(Verse forbidden);
		void setupTranslateTime();
		// Kinematics
		void calcStrafeForwardSpeeds(float module, float xp, float yp);  // finds forward and strafe speed having an exact vector
		void putAngularSpeed(float speed); 							     // sets angular speed directly
		// Distances
		float chooseForSpeedsAndDistance(maxSonar distance);  		     // sets correct speeds and choosing max allowed distance
		// Navigation
		void setupMovement(); 								  		     // makes robot ready to move, initializes with no movement as default
		void setupMovement(unsigned char movement);                		 // makes robot ready to move, beginning with a specific movement
		void stopMoving();                                    		     // stops robot
		// 
		void freeMe();													 // autonomous navigation
		void followMe();
		void driveMe();										   		     // movement dispatcher
		// Leds
		void lightUpLeds(const char* color, const char* animation);		 // handles led stripe during movement
		// Debugging
		// New
		void translateRobot(float distance, float speedF, float speedA, int event, int type, int side);
		void translateRobot2(float distance, float speedF, float speedA, int event, int type, int side, int scanlation);
};
#endif
