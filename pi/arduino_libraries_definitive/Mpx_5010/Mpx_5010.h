/*******************************************************************************************
* Oimi-Robot Mpx_5010.h library, allows usage of MPX pressure sensors
* Oimi has 5 sensors in its body, starting from front-left one, in counterclockwise order from mpx0 to mpx4 
* Created by Giacomo Colombo Airlab Politecnico di Milano 2020
*
* --notes
*	Transfer functions 
*   try analog new??
*	
*
********************************************************************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Conditional compilation */
#ifndef Mpx_5010_h
#define Mpx_5010_h
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include "Arduino.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
#define NUM_MPX 5 
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Class */

class Mpx_5010{
private:
    /******************************************************************* Variables */
	// Arduino pins
	uint8_t _anPin0;
	uint8_t _anPin1;
	uint8_t _anPin2;
	uint8_t _anPin3;
	uint8_t _anPin4;

	/*const int _anPin0; ?? 
	const int _anPin1;
	const int _anPin2;
	const int _anPin3;
	const int _anPin4;
	*/
	//unsigned int val0,val1,val2,val3,val4;
	
	// Pressures
	int outputVals[NUM_MPX];       // raw analog voltage
	float voltageVals[NUM_MPX];    // real analog voltage 
	float pressValsKPa[NUM_MPX];   // Kilo Pascal
	float pressValsBar[NUM_MPX];   // Bar 
	float pressValsPsi[NUM_MPX];   // Pound per square inch 
	float pressValsMmhg[NUM_MPX];  // Torr 
	float mappedVals[NUM_MPX];	   // analog voltage new mapping	
	//newwwwww
	float pressValsKPa2[NUM_MPX];   // Kilo Pascal
	float pressValsBar2[NUM_MPX];   // Bar 
	float pressValsPsi2[NUM_MPX];   // Pound per square inch 
	float pressValsMmhg2[NUM_MPX];  // Torr 
public:
	/********************************************************************************************* Constructor */
	Mpx_5010(uint8_t anpin0, uint8_t anpin1, uint8_t anpin2, uint8_t anpin3, uint8_t anpin4);
	/********************************************************************************************* Functions */
	// Auxiliary 
	float mpxMap(float x, float in_min, float in_max, float out_min, float out_max); 
	// Calculations without mapping 
	void detectPressures();
	// Convert voltage in pressure
	void convertPressures();
	void convertPressures2();
	void mappingPressures();
	// Getters 
	// Raw anog voltage ouputs
	int getRawFrontLeft();
	int getRawFrontRight();
	int getRawRight();
	int getRawBack();
	int getRawLeft();
	// Anog voltage ouputs
	float getPressFrontLeft();
	float getPressFrontRight();
	float getPressRight();
	float getPressBack();
	float getPressLeft();
	// KPa ouputs
	float getKPAFrontLeft();
	float getKPAFrontRight();
	float getKPARight();	 
	float getKPABack();
	float getKPALeft();
	// Bar ouputs
	float getBARFrontLeft();
	float getBARFrontRight();
	float getBARRight();
	float getBARBack();
	float getBARLeft();
	// Psi ouputs
	float getPSIFrontLeft();
	float getPSIFrontRight();
	float getPSIRight();
	float getPSIBack();
	float getPSILeft();
	// Mmhg ouputs
	float getMMHGFrontLeft();
	float getMMHGFrontRight();
	float getMMHGRight();
	float getMMHGBack();
	float getMMHGLeft();
	// Mapped ouputs
	float getMAPFrontLeft();
	float getMAPFrontRight();
	float getMAPRight();
	float getMAPBack();
	float getMAPLeft();
	// Debugging
	void printPressureAll();
};
#endif
