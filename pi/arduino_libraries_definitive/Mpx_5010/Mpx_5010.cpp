/*******************************************************************************
* Oimi-Robot Mpx_5010 pressure sensor library --> look at Mpx_5010.h for info
*******************************************************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Mpx_5010.h>
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructor */
Mpx_5010::Mpx_5010(uint8_t an_0, uint8_t an_1, uint8_t an_2, uint8_t an_3, uint8_t an_4){
  _anPin0 = an_0;
  _anPin1 = an_1;
  _anPin2 = an_2;
  _anPin3 = an_3;
  _anPin4 = an_4;
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Calculations */
float Mpx_5010::mpxMap(float x, float in_min, float in_max, float out_min, float out_max){
/*
Re-maps a number from one range to another
*/
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void Mpx_5010::detectPressures(){
/*
Finds raw analog voltage scans, reading the input on all Mpx_5010 analog pins
5V supply yields ~9.8mV/in 
Analog measurement is between 0 and 1023
Resolution between readings: 5 volts / 1023 units
Finds out actual voltage outputs,
transforming the analog reading raws scans [0-1023] into the actual voltage [0 - 5V]
*/	
	outputVals[0] = analogRead(_anPin1); // value from the specified analog pin
	outputVals[1] = analogRead(_anPin0);
	outputVals[2] = analogRead(_anPin2);
	outputVals[3] = analogRead(_anPin3);
	outputVals[4] = analogRead(_anPin4);

	voltageVals[0] = outputVals[0] * 5.0 / 1023.0;
	voltageVals[1] = outputVals[1] * 5.0 / 1023.0;
	voltageVals[2] = outputVals[2] * 5.0 / 1023.0;
	voltageVals[3] = outputVals[3] * 5.0 / 1023.0;
	voltageVals[4] = outputVals[4] * 5.0 / 1023.0;
}
void Mpx_5010::convertPressures(){
/*
Obtains real standard pressure measurements, from voltage outputs
--> Read Mpx5010 datasheet for precise info about transfer functions volt --> kPa
*/	
	pressValsKPa[0] = (voltageVals[0] - 0.2) / 0.45;
	pressValsKPa[1] = (voltageVals[1] - 0.2) / 0.45;
	pressValsKPa[2] = (voltageVals[2] - 0.2) / 0.45;
	pressValsKPa[3] = (voltageVals[3] - 0.2) / 0.45;
	pressValsKPa[4] = (voltageVals[4] - 0.2) / 0.45;

	pressValsBar[0] = pressValsKPa[0] / 100;
    pressValsBar[1] = pressValsKPa[1] / 100;
    pressValsBar[2] = pressValsKPa[2] / 100;
    pressValsBar[3] = pressValsKPa[3] / 100;
    pressValsBar[4] = pressValsKPa[4] / 100;

	pressValsPsi[0] = pressValsKPa[0] / 6.89476;
	pressValsPsi[1] = pressValsKPa[1] / 6.89476; 
	pressValsPsi[2] = pressValsKPa[2] / 6.89476; 
	pressValsPsi[3] = pressValsKPa[3] / 6.89476; 
	pressValsPsi[4] = pressValsKPa[4] / 6.89476; 

	pressValsMmhg[0] = pressValsKPa[0] / 0.1333224;
    pressValsMmhg[1] = pressValsKPa[1] / 0.1333224;
    pressValsMmhg[2] = pressValsKPa[2] / 0.1333224;
    pressValsMmhg[3] = pressValsKPa[3] / 0.1333224;
    pressValsMmhg[4] = pressValsKPa[4] / 0.1333224;
}
void Mpx_5010::mappingPressures(){
/*
Changes outputs range
Decide a reasonable mapping!??
ouputvals o voltagevals?
*/	
	mappedVals[0] = mpxMap(voltageVals[0], 0, 1023, 0, 2048);
	mappedVals[1] = mpxMap(voltageVals[1], 0, 1023, 0, 2048);
	mappedVals[2] = mpxMap(voltageVals[2], 0, 1023, 0, 2048);
	mappedVals[3] = mpxMap(voltageVals[3], 0, 1023, 0, 2048);
	mappedVals[4] = mpxMap(voltageVals[4], 0, 1023, 0, 2048);
}
void Mpx_5010::convertPressures2(){
/*
Obtains real standard pressure measurements, from voltage outputs
--> Read Mpx5010 datasheet for precise info about transfer functions volt --> kPa
*/	
	pressValsKPa2[0] = (mappedVals[0] - 0.2) / 0.45;
	pressValsKPa2[1] = (mappedVals[1] - 0.2) / 0.45;
	pressValsKPa2[2] = (mappedVals[2] - 0.2) / 0.45;
	pressValsKPa2[3] = (mappedVals[3] - 0.2) / 0.45;
	pressValsKPa2[4] = (mappedVals[4] - 0.2) / 0.45;

	pressValsBar2[0] = pressValsKPa2[0] / 100;
    pressValsBar2[1] = pressValsKPa2[1] / 100;
    pressValsBar2[2] = pressValsKPa2[2] / 100;
    pressValsBar2[3] = pressValsKPa2[3] / 100;
    pressValsBar2[4] = pressValsKPa2[4] / 100;

	pressValsPsi2[0] = pressValsKPa2[0] / 6.89476;
	pressValsPsi2[1] = pressValsKPa2[1] / 6.89476; 
	pressValsPsi2[2] = pressValsKPa2[2] / 6.89476; 
	pressValsPsi2[3] = pressValsKPa2[3] / 6.89476; 
	pressValsPsi2[4] = pressValsKPa2[4] / 6.89476; 

	pressValsMmhg2[0] = pressValsKPa2[0] / 0.1333224;
    pressValsMmhg2[1] = pressValsKPa2[1] / 0.1333224;
    pressValsMmhg2[2] = pressValsKPa2[2] / 0.1333224;
    pressValsMmhg2[3] = pressValsKPa2[3] / 0.1333224;
    pressValsMmhg2[4] = pressValsKPa2[4] / 0.1333224;
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Getters */
// Raw analog voltage ouputs
int Mpx_5010::getRawFrontLeft(){
	return outputVals[0];
}
int Mpx_5010::getRawFrontRight(){
	return outputVals[1];
}
int Mpx_5010::getRawRight(){
	return outputVals[2];
}
int Mpx_5010::getRawBack(){
	return outputVals[3];
}
int Mpx_5010::getRawLeft(){
	return outputVals[4];
}
// Analog voltage ouputs
float Mpx_5010::getPressFrontLeft(){
	return voltageVals[0];
}
float Mpx_5010::getPressFrontRight(){
	return voltageVals[1];
}
float Mpx_5010::getPressRight(){
	return voltageVals[2];
}
float Mpx_5010::getPressBack(){
	return voltageVals[3];
}
float Mpx_5010::getPressLeft(){
	return voltageVals[4];
}
// KPa ouputs
float Mpx_5010::getKPAFrontLeft(){
	return pressValsKPa[0];
}
float Mpx_5010::getKPAFrontRight(){
	return pressValsKPa[1];
}
float Mpx_5010::getKPARight(){
	return pressValsKPa[2];
}
float Mpx_5010::getKPABack(){
	return pressValsKPa[3];
}
float Mpx_5010::getKPALeft(){
	return pressValsKPa[4];
}
// Bar ouputs
float Mpx_5010::getBARFrontLeft(){
	return pressValsBar[0];
}
float Mpx_5010::getBARFrontRight(){
	return pressValsBar[1];
}
float Mpx_5010::getBARRight(){
	return pressValsBar[2];
}
float Mpx_5010::getBARBack(){
	return pressValsBar[3];
}
float Mpx_5010::getBARLeft(){
	return pressValsBar[4];
}
// Psi ouputs
float Mpx_5010::getPSIFrontLeft(){
	return pressValsPsi[0];
}
float Mpx_5010::getPSIFrontRight(){
	return pressValsPsi[1];
}
float Mpx_5010::getPSIRight(){
	return pressValsPsi[2];
}
float Mpx_5010::getPSIBack(){
	return pressValsPsi[3];
}
float Mpx_5010::getPSILeft(){
	return pressValsPsi[4];
}
// Mmhg ouputs
float Mpx_5010::getMMHGFrontLeft(){
	return pressValsMmhg[0];
}
float Mpx_5010::getMMHGFrontRight(){
	return pressValsMmhg[1];
}
float Mpx_5010::getMMHGRight(){
	return pressValsMmhg[2];
}
float Mpx_5010::getMMHGBack(){
	return pressValsMmhg[3];
}
float Mpx_5010::getMMHGLeft(){
	return pressValsMmhg[4];
}
// mapped ouputs
float Mpx_5010::getMAPFrontLeft(){
	return mappedVals[0];
}
float Mpx_5010::getMAPFrontRight(){
	return mappedVals[1];
}
float Mpx_5010::getMAPRight(){
	return mappedVals[2];
}
float Mpx_5010::getMAPBack(){
	return mappedVals[3];
}
float Mpx_5010::getMAPLeft(){
	return mappedVals[4];
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Debugging */
void Mpx_5010::printPressureAll(){
	Serial.println("ALL PRESSURES scans: ");
	Serial.println("-----------------------------------------------------------------");
	Serial.print(" ");
	Serial.print("RAW: ");
	Serial.print(outputVals[0]);
	Serial.print(", ");
	Serial.print(outputVals[1]);
	Serial.print(", ");
	Serial.print(outputVals[2]);
	Serial.print(", ");
	Serial.print(outputVals[3]);
	Serial.print(", ");
	Serial.println(outputVals[4]);
	Serial.print(" ");
	Serial.print("VOLT: ");
	Serial.print(voltageVals[0]);
	Serial.print(", ");
	Serial.print(voltageVals[1]);
	Serial.print(", ");
	Serial.print(voltageVals[2]);
	Serial.print(", ");
	Serial.print(voltageVals[3]);
	Serial.print(", ");
	Serial.println(voltageVals[4]);
	Serial.print(" ");
	Serial.print("KPA: ");
	Serial.print(pressValsKPa[0]);
	Serial.print(", ");
	Serial.print(pressValsKPa[1]);
	Serial.print(", ");
	Serial.print(pressValsKPa[2]);
	Serial.print(", ");
	Serial.print(pressValsKPa[3]);
	Serial.print(", ");
	Serial.println(pressValsKPa[4]);
	Serial.print(" ");
	Serial.print("BAR: ");
	Serial.print(pressValsBar[0]);
	Serial.print(", ");
	Serial.print(pressValsBar[1]);
	Serial.print(", ");
	Serial.print(pressValsBar[2]);
	Serial.print(", ");
	Serial.print(pressValsBar[3]);
	Serial.print(", ");
	Serial.println(pressValsBar[4]);
	Serial.print(" ");
	Serial.print("PSI: ");
	Serial.print(pressValsPsi[0]);
	Serial.print(", ");
	Serial.print(pressValsPsi[1]);
	Serial.print(", ");
	Serial.print(pressValsPsi[2]);
	Serial.print(", ");
	Serial.print(pressValsPsi[3]);
	Serial.print(", ");
	Serial.println(pressValsPsi[4]);
	Serial.print(" ");
	Serial.print("MMHG: ");
	Serial.print(pressValsMmhg[0]);
	Serial.print(", ");
	Serial.print(pressValsMmhg[1]);
	Serial.print(", ");
	Serial.print(pressValsMmhg[2]);
	Serial.print(", ");
	Serial.print(pressValsMmhg[3]);
	Serial.print(", ");
	Serial.println(pressValsMmhg[4]);
	Serial.print(" ");
	Serial.print("MAPPED: ");
	Serial.print(mappedVals[0]);
	Serial.print(", ");
	Serial.print(mappedVals[1]);
	Serial.print(", ");
	Serial.print(mappedVals[2]);
	Serial.print(", ");
	Serial.print(mappedVals[3]);
	Serial.print(", ");
	Serial.println(mappedVals[4]);


	Serial.println("###############################################");
	Serial.print(", ");
	Serial.print("KPA2: ");
	Serial.print(pressValsKPa2[0]);
	Serial.print(", ");
	Serial.print(pressValsKPa2[1]);
	Serial.print(", ");
	Serial.print(pressValsKPa2[2]);
	Serial.print(", ");
	Serial.print(pressValsKPa2[3]);
	Serial.print(", ");
	Serial.println(pressValsKPa2[4]);
	Serial.print(" ");
	Serial.print("BAR2: ");
	Serial.print(pressValsBar2[0]);
	Serial.print(", ");
	Serial.print(pressValsBar2[1]);
	Serial.print(", ");
	Serial.print(pressValsBar2[2]);
	Serial.print(", ");
	Serial.print(pressValsBar2[3]);
	Serial.print(", ");
	Serial.println(pressValsBar2[4]);
	Serial.print(" ");
	Serial.print("PSI2: ");
	Serial.print(pressValsPsi2[0]);
	Serial.print(", ");
	Serial.print(pressValsPsi2[1]);
	Serial.print(", ");
	Serial.print(pressValsPsi2[2]);
	Serial.print(", ");
	Serial.print(pressValsPsi2[3]);
	Serial.print(", ");
	Serial.println(pressValsPsi2[4]);
	Serial.print(" ");
	Serial.print("MMHG2: ");
	Serial.print(pressValsMmhg2[0]);
	Serial.print(", ");
	Serial.print(pressValsMmhg2[1]);
	Serial.print(", ");
	Serial.print(pressValsMmhg2[2]);
	Serial.print(", ");
	Serial.print(pressValsMmhg2[3]);
	Serial.print(", ");
	Serial.println(pressValsMmhg2[4]);
	Serial.print(" ");


}
