/*
* Oimi-Robot - see OimiPid.h for further info 
*
*/

#include "OimiPid.h"

//constructor
OimiPid::OimiPid(CytronDriver_C10 & m1, CytronDriver_C10 & m2, CytronDriver_C10 & m3, Encoder & e1, Encoder & e2, Encoder & e3)
: _m1(m1), _m2(m2), _m3(m3), _e1(e1),_e2(e2),_e3(e3) {
  _m1.initDriver();
  _m1.setSpeed(0);
  _m2.initDriver();
  _m2.setSpeed(0);
  _m3.initDriver();
  _m3.setSpeed(0);

   posX=0;
   posY=0;
   posTh=0;
   speedX=0;
   speedY=0;
   speedTh=0;

   //desired speeds vector
   targetSpeed[0] = 0.0;
   targetSpeed[1] = 0.0;
   targetSpeed[2] = 0.0;
   //actual speeds vector
   actualSpeed[0] = 0.0;
   actualSpeed[1] = 0.0;
   actualSpeed[2] = 0.0;
   //pwm-vals (output pid value) vector
   pwmAmount[0] = 0.0;
   pwmAmount[1] = 0.0;
   pwmAmount[2] = 0.0;
   //errors vector
   previousError[0]=0.0;
   previousError[1]=0.0;
   previousError[2]=0.0;
   //Integrative term vector
   InterTerm[0]=0.0;
   InterTerm[1]=0.0;
   InterTerm[2]=0.0;
   //gains
   Kp = KP;
   Ki = KI;
   Kd = 0.0;
   //last time loop started
   lastMilliLoop=0;
   //last time vector for deltaT calculation to update
   lastMillis[0]=0;
   lastMillis[1]=0;
   lastMillis[2]=0;
   //delta Pid
   deltaPid=0;

}

void OimiPid::PIDLoop(){
	//Serial.println("enterOimiPidPIDLoop");
	deltaPid=millis()-lastMilliLoop;
	// enter tmed loop
	if(deltaPid >= LOOPTIME)   {
	   lastMilliLoop=millis();
	   //difference between time when the position are measured
	   long deltaT;
	   //actualSpeed vector used c
	   long ActualPos[3];

	   ActualPos[0]=_e1.read();
	   deltaT=millis()-lastMillis[0];
	   //calculate speed
	   calcMotorRad(deltaT,ActualPos[0],0);
	   lastMillis[0] = millis();
	   pwmAmount[0]= updatePid(targetSpeed[0], actualSpeed[0], 0);
	   _m1.setSpeed(pwmAmount[0]);


	   ActualPos[1]=_e2.read();
	   deltaT=millis()-lastMillis[1];
	   calcMotorRad(deltaT,ActualPos[1],1);
	   lastMillis[1] = millis();
	   pwmAmount[1]= updatePid(targetSpeed[1], actualSpeed[1] ,1);
	   _m2.setSpeed(pwmAmount[1]);

	   ActualPos[2]=_e3.read();
	   deltaT=millis()-lastMillis[2];
	   calcMotorRad(deltaT,ActualPos[2],2);
	   lastMillis[2] = millis();
	   pwmAmount[2]= updatePid(targetSpeed[2], actualSpeed[2] ,2);
	   _m3.setSpeed(pwmAmount[2]);
	   // PWM value comput
	   // speed calculation
	   //kinematics
	   computeDirectKinematics();
	   estimateOdometry(deltaPid);
	}

}

void OimiPid::PIDLoop2(){
	       long deltaT;
	       long ActualPos[3];
	       ActualPos[0]=_e1.read();
	       deltaT=millis()-lastMillis[0];
	       calcMotorRad(deltaT,ActualPos[0],0);// calculate speed
	       lastMillis[0] = millis();

	       ActualPos[1]=_e2.read();
	       deltaT=millis()-lastMillis[1];
	       calcMotorRad(deltaT,ActualPos[1],1);
	       lastMillis[1] = millis();

	       ActualPos[2]=_e3.read();
	       deltaT=millis()-lastMillis[2];
	       calcMotorRad(deltaT,ActualPos[2],2);
	       lastMillis[2] = millis();

		   // compute PWM value
	       pwmAmount[0]= updatePid(targetSpeed[0], actualSpeed[0], 0);
	       pwmAmount[1]= updatePid(targetSpeed[1], actualSpeed[1] ,1);
	       pwmAmount[2]= updatePid(targetSpeed[2], actualSpeed[2] ,2);

	       // calculate speed
	       _m1.setSpeed(abs(pwmAmount[0]));
	       _m2.setSpeed(abs(pwmAmount[1]));
	       _m3.setSpeed(abs(pwmAmount[2]));

	       computeDirectKinematics();
	       estimateOdometry(deltaPid);
}

void OimiPid::nonHolonomicRun(float forward, float angular) {
    //Serial.println("enter nonHolonomicRun");//speed scomposition
    const float dy12 = C30_R * forward;
    const float dthz123 = mL_R * angular;
    // Wheel angular speeds
    targetSpeed[0] = + dy12 + dthz123;
    targetSpeed[1] = + dthz123;
    targetSpeed[2] = - dy12 + dthz123;

}

void OimiPid::holonomicRun(float strafe, float forward, float angular) {
	#define m1_R     (-1.0 / wheel_radius)
	#define mL_R     (-robot_radius / wheel_radius)
	#define C60_R    (0.500000000 / wheel_radius)   // cos(60°) / R
	#define C30_R    (0.866025400 / wheel_radius)   // cos(30°) / R
    const float dx12 = C60_R * strafe;
	const float dy12 = C30_R * forward;
	const float dthz123 = mL_R * angular;

	targetSpeed[0] = dx12 + dy12 + dthz123;
	targetSpeed[1] = m1_R * strafe + dthz123; //it's the backward motor that permit rotation
	targetSpeed[2] = dx12 - dy12 + dthz123;
}

void OimiPid::completeStop(){
	_m1.setSpeed(0);
	_m2.setSpeed(0);
	_m3.setSpeed(0);
    for(int i=0;i<MOTORS;i++){
    	actualSpeed[i]=0;
	  	targetSpeed[i]=0;
	  	InterTerm[i]=0;
  }
  	posX=0;
	posY=0;
	posTh=0;
	speedX=0;
	speedY=0;
	speedTh=0;
}

void OimiPid::simpleStop(){
	_m1.setSpeed(0);
	_m2.setSpeed(0);
	_m3.setSpeed(0);
	for(int i=0;i<MOTORS;i++){
  		actualSpeed[i]=0;
  		targetSpeed[i]=0;
  }
}

void OimiPid::partialStop(){
	_m1.setSpeed(0);
	_m2.setSpeed(0);
	_m3.setSpeed(0);
	for(int i=0;i<MOTORS;i++){
		actualSpeed[i]=0;
		targetSpeed[i]=0;
	    InterTerm[i]=0;
	}
}

void OimiPid::resetIntegTerm(){
	for (int i=0;i<MOTORS;i++){
		InterTerm[i]=0;
	}
}
void OimiPid::resetPoses(){
	setPosX(0);
	setPosY(0);
	setPosTh(0);
}
void OimiPid::runMotors(float m1, float m2, float m3){ //useless
    // Motor setpoints
	targetSpeed[0] = m1;
	targetSpeed[1] = m2;
	targetSpeed[2] = m3;
}
void OimiPid::setM1Speed(float m1){
	 targetSpeed[0] = m1;
}
void OimiPid::setM2Speed(float m2){
	targetSpeed[1] = m2;
}
void OimiPid::setM3Speed(float m3){
	targetSpeed[2] = m3;
}

//compute velocity in cm/s
void OimiPid::computeDirectKinematics(){
	speedX = 0.5*wheel_radius*(actualSpeed[0] - actualSpeed[2])/cos((float)PI/6.0f);
	speedY = wheel_radius*(actualSpeed[2] + actualSpeed[0]-2.0*actualSpeed[1])/3.0;
	speedTh= wheel_radius*(actualSpeed[2] + actualSpeed[0]+actualSpeed[1])/3.0/robot_radius;

}
//compute odometry
void OimiPid::estimateOdometry(unsigned long int deltaT){
	float delta_x = (speedX * cos(posTh) - speedY * sin(posTh)) * deltaT/1000.0;
	float delta_y = (speedX * sin(posTh) + speedY * cos(posTh)) * deltaT/1000.0;
	float delta_th = speedTh * deltaT/1000.0;

	posX+=delta_x;
	posY+=delta_y;
	posTh+=delta_th;

}
// compute final PWM value
int OimiPid::updatePid(float targetValue, float currentValue, int i)   {
  float pidTerm =0; // PID correction
  float error=0; // error definition
  error = targetValue - currentValue; //set error to difference between targetSpeed - actualSpeed
  InterTerm[i] += error*Ki; //set Integrative Terms to error * coefficient Ki..
  //..this property gives to PI Control the ability to bring the process exactly to the required target speed when the proportional action would be null.
  if(InterTerm[i]>255)
  	InterTerm[i]=255;
  else if(InterTerm[i]<-255)
  	InterTerm[i]=-255;
  float deltaError = error - previousError[i];//integral
  pidTerm = (Kp * error) + (InterTerm[i]) + (Kd * deltaError); //pwm-values calculation (output of PID)
  previousError[i] = error; //save last error
  int output=constrain(int(pidTerm), -255, 255); //Constraining output val to be within a range if its ok return pidTerm
  return output;
}

// calculate speed, volts and Amps
void OimiPid::calcMotorCm(long deltaT,int pos,int i)  {
static int countAnt[3] = {0,0,0}; // last count
actualSpeed[i] = ((pos - countAnt[i])*(2.0f*(float)PI*wheel_radius)*1000.0f)/(deltaT*1920);// 16 pulses X 29 gear ratio = 464 counts per output shaft rev
 countAnt[i] = pos;
}

// calculate speed, volts and Amps
void OimiPid::calcMotorRad(long deltaT,int pos,int i)  {
static int countAnt[3] = {0,0,0}; // last count
 actualSpeed[i] =(float) ((pos - countAnt[i])*(1000L/deltaT)*2.0f*(float)PI)/(1920.0f);// 16 pulses X 29 gear ratio = 464 counts per output shaft rev
 countAnt[i] = pos;
}

// calculate speed, volts and Amps
void OimiPid::calcMotorRPM(long deltaT,int pos,int i)  {
static int countAnt[3] = {0,0,0}; // last count
 actualSpeed[i] = ((pos - countAnt[i])*(60L*(1000L/deltaT)))/(1920);// 16 pulses X 29 gear ratio = 464 counts per output shaft rev
 countAnt[i] = pos;
}

bool OimiPid::isStopped(){
	if(targetSpeed[0]==0 && targetSpeed[1]==0 && targetSpeed[2]==0)
		return true;
	else return false;
}
bool OimiPid::isRotating(){
	if(targetSpeed[0]==targetSpeed[1] && targetSpeed[1]==targetSpeed[2])
		return true;
	else return false;
}
bool OimiPid::isTraslating(){
	if(targetSpeed[0]==-targetSpeed[2])
		return true;
	else return false;
}

float OimiPid::getPosX(){
	return posX;
}
float OimiPid::getPosY(){
	return posY;
}
float OimiPid::getPosTh(){
	return posTh;
}

float OimiPid::getSpeedX(){
	return speedX;
}
float OimiPid::getSpeedY(){
	return speedY;
}
float OimiPid::getSpeedTh(){
	return speedTh;
}

float OimiPid::getActualSpeed1(){
   return actualSpeed[0];
}
float OimiPid::getActualSpeed2(){
   return actualSpeed[1];
}
float OimiPid::getActualSpeed3(){
   return actualSpeed[2];
}

int OimiPid::getPWM0(){
	return 	pwmAmount[0];
}
int OimiPid::getPWM1(){
	return 	pwmAmount[1];
}
int OimiPid::getPWM2(){
	return 	pwmAmount[2];
}

void OimiPid::setPosX(float _posX){
	posX=_posX;
}
void OimiPid::setPosY(float _posY){
	posY=_posY;
}
void OimiPid::setPosTh(float _posTh){
	posTh=_posTh;
}

int OimiPid::getPosWheels(int i){
	int pos=0;
	switch(i){
		case '0':pos=_e1.read(); break;
		case '1':pos=_e2.read(); break;
		case '2':pos=_e3.read(); break;
 	}
 	return pos;
}
void OimiPid::setInterTerm(int i, float val){
	InterTerm[i]=val;
}

void OimiPid::setKp(float val){
	Kp=val;
}
float OimiPid::getKp(){
	return Kp;
}
void OimiPid::resetKp(){
	Kp=KP;
}

void OimiPid::setKi(float val){
	Ki=val;
}
float OimiPid::getKi(){
	return Ki;
}
void OimiPid::resetKi(){
	Ki=KI;
}
