/*
  OimiPid.h - Library for controlling the OimiPid robot.
  Created by Martino Migliavacca, August 2, 2013.
  Released into the public domain.
  Modification made by Matteo Lucchelli including
    - encoders reader
    - PID speed control
	- Odometry
  Modified by Giacomo Colombo for using it with Cytron Motor's Driver

  In order to use the library, put OimiPid.pidLoop() function in the loop of Arduino sketch, use nonHolonomicRun(forward,angular) 
  or holonomicRun(strafe,forward,angular) to move the robot using inverse kinematic. use runMotors(motor1, motor2, motor3) if you 
  want to define each motor's speed. stop() function will stop the robot resetting all speed values. stop2() also stops 
  the robot, but it doesn't reset InterTerm PID value.
  Have a look to the example .ino file for further clarification.
*/

#ifndef OimiPid_h
#define OimiPid_h

#include <Arduino.h>
#include "CytronDriver_C10.h"
#include "Encoder.h"

#define MOTORS 3
#define wheel_radius  3.5 //cm
#define robot_radius  16.0  //cm

class OimiPid
{
private:
	//definitions
	#define KP  0.7 //035  modifica
	#define KI  0.9 //0.8 modifica
	#define _MAX_DTH  16.0  // Maximum wheel angular speed [rad/s]
	#define _MAX_SP   200.0 // Maximum setpoint
	#define _SP_SCALE (_MAX_SP / _MAX_DTH)
	#define LOOPTIME 25 // PID loop time
	///////////////////////////////////////////////
	#define m1_R     (-1.0 / wheel_radius)
	#define mL_R     (-robot_radius / wheel_radius)
	#define C60_R    (0.500000000 / wheel_radius)   // cos(60°) / R
	#define C30_R    (0.866025400 / wheel_radius)   // cos(30°) / R
	///////////////////////////////////////////////

	//SETPOINT
	float targetSpeed[3]; // desired speed vector
	float actualSpeed[3]; //actual speeds vector
	int pwmAmount[3];// pwm-vals (output pid value) vector --> N.B DUTY CYCLE (25% = 64; 50% = 127; 75% = 191; 100% = 255)
	float previousError[3]; //errors vector
	float InterTerm[3]; //Integrative term vector
	float Kp; //proportional gain
	float Ki; //integrative gain
	float Kd; //derivative gain
	float posX;
	float posY;
	float posTh;
	float speedX;
	float speedY;
	float speedTh;

	unsigned long lastMilliLoop;
	unsigned long lastMillis[3];
	unsigned long deltaPid;

	CytronDriver_C10 & _m1;
	CytronDriver_C10 & _m2;
	CytronDriver_C10 & _m3;
	Encoder & _e1;
	Encoder & _e2;
	Encoder & _e3;

	void calcMotorRPM(long deltaT,int pos,int i); //calculate speed in round per minutes
	void calcMotorRad(long deltaT,int pos,int i); //calculate speed in radiant per seconds (actualSpeed)
	void calcMotorCm(long deltaT,int pos,int i); //calculate speed in cm per seconds (linearSpeed)

	int updatePid(float targetValue, float currentValue, int i);
	void computeDirectKinematics();
	void estimateOdometry(unsigned long int deltaT);

public:
	OimiPid(CytronDriver_C10 & m1, CytronDriver_C10 & m2, CytronDriver_C10 & m3,
			Encoder & e1, Encoder & e2, Encoder & e3);
	void nonHolonomicRun(float forward, float angular);
	void holonomicRun(float strafe, float forward, float angular);
	void runMotors(float m1, float m2, float m3);
	void setM1Speed(float m1);
	void setM2Speed(float m2);
	void setM3Speed(float m3);

	void PIDLoop();
	void PIDLoop2();

	float getPosX();
	float getPosY();
	float getPosTh();

	float getSpeedX();
	float getSpeedY();
	float getSpeedTh();

	int getPWM0();
	int getPWM1();
	int getPWM2();

    int getDirection();

	void setPosX(float _posX);
	void setPosY(float _posY);
	void setPosTh(float _posTh);

	void setInterTerm(int i, float val);
	void resetIntegTerm();

	void setKp(float val);
	void resetKp();
	float getKp();
	void setKi(float val);
	void resetKi();
	float getKi();

	void resetPoses();
	void simpleStop(void);
	void partialStop(void);
	void completeStop();

	bool isStopped();
    bool isRotating();
    bool isTraslating();

    float getActualSpeed1();
    float getActualSpeed2();
    float getActualSpeed3();

    int getPosWheels(int i);

};

#endif
