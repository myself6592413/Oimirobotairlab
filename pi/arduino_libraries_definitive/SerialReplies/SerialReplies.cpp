/*******************************************************************
* Oimi-Robot SerialReplies.cpp --> see SerialReplies.h for details
********************************************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include "SerialReplies.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructor */
SerialReplies::SerialReplies(){
	//empty
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Arduino control */
void SerialReplies::printReset(){
	Serial.print(F("<I'm reset"));
	Serial.println(F(">"));
}
void SerialReplies::domenica(){
	Serial.print(F("<backled"));
	Serial.println(F(">"));
}

void SerialReplies::alertSwitch(int modality){
	Serial.print(F("<Modality "));
	Serial.print(modality);
	Serial.print(F(" successfully switched"));
	Serial.println(F(">"));
}
void SerialReplies::alertForNano(int modality){
	Serial.print(F("<Modnano is "));
	Serial.print(modality);
	Serial.println(F(">"));
}

void SerialReplies::printBatteryStatus(float power){
	Serial.print(F("<Current voltage power is "));
	Serial.println(power);
	Serial.println(F(">"));
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Body Pressures */
void SerialReplies::echoPressures(float press0, float press1, float press2, float press3, float press4){
	Serial.print(F("<Pressures are "));
	//da toglire nel caso!!!
	Serial.print(press0);
	Serial.print(F(" "));
	Serial.print(press1);
	Serial.print(F(" "));
	Serial.print(press2);
	Serial.print(F(" "));
	Serial.print(press3);
	Serial.print(F(" "));
	Serial.print(press4);
	//Serial.print(" ");
	//Serial.print(" Time ");
	Serial.println(F(">"));
}
void SerialReplies::echoPressures(float press0, float press1, float press2, float press3, float press4, int press_index){
	Serial.print(F("<Pressures num "));
	Serial.print(press0);
	Serial.print(F(" "));
	Serial.print(press1);
	Serial.print(F(" "));
	Serial.print(press2);
	Serial.print(F(" "));
	Serial.print(press3);
	Serial.print(F(" "));
	Serial.print(press4);
	Serial.print(F(" "));
	Serial.print(press_index);
	//Serial.print(" ");
	//Serial.print(" Time ");
	Serial.println(F(">"));
}
void SerialReplies::respondePush(bool isPushed){
	if(isPushed==0){
		Serial.print(F("<No push, return "));
		Serial.print(" ");
		Serial.print(0);
		Serial.println(F(">"));
	}
	else if(isPushed==1){
		Serial.print(F("<Ok push, return "));
		Serial.print(" ");
		Serial.print(1);
		Serial.println(F(">"));
	}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Sonars */
void SerialReplies::publishSonars(int left, int front, int right, int back){
	Serial.print(F("<Sonar distances are "));
	Serial.print(left);
	Serial.print(F(" "));
	Serial.print(front);
	Serial.print(F(" "));
	Serial.print(right);
	Serial.print(F(" "));
	Serial.print(back);
	Serial.println(F(">"));
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Contacts reactions */
void SerialReplies::printHit(int zone){
/**/
	switch(zone){
		case 0:
			Serial.print(F("<hit_left_found"));
			Serial.println(F(">"));
		break;
		case 1:
			Serial.print(F("<hit_front_found"));
			Serial.println(F(">"));
		break;
		case 2:
			Serial.print(F("<hit_right_found"));
			Serial.println(F(">"));
		break;
		case 3:
			Serial.print(F("<hit_back_found"));
			Serial.println(F(">"));
		break;
	}
}
void SerialReplies::printBody(int state){
	switch(state){
		case 0:
		Serial.print(F("<quiet_found"));
		Serial.println(F(">"));
		break;
		case 1:
		Serial.print(F("<touch_found"));
		Serial.println(F(">"));
		break;
		case 2:
		Serial.print(F("<caress_found"));
		Serial.println(F(">"));
		break;
		case 3:
		Serial.print(F("<squeeze_found"));
		Serial.println(F(">"));
		break;
		case 4:
		Serial.print(F("<shove_found"));
		Serial.println(F(">"));
		break;
		case 5:
		Serial.print(F("<hug_found"));
		Serial.println(F(">"));
		break;
		case 6:
		Serial.print(F("<choke_found"));
		Serial.println(F(">"));
		break;
		case 7:
		Serial.print(F("<hit_found"));
		Serial.println(F(">"));
		break;
	}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Games reactions */
void SerialReplies::answerEx(int ans){
	switch(ans){
		case 0:
			Serial.print(F("<correct ans found"));
			Serial.println(F(">"));
		break;
		case 1:
			Serial.print(F("<wrong ans found"));
			Serial.println(F(">"));
		break;
	}
}
void SerialReplies::printReaction(int type){
	if(!type){
		Serial.print(F("<celebrate"));
		Serial.println(F(">"));
	}
	else{
		Serial.print(F("<console"));
		Serial.println(F(">"));
	}
}

void SerialReplies::printTimeElapsed(){
	Serial.print(F("<elapsed"));
	Serial.println(F(">"));
}
void SerialReplies::printBegin(){
	Serial.print(F("<startmatch"));
	Serial.println(F(">"));
}
void SerialReplies::printEmotion(int kind){
	switch(kind){
		case 0:
			Serial.print(F("<game_red"));
			Serial.println(F(">"));
		break;
		case 1:
			Serial.print(F("<game_green"));
			Serial.println(F(">"));
		break;
		case 2:
			Serial.print(F("<game_yellow"));
			Serial.println(F(">"));
		break;
		case 3:
			Serial.print(F("<game_blue"));
			Serial.println(F(">"));
		break;
	}
}
void SerialReplies::respondeAlreadyGiven(){
	Serial.print(F("<Answer already given!"));
	Serial.println(F(">"));
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Movements */
void SerialReplies::replySpeeds(float strafe, float forward, float angular){
	Serial.print(F("<Strafe Speed is "));
	Serial.print(strafe);
	Serial.print(F(" "));
	Serial.print(F("Forward Speed is "));
	Serial.print(forward);
	Serial.print(F(" "));
	Serial.print(F("Angular Speed is "));
	Serial.print(angular);
	Serial.println(F(">"));
}
void SerialReplies::replyCustomSpeeds(float distance, float forward, float angular, const char* color, const char* animation){
	Serial.print(F("<Distance is "));
	Serial.print(distance);
	Serial.print(F(" "));
	Serial.print(F("<Forward Speed is "));
	Serial.print(forward);
	Serial.print(F(" "));
	Serial.print(F("Angular Speed is "));
	Serial.print(angular);
	Serial.print(F(" "));
	Serial.print(F("color is "));
	Serial.print(color);
	Serial.print(F(" "));
	Serial.print(F("animation is "));
	Serial.print(animation);
	Serial.println(F(">"));
}

//no!!! meglio toglierlo quando si comunica con il raspberry fa solo casino 
//non lo chiamo!
void SerialReplies::publishMovement(char* movetype){
	if(strcmp(movetype, "auto")==0){
		Serial.print(F("< Autonomous movement enabled"));
		Serial.println(F(">"));
	}
	else if(strcmp(movetype, "follo")==0){
		Serial.print(F("<Following movement enabled"));
		Serial.println(F(">"));
	}
	else if(strcmp(movetype, "control")==0){
		Serial.print(F("<Control  movement enabled"));
		Serial.println(F(">"));
	}
	else if(strcmp(movetype, "pattern")==0){
		Serial.print(F("<simple single pattern movement enabled"));
		Serial.println(F(">"));
	}
	else if(strcmp(movetype, "predefinite")==0){
		Serial.print(F("<simple single predefinite movement enabled"));
		Serial.println(F(">"));
	}
	else if(strcmp(movetype, "closesonar")==0){
		Serial.print(F("<simple single closesonars movement enabled"));
		Serial.println(F(">"));
	}	
}
void SerialReplies::printStop(){
	Serial.print(F("<I'm stopped"));
	Serial.println(F(">"));
}
void SerialReplies::printPause(){
	Serial.print(F("<Sono in PAUSA"));
	Serial.println(F(">"));
	confermaPauseMandato = true;
}
bool SerialReplies::getMandato(){
	return confermaPauseMandato;
}