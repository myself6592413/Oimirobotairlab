/**********************************************************************
* Oimi Robot SerialReplies.h - Library for sending back data to raspi
*
* Created by Giacomo Colombo Airlab Politecnico di Milano 2020
**********************************************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Conditional compilation */
#ifndef SerialReplies_h
#define SerialReplies_h
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Class definition */
class SerialReplies{
	public:
		/*************************************************************************************************** Constructor */
		SerialReplies();
		/*************************************************************************************************** Methods */
		void echoPressures(float press0, float press1, float press2, float press3, float press4);
		void echoPressures(float press0, float press1, float press2, float press3, float press4, int press_index);
		void respondePush(bool isPushed);
		void publishSonars(int left, int front, int right, int back);
		void printHit(int zone);
		void printBody(int state);
		void answerEx(int result);
		void replySpeeds(float strafe, float forward, float angular);
		void replyCustomSpeeds(float distance, float forward, float angular, const char* color, const char* animation);
		void printReaction(int type);
		void printTimeElapsed();
		void printBegin();
		void printEmotion(int kind);
		void respondeAlreadyGiven();
		void publishMovement(char* movetype);
		void printStop();
		void printPause();
		void printReset();
		void alertSwitch(int modality);
		void alertForNano(int modality);
		void printBatteryStatus(float power);
		void domenica();

		bool getMandato();
		bool confermaPauseMandato;
};
#endif
