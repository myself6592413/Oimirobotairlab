/**
* Structs and Methods used by game_classes.pxd to create an object in realtime for each real question + answer + scenario and speed up the computation
*/
typedef struct {
	int subject; 
	int color;
	int genre;
} Patch;

typedef struct {
	int size;
	Patch *arr;
} PatchesArray;

typedef struct {
	int kind;
	int the_type;
} QuestionType;

typedef struct {
	int size;
	int number;
	int value;
	int required_time;
	QuestionType *types;
} Question;

typedef struct {
	int audio;
	int required_time;
	int answer1;
	int answer2;
	int answer3;
	int answer4;
} QeA;

typedef struct {
    int audio;
    int required_time;
    int *answers;
    int answer_count;
} QeA2;

typedef struct {
	int size;
	QeA *arr;
} QeAArray;

typedef struct {
	int size;
	QeA2 *arr;
} QeAArray2;

/**
* Methods
*/
Question makeQuestion(int size, int number, int value, int required_time, int*kinds, int*types){
	Question quest;
	quest.size=size;
	quest.number=number;
	quest.value=value;
	quest.required_time=required_time;
	quest.types=(QuestionType *)malloc(size*sizeof(QuestionType));
	for(int j=0; j < size; j++){
		quest.types[j].kind=kinds[j];
		quest.types[j].the_type=types[j];
	}
	return quest;
}
QeAArray makeQeAArray(int size, int*audios, int* required_times, int*answers1, int*answers2, int*answers3, int*answers4){
	QeAArray qu;
	qu.size=size;
	qu.arr=(QeA *)malloc(size*sizeof(QeA));

	for(int i=0; i < size; i++){
		qu.arr[i].audio = audios[i];
		qu.arr[i].required_time = required_times[i];
		qu.arr[i].answer1 = answers1[i];
		qu.arr[i].answer2 = answers2[i];
		qu.arr[i].answer3 = answers3[i];
		qu.arr[i].answer4 = answers4[i];
	}
	return qu;
}
QeAArray2 makeQeAArray2(int size, int*audios, int* required_times, int*answers, int *answer_counts) {
    QeAArray qu;
    qu.size=size;
    qu.arr=(QeA *)malloc(size*sizeof(QeA));

    for(int i=0; i < size; i++){
        qu.arr[i].audio = audios[i];
        qu.arr[i].required_time = required_times[i];
        qu.arr[i].answer_count = answer_counts[i];
        qu.arr[i].answers = (int *)malloc(answer_counts[i]*sizeof(int));
        for(int j=0; j < answer_counts[i]; j++){
            qu.arr[i].answers[j] = answers[i][j];
        }
    }
    return qu;
}
PatchesArray makePatchesArray(int size, int* subjects, int* colors, int* genres){
	PatchesArray res;
	res.size=size;
	res.arr=(Patch *)malloc(size*sizeof(Patch));
	for(int j=0; j < size; j++){
		res.arr[j].subject=subjects[j];
		res.arr[j].color=colors[j];
		res.arr[j].genre=genres[j];
	}
	return res;
}