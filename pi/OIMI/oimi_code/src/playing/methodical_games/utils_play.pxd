""" 
	Methods for game.pyx
	In this case there is not the corresponded pyx file, these declarations serve as a bridge between the C/C++ code and the Python code written in Cython.
Details: 
	inline function are inserted directly into the generated C code instead of being called as a separate function to improve performance 
	by reducing the overhead of function calls.
"""
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import random
from enums_play cimport PatchCategory
from enums_play cimport GameCategory
from enums_play cimport Subject
from enums_play cimport Color
from enums_play cimport MatchDifficulty
from enums_play cimport GameDifficulty
from enums_play cimport SessionDifficulty
from enums_play cimport Feedback
from enums_play cimport QuestionKind
from enums_play cimport QuestionType
from enums_play cimport KidLevel
# ===================================== # ===================================== #
# Random selections:
# ===================================== # ===================================== #
cdef inline numgames_for_easy():
	cdef int howmany = random.randint(2,3)
	return howmany
cdef inline numgames_for_normal():
	cdef int howmany = random.randint(3,4)
	return howmany
cdef inline numgames_for_medium():
	cdef int howmany = random.randint(3,4)
	return howmany
cdef inline numgames_for_hard():
	cdef int howmany = random.randint(4,5)
	return howmany
cdef inline numquestions_for_easy():
	cdef int howmany = random.randint(2,3)
	return howmany
cdef inline numquestions_for_normal():
	cdef int howmany = random.randint(3,4)
	return howmany
cdef inline numquestions_for_medium():
	cdef int howmany = random.randint(4,5)
	return howmany
cdef inline numquestions_for_hard():
	cdef int howmany = random.randint(5,6)
	return howmany
cdef inline nummatches_for_easy():
	cdef int howmany = random.randint(2,3)
	return howmany
cdef inline nummatches_for_normal():
	cdef int howmany = random.randint(3,4)
	return howmany
cdef inline nummatches_for_medium():
	cdef int howmany = random.randint(4,5)
	return howmany
cdef inline nummatches_for_hard():
	cdef int howmany = random.randint(5,6)
	return howmany
# ===================================== # ===================================== #
# Transform in enum:
# ===================================== # ===================================== #
cdef inline QuestionKind transform_question_kind(int x):
	return {
		0: QuestionKind.k1F,
		1: QuestionKind.k2L,
		2: QuestionKind.k3S,
		3: QuestionKind.k4P,
		4: QuestionKind.k5K,
		5: QuestionKind.k6I,
		6: QuestionKind.k7O,
		7: QuestionKind.k8Q,
		8: QuestionKind.k9C
	}[x]

cdef inline QuestionType transform_question_type(int x):
	return {
		0:QuestionType.t1FSUS,
		1:QuestionType.t1FSUD,
		2:QuestionType.t1FSUDT,
		3:QuestionType.t1FCOS,
		4:QuestionType.t1FCOD,
		5:QuestionType.t1FCODT,
		6:QuestionType.t1FSUCOS,
		7:QuestionType.t1FSUCOD,
		8:QuestionType.t1FSUCODT,
		9:QuestionType.t1FGCA,
		10:QuestionType.t1FCACO,
		11:QuestionType.t1FCACOA,
		12:QuestionType.t2LST,
		13:QuestionType.t2LSU,
		14:QuestionType.t2LTWO,
		15:QuestionType.t2LDSU,
		16:QuestionType.t2LDSUOC,
		17:QuestionType.t2LDBOC,
		18:QuestionType.t2LDSUOB,
		19:QuestionType.t2LDBOB,
		20:QuestionType.t2LDSU2,
		21:QuestionType.t3SSU,
		22:QuestionType.t3SCO,
		23:QuestionType.t4PSU,
		24:QuestionType.t4PCO,
		25:QuestionType.t5KS,
		26:QuestionType.t5KD,
		27:QuestionType.t6I,
		28:QuestionType.t7OSU2,
		29:QuestionType.t7OSU3,
		30:QuestionType.t7OSU4,
		31:QuestionType.t7OCO2,
		32:QuestionType.t7OCO3,
		33:QuestionType.t7OCO4,
		34:QuestionType.t7OL2,
		35:QuestionType.t7OL3,
		36:QuestionType.t8QTSUSC1,
		37:QuestionType.t8QTSUSC2,
		38:QuestionType.t8QTCOSC1,
		39:QuestionType.t8QTCOSC2,
		40:QuestionType.t8QTKNSC,
		41:QuestionType.t8QTKNDC,
		42:QuestionType.t8QTPRSC,
		43:QuestionType.t8QTSUDC1,
		44:QuestionType.t8QTSUDC2,
		45:QuestionType.t8QTCODC1,
		46:QuestionType.t8QTCODC2,
		47:QuestionType.t8QTSUSB1,
		48:QuestionType.t8QTSUSB2,
		49:QuestionType.t8QTCOSB1,
		50:QuestionType.t8QTCOSB2,
		51:QuestionType.t8QTKNSB,
		52:QuestionType.t8QTPRSB,
		53:QuestionType.t8QTSUDB1,
		54:QuestionType.t8QTSUDB2,
		55:QuestionType.t8QTCODB1,
		56:QuestionType.t8QTCODB2,
		57:QuestionType.t8QTSUSCOC1,
		58:QuestionType.t9CNSU,
		59:QuestionType.t9CNCO,
		60:QuestionType.t9CNSSU,
		61:QuestionType.t9CNSPA,
		62:QuestionType.t9CNSPX,
		63:QuestionType.tMISCELLANEOUS
	}[x]

cdef inline SessionDifficulty transform_difficulty_session(int x):
	return {
		0: SessionDifficulty.easy_s,
		1: SessionDifficulty.medium_s,
		2: SessionDifficulty.normal_s,
		3: SessionDifficulty.hard_s
	}[x]

cdef inline MatchDifficulty transform_difficulty_match(int x):
	return {
		0: MatchDifficulty.easy_m_1,
		1: MatchDifficulty.easy_m_2,
		2: MatchDifficulty.medium_m_1,
		3: MatchDifficulty.medium_m_2,
		4: MatchDifficulty.normal_m_1,
		5: MatchDifficulty.normal_m_2,
		6: MatchDifficulty.hard_m_1,
		7: MatchDifficulty.hard_m_2
	}[x]

cdef inline GameDifficulty transform_difficulty_game(int x):
	return {
		0: GameDifficulty.easy_g_1,
		1: GameDifficulty.easy_g_2,
		2: GameDifficulty.medium_g_1,
		3: GameDifficulty.medium_g_2,
		4: GameDifficulty.normal_g_1,
		5: GameDifficulty.normal_g_2,
		6: GameDifficulty.hard_g_1,
		7: GameDifficulty.hard_g_2
	}[x]
# ===================================== # ===================================== #
# Conversions:
# ===================================== # ===================================== #
cdef inline str convert_difficulty_session(int x):
	return {
		0: 'easy_s',
		1: 'normal_s',
		2: 'medium_s',
		3: 'hard_s'
	}[x]

cdef inline str convert_difficulty_match(int x):
	return {
		0: 'easy_m_1',
		1: 'easy_m_1',
		2: 'normal_m_1',
		3: 'normal_m_2',
		4: 'medium_m_1',
		5: 'medium_m_2',
		6: 'hard_m_1',
		7: 'hard_m_2'
	}[x]

cdef inline str convert_difficulty_game(int x):
	return {
		0: 'easy_g_1',
		1: 'easy_g_2',
		2: 'normal_g_1',
		3: 'normal_g_2',
		4: 'medium_g_1',
		5: 'medium_g_2',
		6: 'hard_g_1',
		7: 'hard_g_2'
	}[x]

cdef inline str convert_kind_rifare(int x):
	return {
		0 : 'k1F',
		1 : 'k2L',
		2 : 'k3S',
		3 : 'k4P',
		4 : 'k5K',
		5 : 'k6I',
		6 : 'k7O',
		7 : 'k8Q',
		8 : 'k9C'
	}[x]

cdef inline str convert_kind(int x):
	return {
		0 : '1F',
		1 : '2L',
		2 : '3S',
		3 : '4P',
		4 : '5K',
		5 : '6I',
		6 : '7O',
		7 : '8Q',
		8 : '9C'
	}[x]

cdef inline str convert_type_rifare(int x):
	return {
		0 : 't1FSUS',
		1 : 't1FSUD',
		2 : 't1FSUDT',
		3 : 't1FCOS',
		4 : 't1FCOD',
		5 : 't1FCODT',
		6 : 't1FSUCOS',
		7 : 't1FSUCOD',
		8 : 't1FSUCODT',
		9 : 't1FGCA',
		10 : 't1FCACO',
		11 : 't1FCACOA',
		12 : 't2LST',
		13 : 't2LSU',
		14 : 't2LTWO',
		15 : 't2LDSU',
		16 : 't2LDSUOC',
		17 : 't2LDBOC',
		18 : 't2LDSUOB',
		19 : 't2LDBOB',
		20 : 't2LDSU2',
		21 : 't3SSU',
		22 : 't3SCO',
		23 : 't4PSU',
		24 : 't4PCO',
		25 : 't5KS',
		26 : 't5KD',
		27 : 't6I',
		28 : 't7OSU2',
		29 : 't7OSU3',
		30 : 't7OSU4',
		31 : 't7OCO2',
		32 : 't7OCO3',
		33 : 't7OCO4',
		34 : 't7OL2',
		35 : 't7OL3',
		36 : 't8QTSUSC1',
		37 : 't8QTSUSC2',
		38 : 't8QTCOSC1',
		39 : 't8QTCOSC2',
		40 : 't8QTKNSC',
		41 : 't8QTKNDC',
		42 : 't8QTPRSC',
		43 : 't8QTSUDC1',
		44 : 't8QTSUDC2',
		45 : 't8QTCODC1',
		46 : 't8QTCODC2',
		47 : 't8QTSUSB1',
		48 : 't8QTSUSB2',
		49 : 't8QTCOSB1',
		50 : 't8QTCOSB2',
		51 : 't8QTKNSB',
		52 : 't8QTPRSB',
		53 : 't8QTSUDB1',
		54 : 't8QTSUDB2',
		55 : 't8QTCODB1',
		56 : 't8QTCODB2',
		57 : 't8QTSUSCOC1',
		58 : 't9CNSU',
		59 : 't9CNCO',
		60 : 't9CNSSU',
		61 : 't9CNSPA',
		62 : 't9CNSPX'
	}[x]

cdef inline str convert_type(int x):
	return {
		0 : '1FSUS',
		1 : '1FSUD',
		2 : '1FSUDT',
		3 : '1FCOS',
		4 : '1FCOD',
		5 : '1FCODT',
		6 : '1FSUCOS',
		7 : '1FSUCOD',
		8 : '1FSUCODT',
		9 : '1FGCA',
		10 : '1FCACO',
		11 : '1FCACOA',
		12 : '2LST',
		13 : '2LSU',
		14 : '2LTWO',
		15 : '2LDSU',
		16 : '2LDSUOC',
		17 : '2LDBOC',
		18 : '2LDSUOB',
		19 : '2LDBOB',
		20 : '2LDSU2',
		21 : '3SSU',
		22 : '3SCO',
		23 : '4PSU',
		24 : '4PCO',
		25 : '5KS',
		26 : '5KD',
		27 : '6I',
		28 : '7OSU2',
		29 : '7OSU3',
		30 : '7OSU4',
		31 : '7OCO2',
		32 : '7OCO3',
		33 : '7OCO4',
		34 : '7OL2',
		35 : '7OL3',
		36 : '8QTSUSC1',
		37 : '8QTSUSC2',
		38 : '8QTCOSC1',
		39 : '8QTCOSC2',
		40 : '8QTKNSC',
		41 : '8QTKNDC',
		42 : '8QTPRSC',
		43 : '8QTSUDC1',
		44 : '8QTSUDC2',
		45 : '8QTCODC1',
		46 : '8QTCODC2',
		47 : '8QTSUSB1',
		48 : '8QTSUSB2',
		49 : '8QTCOSB1',
		50 : '8QTCOSB2',
		51 : '8QTKNSB',
		52 : '8QTPRSB',
		53 : '8QTSUDB1',
		54 : '8QTSUDB2',
		55 : '8QTCODB1',
		56 : '8QTCODB2',
		57 : '8QTSUSCOC1',
		58 : '9CNSU',
		59 : '9CNCO',
		60 : '9CNSSU',
		61 : '9CNSPA',
		62 : '9CNSPX',
		63 : 'MISCELLANEOUS'
	}[x]
		
cdef inline str convert_subject(int x):
	return {
		0: 'plain',
		1: 'lion',
		2: 'monkey',
		3: 'parrot',
		4: 'octopus',
		5: 'girl',
		6: 'glasses',
		7: 'umbrella',
		8: 'bicycle',
		9: 'push_scooter',
		10: 'icecream'
	}[x]

cdef inline str convert_patch_category(int x):
	return {
		0: 'solidColor',
		2: 'animal',
		3: 'food',
		4: 'dress',
		5: 'thing',
		6: 'human'
	}[x]

cdef inline str convert_color(int x):
	return {
		0: 'Red',
		1: 'Blue',
		2: 'Yellow',
		3: 'Green'
	}[x]
cdef inline str convert_genre(int x):
	return {
		0: 'Solid_color',
		1: 'Animal',
		2: 'People',
		3: 'Dress',
		4: 'Thing',
		5: 'Food'
	}[x]
# ===================================== # ===================================== #
# Choose output int enum:
# ===================================== # ===================================== #
cdef inline int choose_num_game(int x):
	options= {
		0: numgames_for_easy,
		1: numgames_for_normal,
		2: numgames_for_medium,
		3: numgames_for_hard
	}
	return options[x]()

cdef inline int choose_kindof_game(int x):
	return {
		0: QuestionKind.k1F,
		1: QuestionKind.k2L,
		2: QuestionKind.k3S,
		3: QuestionKind.k4P,
		4: QuestionKind.k5K,
		5: QuestionKind.k6I,
		6: QuestionKind.k7O,
		7: QuestionKind.k8Q,
		8: QuestionKind.k9C
	}[x]

cdef inline int choose_typeof_game(int x):
	return {
		0 : QuestionType.t1FSUS,
		1 : QuestionType.t1FSUD,
		2 : QuestionType.t1FSUDT,
		3 : QuestionType.t1FCOS,
		4 : QuestionType.t1FCOD,
		5 : QuestionType.t1FCODT,
		6 : QuestionType.t1FSUCOS,
		7 : QuestionType.t1FSUCOD,
		8 : QuestionType.t1FSUCODT,
		9 : QuestionType.t1FGCA,
		10: QuestionType.t1FCACO,
		11: QuestionType.t1FCACOA,
		12: QuestionType.t2LST,
		13: QuestionType.t2LSU,
		14: QuestionType.t2LTWO,
		15: QuestionType.t2LDSU,
		16: QuestionType.t2LDSUOC,
		17: QuestionType.t2LDBOC,
		18: QuestionType.t2LDSUOB,
		19: QuestionType.t2LDBOB,
		20: QuestionType.t2LDSU2,
		21: QuestionType.t3SSU,
		22: QuestionType.t3SCO,
		23: QuestionType.t4PSU,
		24: QuestionType.t4PCO,
		25: QuestionType.t5KS,
		26: QuestionType.t5KD,
		27: QuestionType.t6I,
		28: QuestionType.t7OSU2,
		29: QuestionType.t7OSU3,
		30: QuestionType.t7OSU4,
		31: QuestionType.t7OCO2,
		32: QuestionType.t7OCO3,
		33: QuestionType.t7OCO4,
		34: QuestionType.t7OL2,
		35: QuestionType.t7OL3,
		36: QuestionType.t8QTSUSC1,
		37: QuestionType.t8QTSUSC2,
		38: QuestionType.t8QTCOSC1,
		39: QuestionType.t8QTCOSC2,
		40: QuestionType.t8QTKNSC,
		41: QuestionType.t8QTKNDC,
		42: QuestionType.t8QTPRSC,
		43: QuestionType.t8QTSUDC1,
		44: QuestionType.t8QTSUDC2,
		45: QuestionType.t8QTCODC1,
		46: QuestionType.t8QTCODC2,
		47: QuestionType.t8QTSUSB1,
		48: QuestionType.t8QTSUSB2,
		49: QuestionType.t8QTCOSB1,
		50: QuestionType.t8QTCOSB2,
		51: QuestionType.t8QTKNSB,
		52: QuestionType.t8QTPRSB,
		53: QuestionType.t8QTSUDB1,
		54: QuestionType.t8QTSUDB2,
		55: QuestionType.t8QTCODB1,
		56: QuestionType.t8QTCODB2,
		57: QuestionType.t8QTSUSCOC1,
		58: QuestionType.t9CNSU,
		59: QuestionType.t9CNCO,
		60: QuestionType.t9CNSSU,
		61: QuestionType.t9CNSPA,
		62: QuestionType.t9CNSPX
	}[x]

cdef inline int choose_audiokind(int x):
	return {
		0: PatchCategory.solidcolor,
		1: PatchCategory.animal,
		2: PatchCategory.food,
		3: PatchCategory.dress,
		4: PatchCategory.thing,
		5: PatchCategory.human
	}[x]
# ===================================== # ===================================== #
# From string to int:
# ===================================== # ===================================== #
cdef inline int opposite_conversion(strg):
	if strg=='Solid_color':
		converted = 0
	elif strg=='Animal':
		converted = 1
	elif strg=='People':
		converted = 2
	elif strg=='Dress':
		converted = 3
	elif strg=='Thing':
		converted = 4
	elif strg=='Food':
		converted = 5

	elif strg=='Plain':
		converted = 0
	elif strg=='Lion':
		converted = 1
	elif strg=='Monkey':
		converted = 2
	elif strg=='Parrot':
		converted = 3
	elif strg=='Octopus':
		converted = 4
	elif strg=='Girl':
		converted = 5
	elif strg=='Glasses':
		converted = 6
	elif strg=='Umbrella':
		converted = 7				 
	elif strg=='Bicycle':
		converted = 8
	elif strg=='Push_scooter':
		converted = 9		 
	elif strg=='Icecream':
		converted = 10

	elif strg=='Red':
		converted = 0
	elif strg=='Blue':
		converted = 1
	elif strg=='Yellow':
		converted = 2		 
	elif strg=='Green':
		converted = 3

	elif strg=='1F':
		converted = 0
	elif strg=='2L':
		converted = 1
	elif strg=='3S':
		converted = 2
	elif strg=='4P':
		converted = 3
	elif strg=='5K':
		converted = 4
	elif strg=='6I':
		converted = 5
	elif strg=='7O':
		converted = 6
	elif strg=='8Q':
		converted = 7
	elif strg=='9C':
		converted = 8

	elif strg=='1FSUS':
		converted = 0
	elif strg=='1FSUD':
		converted = 1
	elif strg=='1FSUDT':
		converted = 2
	elif strg=='1FCOS':
		converted = 3
	elif strg=='1FCOD':
		converted = 4
	elif strg=='1FCODT':
		converted = 5
	elif strg=='1FSUCOS':
		converted = 6
	elif strg=='1FSUCOD':
		converted = 7
	elif strg=='1FSUCODT':
		converted = 8
	elif strg=='1FGCA':
		converted = 9
	elif strg=='1FCACO':
		converted = 10
	elif strg=='1FCACOA':
		converted = 11
	elif strg=='2LST':
		converted = 12
	elif strg=='2LSU':
		converted = 13
	elif strg=='2LTWO':
		converted = 14
	elif strg=='2LDSU':
		converted = 15
	elif strg=='2LDSUOC':
		converted = 16
	elif strg=='2LDBOC':
		converted = 17
	elif strg=='2LDSUOB':
		converted = 18
	elif strg=='2LDBOB':
		converted = 19
	elif strg=='2LDSU2':
		converted = 20
	elif strg=='3SSU':
		converted = 21
	elif strg=='3SCO':
		converted = 22
	elif strg=='4PSU':
		converted = 23
	elif strg=='4PCO':
		converted = 24
	elif strg=='5KS':
		converted = 25
	elif strg=='5KD':
		converted = 26
	elif strg=='6I':
		converted = 27
	elif strg=='7OSU2':
		converted = 28
	elif strg=='7OSU3':
		converted = 29
	elif strg=='7OSU4':
		converted = 30
	elif strg=='7OCO2':
		converted = 31
	elif strg=='7OCO3':
		converted = 32
	elif strg=='7OCO4':
		converted = 33
	elif strg=='7OL2':
		converted = 34
	elif strg=='7OL3':
		converted = 35
	elif strg=='8QTSUSC1':
		converted = 36
	elif strg=='8QTSUSC2':
		converted = 37
	elif strg=='8QTCOSC1':
		converted = 38
	elif strg=='8QTCOSC2':
		converted = 39
	elif strg=='8QTKNSC':
		converted = 40
	elif strg=='8QTKNDC':
		converted = 41
	elif strg=='8QTPRSC':
		converted = 42
	elif strg=='8QTSUDC1':
		converted = 43
	elif strg=='8QTSUDC2':
		converted = 44
	elif strg=='8QTCODC1':
		converted = 45
	elif strg=='8QTCODC2':
		converted = 46
	elif strg=='8QTSUSB1':
		converted = 47
	elif strg=='8QTSUSB2':
		converted = 48
	elif strg=='8QTCOSB1':
		converted = 49
	elif strg=='8QTCOSB2':
		converted = 50
	elif strg=='8QTKNSB':
		converted = 51
	elif strg=='8QTPRSB':
		converted = 52
	elif strg=='8QTSUDB1':
		converted = 53
	elif strg=='8QTSUDB2':
		converted = 54
	elif strg=='8QTCODB1':
		converted = 55
	elif strg=='8QTCODB2':
		converted = 56
	elif strg=='8QTSUSCOC1':
		converted = 57
	elif strg=='9CNSU':
		converted = 58
	elif strg=='9CNCO':
		converted = 59
	elif strg=='9CNSSU':
		converted = 60
	elif strg=='9CNSPA':
		converted = 61
	elif strg=='9CNSPX':
		converted = 62		
	elif strg=='MISCELLANEOUS':
		converted = 63

	elif strg=='Easy':
		converted = 0
	elif strg=='Normal':
		converted = 1
	elif strg=='Medium':
		converted = 2
	elif strg=='Hard':
		converted = 3

	elif strg=='Easy_1':
		converted = 0
	elif strg=='Easy_2':
		converted = 1
	elif strg=='Normal_1':
		converted = 2
	elif strg=='Normal_2':
		converted = 3
	elif strg=='Medium_1':
		converted = 4
	elif strg=='Medium_2':
		converted = 5
	elif strg=='Hard_1':
		converted = 6
	elif strg=='Hard_2':
		converted = 7

	return converted

# ===================================== # ===================================== #
# Simple:
# ===================================== # ===================================== #
cdef inline float cmax(float[:] a):
	cdef int c 
	cdef float maxi
	cdef Py_ssize_t la = len(a)
	maxi = a[0]
	with nogil:
		for c in range(1,la):
			if (a[c] > maxi):
				maxi = a[c]
	return maxi
cdef inline float cavg(float[:] a):
	#cdef float [:] x = a
	cdef float sum = 0
	cdef int N = len(a)
	with nogil:
		for i in range(N):
			sum += a[i]
	return sum/N
# ===================================== # ===================================== #
# Audio patches:
# ===================================== # ===================================== #
cdef inline str select_audio(with_position, sub, col):
	cdef:
		str audio_def
		str red_plain_ask = 'red_plain'
		str red_lion_ask = 'red_lion'
		str red_parrot_ask = 'red_parrot'
		str red_monkey_ask = 'red_monkey'
		str red_octopus_ask = 'red_octopus'
		str red_girl_ask = 'red_girl'
		str red_glasses_ask = 'red_glasses'
		str red_umbrella_ask = 'red_umbrella'
		str red_bicycle_ask = 'red_bicycle'
		str red_pushscooter_ask = 'red_pushscooter'
		str red_icecream_ask = 'red_icecream'
		str blue_plain_ask = 'blue_plain'
		str blue_lion_ask = 'blue_lion'
		str blue_parrot_ask = 'blue_parrot'
		str blue_monkey_ask = 'blue_monkey'
		str blue_octopus_ask = 'blue_octopus'
		str blue_girl_ask = 'blue_girl'
		str blue_glasses_ask = 'blue_glasses'
		str blue_umbrella_ask = 'blue_umbrella'
		str blue_bicycle_ask = 'blue_bicycle'
		str blue_pushscooter_ask = 'blue_pushscooter'
		str blue_icecream_ask = 'blue_icecream'
		str green_plain_ask = 'green_plain'
		str green_lion_ask = 'green_lion'
		str green_parrot_ask = 'green_parrot'
		str green_monkey_ask = 'green_monkey'
		str green_octopus_ask = 'green_octopus'
		str green_girl_ask = 'green_girl'
		str green_glasses_ask = 'green_glasses'
		str green_umbrella_ask = 'green_umbrella'
		str green_bicycle_ask = 'green_bicycle'
		str green_pushscooter_ask = 'green_pushscooter'
		str green_icecream_ask = 'green_icecream'		
		str yellow_plain_ask = 'yellow_plain'
		str yellow_lion_ask = 'yellow_lion'
		str yellow_parrot_ask = 'yellow_parrot'
		str yellow_monkey_ask = 'yellow_monkey'
		str yellow_octopus_ask = 'yellow_octopus'
		str yellow_girl_ask = 'yellow_girl'
		str yellow_glasses_ask = 'yellow_glasses'
		str yellow_umbrella_ask = 'yellow_umbrella'
		str yellow_bicycle_ask = 'yellow_bicycle'
		str yellow_pushscooter_ask = 'yellow_pushscooter'
		str yellow_icecream_ask = 'yellow_icecream'

	#red plain
	if(sub==0 and col==0):
		audio_def = red_plain_ask
	#red lion 
	if(sub==1 and col==0):
		audio_def = red_lion_ask
	#red monkey 
	if(sub==2 and col==0):
		audio_def = red_monkey_ask
	#red parrot 
	if(sub==3 and col==0):
		audio_def = red_parrot_ask
	#red octopus 
	if(sub==4 and col==0):
		audio_def = red_octopus_ask
	#red girl 
	if(sub==5 and col==0):
		audio_def = red_girl_ask
	#red glasses
	if(sub==6 and col==0):
		audio_def = red_glasses_ask
	#red umbrella 
	if(sub==7 and col==0):
		audio_def = red_umbrella_ask
	#red bicycle 
	if(sub==8 and col==0):
		audio_def = red_bicycle_ask
	#red pushscooter 
	if(sub==9 and col==0):
		audio_def = red_pushscooter_ask
	#red icecream
	if(sub==10 and col==0):
		audio_def = red_icecream_ask
	#blue plain
	if(sub==0 and col==1):
		audio_def = blue_plain_ask
	#blue lion 
	if(sub==1 and col==1):
		audio_def = blue_lion_ask
	#blue monkey 
	if(sub==2 and col==1):
		audio_def = blue_monkey_ask
	#blue parrot 
	if(sub==3 and col==1):
		audio_def = blue_parrot_ask
	#blue octopus 
	if(sub==4 and col==1):
		audio_def = blue_octopus_ask
	#blue girl 
	if(sub==5 and col==1):
		audio_def = blue_girl_ask
	#blue glasses
	if(sub==6 and col==1):
		audio_def = blue_glasses_ask
	#blue umbrella 
	if(sub==7 and col==1):
		audio_def = blue_umbrella_ask
	#blue bicycle 
	if(sub==8 and col==1):
		audio_def = blue_bicycle_ask
	#blue pushscooter 
	if(sub==9 and col==1):
		audio_def = blue_pushscooter_ask
	#blue icecream
	if(sub==10 and col==1):
		audio_def = blue_icecream_ask
	#green plain
	if(sub==0 and col==2):
		audio_def = green_plain_ask
	#green lion 
	if(sub==1 and col==2):
		audio_def = green_lion_ask
	#green monkey 
	if(sub==2 and col==2):
		audio_def = green_monkey_ask
	#green parrot 
	if(sub==3 and col==2):
		audio_def = green_parrot_ask
	#green octopus 
	if(sub==4 and col==2):
		audio_def = green_octopus_ask
	#green girl 
	if(sub==5 and col==2):
		audio_def = green_girl_ask
	#green glasses
	if(sub==6 and col==2):
		audio_def = green_glasses_ask
	#green umbrella 
	if(sub==7 and col==2):
		audio_def = green_umbrella_ask
	#green bicycle 
	if(sub==8 and col==2):
		audio_def = green_bicycle_ask
	#green pushscooter 
	if(sub==9 and col==2):
		audio_def = green_pushscooter_ask
	#green icecream
	if(sub==10 and col==2):
		audio_def = green_icecream_ask
	#yellow plain
	if(sub==0 and col==3):
		audio_def = yellow_plain_ask
	#yellow lion 
	if(sub==1 and col==3):
		audio_def = yellow_lion_ask
	#yellow monkey 
	if(sub==2 and col==3):
		audio_def = yellow_monkey_ask
	#yellow parrot 
	if(sub==3 and col==3):
		audio_def = yellow_parrot_ask
	#yellow octopus 
	if(sub==4 and col==3):
		audio_def = yellow_octopus_ask
	#yellow girl 
	if(sub==5 and col==3):
		audio_def = yellow_girl_ask
	#yellow glasses
	if(sub==6 and col==3):
		audio_def = yellow_glasses_ask
	#yellow umbrella 
	if(sub==7 and col==3):
		audio_def = yellow_umbrella_ask
	#yellow bicycle 
	if(sub==8 and col==3):
		audio_def = yellow_bicycle_ask
	#yellow pushscooter 
	if(sub==9 and col==3):
		audio_def = yellow_pushscooter_ask
	#yellow icecream
	if(sub==10 and col==3):
		audio_def = yellow_icecream_ask

	return audio_def

cdef inline select_audio_ask_patch_check_position(sub, col, position, just_choosed, israndomico):
	print("entro nella select_audio_ask_patch_check_position!!")
	if israndomico:
		position = 4 
	cdef int audio_ask_patch
	cdef int right_res 
	if(sub==0 and col==0):
		audio_ask_patch = 248
		right_res = 1
	#red monkey
	elif(sub==1 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 154
			right_res = 0
		if position==1:
			audio_ask_patch = 152
			right_res = 1
		if position==2:
			audio_ask_patch = 153
			right_res = 2
		if position==3:
			audio_ask_patch = 155
			right_res = 3
	#red octopus
	elif(sub==2 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 170
			right_res = 0
		if position==1:
			audio_ask_patch = 168
			right_res = 1
		if position==2:
			audio_ask_patch = 169
			right_res = 2
		if position==3:
			audio_ask_patch = 171
			right_res = 3
	#red parrot
	elif(sub==3 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)			

		if position==0:
			audio_ask_patch = 186
			right_res = 0
		if position==1:
			audio_ask_patch = 184
			right_res = 1
		if position==2:
			audio_ask_patch = 185
			right_res = 2
		if position==3:
			audio_ask_patch = 187
			right_res = 3
	#red lion
	elif(sub==4 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)		

		if position==0:
			audio_ask_patch = 202
			right_res = 0
		if position==1:
			audio_ask_patch = 200
			right_res = 1
		if position==2:
			audio_ask_patch = 201
			right_res = 2
		if position==3:
			audio_ask_patch = 203
			right_res = 3

	elif(sub==5 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	#red_pushscooter
	elif(sub==10 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 218
			right_res = 0
		if position==1:
			audio_ask_patch = 216
			right_res = 1
		if position==2:
			audio_ask_patch = 217
			right_res = 2
		if position==3:
			audio_ask_patch = 219
			right_res = 3	
	
	elif(sub==11 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	#red_bicycle
	elif(sub==12 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 234
			right_res = 0
		if position==1:
			audio_ask_patch = 232
			right_res = 1
		if position==2:
			audio_ask_patch = 233
			right_res = 2
		if position==3:
			audio_ask_patch = 235
			right_res = 3	

	elif(sub==13 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	
	elif(sub==0 and col==1):
		audio_ask_patch = 248
		right_res = 1
	#blue monkey	
	elif(sub==1 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 158
			right_res = 0
		if position==1:
			audio_ask_patch = 156
			right_res = 1
		if position==2:
			audio_ask_patch = 157
			right_res = 2
		if position==3:
			audio_ask_patch = 159
			right_res = 3
	#blue octopus		
	elif(sub==2 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 174
			right_res = 0
		if position==1:
			audio_ask_patch = 172
			right_res = 1
		if position==2:
			audio_ask_patch = 173
			right_res = 2
		if position==3:
			audio_ask_patch = 175
			right_res = 3
	#blue parrot		
	elif(sub==3 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 190
			right_res = 0
		if position==1:
			audio_ask_patch = 188
			right_res = 1
		if position==2:
			audio_ask_patch = 189
			right_res = 2
		if position==3:
			audio_ask_patch = 191
			right_res = 3
	#blue lion		
	elif(sub==4 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 206
			right_res = 0
		if position==1:
			audio_ask_patch = 204
			right_res = 1
		if position==2:
			audio_ask_patch = 205
			right_res = 2
		if position==3:
			audio_ask_patch = 207
			right_res = 3

	elif(sub==5 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	#blue pushscooter
	elif(sub==10 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)				
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 222
			right_res = 0
		if position==1:
			audio_ask_patch = 220
			right_res = 1
		if position==2:
			audio_ask_patch = 221
			right_res = 2
		if position==3:
			audio_ask_patch = 223
			right_res = 3	

	elif(sub==11 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	#blue_bicycle 
	elif(sub==12 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 238
			right_res = 0
		if position==1:
			audio_ask_patch = 236
			right_res = 1
		if position==2:
			audio_ask_patch = 237
			right_res = 2
		if position==3:
			audio_ask_patch = 239
			right_res = 3	
	elif(sub==13 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==1):
		audio_ask_patch = 248
		right_res = 1	

	elif(sub==0 and col==2):
		audio_ask_patch = 248
		right_res = 1
	#yellow monkey
	elif(sub==1 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 162
			right_res = 0
		if position==1:
			audio_ask_patch = 160
			right_res = 1
		if position==2:
			audio_ask_patch = 161
			right_res = 2
		if position==3:
			audio_ask_patch = 163
			right_res = 3
	#yellow octpus		
	elif(sub==2 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 178
			right_res = 0
		if position==1:
			audio_ask_patch = 176
			right_res = 1
		if position==2:
			audio_ask_patch = 177
			right_res = 2
		if position==3:
			audio_ask_patch = 179
			right_res = 3

	#yellow parrot		
	elif(sub==3 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 194
			right_res = 0
		if position==1:
			audio_ask_patch = 192
			right_res = 1
		if position==2:
			audio_ask_patch = 193
			right_res = 2
		if position==3:
			audio_ask_patch = 195
			right_res = 3
	#yellow lion		
	elif(sub==4 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 210
			right_res = 0
		if position==1:
			audio_ask_patch = 208
			right_res = 1
		if position==2:
			audio_ask_patch = 209
			right_res = 2
		if position==3:
			audio_ask_patch = 211
			right_res = 3

	elif(sub==5 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	#yellow_pushscooter
	elif(sub==10 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 226
			right_res = 0
		if position==1:
			audio_ask_patch = 224
			right_res = 1
		if position==2:
			audio_ask_patch = 225
			right_res = 2
		if position==3:
			audio_ask_patch = 227
			right_res = 3	

	elif(sub==11 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	#yellow_bicycle	
	elif(sub==12 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 242
			right_res = 0
		if position==1:
			audio_ask_patch = 240
			right_res = 1
		if position==2:
			audio_ask_patch = 241
			right_res = 2
		if position==3:
			audio_ask_patch = 243
			right_res = 3

	elif(sub==13 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	
	elif(sub==0 and col==3 ):
		audio_ask_patch = 248
		right_res = 1
	#green monkey
	elif(sub==1 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 166
			right_res = 0
		if position==1:
			audio_ask_patch = 164
			right_res = 1
		if position==2:
			audio_ask_patch = 165
			right_res = 2
		if position==3:
			audio_ask_patch = 167
			right_res = 3
	#green octopus		
	elif(sub==2 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 182
			right_res = 0
		if position==1:
			audio_ask_patch = 180
			right_res = 1
		if position==2:
			audio_ask_patch = 181
			right_res = 2
		if position==3:
			audio_ask_patch = 183
			right_res = 3
	#green parrot		
	elif(sub==3 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 198
			right_res = 0
		if position==1:
			audio_ask_patch = 196
			right_res = 1
		if position==2:
			audio_ask_patch = 197
			right_res = 2
		if position==3:
			audio_ask_patch = 199
			right_res = 3
	#green lion		
	elif(sub==4 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 214
			right_res = 0
		if position==1:
			audio_ask_patch = 212
			right_res = 1
		if position==2:
			audio_ask_patch = 213
			right_res = 2
		if position==3:
			audio_ask_patch = 215
			right_res = 3

	elif(sub==5 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==3):
		audio_ask_patch = 248
		right_res = 1
	elif(sub==8 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	#green_pushscooter	
	elif(sub==10 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 230
			right_res = 0
		if position==1:
			audio_ask_patch = 228
			right_res = 1
		if position==2:
			audio_ask_patch = 229
			right_res = 2
		if position==3:
			audio_ask_patch = 231
			right_res = 3	

	elif(sub==11 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	#green_bicycle
	elif(sub==12 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 246
			right_res = 0
		if position==1:
			audio_ask_patch = 244
			right_res = 1
		if position==2:
			audio_ask_patch = 245
			right_res = 2
		if position==3:
			audio_ask_patch = 247
			right_res = 3

	elif(sub==13 and col==3):
		audio_ask_patch = 248
		right_res = 1
	elif(sub==14 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==3):
		audio_ask_patch = 248
		right_res = 1
	elif(sub==16 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	##########################################		
	elif(sub==0 and col==0):
		audio_ask_patch = 248
		
	#red monkey
	elif(sub==1 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 154
			right_res = 0
		if position==1:
			audio_ask_patch = 152
			right_res = 1
		if position==2:
			audio_ask_patch = 153
			right_res = 2
		if position==3:
			audio_ask_patch = 155
			right_res = 3
	#red octopus
	elif(sub==2 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 170
			right_res = 0
		if position==1:
			audio_ask_patch = 168
			right_res = 1
		if position==2:
			audio_ask_patch = 169
			right_res = 2
		if position==3:
			audio_ask_patch = 171
			right_res = 3
	#red parrot
	elif(sub==3 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 186
			right_res = 0
		if position==1:
			audio_ask_patch = 184
			right_res = 1
		if position==2:
			audio_ask_patch = 185
			right_res = 2
		if position==3:
			audio_ask_patch = 187
			right_res = 3
	#red lion
	elif(sub==4 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 202
			right_res = 0
		if position==1:
			audio_ask_patch = 200
			right_res = 1
		if position==2:
			audio_ask_patch = 201
			right_res = 2
		if position==3:
			audio_ask_patch = 203			
			right_res = 3
	elif(sub==5 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	#red_pushscooter
	elif(sub==10 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 218
			right_res = 0
		if position==1:
			audio_ask_patch = 216
			right_res = 1
		if position==2:
			audio_ask_patch = 217
			right_res = 2
		if position==3:
			audio_ask_patch = 219
			right_res = 3	
	elif(sub==11 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	#red_bicycle
	elif(sub==12 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 234
			right_res = 0
		if position==1:
			audio_ask_patch = 232
			right_res = 1
		if position==2:
			audio_ask_patch = 233
			right_res = 2
		if position==3:
			audio_ask_patch = 235
			right_res = 3

	elif(sub==13 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	
	elif(sub==0 and col==1):
		audio_ask_patch = 248
		right_res = 1
	#blue monkey	
	elif(sub==1 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 158
			right_res = 0
		if position==1:
			audio_ask_patch = 156
			right_res = 1
		if position==2:
			audio_ask_patch = 157
			right_res = 2
		if position==3:
			audio_ask_patch = 159
			right_res = 3
	#blue octopus		
	elif(sub==2 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 174
			right_res = 0
		if position==1:
			audio_ask_patch = 172
			right_res = 1
		if position==2:
			audio_ask_patch = 173
			right_res = 2
		if position==3:
			audio_ask_patch = 175
			right_res = 3
	#blue parrot		
	elif(sub==3 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 190
			right_res = 0
		if position==1:
			audio_ask_patch = 188
			right_res = 1
		if position==2:
			audio_ask_patch = 189
			right_res = 2
		if position==3:
			audio_ask_patch = 191
			right_res = 3
	#blue lion		
	elif(sub==4 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 206
			right_res = 0
		if position==1:
			audio_ask_patch = 204
			right_res = 1
		if position==2:
			audio_ask_patch = 205
			right_res = 2
		if position==3:
			audio_ask_patch = 207			
			right_res = 3
	elif(sub==5 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	#blue pushscooter
	elif(sub==10 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 222
			right_res = 0
		if position==1:
			audio_ask_patch = 220
			right_res = 1
		if position==2:
			audio_ask_patch = 221
			right_res = 2
		if position==3:
			audio_ask_patch = 223
			right_res = 3	
	elif(sub==11 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	#blue_bicycle 
	elif(sub==12 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 238
			right_res = 0
		if position==1:
			audio_ask_patch = 236
			right_res = 1
		if position==2:
			audio_ask_patch = 237
			right_res = 2
		if position==3:
			audio_ask_patch = 239
			right_res = 3
	elif(sub==13 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	
	elif(sub==0 and col==2):
		audio_ask_patch = 248
		right_res = 1
	#yellow monkey
	elif(sub==1 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 162
			right_res = 0
		if position==1:
			audio_ask_patch = 160
			right_res = 1
		if position==2:
			audio_ask_patch = 161
			right_res = 2
		if position==3:
			audio_ask_patch = 163
			right_res = 3
	#yellow octpus		
	elif(sub==2 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 178
			right_res = 0
		if position==1:
			audio_ask_patch = 176
			right_res = 1
		if position==2:
			audio_ask_patch = 177
			right_res = 2
		if position==3:
			audio_ask_patch = 179
			right_res = 3
	#yellow parrot		
	elif(sub==3 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 194
			right_res = 0
		if position==1:
			audio_ask_patch = 192
			right_res = 1
		if position==2:
			audio_ask_patch = 193
			right_res = 2
		if position==3:
			audio_ask_patch = 195
			right_res = 3
	#yellow lion		
	elif(sub==4 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 210
			right_res = 0
		if position==1:
			audio_ask_patch = 208
			right_res = 1
		if position==2:
			audio_ask_patch = 209
			right_res = 2
		if position==3:
			audio_ask_patch = 211
			right_res = 3

	elif(sub==5 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	#yellow_pushscooter
	elif(sub==10 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 226
			right_res = 0
		if position==1:
			audio_ask_patch = 224
			right_res = 1
		if position==2:
			audio_ask_patch = 225
			right_res = 2
		if position==3:
			audio_ask_patch = 227
			right_res = 3	
	elif(sub==11 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==12 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 242
			right_res = 0
		if position==1:
			audio_ask_patch = 240
			right_res = 1
		if position==2:
			audio_ask_patch = 241
			right_res = 2
		if position==3:
			audio_ask_patch = 243
			right_res = 3
	elif(sub==13 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	
	elif(sub==0 and col==3 ):
		audio_ask_patch = 248
		right_res = 1
	#green monkey
	elif(sub==1 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 166
			right_res = 0
		if position==1:
			audio_ask_patch = 164
			right_res = 1
		if position==2:
			audio_ask_patch = 165
			right_res = 2
		if position==3:
			audio_ask_patch = 167
			right_res = 3
	#green octopus		
	elif(sub==2 and col==3):	
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 182
			right_res = 0
		if position==1:
			audio_ask_patch = 180
			right_res = 1
		if position==2:
			audio_ask_patch = 181
			right_res = 2
		if position==3:
			audio_ask_patch = 183
			right_res = 3
	#green parrot		
	elif(sub==3 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 198
			right_res = 0
		if position==1:
			audio_ask_patch = 196
			right_res = 1
		if position==2:
			audio_ask_patch = 197
			right_res = 2
		if position==3:
			audio_ask_patch = 199
			right_res = 3
	#green lion		
	elif(sub==4 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 214
			right_res = 0
		if position==1:
			audio_ask_patch = 212
			right_res = 1
		if position==2:
			audio_ask_patch = 213
			right_res = 2
		if position==3:
			audio_ask_patch = 215
			right_res = 3

	elif(sub==5 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	#green_pushscooter	
	elif(sub==10 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 230
			right_res = 0
		if position==1:
			audio_ask_patch = 228
			right_res = 1
		if position==2:
			audio_ask_patch = 229
			right_res = 2
		if position==3:
			audio_ask_patch = 231
			right_res = 3	
	elif(sub==11 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	#green_bicycle 
	elif(sub==12 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 246
			right_res = 0
		if position==1:
			audio_ask_patch = 244
			right_res = 1
		if position==2:
			audio_ask_patch = 245
			right_res = 2
		if position==3:
			audio_ask_patch = 247
			right_res = 3

	elif(sub==13 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==3):
		audio_ask_patch = 248
		right_res = 1
	##########################################
	
	elif(sub==0 and col==0):
		audio_ask_patch = 300
		right_res = 1
	#red monkey
	elif(sub==1 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 154
			right_res = 0
		if position==1:
			audio_ask_patch = 152
			right_res = 1
		if position==2:
			audio_ask_patch = 153
			right_res = 2
		if position==3:
			audio_ask_patch = 155
			right_res = 3
	#red octopus
	elif(sub==2 and col==0):		
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 170
			right_res = 0
		if position==1:
			audio_ask_patch = 168
			right_res = 1
		if position==2:
			audio_ask_patch = 169
			right_res = 2
		if position==3:
			audio_ask_patch = 171
			right_res = 3
	#red parrot
	elif(sub==3 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 186
			right_res = 0
		if position==1:
			audio_ask_patch = 184
			right_res = 1
		if position==2:
			audio_ask_patch = 185
			right_res = 2
		if position==3:
			audio_ask_patch = 187
			right_res = 3
	#red lion
	elif(sub==4 and col==0):	
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 202
			right_res = 0
		if position==1:
			audio_ask_patch = 200
			right_res = 1
		if position==2:
			audio_ask_patch = 201
			right_res = 2
		if position==3:
			audio_ask_patch = 203
			right_res = 3
			
	elif(sub==5 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	#red_pushscooter
	elif(sub==10 and col==0):	
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 218
			right_res = 0
		if position==1:
			audio_ask_patch = 216
			right_res = 1
		if position==2:
			audio_ask_patch = 217
			right_res = 2
		if position==3:
			audio_ask_patch = 219
			right_res = 3	
	elif(sub==11 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	#red_bicycle
	elif(sub==12 and col==0):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 234
			right_res = 0
		if position==1:
			audio_ask_patch = 232
			right_res = 1
		if position==2:
			audio_ask_patch = 233
			right_res = 2
		if position==3:
			audio_ask_patch = 235
			right_res = 3

	elif(sub==13 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==0):
		audio_ask_patch = 248
		right_res = 1	
	
	elif(sub==0 and col==1):
		audio_ask_patch = 248
		right_res = 1
	#blue monkey	
	elif(sub==1 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 158
			right_res = 0
		if position==1:
			audio_ask_patch = 156
			right_res = 1
		if position==2:
			audio_ask_patch = 157
			right_res = 2
		if position==3:
			audio_ask_patch = 159
			right_res = 3
	#blue octopus		
	elif(sub==2 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 174
			right_res = 0
		if position==1:
			audio_ask_patch = 172
			right_res = 1
		if position==2:
			audio_ask_patch = 173
			right_res = 2
		if position==3:
			audio_ask_patch = 175
			right_res = 3
	#blue parrot		
	elif(sub==3 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 190
			right_res = 0
		if position==1:
			audio_ask_patch = 188
			right_res = 1
		if position==2:
			audio_ask_patch = 189
			right_res = 2
		if position==3:
			audio_ask_patch = 191
			right_res = 3
	#blue lion		
	elif(sub==4 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 206
			right_res = 0
		if position==1:
			audio_ask_patch = 204
			right_res = 1
		if position==2:
			audio_ask_patch = 205
			right_res = 2
		if position==3:
			audio_ask_patch = 207
			right_res = 3
			
	elif(sub==5 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	#blue pushscooter
	elif(sub==10 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 222
			right_res = 0
		if position==1:
			audio_ask_patch = 220
			right_res = 1
		if position==2:
			audio_ask_patch = 221
			right_res = 2
		if position==3:
			audio_ask_patch = 223
			right_res = 3		
	elif(sub==11 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	#blue_bicycle
	elif(sub==12 and col==1):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 238
			right_res = 0
		if position==1:
			audio_ask_patch = 236
			right_res = 1
		if position==2:
			audio_ask_patch = 237
			right_res = 2
		if position==3:
			audio_ask_patch = 239
			right_res = 3

	elif(sub==13 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==1):
		audio_ask_patch = 248
		right_res = 1	
	
	elif(sub==0 and col==2):
		audio_ask_patch = 248
		right_res = 1
	#yellow monkey
	elif(sub==1 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 162
			right_res = 0
		if position==1:
			audio_ask_patch = 160
			right_res = 1
		if position==2:
			audio_ask_patch = 161
			right_res = 2
		if position==3:
			audio_ask_patch = 163
			right_res = 3
	#yellow octpus		
	elif(sub==2 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 178
			right_res = 0
		if position==1:
			audio_ask_patch = 176
			right_res = 1
		if position==2:
			audio_ask_patch = 177
			right_res = 2
		if position==3:
			audio_ask_patch = 179
			right_res = 3
	#yellow parrot		
	elif(sub==3 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 194
			right_res = 0
		if position==1:
			audio_ask_patch = 192
			right_res = 1
		if position==2:
			audio_ask_patch = 193
			right_res = 2
		if position==3:
			audio_ask_patch = 195
			right_res = 3
	#yellow lion		
	elif(sub==4 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 210
			right_res = 0
		if position==1:
			audio_ask_patch = 208
			right_res = 1
		if position==2:
			audio_ask_patch = 209
			right_res = 2
		if position==3:
			audio_ask_patch = 211
			right_res = 3
			
	elif(sub==5 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	#yellow_pushscooter
	elif(sub==10 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)

		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 226
			right_res = 0
		if position==1:
			audio_ask_patch = 224
			right_res = 1
		if position==2:
			audio_ask_patch = 225
			right_res = 2
		if position==3:
			audio_ask_patch = 227
			right_res = 3
	elif(sub==11 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	#yellow_bicycle 
	elif(sub==12 and col==2):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 242
			right_res = 0
		if position==1:
			audio_ask_patch = 240
			right_res = 1
		if position==2:
			audio_ask_patch = 241
			right_res = 2
		if position==3:
			audio_ask_patch = 243
			right_res = 3
	elif(sub==13 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==2):
		audio_ask_patch = 248
		right_res = 1	
	
	elif(sub==0 and col==3 ):
		audio_ask_patch = 248
		right_res = 1
	#green monkey
	elif(sub==1 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 166
			right_res = 0
		if position==1:
			audio_ask_patch = 164
			right_res = 1
		if position==2:
			audio_ask_patch = 165
			right_res = 2
		if position==3:
			audio_ask_patch = 167
			right_res = 3
	#green octopus		
	elif(sub==2 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
						
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 182
			right_res = 0
		if position==1:
			audio_ask_patch = 180
			right_res = 1
		if position==2:
			audio_ask_patch = 181
			right_res = 2
		if position==3:
			audio_ask_patch = 183
			right_res = 3
	#green parrot		
	elif(sub==3 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		
				
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 198
			right_res = 0
		if position==1:
			audio_ask_patch = 196
			right_res = 1
		if position==2:
			audio_ask_patch = 197
			right_res = 2
		if position==3:
			audio_ask_patch = 199
			right_res = 3
	#green lion		
	elif(sub==4 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		
				
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 214
			right_res = 0
		if position==1:
			audio_ask_patch = 212
			right_res = 1
		if position==2:
			audio_ask_patch = 213
			right_res = 2
		if position==3:
			audio_ask_patch = 215	
			right_res = 3
			
	elif(sub==5 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==6 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==7 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==8 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==9 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	#green_pushscooter	
	elif(sub==10 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		
				
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 230
			right_res = 0
		if position==1:
			audio_ask_patch = 228
			right_res = 1
		if position==2:
			audio_ask_patch = 229
			right_res = 2
		if position==3:
			audio_ask_patch = 231
			right_res = 3	
	elif(sub==11 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	#green_bicycle 
	elif(sub==12 and col==3):
		if israndomico:
			while position in just_choosed:	
				position = random.randint(0,3)
		
		just_choosed.append(position)
		
		if position==0:
			audio_ask_patch = 246
			right_res = 0
		if position==1:
			audio_ask_patch = 244
			right_res = 1
		if position==2:
			audio_ask_patch = 245
			right_res = 2
		if position==3:
			audio_ask_patch = 247
			right_res = 3
	elif(sub==13 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==14 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==15 and col==3):
		audio_ask_patch = 248
		right_res = 1	
	elif(sub==16 and col==3):
		audio_ask_patch = 248
		right_res = 1

	return position,just_choosed,audio_ask_patch,right_res 
# ===================================== # ===================================== #
# Genres:
# ===================================== # ===================================== #
cdef inline find_genres(int[4] subjects): 
	cdef:
		int i = 0
		int[4] genres = [0,0,0,0]
	for i in range(4):
		if subjects[i]==0:
			genres[i]==0 #solid_color
		if subjects[i]==1 or subjects[i]==2 or subjects[i]==3 or subjects[i]==4 or subjects[i]==5:
			genres[i]==1 #animal
		if subjects[i]==10:
			genres[i]==2 #food
		if subjects[i]==6:
			genres[i]==3 #dress
		if subjects[i]==7 or subjects[i]==8 or subjects[i]==9:
			genres[i]==4 #things
		if subjects[i]==5:
			genres[i]==5 #human
	return genres