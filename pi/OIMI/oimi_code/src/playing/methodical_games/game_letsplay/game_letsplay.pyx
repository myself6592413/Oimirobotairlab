""" Accomplishing methodical games
	Methods
		Create results
		Play game 
			--> ask question, evaluate ans
			--> call game reactions methods
	its_been_a_while_quit = after too much is passed waiting for a patch to be touched
	Notes :
		here audios are indicated as int differently from usual
"""
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/score')
sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/games_reactions')
import copy,time,random
cimport score as res 
import numpy as np
cimport games_reactions as gare
cimport games_calcs as gaca
from collections import Counter
class PlayGameException(Exception):
	print("Too much indecisions")
	print("Too many errors")
    pass
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef create_results(one_game):
	#print("ENTER IN CREATE RESULTS")
	#print("one_game.num_of_questions={}".format(one_game.num_of_questions))
	cdef:
		list temp=[]
		int z
		int j
		Py_ssize_t i = 0
		Py_ssize_t leng = one_game.num_of_questions
		#res.Score[:] results
	for i in range(leng):
		re = res.Score()
		re.set_nameresult(one_game.id_game,one_game.listaudios_int[i])
		re.curr_question = one_game.listaudios_int[i]
		print("letsplay: curr_question to {} ".format(one_game.listaudios_int[i]))
		temp.append(re)
	retemp = np.asarray(temp,dtype = res.Score)
	one_game.results = retemp
	print("one_game.results = {}".format(one_game.results))
	#print("temp is {}".format(temp[0].num_errors))
	#print("one_game.results is {}".format(one_game.results[0].num_errors))
	print("len len RESULTS {}".format(len(one_game.results)))
	# 88 default
	for _,r in enumerate(one_game.results):
		score_ini = r
		score_ini.complexity_sess = 88
		score_ini.category = 88
		score_ini.difficulty_match = 88
		score_ini.difficulty_game = 88
	for j,og in enumerate(one_game.results):
		print("risultati ERRORS {} , {}".format(j,og.num_errors))
		print("risultati CORRECT{} , {}".format(j,og.num_corrects))
		print("risultati INDECISION{} ,{}".format(j,og.num_indecisions))
		print("COMPLECITYU SESSAA IS {} ,{}".format(j,og.complexity_sess))
		print("difficulty_match MATSA IS {} ,{}".format(j,og.difficulty_match))
		print("difficulty_match GANE IS {} ,{}".format(j,og.difficulty_match))
cdef str play_game(one_game):
	cdef:
		Py_ssize_t q = 0 
		Py_ssize_t num_ques = copy.copy(one_game.num_of_questions)
		int risp = 0
		int turn = 0
		#old audios numbers: 
		int audio_conclusion = 86 
		int audio_start_game = 151 
		int audio_end_game = 146 
		bint flag_time_elapsed = 0 
		int audio_ti_manca_ancora = 14 
		int ans1
		int ans2 
		int ans3
		int ans4 
		float wait_time = 0
		list already_given_resp=[]
		bint strange_case_3s = 0
		bint miniround_3s = 0
		bint use_turns = 0
		bint repeat_question = 1 #depends from age...etc
		bint first_time_need_led = 1
		bint choose_ques_ans_to_use
		bint its_been_a_while_quit = 0
		int lion_done3s = 0
		int monkey_done3s = 0
		int parrot_done3s = 0
		int octopus_done3s = 0
		int too_many_errors = 0
		int too_many_consecutive_indecisions = 0
		int too_many_consecutive_wrong_ans = 0
		str everything_went_well='ok_all_done_partita_finita'
	create_results(one_game)
	time_start_game = time.time()
	#print("one_game.num_of_questions {}".format(one_game.num_of_questions))
	#print("one_game.id_gameid_gameid_game {}".format(one_game.id_game))
	#print("one_game.this_kind {}".format(one_game.this_kind))
	#print("one_game.this_type {}".format(one_game.this_type))
    start = time.time()
    end = time.time()
    print(end - start)        
	if one_game.difficulty_of_game in ['Easy_1','Easy_2','Normal_1','Normal_2']:
		use_turns = 1
	try:
		while q < one_game.num_of_questions:
			if too_many_consecutive_indecisions>4 or too_many_consecutive_wrong_ans>4 or too_many_errors>9:
				if too_many_consecutive_indecisions>4:
					print("too_many_consecutive_indecisions==5 basta time scaduto BASTA BASTA BASTA BASTA BASTA BASTA BASTA BASTA BASTA BASTA BASTA BASTA BASTA ")
				if too_many_consecutive_wrong_ans>4:
					print("too_many_consecutive_indecisions==5 basta time scaduto WRONG WRONG WRONG WRONG WRONG WRONG WRONG WRONG WRONG WRONG WRONG ")
				if too_many_errors>9:
					print("too many errors ECCOCI  TROPI errors")
				its_been_a_while_quit = 1
			if not its_been_a_while_quit:
				if one_game.this_kind=='MIX':
					this_momentaneous_kind = acp2.find_kind(one_game.listaTypes[q])
				if this_momentaneous_kind==['1F','4P','5K'] or one_game.this_kind in ['1F','4P','5K']:
					if one_game.this_type in ['1FSUDT','1FCODT'] or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q] in ['1FSUDT','1FCODT']):
						if use_turns==0:
							#prepare MSG SERIAL
							ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
							if ans_array[0]==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0							
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
								print("mm error ans2 == 5 mai possibile qui non entrerò mai")
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
								if len(ans_array)==2:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										print("error")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										if one_game.limit==1 and one_game.limit1==1: #only possibility, there are always 2 answer here 
											if((gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1))):
												tot_res = 1
											else:
												tot_res = 0
										print("tot_res ---> {}".format(tot_res))
										print("both res_ottenute 2 in play_the_game con 2 risp da dare")
										last_ans_reached_flag_at_ans_num = 1
								if len(ans_array)<2:
									continue
								elif len(ans_array)>2:
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
								if len(ans_array)==3:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										print("mm error 333 ")
										print("duplicates are {}".format(duplic_array))
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										if one_game.limit==2 and one_game.limit1==1:
											if((gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2))):
												tot_res = 1
											else:
												tot_res = 0
										if one_game.limit==1 and one_game.limit1==2:
											if((gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1))):
												tot_res = 1
											else:
												tot_res = 0
								if len(ans_array)<3:
									print("mi aspetto piu risposte, ne ho solo 3")
									continue
								elif len(ans_array)>3:
									print("mi aspetto meno risposte ne ho ben 3")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
								last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)==4:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 4444444")
										print("duplicates are {}".format(duplic_array))
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										if one_game.limit==1 and one_game.limit1==3:
											if((gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1))):
												tot_res = 1
											else:
												tot_res = 0
										if one_game.limit==3 and one_game.limit1==1:
											if((gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3))):
												tot_res = 1
											else:
												tot_res = 0										
											#res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											#res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
											#res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
											#res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
											#tot_res_tmp = res1 or res2 or res3
											#tot_res = tot_res_tmp or res4
										if one_game.limit==2 and one_game.limit1==2:
											if((gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)) or \
												(gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4) and gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2))):
												tot_res = 1
											else:
												tot_res = 0										
											#res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											#res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
											#res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											#res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
											#tot_res_tmp_1 = res1 or res2
											#tot_res_tmp_2 = res3 or res4
											#tot_res = tot_res_tmp_1 or tot_res_tmp_2
										last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)<4:
									continue
								elif len(ans_array)>4:
									continue
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								gare.wrong_answer('false ser') 
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue	
						else:
							print("    round{}".format(q))
							if turn==0: #question zero prima che a sua volta a n risposte 
								print("-->siamo in turno zero")
								time.sleep(0.2)
								ans1, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time) 
								#ans1, wait_time = 1,5 #ESEMPI A CASO
								print('stampa response given {}'.format(ans1))
								print('stampa time  {}'.format(wait_time))
								if ans1==30: 
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									print("^^^^^^^ siamo in INDECISION")
									flag_time_elapsed = 1
									gare.express_indecision('messaggio_indecisione')
									continue
								too_many_consecutive_indecisions = 0
								print("ho superato il continue con i turni")
								flag_time_elapsed = 1
								if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
									tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									last_ans_reached_flag_at_ans_num = 0
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #2 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 1
									res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
									print("single res_ottenuta 1 in play_the_game con 2 risp da dare {}".format(res1))
									print("single res_ottenuta 2 in play_the_game con 2 risp da dare {}".format(res2))
									tot_res = res1 or res2 
									print("single res_ottenuta in play_the_game con 2 da dare OR {}".format(tot_res))
									if res1:
										already_given_resp.append(one_game.ques_ans[q].answer1)
									if res2:
										already_given_resp.append(one_game.ques_ans[q].answer2)
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #3 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 2
									if one_game.limit==1:
										tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										print("single res_ottenuta in play_the_game con 3 da dare OR {}".format(tot_res))
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer1)
											too_many_consecutive_wrong_ans = 0
									if one_game.limit==2:
										res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
										tot_res = res1 or res2
										print("single res_ottenuta in play_the_game con 3 da dare OR {}".format(tot_res))
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #4 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 3
									if one_game.limit==1:
										tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer1)
											too_many_consecutive_wrong_ans = 0
									if one_game.limit==2:
										res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
										tot_res = res1 or res2
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)
									if one_game.limit==3:
										res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)					
										res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)					
										res3 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer3)	
										tot_res = res1 or res2 or res3
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('rispesatta')
									turn+=1
									scon = 
									scoc = one_game.results[q]
									scoc.num_corrects+=1					
									if one_game.ques_ans[q].answer2==5: #NEXT STEP non esiste domdanda finita annullo anche gli already
										gare.start_audio(20) 
										too_many_consecutive_wrong_ans = 0
										q+=1
										turn = 0
										already_given_resp=[]
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('wrong') 
									too_many_consecutive_wrong_ans+=1
									too_many_errors+=1
									#start again from beginning with multiple answer , repeat both
									turn = 0
									already_given_resp=[] #solo in turno zero
									continue								
							if turn==1:
								print("-->siamo in turno uno")
								print("secondo round question multipla")
								ans2, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans2, wait_time = 2,5 #ESEMPI A CASO							
								print('stampa response given {}'.format(ans2))
								print('stampa time  {}'.format(wait_time))
								if ans2==30:
									print("indecision")
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									#reactrion indecision start here
									gare.express_indecision('finto_ser_indecisione')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn1")
								if ans2 in already_given_resp or ans2 in already_given_resp:
									print("false already given")
									#reazione already start here
									gare.already_given_answer('finto_ser_already_given')
									scon = one_game.results[q]
									scon.num_given_ans+=1
									continue #for now reapeat from the last right answer
								else:
									if one_game.ques_ans[q].answer3==5:
										last_ans_reached_flag = 1
										tot_res = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											too_many_consecutive_wrong_ans = 0
									elif one_game.ques_ans[q].answer3=5 and  one_game.ques_ans[q].answer4==5:
										last_ans_reached_flag = 2
										if one_game.limit1==1: #MEANS THAT LIMIT WAS = 2 
											tot_res = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
											if tot_res:
												already_given_resp.append(one_game.ques_ans[q].answer3)
												too_many_consecutive_wrong_ans = 0
										elif one_game.limit1==2: #MEANS THAT LIMIT WAS = 1
											res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
											res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
											tot_res = res1 or res2
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
									elif one_game.ques_ans[q].answer3=5 and  one_game.ques_ans[q].answer4=5:
										last_ans_reached_flag = 3
										if one_game.limit1==1: #MEANS THAT LIMIT WAS = 3
											tot_res = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
											if tot_res:
												already_given_resp.append(one_game.ques_ans[q].answer4)
												too_many_consecutive_wrong_ans = 0
										elif one_game.limit1==2: #MEANS THAT LIMIT WAS = 2
											res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
											res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer4)
											tot_res = res1 or res2
										elif one_game.limit1==3: #MEANS THAT LIMIT WAS = 1
											res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
											res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
											res3 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
											tot_res = res1 or res2 or res3
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res3:
												already_given_resp.append(one_game.ques_ans[q].answer4)
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									#reaction start here good boy
									gare.right_answer('finto_ser_right_Ans')			
									turn+=1
									scoc = one_game.results[q]
									scoc.num_corrects+=1		
									if one_game.ques_ans[q].answer3==5: #NEXT STEP
										gare.start_audio(20) 
										too_many_consecutive_wrong_ans = 0
										q+=1
										turn = 0 #reset turn
										already_given_resp=[]
									else:
										continue
								else: 
									print("FALSE, wrong answer")	
									# start bad reaction
									gare.wrong_answer('finto_Ser_wrong') #start again from beginning with multiple answer , repeat both
									too_many_consecutive_wrong_ans+=1
									too_many_errors+=1
									#avro due audio diversi quindi proviamo
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							if turn==2:
								print("-->siamo in turno tre")
								print("terzo round question multipla")
								ans3, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans3, wait_time = 3,5
								print('stampa response given {}'.format(ans3))
								print('stampa time  {}'.format(wait_time))
								#indecision 
								if ans3==30:
									print("indecisions")
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									#audio dovrebbe cambiare in base a che turno sono ti manca un leone da mettere o due o tre....
									flag_time_elapsed = 1
									gare.express_indecision('indeci_fake_ser')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn2")
								if ans3 in already_given_resp:
									#reazione already start here
									print("false already given")
									gare.already_given_answer('fake_already_ser')
									scon = one_game.results[q]
									scon.num_given_ans+=1								
									continue 
								else:
									if one_game.ques_ans[q].answer4==5:
										last_ans_reached_flag = 2
										if one_game.limit1==1: #means limit was = 2
											tot_res = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											if tot_res:
												already_given_resp.append(one_game.ques_ans[q].answer3)
												too_many_consecutive_wrong_ans = 0
										elif one_game.limit1==2: #means limit was = 1
											res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer2) and ans3 not in already_given_resp
											res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer3)
											tot_res = res1 or res2
									elif one_game.ques_ans[q].answer4=5:
										last_ans_reached_flag = 3
										if one_game.limit1==2: #means limit was = 2
											res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer4) and ans3 not in already_given_resp
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer4)
											tot_res = res1 or res2
										elif one_game.limit1==3: #means limit was = 1
											res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer2) and ans3 not in already_given_resp
											res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											res3 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer4) and ans3 not in already_given_resp
											tot_res = res1 or res2 or res3
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res3:
												already_given_resp.append(one_game.ques_ans[q].answer4)	
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('finto_ser_right_a')
									turn+=1
									scoc = one_game.results[q]
									scoc.num_corrects+=1
									if one_game.ques_ans[q].answer4==5: #NEXT STEP
										gare.start_audio(20) 
										too_many_consecutive_wrong_ans = 0
										q+=1
										turn = 0 #reset turn
										already_given_resp=[]
									else:
										continue
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('ser_wrong') 
									too_many_consecutive_wrong_ans+=1
									too_many_errors+=1
									#start again from beginning with multiple answer , repeat both
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							if turn==3:
								print("-->siamo in turno quattro")
								print("quarto round question multipla")
								ns4, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans4, wait_time = 4,5
								print('stampa response given {}'.format(ans4))
								print('stampa time  {}'.format(wait_time))
								if ans4==30:
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									gare.express_indecision('ser_fake_indec')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn3")
								if ans4 in already_given_resp:
									print("false already given")
									gare.already_given_answer('already_ser')
									scon = one_game.results[q]
									scon.num_given_ans+=1
									continue 
								else:
									if one_game.limit1==1: #means limit was = 3
										tot_res = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer4)
											too_many_consecutive_wrong_ans = 0
									elif one_game.limit1==2: #means limit was = 2
										res1 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer3) and ans4 not in already_given_resp
										res2 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer4)
										tot_res = res1 or res2
									elif one_game.limit1==3: #means limit was = 1
										res1 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer2) and ans4 not in already_given_resp
										res2 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer3) and ans4 not in already_given_resp
										res3 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
										tot_res = res1 or res2 or res3
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer4)	
									last_ans_reached_flag = 3
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('ser_right_ok')
									gare.start_audio(20) 
									too_many_consecutive_wrong_ans = 0
									q+=1
									turn = 0 #reset turn
									already_given_resp=[]
									scoc = one_game.results[q]
									scoc.num_corrects+=1								
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('wrong,sir ser')
									too_many_consecutive_wrong_ans+=1
									too_many_errors+=1
									#start again from beginning with multiple answer , repeat both
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							print("FINALE 6666666666666666 already_given_resp {}".format(already_given_resp))
					else:
						######################################################################################################################## useless stuff
						print("CLASSICO STANDARD il genere del gioco in questione è --> {}".format(one_game.this_kind))
						print("round round NUMERO {}".format(q))
						#0 SETTO SCORE OBJ RESULTS
						#print("ennerandom definitivo  è uguale a {}".format(ennerandom))
						print("ECCO SELF ALLOCATED of question {}".format(q))
						print(one_game.ques_ans[q].answer1)
						print(one_game.ques_ans[q].answer2)
						print(one_game.ques_ans[q].answer3)
						print(one_game.ques_ans[q].answer4)
						print()
						#RIFARE O TOGLIERE 
						#one_game.results[q].set_complexity(one_game.ques_ans[q].complexity)
						##metto game complex
						#one_game.results[q].set_category(0)
						#assegnamenti a caso per test che vanno ok
						sco = one_game.results[q]
						sco.complexity_sess = 10
						sco.category = 20
						sco.difficulty_match = 30
						sco.difficulty_game = 40
						#onon vanno questi NB metodo no
						#one_game.sco.set_complexity(0) #game_dificulty
						#one_game.sco.set_category(0) #game_category???? cos'era Animal/food/things??
						#one_game.sco.set_difficulty_match(0)
						#one_game.sco.set_difficulty_of_game(0)
						print("ADESSO DEBUGGHIAMO IL RESULT")
						print("LENGHT results is {}".format(len(one_game.results)))
						print("one_game.results[q].complexity_sess {}".format(one_game.results[q].complexity_sess))
						print("one_game.results[q].difficulty_match{}".format(one_game.results[q].difficulty_match))
						print("one_game.results[q].difficulty_game {}".format(one_game.results[q].difficulty_game))
						print("one_game.results[q].set_category {}".format(one_game.results[q].category))
						print("num_indecisions {}".format(one_game.results[q].num_indecisions))
						######################################################################################################################## useless stuff			
						if use_turns==0:
							ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
							print("ans_array_ricevuto USE_turns = 0 {}".format(ans_array))
							if ans_array[0]==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								if len(ans_array)==1:
									print("how was the limit? LIMIT = {} ".format(one_game.limit))
									print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
									tot_res = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								else:
									print("mi aspetto piu risp, ne ho solo 1")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
								if len(ans_array)==2:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 222")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										tot_res1 = res1 or res2 
										#check first ans
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										tot_res2 = res3 or res4
										print("both res_ottenute 2 in play_the_game con 2 risp da dare")
										last_ans_reached_flag_at_ans_num = 1
										tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<2:
									print("mi aspetto piu risposte, ne ho solo 2")
									continue
								elif len(ans_array)>2:
									print("mi aspetto meno risposte, ne ho ben 2")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
								if len(ans_array)==3:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 333 ")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										print("res")
										print(res1)
										print(res2)
										print(res3)
										tot_res1 = res1 or res2 or res3
										#check second ans
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										tot_res2 = res4 or res5 or res6
										print("res")
										print(res1)
										print(res2)
										print(res3)
										#check third ans
										res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
										res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										tot_res3 = res7 or res8 or res9
										print("res")
										print(res1)
										print(res2)
										print(res3)
										print("both res_ottenute 3 in play_the_game con 3 risp da dare")
										last_ans_reached_flag_at_ans_num = 2 
										tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<3:
									print("mi aspetto piu risposte, ne ho solo 3")
									continue
								elif len(ans_array)>3:
									print("mi aspetto meno risposte ne ho ben 3")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
								last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)==4:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 4444444")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										tot_res1 = res1 or res2 or res3 or res4
										#check second ans
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										tot_res2 = res5 or res6 or res7 or res8
										#check third ans
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
										res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
										res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
										tot_res3 = res9 or res10 or res11 or res12
										#check fourth ans
										res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
										res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer2)
										res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
										res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
										tot_res4 = res13 or res14 or res15 or res16
										print("both res_ottenute 4 in play_the_game con 4 risp da dare")
										last_ans_reached_flag_at_ans_num = 3 
										tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<4:
									print("mi aspetto piu risposte ne ho solo 4 ")
									continue
								elif len(ans_array)>4:
									print("mi aspetto meno risposte ne ho ben 5")
									continue
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								too_many_consecutive_wrong_ans = 0
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser')
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue
						else:
							#mi aspetto una response per volta e poi dico ok ancora una e faccio i turni
							#append to already uno per volta....
							print("    round{}".format(q))
							if turn==0: #question zero prima che a sua volta a n risposte 
								print("-->siamo in turno zero")
								time.sleep(0.2)
								ans1, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time) 
								#ans1, wait_time = 1,5 #ESEMPI A CASO
								print('stampa response given {}'.format(ans1))
								print('stampa time  {}'.format(wait_time))
								if ans1==30: 
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									print("^^^^^^^ siamo in INDECISION")
									flag_time_elapsed = 1
									gare.express_indecision('messaggio_indecisione')
									continue
								too_many_consecutive_indecisions = 0
								print("ho superato il continue con i turni")
								flag_time_elapsed = 1
								if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
									tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									last_ans_reached_flag_at_ans_num = 0
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #2 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 1
									res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
									print("single res_ottenuta 1 in play_the_game con 2 risp da dare {}".format(res1))
									print("single res_ottenuta 2 in play_the_game con 2 risp da dare {}".format(res2))
									tot_res = res1 or res2 
									print("single res_ottenuta in play_the_game con 2 da dare OR {}".format(tot_res))
									if res1:
										already_given_resp.append(one_game.ques_ans[q].answer1)
									if res2:
										already_given_resp.append(one_game.ques_ans[q].answer2)
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #3 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 2
									res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer3)
									print("single res_ottenuta 1 in play_the_game con 3 risp da dare {}".format(res1))
									print("single res_ottenuta 2 in play_the_game con 3 risp da dare {}".format(res2))
									print("single res_ottenuta 3 in play_the_game con 3 risp da dare {}".format(res3))
									tot_res = res1 or res2 or res3 
									print("single res_ottenuta in play_the_game con 3 da dare OR {}".format(tot_res))
									if res1:
										already_given_resp.append(one_game.ques_ans[q].answer1)
									if res2:
										already_given_resp.append(one_game.ques_ans[q].answer2)
									if res3:
										already_given_resp.append(one_game.ques_ans[q].answer3)
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #4 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 3
									res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer3)
									res4 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer4)					
									print("single res_ottenuta 1 in play_the_game con 4 risp da dare {}".format(res1))
									print("single res_ottenuta 2 in play_the_game con 4 risp da dare {}".format(res2))
									print("single res_ottenuta 3 in play_the_game con 4 risp da dare {}".format(res3))
									print("single res_ottenuta 4 in play_the_game con 4 risp da dare {}".format(res4))
									tot_res = res1 or res2 or res3 or res4
									print("single res_ottenuta in play_the_game con 4 da dare OR {}".format(tot_res))
									if res1:
										already_given_resp.append(one_game.ques_ans[q].answer1)
									if res2:
										already_given_resp.append(one_game.ques_ans[q].answer2)
									if res3:
										already_given_resp.append(one_game.ques_ans[q].answer3)
									if res4:
										already_given_resp.append(one_game.ques_ans[q].answer4)	
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('rispesatta')
									turn+=1
									scoc = one_game.results[q]
									scoc.num_corrects+=1								
									if one_game.ques_ans[q].answer2==5: #NEXT STEP non esiste domdanda finita annullo anche gli already
										gare.start_audio(20) 
										too_many_consecutive_wrong_ans = 0
										q+=1
										turn = 0
										already_given_resp=[]
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('wrong') 
									too_many_consecutive_wrong_ans+=1
									too_many_errors+=1
									#start again from beginning with multiple answer , repeat both
									turn = 0
									already_given_resp=[] #solo in turno zero
									continue								
							if turn==1:
								print("-->siamo in turno uno")
								print("secondo round question multipla")
								ans2, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans2, wait_time = 2,5 #ESEMPI A CASO							
								print('stampa response given {}'.format(ans2))
								print('stampa time  {}'.format(wait_time))
								if ans2==30:
									print("indecisione")
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									#reactrion indecision start here
									gare.express_indecision('finto_ser_indecisione')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn1")
								if ans2 in already_given_resp or ans2 in already_given_resp:
									print("false already given")
									#reazione already start here
									gare.already_given_answer('finto_ser_already_given')
									scon = one_game.results[q]
									scon.num_given_ans+=1								
									continue #for now reapeat from the last right answer
								else:
									if one_game.ques_ans[q].answer3==5:
										last_ans_reached_flag = 1
										res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer1) and ans2 not in already_given_resp
										res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										res = res1 or res2 
										print("turno 1 single res_ottenuta 1 in play_the_game con 2 risp da dare {}".format(res1))
										print("turno 1 single res_ottenuta 2 in play_the_game con 2 risp da dare {}".format(res2))
									elif one_game.ques_ans[q].answer3=5 and  one_game.ques_ans[q].answer4==5:
										last_ans_reached_flag = 2
										res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer1) and ans2 not in already_given_resp
										res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
										res3 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
										print("turno 1 single res_ottenuta 1 in play_the_game con 3 risp da dare {}".format(res1))
										print("turno 1 single res_ottenuta 2 in play_the_game con 3 risp da dare {}".format(res2))							
										print("turno 1 single res_ottenuta 3 in play_the_game con 3 risp da dare {}".format(res3))							
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										res = res1 or res2 or res3
									elif one_game.ques_ans[q].answer3=5 and  one_game.ques_ans[q].answer4=5:
										last_ans_reached_flag = 3
										res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer1) and ans2 not in already_given_resp
										res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
										res3 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
										res4 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
										print("turno 1 single res_ottenuta 1 in play_the_game con 4 risp da dare {}".format(res1))
										print("turno 1 single res_ottenuta 2 in play_the_game con 4 risp da dare {}".format(res2))							
										print("turno 1 single res_ottenuta 3 in play_the_game con 4 risp da dare {}".format(res3))
										print("turno 1 single res_ottenuta 4 in play_the_game con 4 risp da dare {}".format(res4))
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										if res4:
											already_given_resp.append(one_game.ques_ans[q].answer4)					 					
										res = res1 or res2 or res3 or res4 
								if res:
									#reaction start here good boy
									gare.right_answer('finto_ser_right_Ans')			
									turn+=1		
									if one_game.ques_ans[q].answer3==5: #NEXT STEP
										gare.start_audio(20) 
										q+=1
										turn = 0 #reset turn
										already_given_resp=[]
									else:
										continue
								else: 
									print("FALSE, wrong answer")	
									# start bad reaction
									gare.wrong_answer('finto_Ser_wrong') #start again from beginning with multiple answer , repeat both
									#avro due audio diversi quindi proviamo
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							if turn==2:
								print("-->siamo in turno tre")
								print("terzo round question multipla")
								ans3, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans3, wait_time = 3,5
								print('stampa response given {}'.format(ans3))
								print('stampa time  {}'.format(wait_time))
								#indecision 
								if ans3==30:
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									#audio dovrebbe cambiare in base a che turno sono ti manca un leone da mettere o due o tre....
									flag_time_elapsed = 1
									gare.express_indecision('indeci_fake_ser')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn2")
								if ans3 in already_given_resp:
									#reazione already start here
									print("false already given")
									gare.already_given_answer('fake_already_ser')
									scon = one_game.results[q]
									scon.num_given_ans+=1								
									continue 
								else:
									if one_game.ques_ans[q].answer4==5:
										last_ans_reached_flag = 2
										res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer1) and ans3 not in already_given_resp
										res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer2) and ans3 not in already_given_resp
										res3 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
										print("turno 2 single res_ottenuta 1 in play_the_game con 3 risp da dare {}".format(res1))
										print("turno 2 single res_ottenuta 2 in play_the_game con 3 risp da dare {}".format(res2))
										print("turno 2 single res_ottenuta 3 in play_the_game con 3 risp da dare {}".format(res3))
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										res = res1 or res2 or res3 
									elif one_game.ques_ans[q].answer4=5:
										last_ans_reached_flag = 3
										res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer1) and ans3 not in already_given_resp
										res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer2) and ans3 not in already_given_resp
										res3 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
										res4 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer4) and ans3 not in already_given_resp
										print("turno 2 single res_ottenuta 1 in play_the_game con 4 risp da dare {}".format(res1))
										print("turno 2 single res_ottenuta 2 in play_the_game con 4 risp da dare {}".format(res2))
										print("turno 2 single res_ottenuta 3 in play_the_game con 4 risp da dare {}".format(res3))
										print("turno 2 single res_ottenuta 4 in play_the_game con 4 risp da dare {}".format(res4))							
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										if res4:
											already_given_resp.append(one_game.ques_ans[q].answer4)					 					
										res = res1 or res2 or res3 or res4 	
								if res:
									print("correct response:esatta")
									gare.right_answer('finto_ser_right_a')
									turn+=1						
									if one_game.ques_ans[q].answer4==5: #NEXT STEP
										gare.start_audio(20) 
										q+=1
										turn = 0 #reset turn
										already_given_resp=[]
									else:
										continue
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('ser_wrong') 
									#start again from beginning with multiple answer , repeat both
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							if turn==3:
								print("-->siamo in turno quattro")
								print("quarto round question multipla")
								ans4, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans4, wait_time = 4,5
								print('stampa response given {}'.format(ans4))
								print('stampa time  {}'.format(wait_time))
								if ans4==30:
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									gare.express_indecision('ser_fake_indec')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn3")
								if ans4 in already_given_resp:
									print("false already given")
									gare.already_given_answer('already_ser')
									scon = one_game.results[q]
									scon.num_given_ans+=1								
									continue 
								else:
									last_ans_reached_flag = 3
									#posso sistemarlo easy
									res1 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer1) and ans4 not in already_given_resp
									res2 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer2) and ans4 not in already_given_resp
									res3 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer3) and ans4 not in already_given_resp
									res4 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
									print("turno 3 single res_ottenuta 1 in play_the_game con 4 risp da dare {}".format(res1))
									print("turno 3 single res_ottenuta 2 in play_the_game con 4 risp da dare {}".format(res2))
									print("turno 3 single res_ottenuta 3 in play_the_game con 4 risp da dare {}".format(res3))
									print("turno 3 single res_ottenuta 4 in play_the_game con 4 risp da dare {}".format(res4))
									res = res1 or res2 or res3 or res4 
								if res:
									print("correct response:esatta")
									gare.right_answer('ser_right_ok')
									gare.start_audio(20) 
									q+=1
									turn = 0 #reset turn
									already_given_resp=[]					
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('wrong,sir ser')
									#start again from beginning with multiple answer , repeat both
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							print("FINALE 6666666666666666 already_given_resp {}".format(already_given_resp))
				if this_momentaneous_kind=='2L' or one_game.this_kind=='2L':
					print("il genere del gioco in questione è 2l --> {}".format(one_game.this_kind))
					if one_game.this_type=='2LDSUOC' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='2LDSUOC'): #Tocca una scimmia se la luce è rossa altrimenti tocca un leone una sola risp 
						#one is enough
						#repeat question 
						if first_time_need_led:
							first_time_need_led = 0
							if repeat_question:
								first_time_need_led = 1
							#prepare MSG SERIAL
							print(one_game.current_game_colors)
							time.sleep(0.2)
							gare.start_audio(53)
							#if type of game serve il led === 2L						
							led_msg_start_color = b'<<f>>'
							lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
							ch = random.choices(lisa,k = 1)
							resp_msg_from_arduino = ch[0]
							print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
							resp_msg_from_arduino = b'<q>'
							if one_game.need_light=='Red' and resp_msg_from_arduino==b'<q>':
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Green' and resp_msg_from_arduino==b'<gre>':
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Blue' and resp_msg_from_arduino==b'<x>':
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Yellow' and resp_msg_from_arduino==b'<y>':
								choose_ques_ans_to_use = 0
							else:
								choose_ques_ans_to_use_to_use = 1
							print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
							print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
							gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
						ans_unique, wait_time= gare.wait_answer(one_game.ques_ans[q].required_time)
						print('stampa response given {}'.format(ans_unique))
						print('stampa time  {}'.format(wait_time))
						print("ho superato il continue in 2LDSUOC")
						flag_time_elapsed = 0
						print("one_game.current_game_genres {}".format(one_game.current_game_genres))
						if choose_ques_ans_to_use==0:
							if ans_unique==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								tot_res = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
								print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #una sola response ma ci sono più possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer2)
								tot_res = res1 or res2 
								last_ans_reached_flag_at_ans_num = 1
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #una sola response ma ci sono 3 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer3)
								tot_res = res1 or res2 or res3
								last_ans_reached_flag_at_ans_num = 2 
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #una sola response ma ci sono 4 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer3)
								res4 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer4)
								tot_res = res1 or res2 or res3 or res4
								last_ans_reached_flag_at_ans_num = 3
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								too_many_consecutive_wrong_ans = 0
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser')
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue 
						else:
							if ans_unique==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans_1[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								tot_res = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3==5: #una sola response ma ci sono più possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								tot_res = res1 or res2 
								last_ans_reached_flag_at_ans_num = 1
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4==5: #una sola response ma ci sono 3 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer3)
								tot_res = res1 or res2 or res3
								last_ans_reached_flag_at_ans_num = 2 
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4=5: #una sola response ma ci sono 4 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer3)
								res4 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer4)
								tot_res = res1 or res2 or res3 or res4
								last_ans_reached_flag_at_ans_num = 3
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue
					elif one_game.this_type=='2LDBOB' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='2LDBOB'): #Premi la mia testa se la luce è rossa altrimenti premi la mia pancia
						if first_time_need_led:
							first_time_need_led = 0
							if repeat_question:
								first_time_need_led = 1
							#prepare MSG SERIAL
							print(one_game.current_game_colors)
							time.sleep(0.2)
							gare.start_audio(53)
							#if type of game serve il led === 2L
							led_msg_start_color = b'<celeb>'
							choose_ques_ans_to_use = 1 #uso tabella ques_ans1 no one_game.ques_ans
							lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
							ch = random.choices(lisa,k = 1)
							resp_msg_from_arduino = ch[0]
							print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
							resp_msg_from_arduino = b'<q>'
							if one_game.need_light=='Red' and resp_msg_from_arduino==b'<q>':
								press_body = 1
							if one_game.need_light=='Green' and resp_msg_from_arduino==b'<gre>':
								press_body = 1
							if one_game.need_light=='Blue' and resp_msg_from_arduino==b'<x>':
								press_body = 1
							if one_game.need_light=='Yellow' and resp_msg_from_arduino==b'<y>':
								press_body = 1
							else:
								press_body = 0
							print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
							gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
							ans_unique, wait_time = gare.wait_mpx('serial_msg_mpx')
							if ans_unique==30: 
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								print("^^^^^^^ siamo in INDECISION")
								flag_time_elapsed = 1
								gare.express_indecision('inde_ser')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di sco {}".format(one_game.results[q].num_indecisions))
								continue			
							too_many_consecutive_indecisions = 0
							print("ho superato indecision")
							if ans_unique in [1,2,3,4] and press_body==1: 
								tot_res = 1
							if ans_unique==5 and press_body==0: 
								tot_res = 0
							print('stampa response given {}'.format(ans_unique))
							print('stampa time  {}'.format(wait_time))
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue
					elif one_game.this_type=='2LDBOC' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='2LDBOC'): #Premi la mia testa se la luce è rossa altrimenti tocca una scimmia
						#one is enough --> #there is no turn here
						if first_time_need_led:
							first_time_need_led = 0
							if repeat_question:
								first_time_need_led = 1
							print(one_game.current_game_colors)
							time.sleep(0.2)
							gare.start_audio(53)
							#if type of game serve il led === 2L
							led_msg_start_color = b'<<f>>'
							lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
							ch = random.choices(lisa,k = 1)
							resp_msg_from_arduino = ch[0]
							print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
							resp_msg_from_arduino = b'<q>'
							if one_game.need_light=='Red' and resp_msg_from_arduino==b'<q>': 
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Green' and resp_msg_from_arduino==b'<z>':
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Blue' and resp_msg_from_arduino==b'<x>': 
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Yellow' and resp_msg_from_arduino==b'<y>':
								choose_ques_ans_to_use = 0
							else:
								choose_ques_ans_to_use_to_use = 1
							print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
							print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
							gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
							print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
							gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
						print("ho superato il continue in 2LDBOC")
						flag_time_elapsed = 0
						print("one_game.current_game_genres {}".format(one_game.current_game_genres))
						if choose_ques_ans_to_use==0:
							ans_unique, wait_time = gare.wait_mpx('serial_msg_mpx')
							if ans_unique==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							if ans_unique in [1,2,3,4]: 
								tot_res = 1
							if ans_unique==5: 
								tot_res = 0
						else:
							ans_unique, wait_time= gare.wait_answer(one_game.ques_ans[q].required_time)
							print('stampa response given {}'.format(ans_unique))
							print('stampa time {}'.format(wait_time))
							if ans_unique==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans_1[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								tot_res = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3==5: #una sola response ma ci sono più possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								tot_res = res1 or res2 
								last_ans_reached_flag_at_ans_num = 1
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4==5: #una sola response ma ci sono 3 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer3)
								tot_res = res1 or res2 or res3
								last_ans_reached_flag_at_ans_num = 2 
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4=5: #una sola response ma ci sono 4 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer3)
								res4 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer4)
								tot_res = res1 or res2 or res3 or res4
								last_ans_reached_flag_at_ans_num = 3
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM correct {}".format(one_game.results[q].num_corrects))
								print("right resp")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue			
					elif one_game.this_type=='2LDSU' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='2LDSU'): #Tocca le scimmie se la luce è rossa 
						# more than one resp if there is one 
						# there is no turn 
						if first_time_need_led:
							first_time_need_led = 0
							if repeat_question:
								first_time_need_led = 1
							print(one_game.current_game_colors)
							time.sleep(0.2)
							gare.start_audio(53)
							#if type of game serve il led === 2L
							led_msg_start_color = b'<timandoilcoloregiusto--rosso>'
							lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
							ch = random.choices(lisa,k = 1)
							resp_msg_from_arduino = ch[0]
							print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
							resp_msg_from_arduino = b'<q>'
							print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
							print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
							gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
						ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
						if ans_array[0]==30: #INDECISION
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							flag_time_elapsed = 1
							gare.express_indecision('sono_il_seriale')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0
						print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
						flag_time_elapsed = 0
						if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
							last_ans_reached_flag_at_ans_num = 0
							if len(ans_array)==1:
								tot_res = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
								print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
							else:
								print("mi aspetto piu risp, ne ho solo 1")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
							if len(ans_array)==2:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									print("mm error 222")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									tot_res1 = res1 or res2 
									#check first ans
									res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									tot_res2 = res3 or res4
									print("both res_ottenute 2 in play_the_game con 2 risp da dare")
									last_ans_reached_flag_at_ans_num = 1
									tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<2:
								print("mi aspetto piu risposte, ne ho solo 2")
								continue
							elif len(ans_array)>2:
								print("mi aspetto meno risposte, ne ho ben 2")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
							if len(ans_array)==3:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 333 ")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									print("res")
									print(res1)
									print(res2)
									print(res3)
									tot_res1 = res1 or res2 or res3
									#check second ans
									res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
									tot_res2 = res4 or res5 or res6
									print("res")
									print(res1)
									print(res2)
									print(res3)
									#check third ans
									res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
									res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
									res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
									tot_res3 = res7 or res8 or res9
									print("res")
									print(res1)
									print(res2)
									print(res3)
									print("both res_ottenute 3 in play_the_game con 3 risp da dare")
									last_ans_reached_flag_at_ans_num = 2 
									tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<3:
								print("mi aspetto piu risposte, ne ho solo 3")
								continue
							elif len(ans_array)>3:
								print("mi aspetto meno risposte ne ho ben 3")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
							last_ans_reached_flag_at_ans_num = 3
							if len(ans_array)==4:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 4444444")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
									tot_res1 = res1 or res2 or res3 or res4
									#check second ans
									res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
									res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
									tot_res2 = res5 or res6 or res7 or res8
									#check third ans
									res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
									res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
									res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
									res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
									tot_res3 = res9 or res10 or res11 or res12
									#check fourth ans
									res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
									res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer2)
									res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
									res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
									tot_res4 = res13 or res14 or res15 or res16
									print("both res_ottenute 4 in play_the_game con 4 risp da dare")
									last_ans_reached_flag_at_ans_num = 3 
									tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<4:
								print("mi aspetto piu risposte ne ho solo 4 ")
								continue
							elif len(ans_array)>4:
								print("mi aspetto meno risposte ne ho ben 4")
								continue
						scon = one_game.results[q]
						scon.num_given_ans+=1
						if tot_res:
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							print("correct response:esatta")
							gare.right_answer('ser_right_resp')
							q+=1
						else:
							print("FALSE, wrong answer")
							gare.wrong_answer('false ser') 
							too_many_consecutive_wrong_ans+=1
							#start again from beginning with multiple answer , repeat both
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
							continue			
					elif one_game.this_type=='2LDSUOB' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='2LDSUOB'): #Tocca una scimmia se la luce è rossa altrimenti premi la mia testa
						#one is enough
						#non c'è turn, one as in enough even ifin one_game.ques_ans there are more
						#repeat question 
						if first_time_need_led:
							first_time_need_led = 0
							if repeat_question:
								first_time_need_led = 1
							#prepare MSG SERIAL
							print(one_game.current_game_colors)
							time.sleep(0.2)
							gare.start_audio(53)
							#if type of game serve il led === 2L
							led_msg_start_color = b'<<f>>'
							lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
							ch = random.choices(lisa,k = 1)
							resp_msg_from_arduino = ch[0]
							print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
							resp_msg_from_arduino = b'<q>'
							if one_game.need_light=='Red' and resp_msg_from_arduino==b'<q>':
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Green' and resp_msg_from_arduino==b'<gre>':
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Blue' and resp_msg_from_arduino==b'<x>':
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Yellow' and resp_msg_from_arduino==b'<y>':
								choose_ques_ans_to_use = 0
							else:
								choose_ques_ans_to_use_to_use = 1
							print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
							print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
							gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
						flag_time_elapsed = 0
						print("one_game.current_game_genres {}".format(one_game.current_game_genres))
						if choose_ques_ans_to_use==1: #contrary of case 2LDBOC
							ans_unique, wait_time = gare.wait_mpx('serial_msg_mpx')
							if ans_unique==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("supero il continue in 2LDSUOB")
							if ans_unique in [1,2,3,4]: 
								tot_res = 1
							if ans_unique==5: 
								tot_res = 0
						else:
							ans_unique, wait_time= gare.wait_answer(one_game.ques_ans_1[q].required_time)
							print('stampa response given {}'.format(ans_unique))
							print('stampa time  {}'.format(wait_time))
							if ans_unique==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans_1[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								tot_res = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3==5: #una sola response ma ci sono più possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								tot_res = res1 or res2 
								last_ans_reached_flag_at_ans_num = 1
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4==5: #una sola response ma ci sono 3 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer3)
								tot_res = res1 or res2 or res3
								last_ans_reached_flag_at_ans_num = 2 
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4=5: #una sola response ma ci sono 4 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer3)
								res4 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer4)
								tot_res = res1 or res2 or res3 or res4
								last_ans_reached_flag_at_ans_num = 3
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue			
					elif one_game.this_type=='2LST' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='2LST'): #Tocca il disegno del colore come questo rosso
						#repeat question 
						if first_time_need_led:
							first_time_need_led = 0
							if repeat_question:
								first_time_need_led = 1
							#prepare MSG SERIAL
							print(one_game.current_game_colors)
							time.sleep(0.2)
							gare.start_audio(53)
							led_msg_start_color = b'<<f>>'
							lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
							ch = random.choices(lisa,k = 1)
							resp_msg_from_arduino = ch[0]
							print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
							resp_msg_from_arduino = b'<q>'
							print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
							print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
							gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
						flag_time_elapsed = 0
						print("one_game.current_game_genres {}".format(one_game.current_game_genres))
						ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
						if ans_array[0]==30: #INDECISION
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							flag_time_elapsed = 1
							gare.express_indecision('sono_il_seriale')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0						
						print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
						flag_time_elapsed = 0
						if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
							last_ans_reached_flag_at_ans_num = 0
							if len(ans_array)==1:
								tot_res = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
								print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
							else:
								print("mi aspetto piu risp, ne ho solo 1")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
							if len(ans_array)==2:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 222")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									tot_res1 = res1 or res2 
									#check first ans
									res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									tot_res2 = res3 or res4
									print("both res_ottenute 2 in play_the_game con 2 risp da dare")
									last_ans_reached_flag_at_ans_num = 1
									tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<2:
								print("mi aspetto piu risposte, ne ho solo 2")
								continue
							elif len(ans_array)>2:
								print("mi aspetto meno risposte, ne ho ben 2")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
							if len(ans_array)==3:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 333 ")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									print("res")
									print(res1)
									print(res2)
									print(res3)
									tot_res1 = res1 or res2 or res3
									#check second ans
									res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
									tot_res2 = res4 or res5 or res6
									print("res")
									print(res1)
									print(res2)
									print(res3)
									#check third ans
									res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
									res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
									res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
									tot_res3 = res7 or res8 or res9
									print("res")
									print(res1)
									print(res2)
									print(res3)
									print("both res_ottenute 3 in play_the_game con 3 risp da dare")
									last_ans_reached_flag_at_ans_num = 2 
									tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<3:
								print("mi aspetto piu risposte, ne ho solo 3")
								continue
							elif len(ans_array)>3:
								print("mi aspetto meno risposte ne ho ben 3")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
							last_ans_reached_flag_at_ans_num = 3
							if len(ans_array)==4:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 4444444")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
									tot_res1 = res1 or res2 or res3 or res4
									#check second ans
									res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
									res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
									tot_res2 = res5 or res6 or res7 or res8
									#check third ans
									res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
									res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
									res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
									res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
									tot_res3 = res9 or res10 or res11 or res12
									#check fourth ans
									res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
									res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer2)
									res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
									res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
									tot_res4 = res13 or res14 or res15 or res16
									print("both res_ottenute 4 in play_the_game con 4 risp da dare")
									last_ans_reached_flag_at_ans_num = 3 
									tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<4:
								print("mi aspetto piu risposte ne ho solo 4 ")
								continue
							elif len(ans_array)>4:
								print("mi aspetto meno risposte ne ho ben 4")
								continue
						scon = one_game.results[q]
						scon.num_given_ans+=1
						if tot_res:
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							print("correct response:esatta")
							gare.right_answer('ser_right_resp')
							q+=1
						else:
							print("FALSE, wrong answer")
							gare.wrong_answer('false ser') 
							too_many_consecutive_wrong_ans+=1
							#start again from beginning with multiple answer , repeat both
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
							continue		
					elif one_game.this_type=='2LSU' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='2LSU'): #Tocca il leone del colore come questo rosso
						if first_time_need_led:
							first_time_need_led = 0
							if repeat_question:
								first_time_need_led = 1
							#prepare MSG SERIAL
							print(one_game.current_game_colors)
							time.sleep(0.2)
							gare.start_audio(53)
							#if type of game serve il led === 2L
							led_msg_start_color = b'<<f>>'
							lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
							ch = random.choices(lisa,k = 1)
							resp_msg_from_arduino = ch[0]
							print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
							resp_msg_from_arduino = b'<q>'
							print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
							print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
							gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
						flag_time_elapsed = 0
						ans_unique, wait_time= gare.wait_answer(one_game.ques_ans[q].required_time)
						print('stampa response given {}'.format(ans_unique))
						print('stampa time  {}'.format(wait_time))
						if ans_unique==30: #INDECISION
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							flag_time_elapsed = 1
							gare.express_indecision('sono_il_seriale')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0
						print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
						flag_time_elapsed = 0
						if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
							last_ans_reached_flag_at_ans_num = 0
							tot_res = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
							print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #una sola response ma ci sono più possibilità
							#check first ans
							res1 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
							res2 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer2)
							tot_res = res1 or res2 
							last_ans_reached_flag_at_ans_num = 1
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #una sola response ma ci sono 3 possibilità
							#check first ans
							res1 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
							res2 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer2)
							res3 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer3)
							tot_res = res1 or res2 or res3
							last_ans_reached_flag_at_ans_num = 2 
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #una sola response ma ci sono 4 possibilità
							#check first ans
							res1 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
							res2 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer2)
							res3 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer3)
							res4 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer4)
							tot_res = res1 or res2 or res3 or res4
							last_ans_reached_flag_at_ans_num = 3
						scon = one_game.results[q]
						scon.num_given_ans+=1
						if tot_res:
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							print("correct response:esatta")
							gare.right_answer('ser_right_resp')
							q+=1
						else:
							print("FALSE, wrong answer")
							gare.wrong_answer('false ser') 
							too_many_consecutive_wrong_ans+=1
							#start again from beginning with multiple answer , repeat both
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
							continue			
					elif one_game.this_type=='2LTWO' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='2LTWO'): #Tocca il disegno del colore uguale al primo colore che ti ho fatto vedere rosso
						#repeat question 
						if first_time_need_led:
							first_time_need_led = 0
							if repeat_question:
								first_time_need_led = 1
							#prepare MSG SERIAL
							print(one_game.current_game_colors)
							time.sleep(0.2)
							gare.start_audio(53)
							#if type of game serve il led === 2L
							led_msg_start_color1 = b'<q>'
							led_msg_start_color2 = b'<x>'
							lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
							ch = random.choices(lisa,k = 1)
							resp_msg_from_arduino = ch[0]
							print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
							resp_msg_from_arduino = b'<q>'
							print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
							print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
							gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
						flag_time_elapsed = 0
						print("one_game.current_game_genres {}".format(one_game.current_game_genres))
						ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
						if ans_array[0]==30: #INDECISION
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							flag_time_elapsed = 1
							gare.express_indecision('sono_il_seriale')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0						
						print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
						flag_time_elapsed = 0
						if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
							last_ans_reached_flag_at_ans_num = 0
							if len(ans_array)==1:
								tot_res = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
								print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
							else:
								print("mi aspetto piu risp, ne ho solo 1")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
							if len(ans_array)==2:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 222")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									tot_res1 = res1 or res2 
									#check first ans
									res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									tot_res2 = res3 or res4
									print("both res_ottenute 2 in play_the_game con 2 risp da dare")
									last_ans_reached_flag_at_ans_num = 1
									tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<2:
								print("mi aspetto piu risposte, ne ho solo 2")
								continue
							elif len(ans_array)>2:
								print("mi aspetto meno risposte, ne ho ben 2")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
							if len(ans_array)==3:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 333 ")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									print("res")
									print(res1)
									print(res2)
									print(res3)
									tot_res1 = res1 or res2 or res3
									#check second ans
									res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
									tot_res2 = res4 or res5 or res6
									print("res")
									print(res1)
									print(res2)
									print(res3)
									#check third ans
									res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
									res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
									res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
									tot_res3 = res7 or res8 or res9
									print("res")
									print(res1)
									print(res2)
									print(res3)
									print("both res_ottenute 3 in play_the_game con 3 risp da dare")
									last_ans_reached_flag_at_ans_num = 2 
									tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<3:
								print("mi aspetto piu risposte, ne ho solo 3")
								continue
							elif len(ans_array)>3:
								print("mi aspetto meno risposte ne ho ben 3")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
							last_ans_reached_flag_at_ans_num = 3
							if len(ans_array)==4:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 4444444")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
									tot_res1 = res1 or res2 or res3 or res4
									#check second ans
									res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
									res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
									tot_res2 = res5 or res6 or res7 or res8
									#check third ans
									res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
									res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
									res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
									res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
									tot_res3 = res9 or res10 or res11 or res12
									#check fourth ans
									res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
									res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer2)
									res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
									res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
									tot_res4 = res13 or res14 or res15 or res16
									print("both res_ottenute 4 in play_the_game con 4 risp da dare")
									last_ans_reached_flag_at_ans_num = 3 
									tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<4:
								print("mi aspetto piu risposte ne ho solo 4 ")
								continue
							elif len(ans_array)>4:
								print("mi aspetto meno risposte ne ho ben 4")
								continue
						scon = one_game.results[q]
						scon.num_given_ans+=1
						if tot_res:
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							print("correct response:esatta")
							gare.right_answer('ser_right_resp')
							q+=1
						else:
							print("FALSE, wrong answer")
							gare.wrong_answer('false ser') 
							too_many_consecutive_wrong_ans+=1
							#start again from beginning with multiple answer , repeat both
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
							continue		
					elif one_game.this_type=='2LDSU2' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='2LDSU2'): #Tocca una scimmia se divento rosso oppure tocca un leone se divento giallo
						#one is enough
						#repeat question 
						if first_time_need_led:
							first_time_need_led = 0
							if repeat_question:
								first_time_need_led = 1
							#prepare MSG SERIAL
							print(one_game.current_game_colors)
							time.sleep(0.2)
							gare.start_audio(53)
							#if type of game serve il led === 2L
							led_msg_start_color = b'<celeb>'
							choose_ques_ans_to_use = 1 #uso tabella ques_ans1 no one_game.ques_ans
							lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
							ch = random.choices(lisa,k = 1)
							resp_msg_from_arduino = ch[0]
							print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
							resp_msg_from_arduino = b'<q>'
							if one_game.need_light=='Red' and resp_msg_from_arduino==b'<q>':
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Green' and resp_msg_from_arduino==b'<gre>':
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Blue' and resp_msg_from_arduino==b'<x>':
								choose_ques_ans_to_use = 0
							if one_game.need_light=='Yellow' and resp_msg_from_arduino==b'<y>':
								choose_ques_ans_to_use = 0
							if one_game.need_light_1=='Red' and resp_msg_from_arduino==b'<q>':
								choose_ques_ans_to_use = 1
							if one_game.need_light_1=='Green' and resp_msg_from_arduino==b'<gre>':
								choose_ques_ans_to_use = 1
							if one_game.need_light_1=='Blue' and resp_msg_from_arduino==b'<x>':
								choose_ques_ans_to_use = 1
							if one_game.need_light_1=='Yellow' and resp_msg_from_arduino==b'<y>':
								choose_ques_ans_to_use = 1						
							print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
							gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
						ans_unique, wait_time= gare.wait_answer(one_game.ques_ans[q].required_time)
						print('stampa response given {}'.format(ans_unique))
						print('stampa time  {}'.format(wait_time))
						if ans_unique==30: 
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							print("^^^^^^^ siamo in INDECISION")
							flag_time_elapsed = 1
							gare.express_indecision('serr_indec')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0
						print("ho superato il continue in 2LDSUOC")
						flag_time_elapsed = 0
						print("one_game.current_game_genres {}".format(one_game.current_game_genres))
						if choose_ques_ans_to_use==0:
							if ans_unique==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								tot_res = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
								print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #una sola response ma ci sono più possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer2)
								tot_res = res1 or res2 
								last_ans_reached_flag_at_ans_num = 1
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #una sola response ma ci sono 3 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer3)
								tot_res = res1 or res2 or res3
								last_ans_reached_flag_at_ans_num = 2 
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #una sola response ma ci sono 4 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer3)
								res4 = gaca.check_correctness(ans_unique,one_game.ques_ans[q].answer4)
								tot_res = res1 or res2 or res3 or res4
								last_ans_reached_flag_at_ans_num = 3
							scon = one_game.results[q]
							scon.num_given_ans+=1						
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue 
						else:
							if ans_unique==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans_1[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								tot_res = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3==5: #una sola response ma ci sono più possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								tot_res = res1 or res2 
								last_ans_reached_flag_at_ans_num = 1
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4==5: #una sola response ma ci sono 3 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer3)
								tot_res = res1 or res2 or res3
								last_ans_reached_flag_at_ans_num = 2 
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4=5: #una sola response ma ci sono 4 possibilità
								#check first ans
								res1 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer1)
								res2 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer2)
								res3 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer3)
								res4 = gaca.check_correctness(ans_unique,one_game.ques_ans_1[q].answer4)
								tot_res = res1 or res2 or res3 or res4
								last_ans_reached_flag_at_ans_num = 3
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue
				if this_momentaneous_kind=='3S' or one_game.this_kind=='3S':
					if one_game.this_type=='3SSU' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='3SSU'):
						if ans_array[0]==30: 
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							flag_time_elapsed = 1
							gare.express_indecision('sono_il_seriale')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0
						print("ho superato il continue in 3SSU")
						if one_game.ques_ans[q].answer2==5:
							print("mm error qui non pouo esserci solo una response in 3ssu tipo")
						if one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
							if len(ans_array)==2: 
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									tot_res1 = res1 or res2 
									#check sec ans
									res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									tot_res2 = res3 or res4
									print("both res_ottenute 2 in play_the_game con 2 risp da dare")
									last_ans_reached_flag_at_ans_num = 1
									tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<2:
								print("mi aspetto piu risposte")
								continue
							elif len(ans_array)>2:
								print("mi aspetto meno risposte")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
							if len(ans_array)==3:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 333 ")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									if one_game.limitLion==3 or one_game.limitMonkey==3 or one_game.limitParrot==3 or one_game.limitOctopus==3:
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										tot_res1 = res1 or res2 or res3
										#check second ans
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										tot_res2 = res4 or res5 or res6
										#check third ans
										res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
										res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										tot_res3 = res7 or res8 or res9
										last_ans_reached_flag_at_ans_num = 2
										tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<3:
								print("mi aspetto piu risposte, ne ho solo 3")
								continue
							elif len(ans_array)>3:
								print("mi aspetto meno risposte ne ho ben 3")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
							if len(ans_array)==3:
								print("erore mene aspettavo piu una")
								continue
							if len(ans_array)==2: #due need inside turni 
								strange_case_3s = 1
								last_ans_reached_flag_at_ans_num = 3
								if one_game.limitLion==2 and lion_done3s==0:
									#one_game.limit work cause sono sempre messi in ordine in one_game.ques_ans  partendo dai leoni ..solito ordine s,m,p,o
									#check first ans
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
													if res2==1:
														tot_res = 1
									lion_done3s = 1
								if one_game.limitMonkey==2 and one_game.limitLion==0 and monkey_done3s==0: #stannno nelle prime due risposte... non c'è leone
									#check first ans
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
													if res2==1:
														tot_res = 1
									monkey_done3s = 1
								elif one_game.limitMonkey==2 and one_game.limitLion==1 and monkey_done3s==0: #leone c'è prima
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
													if res2==1:
														tot_res = 1
									monkey_done3s = 1
								if one_game.limitParrot==2 and (one_game.limitLion==0 and one_game.limitMonkey==0) and parrot_done3s==0: 
									#check first ans
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
													if res2==1:
														tot_res = 1
									parrot_done3s = 1
								elif one_game.limitParrot==2 and (one_game.limitLion==1 or one_game.limitMonkey==1) and parrot_done3s==0: 
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
													if res2==1:
														tot_res = 1
									parrot_done3s = 1
								if one_game.limitOctopus==2 and (one_game.limitLion==0 and one_game.limitMonkey==0 and one_game.limitParrot==0) and octopus_done3s==0:
									#check first ans
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
													if res2==1:
														tot_res = 1
									octopus_done3s = 1																																														
								elif one_game.limitOctopus==2 and (one_game.limitLion==1 or one_game.limitMonkey==1 or one_game.limitParrot==1) and octopus_done3s==0:
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
													if res2==1:
														tot_res = 1
									octopus_done3s = 1
							if len(ans_array)==4:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									if one_game.limitLion==4 or one_game.limitMonkey==4 or one_game.limitParrot==4 or one_game.limitOctopus==4:	
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										tot_res1 = res1 or res2 or res3 or res4
										#check second ans
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										tot_res2 = res5 or res6 or res7 or res8
										#check third ans
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
										res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
										res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
										tot_res3 = res9 or res10 or res11 or res12
										#check fourth ans
										res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
										res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer2)
										res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
										res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
										tot_res4 = res13 or res14 or res15 or res16
										print("both res_ottenute 4 in play_the_game con 4 risp da dare")
										last_ans_reached_flag_at_ans_num = 3 
										tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
							elif len(ans_array)>4:
								print("mi aspetto meno risposte 4")
								continue			
						if strange_case_3s:
							print("TRUE, CORRECT answer")
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							gare.right_answer('right_ans fakeme')			
							print("ripasso ancora provo AGAIN 3ssu ci voglion i risultati2  ERRORS{}".format(one_game.results[q].num_errors))
							print("ripasso ancora provo AGAIN 3ssu ci voglion i risultati2 CORRECT{}".format(one_game.results[q].num_corrects))
							print("ripasso ancora provo AGAIN 3ssu ci voglion i risultati2 INDECISION{}".format(one_game.results[q].num_indecisions))
							if miniround_3s==0:
								print("ok adesso ce un altra coppia rimasta")
								gare.start_audio(232) #ne manca ancora una coppia
								miniround_3s = 1
								continue			
							elif miniround_3s==1:
								print("ok hai finito di darmi le risp")
								gare.start_audio(255) #ne manca ancora una coppia
								miniround_3s = 0
								strange_case_3s = 0
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								#reaction start here good boy
								gare.right_answer('right serìal')						
								q+=1
						else:
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res: #res =true 
								print("TRUE, CORRECT answer")
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								#reaction start here good boy
								gare.right_answer('right serìal')			
								print("ripasso ancora provo AGAIN ci voglion i risultati2  ERRORS{}".format(one_game.results[q].num_errors))
								print("ripasso ancora provo AGAIN ci voglion i risultati2 CORRECT{}".format(one_game.results[q].num_corrects))
								print("ripasso ancora provo AGAIN ci voglion i risultati2 INDECISION{}".format(one_game.results[q].num_indecisions))
								q+=1
							else: 
								print("FALSE, wrong answer")
								# start bad reaction
								gare.wrong_answer('ser wro wro')
								#avro due audio diversi quindi proviamo
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee {}".format(one_game.results[q].num_errors))
								continue
					elif one_game.this_type=='3SCO' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='3SCO'):
						if ans_array[0]==30: 
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							flag_time_elapsed = 1
							gare.express_indecision('sono_il_seriale')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0						
						print("ho superato il continue in 3Sco")
						if one_game.ques_ans[q].answer2==5:
							print("mm error qui non pouo esserci solo una response in 3sco")
						if one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
							if len(ans_array)==2:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 222")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
									tot_res1 = res1 or res2 
									#check first ans
									res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
									res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									tot_res2 = res3 or res4
									print("both res_ottenute 2 in play_the_game con 2 risp da dare")
									last_ans_reached_flag_at_ans_num = 1
									tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<2:
								print("mi aspetto piu risposte, ne ho solo 2")
								continue
							elif len(ans_array)>2:
								print("mi aspetto meno risposte, ne ho ben 2")
								continue							
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
							if len(ans_array)==3:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 333 ")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									if one_game.limitRed==3 or one_game.limitGreen==3 or one_game.limitBlue==3 or one_game.limitYellow==3:
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										tot_res1 = res1 or res2 or res3
										#check second ans
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										tot_res2 = res4 or res5 or res6
										#check third ans
										res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
										res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										tot_res3 = res7 or res8 or res9
										last_ans_reached_flag_at_ans_num = 2
										tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
							if len(ans_array)<3:
								print("mi aspetto piu risposte, ne ho solo 3")
								continue
							elif len(ans_array)>3:
								print("mi aspetto meno risposte ne ho ben 3")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
							last_ans_reached_flag_at_ans_num = 3
							if len(ans_array)==3:
								print("erore mene aspettavo piu, ne manca una")
								gare.start_audio(111) #ne manca 1 
								continue
							if len(ans_array)==2: #due turni 
								if one_game.limitRed==2 and red_done3s==0: #one_game.limit work cause sono sempre messi in ordine in one_game.ques_ans  partendo dai leoni ..solito ordine s,m,p,o
									strange_case_3s = 1
									#check first ans
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
													if res2==1:
														tot_res = 1
									red_done3s = 1
								if one_game.limitGreen==2 and one_game.limitRed==0 and green_done3s==0: #stannno nelle prime due risposte... non c'è leone
									#check first ans
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
													if res2==1:
														tot_res = 1
									green_done3s = 1
								elif one_game.limitGreen==2 and one_game.limitRed==1 and green_done3s==0: #leone c'è prima
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
													if res2==1:
														tot_res = 1
									green_done3s = 1
								if one_game.limitBlue==2 and (one_game.limitRed==0 and one_game.limitGreen==0) and blue_done3s==0: 
									#check first ans
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
													if res2==1:
														tot_res = 1
									blue_done3s = 1
								elif one_game.limitBlue==2 and (one_game.limitRed==1 or one_game.limitGreen==1) and blue_done3s==0: 
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
													if res2==1:
														tot_res = 1
									blue_done3s = 1
								if one_game.limitYellow==2 and (one_game.limitRed==0 and one_game.limitGreen==0 and one_game.limitBlue==0) and yellow_done3s==0:
									#check first ans
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
													if res2==1:
														tot_res = 1
									yellow_done3s = 1																																														
								elif one_game.limitYellow==2 and (one_game.limitRed==1 or one_game.limitGreen==1 or one_game.limitBlue==1) and yellow_done3s==0:
									tot_res = 0
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
									if res1==1:
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										if res2==1:
											tot_res = 1
									if tot_res==0:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										if res1==1:
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											if res2==1:
												tot_res = 1
										if tot_res==0:
											res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
											if res1==1:
												res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
												if res2==1:
													tot_res = 1
											if tot_res==0:
												res1 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
												if res1==1:
													res2 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
													if res2==1:
														tot_res = 1
									yellow_done3s = 1
							if len(ans_array)==4:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									if one_game.limitRed==4 or one_game.limitGreen==4 or one_game.limitBlue==4 or one_game.limitYellow==4:	
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										tot_res1 = res1 or res2 or res3 or res4
										#check second ans
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										tot_res2 = res5 or res6 or res7 or res8
										#check third ans
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
										res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
										res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
										tot_res3 = res9 or res10 or res11 or res12
										#check fourth ans
										res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
										res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer2)
										res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
										res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
										tot_res4 = res13 or res14 or res15 or res16
										print("both res_ottenute 4 in play_the_game con 4 risp da dare")
										last_ans_reached_flag_at_ans_num = 3 
										tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
							elif len(ans_array)>4:
								print("mai 5 rispsoste sono ammesse mi aspetto meno risposte")
								continue		
						if strange_case_3s:
							print("TRUE, CORRECT answer")
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							gare.right_answer('right_ans fakeme')			
							print("ripasso ancora provo AGAIN 3ssu ci voglion i risultati2  ERRORS{}".format(one_game.results[q].num_errors))
							print("ripasso ancora provo AGAIN 3ssu ci voglion i risultati2 CORRECT{}".format(one_game.results[q].num_corrects))
							print("ripasso ancora provo AGAIN 3ssu ci voglion i risultati2 INDECISION{}".format(one_game.results[q].num_indecisions))
							if miniround_3s==0:
								print("ok adesso ce un altra coppia rimasta")
								gare.start_audio(232) #ne manca ancora una coppia
								miniround_3s = 1
								continue			
							elif miniround_3s==1:
								print("ok hai finito di darmi le risp")
								gare.start_audio(255) #ne manca ancora una coppia
								miniround_3s = 0
								strange_case_3s = 0
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								#reaction start here good boy
								gare.right_answer('right serìal')
								q+=1
						else:
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res: #res =true 
								print("TRUE, CORRECT answer")
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								#reaction start here good boy
								gare.right_answer('right serìal')			
								print("ripasso ancora provo AGAIN ci voglion i risultati2  ERRORS{}".format(one_game.results[q].num_errors))
								print("ripasso ancora provo AGAIN ci voglion i risultati2 CORRECT{}".format(one_game.results[q].num_corrects))
								print("ripasso ancora provo AGAIN ci voglion i risultati2 INDECISION{}".format(one_game.results[q].num_indecisions))
								q+=1
							else: #res = false
								print("FALSE, wrong answer")
								# start bad reaction
								gare.wrong_answer('ser wro wro')
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee {}".format(one_game.results[q].num_errors))
								continue
				if this_momentaneous_kind=='6I' or one_game.this_kind=='6I' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='6I'):
					######################################################################################################################## useless stuff
					print("CLASSICO STANDARD il genere del gioco in questione è --> {}".format(one_game.this_kind))
					print("round round NUMERO {}".format(q))
					#0 SETTO SCORE OBJ RESULTS
					#print("ennerandom definitivo  è uguale a {}".format(ennerandom))
					print("ECCO SELF ALLOCATED of question {}".format(q))
					print(one_game.ques_ans[q].answer1)
					print(one_game.ques_ans[q].answer2)
					print(one_game.ques_ans[q].answer3)
					print(one_game.ques_ans[q].answer4)
					print()
					#RIFARE O TOGLIERE 
					#one_game.results[q].set_complexity(one_game.ques_ans[q].complexity)
					##metto game complex
					#one_game.results[q].set_category(0)
					#assegnamenti a caso per test che vanno ok
					sco = one_game.results[q]
					sco.complexity_sess = 10
					sco.category = 20
					sco.difficulty_match = 30
					sco.difficulty_game = 40
					#onon vanno questi NB metodo no
					#one_game.sco.set_complexity(0) #game_dificulty
					#one_game.sco.set_category(0) #game_category???? cos'era Animal/food/things??
					#one_game.sco.set_difficulty_match(0)
					#one_game.sco.set_difficulty_of_game(0)
					print("LENGHT results is {}".format(len(one_game.results)))
					print("one_game.results[q].complexity_sess {}".format(one_game.results[q].complexity_sess))
					print("one_game.results[q].difficulty_match{}".format(one_game.results[q].difficulty_match))
					print("one_game.results[q].difficulty_game {}".format(one_game.results[q].difficulty_game))
					print("one_game.results[q].category {}".format(one_game.results[q].category))
					print("num_indecisions {}".format(one_game.results[q].num_indecisions))
					print("CONTROLLER {}".format(one_game.ques_ans_1.answer1))
					######################################################################################################################## useless stuff			
					ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
					print("ans_array_ricevuto USE_turns = 0 {}".format(ans_array))
					if ans_array[0]==30: #INDECISION
						too_many_consecutive_indecisions+=1
						too_many_consecutive_wrong_ans = 0
						flag_time_elapsed = 1
						gare.express_indecision('sono_il_seriale')
						take_sco = one_game.results[q]
						take_sco.num_indecisions+=1
						print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
						continue
					too_many_consecutive_indecisions = 0
					print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
					flag_time_elapsed = 0
					if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
						last_ans_reached_flag_at_ans_num = 0
						if len(ans_array)==1:
							print("how was the limit? LIMIT = {} ".format(one_game.limit))
							print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
							tot_res = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
							print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
						else:
							print("mi aspetto piu risp, ne ho solo 1")
							continue
					elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
						if len(ans_array)==2:
							duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
							if len(duplic_array):
								#redo response given more times
								print("mm error 222")
								print("duplicates are {}".format(duplic_array))
								print("same ans given more time")
								continue
							else:
								print("how was the limit? LIMIT = {} ".format(one_game.limit))
								print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
								#check first ans
								res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
								res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
								tot_res1 = res1 or res2 
								#check first ans
								res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
								res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
								tot_res2 = res3 or res4
								print("both res_ottenute 2 in play_the_game con 2 risp da dare")
								last_ans_reached_flag_at_ans_num = 1
								tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
						if len(ans_array)<2:
							print("mi aspetto piu risposte, ne ho solo 2")
							continue
						elif len(ans_array)>2:
							print("mi aspetto meno risposte, ne ho ben 2")
							continue
					elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
						if len(ans_array)==3:
							duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
							if len(duplic_array):
								#redo response given more times
								print("mm error 333 ")
								print("duplicates are {}".format(duplic_array))
								print("same ans given more time")
								continue
							else:
								print("how was the limit? LIMIT = {} ".format(one_game.limit))
								print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
								#check first ans
								res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
								res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
								res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
								print("res")
								print(res1)
								print(res2)
								print(res3)
								tot_res1 = res1 or res2 or res3
								#check second ans
								res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
								res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
								res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
								tot_res2 = res4 or res5 or res6
								print("res")
								print(res1)
								print(res2)
								print(res3)
								#check third ans
								res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
								res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
								res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
								tot_res3 = res7 or res8 or res9
								print("res")
								print(res1)
								print(res2)
								print(res3)
								print("both res_ottenute 3 in play_the_game con 3 risp da dare")
								last_ans_reached_flag_at_ans_num = 2 
								tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
						if len(ans_array)<3:
							print("mi aspetto piu risposte, ne ho solo 3")
							continue
						elif len(ans_array)>3:
							print("mi aspetto meno risposte ne ho ben 3")
							continue
					elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
						last_ans_reached_flag_at_ans_num = 3
						if len(ans_array)==4:
							duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
							if len(duplic_array):
								#redo response given more times
								print("mm error 4444444")
								print("duplicates are {}".format(duplic_array))
								print("same ans given more time")
								continue
							else:
								print("how was the limit? LIMIT = {} ".format(one_game.limit))
								print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
								#check first ans
								res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
								res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
								res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
								res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
								tot_res1 = res1 or res2 or res3 or res4
								#check second ans
								res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
								res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
								res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
								res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
								tot_res2 = res5 or res6 or res7 or res8
								#check third ans
								res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
								res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
								res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
								res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
								tot_res3 = res9 or res10 or res11 or res12
								#check fourth ans
								res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
								res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer2)
								res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
								res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
								tot_res4 = res13 or res14 or res15 or res16
								print("both res_ottenute 4 in play_the_game con 4 risp da dare")
								last_ans_reached_flag_at_ans_num = 3 
								tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
						if len(ans_array)<4:
							print("mi aspetto piu risposte ne ho solo 4 ")
							continue
						elif len(ans_array)>4:
							print("mi aspetto meno risposte ne ho ben 5")
							continue
					scon = one_game.results[q]
					scon.num_given_ans+=1
					if tot_res:
						scoc = one_game.results[q]
						scoc.num_corrects+=1
						too_many_consecutive_wrong_ans = 0
						print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
						print("correct response:esatta")
						gare.right_answer('ser_right_resp')
						q+=1
					else:
						print("FALSE, SO HAVE A LOOK ON ANS1 PERCHE LA response PROTREBBE PRVENIRE DAL LI")
						print("FALSE, wrong answer")
						print("CONTROLLER CONTROLLERCONTROLLER CONTROLLER CONTROLLER CONTROLLER CONTROLLER  {}".format(one_game.ques_ans_1.answer1))
						print("CONTROLLER CONTROLLERCONTROLLER CONTROLLER CONTROLLER CONTROLLER CONTROLLER  {}".format(one_game.ques_ans_1.answer2))
						print("CONTROLLER CONTROLLERCONTROLLER CONTROLLER CONTROLLER CONTROLLER CONTROLLER  {}".format(one_game.ques_ans_1.answer3))
						print("CONTROLLER CONTROLLERCONTROLLER CONTROLLER CONTROLLER CONTROLLER CONTROLLER  {}".format(one_game.ques_ans_1.answer4))
						if len(one_game.ques_ans_1.answer1 in [1,2,3,4]):
							print("ENTRO NEL CONTROLLO ")
							if one_game.ques_ans_1[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								if len(ans_array)==1:
									print("how was the limit? LIMIT = {} ".format(one_game.limit))
									print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
									tot_res = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer1)
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								else:
									print("mi aspetto piu risp, ne ho solo 1")
									continue
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3==5: #mi aspetto 2 risposte
								if len(ans_array)==2:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 222")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer2)
										tot_res1 = res1 or res2 
										#check first ans
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer1)
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer2)
										tot_res2 = res3 or res4
										print("both res_ottenute 2 in play_the_game con 2 risp da dare")
										last_ans_reached_flag_at_ans_num = 1
										tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<2:
									print("mi aspetto piu risposte, ne ho solo 2")
									continue
								elif len(ans_array)>2:
									print("mi aspetto meno risposte, ne ho ben 2")
									continue
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4==5: #mi aspetto 3 risposte
								if len(ans_array)==3:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 333 ")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer3)
										print("res")
										print(res1)
										print(res2)
										print(res3)
										tot_res1 = res1 or res2 or res3
										#check second ans
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer1)
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer2)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer3)
										tot_res2 = res4 or res5 or res6
										print("res")
										print(res1)
										print(res2)
										print(res3)
										#check third ans
										res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer1)
										res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer2)
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer3)
										tot_res3 = res7 or res8 or res9
										print("res")
										print(res1)
										print(res2)
										print(res3)
										print("both res_ottenute 3 in play_the_game con 3 risp da dare")
										last_ans_reached_flag_at_ans_num = 2 
										tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<3:
									print("mi aspetto piu risposte, ne ho solo 3")
									continue
								elif len(ans_array)>3:
									print("mi aspetto meno risposte ne ho ben 3")
									continue
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4=5: #mi aspetto 4 risposte
								last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)==4:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 4444444")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer3)
										res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer4)
										tot_res1 = res1 or res2 or res3 or res4
										#check second ans
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer1)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer2)
										res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer3)
										res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer4)
										tot_res2 = res5 or res6 or res7 or res8
										#check third ans
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer1)
										res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer2)
										res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer3)
										res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer4)
										tot_res3 = res9 or res10 or res11 or res12
										#check fourth ans
										res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans_1[q].answer1)
										res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans_1[q].answer2)
										res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans_1[q].answer3)
										res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans_1[q].answer4)
										tot_res4 = res13 or res14 or res15 or res16
										print("both res_ottenute 4 in play_the_game con 4 risp da dare")
										last_ans_reached_flag_at_ans_num = 3 
										tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<4:
									print("mi aspetto piu risposte ne ho solo 4 ")
									continue
								elif len(ans_array)>4:
									print("mi aspetto meno risposte ne ho ben 5")
									continue					
							scon = one_game.results[q]
							scon.num_given_ans+=1						
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								gare.wrong_answer('false ser')
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue
						else:
							gare.wrong_answer('false ser')
							#start again from beginning with multiple answer , repeat both
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
							continue
				if this_momentaneous_kind=='7O' or one_game.this_kind=='7O':
					if one_game.this_type=='7OL2': #Tocca per primo i disegni di questo colore poi tocca per ultimo i disegni di questo colore rosso verde show led
						if use_turns==0:
							if first_time_need_led:
								first_time_need_led = 0
								if repeat_question:
									first_time_need_led = 1
								#repeat question puo esserci anche 
								#prepare MSG SERIAL
								print(one_game.current_game_colors)
								time.sleep(0.2)
								gare.start_audio(53)
								#if type of game serve il led === 2L
								led_msg_start_color = b'<touch3>' 
								lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
								ch = random.choices(lisa,k = 1)
								resp_msg_from_arduino = ch[0]
								print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
								#resp_msg_from_arduino = b'<q>'
								print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
								print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
								gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
							ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
							if ans_array[0]==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0							
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
								print("mm error ans2 == 5 mai possibile qui non entrerò mai")
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
								if len(ans_array)==2:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 222")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										if one_game.limit==1 and one_game.limit1==1: #unica possibilità, avro sempre 2 risposte almeno qui
											#se ci sono i turni senza res2 che scompare
											res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											#this time is ans2 not ans1
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										tot_res = res1 or res2
										print("both res_ottenute 2 in play_the_game con 2 risp da dare")
										last_ans_reached_flag_at_ans_num = 1
								if len(ans_array)<2:
									print("mi aspetto piu risposte, ne ho solo 2")
									continue
								elif len(ans_array)>2:
									print("mi aspetto meno risposte, ne ho ben 2")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
								if len(ans_array)==3:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 333 ")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										if one_game.limit==2 and one_game.limit1==1:
											#se ci sono i turni senza res3 che scompare
											res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
											res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											tot_res_tmp = res1 or res2
											tot_res = tot_res_tmp or res3
										elif one_game.limit==1 and one_game.limit1==2:
											res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
											res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											tot_res_tmp = res2 or res3
											tot_res = tot_res_tmp or res1
											print("single res_ottenuta 1 in play_the_game con 2 risp da dare {}".format(res1))
											print("single res_ottenuta 2 in play_the_game con 2 risp da dare {}".format(res2))
											print("single res_ottenuta 3 in play_the_game con 2 risp da dare {}".format(res3))
								if len(ans_array)<3:
									print("mi aspetto piu risposte, ne ho solo 3")
									continue
								elif len(ans_array)>3:
									print("mi aspetto meno risposte ne ho ben 3")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
								last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)==4:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 4444444")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										if one_game.limit==1 and one_game.limit1==3:
											res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
											res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
											tot_res_tmp = res2 or res3 or res4
											tot_res = tot_res_tmp or res1
										if one_game.limit==3 and one_game.limit1==1:
											res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
											res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
											res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
											tot_res_tmp = res1 or res2 or res3
											tot_res = tot_res_tmp or res4
										if one_game.limit==2 and one_game.limit1==2:
											res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
											res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
											tot_res_tmp_1 = res1 or res2
											tot_res_tmp_2 = res3 or res4
											tot_res = tot_res_tmp_1 or tot_res_tmp_2
										last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)<4:
									print("mi aspetto piu risposte ne ho solo 4 ")
									continue
								elif len(ans_array)>4:
									print("mi aspetto meno risposte ne ho ben 4")
									continue
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue	
						else:
							if first_time_need_led:
								first_time_need_led = 0
								if repeat_question:
									first_time_need_led = 1
								#prepare MSG SERIAL
								#print("turn zero")
								print(one_game.current_game_colors)
								time.sleep(0.2)
								gare.start_audio(53)
								#if type of game serve il led === 2L
								#msg different da colors patch that are in the scenario, depends on the moment
								led_msg_start_color = b'<touch3>' 
								lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
								ch = random.choices(lisa,k = 1)
								resp_msg_from_arduino = ch[0]
								print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
								#resp_msg_from_arduino = b'<q>'
								print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
								print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
								#faccio audio diversi a seconda 
								if first_time_need_led:
									ser='messaggio_seriale_a_caso'
									gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
								else:
									ser='messaggio_seriale_a_caso'
									gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
								#mi aspetto una response per volta e poi dico ok ancora una e faccio i turni
								#append to already one at the time
							print("    round{}".format(q))
							if turn==0: #question zero prima che a sua volta a n risposte 
								print("-->siamo in turno zero")
								time.sleep(0.2)
								ans1, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time) 
								#ans1, wait_time = 1,5 #ESEMPI A CASO
								print('stampa response given {}'.format(ans1))
								print('stampa time  {}'.format(wait_time))
								if ans1==30: 
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									print("^^^^^^^ siamo in INDECISION")
									flag_time_elapsed = 1
									gare.express_indecision('messaggio_indecisione')
									continue
								too_many_consecutive_indecisions = 0
								print("ho superato il continue con i turni")
								flag_time_elapsed = 1
								if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
									tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									last_ans_reached_flag_at_ans_num = 0
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #2 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 1
									res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
									print("single res_ottenuta 1 in play_the_game con 2 risp da dare {}".format(res1))
									print("single res_ottenuta 2 in play_the_game con 2 risp da dare {}".format(res2))
									tot_res = res1 or res2 
									print("single res_ottenuta in play_the_game con 2 da dare OR {}".format(tot_res))
									if res1:
										already_given_resp.append(one_game.ques_ans[q].answer1)
									if res2:
										already_given_resp.append(one_game.ques_ans[q].answer2)
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #3 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 2
									if one_game.limit==1:
										tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										print("single res_ottenuta in play_the_game con 3 da dare OR {}".format(tot_res))
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer1)
									if one_game.limit==2:
										res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
										tot_res = res1 or res2
										print("single res_ottenuta in play_the_game con 3 da dare OR {}".format(tot_res))
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #4 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 3
									if one_game.limit==1:
										tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer1)
									if one_game.limit==2:
										res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
										tot_res = res1 or res2
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)
									if one_game.limit==3:
										res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)					
										res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)					
										res3 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer3)	
										tot_res = res1 or res2 or res3
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('rispesatta')
									turn+=1
									scoc = one_game.results[q]
									scoc.num_corrects+=1								
									if one_game.ques_ans[q].answer2==5: #NEXT STEP non esiste domdanda finita annullo anche gli already
										gare.start_audio(20) 
										q+=1
										turn = 0
										already_given_resp=[]
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('wrong') 
									#start again from beginning with multiple answer , repeat both
									turn = 0
									already_given_resp=[] #solo in turno zero
									continue								
							if turn==1:
								print("-->siamo in turno uno")
								print("secondo round question multipla")
								ans2, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans2, wait_time = 2,5 #ESEMPI A CASO							
								print('stampa response given {}'.format(ans2))
								print('stampa time  {}'.format(wait_time))
								if ans2==30:
									print("indecision")
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									#reactrion indecision start here
									gare.express_indecision('finto_ser_indecisione')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn1")
								if ans2 in already_given_resp or ans2 in already_given_resp:
									print("false already given")
									#reazione already start here
									scon = one_game.results[q]
									scon.num_given_ans+=1
									gare.already_given_answer('finto_ser_already_given')
									continue #for now reapeat from the last right answer
								else:
									if one_game.ques_ans[q].answer3==5:
										last_ans_reached_flag = 1
										tot_res = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
									elif one_game.ques_ans[q].answer3=5 and  one_game.ques_ans[q].answer4==5:
										last_ans_reached_flag = 2
										if one_game.limit1==1:
											tot_res = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
											if tot_res:
												already_given_resp.append(one_game.ques_ans[q].answer3)
										if one_game.limit1==2:
											res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
											res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
											tot_res = res1 or res2
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
									elif one_game.ques_ans[q].answer3=5 and  one_game.ques_ans[q].answer4=5:
										last_ans_reached_flag = 3
										if one_game.limit1==1:
											tot_res = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
											if tot_res:
												already_given_resp.append(one_game.ques_ans[q].answer4)
										if one_game.limit1==2:
											res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
											res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer4)
											tot_res = res1 or res2
										if one_game.limit1==3:
											res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
											res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
											res3 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
											tot_res = res1 or res2 or res3
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res3:
												already_given_resp.append(one_game.ques_ans[q].answer4)
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									#reaction start here good boy
									gare.right_answer('finto_ser_right_Ans')			
									turn+=1
									scoc = one_game.results[q]
									scoc.num_corrects+=1
									if one_game.ques_ans[q].answer3==5: #NEXT STEP
										gare.start_audio(20) 
										q+=1
										turn = 0 #reset turn
										already_given_resp=[]
									else:
										continue
								else: 
									print("FALSE, wrong answer")	
									# start bad reaction
									gare.wrong_answer('finto_Ser_wrong') #start again from beginning with multiple answer , repeat both
									#avro due audio diversi quindi proviamo
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							if turn==2:
								print("-->siamo in turno tre")
								print("terzo round question multipla")
								ans3, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans3, wait_time = 3,5
								print('stampa response given {}'.format(ans3))
								print('stampa time  {}'.format(wait_time))
								#indecision 
								if ans3==30:
									print("indecisions")
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									gare.express_indecision('indeci_fake_ser')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn2")
								if ans3 in already_given_resp:
									#reazione already start here
									print("false already given")
									gare.already_given_answer('fake_already_ser')
									scon = one_game.results[q]
									scon.num_given_ans+=1							
									continue 
								else:
									if one_game.ques_ans[q].answer4==5:
										last_ans_reached_flag = 2
										if one_game.limit1==1:
											tot_res = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											if tot_res:
												already_given_resp.append(one_game.ques_ans[q].answer3)
										if one_game.limit1==2:
											res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer2) and ans3 not in already_given_resp
											res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer3)
											tot_res = res1 or res2
									elif one_game.ques_ans[q].answer4=5:
										last_ans_reached_flag = 3
										if one_game.limit1==2:
											res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer4) and ans3 not in already_given_resp
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer4)
											tot_res = res1 or res2
										if one_game.limit1==3:
											res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer2) and ans3 not in already_given_resp
											res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											res3 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer4) and ans3 not in already_given_resp
											tot_res = res1 or res2 or res3
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res3:
												already_given_resp.append(one_game.ques_ans[q].answer4)	
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('finto_ser_right_a')
									turn+=1						
									scoc = one_game.results[q]
									scoc.num_corrects+=1
									if one_game.ques_ans[q].answer4==5: #NEXT STEP
										gare.start_audio(20) 
										q+=1
										turn = 0 #reset turn
										already_given_resp=[]
									else:
										continue
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('ser_wrong') 
									#start again from beginning with multiple answer , repeat both
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							if turn==3:
								print("-->siamo in turno quattro")
								print("quarto round question multipla")
								ns4, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans4, wait_time = 4,5
								print('stampa response given {}'.format(ans4))
								print('stampa time  {}'.format(wait_time))
								if ans4==30:
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									gare.express_indecision('ser_fake_indec')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn3")
								if ans4 in already_given_resp:
									print("false already given")
									gare.already_given_answer('already_ser')
									scon = one_game.results[q]
									scon.num_given_ans+=1
									continue 
								else:
									if one_game.limit1==1:
										tot_res = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer4)
									if one_game.limit1==2:
										res1 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer3) and ans4 not in already_given_resp
										res2 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer4)
										tot_res = res1 or res2
									if one_game.limit1==3:
										res1 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer2) and ans4 not in already_given_resp
										res2 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer3) and ans4 not in already_given_resp
										res3 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
										tot_res = res1 or res2 or res3
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer4)	
									last_ans_reached_flag = 3
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('ser_right_ok')
									gare.start_audio(20) 
									q+=1
									scoc = one_game.results[q]
									scoc.num_corrects+=1								
									turn = 0 #reset turn
									already_given_resp=[]					
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('wrong,sir ser')
									#start again from beginning with multiple answer , repeat both
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							print("already_given_resp {}".format(already_given_resp))
					elif one_game.this_type=='7OL3': #Tocca per primo i disegni di questo colore poi tocca per secondo i disegni di questo colore poi tocca per ultimo i disegni di questo colore rosso verde blu
						if use_turns==0:
							if first_time_need_led:
								first_time_need_led = 0
								if repeat_question:
									first_time_need_led = 1
								#repeat question puo esserci anche 
								#prepare MSG SERIAL
								print(one_game.current_game_colors)
								time.sleep(0.2)
								gare.start_audio(53)
								#if type of game serve il led === 2L
								led_msg_start_color = b'<touch2>' 
								lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
								ch = random.choices(lisa,k = 1)
								resp_msg_from_arduino = ch[0]
								print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
								#resp_msg_from_arduino = b'<q>'
								print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
								print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
								gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
							ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
							if ans_array[0]==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
								print("mm error ans2 == 5 mai possibile qui non entrerò mai")
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
								print("mm error ans2 == 5 mai possibile qui non entrerò mai")
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
								if len(ans_array)==3:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 333 ")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										if one_game.limit==1 and one_game.limit1==1 and one_game.limit2==1:
											res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
											res3 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
											tot_res = res1 or res2 or res3
								if len(ans_array)<3:
									print("mi aspetto piu risposte, ne ho solo 3")
									continue
								elif len(ans_array)>3:
									print("mi aspetto meno risposte ne ho ben 3")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
								last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)==4:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 4444444")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										if one_game.limit==2 and one_game.limit1==1 and one_game.limit2==1:
											res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
											res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											res4 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
										if one_game.limit==1 and one_game.limit1==2 and one_game.limit2==1:
											res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
											res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
											res4 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
										if one_game.limit==1 and one_game.limit1==1 and one_game.limit2==2:
											res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
											res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
											res3 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
											res4 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
										tot_res = res1 or res2 or res3 or res4
										last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)<4:
									print("mi aspetto piu risposte ne ho solo 4")
									continue
								elif len(ans_array)>4:
									print("mi aspetto meno risposte ne ho ben 4")
									continue
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue	
						else:
							if first_time_need_led:
								first_time_need_led = 0
								if repeat_question:
									first_time_need_led = 1
								#repeat question puo esserci anche 
								#prepare MSG SERIAL
								print("siamo in turno zero")
								print(one_game.current_game_colors)
								time.sleep(0.2)
								gare.start_audio(53)
								#if type of game serve il led === 2L
								led_msg_start_color = b'<touch1>' 
								lisa= [b'<q>',b'<z>',b'<x>',b'<y>']
								ch = random.choices(lisa,k = 1)
								resp_msg_from_arduino = ch[0]
								print("2L LED RIPOSTA RICEVUTO {}".format(resp_msg_from_arduino))
								#resp_msg_from_arduino = b'<q>'
								print("choose_ques_ans_to_use {} ".format(choose_ques_ans_to_use))
								print("resp_msg_from_arduino is {}".format(resp_msg_from_arduino))
								#faccio audio diversi a seconda 
								if first_time_need_led:
									ser='messaggio_seriale_a_caso'
									gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
								else:
									ser='messaggio_seriale_a_caso'
									gare.make_question_with_led(one_game.ques_ans[q].audio,resp_msg_from_arduino,'ser_messaggio')
								#mi aspetto una response per volta e poi dico ok ancora una e faccio i turni
								#append to already uno per volta....
							print("   round{}".format(q))
							if turn==0: #question zero prima che a sua volta a n risposte 
								print("-->siamo in turno zero")
								time.sleep(0.2)
								ans1, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time) 
								#ans1, wait_time = 1,5 #ESEMPI A CASO
								print('stampa response given {}'.format(ans1))
								print('stampa time  {}'.format(wait_time))
								if ans1==30: 
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									print("^^^^^^^ siamo in INDECISION")
									flag_time_elapsed = 1
									gare.express_indecision('messaggio_indecisione')
									continue
								too_many_consecutive_indecisions = 0
								print("ho superato il continue con i turni")
								flag_time_elapsed = 1
								if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
									tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									last_ans_reached_flag_at_ans_num = 0
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #2 risp da date in tutto
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #3 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 2
									if one_game.limit==1:
										tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										print("single res_ottenuta in play_the_game con 3 da dare OR {}".format(tot_res))
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer1)
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #4 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 3
									if one_game.limit==1:
										tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer1)
									if one_game.limit==2:
										res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
										tot_res = res1 or res2
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)
									if one_game.limit==3:
										res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)					
										res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)					
										res3 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer3)	
										tot_res = res1 or res2 or res3
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('rispesatta')
									turn+=1
									scoc = one_game.results[q]
									scoc.num_corrects+=1								
									if one_game.ques_ans[q].answer2==5: #NEXT STEP non esiste domdanda finita annullo anche gli already
										gare.start_audio(20) 
										q+=1
										turn = 0
										already_given_resp=[]
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('wrong') 
									#start again from beginning with multiple answer , repeat both
									turn = 0
									already_given_resp=[] #solo in turno zero
									continue								
							if turn==1:
								print("-->siamo in turno uno")
								print("secondo round question multipla")
								ans2, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans2, wait_time = 2,5 #ESEMPI A CASO							
								print('stampa response given {}'.format(ans2))
								print('stampa time  {}'.format(wait_time))
								if ans2==30:
									print("indecisione")
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									#reactrion indecision start here
									gare.express_indecision('finto_ser_indecisione')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn1")
								if ans2 in already_given_resp or ans2 in already_given_resp:
									print("false already given")
									#reazione already start here
									gare.already_given_answer('finto_ser_already_given')
									scon = one_game.results[q]
									scon.num_given_ans+=1
									continue #for now reapeat from the last right answer
								else:
									if one_game.ques_ans[q].answer3==5:
										print("impossibile mm erroreee") 					
									elif one_game.ques_ans[q].answer3=5 and  one_game.ques_ans[q].answer4==5:
										last_ans_reached_flag = 2
										if one_game.limit1==1:
											tot_res = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
											if tot_res:
												already_given_resp.append(one_game.ques_ans[q].answer3)				 					
									elif one_game.ques_ans[q].answer3=5 and  one_game.ques_ans[q].answer4=5:
										last_ans_reached_flag = 3
										if one_game.limit1==1:
											tot_res = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
											if tot_res:
												already_given_resp.append(one_game.ques_ans[q].answer4)
										if one_game.limit1==2:
											res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
											res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer4)
											tot_res = res1 or res2
										if one_game.limit1==3:
											if one_game.limit1==1:
												tot_res = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
												if tot_res:
													already_given_resp.append(one_game.ques_ans[q].answer4)
											if one_game.limit1==2:
												res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
												res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
												if res1:
													already_given_resp.append(one_game.ques_ans[q].answer3)					 					
												if res2:
													already_given_resp.append(one_game.ques_ans[q].answer4)
												tot_res = res1 or res2
												if tot_res:
													already_given_resp.append(one_game.ques_ans[q].answer4)
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									#reaction start here good boy
									gare.right_answer('finto_ser_right_Ans')			
									turn+=1
									scoc = one_game.results[q]
									scoc.num_corrects+=1										
									if one_game.ques_ans[q].answer3==5: #NEXT STEP
										gare.start_audio(20) 
										q+=1
										turn = 0 #reset turn
										already_given_resp=[]
									else:
										continue
								else: 
									print("FALSE, wrong answer")	
									# start bad reaction
									gare.wrong_answer('finto_Ser_wrong') #start again from beginning with multiple answer , repeat both
									#avro due audio diversi quindi proviamo
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							if turn==2:
								print("-->siamo in turno tre")
								print("terzo round question multipla")
								ans3, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans3, wait_time = 3,5
								print('stampa response given {}'.format(ans3))
								print('stampa time  {}'.format(wait_time))
								#indecision 
								if ans3==30:
									print("indecisione")
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									#audio dovrebbe cambiare in base a che turno sono ti manca un leone da mettere o due o tre....
									flag_time_elapsed = 1
									gare.express_indecision('indeci_fake_ser')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn2")
								if ans3 in already_given_resp:
									#reazione already start here
									print("false already given")
									gare.already_given_answer('fake_already_ser')
									scon = one_game.results[q]
									scon.num_given_ans+=1
									continue 
								else:
									if one_game.ques_ans[q].answer4==5:
										last_ans_reached_flag = 2
										if one_game.limit1==1:
											tot_res = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											if tot_res:
												already_given_resp.append(one_game.ques_ans[q].answer3)
										if one_game.limit1==2:
											res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer2) and ans3 not in already_given_resp
											res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer3)
											tot_res = res1 or res2
									elif one_game.ques_ans[q].answer4=5:
										last_ans_reached_flag = 3
										if one_game.limit1==2:
											res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer4) and ans3 not in already_given_resp
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer4)
											tot_res = res1 or res2
										if one_game.limit1==3:
											res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer2) and ans3 not in already_given_resp
											res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
											res3 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer4) and ans3 not in already_given_resp
											tot_res = res1 or res2 or res3
											if res1:
												already_given_resp.append(one_game.ques_ans[q].answer2)					 					
											if res2:
												already_given_resp.append(one_game.ques_ans[q].answer3)					 					
											if res3:
												already_given_resp.append(one_game.ques_ans[q].answer4)	
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('finto_ser_right_a')
									turn+=1
									scoc = one_game.results[q]
									scoc.num_corrects+=1								
									if one_game.ques_ans[q].answer4==5: #NEXT STEP
										gare.start_audio(20) 
										q+=1
										turn = 0 #reset turn
										already_given_resp=[]
									else:
										continue
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('ser_wrong') 
									#start again from beginning with multiple answer , repeat both
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							if turn==3:
								print("-->siamo in turno quattro")
								print("quarto round question multipla")
								ns4, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans4, wait_time = 4,5
								print('stampa response given {}'.format(ans4))
								print('stampa time  {}'.format(wait_time))
								if ans4==30:
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									gare.express_indecision('ser_fake_indec')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn3")
								if ans4 in already_given_resp:
									print("false already given")
									gare.already_given_answer('already_ser')
									scon = one_game.results[q]
									scon.num_given_ans+=1
									continue 
								else:
									if one_game.limit1==1:
										tot_res = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
										if tot_res:
											already_given_resp.append(one_game.ques_ans[q].answer4)
									if one_game.limit1==2:
										res1 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer3) and ans4 not in already_given_resp
										res2 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer4)
										tot_res = res1 or res2
									if one_game.limit1==3:
										res1 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer2) and ans4 not in already_given_resp
										res2 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer3) and ans4 not in already_given_resp
										res3 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
										tot_res = res1 or res2 or res3
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer4)	
									last_ans_reached_flag = 3
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('ser_right_ok')
									gare.start_audio(20) 
									q+=1
									turn = 0 #reset turn
									already_given_resp=[]					
									scoc = one_game.results[q]
									scoc.num_corrects+=1						
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('wrong,sir ser')
									#start again from beginning with multiple answer , repeat both
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							print("FINALE 6666666666666666 already_given_resp {}".format(already_given_resp))
					elif one_game.this_type in ['7OSU2''7OCO2'] or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q] in ['7OSU2''7OCO2']): #Tocca nel seguente ordine il leone la scimmia ; #Tocca prima un disegno rosso poi un disegno verde
						ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
						if ans_array[0]==30: #INDECISION
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							flag_time_elapsed = 1
							gare.express_indecision('sono_il_seriale')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0						
						print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
						flag_time_elapsed = 0
						if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
							print("mm error ans2 == 5 mai possibile qui non entrerò mai")
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
							if len(ans_array)==2:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 222")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:					
									if one_game.limit==1 and one_game.limit1==1:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										tot_res = res1 or res2
										last_ans_reached_flag_at_ans_num = 1
							if len(ans_array)<2:
								print("mi aspetto piu risposte, ne ho solo 2")
								continue
							elif len(ans_array)>2:
								print("mi aspetto meno risposte, ne ho ben 2")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
							if len(ans_array)==3:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 333 ")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									if one_game.limit==2 and one_game.limit1==1:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										tot_res = res1 or res2 or res3
									if one_game.limit==1 and one_game.limit1==2:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										tot_res = res1 or res2 or res3
									last_ans_reached_flag_at_ans_num = 2 
							if len(ans_array)<3:
								print("mi aspetto piu risposte, ne ho solo 3")
								continue
							elif len(ans_array)>3:
								print("mi aspetto meno risposte ne ho ben 3")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
							last_ans_reached_flag_at_ans_num = 3
							if len(ans_array)==4:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 4444444")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									if one_game.limit==2 and one_game.limit1==2:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
									if one_game.limit==3 and one_game.limit1==1:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
									if one_game.limit==1 and one_game.limit1==3:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
									tot_res = res1 or res2 or res3 or res4
									last_ans_reached_flag_at_ans_num = 3 
							if len(ans_array)<4:
								print("mi aspetto piu risposte ne ho solo 4 ")
								continue
							elif len(ans_array)>4:
								print("mi aspetto meno risposte ne ho ben 5")
								continue
						scon = one_game.results[q]
						scon.num_given_ans+=1
						if tot_res:
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							print("correct response:esatta")
							gare.right_answer('ser_right_resp')
							q+=1
						else:
							print("FALSE, wrong answer")
							gare.wrong_answer('false ser') 
							too_many_consecutive_wrong_ans+=1
							#start again from beginning with multiple answer , repeat both
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
							continue
					elif one_game.this_type in ['7OSU3''7OCO3'] or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q] in ['7OSU3''7OCO3']): #Tocca nel seguente ordine il leone la scimmia il pappagallo #Tocca prima un disegno rosso poi un disegno verde poi uno blu
						ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
						if ans_array[0]==30: #INDECISION
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							flag_time_elapsed = 1
							gare.express_indecision('sono_il_seriale')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0
						print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
						flag_time_elapsed = 0
						if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
							print("mm error ans2 == 5 mai possibile qui non entrerò mai")
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
							print("mm error ans2 == 5 mai possibile qui non entrerò mai")
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
							if len(ans_array)==3:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 333 ")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									if one_game.limit==1 and one_game.limit1==1 and one_game.limit2==1:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										tot_res = res1 or res2 or res3
									last_ans_reached_flag_at_ans_num = 2 
							if len(ans_array)<3:
								print("mi aspetto piu risposte, ne ho solo 3")
								continue
							elif len(ans_array)>3:
								print("mi aspetto meno risposte ne ho ben 3")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
							last_ans_reached_flag_at_ans_num = 3
							if len(ans_array)==4:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 4444444")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									if one_game.limit==2 and one_game.limit1==1 and one_game.limit2==1:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
									if one_game.limit==1 and one_game.limit1==2 and one_game.limit2==1:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
									if one_game.limit==1 and one_game.limit1==1 and one_game.limit2==2:
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
									tot_res = res1 or res2 or res3 or res4
									last_ans_reached_flag_at_ans_num = 3 
							if len(ans_array)<4:
								print("mi aspetto piu risposte ne ho solo 4 ")
								continue
							elif len(ans_array)>4:
								print("mi aspetto meno risposte ne ho ben 5")
								continue
						scon = one_game.results[q]
						scon.num_given_ans+=1
						if tot_res:
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							print("correct response:esatta")
							gare.right_answer('ser_right_resp')
							q+=1
						else:
							print("FALSE, wrong answer")
							gare.wrong_answer('false ser') 
							too_many_consecutive_wrong_ans+=1
							#start again from beginning with multiple answer , repeat both
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
							continue
					elif one_game.this_type in ['7OSU4''7OCO4'] or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q] in ['7OSU4''7OCO4']):
						ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
						if ans_array[0]==30: #INDECISION
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							flag_time_elapsed = 1
							gare.express_indecision('sono_il_seriale')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0
						print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
						flag_time_elapsed = 0
						if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
							print("mm error ans2 == 5 1 mai possibile qui non entrerò mai")
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
							print("mm error ans2 == 5 2 mai possibile qui non entrerò mai")
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
							print("mm error ans2 == 5 3 mai possibile qui non entrerò mai")
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
							last_ans_reached_flag_at_ans_num = 3
							if len(ans_array)==4:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("mm error 4444444")
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
									res4 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
									tot_res = res1 or res2 or res3 or res4
									last_ans_reached_flag_at_ans_num = 3 
							if len(ans_array)<4:
								print("mi aspetto piu risposte ne ho solo 4 ")
								continue
							elif len(ans_array)>4:
								print("mi aspetto meno risposte ne ho ben 5")
								continue
						scon = one_game.results[q]
						scon.num_given_ans+=1					
						if tot_res:
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							print("correct response:esatta")
							gare.right_answer('ser_right_resp')
							q+=1
						else:
							print("FALSE, wrong answer")
							gare.wrong_answer('false ser') 
							too_many_consecutive_wrong_ans+=1
							#start again from beginning with multiple answer , repeat both
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
							continue
				if this_momentaneous_kind=='8Q' or one_game.this_kind=='8Q':
					if one_game.type in ['8QTCODB1','8QTCODB2','8QTCOSB1','8QTCOSB2','8QTKNSB','8QTPRSB','8QTSUDB1','8QTSUDB2','8QTSUSB1','8QTSUSB2'] or \
					(one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q] in ['8QTCODB1','8QTCODB2','8QTCOSB1','8QTCOSB2','8QTKNSB','8QTPRSB','8QTSUDB1','8QTSUDB2','8QTSUSB1','8QTSUSB2']):
						#Premi la mia pancia se ci sono un disegno rosso e un disegno blu ; #Premi la mia pancia se ci sono un disegno rosso e due verdi
						#Premi la mia pancia se vedi un disegno rosso #Premi la mia pancia se vedi un disegno rosso
						#Premi la mia pancia se vedi qualcosa che si può mangiare
						#Premi la mia pancia se ci sono meno di due leoni
						#Premi la mia pancia se vedi un leone e una scimmia
						#Premi la mia pancia se ci sono due leoni e una scimmia
						#Premi la mia pancia se vedi un leone
						#Premi la mia pancia se ci sono due leoni
						#score depends on touch 
						print("no_ans_to_create_only_body {}".format(one_game.no_ans_to_create_only_body)) #check che sia =1 
						print("one_game.resp_in_body {}".format(one_game.resp_in_body)) #check che sia =1 
						resp_mpx, wait_time = gare.wait_mpx('serial_msg_mpx')
						if resp_mpx in [1,2,3,4]:
							tot_res = 1
						else: tot_res = 0
						scon = one_game.results[q]
						scon.num_given_ans+=1
						if tot_res:
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							print("correct response:esatta")
							gare.right_answer('ser_right_resp')
							q+=1
						else:
							print("FALSE, wrong answer")
							gare.wrong_answer('false ser') 
							too_many_consecutive_wrong_ans+=1
							#start again from beginning with multiple answer , repeat both
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
							continue
					elif one_game.type in ['8QTCODC1','8QTCODC2','8QTCOSC1','8QTCOSC2','8QTKNDC','8QTKNSC','8QTPRSC','8QTSUDC1','8QTSUDC2','8QTSUSC1','8QTSUSC2'] or \
					(one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q] in ['8QTCODC1','8QTCODC2','8QTCOSC1','8QTCOSC2','8QTKNDC','8QTKNSC','8QTPRSC','8QTSUDC1','8QTSUDC2','8QTSUSC1','8QTSUSC2']):
						#Tocca i leoni se ci sono un disegno rosso e un disegno blu DOUBLE
						#Tocca i leoni se ci sono un disegno rosso e due disegni blu DOUBLE
						#Tocca i leoni se ci sono un disegno rosso SINGLE					
						#Tocca i leoni se ci sono due disegno rosso SINGLE	
						#Tocca i leoni se vedi qualcuno che sa nuotare e correre
						#Tocca i leoni se vedi qualcosa che si può mangiare
						#Tocca i leoni se ci sono meno di due scimmie
						#Tocca i leoni se vedi una scimmia e un pappagallo
						#Tocca i leoni se ci sono due scimmie e un pappagallo
						#Tocca i leoni se c'è una scimmia
						#Tocca i leoni se ci sono due scimmie
						if use_turns==0:
							ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
							if ans_array[0]==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								if len(ans_array)==1:
									print("how was the limit? LIMIT = {} ".format(one_game.limit))
									print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
									tot_res = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								else:
									print("mi aspetto piu risp, ne ho solo 1")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
								if len(ans_array)==2:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 222")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										tot_res1 = res1 or res2 
										#check first ans
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										tot_res2 = res3 or res4
										print("both res_ottenute 2 in play_the_game con 2 risp da dare")
										last_ans_reached_flag_at_ans_num = 1
										tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<2:
									print("mi aspetto piu risposte, ne ho solo 2")
									continue
								elif len(ans_array)>2:
									print("mi aspetto meno risposte, ne ho ben 2")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
								if len(ans_array)==3:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 333 ")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										print("res")
										print(res1)
										print(res2)
										print(res3)
										tot_res1 = res1 or res2 or res3
										#check second ans
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										tot_res2 = res4 or res5 or res6
										print("res")
										print(res1)
										print(res2)
										print(res3)
										#check third ans
										res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
										res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										tot_res3 = res7 or res8 or res9
										print("res")
										print(res1)
										print(res2)
										print(res3)
										print("both res_ottenute 3 in play_the_game con 3 risp da dare")
										last_ans_reached_flag_at_ans_num = 2 
										tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<3:
									print("mi aspetto piu risposte, ne ho solo 3")
									continue
								elif len(ans_array)>3:
									print("mi aspetto meno risposte ne ho ben 3")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
								last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)==4:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 4444444")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										tot_res1 = res1 or res2 or res3 or res4
										#check second ans
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										tot_res2 = res5 or res6 or res7 or res8
										#check third ans
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
										res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
										res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
										tot_res3 = res9 or res10 or res11 or res12
										#check fourth ans
										res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
										res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer2)
										res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
										res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
										tot_res4 = res13 or res14 or res15 or res16
										print("both res_ottenute 4 in play_the_game con 4 risp da dare")
										last_ans_reached_flag_at_ans_num = 3 
										tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<4:
									print("mi aspetto piu risposte ne ho solo 4 ")
									continue
								elif len(ans_array)>4:
									print("mi aspetto meno risposte ne ho ben 5")
									continue
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue
						else:
							#mi aspetto una response per volta e poi dico ok ancora una e faccio i turni
							#append to already uno per volta....
							print("    round{}".format(q))
							if turn==0: #question zero prima che a sua volta a n risposte 
								print("-->siamo in turno zero")
								time.sleep(0.2)
								ans1, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time) 
								#ans1, wait_time = 1,5 #ESEMPI A CASO
								print('stampa response given {}'.format(ans1))
								print('stampa time  {}'.format(wait_time))
								if ans1==30: 
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									print("^^^^^^^ siamo in INDECISION")
									flag_time_elapsed = 1
									gare.express_indecision('messaggio_indecisione')
									continue
								too_many_consecutive_indecisions = 0
								print("ho superato il continue con i turni")
								flag_time_elapsed = 1
								if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
									tot_res = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									last_ans_reached_flag_at_ans_num = 0
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #2 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 1
									res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
									print("single res_ottenuta 1 in play_the_game con 2 risp da dare {}".format(res1))
									print("single res_ottenuta 2 in play_the_game con 2 risp da dare {}".format(res2))
									tot_res = res1 or res2 
									print("single res_ottenuta in play_the_game con 2 da dare OR {}".format(tot_res))
									if res1:
										already_given_resp.append(one_game.ques_ans[q].answer1)
									if res2:
										already_given_resp.append(one_game.ques_ans[q].answer2)
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #3 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 2
									res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer3)
									print("single res_ottenuta 1 in play_the_game con 3 risp da dare {}".format(res1))
									print("single res_ottenuta 2 in play_the_game con 3 risp da dare {}".format(res2))
									print("single res_ottenuta 3 in play_the_game con 3 risp da dare {}".format(res3))
									tot_res = res1 or res2 or res3 
									print("single res_ottenuta in play_the_game con 3 da dare OR {}".format(tot_res))
									if res1:
										already_given_resp.append(one_game.ques_ans[q].answer1)
									if res2:
										already_given_resp.append(one_game.ques_ans[q].answer2)
									if res3:
										already_given_resp.append(one_game.ques_ans[q].answer3)
								elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #4 risp da date in tutto
									last_ans_reached_flag_at_ans_num = 3
									res1 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer3)
									res4 = gaca.check_correctness(ans1,one_game.ques_ans[q].answer4)					
									print("single res_ottenuta 1 in play_the_game con 4 risp da dare {}".format(res1))
									print("single res_ottenuta 2 in play_the_game con 4 risp da dare {}".format(res2))
									print("single res_ottenuta 3 in play_the_game con 4 risp da dare {}".format(res3))
									print("single res_ottenuta 4 in play_the_game con 4 risp da dare {}".format(res4))
									tot_res = res1 or res2 or res3 or res4
									print("single res_ottenuta in play_the_game con 4 da dare OR {}".format(tot_res))
									if res1:
										already_given_resp.append(one_game.ques_ans[q].answer1)
									if res2:
										already_given_resp.append(one_game.ques_ans[q].answer2)
									if res3:
										already_given_resp.append(one_game.ques_ans[q].answer3)
									if res4:
										already_given_resp.append(one_game.ques_ans[q].answer4)	
								scon = one_game.results[q]
								scon.num_given_ans+=1
								if tot_res:
									print("correct response:esatta")
									gare.right_answer('rispesatta')
									turn+=1
									scoc = one_game.results[q]
									scoc.num_corrects+=1								
									if one_game.ques_ans[q].answer2==5: #NEXT STEP non esiste domdanda finita annullo anche gli already
										gare.start_audio(20) 
										q+=1
										turn = 0
										already_given_resp=[]
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('wrong') 
									#start again from beginning with multiple answer , repeat both
									turn = 0
									already_given_resp=[] #solo in turno zero
									continue								
							if turn==1:
								print("-->siamo in turno uno")
								print("secondo round question multipla")
								ans2, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans2, wait_time = 2,5 #ESEMPI A CASO							
								print('stampa response given {}'.format(ans2))
								print('stampa time  {}'.format(wait_time))
								if ans2==30:
									print("indecisione")
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									#reactrion indecision start here
									gare.express_indecision('finto_ser_indecisione')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn1")
								if ans2 in already_given_resp or ans2 in already_given_resp:
									print("false already given")
									#reazione already start here
									gare.already_given_answer('finto_ser_already_given')
									scon = one_game.results[q]
									scon.num_given_ans+=1
									continue #for now reapeat from the last right answer
								else:
									if one_game.ques_ans[q].answer3==5:
										last_ans_reached_flag = 1
										res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer1) and ans2 not in already_given_resp
										res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										res = res1 or res2 
										print("turno 1 single res_ottenuta 1 in play_the_game con 2 risp da dare {}".format(res1))
										print("turno 1 single res_ottenuta 2 in play_the_game con 2 risp da dare {}".format(res2))
									elif one_game.ques_ans[q].answer3=5 and  one_game.ques_ans[q].answer4==5:
										last_ans_reached_flag = 2
										res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer1) and ans2 not in already_given_resp
										res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
										res3 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
										print("turno 1 single res_ottenuta 1 in play_the_game con 3 risp da dare {}".format(res1))
										print("turno 1 single res_ottenuta 2 in play_the_game con 3 risp da dare {}".format(res2))							
										print("turno 1 single res_ottenuta 3 in play_the_game con 3 risp da dare {}".format(res3))							
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										res = res1 or res2 or res3
									elif one_game.ques_ans[q].answer3=5 and  one_game.ques_ans[q].answer4=5:
										last_ans_reached_flag = 3
										res1 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer1) and ans2 not in already_given_resp
										res2 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer2) and ans2 not in already_given_resp
										res3 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer3) and ans2 not in already_given_resp
										res4 = gaca.check_correctness(ans2,one_game.ques_ans[q].answer4) and ans2 not in already_given_resp
										print("turno 1 single res_ottenuta 1 in play_the_game con 4 risp da dare {}".format(res1))
										print("turno 1 single res_ottenuta 2 in play_the_game con 4 risp da dare {}".format(res2))							
										print("turno 1 single res_ottenuta 3 in play_the_game con 4 risp da dare {}".format(res3))
										print("turno 1 single res_ottenuta 4 in play_the_game con 4 risp da dare {}".format(res4))
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										if res4:
											already_given_resp.append(one_game.ques_ans[q].answer4)					 					
										res = res1 or res2 or res3 or res4 
								if res:
									#reaction start here good boy
									gare.right_answer('finto_ser_right_Ans')			
									turn+=1		
									if one_game.ques_ans[q].answer3==5: #NEXT STEP
										gare.start_audio(20) 
										q+=1
										turn = 0 #reset turn
										already_given_resp=[]
									else:
										continue
								else: 
									print("FALSE, wrong answer")	
									# start bad reaction
									gare.wrong_answer('finto_Ser_wrong') #start again from beginning with multiple answer , repeat both
									#avro due audio diversi quindi proviamo
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							if turn==2:
								print("-->siamo in turno tre")
								print("terzo round question multipla")
								ans3, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans3, wait_time = 3,5
								print('stampa response given {}'.format(ans3))
								print('stampa time  {}'.format(wait_time))
								#indecision 
								if ans3==30:
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									#audio dovrebbe cambiare in base a che turno sono ti manca un leone da mettere o due o tre....
									flag_time_elapsed = 1
									gare.express_indecision('indeci_fake_ser')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn2")
								if ans3 in already_given_resp:
									#reazione already start here
									print("false already given")
									gare.already_given_answer('fake_already_ser')
									scon = one_game.results[q]
									scon.num_given_ans+=1
									continue 
								else:
									if one_game.ques_ans[q].answer4==5:
										last_ans_reached_flag = 2
										res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer1) and ans3 not in already_given_resp
										res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer2) and ans3 not in already_given_resp
										res3 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
										print("turno 2 single res_ottenuta 1 in play_the_game con 3 risp da dare {}".format(res1))
										print("turno 2 single res_ottenuta 2 in play_the_game con 3 risp da dare {}".format(res2))
										print("turno 2 single res_ottenuta 3 in play_the_game con 3 risp da dare {}".format(res3))
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										res = res1 or res2 or res3 
									elif one_game.ques_ans[q].answer4=5:
										last_ans_reached_flag = 3
										res1 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer1) and ans3 not in already_given_resp
										res2 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer2) and ans3 not in already_given_resp
										res3 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer3) and ans3 not in already_given_resp
										res4 = gaca.check_correctness(ans3,one_game.ques_ans[q].answer4) and ans3 not in already_given_resp
										print("turno 2 single res_ottenuta 1 in play_the_game con 4 risp da dare {}".format(res1))
										print("turno 2 single res_ottenuta 2 in play_the_game con 4 risp da dare {}".format(res2))
										print("turno 2 single res_ottenuta 3 in play_the_game con 4 risp da dare {}".format(res3))
										print("turno 2 single res_ottenuta 4 in play_the_game con 4 risp da dare {}".format(res4))							
										if res1:
											already_given_resp.append(one_game.ques_ans[q].answer1)					 					
										if res2:
											already_given_resp.append(one_game.ques_ans[q].answer2)					 					
										if res3:
											already_given_resp.append(one_game.ques_ans[q].answer3)					 					
										if res4:
											already_given_resp.append(one_game.ques_ans[q].answer4)					 					
										res = res1 or res2 or res3 or res4 	
								if res:
									print("correct response:esatta")
									gare.right_answer('finto_ser_right_a')
									turn+=1						
									if one_game.ques_ans[q].answer4==5: #NEXT STEP
										gare.start_audio(20) 
										q+=1
										turn = 0 #reset turn
										already_given_resp=[]
									else:
										continue
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('ser_wrong') 
									#start again from beginning with multiple answer , repeat both
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							if turn==3:
								print("-->siamo in turno quattro")
								print("quarto round question multipla")
								ns4, wait_time = gare.wait_answer(one_game.ques_ans[q].required_time)
								#ans4, wait_time = 4,5
								print('stampa response given {}'.format(ans4))
								print('stampa time  {}'.format(wait_time))
								if ans4==30:
									too_many_consecutive_indecisions+=1
									too_many_consecutive_wrong_ans = 0
									flag_time_elapsed = 1
									gare.express_indecision('ser_fake_indec')
									continue 
								too_many_consecutive_indecisions = 0
								print("continue superato in turn3")
								if ans4 in already_given_resp:
									print("false already given")
									gare.already_given_answer('already_ser')
									scon = one_game.results[q]
									scon.num_given_ans+=1
									continue 
								else:
									last_ans_reached_flag = 3
									#posso sistemarlo easy
									res1 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer1) and ans4 not in already_given_resp
									res2 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer2) and ans4 not in already_given_resp
									res3 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer3) and ans4 not in already_given_resp
									res4 = gaca.check_correctness(ans4,one_game.ques_ans[q].answer4) and ans4 not in already_given_resp
									print("turno 3 single res_ottenuta 1 in play_the_game con 4 risp da dare {}".format(res1))
									print("turno 3 single res_ottenuta 2 in play_the_game con 4 risp da dare {}".format(res2))
									print("turno 3 single res_ottenuta 3 in play_the_game con 4 risp da dare {}".format(res3))
									print("turno 3 single res_ottenuta 4 in play_the_game con 4 risp da dare {}".format(res4))
									res = res1 or res2 or res3 or res4 
								if res:
									print("correct response:esatta")
									gare.right_answer('ser_right_ok')
									gare.start_audio(20) 
									q+=1
									turn = 0 #reset turn
									already_given_resp=[]					
								else:
									print("FALSE, wrong answer")
									gare.wrong_answer('wrong,sir ser')
									#start again from beginning with multiple answer , repeat both
									if already_given_resp:
										already_given_resp.pop()  #resetting turno 1
										print("already dopo pop {}".format(already_given_resp))
									continue
							print("FINALE 6666666666666666 already_given_resp {}".format(already_given_resp))
					elif one_game.type=='8QTSUSCOC1' or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q]=='8QTSUSCOC1'): #Tocca i leoni se vedi una scimmia altrimenti tocca i pappagalli
						if one_game.the_first:
							ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
							if ans_array[0]==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								if len(ans_array)==1:
									print("how was the limit? LIMIT = {} ".format(one_game.limit))
									print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
									tot_res = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								else:
									print("mi aspetto piu risp, ne ho solo 1")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
								if len(ans_array)==2:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 222")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										tot_res1 = res1 or res2 
										#check first ans
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										tot_res2 = res3 or res4
										print("both res_ottenute 2 in play_the_game con 2 risp da dare")
										last_ans_reached_flag_at_ans_num = 1
										tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<2:
									print("mi aspetto piu risposte, ne ho solo 2")
									continue
								elif len(ans_array)>2:
									print("mi aspetto meno risposte, ne ho ben 2")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
								if len(ans_array)==3:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 333 ")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										print("res")
										print(res1)
										print(res2)
										print(res3)
										tot_res1 = res1 or res2 or res3
										#check second ans
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										tot_res2 = res4 or res5 or res6
										print("res")
										print(res1)
										print(res2)
										print(res3)
										#check third ans
										res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
										res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										tot_res3 = res7 or res8 or res9
										print("res")
										print(res1)
										print(res2)
										print(res3)
										print("both res_ottenute 3 in play_the_game con 3 risp da dare")
										last_ans_reached_flag_at_ans_num = 2 
										tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<3:
									print("mi aspetto piu risposte, ne ho solo 3")
									continue
								elif len(ans_array)>3:
									print("mi aspetto meno risposte ne ho ben 3")
									continue
							elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
								last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)==4:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 4444444")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer3)
										res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer4)
										tot_res1 = res1 or res2 or res3 or res4
										#check second ans
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer1)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
										res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer3)
										res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer4)
										tot_res2 = res5 or res6 or res7 or res8
										#check third ans
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer1)
										res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer2)
										res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
										res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer4)
										tot_res3 = res9 or res10 or res11 or res12
										#check fourth ans
										res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer1)
										res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer2)
										res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer3)
										res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
										tot_res4 = res13 or res14 or res15 or res16
										print("both res_ottenute 4 in play_the_game con 4 risp da dare")
										last_ans_reached_flag_at_ans_num = 3 
										tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<4:
									print("mi aspetto piu risposte ne ho solo 4 ")
									continue
								elif len(ans_array)>4:
									print("mi aspetto meno risposte ne ho ben 5")
									continue
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue
						elif one_game.the_second:
							ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
							if ans_array[0]==30: #INDECISION
								too_many_consecutive_indecisions+=1
								too_many_consecutive_wrong_ans = 0
								flag_time_elapsed = 1
								gare.express_indecision('sono_il_seriale')
								take_sco = one_game.results[q]
								take_sco.num_indecisions+=1
								print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
								continue
							too_many_consecutive_indecisions = 0
							print("ho superato il continue sin turnos nel tipo {}".format(one_game.this_type))
							flag_time_elapsed = 0
							if one_game.ques_ans_1[q].answer2==5: #c'è da dare solo una risp
								last_ans_reached_flag_at_ans_num = 0
								if len(ans_array)==1:
									print("how was the limit? LIMIT = {} ".format(one_game.limit))
									print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
									tot_res = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer1)
									print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
								else:
									print("mi aspetto piu risp, ne ho solo 1")
									continue
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3==5: #mi aspetto 2 risposte
								if len(ans_array)==2:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 222")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer2)
										tot_res1 = res1 or res2 
										#check first ans
										res3 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer1)
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer2)
										tot_res2 = res3 or res4
										print("both res_ottenute 2 in play_the_game con 2 risp da dare")
										last_ans_reached_flag_at_ans_num = 1
										tot_res = tot_res1 or tot_res2 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<2:
									print("mi aspetto piu risposte, ne ho solo 2")
									continue
								elif len(ans_array)>2:
									print("mi aspetto meno risposte, ne ho ben 2")
									continue
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4==5: #mi aspetto 3 risposte
								if len(ans_array)==3:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 333 ")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer3)
										print("res")
										print(res1)
										print(res2)
										print(res3)
										tot_res1 = res1 or res2 or res3
										#check second ans
										res4 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer1)
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer2)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer3)
										tot_res2 = res4 or res5 or res6
										print("res")
										print(res1)
										print(res2)
										print(res3)
										#check third ans
										res7 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer1)
										res8 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer2)
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer3)
										tot_res3 = res7 or res8 or res9
										print("res")
										print(res1)
										print(res2)
										print(res3)
										print("both res_ottenute 3 in play_the_game con 3 risp da dare")
										last_ans_reached_flag_at_ans_num = 2 
										tot_res = tot_res1 or tot_res2 or tot_res3 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<3:
									print("mi aspetto piu risposte, ne ho solo 3")
									continue
								elif len(ans_array)>3:
									print("mi aspetto meno risposte ne ho ben 3")
									continue
							elif one_game.ques_ans_1[q].answer2=5 and one_game.ques_ans_1[q].answer3=5 and one_game.ques_ans_1[q].answer4=5: #mi aspetto 4 risposte
								last_ans_reached_flag_at_ans_num = 3
								if len(ans_array)==4:
									duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
									if len(duplic_array):
										#redo response given more times
										print("mm error 4444444")
										print("duplicates are {}".format(duplic_array))
										print("same ans given more time")
										continue
									else:
										print("how was the limit? LIMIT = {} ".format(one_game.limit))
										print("how was the limit? LIMIT 1 = {} ".format(one_game.limit1))
										#check first ans
										res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer1)
										res2 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer2)
										res3 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer3)
										res4 = gaca.check_correctness(ans_array[0],one_game.ques_ans_1[q].answer4)
										tot_res1 = res1 or res2 or res3 or res4
										#check second ans
										res5 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer1)
										res6 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer2)
										res7 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer3)
										res8 = gaca.check_correctness(ans_array[1],one_game.ques_ans_1[q].answer4)
										tot_res2 = res5 or res6 or res7 or res8
										#check third ans
										res9 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer1)
										res10 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer2)
										res11 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer3)
										res12 = gaca.check_correctness(ans_array[2],one_game.ques_ans_1[q].answer4)
										tot_res3 = res9 or res10 or res11 or res12
										#check fourth ans
										res13 = gaca.check_correctness(ans_array[3],one_game.ques_ans_1[q].answer1)
										res14 = gaca.check_correctness(ans_array[3],one_game.ques_ans_1[q].answer2)
										res15 = gaca.check_correctness(ans_array[3],one_game.ques_ans_1[q].answer3)
										res16 = gaca.check_correctness(ans_array[3],one_game.ques_ans_1[q].answer4)
										tot_res4 = res13 or res14 or res15 or res16
										print("both res_ottenute 4 in play_the_game con 4 risp da dare")
										last_ans_reached_flag_at_ans_num = 3 
										tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 #1 response finale ok corretto, 0 altrimenti
								if len(ans_array)<4:
									print("mi aspetto piu risposte ne ho solo 4 ")
									continue
								elif len(ans_array)>4:
									print("mi aspetto meno risposte ne ho ben 5")
									continue
							scon = one_game.results[q]
							scon.num_given_ans+=1
							if tot_res:
								scoc = one_game.results[q]
								scoc.num_corrects+=1
								too_many_consecutive_wrong_ans = 0
								print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
								print("correct response:esatta")
								gare.right_answer('ser_right_resp')
								q+=1
							else:
								print("FALSE, wrong answer")
								gare.wrong_answer('false ser') 
								too_many_consecutive_wrong_ans+=1
								too_many_errors+=1
								#start again from beginning with multiple answer , repeat both
								scoee = one_game.results[q]
								scoee.num_errors+=1
								print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
								continue
				if this_momentaneous_kind=='9C' or one_game.this_kind=='9C':
					if one_game.this_type in ['9CNSU','9CNCO'] or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q] in ['9CNSU','9CNCO']): #"Quanti disegni rossi ci sono? Premi la mia pancia 2 volte se ci sono due rossi" ; #"Quanti leoni ci sono? Premi la mia pancia 1 volta se vedi un leone"
						print("entro in letsplay 9CNSU or 9CNCO")
						#qui indecision e non dare response è la stessa cosa --> incremento 
						#punteggio dipende da se tocco o meno... se non ci sono metto la question lo stesso??? cche non deve ricevere response????
						#pensaci è importante
						print("no_ans_to_create_only_body {}".format(one_game.no_ans_to_create_only_body)) #check che sia =1 
						print("one_game.resp_in_body {}".format(one_game.resp_in_body)) #check che sia =1 
						resp_mpx, wait_time = gare.wait_mpx('serial_msg_mpx')
						if resp_mpx==1:
							ans_right = 11
						if resp_mpx==2:
							ans_right = 12
						if resp_mpx==3:
							ans_right = 13
						if resp_mpx==4:
							ans_right = 14
						tot_res = gaca.check_correctness(ans_right,one_game.ques_ans[q].answer1)
						scon = one_game.results[q]
						scon.num_given_ans+=1
						if tot_res:
							print("TRUE, CORRECT answer")
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW NEW NUM CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							#reaction start here good boy
							gare.right_answer('serial fake 9c')			
							print("ripasso ancora provo AGAIN ci voglion i risultati2  ERRORS{}".format(one_game.results[q].num_errors))
							print("ripasso ancora provo AGAIN ci voglion i risultati2 CORRECT{}".format(one_game.results[q].num_corrects))
							print("ripasso ancora provo AGAIN ci voglion i risultati2 INDECISION{}".format(one_game.results[q].num_indecisions))
						else: 
							print("FALSE, wrong answer")
							# start bad reaction
							gare.wrong_answer('ser wrong') #start again from beginning with multiple answer , repeat both
							#avro due audio diversi quindi proviamo
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee {}".format(one_game.results[q].num_errors))
							continue
					elif one_game.this_type in ['9CNSPA','9CNSPX','9CNSSU'] or (one_game.this_type=='MISCELLANEOUS' and one_game.listaTypes[q] in ['9CNSPA','9CNSPX','9CNSSU']): #"Tocca due volte il leone rosso",6 ;#"Tocca una volta il disegno in alto  a sinistra",7 ;#"Tocca due volte il bottone rosso",3
						print("entro in letsplay 9CNSSU or 9CNSPA or 9CNSPX")
						ans_array, wait_time_array= gare.wait_multiple_answer(one_game.ques_ans[q].required_time)
						if ans_array[0]==30: #INDECISION
							too_many_consecutive_indecisions+=1
							too_many_consecutive_wrong_ans = 0
							flag_time_elapsed = 1
							gare.express_indecision('sono_il_seriale')
							take_sco = one_game.results[q]
							take_sco.num_indecisions+=1
							print("NEW NUM INDECISION di take sco {}".format(one_game.results[q].num_indecisions))
							continue
						too_many_consecutive_indecisions = 0
						print("ho superato il continue 9C")											
						if one_game.ques_ans[q].answer2==5: #c'è da dare solo una risp
							last_ans_reached_flag_at_ans_num = 0
							if len(ans_array)==1:
								tot_res = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
								print("single res_ottenuta in play_the_game con solo una risp da dare {}".format(tot_res))
							else:
								print("mi aspetto piu risp")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3==5: #mi aspetto 2 risposte
							if len(ans_array)==2:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									tot_res1 = res1 or res2 
									print("both res_ottenute 2 in play_the_game con 2 risp da dare")
									last_ans_reached_flag_at_ans_num = 1
							if len(ans_array)<2:
								print("mi aspetto piu risposte")
								continue
							if len(ans_array)>2:
								print("mi aspetto meno risposte")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4==5: #mi aspetto 3 risposte
							if len(ans_array)==3:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									#check first ans
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
									tot_res1 = res1 or res2 or res3
									#check second ans
									print("both res_ottenute 3 in play_the_game con 3 risp da dare")
									last_ans_reached_flag_at_ans_num = 2 
							if len(ans_array)<3:
								print("mi aspetto piu risposte")
								continue
							if len(ans_array)>3:
								print("mi aspetto meno risposte")
								continue
						elif one_game.ques_ans[q].answer2=5 and one_game.ques_ans[q].answer3=5 and one_game.ques_ans[q].answer4=5: #mi aspetto 4 risposte
							last_ans_reached_flag_at_ans_num = 3
							if len(ans_array)==4:
								duplic_array=[item for item, count in Counter(ans_array).items() if count > 1]
								if len(duplic_array):
									#redo response given more times
									print("duplicates are {}".format(duplic_array))
									print("same ans given more time")
									continue
								else:
									res1 = gaca.check_correctness(ans_array[0],one_game.ques_ans[q].answer1)
									res2 = gaca.check_correctness(ans_array[1],one_game.ques_ans[q].answer2)
									res3 = gaca.check_correctness(ans_array[2],one_game.ques_ans[q].answer3)
									res4 = gaca.check_correctness(ans_array[3],one_game.ques_ans[q].answer4)
									tot_res1 = res1 or res2 or res3 or res4
									print("both res_ottenute 4 in play_the_game con 4 risp da dare")
									last_ans_reached_flag_at_ans_num = 3 
									tot_res = tot_res1 or tot_res2 or tot_res3 or tot_res4 
							if len(ans_array)<4:
								print("mi aspetto piu risposte")
								continue
							if len(ans_array)>4:
								print("mi aspetto meno risposte")
								continue
						scon = one_game.results[q]
						scon.num_given_ans+=1
						if tot_res:
							scoc = one_game.results[q]
							scoc.num_corrects+=1
							too_many_consecutive_wrong_ans = 0
							print("NEW CORRETTE di scoc {}".format(one_game.results[q].num_corrects))
							print("correct response:esatta")
							gare.right_answer('ser_right_resp')
							q+=1
						else:
							print("FALSE, wrong answer")
							gare.wrong_answer('false ser') 
							too_many_consecutive_wrong_ans+=1
							#start again from beginning with multiple answer , repeat both
							scoee = one_game.results[q]
							scoee.num_errors+=1
							print("NEW NEW NUM errors di scoee in array multiple {}".format(one_game.results[q].num_errors))
							continue
			else:
				raise PlayGameException("error during game:")
	except PlayGameException:
		q = one_game.num_of_questions
		everything_went_well='error'
		break 
	return everything_went_well
