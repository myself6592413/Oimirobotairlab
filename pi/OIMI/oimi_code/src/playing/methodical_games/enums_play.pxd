from libc.stdint cimport uint32_t

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _GameState 'enums_play::GameState':
		_wait 'enums_play::GameState::wait'
		_question  'enums_play::GameState::question'
		_start 'enums_play::GameState::start'
		_stop  'enums_play::GameState::stop'

cpdef enum GameState:
	wait = <uint32_t> _wait
	question  = <uint32_t> _question
	start = <uint32_t> _start
	stop  = <uint32_t> _stop

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _PatchCategory 'enums_play::PatchCategory':
		_solidcolor 'enums_play::PatchCategory::solidcolor'
		_animal 'enums_play::PatchCategory::animal'
		_food  'enums_play::PatchCategory::food'
		_dress	'enums_play::PatchCategory::dress'
		_thing	'enums_play::PatchCategory::thing'
		_human	'enums_play::PatchCategory::human'

cpdef enum PatchCategory:
	solidcolor = <uint32_t> _solidcolor
	animal = <uint32_t> _animal
	food  = <uint32_t> _food
	dress  = <uint32_t> _dress
	thing  = <uint32_t> _thing
	human  = <uint32_t> _human

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _GameCategory 'enums_play::GameCategory':
		_solidcolors 'enums_play::GameCategory::solidcolors'
		_animals 'enums_play::GameCategory::animals'
		_foods	'enums_play::GameCategory::foods'
		_dresses  'enums_play::GameCategory::dresses'
		_things  'enums_play::GameCategory::things'
		_humans  'enums_play::GameCategory::humans'
		_miscellanous 'enums_play::GameCategory::miscellanous'

cpdef enum GameCategory:
	solidcolors = <uint32_t> _solidcolors
	animals = <uint32_t> _animals
	foods  = <uint32_t> _foods
	dresses  = <uint32_t> _dresses
	things	= <uint32_t> _things
	humans	= <uint32_t> _humans
	miscellanous  = <uint32_t> _miscellanous

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _Color 'enums_play::Color':
		_red	'enums_play::Color::red'
		_blue	'enums_play::Color::blue'
		_yellow 'enums_play::Color::yellow'
		_green	'enums_play::Color::green'

cpdef enum Color:
	red = <uint32_t> _red
	blue  = <uint32_t> _blue
	yellow = <uint32_t> _yellow
	green = <uint32_t> _green

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _Subject 'enums_play::Subject':
		_plain 'enums_play::Subject::plain'
		_monkey 'enums_play::Subject::monkey'
		_octopus  'enums_play::Subject::octopus'
		_parrot 'enums_play::Subject::parrot'
		_lion  'enums_play::Subject::lion'
		_girl  'enums_play::Subject::girl'
		_glasses  'enums_play::Subject::glasses'
		_umbrella  'enums_play::Subject::umbrella'
		_bicycle 'enums_play::Subject::bicycle'
		_push_scooter 'enums_play::Subject::push_scooter'
		_icecream  'enums_play::Subject::icecream'

cpdef enum Subject:
	plain = <uint32_t> _plain
	monkey = <uint32_t> _monkey
	octopus  = <uint32_t> _octopus
	parrot = <uint32_t> _parrot
	lion  = <uint32_t> _lion
	girl = <uint32_t> _girl
	glasses = <uint32_t> _glasses
	umbrella = <uint32_t> _umbrella
	bicycle = <uint32_t> _bicycle
	push_scooter = <uint32_t> _push_scooter
	icecream = <uint32_t> _icecream


cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _GameDifficulty 'enums_play::GameDifficulty':
		_easy_g_1 'enums_play::GameDifficulty::easy_g_1'
		_easy_g_2 'enums_play::GameDifficulty::easy_g_2'
		_normal_g_1 'enums_play::GameDifficulty::normal_g_1'
		_normal_g_2 'enums_play::GameDifficulty::normal_g_2'
		_medium_g_1 'enums_play::GameDifficulty::medium_g_1'
		_medium_g_2 'enums_play::GameDifficulty::medium_g_2'
		_hard_g_1 'enums_play::GameDifficulty::hard_g_1'
		_hard_g_2 'enums_play::GameDifficulty::hard_g_2'

cpdef enum GameDifficulty:
	easy_g_1 = <uint32_t> _easy_g_1
	normal_g_1	= <uint32_t> _normal_g_1
	medium_g_1 = <uint32_t> _medium_g_1
	hard_g_1 = <uint32_t> _hard_g_1
	easy_g_2 = <uint32_t> _easy_g_2
	normal_g_2	= <uint32_t> _normal_g_2
	medium_g_2 = <uint32_t> _medium_g_2
	hard_g_2 = <uint32_t> _hard_g_2

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _MatchDifficulty 'enums_play::MatchDifficulty':
		_easy_m_1 'enums_play::MatchDifficulty::easy_m_1'
		_normal_m_1  'enums_play::MatchDifficulty::normal_m_1'
		_medium_m_1 'enums_play::MatchDifficulty::medium_m_1'
		_hard_m_1  'enums_play::MatchDifficulty::hard_m_1'
		_easy_m_2 'enums_play::MatchDifficulty::easy_m_2'
		_normal_m_2  'enums_play::MatchDifficulty::normal_m_2'
		_medium_m_2 'enums_play::MatchDifficulty::medium_m_2'
		_hard_m_2  'enums_play::MatchDifficulty::hard_m_2'

cpdef enum MatchDifficulty:
	easy_m_1 = <uint32_t> _easy_m_1
	normal_m_1	= <uint32_t> _normal_m_1
	medium_m_1 = <uint32_t> _medium_m_1
	hard_m_1 = <uint32_t> _hard_m_1
	easy_m_2 = <uint32_t> _easy_m_2
	normal_m_2	= <uint32_t> _normal_m_2
	medium_m_2 = <uint32_t> _medium_m_2
	hard_m_2 = <uint32_t> _hard_m_2

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _SessionDifficulty 'enums_play::SessionDifficulty':
		_easy_s 'enums_play::SessionDifficulty::easy_s'
		_normal_s  'enums_play::SessionDifficulty::normal_s'
		_medium_s 'enums_play::SessionDifficulty::medium_s'
		_hard_s  'enums_play::SessionDifficulty::hard_s'

cpdef enum SessionDifficulty:
	easy_s = <uint32_t> _easy_s
	normal_s  = <uint32_t> _normal_s
	medium_s = <uint32_t> _medium_s
	hard_s = <uint32_t> _hard_s

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _OrderOfDifficulty 'enums_play::OrderOfDifficulty':
		_equivalent 'enums_play::OrderOfDifficulty::equivalent' 
		_asc 'enums_play::OrderOfDifficulty::asc'
		_desc  'enums_play::OrderOfDifficulty::desc'
		_random 'enums_play::OrderOfDifficulty::random'

cpdef enum OrderOfDifficulty:
	equivalent = <uint32_t> _equivalent
	asc = <uint32_t> _asc
	desc  = <uint32_t> _desc
	random = <uint32_t> _random

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _NumberIncrement 'enums_play::NumberIncrement':
		_same 'enums_play::NumberIncrement::same'
		_grow  'enums_play::NumberIncrement::grow'
		_lessen 'enums_play::NumberIncrement::lessen'
		_casual 'enums_play::NumberIncrement::casual'

cpdef enum NumberIncrement:
	same = <uint32_t> _same
	grow  = <uint32_t> _grow
	lessen = <uint32_t> _lessen
	casual = <uint32_t> _casual

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _KidLevel 'enums_play::KidLevel':
		_beginner 'enums_play::KidLevel::beginner'
		_elementary  'enums_play::KidLevel::elementary'
		_intermediate 'enums_play::KidLevel::intermediate'
		_advanced 'enums_play::KidLevel::advanced'

cpdef enum KidLevel:
	beginner = <uint32_t> _beginner
	elementary	= <uint32_t> _elementary
	intermediate = <uint32_t> _intermediate
	advanced = <uint32_t> _advanced

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _Result 'enums_play::Feedback':
		_awful 'enums_play::Feedback::awful'
		_bad 'enums_play::Feedback::bad'
		_soandso 'enums_play::Feedback::soandso'
		_ok 'enums_play::Feedback::ok'
		_fine 'enums_play::Feedback::fine'
		_good 'enums_play::Feedback::good'
		_verygood 'enums_play::Feedback::verygood'
		_almostperfect	'enums_play::Feedback::almostperfect'
		_perfect  'enums_play::Feedback::perfect'

cpdef enum Feedback:
	awful = <uint32_t> _awful
	bad = <uint32_t> _bad
	soandso = <uint32_t> _soandso
	ok = <uint32_t> _ok
	fine = <uint32_t> _fine
	good = <uint32_t> _good
	verygood = <uint32_t> _verygood
	almostperfect = <uint32_t> _almostperfect
	perfect = <uint32_t> _perfect
	
cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _QuestionKind 'enums_play::QuestionKind':
		_k1F 'enums_play::QuestionKind::k1F'
		_k2L 'enums_play::QuestionKind::k2L'
		_k3S 'enums_play::QuestionKind::k3S'
		_k4P 'enums_play::QuestionKind::k4P'
		_k5K 'enums_play::QuestionKind::k5K'
		_k6I 'enums_play::QuestionKind::k6I'
		_k7O 'enums_play::QuestionKind::k7O'
		_k8Q 'enums_play::QuestionKind::k8Q'
		_k9C 'enums_play::QuestionKind::k9C'
		_k10A 'enums_play::QuestionKind::k10A'

cpdef enum QuestionKind:
	k1F = <uint32_t> _k1F
	k2L = <uint32_t> _k2L
	k3S = <uint32_t> _k3S 
	k4P = <uint32_t> _k4P
	k5K = <uint32_t> _k5K
	k6I = <uint32_t> _k6I
	k7O = <uint32_t> _k7O
	k8Q = <uint32_t> _k8Q
	k9C = <uint32_t> _k9C
	k10A = <uint32_t> _k10A

cdef extern from "enums_play.h" namespace 'enums_play':
	cdef enum _QuestionType 'enums_play::QuestionType':
		_t1FSUS 'enums_play::QuestionType:: t1FSUS'
		_t1FSUD 'enums_play::QuestionType:: t1FSUD'
		_t1FSUDT 'enums_play::QuestionType:: t1FSUDT'
		_t1FCOS 'enums_play::QuestionType:: t1FCOS'
		_t1FCOD 'enums_play::QuestionType:: t1FCOD'
		_t1FCODT 'enums_play::QuestionType:: t1FCODT'
		_t1FSUCOS 'enums_play::QuestionType:: t1FSUCOS'
		_t1FSUCOD 'enums_play::QuestionType:: t1FSUCOD'
		_t1FSUCODT 'enums_play::QuestionType:: t1FSUCODT'
		_t1FGCA 'enums_play::QuestionType:: t1FGCA'
		_t1FCACO 'enums_play::QuestionType:: t1FCACO'
		_t1FCACOA 'enums_play::QuestionType:: t1FCACOA'
		_t2LST 'enums_play::QuestionType:: t2LST'
		_t2LSU 'enums_play::QuestionType:: t2LSU'
		_t2LTWO 'enums_play::QuestionType:: t2LTWO'
		_t2LDSU 'enums_play::QuestionType:: t2LDSU'
		_t2LDSUOC 'enums_play::QuestionType:: t2LDSUOC'
		_t2LDBOC 'enums_play::QuestionType:: t2LDBOC'
		_t2LDSUOB 'enums_play::QuestionType:: t2LDSUOB'
		_t2LDBOB 'enums_play::QuestionType:: t2LDBOB'
		_t2LDSU2 'enums_play::QuestionType:: t2LDSU2'
		_t3SSU 'enums_play::QuestionType:: t3SSU'
		_t3SCO 'enums_play::QuestionType:: t3SCO'
		_t4PSU 'enums_play::QuestionType:: t4PSU'
		_t4PCO 'enums_play::QuestionType:: t4PCO'
		_t5KS 'enums_play::QuestionType:: t5KS'
		_t5KD 'enums_play::QuestionType:: t5KD'
		_t6I 'enums_play::QuestionType:: t6I'
		_t7OSU2 'enums_play::QuestionType:: t7OSU2'
		_t7OSU3 'enums_play::QuestionType:: t7OSU3'
		_t7OSU4 'enums_play::QuestionType:: t7OSU4'
		_t7OCO2 'enums_play::QuestionType:: t7OCO2'
		_t7OCO3 'enums_play::QuestionType:: t7OCO3'
		_t7OCO4 'enums_play::QuestionType:: t7OCO4'
		_t7OL2 'enums_play::QuestionType:: t7OL2'
		_t7OL3 'enums_play::QuestionType:: t7OL3'
		_t8QTSUSC1 'enums_play::QuestionType:: t8QTSUSC1'
		_t8QTSUSC2 'enums_play::QuestionType:: t8QTSUSC2'
		_t8QTCOSC1 'enums_play::QuestionType:: t8QTCOSC1'
		_t8QTCOSC2 'enums_play::QuestionType:: t8QTCOSC2'
		_t8QTKNSC 'enums_play::QuestionType:: t8QTKNSC'
		_t8QTKNDC 'enums_play::QuestionType:: t8QTKNDC'
		_t8QTPRSC 'enums_play::QuestionType:: t8QTPRSC'
		_t8QTSUDC1 'enums_play::QuestionType:: t8QTSUDC1'
		_t8QTSUDC2 'enums_play::QuestionType:: t8QTSUDC2'
		_t8QTCODC1 'enums_play::QuestionType:: t8QTCODC1'
		_t8QTCODC2 'enums_play::QuestionType:: t8QTCODC2'
		_t8QTSUSB1 'enums_play::QuestionType:: t8QTSUSB1'
		_t8QTSUSB2 'enums_play::QuestionType:: t8QTSUSB2'
		_t8QTCOSB1 'enums_play::QuestionType:: t8QTCOSB1'
		_t8QTCOSB2 'enums_play::QuestionType:: t8QTCOSB2'
		_t8QTKNSB 'enums_play::QuestionType:: t8QTKNSB'
		_t8QTPRSB 'enums_play::QuestionType:: t8QTPRSB'
		_t8QTSUDB1 'enums_play::QuestionType:: t8QTSUDB1'
		_t8QTSUDB2 'enums_play::QuestionType:: t8QTSUDB2'
		_t8QTCODB1 'enums_play::QuestionType:: t8QTCODB1'
		_t8QTCODB2 'enums_play::QuestionType:: t8QTCODB2'
		_t8QTSUSCOC1 'enums_play::QuestionType:: t8QTSUSCOC1'
		_t9CNSU 'enums_play::QuestionType:: t9CNSU'
		_t9CNCO 'enums_play::QuestionType:: t9CNCO'
		_t9CNSSU 'enums_play::QuestionType:: t9CNSSU'
		_t9CNSPA 'enums_play::QuestionType:: t9CNSPA'
		_t9CNSPX 'enums_play::QuestionType:: t9CNSPX'
		_tMISCELLANEOUS 'enums_play::QuestionType:: tMISCELLANEOUS'

cpdef enum QuestionType:
	t1FSUS = <uint32_t> _t1FSUS
	t1FSUD = <uint32_t> _t1FSUD
	t1FSUDT = <uint32_t> _t1FSUDT
	t1FCOS = <uint32_t> _t1FCOS
	t1FCOD = <uint32_t> _t1FCOD
	t1FCODT = <uint32_t> _t1FCODT
	t1FSUCOS = <uint32_t> _t1FSUCOS
	t1FSUCOD = <uint32_t> _t1FSUCOD
	t1FSUCODT = <uint32_t> _t1FSUCODT
	t1FGCA = <uint32_t> _t1FGCA
	t1FCACO = <uint32_t> _t1FCACO
	t1FCACOA = <uint32_t> _t1FCACOA
	t2LST = <uint32_t> _t2LST
	t2LSU = <uint32_t> _t2LSU
	t2LTWO = <uint32_t> _t2LTWO
	t2LDSU = <uint32_t> _t2LDSU
	t2LDSUOC = <uint32_t> _t2LDSUOC
	t2LDBOC = <uint32_t> _t2LDBOC
	t2LDSUOB = <uint32_t> _t2LDSUOB
	t2LDBOB = <uint32_t> _t2LDBOB
	t2LDSU2 = <uint32_t> _t2LDSU2
	t3SSU = <uint32_t> _t3SSU
	t3SCO = <uint32_t> _t3SCO
	t4PSU = <uint32_t> _t4PSU
	t4PCO = <uint32_t> _t4PCO
	t5KS = <uint32_t> _t5KS
	t5KD = <uint32_t> _t5KD
	t6I = <uint32_t> _t6I
	t7OSU2 = <uint32_t> _t7OSU2
	t7OSU3 = <uint32_t> _t7OSU3
	t7OSU4 = <uint32_t> _t7OSU4
	t7OCO2 = <uint32_t> _t7OCO2
	t7OCO3 = <uint32_t> _t7OCO3
	t7OCO4 = <uint32_t> _t7OCO4
	t7OL2 = <uint32_t> _t7OL2
	t7OL3 = <uint32_t> _t7OL3
	t8QTSUSC1 = <uint32_t> _t8QTSUSC1
	t8QTSUSC2 = <uint32_t> _t8QTSUSC2
	t8QTCOSC1 = <uint32_t> _t8QTCOSC1
	t8QTCOSC2 = <uint32_t> _t8QTCOSC2
	t8QTKNSC = <uint32_t> _t8QTKNSC
	t8QTKNDC = <uint32_t> _t8QTKNDC
	t8QTPRSC = <uint32_t> _t8QTPRSC
	t8QTSUDC1 = <uint32_t> _t8QTSUDC1
	t8QTSUDC2 = <uint32_t> _t8QTSUDC2
	t8QTCODC1 = <uint32_t> _t8QTCODC1
	t8QTCODC2 = <uint32_t> _t8QTCODC2
	t8QTSUSB1 = <uint32_t> _t8QTSUSB1
	t8QTSUSB2 = <uint32_t> _t8QTSUSB2
	t8QTCOSB1 = <uint32_t> _t8QTCOSB1
	t8QTCOSB2 = <uint32_t> _t8QTCOSB2
	t8QTKNSB = <uint32_t> _t8QTKNSB
	t8QTPRSB = <uint32_t> _t8QTPRSB
	t8QTSUDB1 = <uint32_t> _t8QTSUDB1
	t8QTSUDB2 = <uint32_t> _t8QTSUDB2
	t8QTCODB1 = <uint32_t> _t8QTCODB1
	t8QTCODB2 = <uint32_t> _t8QTCODB2
	t8QTSUSCOC1 = <uint32_t> _t8QTSUSCOC1
	t9CNSU = <uint32_t> _t9CNSU
	t9CNCO = <uint32_t> _t9CNCO
	t9CNSSU = <uint32_t> _t9CNSSU
	t9CNSPA = <uint32_t> _t9CNSPA
	t9CNSPX = <uint32_t> _t9CNSPX
	tMISCELLANEOUS = <uint32_t> _tMISCELLANEOUS