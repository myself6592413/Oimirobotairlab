# ===========================================================================================================================
#  Imports
# ===========================================================================================================================
import serial,time,random
cimport numpy as np 
import numpy as np
ctypedef np.uint8_t uint8
# ===========================================================================================================================
#  Methods
# ===========================================================================================================================
cpdef inline uint8 check_correctness(int resp, int ans):
	print('calculations stampa correct {}'.format(ans))
	if resp == ans: return True
	else : return False	

cpdef inline percentage(part, whole):
	if whole==0:
		return 0
	else:
		return round(part/whole,2)
