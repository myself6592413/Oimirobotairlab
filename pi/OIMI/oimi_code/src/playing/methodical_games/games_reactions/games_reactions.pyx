""" Oimi reactions
	Notes 
		called by scenario,game,logic
"""
# =============================================================================
# Cython directives
# =============================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=Fals
#cython: cdivision=True
#cython: nonecheck=False
# =============================================================================
# Imports
# =============================================================================
import os 
import sys
import time
import random
from enums_play cimport Subject
from enums_play cimport Shape
from enums_play cimport Color
from enums_play cimport Feedback
from subprocess import run 
cimport sounds as sounds
import games_reactions as capa
# -------------------------------------------------------------------------------------
# Sounds
# -------------------------------------------------------------------------------------
cpdef start_audio(audio):
	comm = (audio).to_bytes(2, 'little')
	sounds.play_audio(comm)

cpdef start_audio_order(audio):
	sounds.play_audio(audio.encode('UTF-8', 'strict'))

cpdef make_question(audio):
	''' just for helping in comphrension in games/insertpatch classes'''
	start_audio(audio)
	time.sleep(1)

cpdef make_question_with_led(audio, led_color, ser):
	start_audio(audio)
	ser.led_co(led_color)
# -------------------------------------------------------------------------------------
# General react
# -------------------------------------------------------------------------------------
#cdef react(int audio,char* led_color,ser):
def react(audio, led_color, ser):
	#print("led arduino")
	ser.led_co(led_color)
	start_audio(audio)
# -------------------------------------------------------------------------------------
# Behaviour 
# -------------------------------------------------------------------------------------
def behave(ser, Feedback feedback):
	#print("feedback")
	cdef:
		char* led = b'<cele>'	
		int[3] audios_bad = [20,21,22]
		int[3] audios_ok = [30,31,32]
		int[3] audios_good = [40,41,42]
		int ra = random.randint(0,2)
	#print('random is{}'.format(ra))
	if feedback < Feedback.ok:
		if ra == 0 : react(audios_bad[0],led,ser)
		elif ra == 1: react(audios_bad[1],led,ser)
		else: react(audios_bad[2],led,ser)
	elif Feedback.ok <= feedback < Feedback.verygood:
		if ra == 0 : react(audios_ok[0],led,ser)
		elif ra == 1: react(audios_ok[1],led,ser)
		else: react(audios_ok[2],led,ser)
	elif feedback >= Feedback.verygood:
		if ra == 0 : react(audios_good[0],led,ser)
		elif ra == 1: react(audios_good[1],led,ser)
		else: react(audios_good[2],led,ser)

def celebrate(ser):	#after one match
	#print("yeah celebration")	
	cdef:
		char* led = b'<cele>'
		int[3] audio = [52,53,54]
		int ra = random.randint(0,2)
	#print('random is{}'.format(ra))
	if ra == 0 : react(audio[0],led,ser)
	elif ra == 1: react(audio[1],led,ser)
	else: react(audio[2],led,ser)

def celebrate_with_mov(ser, move_comma, led_col, animation, for_speed,ang_speed):	#after one match
	'''the order matters '''
	#print("yeah celebration with mov")
	cdef:
		int[3] audio = [52,53,54]
		int audio_music = 127  #poi metto audio music corretto
		int ra = random.randint(0,2)
		int choosen_audio
	if ra == 0 : choosen_audio = audio[0]
	elif ra == 1: choosen_audio = audio[1]
	else: choosen_audio = audio[2]
	ser.react_results_final(move_comma,led_col,animation,for_speed,ang_speed,choosen_audio)
	start_audio(choosen_audio)

def acknowledge_with_mov(ser, move_comma, led_col,animation, for_speed,ang_speed):	#after one match
	#print("yeah aknowledgment with mov")
	cdef:
		int[3] audio = [52,53,54]
		int audio_music = 127  #poi metto audio music corretto
		int ra = random.randint(0,2)
		int choosen_audio
	if ra == 0 : choosen_audio = audio[0]
	elif ra == 1: choosen_audio = audio[1]
	else: choosen_audio = audio[2]
	ser.react_results_final(move_comma,led_col,animation,for_speed,ang_speed,choosen_audio)
	start_audio(choosen_audio)

def disapprove_with_mov(ser, move_comma, led_col,animation, for_speed,ang_speed):	#after one match
	#print("ok disapprove with mov")
	cdef:
		char* led
		int[3] audio = [55,56,57]
		int audio_music = 127 #poi metto audio music corretto
		int ra = random.randint(0,2)
		int choosen_audio
	if ra == 0 : choosen_audio = audio[0]
	elif ra == 1: choosen_audio = audio[1]
	else: choosen_audio = audio[2]
	ser.react_results_final(move_comma,led_col,animation,for_speed,ang_speed,choosen_audio)
	start_audio(choosen_audio)
	
def react_to_results(feed, ser):
	#print("entriamo nella reazione")
	#print(feed)
	cdef: 
		float for_speed
		float ang_speed
		str led_col
		str animation
		str move_comma
	if feed == Feedback.awful:
		for_speed = 20
		ang_speed = 1.5
		led_col = "red"
		animation = "stable"
		move_comma = "sad"
		disapprove_with_mov(ser,move_comma,led_col,animation,for_speed,ang_speed)
	elif feed == Feedback.bad:
		for_speed = 20
		ang_speed = 1.5
		led_col = "red"
		animation = "stable"
		move_comma = "sad"
		disapprove_with_mov(ser,move_comma,led_col,animation,for_speed,ang_speed)
	elif feed == Feedback.soandso:
		for_speed = 20
		ang_speed = 1.5
		led_col = "red"
		animation = "stable"
		move_comma = "sad"
		acknowledge_with_mov(ser,move_comma,led_col,animation,for_speed,ang_speed)
	elif feed == Feedback.ok:
		for_speed = 15
		ang_speed = 1.5
		led_col = "green"
		animation = "stable"
		move_comma = "normal"
		acknowledge_with_mov(ser,move_comma,led_col,animation,for_speed,ang_speed)
	elif feed == Feedback.fine:
		for_speed = 15
		ang_speed = 1.5
		led_col = "green"
		animation = "stable"
		move_comma = "normal"
		acknowledge_with_mov(ser,move_comma,led_col,animation,for_speed,ang_speed)
	elif feed == Feedback.good:
		for_speed = 15
		ang_speed = 1.5
		led_col = "blue"
		animation = "stable"
		move_comma = "happy"
		celebrate_with_mov(ser,move_comma,led_col,animation,for_speed,ang_speed)
	elif feed == 6:
		##print("VEEEEERRRRYYY GOOOD")
		for_speed = 15
		ang_speed = 1.5
		move_comma = "happy"
		animation = "stable"
		led_col = "blue"
		celebrate_with_mov(ser,move_comma,led_col,animation,for_speed,ang_speed)
	elif feed == Feedback.almostperfect:
		for_speed = 15
		ang_speed = 1.5
		move_comma = "enthusiast"
		animation = "stable"
		led_col = "rainbow"
		celebrate_with_mov(ser,move_comma,led_col,animation,for_speed,ang_speed)
	elif feed == Feedback.perfect:
		for_speed = 20
		animation = "stable"
		ang_speed = 1.8
		move_comma = "enthusiast" #same for many reaction with also param passed to arduino 
		led_col = "rainbow"
		celebrate_with_mov(ser,move_comma,led_col,animation,for_speed, ang_speed)		
		
def console(ser): #after one match
	##print("console me")
	cdef:
		char* led = b'<cons>'
		int[3] audio = [55,56,57]
		int ra = random.randint(0,2)
	##print('random is{}'.format(ra))
	if ra==1: react(audio[0],led,ser)
	elif ra ==2: react(audio[1],led,ser)
	else: react(audio[2],led,ser)

def right_answer(ser): #after one question
	##print("right_answer")
	cdef:
		char* led = b'<right>'
		int[3] audio = [52,53,54]
		int ra = random.randint(0,2)
	##print('random is{}'.format(ra))
	if ra == 0 : react(audio[0],led,ser)
	elif ra == 1: react(audio[1],led,ser)
	else: react(audio[2],led,ser)

def wrong_answer(ser): #after one question
	#print("wrong_answer")
	cdef:
		char* led = b'<wrong>'
		int[3] audio = [55,56,57]
		int ra = random.randint(0,2)
	#print('random is{}'.format(ra))
	if ra==1: react(audio[0],led,ser)
	elif ra ==2: react(audio[1],led,ser)
	else: react(audio[2],led,ser)

def express_indecision(ser):
	cdef char* led_elapsed = b'<elaps>'
	cdef int audio_time_elapsed = 58 
	react(audio_time_elapsed,led_elapsed,ser)
			
def already_given_answer(ser):
	cdef char* led_elapsed = b'<already>'
	cdef int audio_time_elapsed = 143 
	react(audio_time_elapsed,led_elapsed,ser)
# -------------------------------------------------------------------------------------
# Await ansewers
# -------------------------------------------------------------------------------------
def wait_mpx(ser):
	risp = ser.takempx()
	return risp 

def wait_answer(maxtime):
	cdef int i
	start_wait_time=time.time()
	#print("waiting_answer")
	i = capa.fun_mpr121(maxtime)
	end_wait_time = time.time()
	total_wait_time = end_wait_time - start_wait_time
	return i, total_wait_time