/* Enum classes = paired to its .pxd 
*/
namespace enums_play {

	enum class GameState : uint32_t {
		wait = 0,
		question = 1,
		start = 2,
		stop = 3
	};
	enum class PatchCategory : uint32_t {
		solidcolor = 0,
		animal = 1,
		food = 2,
		dress = 3,
		thing = 4,
		human = 5
	};
	enum class GameCategory : uint32_t {
		solidcolors = 0,
		animals = 1,
		foods = 2,
		dresses = 3,
		things = 4,
		humans = 5,
		miscellanous = 6
	};
	enum class Subject : uint32_t {
		plain= 0,
		monkey = 1,
		octopus = 2,
		parrot = 3,
		lion = 4,
		girl = 5,
		glasses = 6,
		umbrella = 7,
		bicycle = 8,
		push_scooter = 9,
		icecream = 10
	};

	enum class GameDifficulty : uint32_t {		easy_g_1 = 0,
		easy_g_2 = 1,
		normal_g_1 = 2,
		normal_g_2 = 3,
		medium_g_1 = 4,
		medium_g_2 = 5,
		hard_g_1 = 6,
		hard_g_2 = 7
	};
	enum class MatchDifficulty : uint32_t {
		easy_m_1 = 0,
		easy_m_2 = 1,
		normal_m_1 = 2,
		normal_m_2 = 3,
		medium_m_1 = 4,
		medium_m_2 = 5,
		hard_m_1 = 6,
		hard_m_2 = 7
	};
	enum class SessionDifficulty : uint32_t {
		easy_s = 0,
		normal_s = 1,
		medium_s = 2,
		hard_s = 3
	};
	enum class QuestionKind : uint32_t {
		k1F = 0,
		k2L = 1, 
		k3S = 2, 
		k4P = 3, 
		k5K = 4,
		k6I = 5,
		k7O = 6,
		k8Q = 7,
		k9C = 8,
		k10A = 9
	};
	enum class QuestionType : uint32_t {
		t1FSUS = 0,
		t1FSUD = 1,
		t1FSUDT = 2,
		t1FCOS = 3,
		t1FCOD = 4,
		t1FCODT = 5,
		t1FSUCOS = 6,
		t1FSUCOD = 7,
		t1FSUCODT = 8,
		t1FGCA = 9,
		t1FCACO = 10,
		t1FCACOA = 11,
		t2LST = 12,
		t2LSU = 13,
		t2LTWO = 14,
		t2LDSU = 15,
		t2LDSUOC = 16,
		t2LDBOC = 17,
		t2LDSUOB = 18,
		t2LDBOB = 19,
		t2LDSU2 = 20,
		t3SSU = 21,
		t3SCO = 22,
		t4PSU = 23,
		t4PCO = 24,
		t5KS = 25,
		t5KD = 26,
		t6I = 27,
		t7OSU2 = 28,
		t7OSU3 = 29,
		t7OSU4 = 30,
		t7OCO2 = 31,
		t7OCO3 = 32,
		t7OCO4 = 33,
		t7OL2 = 34,
		t7OL3 = 35,
		t8QTSUSC1 = 36,
		t8QTSUSC2 = 37,
		t8QTCOSC1 = 38,
		t8QTCOSC2 = 39,
		t8QTKNSC = 40,
		t8QTKNDC = 41,
		t8QTPRSC = 42,
		t8QTSUDC1 = 43,
		t8QTSUDC2 = 44,
		t8QTCODC1 = 45,
		t8QTCODC2 = 46,
		t8QTSUSB1 = 47,
		t8QTSUSB2 = 48,
		t8QTCOSB1 = 49,
		t8QTCOSB2 = 50,
		t8QTKNSB = 51,
		t8QTPRSB = 52,
		t8QTSUDB1 = 53,
		t8QTSUDB2 = 54,
		t8QTCODB1 = 55,
		t8QTCODB2 = 56,
		t8QTSUSCOC1 = 57,
		t9CNSU = 58,
		t9CNCO = 59,
		t9CNSSU = 60,
		t9CNSPA = 61,
		t9CNSPX = 62,
		tMISCELLANEOUS = 63
	};
	
	enum class KidLevel : uint32_t{
		beginner = 0,
		elementary = 1,
		intermediate = 2,
		advanced = 3
	};

	enum class Color : uint32_t{
		red = 0,
		blue = 1,
		yellow = 2,
		green = 3
	};

	enum class OrderOfDifficulty : uint32_t{
		equivalent = 0,
		asc = 1,
		desc = 2,
		random = 3
	};
	enum class NumberIncrement : uint32_t{
		same = 0,
		grow = 1,
		lessen = 2,
		casual = 3
	};
	enum class Feedback : uint32_t{
		awful = 0,
		bad = 1,
		soandso = 2,
		ok = 3,
		fine = 4,
		good = 5,
		verygood = 6,
		almostperfect = 7,
		perfect = 8
	};
}