cdef class Scenario:
	cdef:
		id_scenario
		str group_scenario
		bint allow_repetitions 
		bint way_of_insertion
		bint ask_placement
		bint ask_with_position
		bint is_more_difficult
		bint already_displaced
		patches 
		df_patches
		list constraints 
		db_conn
	cpdef dispose_patches(self)
	cpdef select_scenario_fromdb(self)
	cdef create_py_scenarios(self, int id_patch_1, int id_patch_2, int id_patch_3, int id_patch_4)
