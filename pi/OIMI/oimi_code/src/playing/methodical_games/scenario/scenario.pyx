""" Oimi Scenario, contains all attributes and methods involving robot patch arrangement, used in games """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
if '/home/pi/OIMI/oimi_code/src/managers/database_manager':
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games':
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/games_classes':
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/games_classes')
import os
import pandas as pd
from games_classes cimport PyPatch
from games_classes cimport PatchesWrappingList
from games_classes cimport create_current_patches
from enums_play cimport PatchCategory
import games_reactions as gare
cimport utils_play as up
cimport scenario
from enums_play cimport Subject
from enums_play cimport Color
from enums_play cimport PatchCategory
from enums_play cimport GameCategory
from enums_play cimport Subject
from enums_play cimport Color
from enums_play cimport Feedback
from enums_play cimport QuestionKind
from enums_play cimport QuestionType
import database_manager as db 
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class Scenario:
	def __cinit__(self, id_scenario, bint way_of_insertion, bint ask_with_position, bint is_more_difficult): 
		self.id_scenario = id_scenario
		self.way_of_insertion = way_of_insertion
		self.ask_with_position = ask_with_position
		self.is_more_difficult = is_more_difficult
		self.db_conn = db.DatabaseManager()
		self.select_scenario_fromdb()
		#self.dispose_patches()
	def __enter__(self):
		print("Starting Scenario configuration...")

	def __exit__(self, exc_type, exc_val, exc_tb):
		print("Scenario is ready.")

	def __eq__(self, other):
		cdef:
			Py_ssize_t i = 0
			Py_ssize_t lenght = 4
			bint compare = 1 
		for i in range (lenght):
			if self.patches[i].subject[i]!=other.patches[i].subject[i] or \
			self.patches[i].color[i]!=other.patches[i].color[i]:
				compare = 0
				break
		if not isinstance(other, Scenario):
			return NotImplemented
		return compare
	# -----------------------------------------------------------------------------------------------------------------------------------------------------
	# Dispose patch if way_of_insertion is guided = True 
	# -----------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef dispose_patches(self):
		print("entro in dispose")
		cdef:
			int wait_time = 5
			Py_ssize_t l = 0
			str choose_that_patch
			list list_already_putted = [6]
			str audio_which_scenario = 'audio_49' # "scegli lo scenario!"
			int[4] subjects = [0,0,0,0]
			int[4] colors = [0,0,0,0]
			int[4] genres = [0,0,0,0]
		if self.ask_with_position:
			gare.start_audio(audio_which_scenario)
			#print("selection where u want enter")
			#print(self.subjects[0])
			#print(self.colors[0])
			##no shuffle is useless for don't complicate things asked in order 
			##random.shuffle(self.subjects)
			print(self.patches[0].subject)
			print(self.patches[1].subject)
			print(self.patches[2].subject)
			print(self.patches[3].subject)
			for l in range(4):
				su = self.patches[l].subject
				co = self.patches[l].color
				choose_that_patch = up.select_audio(0, su,co)
				print("type choose_that_patch is {}", type(choose_that_patch))
				gare.start_audio_order(choose_that_patch) #create new audio!!
				ans, wait_time = gare.wait_answer(5)
				ans = 3 
				if self.is_more_difficult:
					wait_time = 10 
				else:
					wait_time = 5 
				if ans==5: #no response
					gare.express_indecision(ser)
					print("Error too much time is passed!!")
					continue
				if ans in list_already_putted:
					gare.already_given_answer(ser)
					print("Error! position already choosen! Choose another one")
					continue
				if ans not in list_already_putted and ans==l:
					gare.correct_patch_answer(ser)
					list_already_putted.append(ans)
					print("ok valid position")
				else:
					gare.wrong_given_answer(ser)
					print("Error! position already choosen! Choose another one")
					continue
				if self.is_more_difficult:
					gare.start_(audio_confirm)
					print("aggiungo controllo con body here!!")	
					print("tocca qui!")
		else:
			su0 = self.patches[0].subject
			co0 = self.patches[0].color
			print(su0)
			print(su0)
			print("-----")
			su1 = self.patches[1].subject
			co1 = self.patches[1].color
			print(su1)
			print(su1)
			print("-----")
			su2 = self.patches[2].subject
			co2 = self.patches[2].color
			print(su2)
			print(su2)
			print("-----")
			su3 = self.patches[3].subject
			co3 = self.patches[3].color
			print(su3)
			print(su3)
			print("-----")
			subjects = [su0,su1,su2,su3]
			colors = [co0,co1,co2,co3]
			#genres = [PatchCategory.animal,PatchCategory.animal,PatchCategory.animal,PatchCategory.animal]
			genres = up.find_genres(subjects)
			for k in range(4):
				print("subjects:")
				print(subjects[k])
				print("colors:")
				print(colors[k])
				print("genres:")
				print(genres[k])
			for i in range(4):
				choose_that_patch = up.select_audio(1, subjects[i], colors[i])
				print("type choose_that_patch is for audio")
				print(type(choose_that_patch))
				gare.start_audio_order(choose_that_patch)				
				ans, wait_time = gare.wait_answer(5)
				ans = 1 
				if self.is_more_difficult:
					wait_time = 10 
				else:
					wait_time = 5 
				if ans==5:
					gare.express_indecision(ser)
					print("Error too much time is passed!!") #create exception in custom_Ex??
					continue
				if ans in list_already_putted:
					gare.already_given_answer(ser)
					print("Error! position already choosen! Choose another one")
					continue
				if ans not in list_already_putted:
					if i<3:
						gare.correct_patch_answer(ser)
						list_already_putted.append(ans)
						temp_sub = subjects[ans]
						subjects[ans] = subjects[i]
						subjects[i] = temp_sub
						temp_col = colors[ans]
						colors[ans] = colors[i]
						colors[i] = temp_sub
						print("choose_that_patch new position is {}".format(ans))
			self.patches = create_current_patches(subjects, colors, genres)
	# -----------------------------------------------------------------------------------------------------------------------------------------------------
	# Predetermined way extract scenario
	# -----------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef select_scenario_fromdb(self):
		sql_scenario = "SELECT * FROM Scenarios WHERE scenario_id = ?"
		patch_is = self.db_conn.execute_param_query(sql_scenario, self.id_scenario)
		subj,colo,genr = self.create_py_scenarios(patch_is[2],patch_is[3],patch_is[4],patch_is[5])
		cdef:
			int[4] subjects = subj
			int[4] colors = colo
			int[4] genres = genr
		cdef PatchesWrappingList patches = create_current_patches(subjects, colors, genres) #inside pxd file!
		self.patches = patches
		sql_group = "SELECT scenario_group FROM Scenarios WHERE scenario_id = ?"
		tmp_group_scenario = self.db_conn.execute_param_query(sql_group, self.id_scenario)
		self.group_scenario = tmp_group_scenario[0]		
		print("^^^^^^^^^^^^^^")
		print("SCENARIO GROUP correct  IS {}".format(self.group_scenario))
		print("self.patches prova 0  {}".format(self.patches[0].subject))
		print("self.patches prova 1  {}".format(self.patches[1].subject))
		print("self.patches prova 2  {}".format(self.patches[2].subject))
		print("self.patches prova 3  {}".format(self.patches[3].subject))
		
	cdef create_py_scenarios(self, int id_patch_1, int id_patch_2, int id_patch_3, int id_patch_4):
		sql_patches = "SELECT * FROM Patches WHERE patch_id = ?"
		resulting_patch_1 = self.db_conn.execute_param_query(sql_patches, id_patch_1)
		sub_1 = up.opposite_conversion(resulting_patch_1[1]) 
		col_1 = up.opposite_conversion(resulting_patch_1[2])
		gen_1 = up.opposite_conversion(resulting_patch_1[3])
		sql_patches = "SELECT * FROM Patches WHERE patch_id = ?"
		resulting_patch_2 = self.db_conn.execute_param_query(sql_patches, id_patch_2)
		sub_2 = up.opposite_conversion(resulting_patch_2[1]) 
		col_2 = up.opposite_conversion(resulting_patch_2[2])
		gen_2 = up.opposite_conversion(resulting_patch_2[3])
		sql_patches = "SELECT * FROM Patches WHERE patch_id = ?"
		resulting_patch_3 = self.db_conn.execute_param_query(sql_patches, id_patch_3)
		sub_3 = up.opposite_conversion(resulting_patch_3[1]) 
		col_3 = up.opposite_conversion(resulting_patch_3[2])
		gen_3 = up.opposite_conversion(resulting_patch_3[3])
		sql_patches = "SELECT * FROM Patches WHERE patch_id = ?"
		resulting_patch_4 = self.db_conn.execute_param_query(sql_patches, id_patch_4)
		sub_4 = up.opposite_conversion(resulting_patch_4[1]) 
		col_4 = up.opposite_conversion(resulting_patch_4[2])
		gen_4 = up.opposite_conversion(resulting_patch_4[3])
		subs = [sub_1,sub_2,sub_3,sub_4]
		cols = [col_1,col_2,col_3,col_4]
		gens = [gen_1,gen_2,gen_3,gen_4]
		print(gens)
		return subs, cols, gens
	# -----------------------------------------------------------------------------------------------------------------------------------------------------
	# Test 
	# -----------------------------------------------------------------------------------------------------------------------------------------------------
	def simpleTest(self): 
		"""tranform in real real test testing normal case a priori patches """
		cdef:
			int[4] subjects = [Subject.monkey,Subject.parrot,Subject.lion,Subject.octopus]
			int[4] colors = [Color.blue,Color.green,Color.red,Color.yellow]
			int[4] genres = [PatchCategory.animal,PatchCategory.animal,PatchCategory.animal,PatchCategory.animal]
		cdef PatchesWrappingList patches = create_current_patches(subjects, colors, genres) #testing normal case a priori patches 
		self.patches = patches