# ==========================================================================================================================================================
# Documentation
# ==========================================================================================================================================================
def scenario_class_doc():
	""" Info
			Games' scenario is composed by four patches placed on oimi body capacitive sensors, 
				representing game's environment for question and answers.
			Patches arrangment may be done in two ways (in a prefixed way, randomly).	
				A)Specific prefixed modes:
					*Scenario is insert at the moment receiving data by terminal (or mobile application) through bluetooth 
					*Scenario is choosed automatically among predefinites 
					1) No control assuming that patches are already put manually, stores actual patches configuration 
						--> Can ask or not for confirmation AT THE END touching the body front?? (according to difficulty)
					2) Oimi ask to put patches one at a time (-->"insert first patch")
						waits
						detecs the position
							checks MPR capacitive sensors controlling if position was already selected 
				B)Casual mode:
					Robot will ask to put whichever patch directly to the kid (before starting effectively the match)s
					detecting where are placed in two ways:
						1) Asks also a random position according to difficulty
							Checks if there's alrady a patch in same place, checking MPR capacitive
							(-->"insert parrot blue patch into right-up place")
						2) Waits for one patch at the time	(-->"insert parrot blue patch") and detects where are put, checking with MPR as usual
		Details
			Contains db patch configuration of the match.
			Patch info are stored in permanent database.
			Db_connection speeds up extraction from db
			Completed scenario is saved into database, for restore exactly an interrupted session, or reuse same predefinite scenario in posterior games.
		Parameters 
			_way_of_insertion		# 1 for casual or 0 for predefinite mode 
			_constraints			# list of criteria to consider during random patch selection ['Genre', Animal] or ['None', 0] or ['Subject', 'Monkey', 'Genre', Animal] 
										# more than one constraint is possible! next will eventually starts from pos[2] of list 
			_allow_repetitions		# states if all patches must be selected one different from other, usually false 
			_ask_placement			# if true robot starts "game of patch placement", asking to dispose choosen patch on
			_ask_with_position		# if ask_placement is true, affirm that for each patch also the proper position is asked, otherwise is ignored 
			_is_more_difficult		# if true "game of patch placement" is less driven, thus less easy
			_already_displaced		# tells if patches are already displaced and no "game of patch placement" has to begin 
		Attributes
			-db_conn			# establishes connection to oimi_database.db address 
			-id_scenario		# univoque id of patch copied from patch_id of oimi_database
			-patches			# PatchesWrappingList 
			-df_patches			# dataframe where all patches of a scenario are stored (selected casually or not) 
			-subjects
			-colors
			-shapes
			-genres
		Methods
			:cpdef
				+select_casual_fromdb()
			
				+set_name_score()
				+update_errors()
				+update_corrects()
				+update_indecisions()
				+set_category()
				+set_complexity()
				+build_dataframe_predeterminated_patches()
			:def 
				+__cinit__()
				+__enter__() 
				+__exit__() 
				+__eq__() 
		Notes 
			Oimi is never able in any way to check if an insered path is the right one
			A Scenario is unique within a match
			A Scenario is passed to all games during a match
			Called by games_logic
		Author
			Colombo Giacomo, Airlab Polimi 2020
	"""
def select_casual_fromdb_doc():
	''' Info
			Creates patch dataframe from database tables casually between available patches according some criteria or not
		Attributes
			-element1, color1, shape1		# first patch elements	
			-element2, color2, shape2		# second patch elements
			-element3, color3, shape3		# third patch elements
			-element4, color4, shape4		# fourth patch elements
			-sql_query_specif_patches
			-search_parameter
		Details 
			According to constraints a sqlite_query is picked and sent to database_manager obj.
				=>result_q = self.db_conn.execute_a_query(sql, search_params).
				=> DISTINCT fundamental!
			Query is given by UNION (ALL) of two/three tables for selecting 4 total rows (one for each patch), for restricting the constraint to only n patches and not all. 
				=> n-m random lines + n random that respect the constraint
				(n is 1 usually, max n is 2)
				=> UNION ALL is more accurate than UNION that rarely extract only 3 lines instead of 4.
			Param for query is taken from second constraint element and then is transformed into string.
			Checks if selected patches are all different, otherwise recursively call itself.
				=> if len(self.df_patches['patch_id'].unique())!=4:

		Notes
			Called inside patch inseriment from put_patches
			Shape of all patches must be circle (temporarily), because real available patches for now are all circlular
				=> WHERE Patches.shape = 'Circle' 
				For same reason also shapes control is missing in second case when constraints are two	
			with db.DatabaseManager() allows usage of context managers class methods 
				=> eg __enter__ 
			Comma for last element is fundamental for avoiding typical runtime error "Incorrect number of bindings supplied"
				=> ...str(first_limit),]
	'''
def dispose_casual_patches():
	''' Info
			Starts "displace patches game" asking to put patches in right place and controlling everything is fine.
		Details
			Convert string into corresponding enumeration int as PatchesWrappingList obj require.
			Store arrays for PatchesWrappingList obj
				=>self.subjects = [up.opposite_conversion(self.df_patches['subject'][0]) ...
			Begins procedure for displacement on demand if ask_placement is True
				According to ask_with_position or not, the audio and checks will change, requiring also for proper position
					Prepares correct answers and reasonable time in utils_play file 
						=> choose_that_patch = up.select_audio_where_I_say(self.subjects[l],self.colors[l],self.shapes[l])
					Start asking 
						Plays right audio that include position from game_reactions file 
							=> gare.start_audio_order(choose_that_patch) #create new audio!!
						Retrieves results from game_reactions file from MPU sensors' result while putting patch
							=> ans, wait_time = gare.wait_answer(5)
						For all four patches 
							Waits for insertion 
							Reacts with indecision pattern if answer is not given (is equal to 5)
							Checks if answers is already given 
							Updates list of already given answers
							Encourages continuation if is_more_difficul is false 
							Asks from confirmation touching body if is_more_difficul is true 

			Creates at the end PatchesWrappingList py object and copies it to self.patches
		Notes
			Called by scenario_manager only if self.ask_placement is true
	'''
def store_scenario_into_db_doc():
	''' Info
			Stores a created scenario into Scenarios table. Builds also Scenarios_Patches table with extracted patches.
			Takes the id_scenario.
		Details
			Calls include_new_Scenario for adding the new scenario to oimi_db
			Receives name ???s
			Selectss the autogenerated sscenario id from table 
				=> execute_a_query(sql_take_id, 0)
			Calls place_new_patch_scenario() for creating Scenarios_Patches table 
		Notes
			Called by 
	'''	