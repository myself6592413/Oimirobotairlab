cdef class Score:
	cdef:
		str name_game_result 
		int question_ref
		int num_given_ans 
		int num_errors 
		int num_corrects
		int num_indecisions	
		int category
		int complexity_sess
		int difficulty_match
		int difficulty_game
		int waited_time

	cpdef set_nameresult(self, name_game, name_question)
	cpdef update_errors(self)
	cpdef update_corrects(self)
	cpdef update_indecisions(self)