""" Oimi Result of game, contains all attributes and methods for answers of questions in oimi game. """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class Score:
	def __cinit__(self):
		self.name_game_result = ''
		self.curr_question = 0
		self.num_given_ans = 0 
		self.num_errors = 0
		self.num_corrects = 0
		self.num_indecisions = 0 
		self.category = 0
		self.complexity_sess = 0
		self.difficulty_match = 0
		self.difficulty_game = 0
		self.waited_time = 0

	@property
	def num_given_ans(self):
		return self.num_given_ans
	@num_given_ans.setter
	def num_given_ans(self, value):
		self.num_given_ans = value
	@property
	def num_errors(self):
		return self.num_errors
	@num_errors.setter
	def num_errors(self, value):
		self.num_errors = value
	@property
	def num_corrects(self):
		return self.num_corrects
	@num_corrects.setter
	def num_corrects(self, value):
		self.num_corrects = value
	@property
	def num_indecisions(self):
		return self.num_indecisions
	@num_indecisions.setter
	def num_indecisions(self, value):
		self.num_indecisions = value
	@property
	def category(self):
		return self.category
	@category.setter
	def category(self, value):
		self.category = value
	@property
	def complexity_sess(self):
		return self.complexity_sess
	@complexity_sess.setter
	def complexity_sess(self, value):
		self.complexity_sess = value
	@property
	def difficulty_match(self):
		return self.difficulty_match
	@difficulty_match.setter
	def difficulty_match(self, value):
		self.difficulty_match = value
	@property
	def difficulty_game(self):
		return self.difficulty_game
	@difficulty_game.setter
	def difficulty_game(self, value):
		self.difficulty_game = value
	@property
	def name_game_result(self):
		return self.name_game_result
	@name_game_result.setter
	def name_game_result(self, value):
		self.name_game_result = value
	@property
	def waited_time(self):
		return self.waited_time
	@waited_time.setter
	def waited_time(self, value):
		self.waited_time = value
	@property
	def curr_question(self):
		return self.curr_question
	@curr_question.setter
	def curr_question(self, value):
		self.curr_question = value

	cpdef set_nameresult(self, name_game, name_question):
		name_to_add = 'game_{}_quest_{}_result'.format(name_game, name_question)
		print("inside score!!!! set_nameresult!!!")
		print("name_game_result {}".format(name_game))
		self.name_game_result = name_to_add
	cpdef update_errors(self):
		self.num_errors += 1 
	cpdef update_corrects(self):
		self.num_corrects += 1
	cpdef update_indecisions(self):
		self.num_indecisions += 1