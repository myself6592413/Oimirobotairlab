from enums_play cimport SessionDifficulty
cimport match as mt
cimport score as sc

cdef class SessionBase:
	cdef:
		int id_session
		int num_of_matches
		SessionDifficulty complexity
		str status
		str order_difficulty_matches
		str order_quantity_games
		bint mandatory_impositions
		bint mandatory_needs
		bint extra_time_tolerance
		bint movements_allowed
		bint body_enabled
		bint rules_explication
		bint stress_flag
		bint frequency_exhortations
		bint repeat_question
		bint repeat_intro
		int desirable_time
		str creation_way
		str audio_s_intro
		str audio_s_exit
		mt.Match[:] matches_of_this_session
		int current_match
		list session_feedbacks
		sc.Score session_scores
		db_conn
		list impositions_list
		dict user_dict_sess

	cpdef extract_ready_session(self)
	cpdef store_session_in_progress(self)
	cpdef restore_session_in_progress(self)
	cdef create_obj_session(self, int sess_id, int child_exists)
	cpdef get_dict_kind_imposition(self)
	cpdef get_dict_imposition(self)
	cpdef get_categ_imposition(self)
	cpdef final_activities_session(self)
	
cdef class Session_1(SessionBase):
	cdef:
		int id_child
		id_scenario
	cpdef extract_ready_session(self)
cdef class Session_2(SessionBase):
	cdef:
		int id_child
		list needs_list
		str level_area

	cpdef extract_ready_session(self)
	cpdef realtime_creation(self)

cdef class Session_3(SessionBase):
	cdef:
		int id_child
		list needs_list
		str level_area
		str id_scenario
	cpdef extract_ready_session(self)
	cpdef realtime_creation(self)

cdef class Session_4(SessionBase):
	cdef:
		str id_scenario
		str level_area
	cpdef extract_ready_session(self)
	cpdef realtime_creation(self)

cdef class Session_5(SessionBase):
	cdef:
		str level_area
	cpdef extract_ready_session(self)
	cpdef realtime_creation(self)

cdef class Session_6(SessionBase):
	cdef:
		str id_scenario
	cpdef extract_ready_session(self)
	cpdef realtime_creation(self)
