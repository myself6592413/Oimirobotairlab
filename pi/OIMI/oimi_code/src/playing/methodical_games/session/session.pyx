""" Oimi Session contains all methods involving sessions.
	Defines scenario, games. 
	At runtime this .pyx works only together with corresponding .pxd extension file
	All details can be found looking at session documentation session_docs

	#when imposition or need is null=0 there will be a method between call and class creation that put to zero either impo or need or both...no need for \
	#other 4 class to specify case in which impo or need are null
	
	Notes:
		removed level_area = sa
"""
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ===============================================================================================================
# Imports
# ===============================================================================================================
import sys
import random 
import numpy as np
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/':
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/')
if '/home/pi/OIMI/oimi_code/src/learning/games_classification':
	sys.path.append('/home/pi/OIMI/oimi_code/src/learning/games_classification')
if '/home/pi/OIMI/oimi_code/src/oimi_state_machine/':
	sys.path.append('/home/pi/OIMI/oimi_code/src/oimi_state_machine/')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games':
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games')
import create_session_with_prediction as cswp 
import games_classes as gs
import pickle
import copy
import itertools
import functools
import operator
cimport session
cimport utils_play as up
import add_custom_pred_2 as acp2
import database_manager as db
from iteration_utilities import flatten
import myfsmmain as my
import fsm_event
from fsm_event import Trigger, Target

from enums_play cimport PatchCategory
from enums_play cimport GameCategory
from enums_play cimport Subject
from enums_play cimport Color
from enums_play cimport SessionDifficulty
from enums_play cimport GameDifficulty
from enums_play cimport OrderOfDifficulty
from enums_play cimport NumberIncrement
from enums_play cimport Feedback
from enums_play cimport QuestionKind
from enums_play cimport QuestionType
from enums_play cimport KidLevel
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class SessionBase:
	def __cinit__(self):
		self.session_feedbacks = []
		self.current_match
		self.user_dict_sess = {"task_preli": self.preliminary_activities_session,"task_begin_play":self.go_to_match,"task_finals":self.final_activities_session,} #for fsm 
	def __enter__(self):
		print("Creating Session...")
		print("Searcing for a pending session...")
		print("Initialization...")

	def __exit__(self, exc_type, exc_val, exc_tb):
		print("The session have been created successfully.")

	def __eq__(self, other): 
		cdef:
			bint compare = 1 
			Py_ssize_t i = 0
			Py_ssize_t lenght = len(self.matches_of_this_session)
		for i in range(lenght):
			if self.match[i] = other.match[i]:
				compare = 0
				break
		if not isinstance(other, SessionBase) or not isinstance(other, Session_1) or not isinstance(other, Session_2) or not isinstance(other, Session_3) or \
			not isinstance(other, Session_4) or not isinstance(other, Session_5) or not isinstance(other, Session_6): 
			return NotImplemented
		return len(self.matches_of_this_session)==len(other.matches_of_this_session) and compare
	
	def which_class(self):
		print(self.__class__)
		cla = self.getClass()
		print(cla)

	def getClass(self):
		#return self.__class__.__module__
		return self.__class__.__name__
	# -----------------------------------------------------------------------------
	# Getters, setters 
	# -----------------------------------------------------------------------------
	@property
	def session_feedbacks(self):
		return self.session_feedbacks
	@property
	defcurrent_match(self):
		return self.current_match
	@current_match.setter
	defcurrent_match(self, value):
		self.current_match = value

	def iterFlatten(self, root):
		""" Flattens a nested nested sublists and tuples into a single flattened list.
			Returns a generator object that yields each flattened element of the sub-list/sub-tuple returned by the recursive call. """
		if isinstance(root, (list, tuple)):
			for element in root:
				for e in self.iterFlatten(element):
					yield e
		else:
			yield root

	cpdef extract_ready_session(self):
		print("Starting session extraction")

	cpdef get_dict_kind_imposition(self):
		dict_impo_kind_g = { 
			'kind_game_1F': '1F',
			'kind_game_2L': '2L',
			'kind_game_3S': '3S',
			'kind_game_4P': '4P',
			'kind_game_5K': '5K',
			'kind_game_6I': '6I',
			'kind_game_7O': '7O',
			'kind_game_8Q': '8Q',
			'kind_game_9C': '9C'}
		return dict_impo_kind_g

	cpdef get_dict_imposition(self):
		dict_impo_type_g = { 
			'type_game_1FCACO': '1FCACO',
			'type_game_1FCACOA': '1FCACOA',
			'type_game_1FCOD': '1FCOD',
			'type_game_1FCODT': '1FCODT',
			'type_game_1FCOS': '1FCOS',
			'type_game_1FGCA': '1FGCA',
			'type_game_1FSUCOD': '1FSUCOD',
			'type_game_1FSUCODT': '1FSUCODT',
			'type_game_1FSUCOS': '1FSUCOS',
			'type_game_1FSUD': '1FSUD',
			'type_game_1FSUDT': '1FSUDT',
			'type_game_1FSUS': '1FSUS',
			'type_game_2LDBOB': '2LDBOB',
			'type_game_2LDBOC': '2LDBOC',
			'type_game_2LDSU': '2LDSU',
			'type_game_2LDSU2': '2LDSU2',
			'type_game_2LDSUOB': '2LDSUOB',
			'type_game_2LDSUOC': '2LDSUOC',
			'type_game_2LST': '2LST',
			'type_game_2LSU': '2LSU',
			'type_game_2LTWO': '2LTWO',
			'type_game_3SCO': '3SCO',
			'type_game_3SSU': '3SSU',
			'type_game_4PCO': '4PCO',
			'type_game_4PSU': '4PSU',
			'type_game_5KD': '5KD',
			'type_game_5KS': '5KS',
			'type_game_6I': '6I',
			'type_game_7OCO2': '7OCO2',
			'type_game_7OCO3': '7OCO3',
			'type_game_7OCO4': '7OCO4',
			'type_game_7OL2': '7OL2',
			'type_game_7OL3': '7OL3',
			'type_game_7OSU2': '7OSU2',
			'type_game_7OSU3': '7OSU3',
			'type_game_7OSU4': '7OSU4',
			'type_game_8QTCODB1': '8QTCODB1',
			'type_game_8QTCODB2': '8QTCODB2',
			'type_game_8QTCODC1': '8QTCODC1',
			'type_game_8QTCODC2': '8QTCODC2',
			'type_game_8QTCOSB1': '8QTCOSB1',
			'type_game_8QTCOSB2': '8QTCOSB2',
			'type_game_8QTCOSC1': '8QTCOSC1',
			'type_game_8QTCOSC2': '8QTCOSC2',
			'type_game_8QTKNDC': '8QTKNDC',
			'type_game_8QTKNSB': '8QTKNSB',
			'type_game_8QTKNSC': '8QTKNSC',
			'type_game_8QTPRSB': '8QTPRSB',
			'type_game_8QTPRSC': '8QTPRSC',
			'type_game_8QTSUDB1': '8QTSUDB1',
			'type_game_8QTSUDB2': '8QTSUDB2',
			'type_game_8QTSUDC1': '8QTSUDC1',
			'type_game_8QTSUDC2': '8QTSUDC2',
			'type_game_8QTSUSB1': '8QTSUSB1',
			'type_game_8QTSUSB2': '8QTSUSB2',
			'type_game_8QTSUSC1': '8QTSUSC1',
			'type_game_8QTSUSC2': '8QTSUSC2',
			'type_game_8QTSUSCOC1': '8QTSUSCOC1',
			'type_game_9CNCO': '9CNCO',
			'type_game_9CNSPA': '9CNSPA',
			'type_game_9CNSPX': '9CNSPX',
			'type_game_9CNSSU': '9CNSSU',
			'type_game_9CNSU': '9CNSU'}		
		
		return dict_impo_type_g

	cpdef get_categ_imposition(self):
		impo_categories = []
		for k in range(len(self.impositions_list)):
			if 'avoid' not in self.impositions_list[k]:
				if 'stress' in self.impositions_list[k]:
					impo_categories.append('st')
				elif 'facilitate' in self.impositions_list[k]:
					impo_categories.append('faci')
				elif 'one_ma' in self.impositions_list[k]:
					impo_categories.append('ch1_m')
				elif 'two_ma' in self.impositions_list[k]:
					impo_categories.append('ch2_m')
				elif 'three_ma' in self.impositions_list[k]:
					impo_categories.append('ch3_m')
				elif 'four_ma' in self.impositions_list[k]:
					impo_categories.append('ch4_m')
				elif 'one_ga' in self.impositions_list[k]:
					impo_categories.append('ch1_g')
				elif 'two_ga' in self.impositions_list[k]:
					impo_categories.append('ch2_g')
				elif 'three_ga' in self.impositions_list[k]:
					impo_categories.append('ch3_g')
				elif 'four_ga' in self.impositions_list[k]:
					impo_categories.append('ch4_g')
				elif 'one_qu' in self.impositions_list[k]:
					impo_categories.append('ch1_q')
				elif 'two_qu' in self.impositions_list[k]:
					impo_categories.append('ch2_q')
				elif 'three_qu' in self.impositions_list[k]:
					impo_categories.append('ch3_q')
				elif 'four_qu' in self.impositions_list[k]:
					impo_categories.append('ch4_q')
				elif 'mixed' in self.impositions_list[k]:
					impo_categories.append('mi')
				elif 'complex_easy' in self.impositions_list[k]:
					impo_categories.append('cr_se')
				elif 'complex_normal' in self.impositions_list[k]:
					impo_categories.append('cr_sn')
				elif 'complex_medium' in self.impositions_list[k]:
					impo_categories.append('cr_sm')
				elif 'complex_hard' in self.impositions_list[k]:
					impo_categories.append('cr_sh')
				elif 'Easy_1 match' in self.impositions_list[k]:
					impo_categories.append('cr_e1')
				elif 'Easy_2 match' in self.impositions_list[k]:
					impo_categories.append('cr_e2')
				elif 'Normal_1 match' in self.impositions_list[k]:
					impo_categories.append('cr_n1')
				elif 'Normal_2 match' in self.impositions_list[k]:
					impo_categories.append('cr_n2')
				elif 'Medium_1 match' in self.impositions_list[k]:
					impo_categories.append('cr_m1')
				elif 'Medium_2 match' in self.impositions_list[k]:
					impo_categories.append('cr_m2')
				elif 'Hard_1 match' in self.impositions_list[k]:
					impo_categories.append('cr_h1')
				elif 'Hard_2 match' in self.impositions_list[k]:
					impo_categories.append('cr_h2')
				elif 'Easy_1 game' in self.impositions_list[k]:
					impo_categories.append('cr_e1g')
				elif 'Easy_2 game' in self.impositions_list[k]:
					impo_categories.append('cr_e2g')
				elif 'Normal_1 game' in self.impositions_list[k]:
					impo_categories.append('cr_n1g')
				elif 'Normal_2 game' in self.impositions_list[k]:
					impo_categories.append('cr_n2g')
				elif 'Medium_1 game' in self.impositions_list[k]:
					impo_categories.append('cr_m1g')
				elif 'Medium_2 game' in self.impositions_list[k]:
					impo_categories.append('cr_m2g')
				elif 'Hard_1 game' in self.impositions_list[k]:
					impo_categories.append('cr_h1g')
				elif 'Hard_2 game' in self.impositions_list[k]:
					impo_categories.append('cr_h2g')
				elif 'type' in self.impositions_list[k]:
					impo_categories.append('ty')
				elif 'kind' in self.impositions_list[k]:
					impo_categories.append('ki')
				elif 'question_never' in self.impositions_list[k]:
					impo_categories.append('qu')
				elif 'ask_quest' in self.impositions_list[k]:
					impo_categories.append('as')
			else:
				print("wronf imposition scenario")
			print("imposition category: {}".format(impo_categories))
		return impo_categories

	cdef create_obj_session(self, int sess_id, int child_exists):
		#print("Scenario is not asked for the scenario here")
		if child_exists:
			child_query = "SELECTcurrent_level, age FROM Kids WHERE kid_id = ?"
			kid_info = self.db_conn.execute_param_query(child_query, child_exists)
			level_is = kid_info[0]
			age_is = kid_info[1]
			scen_query = "SELECT imposition_name FROM Kids_Impositions WHERE kid_id = ?"
			permission_scen = self.db_conn.execute_param_query(scen_query, child_exists)
			print("kid_info {}".format(kid_info))
			print("kid_LEVEL is {}".format(level_is))
			print("kid_AGE is {}".format(age_is))
			if 'avoid asking scenario' not in permission_scen:
				way_scen = 0
				ask_pos_scen = 0
				ins_scen_diff = 0 
			else:
				way_scen=1
				if (level_is=='intermediate' or level_is=='advanced') and age_is <= 7:
					ask_pos_scen = random.randint(0,1)
					ins_scen_diff = 0
				if (level_is=='intermediate' or level_is=='advanced') and age_is > 7:	
					ask_pos_scen = random.randint(0,1)
					ins_scen_diff = random.randint(0,1)
				if (level_is=='beginner' or level_is=='elementary'):
					ask_pos_scen = 0
					ins_scen_diff = 0	
			print("sess_id {}".format(sess_id))
		else:
			level_is = 'none'
			ask_pos_scen = 0
			ins_scen_diff = 0				
		se_query = "SELECT * FROM Sessions WHERE session_id = ?"
		result_sess = self.db_conn.execute_param_query(se_query, sess_id)
		self.id_session = result_sess[0]
		self.num_of_matches = result_sess[1]
		aa = up.opposite_conversion(result_sess[2])
		print("aa is {}".format(aa))
		print("type a {}".format(type(aa)))
		bb = up.convert_difficulty_session(aa)
		print("bb is {}".format(bb))
		print("type bb {}".format(type(bb)))
		self.complexity = up.transform_difficulty_session(aa)
		self.status = result_sess[3]
		self.order_difficulty_matches = result_sess[4]
		self.order_quantity_games = result_sess[5]
		self.mandatory_impositions = result_sess[6]
		self.mandatory_needs = result_sess[7]
		self.extra_time_tolerance = result_sess[8]
		self.movements_allowed = result_sess[9]
		self.body_enabled = result_sess[10]
		self.rules_explication = result_sess[11]
		self.stress_flag = result_sess[12]
		self.frequency_exhortations = result_sess[13]
		self.repeat_question = result_sess[14]
		self.repeat_intro = result_sess[15]
		self.desirable_time = result_sess[16]
		self.creation_way = result_sess[17]
		self.audio_s_intro = result_sess[19]
		self.audio_s_exit = result_sess[20]
		print("###################################### sess")
		#old take for now as reference
		##print(self.id_session)
		##print(self.num_of_matches)
		##print(self.complexity)
		##print(self.extra_time)
		##print(self.mvmnt_ok)
		##print(self.body_ok)
		##print(self.audio_slow_ok)
		##print(self.audio_rules_ok)
		##print(self.led_brightness)
		##print(self.exhort)
		##print(self.repeat_ques)
		##print(self.repeat_intro)
		##print(self.audio_s_intro)
		##print(self.audio_s_exit)
		print("###################################### sess")
		sql_take_matches = "SELECT * FROM Matches LEFT JOIN Sessions_Matches USING (match_id) WHERE session_id = ?"
		matches_found = self.db_conn.execute_param_query(sql_take_matches, self.id_session)
		print("matches_f matches_f {}".format(matches_found))
		#if the founded match is only one
		ready_matches = []
		print("NUM OF MATCHES {}".format(self.num_of_matches))
		try:
			if self.num_of_matches==1:
				one_m_match_id = matches_found[0]
				one_m_num_of_games = matches_found[1]
				aa = up.opposite_conversion(matches_found[2])
				one_m_difficulty = up.transform_difficulty_match(aa)
				one_m_variety = matches_found[4]
				one_m_order_difficulty_games = matches_found[5]
				one_m_order_quantity_questions = matches_found[6]
				one_m_criteria_arrangement = matches_found[7]
				one_m_time = matches_found[8]
				one_m_scenario = matches_found[9]
				one_m_audio_intro = matches_found[10]
				one_m_audio_exit = matches_found[11]
				print()
				print("###################################### match")
				print(one_m_match_id)
				print(one_m_num_of_games)
				print(one_m_difficulty)
				print(one_m_variety)
				print(one_m_order_difficulty_games)
				print(one_m_order_quantity_questions)
				print(one_m_criteria_arrangement)
				print(one_m_time)
				print(one_m_scenario)
				print(one_m_audio_intro)
				print(one_m_audio_exit)
				print("###################################### match")
				if level_is=='beginner': 
					self.extra_time_tolerance = 10
				elif level_is=='elementary':
					self.extra_time_tolerance = 5
				else:
					self.extra_time_tolerance = 0
				one_match = mt.Match(one_m_match_id, one_m_num_of_games, one_m_difficulty, one_m_scenario, way_scen, ask_pos_scen, 
					ins_scen_diff, self.extra_time_tolerance, self.db_conn)
				one_match.create_games_of_this_match()
				ready_matches.append(one_match)
			else:
				for i in range(self.num_of_matches):
					#print("now i is {}".format(i))
					#print("loop num {}".format(i))
					one_m_match_id = matches_found[i][0]
					one_m_num_of_games = matches_found[i][1]
					aa = up.opposite_conversion(matches_found[i][2])
					one_m_difficulty = up.transform_difficulty_match(aa)
					one_m_variety = matches_found[i][4]
					one_m_order_difficulty_games = matches_found[i][5]
					one_m_order_quantity_questions = matches_found[i][6]
					one_m_criteria_arrangement = matches_found[i][7]
					one_m_time = matches_found[i][8]
					one_m_scenario = matches_found[i][9]
					one_m_audio_intro = matches_found[i][10]
					one_m_audio_exit = matches_found[i][11]
					print()
					print("###################################### match num ", i)
					print(one_m_match_id)
					print(one_m_num_of_games)
					print(one_m_difficulty)
					print(one_m_variety)
					print(one_m_order_difficulty_games)
					print(one_m_order_quantity_questions)
					print(one_m_criteria_arrangement)
					print(one_m_time)
					print(one_m_scenario)
					print(one_m_audio_intro)
					print(one_m_audio_exit)
					print("###################################### match num", i)
					if level_is=='beginner': 
						self.extra_time_tolerance = 10
					elif level_is=='elementary':
						self.extra_time_tolerance = 5
					else:
						self.extra_time_tolerance = 0
					one_match = mt.Match(one_m_match_id, one_m_num_of_games, one_m_difficulty, one_m_scenario, 
						way_scen, ask_pos_scen, ins_scen_diff, self.extra_time_tolerance, self.db_conn)
					one_match.create_games_of_this_match()
					ready_matches.append(one_match)
		
		except Exception:
				one_m_match_id = matches_found[0]
				one_m_num_of_games = matches_found[1]
				aa = up.opposite_conversion(matches_found[2])
				one_m_difficulty = up.transform_difficulty_match(aa)
				one_m_variety = matches_found[4]
				one_m_order_difficulty_games = matches_found[5]
				one_m_order_quantity_questions = matches_found[6]
				one_m_criteria_arrangement = matches_found[7]
				one_m_time = matches_found[8]
				one_m_scenario = matches_found[9]
				one_m_audio_intro = matches_found[10]
				one_m_audio_exit = matches_found[11]
				print()
				print("###################################### match")
				print(one_m_match_id)
				print(one_m_num_of_games)
				print(one_m_difficulty)
				print(one_m_variety)
				print(one_m_order_difficulty_games)
				print(one_m_order_quantity_questions)
				print(one_m_criteria_arrangement)
				print(one_m_time)
				print(one_m_scenario)
				print(one_m_audio_intro)
				print(one_m_audio_exit)
				print("###################################### match")
				if level_is=='beginner': 
					self.extra_time_tolerance = 10
				elif level_is=='elementary':
					self.extra_time_tolerance = 5
				else:
					self.extra_time_tolerance = 0
				one_match = mt.Match(one_m_match_id, one_m_num_of_games, one_m_difficulty, one_m_scenario, 
					way_scen, ask_pos_scen, ins_scen_diff, self.extra_time_tolerance, self.db_conn)
				one_match.create_games_of_this_match()
				ready_matches.append(one_match)
		
		cdef mt.Match[:] matches_arr
		matemp = np.asarray(ready_matches, dtype = mt.Match)
		matches_arr = matemp
		self.matches_of_this_session = matches_arr
		print("---------------------------------------Final info ----------------------------------------------")
		print("self.matches_of_this_session")
		print(self.matches_of_this_session)
		print("ready_matches")
		print(ready_matches)
		print("ready_matches zero games_of_this_match")
		print(ready_matches[0].games_of_this_match)
		print("ready_matches zero games_of_this_match")
		print(ready_matches[0].games_of_this_match)
		print("self.matches_of_this_session[0].games_of_this_match[0].id_game")
		print(self.matches_of_this_session[0].games_of_this_match[0].id_game)
	
	cpdef store_session_in_progress(self):
		name_pickle_file = 'ses{}_kid{}'.format(self.id_session, self.id_child)
		filename_pk = '/home/pi/OIMI/oimi_code/src/playing/methodical_games/session/{}.pk'.format(name_pickle_file)
		with open(filename_pk, 'wb') as fi:
			pickle.dump(self.id_session, fi)
			pickle.dump(self.id_child, fi)
			pickle.dump(self.id_scenario, fi)
			pickle.dump(self.num_of_matches, fi)
			pickle.dump(self.complexity, fi)
			#pickle.dump(self.SessionDifficulty, fi)
			pickle.dump(self.status, fi)
			pickle.dump(self.order_difficulty_matches, fi)
			pickle.dump(self.order_quantity_games, fi)
			pickle.dump(self.mandatory_impositions, fi)
			pickle.dump(self.mandatory_needs, fi)
			pickle.dump(self.creation_way, fi)
			pickle.dump(self.desirable_time, fi)
			pickle.dump(self.extra_time_tolerance, fi)
			pickle.dump(self.movements_allowed, fi)
			pickle.dump(self.body_enabled, fi)
			pickle.dump(self.rules_explication, fi)
			pickle.dump(self.stress_flag, fi)
			pickle.dump(self.frequency_exhortations, fi)
			pickle.dump(self.repeat_question, fi)
			pickle.dump(self.repeat_intro, fi)
			pickle.dump(self.audio_s_intro, fi)
			pickle.dump(self.audio_s_exit, fi)
			pickle.dump(self.matches_of_this_session, fi)
			pickle.dump(self.current_match, fi)
			pickle.dump(self.session_feedbacks, fi)
			pickle.dump(self.session_scores, fi)

		change_status = "UPDATE Sessions SET status = 'pending' WHERE session_id = ?"
		self.db_conn.execute_param_query(change_status, self.id_session)

	cpdef restore_session_in_progress(self):
		""" Get a pending session if exists before starting a new one """
		name_pickle_file = 'ses{}_kid{}'.format(self.id_session, self.id_child)
		filename_pk = '/home/pi/OIMI/oimi_code/src/playing/methodical_games/session/{}.pk'.format(name_pickle_file)
		with open(filename_pk, 'wb') as fi:
			self.id_session = pickle.load(fi)
			self.id_child = pickle.load(fi)
			self.id_scenario = pickle.load(fi)
			self.num_of_matches = pickle.load(fi)
			self.complexity = pickle.load(fi)
			#self.SessionDifficulty = pickle.load(fi)
			self.status = pickle.load(fi)
			self.order_difficulty_matches = pickle.load(fi)
			self.order_quantity_games = pickle.load(fi)
			self.mandatory_impositions = pickle.load(fi)
			self.mandatory_needs = pickle.load(fi)
			self.creation_way = pickle.load(fi)
			self.desirable_time = pickle.load(fi)
			self.extra_time_tolerance = pickle.load(fi)
			self.movements_allowed = pickle.load(fi)
			self.body_enabled = pickle.load(fi)
			self.rules_explication = pickle.load(fi)
			self.stress_flag = pickle.load(fi)
			self.frequency_exhortations = pickle.load(fi)
			self.repeat_question = pickle.load(fi)
			self.repeat_intro = pickle.load(fi)
			self.audio_s_intro = pickle.load(fi)
			self.audio_s_exit = pickle.load(fi)
			self.matches_of_this_session = pickle.load(fi)
			self.current_match = pickle.load(fi)
			self.session_feedbacks = pickle.load(fi)
			self.session_scores = pickle.load(fi)

	def runme_sess(self):
		state0 = my.PlaySessionState("play_session")
		evt1 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.BEGIN_SES, user_data=self.user_dict_sess,)
		evt2 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.GO_TO_MIDDLE_S, user_data=self.user_dict_sess,)
		evt3 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.GO_TO_LAST_S, user_data=self.user_dict_sess,)
		mainfsm = my.OimiFSMSession()
		mainfsm.update_state_fsm(evt1)
		mainfsm.update_state_fsm(evt2)
		mainfsm.update_state_fsm(evt3)
		mainfsm.update_state_fsm(None)

	def preliminary_activities_session(self):
		print("preliminary_activities")

	def go_to_match(self):
		print("current matches to do are : {}".format(list(self.matches_of_this_session)))
		for i in range(self.num_of_matches):
			print("range round gotogame {}".format(i))
			print(self.matches_of_this_session[0].games_of_this_match[0].id_game)
			print(self.matches_of_this_session[0].games_of_this_match[0].num_of_questions)
			print(self.matches_of_this_session[0].games_of_this_match[0].limit)
			print(self.matches_of_this_session[0].games_of_this_match[0].limit1)
			
			self.matches_of_this_session[i].runme_match()
			
	cpdef final_activities_session(self):
		#print("Last state activity, before quitting")
		which_subclass = self.getClass()
		print("which_class: {}".format(which_subclass))
		if which_subclass in ['Session_1', 'Session_2', 'Session_3']:
			self.db_conn.insertion_recap(self.id_child)
			self.db_conn.insertion_summary(self.id_child)
			for index1, mm in enumerate(self.matches_of_this_session):
				for index2, gg in enumerate(mm.games_of_this_match):
					for index3, rr in enumerate(gg.results):
						print("match num {}".format(mm.id_match))
						print("game num {}".format(gg.id_game))
						print("risultati ERRORS {} , {}".format(index3,rr.num_errors))
						print("risultati CORRECT{} , {}".format(index3,rr.num_corrects))
						print("risultati INDECISION{} ,{}".format(index3,rr.num_indecisions))
						self.db_conn.obtain_scores_current_session(self.id_child, self.id_session, mm.id_match, gg.id_game, rr.curr_question, rr.num_corrects, rr.num_errors, rr.num_indecisions, rr.waited_time)

			
			self.db_conn.update_achievements(self.id_child) #all in once
			#name_pickle_file = 'session {}_final_tmp_scores_file'.format(self.id_session)
			#filename_pk = '/home/pi/OIMI/oimi_code/src/playing/methodical_games/session/{}.pk'.format(name_pickle_file)
			#with open(filename_pk, 'rb') as fi:
			#	self.deletions_list = pickle.dump(fi)
			#
######################################################################################################################################################
cdef class Session_1(SessionBase):
	def __cinit__(self, int id_child):
		self.id_child = id_child
		self.db_conn = db.DatabaseManager()
	
	cpdef extract_ready_session(self):
		cdef:
			bint way_scen = 0
			bint ask_pos_scen = 0
			bint ins_scen_diff = 0
		print(isinstance(self.db_conn, db.DatabaseManager))
		child_query = "SELECTcurrent_level, age FROM Kids WHERE kid_id = ?"
		kid_info = self.db_conn.execute_param_query(child_query, self.id_child)
		print(kid_info)
		if not kid_info:
			#print("no kid found, let's create a default session")
			manda_impo = 1
			manda_need = 1
			self.db_conn.add_new_Session(self.id_child,manda_impo,manda_need)
			return
		#print("yes it this case the kid exists")
		level_is = kid_info[0]
		age_is = kid_info[1]
		next_query = "SELECT Sessions.session_id FROM Sessions JOIN Kids_Sessions USING(session_id) WHERE kid_id = ? AND (status='default' or status='to_do') ORDER BY random() LIMIT 1"
		sess_id = self.db_conn.execute_param_query(next_query, self.id_child)
		sess_id = sess_id[0]
		scen_query = "SELECT imposition_name FROM Kids_Impositions WHERE kid_id = ?"
		permission_scen = self.db_conn.execute_param_query(scen_query, self.id_child)
		print("kid_info {}".format(kid_info))
		print("kid_LEVEL is {}".format(level_is))
		print("kid_AGE is {}".format(age_is))
		self.create_obj_session(sess_id, self.id_child)

	@property
	def id_session(self):
		return self.id_session
	@id_session.setter
	def id_session(self, value):
		self.id_session = value
	@property
	def id_child(self):
		return self.id_child
	@id_child.setter
	def id_child(self, value):
		self.id_child = value
	@property
	def id_scenario(self):
		return self.id_scenario
	@id_scenario.setter
	def id_scenario(self, value):
		self.id_scenario = value
	@property
	def num_of_matches(self):
		return self.num_of_matches
	@num_of_matches.setter
	def num_of_matches(self, value):
		self.num_of_matches = value
	@property
	def complexity(self):
		return self.complexity
	@complexity.setter
	def complexity(self, value):
		self.complexity = value
	@property
	def extra_time_tolerance(self):
		return self.extra_time_tolerance
	@extra_time_tolerance.setter
	def extra_time_tolerance(self, value):
		self.extra_time_tolerance = value
	@property
	def body_enabled(self):
		return self.body_enabled
	@body_enabled.setter
	def body_enabled(self, value):
		self.body_enabled = value
	@property
	def rules_explication(self):
		return self.rules_explication
	@rules_explication.setter
	def rules_explication(self, value):
		self.rules_explication = value
	@property
	def movements_allowed(self):
		return self.movements_allowed
	@movements_allowed.setter
	def movements_allowed(self, value):
		self.movements_allowed = value

	@property
	def status(self):
		return self.status
	@status.setter
	def status(self, value):
		self.status = value
	@property
	def order_difficulty_matches(self):
		return self.order_difficulty_matches
	@order_difficulty_matches.setter
	def order_difficulty_matches(self, value):
		self.order_difficulty_matches = value		
	@property
	def order_quantity_games(self):
		return self.order_quantity_games
	@order_quantity_games.setter
	def order_quantity_games(self, value):
		self.order_quantity_games = value		
	@property
	def mandatory_impositions(self):
		return self.mandatory_impositions
	@mandatory_impositions.setter
	def mandatory_impositions(self, value):
		self.mandatory_impositions = value	
	@property
	def mandatory_needs(self):
		return self.mandatory_needs
	@mandatory_needs.setter
	def mandatory_needs(self, value):
		self.mandatory_needs = value	
	@property
	def desirable_time(self):
		return self.desirable_time
	@desirable_time.setter
	def desirable_time(self, value):
		self.desirable_time = value	
	@property
	def creation_way(self):
		return self.creation_way
	@creation_way.setter
	def creation_way(self, value):
		self.creation_way = value	
	@property
	def stress_flag(self):
		return self.stress_flag
	@stress_flag.setter
	def stress_flag(self, value):
		self.stress_flag = value
	@property
	def frequency_exhortations(self):
		return self.frequency_exhortations
	@frequency_exhortations.setter
	def frequency_exhortations(self, value):
		self.frequency_exhortations = value
	@property
	def repeat_question(self):
		return self.repeat_question
	@repeat_question.setter
	def repeat_question(self, value):
		self.repeat_question = value
	@property
	def repeat_intro(self):
		return self.repeat_intro
	@repeat_intro.setter
	def repeat_intro(self, value):
		self.repeat_intro = value
	@property
	def audio_s_intro(self):
		return self.audio_s_intro
	@audio_s_intro.setter
	def audio_s_intro(self, value):
		self.audio_s_intro = value
	@property
	def audio_s_exit(self):
		return self.audio_s_exit
	@audio_s_exit.setter
	def audio_s_exit(self, value):
		self.audio_s_exit = value													
	@property
	def db_conn(self):
		return self.db_conn
	@db_conn.setter
	def db_conn(self, value):
		self.db_conn = value	

cdef class Session_2(SessionBase):
	def __cinit__(self, int id_child, list impositions_list, list needs_list, str level_area):
		self.id_child = id_child
		self.impositions_list = impositions_list
		self.needs_list = needs_list
		self.level_area = level_area
		self.db_conn = db.DatabaseManager()

	cpdef extract_ready_session(self):
		""" Prepares session find specific impo and need information"""
		cdef:
			bint way_scen = 0
			bint ask_pos_scen = 0
			bint ins_scen_diff = 0
		child_query = "SELECTcurrent_level, age FROM Kids WHERE kid_id = ?"
		#print(":::::::::::::::")
		#print(self.id_child)
		#print(self.impositions_list)
		#print(self.needs_list)
		#print(self.level_area)
		#print(":::::::::::::::")
		kid_info = self.db_conn.execute_param_query(child_query, self.id_child)
		print(kid_info)
		if not kid_info:
			manda_impo = 1
			manda_need = 1
			self.db_conn.add_new_Session(self.id_child,manda_impo,manda_need)
			return

		level_is = kid_info[0]
		age_is = kid_info[1]
		print("length imposition {}".format(len(self.impositions_list)))
		print("length need {}".format(len(self.needs_list)))
		impo_categories = []
		need_categories = []
		id_needs_final, scopes_final, focuses_final = [],[],[]
		dict_impo_kind_g = self.get_dict_kind_imposition()
		dict_impo_kind_g = self.get_dict_kind_imposition()
		dict_impo_type_g = self.get_dict_imposition()
		impo_categories = self.get_categ_imposition()
		#print(dict_impo_type_g)
		#print(dict_impo_kind_g)
		#print(impo_categories)
		if len(self.impositions_list)>6: 
			remove_me = random.randint(0,6)
			del(self.impositions_list[remove_me])
		first_list_of_sess = []
		second_list_of_sess = []
		result_list_of_sess = []
		impo_list_of_sess = []
		all_session_id_results = []
		for l in range(len(self.needs_list)):
			print("needs_list 0",self.needs_list[l][0])
			print("needs_list 1",self.needs_list[l][1])
			find_need_sql = "SELECT need_id FROM Needs WHERE focus = ? AND (match_scope = ? OR game_scope = ? OR question_scope = ?)"
			id_params = (self.needs_list[l][0], self.needs_list[l][1], self.needs_list[l][1], self.needs_list[l][1])
			res_id_needs = self.db_conn.execute_param_query(find_need_sql, id_params)
			print("res_id_needs {}".format(res_id_needs))
			needs_query = "SELECT match_scope, game_scope, question_scope FROM Needs WHERE need_id = ?"
			res_scopes = self.db_conn.execute_param_query(needs_query, res_id_needs[0])
			print("res_scopes {}".format(res_scopes))
			if res_scopes[0]=='generic_match':
				need_categories.append('gen_m')
			elif res_scopes[0]=='mixed_match':
				need_categories.append('mix_m')
			elif res_scopes[0]=='Invalid':
				if res_scopes[1]=='generic_game':
					need_categories.append('gen_g')
				elif res_scopes[1]='Invalid':
					need_categories.append('type_g')
				elif res_scopes[1]=='Invalid':
					if res_scopes[2]='Invalid':
						need_categories.append('ques')
					else:
						need_categories.append('no_need')

			print("need_categories {}".format(need_categories))
			id_needs_final.append(res_id_needs)
			focuses_final.append(self.needs_list[l][0])
			print("id_needs_final{}".format(id_needs_final))
			print("focuses{}".format(focuses_final))

		redo = acp2.create_new_anyway(self.impositions_list)
		if redo==0:
			if len(self.impositions_list)==1 and len(self.needs_list)==0:
				params_case10 = (self.id_child, self.impositions_list[0])
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN Sessions_Impositions USING (session_id)) USING(session_id) \
				WHERE kid_id = ? \
				AND status='default' \
				AND imposition_name = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case10)
			elif len(self.impositions_list)==0 and len(self.needs_list)==1:
				params_case01 = (self.id_child, res_id_needs[0])
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN Sessions_Needs USING (session_id)) USING(session_id) \
				WHERE kid_id = ? \
				AND status='default' \
				AND need_id = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case01)
			elif len(self.impositions_list)==1 and len(self.needs_list)==1:
				params_case11 = (self.id_child, self.impositions_list[0], res_id_needs[0])
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							Sessions_Impositions USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND imposition_name = ? AND need_id = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case11)
			elif len(self.impositions_list)==2 and len(self.needs_list)==1:
				params_case21 = (self.id_child, self.impositions_list[0], self.impositions_list[1], res_id_needs[0])
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							Sessions_Impositions USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? OR imposition_name = ?) AND need_id = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case21)
			elif len(self.impositions_list)==1 and len(self.needs_list)==2:
				params_case12 = (self.id_child, self.impositions_list[0], res_id_needs[0], res_id_needs[1])
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							Sessions_Impositions USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? AND (need_id = ? OR need_id = ?) \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case12)
			elif len(self.impositions_list)==3 and len(self.needs_list)==1:
				params_case31 = (self.id_child, self.impositions_list[0], self.impositions_list[1], self.impositions_list[2], 
					res_id_needs[0])
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							Sessions_Impositions USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? OR imposition_name = ? OR imposition_name = ?) AND need_id = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case31)
			elif len(self.impositions_list)==1 and len(self.needs_list)==3:
				params_case13 = (self.id_child, self.impositions_list[0], self.needs_list[0], 
					res_id_needs[1], res_id_needs[2])
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							Sessions_Impositions USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? AND (need_id = ? OR need_id = ? OR need_id = ?) \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case13)
			elif len(self.impositions_list)==2 and len(self.needs_list)==2:
				params_case22 = (self.id_child, self.impositions_list[0], self.impositions_list[1], 
					res_id_needs[0], res_id_needs[1])
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							Sessions_Impositions USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? OR imposition_name = ?) AND (need_id = ? OR need_id = ?) \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case22)
			elif len(self.impositions_list)==3 and len(self.needs_list)==3:
				params_case33 = (self.id_child, self.impositions_list[0], self.impositions_list[1], self.impositions_list[2], 
					res_id_needs[0], res_id_needs[1], res_id_needs[2])
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							Sessions_Impositions USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? OR imposition_name = ? OR imposition_name = ?) \
				AND (need_id = ? OR need_id = ? OR need_id = ?) \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case33)	
			if len(result_sess)=0: #---> step2
				print("ok found without doing STEP 2")
				print("result_sess {}".format(result_sess))
			else:
				#step 2 
				print("++STEP 2")
				#check inside...I need all possible combinations inside session
				#print("IMPO categories {}".format(impo_categories))
				if len(self.impositions_list)==1 and len(self.needs_list)==0:	
					#print("ONLY IMPO, NO NEED")
					if impo_categories[0]=='as':
						##print("we are IN IMPO ASK QUESTION")
						params_case_first = (self.id_child, self.impositions_list[0])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Impositions_Questions USING (question_id)) \
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do')\
						AND Impositions_Questions.imposition_name = ?"
					if impo_categories[0]=='ty':
						#print("we are IN IMPO TYPE GAME")
						#same type impo query for all 3 cases
						params_case_first = (self.id_child, dict_impo_type_g[self.impositions_list[0]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Questions USING (question_id))\
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do') \
						AND Questions.type = ?" 
					if impo_categories[0]=='ki':
						#print("we are IN IMPO KIND GAME")
						#same type impo query for all 3 cases
						params_case_first = (self.id_child, dict_impo_kind_g[self.impositions_list[0]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Questions USING (question_id))\
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do') \
						AND Questions.kind = ?" 
					if impo_categories[0]=='qu':
						to_do_ques, done_ques = [],[]
						##print("we are IN IMPO NEVER ASKED GAME")
						aa = "SELECT question_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Games_Questions USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND status ='to_do'"
						res_question_to_do= self.db_conn.execute_param_query(aa, self.id_child)
						print("res_question_to_do {}".format(res_question_to_do))
						if len(res_question_to_do)>1: #distinction between [0][0] fetchall result query, create list in this case 
							to_do_ques = [item for sublist in res_question_to_do for item in sublist]
						if len(res_question_to_do)==1:
							to_do_ques.append(res_question_to_do[0])
						print("OLD TEMP OLD TEMP OLD TEMP to do question {}".format(to_do_ques))

						bb = "SELECT question_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Games_Questions USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND status ='done'"
						res_question_done = self.db_conn.execute_param_query(bb, self.id_child)
						print("res_question_done {}".format(res_question_done))
						if len(res_question_done)>1: #distinction between [0][0] fetchall result query, create list in this case 
							done_ques = [item for sublist in res_question_done for item in sublist]
						if len(res_question_done)==1:
							done_ques.append(res_question_done[0])
						print("OLD TEMP OLD TEMP OLD TEMP done_ques {}".format(done_ques))
						ques_to_remove = list(set(to_do_ques).intersection(done_ques))
						print("quelli da rimuovere {}".format(ques_to_remove))
						
						if len(ques_to_remove)=0:
							result_ques_indexes = list(set(to_do_ques) - set(ques_to_remove))							
						else:
							result_ques_indexes = copy.copy(to_do_ques)
						print("result_ques_indexes Final COSA TROVO {}".format(result_ques_indexes))
						result_sess_first = []
						params_case_first = []
						for u in range(len(result_ques_indexes)):
							params_case_first.append(self.id_child)
							params_case_first.append(result_ques_indexes[u])
							session_for_this_ques_query = "SELECT Kids_Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN Games_Questions USING (game_id)) \
										USING (match_id)) \
								USING(session_id)) \
							USING(session_id) \
							WHERE kid_id = ? \
							AND status ='to_do' \
							AND question_id = ?"
							one_sess = self.db_conn.execute_param_query(session_for_this_ques_query, params_case_first)
							print("one session is: {}".format(one_sess))
							result_sess_first.append(one_sess)
							params_case_first = []
						result_sess_first = list(flatten(result_sess_first))
						print("finishde. Result is {}".format(result_sess_first))
						result_sess_first = list(set(result_sess_first))
						print("where to find result: {}".format(result_sess_first))
					if impo_categories[0] in ('cr_se','cr_sn','cr_sm','cr_sh'):
						print("Imposition complexity session")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN Matches USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Sessions.complexity = ?"
						if impo_categories[0]=='cr_se':
							params_case_first = (self.id_child, 'Easy')
						elif impo_categories[0]=='cr_sn':
							params_case_first = (self.id_child, 'Normal')
						elif impo_categories[0]=='cr_sm':
							params_case_first = (self.id_child, 'Medium')
						elif impo_categories[0]=='cr_sh':
							params_case_first = (self.id_child, 'Hard')
					if impo_categories[0] in ('cr_e1','cr_e2','cr_n1','cr_n2','cr_m1','cr_m2','cr_h1','cr_h2'):
						# in focus session need and impo are not considered together
						#print("we are IN IMPO DIFFICULTIY match")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN Matches USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Matches.difficulty = ?"
						
						if impo_categories[0]=='cr_e1':
							params_case_first = (self.id_child, 'Easy_1')
						elif impo_categories[0]=='cr_e2':
							params_case_first = (self.id_child, 'Easy_2')
						elif impo_categories[0]=='cr_n1':
							params_case_first = (self.id_child, 'Normal_1')
						elif impo_categories[0]=='cr_n2':
							params_case_first = (self.id_child, 'Normal_2')
						elif impo_categories[0]=='cr_m1':
							params_case_first = (self.id_child, 'Medium_1')
						elif impo_categories[0]=='cr_m2':
							params_case_first = (self.id_child, 'Medium_2')
						elif impo_categories[0]=='cr_h1':
							params_case_first = (self.id_child, 'Hard_1')
						elif impo_categories[0]=='cr_h2':
							params_case_first = (self.id_child, 'Hard_2')
					if impo_categories[0] in ('cr_e1g','cr_e2g','cr_n1g','cr_n2g','cr_m1g','cr_m2g','cr_h1g','cr_h2g'):
						#print("we are IN IMPO DIFFICULTIY game")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN Games USING (game_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Games.difficulty = ?"
						
						if impo_categories[0]=='cr_e1g':
							params_case_first = (self.id_child, 'Easy_1')
						elif impo_categories[0]=='cr_e2g':
							params_case_first = (self.id_child, 'Easy_2')
						elif impo_categories[0]=='cr_n1g':
							params_case_first = (self.id_child, 'Normal_1')
						elif impo_categories[0]=='cr_n2g':
							params_case_first = (self.id_child, 'Normal_2')
						elif impo_categories[0]=='cr_m1g':
							params_case_first = (self.id_child, 'Medium_1')
						elif impo_categories[0]=='cr_m2g':
							params_case_first = (self.id_child, 'Medium_2')
						elif impo_categories[0]=='cr_h1g':
							params_case_first = (self.id_child, 'Hard_1')
						elif impo_categories[0]=='cr_h2g':
							params_case_first = (self.id_child, 'Hard_2')
					if impo_categories[0] in ('ch1_m','ch2_m','ch3_m','ch4_m','ch1_g','ch2_g','ch3_g','ch4_g','ch1_q','ch2_q','ch3_q','ch4_q'):
						params_case_first = (self.id_child)
						if impo_categories[0]=='ch1_m':
							#print("we are IN IMPO NUM 1 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN Sessions_Matches USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 1"
						if impo_categories[0]=='ch2_m':
							#print("we are IN IMPO NUM 2 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN Sessions_Matches USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 2"
						if impo_categories[0]=='ch3_m':
							#print("we are IN IMPO NUM 3 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN Sessions_Matches USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 3"
						if impo_categories[0]=='ch4_m':
							#print("we are IN IMPO NUM 4 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN Sessions_Matches USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 4"
						if impo_categories[0]=='ch1_g':
							#print("we are IN IMPO NUM 1 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 1"
						if impo_categories[0]=='ch2_g':
							#print("we are IN IMPO NUM 2 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 2"
						if impo_categories[0]=='ch3_g':
							#print("we are IN IMPO NUM 3 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 3"
						if impo_categories[0]=='ch4_g':
							#print("we are IN IMPO NUM 4 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 4"
						if impo_categories[0]=='ch1_q':
							#print("we are IN IMPO NUM 1 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 1"
						if impo_categories[0]=='ch2_q':
							#print("we are IN IMPO NUM 2 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 2"
						if impo_categories[0]=='ch3_q':
							#print("we are IN IMPO NUM 3 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 3"
						if impo_categories[0]=='ch4_q':
							#print("we are IN IMPO NUM 4 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 4"
					if impo_categories[0]=='mi':
						#print("we are IN IMPO mixed match")
						ok_stop_here = 1
						par = (self.id_child)
						sess_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Games JOIN (Games USING(game_id)) \
							USING (session_id)) \
						USING (session_id) \
						WHERE kid_id = ? \
						AND kind = 'MIX' \
						AND (status ='default' or status ='to_do')"
						se_id = self.db_conn.execute_param_query(sess_query, par)						
						if len(se_id)==1:
							result_sess_first = se_id[0]
						if len(se_id)>1:
							result_sess_first = [item for sublist in se_id for item in sublist]
						else:
							result_sess_first = []
						if ok_stop_here=1:
							par = (self.id_child)
							sess_query = "SELECT Sessions.session_id FROM Sessions JOIN Kids_Sessions USING (session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do')"
							se_id = self.db_conn.execute_param_query(sess_query, par)
							
							if len(se_id)>0:
								res_all_sessions = [item for sublist in se_id for item in sublist]
							else:
								res_all_sessions = []
							print("First impression: {}".format(res_all_sessions))
							
							for p in range(len(res_all_sessions)):
								param_diff = (res_all_sessions[p])
								m_query = "SELECT match_id FROM Sessions JOIN Sessions_Matches USING (session_id) \
								WHERE session_id = ?"
								res_matches = self.db_conn.execute_param_query(m_query, param_diff)
								if len(res_matches)>1: 
									current_matches = [item for sublist in res_matches for item in sublist]
								if len(res_matches)==1: 
									current_matches = copy.copy(res_matches)
								if len(res_matches)==0: 
									current_matches = []
								print("current_matches is : ",current_matches)
								

								print("evaluate session result sess {}".format(current_matches))

								second_q_1 = "SELECT num_of_matches FROM Sessions WHERE session_id = ?"
								second_q_2 = "SELECT num_of_games FROM Sessions JOIN \
												(Sessions_Matches JOIN Matches USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								second_q_3 = "SELECT num_questions FROM Sessions JOIN \
												(Sessions_Matches JOIN \
													(Matches_Games JOIN Games USING (game_id)) \
													USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"

								res_mat = self.db_conn.execute_param_query(second_q_1, res_all_sessions[p])
								res_gam = self.db_conn.execute_param_query(second_q_2, res_all_sessions[p])
								res_que = self.db_conn.execute_param_query(second_q_3, res_all_sessions[p])

								if len(res_gam)>1:
									res_num_gam = [item for sublist in res_gam for item in sublist]
								if len(res_gam)==1:
									res_num_gam = copy.copy(res_gam)
								if len(res_gam)==0:
									res_num_gam = []
								if len(res_que)>1:
									res_num_que = [item for sublist in res_que for item in sublist]
								if len(res_que)==1:
									res_num_que = copy.copy(res_que)
								if len(res_que)==0:
									res_num_que = []
								is_mixed_1=0
								is_mixed_2=0
								is_mixed_3=0
								is_mixed_4=0
								print("res_num_mat {}".format(res_mat))
								print("res_num_gam {}".format(res_num_gam))
								print("res_num_que {}".format(res_num_que))

								if len(res_num_gam)==1:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games made by 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[1]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
								elif len(res_num_gam)==2:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										print("numb ma 2")
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
								elif len(res_num_gam)==3:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
								elif len(res_num_gam)==4:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1

									i = res_num_gam[2]
									if res_num_gam[3]==1:
										if res_num_que[i]==1:
											print("fourth match has a game all made by 1 quest")
											is_mixed_4 = 0
									if res_num_gam[3]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("fourth match has 2 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("fourth match has 3 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("fourth match has 4 games enquiry")
											is_mixed_4 = 1
								#print("MIXXXXX")
								#print(is_mixed_1)
								#print(is_mixed_2)
								#print(is_mixed_3)
								#print(is_mixed_4)
								if is_mixed_1=0 or is_mixed_2=0 or is_mixed_3=0 or is_mixed_4=0:
									
									#check control match in wichi there is ask_quest_type if generic and non mixed
									#q0 = "DROP TABLE IF EXISTS mini_tab"
									q0 = "DROP TABLE IF EXISTS mini_tab"
									bah = self.db_conn.execute_a_query(q0)
									#q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
									q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
										AS SELECT session_id,match_id FROM Sessions_Matches WHERE session_id = ?;"
									
									res_q1 = self.db_conn.execute_param_query(q1, res_all_sessions[p])
									final_q1 = [item for sublist in res_q1 for item in sublist]
									print("the length current MAT IS {}".format(len(current_matches)))
									for j in range(len(current_matches)):
										print("numb numb numb numb numb numb numb numb loop tra i match {}".format(j))
										print("session: {} match: {}".format(res_all_sessions,current_matches[j]))
										q2 = "SELECT oid FROM mini_tab WHERE match_id = ?"
										res_q2 = self.db_conn.execute_param_query(q2,current_matches[j])
										final_bo = res_q2
										print("res q2res q2res q2res q2 riga match = {}".format(res_q2))
										#final_bo = res_q2[0]
										print("final_bo {}".format(final_bo))
										#q3 = "DROP TABLE IF EXISTS mini_tab"
										result_sess = 0
										if len(final_bo)>0:
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or \
											(final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
																			
												result_sess = 0
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or (final_bo[0]==3 and is_mixed_3) or \
											(final_bo[0]==4 and is_mixed_4):
												
												result_sess = res_all_sessions[p]
											print("That is {}".format(result_sess))
											if result_sess=0:
												all_session_id_results.append(res_all_sessions[p])
												print("all_session_id_results {}".format(all_session_id_results))
									
							print("all_session_id_results {}".format(all_session_id_results))
							if len(all_session_id_results)==1:
								result_sess_first = copy.copy(all_session_id_results)
							if len(all_session_id_results)>1:
								result_sess_first = [item for sublist in all_session_id_results for item in sublist]
							if len(all_session_id_results)==0:
								result_sess_first = []

							try:
								result_sess_first = list(itertools.chain.from_iterable(result_sess_first))
								print("result_sess_first total after itertools is {}".format(result_sess_first))
							except Exception:
								result_sess_first = list(self.iterFlatten(result_sess_first))
								print("result_sess_first total after flatten is {}".format(result_sess_first))
							result_sess_first = list(set(result_sess_first))
							print("result_sess_first total after il SET is {}".format(result_sess_first))
					#now evaluate resp	
					if impo_categories[0] not in ('qu','mi'):
						result_ques = self.db_conn.execute_param_query(next_query, params_case_first)
						if len(result_ques)==1:
							result_sess_first = copy.copy(result_ques)
						if len(result_ques)>1:
							result_sess_first = [item for sublist in result_ques for item in sublist]
						if len(result_ques)==0:
							result_sess_first = []
						print("Here is the result_ques {} ".format(result_ques))
						print("Here is the result_sess_first {} ".format(result_sess_first))

					if len(result_sess_first)>0:
						one_sess = random.choices(result_sess_first, k=1)
						find_one_sess = one_sess[0]
						print("find_one_sess {}".format(find_one_sess))
						
						last_query = "SELECT * FROM Sessions WHERE session_id = ?"
						result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
						print("End: as type {}".format(result_sess))
					else:
						result_sess = []
						print("End: as type but it is wrong {}".format(result_sess))

				elif len(self.impositions_list)==0 and len(self.needs_list)==1:
					#print("ONLY NEED, NO IMPO")
					se_tmp,result_sess_first = [],[]
					params_case01 = (self.id_child)
					next_query = "SELECT Sessions.session_id FROM Sessions JOIN Kids_Sessions USING(session_id) \
					WHERE kid_id = ? \
					AND (status='default' OR status = 'to_do')"				
					
					se_tmp = self.db_conn.execute_param_query(next_query, params_case01)
					print("se_tmp {}".format(se_tmp))
					if len(se_tmp)>1: 
						result_sess_first = [item for sublist in se_tmp for item in sublist]
						print("first case results_need {}".format(result_sess_first))
					if len(se_tmp)==1:
						result_sess_first.append(se_tmp[0])
						print("second case results_need {}".format(result_sess_first)) 
					if len(result_sess_first)>0:
						print("need category question")
						if need_categories[0]=='ques':
							#need to solutionts separated impo e need
							params_case_second = (self.id_child, res_id_needs[0])
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN \
											(Games JOIN \
												(Games_Questions JOIN Needs_Questions USING (question_id)) \
												USING (game_id)) \
											USING (game_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Needs_Questions.need_id = ?"
						
							result_ques = self.db_conn.execute_param_query(next_query, params_case_second)
							if len(result_ques)==1:
								result_sess_second = copy.copy(result_ques)
							if len(result_ques)>1:
								result_sess_second = [item for sublist in result_ques for item in sublist]
							if len(result_ques)==0:
								result_sess_second = []
							print("recap ques as")
							print("result_sess_first")
							print(result_sess_first)
							print("result_sess_second")
							print(result_sess_second)
							result_sess_def = list(set(result_sess_first).intersection(result_sess_second))
							if len(result_sess_def)>0:
								one_sess = random.choices(result_sess_def, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess {}".format(find_one_sess))
								
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end as ques {}".format(result_sess))
							else:
								result_sess = []
								print("At the end as ques error {}".format(result_sess))

							print("result_sess intersezione = ", result_sess_def)
							print("result_sess definitive = ", result_sess)
						elif need_categories[0]=='gen_m' or need_categories[0]=='mix_m':
							print("need of gen_m")
							#print("we are IN NEED SCOPE MATCH")
							all_ses = []
							for p in range(len(result_sess_first)):
								
								param_diff = (result_sess_first[p])
								m_query = "SELECT match_id FROM Sessions JOIN Sessions_Matches USING (session_id) \
								WHERE session_id = ?"
								res_matches = self.db_conn.execute_param_query(m_query, param_diff)
								if len(res_matches)>1: 
									current_matches = [item for sublist in res_matches for item in sublist]
								if len(res_matches)==1:
									current_matches = copy.copy(res_matches)
								if len(res_matches)==0:
									current_matches = []
								print("current_matches is ",current_matches)
								print("need in gen_m")
								print("evaluate session result sess {}".format(current_matches))

								second_q_1 = "SELECT num_of_matches FROM Sessions WHERE session_id = ?"
								second_q_2 = "SELECT num_of_games FROM Sessions JOIN \
												(Sessions_Matches JOIN Matches USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								second_q_3 = "SELECT num_questions FROM Sessions JOIN \
												(Sessions_Matches JOIN \
													(Matches_Games JOIN Games USING (game_id)) \
													USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"

								res_mat = self.db_conn.execute_param_query(second_q_1, result_sess_first[p])
								res_gam = self.db_conn.execute_param_query(second_q_2, result_sess_first[p])
								res_que = self.db_conn.execute_param_query(second_q_3, result_sess_first[p])

								if len(res_gam)>1:
									res_num_gam = [item for sublist in res_gam for item in sublist]
								if len(res_gam)==1:
									res_num_gam = copy.copy(res_gam)
								if len(res_gam)==0:
									res_num_gam = []
								if len(res_que)>1:
									res_num_que = [item for sublist in res_que for item in sublist]
								if len(res_que)==1:
									res_num_que = copy.copy(res_que)
								if len(res_que)==0:
									res_num_que = []
								is_mixed_1=0
								is_mixed_2=0
								is_mixed_3=0
								is_mixed_4=0
								print("res_num_mat {}".format(res_mat))
								print("res_num_gam {}".format(res_num_gam))
								print("res_num_que {}".format(res_num_que))

								if len(res_num_gam)==1:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games made by 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[1]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
								elif len(res_num_gam)==2:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										print("numb ma 2")
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
								elif len(res_num_gam)==3:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
								elif len(res_num_gam)==4:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
									
									i = res_num_gam[2]
									if res_num_gam[3]==1:
										if res_num_que[i]==1:
											print("fourth match has a game all made by 1 quest")
											is_mixed_4 = 0
									if res_num_gam[3]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("fourth match has 2 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("fourth match has 3 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("fourth match has 4 games enquiry")
											is_mixed_4 = 1
								#print("MIXXXXX")
								#print(is_mixed_1)
								#print(is_mixed_2)
								#print(is_mixed_3)
								#print(is_mixed_4)
								
								#check control match in wichi there is ask_quest_type if generic and non mixed
								#q0 = "DROP TABLE IF EXISTS mini_tab"
								q0 = "DROP TABLE IF EXISTS mini_tab"
								bah = self.db_conn.execute_a_query(q0)
								
								#q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
								q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
									AS SELECT session_id,match_id FROM Sessions_Matches WHERE session_id = ?;"
								
								res_q1 = self.db_conn.execute_param_query(q1, result_sess_first[p])
								final_q1 = [item for sublist in res_q1 for item in sublist]
								print("the length current MAT IS {}".format(len(current_matches)))
								for j in range(len(current_matches)):
									print("numb loop tra i match {}".format(j))
									print("session: {} match: {}".format(result_sess_first,current_matches[j]))
									q2 = "SELECT oid FROM mini_tab WHERE match_id = ?"
									res_q2 = self.db_conn.execute_param_query(q2,current_matches[j])
									final_bo = res_q2
									print("res q2res q2res q2res q2 riga match = {}".format(res_q2))
									#final_bo = res_q2[0]
									print("final_bo {}".format(final_bo))
									#q3 = "DROP TABLE IF EXISTS mini_tab"
									result_sess = 0
									print("need_categories[0] ")
									print(need_categories[0])
									if len(final_bo)>0:
										if need_categories[0]=='gen_m':
											print("sono entrato nel gen_m")
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or \ 
												(final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess = result_sess_first[p]
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or \
												(final_bo[0]==3 and is_mixed_3) or (final_bo[0]==4 and is_mixed_4):
												result_sess = 0
										if need_categories[0]=='mix_m':
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or \ 
												(final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess = 0
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or \
												(final_bo[0]==3 and is_mixed_3) or (final_bo[0]==4 and is_mixed_4):
												result_sess = result_sess_first[p]
										print("result_sess {}".format(result_sess))
										if result_sess=0:
											all_ses.append(result_sess)
											print("all_session_id_results {}".format(all_ses))
							if len(all_ses)>0:
								try:
									results_need_total = list(itertools.chain.from_iterable(all_ses))
									print("results_need total after itertools is {}".format(all_ses))
								except Exception:
									results_need_total = list(self.iterFlatten(all_ses))
									print("results_need total after flatten is {}".format(all_ses))
								all_ses = list(set(all_ses))
								one_sess = random.choices(all_ses, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess {}".format(find_one_sess))
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end {}".format(result_sess))
							else:
								result_sess = []
								print("At the end error {}".format(result_sess))
						#elif need_categories[0]=='gen_g': # third case not exist with focus game
						elif need_categories[0]=='type_g':
							#need to solutions type 
							print("How was need?? {}".format(self.needs_list))
							params_case_second = (self.id_child, self.needs_list[0][1]) #receive directly the type not the id
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN Games USING (game_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Games.type = ?"
						
							result_sec = self.db_conn.execute_param_query(next_query, params_case_second)
							print(".......result_sec {}".format(result_sec))
							if len(result_sec)==1:
								print("1")
								print("result query is un solo index di sessione")
								result_sess_second = copy.copy(result_sec)
							if len(result_sec)>1:
								print("2")
								print("result query sono many indexes devo fare list compr")
								result_sess_second = [item for sublist in result_sec for item in sublist]
							if len(result_sec)==0:
								print("0")
								print("result query type = zero")
								result_sess_second = []
							
							print("result_sess_second {}".format(result_sess_second))

							result_sess_def = list(set(result_sess_first).intersection(result_sess_second))
							if len(result_sess_def)>0:
								one_sess = random.choices(result_sess_def, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess {}".format(find_one_sess))
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end as type {}".format(result_sess))
							else:
								result_sess = []
								print("At the end as type but it is wrong {}".format(result_sess))
																	
				elif len(self.impositions_list)==1 and len(res_id_needs)==1:
					if impo_categories[0]=='as':
						#print("we are IN IMPO ASK QUESTION")
						params_case_first = (self.id_child, self.impositions_list[0])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Impositions_Questions USING (question_id)) \
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do')\
						AND Impositions_Questions.imposition_name = ?"
					if impo_categories[0]=='ty':
						#print("we are IN IMPO TYPE GAME")
						#same type impo query for all 3 cases
						params_case_first = (self.id_child, dict_impo_type_g[self.impositions_list[0]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Questions USING (question_id))\
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do') \
						AND Questions.type = ?"
					if impo_categories[0]=='ki':
						#print("we are IN IMPO KIND GAME")
						#same type impo query for all 3 cases
						params_case_first = (self.id_child, dict_impo_kind_g[self.impositions_list[0]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Questions USING (question_id))\
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do') \
						AND Questions.kind = ?"
					if impo_categories[0]=='qu':
						to_do_ques, done_ques = [],[]
						#print("we are IN IMPO NEVER ASKED GAME")
						aa = "SELECT question_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Games_Questions USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND status ='to_do'"
						res_question_to_do= self.db_conn.execute_param_query(aa, self.id_child)
						print("res_question_to_do {}".format(res_question_to_do))
						if len(res_question_to_do)>1: #distinction between [0][0] fetchall result query, create list in this case 
							to_do_ques = [item for sublist in res_question_to_do for item in sublist]
						if len(res_question_to_do)==1:
							to_do_ques.append(res_question_to_do[0])
						print("OLD TEMP OLD TEMP OLD TEMP to do question {}".format(to_do_ques))

						bb = "SELECT question_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Games_Questions USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND status ='done'"
						res_question_done = self.db_conn.execute_param_query(bb, self.id_child)
						print("res_question_done {}".format(res_question_done))
						if len(res_question_done)>1: #distinction between [0][0] fetchall result query, create list in this case 
							done_ques = [item for sublist in res_question_done for item in sublist]
						if len(res_question_done)==1:
							done_ques.append(res_question_done[0])
						print("OLD TEMP OLD TEMP OLD TEMP done_ques {}".format(done_ques))
						ques_to_remove = list(set(to_do_ques).intersection(done_ques))
						print("quelli da rimuovere {}".format(ques_to_remove))
						if len(ques_to_remove)=0:
							result_ques_indexes = list(set(to_do_ques) - set(ques_to_remove))							
						else:
							result_ques_indexes = copy.copy(to_do_ques)
						print("result_ques_indexes Final COSA TROVO {}".format(result_ques_indexes))
						result_sess_first = []
						params_case_first = []
						for u in range(len(result_ques_indexes)):
							params_case_first.append(self.id_child)
							params_case_first.append(result_ques_indexes[u])
							session_for_this_ques_query = "SELECT Kids_Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN Games_Questions USING (game_id)) \
										USING (match_id)) \
								USING(session_id)) \
							USING(session_id) \
							WHERE kid_id = ? \
							AND status ='to_do' \
							AND question_id = ?"
							one_sess = self.db_conn.execute_param_query(session_for_this_ques_query, params_case_first)
							print("una sessione trovate is {}".format(one_sess))
							result_sess_first.append(one_sess)
							params_case_first = []
						result_sess_first = list(flatten(result_sess_first))
						print("finish session {}".format(result_sess_first))
						result_sess_first = list(set(result_sess_first))
						print("dove cercare 222 nuovo {}".format(result_sess_first))
					if impo_categories[0] in ('cr_se','cr_sn','cr_sm','cr_sh'):
						#print("we are IN IMPO complexity session")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN Matches USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Sessions.complexity = ?"
						if impo_categories[0]=='cr_se':
							params_case_first = (self.id_child, 'Easy')
						elif impo_categories[0]=='cr_sn':
							params_case_first = (self.id_child, 'Normal')
						elif impo_categories[0]=='cr_sm':
							params_case_first = (self.id_child, 'Medium')
						elif impo_categories[0]=='cr_sh':
							params_case_first = (self.id_child, 'Hard')
					if impo_categories[0] in ('cr_e1','cr_e2','cr_n1','cr_n2','cr_m1','cr_m2','cr_h1','cr_h2'):
						#need to solutionts separated impo e need
						#print("we are IN IMPO DIFFICULTIY game")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN Matches USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Matches.difficulty = ?"
						if impo_categories[0]=='cr_e1':
							params_case_first = (self.id_child, 'Easy_1')
						elif impo_categories[0]=='cr_e2':
							params_case_first = (self.id_child, 'Easy_2')
						elif impo_categories[0]=='cr_n1':
							params_case_first = (self.id_child, 'Normal_1')
						elif impo_categories[0]=='cr_n2':
							params_case_first = (self.id_child, 'Normal_2')
						elif impo_categories[0]=='cr_m1':
							params_case_first = (self.id_child, 'Medium_1')
						elif impo_categories[0]=='cr_m2':
							params_case_first = (self.id_child, 'Medium_2')
						elif impo_categories[0]=='cr_h1':
							params_case_first = (self.id_child, 'Hard_1')
						elif impo_categories[0]=='cr_h2':
							params_case_first = (self.id_child, 'Hard_2')
					if impo_categories[0] in ('cr_e1g','cr_e2g','cr_n1g','cr_n2g','cr_m1g','cr_m2g','cr_h1g','cr_h2g'):
						#need to solutionts separated impo e need
						#print("we are IN IMPO DIFFICULTIY game")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN Games USING (game_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Games.difficulty = ?"
						
						if impo_categories[0]=='cr_e1g':
							params_case_first = (self.id_child, 'Easy_1')
						elif impo_categories[0]=='cr_e2g':
							params_case_first = (self.id_child, 'Easy_2')
						elif impo_categories[0]=='cr_n1g':
							params_case_first = (self.id_child, 'Normal_1')
						elif impo_categories[0]=='cr_n2g':
							params_case_first = (self.id_child, 'Normal_2')
						elif impo_categories[0]=='cr_m1g':
							params_case_first = (self.id_child, 'Medium_1')
						elif impo_categories[0]=='cr_m2g':
							params_case_first = (self.id_child, 'Medium_2')
						elif impo_categories[0]=='cr_h1g':
							params_case_first = (self.id_child, 'Hard_1')
						elif impo_categories[0]=='cr_h2g':
							params_case_first = (self.id_child, 'Hard_2')							
					if impo_categories[0] in ('ch1_m','ch2_m','ch3_m','ch4_m','ch1_g','ch2_g','ch3_g','ch4_g','ch1_q','ch2_q','ch3_q','ch4_q'):
						params_case_first = (self.id_child)
						if impo_categories[0]=='ch1_m':
							#print("we are IN IMPO NUM 1 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN Sessions_Matches USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 1"
						if impo_categories[0]=='ch2_m':
							#print("we are IN IMPO NUM 2 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN Sessions_Matches USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 2"
						if impo_categories[0]=='ch3_m':
							#print("we are IN IMPO NUM 3 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN Sessions_Matches USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 3"
						if impo_categories[0]=='ch4_m':
							#print("we are IN IMPO NUM 4 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN Sessions_Matches USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 4"
						if impo_categories[0]=='ch1_g':
							#print("we are IN IMPO NUM 1 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 1"
						if impo_categories[0]=='ch2_g':
							#print("we are IN IMPO NUM 2 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 2"
						if impo_categories[0]=='ch3_g':
							#print("we are IN IMPO NUM 3 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 3"
						if impo_categories[0]=='ch4_g':
							#print("we are IN IMPO NUM 4 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 4"
						if impo_categories[0]=='ch1_q':
							#print("we are IN IMPO NUM 1 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 1"
						if impo_categories[0]=='ch2_q':
							#print("we are IN IMPO NUM 2 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 2"
						if impo_categories[0]=='ch3_q':
							#print("we are IN IMPO NUM 3 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 3"
						if impo_categories[0]=='ch4_q':
							#print("we are IN IMPO NUM 4 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 4"
					if impo_categories[0]=='mi':
						#print("we are IN IMPO mixed match")
						ok_stop_here = 1
						par = (self.id_child)
						sess_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Games JOIN (Games USING(game_id)) \
							USING (session_id)) \
						USING (session_id) \
						WHERE kid_id = ? \
						AND kind = 'MIX' \
						AND (status ='default' or status ='to_do')"
						se_id = self.db_conn.execute_param_query(sess_query, par)						
						if len(se_id)==1:
							result_sess_first = se_id[0]
						if len(se_id)>1:
							result_sess_first = [item for sublist in se_id for item in sublist]
						else:
							result_sess_first = []
						if ok_stop_here=1:
							par = (self.id_child)
							sess_query = "SELECT Sessions.session_id FROM Sessions JOIN Kids_Sessions USING (session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do')"
							se_id = self.db_conn.execute_param_query(sess_query, par)
							
							if len(se_id)>0:
								res_all_sessions = [item for sublist in se_id for item in sublist]
							else:
								res_all_sessions = []
							print("first impression {}".format(res_all_sessions))
							
							for p in range(len(res_all_sessions)):
								param_diff = (res_all_sessions[p])
								m_query = "SELECT match_id FROM Sessions JOIN Sessions_Matches USING (session_id) \
								WHERE session_id = ?"
								res_matches = self.db_conn.execute_param_query(m_query, param_diff)
								if len(res_matches)>1: 
									current_matches = [item for sublist in res_matches for item in sublist]
								if len(res_matches)==1: 
									current_matches = copy.copy(res_matches)
								if len(res_matches)==0: 
									current_matches = []
								print("current_matches is : ",current_matches)
								print("evaluate session result sess {}".format(current_matches))
								second_q_1 = "SELECT num_of_matches FROM Sessions WHERE session_id = ?"
								second_q_2 = "SELECT num_of_games FROM Sessions JOIN \
												(Sessions_Matches JOIN Matches USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								second_q_3 = "SELECT num_questions FROM Sessions JOIN \
												(Sessions_Matches JOIN \
													(Matches_Games JOIN Games USING (game_id)) \
													USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								res_mat = self.db_conn.execute_param_query(second_q_1, res_all_sessions[p])
								res_gam = self.db_conn.execute_param_query(second_q_2, res_all_sessions[p])
								res_que = self.db_conn.execute_param_query(second_q_3, res_all_sessions[p])
								if len(res_gam)>1:
									res_num_gam = [item for sublist in res_gam for item in sublist]
								elif len(res_gam)==1:
									res_num_gam = copy.copy(res_gam)
								elif len(res_gam)==0:
									res_num_gam = []
								elif len(res_que)>1:
									res_num_que = [item for sublist in res_que for item in sublist]
								elif len(res_que)==1:
									res_num_que = copy.copy(res_que)
								elif len(res_que)==0:
									res_num_que = []
								is_mixed_1=0
								is_mixed_2=0
								is_mixed_3=0
								is_mixed_4=0
								print("res_num_mat res_num_mat {}".format(res_mat))
								print("res_num_gam res_num_gam {}".format(res_num_gam))
								print("res_num_que res_num_que {}".format(res_num_que))
								if len(res_num_gam)==1:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has games all composed by only a question")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("the match has two games with both one question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1:
											is_mixed_1 = 1
										if res_num_que[1]==1:
											is_mixed_1 = 1
										if res_num_que[2]==1:
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by one question")
											is_mixed_1 = 1
								elif len(res_num_gam)==2:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has all games with 1 question")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has two games with 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games with 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match with a games with one question")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has two games all made by one question")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has three games all made by one question")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											is_mixed_2 = 1
								elif len(res_num_gam)==3:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											is_mixed_2 = 1
									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											is_mixed_3 = 1
								elif len(res_num_gam)==4:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											is_mixed_2 = 1
									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											is_mixed_3 = 1
									i = res_num_gam[2]
									if res_num_gam[3]==1:
										if res_num_que[i]==1:
											is_mixed_4 = 0
									if res_num_gam[3]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											is_mixed_4 = 1
									if res_num_gam[3]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											is_mixed_4 = 1
									if res_num_gam[3]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											is_mixed_4 = 1
								#print("MIXXXXX")
								#print(is_mixed_1)
								#print(is_mixed_2)
								#print(is_mixed_3)
								#print(is_mixed_4)
								if is_mixed_1=0 or is_mixed_2=0 or is_mixed_3=0 or is_mixed_4=0:
									# controls that the match with lipositione (ask quest type) sia generic e non mixed
									#q0 = "DROP TABLE IF EXISTS mini_tab"
									q0 = "DROP TABLE IF EXISTS mini_tab"
									bah = self.db_conn.execute_a_query(q0)
									#q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
									q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
										AS SELECT session_id,match_id FROM Sessions_Matches WHERE session_id = ?;"
									res_q1 = self.db_conn.execute_param_query(q1, res_all_sessions[p])
									final_q1 = [item for sublist in res_q1 for item in sublist]
									print("the length current MAT IS {}".format(len(current_matches)))
									for j in range(len(current_matches)):
										print("numb numb numb numb numb numb numb numb loop tra i match {}".format(j))
										print("session: {} match: {}".format(res_all_sessions,current_matches[j]))
										q2 = "SELECT oid FROM mini_tab WHERE match_id = ?"
										res_q2 = self.db_conn.execute_param_query(q2,current_matches[j])
										final_bo = res_q2
										print("res q2res q2res q2res q2 riga match = {}".format(res_q2))
										#final_bo = res_q2[0]
										print("final_bo {}".format(final_bo))
										#q3 = "DROP TABLE IF EXISTS mini_tab"
										result_sess = 0
										if len(final_bo)>0:
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or (final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess = 0
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or (final_bo[0]==3 and is_mixed_3) or (final_bo[0]==4 and is_mixed_4):
												result_sess = res_all_sessions[p]
											print("That is {}".format(result_sess))
											if result_sess=0:
												all_session_id_results.append(res_all_sessions[p])
												print("all_session_id_results {}".format(all_session_id_results))
									
							print("all_session_id_results {}".format(all_session_id_results))
							if len(all_session_id_results)==1:
								result_sess_first = copy.copy(all_session_id_results)
							if len(all_session_id_results)>1:
								result_sess_first = [item for sublist in all_session_id_results for item in sublist]
							if len(all_session_id_results)==0:
								result_sess_first = []
							try:
								result_sess_first = list(itertools.chain.from_iterable(result_sess_first))
								print("result_sess_first total after itertools is {}".format(result_sess_first))
							except Exception:
								result_sess_first = list(self.iterFlatten(result_sess_first))
								print("result_sess_first total after flatten is {}".format(result_sess_first))
							result_sess_first = list(set(result_sess_first))
							print("result_sess_first total after il SET is {}".format(result_sess_first))
							
					if impo_categories[0] not in ('qu','mi'):
						result_ques = self.db_conn.execute_param_query(next_query, params_case_first)
						if len(result_ques)==1:
							result_sess_first = copy.copy(result_ques)
						if len(result_ques)>1:
							result_sess_first = [item for sublist in result_ques for item in sublist]
						if len(result_ques)==0:
							result_sess_first = []

					print("IMPO {}".format(result_sess_first))
					if len(result_sess_first)>0:
						print("NEED CATEGORY question")
						if need_categories[0]=='ques':
							#need to solutionts separated impo e need
							params_case_second = (self.id_child, res_id_needs[0])
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN \
											(Games JOIN \
												(Games_Questions JOIN Needs_Questions USING (question_id)) \
												USING (game_id)) \
											USING (game_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Needs_Questions.need_id = ?"
						
							result_ques = self.db_conn.execute_param_query(next_query, params_case_second)
							if len(result_ques)==1:
								result_sess_second = copy.copy(result_ques)
							if len(result_ques)>1:
								result_sess_second = [item for sublist in result_ques for item in sublist]
							if len(result_ques)==0:
								result_sess_second = []
							print("recap ques as")
							print("result_sess_first ", result_sess_first)
							print("result_sess_second ", result_sess_second)
							result_sess_def = list(set(result_sess_first).intersection(result_sess_second))
							if len(result_sess_def)>0:
								one_sess = random.choices(result_sess_def, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess ", find_one_sess)
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end as ques {}".format(result_sess))
							else:
								result_sess = []
								print("At the end as ques but it is wrong {}".format(result_sess))
							print("result_sess intersezione ", result_sess_def)
							print("result_sess definitive ", result_sess)
						elif need_categories[0]=='gen_m' or need_categories[0]=='mix_m':
							#print("we are IN NEED SCOPE MATCH")
							for p in range(len(result_sess_first)):
								all_ses = []
								
								param_diff = (result_sess_first[p])
								m_query = "SELECT match_id FROM Sessions JOIN Sessions_Matches USING (session_id) \
								WHERE session_id = ?"
								res_matches = self.db_conn.execute_param_query(m_query, param_diff)
								if len(res_matches)>1: 
									current_matches = [item for sublist in res_matches for item in sublist]
								if len(res_matches)==1:
									current_matches = copy.copy(res_matches)
								if len(res_matches)==0:
									current_matches = []
								print("current_matches is ::",current_matches)
								print("evaluate session result sess {}".format(current_matches))

								second_q_1 = "SELECT num_of_matches FROM Sessions WHERE session_id = ?"
								second_q_2 = "SELECT num_of_games FROM Sessions JOIN \
												(Sessions_Matches JOIN Matches USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								second_q_3 = "SELECT num_questions FROM Sessions JOIN \
												(Sessions_Matches JOIN \
													(Matches_Games JOIN Games USING (game_id)) \
													USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								res_mat = self.db_conn.execute_param_query(second_q_1, result_sess_first[p])
								res_gam = self.db_conn.execute_param_query(second_q_2, result_sess_first[p])
								res_que = self.db_conn.execute_param_query(second_q_3, result_sess_first[p])

								if len(res_gam)>1:
									res_num_gam = [item for sublist in res_gam for item in sublist]
								if len(res_gam)==1:
									res_num_gam = copy.copy(res_gam)
								if len(res_gam)==0:
									res_num_gam = []
								if len(res_que)>1:
									res_num_que = [item for sublist in res_que for item in sublist]
								if len(res_que)==1:
									res_num_que = copy.copy(res_que)
								if len(res_que)==0:
									res_num_que = []
								is_mixed_1=0
								is_mixed_2=0
								is_mixed_3=0
								is_mixed_4=0
								print("res_num_mat res_num_mat {}".format(res_mat))
								print("res_num_gam res_num_gam {}".format(res_num_gam))
								print("res_num_que res_num_que {}".format(res_num_que))

								if len(res_num_gam)==1:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games made by 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[1]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
								elif len(res_num_gam)==2:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										print("numb ma 2")
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
								elif len(res_num_gam)==3:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
								elif len(res_num_gam)==4:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
									i = res_num_gam[2]
									if res_num_gam[3]==1:
										if res_num_que[i]==1:
											print("fourth match has a game all made by 1 quest")
											is_mixed_4 = 0
									if res_num_gam[3]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("fourth match has 2 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("fourth match has 3 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("fourth match has 4 games enquiry")
											is_mixed_4 = 1
								#print("MIXXXXX")
								#print(is_mixed_1)
								#print(is_mixed_2)
								#print(is_mixed_3)
								#print(is_mixed_4)
								#check control match in wichi there is ask_quest_type if generic and non mixed
								#q0 = "DROP TABLE IF EXISTS mini_tab"
								q0 = "DROP TABLE IF EXISTS mini_tab"
								bah = self.db_conn.execute_a_query(q0)
								#q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
								q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
									AS SELECT session_id,match_id FROM Sessions_Matches WHERE session_id = ?;"
								
								res_q1 = self.db_conn.execute_param_query(q1, result_sess_first[p])
								final_q1 = [item for sublist in res_q1 for item in sublist]
								print("the length current MAT IS {}".format(len(current_matches)))
								for j in range(len(current_matches)):
									print("numb numb numb numb numb numb numb numb loop tra i match {}".format(j))
									print("session: {} match: {}".format(result_sess_first,current_matches[j]))
									q2 = "SELECT oid FROM mini_tab WHERE match_id = ?"
									res_q2 = self.db_conn.execute_param_query(q2,current_matches[j])
									final_bo = res_q2
									print("res q2res q2res q2res q2 riga match = {}".format(res_q2))
									#final_bo = res_q2[0]
									print("final_bo {}".format(final_bo))
									#q3 = "DROP TABLE IF EXISTS mini_tab"
									result_sess = 0
									print("need_categories[0] ")
									print(need_categories[0])
									if len(final_bo)>0:
										if need_categories[0]=='gen_m':
											print("sono entrato nel gen_m")
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or (final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess = result_sess_first[p]
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or (final_bo[0]==3 and is_mixed_3) or (final_bo[0]==4 and is_mixed_4):
												result_sess = 0
										if need_categories[0]=='mix_m':
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or (final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess = 0
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or (final_bo[0]==3 and is_mixed_3) or (final_bo[0]==4 and is_mixed_4):
												result_sess = result_sess_first[p]
										print("That is {}".format(result_sess))
										if result_sess=0:
											all_ses.append(result_sess)
											print("all_session_id_results {}".format(all_ses))
							if len(all_ses)>0:
								try:
									results_need_total = list(itertools.chain.from_iterable(all_ses))
									print("results_need total after itertools is {}".format(all_ses))
								except Exception:
									results_need_total = list(self.iterFlatten(all_ses))
									print("results_need total after flatten is {}".format(all_ses))
								all_ses = list(set(all_ses))
								one_sess = random.choices(all_ses, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess {}".format(find_one_sess))
								
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end {}".format(result_sess))
							else:
								result_sess = []
								print("At the end but it is wrong {}".format(result_sess))
						#elif need_categories[0]=='gen_g':
						# third case not exists in focus level
						# --> pass to step 3
						elif need_categories[0]=='type_g':
							#need to solutionts type 
							print("How was need?? {}".format(self.needs_list))
							params_case_second = (self.id_child, self.needs_list[0][1]) #receive directly the type not the id
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches_Games JOIN Games USING (game_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Games.type = ?"
							result_sec = self.db_conn.execute_param_query(next_query, params_case_second)
							print(".......result_sec {}".format(result_sec))
							if len(result_sec)==1:
								print("1")
								print("result query is un solo index di sessione")
								result_sess_second = copy.copy(result_sec)
							if len(result_sec)>1:
								print("2")
								print("result query sono many indexes devo fare list compr")
								result_sess_second = [item for sublist in result_sec for item in sublist]
							if len(result_sec)==0:
								print("0")
								print("result query type = zero")
								result_sess_second = []
							print("result_sess_second {}".format(result_sess_second))
							result_sess_def = list(set(result_sess_first).intersection(result_sess_second))
							if len(result_sess_def)>0:
								one_sess = random.choices(result_sess_def, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess {}".format(find_one_sess))
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end as type {}".format(result_sess))
							else:
								result_sess = []
								print("At the end as type but it is wrong {}".format(result_sess))
				#if more than 1 --> go to step 3 directly
				else:
					print("go directly to step 3")
		else:
			result_sess = []
		if len(result_sess)=0:
			ready = result_sess[0]
			print("READY is ", ready)
			#step 3
			#creation of a session ad hoc -->randomforest modified
			#customized = []
			#customized.append()
		else:
			ready = []
		return ready
		
	cpdef realtime_creation(self):
		ready = self.extract_ready_session()
		if ready:
			self.create_obj_session(ready,self.id_child)
		else:
			print("STEP 3: ")
			impos_needs = []
			impos_needs.append(self.impositions_list)
			impos_needs.append(self.needs_list)
			impos_needs.append(self.level_area)
			print("impos_needs to pass {}".format(impos_needs))
			manda_impo = 1 #always 1 , if kid is specified ...
			manda_need = 1
			self.db_conn.add_new_Session(self.id_child,manda_impo,manda_need,impos_needs)
			next_query = "SELECT Sessions.session_id FROM Sessions ORDER BY session_id DESC LIMIT 1" 
			#next_query #==> ready
			sess_id = self.db_conn.execute_a_query(next_query)
			sess_id = sess_id[0]
			for imp in self.impositions_list:
				self.db_conn.add_new_SessionImposition(sess_id,imp)
			for nee in self.needs_list:
				n_param = (self.needs_list[0][0],self.needs_list[0][1],self.needs_list[0][1],self.needs_list[0][1])
				n_query = "SELECT need_id FROM Needs WHERE focus=? AND (match_scope=? or game_scope=? or question_scope=?)"
				need_id_is = self.db_conn.execute_param_query(n_query,n_param)
				print("need_id_is need_id_is need_id_is {}".format(need_id_is))
				n_id = need_id_is[0]
				print("n_id n_id n_id {}".format(n_id))
				self.db_conn.add_new_SessionNeed(sess_id,n_id)
			print("RESULT SESS BEFORE CREATING OBJ {}".format(sess_id))
			self.create_obj_session(sess_id,self.id_child)

cdef class Session_3(SessionBase):
	def __cinit__(self, int id_child, list impositions_list, list needs_list, str level_area, str id_scenario):
		self.id_child = id_child
		self.impositions_list = impositions_list
		self.needs_list = needs_list
		self.level_area = level_area
		self.id_scenario = id_scenario
		self.db_conn = db.DatabaseManager()

	cpdef extract_ready_session(self):
		cdef:
			bint way_scen = 0
			bint ask_pos_scen = 0
			bint ins_scen_diff = 0
		""" Prepare session find specific impo and need information"""
		child_query = "SELECTcurrent_level, age FROM Kids WHERE kid_id = ?"
		#print(":::::::::::::::")
		#print(self.id_child)
		#print(self.impositions_list)
		#print(self.needs_list)
		#print(self.level_area)
		#print(":::::::::::::::")
		kid_info = self.db_conn.execute_param_query(child_query, self.id_child)
		if not kid_info:
			manda_impo = 1
			manda_need = 1
			self.db_conn.add_new_Session(self.id_child,manda_impo,manda_need)
			return

		level_is = kid_info[0]
		age_is = kid_info[1]
		print(kid_info)
		print(level_is)
		print("length imposition {}".format(len(self.impositions_list)))
		print("length need {}".format(len(self.needs_list)))
		impo_categories = []
		need_categories = []
		id_needs_final, scopes_final, focuses_final = [],[],[]
		dict_impo_kind_g = self.get_dict_kind_imposition()
		dict_impo_type_g = self.get_dict_imposition()
		impo_categories = self.get_categ_imposition()
		#print("DICT_IMPO obtained")
		#print(dict_impo_type_g)
		#print(dict_impo_kind_g)
		#print("CATEGORI_IMPO obtained")
		print(impo_categories)
		if len(self.impositions_list)>6: 
			remove_me = random.randint(0,6)
			del(self.impositions_list[remove_me])
		first_list_of_sess = []
		second_list_of_sess = []
		result_list_of_sess = []
		impo_list_of_sess = []
		all_session_id_results = []
		for l in range(len(self.needs_list)):
			print("needs_list 0",self.needs_list[l][0])
			print("needs_list 1",self.needs_list[l][1])
			find_need_sql = "SELECT need_id FROM Needs WHERE focus = ? AND (match_scope = ? OR game_scope = ? OR question_scope = ?)"
			id_params = (self.needs_list[l][0], self.needs_list[l][1], self.needs_list[l][1], self.needs_list[l][1])
			res_id_needs = self.db_conn.execute_param_query(find_need_sql, id_params)
			print("res_id_needs res_id_needs {}".format(res_id_needs))
			needs_query = "SELECT match_scope, game_scope, question_scope FROM Needs WHERE need_id = ?"
			res_scopes = self.db_conn.execute_param_query(needs_query, res_id_needs[0])
			print("res_scopes {}".format(res_scopes))
			if res_scopes[0]=='generic_match':
				need_categories.append('gen_m')
			elif res_scopes[0]=='mixed_match':
				need_categories.append('mix_m')
			elif res_scopes[0]=='Invalid':
				if res_scopes[1]=='generic_game':
					need_categories.append('gen_g')
				elif res_scopes[1]='Invalid':
					need_categories.append('type_g')
				elif res_scopes[1]=='Invalid':
					if res_scopes[2]='Invalid':
						need_categories.append('ques')
					else:
						need_categories.append('no_need')

			print("need_categories {}".format(need_categories))
			id_needs_final.append(res_id_needs)
			focuses_final.append(self.needs_list[l][0])
			print("id_needs_final{}".format(id_needs_final))
			print("focuses{}".format(focuses_final))
		redo = acp2.create_new_anyway(self.impositions_list)
		if redo==0:
			if len(self.impositions_list)==1 and len(self.needs_list)==0:
				params_case10 = (self.id_child, self.impositions_list[0],self.id_scenario)
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Impositions JOIN \
							(Sessions_Matches JOIN Matches USING(match_id)) \
							USING (session_id)) \
						USING(session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status='default' \
				AND imposition_name = ? \
				AND Matches.scenario = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case10)
			elif len(self.impositions_list)==0 and len(self.needs_list)==1:
				params_case01 = (self.id_child, res_id_needs[0],self.id_scenario)
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							(Sessions_Matches JOIN Matches USING(match_id)) \
							USING (session_id)) \
						USING(session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status='default' \
				AND need_id = ? \
				AND Matches.scenario = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case01)
			elif len(self.impositions_list)==1 and len(self.needs_list)==1:
				params_case11 = (self.id_child, self.impositions_list[0], res_id_needs[0],self.id_scenario)
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							(Sessions_Impositions JOIN \
								(Sessions_Matches JOIN Matches USING(match_id)) \
								USING (session_id)) \
							USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND imposition_name = ? AND need_id = ? \
				AND Matches.scenario = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case11)
			elif len(self.impositions_list)==2 and len(self.needs_list)==1:
				params_case21 = (self.id_child, self.impositions_list[0], self.impositions_list[1], res_id_needs[0],self.id_scenario)
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							(Sessions_Impositions JOIN\
								(Sessions_Matches JOIN Matches USING(match_id)) \
								USING (session_id)) \
							USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? OR imposition_name = ?) AND need_id = ? \
				AND Matches.scenario = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case21)
			elif len(self.impositions_list)==1 and len(self.needs_list)==2:
				params_case12 = (self.id_child, self.impositions_list[0], res_id_needs[0], res_id_needs[1],self.id_scenario)
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							(Sessions_Impositions JOIN\
								(Sessions_Matches JOIN Matches USING(match_id)) \
								USING (session_id)) \
							USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? AND (need_id = ? OR need_id = ?) \
				AND Matches.scenario = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case12)
			elif len(self.impositions_list)==3 and len(self.needs_list)==1:
				params_case31 = (self.id_child, self.impositions_list[0], self.impositions_list[1], self.impositions_list[2], 
					res_id_needs[0],self.id_scenario)
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							(Sessions_Impositions JOIN \
								(Sessions_Matches JOIN Matches USING(match_id)) \
								USING (session_id)) \
							USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? OR imposition_name = ? OR imposition_name = ?) AND need_id = ? \
				AND Matches.scenario = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case31)
			elif len(self.impositions_list)==1 and len(self.needs_list)==3:
				params_case13 = (self.id_child, self.impositions_list[0], self.needs_list[0], 
					res_id_needs[1], res_id_needs[2],self.id_scenario)
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							(Sessions_Impositions JOIN \
								(Sessions_Matches JOIN Matches USING(match_id)) \
								USING (session_id)) \
							USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? AND (need_id = ? OR need_id = ? OR need_id = ?) \
				AND Matches.scenario = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case13)
			elif len(self.impositions_list)==2 and len(self.needs_list)==2:
				params_case22 = (self.id_child, self.impositions_list[0], self.impositions_list[1], 
					res_id_needs[0], res_id_needs[1],self.id_scenario)
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							(Sessions_Impositions JOIN \
								(Sessions_Matches JOIN Matches USING(match_id)) \
								USING (session_id)) \
							USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? OR imposition_name = ?) AND (need_id = ? OR need_id = ?) \
				AND Matches.scenario = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case22)
			elif len(self.impositions_list)==3 and len(self.needs_list)==3:
				params_case33 = (self.id_child, self.impositions_list[0], self.impositions_list[1], self.impositions_list[2], 
					res_id_needs[0], res_id_needs[1], res_id_needs[2], self.id_scenario)
				next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
					(Kids_Sessions JOIN \
						(Sessions_Needs JOIN \
							(Sessions_Impositions JOIN \
								(Sessions_Matches JOIN Matches USING(match_id)) \
								USING (session_id)) \
							USING (session_id)) \
						USING (session_id)) \
					USING(session_id) \
				WHERE kid_id = ? \
				AND status ='default' \
				AND (imposition_name = ? OR imposition_name = ? OR imposition_name = ?) \
				AND (need_id = ? OR need_id = ? OR need_id = ?) \
				AND Matches.scenario = ? \
				ORDER BY random() LIMIT 1"
				result_sess = self.db_conn.execute_param_query(next_query, params_case33)	
			if len(result_sess)=0: #---> step2
				print("result_sess {}".format(result_sess))
				print("ok found without doing STEP 2")
			else:
				#step 2 
				print("++STEP 2")
				#check inside...I need all possible combinations inside session
				print("IMPO CATCATCATCATCATCAT {}".format(impo_categories))
				if len(self.impositions_list)==1 and len(self.needs_list)==0:	
					print("ONLY IMPO NO NEED")
					if impo_categories[0]=='as':
						#print("we are IN IMPO ASK QUESTION")
						params_case_first = (self.id_child, self.id_scenario, self.impositions_list[0])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Impositions_Questions USING (question_id)) \
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do') \
						AND Matches.scenario = ? \
						AND Impositions_Questions.imposition_name = ?"
					if impo_categories[0]=='ty':
						#print("we are IN IMPO TYPE GAME")
						#same type impo query for all 3 cases
						params_case_first = (self.id_child, self.id_scenario, dict_impo_type_g[self.impositions_list[0]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Questions USING (question_id))\
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do') \
						AND Questions.type = ?" 
					if impo_categories[0]=='ki':
						#print("we are IN IMPO KIND GAME")
						#same type impo query for all 3 cases
						params_case_first = (self.id_child, self.id_scenario, dict_impo_kind_g[self.impositions_list[0]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Questions USING (question_id))\
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do') \
						AND Questions.kind = ?" 
					if impo_categories[0]=='qu':
						to_do_ques, done_ques = [],[]
						#print("we are IN IMPO NEVER ASKED GAME")
						aa = "SELECT question_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Games_Questions USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND status ='to_do'"
						res_question_to_do= self.db_conn.execute_param_query(aa, self.id_child)
						print("res_question_to_do {}".format(res_question_to_do))
						if len(res_question_to_do)>1: #distinction between [0][0] fetchall result query, create list in this case 
							to_do_ques = [item for sublist in res_question_to_do for item in sublist]
						if len(res_question_to_do)==1:
							to_do_ques.append(res_question_to_do[0])
						print("OLD TEMP OLD TEMP OLD TEMP to do question {}".format(to_do_ques))
						param_bb = (self.id_child,)
						bb = "SELECT question_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Games_Questions USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND status ='done'"
						res_question_done = self.db_conn.execute_param_query(bb, param_bb)
						print("res_question_done {}".format(res_question_done))
						if len(res_question_done)>1: #distinction between [0][0] fetchall result query, create list in this case 
							done_ques = [item for sublist in res_question_done for item in sublist]
						if len(res_question_done)==1:
							done_ques.append(res_question_done[0])
						print("OLD TEMP OLD TEMP OLD TEMP done_ques {}".format(done_ques))

						ques_to_remove = list(set(to_do_ques).intersection(done_ques))
						print("quelli da rimuovere {}".format(ques_to_remove))
						
						if len(ques_to_remove)=0:
							result_ques_indexes = list(set(to_do_ques) - set(ques_to_remove))							
						else:
							result_ques_indexes = copy.copy(to_do_ques)
						print("result_ques_indexes Final COSA TROVO {}".format(result_ques_indexes))
						result_sess_first = []
						params_case_first = []
						for u in range(len(result_ques_indexes)):
							params_case_first.append(self.id_child)
							params_case_first.append(self.id_scenario)
							params_case_first.append(result_ques_indexes[u])
							print("params_case_first params_case_firstparams_case_firstparams_case_first {}".format(params_case_first))
							session_for_this_ques_query = "SELECT Kids_Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games_Questions USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
								USING(session_id)) \
							USING(session_id) \
							WHERE kid_id = ? \
							AND status ='to_do' \
							AND Matches.scenario = ? \
							AND question_id = ?"

							one_sess = self.db_conn.execute_param_query(session_for_this_ques_query, params_case_first)
							print("una sessione trovate is {}".format(one_sess))
							result_sess_first.append(one_sess)
							params_case_first = []
						result_sess_first = list(flatten(result_sess_first))
						print("finish session {}".format(result_sess_first))
						result_sess_first = list(set(result_sess_first))
						print("dove cercare 222 nuovo {}".format(result_sess_first))
					if impo_categories[0] in ('cr_se','cr_sn','cr_sm','cr_sh'):
						#print("we are IN IMPO complexity session")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions) JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Matches USING (match_id)) \
									USING (match_id)) \
								USING(session_id)) \
							USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Sessions.complexity = ?"
						if impo_categories[0]=='cr_se':
							params_case_first = (self.id_child, 'Easy')
						elif impo_categories[0]=='cr_sn':
							params_case_first = (self.id_child, 'Normal')
						elif impo_categories[0]=='cr_sm':
							params_case_first = (self.id_child, 'Medium')
						elif impo_categories[0]=='cr_sh':
							params_case_first = (self.id_child, 'Hard')						
					if impo_categories[0] in ('cr_e1','cr_e2','cr_n1','cr_n2','cr_m1','cr_m2','cr_h1','cr_h2'):
						#need to solutionts separated impo e need
						#print("we are IN IMPO DIFFICULTIY match")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND Matches.scenario = ? \
							AND (status ='default' or status ='to_do') \
							AND Matches.difficulty = ?"
						if impo_categories[0]=='cr_e1':
							params_case_first = (self.id_child, self.id_scenario, 'Easy_1')
						elif impo_categories[0]=='cr_e2':
							params_case_first = (self.id_child, self.id_scenario, 'Easy_2')
						elif impo_categories[0]=='cr_n1':
							params_case_first = (self.id_child, self.id_scenario, 'Normal_1')
						elif impo_categories[0]=='cr_n2':
							params_case_first = (self.id_child, self.id_scenario, 'Normal_2')
						elif impo_categories[0]=='cr_m1':
							params_case_first = (self.id_child, self.id_scenario, 'Medium_1')
						elif impo_categories[0]=='cr_m2':
							params_case_first = (self.id_child, self.id_scenario, 'Medium_2')
						elif impo_categories[0]=='cr_h1':
							params_case_first = (self.id_child, self.id_scenario, 'Hard_1')
						elif impo_categories[0]=='cr_h2':
							params_case_first = (self.id_child, self.id_scenario, 'Hard_2')
					if impo_categories[0] in ('cr_e1g','cr_e2g','cr_n1g','cr_n2g','cr_m1g','cr_m2g','cr_h1g','cr_h2g'):
						#need to solutionts separated impo e need
						#print("we are IN IMPO DIFFICULTIY game")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND Matches.scenario = ? \
							AND (status ='default' or status ='to_do') \
							AND Games.difficulty = ?"
						if impo_categories[0]=='cr_e1g':
							params_case_first = (self.id_child, self.id_scenario, 'Easy_1')
						elif impo_categories[0]=='cr_e2g':
							params_case_first = (self.id_child, self.id_scenario, 'Easy_2')
						elif impo_categories[0]=='cr_n1g':
							params_case_first = (self.id_child, self.id_scenario, 'Normal_1')
						elif impo_categories[0]=='cr_n2g':
							params_case_first = (self.id_child, self.id_scenario, 'Normal_2')
						elif impo_categories[0]=='cr_m1g':
							params_case_first = (self.id_child, self.id_scenario, 'Medium_1')
						elif impo_categories[0]=='cr_m2g':
							params_case_first = (self.id_child, self.id_scenario, 'Medium_2')
						elif impo_categories[0]=='cr_h1g':
							params_case_first = (self.id_child, self.id_scenario, 'Hard_1')
						elif impo_categories[0]=='cr_h2g':
							params_case_first = (self.id_child, self.id_scenario, 'Hard_2')
					if impo_categories[0] in ('ch1_m','ch2_m','ch3_m','ch4_m','ch1_g','ch2_g','ch3_g','ch4_g','ch1_q','ch2_q','ch3_q','ch4_q'):					
						params_case_first = (self.id_child,self.id_scenario)
						if impo_categories[0]=='ch1_m':
							#print("we are IN IMPO NUM 1 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING(match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.scenario = ? \
								AND Sessions.num_of_matches = 1"
						if impo_categories[0]=='ch2_m':
							#print("we are IN IMPO NUM 2 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING(match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.scenario = ? \
								AND Sessions.num_of_matches = 2"
						if impo_categories[0]=='ch3_m':
							#print("we are IN IMPO NUM 3 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING(match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.scenario = ? \
								AND Sessions.num_of_matches = 3"
						if impo_categories[0]=='ch4_m':
							#print("we are IN IMPO NUM 4 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING(match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.scenario = ? \
								AND Sessions.num_of_matches = 4"
						if impo_categories[0]=='ch1_g':
							#print("we are IN IMPO NUM 1 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 1"
						if impo_categories[0]=='ch2_g':
							#print("we are IN IMPO NUM 2 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 2"
						if impo_categories[0]=='ch3_g':
							#print("we are IN IMPO NUM 3 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 3"
						if impo_categories[0]=='ch4_g':
							#print("we are IN IMPO NUM 4 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 4"
						if impo_categories[0]=='ch1_q':
							#print("we are IN IMPO NUM 1 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 1"
						if impo_categories[0]=='ch2_q':
							#print("we are IN IMPO NUM 2 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 2"
						if impo_categories[0]=='ch3_q':
							#print("we are IN IMPO NUM 3 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 3"
						if impo_categories[0]=='ch4_q':
							#print("we are IN IMPO NUM 4 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 4"
					if impo_categories[0]=='mi':
						#print("we are IN IMPO mixed match")
						ok_stop_here = 1
						par = (self.id_child)
						sess_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Games JOIN (Games USING(game_id)) \
							USING (session_id)) \
						USING (session_id) \
						WHERE kid_id = ? \
						AND kind = 'MIX' \
						AND (status ='default' or status ='to_do')"
						se_id = self.db_conn.execute_param_query(sess_query, par)						
						if len(se_id)==1:
							result_sess_first = se_id[0]
						if len(se_id)>1:
							result_sess_first = [item for sublist in se_id for item in sublist]
						else:
							result_sess_first = []
						if ok_stop_here==1:
							par = (self.id_child,self.id_scenario)
							sess_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								Kids_Sessions JOIN \
									(Sessions_Matches JOIN Matches USING(match_id)) \
									USING (session_id)) \
								USING (session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do')"
							se_id = self.db_conn.execute_param_query(sess_query, par)
							
							if len(se_id)>0:
								res_all_sessions = [item for sublist in se_id for item in sublist]
							else:
								res_all_sessions = []
							print("first impression {}".format(res_all_sessions))
							
							for p in range(len(res_all_sessions)):
								param_diff = (res_all_sessions[p])
								m_query = "SELECT match_id FROM Sessions JOIN Sessions_Matches USING (session_id) \
								WHERE session_id = ?"
								res_matches = self.db_conn.execute_param_query(m_query, param_diff)
								if len(res_matches)>1: 
									current_matches = [item for sublist in res_matches for item in sublist]
								if len(res_matches)==1: 
									current_matches = copy.copy(res_matches)
								if len(res_matches)==0: 
									current_matches = []
								print("current_matches is : ",current_matches)
								print("evaluate session result sess {}".format(current_matches))
								second_q_1 = "SELECT num_of_matches FROM Sessions WHERE session_id = ?"
								second_q_2 = "SELECT num_of_games FROM Sessions JOIN \
												(Sessions_Matches JOIN Matches USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								second_q_3 = "SELECT num_questions FROM Sessions JOIN \
												(Sessions_Matches JOIN \
													(Matches_Games JOIN Games USING (game_id)) \
													USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								res_mat = self.db_conn.execute_param_query(second_q_1, res_all_sessions[p])
								res_gam = self.db_conn.execute_param_query(second_q_2, res_all_sessions[p])
								res_que = self.db_conn.execute_param_query(second_q_3, res_all_sessions[p])
								if len(res_gam)>1:
									res_num_gam = [item for sublist in res_gam for item in sublist]
								if len(res_gam)==1:
									res_num_gam = copy.copy(res_gam)
								if len(res_gam)==0:
									res_num_gam = []
								if len(res_que)>1:
									res_num_que = [item for sublist in res_que for item in sublist]
								if len(res_que)==1:
									res_num_que = copy.copy(res_que)
								if len(res_que)==0:
									res_num_que = []
								is_mixed_1=0
								is_mixed_2=0
								is_mixed_3=0
								is_mixed_4=0
								print("res_num_mat res_num_mat {}".format(res_mat))
								print("res_num_gam res_num_gam {}".format(res_num_gam))
								print("res_num_que res_num_que {}".format(res_num_que))
								if len(res_num_gam)==1:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games made by 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[1]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
								elif len(res_num_gam)==2:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										print("numb ma 2")
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
								elif len(res_num_gam)==3:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
								elif len(res_num_gam)==4:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
									i = res_num_gam[2]
									if res_num_gam[3]==1:
										if res_num_que[i]==1:
											print("fourth match has a game all made by 1 quest")
											is_mixed_4 = 0
									if res_num_gam[3]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("fourth match has 2 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("fourth match has 3 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("fourth match has 4 games enquiry")
											is_mixed_4 = 1
								#print("MIXXXXX")
								#print(is_mixed_1)
								#print(is_mixed_2)
								#print(is_mixed_3)
								#print(is_mixed_4)
								if is_mixed_1=0 or is_mixed_2=0 or is_mixed_3=0 or is_mixed_4=0:
									q0 = "DROP TABLE IF EXISTS mini_tab"
									bah = self.db_conn.execute_a_query(q0)									
									q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
										AS SELECT session_id,match_id FROM Sessions_Matches WHERE session_id = ?;"
									res_q1 = self.db_conn.execute_param_query(q1, res_all_sessions[p])
									final_q1 = [item for sublist in res_q1 for item in sublist]
									print("The length whencurrent MAT IS {}".format(len(current_matches)))
									for j in range(len(current_matches)):
										print("numb loop tra i match {}".format(j))
										print("session: {} match: {}".format(res_all_sessions,current_matches[j]))
										q2 = "SELECT oid FROM mini_tab WHERE match_id = ?"
										res_q2 = self.db_conn.execute_param_query(q2,current_matches[j])
										final_bo = res_q2
										print("res q2res match = {}".format(res_q2))
										#final_bo = res_q2[0]
										print("final_bo {}".format(final_bo))
										#q3 = "DROP TABLE IF EXISTS mini_tab"
										result_sess = 0
										if len(final_bo)>0:
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or \
												(final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess = 0
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or (final_bo[0]==3 and is_mixed_3) or \
												(final_bo[0]==4 and is_mixed_4):
												result_sess = res_all_sessions[p]
											print("That is {}".format(result_sess))
											if result_sess=0:
												all_session_id_results.append(res_all_sessions[p])
												print("all_session_id_results {}".format(all_session_id_results))
									
							print("all_session_id_results {}".format(all_session_id_results))
							if len(all_session_id_results)==1:
								result_sess_first = copy.copy(all_session_id_results)
							if len(all_session_id_results)>1:
								result_sess_first = [item for sublist in all_session_id_results for item in sublist]
							if len(all_session_id_results)==0:
								result_sess_first = []
							try:
								result_sess_first = list(itertools.chain.from_iterable(result_sess_first))
								print("result_sess_first total after itertools is {}".format(result_sess_first))
							except Exception:
								result_sess_first = list(self.iterFlatten(result_sess_first))
								print("result_sess_first total after flatten is {}".format(result_sess_first))
							result_sess_first = list(set(result_sess_first))
							print("result_sess_first total after il SET is {}".format(result_sess_first))
					#ok now evaluate the resp		
					if impo_categories[0] not in ('qu','mi'):
						result_ques = self.db_conn.execute_param_query(next_query, params_case_first)
						if len(result_ques)==1:
							result_sess_first = copy.copy(result_ques)
						if len(result_ques)>1:
							result_sess_first = [item for sublist in result_ques for item in sublist]
						if len(result_ques)==0:
							result_sess_first = []
						print(" result_ques {} ".format(result_ques))
						print(" result_sess_first {} ".format(result_sess_first))

					if len(result_sess_first)>0:
						one_sess = random.choices(result_sess_first, k=1)
						find_one_sess = one_sess[0]
						print("find_one_sess {}".format(find_one_sess))
						last_query = "SELECT * FROM Sessions WHERE session_id = ?"
						result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
						print("At the end as type {}".format(result_sess))
					else:
						result_sess = []
						print("At the end error {}".format(result_sess))

				elif len(self.impositions_list)==0 and len(self.needs_list)==1:
					print("ONLY NEED, NO IMPO")
					se_tmp,result_sess_first = [],[]
					params_case01 = (self.id_child, self.id_scenario)
					next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
						(Kids_Sessions JOIN \
							(Sessions_Matches JOIN Matches USING(match_id)) \
							USING(session_id) \
					WHERE kid_id = ? \
					AND Matches.scenario = ? \
					AND (status='default' OR status = 'to_do')"				
					se_tmp = self.db_conn.execute_param_query(next_query, params_case01)
					print("se_tmpse_tmpse_tmp se_tmp {}".format(se_tmp))

					if len(se_tmp)>1: 
						result_sess_first = [item for sublist in se_tmp for item in sublist]
						print("first case results_need {}".format(result_sess_first))
					if len(se_tmp)==1:
						result_sess_first.append(se_tmp[0])
						print("second case results_need {}".format(result_sess_first)) 
					if len(result_sess_first)>0:
						print("NEED CATEGORY question")
						if need_categories[0]=='ques':
							#need to solutionts separated impo e need
							params_case_second = (self.id_child, self.id_scenario, res_id_needs[0])
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN \
												(Games JOIN \
													(Games_Questions JOIN Needs_Questions USING (question_id)) \
													USING (game_id)) \
												USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND Matches.scenario = ? \
							AND (status ='default' or status ='to_do') \
							AND Needs_Questions.need_id = ?"
						
							result_ques = self.db_conn.execute_param_query(next_query, params_case_second)
							if len(result_ques)==1:
								result_sess_second = copy.copy(result_ques)
							if len(result_ques)>1:
								result_sess_second = [item for sublist in result_ques for item in sublist]
							if len(result_ques)==0:
								result_sess_second = []
							print("recap ques as")
							print("result_sess_first ", result_sess_first)
							print("result_sess_second", result_sess_second)
							result_sess_def = list(set(result_sess_first).intersection(result_sess_second))
							if len(result_sess_def)>0:
								one_sess = random.choices(result_sess_def, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess {}".format(find_one_sess))
								
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end as ques {}".format(result_sess))
							else:
								result_sess = []
								print("At the end error {}".format(result_sess))
							print("result_sess intersezione ", result_sess_def)
							print("result_sess definitive ", result_sess)
						elif need_categories[0]=='gen_m' or need_categories[0]=='mix_m':
							all_ses = []
							for p in range(len(result_sess_first)):
								param_diff = (result_sess_first[p])
								m_query = "SELECT match_id FROM Sessions JOIN Sessions_Matches USING (session_id) \
								WHERE session_id = ?"
								res_matches = self.db_conn.execute_param_query(m_query, param_diff)
								if len(res_matches)>1: 
									current_matches = [item for sublist in res_matches for item in sublist]
								if len(res_matches)==1:
									current_matches = copy.copy(res_matches)
								if len(res_matches)==0:
									current_matches = []
								print("current_matches is:",current_matches)
								print("evaluate session result sess {}".format(current_matches))
								second_q_1 = "SELECT num_of_matches FROM Sessions WHERE session_id = ?"
								second_q_2 = "SELECT num_of_games FROM Sessions JOIN \
												(Sessions_Matches JOIN Matches USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								second_q_3 = "SELECT num_questions FROM Sessions JOIN \
												(Sessions_Matches JOIN \
													(Matches_Games JOIN Games USING (game_id)) \
													USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"

								res_mat = self.db_conn.execute_param_query(second_q_1, result_sess_first[p])
								res_gam = self.db_conn.execute_param_query(second_q_2, result_sess_first[p])
								res_que = self.db_conn.execute_param_query(second_q_3, result_sess_first[p])
								if len(res_gam)>1:
									res_num_gam = [item for sublist in res_gam for item in sublist]
								if len(res_gam)==1:
									res_num_gam = copy.copy(res_gam)
								if len(res_gam)==0:
									res_num_gam = []
								if len(res_que)>1:
									res_num_que = [item for sublist in res_que for item in sublist]
								if len(res_que)==1:
									res_num_que = copy.copy(res_que)
								if len(res_que)==0:
									res_num_que = []
								is_mixed_1=0
								is_mixed_2=0
								is_mixed_3=0
								is_mixed_4=0
								print("res_num_mat res_num_mat {}".format(res_mat))
								print("res_num_gam res_num_gam {}".format(res_num_gam))
								print("res_num_que res_num_que {}".format(res_num_que))
								if len(res_num_gam)==1:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games made by 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[1]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
								elif len(res_num_gam)==2:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										print("numb ma 2")
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
								elif len(res_num_gam)==3:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
								elif len(res_num_gam)==4:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
									i = res_num_gam[2]
									if res_num_gam[3]==1:
										if res_num_que[i]==1:
											print("fourth match has a game all made by 1 quest")
											is_mixed_4 = 0
									if res_num_gam[3]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("fourth match has 2 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("fourth match has 3 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("fourth match has 4 games enquiry")
											is_mixed_4 = 1
								#print("MIXXXXX")
								#print(is_mixed_1)
								#print(is_mixed_2)
								#print(is_mixed_3)
								#print(is_mixed_4)
								#check control match in wichi there is ask_quest_type if generic and non mixed
								q0 = "DROP TABLE IF EXISTS mini_tab"
								bah = self.db_conn.execute_a_query(q0)
								#q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
								q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
									AS SELECT session_id,match_id FROM Sessions_Matches WHERE session_id = ?;"
								
								res_q1 = self.db_conn.execute_param_query(q1, result_sess_first[p])
								final_q1 = [item for sublist in res_q1 for item in sublist]
								print("the length current MAT IS {}".format(len(current_matches)))
								for j in range(len(current_matches)):
									print("numb numb numb numb numb numb numb numb loop tra i match {}".format(j))
									print("session: {} match: {}".format(result_sess_first,current_matches[j]))
									q2 = "SELECT oid FROM mini_tab WHERE match_id = ?"
									res_q2 = self.db_conn.execute_param_query(q2,current_matches[j])
									final_bo = res_q2
									print("res q2res q2res q2res q2 riga match = {}".format(res_q2))
									#final_bo = res_q2[0]
									print("final_bo {}".format(final_bo))
									#q3 = "DROP TABLE IF EXISTS mini_tab"
									result_sess = 0
									print("need_categories[0] ")
									print(need_categories[0])
									if len(final_bo)>0:
										if need_categories[0]=='gen_m':
											print("sono entrato nel gen_m")
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or (final_bo[0]==3 and not is_mixed_3) or \ 
												(final_bo[0]==4 and not is_mixed_4):
												result_sess = result_sess_first[p]
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or (final_bo[0]==3 and is_mixed_3) or \
												(final_bo[0]==4 and is_mixed_4):
												result_sess = 0
										if need_categories[0]=='mix_m':
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or \
												(final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess = 0
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or \
												(final_bo[0]==3 and is_mixed_3) or (final_bo[0]==4 and is_mixed_4):
												result_sess = result_sess_first[p]
										print("That is {}".format(result_sess))
										if result_sess=0:
											all_ses.append(result_sess)
											print("all_session_id_results {}".format(all_ses))
							if len(all_ses)>0:
								try:
									results_need_total = list(itertools.chain.from_iterable(all_ses))
									print("results_need total after itertools is {}".format(all_ses))
								except Exception:
									results_need_total = list(self.iterFlatten(all_ses))
									print("results_need total after flatten is {}".format(all_ses))
								all_ses = list(set(all_ses))
								one_sess = random.choices(all_ses, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess ", find_one_sess)
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end {}".format(result_sess))
							else:
								result_sess = []
								print("At the end error {}".format(result_sess))
						#elif need_categories[0]=='gen_g':
						# third case not exists in focus level
						# --> pass to step 3
						elif need_categories[0]=='type_g':
							#need to solutionts type 
							print("How was need?? {}".format(self.needs_list))
							params_case_second = (self.id_child, self.id_scenario, self.needs_list[0][1]) #receive directly the type not the id
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND Matches.scenario = ? \
							AND (status ='default' or status ='to_do') \
							AND Games.type = ?"
						
							result_sec = self.db_conn.execute_param_query(next_query, params_case_second)
							print(".......result_sec {}".format(result_sec))
							if len(result_sec)==1:
								print("1")
								print("result query is un solo index di sessione")
								result_sess_second = copy.copy(result_sec)
							if len(result_sec)>1:
								print("2")
								print("result query sono many indexes devo fare list compr")
								result_sess_second = [item for sublist in result_sec for item in sublist]
							if len(result_sec)==0:
								print("0")
								print("result query type = zero")
								result_sess_second = []
							
							print("result_sess_second {}".format(result_sess_second))
							result_sess_def = list(set(result_sess_first).intersection(result_sess_second))
							if len(result_sess_def)>0:
								one_sess = random.choices(result_sess_def, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess {}".format(find_one_sess))
								
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end as type {}".format(result_sess))
							else:
								result_sess = []
								print("At the end as type error {}".format(result_sess))
																	
				elif len(self.impositions_list)==1 and len(res_id_needs)==1:
					if impo_categories[0]=='as':
						#print("we are IN IMPO ASK QUESTION")
						params_case_first = (self.id_child, self.id_scenario, self.impositions_list[0])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches JOIN \
										(Matches_Games JOIN \
											(Games JOIN \
												(Games_Questions JOIN Impositions_Questions USING (question_id)) \
												USING (game_id)) \
											USING (game_id)) \
										USING (match_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND Matches.scenario = ? \
						AND (status ='default' or status ='to_do')\
						AND Impositions_Questions.imposition_name = ?"
					if impo_categories[0]=='ty':
						#print("we are IN IMPO TYPE GAME")
						#same type impo query for all 3 cases
						params_case_first = (self.id_child, dict_impo_type_g[self.impositions_list[0]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Questions USING (question_id))\
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do') \
						AND Questions.type = ?" 
					if impo_categories[0]=='ki':
						#print("we are IN IMPO KIND GAME")
						#same type impo query for all 3 cases
						params_case_first = (self.id_child, dict_impo_kind_g[self.impositions_list[0]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN \
										(Games JOIN \
											(Games_Questions JOIN Questions USING (question_id))\
											USING (game_id)) \
										USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND (status ='default' or status ='to_do') \
						AND Questions.kind = ?" 
					if impo_categories[0]=='qu':
						to_do_ques, done_ques = [],[]
						#print("we are IN IMPO NEVER ASKED GAME")
						aa = "SELECT question_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Games_Questions USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND status ='to_do'"
						res_question_to_do= self.db_conn.execute_param_query(aa, self.id_child)
						print("res_question_to_do {}".format(res_question_to_do))
						if len(res_question_to_do)>1: #distinction between [0][0] fetchall result query, create list in this case 
							to_do_ques = [item for sublist in res_question_to_do for item in sublist]
						if len(res_question_to_do)==1:
							to_do_ques.append(res_question_to_do[0])
						print("OLD TEMP OLD TEMP OLD TEMP to do question {}".format(to_do_ques))

						bb = "SELECT question_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Games_Questions USING (game_id)) \
									USING (match_id)) \
								USING (session_id)) \
							USING(session_id) \
						WHERE kid_id = ? \
						AND status ='done'"
						res_question_done = self.db_conn.execute_param_query(bb, self.id_child)
						print("res_question_done {}".format(res_question_done))
						if len(res_question_done)>1: #distinction between [0][0] fetchall result query, create list in this case 
							done_ques = [item for sublist in res_question_done for item in sublist]
						if len(res_question_done)==1:
							done_ques.append(res_question_done[0])
						print("OLD TEMP OLD TEMP OLD TEMP done_ques {}".format(done_ques))

						ques_to_remove = list(set(to_do_ques).intersection(done_ques))
						print("quelli da rimuovere {}".format(ques_to_remove))
						
						if len(ques_to_remove)=0:
							result_ques_indexes = list(set(to_do_ques) - set(ques_to_remove))							
						else:
							result_ques_indexes = copy.copy(to_do_ques)
						print("result_ques_indexes Final COSA TROVO {}".format(result_ques_indexes))
						result_sess_first = []
						params_case_first = []
						for u in range(len(result_ques_indexes)):
							params_case_first.append(self.id_child)
							params_case_first.append(self.id_scenario)
							params_case_first.append(result_ques_indexes[u])
							session_for_this_ques_query = "SELECT Kids_Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games_Questions USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
								USING(session_id)) \
							USING(session_id) \
							WHERE kid_id = ? \
							AND status ='to_do' \
							AND Matches.scenario = ? \
							AND question_id = ?"
							one_sess = self.db_conn.execute_param_query(session_for_this_ques_query, params_case_first)
							print("una sessione trovate is {}".format(one_sess))
							result_sess_first.append(one_sess)
							params_case_first = []
						result_sess_first = list(flatten(result_sess_first))
						print("finish session {}".format(result_sess_first))
						result_sess_first = list(set(result_sess_first))
						print("dove cercare 222 nuovo {}".format(result_sess_first))
					
					if impo_categories[0] in ('cr_se','cr_sn','cr_sm','cr_sh'):
						#print("we are IN IMPO complexity session")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions) JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Matches USING (match_id)) \
									USING (match_id)) \
								USING(session_id)) \
							USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Sessions.complexity = ?"
						if impo_categories[0]=='cr_se':
							params_case_first = (self.id_child, 'Easy')
						elif impo_categories[0]=='cr_sn':
							params_case_first = (self.id_child, 'Normal')
						elif impo_categories[0]=='cr_sm':
							params_case_first = (self.id_child, 'Medium')
						elif impo_categories[0]=='cr_sh':
							params_case_first = (self.id_child, 'Hard')		
					if impo_categories[0] in ('cr_e1','cr_e2','cr_n1','cr_n2','cr_m1','cr_m2','cr_h1','cr_h2'):
						#need to solutionts separated impo e need
						#print("we are IN IMPO DIFFICULTIY game")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Matches JOIN \
										(Sessions_Matches JOIN \
											(Matches_Games JOIN Matches USING (match_id)) \
											USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND Matches.scenario = ? \
							AND (status ='default' or status ='to_do') \
							AND Matches.difficulty = ?"
						
						if impo_categories[0]=='cr_e1':
							params_case_first = (self.id_child, self.id_scenario, 'Easy_1')
						elif impo_categories[0]=='cr_e2':
							params_case_first = (self.id_child, self.id_scenario, 'Easy_2')
						elif impo_categories[0]=='cr_n1':
							params_case_first = (self.id_child, self.id_scenario, 'Normal_1')
						elif impo_categories[0]=='cr_n2':
							params_case_first = (self.id_child, self.id_scenario, 'Normal_2')
						elif impo_categories[0]=='cr_m1':
							params_case_first = (self.id_child, self.id_scenario, 'Medium_1')
						elif impo_categories[0]=='cr_m2':
							params_case_first = (self.id_child, self.id_scenario, 'Medium_2')
						elif impo_categories[0]=='cr_h1':
							params_case_first = (self.id_child, self.id_scenario, 'Hard_1')
						elif impo_categories[0]=='cr_h2':
							params_case_first = (self.id_child, self.id_scenario, 'Hard_2')
					if impo_categories[0] in ('cr_e1g','cr_e2g','cr_n1g','cr_n2g','cr_m1g','cr_m2g','cr_h1g','cr_h2g'):
						#need to solutionts separated impo e need
						#print("we are IN IMPO DIFFICULTIY game")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Matches JOIN \
										(Sessions_Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND Matches.scenario = ? \
							AND (status ='default' or status ='to_do') \
							AND Games.difficulty = ?"
						
						if impo_categories[0]=='cr_e1g':
							params_case_first = (self.id_child, self.id_scenario, 'Easy_1')
						elif impo_categories[0]=='cr_e2g':
							params_case_first = (self.id_child, self.id_scenario, 'Easy_2')
						elif impo_categories[0]=='cr_n1g':
							params_case_first = (self.id_child, self.id_scenario, 'Normal_1')
						elif impo_categories[0]=='cr_n2g':
							params_case_first = (self.id_child, self.id_scenario, 'Normal_2')
						elif impo_categories[0]=='cr_m1g':
							params_case_first = (self.id_child, self.id_scenario, 'Medium_1')
						elif impo_categories[0]=='cr_m2g':
							params_case_first = (self.id_child, self.id_scenario, 'Medium_2')
						elif impo_categories[0]=='cr_h1g':
							params_case_first = (self.id_child, self.id_scenario, 'Hard_1')
						elif impo_categories[0]=='cr_h2g':
							params_case_first = (self.id_child, 'Hard_2')
					if impo_categories[0] in ('ch1_m','ch2_m','ch3_m','ch4_m','ch1_g','ch2_g','ch3_g','ch4_g','ch1_q','ch2_q','ch3_q','ch4_q'):
						params_case_first = (self.id_child, self.id_scenario)
						if impo_categories[0]=='ch1_m':
							#print("we are IN IMPO NUM 1 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN Sessions_Matches JOIN \
									(Matches JOIN USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 1"
						if impo_categories[0]=='ch2_m':
							#print("we are IN IMPO NUM 2 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN Sessions_Matches JOIN \
									(Matches JOIN USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 2"
						if impo_categories[0]=='ch3_m':
							#print("we are IN IMPO NUM 3 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN Sessions_Matches JOIN \
									(Matches JOIN USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 3"
						if impo_categories[0]=='ch4_m':
							#print("we are IN IMPO NUM 4 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN Sessions_Matches JOIN \
									(Matches JOIN USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 4"
						if impo_categories[0]=='ch1_g':
							#print("we are IN IMPO NUM 1 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN Sessions_Matches JOIN \
									(Matches JOIN USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 1"
						if impo_categories[0]=='ch2_g':
							#print("we are IN IMPO NUM 2 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN Sessions_Matches JOIN \
									(Matches JOIN USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 2"
						if impo_categories[0]=='ch3_g':
							#print("we are IN IMPO NUM 3 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN Sessions_Matches JOIN \
									(Matches JOIN USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 3"
						if impo_categories[0]=='ch4_g':
							#print("we are IN IMPO NUM 4 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN Sessions_Matches JOIN \
									(Matches JOIN USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 4"
						if impo_categories[0]=='ch1_q':
							#print("we are IN IMPO NUM 1 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 1"
						if impo_categories[0]=='ch2_q':
							#print("we are IN IMPO NUM 2 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 2"
						if impo_categories[0]=='ch3_q':
							#print("we are IN IMPO NUM 3 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 3"
						if impo_categories[0]=='ch4_q':
							#print("we are IN IMPO NUM 4 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Kids_Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
										USING (session_id)) \
									USING(session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 4"
					if impo_categories[0]=='mi':
						#print("we are IN IMPO mixed match")
						ok_stop_here = 1
						par = (self.id_child)
						sess_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Games JOIN (Games USING(game_id)) \
							USING (session_id)) \
						USING (session_id) \
						WHERE kid_id = ? \
						AND kind = 'MIX' \
						AND (status ='default' or status ='to_do')"
						se_id = self.db_conn.execute_param_query(sess_query, par)						
						if len(se_id)==1:
							result_sess_first = se_id[0]
						if len(se_id)>1:
							result_sess_first = [item for sublist in se_id for item in sublist]
						else:
							result_sess_first = []
						if ok_stop_here==1:
							par = (self.id_child,self.id_scenario)
							sess_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								Kids_Sessions JOIN \
									(Sessions_Matches JOIN Matches USING(match_id)) \
									USING (session_id)) \
								USING (session_id) \
								WHERE kid_id = ? \
								AND Matches.scenario = ? \
								AND (status ='default' or status ='to_do')"
							se_id = self.db_conn.execute_param_query(sess_query, par)
							if len(se_id)>0:
								res_all_sessions = [item for sublist in se_id for item in sublist]
							else:
								res_all_sessions = []
							print("first impression {}".format(res_all_sessions))
							for p in range(len(res_all_sessions)):
								param_diff = (res_all_sessions[p])
								m_query = "SELECT match_id FROM Sessions JOIN Sessions_Matches USING (session_id) \
								WHERE session_id = ?"
								res_matches = self.db_conn.execute_param_query(m_query, param_diff)
								if len(res_matches)>1: 
									current_matches = [item for sublist in res_matches for item in sublist]
								if len(res_matches)==1: 
									current_matches = copy.copy(res_matches)
								if len(res_matches)==0: 
									current_matches = []
								print("current_matches is : ",current_matches)
								print("evaluate session result sess {}".format(current_matches))

								second_q_1 = "SELECT num_of_matches FROM Sessions WHERE session_id = ?"
								second_q_2 = "SELECT num_of_games FROM Sessions JOIN \
												(Sessions_Matches JOIN Matches USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								second_q_3 = "SELECT num_questions FROM Sessions JOIN \
												(Sessions_Matches JOIN \
													(Matches_Games JOIN Games USING (game_id)) \
													USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"

								res_mat = self.db_conn.execute_param_query(second_q_1, res_all_sessions[p])
								res_gam = self.db_conn.execute_param_query(second_q_2, res_all_sessions[p])
								res_que = self.db_conn.execute_param_query(second_q_3, res_all_sessions[p])

								if len(res_gam)>1:
									res_num_gam = [item for sublist in res_gam for item in sublist]
								if len(res_gam)==1:
									res_num_gam = copy.copy(res_gam)
								if len(res_gam)==0:
									res_num_gam = []
								if len(res_que)>1:
									res_num_que = [item for sublist in res_que for item in sublist]
								if len(res_que)==1:
									res_num_que = copy.copy(res_que)
								if len(res_que)==0:
									res_num_que = []
								is_mixed_1=0
								is_mixed_2=0
								is_mixed_3=0
								is_mixed_4=0
								print("res_num_mat res_num_mat {}".format(res_mat))
								print("res_num_gam res_num_gam {}".format(res_num_gam))
								print("res_num_que res_num_que {}".format(res_num_que))

								if len(res_num_gam)==1:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games made by 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[1]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
								elif len(res_num_gam)==2:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										print("numb ma 2")
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
								elif len(res_num_gam)==3:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
								elif len(res_num_gam)==4:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1

									i = res_num_gam[2]
									if res_num_gam[3]==1:
										if res_num_que[i]==1:
											print("fourth match has a game all made by 1 quest")
											is_mixed_4 = 0
									if res_num_gam[3]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("fourth match has 2 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("fourth match has 3 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("fourth match has 4 games enquiry")
											is_mixed_4 = 1
								#print("MIXXXXX")
								#print(is_mixed_1)
								#print(is_mixed_2)
								#print(is_mixed_3)
								#print(is_mixed_4)
								if is_mixed_1=0 or is_mixed_2=0 or is_mixed_3=0 or is_mixed_4=0:
									#check control match in wichi there is ask_quest_type if generic and non mixed
									#q0 = "DROP TABLE IF EXISTS mini_tab"
									q0 = "DROP TABLE IF EXISTS mini_tab"
									bah = self.db_conn.execute_a_query(q0)
									#q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
									q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
										AS SELECT session_id,match_id FROM Sessions_Matches WHERE session_id = ?;"
									
									res_q1 = self.db_conn.execute_param_query(q1, res_all_sessions[p])
									final_q1 = [item for sublist in res_q1 for item in sublist]
									print("the length current MAT IS {}".format(len(current_matches)))
									for j in range(len(current_matches)):
										print("numb numb numb numb numb numb numb numb loop tra i match {}".format(j))
										print("session: {} match: {}".format(res_all_sessions,current_matches[j]))
										q2 = "SELECT oid FROM mini_tab WHERE match_id = ?"
										res_q2 = self.db_conn.execute_param_query(q2,current_matches[j])
										final_bo = res_q2
										print("res q2res q2res q2res q2 riga match = {}".format(res_q2))
										#final_bo = res_q2[0]
										print("final_bo {}".format(final_bo))
										#q3 = "DROP TABLE IF EXISTS mini_tab"
										result_sess = 0
										if len(final_bo)>0:
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or \
											(final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess = 0
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or (final_bo[0]==3 and is_mixed_3) or \
											(final_bo[0]==4 and is_mixed_4):
												result_sess = res_all_sessions[p]
											print("That is {}".format(result_sess))
											if result_sess=0:
												all_session_id_results.append(res_all_sessions[p])
												print("all_session_id_results {}".format(all_session_id_results))
							print("all_session_id_results {}".format(all_session_id_results))
							if len(all_session_id_results)==1:
								result_sess_first = copy.copy(all_session_id_results)
							if len(all_session_id_results)>1:
								result_sess_first = [item for sublist in all_session_id_results for item in sublist]
							if len(all_session_id_results)==0:
								result_sess_first = []
							try:
								result_sess_first = list(itertools.chain.from_iterable(result_sess_first))
								print("result_sess_first total after itertools is {}".format(result_sess_first))
							except Exception:
								result_sess_first = list(self.iterFlatten(result_sess_first))
								print("result_sess_first total after flatten is {}".format(result_sess_first))
							result_sess_first = list(set(result_sess_first))
							print("result_sess_first total after il SET is {}".format(result_sess_first))
					if impo_categories[0] not in ('qu','mi'):
						result_ques = self.db_conn.execute_param_query(next_query, params_case_first)
						if len(result_ques)==1:
							result_sess_first = copy.copy(result_ques)
						if len(result_ques)>1:
							result_sess_first = [item for sublist in result_ques for item in sublist]
						if len(result_ques)==0:
							result_sess_first = []
					print("What is the result_sess_first {}".format(result_sess_first))
					if len(result_sess_first)>0:
						print("NEED CATEGORY question")
						if need_categories[0]=='ques':
							#need to solutionts separated impo e need
							params_case_second = (self.id_child, self.id_scenario, res_id_needs[0])
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
										(Matches_Games JOIN \
											(Games JOIN \
												(Games_Questions JOIN Needs_Questions USING (question_id)) \
												USING (game_id)) \
											USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND Matches.scenario = ? \
							AND (status ='default' or status ='to_do') \
							AND Needs_Questions.need_id = ?"
						
							result_ques = self.db_conn.execute_param_query(next_query, params_case_second)
							if len(result_ques)==1:
								result_sess_second = copy.copy(result_ques)
							if len(result_ques)>1:
								result_sess_second = [item for sublist in result_ques for item in sublist]
							if len(result_ques)==0:
								result_sess_second = []
							print("recap ques as")
							print("result_sess_first ", result_sess_first)
							print("result_sess_second ", result_sess_second)
							result_sess_def = list(set(result_sess_first).intersection(result_sess_second))
							if len(result_sess_def)>0:
								one_sess = random.choices(result_sess_def, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess {}".format(find_one_sess))
								
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end as ques {}".format(result_sess))
							else:
								result_sess = []
								print("At the end as ques error {}".format(result_sess))
							print("result_sess intersezione " , result_sess_def)
							print("result_sess definitive ", result_sess)
							print(result_sess)
						elif need_categories[0]=='gen_m' or need_categories[0]=='mix_m':
							#print("we are IN NEED SCOPE MATCH")
							for p in range(len(result_sess_first)):
								all_ses = []
								param_diff = (result_sess_first[p])
								m_query = "SELECT match_id FROM Sessions JOIN Sessions_Matches USING (session_id) \
								WHERE session_id = ?"
								res_matches = self.db_conn.execute_param_query(m_query, param_diff)
								if len(res_matches)>1: 
									current_matches = [item for sublist in res_matches for item in sublist]
								if len(res_matches)==1:
									current_matches = copy.copy(res_matches)
								if len(res_matches)==0:
									current_matches = []
								print("current_matches is : ",current_matches)
								print("evaluate session result sess {}".format(current_matches))
								second_q_1 = "SELECT num_of_matches FROM Sessions WHERE session_id = ?"
								second_q_2 = "SELECT num_of_games FROM Sessions JOIN \
												(Sessions_Matches JOIN Matches USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								second_q_3 = "SELECT num_questions FROM Sessions JOIN \
												(Sessions_Matches JOIN \
													(Matches_Games JOIN Games USING (game_id)) \
													USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								res_mat = self.db_conn.execute_param_query(second_q_1, result_sess_first[p])
								res_gam = self.db_conn.execute_param_query(second_q_2, result_sess_first[p])
								res_que = self.db_conn.execute_param_query(second_q_3, result_sess_first[p])

								if len(res_gam)>1:
									res_num_gam = [item for sublist in res_gam for item in sublist]
								if len(res_gam)==1:
									res_num_gam = copy.copy(res_gam)
								if len(res_gam)==0:
									res_num_gam = []
								if len(res_que)>1:
									res_num_que = [item for sublist in res_que for item in sublist]
								if len(res_que)==1:
									res_num_que = copy.copy(res_que)
								if len(res_que)==0:
									res_num_que = []
								is_mixed_1=0
								is_mixed_2=0
								is_mixed_3=0
								is_mixed_4=0
								print("res_num_mat res_num_mat {}".format(res_mat))
								print("res_num_gam res_num_gam {}".format(res_num_gam))
								print("res_num_que res_num_que {}".format(res_num_que))
								if len(res_num_gam)==1:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games made by 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[1]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
								elif len(res_num_gam)==2:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										print("numb ma 2")
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
								elif len(res_num_gam)==3:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
								elif len(res_num_gam)==4:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
									
									i = res_num_gam[2]
									if res_num_gam[3]==1:
										if res_num_que[i]==1:
											print("fourth match has a game all made by 1 quest")
											is_mixed_4 = 0
									if res_num_gam[3]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("fourth match has 2 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("fourth match has 3 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("fourth match has 4 games enquiry")
											is_mixed_4 = 1
								#print("MIXXXXX")
								#print(is_mixed_1)
								#print(is_mixed_2)
								#print(is_mixed_3)
								#print(is_mixed_4)
								#check control match in wichi there is ask_quest_type if generic and non mixed
								#q0 = "DROP TABLE IF EXISTS mini_tab"
								q0 = "DROP TABLE IF EXISTS mini_tab"
								bah = self.db_conn.execute_a_query(q0)
								#q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
								q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
									AS SELECT session_id,match_id FROM Sessions_Matches WHERE session_id = ?;"
								res_q1 = self.db_conn.execute_param_query(q1, result_sess_first[p])
								final_q1 = [item for sublist in res_q1 for item in sublist]
								print("the length current MAT IS {}".format(len(current_matches)))
								for j in range(len(current_matches)):
									print("numb numb numb numb numb numb numb numb loop tra i match {}".format(j))
									print("session: {} match: {}".format(result_sess_first,current_matches[j]))
									q2 = "SELECT oid FROM mini_tab WHERE match_id = ?"
									res_q2 = self.db_conn.execute_param_query(q2,current_matches[j])
									final_bo = res_q2
									print("res q2res q2res q2res q2 riga match = {}".format(res_q2))
									#final_bo = res_q2[0]
									print("final_bo {}".format(final_bo))
									#q3 = "DROP TABLE IF EXISTS mini_tab"
									result_sess = 0
									print("need_categories[0] ")
									print(need_categories[0])
									if len(final_bo)>0:
										if need_categories[0]=='gen_m':
											print("sono entrato nel gen_m")
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or \
												(final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess = result_sess_first[p]
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or (final_bo[0]==3 and is_mixed_3) or \
												(final_bo[0]==4 and is_mixed_4):
												result_sess = 0
										if need_categories[0]=='mix_m':
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or \
												(final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess = 0
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or (final_bo[0]==3 and is_mixed_3) or \
												(final_bo[0]==4 and is_mixed_4):
												result_sess = result_sess_first[p]
										print("That is {}".format(result_sess))
										if result_sess=0:
											all_ses.append(result_sess)
											print("all_session_id_results {}".format(all_ses))
							if len(all_ses)>0:
								try:
									results_need_total = list(itertools.chain.from_iterable(all_ses))
									print("results_need total after itertools is {}".format(all_ses))
								except Exception:
									results_need_total = list(self.iterFlatten(all_ses))
									print("results_need total after flatten is {}".format(all_ses))
								all_ses = list(set(all_ses))
								one_sess = random.choices(all_ses, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess {}".format(find_one_sess))
								
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end {}".format(result_sess))
							else:
								result_sess = []
								print("At the end but it is wrong {}".format(result_sess))
						#elif need_categories[0]=='gen_g':
						# third case not exists in focus level
						# --> pass to step 3
						elif need_categories[0]=='type_g':
							#need to solutionts type 
							print("How was need?? {}".format(self.needs_list))
							params_case_second = (self.id_child, self.id_scenario, self.needs_list[0][1]) #receive directly the type not the id
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE kid_id = ? \
							AND (status ='default' or status ='to_do') \
							AND Games.type = ?"
						
							result_sec = self.db_conn.execute_param_query(next_query, params_case_second)
							print(".......result_sec {}".format(result_sec))
							if len(result_sec)==1:
								print("1")
								print("result query is un solo index di sessione")
								result_sess_second = copy.copy(result_sec)
							if len(result_sec)>1:
								print("2")
								print("result query sono many indexes devo fare list compr")
								result_sess_second = [item for sublist in result_sec for item in sublist]
							if len(result_sec)==0:
								print("0")
								print("result query type = zero")
								result_sess_second = []
							print("result_sess_second {}".format(result_sess_second))
							result_sess_def = list(set(result_sess_first).intersection(result_sess_second))
							if len(result_sess_def)>0:
								one_sess = random.choices(result_sess_def, k=1)
								find_one_sess = one_sess[0]
								print("find_one_sess {}".format(find_one_sess))
								last_query = "SELECT * FROM Sessions WHERE session_id = ?"
								result_sess = self.db_conn.execute_param_query(last_query, find_one_sess)
								print("At the end as type {}".format(result_sess))
							else:
								result_sess = []
								print("At the end as type but it is wrong {}".format(result_sess))
		else:
			result_sess = []
		if len(result_sess)=0:
			ready = result_sess[0]
			print("READY ", ready)
		else:
			ready = []
		return ready
		
	cpdef realtime_creation(self):
		ready = self.extract_ready_session()
		if ready:
			print("RESULT SESS BEFORE CREATING OBJ {}".format(ready))
			self.create_obj_session(ready, self.id_child)
		else:
			#print("STEP 2.1 before 3 look after kid ")
			child_query = "SELECTcurrent_level, age FROM Kids WHERE kid_id = ?"
			kid_info = self.db_conn.execute_param_query(child_query, self.id_child)
			level_is = kid_info[0]
			age_is = kid_info[1]
			#print(":::::::::::::::")
			#print(level_is)
			#print(age_is)
			#print(":::::::::::::::")
			same_kid_query = "SELECT kid_id FROM Kids WHEREcurrent_level = ? AND kid_id = ? AND age>= ?"
			par_same_kid = (level_is, self.id_child, age_is)
			child_similar_exists = self.db_conn.execute_param_query(same_kid_query,par_same_kid)
			print("child_similar_exists ", child_similar_exists)
			if child_similar_exists:
				print("enter in SIMILAR child")
				right_kid = self.id_child
				self.id_child = child_similar_exists
				ready_1 = self.extract_ready_session()
				same_kid_query = "SELECT kid_id FROM Kids WHEREcurrent_level = ? AND kid_id = ? AND age>= ?"
				par_same_kid = (level_is, self.id_child, age_is)
				child_similar_exists = self.db_conn.execute_param_query(same_kid_query,par_same_kid)
				print(child_similar_exists)
				if child_similar_exists:
					self.id_child = right_kid
					self.db_conn.add_new_KidSession(self.id_child,ready_1)
					#this time also kid nella query
					param_ck = (self.id_child)
					next_query = "SELECT Sessions.session_id FROM Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
					#next_query = "SELECT * FROM Sessions JOIN Kids_Sessions USING(session_id) WHERE kid_id = ? AND status='to_do' ORDER BY random() LIMIT 1"
					sess_id = self.db_conn.execute_param_query(next_query,param_ck)
					sess_id = sess_id[0]
					print("RESULT SESS BEFORE CREATING OBJ last tentative => assign existing session to kid{}".format(sess_id))
			else:
				scen = []
				scen_str = 'scenario_{}'.format(self.id_scenario)
				impos_needs = []
				impos_needs.append(self.impositions_list)
				impos_needs.append(self.needs_list)
				impos_needs.append(self.level_area)
				scen.append(self.id_scenario)
				impos_needs.append(scen_str)
				print("impos_needs to pass {}".format(impos_needs))
				manda_impo = 1 #always 1 kid is specified ...
				manda_need = 1
				self.db_conn.add_new_Session(self.id_child,manda_impo,manda_need,impos_needs)
				next_query = "SELECT Sessions.session_id FROM Sessions ORDER BY session_id DESC LIMIT 1"
				#next_query = "SELECT * FROM Sessions JOIN Kids_Sessions USING(session_id) WHERE kid_id = ? AND status='to_do' ORDER BY random() LIMIT 1"
				sess_id = self.db_conn.execute_a_query(next_query)
				sess_id = sess_id[0]
				print("RESULT SESS BEFORE CREATING OBJ {}".format(sess_id))
				for imp in self.impositions_list:
					self.db_conn.add_new_SessionImposition(sess_id,imp)
				for nee in self.needs_list:
					self.db_conn.add_new_SessionNeed(sess_id,nee)					
				self.create_obj_session(sess_id,self.id_child)

cdef class Session_4(SessionBase):
	def __cinit__(self, list impositions_list, str level_area, str id_scenario):
		self.impositions_list = impositions_list
		self.level_area = level_area
		self.id_scenario = id_scenario
		self.db_conn = db.DatabaseManager()

	cpdef extract_ready_session(self):
		""" Prepare session find specific impo => impositions_list """
		cdef:
			bint way_scen = 0
			bint ask_pos_scen = 0
			bint ins_scen_diff = 0
		#print(":::::::::::::::")
		print(self.impositions_list)
		print(self.level_area)
		#print(":::::::::::::::")
		print("length imposition {}".format(len(self.impositions_list)))
		impo_categories = []
		dict_impo_kind_g = self.get_dict_kind_imposition()
		dict_impo_type_g = self.get_dict_imposition()
		impo_categories = self.get_categ_imposition()
		print("DICT_IMPO obtained")
		print(dict_impo_type_g)
		print(dict_impo_kind_g)
		print("CATEGORI_IMPO obtained")
		print(impo_categories)		
		first_list_of_sess = []
		second_list_of_sess = []
		result_list_of_sess = []
		impo_list_of_sess = []
		all_session_id_results = []
		all_sessions_4 = []
		redo = acp2.create_new_anyway(self.impositions_list)
		if redo==0:
			next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
				(Sessions_Impositions JOIN \
					(Sessions_Matches JOIN Matches USING(match_id)) \
					USING (session_id)) \
				USING(session_id) \
			WHERE (status='default' OR status='to_do') \
			AND imposition_name = ? \
			AND Matches.scenario = ? \
			ORDER BY random()"
			check_is_not_over = 1
			while check_is_not_over > 0:
				if len(self.impositions_list)==1:
					params_case1 = (self.impositions_list[0],self.id_scenario)
					result_sess = self.db_conn.execute_param_query(next_query, params_case1)
					result_sess = list(set(result_sess))
					print("  result_SESS",result_sess)
					check_is_not_over = 0
				elif len(self.impositions_list)==2:
					params_case1 = (self.impositions_list[0], self.id_scenario)
					params_case2 = (self.impositions_list[1], self.id_scenario)
					result_1 = self.db_conn.execute_param_query(next_query, params_case1)
					result_1 = list(set(result_1))
					result_2 = self.db_conn.execute_param_query(next_query, params_case2)
					result_2 = list(set(result_2))
					result_sess = list(set(result_1) & set(result_2))
					check_is_not_over = 0
				elif len(self.impositions_list)==3:
					params_case1 = (self.impositions_list[0], self.id_scenario)
					params_case2 = (self.impositions_list[1], self.id_scenario)
					params_case3 = (self.impositions_list[2], self.id_scenario)
					result_1 = self.db_conn.execute_param_query(next_query, params_case1)
					result_1 = list(set(result_1))
					print("  result_1",result_1)
					result_2 = self.db_conn.execute_param_query(next_query, params_case2)
					result_2 = list(set(result_2))
					print("  result_2",result_2)
					result_3 = self.db_conn.execute_param_query(next_query, params_case3)
					print("  result_3",result_3)
					result_3 = list(set(result_3))
					result_sess = list(set(result_1)&set(result_2)&set(result_3))
					check_is_not_over = 0
				elif len(self.impositions_list)==4:
					params_case1 = (self.impositions_list[0], self.id_scenario)
					params_case2 = (self.impositions_list[1], self.id_scenario)
					params_case3 = (self.impositions_list[2], self.id_scenario)
					params_case4 = (self.impositions_list[3], self.id_scenario)
					result_1 = self.db_conn.execute_param_query(next_query, params_case1)
					result_1 = list(set(result_1))
					result_2 = self.db_conn.execute_param_query(next_query, params_case2)
					result_2 = list(set(result_2))
					result_3 = self.db_conn.execute_param_query(next_query, params_case3)
					result_3 = list(set(result_3))
					result_4 = self.db_conn.execute_param_query(next_query, params_case4)
					result_4 = list(set(result_4))
					result_sess = list(set(result_1)&set(result_2)&set(result_3)&set(result_4))
					check_is_not_over = 0
				elif len(self.impositions_list)==5:
					params_case1 = (self.impositions_list[0], self.id_scenario)
					params_case2 = (self.impositions_list[1], self.id_scenario)
					params_case3 = (self.impositions_list[2], self.id_scenario)
					params_case4 = (self.impositions_list[3], self.id_scenario)
					params_case5 = (self.impositions_list[4], self.id_scenario)
					result_1 = self.db_conn.execute_param_query(next_query, params_case1)
					result_1 = list(set(result_1))
					result_2 = self.db_conn.execute_param_query(next_query, params_case2)
					result_2 = list(set(result_2))
					result_3 = self.db_conn.execute_param_query(next_query, params_case3)
					result_3 = list(set(result_3))
					result_4 = self.db_conn.execute_param_query(next_query, params_case4)
					result_4 = list(set(result_4))
					result_5 = self.db_conn.execute_param_query(next_query, params_case5)
					result_5 = list(set(result_5))			
					result_sess = list(set(result_1)&set(result_2)&set(result_3)&set(result_4)&set(result_5))
					check_is_not_over = 0				
				elif len(self.impositions_list)==6:
					params_case1 = (self.impositions_list[0], self.id_scenario)
					params_case2 = (self.impositions_list[1], self.id_scenario)
					params_case3 = (self.impositions_list[2], self.id_scenario)
					params_case4 = (self.impositions_list[3], self.id_scenario)
					params_case5 = (self.impositions_list[4], self.id_scenario)
					params_case6 = (self.impositions_list[6], self.id_scenario)
					result_1 = self.db_conn.execute_param_query(next_query, params_case1)
					result_1 = list(set(result_1))
					result_2 = self.db_conn.execute_param_query(next_query, params_case2)
					result_2 = list(set(result_2))
					result_3 = self.db_conn.execute_param_query(next_query, params_case3)
					result_3 = list(set(result_3))
					result_4 = self.db_conn.execute_param_query(next_query, params_case4)
					result_4 = list(set(result_4))
					result_5 = self.db_conn.execute_param_query(next_query, params_case5)
					result_5 = list(set(result_5))			
					result_6 = self.db_conn.execute_param_query(next_query, params_case6)
					result_6 = list(set(result_6))			
					result_sess = list(set(result_1)&set(result_2)&set(result_3)&set(result_4)&set(result_5)&set(result_6))
					check_is_not_over = 0
				elif len(self.impositions_list)>6: 
					remove_me = random.randint(0,6)
					del(self.impositions_list[remove_me])			

			if len(result_sess)=0: #---> step2
				print(" result_sess  {}".format(result_sess))
				print(" result_sess  {}".format(result_sess))
				print(" result_sess  {}".format(result_sess))
				print("ok found without doing STEP 2")
			else:
				#step 2 
				print("++STEP 2")
				#check inside...I need all possible combinations inside session
				print("IMPO CATCATCATCATCATCAT {}".format(impo_categories))
				for g in range(len(self.impositions_list)):
					print("ONLY IMPO NO NEED")
					if impo_categories[g]=='as':
						#print("we are IN IMPO ASK QUESTION")
						params_case_first = (self.id_scenario, self.impositions_list[0])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Sessions_Matches JOIN \
								(Matches_Games JOIN \
									(Games JOIN \
										(Games_Questions JOIN Impositions_Questions USING (question_id)) \
										USING (game_id)) \
									USING (game_id)) \
								USING (match_id)) \
						USING(session_id) \
						WHERE (status ='default' or status ='to_do') \
						AND Matches.scenario = ? \
						AND Impositions_Questions.imposition_name = ?"
					if impo_categories[g]=='ty':
						#print("we are IN IMPO TYPE GAME")
						#same type impo query for all 3 cases
						params_case_first = (self.id_scenario, dict_impo_type_g[self.impositions_list[0]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Sessions_Matches JOIN \
								(Matches_Games JOIN \
									(Games JOIN \
										(Games_Questions JOIN Questions USING (question_id))\
										USING (game_id)) \
									USING (game_id)) \
								USING (match_id)) \
							USING(session_id) \
						WHERE (status ='default' or status ='to_do') \
						AND Questions.type = ?" 
					if impo_categories[g]=='ki':
						#print("we are IN IMPO KIND GAME")
						#same type impo query for all 3 cases
						params_case_first = (self.id_scenario, dict_impo_kind_g[self.impositions_list[0]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Sessions_Matches JOIN \
								(Matches_Games JOIN \
									(Games JOIN \
										(Games_Questions JOIN Questions USING (question_id))\
										USING (game_id)) \
									USING (game_id)) \
								USING (match_id)) \
							USING(session_id) \
						WHERE (status ='default' or status ='to_do') \
						AND Questions.kind = ?" 
					if impo_categories[g]=='qu':
						print("non c'is kid quindi chissene delle domande non fatte")
					if impo_categories[g] in ('cr_se','cr_sn','cr_sm','cr_sh'):
						#print("we are IN IMPO complexity session")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Matches USING (match_id)) \
									USING (match_id)) \
								USING(session_id) \
							WHERE (status ='default' or status ='to_do') \
							AND Sessions.complexity = ?"
						if impo_categories[g]=='cr_se':
							params_case_first = ('Easy')
						elif impo_categories[g]=='cr_sn':
							params_case_first = ('Normal')
						elif impo_categories[g]=='cr_sm':
							params_case_first = ('Medium')
						elif impo_categories[g]=='cr_sh':
							params_case_first = ('Hard')						
					if impo_categories[g] in ('cr_e1','cr_e2','cr_n1','cr_n2','cr_m1','cr_m2','cr_h1','cr_h2'):
						#need to solutionts separated impo e need
						#print("we are IN IMPO DIFFICULTIY match")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE Matches.scenario = ? \
							AND (status ='default' or status ='to_do') \
							AND Matches.difficulty = ?"
						if impo_categories[g]=='cr_e1':
							params_case_first = (self.id_scenario, 'Easy_1')
						elif impo_categories[g]=='cr_e2':
							params_case_first = (self.id_scenario, 'Easy_2')
						elif impo_categories[g]=='cr_n1':
							params_case_first = (self.id_scenario, 'Normal_1')
						elif impo_categories[g]=='cr_n2':
							params_case_first = (self.id_scenario, 'Normal_2')
						elif impo_categories[g]=='cr_m1':
							params_case_first = (self.id_scenario, 'Medium_1')
						elif impo_categories[g]=='cr_m2':
							params_case_first = (self.id_scenario, 'Medium_2')
						elif impo_categories[g]=='cr_h1':
							params_case_first = (self.id_scenario, 'Hard_1')
						elif impo_categories[g]=='cr_h2':
							params_case_first = (self.id_scenario, 'Hard_2')
					if impo_categories[g] in ('cr_e1g','cr_e2g','cr_n1g','cr_n2g','cr_m1g','cr_m2g','cr_h1g','cr_h2g'):
						#need to solutionts separated impo e need
						#print("we are IN IMPO DIFFICULTIY match")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Kids_Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
									USING (session_id)) \
								USING(session_id) \
							WHERE Matches.scenario = ? \
							AND (status ='default' or status ='to_do') \
							AND Games.difficulty = ?"
						if impo_categories[g]=='cr_e1g':
							params_case_first = (self.id_scenario, 'Easy_1')
						elif impo_categories[g]=='cr_e2g':
							params_case_first = (self.id_scenario, 'Easy_2')
						elif impo_categories[g]=='cr_n1g':
							params_case_first = (self.id_scenario, 'Normal_1')
						elif impo_categories[g]=='cr_n2g':
							params_case_first = (self.id_scenario, 'Normal_2')
						elif impo_categories[g]=='cr_m1g':
							params_case_first = (self.id_scenario,	'Medium_1')
						elif impo_categories[g]=='cr_m2g':
							params_case_first = (self.id_scenario, 'Medium_2')
						elif impo_categories[g]=='cr_h1g':
							params_case_first = (self.id_scenario, 'Hard_1')
						elif impo_categories[g]=='cr_h2g':
							params_case_first = (self.id_scenario, 'Hard_2')
					if impo_categories[g] in ('ch1_m','ch2_m','ch3_m','ch4_m','ch1_q','ch2_q','ch3_q','ch4_q','ch1_g','ch2_g','ch3_g','ch4_g'):
						params_case_first = (self.id_scenario)
						if impo_categories[g]=='ch1_m':
							#print("we are IN IMPO NUM 1 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN Matches USING(match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Matches.scenario = ? \
								AND Sessions.num_of_matches = 1"
						if impo_categories[g]=='ch2_m':
							#print("we are IN IMPO NUM 2 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN Matches USING(match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Matches.scenario = ? \
								AND Sessions.num_of_matches = 2"
						if impo_categories[g]=='ch3_m':
							#print("we are IN IMPO NUM 3 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN Matches USING(match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Matches.scenario = ? \
								AND Sessions.num_of_matches = 3"
						if impo_categories[g]=='ch4_m':
							#print("we are IN IMPO NUM 4 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN Matches USING(match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Matches.scenario = ? \
								AND Sessions.num_of_matches = 4"
						if impo_categories[g]=='ch1_g':
							#print("we are IN IMPO NUM 3 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN Matches USING (match_id)) \
									USING(session_id) \
								WHERE Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 1"
						if impo_categories[g]=='ch2_g':
							#print("we are IN IMPO NUM 2 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN Matches USING (match_id)) \
									USING(session_id) \
								WHERE Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 2"
						if impo_categories[g]=='ch3_g':
							#print("we are IN IMPO NUM 3 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN Matches USING (match_id)) \
									USING(session_id) \
								WHERE Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 3"
						if impo_categories[g]=='ch4_g':
							#print("we are IN IMPO NUM 4 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN Matches USING (match_id)) \
									USING(session_id) \
								WHERE Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 4"
						if impo_categories[g]=='ch1_q':
							#print("we are IN IMPO NUM 1 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
								USING(session_id) \
								WHERE Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 1"
						if impo_categories[g]=='ch2_q':
							#print("we are IN IMPO NUM 2 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
								USING(session_id) \
								WHERE Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 2"
						if impo_categories[g]=='ch3_q':
							#print("we are IN IMPO NUM 3 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
								USING(session_id) \
								WHERE Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 3"
						if impo_categories[g]=='ch4_q':
							#print("we are IN IMPO NUM 4 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
									(Sessions_Matches JOIN \
										(Matches JOIN \
											(Matches_Games JOIN Games USING (game_id)) \
											USING (match_id)) \
										USING (match_id)) \
								USING(session_id) \
								WHERE Matches.scenario = ? \
								AND (status ='default' or status ='to_do') \
								AND Games.num_questions = 4"
					if impo_categories[g]=='mi':
						#print("we are IN IMPO mixed match")
						ok_stop_here = 1
						par = (self.id_child)
						sess_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Games JOIN (Games USING(game_id)) \
							USING (session_id)) \
						USING (session_id) \
						WHERE kid_id = ? \
						AND kind = 'MIX' \
						AND (status ='default' or status ='to_do')"
						se_id = self.db_conn.execute_param_query(sess_query, par)						
						if len(se_id)==1:
							result_sess = se_id[0]
						if len(se_id)>1:
							result_sess = [item for sublist in se_id for item in sublist]
						else:
							result_sess = []
						if ok_stop_here==1:
							par = (self.id_scenario)
							sess_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								Kids_Sessions JOIN \
									(Sessions_Matches JOIN Matches USING(match_id)) \
									USING (session_id)) \
								USING (session_id) \
								WHERE Matches.scenario = ? \
								AND (status ='default' or status ='to_do')"
							se_id = self.db_conn.execute_param_query(sess_query, par)
							
							if len(se_id)>0:
								res_all_sessions = [item for sublist in se_id for item in sublist]
							else:
								res_all_sessions = []
							print("first impression {}".format(res_all_sessions))
							for p in range(len(res_all_sessions)):
								param_diff = (res_all_sessions[p])
								m_query = "SELECT match_id FROM Sessions JOIN Sessions_Matches USING (session_id) \
								WHERE session_id = ?"
								res_matches = self.db_conn.execute_param_query(m_query, param_diff)
								if len(res_matches)>1: 
									current_matches = [item for sublist in res_matches for item in sublist]
								if len(res_matches)==1: 
									current_matches = copy.copy(res_matches)
								if len(res_matches)==0: 
									current_matches = []
								print("current_matches is : ",current_matches)
								print("evaluate session result sess {}".format(current_matches))
								second_q_1 = "SELECT num_of_matches FROM Sessions WHERE session_id = ?"
								second_q_2 = "SELECT num_of_games FROM Sessions JOIN \
												(Sessions_Matches JOIN Matches USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								second_q_3 = "SELECT num_questions FROM Sessions JOIN \
												(Sessions_Matches JOIN \
													(Matches_Games JOIN Games USING (game_id)) \
													USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"

								res_mat = self.db_conn.execute_param_query(second_q_1, res_all_sessions[p])
								res_gam = self.db_conn.execute_param_query(second_q_2, res_all_sessions[p])
								res_que = self.db_conn.execute_param_query(second_q_3, res_all_sessions[p])

								if len(res_gam)>1:
									res_num_gam = [item for sublist in res_gam for item in sublist]
								if len(res_gam)==1:
									res_num_gam = copy.copy(res_gam)
								if len(res_gam)==0:
									res_num_gam = []
								if len(res_que)>1:
									res_num_que = [item for sublist in res_que for item in sublist]
								if len(res_que)==1:
									res_num_que = copy.copy(res_que)
								if len(res_que)==0:
									res_num_que = []
								is_mixed_1=0
								is_mixed_2=0
								is_mixed_3=0
								is_mixed_4=0
								print("res_num_mat res_num_mat {}".format(res_mat))
								print("res_num_gam res_num_gam {}".format(res_num_gam))
								print("res_num_que res_num_que {}".format(res_num_que))

								if len(res_num_gam)==1:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games made by 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[1]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
								elif len(res_num_gam)==2:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										print("numb ma 2")
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
								elif len(res_num_gam)==3:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
								elif len(res_num_gam)==4:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1

									i = res_num_gam[2]
									if res_num_gam[3]==1:
										if res_num_que[i]==1:
											print("fourth match has a game all made by 1 quest")
											is_mixed_4 = 0
									if res_num_gam[3]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("fourth match has 2 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("fourth match has 3 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("fourth match has 4 games enquiry")
											is_mixed_4 = 1
								#print("MIXXXXX")
								#print(is_mixed_1)
								#print(is_mixed_2)
								#print(is_mixed_3)
								#print(is_mixed_4)
								if is_mixed_1=0 or is_mixed_2=0 or is_mixed_3=0 or is_mixed_4=0:
									#check control match in wichi there is ask_quest_type if generic and non mixed
									#q0 = "DROP TABLE IF EXISTS mini_tab"
									q0 = "DROP TABLE IF EXISTS mini_tab"
									bah = self.db_conn.execute_a_query(q0)
									q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
										AS SELECT session_id,match_id FROM Sessions_Matches WHERE session_id = ?;"
									
									res_q1 = self.db_conn.execute_param_query(q1, res_all_sessions[p])
									final_q1 = [item for sublist in res_q1 for item in sublist]
									print("the length current MAT IS {}".format(len(current_matches)))
									for j in range(len(current_matches)):
										print("numb numb numb numb numb numb numb numb loop tra i match {}".format(j))
										print("session: {} match: {}".format(res_all_sessions,current_matches[j]))
										q2 = "SELECT oid FROM mini_tab WHERE match_id = ?"
										res_q2 = self.db_conn.execute_param_query(q2,current_matches[j])
										final_bo = res_q2
										print("res q2res q2res q2res q2 riga match = {}".format(res_q2))
										#final_bo = res_q2[0]
										print("final_bo {}".format(final_bo))
										#q3 = "DROP TABLE IF EXISTS mini_tab"
										result_sess_id = 0
										if len(final_bo)>0:
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or \
											(final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess_id = 0
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or (final_bo[0]==3 and is_mixed_3) or \
												(final_bo[0]==4 and is_mixed_4):
												result_sess_id = res_all_sessions[p]
											print("That is {}".format(result_sess_id))
											if result_sess_id=0:
												all_session_id_results.append(res_all_sessions[p])
												print("all_session_id_results {}".format(all_session_id_results))
							print("all_session_id_results {}".format(all_session_id_results))
							if len(all_session_id_results)==1:
								result_sess = copy.copy(all_session_id_results)
							if len(all_session_id_results)>1:
								result_sess = [item for sublist in all_session_id_results for item in sublist]
							if len(all_session_id_results)==0:
								result_sess = []
							try:
								result_sess = list(itertools.chain.from_iterable(result_sess))
								print("result_sess total after itertools is {}".format(result_sess))
							except Exception:
								result_sess = list(self.iterFlatten(result_sess))
								print("result_sess total after flatten is {}".format(result_sess))
							result_sess = list(set(result_sess))
							print("result_sess total after il SET is {}".format(result_sess))
					#ok now evaluate resp
					if impo_categories[g] not in ('qu','mi'):
						result_ques = self.db_conn.execute_param_query(next_query, params_case_first)
						if len(result_ques)==1:
							result_sess = copy.copy(result_ques)
						if len(result_ques)>1:
							result_sess = [item for sublist in result_ques for item in sublist]
						if len(result_ques)==0:
							result_sess = []
						print(" result_ques {} ".format(result_ques))
						print(" result_sess {} ".format(result_sess))
					if len(result_sess)>0:
						if len(all_sessions_4)>0:
							if self.check_lol(all_sessions_4):
								all_sessions_44 = functools.reduce(operator.iconcat, all_sessions_4, [])
								inter_ses = list(set(result_sess) & set(all_sessions_44))
							else:
								inter_ses = list(set(result_sess) & set(all_sessions_4))
							print("INTER SESS INTER SESS INTER SESS {}".format(inter_ses))
							if len(inter_ses)>0:
								all_sessions_4 = copy.copy(inter_ses)
								print("come is diventata session_4 after copia {}".format(all_sessions_4))
							else:
								result_sess=[]
								break
						else:
							all_sessions_4.append(result_sess)
					else:
						result_sess = []
						break
				if len(result_sess)>0:
					print("all_sessions_4 {}".format(all_sessions_4))
					result_sess = all_sessions_4
		else:
			result_sess=[]
		if len(result_sess)>0:
			print("all_sessions_4 {}".format(result_sess))
			all_sessions_4_ok = list(self.iterFlatten(result_sess))
			print("all_sessions_4_ok ok  {}".format(all_sessions_4_ok))
			result_sess = list(set(all_sessions_4_ok))
			print("Final result session CON many GOAL {}".format(result_sess))
			result_sess = random.choices(result_sess, k=1)
			result_sess = result_sess[0]
			print("OLD find_one_sess ", result_sess)
		else:
			result_sess = 0
		return result_sess
	
	cpdef realtime_creation(self):
		ready = self.extract_ready_session()
		if ready:
			print("RESULT SESS BEFORE CREATING OBJ {}".format(ready))
			self.create_obj_session(ready,0)
		else:
			scen = []
			scen_str = 'scenario_{}'.format(self.id_scenario)
			impos_this_time = []
			impos_this_time.append(self.impositions_list)
			impos_this_time.append(scen_str)
			impos_this_time.append(self.level_area)
			print("impos_this_time to pass {}".format(impos_this_time))
			manda_impo = 1 #always 1 kid is specified ...
			manda_need = 1
			self.db_conn.add_new_Session(0,manda_impo,manda_need,impos_this_time)
			print("END REALTIME CREATION...TUTTO OK")
			next_query = "SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1"
			sess_id = self.db_conn.execute_a_query(next_query)
			sess_id = sess_id[0]
			print("RESULT SESS BEFORE CREATING OBJ {}".format(sess_id))
			for imp in self.impositions_list:
				self.db_conn.add_new_SessionImposition(sess_id,imp)
			self.create_obj_session(sess_id,0)

	def check_lol(self, lis): #avoid clousureclass cpdef cython limit
		if any(isinstance(el, list) for el in lis):
			return 1
		else:
			return 0

cdef class Session_5(SessionBase):
	"""hidden idea: just for testing, choose a scenario later like in randomforest, i mean that before scenario i need consider impositions like """
	def __cinit__(self, list impositions_list, str level_area):
		self.impositions_list = impositions_list
		self.level_area = level_area
		self.db_conn = db.DatabaseManager()

	cpdef extract_ready_session(self):
		cdef:
			bint way_scen = 0
			bint ask_pos_scen = 0
			bint ins_scen_diff = 0
		""" Prepare session find specific impo => impositions_list """
		#print(":::::::::::::::")
		print(self.impositions_list)
		print(self.level_area)
		#print(":::::::::::::::")
		print("length imposition {}".format(len(self.impositions_list)))
		impo_categories = []
		dict_impo_kind_g = self.get_dict_kind_imposition()
		dict_impo_type_g = self.get_dict_imposition()
		impo_categories = self.get_categ_imposition()
		print("DICT_IMPO obtained")
		print(dict_impo_type_g)
		print(dict_impo_kind_g)
		print("CATEGORI_IMPO obtained")
		print(impo_categories)		

		first_list_of_sess = []
		second_list_of_sess = []
		result_list_of_sess = []
		impo_list_of_sess = []
		all_session_id_results = []
		all_sessions_5 = []
		redo = acp2.create_new_anyway(self.impositions_list)
		if redo==0:
			next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
				(Sessions_Impositions JOIN \
					(Sessions_Matches JOIN Matches USING(match_id)) \
					USING (session_id)) \
				USING(session_id) \
			WHERE (status='default' OR status='to_do') \
			AND imposition_name = ? \
			ORDER BY random()"
			check_is_not_over = 1 
			while check_is_not_over > 0:
				if len(self.impositions_list)==1:
					params_case1 = (self.impositions_list[0])
					result_sess = self.db_conn.execute_param_query(next_query, params_case1)
					print("R0 {}".format(result_sess))
					result_sess = list(set(result_sess))
					check_is_not_over = 0
				elif len(self.impositions_list)==2:
					params_case1 = (self.impositions_list[0])
					params_case2 = (self.impositions_list[1])
					result_1 = self.db_conn.execute_param_query(next_query,params_case1)
					print("R1 {}".format(result_1))
					result_1 = list(set(result_1))
					result_2 = self.db_conn.execute_param_query(next_query,params_case2)
					print("R2 {}".format(result_2))
					result_2 = list(set(result_2))
					result_sess = list(set(result_1) & set(result_2))
					check_is_not_over = 0
				elif len(self.impositions_list)==3:
					params_case1 = (self.impositions_list[0])
					params_case2 = (self.impositions_list[1])
					params_case3 = (self.impositions_list[2])
					result_1 = self.db_conn.execute_param_query(next_query, params_case1)
					print("R1 {}".format(result_1))
					result_1 = list(set(result_1))
					result_2 = self.db_conn.execute_param_query(next_query, params_case2)
					print("R2 {}".format(result_2))
					result_2 = list(set(result_2))
					result_3 = self.db_conn.execute_param_query(next_query, params_case3)
					print("R3 {}".format(result_3))
					result_3 = list(set(result_3))
					result_sess = list(set(result_1) & set(result_2) & set(result_3))
					check_is_not_over = 0
				elif len(self.impositions_list)==4:
					params_case1 = (self.impositions_list[0])
					params_case2 = (self.impositions_list[1])
					params_case3 = (self.impositions_list[2])
					params_case4 = (self.impositions_list[3])
					result_1 = self.db_conn.execute_param_query(next_query, params_case1)
					print("R1 {}".format(result_1))
					result_1 = list(set(result_1))
					result_2 = self.db_conn.execute_param_query(next_query, params_case2)
					print("R2 {}".format(result_2))
					result_2 = list(set(result_2))
					result_3 = self.db_conn.execute_param_query(next_query, params_case3)
					print("R3 {}".format(result_3))
					result_3 = list(set(result_3))
					result_4 = self.db_conn.execute_param_query(next_query, params_case4)
					print("R4 {}".format(result_4))
					result_4 = list(set(result_4))
					result_sess = list(set(result_1) & set(result_2) & set(result_3) & set(result_4))
					check_is_not_over = 0
				elif len(self.impositions_list)==5:
					params_case1 = (self.impositions_list[0])
					params_case2 = (self.impositions_list[1])
					params_case3 = (self.impositions_list[2])
					params_case4 = (self.impositions_list[3])
					params_case5 = (self.impositions_list[4])
					result_1 = self.db_conn.execute_param_query(next_query, params_case1)
					print("R1 {}".format(result_1))
					result_1 = list(set(result_1))
					result_2 = self.db_conn.execute_param_query(next_query, params_case2)
					print("R2 {}".format(result_2))
					result_2 = list(set(result_2))
					result_3 = self.db_conn.execute_param_query(next_query, params_case3)
					print("R3 {}".format(result_3))
					result_3 = list(set(result_3))
					result_4 = self.db_conn.execute_param_query(next_query, params_case4)
					print("R4 {}".format(result_4))
					result_4 = list(set(result_4))
					result_5 = self.db_conn.execute_param_query(next_query, params_case5)
					print("R5 {}".format(result_5))
					result_5 = list(set(result_5))
					result_sess = list(set(result_1) & set(result_2) & set(result_3) & set(result_4) & set(result_5))
					check_is_not_over = 0
				elif len(self.impositions_list)==6:
					params_case1 = (self.impositions_list[0])
					params_case2 = (self.impositions_list[1])
					params_case3 = (self.impositions_list[2])
					params_case4 = (self.impositions_list[3])
					params_case5 = (self.impositions_list[4])
					params_case6 = (self.impositions_list[5])
					result_1 = self.db_conn.execute_param_query(next_query, params_case1)
					print("R1 {}".format(result_1))
					result_1 = list(set(result_1))
					result_2 = self.db_conn.execute_param_query(next_query, params_case2)
					print("R2 {}".format(result_2))
					result_2 = list(set(result_2))
					result_3 = self.db_conn.execute_param_query(next_query, params_case3)
					print("R3 {}".format(result_3))
					result_3 = list(set(result_3))
					result_4 = self.db_conn.execute_param_query(next_query, params_case4)
					print("R4 {}".format(result_4))
					result_4 = list(set(result_4))
					result_5 = self.db_conn.execute_param_query(next_query, params_case5)
					print("R5 {}".format(result_5))
					result_5 = list(set(result_5))
					result_6 = self.db_conn.execute_param_query(next_query, params_case6)
					print("R6 {}".format(result_6))
					result_6 = list(set(result_6))
					result_sess = list(set(result_1) & set(result_2) & set(result_3) & set(result_4) & set(result_5) & set(result_6))
					check_is_not_over = 0
				elif len(self.impositions_list)>6: 
					remove_me = random.randint(0,6)
					del(self.impositions_list[remove_me])
					
			print(" result_sess {}".format(result_sess))
			print(" result_sess {}".format(result_sess))
			print(" result_sess {}".format(result_sess))
			print(" result_sess {}".format(result_sess))
			if len(result_sess)=0: #---> step2
				print("ok found without doing STEP 2")
			else:
				#step 2 
				print("++STEP 2")
				#check inside...I need all possible combinations inside session
				print("IMPO CATCATCATCATCATCAT {}".format(impo_categories))
				for g in range(len(self.impositions_list)):
					print("ONLY IMPO NO NEED loop num => {}".format(g))
					if impo_categories[g]=='as':
						#print("we are IN IMPO ASK QUESTION")
						params_case_first = (self.impositions_list[g])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Sessions_Matches JOIN \
								(Matches_Games JOIN \
									(Games JOIN \
										(Games_Questions JOIN Impositions_Questions USING (question_id)) \
										USING (game_id)) \
									USING (game_id)) \
								USING (match_id)) \
						USING(session_id) \
						WHERE (status ='default' or status ='to_do') \
						AND Impositions_Questions.imposition_name = ?"
					if impo_categories[g]=='ty':
						#print("we are IN IMPO TYPE GAME")
						#same type impo query for all 3 cases
						params_case_first = (dict_impo_type_g[self.impositions_list[g]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Sessions_Matches JOIN \
								(Matches_Games JOIN \
									(Games JOIN \
										(Games_Questions JOIN Questions USING (question_id))\
										USING (game_id)) \
									USING (game_id)) \
								USING (match_id)) \
						USING(session_id) \
						WHERE (status ='default' or status ='to_do') \
						AND Questions.type = ?" 
					if impo_categories[g]=='ki':
						#print("we are IN IMPO KIND GAME")
						#same type impo query for all 3 cases
						params_case_first = (dict_impo_kind_g[self.impositions_list[g]])
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Sessions_Matches JOIN \
								(Matches_Games JOIN \
									(Games JOIN \
										(Games_Questions JOIN Questions USING (question_id))\
										USING (game_id)) \
									USING (game_id)) \
								USING (match_id)) \
						USING(session_id) \
						WHERE (status ='default' or status ='to_do') \
						AND Questions.kind = ?" 
					if impo_categories[g]=='qu':
						print("non c'is kid quindi chissene delle domande non fatte")
					if impo_categories[g] in ('cr_se','cr_sn','cr_sm','cr_sh'):
						#print("we are IN IMPO complexity session")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								(Sessions_Matches JOIN \
									(Matches_Games JOIN Matches USING (match_id)) \
									USING (match_id)) \
								USING(session_id) \
							WHERE (status ='default' or status ='to_do') \
							AND Sessions.complexity = ?"
						if impo_categories[g]=='cr_se':
							params_case_first = ('Easy')
						elif impo_categories[g]=='cr_sn':
							params_case_first = ('Normal')
						elif impo_categories[g]=='cr_sm':
							params_case_first = ('Medium')
						elif impo_categories[g]=='cr_sh':
							params_case_first = ('Hard')				
					if impo_categories[g] in ('cr_e1','cr_e2','cr_n1','cr_n2','cr_m1','cr_m2','cr_h1','cr_h2'):
						#print("we are IN IMPO DIFFICULTIY match")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Sessions_Matches JOIN \
								(Matches JOIN \
									(Matches_Games JOIN Games USING (game_id)) \
									USING (match_id)) \
								USING (match_id)) \
							USING(session_id) \
							WHERE (status ='default' or status ='to_do') \
							AND Matches.difficulty = ?"
						if impo_categories[g]=='cr_e1':
							params_case_first = ('Easy_1')
						elif impo_categories[g]=='cr_e2':
							params_case_first = ('Easy_2')
						elif impo_categories[g]=='cr_n1':
							params_case_first = ('Normal_1')
						elif impo_categories[g]=='cr_n2':
							params_case_first = ('Normal_2')
						elif impo_categories[g]=='cr_m1':
							params_case_first = ('Medium_1')
						elif impo_categories[g]=='cr_m2':
							params_case_first = ('Medium_2')
						elif impo_categories[g]=='cr_h1':
							params_case_first = ('Hard_1')
						elif impo_categories[g]=='cr_h2':
							params_case_first = ('Hard_2')
					if impo_categories[g] in ('cr_e1g','cr_e2g','cr_n1g','cr_n2g','cr_m1g','cr_m2g','cr_h1g','cr_h2g'):
						#need to solutionts separated impo e need
						#print("we are IN IMPO DIFFICULTIY match")
						next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Sessions_Matches JOIN \
								(Matches JOIN \
									(Matches_Games JOIN Games USING (game_id)) \
									USING (match_id)) \
								USING (match_id)) \
							USING(session_id) \
							WHERE (status ='default' or status ='to_do') \
							AND Games.difficulty = ?"
						if impo_categories[g]=='cr_e1g':
							params_case_first = ('Easy_1')
						elif impo_categories[g]=='cr_e2g':
							params_case_first = ('Easy_2')
						elif impo_categories[g]=='cr_n1g':
							params_case_first = ('Normal_1')
						elif impo_categories[g]=='cr_n2g':
							params_case_first = ('Normal_2')
						elif impo_categories[g]=='cr_m1g':
							params_case_first = ('Medium_1')
						elif impo_categories[g]=='cr_m2g':
							params_case_first = ('Medium_2')
						elif impo_categories[g]=='cr_h1g':
							params_case_first = ('Hard_1')
						elif impo_categories[g]=='cr_h2g':
							params_case_first = ('Hard_2')
					if impo_categories[g] in ('ch1_m','ch2_m','ch3_m','ch4_m','ch1_q','ch2_q','ch3_q','ch4_q','ch1_g','ch2_g','ch3_g','ch4_g'):
						if impo_categories[g]=='ch1_m':
							#print("we are IN IMPO NUM 1 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN Matches USING(match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 1"
						if impo_categories[g]=='ch2_m':
							#print("we are IN IMPO NUM 2 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN Matches USING(match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 2"
						if impo_categories[g]=='ch3_m':
							#print("we are IN IMPO NUM 3 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN Matches USING(match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 3"
						if impo_categories[g]=='ch4_m':
							#print("we are IN IMPO NUM 4 quantity of matches -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN Matches USING(match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Sessions.num_of_matches = 4"
						if impo_categories[g]=='ch1_g':
							#print("we are IN IMPO NUM 1 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 1"
						if impo_categories[g]=='ch2_g':
							#print("we are IN IMPO NUM 2 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 2"
						if impo_categories[g]=='ch3_g':
							#print("we are IN IMPO NUM 3 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 3"
						if impo_categories[g]=='ch4_g':
							#print("we are IN IMPO NUM 4 quantity of games -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN Matches USING (match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Matches.num_of_games = 4"
						if impo_categories[g]=='ch1_q':
							#print("we are IN IMPO NUM 1 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Games.num_questions = 1"
						if impo_categories[g]=='ch2_q':
							#print("we are IN IMPO NUM 2 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Games.num_questions = 2"
						if impo_categories[g]=='ch3_q':
							#print("we are IN IMPO NUM 3 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Games.num_questions = 3"
						if impo_categories[g]=='ch4_q':
							#print("we are IN IMPO NUM 4 quantity of question -- game")
							next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
										(Sessions_Matches JOIN \
											(Matches JOIN \
												(Matches_Games JOIN Games USING (game_id)) \
												USING (match_id)) \
											USING (match_id)) \
									USING(session_id) \
								WHERE (status ='default' or status ='to_do') \
								AND Games.num_questions = 4"

						result_ques = self.db_conn.execute_a_query(next_query)
						if len(result_ques)==1:
							result_sess = copy.copy(result_ques)
						if len(result_ques)>1:
							result_sess = [item for sublist in result_ques for item in sublist]
						if len(result_ques)==0:
							result_sess = []
						print(" result_ques {} ".format(result_ques))
						print(" result_sess {} ".format(result_sess))

					if impo_categories[g]=='mi':
						#print("we are IN IMPO mixed match")
						ok_stop_here = 1
						par = (self.id_child)
						sess_query = "SELECT Sessions.session_id FROM Sessions JOIN \
							(Kids_Sessions JOIN \
								(Sessions_Games JOIN (Games USING(game_id)) \
							USING (session_id)) \
						USING (session_id) \
						WHERE kid_id = ? \
						AND kind = 'MIX' \
						AND (status ='default' or status ='to_do')"
						se_id = self.db_conn.execute_param_query(sess_query, par)						
						if len(se_id)==1:
							result_sess = se_id[0]
						if len(se_id)>1:
							result_sess = [item for sublist in se_id for item in sublist]
						else:
							result_sess = []
						if ok_stop_here==1:
							sess_query = "SELECT Sessions.session_id FROM Sessions JOIN \
								Kids_Sessions JOIN \
									(Sessions_Matches JOIN Matches USING(match_id)) \
									USING (session_id)) \
								USING (session_id) \
								WHERE (status ='default' or status ='to_do')"
							se_id = self.db_conn.execute_a_query(sess_query)
							if len(se_id)>0:
								res_all_sessions = [item for sublist in se_id for item in sublist]
							else:
								res_all_sessions = []
							print("first impression {}".format(res_all_sessions))
							for p in range(len(res_all_sessions)):
								param_diff = (res_all_sessions[p])
								m_query = "SELECT match_id FROM Sessions JOIN Sessions_Matches USING (session_id) \
								WHERE session_id = ?"
								res_matches = self.db_conn.execute_param_query(m_query, param_diff)
								if len(res_matches)>1: 
									current_matches = [item for sublist in res_matches for item in sublist]
								if len(res_matches)==1: 
									current_matches = copy.copy(res_matches)
								if len(res_matches)==0: 
									current_matches = []
								print("current_matches is : ",current_matches)
								print("evaluate session result sess {}".format(current_matches))
								second_q_1 = "SELECT num_of_matches FROM Sessions WHERE session_id = ?"
								second_q_2 = "SELECT num_of_games FROM Sessions JOIN \
												(Sessions_Matches JOIN Matches USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"
								second_q_3 = "SELECT num_questions FROM Sessions JOIN \
												(Sessions_Matches JOIN \
													(Matches_Games JOIN Games USING (game_id)) \
													USING (match_id)) \
												USING (session_id) \
											WHERE Sessions.session_id = ?"

								res_mat = self.db_conn.execute_param_query(second_q_1, res_all_sessions[p])
								res_gam = self.db_conn.execute_param_query(second_q_2, res_all_sessions[p])
								res_que = self.db_conn.execute_param_query(second_q_3, res_all_sessions[p])

								if len(res_gam)>1:
									res_num_gam = [item for sublist in res_gam for item in sublist]
								if len(res_gam)==1:
									res_num_gam = copy.copy(res_gam)
								if len(res_gam)==0:
									res_num_gam = []
								if len(res_que)>1:
									res_num_que = [item for sublist in res_que for item in sublist]
								if len(res_que)==1:
									res_num_que = copy.copy(res_que)
								if len(res_que)==0:
									res_num_que = []
								is_mixed_1=0
								is_mixed_2=0
								is_mixed_3=0
								is_mixed_4=0
								print("res_num_mat res_num_mat {}".format(res_mat))
								print("res_num_gam res_num_gam {}".format(res_num_gam))
								print("res_num_que res_num_que {}".format(res_num_que))

								if len(res_num_gam)==1:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games made by 1 question")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[1]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
										if res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
								elif len(res_num_gam)==2:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										print("numb ma 2")
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1
									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1
								elif len(res_num_gam)==3:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1
								elif len(res_num_gam)==4:
									if res_num_gam[0]==1:
										if res_num_que[0]==1:
											print("first match has a game all made by 1 quest")
											is_mixed_1 = 0
									if res_num_gam[0]==2:
										if res_num_que[0]==1 and res_num_que[1]==1:
											print("first match has 2 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==3:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1:
											print("first match has 3 games all made by 1 quest")
											is_mixed_1 = 1
									if res_num_gam[0]==4:
										if res_num_que[0]==1 and res_num_que[1]==1 and res_num_que[2]==1 and res_num_que[3]==1:
											print("first match has 4 games all made by 1 quest")
											is_mixed_1 = 1

									i = res_num_gam[0]
									if res_num_gam[1]==1:
										if res_num_que[i]==1:
											print("second match has a game all made by 1 quest")
											is_mixed_2 = 0
									if res_num_gam[1]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("second match has 2 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("second match has 3 games all made by 1 quest")
											is_mixed_2 = 1
									if res_num_gam[1]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("second match has 4 games enquiry")
											is_mixed_2 = 1

									i = res_num_gam[1]
									if res_num_gam[2]==1:
										if res_num_que[i]==1:
											print("third match has a game all made by 1 quest")
											is_mixed_3 = 0
									if res_num_gam[2]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("third match has 2 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("third match has 3 games all made by 1 quest")
											is_mixed_3 = 1
									if res_num_gam[2]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("third match has 4 games enquiry")
											is_mixed_3 = 1

									i = res_num_gam[2]
									if res_num_gam[3]==1:
										if res_num_que[i]==1:
											print("fourth match has a game all made by 1 quest")
											is_mixed_4 = 0
									if res_num_gam[3]==2:
										if res_num_que[i]==1 and res_num_que[i+1]==1:
											print("fourth match has 2 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==3:
										if res_num_que[i]==1 and res_num_que[i+1]==1 and res_num_que[i+2]==1:
											print("fourth match has 3 games all made by 1 quest")
											is_mixed_4 = 1
									if res_num_gam[3]==4:
										if res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1 and res_num_que[i]==1:
											print("fourth match has 4 games enquiry")
											is_mixed_4 = 1
								#print("MIXXXXX")
								#print(is_mixed_1)
								#print(is_mixed_2)
								#print(is_mixed_3)
								#print(is_mixed_4)
								if is_mixed_1=0 or is_mixed_2=0 or is_mixed_3=0 or is_mixed_4=0:
									#check control match in wichi there is ask_quest_type if generic and non mixed
									q0 = "DROP TABLE IF EXISTS mini_tab"
									bah = self.db_conn.execute_a_query(q0)
									q1 = "CREATE TABLE IF NOT EXISTS mini_tab \
										AS SELECT session_id,match_id FROM Sessions_Matches WHERE session_id = ?;"
									
									res_q1 = self.db_conn.execute_param_query(q1, res_all_sessions[p])
									final_q1 = [item for sublist in res_q1 for item in sublist]
									print("the length current MAT IS {}".format(len(current_matches)))
									for j in range(len(current_matches)):
										print("numb numb numb numb numb numb numb numb loop tra i match {}".format(j))
										print("session: {} match: {}".format(res_all_sessions,current_matches[j]))
										q2 = "SELECT oid FROM mini_tab WHERE match_id = ?"
										res_q2 = self.db_conn.execute_param_query(q2,current_matches[j])
										final_bo = res_q2
										print("res q2res q2res q2res q2 riga match = {}".format(res_q2))
										#final_bo = res_q2[0]
										print("final_bo {}".format(final_bo))
										#q3 = "DROP TABLE IF EXISTS mini_tab"
										result_sess_id = 0
										if len(final_bo)>0:
											if (final_bo[0]==1 and not is_mixed_1) or (final_bo[0]==2 and not is_mixed_2) or \
											(final_bo[0]==3 and not is_mixed_3) or (final_bo[0]==4 and not is_mixed_4):
												result_sess_id = 0
											if (final_bo[0]==1 and is_mixed_1) or (final_bo[0]==2 and is_mixed_2) or \
											(final_bo[0]==3 and is_mixed_3) or (final_bo[0]==4 and is_mixed_4):
												result_sess_id = res_all_sessions[p]
											print("That is {}".format(result_sess_id))
											if result_sess_id=0:
												all_session_id_results.append(res_all_sessions[p])
												print("all_session_id_results {}".format(all_session_id_results))
							print("all_session_id_results {}".format(all_session_id_results))
							if len(all_session_id_results)==1:
								result_sess = copy.copy(all_session_id_results)
							if len(all_session_id_results)>1:
								result_sess = [item for sublist in all_session_id_results for item in sublist]
							if len(all_session_id_results)==0:
								result_sess = []
							try:
								result_sess = list(itertools.chain.from_iterable(result_sess))
								print("result_sess total after itertools is {}".format(result_sess))
							except Exception:
								result_sess = list(self.iterFlatten(result_sess))
								print("result_sess total after flatten is {}".format(result_sess))
							result_sess = list(set(result_sess))
							print("result_sess total after il SET is {}".format(result_sess))
					
					if impo_categories[g] not in ('mi','ch1_m','ch2_m','ch3_m','ch4_m','ch1_g','ch2_g','ch3_g','ch4_g','ch1_q','ch2_q','ch3_q','ch4_q'): #no param needed 
						print("NEXT QUERY IS = ")
						print(next_query)
						result_ques = self.db_conn.execute_param_query(next_query,params_case_first)
						print("result_ques {}".format(result_ques))
						if len(result_ques)==1:
							result_sess = copy.copy(result_ques)
						if len(result_ques)>1:
							result_sess = [item for sublist in result_ques for item in sublist]
						if len(result_ques)==0:
							result_sess = []
						print(" result_ques {} ".format(result_ques))
						print(" result_sess {} ".format(result_sess))
					
					if len(result_sess)>0:
						if len(all_sessions_5)>0:
							if self.check_lol(all_sessions_5):
								all_sessions_55 = functools.reduce(operator.iconcat, all_sessions_5, [])
								inter_ses = list(set(result_sess) & set(all_sessions_55))
							else:
								inter_ses = list(set(result_sess) & set(all_sessions_5))
							print("INTER session {}".format(inter_ses))
							if len(inter_ses)>0:
								all_sessions_5 = copy.copy(inter_ses)
								print("how is session_5 after copia {}".format(all_sessions_5))
							else:
								result_sess=[]
								break
						else:									
							all_sessions_5.append(result_sess)
					else:
						result_sess = []
						break
				if len(result_sess)>0:
					print("RESULT SESS 5 after FOR {}".format(result_sess))
					result_sess = all_sessions_5
		else:
			result_sess=[]
		if len(result_sess)>0:
			print("all_sessions_5 {}".format(result_sess))
			all_sessions_5_ok = list(self.iterFlatten(result_sess))
			print("all_sessions_5_ok ok  {}".format(all_sessions_5_ok))
			result_sess = list(set(all_sessions_5_ok))
			print("Final result session CON many GOAL {}".format(result_sess))
			result_sess = random.choices(result_sess, k=1)
			result_sess = result_sess[0]
			print("OLD find_one_sess ", result_sess)
		else:
			result_sess = 0
		return result_sess
		
	cpdef realtime_creation(self):
		ready = self.extract_ready_session()
		if ready:
			print("RESULT SESS BEFORE CREATING OBJ {}".format(ready))
			self.create_obj_session(ready,0)
		else:
			print("STEP 3")
			impos_this_time = [] #always lenght == 1 with one or more impo
			impos_this_time.append(self.impositions_list)
			impos_this_time.append(self.level_area) #add to customized...need flatten later 
			print("impos_this_time to pass {}".format(impos_this_time))
			manda_impo = 1 #always 1 kid is specified ...
			manda_need = 1
			self.db_conn.add_new_Session(0,manda_impo,manda_need,impos_this_time)
			print("END REALTIME CREATION...TUTTO OK")
			next_query = "SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1"
			#next_query = "SELECT * FROM Sessions JOIN Kids_Sessions USING(session_id) WHERE kid_id = ? AND status='to_do' ORDER BY random() LIMIT 1"
			sess_id = self.db_conn.execute_a_query(next_query)
			sess_id = sess_id[0]
			print("RESULT SESS BEFORE CREATING OBJ {}".format(sess_id))
			#add_new_imposition_to_list sess_impo
			for imp in self.impositions_list:
				self.db_conn.add_new_SessionImposition(sess_id,imp)
			self.create_obj_session(sess_id,0)

	def check_lol(self, lis): #avoid clousureclass cpdef cython limit
		if any(isinstance(el, list) for el in lis):
			return 1
		else:
			return 0

cdef class Session_6(SessionBase):
	def __cinit__(self, str id_scenario):
		self.id_scenario = id_scenario
		self.db_conn = db.DatabaseManager()
	
	cpdef extract_ready_session(self):
		cdef:
			bint way_scen = 0
			bint ask_pos_scen = 0
			bint ins_scen_diff = 0
		params6 = (self.id_scenario)
		next_query = "SELECT Sessions.session_id FROM Sessions JOIN \
			(Sessions_Matches JOIN Matches USING(match_id)) \
		USING(session_id) \
		WHERE Matches.scenario = ? \
		ORDER BY random()"
		result_sess = self.db_conn.execute_param_query(next_query, params6)
		result_sess = list(set(result_sess))
		print("$ result_SESS",result_sess)
		if len(result_sess)>0:
			print("all_sessions_6 {}".format(result_sess))
			all_sessions_6_ok = list(self.iterFlatten(result_sess))
			print("all_sessions_4_ok  {}".format(all_sessions_6_ok))
			result_sess = list(set(all_sessions_6_ok))
			print("Final result session {}".format(result_sess))
			result_sess = random.choices(result_sess, k=1)
			result_sess = result_sess[0]
			#print("NE ESTRAGGO UNO SOLO {}".format(result_sess))
		else:
			result_sess = 0
		return result_sess

	cpdef realtime_creation(self):
		ready = self.extract_ready_session()
		if ready:
			print("RESULT SESS BEFORE CREATING OBJ {}".format(ready))
			self.create_obj_session(ready,0) #no kid
		else:
			scen = []
			scen_str = 'scenario_{}'.format(self.id_scenario)
			impos_this_time = []
			scen.append(self.id_scenario)
			impos_this_time.append(scen_str)
			print("impos_this_time to pass {}".format(impos_this_time))
			manda_impo = 0 
			manda_need = 0 
			self.db_conn.add_new_Session(0,manda_impo,manda_need,impos_this_time)
			print("END REALTIME CREATION...TUTTO OK")
			next_query = "SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1"
			sess_id = self.db_conn.execute_a_query(next_query)
			sess_id = sess_id[0]
			print("RESULT SESS BEFORE CREATING OBJ {}".format(sess_id))
			self.create_obj_session(sess_id,0)