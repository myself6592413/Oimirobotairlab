# ==========================================================================================================================================================
# Session whole documentation
# ==========================================================================================================================================================
def session_class_doc():
	""" Info
	A session
		Represents a full playing activity for a specified kid, properly designed according to his pecurial character and needs.
        Is intended to be played once a day.
		Contains all informations and data necessary to succesfully accomplish a therapeutic session for any kid.
           All therapeutic games are those game that involves patches placed on OIMI's front-upper part of the body.
        Is composed by one or more matches, with their (possibly different) scenario, each match contains several games,
            formed by questions (and relative answer) that involves the interaction with all patches placed on robot's body.
        Can have an (external) imposition that define a global goal for the kid is going to face it
		Can have global goal, that influence guide lines of future decisions in creation of certain match, games with some questions.
		
		Session base object cannot be called, only Session_1,2,3...
		Each session is stored into database with status: 
            _default = predefined, can be choosen in any time. It's used not only for general tests on robot, but especially for investigating skills of unkown children, 
                which are interacting with robot for the first time (can be shared by different kids).
            _done = completed session so far, but still ready to use another time by same kid, or another
            _pending = used and unfinished, must be completed before ready to use another time but same kid or another or none (cannot be used by others it's exclusive for kid that is been playing)
            _to-do = ready to use, prepared for each kid for next time. 
        If a session is not ready or does not exist for a child, it is created in realtime
        ##################################################################################################################################
		SessionBase: no parameters given 
		Prepare purpose:
			SessionP1: kid given, scenario not given choosen later according to rules (choosen according to kid questions needs)
			, imposition [yes/no] = save into session table = save into tabellone?? --> no! create a new session in db directly!
			SessionP2: kid not given, scenario given, yes/no imposition= save into session table = save into tabellone ?
				no! create a new session in db directly!
		Use purpose: 
		SessionU1: yes session_id; no kid, no impositions = used for test 
			extract from db Sessions table the one with received id (can be complete or not but not pending) 
			=> kid ignored 
		SessionU2: no session_id; no kid, yes/no impositions = used for test  
			extract from db Sessions table one session (can be complete or not but not pending) 
			=> kid ignored 
			=> seletion according to impositions (if imposition = 'None', completely random selection)
		SessionU3: kid given, no session_id given, yes/no imposition
			=> seletion according to impositions between available ready incomplete sessions (if imposition = 'None', pick up first one)
			case in which it is wanted to try a kid in a determined scenario, to test his abilities
		####no enforcements = need or imposistion
		1 look for existing sessionin db with right scenario created to that kid 
			if not exists:
				find session with right scenario not for that kid, but for another child with similar characteristics (type, age, level, num games done)
		2 create new sessions 
			two possibilities
				_randomforest prediction + controllo che scenario sia suitable ...if not, redo prediction until scenario is valid 
				_normal alg (no randomforest) -> from scenario to kid using basic info like age,level,important issue

		###con need or imposition 
		same steps but considering also need and impo --> looking at session-impo / session-needs
		otherwise it is created

		SessionU4: kid, yes/no imposition
        ##################################################################################################################################

		extract_ready_session()
			Check levels: session, match, game (the new variable TOGETHER that is cancelled)
			goal : search existing ready session (or match or goal) specific for the current kid considering 
			1 impo and 1 need given
			step_1 search into session_impo+session_need (or match_impo+match_need) (or game_impo+game_need)
			(means that this session contains matches that have been really created considering that need and that impo)
			Consider each type of impo combined with each type of need 
			According to each level I have constraints...some couple is forbidden..for example:
			impo = type of game = 7O and need = find a game of type 1fcos ... for same game impossible
			ok find a match with different gamnes with diff kind. als for session is ok


			step 2 if result (s,m or g) doesnt exist---> find in tables ignoring bridge tables 
			session-impo and session-need (or mat_impo + mat_need or gam_impo + gam_need)
			but looking inside table 
			table to_do or default
			step 3 create randomforest modified the table
		
			Imposition impo_categories LEGEND 
				as : ask specific question
				ki , ty : kind , type
				qu : use a never answered question
				complexity:
					cr_se, cr_, cr_, cr_sh (easy,normal,medium,hard)
					cr_e1, cr_e2, cr_n1, cr_n2, cr_m1, cr_m2, cr_h1, cr_h2 (easy_1....)


		Each session is created following these criteria:
			=> multiple __cinit__() and heritance = different constructors 
			OFFLINE (before a known kid starts a new session)
				1) prepare complete session  
					==> based only on available info  
					prepare incomplete session + complete session = use of rules table
				2) prepare incomplete session 
					==> prediction on: 
					_session complexity
					_num of matches
					_order of difficulty for sessions' matches [ASC,DESC,SAME]
					_order of number of matches for sessions' matches [ASC,DESC,SAME]
					_matches_difficulty
					
					==> based only on available info 
					_scenario (hypotized)
					_id session?? (if given)
					_kid (if given)
					_impositions (if given)
						_session_easy/normal/medium/hard
						_match_easy/normal/medium/hard
						_num....
						_order
						_diff
						_multiple conditions
					NB = For incomplete preparation complete session = use of rules table id done realtime according to current scenario 				_
			ONLINE (after session request immediately before starting game)
				extract completed session from DB:
					from:
						id
						kid next to choose
						kid next to choose with impositions 
				extract incompleted session from DB, then predict exact questions completing the session = use of rules, for filling all missing session (natch / game / question) information folliwing rules at r
					from: 
						id
						kid next to choose
						kid next to choose with impositions 


Parameters
	_id_session 			# univoque id current session => id_already stored = 1 
	_id_child  				# kid associated 
	_is_already_stored      # true if the session object is created completely based on an already made session stored into oimi database 
								=> extract_session() instead of prepare_session()
							# false prepare session for creating a ready session 
								=> prepare_session() to create the next session to play for a specific Kid according to its statistics and peculiarity.
	_complexity	 			# session difficulty [Easy, Normal, Medium, Hard] (called 'complexity' only for session)
	
	_mandatory_purpose  	# When creating games also takes into account child impositions, otherwise is less constrained and takes into account kid (level, symptoms, need)
	_goal_or_not 			# When creating games consider if makes or not a session global improvement, consider also scores, achievements (se difficile faccio anche contro-need per vedere se è migliorato)
	_imposition 			# Imposed rules, when child is not given, used for choosing specific matches, games and purpose 

Attributes
	-desirable_time				# reasonalbe amount of time (limit) in which kid should finish session completely [from 5 to 30]
	-audio_intro_session		# 
	-audio_end_session			# 
	-num_of_matches				# int 
	-matches_of_this_session 	# mt.Match[:]
	-current_match 				# int
	-complexity 				# SessionDifficulty 
	-session_scores 			# sc.Score
	-db_conn					# sqlite3 connection to oimi_robot_database in ../../src

Notes: 
	in __enter_ method add the "#return self" line, If you want to call the object with:
	a = 1
	with MyClass(a) as my_object:
	    my_object.method1()
	otherwise it is mandatory to call the class with: ...but the __enter__ and __exit__ methods will be ignored
	with MyClass(a) as my_object:
	    my_object.method1()


Details
	table_1 prepare session => OUTPUTS = session complexity, num of matches, order of difficulty of matches [SAME, ASC, DESC]

		Features:
			Level: kid's level reached 
			S1 = symptom_1: diversion during activities => num of matches
			S2 = symptom_2: endurance => order of difficulty of matches and num of matches = yes desc, no asc!
			S3 = symptom_3: meltdown => complexity of session and num of games
			
			A1 = achievement_1
				number of easy session already completed 
				number of normal session already completed  
				number of medium session already completed  
				number of hard session already completed  
			A2 = achievement_2
				total session completed
				total matches completed
			A3 = achievement_3
				max num of matches in a session find
			A4 = achievement_4
				num of sessions with matches in SAME order of difficulty 
				num of sessions with matches in ASC order of difficulty 
				num of sessions with matches in DESC order of difficulty 

	table_2 prepare session => OUTPUTS =  matches difficulty, games order of difficulty, games order of quantity 
		Three different tables respectively for 3 matches, for 4 matches, for 5 matches

"""
def SessionBase_iterFlatten():
	"""
	Session basis for all shared methods/attributes
	Methos:
	--> iterFlatten: 
		flattens a nested list or tuple by recursively iterating through each element and yielding the individual elements.
		It starts by checking if the "root" parameter is an instance of the list or tuple data type. 
		If it's so, it loops through each element in "root" and recursively calls itself on each element. 
		This is done to handle cases where the nested structure has multiple levels of nesting.
		If the element is not a list or tuple, it means that it's an individual elem in the nested structure and is then yielded. 
		The yield statement allows the method to return a generator object that can be iterated through.
		By using a generator function and yielding elements as they are generated, 
		the fun can handle large nested structures, without requiring for a large amounts of memory.
	"""