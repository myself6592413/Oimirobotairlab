rm *.so
rm -rf build
rm session/session.cpp
rm match/match.cpp
rm game/game.cpp
rm -rf game/__pycache__
rm score/score.cpp
rm scenario/scenario.cpp
rm games_classes/games_classes.cpp
rm games_reactions/games_reactions.cpp
rm game_letsplay/game_letsplay.cpp
