############## KIND 3S

def build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,same_col,same_ele):
	print("WE ARE IN KIND 3S")
	ali,ali_tmp,ali_1,tot,tot_1 = [],[],[],[],[]
	all_alternative_pos=[0,0,0,0]
	one_ans_is_enough=[0,0,0,0]
	multiple_branch_ans=[0,0,0,0]
	multiple_or_ans=[0,0,0,0]
	touch_together=[0,0,0,0]
	no_ans_to_create_only_body=[0,0,0,0]
	body_response=[0,0,0,0]
	body_and_patch=[0,0,0,0]
	need_light=[0,0,0,0]
	need_light1=[0,0,0,0]
	need_light2=[0,0,0,0]
	need_light3=[0,0,0,0]
	the_first=[0,0,0,0]
	the_second=[0,0,0,0]
	body_num_press=[0,0,0,0]
	all_num_touches_to_do=[0,0,0,0]
	ques_9_num = [0,0,0,0]
	limit = [0,0,0,0]
	limit1 = [0,0,0,0]
	limit2 = [0,0,0,0]
	limit3 = [0,0,0,0]
	limitRed = [0,0,0,0]
	limitGreen = [0,0,0,0]
	limitBlue = [0,0,0,0]
	limitYellow  = [0,0,0,0]
	limitLion = [0,0,0,0]
	limitMonkey = [0,0,0,0]
	limitParrot = [0,0,0,0]
	limitOctopus = [0,0,0,0]
	for k,kaudio in enumerate(listaudios):
		###3SCO
		if kaudio=='audio_3012':
			if same_col['Red']>=2:
				alir = [i+1 for i,element in enumerate(patpat_col) if element=='Red']
				lim_r=len(alir)
				limitRed[k]=lim_r
			if same_col['Green']>=2:
				alig = [i+1 for i,element in enumerate(patpat_col) if element=='Green']
				lim_g=len(alig)
				limitGreen[k]=lim_g
			if same_col['Blue']>=2:
				alib = [i+1 for i,element in enumerate(patpat_col) if element=='Blue']
				lim_b=len(alib)
				limitBlue[k]=lim_b
			if same_col['Yellow']>=2:
				aliy = [i+1 for i,element in enumerate(patpat_col) if element=='Yellow']
				lim_y=len(aliy)
				limitYellow[k]=lim_y
			ali = alir+alig+alib+aliy
		###3SSU
		elif kaudio=='audio_3011':
			if same_col['lion']>=2:
				alir = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
				lim_r=len(alir)
				limitLion[k]=lim_r
			if same_col['monkey']>=2:
				alig = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
				lim_g=len(alig)
				limitMonkey[k]=lim_g
			if same_col['parrot']>=2:
				alib = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
				lim_b=len(alib)
				limitParrot[k]=lim_b
			if same_col['octopus']>=2:
				aliy = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
				lim_y=len(aliy)
				limitOctopus[k]=lim_y
			ali = alir+alig+alib+aliy

		if len(ali_1) or len(tot_1):
			tot.append(ali)
			if all_alternative_pos[k] in (0,1):
				tot_1.append(ali) #not ali_1
			else:
				tot_1.append(ali_1)
		else:
			print("partial_ali {}".format(ali))
			tot.append(ali)
		ali=[]
		ali_1=[]
	print("all_alternative_pos={}".format(all_alternative_pos))
	print("tot --> {}".format(tot))
	print("tot_1 --> {}".format(tot_1))

	all_properties = []
	all_properties.append(tot)
	all_properties.append(tot_1)
	all_properties.append(all_alternative_pos)
	all_properties.append(one_ans_is_enough)
	all_properties.append(multiple_branch_ans)
	all_properties.append(multiple_or_ans)
	all_properties.append(touch_together)
	all_properties.append(no_ans_to_create_only_body)
	all_properties.append(body_response)
	all_properties.append(body_and_patch)
	all_properties.append(need_light)
	all_properties.append(need_light1)
	all_properties.append(need_light2)
	all_properties.append(need_light3)
	all_properties.append(the_first)
	all_properties.append(the_second)
	all_properties.append(body_num_press)
	all_properties.append(all_num_touches_to_do)
	all_properties.append(ques_9_num)
	all_properties.append(limit)
	all_properties.append(limit1)
	all_properties.append(limit2)
	all_properties.append(limit3)
	all_properties.append(limitRed)
	all_properties.append(limitGreen)
	all_properties.append(limitBlue)
	all_properties.append(limitYellow)
	all_properties.append(limitLion)
	all_properties.append(limitMonkey)
	all_properties.append(limitParrot)
	all_properties.append(limitOctopus)
	return all_properties
