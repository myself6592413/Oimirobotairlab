"""
In case of MIXED games.
The module is split into 4 parts due to its original huge dimension, using "partial"
"""
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
from functools import partial
import game_answer_shared_methods as gasm
import game_answer_mixed_1 as gami1
import game_answer_mixed_2 as gami2
import game_answer_mixed_3 as gami3
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
def build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios_int,listatypes,same_col,same_ele):
	gami1.res1 = build_Resp_1
	gami2.res2 = build_Resp_2
	gami3.res3 = build_Resp_3
	gami3.res4 = build_Resp_4

	return gami1.res1 + gami2.res2 + gami3.res3 + gami3.res4