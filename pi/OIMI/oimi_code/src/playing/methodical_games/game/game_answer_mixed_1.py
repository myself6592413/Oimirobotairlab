# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
from functools import partial
import game_answer_shared_methods as gasm
import game_answer_mixed_1 as gami1
import game_answer_mixed_2 as gami2
import game_answer_mixed_3 as gami3
# ==========================================================================================================================================================
# Method
# ==========================================================================================================================================================
def build_Resp_1(patpat_ele,patpat_col,patpat_gen,listaudios_int,listatypes,same_col,same_ele):
	ali,ali_tmp,ali_1,tot,tot_1 = [],[],[],[],[]
	all_alternative_pos=[0,0,0,0]
	one_ans_is_enough=[0,0,0,0]
	multiple_branch_ans=[0,0,0,0]
	multiple_or_ans=[0,0,0,0]
	touch_together=[0,0,0,0]
	no_ans_to_create_only_body=[0,0,0,0]
	body_response=[0,0,0,0]
	body_and_patch=[0,0,0,0]
	need_light=[0,0,0,0]
	need_light1=[0,0,0,0]
	need_light2=[0,0,0,0]
	need_light3=[0,0,0,0]
	the_first=[0,0,0,0]
	the_second=[0,0,0,0]
	body_num_press=[0,0,0,0]
	all_num_touches_to_do=[0,0,0,0]
	ques_9_num = [0,0,0,0]
	limit = [0,0,0,0]
	limit1 = [0,0,0,0]
	limit2 = [0,0,0,0]
	limit3 = [0,0,0,0]
	limitRed = [0,0,0,0]
	limitGreen = [0,0,0,0]
	limitBlue = [0,0,0,0]
	limitYellow  = [0,0,0,0]
	limitLion = [0,0,0,0]
	limitMonkey = [0,0,0,0]
	limitParrot = [0,0,0,0]
	limitOctopus = [0,0,0,0]
	for k,kaudio in enumerate(listaudios_int):
		###1FSUS 
		if kaudio==1:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']					
		elif kaudio==2:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']					
		elif kaudio==3:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Blue']					
		elif kaudio==4:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Yellow']					
		elif kaudio==5:
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
		elif kaudio==6:
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
		elif kaudio==7:
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
		elif kaudio==8:
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
		elif kaudio==9:
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
		elif kaudio==10:
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
		elif kaudio==11:
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
		elif kaudio==12:
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
		elif kaudio==13:
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
		elif kaudio==14:
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
	###1FCACO
		elif kaudio==1763:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Red']
			one_ans_is_enough[k]=1
		elif kaudio==1764:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Green']
			one_ans_is_enough[k]=1
		elif kaudio==1765:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Yellow']
			one_ans_is_enough[k]=1
		elif kaudio==1766:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Blue']
			one_ans_is_enough[k]=1
		elif kaudio==1767:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Red']
			one_ans_is_enough[k]=1
		elif kaudio==1768:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Green']
			one_ans_is_enough[k]=1
		elif kaudio==1769:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Yellow']
			one_ans_is_enough[k]=1
		elif kaudio==1770:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Blue']
			one_ans_is_enough[k]=1
		elif kaudio==1771:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Red']
			one_ans_is_enough[k]=1
		elif kaudio==1772:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Green']
			one_ans_is_enough[k]=1
		elif kaudio==1773:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Yellow']
			one_ans_is_enough[k]=1
		elif kaudio==1774:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Blue']
			one_ans_is_enough[k]=1						
		elif kaudio==1775:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Red']
			one_ans_is_enough[k]=1
		elif kaudio==1776:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Green']
			one_ans_is_enough[k]=1
		elif kaudio==1777:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Yellow']
			one_ans_is_enough[k]=1
		elif kaudio==1778:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Blue']
			one_ans_is_enough[k]=1
		elif kaudio==1779:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Red']
			one_ans_is_enough[k]=1
		elif kaudio==1780:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Green']
			one_ans_is_enough[k]=1
		elif kaudio==1781:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Yellow']
			one_ans_is_enough[k]=1
		elif kaudio==1782:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Blue']
			one_ans_is_enough[k]=1
	###1FCACOA
		elif kaudio==1783:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Red']
		elif kaudio==1784:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Green']
		elif kaudio==1785:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Yellow']
		elif kaudio==1786:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Blue']
		elif kaudio==1787:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Red']
		elif kaudio==1788:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Green']
		elif kaudio==1789:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Yellow']
		elif kaudio==1790:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Blue']
		elif kaudio==1791:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Red']
		elif kaudio==1792:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Green']
		elif kaudio==1793:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Yellow']
		elif kaudio==1794:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Blue']
		elif kaudio==1795:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Red']
		elif kaudio==1796:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Green']
		elif kaudio==1797:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Yellow']
		elif kaudio==1798:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Blue']
		elif kaudio==1799:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Red']
		elif kaudio==1800:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Green']
		elif kaudio==1801:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Yellow']
		elif kaudio==1802:
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Blue']
	###1FCOD
		elif kaudio==120:
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Red','Green']]
		elif kaudio==121:
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Red','Yellow']]
		elif kaudio==122:
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Red','Blue']]
		elif kaudio==123:
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Green','Yellow']]
		elif kaudio==124:
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Green','Blue']]
		elif kaudio==125:
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Blue','Yellow']]	
	###1FCOS
		elif kaudio==116:
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']					
		elif kaudio==117:
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
		elif kaudio==118:
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
		elif kaudio==119:
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
	###1FGCA
		elif kaudio==1757:
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='Solid_color']
		elif kaudio==1758:
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='Animal']
		elif kaudio==1759:
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='Thing']
		elif kaudio==1760:
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='Dress']
		elif kaudio==1761:
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='Food']
		elif kaudio==1762:
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='People']
	###1FSUCOD
		elif kaudio==172:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Red','Green']]
		elif kaudio==173:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Red','Yellow']]
		elif kaudio==174:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Red','Blue']]
		elif kaudio==175:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==176:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==177:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==178:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==179:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==180:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==181:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==182:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==183:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==184:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==185:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==186:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==187:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==188:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==189:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==190:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==191:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==192:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==193:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==194:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==195:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==196:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==197:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==198:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==199:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==200:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==201:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==202:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==203:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==204:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==205:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==206:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==207:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==208:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==209:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==210:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==211:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Green','Yellow']]
		elif kaudio==212:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Green','Blue']]
		elif kaudio==213:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==214:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==215:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==216:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==217:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==218:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==219:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==220:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==221:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==222:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==223:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==224:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==225:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==226:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==227:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==228:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==229:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==230:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==231:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==232:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==233:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==234:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==235:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==236:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==237:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==238:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==239:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==240:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==241:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==242:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==243:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==244:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==245:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==246:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==247:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==248:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==249:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Yellow','Blue']]
		elif kaudio==250:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==251:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==252:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==253:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==254:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2
		elif kaudio==255:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2
		elif kaudio==256:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==257:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==258:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==259:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==260:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==261:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==262:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==263:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==264:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==265:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==266:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==267:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==268:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==269:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==270:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==271:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==272:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==273:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==274:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==275:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==276:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==277:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==278:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==279:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==280:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==281:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==282:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==283:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==284:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==285:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==286:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==287:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==288:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==289:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==290:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==291:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==292:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==293:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==294:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==295:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==296:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==297:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==298:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==299:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==300:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==301:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==302:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==303:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==304:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==305:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==306:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==307:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==309:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==310:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==311:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==312:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==313:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==314:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==315:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==316:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==317:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==318:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==319:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==320:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==321:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==322:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Green']]
		elif kaudio==323:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Yellow']]
		elif kaudio==324:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Blue']]
		elif kaudio==325:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==326:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==327:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==328:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==329:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==330:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==331:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==332:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==333:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==334:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==335:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==336:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==337:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==338:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==339:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==340:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==341:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==342:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==343:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2	 				
		elif kaudio==344:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==345:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red ']
			ali = alit1+alit2					
		elif kaudio==346:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green ']
			ali = alit1+alit2					
		elif kaudio==347:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow ']
			ali = alit1+alit2					
		elif kaudio==348:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue ']
			ali = alit1+alit2					
		elif kaudio==349:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==350:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==351:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==352:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==353:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==354:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==355:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==356:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==357:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Green','Yellow']]
		elif kaudio==358:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Green','Blue']]
		elif kaudio==359:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==360:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==361:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrotn' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==362:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrotn' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==363:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==364:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==365:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==366:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==367:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==368:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==369:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==370:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==371:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==372:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==373:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==374:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==375:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==376:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==377:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==378:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==379:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==380:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==381:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==382:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==383:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==384:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==385:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==386:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==387:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==388:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==389:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==390:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==391:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Yellow','Blue']]
		elif kaudio==392:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==393:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==394:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==395:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==396:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==397:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==398:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==399:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==400:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==401:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==402:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==403:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==404:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==405:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==406:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==407:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==408:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==409:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==410:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==411:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==412:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==413:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==414:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==415:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==416:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==417:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==418:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==419:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==420:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==421:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==422:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==423:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==424:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==425:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==426:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==427:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==428:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==429:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==430:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==431:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==432:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==433:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==434:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==435:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==436:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==437:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==438:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==439:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==440:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==441:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==442:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==443:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==444:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==445:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==446:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==447:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==448:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==449:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==450:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==451:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==452:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==453:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==454:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==455:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==456:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Green']]
		elif kaudio==457:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Yellow']]			
		elif kaudio==458:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Blue']]			
		elif kaudio==459:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==460:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==461:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==462:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==463:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==464:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==465:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==466:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==467:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==468:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==469:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==470:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==471:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==472:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==473:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==474:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==475:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==476:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==477:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==478:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==479:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==480:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==481:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==482:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==483:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==484:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==485:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==486:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==487:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc in ['Green','Yellow']]
		elif kaudio==488:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc in ['Green','Blue']]
		elif kaudio==489:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==490:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==491:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==492:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==493:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==494:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==495:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==496:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==497:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==498:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==499:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==500:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==501:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==502:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==503:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==504:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==505:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==506:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==507:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==508:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==509:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==510:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==511:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==512:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==513:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==514:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==515:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==516:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==517:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc in ['Yellow','Blue']]
		elif kaudio==518:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==519:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==520:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==521:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==522:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==523:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==524:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==525:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==526:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==527:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==528:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==529:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==530:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==531:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==532:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==533:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==534:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==535:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==536:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==537:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==538:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==539:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==540:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==541:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==542:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==543:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==544:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==545:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==546:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==547:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==548:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==549:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==550:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==551:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==552:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==553:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==554:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==555:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==556:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==557:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==558:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==559:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==560:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==561:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==562:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==563:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==564:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==565:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==566:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==567:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==568:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==569:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==570:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==571:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==572:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==573:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain2' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==574:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Green']]
		elif kaudio==575:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Yellow']]
		elif kaudio==576:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Blue']]
		elif kaudio==577:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==578:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==579:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==580:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==581:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==582:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==583:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==584:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==585:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==586:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==587:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==588:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==589:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==590:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==591:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==592:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==593:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==594:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==595:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==596:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==597:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==598:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==599:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==600:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==613:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Green','Yellow']]
		elif kaudio==614:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Green','Blue']]
		elif kaudio==615:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==616:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==617:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==618:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==619:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==620:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==621:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==622:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==623:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==624:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==625:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==626:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==627:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==628:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==629:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==630:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==631:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==632:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==633:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==634:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==635:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==636:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==637:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==638:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==639:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in ['Yellow','Blue']]
		elif kaudio==640:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==641:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==642:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==643:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==644:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==645:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==646:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==647:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==648:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==649:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==650:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==651:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==652:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==653:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==654:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==655:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==656:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==657:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==658:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==659:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==660:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==661:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==662:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==663:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==664:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==665:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==666:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==667:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==668:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==669:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==670:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==671:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==672:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==673:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==674:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==675:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==676:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==677:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==678:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==679:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==680:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==681:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==682:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==683:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==684:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==685:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==686:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==687:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==688:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Green']]
		elif kaudio==689:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Yellow']]
		elif kaudio==690:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Blue']]
		elif kaudio==691:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==692:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==693:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==694:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==695:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==696:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==697:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==698:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==699:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==700:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==701:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==702:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==703:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==704:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==705:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==706:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==707:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==708:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==709:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==710:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==711:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc in ['Green','Yellow']]
		elif kaudio==712:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc in ['Green','Blue']]
		elif kaudio==713:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==714:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==715:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==716:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==717:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==718:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==719:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==720:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==721:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==722:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==723:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==724:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==725:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==726:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==727:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==728:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==729:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==730:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==731:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==732:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==733:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc in ['Yellow','Blue']]
		elif kaudio==734:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==735:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==736:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==737:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==738:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==739:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==740:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==741:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==742:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==743:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==744:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==745:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==746:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==747:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==748:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==749:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==750:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==751:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==752:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==753:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==754:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==755:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==756:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==757:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==758:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==759:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==760:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==761:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==762:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==763:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==764:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==765:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==766:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==767:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==768:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==769:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==770:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==771:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==772:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==773:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==774:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc in['Red','Green']]
		elif kaudio==775:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc in['Red','Yellow']]
		elif kaudio==776:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc in['Red','Blue']]			
		elif kaudio==777:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==778:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==779:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==780:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==781:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==782:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==783:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==784:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==785:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==786:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==787:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==788:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==789:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==790:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==791:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==792:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==793:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc in ['Green','Yellow']]
		elif kaudio==794:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc in ['Green','Blue']]
		elif kaudio==795:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==796:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==797:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==798:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==799:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==800:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==801:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==802:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==803:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==804:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==805:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==806:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==807:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==807:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==808:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==809:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==810:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==811:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc in ['Yellow','Blue']]
		elif kaudio==812:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==813:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==814:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==815:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==816:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==817:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==818:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==819:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==820:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==821:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==822:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==823:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==824:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==825:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==826:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==827:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==828:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==829:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==830:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==831:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==832:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==833:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==834:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==835:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==836:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==837:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==838:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==839:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==840:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==841:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==842:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==843:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==844:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc in['Red','Green']]
		elif kaudio==845:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc in['Red','Yellow']]
		elif kaudio==846:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc in['Red','Blue']]
		elif kaudio==847:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==848:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==849:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==850:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==851:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==852:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==853:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==854:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==855:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==856:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==857:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==858:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==859:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc in ['Green','Yellow']]
		elif kaudio==860:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc in ['Green','Blue']]
		elif kaudio==861:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==862:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==863:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==864:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==865:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
		elif kaudio==866:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
		elif kaudio==867:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
		elif kaudio==868:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
		elif kaudio==869:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==870:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==871:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==872:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==873:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc in ['Yellow','Blue']]
		elif kaudio==874:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==875:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==876:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==877:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==878:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==879:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==880:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==881:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==882:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==883:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==884:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==885:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==886:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==887:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==888:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==889:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==890:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==891:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==892:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==893:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==894:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==895:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==896:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio==897:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==898:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Red','Green']]
		elif kaudio==899:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Red','Green']]			
		elif kaudio==900:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Red','Blue']]
		elif kaudio==901:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==902:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==903:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==904:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==905:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==906:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==907:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==908:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==909:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Green','Yellow']]
		elif kaudio==910:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Green','Blue']]
		elif kaudio==911:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==912:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==913:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==914:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==915:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==916:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==917:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==918:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==919:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Yellow','Blue']]
		elif kaudio==920:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==921:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==922:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==923:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==924:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==925:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==926:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==927:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==928:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==929:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==930:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==931:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==932:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==933:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==934:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==935:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio==936:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Red','Green']]
		elif kaudio==937:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Red','Yellow']]
		elif kaudio==938:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Red','Blue']]			
		elif kaudio==939:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==940:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==941:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==942:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==943:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Green','Yellow']]
		elif kaudio==944:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Green','Blue']]
		elif kaudio==945:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==946:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==947:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==948:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==949:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Yellow','Blue']]
			ali = alit1+alit2					
		elif kaudio==950:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==951:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==952:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==953:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==954:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio==955:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio==956:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio==957:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio==958:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Red','Green']]
		elif kaudio==959:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Red','Yellow']]
		elif kaudio==960:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Red','Blue']]
		elif kaudio==961:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Green','Yellow']]
		elif kaudio==962:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Green','Blue']]
		elif kaudio==963:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Yellow','Blue']]
	###1FSUCOS
		elif kaudio==132:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']	
		elif kaudio==133:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']					
		elif kaudio==134:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']					
		elif kaudio==135:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Blue']					
		elif kaudio==136:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']					
		elif kaudio==137:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']					
		elif kaudio==138:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']					
		elif kaudio==139:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']					
		elif kaudio==140:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']					
		elif kaudio==141:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']					
		elif kaudio==142:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']					
		elif kaudio==143:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']					
		elif kaudio==144:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']					
		elif kaudio==145:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']					
		elif kaudio==146:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']					
		elif kaudio==147:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']					
		elif kaudio==148:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']					
		elif kaudio==149:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']					
		elif kaudio==150:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']					
		elif kaudio==151:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']					
		elif kaudio==152:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']					
		elif kaudio==153:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']					
		elif kaudio==154:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']					
		elif kaudio==155:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']					
		elif kaudio==156:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']					
		elif kaudio==157:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']					
		elif kaudio==158:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']					
		elif kaudio==159:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']					
		elif kaudio==160:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']					
		elif kaudio==161:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']					
		elif kaudio==162:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']					
		elif kaudio==163:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']					
		elif kaudio==164:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']					
		elif kaudio==165:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']					
		elif kaudio==166:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']					
		elif kaudio==167:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']					
		elif kaudio==168:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']					
		elif kaudio==169:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']					
		elif kaudio==170:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']					
		elif kaudio==171:
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']					
	###1FSUD
		elif kaudio==15:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']
			ali = alit1+alit2			
		elif kaudio==16:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Yellow']
			ali = alit1+alit2			
		elif kaudio==17:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Blue']
			ali = alit1+alit2			
		elif kaudio==18:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Blue']
			ali = alit1+alit2			
		elif kaudio==19:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Yellow']
			ali = alit1+alit2			
		elif kaudio==20:
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']
			ali = alit1+alit2		
		elif kaudio==21:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','monkey']]
		elif kaudio==22:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','octopus']]
		elif kaudio==23:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','parrot']]
		elif kaudio==24:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','icecream']]
		elif kaudio==25:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','glasses']]
		elif kaudio==26:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','bicycle']]
		elif kaudio==27:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','push_scooter']]
		elif kaudio==28:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','umbrella']]
		elif kaudio==29:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','girl']]
		elif kaudio==30:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','octopus']]
		elif kaudio==31:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','parrot']]
		elif kaudio==32:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','icecream']]
		elif kaudio==33:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','glasses']]
		elif kaudio==34:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','bicycle']]
		elif kaudio==35:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','push_scooter']]
		elif kaudio==36:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','umbrella']]
		elif kaudio==37:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','girl']]
		elif kaudio==38:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','octopus']]
		elif kaudio==39:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','icecream']]
		elif kaudio==40:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','glasses']]
		elif kaudio==41:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','bicycle']]
		elif kaudio==42:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','push_scooter']]
		elif kaudio==43:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','umbrella']]
		elif kaudio==44:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','girl']]
		elif kaudio==45:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','icecream']]
		elif kaudio==46:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','glasses']]
		elif kaudio==47:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','bicycle']]
		elif kaudio==48:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','push_scooter']]
		elif kaudio==49:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','umbrella']]
		elif kaudio==50:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','girl']]
		elif kaudio==51:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','glasses']]
		elif kaudio==52:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['icecream','bicycle']]
		elif kaudio==53:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['icecream','push_scooter']]
		elif kaudio==54:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['icecream','umbrella']]
		elif kaudio==55:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['icecream','girl']]
		elif kaudio==56:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['glasses','bicycle']]
		elif kaudio==57:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['glasses','push_scooter']]
		elif kaudio==58:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['glasses','umbrella']]
		elif kaudio==59:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['glasses','girl']]
		elif kaudio==60:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['bicycle','push_scooter']]
		elif kaudio==61:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['bicycle','umbrella']]
		elif kaudio==62:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['bicycle','girl']]
		elif kaudio==63:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['push_scooter','umbrella']]
		elif kaudio==64:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['push_scooter','girl']]
		elif kaudio==65:
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['umbrella','girl']]
	###1FSUDT
		elif kaudio==66:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('plain','plain','Red','Green',limit,limit1,touch_together)
		elif kaudio==67:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('plain','plain','Red','Yellow',limit,limit1,touch_together)
		elif kaudio==68:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('plain','plain','Red','Blue',limit,limit1,touch_together)
		elif kaudio==69:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('plain','plain','Yellow','Green',limit,limit1,touch_together)
		elif kaudio==70:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('plain','plain','Yellow','Blue',limit,limit1,touch_together)
		elif kaudio==71:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('plain','plain','Blue','Green',limit,limit1,touch_together)
		elif kaudio==72:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('lion','monkey',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==73:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('lion','octopus',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==74:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('lion','parrot',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==75:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('lion','icecream',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==76:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('lion','glasses',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==77:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('lion','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==78:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('lion','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==79:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('lion','umbrella',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==80:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('lion','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==81:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('monkey','octopus',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==82:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('monkey','parrot',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==83:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('monkey','icecream',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==84:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('monkey','glasses',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==85:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('monkey','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==86:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('monkey','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==87:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('monkey','umbrella',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==88:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('monkey','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==89:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('parrot','octopus',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==90:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('parrot','icecream',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==91:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('parrot','glasses',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==92:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('parrot','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==93:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('parrot','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==94:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('parrot','umbrella',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==95:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('parrot','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==96:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('octopus','icecream',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==97:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('octopus','glasses',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==98:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('octopus','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==99:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('octopus','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==100:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('octopus','octopus',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==101:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('octopus','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==102:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('icecream','glasses',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==103:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('icecream','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==104:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('icecream','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==105:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('icecream','umbrella',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==106:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('icecream','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==107:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('glasses','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==108:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('glasses','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==109:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('glasses','umbrella',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==110:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('glasses','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==111:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('bicycle','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio==112:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('bicycle','umbrella',patpat_ele,k,limit,limit1,touch_together)			
		elif kaudio==113:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('push_scooter','umbrella',patpat_ele,k,limit,limit1,touch_together)			
		elif kaudio==114:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('push_scooter','girl',patpat_ele,k,limit,limit1,touch_together)			
		elif kaudio==115:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUDT('umbrella','girl',patpat_ele,k,limit,limit1,touch_together)			
	###1FCODT 
		elif kaudio==126:
			ali,limit,limit1,touch_together = gasm.get_ali_1FCODT('Red','Green',patpat_col,k,limit,limit1,touch_together)
		elif kaudio==127:
			ali,limit,limit1,touch_together = gasm.get_ali_1FCODT('Red','Yellow',patpat_col,k,limit,limit1,touch_together)
		elif kaudio==128:
			ali,limit,limit1,touch_together = gasm.get_ali_1FCODT('Red','Yellow',patpat_col,k,limit,limit1,touch_together)
		elif kaudio==129:
			ali,limit,limit1,touch_together = gasm.get_ali_1FCODT('Green','Yellow',patpat_col,k,limit,limit1,touch_together)
		elif kaudio==130:
			ali,limit,limit1,touch_together = gasm.get_ali_1FCODT('Green','Yellow',patpat_col,k,limit,limit1,touch_together)
		elif kaudio==131:
			ali,limit,limit1,touch_together = gasm.get_ali_1FCODT('Blue','Yellow',patpat_col,k,limit,limit1,touch_together)
	####1FSUCODT
		elif kaudio==964:
			ali,lim,lim1,ttogether = gasm.get_ali_1FSUCODT('monkey','monkey','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==965:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','monkey','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==966:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','monkey','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==967:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==968:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==969:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==970:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==971:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==972:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==973:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==974:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==975:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==976:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==977:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==978:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==979:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==980:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==981:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==982:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==983:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==984:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==985:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==986:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==987:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==988:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==989:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==990:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==991:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==992:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==993:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==994:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==995:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==996:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==997:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==998:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==999:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1000:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1001:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1002:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1003:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','monkey','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1004:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','monkey','Green','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1005:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1006:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1007:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1008:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Green','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1009:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1010:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1011:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1012:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Green','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1013:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1014:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1015:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1016:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Green','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1017:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1018:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1019:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1020:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Green','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1021:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1022:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1023:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1024:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1025:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1026:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1027:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1028:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1029:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1030:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1031:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1032:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1033:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1034:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1035:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1036:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1037:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1038:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1039:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1040:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1041:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','monkey','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1042:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1043:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1044:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1045:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1046:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1047:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1048:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1049:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1050:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1051:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1052:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1053:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1054:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1055:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1056:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1057:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1058:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1059:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1060:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1061:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1062:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1063:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1064:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1065:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1066:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1067:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1068:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1069:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1070:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1071:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1072:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1073:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1074:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1075:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1076:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1077:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1078:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1079:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1080:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1081:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','lion','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1082:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1083:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1084:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1085:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','parrot','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1086:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1087:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1088:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1089:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','octopus','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1090:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1091:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1092:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1093:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1094:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1095:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1096:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1097:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1098:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1099:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1100:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1101:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1102:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1103:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1104:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1105:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1106:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1107:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1108:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1109:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1110:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1111:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1112:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1113:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('monkey','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio==1114:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','lion','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1115:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','lion','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1116:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','lion','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1117:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1118:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1119:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1120:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1121:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1122:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1123:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1124:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1125:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1126:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1127:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1128:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1129:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1130:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1131:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1132:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1133:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1134:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1135:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1136:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1137:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1138:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1139:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1140:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1141:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1142:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1143:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1144:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1145:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1146:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1147:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1148:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1149:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','lion','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1150:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','lion','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1151:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1152:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1153:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1154:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1155:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1156:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1157:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1158:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1159:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1160:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1161:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1162:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1163:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1164:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1165:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1166:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1167:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1168:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1169:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1170:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1171:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1172:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1173:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1174:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1175:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1176:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1177:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1178:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1179:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1180:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1181:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1182:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1183:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','lion','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1184:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1185:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1186:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1187:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1188:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1189:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1190:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1191:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1192:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1193:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1194:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1195:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1196:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1197:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1198:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1199:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1200:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1201:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1202:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1203:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1204:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1205:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1206:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1207:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1208:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1209:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1210:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1211:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1212:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1213:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1214:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1215:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1216:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1217:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1218:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1219:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1220:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1221:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1222:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1223:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','octopus','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1224:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1225:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1226:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1227:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1228:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1229:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1230:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1231:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1232:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1233:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1234:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1235:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1236:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1237:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1238:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1239:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1240:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1241:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1242:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1243:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1244:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1245:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1246:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1247:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('lion','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio==1248:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','parrot','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1249:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1250:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1251:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1252:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1253:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1254:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1255:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1256:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1257:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1258:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1259:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1260:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1261:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1262:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1263:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1264:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1265:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1266:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1267:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1268:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1269:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1270:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1271:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1272:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1273:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1274:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1275:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1276:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1277:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1278:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1279:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1280:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1281:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1282:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1283:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1284:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1285:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1286:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1287:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1288:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1289:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1290:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1291:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1292:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1293:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1294:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1295:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1296:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1297:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1298:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1299:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1300:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1301:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1302:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1303:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1304:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1305:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1306:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1307:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1308:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1309:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1310:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1311:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1312:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1313:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1314:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1315:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1316:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1317:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1318:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1319:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1320:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1321:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1322:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1323:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1324:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1325:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1326:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1327:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1328:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1329:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1330:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1331:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1332:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1333:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1334:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1335:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1336:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1337:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1338:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1339:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1340:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1341:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','octopus','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1342:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1343:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1344:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1345:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1346:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1347:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1348:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1349:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1350:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1351:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1352:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1353:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1354:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1355:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1356:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1357:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1358:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1359:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1360:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1361:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1362:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1363:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1364:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1365:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('parrot','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio==1366:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1367:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1368:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1369:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1370:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1371:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1372:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1373:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1374:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1375:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1376:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1377:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1378:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1379:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1380:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1381:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1382:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1383:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1384:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1385:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1386:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1387:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1388:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1389:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1390:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1391:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1392:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio==1393:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1394:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1395:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1396:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','monkey','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1397:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','monkey','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1398:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','monkey','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1399:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','monkey','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1400:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','lion','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1401:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','lion','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1402:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','lion','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1403:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','lion','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1404:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1405:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1406:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1407:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		
		elif kaudio==1408:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1409:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1410:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1411:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1412:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1413:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1414:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1415:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1416:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1417:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1418:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1419:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1420:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1421:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1422:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1423:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1424:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1425:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1426:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1427:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1428:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1429:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1430:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1431:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1432:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1433:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1434:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1435:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1436:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1437:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1438:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1439:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1440:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1441:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1442:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1443:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1444:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1445:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1446:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1447:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1448:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1449:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1450:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1451:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1452:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1453:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1454:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1455:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1456:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1457:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1458:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1459:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1460:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1461:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1462:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1463:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1464:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1465:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1466:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1467:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1468:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1469:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1470:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1471:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1472:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1473:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1474:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1475:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1476:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1477:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1478:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1479:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1480:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('octopus','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio==1481:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','icecream','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1482:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1483:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1484:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1485:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1486:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1487:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1488:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1489:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1490:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1491:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1492:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1493:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1494:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1495:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1496:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1497:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1498:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1499:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1500:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1501:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1502:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1503:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1504:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1505:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1506:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1507:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1508:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1509:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1510:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1511:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1512:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1513:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1514:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1515:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1516:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1517:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1518:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1519:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1520:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1521:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1522:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1523:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1524:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1525:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1526:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1527:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1528:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1529:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1530:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1531:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1532:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1533:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1534:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1535:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1536:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1537:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1538:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1539:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1540:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1541:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1542:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1543:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1544:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1545:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1546:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1547:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1548:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1549:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1550:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1551:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1552:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1553:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1554:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1555:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1556:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1557:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1558:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1559:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1560:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1561:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1562:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1563:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1564:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1565:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1566:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('icecream','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio==1567:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1568:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1569:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1570:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1571:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1572:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1573:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1574:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1575:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1576:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1577:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1578:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1579:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1580:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1581:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1582:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1583:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1584:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1585:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1586:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1587:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1588:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1589:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1590:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1591:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1592:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1593:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1594:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1595:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1596:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1597:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1598:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1599:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1600:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1601:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1602:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1603:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1604:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1605:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1606:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1607:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1608:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1609:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1610:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1611:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1612:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1613:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1614:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1615:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1616:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1617:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1618:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1619:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1620:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1621:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1622:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1623:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1624:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1625:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1626:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1627:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1628:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1629:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1630:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1631:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1632:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1633:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1634:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1635:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1636:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('glasses','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio==1637:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1638:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1639:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1640:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1641:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1642:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1643:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1644:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1645:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1646:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1647:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1648:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1649:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1650:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1651:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1652:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1653:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1654:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1655:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1656:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1657:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1658:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1659:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1660:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1661:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1662:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1663:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1664:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1665:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1666:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1667:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1668:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1669:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1670:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1671:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1672:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1673:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1674:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1675:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1676:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1677:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1678:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1679:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1680:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1681:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1682:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1683:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1684:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1685:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1686:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1687:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1688:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1689:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1690:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('bicycle','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio==1691:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1692:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1693:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1694:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1695:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1696:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1697:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1698:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1699:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1700:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1701:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1702:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1703:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1704:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1705:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1706:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1707:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1708:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1709:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1710:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1711:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)	
		elif kaudio==1712:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1713:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1714:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1715:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1716:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1717:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1718:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1719:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1720:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1721:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1722:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1723:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1724:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1725:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1726:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1727:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1728:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('push_scooter','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1729:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1730:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1731:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1732:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1733:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1734:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1735:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1736:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1737:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1738:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1739:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1740:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1741:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1742:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1743:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1744:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1745:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1746:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1747:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1748:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1749:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1750:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('umbrella','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1751:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('girl','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1752:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('girl','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1753:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('girl','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1754:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('girl','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1755:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('girl','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio==1756:
			ali,limit,limit1,touch_together = gasm.get_ali_1FSUCODT('girl','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
	
		if len(ali_1) or len(tot_1):
			tot.append(ali)
			if all_alternative_pos[k] in (0,1):
				tot_1.append(ali) #not ali_1
			else:
				tot_1.append(ali_1)
		else:
			print("partial_ali {}".format(ali))
			tot.append(ali)
		ali=[]
		ali_1=[]

	every_characteristics = []
	every_characteristics.append(tot)
	every_characteristics.append(tot_1)
	every_characteristics.append(all_alternative_pos)
	every_characteristics.append(one_ans_is_enough)
	every_characteristics.append(multiple_branch_ans)
	every_characteristics.append(multiple_or_ans)
	every_characteristics.append(touch_together)
	every_characteristics.append(no_ans_to_create_only_body)
	every_characteristics.append(body_response)
	every_characteristics.append(body_and_patch)
	every_characteristics.append(need_light)
	every_characteristics.append(need_light1)
	every_characteristics.append(need_light2)
	every_characteristics.append(need_light3)
	every_characteristics.append(the_first)
	every_characteristics.append(the_second)
	every_characteristics.append(body_num_press)
	every_characteristics.append(all_num_touches_to_do)
	every_characteristics.append(ques_9_num)
	every_characteristics.append(limit)
	every_characteristics.append(limit1)
	every_characteristics.append(limit2)
	every_characteristics.append(limit3)
	every_characteristics.append(limitRed)
	every_characteristics.append(limitGreen)
	every_characteristics.append(limitBlue)
	every_characteristics.append(limitYellow)	
	every_characteristics.append(limitLion)
	every_characteristics.append(limitMonkey)
	every_characteristics.append(limitParrot)
	every_characteristics.append(limitOctopus)
	
	return partial(every_characteristics)