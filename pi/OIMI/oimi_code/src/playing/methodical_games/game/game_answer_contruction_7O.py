import sys,threading
sys.setrecursionlimit(10**7) # max depth of recursion
threading.stack_size(2**27)
import faulthandler; faulthandler.enable()
############## KIND 7O
import game_answer_contruction_7O_1 as gac_1
import game_answer_contruction_7O_2 as gac_2
import game_answer_contruction_7O_3 as gac_3
import game_answer_contruction_7O_4 as gac_4
def build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,listatypes,same_col,same_ele):
	ali,ali_tmp,ali_1,tot,tot_1 = [],[],[],[],[]
	all_alternative_pos=[0,0,0,0]
	one_ans_is_enough=[0,0,0,0]
	multiple_branch_ans=[0,0,0,0]
	multiple_or_ans=[0,0,0,0]
	touch_together=[0,0,0,0]
	no_ans_to_create_only_body=[0,0,0,0]
	body_response=[0,0,0,0]
	body_and_patch=[0,0,0,0]
	need_light=[0,0,0,0]
	need_light1=[0,0,0,0]
	need_light2=[0,0,0,0]
	need_light3=[0,0,0,0]
	the_first=[0,0,0,0]
	the_second=[0,0,0,0]
	body_num_press=[0,0,0,0]
	all_num_touches_to_do=[0,0,0,0]
	ques_9_num = [0,0,0,0]
	limit = [0,0,0,0]
	limit1 = [0,0,0,0]
	limit2 = [0,0,0,0]
	limit3 = [0,0,0,0]
	limitRed = [0,0,0,0]
	limitGreen = [0,0,0,0]
	limitBlue = [0,0,0,0]
	limitYellow  = [0,0,0,0]
	limitLion = [0,0,0,0]
	limitMonkey = [0,0,0,0]
	limitParrot = [0,0,0,0]
	limitOctopus = [0,0,0,0]
	for k,kaudio in enumerate(listaudios):
		if listatypes[k]=='7OL2' or listatypes[k]=='7OL3':
			ali_7,nlight,nlight1,lim,lim1,lim2=gac_1.build_Resp7_1(patpat_ele,patpat_col,kaudio)
			print("tott received 7O_1  {}".format(ali_7))
			ali = ali_7
			need_light[k]=nlight
			need_light1[k]=nlight1
			limit[k]=lim 
			limit1[k]=lim1
			limit2[k]=lim2
			print("TOT AFTER APPEND received 7O_1  {}".format(ali))
		if listatypes[k]=='7OSU2':
			ali_7,one_enough,lim,lim1=gac_2.build_Resp7_2(patpat_ele,kaudio)
			print("tott received 7O_2  {}".format(ali_7))
			print("one_ans_is_enough received 7O_2  {}".format(one_enough))
			ali = ali_7
			limit[k]=lim 
			limit1[k]=lim1
			one_ans_is_enough[k]=one_enough
			print("TOT AFTER APPEND received 7O_2 {}".format(ali))
		if listatypes[k]=='7OSU3':
			ali_7,one_enough,lim,lim1,lim2=gac_3.build_Resp7_3(patpat_ele,kaudio)
			print("tott received 7O_3  {}".format(ali_7))
			print("one_ans_is_enough received 7O_3  {}".format(one_enough))
			ali = ali_7
			limit[k]=lim 
			limit1[k]=lim1
			limit2[k]=lim2
			one_ans_is_enough[k]=one_enough
			print("TOT AFTER APPEND received 7O_3 {}".format(ali))
		if listatypes[k]=='7OSU4':
			ali_7,one_enough=gac_4.build_Resp7_4(patpat_ele,kaudio)
			print("tott received 7O_4  {}".format(ali_7))
			print("one_ans_is_enough received 7O_4  {}".format(one_enough))
			ali = ali_7
			one_ans_is_enough[k]=one_enough
			print("TOT AFTER APPEND received 7O_4 {}".format(ali))
	###7OCO2
		if kaudio=='audio_8901':
			alir = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit[k]=len(ali)
			alig = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit1[k]=len(ali)
			ali = alir+alig
		elif kaudio=='audio_8902':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit1[k]=len(ali2)
			ali = ali1+ali2
		elif kaudio=='audio_8903':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit1[k]=len(ali2)
			ali = ali1+ali2
		elif kaudio=='audio_8904':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit1[k]=len(ali2)
			ali = ali1+ali2
		elif kaudio=='audio_8905':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit1[k]=len(ali2)
			ali = ali1+ali2
		elif kaudio=='audio_8906':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit1[k]=len(ali2)
			ali = ali1+ali2
		elif kaudio=='audio_8907':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit1[k]=len(ali2)
			ali = ali1+ali2
		elif kaudio=='audio_8908':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit1[k]=len(ali2)
			ali = ali1+ali2
		elif kaudio=='audio_8909':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit1[k]=len(ali2)
			ali = ali1+ali2
		elif kaudio=='audio_8910':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit1[k]=len(ali2)
			ali = ali1+ali2
		elif kaudio=='audio_8911':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit1[k]=len(ali2)
			ali = ali1+ali2
		elif kaudio=='audio_8912':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit1[k]=len(ali2)
			ali = ali1+ali2
	###7OCO3
		if kaudio=='audio_8913':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8914':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8915':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8916':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8917':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8918':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8919':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8920':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8921':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8922':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8923':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8924':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8925':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8926':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8927':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8928':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8929':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8930':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8931':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8932':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8933':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8934':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8935':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
		elif kaudio=='audio_8936':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			limit[k]=len(ali1)
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			limit1[k]=len(ali2)
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			limit2[k]=len(ali3)
			ali = ali1+ali2+ali3
	###7OCO4
		elif kaudio=='audio_8937':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8938':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8939':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8940':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8941':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8942':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8943':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8944':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8945':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8946':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8947':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8948':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8949':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8950':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8951':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8952':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8953':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8954':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8955':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8956':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8957':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8958':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8959':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali = ali1+ali2+ali3+ali4
		elif kaudio=='audio_8960':
			ali1 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
			ali2 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
			ali3 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
			ali4 = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
			ali = ali1+ali2+ali3+ali4	
		
		if len(ali_1) or len(tot_1):
			tot.append(ali)
			if all_alternative_pos[k] in (0,1):
				tot_1.append(ali) #not ali_1
			else:
				tot_1.append(ali_1)
		else:
			print("partial_ali {}".format(ali))
			tot.append(ali)
		ali=[]
		ali_1=[]

	all_properties = []
	all_properties.append(tot)
	all_properties.append(tot_1)
	all_properties.append(all_alternative_pos)
	all_properties.append(one_ans_is_enough)
	all_properties.append(multiple_branch_ans)
	all_properties.append(multiple_or_ans)
	all_properties.append(touch_together)
	all_properties.append(no_ans_to_create_only_body)
	all_properties.append(body_response)
	all_properties.append(body_and_patch)
	all_properties.append(need_light)
	all_properties.append(need_light1)
	all_properties.append(need_light2)
	all_properties.append(need_light3)
	all_properties.append(the_first)
	all_properties.append(the_second)
	all_properties.append(body_num_press)
	all_properties.append(all_num_touches_to_do)
	all_properties.append(ques_9_num)
	all_properties.append(limitRed)
	all_properties.append(limitGreen)
	all_properties.append(limitBlue)
	all_properties.append(limitYellow)	
	all_properties.append(limit)
	all_properties.append(limit1)
	all_properties.append(limit2)
	all_properties.append(limit3)
	all_properties.append(limitLion)
	all_properties.append(limitMonkey)
	all_properties.append(limitParrot)
	all_properties.append(limitOctopus)
	return all_properties