############## KIND 4P
import faulthandler; faulthandler.enable()
def build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,same_col,same_ele):
	print("WE ARE IN KIND 4P")
	ali,ali_tmp,ali_1,tot,tot_1 = [],[],[],[],[]
	all_alternative_pos=[0,0,0,0]
	one_ans_is_enough=[0,0,0,0]
	multiple_branch_ans=[0,0,0,0]
	multiple_or_ans=[0,0,0,0]
	touch_together=[0,0,0,0]
	no_ans_to_create_only_body=[0,0,0,0]
	body_response=[0,0,0,0]
	body_and_patch=[0,0,0,0]
	need_light=[0,0,0,0]
	need_light1=[0,0,0,0]
	need_light2=[0,0,0,0]
	need_light3=[0,0,0,0]
	the_first=[0,0,0,0]
	the_second=[0,0,0,0]
	body_num_press=[0,0,0,0]
	all_num_touches_to_do=[0,0,0,0]
	ques_9_num = [0,0,0,0]
	limit = [0,0,0,0]
	limit1 = [0,0,0,0]
	limit2 = [0,0,0,0]
	limit3 = [0,0,0,0]
	limitRed = [0,0,0,0]
	limitGreen = [0,0,0,0]
	limitBlue = [0,0,0,0]
	limitYellow  = [0,0,0,0]
	limitLion = [0,0,0,0]
	limitMonkey = [0,0,0,0]
	limitParrot = [0,0,0,0]
	limitOctopus = [0,0,0,0]
	for kaudio in listaudios:
		###4PSU
		if kaudio=='audio_3013':
			if patpat_ele[0]=='lion' and same_ele['lion']>=2:
				if patpat_ele[1]=='lion':
					ali.append(2)
				if patpat_ele[2]=='lion':
					ali.append(3)
				if patpat_ele[3]=='lion':
					ali.append(4)				
			if patpat_ele[0]=='monkey' and same_ele['monkey']>=2:
				if patpat_ele[1]=='monkey':
					ali.append(2)
				if patpat_ele[2]=='monkey':
					ali.append(3)
				if patpat_ele[3]=='monkey':
					ali.append(4)				
			if patpat_ele[0]=='octopus' and same_ele['octopus']>=2:
				if patpat_ele[1]=='octopus':
					ali.append(2)
				if patpat_ele[2]=='octopus':
					ali.append(3)
				if patpat_ele[3]=='octopus':
					ali.append(4)	
			if patpat_ele[0]=='parrot' and same_ele['parrot']>=2:
				if patpat_ele[1]=='parrot':
					ali.append(2)
				if patpat_ele[2]=='parrot':
					ali.append(3)
				if patpat_ele[3]=='parrot':
					ali.append(4)	
			if patpat_ele[0]=='girl' and same_ele['girl']>=2:
				if patpat_ele[1]=='girl':
					ali.append(2)
				if patpat_ele[2]=='girl':
					ali.append(3)
				if patpat_ele[3]=='girl':
					ali.append(4)	
			if patpat_ele[0]=='glasses' and same_ele['glasses']>=2:
				if patpat_ele[1]=='glasses':
					ali.append(2)
				if patpat_ele[2]=='glasses':
					ali.append(3)
				if patpat_ele[3]=='glasses':
					ali.append(4)	
			if patpat_ele[0]=='bicycle' and same_ele['bicycle']>=2:
				if patpat_ele[1]=='bicycle':
					ali.append(2)
				if patpat_ele[2]=='bicycle':
					ali.append(3)
				if patpat_ele[3]=='bicycle':
					ali.append(4)	
			if patpat_ele[0]=='push_scooter' and same_ele['push_scooter']>=2:
				if patpat_ele[1]=='push_scooter':
					ali.append(2)
				if patpat_ele[2]=='push_scooter':
					ali.append(3)
				if patpat_ele[3]=='push_scooter':
					ali.append(4)				
			if patpat_ele[0]=='umbrella' and same_ele['umbrella']>=2:
				if patpat_ele[1]=='umbrella':
					ali.append(2)
				if patpat_ele[2]=='umbrella':
					ali.append(3)
				if patpat_ele[3]=='umbrella':
					ali.append(4)
		elif kaudio=='audio_3014':
			if patpat_ele[1]=='lion' and same_ele['lion']>=2:
				if patpat_ele[0]=='lion':
					ali.append(1)
				if patpat_ele[2]=='lion':
					ali.append(3)
				if patpat_ele[3]=='lion':
					ali.append(4)
			if patpat_ele[1]=='monkey' and same_ele['monkey']>=2:
				if patpat_ele[0]=='monkey':
					ali.append(1)
				if patpat_ele[2]=='monkey':
					ali.append(3)
				if patpat_ele[3]=='monkey':
					ali.append(4)
			if patpat_ele[1]=='octopus' and same_ele['octopus']>=2:
				if patpat_ele[0]=='octopus':
					ali.append(1)
				if patpat_ele[2]=='octopus':
					ali.append(3)
				if patpat_ele[3]=='octopus':
					ali.append(4)
			if patpat_ele[1]=='parrot' and same_ele['parrot']>=2:
				if patpat_ele[0]=='parrot':
					ali.append(1)
				if patpat_ele[2]=='parrot':
					ali.append(3)
				if patpat_ele[3]=='parrot':
					ali.append(4)
			if patpat_ele[1]=='girl' and same_ele['girl']>=2:
				if patpat_ele[0]=='girl':
					ali.append(1)
				if patpat_ele[2]=='girl':
					ali.append(3)
				if patpat_ele[3]=='girl':
					ali.append(4)
			if patpat_ele[1]=='glasses' and same_ele['glasses']>=2:
				if patpat_ele[0]=='glasses':
					ali.append(1)
				if patpat_ele[2]=='glasses':
					ali.append(3)
				if patpat_ele[3]=='glasses':
					ali.append(4)
			if patpat_ele[1]=='bicycle' and same_ele['bicycle']>=2:
				if patpat_ele[0]=='bicycle':
					ali.append(1)
				if patpat_ele[2]=='bicycle':
					ali.append(3)
				if patpat_ele[3]=='bicycle':
					ali.append(4)
			if patpat_ele[1]=='push_scooter' and same_ele['push_scooter']>=2:
				if patpat_ele[0]=='push_scooter':
					ali.append(1)
				if patpat_ele[2]=='push_scooter':
					ali.append(3)
				if patpat_ele[3]=='push_scooter':
					ali.append(4)			
			if patpat_ele[1]=='umbrella' and same_ele['umbrella']>=2:
				if patpat_ele[0]=='umbrella':
					ali.append(1)
				if patpat_ele[2]=='umbrella':
					ali.append(3)
				if patpat_ele[3]=='umbrella':
					ali.append(4)
		elif kaudio=='audio_3015':
			if patpat_ele[2]=='lion' and same_ele['lion']>=2:
				if patpat_ele[0]=='lion':
					ali.append(1)
				if patpat_ele[1]=='lion':
					ali.append(2)
				if patpat_ele[3]=='lion':
					ali.append(4)
			if patpat_ele[2]=='monkey' and same_ele['monkey']>=2:
				if patpat_ele[0]=='monkey':
					ali.append(1)
				if patpat_ele[1]=='monkey':
					ali.append(2)
				if patpat_ele[3]=='monkey':
					ali.append(4)
			if patpat_ele[2]=='octopus' and same_ele['octopus']>=2:
				if patpat_ele[0]=='octopus':
					ali.append(1)
				if patpat_ele[1]=='octopus':
					ali.append(2)
				if patpat_ele[3]=='octopus':
					ali.append(4)
			if patpat_ele[2]=='parrot' and same_ele['parrot']>=2:
				if patpat_ele[0]=='parrot':
					ali.append(1)
				if patpat_ele[1]=='parrot':
					ali.append(2)
				if patpat_ele[3]=='parrot':
					ali.append(4)
			if patpat_ele[2]=='girl' and same_ele['girl']>=2:
				if patpat_ele[0]=='girl':
					ali.append(1)
				if patpat_ele[1]=='girl':
					ali.append(2)
				if patpat_ele[3]=='girl':
					ali.append(4)
			if patpat_ele[2]=='glasses' and same_ele['glasses']>=2:
				if patpat_ele[0]=='glasses':
					ali.append(1)
				if patpat_ele[1]=='glasses':
					ali.append(2)
				if patpat_ele[3]=='glasses':
					ali.append(4)
			if patpat_ele[2]=='bicycle' and same_ele['bicycle']>=2:
				if patpat_ele[0]=='bicycle':
					ali.append(1)
				if patpat_ele[1]=='bicycle':
					ali.append(2)
				if patpat_ele[3]=='bicycle':
					ali.append(4)
			if patpat_ele[2]=='push_scooter' and same_ele['push_scooter']>=2:
				if patpat_ele[0]=='push_scooter':
					ali.append(1)
				if patpat_ele[1]=='push_scooter':
					ali.append(2)
				if patpat_ele[3]=='push_scooter':
					ali.append(4)
			if patpat_ele[2]=='umbrella' and same_ele['umbrella']>=2:
				if patpat_ele[0]=='umbrella':
					ali.append(1)
				if patpat_ele[1]=='umbrella':
					ali.append(2)
				if patpat_ele[3]=='umbrella':
					ali.append(4)
		elif kaudio=='audio_3016':
			if patpat_ele[3]=='lion' and same_ele['lion']>=2:
				if patpat_ele[0]=='lion':
					ali.append(1)
				if patpat_ele[1]=='lion':
					ali.append(2)
				if patpat_ele[2]=='lion':
					ali.append(3)
			if patpat_ele[3]=='monkey' and same_ele['monkey']>=2:
				if patpat_ele[0]=='monkey':
					ali.append(1)
				if patpat_ele[1]=='monkey':
					ali.append(2)
				if patpat_ele[2]=='monkey':
					ali.append(3)
			if patpat_ele[3]=='octopus' and same_ele['octopus']>=2:
				if patpat_ele[0]=='octopus':
					ali.append(1)
				if patpat_ele[1]=='octopus':
					ali.append(2)
				if patpat_ele[2]=='octopus':
					ali.append(3)
			if patpat_ele[3]=='parrot' and same_ele['parrot']>=2:
				if patpat_ele[0]=='parrot':
					ali.append(1)
				if patpat_ele[1]=='parrot':
					ali.append(2)
				if patpat_ele[2]=='parrot':
					ali.append(3)	
			if patpat_ele[3]=='girl' and same_ele['girl']>=2:
				if patpat_ele[0]=='girl':
					ali.append(1)
				if patpat_ele[1]=='girl':
					ali.append(2)
				if patpat_ele[2]=='girl':
					ali.append(3)
			if patpat_ele[3]=='glasses' and same_ele['glasses']>=2:
				if patpat_ele[0]=='glasses':
					ali.append(1)
				if patpat_ele[1]=='glasses':
					ali.append(2)
				if patpat_ele[2]=='glasses':
					ali.append(3)
			if patpat_ele[3]=='bicycle' and same_ele['bicycle']>=2:
				if patpat_ele[0]=='bicycle':
					ali.append(1)
				if patpat_ele[1]=='bicycle':
					ali.append(2)
				if patpat_ele[2]=='bicycle':
					ali.append(3)
			if patpat_ele[3]=='push_scooter' and same_ele['push_scooter']>=2:
				if patpat_ele[0]=='push_scooter':
					ali.append(1)
				if patpat_ele[1]=='push_scooter':
					ali.append(2)
				if patpat_ele[2]=='push_scooter':
					ali.append(3)
			if patpat_ele[3]=='umbrella' and same_ele['umbrella']>=2:
				if patpat_ele[0]=='umbrella':
					ali.append(1)
				if patpat_ele[1]=='umbrella':
					ali.append(2)
				if patpat_ele[2]=='umbrella':
					ali.append(3)					
		###4PCO
		elif kaudio=='audio_3017':
			if patpat_col[0]=='Red' and same_col['Red']>=2:
				if patpat_col[1]=='Red':
					ali.append(2)
				if patpat_col[2]=='Red':
					ali.append(3)
				if patpat_col[3]=='Red':
					ali.append(4)
			if patpat_col[0]=='Green' and same_col['Green']>=2:
				if patpat_col[1]=='Green':
					ali.append(2)
				if patpat_col[2]=='Green':
					ali.append(3)
				if patpat_col[3]=='Green':
					ali.append(4)
			if patpat_col[0]=='Blue' and same_col['Blue']>=2:
				if patpat_col[1]=='Blue':
					ali.append(2)
				if patpat_col[2]=='Blue':
					ali.append(3)
				if patpat_col[3]=='Blue':
					ali.append(4)
			if patpat_col[0]=='Yellow' and same_col['Yellow']>=2:
				if patpat_col[1]=='Yellow':
					ali.append(2)
				if patpat_col[2]=='Yellow':
					ali.append(3)
				if patpat_col[3]=='Yellow':
					ali.append(4)
		elif kaudio=='audio_3018':
			if patpat_col[2]=='Red' and same_col['Red']>=2:
				if patpat_col[0]=='Red':
					ali.append(1)
				if patpat_col[1]=='Red':
					ali.append(2)
				if patpat_col[3]=='Red':
					ali.append(4)
			if patpat_col[2]=='Green' and same_col['Green']>=2:
				if patpat_col[0]=='Green':
					ali.append(1)
				if patpat_col[1]=='Green':
					ali.append(2)
				if patpat_col[3]=='Green':
					ali.append(4)
			if patpat_col[2]=='Blue' and same_col['Blue']>=2:
				if patpat_col[0]=='Blue':
					ali.append(1)
				if patpat_col[1]=='Blue':
					ali.append(2)
				if patpat_col[3]=='Blue':
					ali.append(4)
			if patpat_col[2]=='Yellow' and same_col['Yellow']>=2:
				if patpat_col[0]=='Yellow':
					ali.append(1)
				if patpat_col[1]=='Yellow':
					ali.append(2)
				if patpat_col[3]=='Yellow':
					ali.append(4)	
		elif kaudio=='audio_3019':
			if patpat_col[1]=='Red' and same_col['Red']>=2:
				if patpat_col[0]=='Red':
					ali.append(1)
				if patpat_col[2]=='Red':
					ali.append(3)
				if patpat_col[3]=='Red':
					ali.append(4)
			if patpat_col[1]=='Green' and same_col['Green']>=2:
				if patpat_col[0]=='Green':
					ali.append(1)
				if patpat_col[2]=='Green':
					ali.append(3)
				if patpat_col[3]=='Green':
					ali.append(4)
			if patpat_col[1]=='Blue' and same_col['Blue']>=2:
				if patpat_col[0]=='Blue':
					ali.append(1)
				if patpat_col[2]=='Blue':
					ali.append(3)
				if patpat_col[3]=='Blue':
					ali.append(4)
			if patpat_col[1]=='Yellow' and same_col['Yellow']>=2:
				if patpat_col[0]=='Yellow':
					ali.append(1)
				if patpat_col[2]=='Yellow':
					ali.append(3)
				if patpat_col[3]=='Yellow':
					ali.append(4)	
		elif kaudio=='audio_3020':
			if patpat_col[3]=='Red' and same_col['Red']>=2:
				if patpat_col[0]=='Red':
					ali.append(1)
				if patpat_col[1]=='Red':
					ali.append(2)
				if patpat_col[2]=='Red':
					ali.append(3)
			if patpat_col[3]=='Green' and same_col['Green']>=2:
				if patpat_col[0]=='Green':
					ali.append(1)
				if patpat_col[1]=='Green':
					ali.append(2)
				if patpat_col[2]=='Green':
					ali.append(3)
			if patpat_col[3]=='Blue' and same_col['Blue']>=2:
				if patpat_col[0]=='Blue':
					ali.append(1)
				if patpat_col[1]=='Blue':
					ali.append(2)
				if patpat_col[2]=='Blue':
					ali.append(3)
			if patpat_col[3]=='Yellow' and same_col['Yellow']>=2:
				if patpat_col[0]=='Yellow':
					ali.append(1)
				if patpat_col[1]=='Yellow':
					ali.append(2)
				if patpat_col[2]=='Yellow':
					ali.append(3)
		if len(ali_1) or len(tot_1):
			tot.append(ali)
			if all_alternative_pos[k] in (0,1):
				tot_1.append(ali) #not ali_1
			else:
				tot_1.append(ali_1)
		else:
			print("partial_ali {}".format(ali))
			tot.append(ali)
		ali=[]
		ali_1=[]
	print("all_alternative_pos={}".format(all_alternative_pos))
	print("tot --> {}".format(tot))
	print("tot_1 --> {}".format(tot_1))

	all_properties = []
	all_properties.append(tot)
	all_properties.append(tot_1)
	all_properties.append(all_alternative_pos)
	all_properties.append(one_ans_is_enough)
	all_properties.append(multiple_branch_ans)
	all_properties.append(multiple_or_ans)
	all_properties.append(touch_together)
	all_properties.append(no_ans_to_create_only_body)
	all_properties.append(body_response)
	all_properties.append(body_and_patch)
	all_properties.append(need_light)
	all_properties.append(need_light1)
	all_properties.append(need_light2)
	all_properties.append(need_light3)
	all_properties.append(the_first)
	all_properties.append(the_second)
	all_properties.append(body_num_press)
	all_properties.append(all_num_touches_to_do)
	all_properties.append(ques_9_num)
	all_properties.append(limit)
	all_properties.append(limit1)
	all_properties.append(limit2)
	all_properties.append(limit3)
	all_properties.append(limitRed)
	all_properties.append(limitGreen)
	all_properties.append(limitBlue)
	all_properties.append(limitYellow)
	all_properties.append(limitLion)
	all_properties.append(limitMonkey)
	all_properties.append(limitParrot)
	all_properties.append(limitOctopus)
	return all_properties
