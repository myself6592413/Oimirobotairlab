############## KIND 1F
def get_ali_1FCODT(pc1,pc2,patpat_col,kidx,limit,limit1,touch_together):
	print("ENTRO IN GET ALI get_ali_1FCODT")
	alitmp1 = [i+1 for i,element in enumerate(patpat_col) if element==pc1]
	lim=len(alitmp1)
	alitmp2 = [i+1 for i,element in enumerate(patpat_col) if element==pc2]
	lim1=len(alitmp2)
	ali = alitmp1+alitmp2
	limit[kidx]=lim
	limit1[kidx]=lim1
	touch_together[kidx]=1
	return ali,limit,limit1,touch_together

def get_ali_1FSUDT(pe1,pe2,patpat_ele,kidx,limit,limit1,touch_together):
	print("ENTRO IN GET ALI get_ali_1FSUDT")
	alitmp1 = [i+1 for i,element in enumerate(patpat_ele) if element==pe1]
	limit=len(alitmp1)
	alitmp2 = [i+1 for i,element in enumerate(patpat_ele) if element==pe2]
	limit1=len(alitmp2)
	ali = alitmp1+alitmp2
	limit[kidx]=lim
	limit1[kidx]=lim1
	touch_together[kidx]=1
	return ali,limit,limit1,touch_together

def get_ali_1FSUCODT(pe1,pe2,pc1,pc2,patpat_ele,patpat_col,kidx,limit,limit1,touch_together):
	print("ENTRO IN GET ALI get_ali_1FSUCODT")
	alimp1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe==pe1 and pc==pc1]
	lim=len(alimp1)
	alimp2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe==pe2 and pc==pc2]
	lim1=len(alimp2)
	ali = alimp1+alimp2
	limit[kidx]=lim
	limit1[kidx]=lim1
	touch_together[kidx]=1
	return ali,limit,limit1,touch_together

def build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,same_col,same_ele):
	print("WE ARE IN KIND 1F")
	ali,ali_tmp,ali_1,tot,tot_1 = [],[],[],[],[]
	all_alternative_pos=[0,0,0,0]
	one_ans_is_enough=[0,0,0,0]
	multiple_branch_ans=[0,0,0,0]
	multiple_or_ans=[0,0,0,0]
	touch_together=[0,0,0,0]
	no_ans_to_create_only_body=[0,0,0,0]
	body_response=[0,0,0,0]
	body_and_patch=[0,0,0,0]
	need_light=[0,0,0,0]
	need_light1=[0,0,0,0]
	need_light2=[0,0,0,0]
	need_light3=[0,0,0,0]
	the_first=[0,0,0,0]
	the_second=[0,0,0,0]
	body_num_press=[0,0,0,0]
	all_num_touches_to_do=[0,0,0,0]
	ques_9_num = [0,0,0,0]
	limit = [0,0,0,0]
	limit1 = [0,0,0,0]
	limit2 = [0,0,0,0]
	limit3 = [0,0,0,0]
	limitRed = [0,0,0,0]
	limitGreen = [0,0,0,0]
	limitBlue = [0,0,0,0]
	limitYellow  = [0,0,0,0]
	limitLion = [0,0,0,0]
	limitMonkey = [0,0,0,0]
	limitParrot = [0,0,0,0]
	limitOctopus = [0,0,0,0]
	for k,kaudio in enumerate(listaudios):
	###1FSUS 
		if kaudio=='audio_1':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']					
		elif kaudio=='audio_2':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']					
		elif kaudio=='audio_3':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Blue']					
		elif kaudio=='audio_4':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Yellow']					
		elif kaudio=='audio_5':
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
		elif kaudio=='audio_6':
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
		elif kaudio=='audio_7':
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
		elif kaudio=='audio_8':
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
		elif kaudio=='audio_9':
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
		elif kaudio=='audio_10':
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
		elif kaudio=='audio_11':
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
		elif kaudio=='audio_12':
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
		elif kaudio=='audio_13':
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
		elif kaudio=='audio_14':
			ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
	###1FCACO
		elif kaudio=='audio_1763':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Red']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1764':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Green']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1765':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Yellow']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1766':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Blue']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1767':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Red']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1768':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Green']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1769':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Yellow']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1770':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Blue']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1771':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Red']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1772':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Green']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1773':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Yellow']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1774':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Blue']
			one_ans_is_enough[k]=1						
		elif kaudio=='audio_1775':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Red']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1776':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Green']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1777':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Yellow']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1778':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Blue']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1779':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Red']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1780':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Green']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1781':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Yellow']
			one_ans_is_enough[k]=1
		elif kaudio=='audio_1782':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Blue']
			one_ans_is_enough[k]=1
	###1FCACOA
		elif kaudio=='audio_1783':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Red']
		elif kaudio=='audio_1784':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Green']
		elif kaudio=='audio_1785':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Yellow']
		elif kaudio=='audio_1786':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Animal' and pc=='Blue']
		elif kaudio=='audio_1787':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Red']
		elif kaudio=='audio_1788':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Green']
		elif kaudio=='audio_1789':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Yellow']
		elif kaudio=='audio_1790':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Thing' and pc=='Blue']
		elif kaudio=='audio_1791':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Red']
		elif kaudio=='audio_1792':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Green']
		elif kaudio=='audio_1793':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Yellow']
		elif kaudio=='audio_1794':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Food' and pc=='Blue']
		elif kaudio=='audio_1795':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Red']
		elif kaudio=='audio_1796':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Green']
		elif kaudio=='audio_1797':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Yellow']
		elif kaudio=='audio_1798':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='Dress' and pc=='Blue']
		elif kaudio=='audio_1799':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Red']
		elif kaudio=='audio_1800':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Green']
		elif kaudio=='audio_1801':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Yellow']
		elif kaudio=='audio_1802':
			ali = [i+1 for i, (pg, pc) in enumerate(zip(patpat_gen,patpat_col)) if pg=='People' and pc=='Blue']
	###1FCOD
		elif kaudio=='audio_120':
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Red','Green']]
		elif kaudio=='audio_121':
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Red','Yellow']]
		elif kaudio=='audio_122':
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Red','Blue']]
		elif kaudio=='audio_123':
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Green','Yellow']]
		elif kaudio=='audio_124':
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Green','Blue']]
		elif kaudio=='audio_125':
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo in ['Blue','Yellow']]	
	###1FCOS
		elif kaudio=='audio_116':
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']					
		elif kaudio=='audio_117':
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
		elif kaudio=='audio_118':
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
		elif kaudio=='audio_119':
			ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
	###1FGCA
		elif kaudio=='audio_1757':
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='Solid_color']
		elif kaudio=='audio_1758':
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='Animal']
		elif kaudio=='audio_1759':
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='Thing']
		elif kaudio=='audio_1760':
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='Dress']
		elif kaudio=='audio_1761':
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='Food']
		elif kaudio=='audio_1762':
			ali = [i+1 for i,gn in enumerate(patpat_gen) if gn=='People']
	###1FSUCOD
		elif kaudio=='audio_172':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Red','Green']]
		elif kaudio=='audio_173':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Red','Yellow']]
		elif kaudio=='audio_174':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Red','Blue']]
		elif kaudio=='audio_175':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_176':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_177':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_178':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_179':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_180':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_181':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_182':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_183':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_184':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_185':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_186':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_187':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_188':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_189':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_190':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_191':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_192':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_193':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_194':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_195':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_196':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_197':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_198':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_199':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_200':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_201':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_202':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_203':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_204':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_205':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_206':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_207':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_208':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_209':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_210':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_211':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Green','Yellow']]
		elif kaudio=='audio_212':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Green','Blue']]
		elif kaudio=='audio_213':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_214':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_215':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_216':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_217':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_218':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_219':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_220':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_221':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_222':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_223':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_224':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_225':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_226':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_227':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_228':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_229':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_230':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_231':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_232':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_233':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_234':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_235':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_236':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_237':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_238':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_239':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_240':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_241':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_242':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_243':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_244':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_245':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_246':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_247':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_248':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_249':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Yellow','Blue']]
		elif kaudio=='audio_250':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_251':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_252':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_253':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_254':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2
		elif kaudio=='audio_255':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2
		elif kaudio=='audio_256':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_257':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_258':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_259':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_260':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_261':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_262':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_263':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_264':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_265':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_266':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_267':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_268':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_269':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_270':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_271':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_272':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_273':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_274':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_275':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_276':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_277':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_278':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_279':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_280':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_281':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_282':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_283':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_284':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_285':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_286':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_287':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_288':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_289':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_290':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_291':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_292':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_293':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_294':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_295':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_296':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_297':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_298':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_299':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_300':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_301':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_302':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_303':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_304':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_305':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_306':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_307':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_309':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_310':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_311':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_312':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_313':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_314':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_315':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_316':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_317':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_318':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_319':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_320':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_321':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_322':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Green']]
		elif kaudio=='audio_323':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Yellow']]
		elif kaudio=='audio_324':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Blue']]
		elif kaudio=='audio_325':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_326':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_327':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_328':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_329':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_330':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_331':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_332':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_333':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_334':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_335':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_336':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_337':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_338':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_339':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_340':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_341':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_342':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_343':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2	 				
		elif kaudio=='audio_344':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_345':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red ']
			ali = alit1+alit2					
		elif kaudio=='audio_346':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green ']
			ali = alit1+alit2					
		elif kaudio=='audio_347':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow ']
			ali = alit1+alit2					
		elif kaudio=='audio_348':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue ']
			ali = alit1+alit2					
		elif kaudio=='audio_349':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_350':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_351':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_352':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_353':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_354':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_355':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_356':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_357':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Green','Yellow']]
		elif kaudio=='audio_358':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Green','Blue']]
		elif kaudio=='audio_359':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_360':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_361':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrotn' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_362':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrotn' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_363':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_364':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_365':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_366':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_367':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_368':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_369':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_370':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_371':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_372':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_373':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_374':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_375':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_376':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_377':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_378':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_379':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_380':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_381':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_382':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_383':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_384':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_385':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_386':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_387':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_388':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_389':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_390':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_391':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc in ['Yellow','Blue']]
		elif kaudio=='audio_392':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_393':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_394':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_395':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_396':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_397':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_398':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_399':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_400':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_401':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_402':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_403':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_404':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_405':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_406':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_407':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_408':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_409':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_410':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_411':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_412':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_413':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_414':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_415':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_416':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_417':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_418':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_419':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_420':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_421':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_422':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_423':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_424':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_425':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_426':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_427':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_428':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_429':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_430':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_431':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_432':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_433':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_434':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_435':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_436':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_437':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_438':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_439':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_440':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_441':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_442':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_443':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_444':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_445':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_446':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_447':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_448':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_449':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_450':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_451':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_452':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_453':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_454':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_455':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_456':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Green']]
		elif kaudio=='audio_457':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Yellow']]			
		elif kaudio=='audio_458':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc in['Red','Blue']]			
		elif kaudio=='audio_459':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_460':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_461':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_462':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_463':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_464':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_465':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_466':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_467':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_468':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_469':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_470':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_471':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_472':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_473':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_474':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_475':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_476':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_477':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_478':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_479':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_480':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_481':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_482':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_483':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_484':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_485':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_486':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_487':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc in ['Green','Yellow']]
		elif kaudio=='audio_488':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc in ['Green','Blue']]
		elif kaudio=='audio_489':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_490':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_491':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_492':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_493':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_494':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_495':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_496':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_497':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_498':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_499':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_500':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_501':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_502':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_503':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_504':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_505':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_506':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_507':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_508':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_509':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_510':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_511':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_512':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_513':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_514':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_515':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_516':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_517':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc in ['Yellow','Blue']]
		elif kaudio=='audio_518':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_519':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_520':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_521':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_522':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_523':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_524':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_525':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_526':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_527':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_528':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_529':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_530':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_531':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_532':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_533':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_534':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_535':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_536':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_537':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_538':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_539':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_540':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_541':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_542':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_543':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_544':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_545':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_546':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_547':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_548':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_549':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_550':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_551':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_552':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_553':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_554':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_555':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_556':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_557':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_558':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_559':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_560':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_561':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_562':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_563':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_564':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_565':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_566':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_567':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_568':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_569':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_570':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_571':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_572':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_573':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain2' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_574':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Green']]
		elif kaudio=='audio_575':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Yellow']]
		elif kaudio=='audio_576':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Blue']]
		elif kaudio=='audio_577':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_578':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_579':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_580':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_581':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_582':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_583':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_584':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_585':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_586':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_587':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_588':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_589':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_590':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_591':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_592':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_593':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_594':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_595':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_596':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_597':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_598':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_599':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_600':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_613':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Green','Yellow']]
		elif kaudio=='audio_614':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Green','Blue']]
		elif kaudio=='audio_615':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_616':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_617':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_618':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_619':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_620':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_621':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_622':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_623':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_624':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_625':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_626':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_627':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_628':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_629':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_630':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_631':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_632':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_633':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_634':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_635':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_636':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_637':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_638':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_639':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in ['Yellow','Blue']]
		elif kaudio=='audio_640':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_641':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_642':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_643':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_644':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_645':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_646':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_647':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_648':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_649':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_650':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_651':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_652':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_653':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_654':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_655':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_656':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_657':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_658':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_659':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_660':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_661':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_662':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_663':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_664':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_665':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_666':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_667':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_668':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_669':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_670':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_671':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_672':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_673':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_674':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_675':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_676':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_677':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_678':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_679':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_680':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_681':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_682':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_683':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_684':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_685':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_686':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_687':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_688':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Green']]
		elif kaudio=='audio_689':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Yellow']]
		elif kaudio=='audio_690':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc in['Red','Blue']]
		elif kaudio=='audio_691':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_692':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_693':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_694':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_695':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_696':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_697':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_698':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_699':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_700':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_701':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_702':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_703':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_704':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_705':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_706':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_707':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_708':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_709':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_710':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_711':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc in ['Green','Yellow']]
		elif kaudio=='audio_712':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc in ['Green','Blue']]
		elif kaudio=='audio_713':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_714':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_715':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_716':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_717':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_718':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_719':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_720':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_721':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_722':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_723':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_724':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_725':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_726':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_727':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_728':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_729':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_730':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_731':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_732':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_733':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc in ['Yellow','Blue']]
		elif kaudio=='audio_734':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_735':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_736':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_737':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_738':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_739':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_740':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_741':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_742':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_743':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_744':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_745':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_746':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_747':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_748':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_749':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_750':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_751':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_752':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_753':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_754':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_755':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_756':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_757':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_758':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_759':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_760':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_761':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_762':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_763':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_764':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_765':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_766':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_767':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_768':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_769':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_770':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_771':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_772':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_773':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_774':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc in['Red','Green']]
		elif kaudio=='audio_775':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc in['Red','Yellow']]
		elif kaudio=='audio_776':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc in['Red','Blue']]			
		elif kaudio=='audio_777':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_778':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_779':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_780':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_781':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_782':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_783':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_784':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_785':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_786':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_787':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_788':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_789':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_790':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_791':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_792':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_793':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc in ['Green','Yellow']]
		elif kaudio=='audio_794':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc in ['Green','Blue']]
		elif kaudio=='audio_795':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_796':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_797':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_798':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_799':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_800':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_801':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_802':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_803':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_804':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_805':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_806':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_807':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_807':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_808':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_809':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_810':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_811':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc in ['Yellow','Blue']]
		elif kaudio=='audio_812':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_813':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_814':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_815':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_816':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_817':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_818':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_819':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_820':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_821':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_822':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_823':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_824':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_825':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_826':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_827':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_828':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_829':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_830':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_831':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_832':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_833':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_834':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_835':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_836':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_837':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_838':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_839':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_840':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_841':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_842':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_843':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_844':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc in['Red','Green']]
		elif kaudio=='audio_845':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc in['Red','Yellow']]
		elif kaudio=='audio_846':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc in['Red','Blue']]
		elif kaudio=='audio_847':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_848':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_849':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_850':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_851':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_852':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_853':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_854':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_855':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_856':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_857':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_858':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_859':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc in ['Green','Yellow']]
		elif kaudio=='audio_860':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc in ['Green','Blue']]
		elif kaudio=='audio_861':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_862':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_863':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_864':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_865':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
		elif kaudio=='audio_866':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
		elif kaudio=='audio_867':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
		elif kaudio=='audio_868':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali = alit1+alit2					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
		elif kaudio=='audio_869':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_870':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_871':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_872':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_873':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc in ['Yellow','Blue']]
		elif kaudio=='audio_874':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_875':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_876':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_877':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_878':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_879':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_880':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_881':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_882':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_883':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_884':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_885':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_886':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_887':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_888':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_889':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_890':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_891':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_892':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_893':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_894':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_895':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_896':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2
		elif kaudio=='audio_897':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_898':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Red','Green']]
		elif kaudio=='audio_899':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Red','Green']]			
		elif kaudio=='audio_900':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Red','Blue']]
		elif kaudio=='audio_901':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_902':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_903':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_904':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_905':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_906':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_907':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_908':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_909':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Green','Yellow']]
		elif kaudio=='audio_910':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Green','Blue']]
		elif kaudio=='audio_911':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_912':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_913':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_914':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_915':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_916':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_917':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_918':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_919':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc in['Yellow','Blue']]
		elif kaudio=='audio_920':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_921':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_922':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_923':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_924':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_925':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_926':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_927':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_928':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_929':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_930':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_931':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_932':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_933':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_934':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_935':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2
		elif kaudio=='audio_936':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Red','Green']]
		elif kaudio=='audio_937':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Red','Yellow']]
		elif kaudio=='audio_938':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Red','Blue']]			
		elif kaudio=='audio_939':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_940':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_941':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_942':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_943':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Green','Yellow']]
		elif kaudio=='audio_944':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Green','Blue']]
		elif kaudio=='audio_945':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_946':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_947':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_948':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_949':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc in['Yellow','Blue']]
			ali = alit1+alit2					
		elif kaudio=='audio_950':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_951':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_952':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_953':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_954':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali = alit1+alit2					
		elif kaudio=='audio_955':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali = alit1+alit2					
		elif kaudio=='audio_956':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali = alit1+alit2					
		elif kaudio=='audio_957':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali = alit1+alit2					
		elif kaudio=='audio_958':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Red','Green']]
		elif kaudio=='audio_959':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Red','Yellow']]
		elif kaudio=='audio_960':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Red','Blue']]
		elif kaudio=='audio_961':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Green','Yellow']]
		elif kaudio=='audio_962':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Green','Blue']]
		elif kaudio=='audio_963':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc in['Yellow','Blue']]
	###1FSUCOS
		elif kaudio=='audio_132':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']	
		elif kaudio=='audio_133':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']					
		elif kaudio=='audio_134':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']					
		elif kaudio=='audio_135':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Blue']					
		elif kaudio=='audio_136':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']					
		elif kaudio=='audio_137':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']					
		elif kaudio=='audio_138':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']					
		elif kaudio=='audio_139':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']					
		elif kaudio=='audio_140':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']					
		elif kaudio=='audio_141':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']					
		elif kaudio=='audio_142':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']					
		elif kaudio=='audio_143':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']					
		elif kaudio=='audio_144':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']					
		elif kaudio=='audio_145':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']					
		elif kaudio=='audio_146':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']					
		elif kaudio=='audio_147':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']					
		elif kaudio=='audio_148':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']					
		elif kaudio=='audio_149':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']					
		elif kaudio=='audio_150':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']					
		elif kaudio=='audio_151':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']					
		elif kaudio=='audio_152':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']					
		elif kaudio=='audio_153':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']					
		elif kaudio=='audio_154':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']					
		elif kaudio=='audio_155':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']					
		elif kaudio=='audio_156':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']					
		elif kaudio=='audio_157':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']					
		elif kaudio=='audio_158':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']					
		elif kaudio=='audio_159':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']					
		elif kaudio=='audio_160':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']					
		elif kaudio=='audio_161':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']					
		elif kaudio=='audio_162':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']					
		elif kaudio=='audio_163':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']					
		elif kaudio=='audio_164':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']					
		elif kaudio=='audio_165':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']					
		elif kaudio=='audio_166':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']					
		elif kaudio=='audio_167':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']					
		elif kaudio=='audio_168':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']					
		elif kaudio=='audio_169':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']					
		elif kaudio=='audio_170':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']					
		elif kaudio=='audio_171':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']					
	###1FSUD
		elif kaudio=='audio_15':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']
			ali = alit1+alit2			
		elif kaudio=='audio_16':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Yellow']
			ali = alit1+alit2			
		elif kaudio=='audio_17':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Blue']
			ali = alit1+alit2			
		elif kaudio=='audio_18':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Blue']
			ali = alit1+alit2			
		elif kaudio=='audio_19':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Yellow']
			ali = alit1+alit2			
		elif kaudio=='audio_20':
			alit1 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']					
			alit2 = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']
			ali = alit1+alit2		
		elif kaudio=='audio_21':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','monkey']]
		elif kaudio=='audio_22':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','octopus']]
		elif kaudio=='audio_23':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','parrot']]
		elif kaudio=='audio_24':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','icecream']]
		elif kaudio=='audio_25':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','glasses']]
		elif kaudio=='audio_26':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','bicycle']]
		elif kaudio=='audio_27':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','push_scooter']]
		elif kaudio=='audio_28':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','umbrella']]
		elif kaudio=='audio_29':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['lion','girl']]
		elif kaudio=='audio_30':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','octopus']]
		elif kaudio=='audio_31':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','parrot']]
		elif kaudio=='audio_32':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','icecream']]
		elif kaudio=='audio_33':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','glasses']]
		elif kaudio=='audio_34':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','bicycle']]
		elif kaudio=='audio_35':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','push_scooter']]
		elif kaudio=='audio_36':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','umbrella']]
		elif kaudio=='audio_37':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['monkey','girl']]
		elif kaudio=='audio_38':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','octopus']]
		elif kaudio=='audio_39':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','icecream']]
		elif kaudio=='audio_40':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','glasses']]
		elif kaudio=='audio_41':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','bicycle']]
		elif kaudio=='audio_42':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','push_scooter']]
		elif kaudio=='audio_43':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','umbrella']]
		elif kaudio=='audio_44':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['parrot','girl']]
		elif kaudio=='audio_45':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','icecream']]
		elif kaudio=='audio_46':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','glasses']]
		elif kaudio=='audio_47':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','bicycle']]
		elif kaudio=='audio_48':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','push_scooter']]
		elif kaudio=='audio_49':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','umbrella']]
		elif kaudio=='audio_50':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','girl']]
		elif kaudio=='audio_51':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['octopus','glasses']]
		elif kaudio=='audio_52':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['icecream','bicycle']]
		elif kaudio=='audio_53':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['icecream','push_scooter']]
		elif kaudio=='audio_54':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['icecream','umbrella']]
		elif kaudio=='audio_55':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['icecream','girl']]
		elif kaudio=='audio_56':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['glasses','bicycle']]
		elif kaudio=='audio_57':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['glasses','push_scooter']]
		elif kaudio=='audio_58':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['glasses','umbrella']]
		elif kaudio=='audio_59':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['glasses','girl']]
		elif kaudio=='audio_60':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['bicycle','push_scooter']]
		elif kaudio=='audio_61':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['bicycle','umbrella']]
		elif kaudio=='audio_62':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['bicycle','girl']]
		elif kaudio=='audio_63':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['push_scooter','umbrella']]
		elif kaudio=='audio_64':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['push_scooter','girl']]
		elif kaudio=='audio_65':
			ali = [i+1 for i,ele in enumerate(patpat_ele) if ele in ['umbrella','girl']]

	###1FSUDT
		elif kaudio=='audio_66':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('plain','plain','Red','Green',limit,limit1,touch_together)
		elif kaudio=='audio_67':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('plain','plain','Red','Yellow',limit,limit1,touch_together)
		elif kaudio=='audio_68':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('plain','plain','Red','Blue',limit,limit1,touch_together)
		elif kaudio=='audio_69':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('plain','plain','Yellow','Green',limit,limit1,touch_together)
		elif kaudio=='audio_70':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('plain','plain','Yellow','Blue',limit,limit1,touch_together)
		elif kaudio=='audio_71':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('plain','plain','Blue','Green',limit,limit1,touch_together)
		elif kaudio=='audio_72':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('lion','monkey',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_73':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('lion','octopus',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_74':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('lion','parrot',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_75':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('lion','icecream',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_76':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('lion','glasses',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_77':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('lion','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_78':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('lion','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_79':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('lion','umbrella',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_80':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('lion','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_81':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('monkey','octopus',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_82':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('monkey','parrot',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_83':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('monkey','icecream',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_84':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('monkey','glasses',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_85':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('monkey','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_86':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('monkey','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_87':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('monkey','umbrella',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_88':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('monkey','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_89':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('parrot','octopus',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_90':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('parrot','icecream',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_91':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('parrot','glasses',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_92':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('parrot','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_93':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('parrot','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_94':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('parrot','umbrella',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_95':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('parrot','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_96':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('octopus','icecream',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_97':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('octopus','glasses',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_98':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('octopus','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_99':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('octopus','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_100':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('octopus','octopus',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_101':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('octopus','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_102':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('icecream','glasses',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_103':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('icecream','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_104':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('icecream','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_105':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('icecream','umbrella',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_106':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('icecream','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_107':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('glasses','bicycle',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_108':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('glasses','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_109':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('glasses','umbrella',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_110':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('glasses','girl',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_111':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('bicycle','push_scooter',patpat_ele,k,limit,limit1,touch_together)
		elif kaudio=='audio_112':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('bicycle','umbrella',patpat_ele,k,limit,limit1,touch_together)			
		elif kaudio=='audio_113':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('push_scooter','umbrella',patpat_ele,k,limit,limit1,touch_together)			
		elif kaudio=='audio_114':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('push_scooter','girl',patpat_ele,k,limit,limit1,touch_together)			
		elif kaudio=='audio_115':
			ali,limit,limit1,touch_together = get_ali_1FSUDT('umbrella','girl',patpat_ele,k,limit,limit1,touch_together)			
	###1FCODT 
		elif kaudio=='audio_126':
			ali,limit,limit1,touch_together = get_ali_1FCODT('Red','Green',patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_127':
			ali,limit,limit1,touch_together = get_ali_1FCODT('Red','Yellow',patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_128':
			ali,limit,limit1,touch_together = get_ali_1FCODT('Red','Yellow',patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_129':
			ali,limit,limit1,touch_together = get_ali_1FCODT('Green','Yellow',patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_130':
			ali,limit,limit1,touch_together = get_ali_1FCODT('Green','Yellow',patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_131':
			ali,limit,limit1,touch_together = get_ali_1FCODT('Blue','Yellow',patpat_col,k,limit,limit1,touch_together)

	####1FSUCODT
		elif kaudio=='audio_964':
			ali,lim,lim1,ttogether = get_ali_1FSUCODT('monkey','monkey','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_965':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','monkey','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_966':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','monkey','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_967':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_968':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_969':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_970':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_971':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_972':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_973':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_974':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_975':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_976':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_977':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_978':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_979':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_980':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_981':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_982':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_983':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_984':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_985':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_986':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_987':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_988':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_989':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_990':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_991':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_992':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_993':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_994':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_995':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_996':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_997':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_998':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_999':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1000':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1001':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1002':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Red','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1003':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','monkey','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1004':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','monkey','Green','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1005':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1006':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1007':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1008':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Green','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1009':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1010':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1011':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1012':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Green','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1013':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1014':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1015':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1016':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Green','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1017':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1018':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1019':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1020':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Green','Blue',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1021':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1022':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1023':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1024':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1025':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1026':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1027':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1028':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1029':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1030':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1031':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1032':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1033':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1034':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1035':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1036':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1037':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1038':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1039':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1040':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1041':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','monkey','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1042':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1043':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1044':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1045':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1046':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1047':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1048':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1049':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1050':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1051':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1052':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1053':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1054':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1055':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1056':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1057':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1058':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1059':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1060':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1061':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1062':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1063':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1064':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1065':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1066':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1067':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1068':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1069':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1070':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1071':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1072':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1073':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1074':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1075':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1076':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1077':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1078':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1079':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1080':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1081':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','lion','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1082':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1083':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1084':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1085':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','parrot','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1086':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1087':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1088':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1089':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','octopus','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1090':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1091':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1092':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1093':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1094':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1095':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1096':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1097':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1098':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1099':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1100':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1101':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1102':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1103':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1104':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1105':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1106':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1107':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1108':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1109':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1110':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1111':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1112':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1113':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('monkey','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio=='audio_1114':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','lion','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1115':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','lion','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1116':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','lion','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1117':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1118':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1119':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1120':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1121':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1122':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1123':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1124':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1125':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1126':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1127':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1128':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1129':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1130':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1131':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1132':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1133':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1134':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1135':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1136':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1137':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1138':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1139':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1140':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1141':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1142':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1143':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1144':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1145':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1146':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1147':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1148':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1149':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','lion','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1150':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','lion','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1151':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1152':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1153':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1154':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1155':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1156':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1157':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1158':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1159':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1160':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1161':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1162':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1163':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1164':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1165':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1166':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1167':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1168':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1169':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1170':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1171':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1172':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1173':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1174':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1175':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1176':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1177':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1178':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1179':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1180':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1181':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1182':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1183':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','lion','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1184':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1185':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1186':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1187':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1188':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1189':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1190':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1191':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1192':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1193':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1194':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1195':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1196':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1197':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1198':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1199':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1200':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1201':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1202':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1203':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1204':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1205':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1206':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1207':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1208':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1209':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1210':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1211':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1212':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1213':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1214':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1215':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1216':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1217':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1218':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1219':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1220':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1221':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1222':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1223':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','octopus','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1224':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1225':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1226':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1227':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1228':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1229':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1230':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1231':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1232':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1233':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1234':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1235':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1236':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1237':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1238':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1239':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1240':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1241':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1242':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1243':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1244':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1245':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1246':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1247':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('lion','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio=='audio_1248':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','parrot','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1249':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1250':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1251':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1252':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1253':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1254':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1255':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1256':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1257':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1258':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1259':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1260':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1261':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1262':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1263':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1264':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1265':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1266':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1267':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1268':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1269':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1270':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1271':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1272':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1273':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1274':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1275':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1276':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1277':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1278':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1279':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1280':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1281':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1282':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1283':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1284':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1285':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1286':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1287':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1288':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1289':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1290':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1291':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1292':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1293':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1294':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1295':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1296':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1297':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1298':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1299':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1300':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1301':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1302':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1303':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1304':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1305':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1306':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1307':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1308':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1309':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1310':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1311':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1312':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1313':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1314':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1315':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1316':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1317':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1318':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1319':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1320':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1321':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1322':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1323':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1324':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1325':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1326':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1327':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1328':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1329':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1330':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1331':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1332':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1333':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1334':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1335':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1336':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1337':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1338':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1339':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1340':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1341':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','octopus','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1342':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1343':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1344':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1345':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1346':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1347':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1348':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1349':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1350':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1351':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1352':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1353':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1354':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1355':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1356':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1357':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1358':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1359':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1360':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1361':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1362':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1363':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1364':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1365':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('parrot','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio=='audio_1366':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1367':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1368':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1369':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1370':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1371':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1372':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1373':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1374':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1375':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1376':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1377':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1378':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1379':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1380':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1381':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1382':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1383':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1384':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1385':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1386':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1387':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1388':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1389':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1390':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1391':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1392':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio=='audio_1393':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1394':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1395':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1396':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','monkey','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1397':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','monkey','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1398':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','monkey','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1399':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','monkey','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1400':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','lion','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1401':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','lion','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1402':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','lion','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1403':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','lion','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1404':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1405':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1406':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1407':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		
		elif kaudio=='audio_1408':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1409':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1410':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1411':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1412':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1413':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1414':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1415':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1416':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1417':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1418':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1419':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1420':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1421':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1422':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1423':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1424':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1425':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1426':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1427':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1428':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1429':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1430':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1431':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1432':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','parrot','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1433':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1434':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1435':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1436':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1437':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1438':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1439':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1440':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1441':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1442':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1443':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1444':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1445':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1446':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1447':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1448':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1449':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1450':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1451':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1452':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1453':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1454':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1455':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1456':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1457':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1458':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1459':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1460':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','icecream','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1461':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1462':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1463':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1464':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1465':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1466':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1467':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1468':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1469':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1470':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1471':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1472':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1473':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1474':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1475':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1476':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1477':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1478':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1479':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1480':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('octopus','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio=='audio_1481':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','icecream','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1482':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1483':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','icecream','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1484':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1485':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1486':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1487':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1488':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1489':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1490':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1491':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1492':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1493':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1494':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1495':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1496':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1497':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1498':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1499':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1500':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1501':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1502':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1503':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1504':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1505':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','icecream','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1506':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1507':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1508':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1509':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1510':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1511':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1512':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1513':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1514':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1515':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1516':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1517':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1518':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1519':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1520':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1521':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1522':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1523':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1524':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1525':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1526':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','icecream','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1527':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1528':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1529':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1530':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1531':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1532':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1533':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1534':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1535':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1536':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1537':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1538':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1539':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1540':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1541':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1542':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1543':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1544':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1545':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1546':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1547':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1548':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1549':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1550':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','glasses','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1551':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1552':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1553':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1554':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1555':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1556':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1557':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1558':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1559':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1560':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1561':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1562':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1563':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1564':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1565':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1566':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('icecream','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio=='audio_1567':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','glasses','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1568':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1569':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','glasses','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1570':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1571':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1572':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1573':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1574':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1575':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1576':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1577':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1578':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1579':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1580':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1581':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1582':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1583':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1584':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1585':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1586':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1587':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','glasses','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1588':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1589':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1590':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1591':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1592':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1593':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1594':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1595':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1596':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1597':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1598':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1599':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1600':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1601':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1602':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1603':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1604':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','glasses','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1605':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1606':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1607':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1608':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1609':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1610':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1611':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1612':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1613':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1614':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1615':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1616':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1617':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1618':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1619':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1620':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1621':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1622':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1623':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1624':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','bicycle','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1625':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1626':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1627':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1628':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1629':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1630':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1631':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1632':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1633':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1634':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1635':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1636':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('glasses','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio=='audio_1637':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','bicycle','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1638':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1639':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','bicycle','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1640':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1641':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1642':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1643':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1644':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1645':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1646':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1647':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1648':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1649':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1650':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1651':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1652':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1653':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','bicycle','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1654':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1655':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1656':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1657':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1658':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1659':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1660':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1661':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1662':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1663':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1664':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1665':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1666':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','bicycle','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1667':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1668':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1669':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1670':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1671':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1672':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1673':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1674':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1675':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1676':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1677':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1678':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1679':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1680':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1681':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1682':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','push_scooter','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1683':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1684':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1685':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1686':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1687':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1688':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1689':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1690':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('bicycle','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)

		elif kaudio=='audio_1691':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','push_scooter','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1692':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1693':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','push_scooter','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1694':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1695':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1696':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1697':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1698':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1699':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1700':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1701':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1702':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1703':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','push_scooter','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1704':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1705':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1706':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1707':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1708':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1709':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1710':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1711':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)	
		elif kaudio=='audio_1712':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','push_scooter','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1713':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1714':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1715':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1716':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1717':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1718':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1719':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1720':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1721':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1722':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1723':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1724':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','umbrella','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1725':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1726':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1727':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1728':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('push_scooter','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1729':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','umbrella','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1730':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1731':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','umbrella','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1732':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Red','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1733':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1734':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1735':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1736':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1737':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','umbrella','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1738':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Green','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1739':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Green','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1740':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1741':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1742':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','umbrella','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1743':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Yellow','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1744':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Yellow','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1745':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1746':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1747':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Blue','Red',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1748':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Blue','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1749':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1750':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('umbrella','girl','Blue','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1751':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('girl','girl','Red','Green',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1752':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('girl','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1753':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('girl','girl','Red','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1754':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('girl','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1755':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('girl','girl','Green','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
		elif kaudio=='audio_1756':
			ali,limit,limit1,touch_together = get_ali_1FSUCODT('girl','girl','Yellow','Yellow',patpat_ele,patpat_col,k,limit,limit1,touch_together)
	
		if len(ali_1) or len(tot_1):
			tot.append(ali)
			if all_alternative_pos[k] in (0,1):
				tot_1.append(ali) #not ali_1
			else:
				tot_1.append(ali_1)
		else:
			print("partial_ali {}".format(ali))
			tot.append(ali)
		ali=[]
		ali_1=[]

	all_properties = []
	all_properties.append(tot)
	all_properties.append(tot_1)
	all_properties.append(all_alternative_pos)
	all_properties.append(one_ans_is_enough)
	all_properties.append(multiple_branch_ans)
	all_properties.append(multiple_or_ans)
	all_properties.append(touch_together)
	all_properties.append(no_ans_to_create_only_body)
	all_properties.append(body_response)
	all_properties.append(body_and_patch)
	all_properties.append(need_light)
	all_properties.append(need_light1)
	all_properties.append(need_light2)
	all_properties.append(need_light3)
	all_properties.append(the_first)
	all_properties.append(the_second)
	all_properties.append(body_num_press)
	all_properties.append(all_num_touches_to_do)
	all_properties.append(ques_9_num)
	all_properties.append(limit)
	all_properties.append(limit1)
	all_properties.append(limit2)
	all_properties.append(limit3)
	all_properties.append(limitRed)
	all_properties.append(limitGreen)
	all_properties.append(limitBlue)
	all_properties.append(limitYellow)	
	all_properties.append(limitLion)
	all_properties.append(limitMonkey)
	all_properties.append(limitParrot)
	all_properties.append(limitOctopus)
	return all_properties