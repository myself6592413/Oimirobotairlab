############## KIND 9C
import faulthandler; faulthandler.enable()
def build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,same_col,same_ele):
	print("WE ARE IN KIND 9C")
	ali,ali_tmp,ali_1,tot,tot_1 = [],[],[],[],[]
	all_alternative_pos=[0,0,0,0]
	one_ans_is_enough=[0,0,0,0]
	multiple_branch_ans=[0,0,0,0]
	multiple_or_ans=[0,0,0,0]
	touch_together=[0,0,0,0]
	no_ans_to_create_only_body=[0,0,0,0]
	body_response=[0,0,0,0]
	body_and_patch=[0,0,0,0]
	need_light=[0,0,0,0]
	need_light1=[0,0,0,0]
	need_light2=[0,0,0,0]
	need_light3=[0,0,0,0]
	the_first=[0,0,0,0]
	the_second=[0,0,0,0]
	body_num_press=[0,0,0,0]
	all_num_touches_to_do=[0,0,0,0]
	ques_9_num = [0,0,0,0]
	limit = [0,0,0,0]
	limit1 = [0,0,0,0]
	limit2 = [0,0,0,0]
	limit3 = [0,0,0,0]
	limitRed = [0,0,0,0]
	limitGreen = [0,0,0,0]
	limitBlue = [0,0,0,0]
	limitYellow  = [0,0,0,0]
	limitLion = [0,0,0,0]
	limitMonkey = [0,0,0,0]
	limitParrot = [0,0,0,0]
	limitOctopus = [0,0,0,0]
	for k,kaudio in enumerate(listaudios):
	###9CNSU
		if kaudio=='audio_11889':
			if same_ele['lion']==1:
				body_response[k] = 1
				body_num_press[k] = 11
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11890':
			if same_ele['lion']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11891':
			if same_ele['lion']==3:
				body_response[k] = 1
				body_num_press[k] = 13
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11892':
			if same_ele['lion']==4:
				body_response[k] = 1
				body_num_press[k] = 14
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11893':
			if same_ele['monkey']==1:
				body_response[k] = 1
				body_num_press = 1
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11894':
			if same_ele['monkey']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11895':
			if same_ele['monkey']==3:
				body_response[k] = 1
				body_num_press[k] = 13
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11896':
			if same_ele['monkey']==4:
				body_response[k] = 1
				body_num_press[k] = 14
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11897':
			if same_ele['parrot']==1:
				body_response[k] = 1
				body_num_press = 1
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11898':
			if same_ele['parrot']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11899':
			if same_ele['parrot']==3:
				body_response[k] = 1
				body_num_press[k] = 13
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11900':
			if same_ele['parrot']==4:
				body_response[k] = 1
				body_num_press[k] = 14
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11901':
			if same_ele['octopus']==1:
				body_response[k] = 1
				body_num_press = 1
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11902':
			if same_ele['octopus']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11903':
			if same_ele['octopus']==3:
				body_response[k] = 1
				body_num_press[k] = 13
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11904':
			if same_ele['octopus']==4:
				body_response[k] = 1
				body_num_press[k] = 14
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11905':
			if same_ele['icecream']==1:
				body_response[k] = 1
				body_num_press = 1
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11906':
			if same_ele['icecream']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11907':
			if same_ele['icecream']==3:
				body_response[k] = 1
				body_num_press[k] = 13
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11908':
			if same_ele['icecream']==4:
				body_response[k] = 1
				body_num_press[k] = 14
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11909':
			if same_ele['glasses']==1:
				body_response[k] = 1
				body_num_press = 1
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11910':
			if same_ele['glasses']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11911':
			if same_ele['glasses']==3:
				body_response[k] = 1
				body_num_press[k] = 13
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11912':
			if same_ele['glasses']==4:
				body_response[k] = 1
				body_num_press[k] = 14
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11913':
			if same_ele['bicycle']==1:
				body_response[k] = 1
				body_num_press = 1
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11914':
			if same_ele['bicycle']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11915':
			if same_ele['bicycle']==3:
				body_response[k] = 1
				body_num_press[k] = 13
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11916':
			if same_ele['bicycle']==4:
				body_response[k] = 1
				body_num_press[k] = 14
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11917':
			if same_ele['push_scooter']==1:
				body_response[k] = 1
				body_num_press = 1
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11918':
			if same_ele['push_scooter']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11919':
			if same_ele['push_scooter']==3:
				body_response[k] = 1
				body_num_press[k] = 13
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11920':
			if same_ele['push_scooter']==4:
				body_response[k] = 1
				body_num_press[k] = 14
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11921':
			if same_ele['umbrella']==1:
				body_response[k] = 1
				body_num_press = 1
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11922':
			if same_ele['umbrella']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11923':
			if same_ele['umbrella']==3:
				body_response[k] = 1
				body_num_press[k] = 13
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11924':
			if same_ele['umbrella']==4:
				body_response[k] = 1
				body_num_press[k] = 14
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11925':
			if same_ele['girl']==1:
				body_response[k] = 1
				body_num_press = 1
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11926':
			if same_ele['girl']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11927':
			if same_ele['girl']==3:
				body_response[k] = 1
				body_num_press[k] = 13
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly
				ali.append(body_num_press)
		elif kaudio=='audio_11928':
			if same_ele['girl']==4:
				body_response[k] = 1
				body_num_press[k] = 14
				no_ans_to_create_only_body[k] = 1
				resp_in_body[k] = 1 #belly	
				ali.append(body_num_press)		
	###9CNCO
		elif kaudio=='audio_11929':
			no_ans_to_create_only_body[k] = 1
			if same_col['Red']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				ali.append(body_num_press)
		elif kaudio=='audio_11930':
			no_ans_to_create_only_body[k] = 1
			if same_col['Red']==2:
				body_response[k] = 1
				body_num_press[k] = 13
				ali.append(body_num_press)
		elif kaudio=='audio_11931':
			no_ans_to_create_only_body[k] = 1
			if same_col['Red']==2:
				body_response[k] = 1
				body_num_press[k] = 14
				ali.append(body_num_press)
		elif kaudio=='audio_11932':
			no_ans_to_create_only_body[k] = 1
			if same_col['Green']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				ali.append(body_num_press)
		elif kaudio=='audio_11933':
			no_ans_to_create_only_body[k] = 1
			if same_col['Green']==2:
				body_response[k] = 1
				body_num_press[k] = 13
				ali.append(body_num_press)
		elif kaudio=='audio_11934':
			no_ans_to_create_only_body[k] = 1
			if same_col['Green']==2:
				body_response[k] = 1
				body_num_press[k] = 14
				ali.append(body_num_press)
		elif kaudio=='audio_11935':
			no_ans_to_create_only_body[k] = 1
			if same_col['Yellow']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				ali.append(body_num_press)
		elif kaudio=='audio_11936':
			no_ans_to_create_only_body[k] = 1
			if same_col['Yellow']==2:
				body_response[k] = 1
				body_num_press[k] = 13
				ali.append(body_num_press)
		elif kaudio=='audio_11937':
			no_ans_to_create_only_body[k] = 1
			if same_col['Yellow']==2:
				body_response[k] = 1
				body_num_press[k] = 14
				ali.append(body_num_press)
		elif kaudio=='audio_11938':
			no_ans_to_create_only_body[k] = 1
			if same_col['Blue']==2:
				body_response[k] = 1
				body_num_press[k] = 12
				ali.append(body_num_press)
		elif kaudio=='audio_11939':
			no_ans_to_create_only_body[k] = 1
			if same_col['Blue']==2:
				body_response[k] = 1
				body_num_press[k] = 13
				ali.append(body_num_press)
		elif kaudio=='audio_11940':
			no_ans_to_create_only_body[k] = 1
			if same_col['Blue']==2:
				body_response[k] = 1
				body_num_press[k] = 14	
				ali.append(body_num_press)
	###9CNSPA
		elif kaudio=='audio_11983':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11984':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11985':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11986':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11987':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11988':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11989':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11990':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11991':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11992':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11993':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11994':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11995':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11996':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11997':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11998':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_11999':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12000':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12001':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12002':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12003':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12004':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12005':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12006':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12007':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12008':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12009':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12010':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12011':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12012':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12013':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12014':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12015':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12016':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12017':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12018':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12019':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12020':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12021':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12022':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12023':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12024':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12025':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12026':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12027':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12028':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12029':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12030':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12031':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12032':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12033':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12034':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12035':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12036':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12037':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12038':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12039':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12040':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12041':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12042':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12043':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12044':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12045':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12046':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12047':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12048':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12049':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12050':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12051':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12052':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12053':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12054':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12055':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12056':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12057':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12058':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12059':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12060':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12061':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12062':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12063':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12064':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12065':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12066':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12067':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12068':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12069':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12070':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12071':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12072':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12073':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12074':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12075':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12076':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12077':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12078':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12079':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12080':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12081':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12082':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12083':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12084':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12085':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12086':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12087':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12088':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12089':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12090':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12091':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12092':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12093':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12094':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12095':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12096':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12097':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12098':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12099':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12100':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12101':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
		elif kaudio=='audio_12102':
			ali_tmp = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
			ali.append(ali_tmp[0])
	###9CNSPX
		elif kaudio=='audio_12103':
			ali.append(0)
		elif kaudio=='audio_12104':
			ali.append(1)
		elif kaudio=='audio_12105':
			ali.append(2)
		elif kaudio=='audio_12106':
			ali.append(3)
		elif kaudio=='audio_12107':
			ali.append(0)
			ali.append(0)
		elif kaudio=='audio_12108':
			ali.append(1)
			ali.append(1)
		elif kaudio=='audio_12109':
			ali.append(2)
			ali.append(2)
		elif kaudio=='audio_12110':
			ali.append(3)
			ali.append(3)
		elif kaudio=='audio_12111':
			ali.append(0)
			ali.append(0)
			ali.append(0)
		elif kaudio=='audio_12112':
			ali.append(1)
			ali.append(1)
			ali.append(1)
		elif kaudio=='audio_12113':
			ali.append(2)
			ali.append(2)
			ali.append(2)
		elif kaudio=='audio_12114':
			ali.append(3)
			ali.append(3)
			ali.append(3)
		elif kaudio=='audio_12115':
			ali.append(0)
			ali.append(0)
			ali.append(0)
			ali.append(0)
		elif kaudio=='audio_12116':
			ali.append(1)
			ali.append(1)
			ali.append(1)
			ali.append(1)
		elif kaudio=='audio_12117':
			ali.append(2)
			ali.append(2)
			ali.append(2)
			ali.append(2)
		elif kaudio=='audio_12118':
			ali.append(3)
			ali.append(3)
			ali.append(3)
			ali.append(3)
	###9CNSSU
		elif kaudio=='audio_11941': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11942': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11943': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Red']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11944': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11945': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11946': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Green']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11947': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Yellow']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11948': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Yellow']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11949': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Yellow']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11950': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Blue']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11951': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Blue']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11952': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='plain' and pc=='Blue']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11953': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11954': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11955': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11956': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11957': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11958': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11959': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11960': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11961': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11962': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11963': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11964': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11965': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11966': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11967': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11968': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11969': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11970': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
			print("ali ali === {}".format(ali))	
		elif kaudio=='audio_11971': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11972': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11973': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
			print("ali ali === {}".format(ali))				
		elif kaudio=='audio_11974': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11975': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11976': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
			print("ali ali === {}".format(ali))				
		elif kaudio=='audio_11977': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11978': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11979': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
			print("ali ali === {}".format(ali))				
		elif kaudio=='audio_11980': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 2
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11981': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 3
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
			print("ali ali === {}".format(ali))
		elif kaudio=='audio_11982': 
			multiple_or_ans[k] = 1
			all_alternative_pos[k]=1
			num_touches_to_do[k] = 4
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
		
		################################################## special case
		print("GIRO NUMERO 9c {}".format(k))
		print("partial_ali {}".format(ali))
		tot.append(ali)
		if all_alternative_pos[k]==1:
			lung = len(ali)
			ques_9_num[k]=lung
			all_num_touches_to_do[k]=num_touches_to_do
		else:
			ques_9_num[k]=0
			all_num_touches_to_do[k]=0
		ali=[]
		ali_tmp=[]
		print("all_alternative_pos={}".format(all_alternative_pos))

	print("NUM APPEND!!!!NUM APPEND!!!!NUM APPEND!!!! {}".format(ques_9_num))
	print("NUM all_num_touches_to_do!!!!NUM all_num_touches_to_do!!!!NUM all_num_touches_to_do!!!! {}".format(all_num_touches_to_do))
	print("!!!!!!!!! REDBULL tot REDBULL tot REDBULL tot")
	print(tot)
	all_properties = []
	all_properties.append(tot)
	all_properties.append(tot_1)
	all_properties.append(all_alternative_pos)
	all_properties.append(one_ans_is_enough)
	all_properties.append(multiple_branch_ans)
	all_properties.append(multiple_or_ans)
	all_properties.append(touch_together)
	all_properties.append(no_ans_to_create_only_body)
	all_properties.append(body_response)
	all_properties.append(body_and_patch)
	all_properties.append(need_light)
	all_properties.append(need_light1)
	all_properties.append(need_light2)
	all_properties.append(need_light3)
	all_properties.append(the_first)
	all_properties.append(the_second)
	all_properties.append(body_num_press)
	all_properties.append(all_num_touches_to_do)
	all_properties.append(ques_9_num)
	all_properties.append(limit)
	all_properties.append(limit1)
	all_properties.append(limit2)
	all_properties.append(limit3)
	all_properties.append(limitRed)
	all_properties.append(limitGreen)
	all_properties.append(limitBlue)
	all_properties.append(limitYellow)	
	all_properties.append(limitLion)
	all_properties.append(limitMonkey)
	all_properties.append(limitParrot)
	all_properties.append(limitOctopus)
	return all_properties

if __name__=="__main__":
	from collections import Counter
	patpat_ele = ['bicycle', 'monkey', 'parrot', 'octopus']
	patpat_col = ['Red', 'Yellow', 'Green', 'Blue']
	patpat_gen = ['Thing', 'Animal', 'Animal', 'Animal']

	listaudios = ['audio_11960', 'audio_11963', 'audio_11972']
	#same_col = ({'Red': 1, 'Yellow': 1, 'Green': 1, 'Blue': 1})
	#same_ele = ({'bicycle': 1, 'monkey': 1, 'parrot': 1, 'octopus': 1})

	same_col=Counter(patpat_col)
	same_ele=Counter(patpat_ele)

	all_propertiesne = build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,same_col,same_ele)
	print(all_propertiesne)