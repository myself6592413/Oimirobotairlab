import sys 
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/game') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/game')
if '/home/pi/OIMI/oimi_code/src/oimi_state_machine/') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/oimi_state_machine/')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/scenario') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/scenario')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/score') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/score')	
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/games_reactions/') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/games_reactions/')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/game_letsplay/') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/game_letsplay/')

cimport scenario as sc
cimport score as res
from enums_play cimport GameDifficulty
from games_classes cimport PyQuestion
from games_classes cimport QeAWrappingList 
cdef class Game:
	cdef:
		int id_game
		int type_of_game # subset
		int kind_of_game # set
		str audio_intro_kind 
		int necessary_time # for ending a single game (required => num questions)
		int ideal_necessary_time
		str this_kind #converted from int to str
		str this_type #converted from int to str
		int extra_time
		int ques_value
		GameDifficulty difficulty_of_game
		int num_questions
		str order_difficulty_questions
		str audio_g_intro
		str audio_g_exit
		str audio_rules
		list current_game_subjects
		list current_game_colors
		list current_game_genres
		list listaudios
		list listaudios_int
		list listaTypes
		QeAWrappingList ques_ans
		QeAWrappingList ques_ans_1
		int num_responses
		dict user_dict_game
		check_res
		preli 
		res.Score[:] results
		sc.Scenario scenario
		db_conn
		PyQuestion real_question
		list resp_in_body #no patch
		list one_ans_is_enough # when more ans are available but only 1 is necessary, no need of touching all patch
		list multiple_branch_ans # otherwise...1 ans is enough 2 resp available but only 1 is correct (Tocca una bicicletta se la luce è rossa altrimenti tocca un ombrello)
		list multiple_or_ans # 9cnssu --> 3 possible answer but only 1 is enough...but need 2/3/4 touch = 2/3/4 = answers 
		list touch_together # when ans is simultanously
		list no_ans_to_create_only_body #dont create listans, wait for body sensor
		list body_response # body sensors
		list body_and_patch # ok patch resp is necessary but also body sensor
		list need_light # correct resp depends on light
		list need_light1
		list need_light2
		list need_light3
		list the_first # correct resp is one of the two (the first) ignore other resp
		list the_second # correct resp is one of the two (the first) ignore other resp
		list body_num_press # num times sensor body pressure to answer
		list all_num_touches_to_do
		list ques_9_num
		list limit
		list limit1
		list limit2
		list limit3
		list limitRed
		list limitGreen
		list limitBlue
		list limitYellow
		list limitLion
		list limitMonkey
		list limitParrot
		list limitOctopus	
		same_col #how many instance of each color in a scenario
		same_ele #how many instance of each subject in a scenario

	cpdef set_audio_introkind(self)
	cpdef set_necessary_time(self)
	cpdef set_num_questions(self, num_questions)
	cdef build_answers(self,lunghezza,tot,l1,l2,l3,l4,all_alternative_pos, ques_9_num, num_touches_to_do)
	cpdef select_questions(self)
	cpdef create_QeA(self)
	cdef write_responses(self, patpat_ele, patpat_col, patpat_gen, listaudios, listaKinds, listaTypes)
	cpdef play_the_game(self)
	#cpdef play_the_game(self,ser) 
	cpdef prova_stupida_per_vedere_se_le_cose_sono_state_modificate_da_play_The_Game(self)
	cpdef finally_calc_results(self)