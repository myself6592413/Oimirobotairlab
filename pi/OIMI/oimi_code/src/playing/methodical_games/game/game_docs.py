# ==========================================================================================================================================================
# Game documentation
# ==========================================================================================================================================================
def game_class_doc():
	""" Info 		

Parameters --> same as in table
	0|game_id|INTEGER|0||1
	1|num_questions|INTEGER|0||0
	2|kind|TEXT|0||0
	3|type|TEXT|0||0
	4|difficulty|TEXT|0||0
	5|order_difficulty_questions|TEXT|0||0
	6|necessary_time|INTEGER|0||0
	7|audio_intro|TEXT|0||0
	8|audio_exit|TEXT|0||0
	9|audio_rules|TEXT|0||0
	10|disposable|INTEGER|0||0
	11|token|TEXT|0||0

Details
	==> List of all games:
		1F = Find game
		2L = Choose colorLED game
		3S = Same pattern game
		4P = Same pair game 
		5K = knowledge game
		6I = Intruder game
		7O = Order game
		8Q = Quiz game
		9C = Count game
	==>List of available questions:
	######################################
	#1F = game_Find
		1FSUS = Find subject single (but multiple answers possible depending on scenario, and also on difficulty =sometimes is enough touch one lion in simplest case even if there are two lions):
			Tocca il bottone rosso
			Tocca il bottone verde
			Tocca il bottone giallo
			Tocca il bottone blu
			Tocca le scimmie
			Tocca i leoni
			Tocca i pappagalli
			Tocca i polipi
			Tocca i gelati
			Tocca i paia di occhiali da sole
			Tocca le biciclette
			Tocca i monopattini
			Tocca gli ombrelli
			Tocca le bambine
		1FSUD = Find subject double (order isnt important) (but multiple answers possible):
			Tocca il bottone rosso e il bottoni verdi
			Tocca il bottone rosso e il bottoni gialli
			Tocca il bottone rosso e il bottoni blu
			Tocca il bottone verde e il bottoni blu
			Tocca il bottone verde e il bottoni gialli
			Tocca il bottone gialloe il bottoni blu
			Tocca i leoni e le scimmie
			Tocca i leoni e i polipi
			Tocca i leoni e i pappagalli
			Tocca i leoni e i gelati
			Tocca i leoni e tutti gli occhiali da sole
			Tocca i leoni e le biciclette
			Tocca i leoni e i monopattini
			Tocca i leoni e gli ombrelli
			Tocca i leoni e le bambine
			Tocca le scimmie e i polipi
			Tocca le scimmie e i pappagalli
			Tocca le scimmie e i gelati
			Tocca le scimmie e tutti gli occhiali da sole
			Tocca le scimmie e le biciclette
			Tocca le scimmie e i monopattini
			Tocca le scimmie e gli ombrelli
			Tocca le scimmie e le bambine
			Tocca i pappagalli e i polipi
			Tocca i pappagalli e i gelati
			Tocca i pappagalli e tutti gli occhiali da sole
			Tocca i pappagalli e le biciclette
			Tocca i pappagalli e i monopattini
			Tocca i pappagalli e gli ombrelli
			Tocca i pappagalli e le bambine
			Tocca i polipi e i gelati
			Tocca i polipi e tutti gli occhiali da sole
			Tocca i polipi e le biciclette
			Tocca i polipi e i monopattini
			Tocca i polipi e gli ombrelli
			Tocca i polipi e le bambine
			Tocca i gelati e tutti gli occhiali da sole
			Tocca i gelati e le biciclette
			Tocca i gelati e i monopattini
			Tocca i gelati e gli ombrelli
			Tocca i gelati e le bambine
			Tocca tutti gli occhiali da sole e le biciclette
			Tocca tutti gli occhiali da sole e i monopattini
			Tocca tutti gli occhiali da sole e gli ombrelli
			Tocca tutti gli occhiali da sole e le bambine
			Tocca le biciclette e i monopattini
			Tocca le biciclette e gli ombrelli
			Tocca le biciclette e le bambine
			Tocca i monopattini e gli ombrelli
			Tocca i monopattini e le bambine
			Tocca gli ombrelli e le bambine
		1FSUDT = Find subject double together! at the same time (less than one second of delay)! (but multiple answers possible):
			Tocca nello stesso momento il bottone tutto rosso e il bottone tutto verde
			Tocca nello stesso momento il bottone tutto rosso e il bottone tutto giallo
			Tocca nello stesso momento il bottone tutto rosso e il bottone tutto blu
			Tocca nello stesso momento il bottone tutto giallo e il bottone tutto verde
			Tocca nello stesso momento il bottone tutto giallo e il bottone tutto blu
			Tocca nello stesso momento il bottone tutto blu e il bottone tutto verde
			Tocca nello stesso momento un leone e una scimmia
			Tocca nello stesso momento un leone e un polipo
			Tocca nello stesso momento un leone e un pappagallo
			Tocca nello stesso momento un leone e un gelato
			Tocca nello stesso momento un leone e un paio di occhiali da sole
			Tocca nello stesso momento un leone e una bicicletta
			Tocca nello stesso momento un leone e un monopattino
			Tocca nello stesso momento un leone e un ombrello
			Tocca nello stesso momento un leone e una bambina
			Tocca nello stesso momento una scimmia e un polipo
			Tocca nello stesso momento una scimmia e un pappagallo
			Tocca nello stesso momento una scimmia e un gelato
			Tocca nello stesso momento una scimmia e un paio di occhiali da sole
			Tocca nello stesso momento una scimmia e una bicicletta
			Tocca nello stesso momento una scimmia e un monopattino
			Tocca nello stesso momento una scimmia e un ombrello
			Tocca nello stesso momento una scimmia e una bambina
			Tocca nello stesso momento un pappagallo e un polipo
			Tocca nello stesso momento un pappagallo e un gelato
			Tocca nello stesso momento un pappagallo e un paio di occhiali da sole
			Tocca nello stesso momento un pappagallo e una bicicletta
			Tocca nello stesso momento un pappagallo e un monopattino
			Tocca nello stesso momento un pappagallo e un ombrello
			Tocca nello stesso momento un pappagallo e una bambina
			Tocca nello stesso momento un polipo e un gelato
			Tocca nello stesso momento un polipo e un paio di occhiali da sole
			Tocca nello stesso momento un polipo e una bicicletta
			Tocca nello stesso momento un polipo e un monopattino
			Tocca nello stesso momento un polipo e un ombrello
			Tocca nello stesso momento un polipo e una bambina
			Tocca nello stesso momento un gelato e un paio di occhiali da sole
			Tocca nello stesso momento un gelato e una bicicletta
			Tocca nello stesso momento un gelato e un monopattino
			Tocca nello stesso momento un gelato e un ombrello
			Tocca nello stesso momento un gelato e una bambina
			Tocca nello stesso momento un paio di occhiali da sole e una bicicletta
			Tocca nello stesso momento un paio di occhiali da sole e un monopattino
			Tocca nello stesso momento un paio di occhiali da sole e un ombrello
			Tocca nello stesso momento un paio di occhiali da sole e una bambina
			Tocca nello stesso momento una bicicletta e un monopattino
			Tocca nello stesso momento una bicicletta e un ombrello
			Tocca nello stesso momento un monopattino e un ombrello
			Tocca nello stesso momento un monopattino e una bambina
			Tocca nello stesso momento un ombrello e una bambina
		1FCOS = Find color single (but multiple answers possible):     
			Tocca i disegni rossi
			Tocca i disegni blu
			Tocca i disegni verdu
			Tocca i disegni gialli
		1FCOD = Find color double (but multiple answers possible):     
			Tocca i disegni rossi e i disegni verdi
			Tocca i disegni rossi e i disegni gialli
			Tocca i disegni rossi e i disegni blu
			Tocca i disegni verdi e i disegni gialli
			Tocca i disegni verdi e i disegni blu
			Tocca i disegni gialli e i disegni blu
		1FCODT = Find color together! double (but multiple answers possible):     
			Tocca insieme un disegno rosso e un disegno verde
			Tocca insieme un disegno rosso e un disegno giallo
			Tocca insieme un disegno rosso e un disegno blu
			Tocca insieme un disegno verde e un disegno giallo
			Tocca insieme un disegno verde e un disegno blu
			Tocca insieme un disegno giallo e un disegno blu
		1FSUCOS = Find subject and color single (one answer allowed here): => NOT HERE Nothing and places 
			Tocca la scimmia rossa
			Tocca la scimmia verde
			Tocca la scimmia gialla
			Tocca la scimmia blu
			Tocca il leone rosso
			Tocca il leone verde
			Tocca il leone giallo
			Tocca il leone blu
			Tocca il pappagallo rosso
			Tocca il pappagallo verde
			Tocca il pappagallo giallo
			Tocca il pappagallo blu
			Tocca il polipo rosso
			Tocca il polipo verde
			Tocca il polipo giallo
			Tocca il polipo blu
			Tocca il gelato rosso
			Tocca il gelato verde
			Tocca il gelato giallo
			Tocca il gelato blu
			Tocca gli occhiali da sole rossi
			Tocca gli occhiali da sole verdi
			Tocca gli occhiali da sole gialli
			Tocca gli occhiali da sole blu
			Tocca la bicicletta rossa
			Tocca la bicicletta verde
			Tocca la bicicletta gialla
			Tocca la bicicletta blu
			Tocca il monopattino rosso
			Tocca il monopattino verde
			Tocca il monopattino giallo
			Tocca il monopattino blu
			Tocca l'ombrello rosso
			Tocca l'ombrello verde
			Tocca l'ombrello giallo
			Tocca l'ombrello blu
			Tocca la bambina vestita in rosso
			Tocca la bambina vestita in verde
			Tocca la bambina vestita in giallo
			Tocca la bambina vestita in blu
		1FSUCOD = Find subject and color double (two answers allowed here): => NOT HERE Nothing and places 
			Tocca la scimmia rossa e la scimmia verde
			Tocca la scimmia rossa e la scimmia gialla
			Tocca la scimmia rossa e la scimmia blu
			Tocca la scimmia rossa e il leone rosso
			Tocca la scimmia rossa e il leone verde
			Tocca la scimmia rossa e il leone giallo
			Tocca la scimmia rossa e il leone blu
			Tocca la scimmia rossa e il pappagallo rosso
			Tocca la scimmia rossa e il pappagallo verde
			Tocca la scimmia rossa e il pappagallo giallo
			Tocca la scimmia rossa e il pappagallo blu
			Tocca la scimmia rossa e il polipo rosso
			Tocca la scimmia rossa e il polipo verde
			Tocca la scimmia rossa e il polipo giallo
			Tocca la scimmia rossa e il polipo blu
			Tocca la scimmia rossa e il gelato rosso
			Tocca la scimmia rossa e il gelato verde
			Tocca la scimmia rossa e il gelato giallo
			Tocca la scimmia rossa e il gelato blu
			Tocca la scimmia rossa e gli occhiali da sole rossi
			Tocca la scimmia rossa e gli occhiali da sole verdi
			Tocca la scimmia rossa e gli occhiali da sole gialli
			Tocca la scimmia rossa e gli occhiali da sole blu
			Tocca la scimmia rossa e la bicicletta rossa
			Tocca la scimmia rossa e la bicicletta verde
			Tocca la scimmia rossa e la bicicletta gialla
			Tocca la scimmia rossa e la bicicletta blu
			Tocca la scimmia rossa e il monopattino rosso
			Tocca la scimmia rossa e il monopattino verde
			Tocca la scimmia rossa e il monopattino giallo
			Tocca la scimmia rossa e il monopattino blu
			Tocca la scimmia rossa e l'ombrello rosso
			Tocca la scimmia rossa e l'ombrello verde
			Tocca la scimmia rossa e l'ombrello giallo
			Tocca la scimmia rossa e l'ombrello blu
			Tocca la scimmia rossa e la bambina vestita in rosso
			Tocca la scimmia rossa e la bambina vestita in verde
			Tocca la scimmia rossa e la bambina vestita in giallo
			Tocca la scimmia rossa e la bambina vestita in blu
			Tocca la scimmia verde e la scimmia gialla
			Tocca la scimmia verde e la scimmia blu
			Tocca la scimmia verde e il leone rosso
			Tocca la scimmia verde e il leone verde
			Tocca la scimmia verde e il leone giallo
			Tocca la scimmia verde e il leone blu
			Tocca la scimmia verde e il pappagallo rosso
			Tocca la scimmia verde e il pappagallo verde
			Tocca la scimmia verde e il pappagallo giallo
			Tocca la scimmia verde e il pappagallo blu
			Tocca la scimmia verde e il polipo rosso
			Tocca la scimmia verde e il polipo verde
			Tocca la scimmia verde e il polipo giallo
			Tocca la scimmia verde e il polipo blu
			Tocca la scimmia verde e il gelato rosso
			Tocca la scimmia verde e il gelato verde
			Tocca la scimmia verde e il gelato giallo
			Tocca la scimmia verde e il gelato blu
			Tocca la scimmia verde e gli occhiali da sole rossi
			Tocca la scimmia verde e gli occhiali da sole verdi
			Tocca la scimmia verde e gli occhiali da sole gialli
			Tocca la scimmia verde e gli occhiali da sole blu
			Tocca la scimmia verde e la bicicletta rossa
			Tocca la scimmia verde e la bicicletta verde
			Tocca la scimmia verde e la bicicletta gialla
			Tocca la scimmia verde e la bicicletta blu
			Tocca la scimmia verde e il monopattino rosso
			Tocca la scimmia verde e il monopattino verde
			Tocca la scimmia verde e il monopattino giallo
			Tocca la scimmia verde e il monopattino blu
			Tocca la scimmia verde e l'ombrello rosso
			Tocca la scimmia verde e l'ombrello verde
			Tocca la scimmia verde e l'ombrello giallo
			Tocca la scimmia verde e l'ombrello blu
			Tocca la scimmia verde e la bambina vestita in rosso
			Tocca la scimmia verde e la bambina vestita in verde
			Tocca la scimmia verde e la bambina vestita in giallo
			Tocca la scimmia verde e la bambina vestita in blu
			Tocca la scimmia gialla e la scimmia blu
			Tocca la scimmia gialla e il leone rosso
			Tocca la scimmia gialla e il leone verde
			Tocca la scimmia gialla e il leone giallo
			Tocca la scimmia gialla e il leone blu
			Tocca la scimmia gialla e il pappagallo rosso
			Tocca la scimmia gialla e il pappagallo verde
			Tocca la scimmia gialla e il pappagallo giallo
			Tocca la scimmia gialla e il pappagallo blu
			Tocca la scimmia gialla e il polipo rosso
			Tocca la scimmia gialla e il polipo verde
			Tocca la scimmia gialla e il polipo giallo
			Tocca la scimmia gialla e il polipo blu
			Tocca la scimmia gialla e il gelato rosso
			Tocca la scimmia gialla e il gelato verde
			Tocca la scimmia gialla e il gelato giallo
			Tocca la scimmia gialla e il gelato blu
			Tocca la scimmia gialla e gli occhiali da sole rossi
			Tocca la scimmia gialla e gli occhiali da sole verdi
			Tocca la scimmia gialla e gli occhiali da sole gialli
			Tocca la scimmia gialla e gli occhiali da sole blu
			Tocca la scimmia gialla e la bicicletta rossa
			Tocca la scimmia gialla e la bicicletta verde
			Tocca la scimmia gialla e la bicicletta gialla
			Tocca la scimmia gialla e la bicicletta blu
			Tocca la scimmia gialla e il monopattino rosso
			Tocca la scimmia gialla e il monopattino verde
			Tocca la scimmia gialla e il monopattino giallo
			Tocca la scimmia gialla e il monopattino blu
			Tocca la scimmia gialla e l'ombrello rosso
			Tocca la scimmia gialla e l'ombrello verde
			Tocca la scimmia gialla e l'ombrello giallo
			Tocca la scimmia gialla e l'ombrello blu
			Tocca la scimmia gialla e la bambina vestita in rosso
			Tocca la scimmia gialla e la bambina vestita in verde
			Tocca la scimmia gialla e la bambina vestita in giallo
			Tocca la scimmia gialla e la bambina vestita in blu
			Tocca la scimmia blu e il leone rosso
			Tocca la scimmia blu e il leone verde
			Tocca la scimmia blu e il leone giallo
			Tocca la scimmia blu e il leone blu
			Tocca la scimmia blu e il pappagallo rosso
			Tocca la scimmia blu e il pappagallo verde
			Tocca la scimmia blu e il pappagallo giallo
			Tocca la scimmia blu e il pappagallo blu
			Tocca la scimmia blu e il polipo rosso
			Tocca la scimmia blu e il polipo verde
			Tocca la scimmia blu e il polipo giallo
			Tocca la scimmia blu e il polipo blu
			Tocca la scimmia blu e il gelato rosso
			Tocca la scimmia blu e il gelato verde
			Tocca la scimmia blu e il gelato giallo
			Tocca la scimmia blu e il gelato blu
			Tocca la scimmia blu e gli occhiali da sole rossi
			Tocca la scimmia blu e gli occhiali da sole verdi
			Tocca la scimmia blu e gli occhiali da sole gialli
			Tocca la scimmia blu e gli occhiali da sole blu
			Tocca la scimmia blu e la bicicletta rossa
			Tocca la scimmia blu e la bicicletta verde
			Tocca la scimmia blu e la bicicletta gialla
			Tocca la scimmia blu e la bicicletta blu
			Tocca la scimmia blu e il monopattino rosso
			Tocca la scimmia blu e il monopattino verde
			Tocca la scimmia blu e il monopattino giallo
			Tocca la scimmia blu e il monopattino blu
			Tocca la scimmia blu e l'ombrello rosso
			Tocca la scimmia blu e l'ombrello verde
			Tocca la scimmia blu e l'ombrello giallo
			Tocca la scimmia blu e l'ombrello blu
			Tocca la scimmia blu e la bambina vestita in rosso
			Tocca la scimmia blu e la bambina vestita in verde
			Tocca la scimmia blu e la bambina vestita in giallo
			Tocca la scimmia blu e la bambina vestita in blu
			Tocca il leone rosso e il leone verde
			Tocca il leone rosso e il leone giallo
			Tocca il leone rosso e il leone blu
			Tocca il leone rosso e il pappagallo rosso
			Tocca il leone rosso e il pappagallo verde
			Tocca il leone rosso e il pappagallo giallo
			Tocca il leone rosso e il pappagallo blu
			Tocca il leone rosso e il polipo rosso
			Tocca il leone rosso e il polipo verde
			Tocca il leone rosso e il polipo giallo
			Tocca il leone rosso e il polipo blu
			Tocca il leone rosso e il gelato rosso
			Tocca il leone rosso e il gelato verde
			Tocca il leone rosso e il gelato giallo
			Tocca il leone rosso e il gelato blu
			Tocca il leone rosso e gli occhiali da sole rossi
			Tocca il leone rosso e gli occhiali da sole verdi
			Tocca il leone rosso e gli occhiali da sole gialli
			Tocca il leone rosso e gli occhiali da sole blu
			Tocca il leone rosso e la bicicletta rossa
			Tocca il leone rosso e la bicicletta verde
			Tocca il leone rosso e la bicicletta gialla
			Tocca il leone rosso e la bicicletta blu
			Tocca il leone rosso e il monopattino rosso
			Tocca il leone rosso e il monopattino verde
			Tocca il leone rosso e il monopattino giallo
			Tocca il leone rosso e il monopattino blu
			Tocca il leone rosso e l'ombrello rosso
			Tocca il leone rosso e l'ombrello verde
			Tocca il leone rosso e l'ombrello giallo
			Tocca il leone rosso e l'ombrello blu
			Tocca il leone rosso e la bambina vestita in rosso
			Tocca il leone rosso e la bambina vestita in verde
			Tocca il leone rosso e la bambina vestita in giallo
			Tocca il leone rosso e la bambina vestita in blu
			Tocca il leone verde e il leone giallo
			Tocca il leone verde e il leone blu
			Tocca il leone verde e il pappagallo rosso
			Tocca il leone verde e il pappagallo verde
			Tocca il leone verde e il pappagallo giallo
			Tocca il leone verde e il pappagallo blu
			Tocca il leone verde e il polipo rosso
			Tocca il leone verde e il polipo verde
			Tocca il leone verde e il polipo giallo
			Tocca il leone verde e il polipo blu
			Tocca il leone verde e il gelato rosso
			Tocca il leone verde e il gelato verde
			Tocca il leone verde e il gelato giallo
			Tocca il leone verde e il gelato blu
			Tocca il leone verde e gli occhiali da sole rossi
			Tocca il leone verde e gli occhiali da sole verdi
			Tocca il leone verde e gli occhiali da sole gialli
			Tocca il leone verde e gli occhiali da sole blu
			Tocca il leone verde e la bicicletta rossa
			Tocca il leone verde e la bicicletta verde
			Tocca il leone verde e la bicicletta gialla
			Tocca il leone verde e la bicicletta blu
			Tocca il leone verde e il monopattino rosso
			Tocca il leone verde e il monopattino verde
			Tocca il leone verde e il monopattino giallo
			Tocca il leone verde e il monopattino blu
			Tocca il leone verde e l'ombrello rosso
			Tocca il leone verde e l'ombrello verde
			Tocca il leone verde e l'ombrello giallo
			Tocca il leone verde e l'ombrello blu
			Tocca il leone verde e la bambina vestita in rosso
			Tocca il leone verde e la bambina vestita in verde
			Tocca il leone verde e la bambina vestita in giallo
			Tocca il leone verde e la bambina vestita in blu
			Tocca il leone giallo e il leone blu
			Tocca il leone giallo e il pappagallo rosso
			Tocca il leone giallo e il pappagallo verde
			Tocca il leone giallo e il pappagallo giallo
			Tocca il leone giallo e il pappagallo blu
			Tocca il leone giallo e il polipo rosso
			Tocca il leone giallo e il polipo verde
			Tocca il leone giallo e il polipo giallo
			Tocca il leone giallo e il polipo blu
			Tocca il leone giallo e il gelato rosso
			Tocca il leone giallo e il gelato verde
			Tocca il leone giallo e il gelato giallo
			Tocca il leone giallo e il gelato blu
			Tocca il leone giallo e gli occhiali da sole rossi
			Tocca il leone giallo e gli occhiali da sole verdi
			Tocca il leone giallo e gli occhiali da sole gialli
			Tocca il leone giallo e gli occhiali da sole blu
			Tocca il leone giallo e la bicicletta rossa
			Tocca il leone giallo e la bicicletta verde
			Tocca il leone giallo e la bicicletta gialla
			Tocca il leone giallo e la bicicletta blu
			Tocca il leone giallo e il monopattino rosso
			Tocca il leone giallo e il monopattino verde
			Tocca il leone giallo e il monopattino giallo
			Tocca il leone giallo e il monopattino blu
			Tocca il leone giallo e l'ombrello rosso
			Tocca il leone giallo e l'ombrello verde
			Tocca il leone giallo e l'ombrello giallo
			Tocca il leone giallo e l'ombrello blu
			Tocca il leone giallo e la bambina vestita in rosso
			Tocca il leone giallo e la bambina vestita in verde
			Tocca il leone giallo e la bambina vestita in giallo
			Tocca il leone giallo e la bambina vestita in blu
			Tocca il leone blu e il pappagallo rosso
			Tocca il leone blu e il pappagallo verde
			Tocca il leone blu e il pappagallo giallo
			Tocca il leone blu e il pappagallo blu
			Tocca il leone blu e il polipo rosso
			Tocca il leone blu e il polipo verde
			Tocca il leone blu e il polipo giallo
			Tocca il leone blu e il polipo blu
			Tocca il leone blu e il gelato rosso
			Tocca il leone blu e il gelato verde
			Tocca il leone blu e il gelato giallo
			Tocca il leone blu e il gelato blu
			Tocca il leone blu e gli occhiali da sole rossi
			Tocca il leone blu e gli occhiali da sole verdi
			Tocca il leone blu e gli occhiali da sole gialli
			Tocca il leone blu e gli occhiali da sole blu
			Tocca il leone blu e la bicicletta rossa
			Tocca il leone blu e la bicicletta verde
			Tocca il leone blu e la bicicletta gialla
			Tocca il leone blu e la bicicletta blu
			Tocca il leone blu e il monopattino rosso
			Tocca il leone blu e il monopattino verde
			Tocca il leone blu e il monopattino giallo
			Tocca il leone blu e il monopattino blu
			Tocca il leone blu e l'ombrello rosso
			Tocca il leone blu e l'ombrello verde
			Tocca il leone blu e l'ombrello giallo
			Tocca il leone blu e l'ombrello blu
			Tocca il leone blu e la bambina vestita in rosso
			Tocca il leone blu e la bambina vestita in verde
			Tocca il leone blu e la bambina vestita in giallo
			Tocca il leone blu e la bambina vestita in blu
			Tocca il pappagallo rosso e il pappagallo verde
			Tocca il pappagallo rosso e il pappagallo giallo
			Tocca il pappagallo rosso e il pappagallo blu
			Tocca il pappagallo rosso e il polipo rosso
			Tocca il pappagallo rosso e il polipo verde
			Tocca il pappagallo rosso e il polipo giallo
			Tocca il pappagallo rosso e il polipo blu
			Tocca il pappagallo rosso e il gelato rosso
			Tocca il pappagallo rosso e il gelato verde
			Tocca il pappagallo rosso e il gelato giallo
			Tocca il pappagallo rosso e il gelato blu
			Tocca il pappagallo rosso e gli occhiali da sole rossi
			Tocca il pappagallo rosso e gli occhiali da sole verdi
			Tocca il pappagallo rosso e gli occhiali da sole gialli
			Tocca il pappagallo rosso e gli occhiali da sole blu
			Tocca il pappagallo rosso e la bicicletta rossa
			Tocca il pappagallo rosso e la bicicletta verde
			Tocca il pappagallo rosso e la bicicletta gialla
			Tocca il pappagallo rosso e la bicicletta blu
			Tocca il pappagallo rosso e il monopattino rosso
			Tocca il pappagallo rosso e il monopattino verde
			Tocca il pappagallo rosso e il monopattino giallo
			Tocca il pappagallo rosso e il monopattino blu
			Tocca il pappagallo rosso e l'ombrello rosso
			Tocca il pappagallo rosso e l'ombrello verde
			Tocca il pappagallo rosso e l'ombrello giallo
			Tocca il pappagallo rosso e l'ombrello blu
			Tocca il pappagallo rosso e la bambina vestita in rosso
			Tocca il pappagallo rosso e la bambina vestita in verde
			Tocca il pappagallo rosso e la bambina vestita in giallo
			Tocca il pappagallo rosso e la bambina vestita in blu
			Tocca il pappagallo verde e il pappagallo giallo
			Tocca il pappagallo verde e il pappagallo blu
			Tocca il pappagallo verde e il polipo rosso
			Tocca il pappagallo verde e il polipo verde
			Tocca il pappagallo verde e il polipo giallo
			Tocca il pappagallo verde e il polipo blu
			Tocca il pappagallo verde e il gelato rosso
			Tocca il pappagallo verde e il gelato verde
			Tocca il pappagallo verde e il gelato giallo
			Tocca il pappagallo verde e il gelato blu
			Tocca il pappagallo verde e gli occhiali da sole rossi
			Tocca il pappagallo verde e gli occhiali da sole verdi
			Tocca il pappagallo verde e gli occhiali da sole gialli
			Tocca il pappagallo verde e gli occhiali da sole blu
			Tocca il pappagallo verde e la bicicletta rossa
			Tocca il pappagallo verde e la bicicletta verde
			Tocca il pappagallo verde e la bicicletta gialla
			Tocca il pappagallo verde e la bicicletta blu
			Tocca il pappagallo verde e il monopattino rosso
			Tocca il pappagallo verde e il monopattino verde
			Tocca il pappagallo verde e il monopattino giallo
			Tocca il pappagallo verde e il monopattino blu
			Tocca il pappagallo verde e l'ombrello rosso
			Tocca il pappagallo verde e l'ombrello verde
			Tocca il pappagallo verde e l'ombrello giallo
			Tocca il pappagallo verde e l'ombrello blu
			Tocca il pappagallo verde e la bambina vestita in rosso
			Tocca il pappagallo verde e la bambina vestita in verde
			Tocca il pappagallo verde e la bambina vestita in giallo
			Tocca il pappagallo verde e la bambina vestita in blu
			Tocca il pappagallo giallo e il pappagallo blu
			Tocca il pappagallo giallo e il polipo rosso
			Tocca il pappagallo giallo e il polipo verde
			Tocca il pappagallo giallo e il polipo giallo
			Tocca il pappagallo giallo e il polipo blu
			Tocca il pappagallo giallo e il gelato rosso
			Tocca il pappagallo giallo e il gelato verde
			Tocca il pappagallo giallo e il gelato giallo
			Tocca il pappagallo giallo e il gelato blu
			Tocca il pappagallo giallo e gli occhiali da sole rossi
			Tocca il pappagallo giallo e gli occhiali da sole verdi
			Tocca il pappagallo giallo e gli occhiali da sole gialli
			Tocca il pappagallo giallo e gli occhiali da sole blu
			Tocca il pappagallo giallo e la bicicletta rossa
			Tocca il pappagallo giallo e la bicicletta verde
			Tocca il pappagallo giallo e la bicicletta gialla
			Tocca il pappagallo giallo e la bicicletta blu
			Tocca il pappagallo giallo e il monopattino rosso
			Tocca il pappagallo giallo e il monopattino verde
			Tocca il pappagallo giallo e il monopattino giallo
			Tocca il pappagallo giallo e il monopattino blu
			Tocca il pappagallo giallo e l'ombrello rosso
			Tocca il pappagallo giallo e l'ombrello verde
			Tocca il pappagallo giallo e l'ombrello giallo
			Tocca il pappagallo giallo e l'ombrello blu
			Tocca il pappagallo giallo e la bambina vestita in rosso
			Tocca il pappagallo giallo e la bambina vestita in verde
			Tocca il pappagallo giallo e la bambina vestita in giallo
			Tocca il pappagallo giallo e la bambina vestita in blu
			Tocca il pappagallo blu e il polipo rosso
			Tocca il pappagallo blu e il polipo verde
			Tocca il pappagallo blu e il polipo giallo
			Tocca il pappagallo blu e il polipo blu
			Tocca il pappagallo blu e il gelato rosso
			Tocca il pappagallo blu e il gelato verde
			Tocca il pappagallo blu e il gelato giallo
			Tocca il pappagallo blu e il gelato blu
			Tocca il pappagallo blu e gli occhiali da sole rossi
			Tocca il pappagallo blu e gli occhiali da sole verdi
			Tocca il pappagallo blu e gli occhiali da sole gialli
			Tocca il pappagallo blu e gli occhiali da sole blu
			Tocca il pappagallo blu e la bicicletta rossa
			Tocca il pappagallo blu e la bicicletta verde
			Tocca il pappagallo blu e la bicicletta gialla
			Tocca il pappagallo blu e la bicicletta blu
			Tocca il pappagallo blu e il monopattino rosso
			Tocca il pappagallo blu e il monopattino verde
			Tocca il pappagallo blu e il monopattino giallo
			Tocca il pappagallo blu e il monopattino blu
			Tocca il pappagallo blu e l'ombrello rosso
			Tocca il pappagallo blu e l'ombrello verde
			Tocca il pappagallo blu e l'ombrello giallo
			Tocca il pappagallo blu e l'ombrello blu
			Tocca il pappagallo blu e la bambina vestita in rosso
			Tocca il pappagallo blu e la bambina vestita in verde
			Tocca il pappagallo blu e la bambina vestita in giallo
			Tocca il pappagallo blu e la bambina vestita in blu
			Tocca il polipo rosso e il polipo verde
			Tocca il polipo rosso e il polipo giallo
			Tocca il polipo rosso e il polipo blu
			Tocca il polipo rosso e il gelato rosso
			Tocca il polipo rosso e il gelato verde
			Tocca il polipo rosso e il gelato giallo
			Tocca il polipo rosso e il gelato blu
			Tocca il polipo rosso e gli occhiali da sole rossi
			Tocca il polipo rosso e gli occhiali da sole verdi
			Tocca il polipo rosso e gli occhiali da sole gialli
			Tocca il polipo rosso e gli occhiali da sole blu
			Tocca il polipo rosso e la bicicletta rossa
			Tocca il polipo rosso e la bicicletta verde
			Tocca il polipo rosso e la bicicletta gialla
			Tocca il polipo rosso e la bicicletta blu
			Tocca il polipo rosso e il monopattino rosso
			Tocca il polipo rosso e il monopattino verde
			Tocca il polipo rosso e il monopattino giallo
			Tocca il polipo rosso e il monopattino blu
			Tocca il polipo rosso e l'ombrello rosso
			Tocca il polipo rosso e l'ombrello verde
			Tocca il polipo rosso e l'ombrello giallo
			Tocca il polipo rosso e l'ombrello blu
			Tocca il polipo rosso e la bambina vestita in rosso
			Tocca il polipo rosso e la bambina vestita in verde
			Tocca il polipo rosso e la bambina vestita in giallo
			Tocca il polipo rosso e la bambina vestita in blu
			Tocca il polipo verde e la scimmia rossa
			Tocca il polipo verde e la scimmia verde
			Tocca il polipo verde e la scimmia gialla
			Tocca il polipo verde e il scimmia blu
			Tocca il polipo verde e il leone rosso
			Tocca il polipo verde e il leone verde
			Tocca il polipo verde e il leone giallo
			Tocca il polipo verde e il leone blu
			Tocca il polipo verde e il pappagallo rosso
			Tocca il polipo verde e il pappagallo verde
			Tocca il polipo verde e il pappagallo giallo
			Tocca il polipo verde e il pappagallo blu
			Tocca il polipo verde e il polipo giallo
			Tocca il polipo verde e il polipo blu
			Tocca il polipo verde e il gelato rosso
			Tocca il polipo verde e il gelato verde
			Tocca il polipo verde e il gelato giallo
			Tocca il polipo verde e il gelato blu
			Tocca il polipo verde e gli occhiali da sole rossi
			Tocca il polipo verde e gli occhiali da sole verdi
			Tocca il polipo verde e gli occhiali da sole gialli
			Tocca il polipo verde e gli occhiali da sole blu
			Tocca il polipo verde e la bicicletta rossa
			Tocca il polipo verde e la bicicletta verde
			Tocca il polipo verde e la bicicletta gialla
			Tocca il polipo verde e la bicicletta blu
			Tocca il polipo verde e il monopattino rosso
			Tocca il polipo verde e il monopattino verde
			Tocca il polipo verde e il monopattino giallo
			Tocca il polipo verde e il monopattino blu
			Tocca il polipo verde e l'ombrello rosso
			Tocca il polipo verde e l'ombrello verde
			Tocca il polipo verde e l'ombrello giallo
			Tocca il polipo verde e l'ombrello blu
			Tocca il polipo verde e la bambina vestita in rosso
			Tocca il polipo verde e la bambina vestita in verde
			Tocca il polipo verde e la bambina vestita in giallo
			Tocca il polipo verde e la bambina vestita in blu
			Tocca il polipo giallo e il polipo blu
			Tocca il polipo giallo e il gelato rosso
			Tocca il polipo giallo e il gelato verde
			Tocca il polipo giallo e il gelato giallo
			Tocca il polipo giallo e il gelato blu
			Tocca il polipo giallo e gli occhiali da sole rossi
			Tocca il polipo giallo e gli occhiali da sole verdi
			Tocca il polipo giallo e gli occhiali da sole gialli
			Tocca il polipo giallo e gli occhiali da sole blu
			Tocca il polipo giallo e la bicicletta rossa
			Tocca il polipo giallo e la bicicletta verde
			Tocca il polipo giallo e la bicicletta gialla
			Tocca il polipo giallo e la bicicletta blu
			Tocca il polipo giallo e il monopattino rosso
			Tocca il polipo giallo e il monopattino verde
			Tocca il polipo giallo e il monopattino giallo
			Tocca il polipo giallo e il monopattino blu
			Tocca il polipo giallo e l'ombrello rosso
			Tocca il polipo giallo e l'ombrello verde
			Tocca il polipo giallo e l'ombrello giallo
			Tocca il polipo giallo e l'ombrello blu
			Tocca il polipo giallo e la bambina vestita in rosso
			Tocca il polipo giallo e la bambina vestita in verde
			Tocca il polipo giallo e la bambina vestita in giallo
			Tocca il polipo giallo e la bambina vestita in blu
			Tocca il polipo blu e il gelato rosso
			Tocca il polipo blu e il gelato verde
			Tocca il polipo blu e il gelato giallo
			Tocca il polipo blu e il gelato blu
			Tocca il polipo blu e gli occhiali da sole rossi
			Tocca il polipo blu e gli occhiali da sole verdi
			Tocca il polipo blu e gli occhiali da sole gialli
			Tocca il polipo blu e gli occhiali da sole blu
			Tocca il polipo blu e la bicicletta rossa
			Tocca il polipo blu e la bicicletta verde
			Tocca il polipo blu e la bicicletta gialla
			Tocca il polipo blu e la bicicletta blu
			Tocca il polipo blu e il monopattino rosso
			Tocca il polipo blu e il monopattino verde
			Tocca il polipo blu e il monopattino giallo
			Tocca il polipo blu e il monopattino blu
			Tocca il polipo blu e l'ombrello rosso
			Tocca il polipo blu e l'ombrello verde
			Tocca il polipo blu e l'ombrello giallo
			Tocca il polipo blu e l'ombrello blu
			Tocca il polipo blu e la bambina vestita in rosso
			Tocca il polipo blu e la bambina vestita in verde
			Tocca il polipo blu e la bambina vestita in giallo
			Tocca il polipo blu e la bambina vestita in blu
			Tocca il gelato rosso e il gelato verde
			Tocca il gelato rosso e il gelato giallo
			Tocca il gelato rosso e il gelato blu
			Tocca il gelato rosso e gli occhiali da sole rossi
			Tocca il gelato rosso e gli occhiali da sole verdi
			Tocca il gelato rosso e gli occhiali da sole gialli
			Tocca il gelato rosso e gli occhiali da sole blu
			Tocca il gelato rosso e la bicicletta rossa
			Tocca il gelato rosso e la bicicletta verde
			Tocca il gelato rosso e la bicicletta gialla
			Tocca il gelato rosso e la bicicletta blu
			Tocca il gelato rosso e il monopattino rosso
			Tocca il gelato rosso e il monopattino verde
			Tocca il gelato rosso e il monopattino giallo
			Tocca il gelato rosso e il monopattino blu
			Tocca il gelato rosso e l'ombrello rosso
			Tocca il gelato rosso e l'ombrello verde
			Tocca il gelato rosso e l'ombrello giallo
			Tocca il gelato rosso e l'ombrello blu
			Tocca il gelato rosso e la bambina vestita in rosso
			Tocca il gelato rosso e la bambina vestita in verde
			Tocca il gelato rosso e la bambina vestita in giallo
			Tocca il gelato rosso e la bambina vestita in blu
			Tocca il gelato verde e il gelato giallo
			Tocca il gelato verde e il gelato blu
			Tocca il gelato verde e gli occhiali da sole rossi
			Tocca il gelato verde e gli occhiali da sole verdi
			Tocca il gelato verde e gli occhiali da sole gialli
			Tocca il gelato verde e gli occhiali da sole blu
			Tocca il gelato verde e la bicicletta rossa
			Tocca il gelato verde e la bicicletta verde
			Tocca il gelato verde e la bicicletta gialla
			Tocca il gelato verde e la bicicletta blu
			Tocca il gelato verde e il monopattino rosso
			Tocca il gelato verde e il monopattino verde
			Tocca il gelato verde e il monopattino giallo
			Tocca il gelato verde e il monopattino blu
			Tocca il gelato verde e l'ombrello rosso
			Tocca il gelato verde e l'ombrello verde
			Tocca il gelato verde e l'ombrello giallo
			Tocca il gelato verde e l'ombrello blu
			Tocca il gelato verde e la bambina vestita in rosso
			Tocca il gelato verde e la bambina vestita in verde
			Tocca il gelato verde e la bambina vestita in giallo
			Tocca il gelato verde e la bambina vestita in blu
			Tocca il gelato giallo e il gelato blu
			Tocca il gelato giallo e gli occhiali da sole rossi
			Tocca il gelato giallo e gli occhiali da sole verdi
			Tocca il gelato giallo e gli occhiali da sole gialli
			Tocca il gelato giallo e gli occhiali da sole blu
			Tocca il gelato giallo e la bicicletta rossa
			Tocca il gelato giallo e la bicicletta verde
			Tocca il gelato giallo e la bicicletta gialla
			Tocca il gelato giallo e la bicicletta blu
			Tocca il gelato giallo e il monopattino rosso
			Tocca il gelato giallo e il monopattino verde
			Tocca il gelato giallo e il monopattino giallo
			Tocca il gelato giallo e il monopattino blu
			Tocca il gelato giallo e l'ombrello rosso
			Tocca il gelato giallo e l'ombrello verde
			Tocca il gelato giallo e l'ombrello giallo
			Tocca il gelato giallo e l'ombrello blu
			Tocca il gelato giallo e la bambina vestita in rosso
			Tocca il gelato giallo e la bambina vestita in verde
			Tocca il gelato giallo e la bambina vestita in giallo
			Tocca il gelato giallo e la bambina vestita in blu
			Tocca il gelato blu e gli occhiali da sole rossi
			Tocca il gelato blu e gli occhiali da sole verdi
			Tocca il gelato blu e gli occhiali da sole gialli
			Tocca il gelato blu e gli occhiali da sole blu
			Tocca il gelato blu e la bicicletta rossa
			Tocca il gelato blu e la bicicletta verde
			Tocca il gelato blu e la bicicletta gialla
			Tocca il gelato blu e la bicicletta blu
			Tocca il gelato blu e il monopattino rosso
			Tocca il gelato blu e il monopattino verde
			Tocca il gelato blu e il monopattino giallo
			Tocca il gelato blu e il monopattino blu
			Tocca il gelato blu e l'ombrello rosso
			Tocca il gelato blu e l'ombrello verde
			Tocca il gelato blu e l'ombrello giallo
			Tocca il gelato blu e l'ombrello blu
			Tocca il gelato blu e la bambina vestita in rosso
			Tocca il gelato blu e la bambina vestita in verde
			Tocca il gelato blu e la bambina vestita in giallo
			Tocca il gelato blu e la bambina vestita in blu
			Tocca gli occhiali da sole rossi e gli occhiali da sole verdi
			Tocca gli occhiali da sole rossi e gli occhiali da sole gialli
			Tocca gli occhiali da sole rossi e gli occhiali da sole blu
			Tocca gli occhiali da sole rossi e la bicicletta rossa
			Tocca gli occhiali da sole rossi e la bicicletta verde
			Tocca gli occhiali da sole rossi e la bicicletta gialla
			Tocca gli occhiali da sole rossi e la bicicletta blu
			Tocca gli occhiali da sole rossi e il monopattino rosso
			Tocca gli occhiali da sole rossi e il monopattino verde
			Tocca gli occhiali da sole rossi e il monopattino giallo
			Tocca gli occhiali da sole rossi e il monopattino blu
			Tocca gli occhiali da sole rossi e l'ombrello rosso
			Tocca gli occhiali da sole rossi e l'ombrello verde
			Tocca gli occhiali da sole rossi e l'ombrello giallo
			Tocca gli occhiali da sole rossi e l'ombrello blu
			Tocca gli occhiali da sole rossi e la bambina vestita in rosso
			Tocca gli occhiali da sole rossi e la bambina vestita in verde
			Tocca gli occhiali da sole rossi e la bambina vestita in giallo
			Tocca gli occhiali da sole rossi e la bambina vestita in blu
			Tocca gli occhiali da sole verdi e gli occhiali da sole gialli
			Tocca gli occhiali da sole verdi e gli occhiali da sole blu
			Tocca gli occhiali da sole verdi e la bicicletta rossa
			Tocca gli occhiali da sole verdi e la bicicletta verde
			Tocca gli occhiali da sole verdi e la bicicletta gialla
			Tocca gli occhiali da sole verdi e la bicicletta blu
			Tocca gli occhiali da sole verdi e il monopattino rosso
			Tocca gli occhiali da sole verdi e il monopattino verde
			Tocca gli occhiali da sole verdi e il monopattino giallo
			Tocca gli occhiali da sole verdi e il monopattino blu
			Tocca gli occhiali da sole verdi e l'ombrello rosso
			Tocca gli occhiali da sole verdi e l'ombrello verde
			Tocca gli occhiali da sole verdi e l'ombrello giallo
			Tocca gli occhiali da sole verdi e l'ombrello blu
			Tocca gli occhiali da sole verdi e la bambina vestita in rosso
			Tocca gli occhiali da sole verdi e la bambina vestita in verde
			Tocca gli occhiali da sole verdi e la bambina vestita in giallo
			Tocca gli occhiali da sole verdi e la bambina vestita in blu
			Tocca gli occhiali da sole gialli e gli occhiali da sole blu
			Tocca gli occhiali da sole gialli e la bicicletta rossa
			Tocca gli occhiali da sole gialli e la bicicletta verde
			Tocca gli occhiali da sole gialli e la bicicletta gialla
			Tocca gli occhiali da sole gialli e la bicicletta blu
			Tocca gli occhiali da sole gialli e il monopattino rosso
			Tocca gli occhiali da sole gialli e il monopattino verde
			Tocca gli occhiali da sole gialli e il monopattino giallo
			Tocca gli occhiali da sole gialli e il monopattino blu
			Tocca gli occhiali da sole gialli e l'ombrello rosso
			Tocca gli occhiali da sole gialli e l'ombrello verde
			Tocca gli occhiali da sole gialli e l'ombrello giallo
			Tocca gli occhiali da sole gialli e l'ombrello blu
			Tocca gli occhiali da sole gialli e la bambina vestita in rosso
			Tocca gli occhiali da sole gialli e la bambina vestita in verde
			Tocca gli occhiali da sole gialli e la bambina vestita in giallo
			Tocca gli occhiali da sole gialli e la bambina vestita in blu
			Tocca gli occhiali da sole blu e la bicicletta rossa
			Tocca gli occhiali da sole blu e la bicicletta verde
			Tocca gli occhiali da sole blu e la bicicletta gialla
			Tocca gli occhiali da sole blu e la bicicletta blu
			Tocca gli occhiali da sole blu e il monopattino rosso
			Tocca gli occhiali da sole blu e il monopattino verde
			Tocca gli occhiali da sole blu e il monopattino giallo
			Tocca gli occhiali da sole blu e il monopattino blu
			Tocca gli occhiali da sole blu e l'ombrello rosso
			Tocca gli occhiali da sole blu e l'ombrello verde
			Tocca gli occhiali da sole blu e l'ombrello giallo
			Tocca gli occhiali da sole blu e l'ombrello blu
			Tocca gli occhiali da sole blu e la bambina vestita in rosso
			Tocca gli occhiali da sole blu e la bambina vestita in verde
			Tocca gli occhiali da sole blu e la bambina vestita in giallo
			Tocca gli occhiali da sole blu e la bambina vestita in blu
			Tocca la bicicletta rossa e la bicicletta verde
			Tocca la bicicletta rossa e la bicicletta gialla
			Tocca la bicicletta rossa e la bicicletta blu
			Tocca la bicicletta rossa e il monopattino rosso
			Tocca la bicicletta rossa e il monopattino verde
			Tocca la bicicletta rossa e il monopattino giallo
			Tocca la bicicletta rossa e il monopattino blu
			Tocca la bicicletta rossa e l'ombrello rosso
			Tocca la bicicletta rossa e l'ombrello verde
			Tocca la bicicletta rossa e l'ombrello giallo
			Tocca la bicicletta rossa e l'ombrello blu
			Tocca la bicicletta rossa e la bambina vestita in rosso
			Tocca la bicicletta rossa e la bambina vestita in verde
			Tocca la bicicletta rossa e la bambina vestita in giallo
			Tocca la bicicletta rossa e la bambina vestita in blu
			Tocca la bicicletta verde e la bicicletta gialla
			Tocca la bicicletta verde e la bicicletta blu
			Tocca la bicicletta verde e il monopattino rosso
			Tocca la bicicletta verde e il monopattino verde
			Tocca la bicicletta verde e il monopattino giallo
			Tocca la bicicletta verde e il monopattino blu
			Tocca la bicicletta verde e l'ombrello rosso
			Tocca la bicicletta verde e l'ombrello verde
			Tocca la bicicletta verde e l'ombrello giallo
			Tocca la bicicletta verde e l'ombrello blu
			Tocca la bicicletta verde e la bambina vestita in rosso
			Tocca la bicicletta verde e la bambina vestita in verde
			Tocca la bicicletta verde e la bambina vestita in giallo
			Tocca la bicicletta verde e la bambina vestita in blu
			Tocca la bicicletta gialla e la bicicletta blu
			Tocca la bicicletta gialla e il monopattino rosso
			Tocca la bicicletta gialla e il monopattino verde
			Tocca la bicicletta gialla e il monopattino giallo
			Tocca la bicicletta gialla e il monopattino blu
			Tocca la bicicletta gialla e l'ombrello rosso
			Tocca la bicicletta gialla e l'ombrello verde
			Tocca la bicicletta gialla e l'ombrello giallo
			Tocca la bicicletta gialla e l'ombrello blu
			Tocca la bicicletta gialla e la bambina vestita in rosso
			Tocca la bicicletta gialla e la bambina vestita in verde
			Tocca la bicicletta gialla e la bambina vestita in giallo
			Tocca la bicicletta gialla e la bambina vestita in blu
			Tocca la bicicletta blu e il monopattino rosso
			Tocca la bicicletta blu e il monopattino verde
			Tocca la bicicletta blu e il monopattino giallo
			Tocca la bicicletta blu e il monopattino blu
			Tocca la bicicletta blu e l'ombrello rosso
			Tocca la bicicletta blu e l'ombrello verde
			Tocca la bicicletta blu e l'ombrello giallo
			Tocca la bicicletta blu e l'ombrello blu
			Tocca la bicicletta blu e la bambina vestita in rosso
			Tocca la bicicletta blu e la bambina vestita in verde
			Tocca la bicicletta blu e la bambina vestita in giallo
			Tocca la bicicletta blu e la bambina vestita in blu
			Tocca il monopattino rosso e il monopattino verde
			Tocca il monopattino rosso e il monopattino giallo
			Tocca il monopattino rosso e il monopattino blu
			Tocca il monopattino rosso e l'ombrello rosso
			Tocca il monopattino rosso e l'ombrello verde
			Tocca il monopattino rosso e l'ombrello giallo
			Tocca il monopattino rosso e l'ombrello blu
			Tocca il monopattino rosso e la bambina vestita in rosso
			Tocca il monopattino rosso e la bambina vestita in verde
			Tocca il monopattino rosso e la bambina vestita in giallo
			Tocca il monopattino rosso e la bambina vestita in blu
			Tocca il monopattino verde e il monopattino giallo
			Tocca il monopattino verde e il monopattino blu
			Tocca il monopattino verde e l'ombrello rosso
			Tocca il monopattino verde e l'ombrello verde
			Tocca il monopattino verde e l'ombrello giallo
			Tocca il monopattino verde e l'ombrello blu
			Tocca il monopattino verde e la bambina vestita in rosso
			Tocca il monopattino verde e la bambina vestita in verde
			Tocca il monopattino verde e la bambina vestita in giallo
			Tocca il monopattino verde e la bambina vestita in blu
			Tocca il monopattino giallo e il monopattino blu
			Tocca il monopattino giallo e l'ombrello rosso
			Tocca il monopattino giallo e l'ombrello verde
			Tocca il monopattino giallo e l'ombrello giallo
			Tocca il monopattino giallo e l'ombrello blu
			Tocca il monopattino giallo e la bambina vestita in rosso
			Tocca il monopattino giallo e la bambina vestita in verde
			Tocca il monopattino giallo e la bambina vestita in giallo
			Tocca il monopattino giallo e la bambina vestita in blu
			Tocca il monopattino blu e l'ombrello rosso
			Tocca il monopattino blu e l'ombrello verde
			Tocca il monopattino blu e l'ombrello giallo
			Tocca il monopattino blu e l'ombrello blu
			Tocca il monopattino blu e la bambina vestita in rosso
			Tocca il monopattino blu e la bambina vestita in verde
			Tocca il monopattino blu e la bambina vestita in giallo
			Tocca il monopattino blu e la bambina vestita in blu
			Tocca l'ombrello rosso e l'ombrello verde
			Tocca l'ombrello rosso e l'ombrello giallo
			Tocca l'ombrello rosso e l'ombrello blu
			Tocca l'ombrello rosso e la bambina vestita in rosso
			Tocca l'ombrello rosso e la bambina vestita in verde
			Tocca l'ombrello rosso e la bambina vestita in giallo
			Tocca l'ombrello rosso e la bambina vestita in blu
			Tocca l'ombrello verde e l'ombrello giallo
			Tocca l'ombrello verde e l'ombrello blu
			Tocca l'ombrello verde e la bambina vestita in rosso
			Tocca l'ombrello verde e la bambina vestita in verde
			Tocca l'ombrello verde e la bambina vestita in giallo
			Tocca l'ombrello verde e la bambina vestita in blu
			Tocca l'ombrello giallo e l'ombrello blu
			Tocca l'ombrello giallo e la bambina vestita in rosso
			Tocca l'ombrello giallo e la bambina vestita in verde
			Tocca l'ombrello giallo e la bambina vestita in giallo
			Tocca l'ombrello giallo e la bambina vestita in blu
			Tocca l'ombrello blu e la bambina vestita in rosso
			Tocca l'ombrello blu e la bambina vestita in verde
			Tocca l'ombrello blu e la bambina vestita in giallo
			Tocca l'ombrello blu e la bambina vestita in blu
			Tocca la bambina vestita in rosso e la bambina vestita in verde
			Tocca la bambina vestita in rosso e la bambina vestita in giallo
			Tocca la bambina vestita in rosso e la bambina vestita in blu
			Tocca la bambina vestita in verde e la bambina vestita in giallo
			Tocca la bambina vestita in verde e la bambina vestita in blu
			Tocca la bambina vestita in giallo e la bambina vestita in blu
		1FSUCODT = Find together subject and color double (two answers allowed here): => NOT HERE Nothing and places 
			Tocca insieme la scimmia rossa e la scimmia verde
			Tocca insieme la scimmia rossa e la scimmia gialla
			Tocca insieme la scimmia rossa e la scimmia blu
			Tocca insieme la scimmia rossa e il leone rosso
			Tocca insieme la scimmia rossa e il leone verde
			Tocca insieme la scimmia rossa e il leone giallo
			Tocca insieme la scimmia rossa e il leone blu
			Tocca insieme la scimmia rossa e il pappagallo rosso
			Tocca insieme la scimmia rossa e il pappagallo verde
			Tocca insieme la scimmia rossa e il pappagallo giallo
			Tocca insieme la scimmia rossa e il pappagallo blu
			Tocca insieme la scimmia rossa e il polipo rosso
			Tocca insieme la scimmia rossa e il polipo verde
			Tocca insieme la scimmia rossa e il polipo giallo
			Tocca insieme la scimmia rossa e il polipo blu
			Tocca insieme la scimmia rossa e il gelato rosso
			Tocca insieme la scimmia rossa e il gelato verde
			Tocca insieme la scimmia rossa e il gelato giallo
			Tocca insieme la scimmia rossa e il gelato blu
			Tocca insieme la scimmia rossa e gli occhiali da sole rossi
			Tocca insieme la scimmia rossa e gli occhiali da sole verdi
			Tocca insieme la scimmia rossa e gli occhiali da sole gialli
			Tocca insieme la scimmia rossa e gli occhiali da sole blu
			Tocca insieme la scimmia rossa e la bicicletta rossa
			Tocca insieme la scimmia rossa e la bicicletta verde
			Tocca insieme la scimmia rossa e la bicicletta gialla
			Tocca insieme la scimmia rossa e la bicicletta blu
			Tocca insieme la scimmia rossa e il monopattino rosso
			Tocca insieme la scimmia rossa e il monopattino verde
			Tocca insieme la scimmia rossa e il monopattino giallo
			Tocca insieme la scimmia rossa e il monopattino blu
			Tocca insieme la scimmia rossa e l'ombrello rosso
			Tocca insieme la scimmia rossa e l'ombrello verde
			Tocca insieme la scimmia rossa e l'ombrello giallo
			Tocca insieme la scimmia rossa e l'ombrello blu
			Tocca insieme la scimmia rossa e la bambina vestita in rosso
			Tocca insieme la scimmia rossa e la bambina vestita in verde
			Tocca insieme la scimmia rossa e la bambina vestita in giallo
			Tocca insieme la scimmia rossa e la bambina vestita in blu
			Tocca insieme la scimmia verde e la scimmia gialla
			Tocca insieme la scimmia verde e la scimmia blu
			Tocca insieme la scimmia verde e il leone rosso
			Tocca insieme la scimmia verde e il leone verde
			Tocca insieme la scimmia verde e il leone giallo
			Tocca insieme la scimmia verde e il leone blu
			Tocca insieme la scimmia verde e il pappagallo rosso
			Tocca insieme la scimmia verde e il pappagallo verde
			Tocca insieme la scimmia verde e il pappagallo giallo
			Tocca insieme la scimmia verde e il pappagallo blu
			Tocca insieme la scimmia verde e il polipo rosso
			Tocca insieme la scimmia verde e il polipo verde
			Tocca insieme la scimmia verde e il polipo giallo
			Tocca insieme la scimmia verde e il polipo blu
			Tocca insieme la scimmia verde e il gelato rosso
			Tocca insieme la scimmia verde e il gelato verde
			Tocca insieme la scimmia verde e il gelato giallo
			Tocca insieme la scimmia verde e il gelato blu
			Tocca insieme la scimmia verde e gli occhiali da sole rossi
			Tocca insieme la scimmia verde e gli occhiali da sole verdi
			Tocca insieme la scimmia verde e gli occhiali da sole gialli
			Tocca insieme la scimmia verde e gli occhiali da sole blu
			Tocca insieme la scimmia verde e la bicicletta rossa
			Tocca insieme la scimmia verde e la bicicletta verde
			Tocca insieme la scimmia verde e la bicicletta gialla
			Tocca insieme la scimmia verde e la bicicletta blu
			Tocca insieme la scimmia verde e il monopattino rosso
			Tocca insieme la scimmia verde e il monopattino verde
			Tocca insieme la scimmia verde e il monopattino giallo
			Tocca insieme la scimmia verde e il monopattino blu
			Tocca insieme la scimmia verde e l'ombrello rosso
			Tocca insieme la scimmia verde e l'ombrello verde
			Tocca insieme la scimmia verde e l'ombrello giallo
			Tocca insieme la scimmia verde e l'ombrello blu
			Tocca insieme la scimmia verde e la bambina vestita in rosso
			Tocca insieme la scimmia verde e la bambina vestita in verde
			Tocca insieme la scimmia verde e la bambina vestita in giallo
			Tocca insieme la scimmia verde e la bambina vestita in blu
			Tocca insieme la scimmia gialla e la scimmia blu
			Tocca insieme la scimmia gialla e il leone rosso
			Tocca insieme la scimmia gialla e il leone verde
			Tocca insieme la scimmia gialla e il leone giallo
			Tocca insieme la scimmia gialla e il leone blu
			Tocca insieme la scimmia gialla e il pappagallo rosso
			Tocca insieme la scimmia gialla e il pappagallo verde
			Tocca insieme la scimmia gialla e il pappagallo giallo
			Tocca insieme la scimmia gialla e il pappagallo blu
			Tocca insieme la scimmia gialla e il polipo rosso
			Tocca insieme la scimmia gialla e il polipo verde
			Tocca insieme la scimmia gialla e il polipo giallo
			Tocca insieme la scimmia gialla e il polipo blu
			Tocca insieme la scimmia gialla e il gelato rosso
			Tocca insieme la scimmia gialla e il gelato verde
			Tocca insieme la scimmia gialla e il gelato giallo
			Tocca insieme la scimmia gialla e il gelato blu
			Tocca insieme la scimmia gialla e gli occhiali da sole rossi
			Tocca insieme la scimmia gialla e gli occhiali da sole verdi
			Tocca insieme la scimmia gialla e gli occhiali da sole gialli
			Tocca insieme la scimmia gialla e gli occhiali da sole blu
			Tocca insieme la scimmia gialla e la bicicletta rossa
			Tocca insieme la scimmia gialla e la bicicletta verde
			Tocca insieme la scimmia gialla e la bicicletta gialla
			Tocca insieme la scimmia gialla e la bicicletta blu
			Tocca insieme la scimmia gialla e il monopattino rosso
			Tocca insieme la scimmia gialla e il monopattino verde
			Tocca insieme la scimmia gialla e il monopattino giallo
			Tocca insieme la scimmia gialla e il monopattino blu
			Tocca insieme la scimmia gialla e l'ombrello rosso
			Tocca insieme la scimmia gialla e l'ombrello verde
			Tocca insieme la scimmia gialla e l'ombrello giallo
			Tocca insieme la scimmia gialla e l'ombrello blu
			Tocca insieme la scimmia gialla e la bambina vestita in rosso
			Tocca insieme la scimmia gialla e la bambina vestita in verde
			Tocca insieme la scimmia gialla e la bambina vestita in giallo
			Tocca insieme la scimmia gialla e la bambina vestita in blu
			Tocca insieme la scimmia blu e il leone rosso
			Tocca insieme la scimmia blu e il leone verde
			Tocca insieme la scimmia blu e il leone giallo
			Tocca insieme la scimmia blu e il leone blu
			Tocca insieme la scimmia blu e il pappagallo rosso
			Tocca insieme la scimmia blu e il pappagallo verde
			Tocca insieme la scimmia blu e il pappagallo giallo
			Tocca insieme la scimmia blu e il pappagallo blu
			Tocca insieme la scimmia blu e il polipo rosso
			Tocca insieme la scimmia blu e il polipo verde
			Tocca insieme la scimmia blu e il polipo giallo
			Tocca insieme la scimmia blu e il polipo blu
			Tocca insieme la scimmia blu e il gelato rosso
			Tocca insieme la scimmia blu e il gelato verde
			Tocca insieme la scimmia blu e il gelato giallo
			Tocca insieme la scimmia blu e il gelato blu
			Tocca insieme la scimmia blu e gli occhiali da sole rossi
			Tocca insieme la scimmia blu e gli occhiali da sole verdi
			Tocca insieme la scimmia blu e gli occhiali da sole gialli
			Tocca insieme la scimmia blu e gli occhiali da sole blu
			Tocca insieme la scimmia blu e la bicicletta rossa
			Tocca insieme la scimmia blu e la bicicletta verde
			Tocca insieme la scimmia blu e la bicicletta gialla
			Tocca insieme la scimmia blu e la bicicletta blu
			Tocca insieme la scimmia blu e il monopattino rosso
			Tocca insieme la scimmia blu e il monopattino verde
			Tocca insieme la scimmia blu e il monopattino giallo
			Tocca insieme la scimmia blu e il monopattino blu
			Tocca insieme la scimmia blu e l'ombrello rosso
			Tocca insieme la scimmia blu e l'ombrello verde
			Tocca insieme la scimmia blu e l'ombrello giallo
			Tocca insieme la scimmia blu e l'ombrello blu
			Tocca insieme la scimmia blu e la bambina vestita in rosso
			Tocca insieme la scimmia blu e la bambina vestita in verde
			Tocca insieme la scimmia blu e la bambina vestita in giallo
			Tocca insieme la scimmia blu e la bambina vestita in blu
			Tocca insieme il leone rosso e il leone verde
			Tocca insieme il leone rosso e il leone giallo
			Tocca insieme il leone rosso e il leone blu
			Tocca insieme il leone rosso e il pappagallo rosso
			Tocca insieme il leone rosso e il pappagallo verde
			Tocca insieme il leone rosso e il pappagallo giallo
			Tocca insieme il leone rosso e il pappagallo blu
			Tocca insieme il leone rosso e il polipo rosso
			Tocca insieme il leone rosso e il polipo verde
			Tocca insieme il leone rosso e il polipo giallo
			Tocca insieme il leone rosso e il polipo blu
			Tocca insieme il leone rosso e il gelato rosso
			Tocca insieme il leone rosso e il gelato verde
			Tocca insieme il leone rosso e il gelato giallo
			Tocca insieme il leone rosso e il gelato blu
			Tocca insieme il leone rosso e gli occhiali da sole rossi
			Tocca insieme il leone rosso e gli occhiali da sole verdi
			Tocca insieme il leone rosso e gli occhiali da sole gialli
			Tocca insieme il leone rosso e gli occhiali da sole blu
			Tocca insieme il leone rosso e la bicicletta rossa
			Tocca insieme il leone rosso e la bicicletta verde
			Tocca insieme il leone rosso e la bicicletta gialla
			Tocca insieme il leone rosso e la bicicletta blu
			Tocca insieme il leone rosso e il monopattino rosso
			Tocca insieme il leone rosso e il monopattino verde
			Tocca insieme il leone rosso e il monopattino giallo
			Tocca insieme il leone rosso e il monopattino blu
			Tocca insieme il leone rosso e l'ombrello rosso
			Tocca insieme il leone rosso e l'ombrello verde
			Tocca insieme il leone rosso e l'ombrello giallo
			Tocca insieme il leone rosso e l'ombrello blu
			Tocca insieme il leone rosso e la bambina vestita in rosso
			Tocca insieme il leone rosso e la bambina vestita in verde
			Tocca insieme il leone rosso e la bambina vestita in giallo
			Tocca insieme il leone rosso e la bambina vestita in blu
			Tocca insieme il leone verde e il leone giallo
			Tocca insieme il leone verde e il leone blu
			Tocca insieme il leone verde e il pappagallo rosso
			Tocca insieme il leone verde e il pappagallo verde
			Tocca insieme il leone verde e il pappagallo giallo
			Tocca insieme il leone verde e il pappagallo blu
			Tocca insieme il leone verde e il polipo rosso
			Tocca insieme il leone verde e il polipo verde
			Tocca insieme il leone verde e il polipo giallo
			Tocca insieme il leone verde e il polipo blu
			Tocca insieme il leone verde e il gelato rosso
			Tocca insieme il leone verde e il gelato verde
			Tocca insieme il leone verde e il gelato giallo
			Tocca insieme il leone verde e il gelato blu
			Tocca insieme il leone verde e gli occhiali da sole rossi
			Tocca insieme il leone verde e gli occhiali da sole verdi
			Tocca insieme il leone verde e gli occhiali da sole gialli
			Tocca insieme il leone verde e gli occhiali da sole blu
			Tocca insieme il leone verde e la bicicletta rossa
			Tocca insieme il leone verde e la bicicletta verde
			Tocca insieme il leone verde e la bicicletta gialla
			Tocca insieme il leone verde e la bicicletta blu
			Tocca insieme il leone verde e il monopattino rosso
			Tocca insieme il leone verde e il monopattino verde
			Tocca insieme il leone verde e il monopattino giallo
			Tocca insieme il leone verde e il monopattino blu
			Tocca insieme il leone verde e l'ombrello rosso
			Tocca insieme il leone verde e l'ombrello verde
			Tocca insieme il leone verde e l'ombrello giallo
			Tocca insieme il leone verde e l'ombrello blu
			Tocca insieme il leone verde e la bambina vestita in rosso
			Tocca insieme il leone verde e la bambina vestita in verde
			Tocca insieme il leone verde e la bambina vestita in giallo
			Tocca insieme il leone verde e la bambina vestita in blu
			Tocca insieme il leone giallo e il leone blu
			Tocca insieme il leone giallo e il pappagallo rosso
			Tocca insieme il leone giallo e il pappagallo verde
			Tocca insieme il leone giallo e il pappagallo giallo
			Tocca insieme il leone giallo e il pappagallo blu
			Tocca insieme il leone giallo e il polipo rosso
			Tocca insieme il leone giallo e il polipo verde
			Tocca insieme il leone giallo e il polipo giallo
			Tocca insieme il leone giallo e il polipo blu
			Tocca insieme il leone giallo e il gelato rosso
			Tocca insieme il leone giallo e il gelato verde
			Tocca insieme il leone giallo e il gelato giallo
			Tocca insieme il leone giallo e il gelato blu
			Tocca insieme il leone giallo e gli occhiali da sole rossi
			Tocca insieme il leone giallo e gli occhiali da sole verdi
			Tocca insieme il leone giallo e gli occhiali da sole gialli
			Tocca insieme il leone giallo e gli occhiali da sole blu
			Tocca insieme il leone giallo e la bicicletta rossa
			Tocca insieme il leone giallo e la bicicletta verde
			Tocca insieme il leone giallo e la bicicletta gialla
			Tocca insieme il leone giallo e la bicicletta blu
			Tocca insieme il leone giallo e il monopattino rosso
			Tocca insieme il leone giallo e il monopattino verde
			Tocca insieme il leone giallo e il monopattino giallo
			Tocca insieme il leone giallo e il monopattino blu
			Tocca insieme il leone giallo e l'ombrello rosso
			Tocca insieme il leone giallo e l'ombrello verde
			Tocca insieme il leone giallo e l'ombrello giallo
			Tocca insieme il leone giallo e l'ombrello blu
			Tocca insieme il leone giallo e la bambina vestita in rosso
			Tocca insieme il leone giallo e la bambina vestita in verde
			Tocca insieme il leone giallo e la bambina vestita in giallo
			Tocca insieme il leone giallo e la bambina vestita in blu
			Tocca insieme il leone blu e il pappagallo rosso
			Tocca insieme il leone blu e il pappagallo verde
			Tocca insieme il leone blu e il pappagallo giallo
			Tocca insieme il leone blu e il pappagallo blu
			Tocca insieme il leone blu e il polipo rosso
			Tocca insieme il leone blu e il polipo verde
			Tocca insieme il leone blu e il polipo giallo
			Tocca insieme il leone blu e il polipo blu
			Tocca insieme il leone blu e il gelato rosso
			Tocca insieme il leone blu e il gelato verde
			Tocca insieme il leone blu e il gelato giallo
			Tocca insieme il leone blu e il gelato blu
			Tocca insieme il leone blu e gli occhiali da sole rossi
			Tocca insieme il leone blu e gli occhiali da sole verdi
			Tocca insieme il leone blu e gli occhiali da sole gialli
			Tocca insieme il leone blu e gli occhiali da sole blu
			Tocca insieme il leone blu e la bicicletta rossa
			Tocca insieme il leone blu e la bicicletta verde
			Tocca insieme il leone blu e la bicicletta gialla
			Tocca insieme il leone blu e la bicicletta blu
			Tocca insieme il leone blu e il monopattino rosso
			Tocca insieme il leone blu e il monopattino verde
			Tocca insieme il leone blu e il monopattino giallo
			Tocca insieme il leone blu e il monopattino blu
			Tocca insieme il leone blu e l'ombrello rosso
			Tocca insieme il leone blu e l'ombrello verde
			Tocca insieme il leone blu e l'ombrello giallo
			Tocca insieme il leone blu e l'ombrello blu
			Tocca insieme il leone blu e la bambina vestita in rosso
			Tocca insieme il leone blu e la bambina vestita in verde
			Tocca insieme il leone blu e la bambina vestita in giallo
			Tocca insieme il leone blu e la bambina vestita in blu
			Tocca insieme il pappagallo rosso e il pappagallo verde
			Tocca insieme il pappagallo rosso e il pappagallo giallo
			Tocca insieme il pappagallo rosso e il pappagallo blu
			Tocca insieme il pappagallo rosso e il polipo rosso
			Tocca insieme il pappagallo rosso e il polipo verde
			Tocca insieme il pappagallo rosso e il polipo giallo
			Tocca insieme il pappagallo rosso e il polipo blu
			Tocca insieme il pappagallo rosso e il gelato rosso
			Tocca insieme il pappagallo rosso e il gelato verde
			Tocca insieme il pappagallo rosso e il gelato giallo
			Tocca insieme il pappagallo rosso e il gelato blu
			Tocca insieme il pappagallo rosso e gli occhiali da sole rossi
			Tocca insieme il pappagallo rosso e gli occhiali da sole verdi
			Tocca insieme il pappagallo rosso e gli occhiali da sole gialli
			Tocca insieme il pappagallo rosso e gli occhiali da sole blu
			Tocca insieme il pappagallo rosso e la bicicletta rossa
			Tocca insieme il pappagallo rosso e la bicicletta verde
			Tocca insieme il pappagallo rosso e la bicicletta gialla
			Tocca insieme il pappagallo rosso e la bicicletta blu
			Tocca insieme il pappagallo rosso e il monopattino rosso
			Tocca insieme il pappagallo rosso e il monopattino verde
			Tocca insieme il pappagallo rosso e il monopattino giallo
			Tocca insieme il pappagallo rosso e il monopattino blu
			Tocca insieme il pappagallo rosso e l'ombrello rosso
			Tocca insieme il pappagallo rosso e l'ombrello verde
			Tocca insieme il pappagallo rosso e l'ombrello giallo
			Tocca insieme il pappagallo rosso e l'ombrello blu
			Tocca insieme il pappagallo rosso e la bambina vestita in rosso
			Tocca insieme il pappagallo rosso e la bambina vestita in verde
			Tocca insieme il pappagallo rosso e la bambina vestita in giallo
			Tocca insieme il pappagallo rosso e la bambina vestita in blu
			Tocca insieme il pappagallo verde e il pappagallo giallo
			Tocca insieme il pappagallo verde e il pappagallo blu
			Tocca insieme il pappagallo verde e il polipo rosso
			Tocca insieme il pappagallo verde e il polipo verde
			Tocca insieme il pappagallo verde e il polipo giallo
			Tocca insieme il pappagallo verde e il polipo blu
			Tocca insieme il pappagallo verde e il gelato rosso
			Tocca insieme il pappagallo verde e il gelato verde
			Tocca insieme il pappagallo verde e il gelato giallo
			Tocca insieme il pappagallo verde e il gelato blu
			Tocca insieme il pappagallo verde e gli occhiali da sole rossi
			Tocca insieme il pappagallo verde e gli occhiali da sole verdi
			Tocca insieme il pappagallo verde e gli occhiali da sole gialli
			Tocca insieme il pappagallo verde e gli occhiali da sole blu
			Tocca insieme il pappagallo verde e la bicicletta rossa
			Tocca insieme il pappagallo verde e la bicicletta verde
			Tocca insieme il pappagallo verde e la bicicletta gialla
			Tocca insieme il pappagallo verde e la bicicletta blu
			Tocca insieme il pappagallo verde e il monopattino rosso
			Tocca insieme il pappagallo verde e il monopattino verde
			Tocca insieme il pappagallo verde e il monopattino giallo
			Tocca insieme il pappagallo verde e il monopattino blu
			Tocca insieme il pappagallo verde e l'ombrello rosso
			Tocca insieme il pappagallo verde e l'ombrello verde
			Tocca insieme il pappagallo verde e l'ombrello giallo
			Tocca insieme il pappagallo verde e l'ombrello blu
			Tocca insieme il pappagallo verde e la bambina vestita in rosso
			Tocca insieme il pappagallo verde e la bambina vestita in verde
			Tocca insieme il pappagallo verde e la bambina vestita in giallo
			Tocca insieme il pappagallo verde e la bambina vestita in blu
			Tocca insieme il pappagallo giallo e il pappagallo blu
			Tocca insieme il pappagallo giallo e il polipo rosso
			Tocca insieme il pappagallo giallo e il polipo verde
			Tocca insieme il pappagallo giallo e il polipo giallo
			Tocca insieme il pappagallo giallo e il polipo blu
			Tocca insieme il pappagallo giallo e il gelato rosso
			Tocca insieme il pappagallo giallo e il gelato verde
			Tocca insieme il pappagallo giallo e il gelato giallo
			Tocca insieme il pappagallo giallo e il gelato blu
			Tocca insieme il pappagallo giallo e gli occhiali da sole rossi
			Tocca insieme il pappagallo giallo e gli occhiali da sole verdi
			Tocca insieme il pappagallo giallo e gli occhiali da sole gialli
			Tocca insieme il pappagallo giallo e gli occhiali da sole blu
			Tocca insieme il pappagallo giallo e la bicicletta rossa
			Tocca insieme il pappagallo giallo e la bicicletta verde
			Tocca insieme il pappagallo giallo e la bicicletta gialla
			Tocca insieme il pappagallo giallo e la bicicletta blu
			Tocca insieme il pappagallo giallo e il monopattino rosso
			Tocca insieme il pappagallo giallo e il monopattino verde
			Tocca insieme il pappagallo giallo e il monopattino giallo
			Tocca insieme il pappagallo giallo e il monopattino blu
			Tocca insieme il pappagallo giallo e l'ombrello rosso
			Tocca insieme il pappagallo giallo e l'ombrello verde
			Tocca insieme il pappagallo giallo e l'ombrello giallo
			Tocca insieme il pappagallo giallo e l'ombrello blu
			Tocca insieme il pappagallo giallo e la bambina vestita in rosso
			Tocca insieme il pappagallo giallo e la bambina vestita in verde
			Tocca insieme il pappagallo giallo e la bambina vestita in giallo
			Tocca insieme il pappagallo giallo e la bambina vestita in blu
			Tocca insieme il pappagallo blu e il polipo rosso
			Tocca insieme il pappagallo blu e il polipo verde
			Tocca insieme il pappagallo blu e il polipo giallo
			Tocca insieme il pappagallo blu e il polipo blu
			Tocca insieme il pappagallo blu e il gelato rosso
			Tocca insieme il pappagallo blu e il gelato verde
			Tocca insieme il pappagallo blu e il gelato giallo
			Tocca insieme il pappagallo blu e il gelato blu
			Tocca insieme il pappagallo blu e gli occhiali da sole rossi
			Tocca insieme il pappagallo blu e gli occhiali da sole verdi
			Tocca insieme il pappagallo blu e gli occhiali da sole gialli
			Tocca insieme il pappagallo blu e gli occhiali da sole blu
			Tocca insieme il pappagallo blu e la bicicletta rossa
			Tocca insieme il pappagallo blu e la bicicletta verde
			Tocca insieme il pappagallo blu e la bicicletta gialla
			Tocca insieme il pappagallo blu e la bicicletta blu
			Tocca insieme il pappagallo blu e il monopattino rosso
			Tocca insieme il pappagallo blu e il monopattino verde
			Tocca insieme il pappagallo blu e il monopattino giallo
			Tocca insieme il pappagallo blu e il monopattino blu
			Tocca insieme il pappagallo blu e l'ombrello rosso
			Tocca insieme il pappagallo blu e l'ombrello verde
			Tocca insieme il pappagallo blu e l'ombrello giallo
			Tocca insieme il pappagallo blu e l'ombrello blu
			Tocca insieme il pappagallo blu e la bambina vestita in rosso
			Tocca insieme il pappagallo blu e la bambina vestita in verde
			Tocca insieme il pappagallo blu e la bambina vestita in giallo
			Tocca insieme il pappagallo blu e la bambina vestita in blu
			Tocca insieme il polipo rosso e il polipo verde
			Tocca insieme il polipo rosso e il polipo giallo
			Tocca insieme il polipo rosso e il polipo blu
			Tocca insieme il polipo rosso e il gelato rosso
			Tocca insieme il polipo rosso e il gelato verde
			Tocca insieme il polipo rosso e il gelato giallo
			Tocca insieme il polipo rosso e il gelato blu
			Tocca insieme il polipo rosso e gli occhiali da sole rossi
			Tocca insieme il polipo rosso e gli occhiali da sole verdi
			Tocca insieme il polipo rosso e gli occhiali da sole gialli
			Tocca insieme il polipo rosso e gli occhiali da sole blu
			Tocca insieme il polipo rosso e la bicicletta rossa
			Tocca insieme il polipo rosso e la bicicletta verde
			Tocca insieme il polipo rosso e la bicicletta gialla
			Tocca insieme il polipo rosso e la bicicletta blu
			Tocca insieme il polipo rosso e il monopattino rosso
			Tocca insieme il polipo rosso e il monopattino verde
			Tocca insieme il polipo rosso e il monopattino giallo
			Tocca insieme il polipo rosso e il monopattino blu
			Tocca insieme il polipo rosso e l'ombrello rosso
			Tocca insieme il polipo rosso e l'ombrello verde
			Tocca insieme il polipo rosso e l'ombrello giallo
			Tocca insieme il polipo rosso e l'ombrello blu
			Tocca insieme il polipo rosso e la bambina vestita in rosso
			Tocca insieme il polipo rosso e la bambina vestita in verde
			Tocca insieme il polipo rosso e la bambina vestita in giallo
			Tocca insieme il polipo rosso e la bambina vestita in blu
			Tocca insieme il polipo verde e il polipo giallo
			Tocca insieme il polipo verde e il polipo blu
			Tocca insieme il polipo verde e la scimmia rossa
			Tocca insieme il polipo verde e la scimmia verde
			Tocca insieme il polipo verde e la scimmia gialla
			Tocca insieme il polipo verde e il scimmia blu
			Tocca insieme il polipo verde e il leone rosso
			Tocca insieme il polipo verde e il leone verde
			Tocca insieme il polipo verde e il leone giallo
			Tocca insieme il polipo verde e il leone blu
			Tocca insieme il polipo verde e il pappagallo rosso
			Tocca insieme il polipo verde e il pappagallo verde
			Tocca insieme il polipo verde e il pappagallo giallo
			Tocca insieme il polipo verde e il pappagallo blu
			Tocca insieme il polipo verde e il polipo giallo
			Tocca insieme il polipo verde e il polipo blu
			Tocca insieme il polipo verde e il gelato rosso
			Tocca insieme il polipo verde e il gelato verde
			Tocca insieme il polipo verde e il gelato giallo
			Tocca insieme il polipo verde e il gelato blu
			Tocca insieme il polipo verde e gli occhiali da sole rossi
			Tocca insieme il polipo verde e gli occhiali da sole verdi
			Tocca insieme il polipo verde e gli occhiali da sole gialli
			Tocca insieme il polipo verde e gli occhiali da sole blu
			Tocca insieme il polipo verde e la bicicletta rossa
			Tocca insieme il polipo verde e la bicicletta verde
			Tocca insieme il polipo verde e la bicicletta gialla
			Tocca insieme il polipo verde e la bicicletta blu
			Tocca insieme il polipo verde e il monopattino rosso
			Tocca insieme il polipo verde e il monopattino verde
			Tocca insieme il polipo verde e il monopattino giallo
			Tocca insieme il polipo verde e il monopattino blu
			Tocca insieme il polipo verde e l'ombrello rosso
			Tocca insieme il polipo verde e l'ombrello verde
			Tocca insieme il polipo verde e l'ombrello giallo
			Tocca insieme il polipo verde e l'ombrello blu
			Tocca insieme il polipo verde e la bambina vestita in rosso
			Tocca insieme il polipo verde e la bambina vestita in verde
			Tocca insieme il polipo verde e la bambina vestita in giallo
			Tocca insieme il polipo verde e la bambina vestita in blu
			Tocca insieme il polipo giallo e il polipo blu
			Tocca insieme il polipo giallo e il gelato rosso
			Tocca insieme il polipo giallo e il gelato verde
			Tocca insieme il polipo giallo e il gelato giallo
			Tocca insieme il polipo giallo e il gelato blu
			Tocca insieme il polipo giallo e gli occhiali da sole rossi
			Tocca insieme il polipo giallo e gli occhiali da sole verdi
			Tocca insieme il polipo giallo e gli occhiali da sole gialli
			Tocca insieme il polipo giallo e gli occhiali da sole blu
			Tocca insieme il polipo giallo e la bicicletta rossa
			Tocca insieme il polipo giallo e la bicicletta verde
			Tocca insieme il polipo giallo e la bicicletta gialla
			Tocca insieme il polipo giallo e la bicicletta blu
			Tocca insieme il polipo giallo e il monopattino rosso
			Tocca insieme il polipo giallo e il monopattino verde
			Tocca insieme il polipo giallo e il monopattino giallo
			Tocca insieme il polipo giallo e il monopattino blu
			Tocca insieme il polipo giallo e l'ombrello rosso
			Tocca insieme il polipo giallo e l'ombrello verde
			Tocca insieme il polipo giallo e l'ombrello giallo
			Tocca insieme il polipo giallo e l'ombrello blu
			Tocca insieme il polipo giallo e la bambina vestita in rosso
			Tocca insieme il polipo giallo e la bambina vestita in verde
			Tocca insieme il polipo giallo e la bambina vestita in giallo
			Tocca insieme il polipo giallo e la bambina vestita in blu
			Tocca insieme il polipo blu e il gelato rosso
			Tocca insieme il polipo blu e il gelato verde
			Tocca insieme il polipo blu e il gelato giallo
			Tocca insieme il polipo blu e il gelato blu
			Tocca insieme il polipo blu e gli occhiali da sole rossi
			Tocca insieme il polipo blu e gli occhiali da sole verdi
			Tocca insieme il polipo blu e gli occhiali da sole gialli
			Tocca insieme il polipo blu e gli occhiali da sole blu
			Tocca insieme il polipo blu e la bicicletta rossa
			Tocca insieme il polipo blu e la bicicletta verde
			Tocca insieme il polipo blu e la bicicletta gialla
			Tocca insieme il polipo blu e la bicicletta blu
			Tocca insieme il polipo blu e il monopattino rosso
			Tocca insieme il polipo blu e il monopattino verde
			Tocca insieme il polipo blu e il monopattino giallo
			Tocca insieme il polipo blu e il monopattino blu
			Tocca insieme il polipo blu e l'ombrello rosso
			Tocca insieme il polipo blu e l'ombrello verde
			Tocca insieme il polipo blu e l'ombrello giallo
			Tocca insieme il polipo blu e l'ombrello blu
			Tocca insieme il polipo blu e la bambina vestita in rosso
			Tocca insieme il polipo blu e la bambina vestita in verde
			Tocca insieme il polipo blu e la bambina vestita in giallo
			Tocca insieme il polipo blu e la bambina vestita in blu
			Tocca insieme il gelato rosso e il gelato verde
			Tocca insieme il gelato rosso e il gelato giallo
			Tocca insieme il gelato rosso e il gelato blu
			Tocca insieme il gelato rosso e gli occhiali da sole rossi
			Tocca insieme il gelato rosso e gli occhiali da sole verdi
			Tocca insieme il gelato rosso e gli occhiali da sole gialli
			Tocca insieme il gelato rosso e gli occhiali da sole blu
			Tocca insieme il gelato rosso e la bicicletta rossa
			Tocca insieme il gelato rosso e la bicicletta verde
			Tocca insieme il gelato rosso e la bicicletta gialla
			Tocca insieme il gelato rosso e la bicicletta blu
			Tocca insieme il gelato rosso e il monopattino rosso
			Tocca insieme il gelato rosso e il monopattino verde
			Tocca insieme il gelato rosso e il monopattino giallo
			Tocca insieme il gelato rosso e il monopattino blu
			Tocca insieme il gelato rosso e l'ombrello rosso
			Tocca insieme il gelato rosso e l'ombrello verde
			Tocca insieme il gelato rosso e l'ombrello giallo
			Tocca insieme il gelato rosso e l'ombrello blu
			Tocca insieme il gelato rosso e la bambina vestita in rosso
			Tocca insieme il gelato rosso e la bambina vestita in verde
			Tocca insieme il gelato rosso e la bambina vestita in giallo
			Tocca insieme il gelato rosso e la bambina vestita in blu
			Tocca insieme il gelato verde e il gelato giallo
			Tocca insieme il gelato verde e il gelato blu
			Tocca insieme il gelato verde e gli occhiali da sole rossi
			Tocca insieme il gelato verde e gli occhiali da sole verdi
			Tocca insieme il gelato verde e gli occhiali da sole gialli
			Tocca insieme il gelato verde e gli occhiali da sole blu
			Tocca insieme il gelato verde e la bicicletta rossa
			Tocca insieme il gelato verde e la bicicletta verde
			Tocca insieme il gelato verde e la bicicletta gialla
			Tocca insieme il gelato verde e la bicicletta blu
			Tocca insieme il gelato verde e il monopattino rosso
			Tocca insieme il gelato verde e il monopattino verde
			Tocca insieme il gelato verde e il monopattino giallo
			Tocca insieme il gelato verde e il monopattino blu
			Tocca insieme il gelato verde e l'ombrello rosso
			Tocca insieme il gelato verde e l'ombrello verde
			Tocca insieme il gelato verde e l'ombrello giallo
			Tocca insieme il gelato verde e l'ombrello blu
			Tocca insieme il gelato verde e la bambina vestita in rosso
			Tocca insieme il gelato verde e la bambina vestita in verde
			Tocca insieme il gelato verde e la bambina vestita in giallo
			Tocca insieme il gelato verde e la bambina vestita in blu
			Tocca insieme il gelato giallo e il gelato blu
			Tocca insieme il gelato giallo e gli occhiali da sole rossi
			Tocca insieme il gelato giallo e gli occhiali da sole verdi
			Tocca insieme il gelato giallo e gli occhiali da sole gialli
			Tocca insieme il gelato giallo e gli occhiali da sole blu
			Tocca insieme il gelato giallo e la bicicletta rossa
			Tocca insieme il gelato giallo e la bicicletta verde
			Tocca insieme il gelato giallo e la bicicletta gialla
			Tocca insieme il gelato giallo e la bicicletta blu
			Tocca insieme il gelato giallo e il monopattino rosso
			Tocca insieme il gelato giallo e il monopattino verde
			Tocca insieme il gelato giallo e il monopattino giallo
			Tocca insieme il gelato giallo e il monopattino blu
			Tocca insieme il gelato giallo e l'ombrello rosso
			Tocca insieme il gelato giallo e l'ombrello verde
			Tocca insieme il gelato giallo e l'ombrello giallo
			Tocca insieme il gelato giallo e l'ombrello blu
			Tocca insieme il gelato giallo e la bambina vestita in rosso
			Tocca insieme il gelato giallo e la bambina vestita in verde
			Tocca insieme il gelato giallo e la bambina vestita in giallo
			Tocca insieme il gelato giallo e la bambina vestita in blu
			Tocca insieme il gelato blu e gli occhiali da sole rossi
			Tocca insieme il gelato blu e gli occhiali da sole verdi
			Tocca insieme il gelato blu e gli occhiali da sole gialli
			Tocca insieme il gelato blu e gli occhiali da sole blu
			Tocca insieme il gelato blu e la bicicletta rossa
			Tocca insieme il gelato blu e la bicicletta verde
			Tocca insieme il gelato blu e la bicicletta gialla
			Tocca insieme il gelato blu e la bicicletta blu
			Tocca insieme il gelato blu e il monopattino rosso
			Tocca insieme il gelato blu e il monopattino verde
			Tocca insieme il gelato blu e il monopattino giallo
			Tocca insieme il gelato blu e il monopattino blu
			Tocca insieme il gelato blu e l'ombrello rosso
			Tocca insieme il gelato blu e l'ombrello verde
			Tocca insieme il gelato blu e l'ombrello giallo
			Tocca insieme il gelato blu e l'ombrello blu
			Tocca insieme il gelato blu e la bambina vestita in rosso
			Tocca insieme il gelato blu e la bambina vestita in verde
			Tocca insieme il gelato blu e la bambina vestita in giallo
			Tocca insieme il gelato blu e la bambina vestita in blu
			Tocca insieme gli occhiali da sole rossi e gli occhiali da sole verdi
			Tocca insieme gli occhiali da sole rossi e gli occhiali da sole gialli
			Tocca insieme gli occhiali da sole rossi e gli occhiali da sole blu
			Tocca insieme gli occhiali da sole rossi e la bicicletta rossa
			Tocca insieme gli occhiali da sole rossi e la bicicletta verde
			Tocca insieme gli occhiali da sole rossi e la bicicletta gialla
			Tocca insieme gli occhiali da sole rossi e la bicicletta blu
			Tocca insieme gli occhiali da sole rossi e il monopattino rosso
			Tocca insieme gli occhiali da sole rossi e il monopattino verde
			Tocca insieme gli occhiali da sole rossi e il monopattino giallo
			Tocca insieme gli occhiali da sole rossi e il monopattino blu
			Tocca insieme gli occhiali da sole rossi e l'ombrello rosso
			Tocca insieme gli occhiali da sole rossi e l'ombrello verde
			Tocca insieme gli occhiali da sole rossi e l'ombrello giallo
			Tocca insieme gli occhiali da sole rossi e l'ombrello blu
			Tocca insieme gli occhiali da sole rossi e la bambina vestita in rosso
			Tocca insieme gli occhiali da sole rossi e la bambina vestita in verde
			Tocca insieme gli occhiali da sole rossi e la bambina vestita in giallo
			Tocca insieme gli occhiali da sole rossi e la bambina vestita in blu
			Tocca insieme gli occhiali da sole verdi e gli occhiali da sole gialli
			Tocca insieme gli occhiali da sole verdi e gli occhiali da sole blu
			Tocca insieme gli occhiali da sole verdi e la bicicletta rossa
			Tocca insieme gli occhiali da sole verdi e la bicicletta verde
			Tocca insieme gli occhiali da sole verdi e la bicicletta gialla
			Tocca insieme gli occhiali da sole verdi e la bicicletta blu
			Tocca insieme gli occhiali da sole verdi e il monopattino rosso
			Tocca insieme gli occhiali da sole verdi e il monopattino verde
			Tocca insieme gli occhiali da sole verdi e il monopattino giallo
			Tocca insieme gli occhiali da sole verdi e il monopattino blu
			Tocca insieme gli occhiali da sole verdi e l'ombrello rosso
			Tocca insieme gli occhiali da sole verdi e l'ombrello verde
			Tocca insieme gli occhiali da sole verdi e l'ombrello giallo
			Tocca insieme gli occhiali da sole verdi e l'ombrello blu
			Tocca insieme gli occhiali da sole verdi e la bambina vestita in rosso
			Tocca insieme gli occhiali da sole verdi e la bambina vestita in verde
			Tocca insieme gli occhiali da sole verdi e la bambina vestita in giallo
			Tocca insieme gli occhiali da sole verdi e la bambina vestita in blu
			Tocca insieme gli occhiali da sole gialli e gli occhiali da sole blu
			Tocca insieme gli occhiali da sole gialli e la bicicletta rossa
			Tocca insieme gli occhiali da sole gialli e la bicicletta verde
			Tocca insieme gli occhiali da sole gialli e la bicicletta gialla
			Tocca insieme gli occhiali da sole gialli e la bicicletta blu
			Tocca insieme gli occhiali da sole gialli e il monopattino rosso
			Tocca insieme gli occhiali da sole gialli e il monopattino verde
			Tocca insieme gli occhiali da sole gialli e il monopattino giallo
			Tocca insieme gli occhiali da sole gialli e il monopattino blu
			Tocca insieme gli occhiali da sole gialli e l'ombrello rosso
			Tocca insieme gli occhiali da sole gialli e l'ombrello verde
			Tocca insieme gli occhiali da sole gialli e l'ombrello giallo
			Tocca insieme gli occhiali da sole gialli e l'ombrello blu
			Tocca insieme gli occhiali da sole gialli e la bambina vestita in rosso
			Tocca insieme gli occhiali da sole gialli e la bambina vestita in verde
			Tocca insieme gli occhiali da sole gialli e la bambina vestita in giallo
			Tocca insieme gli occhiali da sole gialli e la bambina vestita in blu
			Tocca insieme gli occhiali da sole blu e la bicicletta rossa
			Tocca insieme gli occhiali da sole blu e la bicicletta verde
			Tocca insieme gli occhiali da sole blu e la bicicletta gialla
			Tocca insieme gli occhiali da sole blu e la bicicletta blu
			Tocca insieme gli occhiali da sole blu e il monopattino rosso
			Tocca insieme gli occhiali da sole blu e il monopattino verde
			Tocca insieme gli occhiali da sole blu e il monopattino giallo
			Tocca insieme gli occhiali da sole blu e il monopattino blu
			Tocca insieme gli occhiali da sole blu e l'ombrello rosso
			Tocca insieme gli occhiali da sole blu e l'ombrello verde
			Tocca insieme gli occhiali da sole blu e l'ombrello giallo
			Tocca insieme gli occhiali da sole blu e l'ombrello blu
			Tocca insieme gli occhiali da sole blu e la bambina vestita in rosso
			Tocca insieme gli occhiali da sole blu e la bambina vestita in verde
			Tocca insieme gli occhiali da sole blu e la bambina vestita in giallo
			Tocca insieme gli occhiali da sole blu e la bambina vestita in blu
			Tocca insieme la bicicletta rossa e la bicicletta verde
			Tocca insieme la bicicletta rossa e la bicicletta gialla
			Tocca insieme la bicicletta rossa e la bicicletta blu
			Tocca insieme la bicicletta rossa e il monopattino rosso
			Tocca insieme la bicicletta rossa e il monopattino verde
			Tocca insieme la bicicletta rossa e il monopattino giallo
			Tocca insieme la bicicletta rossa e il monopattino blu
			Tocca insieme la bicicletta rossa e l'ombrello rosso
			Tocca insieme la bicicletta rossa e l'ombrello verde
			Tocca insieme la bicicletta rossa e l'ombrello giallo
			Tocca insieme la bicicletta rossa e l'ombrello blu
			Tocca insieme la bicicletta rossa e la bambina vestita in rosso
			Tocca insieme la bicicletta rossa e la bambina vestita in verde
			Tocca insieme la bicicletta rossa e la bambina vestita in giallo
			Tocca insieme la bicicletta rossa e la bambina vestita in blu
			Tocca insieme la bicicletta verde e la bicicletta gialla
			Tocca insieme la bicicletta verde e la bicicletta blu
			Tocca insieme la bicicletta verde e il monopattino rosso
			Tocca insieme la bicicletta verde e il monopattino verde
			Tocca insieme la bicicletta verde e il monopattino giallo
			Tocca insieme la bicicletta verde e il monopattino blu
			Tocca insieme la bicicletta verde e l'ombrello rosso
			Tocca insieme la bicicletta verde e l'ombrello verde
			Tocca insieme la bicicletta verde e l'ombrello giallo
			Tocca insieme la bicicletta verde e l'ombrello blu
			Tocca insieme la bicicletta verde e la bambina vestita in rosso
			Tocca insieme la bicicletta verde e la bambina vestita in verde
			Tocca insieme la bicicletta verde e la bambina vestita in giallo
			Tocca insieme la bicicletta verde e la bambina vestita in blu
			Tocca insieme la bicicletta gialla e la bicicletta blu
			Tocca insieme la bicicletta gialla e il monopattino rosso
			Tocca insieme la bicicletta gialla e il monopattino verde
			Tocca insieme la bicicletta gialla e il monopattino giallo
			Tocca insieme la bicicletta gialla e il monopattino blu
			Tocca insieme la bicicletta gialla e l'ombrello rosso
			Tocca insieme la bicicletta gialla e l'ombrello verde
			Tocca insieme la bicicletta gialla e l'ombrello giallo
			Tocca insieme la bicicletta gialla e l'ombrello blu
			Tocca insieme la bicicletta gialla e la bambina vestita in rosso
			Tocca insieme la bicicletta gialla e la bambina vestita in verde
			Tocca insieme la bicicletta gialla e la bambina vestita in giallo
			Tocca insieme la bicicletta gialla e la bambina vestita in blu
			Tocca insieme la bicicletta blu e il monopattino rosso
			Tocca insieme la bicicletta blu e il monopattino verde
			Tocca insieme la bicicletta blu e il monopattino giallo
			Tocca insieme la bicicletta blu e il monopattino blu
			Tocca insieme la bicicletta blu e l'ombrello rosso
			Tocca insieme la bicicletta blu e l'ombrello verde
			Tocca insieme la bicicletta blu e l'ombrello giallo
			Tocca insieme la bicicletta blu e l'ombrello blu
			Tocca insieme la bicicletta blu e la bambina vestita in rosso
			Tocca insieme la bicicletta blu e la bambina vestita in verde
			Tocca insieme la bicicletta blu e la bambina vestita in giallo
			Tocca insieme la bicicletta blu e la bambina vestita in blu
			Tocca insieme il monopattino rosso e il monopattino verde
			Tocca insieme il monopattino rosso e il monopattino giallo
			Tocca insieme il monopattino rosso e il monopattino blu
			Tocca insieme il monopattino rosso e l'ombrello rosso
			Tocca insieme il monopattino rosso e l'ombrello verde
			Tocca insieme il monopattino rosso e l'ombrello giallo
			Tocca insieme il monopattino rosso e l'ombrello blu
			Tocca insieme il monopattino rosso e la bambina vestita in rosso
			Tocca insieme il monopattino rosso e la bambina vestita in verde
			Tocca insieme il monopattino rosso e la bambina vestita in giallo
			Tocca insieme il monopattino rosso e la bambina vestita in blu
			Tocca insieme il monopattino verde e il monopattino giallo
			Tocca insieme il monopattino verde e il monopattino blu
			Tocca insieme il monopattino verde e l'ombrello rosso
			Tocca insieme il monopattino verde e l'ombrello verde
			Tocca insieme il monopattino verde e l'ombrello giallo
			Tocca insieme il monopattino verde e l'ombrello blu
			Tocca insieme il monopattino verde e la bambina vestita in rosso
			Tocca insieme il monopattino verde e la bambina vestita in verde
			Tocca insieme il monopattino verde e la bambina vestita in giallo
			Tocca insieme il monopattino verde e la bambina vestita in blu
			Tocca insieme il monopattino giallo e il monopattino blu
			Tocca insieme il monopattino giallo e l'ombrello rosso
			Tocca insieme il monopattino giallo e l'ombrello verde
			Tocca insieme il monopattino giallo e l'ombrello giallo
			Tocca insieme il monopattino giallo e l'ombrello blu
			Tocca insieme il monopattino giallo e la bambina vestita in rosso
			Tocca insieme il monopattino giallo e la bambina vestita in verde
			Tocca insieme il monopattino giallo e la bambina vestita in giallo
			Tocca insieme il monopattino giallo e la bambina vestita in blu
			Tocca insieme il monopattino blu e l'ombrello rosso
			Tocca insieme il monopattino blu e l'ombrello verde
			Tocca insieme il monopattino blu e l'ombrello giallo
			Tocca insieme il monopattino blu e l'ombrello blu
			Tocca insieme il monopattino blu e la bambina vestita in rosso
			Tocca insieme il monopattino blu e la bambina vestita in verde
			Tocca insieme il monopattino blu e la bambina vestita in giallo
			Tocca insieme il monopattino blu e la bambina vestita in blu
			Tocca insieme l'ombrello rosso e l'ombrello verde
			Tocca insieme l'ombrello rosso e l'ombrello giallo
			Tocca insieme l'ombrello rosso e l'ombrello blu
			Tocca insieme l'ombrello rosso e la bambina vestita in rosso
			Tocca insieme l'ombrello rosso e la bambina vestita in verde
			Tocca insieme l'ombrello rosso e la bambina vestita in giallo
			Tocca insieme l'ombrello rosso e la bambina vestita in blu
			Tocca insieme l'ombrello verde e l'ombrello giallo
			Tocca insieme l'ombrello verde e l'ombrello blu
			Tocca insieme l'ombrello verde e la bambina vestita in rosso
			Tocca insieme l'ombrello verde e la bambina vestita in verde
			Tocca insieme l'ombrello verde e la bambina vestita in giallo
			Tocca insieme l'ombrello verde e la bambina vestita in blu
			Tocca insieme l'ombrello giallo e l'ombrello blu
			Tocca insieme l'ombrello giallo e la bambina vestita in rosso
			Tocca insieme l'ombrello giallo e la bambina vestita in verde
			Tocca insieme l'ombrello giallo e la bambina vestita in giallo
			Tocca insieme l'ombrello giallo e la bambina vestita in blu
			Tocca insieme l'ombrello blu e la bambina vestita in rosso
			Tocca insieme l'ombrello blu e la bambina vestita in verde
			Tocca insieme l'ombrello blu e la bambina vestita in giallo
			Tocca insieme l'ombrello blu e la bambina vestita in blu
			Tocca insieme la bambina vestita in rosso e la bambina vestita in verde
			Tocca insieme la bambina vestita in rosso e la bambina vestita in giallo
			Tocca insieme la bambina vestita in rosso e la bambina vestita in blu
			Tocca insieme la bambina vestita in verde e la bambina vestita in giallo
			Tocca insieme la bambina vestita in verde e la bambina vestita in blu
			Tocca insieme la bambina vestita in giallo e la bambina vestita in blu
		1FGCA = Find category:
			Tocca tutti il bottone a tinta unita
			Tocca tutti gli animali
			Tocca tutti gli oggetti
			Tocca tutti i vestiti
			Tocca tutte le cose da mangiare
			Tocca tutte le persone
		1FCACO = Find category and color:
			Tocca un animale rosso
			Tocca un animale verde
			Tocca un animale giallo
			Tocca un animale blu
			Tocca un oggetto rosso
			Tocca un oggetto verde
			Tocca un oggetto giallo
			Tocca un oggetto blu
			Tocca un cibo rosso
			Tocca un cibo verde
			Tocca un cibo giallo
			Tocca un cibo blu
			Tocca un vestito rosso
			Tocca un vestito verde
			Tocca un vestito giallo
			Tocca un vestito blu
			Tocca una persona vestita di rosso
			Tocca una persona vestita di verde
			Tocca una persona vestita di giallo
			Tocca una persona vestita di blu
		1FCACOA = Find category and color all:
			Tocca tutti gli animali rossi
			Tocca tutti gli animali verdi
			Tocca tutti gli animali gialli
			Tocca tutti gli animali blu
			Tocca tutti gli oggetti rossi
			Tocca tutti gli oggetti verdi
			Tocca tutti gli oggetti gialli
			Tocca tutti gli oggetti blu
			Tocca tutti i cibi rossi
			Tocca tutti i cibi verdi
			Tocca tutti i cibi gialli
			Tocca tutti i cibi blu
			Tocca tutti i vestiti rossi
			Tocca tutti i vestiti verdi
			Tocca tutti i vestiti gialli
			Tocca tutti i vestiti blu
			Tocca tutte le persone vestite di rosso
			Tocca tutte le persone vestite di verde
			Tocca tutte le persone vestite di giallo
			Tocca tutte le persone vestite di blu
	######################################
	#2L = game_ColorLed
		2LST = ColorLed std:
			Tocca il colore come questo
		2LSU = ColorLed and subject:
			Tocca la scimmia di questo colore 
			Tocca il leone di questo colore 
			Tocca il pappagallo di questo colore 
			Tocca il polipo di questo colore 
			Tocca il gelato di questo colore 
			Tocca gli occhiali di questo colore 
			Tocca la bicicletta di questo colore 
			Tocca il monopattino di questo colore 
			Tocca l ombrello di questo colore 
			Tocca la bambina di questo colore 
		2LTWO1 = ColorLed two,but use first one:
			Tocca il secondo colore che ti ho fatto vedere
		2LTWO2 = ColorLed two,but use second one:
			Tocca il primo colore che ti ho fatto vedere
		2LDSU = ColorLED different subject:
			Tocca le scimmie se la luce è rossa
			Tocca le scimmie se la luce è verde
			Tocca le scimmie se la luce è gialla
			Tocca le scimmie se la luce è blu
			Tocca il leone se la luce è rossa
			Tocca il leone se la luce è verde
			Tocca il leone se la luce è gialla
			Tocca il leone se la luce è blu
			Tocca i pappagalli se la luce è rossa
			Tocca i pappagalli se la luce è verde
			Tocca i pappagalli se la luce è gialla
			Tocca i pappagalli se la luce è blu
			Tocca i polipi se la luce è rossa
			Tocca i polipi se la luce è verde
			Tocca i polipi se la luce è gialla
			Tocca i polipi se la luce è blu
			Tocca i gelati se la luce è rossa
			Tocca i gelati se la luce è verde
			Tocca i gelati se la luce è gialla
			Tocca i gelati se la luce è blu
			Tocca tutti gli occhiali da sole se la luce è rossa
			Tocca tutti gli occhiali da sole se la luce è verde
			Tocca tutti gli occhiali da sole se la luce è gialla
			Tocca tutti gli occhiali da sole se la luce è blu
			Tocca le biciclette se la luce è rossa
			Tocca le biciclette se la luce è verde
			Tocca le biciclette se la luce è gialla
			Tocca le biciclette se la luce è blu
			Tocca i monopattini se la luce è rossa
			Tocca i monopattini se la luce è verde
			Tocca i monopattini se la luce è gialla
			Tocca i monopattini se la luce è blu
			Tocca gli ombrelli se la luce è rossa
			Tocca gli ombrelli se la luce è verde
			Tocca gli ombrelli se la luce è gialla
			Tocca gli ombrelli se la luce è blu
			Tocca le bambine se la luce è rossa
			Tocca le bambine se la luce è verde
			Tocca le bambine se la luce è gialla
			Tocca le bambine se la luce è blu
		2LDSUOC = ColorLED different subject, otherwise capacitive:
			Tocca una scimmia se la luce è rossa altrimenti tocca un leone
			Tocca una scimmia se la luce è rossa altrimenti tocca un pappagallo
			Tocca una scimmia se la luce è rossa altrimenti tocca un polipo
			Tocca una scimmia se la luce è rossa altrimenti tocca un gelato
			Tocca una scimmia se la luce è rossa altrimenti tocca un paio di occhiali da sole
			Tocca una scimmia se la luce è rossa altrimenti tocca una bicicletta
			Tocca una scimmia se la luce è rossa altrimenti tocca un monopattino
			Tocca una scimmia se la luce è rossa altrimenti tocca un ombrello
			Tocca una scimmia se la luce è rossa altrimenti tocca una bambina
			Tocca una scimmia se la luce è verde altrimenti tocca un leone
			Tocca una scimmia se la luce è verde altrimenti tocca un pappagallo
			Tocca una scimmia se la luce è verde altrimenti tocca un polipo
			Tocca una scimmia se la luce è verde altrimenti tocca un gelato
			Tocca una scimmia se la luce è verde altrimenti tocca un paio di occhiali da sole
			Tocca una scimmia se la luce è verde altrimenti tocca una bicicletta
			Tocca una scimmia se la luce è verde altrimenti tocca un monopattino
			Tocca una scimmia se la luce è verde altrimenti tocca un ombrello
			Tocca una scimmia se la luce è verde altrimenti tocca una bambina
			Tocca una scimmia se la luce è gialla altrimenti tocca un leone
			Tocca una scimmia se la luce è gialla altrimenti tocca un pappagallo
			Tocca una scimmia se la luce è gialla altrimenti tocca un polipo
			Tocca una scimmia se la luce è gialla altrimenti tocca un gelato
			Tocca una scimmia se la luce è gialla altrimenti tocca un paio di occhiali da sole
			Tocca una scimmia se la luce è gialla altrimenti tocca una bicicletta
			Tocca una scimmia se la luce è gialla altrimenti tocca un monopattino
			Tocca una scimmia se la luce è gialla altrimenti tocca un ombrello
			Tocca una scimmia se la luce è gialla altrimenti tocca una bambina
			Tocca una scimmia se la luce è blu altrimenti tocca un leone
			Tocca una scimmia se la luce è blu altrimenti tocca un pappagallo
			Tocca una scimmia se la luce è blu altrimenti tocca un polipo
			Tocca una scimmia se la luce è blu altrimenti tocca un gelato
			Tocca una scimmia se la luce è blu altrimenti tocca un paio di occhiali da sole
			Tocca una scimmia se la luce è blu altrimenti tocca una bicicletta
			Tocca una scimmia se la luce è blu altrimenti tocca un monopattino
			Tocca una scimmia se la luce è blu altrimenti tocca un ombrello
			Tocca una scimmia se la luce è blu altrimenti tocca una bambina
			Tocca un leone se la luce è rossa altrimenti tocca una scimmia
			Tocca un leone se la luce è rossa altrimenti tocca un pappagallo
			Tocca un leone se la luce è rossa altrimenti tocca un polipo
			Tocca un leone se la luce è rossa altrimenti tocca un gelato
			Tocca un leone se la luce è rossa altrimenti tocca un paio di occhiali da sole
			Tocca un leone se la luce è rossa altrimenti tocca una bicicletta
			Tocca un leone se la luce è rossa altrimenti tocca un monopattino
			Tocca un leone se la luce è rossa altrimenti tocca un ombrello
			Tocca un leone se la luce è rossa altrimenti tocca una bambina
			Tocca un leone se la luce è verde altrimenti tocca una scimmia
			Tocca un leone se la luce è verde altrimenti tocca un pappagallo
			Tocca un leone se la luce è verde altrimenti tocca un polipo
			Tocca un leone se la luce è verde altrimenti tocca un gelato
			Tocca un leone se la luce è verde altrimenti tocca un paio di occhiali da sole
			Tocca un leone se la luce è verde altrimenti tocca una bicicletta
			Tocca un leone se la luce è verde altrimenti tocca un monopattino
			Tocca un leone se la luce è verde altrimenti tocca un ombrello
			Tocca un leone se la luce è verde altrimenti tocca una bambina
			Tocca un leone se la luce è gialla altrimenti tocca una scimmia
			Tocca un leone se la luce è gialla altrimenti tocca un pappagallo
			Tocca un leone se la luce è gialla altrimenti tocca un polipo
			Tocca un leone se la luce è gialla altrimenti tocca un gelato
			Tocca un leone se la luce è gialla altrimenti tocca un paio di occhiali da sole
			Tocca un leone se la luce è gialla altrimenti tocca una bicicletta
			Tocca un leone se la luce è gialla altrimenti tocca un monopattino
			Tocca un leone se la luce è gialla altrimenti tocca un ombrello
			Tocca un leone se la luce è gialla altrimenti tocca una bambina
			Tocca un leone se la luce è blu altrimenti tocca una scimmia
			Tocca un leone se la luce è blu altrimenti tocca un pappagallo
			Tocca un leone se la luce è blu altrimenti tocca un polipo
			Tocca un leone se la luce è blu altrimenti tocca un gelato
			Tocca un leone se la luce è blu altrimenti tocca un paio di occhiali da sole
			Tocca un leone se la luce è blu altrimenti tocca una bicicletta
			Tocca un leone se la luce è blu altrimenti tocca un monopattino
			Tocca un leone se la luce è blu altrimenti tocca un ombrello
			Tocca un leone se la luce è blu altrimenti tocca una bambina
			Tocca un pappagallo se la luce è rossa altrimenti tocca un leone
			Tocca un pappagallo se la luce è rossa altrimenti tocca una scimmia
			Tocca un pappagallo se la luce è rossa altrimenti tocca un polipo
			Tocca un pappagallo se la luce è rossa altrimenti tocca un gelato
			Tocca un pappagallo se la luce è rossa altrimenti tocca un paio di occhiali da sole
			Tocca un pappagallo se la luce è rossa altrimenti tocca una bicicletta
			Tocca un pappagallo se la luce è rossa altrimenti tocca un monopattino
			Tocca un pappagallo se la luce è rossa altrimenti tocca un ombrello
			Tocca un pappagallo se la luce è rossa altrimenti tocca una bambina
			Tocca un pappagallo se la luce è verde altrimenti tocca un leone
			Tocca un pappagallo se la luce è verde altrimenti tocca una scimmia
			Tocca un pappagallo se la luce è verde altrimenti tocca un polipo
			Tocca un pappagallo se la luce è verde altrimenti tocca un gelato
			Tocca un pappagallo se la luce è verde altrimenti tocca un paio di occhiali da sole
			Tocca un pappagallo se la luce è verde altrimenti tocca una bicicletta
			Tocca un pappagallo se la luce è verde altrimenti tocca un monopattino
			Tocca un pappagallo se la luce è verde altrimenti tocca un ombrello
			Tocca un pappagallo se la luce è verde altrimenti tocca una bambina
			Tocca un pappagallo se la luce è gialla altrimenti tocca un leone
			Tocca un pappagallo se la luce è gialla altrimenti tocca una scimmia
			Tocca un pappagallo se la luce è gialla altrimenti tocca un polipo
			Tocca un pappagallo se la luce è gialla altrimenti tocca un gelato
			Tocca un pappagallo se la luce è gialla altrimenti tocca un paio di occhiali da sole
			Tocca un pappagallo se la luce è gialla altrimenti tocca una bicicletta
			Tocca un pappagallo se la luce è gialla altrimenti tocca un monopattino
			Tocca un pappagallo se la luce è gialla altrimenti tocca un ombrello
			Tocca un pappagallo se la luce è gialla altrimenti tocca una bambina
			Tocca un pappagallo se la luce è blu altrimenti tocca un leone
			Tocca un pappagallo se la luce è blu altrimenti tocca una scimmia
			Tocca un pappagallo se la luce è blu altrimenti tocca un polipo
			Tocca un pappagallo se la luce è blu altrimenti tocca un gelato
			Tocca un pappagallo se la luce è blu altrimenti tocca un paio di occhiali da sole
			Tocca un pappagallo se la luce è blu altrimenti tocca una bicicletta
			Tocca un pappagallo se la luce è blu altrimenti tocca un monopattino
			Tocca un pappagallo se la luce è blu altrimenti tocca un ombrello
			Tocca un pappagallo se la luce è blu altrimenti tocca una bambina
			Tocca un polipo se la luce è rossa altrimenti tocca un leone
			Tocca un polipo se la luce è rossa altrimenti tocca una scimmia
			Tocca un polipo se la luce è rossa altrimenti tocca un pappagallo
			Tocca un polipo se la luce è rossa altrimenti tocca un gelato
			Tocca un polipo se la luce è rossa altrimenti tocca un paio di occhiali da sole
			Tocca un polipo se la luce è rossa altrimenti tocca una bicicletta
			Tocca un polipo se la luce è rossa altrimenti tocca un monopattino
			Tocca un polipo se la luce è rossa altrimenti tocca un ombrello
			Tocca un polipo se la luce è rossa altrimenti tocca una bambina
			Tocca un polipo se la luce è verde altrimenti tocca un leone
			Tocca un polipo se la luce è verde altrimenti tocca una scimmia
			Tocca un polipo se la luce è verde altrimenti tocca un pappagallo
			Tocca un polipo se la luce è verde altrimenti tocca un gelato
			Tocca un polipo se la luce è verde altrimenti tocca un paio di occhiali da sole
			Tocca un polipo se la luce è verde altrimenti tocca una bicicletta
			Tocca un polipo se la luce è verde altrimenti tocca un monopattino
			Tocca un polipo se la luce è verde altrimenti tocca un ombrello
			Tocca un polipo se la luce è verde altrimenti tocca una bambina
			Tocca un polipo se la luce è gialla altrimenti tocca un leone
			Tocca un polipo se la luce è gialla altrimenti tocca una scimmia
			Tocca un polipo se la luce è gialla altrimenti tocca un pappagallo
			Tocca un polipo se la luce è gialla altrimenti tocca un gelato
			Tocca un polipo se la luce è gialla altrimenti tocca un paio di occhiali da sole
			Tocca un polipo se la luce è gialla altrimenti tocca una bicicletta
			Tocca un polipo se la luce è gialla altrimenti tocca un monopattino
			Tocca un polipo se la luce è gialla altrimenti tocca un ombrello
			Tocca un polipo se la luce è gialla altrimenti tocca una bambina
			Tocca un polipo se la luce è blu altrimenti tocca un leone
			Tocca un polipo se la luce è blu altrimenti tocca una scimmia
			Tocca un polipo se la luce è blu altrimenti tocca un pappagallo
			Tocca un polipo se la luce è blu altrimenti tocca un gelato
			Tocca un polipo se la luce è blu altrimenti tocca un paio di occhiali da sole
			Tocca un polipo se la luce è blu altrimenti tocca una bicicletta
			Tocca un polipo se la luce è blu altrimenti tocca un monopattino
			Tocca un polipo se la luce è blu altrimenti tocca un ombrello
			Tocca un polipo se la luce è blu altrimenti tocca una bambina
			Tocca un gelato se la luce è rossa altrimenti tocca un leone
			Tocca un gelato se la luce è rossa altrimenti tocca una scimmia
			Tocca un gelato se la luce è rossa altrimenti tocca un pappagallo
			Tocca un gelato se la luce è rossa altrimenti tocca un polipo
			Tocca un gelato se la luce è rossa altrimenti tocca un paio di occhiali da sole
			Tocca un gelato se la luce è rossa altrimenti tocca una bicicletta
			Tocca un gelato se la luce è rossa altrimenti tocca un monopattino
			Tocca un gelato se la luce è rossa altrimenti tocca un ombrello
			Tocca un gelato se la luce è rossa altrimenti tocca una bambina
			Tocca un gelato se la luce è verde altrimenti tocca un leone
			Tocca un gelato se la luce è verde altrimenti tocca una scimmia
			Tocca un gelato se la luce è verde altrimenti tocca un pappagallo
			Tocca un gelato se la luce è verde altrimenti tocca un polipo
			Tocca un gelato se la luce è verde altrimenti tocca un paio di occhiali da sole
			Tocca un gelato se la luce è verde altrimenti tocca una bicicletta
			Tocca un gelato se la luce è verde altrimenti tocca un monopattino
			Tocca un gelato se la luce è verde altrimenti tocca un ombrello
			Tocca un gelato se la luce è verde altrimenti tocca una bambina
			Tocca un gelato se la luce è gialla altrimenti tocca un leone
			Tocca un gelato se la luce è gialla altrimenti tocca una scimmia
			Tocca un gelato se la luce è gialla altrimenti tocca un pappagallo
			Tocca un gelato se la luce è gialla altrimenti tocca un polipo
			Tocca un gelato se la luce è gialla altrimenti tocca un paio di occhiali da sole
			Tocca un gelato se la luce è gialla altrimenti tocca una bicicletta
			Tocca un gelato se la luce è gialla altrimenti tocca un monopattino
			Tocca un gelato se la luce è gialla altrimenti tocca un ombrello
			Tocca un gelato se la luce è gialla altrimenti tocca una bambina
			Tocca un gelato se la luce è blu altrimenti tocca un leone
			Tocca un gelato se la luce è blu altrimenti tocca una scimmia
			Tocca un gelato se la luce è blu altrimenti tocca un pappagallo
			Tocca un gelato se la luce è blu altrimenti tocca un polipo
			Tocca un gelato se la luce è blu altrimenti tocca un paio di occhiali da sole
			Tocca un gelato se la luce è blu altrimenti tocca una bicicletta
			Tocca un gelato se la luce è blu altrimenti tocca un monopattino
			Tocca un gelato se la luce è blu altrimenti tocca un ombrello
			Tocca un gelato se la luce è blu altrimenti tocca una bambina
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti tocca un leone
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti tocca una scimmia
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti tocca un pappagallo
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti tocca un polipo
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti tocca un gelato
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti tocca una bicicletta
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti tocca un monopattino
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti tocca un ombrello
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti tocca una bambina
			Tocca un paio di occhiali da sole se la luce è verde altrimenti tocca un leone
			Tocca un paio di occhiali da sole se la luce è verde altrimenti tocca una scimmia
			Tocca un paio di occhiali da sole se la luce è verde altrimenti tocca un pappagallo
			Tocca un paio di occhiali da sole se la luce è verde altrimenti tocca un polipo
			Tocca un paio di occhiali da sole se la luce è verde altrimenti tocca un gelato
			Tocca un paio di occhiali da sole se la luce è verde altrimenti tocca una bicicletta
			Tocca un paio di occhiali da sole se la luce è verde altrimenti tocca un monopattino
			Tocca un paio di occhiali da sole se la luce è verde altrimenti tocca un ombrello
			Tocca un paio di occhiali da sole se la luce è verde altrimenti tocca una bambina
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti tocca un leone
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti tocca una scimmia
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti tocca un pappagallo
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti tocca un polipo
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti tocca un gelato
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti tocca una bicicletta
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti tocca un monopattino
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti tocca un ombrello
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti tocca una bambina
			Tocca un paio di occhiali da sole se la luce è blu altrimenti tocca un leone
			Tocca un paio di occhiali da sole se la luce è blu altrimenti tocca una scimmia
			Tocca un paio di occhiali da sole se la luce è blu altrimenti tocca un pappagallo
			Tocca un paio di occhiali da sole se la luce è blu altrimenti tocca un polipo
			Tocca un paio di occhiali da sole se la luce è blu altrimenti tocca un gelato
			Tocca un paio di occhiali da sole se la luce è blu altrimenti tocca una bicicletta
			Tocca un paio di occhiali da sole se la luce è blu altrimenti tocca un monopattino
			Tocca un paio di occhiali da sole se la luce è blu altrimenti tocca un ombrello
			Tocca un paio di occhiali da sole se la luce è blu altrimenti tocca una bambina
			Tocca una bicicletta se la luce è rossa altrimenti tocca un leone
			Tocca una bicicletta se la luce è rossa altrimenti tocca una scimmia
			Tocca una bicicletta se la luce è rossa altrimenti tocca un pappagallo
			Tocca una bicicletta se la luce è rossa altrimenti tocca un polipo
			Tocca una bicicletta se la luce è rossa altrimenti tocca un gelato
			Tocca una bicicletta se la luce è rossa altrimenti tocca un paio di occhiali da sole
			Tocca una bicicletta se la luce è rossa altrimenti tocca un monopattino
			Tocca una bicicletta se la luce è rossa altrimenti tocca un ombrello
			Tocca una bicicletta se la luce è rossa altrimenti tocca una bambina
			Tocca una bicicletta se la luce è verde altrimenti tocca un leone
			Tocca una bicicletta se la luce è verde altrimenti tocca una scimmia
			Tocca una bicicletta se la luce è verde altrimenti tocca un pappagallo
			Tocca una bicicletta se la luce è verde altrimenti tocca un polipo
			Tocca una bicicletta se la luce è verde altrimenti tocca un gelato
			Tocca una bicicletta se la luce è verde altrimenti tocca un paio di occhiali da sole
			Tocca una bicicletta se la luce è verde altrimenti tocca un monopattino
			Tocca una bicicletta se la luce è verde altrimenti tocca un ombrello
			Tocca una bicicletta se la luce è verde altrimenti tocca una bambina
			Tocca una bicicletta se la luce è gialla altrimenti tocca un leone
			Tocca una bicicletta se la luce è gialla altrimenti tocca una scimmia
			Tocca una bicicletta se la luce è gialla altrimenti tocca un pappagallo
			Tocca una bicicletta se la luce è gialla altrimenti tocca un polipo
			Tocca una bicicletta se la luce è gialla altrimenti tocca un gelato
			Tocca una bicicletta se la luce è gialla altrimenti tocca un paio di occhiali da sole
			Tocca una bicicletta se la luce è gialla altrimenti tocca un monopattino
			Tocca una bicicletta se la luce è gialla altrimenti tocca un ombrello
			Tocca una bicicletta se la luce è gialla altrimenti tocca una bambina
			Tocca una bicicletta se la luce è blu altrimenti tocca un leone
			Tocca una bicicletta se la luce è blu altrimenti tocca una scimmia
			Tocca una bicicletta se la luce è blu altrimenti tocca un pappagallo
			Tocca una bicicletta se la luce è blu altrimenti tocca un polipo
			Tocca una bicicletta se la luce è blu altrimenti tocca un gelato
			Tocca una bicicletta se la luce è blu altrimenti tocca un paio di occhiali da sole
			Tocca una bicicletta se la luce è blu altrimenti tocca un monopattino
			Tocca una bicicletta se la luce è blu altrimenti tocca un ombrello
			Tocca una bicicletta se la luce è blu altrimenti tocca una bambina
			Tocca un monopattino se la luce è rossa altrimenti tocca un leone
			Tocca un monopattino se la luce è rossa altrimenti tocca una scimmia
			Tocca un monopattino se la luce è rossa altrimenti tocca un pappagallo
			Tocca un monopattino se la luce è rossa altrimenti tocca un polipo
			Tocca un monopattino se la luce è rossa altrimenti tocca un gelato
			Tocca un monopattino se la luce è rossa altrimenti tocca un paio di occhiali da sole
			Tocca un monopattino se la luce è rossa altrimenti tocca una bicicletta
			Tocca un monopattino se la luce è rossa altrimenti tocca un ombrello
			Tocca un monopattino se la luce è rossa altrimenti tocca una bambina
			Tocca un monopattino se la luce è verde altrimenti tocca un leone
			Tocca un monopattino se la luce è verde altrimenti tocca una scimmia
			Tocca un monopattino se la luce è verde altrimenti tocca un pappagallo
			Tocca un monopattino se la luce è verde altrimenti tocca un polipo
			Tocca un monopattino se la luce è verde altrimenti tocca un gelato
			Tocca un monopattino se la luce è verde altrimenti tocca un paio di occhiali da sole
			Tocca un monopattino se la luce è verde altrimenti tocca una bicicletta
			Tocca un monopattino se la luce è verde altrimenti tocca un ombrello
			Tocca un monopattino se la luce è verde altrimenti tocca una bambina
			Tocca un monopattino se la luce è gialla altrimenti tocca un leone
			Tocca un monopattino se la luce è gialla altrimenti tocca una scimmia
			Tocca un monopattino se la luce è gialla altrimenti tocca un pappagallo
			Tocca un monopattino se la luce è gialla altrimenti tocca un polipo
			Tocca un monopattino se la luce è gialla altrimenti tocca un gelato
			Tocca un monopattino se la luce è gialla altrimenti tocca un paio di occhiali da sole
			Tocca un monopattino se la luce è gialla altrimenti tocca una bicicletta
			Tocca un monopattino se la luce è gialla altrimenti tocca un ombrello
			Tocca un monopattino se la luce è gialla altrimenti tocca una bambina
			Tocca un monopattino se la luce è blu altrimenti tocca un leone
			Tocca un monopattino se la luce è blu altrimenti tocca una scimmia
			Tocca un monopattino se la luce è blu altrimenti tocca un pappagallo
			Tocca un monopattino se la luce è blu altrimenti tocca un polipo
			Tocca un monopattino se la luce è blu altrimenti tocca un gelato
			Tocca un monopattino se la luce è blu altrimenti tocca un paio di occhiali da sole
			Tocca un monopattino se la luce è blu altrimenti tocca una bicicletta
			Tocca un monopattino se la luce è blu altrimenti tocca un ombrello
			Tocca un monopattino se la luce è blu altrimenti tocca una bambina
			Tocca un ombrello se la luce è rossa altrimenti tocca un leone
			Tocca un ombrello se la luce è rossa altrimenti tocca una scimmia
			Tocca un ombrello se la luce è rossa altrimenti tocca un pappagallo
			Tocca un ombrello se la luce è rossa altrimenti tocca un polipo
			Tocca un ombrello se la luce è rossa altrimenti tocca un gelato
			Tocca un ombrello se la luce è rossa altrimenti tocca un paio di occhiali da sole
			Tocca un ombrello se la luce è rossa altrimenti tocca una bicicletta
			Tocca un ombrello se la luce è rossa altrimenti tocca un monopattino
			Tocca un ombrello se la luce è rossa altrimenti tocca una bambina
			Tocca un ombrello se la luce è verde altrimenti tocca un leone
			Tocca un ombrello se la luce è verde altrimenti tocca una scimmia
			Tocca un ombrello se la luce è verde altrimenti tocca un pappagallo
			Tocca un ombrello se la luce è verde altrimenti tocca un polipo
			Tocca un ombrello se la luce è verde altrimenti tocca un gelato
			Tocca un ombrello se la luce è verde altrimenti tocca un paio di occhiali da sole
			Tocca un ombrello se la luce è verde altrimenti tocca una bicicletta
			Tocca un ombrello se la luce è verde altrimenti tocca un monopattino
			Tocca un ombrello se la luce è verde altrimenti tocca una bambina
			Tocca un ombrello se la luce è gialla altrimenti tocca un leone
			Tocca un ombrello se la luce è gialla altrimenti tocca una scimmia
			Tocca un ombrello se la luce è gialla altrimenti tocca un pappagallo
			Tocca un ombrello se la luce è gialla altrimenti tocca un polipo
			Tocca un ombrello se la luce è gialla altrimenti tocca un gelato
			Tocca un ombrello se la luce è gialla altrimenti tocca un paio di occhiali da sole
			Tocca un ombrello se la luce è gialla altrimenti tocca una bicicletta
			Tocca un ombrello se la luce è gialla altrimenti tocca un monopattino
			Tocca un ombrello se la luce è gialla altrimenti tocca una bambina
			Tocca un ombrello se la luce è blu altrimenti tocca un leone
			Tocca un ombrello se la luce è blu altrimenti tocca una scimmia
			Tocca un ombrello se la luce è blu altrimenti tocca un pappagallo
			Tocca un ombrello se la luce è blu altrimenti tocca un polipo
			Tocca un ombrello se la luce è blu altrimenti tocca un gelato
			Tocca un ombrello se la luce è blu altrimenti tocca un paio di occhiali da sole
			Tocca un ombrello se la luce è blu altrimenti tocca una bicicletta
			Tocca un ombrello se la luce è blu altrimenti tocca un monopattino
			Tocca un ombrello se la luce è blu altrimenti tocca una bambina
			Tocca una bambina se la luce è rossa altrimenti tocca un leone
			Tocca una bambina se la luce è rossa altrimenti tocca una scimmia
			Tocca una bambina se la luce è rossa altrimenti tocca un pappagallo
			Tocca una bambina se la luce è rossa altrimenti tocca un polipo
			Tocca una bambina se la luce è rossa altrimenti tocca un gelato
			Tocca una bambina se la luce è rossa altrimenti tocca un paio di occhiali da sole
			Tocca una bambina se la luce è rossa altrimenti tocca una bicicletta
			Tocca una bambina se la luce è rossa altrimenti tocca un monopattino
			Tocca una bambina se la luce è rossa altrimenti tocca un ombrello
			Tocca una bambina se la luce è verde altrimenti tocca un leone
			Tocca una bambina se la luce è verde altrimenti tocca una scimmia
			Tocca una bambina se la luce è verde altrimenti tocca un pappagallo
			Tocca una bambina se la luce è verde altrimenti tocca un polipo
			Tocca una bambina se la luce è verde altrimenti tocca un gelato
			Tocca una bambina se la luce è verde altrimenti tocca un paio di occhiali da sole
			Tocca una bambina se la luce è verde altrimenti tocca una bicicletta
			Tocca una bambina se la luce è verde altrimenti tocca un monopattino
			Tocca una bambina se la luce è verde altrimenti tocca un ombrello
			Tocca una bambina se la luce è gialla altrimenti tocca un leone
			Tocca una bambina se la luce è gialla altrimenti tocca una scimmia
			Tocca una bambina se la luce è gialla altrimenti tocca un pappagallo
			Tocca una bambina se la luce è gialla altrimenti tocca un polipo
			Tocca una bambina se la luce è gialla altrimenti tocca un gelato
			Tocca una bambina se la luce è gialla altrimenti tocca un paio di occhiali da sole
			Tocca una bambina se la luce è gialla altrimenti tocca una bicicletta
			Tocca una bambina se la luce è gialla altrimenti tocca un monopattino
			Tocca una bambina se la luce è gialla altrimenti tocca un ombrello
			Tocca una bambina se la luce è blu altrimenti tocca un leone
			Tocca una bambina se la luce è blu altrimenti tocca una scimmia
			Tocca una bambina se la luce è blu altrimenti tocca un pappagallo
			Tocca una bambina se la luce è blu altrimenti tocca un polipo
			Tocca una bambina se la luce è blu altrimenti tocca un gelato
			Tocca una bambina se la luce è blu altrimenti tocca un paio di occhiali da sole
			Tocca una bambina se la luce è blu altrimenti tocca una bicicletta
			Tocca una bambina se la luce è blu altrimenti tocca un monopattino
			Tocca una bambina se la luce è blu altrimenti tocca un ombrello
		2LDBOC  = ColorLED body, otherwise capacitive:
			Toccami la testa se la luce è rossa altrimenti tocca la scimmia
			Toccami la testa se la luce è rossa altrimenti tocca il leone
			Toccami la testa se la luce è rossa altrimenti tocca il pappagallo
			Toccami la testa se la luce è rossa altrimenti tocca il polipo
			Toccami la testa se la luce è rossa altrimenti tocca il gelato
			Toccami la testa se la luce è rossa altrimenti tocca gli occhiali da sole
			Toccami la testa se la luce è rossa altrimenti tocca la bicicletta
			Toccami la testa se la luce è rossa altrimenti tocca il monopattino
			Toccami la testa se la luce è rossa altrimenti tocca l'ombrello
			Toccami la testa se la luce è rossa altrimenti tocca la bambina
			Toccami la testa se la luce è verde altrimenti tocca la scimmia
			Toccami la testa se la luce è verde altrimenti tocca il leone
			Toccami la testa se la luce è verde altrimenti tocca il pappagallo
			Toccami la testa se la luce è verde altrimenti tocca il polipo
			Toccami la testa se la luce è verde altrimenti tocca il gelato
			Toccami la testa se la luce è verde altrimenti tocca gli occhiali da sole
			Toccami la testa se la luce è verde altrimenti tocca la bicicletta
			Toccami la testa se la luce è verde altrimenti tocca il monopattino
			Toccami la testa se la luce è verde altrimenti tocca l'ombrello
			Toccami la testa se la luce è verde altrimenti tocca la bambina
			Toccami la testa se la luce è gialla altrimenti tocca la scimmia
			Toccami la testa se la luce è gialla altrimenti tocca il leone
			Toccami la testa se la luce è gialla altrimenti tocca il pappagallo
			Toccami la testa se la luce è gialla altrimenti tocca il polipo
			Toccami la testa se la luce è gialla altrimenti tocca il gelato
			Toccami la testa se la luce è gialla altrimenti tocca gli occhiali da sole
			Toccami la testa se la luce è gialla altrimenti tocca la bicicletta
			Toccami la testa se la luce è gialla altrimenti tocca il monopattino
			Toccami la testa se la luce è gialla altrimenti tocca l'ombrello
			Toccami la testa se la luce è gialla altrimenti tocca la bambina
			Toccami la testa se la luce è blu altrimenti tocca la scimmia
			Toccami la testa se la luce è blu altrimenti tocca il leone
			Toccami la testa se la luce è blu altrimenti tocca il pappagallo
			Toccami la testa se la luce è blu altrimenti tocca il polipo
			Toccami la testa se la luce è blu altrimenti tocca il gelato
			Toccami la testa se la luce è blu altrimenti tocca gli occhiali da sole
			Toccami la testa se la luce è blu altrimenti tocca la bicicletta
			Toccami la testa se la luce è blu altrimenti tocca il monopattino
			Toccami la testa se la luce è blu altrimenti tocca l'ombrello
			Toccami la testa se la luce è blu altrimenti tocca la bambina
			Toccami la pancia se la luce è rossa altrimenti tocca la scimmia
			Toccami la pancia se la luce è rossa altrimenti tocca il leone
			Toccami la pancia se la luce è rossa altrimenti tocca il pappagallo
			Toccami la pancia se la luce è rossa altrimenti tocca il polipo
			Toccami la pancia se la luce è rossa altrimenti tocca il gelato
			Toccami la pancia se la luce è rossa altrimenti tocca gli occhiali da sole
			Toccami la pancia se la luce è rossa altrimenti tocca la bicicletta
			Toccami la pancia se la luce è rossa altrimenti tocca il monopattino
			Toccami la pancia se la luce è rossa altrimenti tocca l'ombrello
			Toccami la pancia se la luce è rossa altrimenti tocca la bambina
			Toccami la pancia se la luce è verde altrimenti tocca la scimmia
			Toccami la pancia se la luce è verde altrimenti tocca il leone
			Toccami la pancia se la luce è verde altrimenti tocca il pappagallo
			Toccami la pancia se la luce è verde altrimenti tocca il polipo
			Toccami la pancia se la luce è verde altrimenti tocca il gelato
			Toccami la pancia se la luce è verde altrimenti tocca gli occhiali da sole
			Toccami la pancia se la luce è verde altrimenti tocca la bicicletta
			Toccami la pancia se la luce è verde altrimenti tocca il monopattino
			Toccami la pancia se la luce è verde altrimenti tocca l'ombrello
			Toccami la pancia se la luce è verde altrimenti tocca la bambina
			Toccami la pancia se la luce è gialla altrimenti tocca la scimmia
			Toccami la pancia se la luce è gialla altrimenti tocca il leone
			Toccami la pancia se la luce è gialla altrimenti tocca il pappagallo
			Toccami la pancia se la luce è gialla altrimenti tocca il polipo
			Toccami la pancia se la luce è gialla altrimenti tocca il gelato
			Toccami la pancia se la luce è gialla altrimenti tocca gli occhiali da sole
			Toccami la pancia se la luce è gialla altrimenti tocca la bicicletta
			Toccami la pancia se la luce è gialla altrimenti tocca il monopattino
			Toccami la pancia se la luce è gialla altrimenti tocca l'ombrello
			Toccami la pancia se la luce è gialla altrimenti tocca la bambina
			Toccami la pancia se la luce è blu altrimenti tocca la scimmia
			Toccami la pancia se la luce è blu altrimenti tocca il leone
			Toccami la pancia se la luce è blu altrimenti tocca il pappagallo
			Toccami la pancia se la luce è blu altrimenti tocca il polipo
			Toccami la pancia se la luce è blu altrimenti tocca il gelato
			Toccami la pancia se la luce è blu altrimenti tocca gli occhiali da sole
			Toccami la pancia se la luce è blu altrimenti tocca la bicicletta
			Toccami la pancia se la luce è blu altrimenti tocca il monopattino
			Toccami la pancia se la luce è blu altrimenti tocca l'ombrello
			Toccami la pancia se la luce è blu altrimenti tocca la bambina
		2LDSUOB = ColorLED different subject, otherwise body: (this time singular version only)
			Tocca una scimmia se la luce è rossa altrimenti toccami la testa
			Tocca una scimmia se la luce è rossa altrimenti toccami la pancia
			Tocca una scimmia se la luce è verde altrimenti toccami la testa
			Tocca una scimmia se la luce è verde altrimenti toccami la pancia
			Tocca una scimmia se la luce è gialla altrimenti toccami la testa
			Tocca una scimmia se la luce è gialla altrimenti toccami la pancia
			Tocca una scimmia se la luce è blu altrimenti toccami la testa
			Tocca una scimmia se la luce è blu altrimenti toccami la pancia
			Tocca un leone se la luce è rossa altrimenti toccami la testa
			Tocca un leone se la luce è rossa altrimenti toccami la pancia
			Tocca un leone se la luce è verde altrimenti toccami la testa
			Tocca un leone se la luce è verde altrimenti toccami la pancia
			Tocca un leone se la luce è gialla altrimenti toccami la testa
			Tocca un leone se la luce è gialla altrimenti toccami la pancia
			Tocca un leone se la luce è blu altrimenti toccami la testa
			Tocca un leone se la luce è blu altrimenti toccami la pancia
			Tocca un pappagallo se la luce è rossa altrimenti toccami la testa
			Tocca un pappagallo se la luce è rossa altrimenti toccami la pancia
			Tocca un pappagallo se la luce è verde altrimenti toccami la testa
			Tocca un pappagallo se la luce è verde altrimenti toccami la pancia
			Tocca un pappagallo se la luce è gialla altrimenti toccami la testa
			Tocca un pappagallo se la luce è gialla altrimenti toccami la pancia
			Tocca un pappagallo se la luce è blu altrimenti toccami la testa
			Tocca un pappagallo se la luce è blu altrimenti toccami la pancia
			Tocca un polipo se la luce è rossa altrimenti toccami la testa
			Tocca un polipo se la luce è rossa altrimenti toccami la pancia
			Tocca un polipo se la luce è verde altrimenti toccami la testa
			Tocca un polipo se la luce è verde altrimenti toccami la pancia
			Tocca un polipo se la luce è gialla altrimenti toccami la testa
			Tocca un polipo se la luce è gialla altrimenti toccami la pancia
			Tocca un polipo se la luce è blu altrimenti toccami la testa
			Tocca un polipo se la luce è blu altrimenti toccami la pancia
			Tocca il gelato se la luce è rossa altrimenti toccami la testa
			Tocca il gelato se la luce è rossa altrimenti toccami la pancia
			Tocca il gelato se la luce è verde altrimenti toccami la testa
			Tocca il gelato se la luce è verde altrimenti toccami la pancia
			Tocca il gelato se la luce è gialla altrimenti toccami la testa
			Tocca il gelato se la luce è gialla altrimenti toccami la pancia
			Tocca il gelato se la luce è blu altrimenti toccami la testa
			Tocca il gelato se la luce è blu altrimenti toccami la pancia
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti toccami la testa
			Tocca un paio di occhiali da sole se la luce è rossa altrimenti toccami la pancia
			Tocca un paio di occhiali da sole se la luce è verde altrimenti toccami la testa
			Tocca un paio di occhiali da sole se la luce è verde altrimenti toccami la pancia
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti toccami la testa
			Tocca un paio di occhiali da sole se la luce è gialla altrimenti toccami la pancia
			Tocca un paio di occhiali da sole se la luce è blu altrimenti toccami la testa
			Tocca un paio di occhiali da sole se la luce è blu altrimenti toccami la pancia
			Tocca una bicicletta se la luce è rossa altrimenti toccami la testa
			Tocca una bicicletta se la luce è rossa altrimenti toccami la pancia
			Tocca una bicicletta se la luce è verde altrimenti toccami la testa
			Tocca una bicicletta se la luce è verde altrimenti toccami la pancia
			Tocca una bicicletta se la luce è gialla altrimenti toccami la testa
			Tocca una bicicletta se la luce è gialla altrimenti toccami la pancia
			Tocca una bicicletta se la luce è blu altrimenti toccami la testa
			Tocca una bicicletta se la luce è blu altrimenti toccami la pancia
			Tocca un monopattino se la luce è rossa altrimenti toccami la testa
			Tocca un monopattino se la luce è rossa altrimenti toccami la pancia
			Tocca un monopattino se la luce è verde altrimenti toccami la testa
			Tocca un monopattino se la luce è verde altrimenti toccami la pancia
			Tocca un monopattino se la luce è gialla altrimenti toccami la testa
			Tocca un monopattino se la luce è gialla altrimenti toccami la pancia
			Tocca un monopattino se la luce è blu altrimenti toccami la testa
			Tocca un monopattino se la luce è blu altrimenti toccami la pancia
			Tocca un ombrello se la luce è rossa altrimenti toccami la testa
			Tocca un ombrello se la luce è rossa altrimenti toccami la pancia
			Tocca un ombrello se la luce è verde altrimenti toccami la testa
			Tocca un ombrello se la luce è verde altrimenti toccami la pancia
			Tocca un ombrello se la luce è gialla altrimenti toccami la testa
			Tocca un ombrello se la luce è gialla altrimenti toccami la pancia
			Tocca un ombrello se la luce è blu altrimenti toccami la testa
			Tocca un ombrello se la luce è blu altrimenti toccami la pancia
			Tocca la bambina se la luce è rossa altrimenti toccami la testa
			Tocca la bambina se la luce è rossa altrimenti toccami la pancia
			Tocca la bambina se la luce è verde altrimenti toccami la testa
			Tocca la bambina se la luce è verde altrimenti toccami la pancia
			Tocca la bambina se la luce è gialla altrimenti toccami la testa
			Tocca la bambina se la luce è gialla altrimenti toccami la pancia
			Tocca la bambina se la luce è blu altrimenti toccami la testa
			Tocca la bambina se la luce è blu altrimenti toccami la pancia
		2LDBOB  = ColorLED body, otherwise body:
			Toccami la testa se la luce è rossa altrimenti toccami la pancia
			Toccami la testa se la luce è verde altrimenti toccami la pancia
			Toccami la testa se la luce è gialla altrimenti toccami la pancia
			Toccami la testa se la luce è blu altrimenti toccami la pancia
			Toccami la pancia se la luce è rossa altrimenti toccami la testa
			Toccami la pancia se la luce è verde altrimenti toccami la testa
			Toccami la pancia se la luce è gialla altrimenti toccami la testa
			Toccami la pancia se la luce è blu altrimenti toccami la testa
		2LDSU2 = ColorLED two colors different subject:
			Tocca la scimmia se divento rosso oppure tocca il leone se divento giallo
			Tocca la scimmia se divento rosso oppure tocca il leone se divento verde
			Tocca la scimmia se divento rosso oppure tocca il leone se divento blu
			Tocca la scimmia se divento verde oppure tocca il leone se divento rosso
			Tocca la scimmia se divento verde oppure tocca il leone se divento giallo
			Tocca la scimmia se divento verde oppure tocca il leone se divento blu
			Tocca la scimmia se divento giallo oppure tocca il leone se divento rosso
			Tocca la scimmia se divento giallo oppure tocca il leone se divento giallo
			Tocca la scimmia se divento giallo oppure tocca il leone se divento blu
			Tocca la scimmia se divento blu oppure tocca il leone se divento rosso
			Tocca la scimmia se divento blu oppure tocca il leone se divento giallo
			Tocca la scimmia se divento blu oppure tocca il leone se divento verde
			Tocca la scimmia se divento rosso oppure tocca il pappagallo se divento giallo
			Tocca la scimmia se divento rosso oppure tocca il pappagallo se divento verde
			Tocca la scimmia se divento rosso oppure tocca il pappagallo se compare il colore blugiallo
			Tocca la scimmia se divento verde oppure tocca il pappagallo se divento rosso
			Tocca la scimmia se divento verde oppure tocca il pappagallo se divento giallo
			Tocca la scimmia se divento verde oppure tocca il pappagallo se divento blu
			Tocca la scimmia se divento giallo oppure tocca il pappagallo se divento rosso
			Tocca la scimmia se divento giallo oppure tocca il pappagallo se divento giallo
			Tocca la scimmia se divento giallo oppure tocca il pappagallo se divento blu
			Tocca la scimmia se divento blu oppure tocca il pappagallo se divento rosso
			Tocca la scimmia se divento blu oppure tocca il pappagallo se divento giallo
			Tocca la scimmia se divento blu oppure tocca il pappagallo se divento verde
			Tocca la scimmia se divento rosso oppure tocca il polipo se divento giallo
			Tocca la scimmia se divento rosso oppure tocca il polipo se divento verde
			Tocca la scimmia se divento rosso oppure tocca il polipo se divento blu
			Tocca la scimmia se divento verde oppure tocca il polipo se divento rosso
			Tocca la scimmia se divento verde oppure tocca il polipo se divento giallo
			Tocca la scimmia se divento verde oppure tocca il polipo se divento blu
			Tocca la scimmia se divento giallo oppure tocca il polipo se divento rosso
			Tocca la scimmia se divento giallo oppure tocca il polipo se divento giallo
			Tocca la scimmia se divento giallo oppure tocca il polipo se divento blu
			Tocca la scimmia se divento blu oppure tocca il polipo se divento rosso
			Tocca la scimmia se divento blu oppure tocca il polipo se divento giallo
			Tocca la scimmia se divento blu oppure tocca il polipo se divento verde
			Tocca la scimmia se divento rosso oppure tocca il gelato se divento giallo
			Tocca la scimmia se divento rosso oppure tocca il gelato se divento verde
			Tocca la scimmia se divento rosso oppure tocca il gelato se divento blu
			Tocca la scimmia se divento verde oppure tocca il gelato se divento rosso
			Tocca la scimmia se divento verde oppure tocca il gelato se divento giallo
			Tocca la scimmia se divento verde oppure tocca il gelato se divento blu
			Tocca la scimmia se divento giallo oppure tocca il gelato se divento rosso
			Tocca la scimmia se divento giallo oppure tocca il gelato se divento giallo
			Tocca la scimmia se divento giallo oppure tocca il gelato se divento blu
			Tocca la scimmia se divento blu oppure tocca il gelato se divento rosso
			Tocca la scimmia se divento blu oppure tocca il gelato se divento giallo
			Tocca la scimmia se divento blu oppure tocca il gelato se divento verde
			Tocca la scimmia se divento rosso oppure tocca la bambina se divento giallo
			Tocca la scimmia se divento rosso oppure tocca la bambina se divento verde
			Tocca la scimmia se divento rosso oppure tocca la bambina se divento blu
			Tocca la scimmia se divento verde oppure tocca la bambina se divento rosso
			Tocca la scimmia se divento verde oppure tocca la bambina se divento giallo
			Tocca la scimmia se divento verde oppure tocca la bambina se divento blu
			Tocca la scimmia se divento giallo oppure tocca la bambina se divento rosso
			Tocca la scimmia se divento giallo oppure tocca la bambina se divento giallo
			Tocca la scimmia se divento giallo oppure tocca la bambina se divento blu
			Tocca la scimmia se divento blu oppure tocca la bambina se divento rosso
			Tocca la scimmia se divento blu oppure tocca la bambina se divento giallo
			Tocca la scimmia se divento blu oppure tocca la bambina se divento verde
			Tocca la scimmia se divento rosso oppure tocca gli occhiali da sole se divento giallo
			Tocca la scimmia se divento rosso oppure tocca gli occhiali da sole se divento verde
			Tocca la scimmia se divento rosso oppure tocca gli occhiali da sole se divento blu
			Tocca la scimmia se divento verde oppure tocca gli occhiali da sole se divento rosso
			Tocca la scimmia se divento verde oppure tocca gli occhiali da sole se divento giallo
			Tocca la scimmia se divento verde oppure tocca gli occhiali da sole se divento blu
			Tocca la scimmia se divento giallo oppure tocca gli occhiali da sole se divento rosso
			Tocca la scimmia se divento giallo oppure tocca gli occhiali da sole se divento giallo
			Tocca la scimmia se divento giallo oppure tocca gli occhiali da sole se divento blu
			Tocca la scimmia se divento blu oppure tocca gli occhiali da sole se divento rosso
			Tocca la scimmia se divento blu oppure tocca gli occhiali da sole se divento giallo
			Tocca la scimmia se divento blu oppure tocca gli occhiali da sole se divento verde
			Tocca la scimmia se divento rosso oppure tocca la bicicletta se divento giallo
			Tocca la scimmia se divento rosso oppure tocca la bicicletta se divento verde
			Tocca la scimmia se divento rosso oppure tocca la bicicletta se divento blu
			Tocca la scimmia se divento verde oppure tocca la bicicletta se divento rosso
			Tocca la scimmia se divento verde oppure tocca la bicicletta se divento giallo
			Tocca la scimmia se divento verde oppure tocca la bicicletta se divento blu
			Tocca la scimmia se divento giallo oppure tocca la bicicletta se divento rosso
			Tocca la scimmia se divento giallo oppure tocca la bicicletta se divento giallo
			Tocca la scimmia se divento giallo oppure tocca la bicicletta se divento blu
			Tocca la scimmia se divento blu oppure tocca la bicicletta se divento rosso
			Tocca la scimmia se divento blu oppure tocca la bicicletta se divento giallo
			Tocca la scimmia se divento blu oppure tocca la bicicletta se divento verde
			Tocca la scimmia se divento rosso oppure tocca il monopattino se divento giallo
			Tocca la scimmia se divento rosso oppure tocca il monopattino se divento verde
			Tocca la scimmia se divento rosso oppure tocca il monopattino se divento blu
			Tocca la scimmia se divento verde oppure tocca il monopattino se divento rosso
			Tocca la scimmia se divento verde oppure tocca il monopattino se divento giallo
			Tocca la scimmia se divento verde oppure tocca il monopattino se divento blu
			Tocca la scimmia se divento giallo oppure tocca il monopattino se divento rosso
			Tocca la scimmia se divento giallo oppure tocca il monopattino se divento giallo
			Tocca la scimmia se divento giallo oppure tocca il monopattino se divento blu
			Tocca la scimmia se divento blu oppure tocca il monopattino se divento rosso
			Tocca la scimmia se divento blu oppure tocca il monopattino se divento giallo
			Tocca la scimmia se divento blu oppure tocca il monopattino se divento verde
			Tocca la scimmia se divento rosso oppure tocca l'ombrello se divento giallo
			Tocca la scimmia se divento rosso oppure tocca l'ombrello se divento verde
			Tocca la scimmia se divento rosso oppure tocca l'ombrello se divento blu
			Tocca la scimmia se divento verde oppure tocca l'ombrello se divento rosso
			Tocca la scimmia se divento verde oppure tocca l'ombrello se divento giallo
			Tocca la scimmia se divento verde oppure tocca l'ombrello se divento blu
			Tocca la scimmia se divento giallo oppure tocca l'ombrello se divento rosso
			Tocca la scimmia se divento giallo oppure tocca l'ombrello se divento giallo
			Tocca la scimmia se divento giallo oppure tocca l'ombrello se divento blu
			Tocca la scimmia se divento blu oppure tocca l'ombrello se divento rosso
			Tocca la scimmia se divento blu oppure tocca l'ombrello se divento giallo
			Tocca la scimmia se divento blu oppure tocca l'ombrello se divento verde
			Tocca il leone se divento rosso oppure tocca il pappagallo se divento giallo
			Tocca il leone se divento rosso oppure tocca il pappagallo se divento verde
			Tocca il leone se divento rosso oppure tocca il pappagallo se divento blu
			Tocca il leone se divento verde oppure tocca il pappagallo se divento rosso
			Tocca il leone se divento verde oppure tocca il pappagallo se divento giallo
			Tocca il leone se divento verde oppure tocca il pappagallo se divento blu
			Tocca il leone se divento giallo oppure tocca il pappagallo se divento rosso
			Tocca il leone se divento giallo oppure tocca il pappagallo se divento giallo
			Tocca il leone se divento giallo oppure tocca il pappagallo se divento blu
			Tocca il leone se divento blu oppure tocca il pappagallo se divento rosso
			Tocca il leone se divento blu oppure tocca il pappagallo se divento giallo
			Tocca il leone se divento blu oppure tocca il pappagallo se divento verde
			Tocca il leone se divento rosso oppure tocca il polipo se divento giallo
			Tocca il leone se divento rosso oppure tocca il polipo se divento verde
			Tocca il leone se divento rosso oppure tocca il polipo se divento blu
			Tocca il leone se divento verde oppure tocca il polipo se divento rosso
			Tocca il leone se divento verde oppure tocca il polipo se divento giallo
			Tocca il leone se divento verde oppure tocca il polipo se divento blu
			Tocca il leone se divento giallo oppure tocca il polipo se divento rosso
			Tocca il leone se divento giallo oppure tocca il polipo se divento giallo
			Tocca il leone se divento giallo oppure tocca il polipo se divento blu
			Tocca il leone se divento blu oppure tocca il polipo se divento rosso
			Tocca il leone se divento blu oppure tocca il polipo se divento giallo
			Tocca il leone se divento blu oppure tocca il polipo se divento verde
			Tocca il leone se divento rosso oppure tocca il gelato se divento giallo
			Tocca il leone se divento rosso oppure tocca il gelato se divento verde
			Tocca il leone se divento rosso oppure tocca il gelato se divento blu
			Tocca il leone se divento verde oppure tocca il gelato se divento rosso
			Tocca il leone se divento verde oppure tocca il gelato se divento giallo
			Tocca il leone se divento verde oppure tocca il gelato se divento blu
			Tocca il leone se divento giallo oppure tocca il gelato se divento rosso
			Tocca il leone se divento giallo oppure tocca il gelato se divento giallo
			Tocca il leone se divento giallo oppure tocca il gelato se divento blu
			Tocca il leone se divento blu oppure tocca il gelato se divento rosso
			Tocca il leone se divento blu oppure tocca il gelato se divento giallo
			Tocca il leone se divento blu oppure tocca il gelato se divento verde
			Tocca il leone se divento rosso oppure tocca la bambina se divento giallo
			Tocca il leone se divento rosso oppure tocca la bambina se divento verde
			Tocca il leone se divento rosso oppure tocca la bambina se divento blu
			Tocca il leone se divento verde oppure tocca la bambina se divento rosso
			Tocca il leone se divento verde oppure tocca la bambina se divento giallo
			Tocca il leone se divento verde oppure tocca la bambina se divento blu
			Tocca il leone se divento giallo oppure tocca la bambina se divento rosso
			Tocca il leone se divento giallo oppure tocca la bambina se divento giallo
			Tocca il leone se divento giallo oppure tocca la bambina se divento blu
			Tocca il leone se divento blu oppure tocca la bambina se divento rosso
			Tocca il leone se divento blu oppure tocca la bambina se divento giallo
			Tocca il leone se divento blu oppure tocca la bambina se divento verde
			Tocca il leone se divento rosso oppure tocca gli occhiali da sole se divento giallo
			Tocca il leone se divento rosso oppure tocca gli occhiali da sole se divento verde
			Tocca il leone se divento rosso oppure tocca gli occhiali da sole se divento blu
			Tocca il leone se divento verde oppure tocca gli occhiali da sole se divento rosso
			Tocca il leone se divento verde oppure tocca gli occhiali da sole se divento giallo
			Tocca il leone se divento verde oppure tocca gli occhiali da sole se divento blu
			Tocca il leone se divento giallo oppure tocca gli occhiali da sole se divento rosso
			Tocca il leone se divento giallo oppure tocca gli occhiali da sole se divento giallo
			Tocca il leone se divento giallo oppure tocca gli occhiali da sole se divento blu
			Tocca il leone se divento blu oppure tocca gli occhiali da sole se divento rosso
			Tocca il leone se divento blu oppure tocca gli occhiali da sole se divento giallo
			Tocca il leone se divento blu oppure tocca gli occhiali da sole se divento verde
			Tocca il leone se divento rosso oppure tocca la bicicletta se divento giallo
			Tocca il leone se divento rosso oppure tocca la bicicletta se divento verde
			Tocca il leone se divento rosso oppure tocca la bicicletta se divento blu
			Tocca il leone se divento verde oppure tocca la bicicletta se divento rosso
			Tocca il leone se divento verde oppure tocca la bicicletta se divento giallo
			Tocca il leone se divento verde oppure tocca la bicicletta se divento blu
			Tocca il leone se divento giallo oppure tocca la bicicletta se divento rosso
			Tocca il leone se divento giallo oppure tocca la bicicletta se divento giallo
			Tocca il leone se divento giallo oppure tocca la bicicletta se divento blu
			Tocca il leone se divento blu oppure tocca la bicicletta se divento rosso
			Tocca il leone se divento blu oppure tocca la bicicletta se divento giallo
			Tocca il leone se divento blu oppure tocca la bicicletta se divento verde
			Tocca il leone se divento rosso oppure tocca il monopattino se divento giallo
			Tocca il leone se divento rosso oppure tocca il monopattino se divento verde
			Tocca il leone se divento rosso oppure tocca il monopattino se divento blu
			Tocca il leone se divento verde oppure tocca il monopattino se divento rosso
			Tocca il leone se divento verde oppure tocca il monopattino se divento giallo
			Tocca il leone se divento verde oppure tocca il monopattino se divento blu
			Tocca il leone se divento giallo oppure tocca il monopattino se divento rosso
			Tocca il leone se divento giallo oppure tocca il monopattino se divento giallo
			Tocca il leone se divento giallo oppure tocca il monopattino se divento blu
			Tocca il leone se divento blu oppure tocca il monopattino se divento rosso
			Tocca il leone se divento blu oppure tocca il monopattino se divento giallo
			Tocca il leone se divento blu oppure tocca il monopattino se divento verde
			Tocca il leone se divento rosso oppure tocca l'ombrello se divento giallo
			Tocca il leone se divento rosso oppure tocca l'ombrello se divento verde
			Tocca il leone se divento rosso oppure tocca l'ombrello se divento blu
			Tocca il leone se divento verde oppure tocca l'ombrello se divento rosso
			Tocca il leone se divento verde oppure tocca l'ombrello se divento giallo
			Tocca il leone se divento verde oppure tocca l'ombrello se divento blu
			Tocca il leone se divento giallo oppure tocca l'ombrello se divento rosso
			Tocca il leone se divento giallo oppure tocca l'ombrello se divento giallo
			Tocca il leone se divento giallo oppure tocca l'ombrello se divento blu
			Tocca il leone se divento blu oppure tocca l'ombrello se divento rosso
			Tocca il leone se divento blu oppure tocca l'ombrello se divento giallo
			Tocca il leone se divento blu oppure tocca l'ombrello se divento verde
			Tocca il polipo se divento rosso oppure tocca il pappagallo se divento giallo
			Tocca il polipo se divento rosso oppure tocca il pappagallo se divento verde
			Tocca il polipo se divento rosso oppure tocca il pappagallo se divento blu
			Tocca il polipo se divento verde oppure tocca il pappagallo se divento rosso
			Tocca il polipo se divento verde oppure tocca il pappagallo se divento giallo
			Tocca il polipo se divento verde oppure tocca il pappagallo se divento blu
			Tocca il polipo se divento giallo oppure tocca il pappagallo se divento rosso
			Tocca il polipo se divento giallo oppure tocca il pappagallo se divento giallo
			Tocca il polipo se divento giallo oppure tocca il pappagallo se divento blu
			Tocca il polipo se divento blu oppure tocca il pappagallo se divento rosso
			Tocca il polipo se divento blu oppure tocca il pappagallo se divento giallo
			Tocca il polipo se divento blu oppure tocca il pappagallo se divento verde
			Tocca il polipo se divento rosso oppure tocca il gelato se divento giallo
			Tocca il polipo se divento rosso oppure tocca il gelato se divento verde
			Tocca il polipo se divento rosso oppure tocca il gelato se divento blu
			Tocca il polipo se divento verde oppure tocca il gelato se divento rosso
			Tocca il polipo se divento verde oppure tocca il gelato se divento giallo
			Tocca il polipo se divento verde oppure tocca il gelato se divento blu
			Tocca il polipo se divento giallo oppure tocca il gelato se divento rosso
			Tocca il polipo se divento giallo oppure tocca il gelato se divento giallo
			Tocca il polipo se divento giallo oppure tocca il gelato se divento blu
			Tocca il polipo se divento blu oppure tocca il gelato se divento rosso
			Tocca il polipo se divento blu oppure tocca il gelato se divento giallo
			Tocca il polipo se divento blu oppure tocca il gelato se divento verde
			Tocca il polipo se divento rosso oppure tocca la bambina se divento giallo
			Tocca il polipo se divento rosso oppure tocca la bambina se divento verde
			Tocca il polipo se divento rosso oppure tocca la bambina se divento blu
			Tocca il polipo se divento verde oppure tocca la bambina se divento rosso
			Tocca il polipo se divento verde oppure tocca la bambina se divento giallo
			Tocca il polipo se divento verde oppure tocca la bambina se divento blu
			Tocca il polipo se divento giallo oppure tocca la bambina se divento rosso
			Tocca il polipo se divento giallo oppure tocca la bambina se divento giallo
			Tocca il polipo se divento giallo oppure tocca la bambina se divento blu
			Tocca il polipo se divento blu oppure tocca la bambina se divento rosso
			Tocca il polipo se divento blu oppure tocca la bambina se divento giallo
			Tocca il polipo se divento blu oppure tocca la bambina se divento verde
			Tocca il polipo se divento rosso oppure tocca gli occhiali da sole se divento giallo
			Tocca il polipo se divento rosso oppure tocca gli occhiali da sole se divento verde
			Tocca il polipo se divento rosso oppure tocca gli occhiali da sole se divento blu
			Tocca il polipo se divento verde oppure tocca gli occhiali da sole se divento rosso
			Tocca il polipo se divento verde oppure tocca gli occhiali da sole se divento giallo
			Tocca il polipo se divento verde oppure tocca gli occhiali da sole se divento blu
			Tocca il polipo se divento giallo oppure tocca gli occhiali da sole se divento rosso
			Tocca il polipo se divento giallo oppure tocca gli occhiali da sole se divento giallo
			Tocca il polipo se divento giallo oppure tocca gli occhiali da sole se divento blu
			Tocca il polipo se divento blu oppure tocca gli occhiali da sole se divento rosso
			Tocca il polipo se divento blu oppure tocca gli occhiali da sole se divento giallo
			Tocca il polipo se divento blu oppure tocca gli occhiali da sole se divento verde
			Tocca il polipo se divento rosso oppure tocca la bicicletta se divento giallo
			Tocca il polipo se divento rosso oppure tocca la bicicletta se divento verde
			Tocca il polipo se divento rosso oppure tocca la bicicletta se divento blu
			Tocca il polipo se divento verde oppure tocca la bicicletta se divento rosso
			Tocca il polipo se divento verde oppure tocca la bicicletta se divento giallo
			Tocca il polipo se divento verde oppure tocca la bicicletta se divento blu
			Tocca il polipo se divento giallo oppure tocca la bicicletta se divento rosso
			Tocca il polipo se divento giallo oppure tocca la bicicletta se divento giallo
			Tocca il polipo se divento giallo oppure tocca la bicicletta se divento blu
			Tocca il polipo se divento blu oppure tocca la bicicletta se divento rosso
			Tocca il polipo se divento blu oppure tocca la bicicletta se divento giallo
			Tocca il polipo se divento blu oppure tocca la bicicletta se divento verde
			Tocca il polipo se divento rosso oppure tocca il monopattino se divento giallo
			Tocca il polipo se divento rosso oppure tocca il monopattino se divento verde
			Tocca il polipo se divento rosso oppure tocca il monopattino se divento blu
			Tocca il polipo se divento verde oppure tocca il monopattino se divento rosso
			Tocca il polipo se divento verde oppure tocca il monopattino se divento giallo
			Tocca il polipo se divento verde oppure tocca il monopattino se divento blu
			Tocca il polipo se divento giallo oppure tocca il monopattino se divento rosso
			Tocca il polipo se divento giallo oppure tocca il monopattino se divento giallo
			Tocca il polipo se divento giallo oppure tocca il monopattino se divento blu
			Tocca il polipo se divento blu oppure tocca il monopattino se divento rosso
			Tocca il polipo se divento blu oppure tocca il monopattino se divento giallo
			Tocca il polipo se divento blu oppure tocca il monopattino se divento verde
			Tocca il polipo se divento rosso oppure tocca l'ombrello se divento giallo
			Tocca il polipo se divento rosso oppure tocca l'ombrello se divento verde
			Tocca il polipo se divento rosso oppure tocca l'ombrello se divento blu
			Tocca il polipo se divento verde oppure tocca l'ombrello se divento rosso
			Tocca il polipo se divento verde oppure tocca l'ombrello se divento giallo
			Tocca il polipo se divento verde oppure tocca l'ombrello se divento blu
			Tocca il polipo se divento giallo oppure tocca l'ombrello se divento rosso
			Tocca il polipo se divento giallo oppure tocca l'ombrello se divento giallo
			Tocca il polipo se divento giallo oppure tocca l'ombrello se divento blu
			Tocca il polipo se divento blu oppure tocca l'ombrello se divento rosso
			Tocca il polipo se divento blu oppure tocca l'ombrello se divento giallo
			Tocca il polipo se divento blu oppure tocca l'ombrello se divento verde
			Tocca il pappagallo se divento rosso opure tocca il gelato se divento giallo
			Tocca il pappagallo se divento rosso opure tocca il gelato se divento verde
			Tocca il pappagallo se divento rosso opure tocca il gelato se divento blu
			Tocca il pappagallo se divento verde opure tocca il gelato se divento rosso
			Tocca il pappagallo se divento verde opure tocca il gelato se divento giallo
			Tocca il pappagallo se divento verde opure tocca il gelato se divento blu
			Tocca il pappagallo se divento giallo opure tocca il gelato se divento rosso
			Tocca il pappagallo se divento giallo opure tocca il gelato se divento giallo
			Tocca il pappagallo se divento giallo opure tocca il gelato se divento blu
			Tocca il pappagallo se divento blu opure tocca il gelato se divento rosso
			Tocca il pappagallo se divento blu opure tocca il gelato se divento giallo
			Tocca il pappagallo se divento blu opure tocca il gelato se divento verde
			Tocca il pappagallo se divento rosso opure tocca la bambina se divento giallo
			Tocca il pappagallo se divento rosso opure tocca la bambina se divento verde
			Tocca il pappagallo se divento rosso opure tocca la bambina se divento blu
			Tocca il pappagallo se divento verde opure tocca la bambina se divento rosso
			Tocca il pappagallo se divento verde opure tocca la bambina se divento giallo
			Tocca il pappagallo se divento verde opure tocca la bambina se divento blu
			Tocca il pappagallo se divento giallo opure tocca la bambina se divento rosso
			Tocca il pappagallo se divento giallo opure tocca la bambina se divento giallo
			Tocca il pappagallo se divento giallo opure tocca la bambina se divento blu
			Tocca il pappagallo se divento blu opure tocca la bambina se divento rosso
			Tocca il pappagallo se divento blu opure tocca la bambina se divento giallo
			Tocca il pappagallo se divento blu opure tocca la bambina se divento verde
			Tocca il pappagallo se divento rosso opure tocca gli occhiali da sole se divento giallo
			Tocca il pappagallo se divento rosso opure tocca gli occhiali da sole se divento verde
			Tocca il pappagallo se divento rosso opure tocca gli occhiali da sole se divento blu
			Tocca il pappagallo se divento verde opure tocca gli occhiali da sole se divento rosso
			Tocca il pappagallo se divento verde opure tocca gli occhiali da sole se divento giallo
			Tocca il pappagallo se divento verde opure tocca gli occhiali da sole se divento blu
			Tocca il pappagallo se divento giallo opure tocca gli occhiali da sole se divento rosso
			Tocca il pappagallo se divento giallo opure tocca gli occhiali da sole se divento giallo
			Tocca il pappagallo se divento giallo opure tocca gli occhiali da sole se divento blu
			Tocca il pappagallo se divento blu opure tocca gli occhiali da sole se divento rosso
			Tocca il pappagallo se divento blu opure tocca gli occhiali da sole se divento giallo
			Tocca il pappagallo se divento blu opure tocca gli occhiali da sole se divento verde
			Tocca il pappagallo se divento rosso opure tocca la bicicletta se divento giallo
			Tocca il pappagallo se divento rosso opure tocca la bicicletta se divento verde
			Tocca il pappagallo se divento rosso opure tocca la bicicletta se divento blu
			Tocca il pappagallo se divento verde opure tocca la bicicletta se divento rosso
			Tocca il pappagallo se divento verde opure tocca la bicicletta se divento giallo
			Tocca il pappagallo se divento verde opure tocca la bicicletta se divento blu
			Tocca il pappagallo se divento giallo opure tocca la bicicletta se divento rosso
			Tocca il pappagallo se divento giallo opure tocca la bicicletta se divento giallo
			Tocca il pappagallo se divento giallo opure tocca la bicicletta se divento blu
			Tocca il pappagallo se divento blu opure tocca la bicicletta se divento rosso
			Tocca il pappagallo se divento blu opure tocca la bicicletta se divento giallo
			Tocca il pappagallo se divento blu opure tocca la bicicletta se divento verde
			Tocca il pappagallo se divento rosso opure tocca il monopattino se divento giallo
			Tocca il pappagallo se divento rosso opure tocca il monopattino se divento verde
			Tocca il pappagallo se divento rosso opure tocca il monopattino se divento blu
			Tocca il pappagallo se divento verde opure tocca il monopattino se divento rosso
			Tocca il pappagallo se divento verde opure tocca il monopattino se divento giallo
			Tocca il pappagallo se divento verde opure tocca il monopattino se divento blu
			Tocca il pappagallo se divento giallo opure tocca il monopattino se divento rosso
			Tocca il pappagallo se divento giallo opure tocca il monopattino se divento giallo
			Tocca il pappagallo se divento giallo opure tocca il monopattino se divento blu
			Tocca il pappagallo se divento blu opure tocca il monopattino se divento rosso
			Tocca il pappagallo se divento blu opure tocca il monopattino se divento giallo
			Tocca il pappagallo se divento blu opure tocca il monopattino se divento verde
			Tocca il pappagallo se divento rosso opure tocca l'ombrello se divento giallo
			Tocca il pappagallo se divento rosso opure tocca l'ombrello se divento verde
			Tocca il pappagallo se divento rosso opure tocca l'ombrello se divento blu
			Tocca il pappagallo se divento verde opure tocca l'ombrello se divento rosso
			Tocca il pappagallo se divento verde opure tocca l'ombrello se divento giallo
			Tocca il pappagallo se divento verde opure tocca l'ombrello se divento blu
			Tocca il pappagallo se divento giallo opure tocca l'ombrello se divento rosso
			Tocca il pappagallo se divento giallo opure tocca l'ombrello se divento giallo
			Tocca il pappagallo se divento giallo opure tocca l'ombrello se divento blu
			Tocca il pappagallo se divento blu opure tocca l'ombrello se divento rosso
			Tocca il pappagallo se divento blu opure tocca l'ombrello se divento giallo
			Tocca il pappagallo se divento blu opure tocca l'ombrello se divento verde
			Tocca il gelato se divento rosso oppure tocca la bambina se divento giallo
			Tocca il gelato se divento rosso oppure tocca la bambina se divento verde
			Tocca il gelato se divento rosso oppure tocca la bambina se divento blu
			Tocca il gelato se divento verde oppure tocca la bambina se divento rosso
			Tocca il gelato se divento verde oppure tocca la bambina se divento giallo
			Tocca il gelato se divento verde oppure tocca la bambina se divento blu
			Tocca il gelato se divento giallo oppure tocca la bambina se divento rosso
			Tocca il gelato se divento giallo oppure tocca la bambina se divento giallo
			Tocca il gelato se divento giallo oppure tocca la bambina se divento blu
			Tocca il gelato se divento blu oppure tocca la bambina se divento rosso
			Tocca il gelato se divento blu oppure tocca la bambina se divento giallo
			Tocca il gelato se divento blu oppure tocca la bambina se divento verde
			Tocca il gelato se divento rosso oppure tocca gli occhiali da sole se divento giallo
			Tocca il gelato se divento rosso oppure tocca gli occhiali da sole se divento verde
			Tocca il gelato se divento rosso oppure tocca gli occhiali da sole se divento blu
			Tocca il gelato se divento verde oppure tocca gli occhiali da sole se divento rosso
			Tocca il gelato se divento verde oppure tocca gli occhiali da sole se divento giallo
			Tocca il gelato se divento verde oppure tocca gli occhiali da sole se divento blu
			Tocca il gelato se divento giallo oppure tocca gli occhiali da sole se divento rosso
			Tocca il gelato se divento giallo oppure tocca gli occhiali da sole se divento giallo
			Tocca il gelato se divento giallo oppure tocca gli occhiali da sole se divento blu
			Tocca il gelato se divento blu oppure tocca gli occhiali da sole se divento rosso
			Tocca il gelato se divento blu oppure tocca gli occhiali da sole se divento giallo
			Tocca il gelato se divento blu oppure tocca gli occhiali da sole se divento verde
			Tocca il gelato se divento rosso oppure tocca la bicicletta se divento giallo
			Tocca il gelato se divento rosso oppure tocca la bicicletta se divento verde
			Tocca il gelato se divento rosso oppure tocca la bicicletta se divento blu
			Tocca il gelato se divento verde oppure tocca la bicicletta se divento rosso
			Tocca il gelato se divento verde oppure tocca la bicicletta se divento giallo
			Tocca il gelato se divento verde oppure tocca la bicicletta se divento blu
			Tocca il gelato se divento giallo oppure tocca la bicicletta se divento rosso
			Tocca il gelato se divento giallo oppure tocca la bicicletta se divento giallo
			Tocca il gelato se divento giallo oppure tocca la bicicletta se divento blu
			Tocca il gelato se divento blu oppure tocca la bicicletta se divento rosso
			Tocca il gelato se divento blu oppure tocca la bicicletta se divento giallo
			Tocca il gelato se divento blu oppure tocca la bicicletta se divento verde
			Tocca il gelato se divento rosso oppure tocca il monopattino se divento giallo
			Tocca il gelato se divento rosso oppure tocca il monopattino se divento verde
			Tocca il gelato se divento rosso oppure tocca il monopattino se divento blu
			Tocca il gelato se divento verde oppure tocca il monopattino se divento rosso
			Tocca il gelato se divento verde oppure tocca il monopattino se divento giallo
			Tocca il gelato se divento verde oppure tocca il monopattino se divento blu
			Tocca il gelato se divento giallo oppure tocca il monopattino se divento rosso
			Tocca il gelato se divento giallo oppure tocca il monopattino se divento giallo
			Tocca il gelato se divento giallo oppure tocca il monopattino se divento blu
			Tocca il gelato se divento blu oppure tocca il monopattino se divento rosso
			Tocca il gelato se divento blu oppure tocca il monopattino se divento giallo
			Tocca il gelato se divento blu oppure tocca il monopattino se divento verde
			Tocca il gelato se divento rosso oppure tocca l'ombrello se divento giallo
			Tocca il gelato se divento rosso oppure tocca l'ombrello se divento verde
			Tocca il gelato se divento rosso oppure tocca l'ombrello se divento blu
			Tocca il gelato se divento verde oppure tocca l'ombrello se divento rosso
			Tocca il gelato se divento verde oppure tocca l'ombrello se divento giallo
			Tocca il gelato se divento verde oppure tocca l'ombrello se divento blu
			Tocca il gelato se divento giallo oppure tocca l'ombrello se divento rosso
			Tocca il gelato se divento giallo oppure tocca l'ombrello se divento giallo
			Tocca il gelato se divento giallo oppure tocca l'ombrello se divento blu
			Tocca il gelato se divento blu oppure tocca l'ombrello se divento rosso
			Tocca il gelato se divento blu oppure tocca l'ombrello se divento giallo
			Tocca il gelato se divento blu oppure tocca l'ombrello se divento verde
			Tocca gli occhiali da sole se divento rosso oppure tocca la bicicletta se divento giallo
			Tocca gli occhiali da sole se divento rosso oppure tocca la bicicletta se divento verde
			Tocca gli occhiali da sole se divento rosso oppure tocca la bicicletta se divento blu
			Tocca gli occhiali da sole se divento verde oppure tocca la bicicletta se divento rosso
			Tocca gli occhiali da sole se divento verde oppure tocca la bicicletta se divento giallo
			Tocca gli occhiali da sole se divento verde oppure tocca la bicicletta se divento blu
			Tocca gli occhiali da sole se divento giallo oppure tocca la bicicletta se divento rosso
			Tocca gli occhiali da sole se divento giallo oppure tocca la bicicletta se divento giallo
			Tocca gli occhiali da sole se divento giallo oppure tocca la bicicletta se divento blu
			Tocca gli occhiali da sole se divento blu oppure tocca la bicicletta se divento rosso
			Tocca gli occhiali da sole se divento blu oppure tocca la bicicletta se divento giallo
			Tocca gli occhiali da sole se divento blu oppure tocca la bicicletta se divento verde
			Tocca gli occhiali da sole se divento rosso oppure tocca il monopattino se divento giallo
			Tocca gli occhiali da sole se divento rosso oppure tocca il monopattino se divento verde
			Tocca gli occhiali da sole se divento rosso oppure tocca il monopattino se divento blu
			Tocca gli occhiali da sole se divento verde oppure tocca il monopattino se divento rosso
			Tocca gli occhiali da sole se divento verde oppure tocca il monopattino se divento giallo
			Tocca gli occhiali da sole se divento verde oppure tocca il monopattino se divento blu
			Tocca gli occhiali da sole se divento giallo oppure tocca il monopattino se divento rosso
			Tocca gli occhiali da sole se divento giallo oppure tocca il monopattino se divento giallo
			Tocca gli occhiali da sole se divento giallo oppure tocca il monopattino se divento blu
			Tocca gli occhiali da sole se divento blu oppure tocca il monopattino se divento rosso
			Tocca gli occhiali da sole se divento blu oppure tocca il monopattino se divento giallo
			Tocca gli occhiali da sole se divento blu oppure tocca il monopattino se divento verde
			Tocca gli occhiali da sole se divento rosso oppure tocca l'ombrello se divento giallo
			Tocca gli occhiali da sole se divento rosso oppure tocca l'ombrello se divento verde
			Tocca gli occhiali da sole se divento rosso oppure tocca l'ombrello se divento blu
			Tocca gli occhiali da sole se divento verde oppure tocca l'ombrello se divento rosso
			Tocca gli occhiali da sole se divento verde oppure tocca l'ombrello se divento giallo
			Tocca gli occhiali da sole se divento verde oppure tocca l'ombrello se divento blu
			Tocca gli occhiali da sole se divento giallo oppure tocca l'ombrello se divento rosso
			Tocca gli occhiali da sole se divento giallo oppure tocca l'ombrello se divento giallo
			Tocca gli occhiali da sole se divento giallo oppure tocca l'ombrello se divento blu
			Tocca gli occhiali da sole se divento blu oppure tocca l'ombrello se divento rosso
			Tocca gli occhiali da sole se divento blu oppure tocca l'ombrello se divento giallo
			Tocca gli occhiali da sole se divento blu oppure tocca l'ombrello se divento verde
			Tocca la bicicletta se divento rosso oppure tocca il monopattino se divento giallo
			Tocca la bicicletta se divento rosso oppure tocca il monopattino se divento verde
			Tocca la bicicletta se divento rosso oppure tocca il monopattino se divento blu
			Tocca la bicicletta se divento verde oppure tocca il monopattino se divento rosso
			Tocca la bicicletta se divento verde oppure tocca il monopattino se divento giallo
			Tocca la bicicletta se divento verde oppure tocca il monopattino se divento blu
			Tocca la bicicletta se divento giallo oppure tocca il monopattino se divento rosso
			Tocca la bicicletta se divento giallo oppure tocca il monopattino se divento giallo
			Tocca la bicicletta se divento giallo oppure tocca il monopattino se divento blu
			Tocca la bicicletta se divento blu oppure tocca il monopattino se divento rosso
			Tocca la bicicletta se divento blu oppure tocca il monopattino se divento giallo
			Tocca la bicicletta se divento blu oppure tocca il monopattino se divento verde
			Tocca la bicicletta se divento rosso oppure tocca l'ombrello se divento giallo
			Tocca la bicicletta se divento rosso oppure tocca l'ombrello se divento verde
			Tocca la bicicletta se divento rosso oppure tocca l'ombrello se divento blu
			Tocca la bicicletta se divento verde oppure tocca l'ombrello se divento rosso
			Tocca la bicicletta se divento verde oppure tocca l'ombrello se divento giallo
			Tocca la bicicletta se divento verde oppure tocca l'ombrello se divento blu
			Tocca la bicicletta se divento giallo oppure tocca l'ombrello se divento rosso
			Tocca la bicicletta se divento giallo oppure tocca l'ombrello se divento giallo
			Tocca la bicicletta se divento giallo oppure tocca l'ombrello se divento blu
			Tocca la bicicletta se divento blu oppure tocca l'ombrello se divento rosso
			Tocca la bicicletta se divento blu oppure tocca l'ombrello se divento giallo
			Tocca la bicicletta se divento blu oppure tocca l'ombrello se divento verde
			Tocca il monopattino se divento rosso oppure tocca l'ombrello se divento giallo
			Tocca il monopattino se divento rosso oppure tocca l'ombrello se divento verde
			Tocca il monopattino se divento rosso oppure tocca l'ombrello se divento blu
			Tocca il monopattino se divento verde oppure tocca l'ombrello se divento rosso
			Tocca il monopattino se divento verde oppure tocca l'ombrello se divento giallo
			Tocca il monopattino se divento verde oppure tocca l'ombrello se divento blu
			Tocca il monopattino se divento giallo oppure tocca l'ombrello se divento rosso
			Tocca il monopattino se divento giallo oppure tocca l'ombrello se divento giallo
			Tocca il monopattino se divento giallo oppure tocca l'ombrello se divento blu
			Tocca il monopattino se divento blu oppure tocca l'ombrello se divento rosso
			Tocca il monopattino se divento blu oppure tocca l'ombrello se divento giallo
			Tocca il monopattino se divento blu oppure tocca l'ombrello se divento verde
			Tocca la bambina se divento rosso oppure tocca gli occhiali da sole se divento giallo
			Tocca la bambina se divento rosso oppure tocca gli occhiali da sole se divento verde
			Tocca la bambina se divento rosso oppure tocca gli occhiali da sole se divento blu
			Tocca la bambina se divento verde oppure tocca gli occhiali da sole se divento rosso
			Tocca la bambina se divento verde oppure tocca gli occhiali da sole se divento giallo
			Tocca la bambina se divento verde oppure tocca gli occhiali da sole se divento blu
			Tocca la bambina se divento giallo oppure tocca gli occhiali da sole se divento rosso
			Tocca la bambina se divento giallo oppure tocca gli occhiali da sole se divento giallo
			Tocca la bambina se divento giallo oppure tocca gli occhiali da sole se divento blu
			Tocca la bambina se divento blu oppure tocca gli occhiali da sole se divento rosso
			Tocca la bambina se divento blu oppure tocca gli occhiali da sole se divento giallo
			Tocca la bambina se divento blu oppure tocca gli occhiali da sole se divento verde
			Tocca la bambina se divento rosso oppure tocca la bicicletta se divento giallo
			Tocca la bambina se divento rosso oppure tocca la bicicletta se divento verde
			Tocca la bambina se divento rosso oppure tocca la bicicletta se divento blu
			Tocca la bambina se divento verde oppure tocca la bicicletta se divento rosso
			Tocca la bambina se divento verde oppure tocca la bicicletta se divento giallo
			Tocca la bambina se divento verde oppure tocca la bicicletta se divento blu
			Tocca la bambina se divento giallo oppure tocca la bicicletta se divento rosso
			Tocca la bambina se divento giallo oppure tocca la bicicletta se divento giallo
			Tocca la bambina se divento giallo oppure tocca la bicicletta se divento blu
			Tocca la bambina se divento blu oppure tocca la bicicletta se divento rosso
			Tocca la bambina se divento blu oppure tocca la bicicletta se divento giallo
			Tocca la bambina se divento blu oppure tocca la bicicletta se divento verde
			Tocca la bambina se divento rosso oppure tocca il monopattino se divento giallo
			Tocca la bambina se divento rosso oppure tocca il monopattino se divento verde
			Tocca la bambina se divento rosso oppure tocca il monopattino se divento blu
			Tocca la bambina se divento verde oppure tocca il monopattino se divento rosso
			Tocca la bambina se divento verde oppure tocca il monopattino se divento giallo
			Tocca la bambina se divento verde oppure tocca il monopattino se divento blu
			Tocca la bambina se divento giallo oppure tocca il monopattino se divento rosso
			Tocca la bambina se divento giallo oppure tocca il monopattino se divento giallo
			Tocca la bambina se divento giallo oppure tocca il monopattino se divento blu
			Tocca la bambina se divento blu oppure tocca il monopattino se divento rosso
			Tocca la bambina se divento blu oppure tocca il monopattino se divento giallo
			Tocca la bambina se divento blu oppure tocca il monopattino se divento verde
			Tocca la bambina se divento rosso oppure tocca l'ombrello se divento giallo
			Tocca la bambina se divento rosso oppure tocca l'ombrello se divento verde
			Tocca la bambina se divento rosso oppure tocca l'ombrello se divento blu
			Tocca la bambina se divento verde oppure tocca l'ombrello se divento rosso
			Tocca la bambina se divento verde oppure tocca l'ombrello se divento giallo
			Tocca la bambina se divento verde oppure tocca l'ombrello se divento blu
			Tocca la bambina se divento giallo oppure tocca l'ombrello se divento rosso
			Tocca la bambina se divento giallo oppure tocca l'ombrello se divento giallo
			Tocca la bambina se divento giallo oppure tocca l'ombrello se divento blu
			Tocca la bambina se divento blu oppure tocca l'ombrello se divento rosso
			Tocca la bambina se divento blu oppure tocca l'ombrello se divento giallo
			Tocca la bambina se divento blu oppure tocca l'ombrello se divento verde
	######################################
	#3S = Same pattern
	    3SSU = Same patches according to subject
	    	Tocca le figure dello stesso disegno 
	    3SCO = Same patches according to color
	    	Tocca le figure dello stesso colore 
	######################################
	#4P = Same pair as 
		4PSU = 
			Tocca lo stesso soggetto della figura in alto a sinistra
			Tocca lo stesso soggetto della figura in basso a sinistra
			Tocca lo stesso soggetto della figura in alto a destra
			Tocca lo stesso soggetto della figura in basso a destra
		4PCO= 
			Tocca le figure dello stesso colore di quella in alto a sinistra
			Tocca le figure dello stesso colore di quella in alto a destra
			Tocca le figure dello stesso colore di quella in basso a sinistra
			Tocca le figure dello stesso colore di quella in basso a destra
	######################################
	#5K = Knowledge 
		5KS = Knowledge single
			Tocca cosa si può mangiare
			Tocca cosa si può mettere sulla testa
			Tocca cosa si può guidare
			Tocca cosa si può indossare
			Tocca cosa si può pedalare
			Tocca cosa si usa quando piove
			Tocca chi sa parlare
			Tocca chi non sa parlare
			Tocca chi sa volare
			Tocca chi non sa volare
			Tocca cosa ha le route
			Tocca chi sa nuotare
			Tocca chi non ha le zampe
			Tocca chi cammina su due zampe
			Tocca chi cammina a quattro zampe
			Tocca chi ruggisce
			Tocca chi ha il becco
			Tocca chi può vivere in casa
			Tocca chi non può vivere in città
			Tocca cosa si può metttere sopra un tavolo
			Tocca cosa può stare nella cameretta
			Tocca chi sa correre
			Tocca chi può stare sugli alberi
			Tocca chi può arrampicarsi
			Tocca chi va più veloce
			Tocca il più giovane
			Tocca il più vecchio
		5KD = Knowledge double 
			Tocca chi sa nuotare e correre
			Tocca chi sa correre e parlare
			Tocca chi cammina su due zampe e non può volare
	######################################
	#6I = Intruder game 
		6IS = Tocca l intruso 
	######################################
	#7O = Order game 
		7OSU2 = Order subject for 2 patches
			Tocca prima il leone e poi la scimmia
			Tocca prima il leone e poi il pappagallo
			Tocca prima il leone e poi il polipo
			Tocca prima il leone e poi il gelato
			Tocca prima il leone e poi gli occhiali da sole
			Tocca prima il leone e poi la bicicletta
			Tocca prima il leone e poi il monopattino
			Tocca prima il leone e poi la bambina
			Tocca prima il leone e poi l'ombrello
			Tocca prima la scimmia e poi il leone
			Tocca prima la scimmia e poi il pappagallo
			Tocca prima la scimmia e poi il polipo
			Tocca prima la scimmia e poi il gelato
			Tocca prima la sci   mmia e poi gli occhiali da sole
			Tocca prima la scimmia e poi la bicicletta
			Tocca prima la scimmia e poi il monopattino
			Tocca prima la scimmia e poi la bambina
			Tocca prima la scimmia e poi l'ombrello
			Tocca prima il pappagallo e poi il leone
			Tocca prima il pappagallo e poi la scimmia
			Tocca prima il pappagallo e poi il polipo
			Tocca prima il pappagallo e poi il gelato
			Tocca prima il pappagallo e poi gli occhiali da sole
			Tocca prima il pappagallo e poi la bicicletta
			Tocca prima il pappagallo e poi il monopattino
			Tocca prima il pappagallo e poi la bambina
			Tocca prima il pappagallo e poi l'ombrello
			Tocca prima il polipo e poi il leone
			Tocca prima il polipo e poi la scimmia
			Tocca prima il polipo e poi il pappagallo
			Tocca prima il polipo e poi il gelato
			Tocca prima il polipo e poi gli occhiali da sole
			Tocca prima il polipo e poi la bicicletta
			Tocca prima il polipo e poi il monopattino
			Tocca prima il polipo e poi la bambina
			Tocca prima il polipo e poi l'ombrello
			Tocca prima il gelato e poi il leone
			Tocca prima il gelato e poi la scimmia
			Tocca prima il gelato e poi il pappagallo
			Tocca prima il gelato e poi il polipo
			Tocca prima il gelato e poi gli occhiali da sole
			Tocca prima il gelato e poi la bicicletta
			Tocca prima il gelato e poi il monopattino
			Tocca prima il gelato e poi la bambina
			Tocca prima il gelato e poi l'ombrello
			Tocca prima gli occhiali da sole e poi il leone
			Tocca prima gli occhiali da sole e poi la scimmia
			Tocca prima gli occhiali da sole e poi il pappagallo
			Tocca prima gli occhiali da sole e poi il polipo
			Tocca prima gli occhiali da sole e poi il gelato
			Tocca prima gli occhiali da sole e poi la bicicletta
			Tocca prima gli occhiali da sole e poi il monopattino
			Tocca prima gli occhiali da sole e poi la bambina
			Tocca prima gli occhiali da sole e poi l'ombrello
			Tocca prima la bicicletta e poi il leone
			Tocca prima la bicicletta e poi la scimmia
			Tocca prima la bicicletta e poi il pappagallo
			Tocca prima la bicicletta e poi il polipo
			Tocca prima la bicicletta e poi il gelato
			Tocca prima la bicicletta e poi gli occhiali da sole
			Tocca prima la bicicletta e poi il monopattino
			Tocca prima la bicicletta e poi la bambina
			Tocca prima la bicicletta e poi l'ombrello
			Tocca prima il monopattino e poi il leone
			Tocca prima il monopattino e poi la scimmia
			Tocca prima il monopattino e poi il pappagallo
			Tocca prima il monopattino e poi il polipo
			Tocca prima il monopattino e poi il gelato
			Tocca prima il monopattino e poi gli occhiali da sole
			Tocca prima il monopattino e poi la bicicletta
			Tocca prima il monopattino e poi la bambina
			Tocca prima il monopattino e poi l'ombrello
			Tocca prima la bambina e poi il leone
			Tocca prima la bambina e poi la scimmia
			Tocca prima la bambina e poi il pappagallo
			Tocca prima la bambina e poi il polipo
			Tocca prima la bambina e poi il gelato
			Tocca prima la bambina e poi gli occhiali da sole
			Tocca prima la bambina e poi la bicicletta
			Tocca prima la bambina e poi il monopattino
			Tocca prima la bambina e poi l'ombrello
			Tocca prima l'ombrello e poi il leone
			Tocca prima l'ombrello e poi la scimmia
			Tocca prima l'ombrello e poi il pappagallo
			Tocca prima l'ombrello e poi il polipo
			Tocca prima l'ombrello e poi il gelato
			Tocca prima l'ombrello e poi gli occhiali da sole
			Tocca prima l'ombrello e poi la bicicletta
			Tocca prima l'ombrello e poi il monopattino
			Tocca prima l'ombrello e poi la bambina
		7OSU3 = Order subject for 3 patches
			Tocca in ordine il leone poi la scimmia poi il pappagallo
			Tocca in ordine il leone poi la scimmia poi il polipo
			Tocca in ordine il leone poi la scimmia poi il gelato
			Tocca in ordine il leone poi la scimmia poi gli occhiali da sole
			Tocca in ordine il leone poi la scimmia poi la bicicletta
			Tocca in ordine il leone poi la scimmia poi il monopattino
			Tocca in ordine il leone poi la scimmia poi la bambina
			Tocca in ordine il leone poi la scimmia poi l'ombrello
			Tocca in ordine il leone poi il pappagallo poi la scimmia
			Tocca in ordine il leone poi il pappagallo poi il polipo
			Tocca in ordine il leone poi il pappagallo poi il gelato
			Tocca in ordine il leone poi il pappagallo poi gli occhiali da sole
			Tocca in ordine il leone poi il pappagallo poi la bicicletta
			Tocca in ordine il leone poi il pappagallo poi il monopattino
			Tocca in ordine il leone poi il pappagallo poi la bambina
			Tocca in ordine il leone poi il pappagallo poi l'ombrello
			Tocca in ordine il leone poi polipo poi la scimmia
			Tocca in ordine il leone poi polipo poi il pappagallo
			Tocca in ordine il leone poi polipo poi il gelato
			Tocca in ordine il leone poi polipo poi gli occhiali da sole
			Tocca in ordine il leone poi polipo poi la bicicletta
			Tocca in ordine il leone poi polipo poi il monopattino
			Tocca in ordine il leone poi polipo poi la bambina
			Tocca in ordine il leone poi polipo poi l'ombrello
			Tocca in ordine il leone poi gelato poi la scimmia
			Tocca in ordine il leone poi gelato poi il pappagallo
			Tocca in ordine il leone poi gelato poi il polipo
			Tocca in ordine il leone poi gelato poi gli occhiali da sole
			Tocca in ordine il leone poi gelato poi la bicicletta
			Tocca in ordine il leone poi gelato poi il monopattino
			Tocca in ordine il leone poi gelato poi la bambina
			Tocca in ordine il leone poi gelato poi l'ombrello
			Tocca in ordine il leone poi gli occhiali da sole poi la scimmia
			Tocca in ordine il leone poi gli occhiali da sole poi il pappagallo
			Tocca in ordine il leone poi gli occhiali da sole poi il polipo
			Tocca in ordine il leone poi gli occhiali da sole poi il gelato
			Tocca in ordine il leone poi gli occhiali da sole poi la bicicletta
			Tocca in ordine il leone poi gli occhiali da sole poi il monopattino
			Tocca in ordine il leone poi gli occhiali da sole poi la bambina
			Tocca in ordine il leone poi gli occhiali da sole poi l'ombrello
			Tocca in ordine il leone poi la bicicletta poi la scimmia
			Tocca in ordine il leone poi la bicicletta poi il pappagallo
			Tocca in ordine il leone poi la bicicletta poi il polipo
			Tocca in ordine il leone poi la bicicletta poi il gelato
			Tocca in ordine il leone poi la bicicletta poi gli occhiali da sole
			Tocca in ordine il leone poi la bicicletta poi il monopattino
			Tocca in ordine il leone poi la bicicletta poi la bambina
			Tocca in ordine il leone poi la bicicletta poi l'ombrello
			Tocca in ordine il leone poi monopattino poi la scimmia
			Tocca in ordine il leone poi monopattino poi il pappagallo
			Tocca in ordine il leone poi monopattino poi il polipo
			Tocca in ordine il leone poi monopattino poi il gelato
			Tocca in ordine il leone poi monopattino poi gli occhiali da sole
			Tocca in ordine il leone poi monopattino poi la bicicletta
			Tocca in ordine il leone poi monopattino poi la bambina
			Tocca in ordine il leone poi monopattino poi l'ombrello
			Tocca in ordine il leone poi la bambina poi la scimmia
			Tocca in ordine il leone poi la bambina poi il pappagallo
			Tocca in ordine il leone poi la bambina poi il polipo
			Tocca in ordine il leone poi la bambina poi il gelato
			Tocca in ordine il leone poi la bambina poi gli occhiali da sole
			Tocca in ordine il leone poi la bambina poi la bicicletta
			Tocca in ordine il leone poi la bambina poi il monopattino
			Tocca in ordine il leone poi la bambina poi l'ombrello
			Tocca in ordine il leone poi l'ombrello poi la scimmia
			Tocca in ordine il leone poi l'ombrello poi il pappagallo
			Tocca in ordine il leone poi l'ombrello poi il polipo
			Tocca in ordine il leone poi l'ombrello poi il gelato
			Tocca in ordine il leone poi l'ombrello poi gli occhiali da sole
			Tocca in ordine il leone poi l'ombrello poi la bicicletta
			Tocca in ordine il leone poi l'ombrello poi il monopattino
			Tocca in ordine il leone poi l'ombrello poi la bambina
			Tocca in ordine la scimmia poi il leone poi il pappagallo
			Tocca in ordine la scimmia poi il leone poi il polipo
			Tocca in ordine la scimmia poi il leone poi il gelato
			Tocca in ordine la scimmia poi il leone poi gli occhiali da sole
			Tocca in ordine la scimmia poi il leone poi la bicicletta
			Tocca in ordine la scimmia poi il leone poi il monopattino
			Tocca in ordine la scimmia poi il leone poi la bambina
			Tocca in ordine la scimmia poi il leone poi l'ombrello
			Tocca in ordine la scimmia poi il pappagallo poi il leone
			Tocca in ordine la scimmia poi il pappagallo poi il polipo
			Tocca in ordine la scimmia poi il pappagallo poi il gelato
			Tocca in ordine la scimmia poi il pappagallo poi gli occhiali da sole
			Tocca in ordine la scimmia poi il pappagallo poi la bicicletta
			Tocca in ordine la scimmia poi il pappagallo poi il monopattino
			Tocca in ordine la scimmia poi il pappagallo poi la bambina
			Tocca in ordine la scimmia poi il pappagallo poi l'ombrello
			Tocca in ordine la scimmia poi il polipo poi il leone
			Tocca in ordine la scimmia poi il polipo poi il pappagallo
			Tocca in ordine la scimmia poi il polipo poi il gelato
			Tocca in ordine la scimmia poi il polipo poi gli occhiali da sole
			Tocca in ordine la scimmia poi il polipo poi la bicicletta
			Tocca in ordine la scimmia poi il polipo poi il monopattino
			Tocca in ordine la scimmia poi il polipo poi la bambina
			Tocca in ordine la scimmia poi il polipo poi l'ombrello
			Tocca in ordine la scimmia poi il gelato poi il leone
			Tocca in ordine la scimmia poi il gelato poi il pappagallo
			Tocca in ordine la scimmia poi il gelato poi il polipo
			Tocca in ordine la scimmia poi il gelato poi gli occhiali da sole
			Tocca in ordine la scimmia poi il gelato poi la bicicletta
			Tocca in ordine la scimmia poi il gelato poi il monopattino
			Tocca in ordine la scimmia poi il gelato poi la bambina
			Tocca in ordine la scimmia poi il gelato poi l'ombrello
			Tocca in ordine la scimmia poi gli occhiali da sole poi il leone
			Tocca in ordine la scimmia poi gli occhiali da sole poi il pappagallo
			Tocca in ordine la scimmia poi gli occhiali da sole poi il polipo
			Tocca in ordine la scimmia poi gli occhiali da sole poi il gelato
			Tocca in ordine la scimmia poi gli occhiali da sole poi la bicicletta
			Tocca in ordine la scimmia poi gli occhiali da sole poi il monopattino
			Tocca in ordine la scimmia poi gli occhiali da sole poi la bambina
			Tocca in ordine la scimmia poi gli occhiali da sole poi l'ombrello
			Tocca in ordine la scimmia poi la bicicletta poi il leone
			Tocca in ordine la scimmia poi la bicicletta poi il pappagallo
			Tocca in ordine la scimmia poi la bicicletta poi il polipo
			Tocca in ordine la scimmia poi la bicicletta poi il gelato
			Tocca in ordine la scimmia poi la bicicletta poi gli occhiali da sole
			Tocca in ordine la scimmia poi la bicicletta poi il monopattino
			Tocca in ordine la scimmia poi la bicicletta poi la bambina
			Tocca in ordine la scimmia poi la bicicletta poi l'ombrello
			Tocca in ordine la scimmia poi il monopattino poi il leone
			Tocca in ordine la scimmia poi il monopattino poi il pappagallo
			Tocca in ordine la scimmia poi il monopattino poi il polipo
			Tocca in ordine la scimmia poi il monopattino poi il gelato
			Tocca in ordine la scimmia poi il monopattino poi gli occhiali da sole
			Tocca in ordine la scimmia poi il monopattino poi la bicicletta
			Tocca in ordine la scimmia poi il monopattino poi la bambina
			Tocca in ordine la scimmia poi il monopattino poi l'ombrello
			Tocca in ordine la scimmia poi la bambina poi il leone
			Tocca in ordine la scimmia poi la bambina poi il pappagallo
			Tocca in ordine la scimmia poi la bambina poi il polipo
			Tocca in ordine la scimmia poi la bambina poi il gelato
			Tocca in ordine la scimmia poi la bambina poi gli occhiali da sole
			Tocca in ordine la scimmia poi la bambina poi la bicicletta
			Tocca in ordine la scimmia poi la bambina poi il monopattino
			Tocca in ordine la scimmia poi la bambina poi l'ombrello
			Tocca in ordine la scimmia poi l'ombrello poi il leone
			Tocca in ordine la scimmia poi l'ombrello poi il pappagallo
			Tocca in ordine la scimmia poi l'ombrello poi il polipo
			Tocca in ordine la scimmia poi l'ombrello poi il gelato
			Tocca in ordine la scimmia poi l'ombrello poi gli occhiali da sole
			Tocca in ordine la scimmia poi l'ombrello poi la bicicletta
			Tocca in ordine la scimmia poi l'ombrello poi il monopattino
			Tocca in ordine la scimmia poi l'ombrello poi la bambina
			Tocca in ordine il pappagallo poi il leone poi la scimmia
			Tocca in ordine il pappagallo poi il leone poi il polipo
			Tocca in ordine il pappagallo poi il leone poi il gelato
			Tocca in ordine il pappagallo poi il leone poi gli occhiali da sole
			Tocca in ordine il pappagallo poi il leone poi la bicicletta
			Tocca in ordine il pappagallo poi il leone poi il monopattino
			Tocca in ordine il pappagallo poi il leone poi la bambina
			Tocca in ordine il pappagallo poi il leone poi l'ombrello
			Tocca in ordine il pappagallo poi la scimmia poi il leone
			Tocca in ordine il pappagallo poi la scimmia poi il polipo
			Tocca in ordine il pappagallo poi la scimmia poi il gelato
			Tocca in ordine il pappagallo poi la scimmia poi gli occhiali da sole
			Tocca in ordine il pappagallo poi la scimmia poi la bicicletta
			Tocca in ordine il pappagallo poi la scimmia poi il monopattino
			Tocca in ordine il pappagallo poi la scimmia poi la bambina
			Tocca in ordine il pappagallo poi la scimmia poi l'ombrello
			Tocca in ordine il pappagallo poi il polipo poi il leone
			Tocca in ordine il pappagallo poi il polipo poi la scimmia
			Tocca in ordine il pappagallo poi il polipo poi il gelato
			Tocca in ordine il pappagallo poi il polipo poi gli occhiali da sole
			Tocca in ordine il pappagallo poi il polipo poi la bicicletta
			Tocca in ordine il pappagallo poi il polipo poi il monopattino
			Tocca in ordine il pappagallo poi il polipo poi la bambina
			Tocca in ordine il pappagallo poi il polipo poi l'ombrello
			Tocca in ordine il pappagallo poi il gelato poi il leone
			Tocca in ordine il pappagallo poi il gelato poi la scimmia
			Tocca in ordine il pappagallo poi il gelato poi il polipo
			Tocca in ordine il pappagallo poi il gelato poi gli occhiali da sole
			Tocca in ordine il pappagallo poi il gelato poi la bicicletta
			Tocca in ordine il pappagallo poi il gelato poi il monopattino
			Tocca in ordine il pappagallo poi il gelato poi la bambina
			Tocca in ordine il pappagallo poi il gelato poi l'ombrello
			Tocca in ordine il pappagallo poi gli occhiali da sole poi il leone
			Tocca in ordine il pappagallo poi gli occhiali da sole poi la scimmia
			Tocca in ordine il pappagallo poi gli occhiali da sole poi il polipo
			Tocca in ordine il pappagallo poi gli occhiali da sole poi il gelato
			Tocca in ordine il pappagallo poi gli occhiali da sole poi la bicicletta
			Tocca in ordine il pappagallo poi gli occhiali da sole poi il monopattino
			Tocca in ordine il pappagallo poi gli occhiali da sole poi la bambina
			Tocca in ordine il pappagallo poi gli occhiali da sole poi l'ombrello
			Tocca in ordine il pappagallo poi la bicicletta poi il leone
			Tocca in ordine il pappagallo poi la bicicletta poi la scimmia
			Tocca in ordine il pappagallo poi la bicicletta poi il polipo
			Tocca in ordine il pappagallo poi la bicicletta poi il gelato
			Tocca in ordine il pappagallo poi la bicicletta poi gli occhiali da sole
			Tocca in ordine il pappagallo poi la bicicletta poi il monopattino
			Tocca in ordine il pappagallo poi la bicicletta poi la bambina
			Tocca in ordine il pappagallo poi la bicicletta poi l'ombrello
			Tocca in ordine il pappagallo poi il monopattino poi il leone
			Tocca in ordine il pappagallo poi il monopattino poi la scimmia
			Tocca in ordine il pappagallo poi il monopattino poi il polipo
			Tocca in ordine il pappagallo poi il monopattino poi il gelato
			Tocca in ordine il pappagallo poi il monopattino poi gli occhiali da sole
			Tocca in ordine il pappagallo poi il monopattino poi la bicicletta
			Tocca in ordine il pappagallo poi il monopattino poi la bambina
			Tocca in ordine il pappagallo poi il monopattino poi l'ombrello
			Tocca in ordine il pappagallo poi la bambina poi il leone
			Tocca in ordine il pappagallo poi la bambina poi la scimmia
			Tocca in ordine il pappagallo poi la bambina poi il polipo
			Tocca in ordine il pappagallo poi la bambina poi il gelato
			Tocca in ordine il pappagallo poi la bambina poi gli occhiali da sole
			Tocca in ordine il pappagallo poi la bambina poi la bicicletta
			Tocca in ordine il pappagallo poi la bambina poi il monopattino
			Tocca in ordine il pappagallo poi la bambina poi l'ombrello
			Tocca in ordine il pappagallo poi l'ombrello poi il leone
			Tocca in ordine il pappagallo poi l'ombrello poi la scimmia
			Tocca in ordine il pappagallo poi l'ombrello poi il polipo
			Tocca in ordine il pappagallo poi l'ombrello poi il gelato
			Tocca in ordine il pappagallo poi l'ombrello poi gli occhiali da sole
			Tocca in ordine il pappagallo poi l'ombrello poi la bicicletta
			Tocca in ordine il pappagallo poi l'ombrello poi il monopattino
			Tocca in ordine il pappagallo poi l'ombrello poi la bambina
			Tocca in ordine il polipo poi il leone poi la scimmia
			Tocca in ordine il polipo poi il leone poi il pappagallo
			Tocca in ordine il polipo poi il leone poi il gelato
			Tocca in ordine il polipo poi il leone poi gli occhiali da sole
			Tocca in ordine il polipo poi il leone poi la bicicletta
			Tocca in ordine il polipo poi il leone poi il monopattino
			Tocca in ordine il polipo poi il leone poi la bambina
			Tocca in ordine il polipo poi il leone poi l'ombrello
			Tocca in ordine il polipo poi la scimmia poi il leone
			Tocca in ordine il polipo poi la scimmia poi il pappagallo
			Tocca in ordine il polipo poi la scimmia poi il gelato
			Tocca in ordine il polipo poi la scimmia poi gli occhiali da sole
			Tocca in ordine il polipo poi la scimmia poi la bicicletta
			Tocca in ordine il polipo poi la scimmia poi il monopattino
			Tocca in ordine il polipo poi la scimmia poi la bambina
			Tocca in ordine il polipo poi la scimmia poi l'ombrello
			Tocca in ordine il polipo poi il pappagallo poi il leone
			Tocca in ordine il polipo poi il pappagallo poi la scimmia
			Tocca in ordine il polipo poi il pappagallo poi il gelato
			Tocca in ordine il polipo poi il pappagallo poi gli occhiali da sole
			Tocca in ordine il polipo poi il pappagallo poi la bicicletta
			Tocca in ordine il polipo poi il pappagallo poi il monopattino
			Tocca in ordine il polipo poi il pappagallo poi la bambina
			Tocca in ordine il polipo poi il pappagallo poi l'ombrello
			Tocca in ordine il polipo il gelato poi il leone
			Tocca in ordine il polipo il gelato poi la scimmia
			Tocca in ordine il polipo il gelato poi il pappagallo
			Tocca in ordine il polipo il gelato poi gli occhiali da sole
			Tocca in ordine il polipo il gelato poi la bicicletta
			Tocca in ordine il polipo il gelato poi il monopattino
			Tocca in ordine il polipo il gelato poi la bambina
			Tocca in ordine il polipo il gelato poi l'ombrello
			Tocca in ordine il polipo gli occhiali da sole poi il leone
			Tocca in ordine il polipo gli occhiali da sole poi la scimmia
			Tocca in ordine il polipo gli occhiali da sole poi il pappagallo
			Tocca in ordine il polipo gli occhiali da sole poi il gelato
			Tocca in ordine il polipo gli occhiali da sole poi la bicicletta
			Tocca in ordine il polipo gli occhiali da sole poi il monopattino
			Tocca in ordine il polipo gli occhiali da sole poi la bambina
			Tocca in ordine il polipo gli occhiali da sole poi l'ombrello
			Tocca in ordine il polipo la bicicletta poi il leone
			Tocca in ordine il polipo la bicicletta poi la scimmia
			Tocca in ordine il polipo la bicicletta poi il pappagallo
			Tocca in ordine il polipo la bicicletta poi il gelato
			Tocca in ordine il polipo la bicicletta poi gli occhiali da sole
			Tocca in ordine il polipo la bicicletta poi il monopattino
			Tocca in ordine il polipo la bicicletta poi la bambina
			Tocca in ordine il polipo la bicicletta poi l'ombrello
			Tocca in ordine il polipo il monopattino poi il leone
			Tocca in ordine il polipo il monopattino poi la scimmia
			Tocca in ordine il polipo il monopattino poi il pappagallo
			Tocca in ordine il polipo il monopattino poi il gelato
			Tocca in ordine il polipo il monopattino poi gli occhiali da sole
			Tocca in ordine il polipo il monopattino poi la bicicletta
			Tocca in ordine il polipo il monopattino poi la bambina
			Tocca in ordine il polipo il monopattino poi l'ombrello
			Tocca in ordine il polipo la bambina poi il leone
			Tocca in ordine il polipo la bambina poi la scimmia
			Tocca in ordine il polipo la bambina poi il pappagallo
			Tocca in ordine il polipo la bambina poi il gelato
			Tocca in ordine il polipo la bambina poi gli occhiali da sole
			Tocca in ordine il polipo la bambina poi la bicicletta
			Tocca in ordine il polipo la bambina poi il monopattino
			Tocca in ordine il polipo la bambina poi l'ombrello
			Tocca in ordine il polipo l'ombrello poi il leone
			Tocca in ordine il polipo l'ombrello poi la scimmia
			Tocca in ordine il polipo l'ombrello poi il pappagallo
			Tocca in ordine il polipo l'ombrello poi il gelato
			Tocca in ordine il polipo l'ombrello poi gli occhiali da sole
			Tocca in ordine il polipo l'ombrello poi la bicicletta
			Tocca in ordine il polipo l'ombrello poi il monopattino
			Tocca in ordine il polipo l'ombrello poi la bambina
			Tocca in ordine il gelato il leone poi la scimmia
			Tocca in ordine il gelato il leone poi il pappagallo
			Tocca in ordine il gelato il leone poi il polipo
			Tocca in ordine il gelato il leone poi gli occhiali da sole
			Tocca in ordine il gelato il leone poi la bicicletta
			Tocca in ordine il gelato il leone poi il monopattino
			Tocca in ordine il gelato il leone poi la bambina
			Tocca in ordine il gelato il leone poi l'ombrello
			Tocca in ordine il gelato la scimmia poi il leone
			Tocca in ordine il gelato la scimmia poi il pappagallo
			Tocca in ordine il gelato la scimmia poi il polipo
			Tocca in ordine il gelato la scimmia poi gli occhiali da sole
			Tocca in ordine il gelato la scimmia poi la bicicletta
			Tocca in ordine il gelato la scimmia poi il monopattino
			Tocca in ordine il gelato la scimmia poi la bambina
			Tocca in ordine il gelato la scimmia poi l'ombrello
			Tocca in ordine il gelato poi il pappagallo poi il leone
			Tocca in ordine il gelato poi il pappagallo poi la scimmia
			Tocca in ordine il gelato poi il pappagallo poi il polipo
			Tocca in ordine il gelato poi il pappagallo poi gli occhiali da sole
			Tocca in ordine il gelato poi il pappagallo poi la bicicletta
			Tocca in ordine il gelato poi il pappagallo poi il monopattino
			Tocca in ordine il gelato poi il pappagallo poi la bambina
			Tocca in ordine il gelato poi il pappagallo poi l'ombrello
			Tocca in ordine il gelato poi il polipo poi il leone
			Tocca in ordine il gelato poi il polipo poi la scimmia
			Tocca in ordine il gelato poi il polipo poi il pappagallo
			Tocca in ordine il gelato poi il polipo poi gli occhiali da sole
			Tocca in ordine il gelato poi il polipo poi la bicicletta
			Tocca in ordine il gelato poi il polipo poi il monopattino
			Tocca in ordine il gelato poi il polipo poi la bambina
			Tocca in ordine il gelato poi il polipo poi l'ombrello
			Tocca in ordine il gelato pli gli occhiali da sole poi il leone
			Tocca in ordine il gelato pli gli occhiali da sole poi la scimmia
			Tocca in ordine il gelato pli gli occhiali da sole poi il pappagallo
			Tocca in ordine il gelato pli gli occhiali da sole poi il polipo
			Tocca in ordine il gelato pli gli occhiali da sole poi la bicicletta
			Tocca in ordine il gelato pli gli occhiali da sole poi il monopattino
			Tocca in ordine il gelato pli gli occhiali da sole poi la bambina
			Tocca in ordine il gelato pli gli occhiali da sole poi l'ombrello
			Tocca in ordine il gelato la la bicicletta poi il leone
			Tocca in ordine il gelato la la bicicletta poi la scimmia
			Tocca in ordine il gelato la la bicicletta poi il pappagallo
			Tocca in ordine il gelato la la bicicletta poi il polipo
			Tocca in ordine il gelato la la bicicletta poi gli occhiali da sole
			Tocca in ordine il gelato la la bicicletta poi il monopattino
			Tocca in ordine il gelato la la bicicletta poi la bambina
			Tocca in ordine il gelato la la bicicletta poi l'ombrello
			Tocca in ordine il gelato il monopattino poi il leone
			Tocca in ordine il gelato il monopattino poi la scimmia
			Tocca in ordine il gelato il monopattino poi il pappagallo
			Tocca in ordine il gelato il monopattino poi il polipo
			Tocca in ordine il gelato il monopattino poi gli occhiali da sole
			Tocca in ordine il gelato il monopattino poi la bicicletta
			Tocca in ordine il gelato il monopattino poi la bambina
			Tocca in ordine il gelato il monopattino poi l'ombrello
			Tocca in ordine il gelato la bambina poi il leone
			Tocca in ordine il gelato la bambina poi la scimmia
			Tocca in ordine il gelato la bambina poi il pappagallo
			Tocca in ordine il gelato la bambina poi il polipo
			Tocca in ordine il gelato la bambina poi gli occhiali da sole
			Tocca in ordine il gelato la bambina poi la bicicletta
			Tocca in ordine il gelato la bambina poi il monopattino
			Tocca in ordine il gelato la bambina poi l'ombrello
			Tocca in ordine il gelato l'ombrello poi leone
			Tocca in ordine il gelato l'ombrello poi la scimmia
			Tocca in ordine il gelato l'ombrello poi il pappagallo
			Tocca in ordine il gelato l'ombrello poi il polipo
			Tocca in ordine il gelato l'ombrello poi gli occhiali da sole
			Tocca in ordine il gelato l'ombrello poi la bicicletta
			Tocca in ordine il gelato l'ombrello poi il monopattino
			Tocca in ordine il gelato l'ombrello poi la bambina
			Tocca in ordine gli occhiali da sole poi il leone poi la scimmia
			Tocca in ordine gli occhiali da sole poi il leone poi il pappagallo
			Tocca in ordine gli occhiali da sole poi il leone poi il polipo
			Tocca in ordine gli occhiali da sole poi il leone poi il gelato
			Tocca in ordine gli occhiali da sole poi il leone poi la bicicletta
			Tocca in ordine gli occhiali da sole poi il leone poi il monopattino
			Tocca in ordine gli occhiali da sole poi il leone poi la bambina
			Tocca in ordine gli occhiali da sole poi il leone poi l'ombrello
			Tocca in ordine gli occhiali da sole poi la scimmia poi il leone
			Tocca in ordine gli occhiali da sole poi la scimmia poi il pappagallo
			Tocca in ordine gli occhiali da sole poi la scimmia poi il polipo
			Tocca in ordine gli occhiali da sole poi la scimmia poi il gelato
			Tocca in ordine gli occhiali da sole poi la scimmia poi la bicicletta
			Tocca in ordine gli occhiali da sole poi la scimmia poi il monopattino
			Tocca in ordine gli occhiali da sole poi la scimmia poi la bambina
			Tocca in ordine gli occhiali da sole poi la scimmia poi l'ombrello
			Tocca in ordine gli occhiali da sole poi il pappagallo poi il leone
			Tocca in ordine gli occhiali da sole poi il pappagallo poi la scimmia
			Tocca in ordine gli occhiali da sole poi il pappagallo poi il polipo
			Tocca in ordine gli occhiali da sole poi il pappagallo poi il gelato
			Tocca in ordine gli occhiali da sole poi il pappagallo poi la bicicletta
			Tocca in ordine gli occhiali da sole poi il pappagallo poi il monopattino
			Tocca in ordine gli occhiali da sole poi il pappagallo poi la bambina
			Tocca in ordine gli occhiali da sole poi il pappagallo poi l'ombrello
			Tocca in ordine gli occhiali da sole poi il polipo poi il leone
			Tocca in ordine gli occhiali da sole poi il polipo poi la scimmia
			Tocca in ordine gli occhiali da sole poi il polipo poi il pappagallo
			Tocca in ordine gli occhiali da sole poi il polipo poi il gelato
			Tocca in ordine gli occhiali da sole poi il polipo poi la bicicletta
			Tocca in ordine gli occhiali da sole poi il polipo poi il monopattino
			Tocca in ordine gli occhiali da sole poi il polipo poi la bambina
			Tocca in ordine gli occhiali da sole poi il polipo poi l'ombrello
			Tocca in ordine gli occhiali da sole poi il gelato poi il leone
			Tocca in ordine gli occhiali da sole poi il gelato poi la scimmia
			Tocca in ordine gli occhiali da sole poi il gelato poi il pappagallo
			Tocca in ordine gli occhiali da sole poi il gelato poi il polipo
			Tocca in ordine gli occhiali da sole poi il gelato poi la bicicletta
			Tocca in ordine gli occhiali da sole poi il gelato poi il monopattino
			Tocca in ordine gli occhiali da sole poi il gelato poi la bambina
			Tocca in ordine gli occhiali da sole poi il gelato poi l'ombrello
			Tocca in ordine gli occhiali da sole poi la bicicletta poi il leone
			Tocca in ordine gli occhiali da sole poi la bicicletta poi la scimmia
			Tocca in ordine gli occhiali da sole poi la bicicletta poi il pappagallo
			Tocca in ordine gli occhiali da sole poi la bicicletta poi il polipo
			Tocca in ordine gli occhiali da sole poi la bicicletta poi il gelato
			Tocca in ordine gli occhiali da sole poi la bicicletta poi il monopattino
			Tocca in ordine gli occhiali da sole poi la bicicletta poi la bambina
			Tocca in ordine gli occhiali da sole poi la bicicletta poi l'ombrello
			Tocca in ordine gli occhiali da sole poi il monopattino poi il leone
			Tocca in ordine gli occhiali da sole poi il monopattino poi la scimmia
			Tocca in ordine gli occhiali da sole poi il monopattino poi il pappagallo
			Tocca in ordine gli occhiali da sole poi il monopattino poi il polipo
			Tocca in ordine gli occhiali da sole poi il monopattino poi il gelato
			Tocca in ordine gli occhiali da sole poi il monopattino poi la bicicletta
			Tocca in ordine gli occhiali da sole poi il monopattino poi la bambina
			Tocca in ordine gli occhiali da sole poi il monopattino poi l'ombrello
			Tocca in ordine gli occhiali da sole poi la bambina poi il leone
			Tocca in ordine gli occhiali da sole poi la bambina poi la scimmia
			Tocca in ordine gli occhiali da sole poi la bambina poi il pappagallo
			Tocca in ordine gli occhiali da sole poi la bambina poi il polipo
			Tocca in ordine gli occhiali da sole poi la bambina poi il gelato
			Tocca in ordine gli occhiali da sole poi la bambina poi la bicicletta
			Tocca in ordine gli occhiali da sole poi la bambina poi il monopattino
			Tocca in ordine gli occhiali da sole poi la bambina poi l'ombrello
			Tocca in ordine gli occhiali da sole poi l'ombrello poi il leone
			Tocca in ordine gli occhiali da sole poi l'ombrello poi la scimmia
			Tocca in ordine gli occhiali da sole poi l'ombrello poi il pappagallo
			Tocca in ordine gli occhiali da sole poi l'ombrello poi il polipo
			Tocca in ordine gli occhiali da sole poi l'ombrello poi il gelato
			Tocca in ordine gli occhiali da sole poi l'ombrello poi la bicicletta
			Tocca in ordine gli occhiali da sole poi l'ombrello poi il monopattino
			Tocca in ordine gli occhiali da sole poi l'ombrello poi la bambina
			Tocca in ordine la bicicletta poi il leone poi la scimmia
			Tocca in ordine la bicicletta poi il leone poi il pappagallo
			Tocca in ordine la bicicletta poi il leone poi il polipo
			Tocca in ordine la bicicletta poi il leone poi il gelato
			Tocca in ordine la bicicletta poi il leone poi gli occhiali da sole
			Tocca in ordine la bicicletta poi il leone poi il monopattino
			Tocca in ordine la bicicletta poi il leone poi la bambina
			Tocca in ordine la bicicletta poi il leone poi l'ombrello
			Tocca in ordine la bicicletta poi la scimmia poi il leone
			Tocca in ordine la bicicletta poi la scimmia poi il pappagallo
			Tocca in ordine la bicicletta poi la scimmia poi il polipo
			Tocca in ordine la bicicletta poi la scimmia poi il gelato
			Tocca in ordine la bicicletta poi la scimmia poi gli occhiali da sole
			Tocca in ordine la bicicletta poi la scimmia poi il monopattino
			Tocca in ordine la bicicletta poi la scimmia poi la bambina
			Tocca in ordine la bicicletta poi la scimmia poi l'ombrello
			Tocca in ordine la bicicletta poi il pappagallo poi il leone
			Tocca in ordine la bicicletta poi il pappagallo poi la scimmia
			Tocca in ordine la bicicletta poi il pappagallo poi il polipo
			Tocca in ordine la bicicletta poi il pappagallo poi il gelato
			Tocca in ordine la bicicletta poi il pappagallo poi gli occhiali da sole
			Tocca in ordine la bicicletta poi il pappagallo poi il monopattino
			Tocca in ordine la bicicletta poi il pappagallo poi la bambina
			Tocca in ordine la bicicletta poi il pappagallo poi l'ombrello
			Tocca in ordine la bicicletta poi il polipo poi il leone
			Tocca in ordine la bicicletta poi il polipo poi la scimmia
			Tocca in ordine la bicicletta poi il polipo poi il pappagallo
			Tocca in ordine la bicicletta poi il polipo poi il gelato
			Tocca in ordine la bicicletta poi il polipo poi gli occhiali da sole
			Tocca in ordine la bicicletta poi il polipo poi il monopattino
			Tocca in ordine la bicicletta poi il polipo poi la bambina
			Tocca in ordine la bicicletta poi il polipo poi l'ombrello
			Tocca in ordine la bicicletta poi il gelato poi il leone
			Tocca in ordine la bicicletta poi il gelato poi la scimmia
			Tocca in ordine la bicicletta poi il gelato poi il pappagallo
			Tocca in ordine la bicicletta poi il gelato poi il polipo
			Tocca in ordine la bicicletta poi il gelato poi gli occhiali da sole
			Tocca in ordine la bicicletta poi il gelato poi il monopattino
			Tocca in ordine la bicicletta poi il gelato poi la bambina
			Tocca in ordine la bicicletta poi il gelato poi l'ombrello
			Tocca in ordine la bicicletta poi gli occhiali da sole poi il leone
			Tocca in ordine la bicicletta poi gli occhiali da sole poi la scimmia
			Tocca in ordine la bicicletta poi gli occhiali da sole poi il pappagallo
			Tocca in ordine la bicicletta poi gli occhiali da sole poi il polipo
			Tocca in ordine la bicicletta poi gli occhiali da sole poi il gelato
			Tocca in ordine la bicicletta poi gli occhiali da sole poi il monopattino
			Tocca in ordine la bicicletta poi gli occhiali da sole poi la bambina
			Tocca in ordine la bicicletta poi gli occhiali da sole poi l'ombrello
			Tocca in ordine la bicicletta poi il monopattino poi il leone
			Tocca in ordine la bicicletta poi il monopattino poi la scimmia
			Tocca in ordine la bicicletta poi il monopattino poi il pappagallo
			Tocca in ordine la bicicletta poi il monopattino poi il polipo
			Tocca in ordine la bicicletta poi il monopattino poi il gelato
			Tocca in ordine la bicicletta poi il monopattino poi gli occhiali da sole
			Tocca in ordine la bicicletta poi il monopattino poi la bambina
			Tocca in ordine la bicicletta poi il monopattino poi l'ombrello
			Tocca in ordine la bicicletta poi la bambina poi il leone
			Tocca in ordine la bicicletta poi la bambina poi la scimmia
			Tocca in ordine la bicicletta poi la bambina poi il pappagallo
			Tocca in ordine la bicicletta poi la bambina poi il polipo
			Tocca in ordine la bicicletta poi la bambina poi il gelato
			Tocca in ordine la bicicletta poi la bambina poi gli occhiali da sole
			Tocca in ordine la bicicletta poi la bambina poi il monopattino
			Tocca in ordine la bicicletta poi la bambina poi l'ombrello
			Tocca in ordine la bicicletta poi l'ombrello poi il leone
			Tocca in ordine la bicicletta poi l'ombrello poi la scimmia
			Tocca in ordine la bicicletta poi l'ombrello poi il pappagallo
			Tocca in ordine la bicicletta poi l'ombrello poi il polipo
			Tocca in ordine la bicicletta poi l'ombrello poi il gelato
			Tocca in ordine la bicicletta poi l'ombrello poi gli occhiali da sole
			Tocca in ordine la bicicletta poi l'ombrello poi il monopattino
			Tocca in ordine la bicicletta poi l'ombrello poi la bambina
			Tocca in ordine il monopattino poi il leone poi la scimmia
			Tocca in ordine il monopattino poi il leone poi il pappagallo
			Tocca in ordine il monopattino poi il leone poi il polipo
			Tocca in ordine il monopattino poi il leone poi il gelato
			Tocca in ordine il monopattino poi il leone poi gli occhiali da sole
			Tocca in ordine il monopattino poi il leone poi la bicicletta
			Tocca in ordine il monopattino poi il leone poi la bambina
			Tocca in ordine il monopattino poi il leone poi l'ombrello
			Tocca in ordine il monopattino poi la scimmia poi il leone
			Tocca in ordine il monopattino poi la scimmia poi il pappagallo
			Tocca in ordine il monopattino poi la scimmia poi il polipo
			Tocca in ordine il monopattino poi la scimmia poi il gelato
			Tocca in ordine il monopattino poi la scimmia poi gli occhiali da sole
			Tocca in ordine il monopattino poi la scimmia poi la bicicletta
			Tocca in ordine il monopattino poi la scimmia poi la bambina
			Tocca in ordine il monopattino poi la scimmia poi l'ombrello
			Tocca in ordine il monopattino poi il pappagallo poi il leone
			Tocca in ordine il monopattino poi il pappagallo poi la scimmia
			Tocca in ordine il monopattino poi il pappagallo poi il polipo
			Tocca in ordine il monopattino poi il pappagallo poi il gelato
			Tocca in ordine il monopattino poi il pappagallo poi gli occhiali da sole
			Tocca in ordine il monopattino poi il pappagallo poi la bicicletta
			Tocca in ordine il monopattino poi il pappagallo poi la bambina
			Tocca in ordine il monopattino poi il pappagallo poi l'ombrello
			Tocca in ordine il monopattino poi il polipo poi il leone
			Tocca in ordine il monopattino poi il polipo poi la scimmia
			Tocca in ordine il monopattino poi il polipo poi il pappagallo
			Tocca in ordine il monopattino poi il polipo poi il gelato
			Tocca in ordine il monopattino poi il polipo poi gli occhiali da sole
			Tocca in ordine il monopattino poi il polipo poi la bicicletta
			Tocca in ordine il monopattino poi il polipo poi la bambina
			Tocca in ordine il monopattino poi il polipo poi l'ombrello
			Tocca in ordine il monopattino poi il gelato poi il leone
			Tocca in ordine il monopattino poi il gelato poi la scimmia
			Tocca in ordine il monopattino poi il gelato poi il pappagallo
			Tocca in ordine il monopattino poi il gelato poi il polipo
			Tocca in ordine il monopattino poi il gelato poi gli occhiali da sole
			Tocca in ordine il monopattino poi il gelato poi la bicicletta
			Tocca in ordine il monopattino poi il gelato poi la bambina
			Tocca in ordine il monopattino poi il gelato poi l'ombrello
			Tocca in ordine il monopattino poi gli occhiali da sole poi il leone
			Tocca in ordine il monopattino poi gli occhiali da sole poi la scimmia
			Tocca in ordine il monopattino poi gli occhiali da sole poi il pappagallo
			Tocca in ordine il monopattino poi gli occhiali da sole poi il polipo
			Tocca in ordine il monopattino poi gli occhiali da sole poi il gelato
			Tocca in ordine il monopattino poi gli occhiali da sole poi la bicicletta
			Tocca in ordine il monopattino poi gli occhiali da sole poi la bambina
			Tocca in ordine il monopattino poi gli occhiali da sole poi l'ombrello
			Tocca in ordine il monopattino poi la bicicletta poi il leone
			Tocca in ordine il monopattino poi la bicicletta poi la scimmia
			Tocca in ordine il monopattino poi la bicicletta poi il pappagallo
			Tocca in ordine il monopattino poi la bicicletta poi il polipo
			Tocca in ordine il monopattino poi la bicicletta poi il gelato
			Tocca in ordine il monopattino poi la bicicletta poi gli occhiali da sole
			Tocca in ordine il monopattino poi la bicicletta poi la bambina
			Tocca in ordine il monopattino poi la bicicletta poi l'ombrello
			Tocca in ordine il monopattino poi la bambina poi il leone
			Tocca in ordine il monopattino poi la bambina poi la scimmia
			Tocca in ordine il monopattino poi la bambina poi il pappagallo
			Tocca in ordine il monopattino poi la bambina poi il polipo
			Tocca in ordine il monopattino poi la bambina poi il gelato
			Tocca in ordine il monopattino poi la bambina poi gli occhiali da sole
			Tocca in ordine il monopattino poi la bambina poi la bicicletta
			Tocca in ordine il monopattino poi la bambina poi l'ombrello
			Tocca in ordine il monopattino poi l'ombrello poi il leone
			Tocca in ordine il monopattino poi l'ombrello poi la scimmia
			Tocca in ordine il monopattino poi l'ombrello poi il pappagallo
			Tocca in ordine il monopattino poi l'ombrello poi il polipo
			Tocca in ordine il monopattino poi l'ombrello poi il gelato
			Tocca in ordine il monopattino poi l'ombrello poi gli occhiali da sole
			Tocca in ordine il monopattino poi l'ombrello poi la bicicletta
			Tocca in ordine il monopattino poi l'ombrello poi la bambina
			Tocca in ordine la bambina poi il leone poi la scimmia
			Tocca in ordine la bambina poi il leone poi il pappagallo
			Tocca in ordine la bambina poi il leone poi il polipo
			Tocca in ordine la bambina poi il leone poi il gelato
			Tocca in ordine la bambina poi il leone poi gli occhiali da sole
			Tocca in ordine la bambina poi il leone poi la bicicletta
			Tocca in ordine la bambina poi il leone poi il monopattino
			Tocca in ordine la bambina poi il leone poi l'ombrello
			Tocca in ordine la bambina poi la scimmia poi il leone
			Tocca in ordine la bambina poi la scimmia poi il pappagallo
			Tocca in ordine la bambina poi la scimmia poi il polipo
			Tocca in ordine la bambina poi la scimmia poi il gelato
			Tocca in ordine la bambina poi la scimmia poi gli occhiali da sole
			Tocca in ordine la bambina poi la scimmia poi la bicicletta
			Tocca in ordine la bambina poi la scimmia poi il monopattino
			Tocca in ordine la bambina poi la scimmia poi l'ombrello
			Tocca in ordine la bambina poi il pappagallo poi il leone
			Tocca in ordine la bambina poi il pappagallo poi la scimmia
			Tocca in ordine la bambina poi il pappagallo poi il polipo
			Tocca in ordine la bambina poi il pappagallo poi il gelato
			Tocca in ordine la bambina poi il pappagallo poi gli occhiali da sole
			Tocca in ordine la bambina poi il pappagallo poi la bicicletta
			Tocca in ordine la bambina poi il pappagallo poi il monopattino
			Tocca in ordine la bambina poi il pappagallo poi l'ombrello
			Tocca in ordine la bambina poi il polipo poi il leone
			Tocca in ordine la bambina poi il polipo poi la scimmia
			Tocca in ordine la bambina poi il polipo poi il pappagallo
			Tocca in ordine la bambina poi il polipo poi il gelato
			Tocca in ordine la bambina poi il polipo poi gli occhiali da sole
			Tocca in ordine la bambina poi il polipo poi la bicicletta
			Tocca in ordine la bambina poi il polipo poi il monopattino
			Tocca in ordine la bambina poi il polipo poi l'ombrello
			Tocca in ordine la bambina poi il gelato poi il leone
			Tocca in ordine la bambina poi il gelato poi la scimmia
			Tocca in ordine la bambina poi il gelato poi il pappagallo
			Tocca in ordine la bambina poi il gelato poi il polipo
			Tocca in ordine la bambina poi il gelato poi gli occhiali da sole
			Tocca in ordine la bambina poi il gelato poi la bicicletta
			Tocca in ordine la bambina poi il gelato poi il monopattino
			Tocca in ordine la bambina poi il gelato poi l'ombrello
			Tocca in ordine la bambina poi gli occhiali da sole poi da sole il leone
			Tocca in ordine la bambina poi gli occhiali da sole poi da sole la scimmia
			Tocca in ordine la bambina poi gli occhiali da sole poi da sole il pappagallo
			Tocca in ordine la bambina poi gli occhiali da sole poi da sole il polipo
			Tocca in ordine la bambina poi gli occhiali da sole poi da sole il gelato
			Tocca in ordine la bambina poi gli occhiali da sole poi da sole la bicicletta
			Tocca in ordine la bambina poi gli occhiali da sole poi da sole il monopattino
			Tocca in ordine la bambina poi gli occhiali da sole poi da sole l'ombrello
			Tocca in ordine la bambina poi la bicicletta poi il leone
			Tocca in ordine la bambina poi la bicicletta poi la scimmia
			Tocca in ordine la bambina poi la bicicletta poi il pappagallo
			Tocca in ordine la bambina poi la bicicletta poi il polipo
			Tocca in ordine la bambina poi la bicicletta poi il gelato
			Tocca in ordine la bambina poi la bicicletta poi gli occhiali da sole
			Tocca in ordine la bambina poi la bicicletta poi il monopattino
			Tocca in ordine la bambina poi la bicicletta poi l'ombrello
			Tocca in ordine la bambina poi il monopattino poi il leone
			Tocca in ordine la bambina poi il monopattino poi la scimmia
			Tocca in ordine la bambina poi il monopattino poi il pappagallo
			Tocca in ordine la bambina poi il monopattino poi il polipo
			Tocca in ordine la bambina poi il monopattino poi il gelato
			Tocca in ordine la bambina poi il monopattino poi gli occhiali da sole
			Tocca in ordine la bambina poi il monopattino poi la bicicletta
			Tocca in ordine la bambina poi il monopattino poi l'ombrello
			Tocca in ordine la bambina poi l'ombrello poi il leone
			Tocca in ordine la bambina poi l'ombrello poi la scimmia
			Tocca in ordine la bambina poi l'ombrello poi il pappagallo
			Tocca in ordine la bambina poi l'ombrello poi il polipo
			Tocca in ordine la bambina poi l'ombrello poi il gelato
			Tocca in ordine la bambina poi l'ombrello poi gli occhiali da sole
			Tocca in ordine la bambina poi l'ombrello poi la bicicletta
			Tocca in ordine la bambina poi l'ombrello poi il monopattino
			Tocca in ordine l'ombrello poi il leone poi la scimmia
			Tocca in ordine l'ombrello poi il leone poi il pappagallo
			Tocca in ordine l'ombrello poi il leone poi il polipo
			Tocca in ordine l'ombrello poi il leone poi il gelato
			Tocca in ordine l'ombrello poi il leone poi gli occhiali da sole
			Tocca in ordine l'ombrello poi il leone poi la bicicletta
			Tocca in ordine l'ombrello poi il leone poi il monopattino
			Tocca in ordine l'ombrello poi il leone poi la bambina
			Tocca in ordine l'ombrello poi la scimmia poi il leone
			Tocca in ordine l'ombrello poi la scimmia poi il pappagallo
			Tocca in ordine l'ombrello poi la scimmia poi il polipo
			Tocca in ordine l'ombrello poi la scimmia poi il gelato
			Tocca in ordine l'ombrello poi la scimmia poi gli occhiali da sole
			Tocca in ordine l'ombrello poi la scimmia poi la bicicletta
			Tocca in ordine l'ombrello poi la scimmia poi il monopattino
			Tocca in ordine l'ombrello poi la scimmia poi la bambina
			Tocca in ordine l'ombrello poi il pappagallo poi il leone
			Tocca in ordine l'ombrello poi il pappagallo poi la scimmia
			Tocca in ordine l'ombrello poi il pappagallo poi il polipo
			Tocca in ordine l'ombrello poi il pappagallo poi il gelato
			Tocca in ordine l'ombrello poi il pappagallo poi gli occhiali da sole
			Tocca in ordine l'ombrello poi il pappagallo poi la bicicletta
			Tocca in ordine l'ombrello poi il pappagallo poi il monopattino
			Tocca in ordine l'ombrello poi il pappagallo poi la bambina
			Tocca in ordine l'ombrello poi il polipo poi il leone
			Tocca in ordine l'ombrello poi il polipo poi la scimmia
			Tocca in ordine l'ombrello poi il polipo poi il pappagallo
			Tocca in ordine l'ombrello poi il polipo poi il gelato
			Tocca in ordine l'ombrello poi il polipo poi gli occhiali da sole
			Tocca in ordine l'ombrello poi il polipo poi la bicicletta
			Tocca in ordine l'ombrello poi il polipo poi il monopattino
			Tocca in ordine l'ombrello poi il polipo poi la bambina
			Tocca in ordine l'ombrello poi il gelato poi il leone
			Tocca in ordine l'ombrello poi il gelato poi la scimmia
			Tocca in ordine l'ombrello poi il gelato poi il pappagallo
			Tocca in ordine l'ombrello poi il gelato poi il polipo
			Tocca in ordine l'ombrello poi il gelato poi gli occhiali da sole
			Tocca in ordine l'ombrello poi il gelato poi la bicicletta
			Tocca in ordine l'ombrello poi il gelato poi il monopattino
			Tocca in ordine l'ombrello poi il gelato poi la bambina
			Tocca in ordine l'ombrello poi gli occhiali da sole poi il leone
			Tocca in ordine l'ombrello poi gli occhiali da sole poi la scimmia
			Tocca in ordine l'ombrello poi gli occhiali da sole poi il pappagallo
			Tocca in ordine l'ombrello poi gli occhiali da sole poi il polipo
			Tocca in ordine l'ombrello poi gli occhiali da sole poi il gelato
			Tocca in ordine l'ombrello poi gli occhiali da sole poi la bicicletta
			Tocca in ordine l'ombrello poi gli occhiali da sole poi il monopattino
			Tocca in ordine l'ombrello poi gli occhiali da sole poi la bambina
			Tocca in ordine l'ombrello poi la bicicletta poi il leone
			Tocca in ordine l'ombrello poi la bicicletta poi la scimmia
			Tocca in ordine l'ombrello poi la bicicletta poi il pappagallo
			Tocca in ordine l'ombrello poi la bicicletta poi il polipo
			Tocca in ordine l'ombrello poi la bicicletta poi il gelato
			Tocca in ordine l'ombrello poi la bicicletta poi gli occhiali da sole
			Tocca in ordine l'ombrello poi la bicicletta poi il monopattino
			Tocca in ordine l'ombrello poi la bicicletta poi la bambina
			Tocca in ordine l'ombrello poi il monopattino poi il leone
			Tocca in ordine l'ombrello poi il monopattino poi la scimmia
			Tocca in ordine l'ombrello poi il monopattino poi il pappagallo
			Tocca in ordine l'ombrello poi il monopattino poi il polipo
			Tocca in ordine l'ombrello poi il monopattino poi il gelato
			Tocca in ordine l'ombrello poi il monopattino poi gli occhiali da sole
			Tocca in ordine l'ombrello poi il monopattino poi la bicicletta
			Tocca in ordine l'ombrello poi il monopattino poi la bambina
			Tocca in ordine l'ombrello poi la bambina poi il leone
			Tocca in ordine l'ombrello poi la bambina poi la scimmia
			Tocca in ordine l'ombrello poi la bambina poi il pappagallo
			Tocca in ordine l'ombrello poi la bambina poi il polipo
			Tocca in ordine l'ombrello poi la bambina poi il gelato
			Tocca in ordine l'ombrello poi la bambina poi gli occhiali da sole
			Tocca in ordine l'ombrello poi la bambina poi la bicicletta
			Tocca in ordine l'ombrello poi la bambina poi il monopattino
		7OSU4 = Order subject for 4 patches
			Tocca nel seguente ordine il leone la scimmia il pappagallo il polipo
			Tocca nel seguente ordine il leone la scimmia il pappagallo il gelato
			Tocca nel seguente ordine il leone la scimmia il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il leone la scimmia il pappagallo la bicicletta
			Tocca nel seguente ordine il leone la scimmia il pappagallo il monopattino
			Tocca nel seguente ordine il leone la scimmia il pappagallo la bambina
			Tocca nel seguente ordine il leone la scimmia il pappagallo l'ombrello
			Tocca nel seguente ordine il leone la scimmia il polipo il pappagallo
			Tocca nel seguente ordine il leone la scimmia il polipo il gelato
			Tocca nel seguente ordine il leone la scimmia il polipo gli occhiali da sole
			Tocca nel seguente ordine il leone la scimmia il polipo la bicicletta
			Tocca nel seguente ordine il leone la scimmia il polipo il monopattino
			Tocca nel seguente ordine il leone la scimmia il polipo la bambina
			Tocca nel seguente ordine il leone la scimmia il polipo l'ombrello
			Tocca nel seguente ordine il leone la scimmia il gelato il polipo
			Tocca nel seguente ordine il leone la scimmia il gelato il pappagallo
			Tocca nel seguente ordine il leone la scimmia il gelato gli occhiali da sole
			Tocca nel seguente ordine il leone la scimmia il gelato la bicicletta
			Tocca nel seguente ordine il leone la scimmia il gelato il monopattino
			Tocca nel seguente ordine il leone la scimmia il gelato la bambina
			Tocca nel seguente ordine il leone la scimmia il gelato l'ombrello
			Tocca nel seguente ordine il leone la scimmia gli occhiali da sole il gelato
			Tocca nel seguente ordine il leone la scimmia gli occhiali da sole il polipo
			Tocca nel seguente ordine il leone la scimmia gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il leone la scimmia gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il leone la scimmia gli occhiali da sole il monopattino
			Tocca nel seguente ordine il leone la scimmia gli occhiali da sole la bambina
			Tocca nel seguente ordine il leone la scimmia gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il leone la scimmia la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il leone la scimmia la bicicletta il polipo
			Tocca nel seguente ordine il leone la scimmia la bicicletta il pappagallo
			Tocca nel seguente ordine il leone la scimmia la bicicletta il gelato
			Tocca nel seguente ordine il leone la scimmia la bicicletta il monopattino
			Tocca nel seguente ordine il leone la scimmia la bicicletta la bambina
			Tocca nel seguente ordine il leone la scimmia la bicicletta l'ombrello
			Tocca nel seguente ordine il leone la scimmia il monopattino gli occhiali da sole
			Tocca nel seguente ordine il leone la scimmia il monopattino il polipo
			Tocca nel seguente ordine il leone la scimmia il monopattino il pappagallo
			Tocca nel seguente ordine il leone la scimmia il monopattino la bicicletta
			Tocca nel seguente ordine il leone la scimmia il monopattino il gelato
			Tocca nel seguente ordine il leone la scimmia il monopattino la bambina
			Tocca nel seguente ordine il leone la scimmia il monopattino l'ombrello
			Tocca nel seguente ordine il leone la scimmia la bambina gli occhiali da sole
			Tocca nel seguente ordine il leone la scimmia la bambina il polipo
			Tocca nel seguente ordine il leone la scimmia la bambina il pappagallo
			Tocca nel seguente ordine il leone la scimmia la bambina la bicicletta
			Tocca nel seguente ordine il leone la scimmia la bambina il monopattino
			Tocca nel seguente ordine il leone la scimmia la bambina il gelato
			Tocca nel seguente ordine il leone la scimmia la bambina l'ombrello
			Tocca nel seguente ordine il leone la scimmia l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il leone la scimmia l'ombrello il polipo
			Tocca nel seguente ordine il leone la scimmia l'ombrello il pappagallo
			Tocca nel seguente ordine il leone la scimmia l'ombrello la bicicletta
			Tocca nel seguente ordine il leone la scimmia l'ombrello il monopattino
			Tocca nel seguente ordine il leone la scimmia l'ombrello la bambina
			Tocca nel seguente ordine il leone la scimmia l'ombrello il gelato
			Tocca nel seguente ordine il leone il pappagallo la scimmia gli occhiali da sole
			Tocca nel seguente ordine il leone il pappagallo la scimmia il polipo
			Tocca nel seguente ordine il leone il pappagallo la scimmia il gelato
			Tocca nel seguente ordine il leone il pappagallo la scimmia la bicicletta
			Tocca nel seguente ordine il leone il pappagallo la scimmia il monopattino
			Tocca nel seguente ordine il leone il pappagallo la scimmia la bambina
			Tocca nel seguente ordine il leone il pappagallo la scimmia l'ombrello
			Tocca nel seguente ordine il leone il pappagallo il polipo la scimmia
			Tocca nel seguente ordine il leone il pappagallo il polipo il gelato
			Tocca nel seguente ordine il leone il pappagallo il polipo gli occhiali da sole
			Tocca nel seguente ordine il leone il pappagallo il polipo la bicicletta
			Tocca nel seguente ordine il leone il pappagallo il polipo il monopattino
			Tocca nel seguente ordine il leone il pappagallo il polipo la bambina
			Tocca nel seguente ordine il leone il pappagallo il polipo l'ombrello
			Tocca nel seguente ordine il leone il pappagallo il gelato la scimmia
			Tocca nel seguente ordine il leone il pappagallo il gelato il polipo
			Tocca nel seguente ordine il leone il pappagallo il gelato gli occhiali da sole
			Tocca nel seguente ordine il leone il pappagallo il gelato la bicicletta
			Tocca nel seguente ordine il leone il pappagallo il gelato il monopattino
			Tocca nel seguente ordine il leone il pappagallo il gelato la bambina
			Tocca nel seguente ordine il leone il pappagallo il gelato l'ombrello
			Tocca nel seguente ordine il leone il pappagallo gli occhiali da sole la scimmia
			Tocca nel seguente ordine il leone il pappagallo gli occhiali da sole il polipo
			Tocca nel seguente ordine il leone il pappagallo gli occhiali da sole il gelato
			Tocca nel seguente ordine il leone il pappagallo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il leone il pappagallo gli occhiali da sole il monopattino
			Tocca nel seguente ordine il leone il pappagallo gli occhiali da sole la bambina
			Tocca nel seguente ordine il leone il pappagallo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il leone il pappagallo la bicicletta la scimmia
			Tocca nel seguente ordine il leone il pappagallo la bicicletta il polipo
			Tocca nel seguente ordine il leone il pappagallo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il leone il pappagallo la bicicletta il gelato
			Tocca nel seguente ordine il leone il pappagallo la bicicletta il monopattino
			Tocca nel seguente ordine il leone il pappagallo la bicicletta la bambina
			Tocca nel seguente ordine il leone il pappagallo la bicicletta l'ombrello
			Tocca nel seguente ordine il leone il pappagallo il monopattino la scimmia
			Tocca nel seguente ordine il leone il pappagallo il monopattino il polipo
			Tocca nel seguente ordine il leone il pappagallo il monopattino gli occhiali da sole
			Tocca nel seguente ordine il leone il pappagallo il monopattino la bicicletta
			Tocca nel seguente ordine il leone il pappagallo il monopattino il gelato
			Tocca nel seguente ordine il leone il pappagallo il monopattino la bambina
			Tocca nel seguente ordine il leone il pappagallo il monopattino l'ombrello
			Tocca nel seguente ordine il leone il pappagallo la bambina la scimmia
			Tocca nel seguente ordine il leone il pappagallo la bambina il polipo
			Tocca nel seguente ordine il leone il pappagallo la bambina gli occhiali da sole
			Tocca nel seguente ordine il leone il pappagallo la bambina la bicicletta
			Tocca nel seguente ordine il leone il pappagallo la bambina il monopattino
			Tocca nel seguente ordine il leone il pappagallo la bambina il gelato
			Tocca nel seguente ordine il leone il pappagallo la bambina l'ombrello
			Tocca nel seguente ordine il leone il pappagallo l'ombrello la scimmia
			Tocca nel seguente ordine il leone il pappagallo l'ombrello il polipo
			Tocca nel seguente ordine il leone il pappagallo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il leone il pappagallo l'ombrello la bicicletta
			Tocca nel seguente ordine il leone il pappagallo l'ombrello il monopattino
			Tocca nel seguente ordine il leone il pappagallo l'ombrello la bambina
			Tocca nel seguente ordine il leone il pappagallo l'ombrello il gelato
			Tocca nel seguente ordine il leone il polipo la scimmia il gelato
			Tocca nel seguente ordine il leone il polipo la scimmia il gli occhiali da sole
			Tocca nel seguente ordine il leone il polipo la scimmia il pappagallo
			Tocca nel seguente ordine il leone il polipo la scimmia la bicicletta
			Tocca nel seguente ordine il leone il polipo la scimmia il monopattino
			Tocca nel seguente ordine il leone il polipo la scimmia la bambina
			Tocca nel seguente ordine il leone il polipo la scimmia l'ombrello
			Tocca nel seguente ordine il leone il polipo il pappagallo la scimmia
			Tocca nel seguente ordine il leone il polipo il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il leone il polipo il pappagallo il gelato
			Tocca nel seguente ordine il leone il polipo il pappagallo la bicicletta
			Tocca nel seguente ordine il leone il polipo il pappagallo il monopattino
			Tocca nel seguente ordine il leone il polipo il pappagallo la bambina
			Tocca nel seguente ordine il leone il polipo il pappagallo l'ombrello
			Tocca nel seguente ordine il leone il polipo il gelato la scimmia
			Tocca nel seguente ordine il leone il polipo il gelato gli occhiali da sole
			Tocca nel seguente ordine il leone il polipo il gelato il pappagallo
			Tocca nel seguente ordine il leone il polipo il gelato la bicicletta
			Tocca nel seguente ordine il leone il polipo il gelato il monopattino
			Tocca nel seguente ordine il leone il polipo il gelato la bambina
			Tocca nel seguente ordine il leone il polipo il gelato l'ombrello
			Tocca nel seguente ordine il leone il polipo gli occhiali da sole la scimmia
			Tocca nel seguente ordine il leone il polipo gli occhiali da sole il gelato
			Tocca nel seguente ordine il leone il polipo gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il leone il polipo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il leone il polipo gli occhiali da sole il monopattino
			Tocca nel seguente ordine il leone il polipo gli occhiali da sole la bambina
			Tocca nel seguente ordine il leone il polipo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il leone il polipo la bicicletta la scimmia
			Tocca nel seguente ordine il leone il polipo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il leone il polipo la bicicletta il pappagallo
			Tocca nel seguente ordine il leone il polipo la bicicletta il gelato
			Tocca nel seguente ordine il leone il polipo la bicicletta il monopattino
			Tocca nel seguente ordine il leone il polipo la bicicletta la bambina
			Tocca nel seguente ordine il leone il polipo la bicicletta l'ombrello
			Tocca nel seguente ordine il leone il polipo il monopattino la scimmia
			Tocca nel seguente ordine il leone il polipo il monopattino gli occhiali da sole
			Tocca nel seguente ordine il leone il polipo il monopattino il pappagallo
			Tocca nel seguente ordine il leone il polipo il monopattino la bicicletta
			Tocca nel seguente ordine il leone il polipo il monopattino il gelato
			Tocca nel seguente ordine il leone il polipo il monopattino la bambina
			Tocca nel seguente ordine il leone il polipo il monopattino l'ombrello
			Tocca nel seguente ordine il leone il polipo la bambina la scimmia
			Tocca nel seguente ordine il leone il polipo la bambina gli occhiali da sole
			Tocca nel seguente ordine il leone il polipo la bambina il pappagallo
			Tocca nel seguente ordine il leone il polipo la bambina la bicicletta
			Tocca nel seguente ordine il leone il polipo la bambina il monopattino
			Tocca nel seguente ordine il leone il polipo la bambina il gelato
			Tocca nel seguente ordine il leone il polipo la bambina l'ombrello
			Tocca nel seguente ordine il leone il polipo l'ombrello la scimmia
			Tocca nel seguente ordine il leone il polipo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il leone il polipo l'ombrello il pappagallo
			Tocca nel seguente ordine il leone il polipo l'ombrello la bicicletta
			Tocca nel seguente ordine il leone il polipo l'ombrello il monopattino
			Tocca nel seguente ordine il leone il polipo l'ombrello la bambina
			Tocca nel seguente ordine il leone il polipo l'ombrello il gelato
			Tocca nel seguente ordine il leone il gelato la scimmia il polipo
			Tocca nel seguente ordine il leone il gelato la scimmia gli occhiali da sole
			Tocca nel seguente ordine il leone il gelato la scimmia il pappagallo
			Tocca nel seguente ordine il leone il gelato la scimmia la bicicletta
			Tocca nel seguente ordine il leone il gelato la scimmia il monopattino
			Tocca nel seguente ordine il leone il gelato la scimmia la bambina
			Tocca nel seguente ordine il leone il gelato la scimmia l'ombrello
			Tocca nel seguente ordine il leone il gelato il pappagallo la scimmia
			Tocca nel seguente ordine il leone il gelato il pappagallo il polipo
			Tocca nel seguente ordine il leone il gelato il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il leone il gelato il pappagallo la bicicletta
			Tocca nel seguente ordine il leone il gelato il pappagallo il monopattino
			Tocca nel seguente ordine il leone il gelato il pappagallo la bambina
			Tocca nel seguente ordine il leone il gelato il pappagallo l'ombrello
			Tocca nel seguente ordine il leone il gelato il polipo la scimmia
			Tocca nel seguente ordine il leone il gelato il polipo gli occhiali da sole
			Tocca nel seguente ordine il leone il gelato il polipo il pappagallo
			Tocca nel seguente ordine il leone il gelato il polipo la bicicletta
			Tocca nel seguente ordine il leone il gelato il polipo il monopattino
			Tocca nel seguente ordine il leone il gelato il polipo la bambina
			Tocca nel seguente ordine il leone il gelato il polipo l'ombrello
			Tocca nel seguente ordine il leone il gelato gli occhiali da sole la scimmia
			Tocca nel seguente ordine il leone il gelato gli occhiali da sole il polipo
			Tocca nel seguente ordine il leone il gelato gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il leone il gelato gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il leone il gelato gli occhiali da sole il monopattino
			Tocca nel seguente ordine il leone il gelato gli occhiali da sole la bambina
			Tocca nel seguente ordine il leone il gelato gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il leone il gelato la bicicletta la scimmia
			Tocca nel seguente ordine il leone il gelato la bicicletta il polipo
			Tocca nel seguente ordine il leone il gelato la bicicletta il pappagallo
			Tocca nel seguente ordine il leone il gelato la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il leone il gelato la bicicletta il monopattino
			Tocca nel seguente ordine il leone il gelato la bicicletta la bambina
			Tocca nel seguente ordine il leone il gelato la bicicletta l'ombrello
			Tocca nel seguente ordine il leone il gelato il monopattino la scimmia
			Tocca nel seguente ordine il leone il gelato il monopattino il polipo
			Tocca nel seguente ordine il leone il gelato il monopattino il pappagallo
			Tocca nel seguente ordine il leone il gelato il monopattino la bicicletta
			Tocca nel seguente ordine il leone il gelato il monopattino gli occhiali da sole
			Tocca nel seguente ordine il leone il gelato il monopattino la bambina
			Tocca nel seguente ordine il leone il gelato il monopattino l'ombrello
			Tocca nel seguente ordine il leone il gelato la bambina la scimmia
			Tocca nel seguente ordine il leone il gelato la bambina il polipo
			Tocca nel seguente ordine il leone il gelato la bambina il pappagallo
			Tocca nel seguente ordine il leone il gelato la bambina la bicicletta
			Tocca nel seguente ordine il leone il gelato la bambina il monopattino
			Tocca nel seguente ordine il leone il gelato la bambina gli occhiali da sole
			Tocca nel seguente ordine il leone il gelato la bambina l'ombrello
			Tocca nel seguente ordine il leone il gelato l'ombrello la scimmia
			Tocca nel seguente ordine il leone il gelato l'ombrello il polipo
			Tocca nel seguente ordine il leone il gelato l'ombrello il pappagallo
			Tocca nel seguente ordine il leone il gelato l'ombrello la bicicletta
			Tocca nel seguente ordine il leone il gelato l'ombrello il monopattino
			Tocca nel seguente ordine il leone il gelato l'ombrello la bambina
			Tocca nel seguente ordine il leone il gelato l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il leone gli occhiali da sole la scimmia il gelato
			Tocca nel seguente ordine il leone gli occhiali da sole la scimmia il polipo
			Tocca nel seguente ordine il leone gli occhiali da sole la scimmia il pappagallo
			Tocca nel seguente ordine il leone gli occhiali da sole la scimmia la bicicletta
			Tocca nel seguente ordine il leone gli occhiali da sole la scimmia il monopattino
			Tocca nel seguente ordine il leone gli occhiali da sole la scimmia la bambina
			Tocca nel seguente ordine il leone gli occhiali da sole la scimmia l'ombrello
			Tocca nel seguente ordine il leone gli occhiali da sole il pappagallo la scimmia
			Tocca nel seguente ordine il leone gli occhiali da sole il pappagallo il polipo
			Tocca nel seguente ordine il leone gli occhiali da sole il pappagallo il gelato
			Tocca nel seguente ordine il leone gli occhiali da sole il pappagallo la bicicletta
			Tocca nel seguente ordine il leone gli occhiali da sole il pappagallo il monopattino
			Tocca nel seguente ordine il leone gli occhiali da sole il pappagallo la bambina
			Tocca nel seguente ordine il leone gli occhiali da sole il pappagallo l'ombrello
			Tocca nel seguente ordine il leone gli occhiali da sole il polipo la scimmia
			Tocca nel seguente ordine il leone gli occhiali da sole il polipo il gelato
			Tocca nel seguente ordine il leone gli occhiali da sole il polipo il pappagallo
			Tocca nel seguente ordine il leone gli occhiali da sole il polipo la bicicletta
			Tocca nel seguente ordine il leone gli occhiali da sole il polipo il monopattino
			Tocca nel seguente ordine il leone gli occhiali da sole il polipo la bambina
			Tocca nel seguente ordine il leone gli occhiali da sole il polipo l'ombrello
			Tocca nel seguente ordine il leone gli occhiali da sole il gelato la scimmia
			Tocca nel seguente ordine il leone gli occhiali da sole il gelato il polipo
			Tocca nel seguente ordine il leone gli occhiali da sole il gelato il pappagallo
			Tocca nel seguente ordine il leone gli occhiali da sole il gelato la bicicletta
			Tocca nel seguente ordine il leone gli occhiali da sole il gelato il monopattino
			Tocca nel seguente ordine il leone gli occhiali da sole il gelato la bambina
			Tocca nel seguente ordine il leone gli occhiali da sole il gelato l'ombrello
			Tocca nel seguente ordine il leone gli occhiali da sole la bicicletta la scimmia
			Tocca nel seguente ordine il leone gli occhiali da sole la bicicletta il polipo
			Tocca nel seguente ordine il leone gli occhiali da sole la bicicletta il pappagallo
			Tocca nel seguente ordine il leone gli occhiali da sole la bicicletta il gelato
			Tocca nel seguente ordine il leone gli occhiali da sole la bicicletta il monopattino
			Tocca nel seguente ordine il leone gli occhiali da sole la bicicletta la bambina
			Tocca nel seguente ordine il leone gli occhiali da sole la bicicletta l'ombrello
			Tocca nel seguente ordine il leone gli occhiali da sole il monopattino la scimmia
			Tocca nel seguente ordine il leone gli occhiali da sole il monopattino il polipo
			Tocca nel seguente ordine il leone gli occhiali da sole il monopattino il pappagallo
			Tocca nel seguente ordine il leone gli occhiali da sole il monopattino la bicicletta
			Tocca nel seguente ordine il leone gli occhiali da sole il monopattino il gelato
			Tocca nel seguente ordine il leone gli occhiali da sole il monopattino la bambina
			Tocca nel seguente ordine il leone gli occhiali da sole il monopattino l'ombrello
			Tocca nel seguente ordine il leone gli occhiali da sole la bambina la scimmia
			Tocca nel seguente ordine il leone gli occhiali da sole la bambina il polipo
			Tocca nel seguente ordine il leone gli occhiali da sole la bambina il pappagallo
			Tocca nel seguente ordine il leone gli occhiali da sole la bambina la bicicletta
			Tocca nel seguente ordine il leone gli occhiali da sole la bambina il monopattino
			Tocca nel seguente ordine il leone gli occhiali da sole la bambina il gelato
			Tocca nel seguente ordine il leone gli occhiali da sole la bambina l'ombrello
			Tocca nel seguente ordine il leone gli occhiali da sole l'ombrello la scimmia
			Tocca nel seguente ordine il leone gli occhiali da sole l'ombrello il polipo
			Tocca nel seguente ordine il leone gli occhiali da sole l'ombrello il pappagallo
			Tocca nel seguente ordine il leone gli occhiali da sole l'ombrello la bicicletta
			Tocca nel seguente ordine il leone gli occhiali da sole l'ombrello il monopattino
			Tocca nel seguente ordine il leone gli occhiali da sole l'ombrello la bambina
			Tocca nel seguente ordine il leone gli occhiali da sole l'ombrello il gelato
			Tocca nel seguente ordine il leone la bicicletta la scimmia gli occhiali da sole
			Tocca nel seguente ordine il leone la bicicletta la scimmia il polipo
			Tocca nel seguente ordine il leone la bicicletta la scimmia il pappagallo
			Tocca nel seguente ordine il leone la bicicletta la scimmia il gelato
			Tocca nel seguente ordine il leone la bicicletta la scimmia il monopattino
			Tocca nel seguente ordine il leone la bicicletta la scimmia la bambina
			Tocca nel seguente ordine il leone la bicicletta la scimmia l'ombrello
			Tocca nel seguente ordine il leone la bicicletta il pappagallo la scimmia
			Tocca nel seguente ordine il leone la bicicletta il pappagallo il polipo
			Tocca nel seguente ordine il leone la bicicletta il pappagallo il gelato
			Tocca nel seguente ordine il leone la bicicletta il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il leone la bicicletta il pappagallo il monopattino
			Tocca nel seguente ordine il leone la bicicletta il pappagallo la bambina
			Tocca nel seguente ordine il leone la bicicletta il pappagallo l'ombrello
			Tocca nel seguente ordine il leone la bicicletta il polipo la scimmia
			Tocca nel seguente ordine il leone la bicicletta il polipo il gelato
			Tocca nel seguente ordine il leone la bicicletta il polipo il pappagallo
			Tocca nel seguente ordine il leone la bicicletta il polipo glu occhiali da sole
			Tocca nel seguente ordine il leone la bicicletta il polipo il monopattino
			Tocca nel seguente ordine il leone la bicicletta il polipo la bambina
			Tocca nel seguente ordine il leone la bicicletta il polipo l'ombrello
			Tocca nel seguente ordine il leone la bicicletta il gelato la scimmia
			Tocca nel seguente ordine il leone la bicicletta il gelato il polipo
			Tocca nel seguente ordine il leone la bicicletta il gelato il pappagallo
			Tocca nel seguente ordine il leone la bicicletta il gelato gli occhiali da sole
			Tocca nel seguente ordine il leone la bicicletta il gelato il monopattino
			Tocca nel seguente ordine il leone la bicicletta il gelato la bambina
			Tocca nel seguente ordine il leone la bicicletta il gelato l'ombrello
			Tocca nel seguente ordine il leone la bicicletta gli occhiali da sole la scimmia
			Tocca nel seguente ordine il leone la bicicletta gli occhiali da sole il polipo
			Tocca nel seguente ordine il leone la bicicletta gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il leone la bicicletta gli occhiali da sole il gelato
			Tocca nel seguente ordine il leone la bicicletta gli occhiali da sole il monopattino
			Tocca nel seguente ordine il leone la bicicletta gli occhiali da sole la bambina
			Tocca nel seguente ordine il leone la bicicletta gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il leone la bicicletta il monopattino la scimmia
			Tocca nel seguente ordine il leone la bicicletta il monopattino il polipo
			Tocca nel seguente ordine il leone la bicicletta il monopattino il pappagallo
			Tocca nel seguente ordine il leone la bicicletta il monopattino gli occhiali da sole
			Tocca nel seguente ordine il leone la bicicletta il monopattino il gelato
			Tocca nel seguente ordine il leone la bicicletta il monopattino la bambina
			Tocca nel seguente ordine il leone la bicicletta il monopattino l'ombrello
			Tocca nel seguente ordine il leone la bicicletta la bambina la scimmia
			Tocca nel seguente ordine il leone la bicicletta la bambina il polipo
			Tocca nel seguente ordine il leone la bicicletta la bambina il pappagallo
			Tocca nel seguente ordine il leone la bicicletta la bambina gli occhiali da sole
			Tocca nel seguente ordine il leone la bicicletta la bambina il monopattino
			Tocca nel seguente ordine il leone la bicicletta la bambina il gelato
			Tocca nel seguente ordine il leone la bicicletta la bambina l'ombrello
			Tocca nel seguente ordine il leone la bicicletta l'ombrello la scimmia
			Tocca nel seguente ordine il leone la bicicletta l'ombrello il polipo
			Tocca nel seguente ordine il leone la bicicletta l'ombrello il pappagallo
			Tocca nel seguente ordine il leone la bicicletta l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il leone la bicicletta l'ombrello il monopattino
			Tocca nel seguente ordine il leone la bicicletta l'ombrello la bambina
			Tocca nel seguente ordine il leone la bicicletta l'ombrello il gelato
			Tocca nel seguente ordine il leone il monopattino la scimmia il gelato
			Tocca nel seguente ordine il leone il monopattino la scimmia il polipo
			Tocca nel seguente ordine il leone il monopattino la scimmia il pappagallo
			Tocca nel seguente ordine il leone il monopattino la scimmia la bicicletta
			Tocca nel seguente ordine il leone il monopattino la scimmia gli occhiali da sole
			Tocca nel seguente ordine il leone il monopattino la scimmia la bambina
			Tocca nel seguente ordine il leone il monopattino la scimmia l'ombrello
			Tocca nel seguente ordine il leone il monopattino il pappagallo la scimmia
			Tocca nel seguente ordine il leone il monopattino il pappagallo il polipo
			Tocca nel seguente ordine il leone il monopattino il pappagallo il gelato
			Tocca nel seguente ordine il leone il monopattino il pappagallo la bicicletta
			Tocca nel seguente ordine il leone il monopattino il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il leone il monopattino il pappagallo la bambina
			Tocca nel seguente ordine il leone il monopattino il pappagallo l'ombrello
			Tocca nel seguente ordine il leone il monopattino il polipo la scimmia
			Tocca nel seguente ordine il leone il monopattino il polipo il gelato
			Tocca nel seguente ordine il leone il monopattino il polipo il pappagallo
			Tocca nel seguente ordine il leone il monopattino il polipo la bicicletta
			Tocca nel seguente ordine il leone il monopattino il polipo il gelato
			Tocca nel seguente ordine il leone il monopattino il polipo la bambina
			Tocca nel seguente ordine il leone il monopattino il polipo l'ombrello
			Tocca nel seguente ordine il leone il monopattino il gelato la scimmia
			Tocca nel seguente ordine il leone il monopattino il gelato il polipo
			Tocca nel seguente ordine il leone il monopattino il gelato il pappagallo
			Tocca nel seguente ordine il leone il monopattino il gelato la bicicletta
			Tocca nel seguente ordine il leone il monopattino il gelato il gelato
			Tocca nel seguente ordine il leone il monopattino il gelato la bambina
			Tocca nel seguente ordine il leone il monopattino il gelato l'ombrello
			Tocca nel seguente ordine il leone il monopattino gli occhiali da sole la scimmia
			Tocca nel seguente ordine il leone il monopattino gli occhiali da sole il polipo
			Tocca nel seguente ordine il leone il monopattino gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il leone il monopattino gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il leone il monopattino gli occhiali da sole il gelato
			Tocca nel seguente ordine il leone il monopattino gli occhiali da sole la bambina
			Tocca nel seguente ordine il leone il monopattino gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il leone il monopattino la bicicletta la scimmia
			Tocca nel seguente ordine il leone il monopattino la bicicletta il polipo
			Tocca nel seguente ordine il leone il monopattino la bicicletta il pappagallo
			Tocca nel seguente ordine il leone il monopattino la bicicletta il gelato
			Tocca nel seguente ordine il leone il monopattino la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il leone il monopattino la bicicletta la bambina
			Tocca nel seguente ordine il leone il monopattino la bicicletta l'ombrello
			Tocca nel seguente ordine il leone il monopattino la bambina la scimmia
			Tocca nel seguente ordine il leone il monopattino la bambina il polipo
			Tocca nel seguente ordine il leone il monopattino la bambina il pappagallo
			Tocca nel seguente ordine il leone il monopattino la bambina la bicicletta
			Tocca nel seguente ordine il leone il monopattino la bambina il gelato
			Tocca nel seguente ordine il leone il monopattino la bambina gli occhiali da sole
			Tocca nel seguente ordine il leone il monopattino la bambina l'ombrello
			Tocca nel seguente ordine il leone il monopattino l'ombrello la scimmia
			Tocca nel seguente ordine il leone il monopattino l'ombrello il polipo
			Tocca nel seguente ordine il leone il monopattino l'ombrello il pappagallo
			Tocca nel seguente ordine il leone il monopattino l'ombrello la bicicletta
			Tocca nel seguente ordine il leone il monopattino l'ombrello il gelato
			Tocca nel seguente ordine il leone il monopattino l'ombrello la bambina
			Tocca nel seguente ordine il leone il monopattino l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il leone la bambina la scimmia il gelato
			Tocca nel seguente ordine il leone la bambina la scimmia il polipo
			Tocca nel seguente ordine il leone la bambina la scimmia il pappagallo
			Tocca nel seguente ordine il leone la bambina la scimmia la bicicletta
			Tocca nel seguente ordine il leone la bambina la scimmia il monopattino
			Tocca nel seguente ordine il leone la bambina la scimmia gli occhiali da sole
			Tocca nel seguente ordine il leone la bambina la scimmia l'ombrello
			Tocca nel seguente ordine il leone la bambina il pappagallo la scimmia
			Tocca nel seguente ordine il leone la bambina il pappagallo il polipo
			Tocca nel seguente ordine il leone la bambina il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il leone la bambina il pappagallo la bicicletta
			Tocca nel seguente ordine il leone la bambina il pappagallo il monopattino
			Tocca nel seguente ordine il leone la bambina il pappagallo il gelato
			Tocca nel seguente ordine il leone la bambina il pappagallo l'ombrello
			Tocca nel seguente ordine il leone la bambina il polipo la scimmia
			Tocca nel seguente ordine il leone la bambina il polipo gli occhiali da sole
			Tocca nel seguente ordine il leone la bambina il polipo il pappagallo
			Tocca nel seguente ordine il leone la bambina il polipo la bicicletta
			Tocca nel seguente ordine il leone la bambina il polipo il monopattino
			Tocca nel seguente ordine il leone la bambina il polipo il gelato
			Tocca nel seguente ordine il leone la bambina il polipo l'ombrello
			Tocca nel seguente ordine il leone la bambina il gelato la scimmia
			Tocca nel seguente ordine il leone la bambina il gelato il polipo
			Tocca nel seguente ordine il leone la bambina il gelato il pappagallo
			Tocca nel seguente ordine il leone la bambina il gelato la bicicletta
			Tocca nel seguente ordine il leone la bambina il gelato il monopattino
			Tocca nel seguente ordine il leone la bambina il gelato gli occhiali da sole
			Tocca nel seguente ordine il leone la bambina il gelato l'ombrello
			Tocca nel seguente ordine il leone la bambina gli occhiali da sole la scimmia
			Tocca nel seguente ordine il leone la bambina gli occhiali da sole il polipo
			Tocca nel seguente ordine il leone la bambina gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il leone la bambina gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il leone la bambina gli occhiali da sole il monopattino
			Tocca nel seguente ordine il leone la bambina gli occhiali da sole il gelato
			Tocca nel seguente ordine il leone la bambina gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il leone la bambina la bicicletta la scimmia
			Tocca nel seguente ordine il leone la bambina la bicicletta il polipo
			Tocca nel seguente ordine il leone la bambina la bicicletta il pappagallo
			Tocca nel seguente ordine il leone la bambina la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il leone la bambina la bicicletta il monopattino
			Tocca nel seguente ordine il leone la bambina la bicicletta il gelato
			Tocca nel seguente ordine il leone la bambina la bicicletta l'ombrello
			Tocca nel seguente ordine il leone la bambina il monopattino la scimmia
			Tocca nel seguente ordine il leone la bambina il monopattino il polipo
			Tocca nel seguente ordine il leone la bambina il monopattino il pappagallo
			Tocca nel seguente ordine il leone la bambina il monopattino la bicicletta
			Tocca nel seguente ordine il leone la bambina il monopattino gli occhiali da sole
			Tocca nel seguente ordine il leone la bambina il monopattino il gelato
			Tocca nel seguente ordine il leone la bambina il monopattino l'ombrello
			Tocca nel seguente ordine il leone la bambina l'ombrello la scimmia
			Tocca nel seguente ordine il leone la bambina l'ombrello il polipo
			Tocca nel seguente ordine il leone la bambina l'ombrello il pappagallo
			Tocca nel seguente ordine il leone la bambina l'ombrello la bicicletta
			Tocca nel seguente ordine il leone la bambina l'ombrello il monopattino
			Tocca nel seguente ordine il leone la bambina l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il leone la bambina l'ombrello il gelato
			Tocca nel seguente ordine il leone l'ombrello la scimmia il gelato
			Tocca nel seguente ordine il leone l'ombrello la scimmia il polipo
			Tocca nel seguente ordine il leone l'ombrello la scimmia il pappagallo
			Tocca nel seguente ordine il leone l'ombrello la scimmia la bicicletta
			Tocca nel seguente ordine il leone l'ombrello la scimmia il monopattino
			Tocca nel seguente ordine il leone l'ombrello la scimmia la bambina
			Tocca nel seguente ordine il leone l'ombrello la scimmia gli occhiali da sole
			Tocca nel seguente ordine il leone l'ombrello il pappagallo la scimmia
			Tocca nel seguente ordine il leone l'ombrello il pappagallo il polipo
			Tocca nel seguente ordine il leone l'ombrello il pappagallo il gelato
			Tocca nel seguente ordine il leone l'ombrello il pappagallo la bicicletta
			Tocca nel seguente ordine il leone l'ombrello il pappagallo il monopattino
			Tocca nel seguente ordine il leone l'ombrello il pappagallo la bambina
			Tocca nel seguente ordine il leone l'ombrello il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il leone l'ombrello il polipo la scimmia
			Tocca nel seguente ordine il leone l'ombrello il polipo il gelato
			Tocca nel seguente ordine il leone l'ombrello il polipo il pappagallo
			Tocca nel seguente ordine il leone l'ombrello il polipo la bicicletta
			Tocca nel seguente ordine il leone l'ombrello il polipo il monopattino
			Tocca nel seguente ordine il leone l'ombrello il polipo la bambina
			Tocca nel seguente ordine il leone l'ombrello il polipo gli occhiali da sole
			Tocca nel seguente ordine il leone l'ombrello il gelato la scimmia
			Tocca nel seguente ordine il leone l'ombrello il gelato il polipo
			Tocca nel seguente ordine il leone l'ombrello il gelato il pappagallo
			Tocca nel seguente ordine il leone l'ombrello il gelato la bicicletta
			Tocca nel seguente ordine il leone l'ombrello il gelato il monopattino
			Tocca nel seguente ordine il leone l'ombrello il gelato la bambina
			Tocca nel seguente ordine il leone l'ombrello il gelato gli occhiali da sole
			Tocca nel seguente ordine il leone l'ombrello gli occhiali da sole la scimmia
			Tocca nel seguente ordine il leone l'ombrello gli occhiali da sole il polipo
			Tocca nel seguente ordine il leone l'ombrello gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il leone l'ombrello gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il leone l'ombrello gli occhiali da sole il monopattino
			Tocca nel seguente ordine il leone l'ombrello gli occhiali da sole la bambina
			Tocca nel seguente ordine il leone l'ombrello gli occhiali da sole il gelato
			Tocca nel seguente ordine il leone l'ombrello la bicicletta la scimmia
			Tocca nel seguente ordine il leone l'ombrello la bicicletta il polipo
			Tocca nel seguente ordine il leone l'ombrello la bicicletta il pappagallo
			Tocca nel seguente ordine il leone l'ombrello la bicicletta il gelato
			Tocca nel seguente ordine il leone l'ombrello la bicicletta il monopattino
			Tocca nel seguente ordine il leone l'ombrello la bicicletta la bambina
			Tocca nel seguente ordine il leone l'ombrello la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il leone l'ombrello il monopattino la scimmia
			Tocca nel seguente ordine il leone l'ombrello il monopattino il polipo
			Tocca nel seguente ordine il leone l'ombrello il monopattino il pappagallo
			Tocca nel seguente ordine il leone l'ombrello il monopattino la bicicletta
			Tocca nel seguente ordine il leone l'ombrello il monopattino il gelato
			Tocca nel seguente ordine il leone l'ombrello il monopattino la bambina
			Tocca nel seguente ordine il leone l'ombrello il monopattino gli occhiali da sole
			Tocca nel seguente ordine il leone l'ombrello la bambina la scimmia
			Tocca nel seguente ordine il leone l'ombrello la bambina il polipo
			Tocca nel seguente ordine il leone l'ombrello la bambina il pappagallo
			Tocca nel seguente ordine il leone l'ombrello la bambina la bicicletta
			Tocca nel seguente ordine il leone l'ombrello la bambina il monopattino
			Tocca nel seguente ordine il leone l'ombrello la bambina il gelato
			Tocca nel seguente ordine il leone l'ombrello la bambina gli occhiali da sole
			Tocca nel seguente ordine la scimmia il leone il pappagallo il polipo
			Tocca nel seguente ordine la scimmia il leone il pappagallo il gelato
			Tocca nel seguente ordine la scimmia il leone il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la scimmia il leone il pappagallo la bicicletta
			Tocca nel seguente ordine la scimmia il leone il pappagallo il monopattino
			Tocca nel seguente ordine la scimmia il leone il pappagallo la bambina
			Tocca nel seguente ordine la scimmia il leone il pappagallo l'ombrello
			Tocca nel seguente ordine la scimmia il leone il polipo il pappagallo
			Tocca nel seguente ordine la scimmia il leone il polipo il gelato
			Tocca nel seguente ordine la scimmia il leone il polipo gli occhiali da sole
			Tocca nel seguente ordine la scimmia il leone il polipo la bicicletta
			Tocca nel seguente ordine la scimmia il leone il polipo il monopattino
			Tocca nel seguente ordine la scimmia il leone il polipo la bambina
			Tocca nel seguente ordine la scimmia il leone il polipo l'ombrello
			Tocca nel seguente ordine la scimmia il leone il gelato il polipo
			Tocca nel seguente ordine la scimmia il leone il gelato il pappagallo
			Tocca nel seguente ordine la scimmia il leone il gelato gli occhiali da sole
			Tocca nel seguente ordine la scimmia il leone il gelato la bicicletta
			Tocca nel seguente ordine la scimmia il leone il gelato il monopattino
			Tocca nel seguente ordine la scimmia il leone il gelato la bambina
			Tocca nel seguente ordine la scimmia il leone il gelato l'ombrello
			Tocca nel seguente ordine la scimmia il leone gli occhiali da sole il polipo
			Tocca nel seguente ordine la scimmia il leone gli occhiali da sole il gelato
			Tocca nel seguente ordine la scimmia il leone gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la scimmia il leone gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la scimmia il leone gli occhiali da sole il monopattino
			Tocca nel seguente ordine la scimmia il leone gli occhiali da sole la bambina
			Tocca nel seguente ordine la scimmia il leone gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la scimmia il leone la bicicletta il polipo
			Tocca nel seguente ordine la scimmia il leone la bicicletta il gelato
			Tocca nel seguente ordine la scimmia il leone la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la scimmia il leone la bicicletta il pappagallo
			Tocca nel seguente ordine la scimmia il leone la bicicletta il monopattino
			Tocca nel seguente ordine la scimmia il leone la bicicletta la bambina
			Tocca nel seguente ordine la scimmia il leone la bicicletta l'ombrello
			Tocca nel seguente ordine la scimmia il leone il monopattino il polipo
			Tocca nel seguente ordine la scimmia il leone il monopattino il gelato
			Tocca nel seguente ordine la scimmia il leone il monopattino gli occhiali da sole
			Tocca nel seguente ordine la scimmia il leone il monopattino la bicicletta
			Tocca nel seguente ordine la scimmia il leone il monopattino il pappagallo
			Tocca nel seguente ordine la scimmia il leone il monopattino la bambina
			Tocca nel seguente ordine la scimmia il leone il monopattino l'ombrello
			Tocca nel seguente ordine la scimmia il leone la bambina il polipo
			Tocca nel seguente ordine la scimmia il leone la bambina il gelato
			Tocca nel seguente ordine la scimmia il leone la bambina gli occhiali da sole
			Tocca nel seguente ordine la scimmia il leone la bambina la bicicletta
			Tocca nel seguente ordine la scimmia il leone la bambina il monopattino
			Tocca nel seguente ordine la scimmia il leone la bambina il pappagallo
			Tocca nel seguente ordine la scimmia il leone la bambina l'ombrello
			Tocca nel seguente ordine la scimmia il leone l'ombrello il polipo
			Tocca nel seguente ordine la scimmia il leone l'ombrello il gelato
			Tocca nel seguente ordine la scimmia il leone l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la scimmia il leone l'ombrello la bicicletta
			Tocca nel seguente ordine la scimmia il leone l'ombrello il monopattino
			Tocca nel seguente ordine la scimmia il leone l'ombrello la bambina
			Tocca nel seguente ordine la scimmia il leone l'ombrello il pappagallo
			Tocca nel seguente ordine la scimmia il pappagallo il leone il polipo
			Tocca nel seguente ordine la scimmia il pappagallo il leone il gelato
			Tocca nel seguente ordine la scimmia il pappagallo il leone gli occhiali da sole
			Tocca nel seguente ordine la scimmia il pappagallo il leone la bicicletta
			Tocca nel seguente ordine la scimmia il pappagallo il leone il monopattino
			Tocca nel seguente ordine la scimmia il pappagallo il leone la bambina
			Tocca nel seguente ordine la scimmia il pappagallo il leone l'ombrello
			Tocca nel seguente ordine la scimmia il pappagallo il polipo il leone
			Tocca nel seguente ordine la scimmia il pappagallo il polipo il gelato
			Tocca nel seguente ordine la scimmia il pappagallo il polipo gli occhiali da sole
			Tocca nel seguente ordine la scimmia il pappagallo il polipo la bicicletta
			Tocca nel seguente ordine la scimmia il pappagallo il polipo il monopattino
			Tocca nel seguente ordine la scimmia il pappagallo il polipo la bambina
			Tocca nel seguente ordine la scimmia il pappagallo il polipo l'ombrello
			Tocca nel seguente ordine la scimmia il pappagallo il gelato il polipo
			Tocca nel seguente ordine la scimmia il pappagallo il gelato il leone
			Tocca nel seguente ordine la scimmia il pappagallo il gelato gli occhiali da sole
			Tocca nel seguente ordine la scimmia il pappagallo il gelato la bicicletta
			Tocca nel seguente ordine la scimmia il pappagallo il gelato il monopattino
			Tocca nel seguente ordine la scimmia il pappagallo il gelato la bambina
			Tocca nel seguente ordine la scimmia il pappagallo il gelato l'ombrello
			Tocca nel seguente ordine la scimmia il pappagallo gli occhiali da sole il polipo
			Tocca nel seguente ordine la scimmia il pappagallo gli occhiali da sole il gelato
			Tocca nel seguente ordine la scimmia il pappagallo gli occhiali da sole il leone
			Tocca nel seguente ordine la scimmia il pappagallo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la scimmia il pappagallo gli occhiali da sole il monopattino
			Tocca nel seguente ordine la scimmia il pappagallo gli occhiali da sole la bambina
			Tocca nel seguente ordine la scimmia il pappagallo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la scimmia il pappagallo la bicicletta il polipo
			Tocca nel seguente ordine la scimmia il pappagallo la bicicletta il gelato
			Tocca nel seguente ordine la scimmia il pappagallo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la scimmia il pappagallo la bicicletta il leone
			Tocca nel seguente ordine la scimmia il pappagallo la bicicletta il monopattino
			Tocca nel seguente ordine la scimmia il pappagallo la bicicletta la bambina
			Tocca nel seguente ordine la scimmia il pappagallo la bicicletta l'ombrello
			Tocca nel seguente ordine la scimmia il pappagallo il monopattino il polipo
			Tocca nel seguente ordine la scimmia il pappagallo il monopattino il gelato
			Tocca nel seguente ordine la scimmia il pappagallo il monopattino gli occhiali da sole
			Tocca nel seguente ordine la scimmia il pappagallo il monopattino la bicicletta
			Tocca nel seguente ordine la scimmia il pappagallo il monopattino il leone
			Tocca nel seguente ordine la scimmia il pappagallo il monopattino la bambina
			Tocca nel seguente ordine la scimmia il pappagallo il monopattino l'ombrello
			Tocca nel seguente ordine la scimmia il pappagallo la bambina il polipo
			Tocca nel seguente ordine la scimmia il pappagallo la bambina il gelato
			Tocca nel seguente ordine la scimmia il pappagallo la bambina gli occhiali da sole
			Tocca nel seguente ordine la scimmia il pappagallo la bambina la bicicletta
			Tocca nel seguente ordine la scimmia il pappagallo la bambina il monopattino
			Tocca nel seguente ordine la scimmia il pappagallo la bambina il leone
			Tocca nel seguente ordine la scimmia il pappagallo la bambina l'ombrello
			Tocca nel seguente ordine la scimmia il pappagallo l'ombrello il polipo
			Tocca nel seguente ordine la scimmia il pappagallo l'ombrello il gelato
			Tocca nel seguente ordine la scimmia il pappagallo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la scimmia il pappagallo l'ombrello la bicicletta
			Tocca nel seguente ordine la scimmia il pappagallo l'ombrello il monopattino
			Tocca nel seguente ordine la scimmia il pappagallo l'ombrello la bambina
			Tocca nel seguente ordine la scimmia il pappagallo l'ombrello il leone
			Tocca nel seguente ordine la scimmia il polipo il leone il pappagallo
			Tocca nel seguente ordine la scimmia il polipo il leone il gelato
			Tocca nel seguente ordine la scimmia il polipo il leone gli occhiali da sole
			Tocca nel seguente ordine la scimmia il polipo il leone la bicicletta
			Tocca nel seguente ordine la scimmia il polipo il leone il monopattino
			Tocca nel seguente ordine la scimmia il polipo il leone la bambina
			Tocca nel seguente ordine la scimmia il polipo il leone l'ombrello
			Tocca nel seguente ordine la scimmia il polipo il pappagallo il leone
			Tocca nel seguente ordine la scimmia il polipo il pappagallo il gelato
			Tocca nel seguente ordine la scimmia il polipo il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la scimmia il polipo il pappagallo la bicicletta
			Tocca nel seguente ordine la scimmia il polipo il pappagallo il monopattino
			Tocca nel seguente ordine la scimmia il polipo il pappagallo la bambina
			Tocca nel seguente ordine la scimmia il polipo il pappagallo l'ombrello
			Tocca nel seguente ordine la scimmia il polipo il gelato il leone
			Tocca nel seguente ordine la scimmia il polipo il gelato il pappagallo
			Tocca nel seguente ordine la scimmia il polipo il gelato gli occhiali da sole
			Tocca nel seguente ordine la scimmia il polipo il gelato la bicicletta
			Tocca nel seguente ordine la scimmia il polipo il gelato il monopattino
			Tocca nel seguente ordine la scimmia il polipo il gelato la bambina
			Tocca nel seguente ordine la scimmia il polipo il gelato l'ombrello
			Tocca nel seguente ordine la scimmia il polipo gli occhiali da sole il leone
			Tocca nel seguente ordine la scimmia il polipo gli occhiali da sole il gelato
			Tocca nel seguente ordine la scimmia il polipo gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la scimmia il polipo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la scimmia il polipo gli occhiali da sole il monopattino
			Tocca nel seguente ordine la scimmia il polipo gli occhiali da sole la bambina
			Tocca nel seguente ordine la scimmia il polipo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la scimmia il polipo la bicicletta il leone
			Tocca nel seguente ordine la scimmia il polipo la bicicletta il gelato
			Tocca nel seguente ordine la scimmia il polipo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la scimmia il polipo la bicicletta il pappagallo
			Tocca nel seguente ordine la scimmia il polipo la bicicletta il monopattino
			Tocca nel seguente ordine la scimmia il polipo la bicicletta la bambina
			Tocca nel seguente ordine la scimmia il polipo la bicicletta l'ombrello
			Tocca nel seguente ordine la scimmia il polipo il monopattino il leone
			Tocca nel seguente ordine la scimmia il polipo il monopattino il gelato
			Tocca nel seguente ordine la scimmia il polipo il monopattino gli occhiali da sole
			Tocca nel seguente ordine la scimmia il polipo il monopattino la bicicletta
			Tocca nel seguente ordine la scimmia il polipo il monopattino il pappagallo
			Tocca nel seguente ordine la scimmia il polipo il monopattino la bambina
			Tocca nel seguente ordine la scimmia il polipo il monopattino l'ombrello
			Tocca nel seguente ordine la scimmia il polipo la bambina il leone
			Tocca nel seguente ordine la scimmia il polipo la bambina il gelato
			Tocca nel seguente ordine la scimmia il polipo la bambina gli occhiali da sole
			Tocca nel seguente ordine la scimmia il polipo la bambina la bicicletta
			Tocca nel seguente ordine la scimmia il polipo la bambina il monopattino
			Tocca nel seguente ordine la scimmia il polipo la bambina il pappagallo
			Tocca nel seguente ordine la scimmia il polipo la bambina l'ombrello
			Tocca nel seguente ordine la scimmia il polipo l'ombrello il leone
			Tocca nel seguente ordine la scimmia il polipo l'ombrello il gelato
			Tocca nel seguente ordine la scimmia il polipo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la scimmia il polipo l'ombrello la bicicletta
			Tocca nel seguente ordine la scimmia il polipo l'ombrello il monopattino
			Tocca nel seguente ordine la scimmia il polipo l'ombrello la bambina
			Tocca nel seguente ordine la scimmia il polipo l'ombrello il pappagallo
			Tocca nel seguente ordine la scimmia il gelato il leone il polipo
			Tocca nel seguente ordine la scimmia il gelato il leone il pappagallo
			Tocca nel seguente ordine la scimmia il gelato il leone gli occhiali da sole
			Tocca nel seguente ordine la scimmia il gelato il leone la bicicletta
			Tocca nel seguente ordine la scimmia il gelato il leone il monopattino
			Tocca nel seguente ordine la scimmia il gelato il leone la bambina
			Tocca nel seguente ordine la scimmia il gelato il leone l'ombrello
			Tocca nel seguente ordine la scimmia il gelato il pappagallo il polipo
			Tocca nel seguente ordine la scimmia il gelato il pappagallo il leone
			Tocca nel seguente ordine la scimmia il gelato il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la scimmia il gelato il pappagallo la bicicletta
			Tocca nel seguente ordine la scimmia il gelato il pappagallo il monopattino
			Tocca nel seguente ordine la scimmia il gelato il pappagallo la bambina
			Tocca nel seguente ordine la scimmia il gelato il pappagallo l'ombrello
			Tocca nel seguente ordine la scimmia il gelato il polipo il pappagallo
			Tocca nel seguente ordine la scimmia il gelato il polipo il leone
			Tocca nel seguente ordine la scimmia il gelato il polipo gli occhiali da sole
			Tocca nel seguente ordine la scimmia il gelato il polipo la bicicletta
			Tocca nel seguente ordine la scimmia il gelato il polipo il monopattino
			Tocca nel seguente ordine la scimmia il gelato il polipo la bambina
			Tocca nel seguente ordine la scimmia il gelato il polipo l'ombrello
			Tocca nel seguente ordine la scimmia il gelato gli occhiali da sole il polipo
			Tocca nel seguente ordine la scimmia il gelato gli occhiali da sole il leone
			Tocca nel seguente ordine la scimmia il gelato gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la scimmia il gelato gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la scimmia il gelato gli occhiali da sole il monopattino
			Tocca nel seguente ordine la scimmia il gelato gli occhiali da sole la bambina
			Tocca nel seguente ordine la scimmia il gelato gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la scimmia il gelato la bicicletta il polipo
			Tocca nel seguente ordine la scimmia il gelato la bicicletta il leone
			Tocca nel seguente ordine la scimmia il gelato la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la scimmia il gelato la bicicletta il pappagallo
			Tocca nel seguente ordine la scimmia il gelato la bicicletta il monopattino
			Tocca nel seguente ordine la scimmia il gelato la bicicletta la bambina
			Tocca nel seguente ordine la scimmia il gelato la bicicletta l'ombrello
			Tocca nel seguente ordine la scimmia il gelato il monopattino il polipo
			Tocca nel seguente ordine la scimmia il gelato il monopattino il leone
			Tocca nel seguente ordine la scimmia il gelato il monopattino gli occhiali da sole
			Tocca nel seguente ordine la scimmia il gelato il monopattino la bicicletta
			Tocca nel seguente ordine la scimmia il gelato il monopattino il pappagallo
			Tocca nel seguente ordine la scimmia il gelato il monopattino la bambina
			Tocca nel seguente ordine la scimmia il gelato il monopattino l'ombrello
			Tocca nel seguente ordine la scimmia il gelato la bambina il polipo
			Tocca nel seguente ordine la scimmia il gelato la bambina il leone
			Tocca nel seguente ordine la scimmia il gelato la bambina gli occhiali da sole
			Tocca nel seguente ordine la scimmia il gelato la bambina la bicicletta
			Tocca nel seguente ordine la scimmia il gelato la bambina il monopattino
			Tocca nel seguente ordine la scimmia il gelato la bambina il pappagallo
			Tocca nel seguente ordine la scimmia il gelato la bambina l'ombrello
			Tocca nel seguente ordine la scimmia il gelato l'ombrello il polipo
			Tocca nel seguente ordine la scimmia il gelato l'ombrello il leone
			Tocca nel seguente ordine la scimmia il gelato l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la scimmia il gelato l'ombrello la bicicletta
			Tocca nel seguente ordine la scimmia il gelato l'ombrello il monopattino
			Tocca nel seguente ordine la scimmia il gelato l'ombrello la bambina
			Tocca nel seguente ordine la scimmia il gelato l'ombrello il pappagallo
			Tocca nel seguente ordine la scimmia gli occhiali da sole il leone il polipo
			Tocca nel seguente ordine la scimmia gli occhiali da sole il leone il gelato
			Tocca nel seguente ordine la scimmia gli occhiali da sole il leone il pappagallo
			Tocca nel seguente ordine la scimmia gli occhiali da sole il leone la bicicletta
			Tocca nel seguente ordine la scimmia gli occhiali da sole il leone il monopattino
			Tocca nel seguente ordine la scimmia gli occhiali da sole il leone la bambina
			Tocca nel seguente ordine la scimmia gli occhiali da sole il leone l'ombrello
			Tocca nel seguente ordine la scimmia gli occhiali da sole il pappagallo il polipo
			Tocca nel seguente ordine la scimmia gli occhiali da sole il pappagallo il gelato
			Tocca nel seguente ordine la scimmia gli occhiali da sole il pappagallo il leone
			Tocca nel seguente ordine la scimmia gli occhiali da sole il pappagallo la bicicletta
			Tocca nel seguente ordine la scimmia gli occhiali da sole il pappagallo il monopattino
			Tocca nel seguente ordine la scimmia gli occhiali da sole il pappagallo la bambina
			Tocca nel seguente ordine la scimmia gli occhiali da sole il pappagallo l'ombrello
			Tocca nel seguente ordine la scimmia gli occhiali da sole il polipo il leone
			Tocca nel seguente ordine la scimmia gli occhiali da sole il polipo il gelato
			Tocca nel seguente ordine la scimmia gli occhiali da sole il polipo il pappagallo
			Tocca nel seguente ordine la scimmia gli occhiali da sole il polipo la bicicletta
			Tocca nel seguente ordine la scimmia gli occhiali da sole il polipo il monopattino
			Tocca nel seguente ordine la scimmia gli occhiali da sole il polipo la bambina
			Tocca nel seguente ordine la scimmia gli occhiali da sole il polipo l'ombrello
			Tocca nel seguente ordine la scimmia gli occhiali da sole il gelato il polipo
			Tocca nel seguente ordine la scimmia gli occhiali da sole il gelato il leone
			Tocca nel seguente ordine la scimmia gli occhiali da sole il gelato il pappagallo
			Tocca nel seguente ordine la scimmia gli occhiali da sole il gelato la bicicletta
			Tocca nel seguente ordine la scimmia gli occhiali da sole il gelato il monopattino
			Tocca nel seguente ordine la scimmia gli occhiali da sole il gelato la bambina
			Tocca nel seguente ordine la scimmia gli occhiali da sole il gelato l'ombrello
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bicicletta il polipo
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bicicletta il gelato
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bicicletta il pappagallo
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bicicletta il leone
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bicicletta il monopattino
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bicicletta la bambina
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bicicletta l'ombrello
			Tocca nel seguente ordine la scimmia gli occhiali da sole il monopattino il polipo
			Tocca nel seguente ordine la scimmia gli occhiali da sole il monopattino il gelato
			Tocca nel seguente ordine la scimmia gli occhiali da sole il monopattino il pappagallo
			Tocca nel seguente ordine la scimmia gli occhiali da sole il monopattino la bicicletta
			Tocca nel seguente ordine la scimmia gli occhiali da sole il monopattino il leone
			Tocca nel seguente ordine la scimmia gli occhiali da sole il monopattino la bambina
			Tocca nel seguente ordine la scimmia gli occhiali da sole il monopattino l'ombrello
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bambina il polipo
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bambina il gelato
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bambina il pappagallo
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bambina la bicicletta
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bambina il monopattino
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bambina il leone
			Tocca nel seguente ordine la scimmia gli occhiali da sole la bambina l'ombrello
			Tocca nel seguente ordine la scimmia gli occhiali da sole l'ombrello il polipo
			Tocca nel seguente ordine la scimmia gli occhiali da sole l'ombrello il gelato
			Tocca nel seguente ordine la scimmia gli occhiali da sole l'ombrello il pappagallo
			Tocca nel seguente ordine la scimmia gli occhiali da sole l'ombrello la bicicletta
			Tocca nel seguente ordine la scimmia gli occhiali da sole l'ombrello il monopattino
			Tocca nel seguente ordine la scimmia gli occhiali da sole l'ombrello la bambina
			Tocca nel seguente ordine la scimmia gli occhiali da sole l'ombrello il leone
			Tocca nel seguente ordine la scimmia la bicicletta il leone il polipo
			Tocca nel seguente ordine la scimmia la bicicletta il leone il gelato
			Tocca nel seguente ordine la scimmia la bicicletta il leone gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bicicletta il leone il pappagallo
			Tocca nel seguente ordine la scimmia la bicicletta il leone il monopattino
			Tocca nel seguente ordine la scimmia la bicicletta il leone la bambina
			Tocca nel seguente ordine la scimmia la bicicletta il leone l'ombrello
			Tocca nel seguente ordine la scimmia la bicicletta il pappagallo il polipo
			Tocca nel seguente ordine la scimmia la bicicletta il pappagallo il gelato
			Tocca nel seguente ordine la scimmia la bicicletta il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bicicletta il pappagallo il leone
			Tocca nel seguente ordine la scimmia la bicicletta il pappagallo il monopattino
			Tocca nel seguente ordine la scimmia la bicicletta il pappagallo la bambina
			Tocca nel seguente ordine la scimmia la bicicletta il pappagallo l'ombrello
			Tocca nel seguente ordine la scimmia la bicicletta il polipo il leone
			Tocca nel seguente ordine la scimmia la bicicletta il polipo il gelato
			Tocca nel seguente ordine la scimmia la bicicletta il polipo gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bicicletta il polipo il pappagallo
			Tocca nel seguente ordine la scimmia la bicicletta il polipo il monopattino
			Tocca nel seguente ordine la scimmia la bicicletta il polipo la bambina
			Tocca nel seguente ordine la scimmia la bicicletta il polipo l'ombrello
			Tocca nel seguente ordine la scimmia la bicicletta il gelato il polipo
			Tocca nel seguente ordine la scimmia la bicicletta il gelato il pappagallo
			Tocca nel seguente ordine la scimmia la bicicletta il gelato gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bicicletta il gelato il leone
			Tocca nel seguente ordine la scimmia la bicicletta il gelato il monopattino
			Tocca nel seguente ordine la scimmia la bicicletta il gelato la bambina
			Tocca nel seguente ordine la scimmia la bicicletta il gelato l'ombrello
			Tocca nel seguente ordine la scimmia la bicicletta gli occhiali da sole il polipo
			Tocca nel seguente ordine la scimmia la bicicletta gli occhiali da sole il gelato
			Tocca nel seguente ordine la scimmia la bicicletta gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la scimmia la bicicletta gli occhiali da sole il leone
			Tocca nel seguente ordine la scimmia la bicicletta gli occhiali da sole il monopattino
			Tocca nel seguente ordine la scimmia la bicicletta gli occhiali da sole la bambina
			Tocca nel seguente ordine la scimmia la bicicletta gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la scimmia la bicicletta il monopattino il polipo
			Tocca nel seguente ordine la scimmia la bicicletta il monopattino il gelato
			Tocca nel seguente ordine la scimmia la bicicletta il monopattino gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bicicletta il monopattino il leone
			Tocca nel seguente ordine la scimmia la bicicletta il monopattino il pappagallo
			Tocca nel seguente ordine la scimmia la bicicletta il monopattino la bambina
			Tocca nel seguente ordine la scimmia la bicicletta il monopattino l'ombrello
			Tocca nel seguente ordine la scimmia la bicicletta la bambina il polipo
			Tocca nel seguente ordine la scimmia la bicicletta la bambina il gelato
			Tocca nel seguente ordine la scimmia la bicicletta la bambina gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bicicletta la bambina il leone
			Tocca nel seguente ordine la scimmia la bicicletta la bambina il monopattino
			Tocca nel seguente ordine la scimmia la bicicletta la bambina il pappagallo
			Tocca nel seguente ordine la scimmia la bicicletta la bambina l'ombrello
			Tocca nel seguente ordine la scimmia la bicicletta l'ombrello il polipo
			Tocca nel seguente ordine la scimmia la bicicletta l'ombrello il gelato
			Tocca nel seguente ordine la scimmia la bicicletta l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bicicletta l'ombrello il pappagallo
			Tocca nel seguente ordine la scimmia la bicicletta l'ombrello il monopattino
			Tocca nel seguente ordine la scimmia la bicicletta l'ombrello la bambina
			Tocca nel seguente ordine la scimmia la bicicletta l'ombrello il leone
			Tocca nel seguente ordine la scimmia il monopattino il leone il polipo
			Tocca nel seguente ordine la scimmia il monopattino il leone il gelato
			Tocca nel seguente ordine la scimmia il monopattino il leone gli occhiali da sole
			Tocca nel seguente ordine la scimmia il monopattino il leone la bicicletta
			Tocca nel seguente ordine la scimmia il monopattino il leone il pappagallo
			Tocca nel seguente ordine la scimmia il monopattino il leone la bambina
			Tocca nel seguente ordine la scimmia il monopattino il leone l'ombrello
			Tocca nel seguente ordine la scimmia il monopattino il pappagallo il polipo
			Tocca nel seguente ordine la scimmia il monopattino il pappagallo il gelato
			Tocca nel seguente ordine la scimmia il monopattino il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la scimmia il monopattino il pappagallo la bicicletta
			Tocca nel seguente ordine la scimmia il monopattino il pappagallo il leone
			Tocca nel seguente ordine la scimmia il monopattino il pappagallo la bambina
			Tocca nel seguente ordine la scimmia il monopattino il pappagallo l'ombrello
			Tocca nel seguente ordine la scimmia il monopattino il polipo il pappagallo
			Tocca nel seguente ordine la scimmia il monopattino il polipo il gelato
			Tocca nel seguente ordine la scimmia il monopattino il polipo gli occhiali da sole
			Tocca nel seguente ordine la scimmia il monopattino il polipo la bicicletta
			Tocca nel seguente ordine la scimmia il monopattino il polipo il leone
			Tocca nel seguente ordine la scimmia il monopattino il polipo la bambina
			Tocca nel seguente ordine la scimmia il monopattino il polipo l'ombrello
			Tocca nel seguente ordine la scimmia il monopattino il gelato il polipo
			Tocca nel seguente ordine la scimmia il monopattino il gelato il pappagallo
			Tocca nel seguente ordine la scimmia il monopattino il gelato gli occhiali da sole
			Tocca nel seguente ordine la scimmia il monopattino il gelato la bicicletta
			Tocca nel seguente ordine la scimmia il monopattino il gelato il leone
			Tocca nel seguente ordine la scimmia il monopattino il gelato la bambina
			Tocca nel seguente ordine la scimmia il monopattino il gelato l'ombrello
			Tocca nel seguente ordine la scimmia il monopattino gli occhiali da sole il polipo
			Tocca nel seguente ordine la scimmia il monopattino gli occhiali da sole il gelato
			Tocca nel seguente ordine la scimmia il monopattino gli occhiali da sole il leone
			Tocca nel seguente ordine la scimmia il monopattino gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la scimmia il monopattino gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la scimmia il monopattino gli occhiali da sole la bambina
			Tocca nel seguente ordine la scimmia il monopattino gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la scimmia il monopattino la bicicletta il polipo
			Tocca nel seguente ordine la scimmia il monopattino la bicicletta il gelato
			Tocca nel seguente ordine la scimmia il monopattino la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la scimmia il monopattino la bicicletta il leone
			Tocca nel seguente ordine la scimmia il monopattino la bicicletta il pappagallo
			Tocca nel seguente ordine la scimmia il monopattino la bicicletta la bambina
			Tocca nel seguente ordine la scimmia il monopattino la bicicletta l'ombrello
			Tocca nel seguente ordine la scimmia il monopattino la bambina il polipo
			Tocca nel seguente ordine la scimmia il monopattino la bambina il gelato
			Tocca nel seguente ordine la scimmia il monopattino la bambina gli occhiali da sole
			Tocca nel seguente ordine la scimmia il monopattino la bambina il leone
			Tocca nel seguente ordine la scimmia il monopattino la bambina il monopattino
			Tocca nel seguente ordine la scimmia il monopattino la bambina il pappagallo
			Tocca nel seguente ordine la scimmia il monopattino la bambina l'ombrello
			Tocca nel seguente ordine la scimmia il monopattino l'ombrello il polipo
			Tocca nel seguente ordine la scimmia il monopattino l'ombrello il gelato
			Tocca nel seguente ordine la scimmia il monopattino l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la scimmia il monopattino l'ombrello la bicicletta
			Tocca nel seguente ordine la scimmia il monopattino l'ombrello il leone
			Tocca nel seguente ordine la scimmia il monopattino l'ombrello la bambina
			Tocca nel seguente ordine la scimmia il monopattino l'ombrello il pappagallo
			Tocca nel seguente ordine la scimmia la bambina il leone il polipo
			Tocca nel seguente ordine la scimmia la bambina il leone il gelato
			Tocca nel seguente ordine la scimmia la bambina il leone gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bambina il leone la bicicletta
			Tocca nel seguente ordine la scimmia la bambina il leone il monopattino
			Tocca nel seguente ordine la scimmia la bambina il leone il pappagallo
			Tocca nel seguente ordine la scimmia la bambina il leone l'ombrello
			Tocca nel seguente ordine la scimmia la bambina il pappagallo il polipo
			Tocca nel seguente ordine la scimmia la bambina il pappagallo il gelato
			Tocca nel seguente ordine la scimmia la bambina il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bambina il pappagallo la bicicletta
			Tocca nel seguente ordine la scimmia la bambina il pappagallo il monopattino
			Tocca nel seguente ordine la scimmia la bambina il pappagallo il leone
			Tocca nel seguente ordine la scimmia la bambina il pappagallo l'ombrello
			Tocca nel seguente ordine la scimmia la bambina il polipo il pappagallo
			Tocca nel seguente ordine la scimmia la bambina il polipo il gelato
			Tocca nel seguente ordine la scimmia la bambina il polipo gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bambina il polipo la bicicletta
			Tocca nel seguente ordine la scimmia la bambina il polipo il monopattino
			Tocca nel seguente ordine la scimmia la bambina il polipo il leone
			Tocca nel seguente ordine la scimmia la bambina il polipo l'ombrello
			Tocca nel seguente ordine la scimmia la bambina il gelato il polipo
			Tocca nel seguente ordine la scimmia la bambina il gelato il pappagallo
			Tocca nel seguente ordine la scimmia la bambina il gelato gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bambina il gelato la bicicletta
			Tocca nel seguente ordine la scimmia la bambina il gelato il monopattino
			Tocca nel seguente ordine la scimmia la bambina il gelato il leone
			Tocca nel seguente ordine la scimmia la bambina il gelato l'ombrello
			Tocca nel seguente ordine la scimmia la bambina gli occhiali da sole il polipo
			Tocca nel seguente ordine la scimmia la bambina gli occhiali da sole il gelato
			Tocca nel seguente ordine la scimmia la bambina gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la scimmia la bambina gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la scimmia la bambina gli occhiali da sole il monopattino
			Tocca nel seguente ordine la scimmia la bambina gli occhiali da sole il leone
			Tocca nel seguente ordine la scimmia la bambina gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la scimmia la bambina la bicicletta il polipo
			Tocca nel seguente ordine la scimmia la bambina la bicicletta il gelato
			Tocca nel seguente ordine la scimmia la bambina la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bambina la bicicletta il pappagallo
			Tocca nel seguente ordine la scimmia la bambina la bicicletta il monopattino
			Tocca nel seguente ordine la scimmia la bambina la bicicletta il leone
			Tocca nel seguente ordine la scimmia la bambina la bicicletta l'ombrello
			Tocca nel seguente ordine la scimmia la bambina il monopattino il polipo
			Tocca nel seguente ordine la scimmia la bambina il monopattino il gelato
			Tocca nel seguente ordine la scimmia la bambina il monopattino gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bambina il monopattino la bicicletta
			Tocca nel seguente ordine la scimmia la bambina il monopattino il leone
			Tocca nel seguente ordine la scimmia la bambina il monopattino il pappagallo
			Tocca nel seguente ordine la scimmia la bambina il monopattino l'ombrello
			Tocca nel seguente ordine la scimmia la bambina l'ombrello il polipo
			Tocca nel seguente ordine la scimmia la bambina l'ombrello il gelato
			Tocca nel seguente ordine la scimmia la bambina l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la scimmia la bambina l'ombrello la bicicletta
			Tocca nel seguente ordine la scimmia la bambina l'ombrello il monopattino
			Tocca nel seguente ordine la scimmia la bambina l'ombrello il leone
			Tocca nel seguente ordine la scimmia la bambina l'ombrello il pappagallo
			Tocca nel seguente ordine la scimmia l'ombrello il leone il polipo
			Tocca nel seguente ordine la scimmia l'ombrello il leone il gelato
			Tocca nel seguente ordine la scimmia l'ombrello il leone gli occhiali da sole
			Tocca nel seguente ordine la scimmia l'ombrello il leone la bicicletta
			Tocca nel seguente ordine la scimmia l'ombrello il leone il monopattino
			Tocca nel seguente ordine la scimmia l'ombrello il leone la bambina
			Tocca nel seguente ordine la scimmia l'ombrello il leone il pappagallo
			Tocca nel seguente ordine la scimmia l'ombrello il pappagallo il polipo
			Tocca nel seguente ordine la scimmia l'ombrello il pappagallo il gelato
			Tocca nel seguente ordine la scimmia l'ombrello il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la scimmia l'ombrello il pappagallo la bicicletta
			Tocca nel seguente ordine la scimmia l'ombrello il pappagallo il monopattino
			Tocca nel seguente ordine la scimmia l'ombrello il pappagallo la bambina
			Tocca nel seguente ordine la scimmia l'ombrello il pappagallo il leone
			Tocca nel seguente ordine la scimmia l'ombrello il polipo il leone
			Tocca nel seguente ordine la scimmia l'ombrello il polipo il gelato
			Tocca nel seguente ordine la scimmia l'ombrello il polipo gli occhiali da sole
			Tocca nel seguente ordine la scimmia l'ombrello il polipo la bicicletta
			Tocca nel seguente ordine la scimmia l'ombrello il polipo il monopattino
			Tocca nel seguente ordine la scimmia l'ombrello il polipo la bambina
			Tocca nel seguente ordine la scimmia l'ombrello il polipo il pappagallo
			Tocca nel seguente ordine la scimmia l'ombrello il gelato il polipo
			Tocca nel seguente ordine la scimmia l'ombrello il gelato il leone
			Tocca nel seguente ordine la scimmia l'ombrello il gelato gli occhiali da sole
			Tocca nel seguente ordine la scimmia l'ombrello il gelato la bicicletta
			Tocca nel seguente ordine la scimmia l'ombrello il gelato il monopattino
			Tocca nel seguente ordine la scimmia l'ombrello il gelato la bambina
			Tocca nel seguente ordine la scimmia l'ombrello il gelato il pappagallo
			Tocca nel seguente ordine la scimmia l'ombrello gli occhiali da sole il polipo
			Tocca nel seguente ordine la scimmia l'ombrello gli occhiali da sole il gelato
			Tocca nel seguente ordine la scimmia l'ombrello gli occhiali da sole il leone
			Tocca nel seguente ordine la scimmia l'ombrello gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la scimmia l'ombrello gli occhiali da sole il monopattino
			Tocca nel seguente ordine la scimmia l'ombrello gli occhiali da sole la bambina
			Tocca nel seguente ordine la scimmia l'ombrello gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la scimmia l'ombrello la bicicletta il polipo
			Tocca nel seguente ordine la scimmia l'ombrello la bicicletta il gelato
			Tocca nel seguente ordine la scimmia l'ombrello la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la scimmia l'ombrello la bicicletta il leone
			Tocca nel seguente ordine la scimmia l'ombrello la bicicletta il monopattino
			Tocca nel seguente ordine la scimmia l'ombrello la bicicletta la bambina
			Tocca nel seguente ordine la scimmia l'ombrello la bicicletta il pappagallo
			Tocca nel seguente ordine la scimmia l'ombrello il monopattino il polipo
			Tocca nel seguente ordine la scimmia l'ombrello il monopattino il gelato
			Tocca nel seguente ordine la scimmia l'ombrello il monopattino gli occhiali da sole
			Tocca nel seguente ordine la scimmia l'ombrello il monopattino la bicicletta
			Tocca nel seguente ordine la scimmia l'ombrello il monopattino il leone
			Tocca nel seguente ordine la scimmia l'ombrello il monopattino la bambina
			Tocca nel seguente ordine la scimmia l'ombrello il monopattino il pappagallo
			Tocca nel seguente ordine la scimmia l'ombrello la bambina il polipo
			Tocca nel seguente ordine la scimmia l'ombrello la bambina il gelato
			Tocca nel seguente ordine la scimmia l'ombrello la bambina gli occhiali da sole
			Tocca nel seguente ordine la scimmia l'ombrello la bambina la bicicletta
			Tocca nel seguente ordine la scimmia l'ombrello la bambina il monopattino
			Tocca nel seguente ordine la scimmia l'ombrello la bambina il leone
			Tocca nel seguente ordine la scimmia l'ombrello la bambina il pappagallo
			Tocca nel seguente ordine il pappagallo il leone la scimmia il polipo
			Tocca nel seguente ordine il pappagallo il leone la scimmia il gelato
			Tocca nel seguente ordine il pappagallo il leone la scimmia gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il leone la scimmia la bicicletta
			Tocca nel seguente ordine il pappagallo il leone la scimmia il monopattino
			Tocca nel seguente ordine il pappagallo il leone la scimmia la bambina
			Tocca nel seguente ordine il pappagallo il leone la scimmia l'ombrello
			Tocca nel seguente ordine il pappagallo il leone il polipo la scimmia
			Tocca nel seguente ordine il pappagallo il leone il polipo il gelato
			Tocca nel seguente ordine il pappagallo il leone il polipo gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il leone il polipo la bicicletta
			Tocca nel seguente ordine il pappagallo il leone il polipo il monopattino
			Tocca nel seguente ordine il pappagallo il leone il polipo la bambina
			Tocca nel seguente ordine il pappagallo il leone il polipo l'ombrello
			Tocca nel seguente ordine il pappagallo il leone il gelato il polipo
			Tocca nel seguente ordine il pappagallo il leone il gelato la scimmia
			Tocca nel seguente ordine il pappagallo il leone il gelato gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il leone il gelato la bicicletta
			Tocca nel seguente ordine il pappagallo il leone il gelato il monopattino
			Tocca nel seguente ordine il pappagallo il leone il gelato la bambina
			Tocca nel seguente ordine il pappagallo il leone il gelato l'ombrello
			Tocca nel seguente ordine il pappagallo il leone gli occhiali da sole il polipo
			Tocca nel seguente ordine il pappagallo il leone gli occhiali da sole il gelato
			Tocca nel seguente ordine il pappagallo il leone gli occhiali da sole la scimmia
			Tocca nel seguente ordine il pappagallo il leone gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il pappagallo il leone gli occhiali da sole il monopattino
			Tocca nel seguente ordine il pappagallo il leone gli occhiali da sole la bambina
			Tocca nel seguente ordine il pappagallo il leone gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il pappagallo il leone la bicicletta il polipo
			Tocca nel seguente ordine il pappagallo il leone la bicicletta il gelato
			Tocca nel seguente ordine il pappagallo il leone la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il leone la bicicletta la scimmia
			Tocca nel seguente ordine il pappagallo il leone la bicicletta il monopattino
			Tocca nel seguente ordine il pappagallo il leone la bicicletta la bambina
			Tocca nel seguente ordine il pappagallo il leone la bicicletta l'ombrello
			Tocca nel seguente ordine il pappagallo il leone il monopattino il polipo
			Tocca nel seguente ordine il pappagallo il leone il monopattino il gelato
			Tocca nel seguente ordine il pappagallo il leone il monopattino gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il leone il monopattino la bicicletta
			Tocca nel seguente ordine il pappagallo il leone il monopattino la scimmia
			Tocca nel seguente ordine il pappagallo il leone il monopattino la bambina
			Tocca nel seguente ordine il pappagallo il leone il monopattino l'ombrello
			Tocca nel seguente ordine il pappagallo il leone la bambina il polipo
			Tocca nel seguente ordine il pappagallo il leone la bambina il gelato
			Tocca nel seguente ordine il pappagallo il leone la bambina gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il leone la bambina la bicicletta
			Tocca nel seguente ordine il pappagallo il leone la bambina il monopattino
			Tocca nel seguente ordine il pappagallo il leone la bambina la scimmia
			Tocca nel seguente ordine il pappagallo il leone la bambina l'ombrello
			Tocca nel seguente ordine il pappagallo il leone l'ombrello il polipo
			Tocca nel seguente ordine il pappagallo il leone l'ombrello il gelato
			Tocca nel seguente ordine il pappagallo il leone l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il leone l'ombrello la bicicletta
			Tocca nel seguente ordine il pappagallo il leone l'ombrello il monopattino
			Tocca nel seguente ordine il pappagallo il leone l'ombrello la bambina
			Tocca nel seguente ordine il pappagallo il leone l'ombrello la scimmia
			Tocca nel seguente ordine il pappagallo la scimmia il leone il polipo
			Tocca nel seguente ordine il pappagallo la scimmia il leone il gelato
			Tocca nel seguente ordine il pappagallo la scimmia il leone gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la scimmia il leone la bicicletta
			Tocca nel seguente ordine il pappagallo la scimmia il leone il monopattino
			Tocca nel seguente ordine il pappagallo la scimmia il leone la bambina
			Tocca nel seguente ordine il pappagallo la scimmia il leone l'ombrello
			Tocca nel seguente ordine il pappagallo la scimmia il polipo il leone
			Tocca nel seguente ordine il pappagallo la scimmia il polipo il gelato
			Tocca nel seguente ordine il pappagallo la scimmia il polipo gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la scimmia il polipo la bicicletta
			Tocca nel seguente ordine il pappagallo la scimmia il polipo il monopattino
			Tocca nel seguente ordine il pappagallo la scimmia il polipo la bambina
			Tocca nel seguente ordine il pappagallo la scimmia il polipo l'ombrello
			Tocca nel seguente ordine il pappagallo la scimmia il gelato il polipo
			Tocca nel seguente ordine il pappagallo la scimmia il gelato il leone
			Tocca nel seguente ordine il pappagallo la scimmia il gelato gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la scimmia il gelato la bicicletta
			Tocca nel seguente ordine il pappagallo la scimmia il gelato il monopattino
			Tocca nel seguente ordine il pappagallo la scimmia il gelato la bambina
			Tocca nel seguente ordine il pappagallo la scimmia il gelato l'ombrello
			Tocca nel seguente ordine il pappagallo la scimmia gli occhiali da sole il polipo
			Tocca nel seguente ordine il pappagallo la scimmia gli occhiali da sole il gelato
			Tocca nel seguente ordine il pappagallo la scimmia gli occhiali da sole il leone
			Tocca nel seguente ordine il pappagallo la scimmia gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il pappagallo la scimmia gli occhiali da sole il monopattino
			Tocca nel seguente ordine il pappagallo la scimmia gli occhiali da sole la bambina
			Tocca nel seguente ordine il pappagallo la scimmia gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il pappagallo la scimmia la bicicletta il polipo
			Tocca nel seguente ordine il pappagallo la scimmia la bicicletta il gelato
			Tocca nel seguente ordine il pappagallo la scimmia la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la scimmia la bicicletta il leone
			Tocca nel seguente ordine il pappagallo la scimmia la bicicletta il monopattino
			Tocca nel seguente ordine il pappagallo la scimmia la bicicletta la bambina
			Tocca nel seguente ordine il pappagallo la scimmia la bicicletta l'ombrello
			Tocca nel seguente ordine il pappagallo la scimmia il monopattino il polipo
			Tocca nel seguente ordine il pappagallo la scimmia il monopattino il gelato
			Tocca nel seguente ordine il pappagallo la scimmia il monopattino gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la scimmia il monopattino la bicicletta
			Tocca nel seguente ordine il pappagallo la scimmia il monopattino il leone
			Tocca nel seguente ordine il pappagallo la scimmia il monopattino la bambina
			Tocca nel seguente ordine il pappagallo la scimmia il monopattino l'ombrello
			Tocca nel seguente ordine il pappagallo la scimmia la bambina il polipo
			Tocca nel seguente ordine il pappagallo la scimmia la bambina il gelato
			Tocca nel seguente ordine il pappagallo la scimmia la bambina gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la scimmia la bambina la bicicletta
			Tocca nel seguente ordine il pappagallo la scimmia la bambina il monopattino
			Tocca nel seguente ordine il pappagallo la scimmia la bambina il leone
			Tocca nel seguente ordine il pappagallo la scimmia la bambina l'ombrello
			Tocca nel seguente ordine il pappagallo la scimmia l'ombrello il polipo
			Tocca nel seguente ordine il pappagallo la scimmia l'ombrello il gelato
			Tocca nel seguente ordine il pappagallo la scimmia l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la scimmia l'ombrello la bicicletta
			Tocca nel seguente ordine il pappagallo la scimmia l'ombrello il monopattino
			Tocca nel seguente ordine il pappagallo la scimmia l'ombrello la bambina
			Tocca nel seguente ordine il pappagallo la scimmia l'ombrello il leone
			Tocca nel seguente ordine il pappagallo il polipo il leone la scimmia
			Tocca nel seguente ordine il pappagallo il polipo il leone il gelato
			Tocca nel seguente ordine il pappagallo il polipo il leone gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il polipo il leone la bicicletta
			Tocca nel seguente ordine il pappagallo il polipo il leone il monopattino
			Tocca nel seguente ordine il pappagallo il polipo il leone la bambina
			Tocca nel seguente ordine il pappagallo il polipo il leone l'ombrello
			Tocca nel seguente ordine il pappagallo il polipo la scimmia il leone
			Tocca nel seguente ordine il pappagallo il polipo la scimmia il gelato
			Tocca nel seguente ordine il pappagallo il polipo la scimmia gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il polipo la scimmia la bicicletta
			Tocca nel seguente ordine il pappagallo il polipo la scimmia il monopattino
			Tocca nel seguente ordine il pappagallo il polipo la scimmia la bambina
			Tocca nel seguente ordine il pappagallo il polipo la scimmia l'ombrello
			Tocca nel seguente ordine il pappagallo il polipo il gelato il leone
			Tocca nel seguente ordine il pappagallo il polipo il gelato la scimmia
			Tocca nel seguente ordine il pappagallo il polipo il gelato gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il polipo il gelato la bicicletta
			Tocca nel seguente ordine il pappagallo il polipo il gelato il monopattino
			Tocca nel seguente ordine il pappagallo il polipo il gelato la bambina
			Tocca nel seguente ordine il pappagallo il polipo il gelato l'ombrello
			Tocca nel seguente ordine il pappagallo il polipo gli occhiali da sole il gelato
			Tocca nel seguente ordine il pappagallo il polipo gli occhiali da sole il leone
			Tocca nel seguente ordine il pappagallo il polipo gli occhiali da sole la scimmia
			Tocca nel seguente ordine il pappagallo il polipo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il pappagallo il polipo gli occhiali da sole il monopattino
			Tocca nel seguente ordine il pappagallo il polipo gli occhiali da sole la bambina
			Tocca nel seguente ordine il pappagallo il polipo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il pappagallo il polipo la bicicletta il leone
			Tocca nel seguente ordine il pappagallo il polipo la bicicletta il gelato
			Tocca nel seguente ordine il pappagallo il polipo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il polipo la bicicletta la scimmia
			Tocca nel seguente ordine il pappagallo il polipo la bicicletta il monopattino
			Tocca nel seguente ordine il pappagallo il polipo la bicicletta la bambina
			Tocca nel seguente ordine il pappagallo il polipo la bicicletta l'ombrello
			Tocca nel seguente ordine il pappagallo il polipo il monopattino la scimmia
			Tocca nel seguente ordine il pappagallo il polipo il monopattino il gelato
			Tocca nel seguente ordine il pappagallo il polipo il monopattino gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il polipo il monopattino la bicicletta
			Tocca nel seguente ordine il pappagallo il polipo il monopattino il leone
			Tocca nel seguente ordine il pappagallo il polipo il monopattino la bambina
			Tocca nel seguente ordine il pappagallo il polipo il monopattino l'ombrello
			Tocca nel seguente ordine il pappagallo il polipo la bambina il leone
			Tocca nel seguente ordine il pappagallo il polipo la bambina il gelato
			Tocca nel seguente ordine il pappagallo il polipo la bambina gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il polipo la bambina la bicicletta
			Tocca nel seguente ordine il pappagallo il polipo la bambina il monopattino
			Tocca nel seguente ordine il pappagallo il polipo la bambina la scimmia
			Tocca nel seguente ordine il pappagallo il polipo la bambina l'ombrello
			Tocca nel seguente ordine il pappagallo il polipo l'ombrello il leone
			Tocca nel seguente ordine il pappagallo il polipo l'ombrello il gelato
			Tocca nel seguente ordine il pappagallo il polipo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il polipo l'ombrello la bicicletta
			Tocca nel seguente ordine il pappagallo il polipo l'ombrello il monopattino
			Tocca nel seguente ordine il pappagallo il polipo l'ombrello la bambina
			Tocca nel seguente ordine il pappagallo il polipo l'ombrello la scimmia
			Tocca nel seguente ordine il pappagallo il gelato il leone il polipo
			Tocca nel seguente ordine il pappagallo il gelato il leone la scimmia
			Tocca nel seguente ordine il pappagallo il gelato il leone gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il gelato il leone la bicicletta
			Tocca nel seguente ordine il pappagallo il gelato il leone il monopattino
			Tocca nel seguente ordine il pappagallo il gelato il leone la bambina
			Tocca nel seguente ordine il pappagallo il gelato il leone l'ombrello
			Tocca nel seguente ordine il pappagallo il gelato la scimmia il polipo
			Tocca nel seguente ordine il pappagallo il gelato la scimmia il leone
			Tocca nel seguente ordine il pappagallo il gelato la scimmia gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il gelato la scimmia la bicicletta
			Tocca nel seguente ordine il pappagallo il gelato la scimmia il monopattino
			Tocca nel seguente ordine il pappagallo il gelato la scimmia la bambina
			Tocca nel seguente ordine il pappagallo il gelato la scimmia l'ombrello
			Tocca nel seguente ordine il pappagallo il gelato il polipo il leone
			Tocca nel seguente ordine il pappagallo il gelato il polipo la scimmia
			Tocca nel seguente ordine il pappagallo il gelato il polipo gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il gelato il polipo la bicicletta
			Tocca nel seguente ordine il pappagallo il gelato il polipo il monopattino
			Tocca nel seguente ordine il pappagallo il gelato il polipo la bambina
			Tocca nel seguente ordine il pappagallo il gelato il polipo l'ombrello
			Tocca nel seguente ordine il pappagallo il gelato gli occhiali da sole il polipo
			Tocca nel seguente ordine il pappagallo il gelato gli occhiali da sole il leone
			Tocca nel seguente ordine il pappagallo il gelato gli occhiali da sole la scimmia
			Tocca nel seguente ordine il pappagallo il gelato gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il pappagallo il gelato gli occhiali da sole il monopattino
			Tocca nel seguente ordine il pappagallo il gelato gli occhiali da sole la bambina
			Tocca nel seguente ordine il pappagallo il gelato gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il pappagallo il gelato la bicicletta il polipo
			Tocca nel seguente ordine il pappagallo il gelato la bicicletta il leone
			Tocca nel seguente ordine il pappagallo il gelato la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il gelato la bicicletta la scimmia
			Tocca nel seguente ordine il pappagallo il gelato la bicicletta il monopattino
			Tocca nel seguente ordine il pappagallo il gelato la bicicletta la bambina
			Tocca nel seguente ordine il pappagallo il gelato la bicicletta l'ombrello
			Tocca nel seguente ordine il pappagallo il gelato il monopattino il polipo
			Tocca nel seguente ordine il pappagallo il gelato il monopattino il leone
			Tocca nel seguente ordine il pappagallo il gelato il monopattino gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il gelato il monopattino la bicicletta
			Tocca nel seguente ordine il pappagallo il gelato il monopattino la scimmia
			Tocca nel seguente ordine il pappagallo il gelato il monopattino la bambina
			Tocca nel seguente ordine il pappagallo il gelato il monopattino l'ombrello
			Tocca nel seguente ordine il pappagallo il gelato la bambina il polipo
			Tocca nel seguente ordine il pappagallo il gelato la bambina il leone
			Tocca nel seguente ordine il pappagallo il gelato la bambina gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il gelato la bambina la bicicletta
			Tocca nel seguente ordine il pappagallo il gelato la bambina il monopattino
			Tocca nel seguente ordine il pappagallo il gelato la bambina la scimmia
			Tocca nel seguente ordine il pappagallo il gelato la bambina l'ombrello
			Tocca nel seguente ordine il pappagallo il gelato l'ombrello il polipo
			Tocca nel seguente ordine il pappagallo il gelato l'ombrello il leone
			Tocca nel seguente ordine il pappagallo il gelato l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il gelato l'ombrello la bicicletta
			Tocca nel seguente ordine il pappagallo il gelato l'ombrello il monopattino
			Tocca nel seguente ordine il pappagallo il gelato l'ombrello la bambina
			Tocca nel seguente ordine il pappagallo il gelato l'ombrello la scimmia
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il leone il polipo
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il leone il gelato
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il leone la scimmia
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il leone la bicicletta
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il leone il monopattino
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il leone la bambina
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il leone l'ombrello
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la scimmia il polipo
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la scimmia il gelato
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la scimmia il leone
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la scimmia la bicicletta
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la scimmia il monopattino
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la scimmia la bambina
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la scimmia l'ombrello
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il polipo il leone
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il polipo il gelato
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il polipo la scimmia
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il polipo la bicicletta
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il polipo il monopattino
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il polipo la bambina
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il polipo l'ombrello
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il gelato il polipo
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il gelato il leone
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il gelato la scimmia
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il gelato la bicicletta
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il gelato il monopattino
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il gelato la bambina
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il gelato l'ombrello
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bicicletta il polipo
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bicicletta il gelato
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bicicletta il leone
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bicicletta la scimmia
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bicicletta il monopattino
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bicicletta la bambina
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bicicletta l'ombrello
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il monopattino il polipo
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il monopattino il gelato
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il monopattino la scimmia
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il monopattino la bicicletta
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il monopattino il leone
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il monopattino la bambina
			Tocca nel seguente ordine il pappagallo gli occhiali da sole il monopattino l'ombrello
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bambina il polipo
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bambina il gelato
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bambina la scimmia
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bambina la bicicletta
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bambina il monopattino
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bambina il leone
			Tocca nel seguente ordine il pappagallo gli occhiali da sole la bambina l'ombrello
			Tocca nel seguente ordine il pappagallo gli occhiali da sole l'ombrello il polipo
			Tocca nel seguente ordine il pappagallo gli occhiali da sole l'ombrello il gelato
			Tocca nel seguente ordine il pappagallo gli occhiali da sole l'ombrello il leone
			Tocca nel seguente ordine il pappagallo gli occhiali da sole l'ombrello la bicicletta
			Tocca nel seguente ordine il pappagallo gli occhiali da sole l'ombrello il monopattino
			Tocca nel seguente ordine il pappagallo gli occhiali da sole l'ombrello la bambina
			Tocca nel seguente ordine il pappagallo gli occhiali da sole l'ombrello la scimmia
			Tocca nel seguente ordine il pappagallo la bicicletta il leone il polipo
			Tocca nel seguente ordine il pappagallo la bicicletta il leone il gelato
			Tocca nel seguente ordine il pappagallo la bicicletta il leone gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bicicletta il leone la scimmia
			Tocca nel seguente ordine il pappagallo la bicicletta il leone il monopattino
			Tocca nel seguente ordine il pappagallo la bicicletta il leone la bambina
			Tocca nel seguente ordine il pappagallo la bicicletta il leone l'ombrello
			Tocca nel seguente ordine il pappagallo la bicicletta la scimmia il polipo
			Tocca nel seguente ordine il pappagallo la bicicletta la scimmia il gelato
			Tocca nel seguente ordine il pappagallo la bicicletta la scimmia gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bicicletta la scimmia il leone
			Tocca nel seguente ordine il pappagallo la bicicletta la scimmia il monopattino
			Tocca nel seguente ordine il pappagallo la bicicletta la scimmia la bambina
			Tocca nel seguente ordine il pappagallo la bicicletta la scimmia l'ombrello
			Tocca nel seguente ordine il pappagallo la bicicletta il polipo il leone
			Tocca nel seguente ordine il pappagallo la bicicletta il polipo il gelato
			Tocca nel seguente ordine il pappagallo la bicicletta il polipo gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bicicletta il polipo la scimmia
			Tocca nel seguente ordine il pappagallo la bicicletta il polipo il monopattino
			Tocca nel seguente ordine il pappagallo la bicicletta il polipo la bambina
			Tocca nel seguente ordine il pappagallo la bicicletta il polipo l'ombrello
			Tocca nel seguente ordine il pappagallo la bicicletta il gelato il polipo
			Tocca nel seguente ordine il pappagallo la bicicletta il gelato il leone
			Tocca nel seguente ordine il pappagallo la bicicletta il gelato gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bicicletta il gelato la scimmia
			Tocca nel seguente ordine il pappagallo la bicicletta il gelato il monopattino
			Tocca nel seguente ordine il pappagallo la bicicletta il gelato la bambina
			Tocca nel seguente ordine il pappagallo la bicicletta il gelato l'ombrello
			Tocca nel seguente ordine il pappagallo la bicicletta gli occhiali da sole il polipo
			Tocca nel seguente ordine il pappagallo la bicicletta gli occhiali da sole il gelato
			Tocca nel seguente ordine il pappagallo la bicicletta gli occhiali da sole il leone
			Tocca nel seguente ordine il pappagallo la bicicletta gli occhiali da sole la scimmia
			Tocca nel seguente ordine il pappagallo la bicicletta gli occhiali da sole il monopattino
			Tocca nel seguente ordine il pappagallo la bicicletta gli occhiali da sole la bambina
			Tocca nel seguente ordine il pappagallo la bicicletta gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il pappagallo la bicicletta il monopattino il polipo
			Tocca nel seguente ordine il pappagallo la bicicletta il monopattino il gelato
			Tocca nel seguente ordine il pappagallo la bicicletta il monopattino gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bicicletta il monopattino la scimmia
			Tocca nel seguente ordine il pappagallo la bicicletta il monopattino il leone
			Tocca nel seguente ordine il pappagallo la bicicletta il monopattino la bambina
			Tocca nel seguente ordine il pappagallo la bicicletta il monopattino l'ombrello
			Tocca nel seguente ordine il pappagallo la bicicletta la bambina il polipo
			Tocca nel seguente ordine il pappagallo la bicicletta la bambina il gelato
			Tocca nel seguente ordine il pappagallo la bicicletta la bambina gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bicicletta la bambina la scimmia
			Tocca nel seguente ordine il pappagallo la bicicletta la bambina il monopattino
			Tocca nel seguente ordine il pappagallo la bicicletta la bambina il leone
			Tocca nel seguente ordine il pappagallo la bicicletta la bambina l'ombrello
			Tocca nel seguente ordine il pappagallo la bicicletta l'ombrello il polipo
			Tocca nel seguente ordine il pappagallo la bicicletta l'ombrello il gelato
			Tocca nel seguente ordine il pappagallo la bicicletta l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bicicletta l'ombrello la scimmia
			Tocca nel seguente ordine il pappagallo la bicicletta l'ombrello il monopattino
			Tocca nel seguente ordine il pappagallo la bicicletta l'ombrello la bambina
			Tocca nel seguente ordine il pappagallo la bicicletta l'ombrello il leone
			Tocca nel seguente ordine il pappagallo il monopattino il leone il polipo
			Tocca nel seguente ordine il pappagallo il monopattino il leone il gelato
			Tocca nel seguente ordine il pappagallo il monopattino il leone gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il monopattino il leone la bicicletta
			Tocca nel seguente ordine il pappagallo il monopattino il leone la scimmia
			Tocca nel seguente ordine il pappagallo il monopattino il leone la bambina
			Tocca nel seguente ordine il pappagallo il monopattino il leone l'ombrello
			Tocca nel seguente ordine il pappagallo il monopattino la scimmia il polipo
			Tocca nel seguente ordine il pappagallo il monopattino la scimmia il gelato
			Tocca nel seguente ordine il pappagallo il monopattino la scimmia gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il monopattino la scimmia la bicicletta
			Tocca nel seguente ordine il pappagallo il monopattino la scimmia il leone
			Tocca nel seguente ordine il pappagallo il monopattino la scimmia la bambina
			Tocca nel seguente ordine il pappagallo il monopattino la scimmia l'ombrello
			Tocca nel seguente ordine il pappagallo il monopattino il polipo la scimmia
			Tocca nel seguente ordine il pappagallo il monopattino il polipo il gelato
			Tocca nel seguente ordine il pappagallo il monopattino il polipo gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il monopattino il polipo la bicicletta
			Tocca nel seguente ordine il pappagallo il monopattino il polipo il leone
			Tocca nel seguente ordine il pappagallo il monopattino il polipo la bambina
			Tocca nel seguente ordine il pappagallo il monopattino il polipo l'ombrello
			Tocca nel seguente ordine il pappagallo il monopattino il gelato il polipo
			Tocca nel seguente ordine il pappagallo il monopattino il gelato il leone
			Tocca nel seguente ordine il pappagallo il monopattino il gelato gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il monopattino il gelato la bicicletta
			Tocca nel seguente ordine il pappagallo il monopattino il gelato la scimmia
			Tocca nel seguente ordine il pappagallo il monopattino il gelato la bambina
			Tocca nel seguente ordine il pappagallo il monopattino il gelato l'ombrello
			Tocca nel seguente ordine il pappagallo il monopattino gli occhiali da sole il polipo
			Tocca nel seguente ordine il pappagallo il monopattino gli occhiali da sole il gelato
			Tocca nel seguente ordine il pappagallo il monopattino gli occhiali da sole il leone
			Tocca nel seguente ordine il pappagallo il monopattino gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il pappagallo il monopattino gli occhiali da sole la scimmia
			Tocca nel seguente ordine il pappagallo il monopattino gli occhiali da sole la bambina
			Tocca nel seguente ordine il pappagallo il monopattino gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il pappagallo il monopattino la bicicletta il polipo
			Tocca nel seguente ordine il pappagallo il monopattino la bicicletta il gelato
			Tocca nel seguente ordine il pappagallo il monopattino la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il monopattino la bicicletta il leone
			Tocca nel seguente ordine il pappagallo il monopattino la bicicletta la scimmia
			Tocca nel seguente ordine il pappagallo il monopattino la bicicletta la bambina
			Tocca nel seguente ordine il pappagallo il monopattino la bicicletta l'ombrello
			Tocca nel seguente ordine il pappagallo il monopattino la bambina il polipo
			Tocca nel seguente ordine il pappagallo il monopattino la bambina il gelato
			Tocca nel seguente ordine il pappagallo il monopattino la bambina gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il monopattino la bambina la bicicletta
			Tocca nel seguente ordine il pappagallo il monopattino la bambina la scimmia
			Tocca nel seguente ordine il pappagallo il monopattino la bambina il leone
			Tocca nel seguente ordine il pappagallo il monopattino la bambina l'ombrello
			Tocca nel seguente ordine il pappagallo il monopattino l'ombrello il polipo
			Tocca nel seguente ordine il pappagallo il monopattino l'ombrello il gelato
			Tocca nel seguente ordine il pappagallo il monopattino l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il pappagallo il monopattino l'ombrello la bicicletta
			Tocca nel seguente ordine il pappagallo il monopattino l'ombrello la scimmia
			Tocca nel seguente ordine il pappagallo il monopattino l'ombrello la bambina
			Tocca nel seguente ordine il pappagallo il monopattino l'ombrello il leone
			Tocca nel seguente ordine il pappagallo la bambina il leone il polipo
			Tocca nel seguente ordine il pappagallo la bambina il leone il gelato
			Tocca nel seguente ordine il pappagallo la bambina il leone gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bambina il leone la bicicletta
			Tocca nel seguente ordine il pappagallo la bambina il leone il monopattino
			Tocca nel seguente ordine il pappagallo la bambina il leone la scimmia
			Tocca nel seguente ordine il pappagallo la bambina il leone l'ombrello
			Tocca nel seguente ordine il pappagallo la bambina la scimmia il polipo
			Tocca nel seguente ordine il pappagallo la bambina la scimmia il gelato
			Tocca nel seguente ordine il pappagallo la bambina la scimmia gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bambina la scimmia la bicicletta
			Tocca nel seguente ordine il pappagallo la bambina la scimmia il monopattino
			Tocca nel seguente ordine il pappagallo la bambina la scimmia il leone
			Tocca nel seguente ordine il pappagallo la bambina la scimmia l'ombrello
			Tocca nel seguente ordine il pappagallo la bambina il polipo il leone
			Tocca nel seguente ordine il pappagallo la bambina il polipo il gelato
			Tocca nel seguente ordine il pappagallo la bambina il polipo gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bambina il polipo la bicicletta
			Tocca nel seguente ordine il pappagallo la bambina il polipo il monopattino
			Tocca nel seguente ordine il pappagallo la bambina il polipo la scimmia
			Tocca nel seguente ordine il pappagallo la bambina il polipo l'ombrello
			Tocca nel seguente ordine il pappagallo la bambina il gelato il polipo
			Tocca nel seguente ordine il pappagallo la bambina il gelato il leone
			Tocca nel seguente ordine il pappagallo la bambina il gelato gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bambina il gelato la bicicletta
			Tocca nel seguente ordine il pappagallo la bambina il gelato il monopattino
			Tocca nel seguente ordine il pappagallo la bambina il gelato la scimmia
			Tocca nel seguente ordine il pappagallo la bambina il gelato l'ombrello
			Tocca nel seguente ordine il pappagallo la bambina gli occhiali da sole il polipo
			Tocca nel seguente ordine il pappagallo la bambina gli occhiali da sole il gelato
			Tocca nel seguente ordine il pappagallo la bambina gli occhiali da sole il leone
			Tocca nel seguente ordine il pappagallo la bambina gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il pappagallo la bambina gli occhiali da sole il monopattino
			Tocca nel seguente ordine il pappagallo la bambina gli occhiali da sole la scimmia
			Tocca nel seguente ordine il pappagallo la bambina gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il pappagallo la bambina la bicicletta il polipo
			Tocca nel seguente ordine il pappagallo la bambina la bicicletta il gelato
			Tocca nel seguente ordine il pappagallo la bambina la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bambina la bicicletta il leone
			Tocca nel seguente ordine il pappagallo la bambina la bicicletta il monopattino
			Tocca nel seguente ordine il pappagallo la bambina la bicicletta la scimmia
			Tocca nel seguente ordine il pappagallo la bambina la bicicletta l'ombrello
			Tocca nel seguente ordine il pappagallo la bambina il monopattino il polipo
			Tocca nel seguente ordine il pappagallo la bambina il monopattino il gelato
			Tocca nel seguente ordine il pappagallo la bambina il monopattino gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bambina il monopattino la bicicletta
			Tocca nel seguente ordine il pappagallo la bambina il monopattino il leone
			Tocca nel seguente ordine il pappagallo la bambina il monopattino la scimmia
			Tocca nel seguente ordine il pappagallo la bambina il monopattino l'ombrello
			Tocca nel seguente ordine il pappagallo la bambina l'ombrello il polipo
			Tocca nel seguente ordine il pappagallo la bambina l'ombrello il gelato
			Tocca nel seguente ordine il pappagallo la bambina l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il pappagallo la bambina l'ombrello la bicicletta
			Tocca nel seguente ordine il pappagallo la bambina l'ombrello il monopattino
			Tocca nel seguente ordine il pappagallo la bambina l'ombrello la scimmia
			Tocca nel seguente ordine il pappagallo la bambina l'ombrello il leone
			Tocca nel seguente ordine il pappagallo l'ombrello il leone il polipo
			Tocca nel seguente ordine il pappagallo l'ombrello il leone il gelato
			Tocca nel seguente ordine il pappagallo l'ombrello il leone gli occhiali da sole
			Tocca nel seguente ordine il pappagallo l'ombrello il leone la bicicletta
			Tocca nel seguente ordine il pappagallo l'ombrello il leone il monopattino
			Tocca nel seguente ordine il pappagallo l'ombrello il leone la bambina
			Tocca nel seguente ordine il pappagallo l'ombrello il leone la scimmia
			Tocca nel seguente ordine il pappagallo l'ombrello la scimmia il polipo
			Tocca nel seguente ordine il pappagallo l'ombrello la scimmia il gelato
			Tocca nel seguente ordine il pappagallo l'ombrello la scimmia gli occhiali da sole
			Tocca nel seguente ordine il pappagallo l'ombrello la scimmia la bicicletta
			Tocca nel seguente ordine il pappagallo l'ombrello la scimmia il monopattino
			Tocca nel seguente ordine il pappagallo l'ombrello la scimmia la bambina
			Tocca nel seguente ordine il pappagallo l'ombrello la scimmia il leone
			Tocca nel seguente ordine il pappagallo l'ombrello il polipo il leone
			Tocca nel seguente ordine il pappagallo l'ombrello il polipo il gelato
			Tocca nel seguente ordine il pappagallo l'ombrello il polipo gli occhiali da sole
			Tocca nel seguente ordine il pappagallo l'ombrello il polipo la bicicletta
			Tocca nel seguente ordine il pappagallo l'ombrello il polipo il monopattino
			Tocca nel seguente ordine il pappagallo l'ombrello il polipo la bambina
			Tocca nel seguente ordine il pappagallo l'ombrello il polipo la scimmia
			Tocca nel seguente ordine il pappagallo l'ombrello il gelato il polipo
			Tocca nel seguente ordine il pappagallo l'ombrello il gelato il leone
			Tocca nel seguente ordine il pappagallo l'ombrello il gelato gli occhiali da sole
			Tocca nel seguente ordine il pappagallo l'ombrello il gelato la bicicletta
			Tocca nel seguente ordine il pappagallo l'ombrello il gelato il monopattino
			Tocca nel seguente ordine il pappagallo l'ombrello il gelato la bambina
			Tocca nel seguente ordine il pappagallo l'ombrello il gelato la scimmia
			Tocca nel seguente ordine il pappagallo l'ombrello gli occhiali da sole il polipo
			Tocca nel seguente ordine il pappagallo l'ombrello gli occhiali da sole il gelato
			Tocca nel seguente ordine il pappagallo l'ombrello gli occhiali da sole il leone
			Tocca nel seguente ordine il pappagallo l'ombrello gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il pappagallo l'ombrello gli occhiali da sole il monopattino
			Tocca nel seguente ordine il pappagallo l'ombrello gli occhiali da sole la bambina
			Tocca nel seguente ordine il pappagallo l'ombrello gli occhiali da sole la scimmia
			Tocca nel seguente ordine il pappagallo l'ombrello la bicicletta il polipo
			Tocca nel seguente ordine il pappagallo l'ombrello la bicicletta il gelato
			Tocca nel seguente ordine il pappagallo l'ombrello la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il pappagallo l'ombrello la bicicletta il leone
			Tocca nel seguente ordine il pappagallo l'ombrello la bicicletta il monopattino
			Tocca nel seguente ordine il pappagallo l'ombrello la bicicletta la bambina
			Tocca nel seguente ordine il pappagallo l'ombrello la bicicletta la scimmia
			Tocca nel seguente ordine il pappagallo l'ombrello il monopattino il polipo
			Tocca nel seguente ordine il pappagallo l'ombrello il monopattino il gelato
			Tocca nel seguente ordine il pappagallo l'ombrello il monopattino gli occhiali da sole
			Tocca nel seguente ordine il pappagallo l'ombrello il monopattino la bicicletta
			Tocca nel seguente ordine il pappagallo l'ombrello il monopattino il leone
			Tocca nel seguente ordine il pappagallo l'ombrello il monopattino la bambina
			Tocca nel seguente ordine il pappagallo l'ombrello il monopattino la scimmia
			Tocca nel seguente ordine il pappagallo l'ombrello la bambina il polipo
			Tocca nel seguente ordine il pappagallo l'ombrello la bambina il gelato
			Tocca nel seguente ordine il pappagallo l'ombrello la bambina gli occhiali da sole
			Tocca nel seguente ordine il pappagallo l'ombrello la bambina la bicicletta
			Tocca nel seguente ordine il pappagallo l'ombrello la bambina il monopattino
			Tocca nel seguente ordine il pappagallo l'ombrello la bambina il leone
			Tocca nel seguente ordine il pappagallo l'ombrello la bambina la scimmia
			Tocca nel seguente ordine il polipo il leone la scimmia il pappagallo
			Tocca nel seguente ordine il polipo il leone la scimmia il gelato
			Tocca nel seguente ordine il polipo il leone la scimmia gli occhiali da sole
			Tocca nel seguente ordine il polipo il leone la scimmia la bicicletta
			Tocca nel seguente ordine il polipo il leone la scimmia il monopattino
			Tocca nel seguente ordine il polipo il leone la scimmia la bambina
			Tocca nel seguente ordine il polipo il leone la scimmia l'ombrello
			Tocca nel seguente ordine il polipo il leone il pappagallo la scimmia
			Tocca nel seguente ordine il polipo il leone il pappagallo il gelato
			Tocca nel seguente ordine il polipo il leone il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il polipo il leone il pappagallo la bicicletta
			Tocca nel seguente ordine il polipo il leone il pappagallo il monopattino
			Tocca nel seguente ordine il polipo il leone il pappagallo la bambina
			Tocca nel seguente ordine il polipo il leone il pappagallo l'ombrello
			Tocca nel seguente ordine il polipo il leone il gelato la scimmia
			Tocca nel seguente ordine il polipo il leone il gelato il pappagallo
			Tocca nel seguente ordine il polipo il leone il gelato gli occhiali da sole
			Tocca nel seguente ordine il polipo il leone il gelato la bicicletta
			Tocca nel seguente ordine il polipo il leone il gelato il monopattino
			Tocca nel seguente ordine il polipo il leone il gelato la bambina
			Tocca nel seguente ordine il polipo il leone il gelato l'ombrello
			Tocca nel seguente ordine il polipo il leone gli occhiali da sole la scimmia
			Tocca nel seguente ordine il polipo il leone gli occhiali da sole il gelato
			Tocca nel seguente ordine il polipo il leone gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il polipo il leone gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il polipo il leone gli occhiali da sole il monopattino
			Tocca nel seguente ordine il polipo il leone gli occhiali da sole la bambina
			Tocca nel seguente ordine il polipo il leone gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il polipo il leone la bicicletta la scimmia
			Tocca nel seguente ordine il polipo il leone la bicicletta il gelato
			Tocca nel seguente ordine il polipo il leone la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il polipo il leone la bicicletta il pappagallo
			Tocca nel seguente ordine il polipo il leone la bicicletta il monopattino
			Tocca nel seguente ordine il polipo il leone la bicicletta la bambina
			Tocca nel seguente ordine il polipo il leone la bicicletta l'ombrello
			Tocca nel seguente ordine il polipo il leone il monopattino la scimmia
			Tocca nel seguente ordine il polipo il leone il monopattino il gelato
			Tocca nel seguente ordine il polipo il leone il monopattino gli occhiali da sole
			Tocca nel seguente ordine il polipo il leone il monopattino la bicicletta
			Tocca nel seguente ordine il polipo il leone il monopattino il pappagallo
			Tocca nel seguente ordine il polipo il leone il monopattino la bambina
			Tocca nel seguente ordine il polipo il leone il monopattino l'ombrello
			Tocca nel seguente ordine il polipo il leone la bambina la scimmia
			Tocca nel seguente ordine il polipo il leone la bambina il gelato
			Tocca nel seguente ordine il polipo il leone la bambina gli occhiali da sole
			Tocca nel seguente ordine il polipo il leone la bambina la bicicletta
			Tocca nel seguente ordine il polipo il leone la bambina il monopattino
			Tocca nel seguente ordine il polipo il leone la bambina il pappagallo
			Tocca nel seguente ordine il polipo il leone la bambina l'ombrello
			Tocca nel seguente ordine il polipo il leone l'ombrello la scimmia
			Tocca nel seguente ordine il polipo il leone l'ombrello il gelato
			Tocca nel seguente ordine il polipo il leone l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il polipo il leone l'ombrello la bicicletta
			Tocca nel seguente ordine il polipo il leone l'ombrello il monopattino
			Tocca nel seguente ordine il polipo il leone l'ombrello la bambina
			Tocca nel seguente ordine il polipo il leone l'ombrello il pappagallo
			Tocca nel seguente ordine il polipo la scimmia il leone il leone
			Tocca nel seguente ordine il polipo la scimmia il leone il gelato
			Tocca nel seguente ordine il polipo la scimmia il leone gli occhiali da sole
			Tocca nel seguente ordine il polipo la scimmia il leone la bicicletta
			Tocca nel seguente ordine il polipo la scimmia il leone il monopattino
			Tocca nel seguente ordine il polipo la scimmia il leone la bambina
			Tocca nel seguente ordine il polipo la scimmia il leone l'ombrello
			Tocca nel seguente ordine il polipo la scimmia il pappagallo il leone
			Tocca nel seguente ordine il polipo la scimmia il pappagallo il gelato
			Tocca nel seguente ordine il polipo la scimmia il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il polipo la scimmia il pappagallo la bicicletta
			Tocca nel seguente ordine il polipo la scimmia il pappagallo il monopattino
			Tocca nel seguente ordine il polipo la scimmia il pappagallo la bambina
			Tocca nel seguente ordine il polipo la scimmia il pappagallo l'ombrello
			Tocca nel seguente ordine il polipo la scimmia il gelato il leone
			Tocca nel seguente ordine il polipo la scimmia il gelato il pappagallo
			Tocca nel seguente ordine il polipo la scimmia il gelato gli occhiali da sole
			Tocca nel seguente ordine il polipo la scimmia il gelato la bicicletta
			Tocca nel seguente ordine il polipo la scimmia il gelato il monopattino
			Tocca nel seguente ordine il polipo la scimmia il gelato la bambina
			Tocca nel seguente ordine il polipo la scimmia il gelato l'ombrello
			Tocca nel seguente ordine il polipo la scimmia gli occhiali da sole il leone
			Tocca nel seguente ordine il polipo la scimmia gli occhiali da sole il gelato
			Tocca nel seguente ordine il polipo la scimmia gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il polipo la scimmia gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il polipo la scimmia gli occhiali da sole il monopattino
			Tocca nel seguente ordine il polipo la scimmia gli occhiali da sole la bambina
			Tocca nel seguente ordine il polipo la scimmia gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il polipo la scimmia la bicicletta il leone
			Tocca nel seguente ordine il polipo la scimmia la bicicletta il gelato
			Tocca nel seguente ordine il polipo la scimmia la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il polipo la scimmia la bicicletta il pappagallo
			Tocca nel seguente ordine il polipo la scimmia la bicicletta il monopattino
			Tocca nel seguente ordine il polipo la scimmia la bicicletta la bambina
			Tocca nel seguente ordine il polipo la scimmia la bicicletta l'ombrello
			Tocca nel seguente ordine il polipo la scimmia il monopattino il leone
			Tocca nel seguente ordine il polipo la scimmia il monopattino il gelato
			Tocca nel seguente ordine il polipo la scimmia il monopattino gli occhiali da sole
			Tocca nel seguente ordine il polipo la scimmia il monopattino la bicicletta
			Tocca nel seguente ordine il polipo la scimmia il monopattino il pappagallo
			Tocca nel seguente ordine il polipo la scimmia il monopattino la bambina
			Tocca nel seguente ordine il polipo la scimmia il monopattino l'ombrello
			Tocca nel seguente ordine il polipo la scimmia la bambina il leone
			Tocca nel seguente ordine il polipo la scimmia la bambina il gelato
			Tocca nel seguente ordine il polipo la scimmia la bambina gli occhiali da sole
			Tocca nel seguente ordine il polipo la scimmia la bambina la bicicletta
			Tocca nel seguente ordine il polipo la scimmia la bambina il monopattino
			Tocca nel seguente ordine il polipo la scimmia la bambina il pappagallo
			Tocca nel seguente ordine il polipo la scimmia la bambina l'ombrello
			Tocca nel seguente ordine il polipo la scimmia l'ombrello il leone
			Tocca nel seguente ordine il polipo la scimmia l'ombrello il gelato
			Tocca nel seguente ordine il polipo la scimmia l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il polipo la scimmia l'ombrello la bicicletta
			Tocca nel seguente ordine il polipo la scimmia l'ombrello il monopattino
			Tocca nel seguente ordine il polipo la scimmia l'ombrello la bambina
			Tocca nel seguente ordine il polipo la scimmia l'ombrello il pappagallo
			Tocca nel seguente ordine il polipo il pappagallo il leone la scimmia
			Tocca nel seguente ordine il polipo il pappagallo il leone il gelato
			Tocca nel seguente ordine il polipo il pappagallo il leone gli occhiali da sole
			Tocca nel seguente ordine il polipo il pappagallo il leone la bicicletta
			Tocca nel seguente ordine il polipo il pappagallo il leone il monopattino
			Tocca nel seguente ordine il polipo il pappagallo il leone la bambina
			Tocca nel seguente ordine il polipo il pappagallo il leone l'ombrello
			Tocca nel seguente ordine il polipo il pappagallo la scimmia il leone
			Tocca nel seguente ordine il polipo il pappagallo la scimmia il gelato
			Tocca nel seguente ordine il polipo il pappagallo la scimmia gli occhiali da sole
			Tocca nel seguente ordine il polipo il pappagallo la scimmia la bicicletta
			Tocca nel seguente ordine il polipo il pappagallo la scimmia il monopattino
			Tocca nel seguente ordine il polipo il pappagallo la scimmia la bambina
			Tocca nel seguente ordine il polipo il pappagallo la scimmia l'ombrello
			Tocca nel seguente ordine il polipo il pappagallo il gelato la scimmia
			Tocca nel seguente ordine il polipo il pappagallo il gelato il leone
			Tocca nel seguente ordine il polipo il pappagallo il gelato gli occhiali da sole
			Tocca nel seguente ordine il polipo il pappagallo il gelato la bicicletta
			Tocca nel seguente ordine il polipo il pappagallo il gelato il monopattino
			Tocca nel seguente ordine il polipo il pappagallo il gelato la bambina
			Tocca nel seguente ordine il polipo il pappagallo il gelato l'ombrello
			Tocca nel seguente ordine il polipo il pappagallo gli occhiali da sole la scimmia
			Tocca nel seguente ordine il polipo il pappagallo gli occhiali da sole il gelato
			Tocca nel seguente ordine il polipo il pappagallo gli occhiali da sole il leone
			Tocca nel seguente ordine il polipo il pappagallo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il polipo il pappagallo gli occhiali da sole il monopattino
			Tocca nel seguente ordine il polipo il pappagallo gli occhiali da sole la bambina
			Tocca nel seguente ordine il polipo il pappagallo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il polipo il pappagallo la bicicletta la scimmia
			Tocca nel seguente ordine il polipo il pappagallo la bicicletta il gelato
			Tocca nel seguente ordine il polipo il pappagallo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il polipo il pappagallo la bicicletta il leone
			Tocca nel seguente ordine il polipo il pappagallo la bicicletta il monopattino
			Tocca nel seguente ordine il polipo il pappagallo la bicicletta la bambina
			Tocca nel seguente ordine il polipo il pappagallo la bicicletta l'ombrello
			Tocca nel seguente ordine il polipo il pappagallo il monopattino la scimmia
			Tocca nel seguente ordine il polipo il pappagallo il monopattino il gelato
			Tocca nel seguente ordine il polipo il pappagallo il monopattino gli occhiali da sole
			Tocca nel seguente ordine il polipo il pappagallo il monopattino la bicicletta
			Tocca nel seguente ordine il polipo il pappagallo il monopattino il leone
			Tocca nel seguente ordine il polipo il pappagallo il monopattino la bambina
			Tocca nel seguente ordine il polipo il pappagallo il monopattino l'ombrello
			Tocca nel seguente ordine il polipo il pappagallo la bambina la scimmia
			Tocca nel seguente ordine il polipo il pappagallo la bambina il gelato
			Tocca nel seguente ordine il polipo il pappagallo la bambina gli occhiali da sole
			Tocca nel seguente ordine il polipo il pappagallo la bambina la bicicletta
			Tocca nel seguente ordine il polipo il pappagallo la bambina il monopattino
			Tocca nel seguente ordine il polipo il pappagallo la bambina il leone
			Tocca nel seguente ordine il polipo il pappagallo la bambina l'ombrello
			Tocca nel seguente ordine il polipo il pappagallo l'ombrello la scimmia
			Tocca nel seguente ordine il polipo il pappagallo l'ombrello il gelato
			Tocca nel seguente ordine il polipo il pappagallo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il polipo il pappagallo l'ombrello la bicicletta
			Tocca nel seguente ordine il polipo il pappagallo l'ombrello il monopattino
			Tocca nel seguente ordine il polipo il pappagallo l'ombrello la bambina
			Tocca nel seguente ordine il polipo il pappagallo l'ombrello il leone
			Tocca nel seguente ordine il polipo il gelato il leone la scimmia
			Tocca nel seguente ordine il polipo il gelato il leone il pappagallo
			Tocca nel seguente ordine il polipo il gelato il leone gli occhiali da sole
			Tocca nel seguente ordine il polipo il gelato il leone la bicicletta
			Tocca nel seguente ordine il polipo il gelato il leone il monopattino
			Tocca nel seguente ordine il polipo il gelato il leone la bambina
			Tocca nel seguente ordine il polipo il gelato il leone l'ombrello
			Tocca nel seguente ordine il polipo il gelato la scimmia il leone
			Tocca nel seguente ordine il polipo il gelato la scimmia il pappagallo
			Tocca nel seguente ordine il polipo il gelato la scimmia gli occhiali da sole
			Tocca nel seguente ordine il polipo il gelato la scimmia la bicicletta
			Tocca nel seguente ordine il polipo il gelato la scimmia il monopattino
			Tocca nel seguente ordine il polipo il gelato la scimmia la bambina
			Tocca nel seguente ordine il polipo il gelato la scimmia l'ombrello
			Tocca nel seguente ordine il polipo il gelato il pappagallo la scimmia
			Tocca nel seguente ordine il polipo il gelato il pappagallo il leone
			Tocca nel seguente ordine il polipo il gelato il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il polipo il gelato il pappagallo la bicicletta
			Tocca nel seguente ordine il polipo il gelato il pappagallo il monopattino
			Tocca nel seguente ordine il polipo il gelato il pappagallo la bambina
			Tocca nel seguente ordine il polipo il gelato il pappagallo l'ombrello
			Tocca nel seguente ordine il polipo il gelato gli occhiali da sole la scimmia
			Tocca nel seguente ordine il polipo il gelato gli occhiali da sole il leone
			Tocca nel seguente ordine il polipo il gelato gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il polipo il gelato gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il polipo il gelato gli occhiali da sole il monopattino
			Tocca nel seguente ordine il polipo il gelato gli occhiali da sole la bambina
			Tocca nel seguente ordine il polipo il gelato gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il polipo il gelato la bicicletta la scimmia
			Tocca nel seguente ordine il polipo il gelato la bicicletta il leone
			Tocca nel seguente ordine il polipo il gelato la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il polipo il gelato la bicicletta il pappagallo
			Tocca nel seguente ordine il polipo il gelato la bicicletta il monopattino
			Tocca nel seguente ordine il polipo il gelato la bicicletta la bambina
			Tocca nel seguente ordine il polipo il gelato la bicicletta l'ombrello
			Tocca nel seguente ordine il polipo il gelato il monopattino la scimmia
			Tocca nel seguente ordine il polipo il gelato il monopattino il leone
			Tocca nel seguente ordine il polipo il gelato il monopattino gli occhiali da sole
			Tocca nel seguente ordine il polipo il gelato il monopattino la bicicletta
			Tocca nel seguente ordine il polipo il gelato il monopattino il pappagallo
			Tocca nel seguente ordine il polipo il gelato il monopattino la bambina
			Tocca nel seguente ordine il polipo il gelato il monopattino l'ombrello
			Tocca nel seguente ordine il polipo il gelato la bambina la scimmia
			Tocca nel seguente ordine il polipo il gelato la bambina il leone
			Tocca nel seguente ordine il polipo il gelato la bambina gli occhiali da sole
			Tocca nel seguente ordine il polipo il gelato la bambina la bicicletta
			Tocca nel seguente ordine il polipo il gelato la bambina il monopattino
			Tocca nel seguente ordine il polipo il gelato la bambina il pappagallo
			Tocca nel seguente ordine il polipo il gelato la bambina l'ombrello
			Tocca nel seguente ordine il polipo il gelato l'ombrello la scimmia
			Tocca nel seguente ordine il polipo il gelato l'ombrello il leone
			Tocca nel seguente ordine il polipo il gelato l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il polipo il gelato l'ombrello la bicicletta
			Tocca nel seguente ordine il polipo il gelato l'ombrello il monopattino
			Tocca nel seguente ordine il polipo il gelato l'ombrello la bambina
			Tocca nel seguente ordine il polipo il gelato l'ombrello il pappagallo
			Tocca nel seguente ordine il polipo gli occhiali da sole il leone la scimmia
			Tocca nel seguente ordine il polipo gli occhiali da sole il leone il gelato
			Tocca nel seguente ordine il polipo gli occhiali da sole il leone il pappagallo
			Tocca nel seguente ordine il polipo gli occhiali da sole il leone la bicicletta
			Tocca nel seguente ordine il polipo gli occhiali da sole il leone il monopattino
			Tocca nel seguente ordine il polipo gli occhiali da sole il leone la bambina
			Tocca nel seguente ordine il polipo gli occhiali da sole il leone l'ombrello
			Tocca nel seguente ordine il polipo gli occhiali da sole la scimmia il leone
			Tocca nel seguente ordine il polipo gli occhiali da sole la scimmia il gelato
			Tocca nel seguente ordine il polipo gli occhiali da sole la scimmia il pappagallo
			Tocca nel seguente ordine il polipo gli occhiali da sole la scimmia la bicicletta
			Tocca nel seguente ordine il polipo gli occhiali da sole la scimmia il monopattino
			Tocca nel seguente ordine il polipo gli occhiali da sole la scimmia la bambina
			Tocca nel seguente ordine il polipo gli occhiali da sole la scimmia l'ombrello
			Tocca nel seguente ordine il polipo gli occhiali da sole il pappagallo la scimmia
			Tocca nel seguente ordine il polipo gli occhiali da sole il pappagallo il gelato
			Tocca nel seguente ordine il polipo gli occhiali da sole il pappagallo il leone
			Tocca nel seguente ordine il polipo gli occhiali da sole il pappagallo la bicicletta
			Tocca nel seguente ordine il polipo gli occhiali da sole il pappagallo il monopattino
			Tocca nel seguente ordine il polipo gli occhiali da sole il pappagallo la bambina
			Tocca nel seguente ordine il polipo gli occhiali da sole il pappagallo l'ombrello
			Tocca nel seguente ordine il polipo gli occhiali da sole il gelato la scimmia
			Tocca nel seguente ordine il polipo gli occhiali da sole il gelato il pappagallo
			Tocca nel seguente ordine il polipo gli occhiali da sole il gelato il leone
			Tocca nel seguente ordine il polipo gli occhiali da sole il gelato la bicicletta
			Tocca nel seguente ordine il polipo gli occhiali da sole il gelato il monopattino
			Tocca nel seguente ordine il polipo gli occhiali da sole il gelato la bambina
			Tocca nel seguente ordine il polipo gli occhiali da sole il gelato l'ombrello
			Tocca nel seguente ordine il polipo gli occhiali da sole la bicicletta la scimmia
			Tocca nel seguente ordine il polipo gli occhiali da sole la bicicletta il gelato
			Tocca nel seguente ordine il polipo gli occhiali da sole la bicicletta il pappagallo
			Tocca nel seguente ordine il polipo gli occhiali da sole la bicicletta il leone
			Tocca nel seguente ordine il polipo gli occhiali da sole la bicicletta il monopattino
			Tocca nel seguente ordine il polipo gli occhiali da sole la bicicletta la bambina
			Tocca nel seguente ordine il polipo gli occhiali da sole la bicicletta l'ombrello
			Tocca nel seguente ordine il polipo gli occhiali da sole il monopattino la scimmia
			Tocca nel seguente ordine il polipo gli occhiali da sole il monopattino il gelato
			Tocca nel seguente ordine il polipo gli occhiali da sole il monopattino il pappagallo
			Tocca nel seguente ordine il polipo gli occhiali da sole il monopattino la bicicletta
			Tocca nel seguente ordine il polipo gli occhiali da sole il monopattino il leone
			Tocca nel seguente ordine il polipo gli occhiali da sole il monopattino la bambina
			Tocca nel seguente ordine il polipo gli occhiali da sole il monopattino l'ombrello
			Tocca nel seguente ordine il polipo gli occhiali da sole la bambina la scimmia
			Tocca nel seguente ordine il polipo gli occhiali da sole la bambina il gelato
			Tocca nel seguente ordine il polipo gli occhiali da sole la bambina il leone
			Tocca nel seguente ordine il polipo gli occhiali da sole la bambina la bicicletta
			Tocca nel seguente ordine il polipo gli occhiali da sole la bambina il monopattino
			Tocca nel seguente ordine il polipo gli occhiali da sole la bambina il leone
			Tocca nel seguente ordine il polipo gli occhiali da sole la bambina l'ombrello
			Tocca nel seguente ordine il polipo gli occhiali da sole l'ombrello la scimmia
			Tocca nel seguente ordine il polipo gli occhiali da sole l'ombrello il gelato
			Tocca nel seguente ordine il polipo gli occhiali da sole l'ombrello il pappagallo
			Tocca nel seguente ordine il polipo gli occhiali da sole l'ombrello la bicicletta
			Tocca nel seguente ordine il polipo gli occhiali da sole l'ombrello il monopattino
			Tocca nel seguente ordine il polipo gli occhiali da sole l'ombrello la bambina
			Tocca nel seguente ordine il polipo gli occhiali da sole l'ombrello il leone
			Tocca nel seguente ordine il polipo la bicicletta il leone la scimmia
			Tocca nel seguente ordine il polipo la bicicletta il leone il gelato
			Tocca nel seguente ordine il polipo la bicicletta il leone gli occhiali da sole
			Tocca nel seguente ordine il polipo la bicicletta il leone il pappagallo
			Tocca nel seguente ordine il polipo la bicicletta il leone il monopattino
			Tocca nel seguente ordine il polipo la bicicletta il leone la bambina
			Tocca nel seguente ordine il polipo la bicicletta il leone l'ombrello
			Tocca nel seguente ordine il polipo la bicicletta la scimmia il leone
			Tocca nel seguente ordine il polipo la bicicletta la scimmia il gelato
			Tocca nel seguente ordine il polipo la bicicletta la scimmia gli occhiali da sole
			Tocca nel seguente ordine il polipo la bicicletta la scimmia il pappagallo
			Tocca nel seguente ordine il polipo la bicicletta la scimmia il monopattino
			Tocca nel seguente ordine il polipo la bicicletta la scimmia la bambina
			Tocca nel seguente ordine il polipo la bicicletta la scimmia l'ombrello
			Tocca nel seguente ordine il polipo la bicicletta il pappagallo la scimmia
			Tocca nel seguente ordine il polipo la bicicletta il pappagallo il gelato
			Tocca nel seguente ordine il polipo la bicicletta il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il polipo la bicicletta il pappagallo il leone
			Tocca nel seguente ordine il polipo la bicicletta il pappagallo il monopattino
			Tocca nel seguente ordine il polipo la bicicletta il pappagallo la bambina
			Tocca nel seguente ordine il polipo la bicicletta il pappagallo l'ombrello
			Tocca nel seguente ordine il polipo la bicicletta il gelato la scimmia
			Tocca nel seguente ordine il polipo la bicicletta il gelato il leone
			Tocca nel seguente ordine il polipo la bicicletta il gelato gli occhiali da sole
			Tocca nel seguente ordine il polipo la bicicletta il gelato il pappagallo
			Tocca nel seguente ordine il polipo la bicicletta il gelato il monopattino
			Tocca nel seguente ordine il polipo la bicicletta il gelato la bambina
			Tocca nel seguente ordine il polipo la bicicletta il gelato l'ombrello
			Tocca nel seguente ordine il polipo la bicicletta gli occhiali da sole la scimmia
			Tocca nel seguente ordine il polipo la bicicletta gli occhiali da sole il gelato
			Tocca nel seguente ordine il polipo la bicicletta gli occhiali da sole il leone
			Tocca nel seguente ordine il polipo la bicicletta gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il polipo la bicicletta gli occhiali da sole il monopattino
			Tocca nel seguente ordine il polipo la bicicletta gli occhiali da sole la bambina
			Tocca nel seguente ordine il polipo la bicicletta gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il polipo la bicicletta il monopattino la scimmia
			Tocca nel seguente ordine il polipo la bicicletta il monopattino il gelato
			Tocca nel seguente ordine il polipo la bicicletta il monopattino gli occhiali da sole
			Tocca nel seguente ordine il polipo la bicicletta il monopattino il pappagallo
			Tocca nel seguente ordine il polipo la bicicletta il monopattino il leone
			Tocca nel seguente ordine il polipo la bicicletta il monopattino la bambina
			Tocca nel seguente ordine il polipo la bicicletta il monopattino l'ombrello
			Tocca nel seguente ordine il polipo la bicicletta la bambina la scimmia
			Tocca nel seguente ordine il polipo la bicicletta la bambina il gelato
			Tocca nel seguente ordine il polipo la bicicletta la bambina gli occhiali da sole
			Tocca nel seguente ordine il polipo la bicicletta la bambina il pappagallo
			Tocca nel seguente ordine il polipo la bicicletta la bambina il monopattino
			Tocca nel seguente ordine il polipo la bicicletta la bambina il leone
			Tocca nel seguente ordine il polipo la bicicletta la bambina l'ombrello
			Tocca nel seguente ordine il polipo la bicicletta l'ombrello la scimmia
			Tocca nel seguente ordine il polipo la bicicletta l'ombrello il gelato
			Tocca nel seguente ordine il polipo la bicicletta l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il polipo la bicicletta l'ombrello il pappagallo
			Tocca nel seguente ordine il polipo la bicicletta l'ombrello il monopattino
			Tocca nel seguente ordine il polipo la bicicletta l'ombrello la bambina
			Tocca nel seguente ordine il polipo la bicicletta l'ombrello il leone
			Tocca nel seguente ordine il polipo il monopattino il leone la scimmia
			Tocca nel seguente ordine il polipo il monopattino il leone il gelato
			Tocca nel seguente ordine il polipo il monopattino il leone gli occhiali da sole
			Tocca nel seguente ordine il polipo il monopattino il leone la bicicletta
			Tocca nel seguente ordine il polipo il monopattino il leone il pappagallo
			Tocca nel seguente ordine il polipo il monopattino il leone la bambina
			Tocca nel seguente ordine il polipo il monopattino il leone l'ombrello
			Tocca nel seguente ordine il polipo il monopattino la scimmia il leone
			Tocca nel seguente ordine il polipo il monopattino la scimmia il gelato
			Tocca nel seguente ordine il polipo il monopattino la scimmia gli occhiali da sole
			Tocca nel seguente ordine il polipo il monopattino la scimmia la bicicletta
			Tocca nel seguente ordine il polipo il monopattino la scimmia il pappagallo
			Tocca nel seguente ordine il polipo il monopattino la scimmia la bambina
			Tocca nel seguente ordine il polipo il monopattino la scimmia l'ombrello
			Tocca nel seguente ordine il polipo il monopattino il pappagallo la scimmia
			Tocca nel seguente ordine il polipo il monopattino il pappagallo il gelato
			Tocca nel seguente ordine il polipo il monopattino il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il polipo il monopattino il pappagallo la bicicletta
			Tocca nel seguente ordine il polipo il monopattino il pappagallo il leone
			Tocca nel seguente ordine il polipo il monopattino il pappagallo la bambina
			Tocca nel seguente ordine il polipo il monopattino il pappagallo l'ombrello
			Tocca nel seguente ordine il polipo il monopattino il gelato la scimmia
			Tocca nel seguente ordine il polipo il monopattino il gelato il leone
			Tocca nel seguente ordine il polipo il monopattino il gelato gli occhiali da sole
			Tocca nel seguente ordine il polipo il monopattino il gelato la bicicletta
			Tocca nel seguente ordine il polipo il monopattino il gelato il pappagallo
			Tocca nel seguente ordine il polipo il monopattino il gelato la bambina
			Tocca nel seguente ordine il polipo il monopattino il gelato l'ombrello
			Tocca nel seguente ordine il polipo il monopattino gli occhiali da sole la scimmia
			Tocca nel seguente ordine il polipo il monopattino gli occhiali da sole il gelato
			Tocca nel seguente ordine il polipo il monopattino gli occhiali da sole il leone
			Tocca nel seguente ordine il polipo il monopattino gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il polipo il monopattino gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il polipo il monopattino gli occhiali da sole la bambina
			Tocca nel seguente ordine il polipo il monopattino gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il polipo il monopattino la bicicletta la scimmia
			Tocca nel seguente ordine il polipo il monopattino la bicicletta il gelato
			Tocca nel seguente ordine il polipo il monopattino la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il polipo il monopattino la bicicletta il leone
			Tocca nel seguente ordine il polipo il monopattino la bicicletta il pappagallo
			Tocca nel seguente ordine il polipo il monopattino la bicicletta la bambina
			Tocca nel seguente ordine il polipo il monopattino la bicicletta l'ombrello
			Tocca nel seguente ordine il polipo il monopattino la bambina la scimmia
			Tocca nel seguente ordine il polipo il monopattino la bambina il gelato
			Tocca nel seguente ordine il polipo il monopattino la bambina gli occhiali da sole
			Tocca nel seguente ordine il polipo il monopattino la bambina la bicicletta
			Tocca nel seguente ordine il polipo il monopattino la bambina il pappagallo
			Tocca nel seguente ordine il polipo il monopattino la bambina il leone
			Tocca nel seguente ordine il polipo il monopattino la bambina l'ombrello
			Tocca nel seguente ordine il polipo il monopattino l'ombrello la scimmia
			Tocca nel seguente ordine il polipo il monopattino l'ombrello il gelato
			Tocca nel seguente ordine il polipo il monopattino l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il polipo il monopattino l'ombrello la bicicletta
			Tocca nel seguente ordine il polipo il monopattino l'ombrello il pappagallo
			Tocca nel seguente ordine il polipo il monopattino l'ombrello la bambina
			Tocca nel seguente ordine il polipo il monopattino l'ombrello il leone
			Tocca nel seguente ordine il polipo la bambina il leone la scimmia
			Tocca nel seguente ordine il polipo la bambina il leone il gelato
			Tocca nel seguente ordine il polipo la bambina il leone gli occhiali da sole
			Tocca nel seguente ordine il polipo la bambina il leone la bicicletta
			Tocca nel seguente ordine il polipo la bambina il leone il monopattino
			Tocca nel seguente ordine il polipo la bambina il leone il pappagallo
			Tocca nel seguente ordine il polipo la bambina il leone l'ombrello
			Tocca nel seguente ordine il polipo la bambina la scimmia il leone
			Tocca nel seguente ordine il polipo la bambina la scimmia il gelato
			Tocca nel seguente ordine il polipo la bambina la scimmia gli occhiali da sole
			Tocca nel seguente ordine il polipo la bambina la scimmia la bicicletta
			Tocca nel seguente ordine il polipo la bambina la scimmia il monopattino
			Tocca nel seguente ordine il polipo la bambina la scimmia il pappagallo
			Tocca nel seguente ordine il polipo la bambina la scimmia l'ombrello
			Tocca nel seguente ordine il polipo la bambina il pappagallo la scimmia
			Tocca nel seguente ordine il polipo la bambina il pappagallo il gelato
			Tocca nel seguente ordine il polipo la bambina il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il polipo la bambina il pappagallo la bicicletta
			Tocca nel seguente ordine il polipo la bambina il pappagallo il monopattino
			Tocca nel seguente ordine il polipo la bambina il pappagallo il leone
			Tocca nel seguente ordine il polipo la bambina il pappagallo l'ombrello
			Tocca nel seguente ordine il polipo la bambina il gelato la scimmia
			Tocca nel seguente ordine il polipo la bambina il gelato il leone
			Tocca nel seguente ordine il polipo la bambina il gelato gli occhiali da sole
			Tocca nel seguente ordine il polipo la bambina il gelato la bicicletta
			Tocca nel seguente ordine il polipo la bambina il gelato il monopattino
			Tocca nel seguente ordine il polipo la bambina il gelato il pappagallo
			Tocca nel seguente ordine il polipo la bambina il gelato l'ombrello
			Tocca nel seguente ordine il polipo la bambina gli occhiali da sole la scimmia
			Tocca nel seguente ordine il polipo la bambina gli occhiali da sole il gelato
			Tocca nel seguente ordine il polipo la bambina gli occhiali da sole il leone
			Tocca nel seguente ordine il polipo la bambina gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il polipo la bambina gli occhiali da sole il monopattino
			Tocca nel seguente ordine il polipo la bambina gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il polipo la bambina gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il polipo la bambina la bicicletta la scimmia
			Tocca nel seguente ordine il polipo la bambina la bicicletta il gelato
			Tocca nel seguente ordine il polipo la bambina la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il polipo la bambina la bicicletta il leone
			Tocca nel seguente ordine il polipo la bambina la bicicletta il monopattino
			Tocca nel seguente ordine il polipo la bambina la bicicletta il pappagallo
			Tocca nel seguente ordine il polipo la bambina la bicicletta l'ombrello
			Tocca nel seguente ordine il polipo la bambina il monopattino la scimmia
			Tocca nel seguente ordine il polipo la bambina il monopattino il gelato
			Tocca nel seguente ordine il polipo la bambina il monopattino gli occhiali da sole
			Tocca nel seguente ordine il polipo la bambina il monopattino la bicicletta
			Tocca nel seguente ordine il polipo la bambina il monopattino il leone
			Tocca nel seguente ordine il polipo la bambina il monopattino il pappagallo
			Tocca nel seguente ordine il polipo la bambina il monopattino l'ombrello
			Tocca nel seguente ordine il polipo la bambina l'ombrello la scimmia
			Tocca nel seguente ordine il polipo la bambina l'ombrello il gelato
			Tocca nel seguente ordine il polipo la bambina l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il polipo la bambina l'ombrello la bicicletta
			Tocca nel seguente ordine il polipo la bambina l'ombrello il monopattino
			Tocca nel seguente ordine il polipo la bambina l'ombrello il pappagallo
			Tocca nel seguente ordine il polipo la bambina l'ombrello il leone
			Tocca nel seguente ordine il polipo l'ombrello il leone la scimmia
			Tocca nel seguente ordine il polipo l'ombrello il leone il gelato
			Tocca nel seguente ordine il polipo l'ombrello il leone gli occhiali da sole
			Tocca nel seguente ordine il polipo l'ombrello il leone la bicicletta
			Tocca nel seguente ordine il polipo l'ombrello il leone il monopattino
			Tocca nel seguente ordine il polipo l'ombrello il leone la bambina
			Tocca nel seguente ordine il polipo l'ombrello il leone il pappagallo
			Tocca nel seguente ordine il polipo l'ombrello la scimmia il leone
			Tocca nel seguente ordine il polipo l'ombrello la scimmia il gelato
			Tocca nel seguente ordine il polipo l'ombrello la scimmia gli occhiali da sole
			Tocca nel seguente ordine il polipo l'ombrello la scimmia la bicicletta
			Tocca nel seguente ordine il polipo l'ombrello la scimmia il monopattino
			Tocca nel seguente ordine il polipo l'ombrello la scimmia la bambina
			Tocca nel seguente ordine il polipo l'ombrello la scimmia il pappagallo
			Tocca nel seguente ordine il polipo l'ombrello il pappagallo la scimmia
			Tocca nel seguente ordine il polipo l'ombrello il pappagallo il gelato
			Tocca nel seguente ordine il polipo l'ombrello il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il polipo l'ombrello il pappagallo la bicicletta
			Tocca nel seguente ordine il polipo l'ombrello il pappagallo il monopattino
			Tocca nel seguente ordine il polipo l'ombrello il pappagallo la bambina
			Tocca nel seguente ordine il polipo l'ombrello il pappagallo il leone
			Tocca nel seguente ordine il polipo l'ombrello il gelato la scimmia
			Tocca nel seguente ordine il polipo l'ombrello il gelato il leone
			Tocca nel seguente ordine il polipo l'ombrello il gelato gli occhiali da sole
			Tocca nel seguente ordine il polipo l'ombrello il gelato la bicicletta
			Tocca nel seguente ordine il polipo l'ombrello il gelato il monopattino
			Tocca nel seguente ordine il polipo l'ombrello il gelato la bambina
			Tocca nel seguente ordine il polipo l'ombrello il gelato il pappagallo
			Tocca nel seguente ordine il polipo l'ombrello gli occhiali da sole la scimmia
			Tocca nel seguente ordine il polipo l'ombrello gli occhiali da sole il gelato
			Tocca nel seguente ordine il polipo l'ombrello gli occhiali da sole il leone
			Tocca nel seguente ordine il polipo l'ombrello gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il polipo l'ombrello gli occhiali da sole il monopattino
			Tocca nel seguente ordine il polipo l'ombrello gli occhiali da sole la bambina
			Tocca nel seguente ordine il polipo l'ombrello gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il polipo l'ombrello la bicicletta la scimmia
			Tocca nel seguente ordine il polipo l'ombrello la bicicletta il gelato
			Tocca nel seguente ordine il polipo l'ombrello la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il polipo l'ombrello la bicicletta il leone
			Tocca nel seguente ordine il polipo l'ombrello la bicicletta il monopattino
			Tocca nel seguente ordine il polipo l'ombrello la bicicletta la bambina
			Tocca nel seguente ordine il polipo l'ombrello la bicicletta il pappagallo
			Tocca nel seguente ordine il polipo l'ombrello il monopattino la scimmia
			Tocca nel seguente ordine il polipo l'ombrello il monopattino il gelato
			Tocca nel seguente ordine il polipo l'ombrello il monopattino gli occhiali da sole
			Tocca nel seguente ordine il polipo l'ombrello il monopattino la bicicletta
			Tocca nel seguente ordine il polipo l'ombrello il monopattino il leone
			Tocca nel seguente ordine il polipo l'ombrello il monopattino la bambina
			Tocca nel seguente ordine il polipo l'ombrello il monopattino il pappagallo
			Tocca nel seguente ordine il polipo l'ombrello la bambina la scimmia
			Tocca nel seguente ordine il polipo l'ombrello la bambina il gelato
			Tocca nel seguente ordine il polipo l'ombrello la bambina gli occhiali da sole
			Tocca nel seguente ordine il polipo l'ombrello la bambina la bicicletta
			Tocca nel seguente ordine il polipo l'ombrello la bambina il monopattino
			Tocca nel seguente ordine il polipo l'ombrello la bambina il leone
			Tocca nel seguente ordine il polipo l'ombrello la bambina il pappagallo
			Tocca nel seguente ordine il gelato il leone la scimmia il pappagallo
			Tocca nel seguente ordine il gelato il leone la scimmia il polipo
			Tocca nel seguente ordine il gelato il leone la scimmia gli occhiali da sole
			Tocca nel seguente ordine il gelato il leone la scimmia la bicicletta
			Tocca nel seguente ordine il gelato il leone la scimmia il monopattino
			Tocca nel seguente ordine il gelato il leone la scimmia la bambina
			Tocca nel seguente ordine il gelato il leone la scimmia l'ombrello
			Tocca nel seguente ordine il gelato il leone il pappagallo la scimmia
			Tocca nel seguente ordine il gelato il leone il pappagallo il polipo
			Tocca nel seguente ordine il gelato il leone il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il gelato il leone il pappagallo la bicicletta
			Tocca nel seguente ordine il gelato il leone il pappagallo il leone
			Tocca nel seguente ordine il gelato il leone il pappagallo la bambina
			Tocca nel seguente ordine il gelato il leone il pappagallo l'ombrello
			Tocca nel seguente ordine il gelato il leone il polipo il pappagallo
			Tocca nel seguente ordine il gelato il leone il polipo la scimmia
			Tocca nel seguente ordine il gelato il leone il polipo gli occhiali da sole
			Tocca nel seguente ordine il gelato il leone il polipo la bicicletta
			Tocca nel seguente ordine il gelato il leone il polipo il monopattino
			Tocca nel seguente ordine il gelato il leone il polipo la bambina
			Tocca nel seguente ordine il gelato il leone il polipo l'ombrello
			Tocca nel seguente ordine il gelato il leone gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il gelato il leone gli occhiali da sole il polipo
			Tocca nel seguente ordine il gelato il leone gli occhiali da sole la scimmia
			Tocca nel seguente ordine il gelato il leone gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il gelato il leone gli occhiali da sole il monopattino
			Tocca nel seguente ordine il gelato il leone gli occhiali da sole la bambina
			Tocca nel seguente ordine il gelato il leone gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il gelato il leone la bicicletta il pappagallo
			Tocca nel seguente ordine il gelato il leone la bicicletta il polipo
			Tocca nel seguente ordine il gelato il leone la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il gelato il leone la bicicletta la scimmia
			Tocca nel seguente ordine il gelato il leone la bicicletta il monopattino
			Tocca nel seguente ordine il gelato il leone la bicicletta la bambina
			Tocca nel seguente ordine il gelato il leone la bicicletta l'ombrello
			Tocca nel seguente ordine il gelato il leone il monopattino il pappagallo
			Tocca nel seguente ordine il gelato il leone il monopattino il polipo
			Tocca nel seguente ordine il gelato il leone il monopattino gli occhiali da sole
			Tocca nel seguente ordine il gelato il leone il monopattino la bicicletta
			Tocca nel seguente ordine il gelato il leone il monopattino la scimmia
			Tocca nel seguente ordine il gelato il leone il monopattino la bambina
			Tocca nel seguente ordine il gelato il leone il monopattino l'ombrello
			Tocca nel seguente ordine il gelato il leone la bambina il pappagallo
			Tocca nel seguente ordine il gelato il leone la bambina il polipo
			Tocca nel seguente ordine il gelato il leone la bambina gli occhiali da sole
			Tocca nel seguente ordine il gelato il leone la bambina la bicicletta
			Tocca nel seguente ordine il gelato il leone la bambina il monopattino
			Tocca nel seguente ordine il gelato il leone la bambina la scimmia
			Tocca nel seguente ordine il gelato il leone la bambina l'ombrello
			Tocca nel seguente ordine il gelato il leone l'ombrello il pappagallo
			Tocca nel seguente ordine il gelato il leone l'ombrello il polipo
			Tocca nel seguente ordine il gelato il leone l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il gelato il leone l'ombrello la bicicletta
			Tocca nel seguente ordine il gelato il leone l'ombrello il monopattino
			Tocca nel seguente ordine il gelato il leone l'ombrello la bambina
			Tocca nel seguente ordine il gelato il leone l'ombrello la scimmia
			Tocca nel seguente ordine il gelato la scimmia il leone il pappagallo
			Tocca nel seguente ordine il gelato la scimmia il leone il polipo
			Tocca nel seguente ordine il gelato la scimmia il leone gli occhiali da sole
			Tocca nel seguente ordine il gelato la scimmia il leone la bicicletta
			Tocca nel seguente ordine il gelato la scimmia il leone il monopattino
			Tocca nel seguente ordine il gelato la scimmia il leone la bambina
			Tocca nel seguente ordine il gelato la scimmia il leone l'ombrello
			Tocca nel seguente ordine il gelato la scimmia il pappagallo il leone
			Tocca nel seguente ordine il gelato la scimmia il pappagallo il polipo
			Tocca nel seguente ordine il gelato la scimmia il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il gelato la scimmia il pappagallo la bicicletta
			Tocca nel seguente ordine il gelato la scimmia il pappagallo il monopattino
			Tocca nel seguente ordine il gelato la scimmia il pappagallo la bambina
			Tocca nel seguente ordine il gelato la scimmia il pappagallo l'ombrello
			Tocca nel seguente ordine il gelato la scimmia il polipo il leone
			Tocca nel seguente ordine il gelato la scimmia il polipo la scimmia
			Tocca nel seguente ordine il gelato la scimmia il polipo gli occhiali da sole
			Tocca nel seguente ordine il gelato la scimmia il polipo la bicicletta
			Tocca nel seguente ordine il gelato la scimmia il polipo il monopattino
			Tocca nel seguente ordine il gelato la scimmia il polipo la bambina
			Tocca nel seguente ordine il gelato la scimmia il polipo l'ombrello
			Tocca nel seguente ordine il gelato la scimmia gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il gelato la scimmia gli occhiali da sole il polipo
			Tocca nel seguente ordine il gelato la scimmia gli occhiali da sole il leone
			Tocca nel seguente ordine il gelato la scimmia gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il gelato la scimmia gli occhiali da sole il monopattino
			Tocca nel seguente ordine il gelato la scimmia gli occhiali da sole la bambina
			Tocca nel seguente ordine il gelato la scimmia gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il gelato la scimmia la bicicletta il pappagallo
			Tocca nel seguente ordine il gelato la scimmia la bicicletta il polipo
			Tocca nel seguente ordine il gelato la scimmia la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il gelato la scimmia la bicicletta il monopattino
			Tocca nel seguente ordine il gelato la scimmia la bicicletta il leone
			Tocca nel seguente ordine il gelato la scimmia la bicicletta la bambina
			Tocca nel seguente ordine il gelato la scimmia la bicicletta l'ombrello
			Tocca nel seguente ordine il gelato la scimmia il monopattino il pappagallo
			Tocca nel seguente ordine il gelato la scimmia il monopattino il polipo
			Tocca nel seguente ordine il gelato la scimmia il monopattino gli occhiali da sole
			Tocca nel seguente ordine il gelato la scimmia il monopattino la bicicletta
			Tocca nel seguente ordine il gelato la scimmia il monopattino il leone
			Tocca nel seguente ordine il gelato la scimmia il monopattino la bambina
			Tocca nel seguente ordine il gelato la scimmia il monopattino l'ombrello
			Tocca nel seguente ordine il gelato la scimmia la bambina il pappagallo
			Tocca nel seguente ordine il gelato la scimmia la bambina il polipo
			Tocca nel seguente ordine il gelato la scimmia la bambina gli occhiali da sole
			Tocca nel seguente ordine il gelato la scimmia la bambina la bicicletta
			Tocca nel seguente ordine il gelato la scimmia la bambina il leone
			Tocca nel seguente ordine il gelato la scimmia la bambina il monopattino
			Tocca nel seguente ordine il gelato la scimmia la bambina l'ombrello
			Tocca nel seguente ordine il gelato la scimmia l'ombrello il pappagallo
			Tocca nel seguente ordine il gelato la scimmia l'ombrello il polipo
			Tocca nel seguente ordine il gelato la scimmia l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il gelato la scimmia l'ombrello la bicicletta
			Tocca nel seguente ordine il gelato la scimmia l'ombrello il leone
			Tocca nel seguente ordine il gelato la scimmia l'ombrello la bambina
			Tocca nel seguente ordine il gelato la scimmia l'ombrello il monopattino
			Tocca nel seguente ordine il gelato il pappagallo il leone la scimmia
			Tocca nel seguente ordine il gelato il pappagallo il leone il polipo
			Tocca nel seguente ordine il gelato il pappagallo il leone gli occhiali da sole
			Tocca nel seguente ordine il gelato il pappagallo il leone la bicicletta
			Tocca nel seguente ordine il gelato il pappagallo il leone il monopattino
			Tocca nel seguente ordine il gelato il pappagallo il leone la bambina
			Tocca nel seguente ordine il gelato il pappagallo il leone l'ombrello
			Tocca nel seguente ordine il gelato il pappagallo la scimmia il polipo
			Tocca nel seguente ordine il gelato il pappagallo la scimmia gli occhiali da sole
			Tocca nel seguente ordine il gelato il pappagallo la scimmia la bicicletta
			Tocca nel seguente ordine il gelato il pappagallo la scimmia il monopattino
			Tocca nel seguente ordine il gelato il pappagallo la scimmia il leone
			Tocca nel seguente ordine il gelato il pappagallo la scimmia la bambina
			Tocca nel seguente ordine il gelato il pappagallo la scimmia l'ombrello
			Tocca nel seguente ordine il gelato il pappagallo il polipo il monopattino
			Tocca nel seguente ordine il gelato il pappagallo il polipo la scimmia
			Tocca nel seguente ordine il gelato il pappagallo il polipo gli occhiali da sole
			Tocca nel seguente ordine il gelato il pappagallo il polipo la bicicletta
			Tocca nel seguente ordine il gelato il pappagallo il polipo il leone
			Tocca nel seguente ordine il gelato il pappagallo il polipo la bambina
			Tocca nel seguente ordine il gelato il pappagallo il polipo l'ombrello
			Tocca nel seguente ordine il gelato il pappagallo gli occhiali da sole il monopattino
			Tocca nel seguente ordine il gelato il pappagallo gli occhiali da sole il polipo
			Tocca nel seguente ordine il gelato il pappagallo gli occhiali da sole la scimmia
			Tocca nel seguente ordine il gelato il pappagallo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il gelato il pappagallo gli occhiali da sole il leone
			Tocca nel seguente ordine il gelato il pappagallo gli occhiali da sole la bambina
			Tocca nel seguente ordine il gelato il pappagallo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il gelato il pappagallo la bicicletta il monopattino
			Tocca nel seguente ordine il gelato il pappagallo la bicicletta il polipo
			Tocca nel seguente ordine il gelato il pappagallo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il gelato il pappagallo la bicicletta la scimmia
			Tocca nel seguente ordine il gelato il pappagallo la bicicletta il leone
			Tocca nel seguente ordine il gelato il pappagallo la bicicletta la bambina
			Tocca nel seguente ordine il gelato il pappagallo la bicicletta l'ombrello
			Tocca nel seguente ordine il gelato il pappagallo il monopattino la scimmia
			Tocca nel seguente ordine il gelato il pappagallo il monopattino il polipo
			Tocca nel seguente ordine il gelato il pappagallo il monopattino gli occhiali da sole
			Tocca nel seguente ordine il gelato il pappagallo il monopattino la bicicletta
			Tocca nel seguente ordine il gelato il pappagallo il monopattino il leone
			Tocca nel seguente ordine il gelato il pappagallo il monopattino la bambina
			Tocca nel seguente ordine il gelato il pappagallo il monopattino l'ombrello
			Tocca nel seguente ordine il gelato il pappagallo la bambina la scimmia
			Tocca nel seguente ordine il gelato il pappagallo la bambina il polipo
			Tocca nel seguente ordine il gelato il pappagallo la bambina gli occhiali da sole
			Tocca nel seguente ordine il gelato il pappagallo la bambina la bicicletta
			Tocca nel seguente ordine il gelato il pappagallo la bambina il leone
			Tocca nel seguente ordine il gelato il pappagallo la bambina il monopattino
			Tocca nel seguente ordine il gelato il pappagallo la bambina l'ombrello
			Tocca nel seguente ordine il gelato il pappagallo l'ombrello la scimmia
			Tocca nel seguente ordine il gelato il pappagallo l'ombrello il polipo
			Tocca nel seguente ordine il gelato il pappagallo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il gelato il pappagallo l'ombrello la bicicletta
			Tocca nel seguente ordine il gelato il pappagallo l'ombrello il leone
			Tocca nel seguente ordine il gelato il pappagallo l'ombrello la bambina
			Tocca nel seguente ordine il gelato il pappagallo l'ombrello il monopattino
			Tocca nel seguente ordine il gelato il polipo il leone il pappagallo
			Tocca nel seguente ordine il gelato il polipo il leone il monopattino
			Tocca nel seguente ordine il gelato il polipo il leone gli occhiali da sole
			Tocca nel seguente ordine il gelato il polipo il leone la bicicletta
			Tocca nel seguente ordine il gelato il polipo il leone la scimmia
			Tocca nel seguente ordine il gelato il polipo il leone la bambina
			Tocca nel seguente ordine il gelato il polipo il leone l'ombrello
			Tocca nel seguente ordine il gelato il polipo la scimmia il pappagallo
			Tocca nel seguente ordine il gelato il polipo la scimmia il monopattino
			Tocca nel seguente ordine il gelato il polipo la scimmia gli occhiali da sole
			Tocca nel seguente ordine il gelato il polipo la scimmia la bicicletta
			Tocca nel seguente ordine il gelato il polipo la scimmia il leone
			Tocca nel seguente ordine il gelato il polipo la scimmia la bambina
			Tocca nel seguente ordine il gelato il polipo la scimmia l'ombrello
			Tocca nel seguente ordine il gelato il polipo il pappagallo la scimmia
			Tocca nel seguente ordine il gelato il polipo il pappagallo il monopattino
			Tocca nel seguente ordine il gelato il polipo il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il gelato il polipo il pappagallo la bicicletta
			Tocca nel seguente ordine il gelato il polipo il pappagallo il leone
			Tocca nel seguente ordine il gelato il polipo il pappagallo la bambina
			Tocca nel seguente ordine il gelato il polipo il pappagallo l'ombrello
			Tocca nel seguente ordine il gelato il polipo gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il gelato il polipo gli occhiali da sole il monopattino
			Tocca nel seguente ordine il gelato il polipo gli occhiali da sole la scimmia
			Tocca nel seguente ordine il gelato il polipo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il gelato il polipo gli occhiali da sole il leone
			Tocca nel seguente ordine il gelato il polipo gli occhiali da sole la bambina
			Tocca nel seguente ordine il gelato il polipo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il gelato il polipo la bicicletta il pappagallo
			Tocca nel seguente ordine il gelato il polipo la bicicletta il monopattino
			Tocca nel seguente ordine il gelato il polipo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il gelato il polipo la bicicletta la scimmia
			Tocca nel seguente ordine il gelato il polipo la bicicletta il leone
			Tocca nel seguente ordine il gelato il polipo la bicicletta la bambina
			Tocca nel seguente ordine il gelato il polipo la bicicletta l'ombrello
			Tocca nel seguente ordine il gelato il polipo il monopattino il pappagallo
			Tocca nel seguente ordine il gelato il polipo il monopattino la scimmia
			Tocca nel seguente ordine il gelato il polipo il monopattino gli occhiali da sole
			Tocca nel seguente ordine il gelato il polipo il monopattino la bicicletta
			Tocca nel seguente ordine il gelato il polipo il monopattino il leone
			Tocca nel seguente ordine il gelato il polipo il monopattino la bambina
			Tocca nel seguente ordine il gelato il polipo il monopattino l'ombrello
			Tocca nel seguente ordine il gelato il polipo la bambina il pappagallo
			Tocca nel seguente ordine il gelato il polipo la bambina il monopattino
			Tocca nel seguente ordine il gelato il polipo la bambina gli occhiali da sole
			Tocca nel seguente ordine il gelato il polipo la bambina la bicicletta
			Tocca nel seguente ordine il gelato il polipo la bambina il leone
			Tocca nel seguente ordine il gelato il polipo la bambina la scimmia
			Tocca nel seguente ordine il gelato il polipo la bambina l'ombrello
			Tocca nel seguente ordine il gelato il polipo l'ombrello il pappagallo
			Tocca nel seguente ordine il gelato il polipo l'ombrello il monopattino
			Tocca nel seguente ordine il gelato il polipo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il gelato il polipo l'ombrello la bicicletta
			Tocca nel seguente ordine il gelato il polipo l'ombrello il leone
			Tocca nel seguente ordine il gelato il polipo l'ombrello la bambina
			Tocca nel seguente ordine il gelato il polipo l'ombrello la scimmia
			Tocca nel seguente ordine il gelato gli occhiali da sole il leone il pappagallo
			Tocca nel seguente ordine il gelato gli occhiali da sole il leone la scimmia
			Tocca nel seguente ordine il gelato gli occhiali da sole il leone il monopattino
			Tocca nel seguente ordine il gelato gli occhiali da sole il leone la bicicletta
			Tocca nel seguente ordine il gelato gli occhiali da sole il leone il leone
			Tocca nel seguente ordine il gelato gli occhiali da sole il leone la bambina
			Tocca nel seguente ordine il gelato gli occhiali da sole il leone l'ombrello
			Tocca nel seguente ordine il gelato gli occhiali da sole la scimmia il pappagallo
			Tocca nel seguente ordine il gelato gli occhiali da sole la scimmia il polipo
			Tocca nel seguente ordine il gelato gli occhiali da sole la scimmia il monopattino
			Tocca nel seguente ordine il gelato gli occhiali da sole la scimmia la bicicletta
			Tocca nel seguente ordine il gelato gli occhiali da sole la scimmia il leone
			Tocca nel seguente ordine il gelato gli occhiali da sole la scimmia la bambina
			Tocca nel seguente ordine il gelato gli occhiali da sole la scimmia l'ombrello
			Tocca nel seguente ordine il gelato gli occhiali da sole il pappagallo la scimmia
			Tocca nel seguente ordine il gelato gli occhiali da sole il pappagallo il polipo
			Tocca nel seguente ordine il gelato gli occhiali da sole il pappagallo il monopattino
			Tocca nel seguente ordine il gelato gli occhiali da sole il pappagallo la bicicletta
			Tocca nel seguente ordine il gelato gli occhiali da sole il pappagallo il leone
			Tocca nel seguente ordine il gelato gli occhiali da sole il pappagallo la bambina
			Tocca nel seguente ordine il gelato gli occhiali da sole il pappagallo l'ombrello
			Tocca nel seguente ordine il gelato gli occhiali da sole il polipo il pappagallo
			Tocca nel seguente ordine il gelato gli occhiali da sole il polipo il monopattino
			Tocca nel seguente ordine il gelato gli occhiali da sole il polipo la scimmia
			Tocca nel seguente ordine il gelato gli occhiali da sole il polipo la bicicletta
			Tocca nel seguente ordine il gelato gli occhiali da sole il polipo il leone
			Tocca nel seguente ordine il gelato gli occhiali da sole il polipo la bambina
			Tocca nel seguente ordine il gelato gli occhiali da sole il polipo l'ombrello
			Tocca nel seguente ordine il gelato gli occhiali da sole la bicicletta il pappagallo
			Tocca nel seguente ordine il gelato gli occhiali da sole la bicicletta il polipo
			Tocca nel seguente ordine il gelato gli occhiali da sole la bicicletta la scimmia
			Tocca nel seguente ordine il gelato gli occhiali da sole la bicicletta il monopattino
			Tocca nel seguente ordine il gelato gli occhiali da sole la bicicletta il leone
			Tocca nel seguente ordine il gelato gli occhiali da sole la bicicletta la bambina
			Tocca nel seguente ordine il gelato gli occhiali da sole la bicicletta l'ombrello
			Tocca nel seguente ordine il gelato gli occhiali da sole il monopattino il pappagallo
			Tocca nel seguente ordine il gelato gli occhiali da sole il monopattino il polipo
			Tocca nel seguente ordine il gelato gli occhiali da sole il monopattino la scimmia
			Tocca nel seguente ordine il gelato gli occhiali da sole il monopattino la bicicletta
			Tocca nel seguente ordine il gelato gli occhiali da sole il monopattino il leone
			Tocca nel seguente ordine il gelato gli occhiali da sole il monopattino la bambina
			Tocca nel seguente ordine il gelato gli occhiali da sole il monopattino l'ombrello
			Tocca nel seguente ordine il gelato gli occhiali da sole la bambina il pappagallo
			Tocca nel seguente ordine il gelato gli occhiali da sole la bambina il polipo
			Tocca nel seguente ordine il gelato gli occhiali da sole la bambina la scimmia
			Tocca nel seguente ordine il gelato gli occhiali da sole la bambina la bicicletta
			Tocca nel seguente ordine il gelato gli occhiali da sole la bambina il leone
			Tocca nel seguente ordine il gelato gli occhiali da sole la bambina il monopattino
			Tocca nel seguente ordine il gelato gli occhiali da sole la bambina l'ombrello
			Tocca nel seguente ordine il gelato gli occhiali da sole l'ombrello il pappagallo
			Tocca nel seguente ordine il gelato gli occhiali da sole l'ombrello il polipo
			Tocca nel seguente ordine il gelato gli occhiali da sole l'ombrello la scimmia
			Tocca nel seguente ordine il gelato gli occhiali da sole l'ombrello la bicicletta
			Tocca nel seguente ordine il gelato gli occhiali da sole l'ombrello il leone
			Tocca nel seguente ordine il gelato gli occhiali da sole l'ombrello la bambina
			Tocca nel seguente ordine il gelato gli occhiali da sole l'ombrello il monopattino
			Tocca nel seguente ordine il gelato la bicicletta il leone il pappagallo
			Tocca nel seguente ordine il gelato la bicicletta il leone il polipo
			Tocca nel seguente ordine il gelato la bicicletta il leone gli occhiali da sole
			Tocca nel seguente ordine il gelato la bicicletta il leone il monopattino
			Tocca nel seguente ordine il gelato la bicicletta il leone la scimmia
			Tocca nel seguente ordine il gelato la bicicletta il leone la bambina
			Tocca nel seguente ordine il gelato la bicicletta il leone l'ombrello
			Tocca nel seguente ordine il gelato la bicicletta la scimmia il pappagallo
			Tocca nel seguente ordine il gelato la bicicletta la scimmia il polipo
			Tocca nel seguente ordine il gelato la bicicletta la scimmia gli occhiali da sole
			Tocca nel seguente ordine il gelato la bicicletta la scimmia il monopattino
			Tocca nel seguente ordine il gelato la bicicletta la scimmia il leone
			Tocca nel seguente ordine il gelato la bicicletta la scimmia la bambina
			Tocca nel seguente ordine il gelato la bicicletta la scimmia l'ombrello
			Tocca nel seguente ordine il gelato la bicicletta il pappagallo la scimmia
			Tocca nel seguente ordine il gelato la bicicletta il pappagallo il polipo
			Tocca nel seguente ordine il gelato la bicicletta il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il gelato la bicicletta il pappagallo il monopattino
			Tocca nel seguente ordine il gelato la bicicletta il pappagallo il leone
			Tocca nel seguente ordine il gelato la bicicletta il pappagallo la bambina
			Tocca nel seguente ordine il gelato la bicicletta il pappagallo l'ombrello
			Tocca nel seguente ordine il gelato la bicicletta il polipo il pappagallo
			Tocca nel seguente ordine il gelato la bicicletta il polipo la scimmia
			Tocca nel seguente ordine il gelato la bicicletta il polipo gli occhiali da sole
			Tocca nel seguente ordine il gelato la bicicletta il polipo il monopattino
			Tocca nel seguente ordine il gelato la bicicletta il polipo il leone
			Tocca nel seguente ordine il gelato la bicicletta il polipo la bambina
			Tocca nel seguente ordine il gelato la bicicletta il polipo l'ombrello
			Tocca nel seguente ordine il gelato la bicicletta gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il gelato la bicicletta gli occhiali da sole il polipo
			Tocca nel seguente ordine il gelato la bicicletta gli occhiali da sole la scimmia
			Tocca nel seguente ordine il gelato la bicicletta gli occhiali da sole il monopattino
			Tocca nel seguente ordine il gelato la bicicletta gli occhiali da sole il leone
			Tocca nel seguente ordine il gelato la bicicletta gli occhiali da sole la bambina
			Tocca nel seguente ordine il gelato la bicicletta gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il gelato la bicicletta il monopattino il pappagallo
			Tocca nel seguente ordine il gelato la bicicletta il monopattino il polipo
			Tocca nel seguente ordine il gelato la bicicletta il monopattino gli occhiali da sole
			Tocca nel seguente ordine il gelato la bicicletta il monopattino la scimmia
			Tocca nel seguente ordine il gelato la bicicletta il monopattino il leone
			Tocca nel seguente ordine il gelato la bicicletta il monopattino la bambina
			Tocca nel seguente ordine il gelato la bicicletta il monopattino l'ombrello
			Tocca nel seguente ordine il gelato la bicicletta la bambina il pappagallo
			Tocca nel seguente ordine il gelato la bicicletta la bambina il polipo
			Tocca nel seguente ordine il gelato la bicicletta la bambina gli occhiali da sole
			Tocca nel seguente ordine il gelato la bicicletta la bambina il monopattino
			Tocca nel seguente ordine il gelato la bicicletta la bambina il leone
			Tocca nel seguente ordine il gelato la bicicletta la bambina la scimmia
			Tocca nel seguente ordine il gelato la bicicletta la bambina l'ombrello
			Tocca nel seguente ordine il gelato la bicicletta l'ombrello il pappagallo
			Tocca nel seguente ordine il gelato la bicicletta l'ombrello il polipo
			Tocca nel seguente ordine il gelato la bicicletta l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il gelato la bicicletta l'ombrello il monopattino
			Tocca nel seguente ordine il gelato la bicicletta l'ombrello il leone
			Tocca nel seguente ordine il gelato la bicicletta l'ombrello la bambina
			Tocca nel seguente ordine il gelato la bicicletta l'ombrello la scimmia
			Tocca nel seguente ordine il gelato il monopattino il leone il pappagallo
			Tocca nel seguente ordine il gelato il monopattino il leone il polipo
			Tocca nel seguente ordine il gelato il monopattino il leone gli occhiali da sole
			Tocca nel seguente ordine il gelato il monopattino il leone la bicicletta
			Tocca nel seguente ordine il gelato il monopattino il leone la scimmia
			Tocca nel seguente ordine il gelato il monopattino il leone la bambina
			Tocca nel seguente ordine il gelato il monopattino il leone l'ombrello
			Tocca nel seguente ordine il gelato il monopattino la scimmia il pappagallo
			Tocca nel seguente ordine il gelato il monopattino la scimmia il polipo
			Tocca nel seguente ordine il gelato il monopattino la scimmia gli occhiali da sole
			Tocca nel seguente ordine il gelato il monopattino la scimmia la bicicletta
			Tocca nel seguente ordine il gelato il monopattino la scimmia il leone
			Tocca nel seguente ordine il gelato il monopattino la scimmia la bambina
			Tocca nel seguente ordine il gelato il monopattino la scimmia l'ombrello
			Tocca nel seguente ordine il gelato il monopattino il pappagallo la scimmia
			Tocca nel seguente ordine il gelato il monopattino il pappagallo il polipo
			Tocca nel seguente ordine il gelato il monopattino il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il gelato il monopattino il pappagallo la bicicletta
			Tocca nel seguente ordine il gelato il monopattino il pappagallo il leone
			Tocca nel seguente ordine il gelato il monopattino il pappagallo la bambina
			Tocca nel seguente ordine il gelato il monopattino il pappagallo l'ombrello
			Tocca nel seguente ordine il gelato il monopattino il polipo il pappagallo
			Tocca nel seguente ordine il gelato il monopattino il polipo la scimmia
			Tocca nel seguente ordine il gelato il monopattino il polipo gli occhiali da sole
			Tocca nel seguente ordine il gelato il monopattino il polipo la bicicletta
			Tocca nel seguente ordine il gelato il monopattino il polipo il leone
			Tocca nel seguente ordine il gelato il monopattino il polipo la bambina
			Tocca nel seguente ordine il gelato il monopattino il polipo l'ombrello
			Tocca nel seguente ordine il gelato il monopattino gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il gelato il monopattino gli occhiali da sole il polipo
			Tocca nel seguente ordine il gelato il monopattino gli occhiali da sole la scimmia
			Tocca nel seguente ordine il gelato il monopattino gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il gelato il monopattino gli occhiali da sole il leone
			Tocca nel seguente ordine il gelato il monopattino gli occhiali da sole la bambina
			Tocca nel seguente ordine il gelato il monopattino gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il gelato il monopattino la bicicletta il pappagallo
			Tocca nel seguente ordine il gelato il monopattino la bicicletta il polipo
			Tocca nel seguente ordine il gelato il monopattino la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il gelato il monopattino la bicicletta la scimmia
			Tocca nel seguente ordine il gelato il monopattino la bicicletta il leone
			Tocca nel seguente ordine il gelato il monopattino la bicicletta la bambina
			Tocca nel seguente ordine il gelato il monopattino la bicicletta l'ombrello
			Tocca nel seguente ordine il gelato il monopattino la bambina il pappagallo
			Tocca nel seguente ordine il gelato il monopattino la bambina il polipo
			Tocca nel seguente ordine il gelato il monopattino la bambina gli occhiali da sole
			Tocca nel seguente ordine il gelato il monopattino la bambina la bicicletta
			Tocca nel seguente ordine il gelato il monopattino la bambina il leone
			Tocca nel seguente ordine il gelato il monopattino la bambina la scimmia
			Tocca nel seguente ordine il gelato il monopattino la bambina l'ombrello
			Tocca nel seguente ordine il gelato il monopattino l'ombrello il pappagallo
			Tocca nel seguente ordine il gelato il monopattino l'ombrello il polipo
			Tocca nel seguente ordine il gelato il monopattino l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il gelato il monopattino l'ombrello la bicicletta
			Tocca nel seguente ordine il gelato il monopattino l'ombrello il leone
			Tocca nel seguente ordine il gelato il monopattino l'ombrello la bambina
			Tocca nel seguente ordine il gelato il monopattino l'ombrello la scimmia
			Tocca nel seguente ordine il gelato la bambina il leone il pappagallo
			Tocca nel seguente ordine il gelato la bambina il leone il polipo
			Tocca nel seguente ordine il gelato la bambina il leone gli occhiali da sole
			Tocca nel seguente ordine il gelato la bambina il leone la bicicletta
			Tocca nel seguente ordine il gelato la bambina il leone il monopattino
			Tocca nel seguente ordine il gelato la bambina il leone la scimmia
			Tocca nel seguente ordine il gelato la bambina il leone l'ombrello
			Tocca nel seguente ordine il gelato la bambina la scimmia il pappagallo
			Tocca nel seguente ordine il gelato la bambina la scimmia il polipo
			Tocca nel seguente ordine il gelato la bambina la scimmia gli occhiali da sole
			Tocca nel seguente ordine il gelato la bambina la scimmia la bicicletta
			Tocca nel seguente ordine il gelato la bambina la scimmia il leone
			Tocca nel seguente ordine il gelato la bambina la scimmia il monopattino
			Tocca nel seguente ordine il gelato la bambina la scimmia l'ombrello
			Tocca nel seguente ordine il gelato la bambina il pappagallo la scimmia
			Tocca nel seguente ordine il gelato la bambina il pappagallo il polipo
			Tocca nel seguente ordine il gelato la bambina il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il gelato la bambina il pappagallo la bicicletta
			Tocca nel seguente ordine il gelato la bambina il pappagallo il leone
			Tocca nel seguente ordine il gelato la bambina il pappagallo il monopattino
			Tocca nel seguente ordine il gelato la bambina il pappagallo l'ombrello
			Tocca nel seguente ordine il gelato la bambina il polipo il pappagallo
			Tocca nel seguente ordine il gelato la bambina il polipo la scimmia
			Tocca nel seguente ordine il gelato la bambina il polipo gli occhiali da sole
			Tocca nel seguente ordine il gelato la bambina il polipo la bicicletta
			Tocca nel seguente ordine il gelato la bambina il polipo il leone
			Tocca nel seguente ordine il gelato la bambina il polipo il monopattino
			Tocca nel seguente ordine il gelato la bambina il polipo l'ombrello
			Tocca nel seguente ordine il gelato la bambina gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il gelato la bambina gli occhiali da sole il polipo
			Tocca nel seguente ordine il gelato la bambina gli occhiali da sole la scimmia
			Tocca nel seguente ordine il gelato la bambina gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il gelato la bambina gli occhiali da sole il leone
			Tocca nel seguente ordine il gelato la bambina gli occhiali da sole il monopattino
			Tocca nel seguente ordine il gelato la bambina gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il gelato la bambina la bicicletta il pappagallo
			Tocca nel seguente ordine il gelato la bambina la bicicletta il polipo
			Tocca nel seguente ordine il gelato la bambina la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il gelato la bambina la bicicletta la scimmia
			Tocca nel seguente ordine il gelato la bambina la bicicletta il leone
			Tocca nel seguente ordine il gelato la bambina la bicicletta il monopattino
			Tocca nel seguente ordine il gelato la bambina la bicicletta l'ombrello
			Tocca nel seguente ordine il gelato la bambina il monopattino il pappagallo
			Tocca nel seguente ordine il gelato la bambina il monopattino il polipo
			Tocca nel seguente ordine il gelato la bambina il monopattino gli occhiali da sole
			Tocca nel seguente ordine il gelato la bambina il monopattino la bicicletta
			Tocca nel seguente ordine il gelato la bambina il monopattino il leone
			Tocca nel seguente ordine il gelato la bambina il monopattino la scimmia
			Tocca nel seguente ordine il gelato la bambina il monopattino l'ombrello
			Tocca nel seguente ordine il gelato la bambina l'ombrello il pappagallo
			Tocca nel seguente ordine il gelato la bambina l'ombrello il polipo
			Tocca nel seguente ordine il gelato la bambina l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il gelato la bambina l'ombrello la bicicletta
			Tocca nel seguente ordine il gelato la bambina l'ombrello il leone
			Tocca nel seguente ordine il gelato la bambina l'ombrello il monopattino
			Tocca nel seguente ordine il gelato la bambina l'ombrello la scimmia
			Tocca nel seguente ordine il gelato l'ombrello il leone il pappagallo
			Tocca nel seguente ordine il gelato l'ombrello il leone il polipo
			Tocca nel seguente ordine il gelato l'ombrello il leone gli occhiali da sole
			Tocca nel seguente ordine il gelato l'ombrello il leone la bicicletta
			Tocca nel seguente ordine il gelato l'ombrello il leone la scimmia
			Tocca nel seguente ordine il gelato l'ombrello il leone la bambina
			Tocca nel seguente ordine il gelato l'ombrello il leone il monopattino
			Tocca nel seguente ordine il gelato l'ombrello la scimmia il pappagallo
			Tocca nel seguente ordine il gelato l'ombrello la scimmia il polipo
			Tocca nel seguente ordine il gelato l'ombrello la scimmia gli occhiali da sole
			Tocca nel seguente ordine il gelato l'ombrello la scimmia la bicicletta
			Tocca nel seguente ordine il gelato l'ombrello la scimmia il leone
			Tocca nel seguente ordine il gelato l'ombrello la scimmia la bambina
			Tocca nel seguente ordine il gelato l'ombrello la scimmia il monopattino
			Tocca nel seguente ordine il gelato l'ombrello il pappagallo la scimmia
			Tocca nel seguente ordine il gelato l'ombrello il pappagallo il polipo
			Tocca nel seguente ordine il gelato l'ombrello il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il gelato l'ombrello il pappagallo la bicicletta
			Tocca nel seguente ordine il gelato l'ombrello il pappagallo il leone
			Tocca nel seguente ordine il gelato l'ombrello il pappagallo la bambina
			Tocca nel seguente ordine il gelato l'ombrello il pappagallo il monopattino
			Tocca nel seguente ordine il gelato l'ombrello il polipo il pappagallo
			Tocca nel seguente ordine il gelato l'ombrello il polipo la scimmia
			Tocca nel seguente ordine il gelato l'ombrello il polipo gli occhiali da sole
			Tocca nel seguente ordine il gelato l'ombrello il polipo la bicicletta
			Tocca nel seguente ordine il gelato l'ombrello il polipo il leone
			Tocca nel seguente ordine il gelato l'ombrello il polipo la bambina
			Tocca nel seguente ordine il gelato l'ombrello il polipo il monopattino
			Tocca nel seguente ordine il gelato l'ombrello gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il gelato l'ombrello gli occhiali da sole il polipo
			Tocca nel seguente ordine il gelato l'ombrello gli occhiali da sole la scimmia
			Tocca nel seguente ordine il gelato l'ombrello gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il gelato l'ombrello gli occhiali da sole il leone
			Tocca nel seguente ordine il gelato l'ombrello gli occhiali da sole la bambina
			Tocca nel seguente ordine il gelato l'ombrello gli occhiali da sole il monopattino
			Tocca nel seguente ordine il gelato l'ombrello la bicicletta il pappagallo
			Tocca nel seguente ordine il gelato l'ombrello la bicicletta il polipo
			Tocca nel seguente ordine il gelato l'ombrello la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il gelato l'ombrello la bicicletta la scimmia
			Tocca nel seguente ordine il gelato l'ombrello la bicicletta il leone
			Tocca nel seguente ordine il gelato l'ombrello la bicicletta la bambina
			Tocca nel seguente ordine il gelato l'ombrello la bicicletta il monopattino
			Tocca nel seguente ordine il gelato l'ombrello il monopattino il pappagallo
			Tocca nel seguente ordine il gelato l'ombrello il monopattino il polipo
			Tocca nel seguente ordine il gelato l'ombrello il monopattino gli occhiali da sole
			Tocca nel seguente ordine il gelato l'ombrello il monopattino la bicicletta
			Tocca nel seguente ordine il gelato l'ombrello il monopattino il leone
			Tocca nel seguente ordine il gelato l'ombrello il monopattino la bambina
			Tocca nel seguente ordine il gelato l'ombrello il monopattino la scimmia
			Tocca nel seguente ordine il gelato l'ombrello la bambina il pappagallo
			Tocca nel seguente ordine il gelato l'ombrello la bambina il polipo
			Tocca nel seguente ordine il gelato l'ombrello la bambina gli occhiali da sole
			Tocca nel seguente ordine il gelato l'ombrello la bambina la bicicletta
			Tocca nel seguente ordine il gelato l'ombrello la bambina il leone
			Tocca nel seguente ordine il gelato l'ombrello la bambina la scimmia
			Tocca nel seguente ordine il gelato l'ombrello la bambina il monopattino
			Tocca nel seguente ordine gli occhiali da sole il leone la scimmia il gelato
			Tocca nel seguente ordine gli occhiali da sole il leone la scimmia il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il leone la scimmia il polipo
			Tocca nel seguente ordine gli occhiali da sole il leone la scimmia la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il leone la scimmia il monopattino
			Tocca nel seguente ordine gli occhiali da sole il leone la scimmia la bambina
			Tocca nel seguente ordine gli occhiali da sole il leone la scimmia l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il leone il pappagallo il gelato
			Tocca nel seguente ordine gli occhiali da sole il leone il pappagallo la scimmia
			Tocca nel seguente ordine gli occhiali da sole il leone il pappagallo il polipo
			Tocca nel seguente ordine gli occhiali da sole il leone il pappagallo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il leone il pappagallo il monopattino
			Tocca nel seguente ordine gli occhiali da sole il leone il pappagallo la bambina
			Tocca nel seguente ordine gli occhiali da sole il leone il pappagallo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il leone il polipo il gelato
			Tocca nel seguente ordine gli occhiali da sole il leone il polipo il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il leone il polipo la scimmia
			Tocca nel seguente ordine gli occhiali da sole il leone il polipo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il leone il polipo il monopattino
			Tocca nel seguente ordine gli occhiali da sole il leone il polipo la bambina
			Tocca nel seguente ordine gli occhiali da sole il leone il polipo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il leone il gelato la scimmia
			Tocca nel seguente ordine gli occhiali da sole il leone il gelato il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il leone il gelato il polipo
			Tocca nel seguente ordine gli occhiali da sole il leone il gelato la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il leone il gelato il monopattino
			Tocca nel seguente ordine gli occhiali da sole il leone il gelato la bambina
			Tocca nel seguente ordine gli occhiali da sole il leone il gelato l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il leone la bicicletta il gelato
			Tocca nel seguente ordine gli occhiali da sole il leone la bicicletta il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il leone la bicicletta il polipo
			Tocca nel seguente ordine gli occhiali da sole il leone la bicicletta la scimmia
			Tocca nel seguente ordine gli occhiali da sole il leone la bicicletta il monopattino
			Tocca nel seguente ordine gli occhiali da sole il leone la bicicletta la bambina
			Tocca nel seguente ordine gli occhiali da sole il leone la bicicletta l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il leone il monopattino il gelato
			Tocca nel seguente ordine gli occhiali da sole il leone il monopattino il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il leone il monopattino il polipo
			Tocca nel seguente ordine gli occhiali da sole il leone il monopattino la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il leone il monopattino la scimmia
			Tocca nel seguente ordine gli occhiali da sole il leone il monopattino la bambina
			Tocca nel seguente ordine gli occhiali da sole il leone il monopattino l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il leone la bambina il gelato
			Tocca nel seguente ordine gli occhiali da sole il leone la bambina il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il leone la bambina il polipo
			Tocca nel seguente ordine gli occhiali da sole il leone la bambina la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il leone la bambina il monopattino
			Tocca nel seguente ordine gli occhiali da sole il leone la bambina la scimmia
			Tocca nel seguente ordine gli occhiali da sole il leone la bambina l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il leone l'ombrello il gelato
			Tocca nel seguente ordine gli occhiali da sole il leone l'ombrello il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il leone l'ombrello il polipo
			Tocca nel seguente ordine gli occhiali da sole il leone l'ombrello la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il leone l'ombrello il monopattino
			Tocca nel seguente ordine gli occhiali da sole il leone l'ombrello la bambina
			Tocca nel seguente ordine gli occhiali da sole il leone l'ombrello la scimmia
			Tocca nel seguente ordine gli occhiali da sole la scimmia il leone il gelato
			Tocca nel seguente ordine gli occhiali da sole la scimmia il leone il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la scimmia il leone il polipo
			Tocca nel seguente ordine gli occhiali da sole la scimmia il leone la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la scimmia il leone il monopattino
			Tocca nel seguente ordine gli occhiali da sole la scimmia il leone la bambina
			Tocca nel seguente ordine gli occhiali da sole la scimmia il leone l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la scimmia il pappagallo il gelato
			Tocca nel seguente ordine gli occhiali da sole la scimmia il pappagallo il leone
			Tocca nel seguente ordine gli occhiali da sole la scimmia il pappagallo il polipo
			Tocca nel seguente ordine gli occhiali da sole la scimmia il pappagallo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la scimmia il pappagallo il monopattino
			Tocca nel seguente ordine gli occhiali da sole la scimmia il pappagallo la bambina
			Tocca nel seguente ordine gli occhiali da sole la scimmia il pappagallo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la scimmia il polipo il gelato
			Tocca nel seguente ordine gli occhiali da sole la scimmia il polipo il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la scimmia il polipo il leone
			Tocca nel seguente ordine gli occhiali da sole la scimmia il polipo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la scimmia il polipo il monopattino
			Tocca nel seguente ordine gli occhiali da sole la scimmia il polipo la bambina
			Tocca nel seguente ordine gli occhiali da sole la scimmia il polipo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la scimmia il gelato il leone
			Tocca nel seguente ordine gli occhiali da sole la scimmia il gelato il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la scimmia il gelato il polipo
			Tocca nel seguente ordine gli occhiali da sole la scimmia il gelato la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la scimmia il gelato il monopattino
			Tocca nel seguente ordine gli occhiali da sole la scimmia il gelato la bambina
			Tocca nel seguente ordine gli occhiali da sole la scimmia il gelato l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bicicletta il gelato
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bicicletta il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bicicletta il polipo
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bicicletta il leone
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bicicletta il monopattino
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bicicletta la bambina
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bicicletta l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la scimmia il monopattino il gelato
			Tocca nel seguente ordine gli occhiali da sole la scimmia il monopattino il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la scimmia il monopattino il polipo
			Tocca nel seguente ordine gli occhiali da sole la scimmia il monopattino la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la scimmia il monopattino il leone
			Tocca nel seguente ordine gli occhiali da sole la scimmia il monopattino la bambina
			Tocca nel seguente ordine gli occhiali da sole la scimmia il monopattino l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bambina il gelato
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bambina il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bambina il polipo
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bambina la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bambina il monopattino
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bambina il leone
			Tocca nel seguente ordine gli occhiali da sole la scimmia la bambina l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la scimmia l'ombrello il gelato
			Tocca nel seguente ordine gli occhiali da sole la scimmia l'ombrello il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la scimmia l'ombrello il polipo
			Tocca nel seguente ordine gli occhiali da sole la scimmia l'ombrello la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la scimmia l'ombrello il monopattino
			Tocca nel seguente ordine gli occhiali da sole la scimmia l'ombrello la bambina
			Tocca nel seguente ordine gli occhiali da sole la scimmia l'ombrello il leone
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il leone il gelato
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il leone la scimmia
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il leone il polipo
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il leone la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il leone il monopattino
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il leone la bambina
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il leone l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la scimmia il gelato
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la scimmia il leone
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la scimmia il polipo
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la scimmia la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la scimmia il monopattino
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la scimmia la bambina
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la scimmia l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il polipo il gelato
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il polipo il leone
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il polipo la scimmia
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il polipo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il polipo il monopattino
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il polipo la bambina
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il polipo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il gelato la scimmia
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il gelato il leone
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il gelato il polipo
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il gelato la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il gelato il monopattino
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il gelato la bambina
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il gelato l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bicicletta il gelato
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bicicletta il leone
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bicicletta il polipo
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bicicletta la scimmia
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bicicletta il monopattino
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bicicletta la bambina
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bicicletta l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il monopattino il gelato
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il monopattino il leone
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il monopattino il polipo
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il monopattino la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il monopattino la scimmia
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il monopattino la bambina
			Tocca nel seguente ordine gli occhiali da sole il pappagallo il monopattino l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bambina il leone
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bambina il polipo
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bambina la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bambina il monopattino
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bambina la scimmia
			Tocca nel seguente ordine gli occhiali da sole il pappagallo la bambina l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il pappagallo l'ombrello il gelato
			Tocca nel seguente ordine gli occhiali da sole il pappagallo l'ombrello il leone
			Tocca nel seguente ordine gli occhiali da sole il pappagallo l'ombrello il polipo
			Tocca nel seguente ordine gli occhiali da sole il pappagallo l'ombrello la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il pappagallo l'ombrello il monopattino
			Tocca nel seguente ordine gli occhiali da sole il pappagallo l'ombrello la bambina
			Tocca nel seguente ordine gli occhiali da sole il pappagallo l'ombrello la scimmia
			Tocca nel seguente ordine gli occhiali da sole il polipo il leone il gelato
			Tocca nel seguente ordine gli occhiali da sole il polipo il leone il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il polipo il leone la scimmia
			Tocca nel seguente ordine gli occhiali da sole il polipo il leone la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il polipo il leone il monopattino
			Tocca nel seguente ordine gli occhiali da sole il polipo il leone la bambina
			Tocca nel seguente ordine gli occhiali da sole il polipo il leone l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il polipo la scimmia il gelato
			Tocca nel seguente ordine gli occhiali da sole il polipo la scimmia il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il polipo la scimmia il leone
			Tocca nel seguente ordine gli occhiali da sole il polipo la scimmia la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il polipo la scimmia il monopattino
			Tocca nel seguente ordine gli occhiali da sole il polipo la scimmia la bambina
			Tocca nel seguente ordine gli occhiali da sole il polipo la scimmia l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il polipo il pappagallo il gelato
			Tocca nel seguente ordine gli occhiali da sole il polipo il pappagallo il leone
			Tocca nel seguente ordine gli occhiali da sole il polipo il pappagallo la scimmia
			Tocca nel seguente ordine gli occhiali da sole il polipo il pappagallo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il polipo il pappagallo il monopattino
			Tocca nel seguente ordine gli occhiali da sole il polipo il pappagallo la bambina
			Tocca nel seguente ordine gli occhiali da sole il polipo il pappagallo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il polipo il gelato il leone
			Tocca nel seguente ordine gli occhiali da sole il polipo il gelato il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il polipo il gelato la scimmia
			Tocca nel seguente ordine gli occhiali da sole il polipo il gelato la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il polipo il gelato il monopattino
			Tocca nel seguente ordine gli occhiali da sole il polipo il gelato la bambina
			Tocca nel seguente ordine gli occhiali da sole il polipo il gelato l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il polipo la bicicletta il gelato
			Tocca nel seguente ordine gli occhiali da sole il polipo la bicicletta il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il polipo la bicicletta il leone
			Tocca nel seguente ordine gli occhiali da sole il polipo la bicicletta la scimmia
			Tocca nel seguente ordine gli occhiali da sole il polipo la bicicletta il monopattino
			Tocca nel seguente ordine gli occhiali da sole il polipo la bicicletta la bambina
			Tocca nel seguente ordine gli occhiali da sole il polipo la bicicletta l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il polipo il monopattino il gelato
			Tocca nel seguente ordine gli occhiali da sole il polipo il monopattino il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il polipo il monopattino il leone
			Tocca nel seguente ordine gli occhiali da sole il polipo il monopattino la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il polipo il monopattino la scimmia
			Tocca nel seguente ordine gli occhiali da sole il polipo il monopattino la bambina
			Tocca nel seguente ordine gli occhiali da sole il polipo il monopattino l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il polipo la bambina il gelato
			Tocca nel seguente ordine gli occhiali da sole il polipo la bambina il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il polipo la bambina il leone
			Tocca nel seguente ordine gli occhiali da sole il polipo la bambina la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il polipo la bambina il monopattino
			Tocca nel seguente ordine gli occhiali da sole il polipo la bambina la scimmia
			Tocca nel seguente ordine gli occhiali da sole il polipo la bambina l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il polipo l'ombrello il gelato
			Tocca nel seguente ordine gli occhiali da sole il polipo l'ombrello il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il polipo l'ombrello il leone
			Tocca nel seguente ordine gli occhiali da sole il polipo l'ombrello la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il polipo l'ombrello il monopattino
			Tocca nel seguente ordine gli occhiali da sole il polipo l'ombrello la bambina
			Tocca nel seguente ordine gli occhiali da sole il polipo l'ombrello la scimmia
			Tocca nel seguente ordine gli occhiali da sole il gelato il leone la scimmia
			Tocca nel seguente ordine gli occhiali da sole il gelato il leone il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il gelato il leone il polipo
			Tocca nel seguente ordine gli occhiali da sole il gelato il leone la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il gelato il leone il monopattino
			Tocca nel seguente ordine gli occhiali da sole il gelato il leone la bambina
			Tocca nel seguente ordine gli occhiali da sole il gelato il leone l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il gelato la scimmia il leone
			Tocca nel seguente ordine gli occhiali da sole il gelato la scimmia il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il gelato la scimmia il polipo
			Tocca nel seguente ordine gli occhiali da sole il gelato la scimmia la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il gelato la scimmia il monopattino
			Tocca nel seguente ordine gli occhiali da sole il gelato la scimmia la bambina
			Tocca nel seguente ordine gli occhiali da sole il gelato la scimmia l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il gelato il pappagallo il leone
			Tocca nel seguente ordine gli occhiali da sole il gelato il pappagallo la scimmia
			Tocca nel seguente ordine gli occhiali da sole il gelato il pappagallo il polipo
			Tocca nel seguente ordine gli occhiali da sole il gelato il pappagallo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il gelato il pappagallo il monopattino
			Tocca nel seguente ordine gli occhiali da sole il gelato il pappagallo la bambina
			Tocca nel seguente ordine gli occhiali da sole il gelato il pappagallo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il gelato il polipo il leone
			Tocca nel seguente ordine gli occhiali da sole il gelato il polipo il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il gelato il polipo la scimmia
			Tocca nel seguente ordine gli occhiali da sole il gelato il polipo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il gelato il polipo il monopattino
			Tocca nel seguente ordine gli occhiali da sole il gelato il polipo la bambina
			Tocca nel seguente ordine gli occhiali da sole il gelato il polipo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il gelato la bicicletta il leone
			Tocca nel seguente ordine gli occhiali da sole il gelato la bicicletta il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il gelato la bicicletta il polipo
			Tocca nel seguente ordine gli occhiali da sole il gelato la bicicletta la scimmia
			Tocca nel seguente ordine gli occhiali da sole il gelato la bicicletta il monopattino
			Tocca nel seguente ordine gli occhiali da sole il gelato la bicicletta la bambina
			Tocca nel seguente ordine gli occhiali da sole il gelato la bicicletta l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il gelato il monopattino il leone
			Tocca nel seguente ordine gli occhiali da sole il gelato il monopattino il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il gelato il monopattino il polipo
			Tocca nel seguente ordine gli occhiali da sole il gelato il monopattino la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il gelato il monopattino la scimmia
			Tocca nel seguente ordine gli occhiali da sole il gelato il monopattino la bambina
			Tocca nel seguente ordine gli occhiali da sole il gelato il monopattino l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il gelato la bambina il leone
			Tocca nel seguente ordine gli occhiali da sole il gelato la bambina il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il gelato la bambina il polipo
			Tocca nel seguente ordine gli occhiali da sole il gelato la bambina la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il gelato la bambina il monopattino
			Tocca nel seguente ordine gli occhiali da sole il gelato la bambina la scimmia
			Tocca nel seguente ordine gli occhiali da sole il gelato la bambina l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il gelato l'ombrello il leone
			Tocca nel seguente ordine gli occhiali da sole il gelato l'ombrello il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il gelato l'ombrello il polipo
			Tocca nel seguente ordine gli occhiali da sole il gelato l'ombrello la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il gelato l'ombrello il monopattino
			Tocca nel seguente ordine gli occhiali da sole il gelato l'ombrello la bambina
			Tocca nel seguente ordine gli occhiali da sole il gelato l'ombrello la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il leone il gelato
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il leone il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il leone il polipo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il leone la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il leone il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il leone la bambina
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il leone l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la scimmia il gelato
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la scimmia il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la scimmia il polipo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la scimmia il leone
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la scimmia il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la scimmia la bambina
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la scimmia l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il pappagallo il gelato
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il pappagallo il leone
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il pappagallo il polipo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il pappagallo la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il pappagallo il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il pappagallo la bambina
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il pappagallo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il polipo il gelato
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il polipo il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il polipo il leone
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il polipo la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il polipo il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il polipo la bambina
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il polipo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il gelato il leone
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il gelato il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il gelato il polipo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il gelato la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il gelato il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il gelato la bambina
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il gelato l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il monopattino il gelato
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il monopattino il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il monopattino il polipo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il monopattino la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il monopattino il leone
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il monopattino la bambina
			Tocca nel seguente ordine gli occhiali da sole la bicicletta il monopattino l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la bambina il gelato
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la bambina il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la bambina il polipo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la bambina la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la bambina il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la bambina il leone
			Tocca nel seguente ordine gli occhiali da sole la bicicletta la bambina l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bicicletta l'ombrello il gelato
			Tocca nel seguente ordine gli occhiali da sole la bicicletta l'ombrello il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta l'ombrello il polipo
			Tocca nel seguente ordine gli occhiali da sole la bicicletta l'ombrello il leone
			Tocca nel seguente ordine gli occhiali da sole la bicicletta l'ombrello il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bicicletta l'ombrello la bambina
			Tocca nel seguente ordine gli occhiali da sole la bicicletta l'ombrello la scimmia
			Tocca nel seguente ordine gli occhiali da sole il monopattino il leone il gelato
			Tocca nel seguente ordine gli occhiali da sole il monopattino il leone il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il monopattino il leone il polipo
			Tocca nel seguente ordine gli occhiali da sole il monopattino il leone la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il monopattino il leone la scimmia
			Tocca nel seguente ordine gli occhiali da sole il monopattino il leone la bambina
			Tocca nel seguente ordine gli occhiali da sole il monopattino il leone l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il monopattino la scimmia il gelato
			Tocca nel seguente ordine gli occhiali da sole il monopattino la scimmia il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il monopattino la scimmia il polipo
			Tocca nel seguente ordine gli occhiali da sole il monopattino la scimmia la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il monopattino la scimmia il leone
			Tocca nel seguente ordine gli occhiali da sole il monopattino la scimmia la bambina
			Tocca nel seguente ordine gli occhiali da sole il monopattino la scimmia l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il monopattino il pappagallo il gelato
			Tocca nel seguente ordine gli occhiali da sole il monopattino il pappagallo la scimmia
			Tocca nel seguente ordine gli occhiali da sole il monopattino il pappagallo il polipo
			Tocca nel seguente ordine gli occhiali da sole il monopattino il pappagallo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il monopattino il pappagallo il leone
			Tocca nel seguente ordine gli occhiali da sole il monopattino il pappagallo la bambina
			Tocca nel seguente ordine gli occhiali da sole il monopattino il pappagallo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il monopattino il polipo il gelato
			Tocca nel seguente ordine gli occhiali da sole il monopattino il polipo il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il monopattino il polipo il leone
			Tocca nel seguente ordine gli occhiali da sole il monopattino il polipo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il monopattino il polipo la scimmia
			Tocca nel seguente ordine gli occhiali da sole il monopattino il polipo la bambina
			Tocca nel seguente ordine gli occhiali da sole il monopattino il polipo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il monopattino il gelato il leone
			Tocca nel seguente ordine gli occhiali da sole il monopattino il gelato il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il monopattino il gelato il polipo
			Tocca nel seguente ordine gli occhiali da sole il monopattino il gelato la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il monopattino il gelato la scimmia
			Tocca nel seguente ordine gli occhiali da sole il monopattino il gelato la bambina
			Tocca nel seguente ordine gli occhiali da sole il monopattino il gelato l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bicicletta il gelato
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bicicletta il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bicicletta il polipo
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bicicletta la scimmia
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bicicletta il leone
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bicicletta la bambina
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bicicletta l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bambina il gelato
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bambina il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bambina il polipo
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bambina la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bambina il leone
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bambina la scimmia
			Tocca nel seguente ordine gli occhiali da sole il monopattino la bambina l'ombrello
			Tocca nel seguente ordine gli occhiali da sole il monopattino l'ombrello il gelato
			Tocca nel seguente ordine gli occhiali da sole il monopattino l'ombrello il pappagallo
			Tocca nel seguente ordine gli occhiali da sole il monopattino l'ombrello il polipo
			Tocca nel seguente ordine gli occhiali da sole il monopattino l'ombrello la bicicletta
			Tocca nel seguente ordine gli occhiali da sole il monopattino l'ombrello il leone
			Tocca nel seguente ordine gli occhiali da sole il monopattino l'ombrello la bambina
			Tocca nel seguente ordine gli occhiali da sole il monopattino l'ombrello la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bambina il leone il gelato
			Tocca nel seguente ordine gli occhiali da sole la bambina il leone il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bambina il leone il polipo
			Tocca nel seguente ordine gli occhiali da sole la bambina il leone la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la bambina il leone il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bambina il leone la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bambina il leone l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bambina la scimmia il gelato
			Tocca nel seguente ordine gli occhiali da sole la bambina la scimmia il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bambina la scimmia il polipo
			Tocca nel seguente ordine gli occhiali da sole la bambina la scimmia la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la bambina la scimmia il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bambina la scimmia il leone
			Tocca nel seguente ordine gli occhiali da sole la bambina la scimmia l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bambina il pappagallo il gelato
			Tocca nel seguente ordine gli occhiali da sole la bambina il pappagallo il leone
			Tocca nel seguente ordine gli occhiali da sole la bambina il pappagallo il polipo
			Tocca nel seguente ordine gli occhiali da sole la bambina il pappagallo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la bambina il pappagallo il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bambina il pappagallo la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bambina il pappagallo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bambina il polipo il gelato
			Tocca nel seguente ordine gli occhiali da sole la bambina il polipo il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bambina il polipo il leone
			Tocca nel seguente ordine gli occhiali da sole la bambina il polipo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la bambina il polipo il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bambina il polipo la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bambina il polipo l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bambina il gelato il leone
			Tocca nel seguente ordine gli occhiali da sole la bambina il gelato il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bambina il gelato il polipo
			Tocca nel seguente ordine gli occhiali da sole la bambina il gelato la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la bambina il gelato il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bambina il gelato la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bambina il gelato l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bambina la bicicletta il gelato
			Tocca nel seguente ordine gli occhiali da sole la bambina la bicicletta il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bambina la bicicletta il polipo
			Tocca nel seguente ordine gli occhiali da sole la bambina la bicicletta il leone
			Tocca nel seguente ordine gli occhiali da sole la bambina la bicicletta il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bambina la bicicletta la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bambina la bicicletta l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bambina il monopattino il gelato
			Tocca nel seguente ordine gli occhiali da sole la bambina il monopattino il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bambina il monopattino il polipo
			Tocca nel seguente ordine gli occhiali da sole la bambina il monopattino la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la bambina il monopattino il leone
			Tocca nel seguente ordine gli occhiali da sole la bambina il monopattino la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bambina il monopattino l'ombrello
			Tocca nel seguente ordine gli occhiali da sole la bambina l'ombrello il gelato
			Tocca nel seguente ordine gli occhiali da sole la bambina l'ombrello il pappagallo
			Tocca nel seguente ordine gli occhiali da sole la bambina l'ombrello il polipo
			Tocca nel seguente ordine gli occhiali da sole la bambina l'ombrello la bicicletta
			Tocca nel seguente ordine gli occhiali da sole la bambina l'ombrello il monopattino
			Tocca nel seguente ordine gli occhiali da sole la bambina l'ombrello la scimmia
			Tocca nel seguente ordine gli occhiali da sole la bambina l'ombrello il leone
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il leone il gelato
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il leone il pappagallo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il leone il polipo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il leone la bicicletta
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il leone il monopattino
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il leone la bambina
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il leone la scimmia
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la scimmia il gelato
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la scimmia il pappagallo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la scimmia il polipo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la scimmia la bicicletta
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la scimmia il monopattino
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la scimmia la bambina
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la scimmia il leone
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il pappagallo il gelato
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il pappagallo il leone
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il pappagallo il polipo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il pappagallo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il pappagallo il monopattino
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il pappagallo la bambina
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il pappagallo la scimmia
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il polipo il gelato
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il polipo il pappagallo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il polipo il leone
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il polipo la bicicletta
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il polipo il monopattino
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il polipo la bambina
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il polipo la scimmia
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il gelato il leone
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il gelato il pappagallo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il gelato il polipo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il gelato la bicicletta
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il gelato il monopattino
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il gelato la bambina
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il gelato la scimmia
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bicicletta il gelato
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bicicletta il pappagallo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bicicletta il polipo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bicicletta il leone
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bicicletta il monopattino
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bicicletta la bambina
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bicicletta la scimmia
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il monopattino il gelato
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il monopattino il pappagallo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il monopattino il polipo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il monopattino la bicicletta
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il monopattino il leone
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il monopattino la bambina
			Tocca nel seguente ordine gli occhiali da sole l'ombrello il monopattino la scimmia
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bambina il gelato
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bambina il pappagallo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bambina il polipo
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bambina la bicicletta
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bambina il monopattino
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bambina il leone
			Tocca nel seguente ordine gli occhiali da sole l'ombrello la bambina la scimmia
			Tocca nel seguente ordine la bicicletta il leone la scimmia il polipo
			Tocca nel seguente ordine la bicicletta il leone la scimmia il pappagallo
			Tocca nel seguente ordine la bicicletta il leone la scimmia il gelato
			Tocca nel seguente ordine la bicicletta il leone la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il leone la scimmia il monopattino
			Tocca nel seguente ordine la bicicletta il leone la scimmia la bambina
			Tocca nel seguente ordine la bicicletta il leone la scimmia l'ombrello
			Tocca nel seguente ordine la bicicletta il leone il pappagallo il polipo
			Tocca nel seguente ordine la bicicletta il leone il pappagallo la scimmia
			Tocca nel seguente ordine la bicicletta il leone il pappagallo il gelato
			Tocca nel seguente ordine la bicicletta il leone il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il leone il pappagallo il monopattino
			Tocca nel seguente ordine la bicicletta il leone il pappagallo la bambina
			Tocca nel seguente ordine la bicicletta il leone il pappagallo l'ombrello
			Tocca nel seguente ordine la bicicletta il leone il polipo la scimmia
			Tocca nel seguente ordine la bicicletta il leone il polipo il pappagallo
			Tocca nel seguente ordine la bicicletta il leone il polipo il gelato
			Tocca nel seguente ordine la bicicletta il leone il polipo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il leone il polipo il monopattino
			Tocca nel seguente ordine la bicicletta il leone il polipo la bambina
			Tocca nel seguente ordine la bicicletta il leone il polipo l'ombrello
			Tocca nel seguente ordine la bicicletta il leone il gelato il polipo
			Tocca nel seguente ordine la bicicletta il leone il gelato il pappagallo
			Tocca nel seguente ordine la bicicletta il leone il gelato la scimmia
			Tocca nel seguente ordine la bicicletta il leone il gelato gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il leone il gelato il monopattino
			Tocca nel seguente ordine la bicicletta il leone il gelato la bambina
			Tocca nel seguente ordine la bicicletta il leone il gelato l'ombrello
			Tocca nel seguente ordine la bicicletta il leone gli occhiali da sole il polipo
			Tocca nel seguente ordine la bicicletta il leone gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bicicletta il leone gli occhiali da sole il gelato
			Tocca nel seguente ordine la bicicletta il leone gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bicicletta il leone gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bicicletta il leone gli occhiali da sole la bambina
			Tocca nel seguente ordine la bicicletta il leone gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bicicletta il leone il monopattino il polipo
			Tocca nel seguente ordine la bicicletta il leone il monopattino il pappagallo
			Tocca nel seguente ordine la bicicletta il leone il monopattino il gelato
			Tocca nel seguente ordine la bicicletta il leone il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il leone il monopattino la scimmia
			Tocca nel seguente ordine la bicicletta il leone il monopattino la bambina
			Tocca nel seguente ordine la bicicletta il leone il monopattino l'ombrello
			Tocca nel seguente ordine la bicicletta il leone la bambina il polipo
			Tocca nel seguente ordine la bicicletta il leone la bambina il pappagallo
			Tocca nel seguente ordine la bicicletta il leone la bambina il gelato
			Tocca nel seguente ordine la bicicletta il leone la bambina gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il leone la bambina il monopattino
			Tocca nel seguente ordine la bicicletta il leone la bambina la scimmia
			Tocca nel seguente ordine la bicicletta il leone la bambina l'ombrello
			Tocca nel seguente ordine la bicicletta il leone l'ombrello il polipo
			Tocca nel seguente ordine la bicicletta il leone l'ombrello il pappagallo
			Tocca nel seguente ordine la bicicletta il leone l'ombrello il gelato
			Tocca nel seguente ordine la bicicletta il leone l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il leone l'ombrello il monopattino
			Tocca nel seguente ordine la bicicletta il leone l'ombrello la bambina
			Tocca nel seguente ordine la bicicletta il leone l'ombrello la scimmia
			Tocca nel seguente ordine la bicicletta la scimmia il leone il polipo
			Tocca nel seguente ordine la bicicletta la scimmia il leone il pappagallo
			Tocca nel seguente ordine la bicicletta la scimmia il leone il gelato
			Tocca nel seguente ordine la bicicletta la scimmia il leone gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la scimmia il leone il monopattino
			Tocca nel seguente ordine la bicicletta la scimmia il leone la bambina
			Tocca nel seguente ordine la bicicletta la scimmia il leone l'ombrello
			Tocca nel seguente ordine la bicicletta la scimmia il pappagallo il polipo
			Tocca nel seguente ordine la bicicletta la scimmia il pappagallo il leone
			Tocca nel seguente ordine la bicicletta la scimmia il pappagallo il gelato
			Tocca nel seguente ordine la bicicletta la scimmia il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la scimmia il pappagallo il monopattino
			Tocca nel seguente ordine la bicicletta la scimmia il pappagallo la bambina
			Tocca nel seguente ordine la bicicletta la scimmia il pappagallo l'ombrello
			Tocca nel seguente ordine la bicicletta la scimmia il polipo il leone
			Tocca nel seguente ordine la bicicletta la scimmia il polipo il pappagallo
			Tocca nel seguente ordine la bicicletta la scimmia il polipo il gelato
			Tocca nel seguente ordine la bicicletta la scimmia il polipo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la scimmia il polipo il monopattino
			Tocca nel seguente ordine la bicicletta la scimmia il polipo la bambina
			Tocca nel seguente ordine la bicicletta la scimmia il polipo l'ombrello
			Tocca nel seguente ordine la bicicletta la scimmia il gelato il polipo
			Tocca nel seguente ordine la bicicletta la scimmia il gelato il pappagallo
			Tocca nel seguente ordine la bicicletta la scimmia il gelato il leone
			Tocca nel seguente ordine la bicicletta la scimmia il gelato gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la scimmia il gelato il monopattino
			Tocca nel seguente ordine la bicicletta la scimmia il gelato la bambina
			Tocca nel seguente ordine la bicicletta la scimmia il gelato l'ombrello
			Tocca nel seguente ordine la bicicletta la scimmia gli occhiali da sole il polipo
			Tocca nel seguente ordine la bicicletta la scimmia gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bicicletta la scimmia gli occhiali da sole il gelato
			Tocca nel seguente ordine la bicicletta la scimmia gli occhiali da sole il leone
			Tocca nel seguente ordine la bicicletta la scimmia gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bicicletta la scimmia gli occhiali da sole la bambina
			Tocca nel seguente ordine la bicicletta la scimmia gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bicicletta la scimmia il monopattino il polipo
			Tocca nel seguente ordine la bicicletta la scimmia il monopattino il pappagallo
			Tocca nel seguente ordine la bicicletta la scimmia il monopattino il gelato
			Tocca nel seguente ordine la bicicletta la scimmia il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la scimmia il monopattino il leone
			Tocca nel seguente ordine la bicicletta la scimmia il monopattino la bambina
			Tocca nel seguente ordine la bicicletta la scimmia il monopattino l'ombrello
			Tocca nel seguente ordine la bicicletta la scimmia la bambina il polipo
			Tocca nel seguente ordine la bicicletta la scimmia la bambina il pappagallo
			Tocca nel seguente ordine la bicicletta la scimmia la bambina il gelato
			Tocca nel seguente ordine la bicicletta la scimmia la bambina gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la scimmia la bambina il monopattino
			Tocca nel seguente ordine la bicicletta la scimmia la bambina il leone
			Tocca nel seguente ordine la bicicletta la scimmia la bambina l'ombrello
			Tocca nel seguente ordine la bicicletta la scimmia l'ombrello il polipo
			Tocca nel seguente ordine la bicicletta la scimmia l'ombrello il pappagallo
			Tocca nel seguente ordine la bicicletta la scimmia l'ombrello il gelato
			Tocca nel seguente ordine la bicicletta la scimmia l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la scimmia l'ombrello il monopattino
			Tocca nel seguente ordine la bicicletta la scimmia l'ombrello la bambina
			Tocca nel seguente ordine la bicicletta la scimmia l'ombrello il leone
			Tocca nel seguente ordine la bicicletta il pappagallo il leone il polipo
			Tocca nel seguente ordine la bicicletta il pappagallo il leone la scimmia
			Tocca nel seguente ordine la bicicletta il pappagallo il leone il gelato
			Tocca nel seguente ordine la bicicletta il pappagallo il leone gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il pappagallo il leone il monopattino
			Tocca nel seguente ordine la bicicletta il pappagallo il leone la bambina
			Tocca nel seguente ordine la bicicletta il pappagallo il leone l'ombrello
			Tocca nel seguente ordine la bicicletta il pappagallo la scimmia il polipo
			Tocca nel seguente ordine la bicicletta il pappagallo la scimmia il leone
			Tocca nel seguente ordine la bicicletta il pappagallo la scimmia il gelato
			Tocca nel seguente ordine la bicicletta il pappagallo la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il pappagallo la scimmia il monopattino
			Tocca nel seguente ordine la bicicletta il pappagallo la scimmia la bambina
			Tocca nel seguente ordine la bicicletta il pappagallo la scimmia l'ombrello
			Tocca nel seguente ordine la bicicletta il pappagallo il polipo il leone
			Tocca nel seguente ordine la bicicletta il pappagallo il polipo la scimmia
			Tocca nel seguente ordine la bicicletta il pappagallo il polipo il gelato
			Tocca nel seguente ordine la bicicletta il pappagallo il polipo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il pappagallo il polipo il monopattino
			Tocca nel seguente ordine la bicicletta il pappagallo il polipo la bambina
			Tocca nel seguente ordine la bicicletta il pappagallo il polipo l'ombrello
			Tocca nel seguente ordine la bicicletta il pappagallo il gelato il polipo
			Tocca nel seguente ordine la bicicletta il pappagallo il gelato la scimmia
			Tocca nel seguente ordine la bicicletta il pappagallo il gelato il leone
			Tocca nel seguente ordine la bicicletta il pappagallo il gelato gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il pappagallo il gelato il monopattino
			Tocca nel seguente ordine la bicicletta il pappagallo il gelato la bambina
			Tocca nel seguente ordine la bicicletta il pappagallo il gelato l'ombrello
			Tocca nel seguente ordine la bicicletta il pappagallo gli occhiali da sole il polipo
			Tocca nel seguente ordine la bicicletta il pappagallo gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bicicletta il pappagallo gli occhiali da sole il gelato
			Tocca nel seguente ordine la bicicletta il pappagallo gli occhiali da sole il leone
			Tocca nel seguente ordine la bicicletta il pappagallo gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bicicletta il pappagallo gli occhiali da sole la bambina
			Tocca nel seguente ordine la bicicletta il pappagallo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bicicletta il pappagallo il monopattino il polipo
			Tocca nel seguente ordine la bicicletta il pappagallo il monopattino la scimmia
			Tocca nel seguente ordine la bicicletta il pappagallo il monopattino il gelato
			Tocca nel seguente ordine la bicicletta il pappagallo il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il pappagallo il monopattino il leone
			Tocca nel seguente ordine la bicicletta il pappagallo il monopattino la bambina
			Tocca nel seguente ordine la bicicletta il pappagallo il monopattino l'ombrello
			Tocca nel seguente ordine la bicicletta il pappagallo la bambina il polipo
			Tocca nel seguente ordine la bicicletta il pappagallo la bambina la scimmia
			Tocca nel seguente ordine la bicicletta il pappagallo la bambina il gelato
			Tocca nel seguente ordine la bicicletta il pappagallo la bambina gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il pappagallo la bambina il monopattino
			Tocca nel seguente ordine la bicicletta il pappagallo la bambina il leone
			Tocca nel seguente ordine la bicicletta il pappagallo la bambina l'ombrello
			Tocca nel seguente ordine la bicicletta il pappagallo l'ombrello il polipo
			Tocca nel seguente ordine la bicicletta il pappagallo l'ombrello il leone
			Tocca nel seguente ordine la bicicletta il pappagallo l'ombrello il gelato
			Tocca nel seguente ordine la bicicletta il pappagallo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il pappagallo l'ombrello il monopattino
			Tocca nel seguente ordine la bicicletta il pappagallo l'ombrello la bambina
			Tocca nel seguente ordine la bicicletta il pappagallo l'ombrello la scimmia
			Tocca nel seguente ordine la bicicletta il polipo il leone la scimmia
			Tocca nel seguente ordine la bicicletta il polipo il leone il pappagallo
			Tocca nel seguente ordine la bicicletta il polipo il leone il gelato
			Tocca nel seguente ordine la bicicletta il polipo il leone gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il polipo il leone il monopattino
			Tocca nel seguente ordine la bicicletta il polipo il leone la bambina
			Tocca nel seguente ordine la bicicletta il polipo il leone l'ombrello
			Tocca nel seguente ordine la bicicletta il polipo la scimmia il leone
			Tocca nel seguente ordine la bicicletta il polipo la scimmia il pappagallo
			Tocca nel seguente ordine la bicicletta il polipo la scimmia il gelato
			Tocca nel seguente ordine la bicicletta il polipo la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il polipo la scimmia il monopattino
			Tocca nel seguente ordine la bicicletta il polipo la scimmia la bambina
			Tocca nel seguente ordine la bicicletta il polipo la scimmia l'ombrello
			Tocca nel seguente ordine la bicicletta il polipo il pappagallo il leone
			Tocca nel seguente ordine la bicicletta il polipo il pappagallo la scimmia
			Tocca nel seguente ordine la bicicletta il polipo il pappagallo il gelato
			Tocca nel seguente ordine la bicicletta il polipo il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il polipo il pappagallo il monopattino
			Tocca nel seguente ordine la bicicletta il polipo il pappagallo la bambina
			Tocca nel seguente ordine la bicicletta il polipo il pappagallo l'ombrello
			Tocca nel seguente ordine la bicicletta il polipo il gelato il leone
			Tocca nel seguente ordine la bicicletta il polipo il gelato il pappagallo
			Tocca nel seguente ordine la bicicletta il polipo il gelato la scimmia
			Tocca nel seguente ordine la bicicletta il polipo il gelato gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il polipo il gelato il monopattino
			Tocca nel seguente ordine la bicicletta il polipo il gelato la bambina
			Tocca nel seguente ordine la bicicletta il polipo il gelato l'ombrello
			Tocca nel seguente ordine la bicicletta il polipo gli occhiali da sole il leone
			Tocca nel seguente ordine la bicicletta il polipo gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bicicletta il polipo gli occhiali da sole il gelato
			Tocca nel seguente ordine la bicicletta il polipo gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bicicletta il polipo gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bicicletta il polipo gli occhiali da sole la bambina
			Tocca nel seguente ordine la bicicletta il polipo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bicicletta il polipo il monopattino il leone
			Tocca nel seguente ordine la bicicletta il polipo il monopattino il pappagallo
			Tocca nel seguente ordine la bicicletta il polipo il monopattino il gelato
			Tocca nel seguente ordine la bicicletta il polipo il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il polipo il monopattino la scimmia
			Tocca nel seguente ordine la bicicletta il polipo il monopattino la bambina
			Tocca nel seguente ordine la bicicletta il polipo il monopattino l'ombrello
			Tocca nel seguente ordine la bicicletta il polipo la bambina il leone
			Tocca nel seguente ordine la bicicletta il polipo la bambina il pappagallo
			Tocca nel seguente ordine la bicicletta il polipo la bambina il gelato
			Tocca nel seguente ordine la bicicletta il polipo la bambina gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il polipo la bambina il monopattino
			Tocca nel seguente ordine la bicicletta il polipo la bambina la scimmia
			Tocca nel seguente ordine la bicicletta il polipo la bambina l'ombrello
			Tocca nel seguente ordine la bicicletta il polipo l'ombrello il leone
			Tocca nel seguente ordine la bicicletta il polipo l'ombrello il pappagallo
			Tocca nel seguente ordine la bicicletta il polipo l'ombrello il gelato
			Tocca nel seguente ordine la bicicletta il polipo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il polipo l'ombrello il monopattino
			Tocca nel seguente ordine la bicicletta il polipo l'ombrello la bambina
			Tocca nel seguente ordine la bicicletta il polipo l'ombrello la scimmia
			Tocca nel seguente ordine la bicicletta il gelato il leone il polipo
			Tocca nel seguente ordine la bicicletta il gelato il leone il pappagallo
			Tocca nel seguente ordine la bicicletta il gelato il leone la scimmia
			Tocca nel seguente ordine la bicicletta il gelato il leone gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il gelato il leone il monopattino
			Tocca nel seguente ordine la bicicletta il gelato il leone la bambina
			Tocca nel seguente ordine la bicicletta il gelato il leone l'ombrello
			Tocca nel seguente ordine la bicicletta il gelato la scimmia il polipo
			Tocca nel seguente ordine la bicicletta il gelato la scimmia il pappagallo
			Tocca nel seguente ordine la bicicletta il gelato la scimmia il leone
			Tocca nel seguente ordine la bicicletta il gelato la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il gelato la scimmia il monopattino
			Tocca nel seguente ordine la bicicletta il gelato la scimmia la bambina
			Tocca nel seguente ordine la bicicletta il gelato la scimmia l'ombrello
			Tocca nel seguente ordine la bicicletta il gelato il pappagallo il polipo
			Tocca nel seguente ordine la bicicletta il gelato il pappagallo la scimmia
			Tocca nel seguente ordine la bicicletta il gelato il pappagallo il leone
			Tocca nel seguente ordine la bicicletta il gelato il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il gelato il pappagallo il monopattino
			Tocca nel seguente ordine la bicicletta il gelato il pappagallo la bambina
			Tocca nel seguente ordine la bicicletta il gelato il pappagallo l'ombrello
			Tocca nel seguente ordine la bicicletta il gelato il polipo il leone
			Tocca nel seguente ordine la bicicletta il gelato il polipo il pappagallo
			Tocca nel seguente ordine la bicicletta il gelato il polipo la scimmia
			Tocca nel seguente ordine la bicicletta il gelato il polipo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il gelato il polipo il monopattino
			Tocca nel seguente ordine la bicicletta il gelato il polipo la bambina
			Tocca nel seguente ordine la bicicletta il gelato il polipo l'ombrello
			Tocca nel seguente ordine la bicicletta il gelato gli occhiali da sole il polipo
			Tocca nel seguente ordine la bicicletta il gelato gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bicicletta il gelato gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bicicletta il gelato gli occhiali da sole il leone
			Tocca nel seguente ordine la bicicletta il gelato gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bicicletta il gelato gli occhiali da sole la bambina
			Tocca nel seguente ordine la bicicletta il gelato gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bicicletta il gelato il monopattino il polipo
			Tocca nel seguente ordine la bicicletta il gelato il monopattino il pappagallo
			Tocca nel seguente ordine la bicicletta il gelato il monopattino la scimmia
			Tocca nel seguente ordine la bicicletta il gelato il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il gelato il monopattino il leone
			Tocca nel seguente ordine la bicicletta il gelato il monopattino la bambina
			Tocca nel seguente ordine la bicicletta il gelato il monopattino l'ombrello
			Tocca nel seguente ordine la bicicletta il gelato la bambina il polipo
			Tocca nel seguente ordine la bicicletta il gelato la bambina il pappagallo
			Tocca nel seguente ordine la bicicletta il gelato la bambina la scimmia
			Tocca nel seguente ordine la bicicletta il gelato la bambina gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il gelato la bambina il monopattino
			Tocca nel seguente ordine la bicicletta il gelato la bambina il leone
			Tocca nel seguente ordine la bicicletta il gelato la bambina l'ombrello
			Tocca nel seguente ordine la bicicletta il gelato l'ombrello il polipo
			Tocca nel seguente ordine la bicicletta il gelato l'ombrello il pappagallo
			Tocca nel seguente ordine la bicicletta il gelato l'ombrello la scimmia
			Tocca nel seguente ordine la bicicletta il gelato l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il gelato l'ombrello il monopattino
			Tocca nel seguente ordine la bicicletta il gelato l'ombrello la bambina
			Tocca nel seguente ordine la bicicletta il gelato l'ombrello il leone
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il leone il polipo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il leone il pappagallo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il leone il gelato
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il leone la scimmia
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il leone il monopattino
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il leone la bambina
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il leone l'ombrello
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la scimmia il polipo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la scimmia il pappagallo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la scimmia il gelato
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la scimmia il leone
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la scimmia il monopattino
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la scimmia la bambina
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la scimmia l'ombrello
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il pappagallo il polipo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il pappagallo la scimmia
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il pappagallo il gelato
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il pappagallo il leone
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il pappagallo il monopattino
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il pappagallo la bambina
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il pappagallo l'ombrello
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il polipo la scimmia
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il polipo il pappagallo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il polipo il gelato
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il polipo il leone
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il polipo il monopattino
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il polipo la bambina
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il polipo l'ombrello
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il gelato il polipo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il gelato il pappagallo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il gelato la scimmia
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il gelato il leone
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il gelato il monopattino
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il gelato la bambina
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il gelato l'ombrello
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il monopattino il polipo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il monopattino il pappagallo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il monopattino il gelato
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il monopattino il leone
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il monopattino la scimmia
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il monopattino la bambina
			Tocca nel seguente ordine la bicicletta gli occhiali da sole il monopattino l'ombrello
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la bambina il polipo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la bambina il pappagallo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la bambina il gelato
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la bambina la scimmia
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la bambina il monopattino
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la bambina il leone
			Tocca nel seguente ordine la bicicletta gli occhiali da sole la bambina l'ombrello
			Tocca nel seguente ordine la bicicletta gli occhiali da sole l'ombrello il polipo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole l'ombrello il pappagallo
			Tocca nel seguente ordine la bicicletta gli occhiali da sole l'ombrello il gelato
			Tocca nel seguente ordine la bicicletta gli occhiali da sole l'ombrello la scimmia
			Tocca nel seguente ordine la bicicletta gli occhiali da sole l'ombrello il monopattino
			Tocca nel seguente ordine la bicicletta gli occhiali da sole l'ombrello la bambina
			Tocca nel seguente ordine la bicicletta gli occhiali da sole l'ombrello il leone
			Tocca nel seguente ordine la bicicletta il monopattino il leone il polipo
			Tocca nel seguente ordine la bicicletta il monopattino il leone il pappagallo
			Tocca nel seguente ordine la bicicletta il monopattino il leone il gelato
			Tocca nel seguente ordine la bicicletta il monopattino il leone gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il monopattino il leone la scimmia
			Tocca nel seguente ordine la bicicletta il monopattino il leone la bambina
			Tocca nel seguente ordine la bicicletta il monopattino il leone l'ombrello
			Tocca nel seguente ordine la bicicletta il monopattino la scimmia il polipo
			Tocca nel seguente ordine la bicicletta il monopattino la scimmia il pappagallo
			Tocca nel seguente ordine la bicicletta il monopattino la scimmia il gelato
			Tocca nel seguente ordine la bicicletta il monopattino la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il monopattino la scimmia il leone
			Tocca nel seguente ordine la bicicletta il monopattino la scimmia la bambina
			Tocca nel seguente ordine la bicicletta il monopattino la scimmia l'ombrello
			Tocca nel seguente ordine la bicicletta il monopattino il pappagallo il polipo
			Tocca nel seguente ordine la bicicletta il monopattino il pappagallo la scimmia
			Tocca nel seguente ordine la bicicletta il monopattino il pappagallo il gelato
			Tocca nel seguente ordine la bicicletta il monopattino il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il monopattino il pappagallo il leone
			Tocca nel seguente ordine la bicicletta il monopattino il pappagallo la bambina
			Tocca nel seguente ordine la bicicletta il monopattino il pappagallo l'ombrello
			Tocca nel seguente ordine la bicicletta il monopattino il polipo la scimmia
			Tocca nel seguente ordine la bicicletta il monopattino il polipo il pappagallo
			Tocca nel seguente ordine la bicicletta il monopattino il polipo il gelato
			Tocca nel seguente ordine la bicicletta il monopattino il polipo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il monopattino il polipo il leone
			Tocca nel seguente ordine la bicicletta il monopattino il polipo la bambina
			Tocca nel seguente ordine la bicicletta il monopattino il polipo l'ombrello
			Tocca nel seguente ordine la bicicletta il monopattino il gelato il polipo
			Tocca nel seguente ordine la bicicletta il monopattino il gelato il pappagallo
			Tocca nel seguente ordine la bicicletta il monopattino il gelato la scimmia
			Tocca nel seguente ordine la bicicletta il monopattino il gelato gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il monopattino il gelato il leone
			Tocca nel seguente ordine la bicicletta il monopattino il gelato la bambina
			Tocca nel seguente ordine la bicicletta il monopattino il gelato l'ombrello
			Tocca nel seguente ordine la bicicletta il monopattino gli occhiali da sole il polipo
			Tocca nel seguente ordine la bicicletta il monopattino gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bicicletta il monopattino gli occhiali da sole il gelato
			Tocca nel seguente ordine la bicicletta il monopattino gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bicicletta il monopattino gli occhiali da sole il leone
			Tocca nel seguente ordine la bicicletta il monopattino gli occhiali da sole la bambina
			Tocca nel seguente ordine la bicicletta il monopattino gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bicicletta il monopattino la bambina il polipo
			Tocca nel seguente ordine la bicicletta il monopattino la bambina il pappagallo
			Tocca nel seguente ordine la bicicletta il monopattino la bambina il gelato
			Tocca nel seguente ordine la bicicletta il monopattino la bambina gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il monopattino la bambina il leone
			Tocca nel seguente ordine la bicicletta il monopattino la bambina la scimmia
			Tocca nel seguente ordine la bicicletta il monopattino la bambina l'ombrello
			Tocca nel seguente ordine la bicicletta il monopattino l'ombrello il polipo
			Tocca nel seguente ordine la bicicletta il monopattino l'ombrello il pappagallo
			Tocca nel seguente ordine la bicicletta il monopattino l'ombrello il gelato
			Tocca nel seguente ordine la bicicletta il monopattino l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bicicletta il monopattino l'ombrello il leone
			Tocca nel seguente ordine la bicicletta il monopattino l'ombrello la bambina
			Tocca nel seguente ordine la bicicletta il monopattino l'ombrello la scimmia
			Tocca nel seguente ordine la bicicletta la bambina il leone il polipo
			Tocca nel seguente ordine la bicicletta la bambina il leone il pappagallo
			Tocca nel seguente ordine la bicicletta la bambina il leone il gelato
			Tocca nel seguente ordine la bicicletta la bambina il leone gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la bambina il leone il monopattino
			Tocca nel seguente ordine la bicicletta la bambina il leone la scimmia
			Tocca nel seguente ordine la bicicletta la bambina il leone l'ombrello
			Tocca nel seguente ordine la bicicletta la bambina la scimmia il polipo
			Tocca nel seguente ordine la bicicletta la bambina la scimmia il pappagallo
			Tocca nel seguente ordine la bicicletta la bambina la scimmia il gelato
			Tocca nel seguente ordine la bicicletta la bambina la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la bambina la scimmia il monopattino
			Tocca nel seguente ordine la bicicletta la bambina la scimmia il leone
			Tocca nel seguente ordine la bicicletta la bambina la scimmia l'ombrello
			Tocca nel seguente ordine la bicicletta la bambina il pappagallo il polipo
			Tocca nel seguente ordine la bicicletta la bambina il pappagallo la scimmia
			Tocca nel seguente ordine la bicicletta la bambina il pappagallo il gelato
			Tocca nel seguente ordine la bicicletta la bambina il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la bambina il pappagallo il monopattino
			Tocca nel seguente ordine la bicicletta la bambina il pappagallo il leone
			Tocca nel seguente ordine la bicicletta la bambina il pappagallo l'ombrello
			Tocca nel seguente ordine la bicicletta la bambina il polipo la scimmia
			Tocca nel seguente ordine la bicicletta la bambina il polipo il pappagallo
			Tocca nel seguente ordine la bicicletta la bambina il polipo il gelato
			Tocca nel seguente ordine la bicicletta la bambina il polipo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la bambina il polipo il monopattino
			Tocca nel seguente ordine la bicicletta la bambina il polipo il leone
			Tocca nel seguente ordine la bicicletta la bambina il polipo l'ombrello
			Tocca nel seguente ordine la bicicletta la bambina il gelato il polipo
			Tocca nel seguente ordine la bicicletta la bambina il gelato il pappagallo
			Tocca nel seguente ordine la bicicletta la bambina il gelato la scimmia
			Tocca nel seguente ordine la bicicletta la bambina il gelato gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la bambina il gelato il monopattino
			Tocca nel seguente ordine la bicicletta la bambina il gelato il leone
			Tocca nel seguente ordine la bicicletta la bambina il gelato l'ombrello
			Tocca nel seguente ordine la bicicletta la bambina gli occhiali da sole il polipo
			Tocca nel seguente ordine la bicicletta la bambina gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bicicletta la bambina gli occhiali da sole il gelato
			Tocca nel seguente ordine la bicicletta la bambina gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bicicletta la bambina gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bicicletta la bambina gli occhiali da sole il leone
			Tocca nel seguente ordine la bicicletta la bambina gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bicicletta la bambina il monopattino il polipo
			Tocca nel seguente ordine la bicicletta la bambina il monopattino il pappagallo
			Tocca nel seguente ordine la bicicletta la bambina il monopattino il gelato
			Tocca nel seguente ordine la bicicletta la bambina il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la bambina il monopattino il leone
			Tocca nel seguente ordine la bicicletta la bambina il monopattino la scimmia
			Tocca nel seguente ordine la bicicletta la bambina il monopattino l'ombrello
			Tocca nel seguente ordine la bicicletta la bambina l'ombrello il polipo
			Tocca nel seguente ordine la bicicletta la bambina l'ombrello il pappagallo
			Tocca nel seguente ordine la bicicletta la bambina l'ombrello il gelato
			Tocca nel seguente ordine la bicicletta la bambina l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bicicletta la bambina l'ombrello il monopattino
			Tocca nel seguente ordine la bicicletta la bambina l'ombrello la scimmia
			Tocca nel seguente ordine la bicicletta la bambina l'ombrello il leone
			Tocca nel seguente ordine la bicicletta l'ombrello il leone il polipo
			Tocca nel seguente ordine la bicicletta l'ombrello il leone il pappagallo
			Tocca nel seguente ordine la bicicletta l'ombrello il leone il gelato
			Tocca nel seguente ordine la bicicletta l'ombrello il leone gli occhiali da sole
			Tocca nel seguente ordine la bicicletta l'ombrello il leone il monopattino
			Tocca nel seguente ordine la bicicletta l'ombrello il leone la bambina
			Tocca nel seguente ordine la bicicletta l'ombrello il leone la scimmia
			Tocca nel seguente ordine la bicicletta l'ombrello la scimmia il polipo
			Tocca nel seguente ordine la bicicletta l'ombrello la scimmia il pappagallo
			Tocca nel seguente ordine la bicicletta l'ombrello la scimmia il gelato
			Tocca nel seguente ordine la bicicletta l'ombrello la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bicicletta l'ombrello la scimmia il monopattino
			Tocca nel seguente ordine la bicicletta l'ombrello la scimmia la bambina
			Tocca nel seguente ordine la bicicletta l'ombrello la scimmia il leone
			Tocca nel seguente ordine la bicicletta l'ombrello il pappagallo il polipo
			Tocca nel seguente ordine la bicicletta l'ombrello il pappagallo la scimmia
			Tocca nel seguente ordine la bicicletta l'ombrello il pappagallo il gelato
			Tocca nel seguente ordine la bicicletta l'ombrello il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta l'ombrello il pappagallo il monopattino
			Tocca nel seguente ordine la bicicletta l'ombrello il pappagallo la bambina
			Tocca nel seguente ordine la bicicletta l'ombrello il pappagallo il leone
			Tocca nel seguente ordine la bicicletta l'ombrello il polipo la scimmia
			Tocca nel seguente ordine la bicicletta l'ombrello il polipo il pappagallo
			Tocca nel seguente ordine la bicicletta l'ombrello il polipo il gelato
			Tocca nel seguente ordine la bicicletta l'ombrello il polipo gli occhiali da sole
			Tocca nel seguente ordine la bicicletta l'ombrello il polipo il monopattino
			Tocca nel seguente ordine la bicicletta l'ombrello il polipo la bambina
			Tocca nel seguente ordine la bicicletta l'ombrello il polipo il leone
			Tocca nel seguente ordine la bicicletta l'ombrello il gelato il polipo
			Tocca nel seguente ordine la bicicletta l'ombrello il gelato il pappagallo
			Tocca nel seguente ordine la bicicletta l'ombrello il gelato la scimmia
			Tocca nel seguente ordine la bicicletta l'ombrello il gelato gli occhiali da sole
			Tocca nel seguente ordine la bicicletta l'ombrello il gelato il monopattino
			Tocca nel seguente ordine la bicicletta l'ombrello il gelato la bambina
			Tocca nel seguente ordine la bicicletta l'ombrello il gelato il leone
			Tocca nel seguente ordine la bicicletta l'ombrello gli occhiali da sole il polipo
			Tocca nel seguente ordine la bicicletta l'ombrello gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bicicletta l'ombrello gli occhiali da sole il gelato
			Tocca nel seguente ordine la bicicletta l'ombrello gli occhiali da sole gli occhiali da sole
			Tocca nel seguente ordine la bicicletta l'ombrello gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bicicletta l'ombrello gli occhiali da sole la bambina
			Tocca nel seguente ordine la bicicletta l'ombrello gli occhiali da sole il leone
			Tocca nel seguente ordine la bicicletta l'ombrello il monopattino il polipo
			Tocca nel seguente ordine la bicicletta l'ombrello il monopattino il pappagallo
			Tocca nel seguente ordine la bicicletta l'ombrello il monopattino il gelato
			Tocca nel seguente ordine la bicicletta l'ombrello il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bicicletta l'ombrello il monopattino la scimmia
			Tocca nel seguente ordine la bicicletta l'ombrello il monopattino la bambina
			Tocca nel seguente ordine la bicicletta l'ombrello il monopattino il leone
			Tocca nel seguente ordine la bicicletta l'ombrello la bambina il polipo
			Tocca nel seguente ordine la bicicletta l'ombrello la bambina il pappagallo
			Tocca nel seguente ordine la bicicletta l'ombrello la bambina il gelato
			Tocca nel seguente ordine la bicicletta l'ombrello la bambina gli occhiali da sole
			Tocca nel seguente ordine la bicicletta l'ombrello la bambina il monopattino
			Tocca nel seguente ordine la bicicletta l'ombrello la bambina la scimmia
			Tocca nel seguente ordine la bicicletta l'ombrello la bambina il leone
			Tocca nel seguente ordine il monopattino il leone la scimmia il polipo
			Tocca nel seguente ordine il monopattino il leone la scimmia il pappagallo
			Tocca nel seguente ordine il monopattino il leone la scimmia il gelato
			Tocca nel seguente ordine il monopattino il leone la scimmia la bambina
			Tocca nel seguente ordine il monopattino il leone la scimmia gli occhiali da sole
			Tocca nel seguente ordine il monopattino il leone la scimmia la bicicletta
			Tocca nel seguente ordine il monopattino il leone la scimmia l'ombrello
			Tocca nel seguente ordine il monopattino il leone il pappagallo il polipo
			Tocca nel seguente ordine il monopattino il leone il pappagallo la scimmia
			Tocca nel seguente ordine il monopattino il leone il pappagallo il gelato
			Tocca nel seguente ordine il monopattino il leone il pappagallo la bambina
			Tocca nel seguente ordine il monopattino il leone il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il monopattino il leone il pappagallo la bicicletta
			Tocca nel seguente ordine il monopattino il leone il pappagallo l'ombrello
			Tocca nel seguente ordine il monopattino il leone il polipo la scimmia
			Tocca nel seguente ordine il monopattino il leone il polipo il pappagallo
			Tocca nel seguente ordine il monopattino il leone il polipo il gelato
			Tocca nel seguente ordine il monopattino il leone il polipo la bambina
			Tocca nel seguente ordine il monopattino il leone il polipo gli occhiali da sole
			Tocca nel seguente ordine il monopattino il leone il polipo la bicicletta
			Tocca nel seguente ordine il monopattino il leone il polipo l'ombrello
			Tocca nel seguente ordine il monopattino il leone il gelato il polipo
			Tocca nel seguente ordine il monopattino il leone il gelato il pappagallo
			Tocca nel seguente ordine il monopattino il leone il gelato la scimmia
			Tocca nel seguente ordine il monopattino il leone il gelato la bambina
			Tocca nel seguente ordine il monopattino il leone il gelato gli occhiali da sole
			Tocca nel seguente ordine il monopattino il leone il gelato la bicicletta
			Tocca nel seguente ordine il monopattino il leone il gelato l'ombrello
			Tocca nel seguente ordine il monopattino il leone gli occhiali da sole il polipo
			Tocca nel seguente ordine il monopattino il leone gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il monopattino il leone gli occhiali da sole il gelato
			Tocca nel seguente ordine il monopattino il leone gli occhiali da sole la bambina
			Tocca nel seguente ordine il monopattino il leone gli occhiali da sole la scimmia
			Tocca nel seguente ordine il monopattino il leone gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il monopattino il leone gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il monopattino il leone la bicicletta il polipo
			Tocca nel seguente ordine il monopattino il leone la bicicletta il pappagallo
			Tocca nel seguente ordine il monopattino il leone la bicicletta il gelato
			Tocca nel seguente ordine il monopattino il leone la bicicletta la bambina
			Tocca nel seguente ordine il monopattino il leone la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il monopattino il leone la bicicletta la scimmia
			Tocca nel seguente ordine il monopattino il leone la bicicletta l'ombrello
			Tocca nel seguente ordine il monopattino il leone la bambina il polipo
			Tocca nel seguente ordine il monopattino il leone la bambina il pappagallo
			Tocca nel seguente ordine il monopattino il leone la bambina il gelato
			Tocca nel seguente ordine il monopattino il leone la bambina la scimmia
			Tocca nel seguente ordine il monopattino il leone la bambina gli occhiali da sole
			Tocca nel seguente ordine il monopattino il leone la bambina la bicicletta
			Tocca nel seguente ordine il monopattino il leone la bambina l'ombrello
			Tocca nel seguente ordine il monopattino il leone l'ombrello il polipo
			Tocca nel seguente ordine il monopattino il leone l'ombrello il pappagallo
			Tocca nel seguente ordine il monopattino il leone l'ombrello il gelato
			Tocca nel seguente ordine il monopattino il leone l'ombrello la bambina
			Tocca nel seguente ordine il monopattino il leone l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il monopattino il leone l'ombrello la bicicletta
			Tocca nel seguente ordine il monopattino il leone l'ombrello la scimmia
			Tocca nel seguente ordine il monopattino la scimmia il leone il polipo
			Tocca nel seguente ordine il monopattino la scimmia il leone il pappagallo
			Tocca nel seguente ordine il monopattino la scimmia il leone il gelato
			Tocca nel seguente ordine il monopattino la scimmia il leone la bambina
			Tocca nel seguente ordine il monopattino la scimmia il leone gli occhiali da sole
			Tocca nel seguente ordine il monopattino la scimmia il leone la bicicletta
			Tocca nel seguente ordine il monopattino la scimmia il leone l'ombrello
			Tocca nel seguente ordine il monopattino la scimmia il pappagallo il polipo
			Tocca nel seguente ordine il monopattino la scimmia il pappagallo il leone
			Tocca nel seguente ordine il monopattino la scimmia il pappagallo il gelato
			Tocca nel seguente ordine il monopattino la scimmia il pappagallo la bambina
			Tocca nel seguente ordine il monopattino la scimmia il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il monopattino la scimmia il pappagallo la bicicletta
			Tocca nel seguente ordine il monopattino la scimmia il pappagallo l'ombrello
			Tocca nel seguente ordine il monopattino la scimmia il polipo il leone
			Tocca nel seguente ordine il monopattino la scimmia il polipo il pappagallo
			Tocca nel seguente ordine il monopattino la scimmia il polipo il gelato
			Tocca nel seguente ordine il monopattino la scimmia il polipo la bambina
			Tocca nel seguente ordine il monopattino la scimmia il polipo gli occhiali da sole
			Tocca nel seguente ordine il monopattino la scimmia il polipo la bicicletta
			Tocca nel seguente ordine il monopattino la scimmia il polipo l'ombrello
			Tocca nel seguente ordine il monopattino la scimmia il gelato il polipo
			Tocca nel seguente ordine il monopattino la scimmia il gelato il pappagallo
			Tocca nel seguente ordine il monopattino la scimmia il gelato il leone
			Tocca nel seguente ordine il monopattino la scimmia il gelato la bambina
			Tocca nel seguente ordine il monopattino la scimmia il gelato gli occhiali da sole
			Tocca nel seguente ordine il monopattino la scimmia il gelato la bicicletta
			Tocca nel seguente ordine il monopattino la scimmia il gelato l'ombrello
			Tocca nel seguente ordine il monopattino la scimmia gli occhiali da sole il polipo
			Tocca nel seguente ordine il monopattino la scimmia gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il monopattino la scimmia gli occhiali da sole il gelato
			Tocca nel seguente ordine il monopattino la scimmia gli occhiali da sole la bambina
			Tocca nel seguente ordine il monopattino la scimmia gli occhiali da sole il leone
			Tocca nel seguente ordine il monopattino la scimmia gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il monopattino la scimmia gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il monopattino la scimmia la bicicletta il polipo
			Tocca nel seguente ordine il monopattino la scimmia la bicicletta il pappagallo
			Tocca nel seguente ordine il monopattino la scimmia la bicicletta il gelato
			Tocca nel seguente ordine il monopattino la scimmia la bicicletta la bambina
			Tocca nel seguente ordine il monopattino la scimmia la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il monopattino la scimmia la bicicletta il leone
			Tocca nel seguente ordine il monopattino la scimmia la bicicletta l'ombrello
			Tocca nel seguente ordine il monopattino la scimmia la bambina il polipo
			Tocca nel seguente ordine il monopattino la scimmia la bambina il pappagallo
			Tocca nel seguente ordine il monopattino la scimmia la bambina il gelato
			Tocca nel seguente ordine il monopattino la scimmia la bambina il leone
			Tocca nel seguente ordine il monopattino la scimmia la bambina gli occhiali da sole
			Tocca nel seguente ordine il monopattino la scimmia la bambina la bicicletta
			Tocca nel seguente ordine il monopattino la scimmia la bambina l'ombrello
			Tocca nel seguente ordine il monopattino la scimmia l'ombrello il polipo
			Tocca nel seguente ordine il monopattino la scimmia l'ombrello il pappagallo
			Tocca nel seguente ordine il monopattino la scimmia l'ombrello il gelato
			Tocca nel seguente ordine il monopattino la scimmia l'ombrello la bambina
			Tocca nel seguente ordine il monopattino la scimmia l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il monopattino la scimmia l'ombrello la bicicletta
			Tocca nel seguente ordine il monopattino la scimmia l'ombrello il leone
			Tocca nel seguente ordine il monopattino il pappagallo il leone il polipo
			Tocca nel seguente ordine il monopattino il pappagallo il leone la scimmia
			Tocca nel seguente ordine il monopattino il pappagallo il leone il gelato
			Tocca nel seguente ordine il monopattino il pappagallo il leone la bambina
			Tocca nel seguente ordine il monopattino il pappagallo il leone gli occhiali da sole
			Tocca nel seguente ordine il monopattino il pappagallo il leone la bicicletta
			Tocca nel seguente ordine il monopattino il pappagallo il leone l'ombrello
			Tocca nel seguente ordine il monopattino il pappagallo la scimmia il polipo
			Tocca nel seguente ordine il monopattino il pappagallo la scimmia il leone
			Tocca nel seguente ordine il monopattino il pappagallo la scimmia il gelato
			Tocca nel seguente ordine il monopattino il pappagallo la scimmia la bambina
			Tocca nel seguente ordine il monopattino il pappagallo la scimmia gli occhiali da sole
			Tocca nel seguente ordine il monopattino il pappagallo la scimmia la bicicletta
			Tocca nel seguente ordine il monopattino il pappagallo la scimmia l'ombrello
			Tocca nel seguente ordine il monopattino il pappagallo il polipo il leone
			Tocca nel seguente ordine il monopattino il pappagallo il polipo la scimmia
			Tocca nel seguente ordine il monopattino il pappagallo il polipo il gelato
			Tocca nel seguente ordine il monopattino il pappagallo il polipo la bambina
			Tocca nel seguente ordine il monopattino il pappagallo il polipo gli occhiali da sole
			Tocca nel seguente ordine il monopattino il pappagallo il polipo la bicicletta
			Tocca nel seguente ordine il monopattino il pappagallo il polipo l'ombrello
			Tocca nel seguente ordine il monopattino il pappagallo il gelato il polipo
			Tocca nel seguente ordine il monopattino il pappagallo il gelato la scimmia
			Tocca nel seguente ordine il monopattino il pappagallo il gelato il leone
			Tocca nel seguente ordine il monopattino il pappagallo il gelato la bambina
			Tocca nel seguente ordine il monopattino il pappagallo il gelato gli occhiali da sole
			Tocca nel seguente ordine il monopattino il pappagallo il gelato la bicicletta
			Tocca nel seguente ordine il monopattino il pappagallo il gelato l'ombrello
			Tocca nel seguente ordine il monopattino il pappagallo gli occhiali da sole il polipo
			Tocca nel seguente ordine il monopattino il pappagallo gli occhiali da sole la scimmia
			Tocca nel seguente ordine il monopattino il pappagallo gli occhiali da sole il gelato
			Tocca nel seguente ordine il monopattino il pappagallo gli occhiali da sole la bambina
			Tocca nel seguente ordine il monopattino il pappagallo gli occhiali da sole il leone
			Tocca nel seguente ordine il monopattino il pappagallo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il monopattino il pappagallo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il monopattino il pappagallo la bicicletta il polipo
			Tocca nel seguente ordine il monopattino il pappagallo la bicicletta la scimmia
			Tocca nel seguente ordine il monopattino il pappagallo la bicicletta il gelato
			Tocca nel seguente ordine il monopattino il pappagallo la bicicletta la bambina
			Tocca nel seguente ordine il monopattino il pappagallo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il monopattino il pappagallo la bicicletta il leone
			Tocca nel seguente ordine il monopattino il pappagallo la bicicletta l'ombrello
			Tocca nel seguente ordine il monopattino il pappagallo la bambina il polipo
			Tocca nel seguente ordine il monopattino il pappagallo la bambina la scimmia
			Tocca nel seguente ordine il monopattino il pappagallo la bambina il gelato
			Tocca nel seguente ordine il monopattino il pappagallo la bambina il leone
			Tocca nel seguente ordine il monopattino il pappagallo la bambina gli occhiali da sole
			Tocca nel seguente ordine il monopattino il pappagallo la bambina la bicicletta
			Tocca nel seguente ordine il monopattino il pappagallo la bambina l'ombrello
			Tocca nel seguente ordine il monopattino il pappagallo l'ombrello il polipo
			Tocca nel seguente ordine il monopattino il pappagallo l'ombrello la scimmia
			Tocca nel seguente ordine il monopattino il pappagallo l'ombrello il gelato
			Tocca nel seguente ordine il monopattino il pappagallo l'ombrello la bambina
			Tocca nel seguente ordine il monopattino il pappagallo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il monopattino il pappagallo l'ombrello la bicicletta
			Tocca nel seguente ordine il monopattino il pappagallo l'ombrello il leone
			Tocca nel seguente ordine il monopattino il polipo il leone la scimmia
			Tocca nel seguente ordine il monopattino il polipo il leone il pappagallo
			Tocca nel seguente ordine il monopattino il polipo il leone il gelato
			Tocca nel seguente ordine il monopattino il polipo il leone la bambina
			Tocca nel seguente ordine il monopattino il polipo il leone gli occhiali da sole
			Tocca nel seguente ordine il monopattino il polipo il leone la bicicletta
			Tocca nel seguente ordine il monopattino il polipo il leone l'ombrello
			Tocca nel seguente ordine il monopattino il polipo la scimmia il leone
			Tocca nel seguente ordine il monopattino il polipo la scimmia il pappagallo
			Tocca nel seguente ordine il monopattino il polipo la scimmia il gelato
			Tocca nel seguente ordine il monopattino il polipo la scimmia la bambina
			Tocca nel seguente ordine il monopattino il polipo la scimmia gli occhiali da sole
			Tocca nel seguente ordine il monopattino il polipo la scimmia la bicicletta
			Tocca nel seguente ordine il monopattino il polipo la scimmia l'ombrello
			Tocca nel seguente ordine il monopattino il polipo il pappagallo il leone
			Tocca nel seguente ordine il monopattino il polipo il pappagallo la scimmia
			Tocca nel seguente ordine il monopattino il polipo il pappagallo il gelato
			Tocca nel seguente ordine il monopattino il polipo il pappagallo la bambina
			Tocca nel seguente ordine il monopattino il polipo il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il monopattino il polipo il pappagallo la bicicletta
			Tocca nel seguente ordine il monopattino il polipo il pappagallo l'ombrello
			Tocca nel seguente ordine il monopattino il polipo il gelato il leone
			Tocca nel seguente ordine il monopattino il polipo il gelato il pappagallo
			Tocca nel seguente ordine il monopattino il polipo il gelato la scimmia
			Tocca nel seguente ordine il monopattino il polipo il gelato la bambina
			Tocca nel seguente ordine il monopattino il polipo il gelato gli occhiali da sole
			Tocca nel seguente ordine il monopattino il polipo il gelato la bicicletta
			Tocca nel seguente ordine il monopattino il polipo il gelato l'ombrello
			Tocca nel seguente ordine il monopattino il polipo gli occhiali da sole il leone
			Tocca nel seguente ordine il monopattino il polipo gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il monopattino il polipo gli occhiali da sole il gelato
			Tocca nel seguente ordine il monopattino il polipo gli occhiali da sole la bambina
			Tocca nel seguente ordine il monopattino il polipo gli occhiali da sole la scimmia
			Tocca nel seguente ordine il monopattino il polipo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il monopattino il polipo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il monopattino il polipo la bicicletta il leone
			Tocca nel seguente ordine il monopattino il polipo la bicicletta il pappagallo
			Tocca nel seguente ordine il monopattino il polipo la bicicletta il gelato
			Tocca nel seguente ordine il monopattino il polipo la bicicletta la bambina
			Tocca nel seguente ordine il monopattino il polipo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il monopattino il polipo la bicicletta la scimmia
			Tocca nel seguente ordine il monopattino il polipo la bicicletta l'ombrello
			Tocca nel seguente ordine il monopattino il polipo la bambina il leone
			Tocca nel seguente ordine il monopattino il polipo la bambina il pappagallo
			Tocca nel seguente ordine il monopattino il polipo la bambina il gelato
			Tocca nel seguente ordine il monopattino il polipo la bambina la scimmia
			Tocca nel seguente ordine il monopattino il polipo la bambina gli occhiali da sole
			Tocca nel seguente ordine il monopattino il polipo la bambina la bicicletta
			Tocca nel seguente ordine il monopattino il polipo la bambina l'ombrello
			Tocca nel seguente ordine il monopattino il polipo l'ombrello il leone
			Tocca nel seguente ordine il monopattino il polipo l'ombrello il pappagallo
			Tocca nel seguente ordine il monopattino il polipo l'ombrello il gelato
			Tocca nel seguente ordine il monopattino il polipo l'ombrello la bambina
			Tocca nel seguente ordine il monopattino il polipo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il monopattino il polipo l'ombrello la bicicletta
			Tocca nel seguente ordine il monopattino il polipo l'ombrello la scimmia
			Tocca nel seguente ordine il monopattino il gelato il leone il polipo
			Tocca nel seguente ordine il monopattino il gelato il leone il pappagallo
			Tocca nel seguente ordine il monopattino il gelato il leone la scimmia
			Tocca nel seguente ordine il monopattino il gelato il leone la bambina
			Tocca nel seguente ordine il monopattino il gelato il leone gli occhiali da sole
			Tocca nel seguente ordine il monopattino il gelato il leone la bicicletta
			Tocca nel seguente ordine il monopattino il gelato il leone l'ombrello
			Tocca nel seguente ordine il monopattino il gelato la scimmia il polipo
			Tocca nel seguente ordine il monopattino il gelato la scimmia il pappagallo
			Tocca nel seguente ordine il monopattino il gelato la scimmia il leone
			Tocca nel seguente ordine il monopattino il gelato la scimmia la bambina
			Tocca nel seguente ordine il monopattino il gelato la scimmia gli occhiali da sole
			Tocca nel seguente ordine il monopattino il gelato la scimmia la bicicletta
			Tocca nel seguente ordine il monopattino il gelato la scimmia l'ombrello
			Tocca nel seguente ordine il monopattino il gelato il pappagallo il polipo
			Tocca nel seguente ordine il monopattino il gelato il pappagallo il leone
			Tocca nel seguente ordine il monopattino il gelato il pappagallo la scimmia
			Tocca nel seguente ordine il monopattino il gelato il pappagallo la bambina
			Tocca nel seguente ordine il monopattino il gelato il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il monopattino il gelato il pappagallo la bicicletta
			Tocca nel seguente ordine il monopattino il gelato il pappagallo l'ombrello
			Tocca nel seguente ordine il monopattino il gelato il polipo il leone
			Tocca nel seguente ordine il monopattino il gelato il polipo il pappagallo
			Tocca nel seguente ordine il monopattino il gelato il polipo la scimmia
			Tocca nel seguente ordine il monopattino il gelato il polipo la bambina
			Tocca nel seguente ordine il monopattino il gelato il polipo gli occhiali da sole
			Tocca nel seguente ordine il monopattino il gelato il polipo la bicicletta
			Tocca nel seguente ordine il monopattino il gelato il polipo l'ombrello
			Tocca nel seguente ordine il monopattino il gelato gli occhiali da sole il polipo
			Tocca nel seguente ordine il monopattino il gelato gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il monopattino il gelato gli occhiali da sole la scimmia
			Tocca nel seguente ordine il monopattino il gelato gli occhiali da sole la bambina
			Tocca nel seguente ordine il monopattino il gelato gli occhiali da sole il leone
			Tocca nel seguente ordine il monopattino il gelato gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il monopattino il gelato gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il monopattino il gelato la bicicletta il polipo
			Tocca nel seguente ordine il monopattino il gelato la bicicletta il pappagallo
			Tocca nel seguente ordine il monopattino il gelato la bicicletta la scimmia
			Tocca nel seguente ordine il monopattino il gelato la bicicletta la bambina
			Tocca nel seguente ordine il monopattino il gelato la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il monopattino il gelato la bicicletta il leone
			Tocca nel seguente ordine il monopattino il gelato la bicicletta l'ombrello
			Tocca nel seguente ordine il monopattino il gelato la bambina il polipo
			Tocca nel seguente ordine il monopattino il gelato la bambina il pappagallo
			Tocca nel seguente ordine il monopattino il gelato la bambina la scimmia
			Tocca nel seguente ordine il monopattino il gelato la bambina il leone
			Tocca nel seguente ordine il monopattino il gelato la bambina gli occhiali da sole
			Tocca nel seguente ordine il monopattino il gelato la bambina la bicicletta
			Tocca nel seguente ordine il monopattino il gelato la bambina l'ombrello
			Tocca nel seguente ordine il monopattino il gelato l'ombrello il polipo
			Tocca nel seguente ordine il monopattino il gelato l'ombrello il pappagallo
			Tocca nel seguente ordine il monopattino il gelato l'ombrello la scimmia
			Tocca nel seguente ordine il monopattino il gelato l'ombrello la bambina
			Tocca nel seguente ordine il monopattino il gelato l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il monopattino il gelato l'ombrello la bicicletta
			Tocca nel seguente ordine il monopattino il gelato l'ombrello il leone
			Tocca nel seguente ordine il monopattino gli occhiali da sole il leone il polipo
			Tocca nel seguente ordine il monopattino gli occhiali da sole il leone il pappagallo
			Tocca nel seguente ordine il monopattino gli occhiali da sole il leone il gelato
			Tocca nel seguente ordine il monopattino gli occhiali da sole il leone la bambina
			Tocca nel seguente ordine il monopattino gli occhiali da sole il leone la scimmia
			Tocca nel seguente ordine il monopattino gli occhiali da sole il leone la bicicletta
			Tocca nel seguente ordine il monopattino gli occhiali da sole il leone l'ombrello
			Tocca nel seguente ordine il monopattino gli occhiali da sole la scimmia il polipo
			Tocca nel seguente ordine il monopattino gli occhiali da sole la scimmia il pappagallo
			Tocca nel seguente ordine il monopattino gli occhiali da sole la scimmia il gelato
			Tocca nel seguente ordine il monopattino gli occhiali da sole la scimmia la bambina
			Tocca nel seguente ordine il monopattino gli occhiali da sole la scimmia il leone
			Tocca nel seguente ordine il monopattino gli occhiali da sole la scimmia la bicicletta
			Tocca nel seguente ordine il monopattino gli occhiali da sole la scimmia l'ombrello
			Tocca nel seguente ordine il monopattino gli occhiali da sole il pappagallo il polipo
			Tocca nel seguente ordine il monopattino gli occhiali da sole il pappagallo la scimmia
			Tocca nel seguente ordine il monopattino gli occhiali da sole il pappagallo il gelato
			Tocca nel seguente ordine il monopattino gli occhiali da sole il pappagallo la bambina
			Tocca nel seguente ordine il monopattino gli occhiali da sole il pappagallo il leone
			Tocca nel seguente ordine il monopattino gli occhiali da sole il pappagallo la bicicletta
			Tocca nel seguente ordine il monopattino gli occhiali da sole il pappagallo l'ombrello
			Tocca nel seguente ordine il monopattino gli occhiali da sole il polipo la scimmia
			Tocca nel seguente ordine il monopattino gli occhiali da sole il polipo il pappagallo
			Tocca nel seguente ordine il monopattino gli occhiali da sole il polipo il gelato
			Tocca nel seguente ordine il monopattino gli occhiali da sole il polipo la bambina
			Tocca nel seguente ordine il monopattino gli occhiali da sole il polipo il leone
			Tocca nel seguente ordine il monopattino gli occhiali da sole il polipo la bicicletta
			Tocca nel seguente ordine il monopattino gli occhiali da sole il polipo l'ombrello
			Tocca nel seguente ordine il monopattino gli occhiali da sole il gelato il polipo
			Tocca nel seguente ordine il monopattino gli occhiali da sole il gelato il pappagallo
			Tocca nel seguente ordine il monopattino gli occhiali da sole il gelato il leone
			Tocca nel seguente ordine il monopattino gli occhiali da sole il gelato la bambina
			Tocca nel seguente ordine il monopattino gli occhiali da sole il gelato la scimmia
			Tocca nel seguente ordine il monopattino gli occhiali da sole il gelato la bicicletta
			Tocca nel seguente ordine il monopattino gli occhiali da sole il gelato l'ombrello
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bicicletta il polipo
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bicicletta il pappagallo
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bicicletta il gelato
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bicicletta la bambina
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bicicletta il leone
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bicicletta la scimmia
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bicicletta l'ombrello
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bambina il polipo
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bambina il pappagallo
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bambina il gelato
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bambina il leone
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bambina la scimmia
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bambina la bicicletta
			Tocca nel seguente ordine il monopattino gli occhiali da sole la bambina l'ombrello
			Tocca nel seguente ordine il monopattino gli occhiali da sole l'ombrello il polipo
			Tocca nel seguente ordine il monopattino gli occhiali da sole l'ombrello il pappagallo
			Tocca nel seguente ordine il monopattino gli occhiali da sole l'ombrello il gelato
			Tocca nel seguente ordine il monopattino gli occhiali da sole l'ombrello la bambina
			Tocca nel seguente ordine il monopattino gli occhiali da sole l'ombrello la scimmia
			Tocca nel seguente ordine il monopattino gli occhiali da sole l'ombrello la bicicletta
			Tocca nel seguente ordine il monopattino gli occhiali da sole l'ombrello il leone
			Tocca nel seguente ordine il monopattino la bicicletta il leone il polipo
			Tocca nel seguente ordine il monopattino la bicicletta il leone il pappagallo
			Tocca nel seguente ordine il monopattino la bicicletta il leone il gelato
			Tocca nel seguente ordine il monopattino la bicicletta il leone la bambina
			Tocca nel seguente ordine il monopattino la bicicletta il leone gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bicicletta il leone la scimmia
			Tocca nel seguente ordine il monopattino la bicicletta il leone l'ombrello
			Tocca nel seguente ordine il monopattino la bicicletta la scimmia il polipo
			Tocca nel seguente ordine il monopattino la bicicletta la scimmia il pappagallo
			Tocca nel seguente ordine il monopattino la bicicletta la scimmia il gelato
			Tocca nel seguente ordine il monopattino la bicicletta la scimmia la bambina
			Tocca nel seguente ordine il monopattino la bicicletta la scimmia gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bicicletta la scimmia il leone
			Tocca nel seguente ordine il monopattino la bicicletta la scimmia l'ombrello
			Tocca nel seguente ordine il monopattino la bicicletta il pappagallo il polipo
			Tocca nel seguente ordine il monopattino la bicicletta il pappagallo il leone
			Tocca nel seguente ordine il monopattino la bicicletta il pappagallo il gelato
			Tocca nel seguente ordine il monopattino la bicicletta il pappagallo la bambina
			Tocca nel seguente ordine il monopattino la bicicletta il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bicicletta il pappagallo la scimmia
			Tocca nel seguente ordine il monopattino la bicicletta il pappagallo l'ombrello
			Tocca nel seguente ordine il monopattino la bicicletta il polipo il leone
			Tocca nel seguente ordine il monopattino la bicicletta il polipo il pappagallo
			Tocca nel seguente ordine il monopattino la bicicletta il polipo il gelato
			Tocca nel seguente ordine il monopattino la bicicletta il polipo la bambina
			Tocca nel seguente ordine il monopattino la bicicletta il polipo gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bicicletta il polipo la scimmia
			Tocca nel seguente ordine il monopattino la bicicletta il polipo l'ombrello
			Tocca nel seguente ordine il monopattino la bicicletta il gelato il polipo
			Tocca nel seguente ordine il monopattino la bicicletta il gelato il pappagallo
			Tocca nel seguente ordine il monopattino la bicicletta il gelato il leone
			Tocca nel seguente ordine il monopattino la bicicletta il gelato la bambina
			Tocca nel seguente ordine il monopattino la bicicletta il gelato gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bicicletta il gelato la scimmia
			Tocca nel seguente ordine il monopattino la bicicletta il gelato l'ombrello
			Tocca nel seguente ordine il monopattino la bicicletta gli occhiali da sole il polipo
			Tocca nel seguente ordine il monopattino la bicicletta gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il monopattino la bicicletta gli occhiali da sole il gelato
			Tocca nel seguente ordine il monopattino la bicicletta gli occhiali da sole la bambina
			Tocca nel seguente ordine il monopattino la bicicletta gli occhiali da sole il leone
			Tocca nel seguente ordine il monopattino la bicicletta gli occhiali da sole la scimmia
			Tocca nel seguente ordine il monopattino la bicicletta gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il monopattino la bicicletta la bambina il polipo
			Tocca nel seguente ordine il monopattino la bicicletta la bambina il pappagallo
			Tocca nel seguente ordine il monopattino la bicicletta la bambina il gelato
			Tocca nel seguente ordine il monopattino la bicicletta la bambina la scimmia
			Tocca nel seguente ordine il monopattino la bicicletta la bambina gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bicicletta la bambina il leone
			Tocca nel seguente ordine il monopattino la bicicletta la bambina l'ombrello
			Tocca nel seguente ordine il monopattino la bicicletta l'ombrello il polipo
			Tocca nel seguente ordine il monopattino la bicicletta l'ombrello il pappagallo
			Tocca nel seguente ordine il monopattino la bicicletta l'ombrello il gelato
			Tocca nel seguente ordine il monopattino la bicicletta l'ombrello la bambina
			Tocca nel seguente ordine il monopattino la bicicletta l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bicicletta l'ombrello la scimmia
			Tocca nel seguente ordine il monopattino la bicicletta l'ombrello il leone
			Tocca nel seguente ordine il monopattino la bambina il leone il polipo
			Tocca nel seguente ordine il monopattino la bambina il leone il pappagallo
			Tocca nel seguente ordine il monopattino la bambina il leone il gelato
			Tocca nel seguente ordine il monopattino la bambina il leone la scimmia
			Tocca nel seguente ordine il monopattino la bambina il leone gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bambina il leone la bicicletta
			Tocca nel seguente ordine il monopattino la bambina il leone l'ombrello
			Tocca nel seguente ordine il monopattino la bambina la scimmia il polipo
			Tocca nel seguente ordine il monopattino la bambina la scimmia il pappagallo
			Tocca nel seguente ordine il monopattino la bambina la scimmia il gelato
			Tocca nel seguente ordine il monopattino la bambina la scimmia il leone
			Tocca nel seguente ordine il monopattino la bambina la scimmia gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bambina la scimmia la bicicletta
			Tocca nel seguente ordine il monopattino la bambina la scimmia l'ombrello
			Tocca nel seguente ordine il monopattino la bambina il pappagallo il polipo
			Tocca nel seguente ordine il monopattino la bambina il pappagallo il leone
			Tocca nel seguente ordine il monopattino la bambina il pappagallo il gelato
			Tocca nel seguente ordine il monopattino la bambina il pappagallo la scimmia
			Tocca nel seguente ordine il monopattino la bambina il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bambina il pappagallo la bicicletta
			Tocca nel seguente ordine il monopattino la bambina il pappagallo l'ombrello
			Tocca nel seguente ordine il monopattino la bambina il polipo il leone
			Tocca nel seguente ordine il monopattino la bambina il polipo il pappagallo
			Tocca nel seguente ordine il monopattino la bambina il polipo il gelato
			Tocca nel seguente ordine il monopattino la bambina il polipo la scimmia
			Tocca nel seguente ordine il monopattino la bambina il polipo gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bambina il polipo la bicicletta
			Tocca nel seguente ordine il monopattino la bambina il polipo l'ombrello
			Tocca nel seguente ordine il monopattino la bambina il gelato il polipo
			Tocca nel seguente ordine il monopattino la bambina il gelato il pappagallo
			Tocca nel seguente ordine il monopattino la bambina il gelato il leone
			Tocca nel seguente ordine il monopattino la bambina il gelato la scimmia
			Tocca nel seguente ordine il monopattino la bambina il gelato gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bambina il gelato la bicicletta
			Tocca nel seguente ordine il monopattino la bambina il gelato l'ombrello
			Tocca nel seguente ordine il monopattino la bambina gli occhiali da sole il polipo
			Tocca nel seguente ordine il monopattino la bambina gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il monopattino la bambina gli occhiali da sole il gelato
			Tocca nel seguente ordine il monopattino la bambina gli occhiali da sole la scimmia
			Tocca nel seguente ordine il monopattino la bambina gli occhiali da sole il leone
			Tocca nel seguente ordine il monopattino la bambina gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il monopattino la bambina gli occhiali da sole l'ombrello
			Tocca nel seguente ordine il monopattino la bambina la bicicletta il polipo
			Tocca nel seguente ordine il monopattino la bambina la bicicletta il pappagallo
			Tocca nel seguente ordine il monopattino la bambina la bicicletta il gelato
			Tocca nel seguente ordine il monopattino la bambina la bicicletta la scimmia
			Tocca nel seguente ordine il monopattino la bambina la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bambina la bicicletta il leone
			Tocca nel seguente ordine il monopattino la bambina la bicicletta l'ombrello
			Tocca nel seguente ordine il monopattino la bambina l'ombrello il polipo
			Tocca nel seguente ordine il monopattino la bambina l'ombrello il pappagallo
			Tocca nel seguente ordine il monopattino la bambina l'ombrello il gelato
			Tocca nel seguente ordine il monopattino la bambina l'ombrello la scimmia
			Tocca nel seguente ordine il monopattino la bambina l'ombrello gli occhiali da sole
			Tocca nel seguente ordine il monopattino la bambina l'ombrello la bicicletta
			Tocca nel seguente ordine il monopattino la bambina l'ombrello il leone
			Tocca nel seguente ordine il monopattino l'ombrello il leone il polipo
			Tocca nel seguente ordine il monopattino l'ombrello il leone il pappagallo
			Tocca nel seguente ordine il monopattino l'ombrello il leone il gelato
			Tocca nel seguente ordine il monopattino l'ombrello il leone la bambina
			Tocca nel seguente ordine il monopattino l'ombrello il leone gli occhiali da sole
			Tocca nel seguente ordine il monopattino l'ombrello il leone la bicicletta
			Tocca nel seguente ordine il monopattino l'ombrello il leone la scimmia
			Tocca nel seguente ordine il monopattino l'ombrello la scimmia il polipo
			Tocca nel seguente ordine il monopattino l'ombrello la scimmia il pappagallo
			Tocca nel seguente ordine il monopattino l'ombrello la scimmia il gelato
			Tocca nel seguente ordine il monopattino l'ombrello la scimmia la bambina
			Tocca nel seguente ordine il monopattino l'ombrello la scimmia gli occhiali da sole
			Tocca nel seguente ordine il monopattino l'ombrello la scimmia la bicicletta
			Tocca nel seguente ordine il monopattino l'ombrello la scimmia il leone
			Tocca nel seguente ordine il monopattino l'ombrello il pappagallo il polipo
			Tocca nel seguente ordine il monopattino l'ombrello il pappagallo il leone
			Tocca nel seguente ordine il monopattino l'ombrello il pappagallo il gelato
			Tocca nel seguente ordine il monopattino l'ombrello il pappagallo la bambina
			Tocca nel seguente ordine il monopattino l'ombrello il pappagallo gli occhiali da sole
			Tocca nel seguente ordine il monopattino l'ombrello il pappagallo la bicicletta
			Tocca nel seguente ordine il monopattino l'ombrello il pappagallo la scimmia
			Tocca nel seguente ordine il monopattino l'ombrello il polipo il leone
			Tocca nel seguente ordine il monopattino l'ombrello il polipo il pappagallo
			Tocca nel seguente ordine il monopattino l'ombrello il polipo il gelato
			Tocca nel seguente ordine il monopattino l'ombrello il polipo la bambina
			Tocca nel seguente ordine il monopattino l'ombrello il polipo gli occhiali da sole
			Tocca nel seguente ordine il monopattino l'ombrello il polipo la bicicletta
			Tocca nel seguente ordine il monopattino l'ombrello il polipo la scimmia
			Tocca nel seguente ordine il monopattino l'ombrello il gelato il polipo
			Tocca nel seguente ordine il monopattino l'ombrello il gelato il pappagallo
			Tocca nel seguente ordine il monopattino l'ombrello il gelato il leone
			Tocca nel seguente ordine il monopattino l'ombrello il gelato la bambina
			Tocca nel seguente ordine il monopattino l'ombrello il gelato gli occhiali da sole
			Tocca nel seguente ordine il monopattino l'ombrello il gelato la bicicletta
			Tocca nel seguente ordine il monopattino l'ombrello il gelato la scimmia
			Tocca nel seguente ordine il monopattino l'ombrello gli occhiali da sole il polipo
			Tocca nel seguente ordine il monopattino l'ombrello gli occhiali da sole il pappagallo
			Tocca nel seguente ordine il monopattino l'ombrello gli occhiali da sole il gelato
			Tocca nel seguente ordine il monopattino l'ombrello gli occhiali da sole la bambina
			Tocca nel seguente ordine il monopattino l'ombrello gli occhiali da sole il leone
			Tocca nel seguente ordine il monopattino l'ombrello gli occhiali da sole la bicicletta
			Tocca nel seguente ordine il monopattino l'ombrello gli occhiali da sole la scimmia
			Tocca nel seguente ordine il monopattino l'ombrello la bicicletta il polipo
			Tocca nel seguente ordine il monopattino l'ombrello la bicicletta il pappagallo
			Tocca nel seguente ordine il monopattino l'ombrello la bicicletta il gelato
			Tocca nel seguente ordine il monopattino l'ombrello la bicicletta la bambina
			Tocca nel seguente ordine il monopattino l'ombrello la bicicletta gli occhiali da sole
			Tocca nel seguente ordine il monopattino l'ombrello la bicicletta il leone
			Tocca nel seguente ordine il monopattino l'ombrello la bicicletta la scimmia
			Tocca nel seguente ordine il monopattino l'ombrello la bambina il polipo
			Tocca nel seguente ordine il monopattino l'ombrello la bambina il pappagallo
			Tocca nel seguente ordine il monopattino l'ombrello la bambina il gelato
			Tocca nel seguente ordine il monopattino l'ombrello la bambina la scimmia
			Tocca nel seguente ordine il monopattino l'ombrello la bambina gli occhiali da sole
			Tocca nel seguente ordine il monopattino l'ombrello la bambina la bicicletta
			Tocca nel seguente ordine il monopattino l'ombrello la bambina il leone
			Tocca nel seguente ordine la bambina il leone la scimmia il polipo
			Tocca nel seguente ordine la bambina il leone la scimmia il pappagallo
			Tocca nel seguente ordine la bambina il leone la scimmia il gelato
			Tocca nel seguente ordine la bambina il leone la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bambina il leone la scimmia il monopattino
			Tocca nel seguente ordine la bambina il leone la scimmia la bicicletta
			Tocca nel seguente ordine la bambina il leone la scimmia l'ombrello
			Tocca nel seguente ordine la bambina il leone il pappagallo il polipo
			Tocca nel seguente ordine la bambina il leone il pappagallo la scimmia
			Tocca nel seguente ordine la bambina il leone il pappagallo il gelato
			Tocca nel seguente ordine la bambina il leone il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bambina il leone il pappagallo il monopattino
			Tocca nel seguente ordine la bambina il leone il pappagallo la bicicletta
			Tocca nel seguente ordine la bambina il leone il pappagallo l'ombrello
			Tocca nel seguente ordine la bambina il leone il polipo la scimmia
			Tocca nel seguente ordine la bambina il leone il polipo il pappagallo
			Tocca nel seguente ordine la bambina il leone il polipo il gelato
			Tocca nel seguente ordine la bambina il leone il polipo gli occhiali da sole
			Tocca nel seguente ordine la bambina il leone il polipo il monopattino
			Tocca nel seguente ordine la bambina il leone il polipo la bicicletta
			Tocca nel seguente ordine la bambina il leone il polipo l'ombrello
			Tocca nel seguente ordine la bambina il leone il gelato il polipo
			Tocca nel seguente ordine la bambina il leone il gelato il pappagallo
			Tocca nel seguente ordine la bambina il leone il gelato la scimmia
			Tocca nel seguente ordine la bambina il leone il gelato gli occhiali da sole
			Tocca nel seguente ordine la bambina il leone il gelato il monopattino
			Tocca nel seguente ordine la bambina il leone il gelato la bicicletta
			Tocca nel seguente ordine la bambina il leone il gelato l'ombrello
			Tocca nel seguente ordine la bambina il leone gli occhiali da sole il polipo
			Tocca nel seguente ordine la bambina il leone gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bambina il leone gli occhiali da sole il gelato
			Tocca nel seguente ordine la bambina il leone gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bambina il leone gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bambina il leone gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la bambina il leone gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bambina il leone la bicicletta il polipo
			Tocca nel seguente ordine la bambina il leone la bicicletta il pappagallo
			Tocca nel seguente ordine la bambina il leone la bicicletta il gelato
			Tocca nel seguente ordine la bambina il leone la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la bambina il leone la bicicletta il monopattino
			Tocca nel seguente ordine la bambina il leone la bicicletta la scimmia
			Tocca nel seguente ordine la bambina il leone la bicicletta l'ombrello
			Tocca nel seguente ordine la bambina il leone il monopattino il polipo
			Tocca nel seguente ordine la bambina il leone il monopattino il pappagallo
			Tocca nel seguente ordine la bambina il leone il monopattino il gelato
			Tocca nel seguente ordine la bambina il leone il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bambina il leone il monopattino la scimmia
			Tocca nel seguente ordine la bambina il leone il monopattino la bicicletta
			Tocca nel seguente ordine la bambina il leone il monopattino l'ombrello
			Tocca nel seguente ordine la bambina il leone l'ombrello il polipo
			Tocca nel seguente ordine la bambina il leone l'ombrello il pappagallo
			Tocca nel seguente ordine la bambina il leone l'ombrello il gelato
			Tocca nel seguente ordine la bambina il leone l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bambina il leone l'ombrello il monopattino
			Tocca nel seguente ordine la bambina il leone l'ombrello la bicicletta
			Tocca nel seguente ordine la bambina il leone l'ombrello la scimmia
			Tocca nel seguente ordine la bambina la scimmia il leone il polipo
			Tocca nel seguente ordine la bambina la scimmia il leone il pappagallo
			Tocca nel seguente ordine la bambina la scimmia il leone il gelato
			Tocca nel seguente ordine la bambina la scimmia il leone gli occhiali da sole
			Tocca nel seguente ordine la bambina la scimmia il leone il monopattino
			Tocca nel seguente ordine la bambina la scimmia il leone la bicicletta
			Tocca nel seguente ordine la bambina la scimmia il leone l'ombrello
			Tocca nel seguente ordine la bambina la scimmia il pappagallo il polipo
			Tocca nel seguente ordine la bambina la scimmia il pappagallo il leone
			Tocca nel seguente ordine la bambina la scimmia il pappagallo il gelato
			Tocca nel seguente ordine la bambina la scimmia il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bambina la scimmia il pappagallo il monopattino
			Tocca nel seguente ordine la bambina la scimmia il pappagallo la bicicletta
			Tocca nel seguente ordine la bambina la scimmia il pappagallo l'ombrello
			Tocca nel seguente ordine la bambina la scimmia il polipo il leone
			Tocca nel seguente ordine la bambina la scimmia il polipo il pappagallo
			Tocca nel seguente ordine la bambina la scimmia il polipo il gelato
			Tocca nel seguente ordine la bambina la scimmia il polipo gli occhiali da sole
			Tocca nel seguente ordine la bambina la scimmia il polipo il monopattino
			Tocca nel seguente ordine la bambina la scimmia il polipo la bicicletta
			Tocca nel seguente ordine la bambina la scimmia il polipo l'ombrello
			Tocca nel seguente ordine la bambina la scimmia il gelato il polipo
			Tocca nel seguente ordine la bambina la scimmia il gelato il pappagallo
			Tocca nel seguente ordine la bambina la scimmia il gelato il leone
			Tocca nel seguente ordine la bambina la scimmia il gelato gli occhiali da sole
			Tocca nel seguente ordine la bambina la scimmia il gelato il monopattino
			Tocca nel seguente ordine la bambina la scimmia il gelato la bicicletta
			Tocca nel seguente ordine la bambina la scimmia il gelato l'ombrello
			Tocca nel seguente ordine la bambina la scimmia gli occhiali da sole il polipo
			Tocca nel seguente ordine la bambina la scimmia gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bambina la scimmia gli occhiali da sole il gelato
			Tocca nel seguente ordine la bambina la scimmia gli occhiali da sole il leone
			Tocca nel seguente ordine la bambina la scimmia gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bambina la scimmia gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la bambina la scimmia gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bambina la scimmia la bicicletta il polipo
			Tocca nel seguente ordine la bambina la scimmia la bicicletta il pappagallo
			Tocca nel seguente ordine la bambina la scimmia la bicicletta il gelato
			Tocca nel seguente ordine la bambina la scimmia la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la bambina la scimmia la bicicletta il monopattino
			Tocca nel seguente ordine la bambina la scimmia la bicicletta il leone
			Tocca nel seguente ordine la bambina la scimmia la bicicletta l'ombrello
			Tocca nel seguente ordine la bambina la scimmia il monopattino il polipo
			Tocca nel seguente ordine la bambina la scimmia il monopattino il pappagallo
			Tocca nel seguente ordine la bambina la scimmia il monopattino il gelato
			Tocca nel seguente ordine la bambina la scimmia il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bambina la scimmia il monopattino il leone
			Tocca nel seguente ordine la bambina la scimmia il monopattino la bicicletta
			Tocca nel seguente ordine la bambina la scimmia il monopattino l'ombrello
			Tocca nel seguente ordine la bambina la scimmia l'ombrello il polipo
			Tocca nel seguente ordine la bambina la scimmia l'ombrello il pappagallo
			Tocca nel seguente ordine la bambina la scimmia l'ombrello il gelato
			Tocca nel seguente ordine la bambina la scimmia l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bambina la scimmia l'ombrello il monopattino
			Tocca nel seguente ordine la bambina la scimmia l'ombrello la bicicletta
			Tocca nel seguente ordine la bambina la scimmia l'ombrello il leone
			Tocca nel seguente ordine la bambina il pappagallo il leone il polipo
			Tocca nel seguente ordine la bambina il pappagallo il leone la scimmia
			Tocca nel seguente ordine la bambina il pappagallo il leone il gelato
			Tocca nel seguente ordine la bambina il pappagallo il leone gli occhiali da sole
			Tocca nel seguente ordine la bambina il pappagallo il leone il monopattino
			Tocca nel seguente ordine la bambina il pappagallo il leone la bicicletta
			Tocca nel seguente ordine la bambina il pappagallo il leone l'ombrello
			Tocca nel seguente ordine la bambina il pappagallo la scimmia il polipo
			Tocca nel seguente ordine la bambina il pappagallo la scimmia il leone
			Tocca nel seguente ordine la bambina il pappagallo la scimmia il gelato
			Tocca nel seguente ordine la bambina il pappagallo la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bambina il pappagallo la scimmia il monopattino
			Tocca nel seguente ordine la bambina il pappagallo la scimmia la bicicletta
			Tocca nel seguente ordine la bambina il pappagallo la scimmia l'ombrello
			Tocca nel seguente ordine la bambina il pappagallo il polipo il leone
			Tocca nel seguente ordine la bambina il pappagallo il polipo la scimmia
			Tocca nel seguente ordine la bambina il pappagallo il polipo il gelato
			Tocca nel seguente ordine la bambina il pappagallo il polipo gli occhiali da sole
			Tocca nel seguente ordine la bambina il pappagallo il polipo il monopattino
			Tocca nel seguente ordine la bambina il pappagallo il polipo la bicicletta
			Tocca nel seguente ordine la bambina il pappagallo il polipo l'ombrello
			Tocca nel seguente ordine la bambina il pappagallo il gelato il polipo
			Tocca nel seguente ordine la bambina il pappagallo il gelato la scimmia
			Tocca nel seguente ordine la bambina il pappagallo il gelato il leone
			Tocca nel seguente ordine la bambina il pappagallo il gelato gli occhiali da sole
			Tocca nel seguente ordine la bambina il pappagallo il gelato il monopattino
			Tocca nel seguente ordine la bambina il pappagallo il gelato la bicicletta
			Tocca nel seguente ordine la bambina il pappagallo il gelato l'ombrello
			Tocca nel seguente ordine la bambina il pappagallo gli occhiali da sole il polipo
			Tocca nel seguente ordine la bambina il pappagallo gli occhiali da sole il leone
			Tocca nel seguente ordine la bambina il pappagallo gli occhiali da sole il gelato
			Tocca nel seguente ordine la bambina il pappagallo gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bambina il pappagallo gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bambina il pappagallo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la bambina il pappagallo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bambina il pappagallo la bicicletta il polipo
			Tocca nel seguente ordine la bambina il pappagallo la bicicletta il leone
			Tocca nel seguente ordine la bambina il pappagallo la bicicletta il gelato
			Tocca nel seguente ordine la bambina il pappagallo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la bambina il pappagallo la bicicletta il monopattino
			Tocca nel seguente ordine la bambina il pappagallo la bicicletta la scimmia
			Tocca nel seguente ordine la bambina il pappagallo la bicicletta l'ombrello
			Tocca nel seguente ordine la bambina il pappagallo il monopattino il polipo
			Tocca nel seguente ordine la bambina il pappagallo il monopattino la scimmia
			Tocca nel seguente ordine la bambina il pappagallo il monopattino il gelato
			Tocca nel seguente ordine la bambina il pappagallo il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bambina il pappagallo il monopattino il leone
			Tocca nel seguente ordine la bambina il pappagallo il monopattino la bicicletta
			Tocca nel seguente ordine la bambina il pappagallo il monopattino l'ombrello
			Tocca nel seguente ordine la bambina il pappagallo l'ombrello il polipo
			Tocca nel seguente ordine la bambina il pappagallo l'ombrello il leone
			Tocca nel seguente ordine la bambina il pappagallo l'ombrello il gelato
			Tocca nel seguente ordine la bambina il pappagallo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bambina il pappagallo l'ombrello il monopattino
			Tocca nel seguente ordine la bambina il pappagallo l'ombrello la bicicletta
			Tocca nel seguente ordine la bambina il pappagallo l'ombrello la scimmia
			Tocca nel seguente ordine la bambina il polipo il leone la scimmia
			Tocca nel seguente ordine la bambina il polipo il leone il pappagallo
			Tocca nel seguente ordine la bambina il polipo il leone il gelato
			Tocca nel seguente ordine la bambina il polipo il leone gli occhiali da sole
			Tocca nel seguente ordine la bambina il polipo il leone il monopattino
			Tocca nel seguente ordine la bambina il polipo il leone la bicicletta
			Tocca nel seguente ordine la bambina il polipo il leone l'ombrello
			Tocca nel seguente ordine la bambina il polipo la scimmia il leone
			Tocca nel seguente ordine la bambina il polipo la scimmia il pappagallo
			Tocca nel seguente ordine la bambina il polipo la scimmia il gelato
			Tocca nel seguente ordine la bambina il polipo la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bambina il polipo la scimmia il monopattino
			Tocca nel seguente ordine la bambina il polipo la scimmia la bicicletta
			Tocca nel seguente ordine la bambina il polipo la scimmia l'ombrello
			Tocca nel seguente ordine la bambina il polipo il pappagallo il leone
			Tocca nel seguente ordine la bambina il polipo il pappagallo la scimmia
			Tocca nel seguente ordine la bambina il polipo il pappagallo il gelato
			Tocca nel seguente ordine la bambina il polipo il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bambina il polipo il pappagallo il monopattino
			Tocca nel seguente ordine la bambina il polipo il pappagallo la bicicletta
			Tocca nel seguente ordine la bambina il polipo il pappagallo l'ombrello
			Tocca nel seguente ordine la bambina il polipo il gelato il leone
			Tocca nel seguente ordine la bambina il polipo il gelato il pappagallo
			Tocca nel seguente ordine la bambina il polipo il gelato la scimmia
			Tocca nel seguente ordine la bambina il polipo il gelato gli occhiali da sole
			Tocca nel seguente ordine la bambina il polipo il gelato il monopattino
			Tocca nel seguente ordine la bambina il polipo il gelato la bicicletta
			Tocca nel seguente ordine la bambina il polipo il gelato l'ombrello
			Tocca nel seguente ordine la bambina il polipo gli occhiali da sole il leone
			Tocca nel seguente ordine la bambina il polipo gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bambina il polipo gli occhiali da sole il gelato
			Tocca nel seguente ordine la bambina il polipo gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bambina il polipo gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bambina il polipo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la bambina il polipo gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bambina il polipo la bicicletta il leone
			Tocca nel seguente ordine la bambina il polipo la bicicletta il pappagallo
			Tocca nel seguente ordine la bambina il polipo la bicicletta il gelato
			Tocca nel seguente ordine la bambina il polipo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la bambina il polipo la bicicletta il monopattino
			Tocca nel seguente ordine la bambina il polipo la bicicletta la scimmia
			Tocca nel seguente ordine la bambina il polipo la bicicletta l'ombrello
			Tocca nel seguente ordine la bambina il polipo il monopattino il leone
			Tocca nel seguente ordine la bambina il polipo il monopattino il pappagallo
			Tocca nel seguente ordine la bambina il polipo il monopattino il gelato
			Tocca nel seguente ordine la bambina il polipo il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bambina il polipo il monopattino la scimmia
			Tocca nel seguente ordine la bambina il polipo il monopattino la bicicletta
			Tocca nel seguente ordine la bambina il polipo il monopattino l'ombrello
			Tocca nel seguente ordine la bambina il polipo l'ombrello il leone
			Tocca nel seguente ordine la bambina il polipo l'ombrello il pappagallo
			Tocca nel seguente ordine la bambina il polipo l'ombrello il gelato
			Tocca nel seguente ordine la bambina il polipo l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bambina il polipo l'ombrello il monopattino
			Tocca nel seguente ordine la bambina il polipo l'ombrello la bicicletta
			Tocca nel seguente ordine la bambina il polipo l'ombrello la scimmia
			Tocca nel seguente ordine la bambina il gelato il leone il polipo
			Tocca nel seguente ordine la bambina il gelato il leone il pappagallo
			Tocca nel seguente ordine la bambina il gelato il leone la scimmia
			Tocca nel seguente ordine la bambina il gelato il leone gli occhiali da sole
			Tocca nel seguente ordine la bambina il gelato il leone il monopattino
			Tocca nel seguente ordine la bambina il gelato il leone la bicicletta
			Tocca nel seguente ordine la bambina il gelato il leone l'ombrello
			Tocca nel seguente ordine la bambina il gelato la scimmia il polipo
			Tocca nel seguente ordine la bambina il gelato la scimmia il pappagallo
			Tocca nel seguente ordine la bambina il gelato la scimmia il leone
			Tocca nel seguente ordine la bambina il gelato la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bambina il gelato la scimmia il monopattino
			Tocca nel seguente ordine la bambina il gelato la scimmia la bicicletta
			Tocca nel seguente ordine la bambina il gelato la scimmia l'ombrello
			Tocca nel seguente ordine la bambina il gelato il pappagallo il polipo
			Tocca nel seguente ordine la bambina il gelato il pappagallo il leone
			Tocca nel seguente ordine la bambina il gelato il pappagallo la scimmia
			Tocca nel seguente ordine la bambina il gelato il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bambina il gelato il pappagallo il monopattino
			Tocca nel seguente ordine la bambina il gelato il pappagallo la bicicletta
			Tocca nel seguente ordine la bambina il gelato il pappagallo l'ombrello
			Tocca nel seguente ordine la bambina il gelato il polipo il leone
			Tocca nel seguente ordine la bambina il gelato il polipo il pappagallo
			Tocca nel seguente ordine la bambina il gelato il polipo la scimmia
			Tocca nel seguente ordine la bambina il gelato il polipo gli occhiali da sole
			Tocca nel seguente ordine la bambina il gelato il polipo il monopattino
			Tocca nel seguente ordine la bambina il gelato il polipo la bicicletta
			Tocca nel seguente ordine la bambina il gelato il polipo l'ombrello
			Tocca nel seguente ordine la bambina il gelato gli occhiali da sole il polipo
			Tocca nel seguente ordine la bambina il gelato gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bambina il gelato gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bambina il gelato gli occhiali da sole il leone
			Tocca nel seguente ordine la bambina il gelato gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bambina il gelato gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la bambina il gelato gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bambina il gelato la bicicletta il polipo
			Tocca nel seguente ordine la bambina il gelato la bicicletta il pappagallo
			Tocca nel seguente ordine la bambina il gelato la bicicletta il leone
			Tocca nel seguente ordine la bambina il gelato la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la bambina il gelato la bicicletta il monopattino
			Tocca nel seguente ordine la bambina il gelato la bicicletta la scimmia
			Tocca nel seguente ordine la bambina il gelato la bicicletta l'ombrello
			Tocca nel seguente ordine la bambina il gelato il monopattino il polipo
			Tocca nel seguente ordine la bambina il gelato il monopattino il pappagallo
			Tocca nel seguente ordine la bambina il gelato il monopattino la scimmia
			Tocca nel seguente ordine la bambina il gelato il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bambina il gelato il monopattino il leone
			Tocca nel seguente ordine la bambina il gelato il monopattino la bicicletta
			Tocca nel seguente ordine la bambina il gelato il monopattino l'ombrello
			Tocca nel seguente ordine la bambina il gelato l'ombrello il polipo
			Tocca nel seguente ordine la bambina il gelato l'ombrello il pappagallo
			Tocca nel seguente ordine la bambina il gelato l'ombrello la scimmia
			Tocca nel seguente ordine la bambina il gelato l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bambina il gelato l'ombrello il monopattino
			Tocca nel seguente ordine la bambina il gelato l'ombrello la bicicletta
			Tocca nel seguente ordine la bambina il gelato l'ombrello il leone
			Tocca nel seguente ordine la bambina gli occhiali da sole il leone il polipo
			Tocca nel seguente ordine la bambina gli occhiali da sole il leone il pappagallo
			Tocca nel seguente ordine la bambina gli occhiali da sole il leone il gelato
			Tocca nel seguente ordine la bambina gli occhiali da sole il leone la scimmia
			Tocca nel seguente ordine la bambina gli occhiali da sole il leone il monopattino
			Tocca nel seguente ordine la bambina gli occhiali da sole il leone la bicicletta
			Tocca nel seguente ordine la bambina gli occhiali da sole il leone l'ombrello
			Tocca nel seguente ordine la bambina gli occhiali da sole la scimmia il polipo
			Tocca nel seguente ordine la bambina gli occhiali da sole la scimmia il pappagallo
			Tocca nel seguente ordine la bambina gli occhiali da sole la scimmia il gelato
			Tocca nel seguente ordine la bambina gli occhiali da sole la scimmia il leone
			Tocca nel seguente ordine la bambina gli occhiali da sole la scimmia il monopattino
			Tocca nel seguente ordine la bambina gli occhiali da sole la scimmia la bicicletta
			Tocca nel seguente ordine la bambina gli occhiali da sole la scimmia l'ombrello
			Tocca nel seguente ordine la bambina gli occhiali da sole il pappagallo il polipo
			Tocca nel seguente ordine la bambina gli occhiali da sole il pappagallo il leone
			Tocca nel seguente ordine la bambina gli occhiali da sole il pappagallo il gelato
			Tocca nel seguente ordine la bambina gli occhiali da sole il pappagallo la scimmia
			Tocca nel seguente ordine la bambina gli occhiali da sole il pappagallo il monopattino
			Tocca nel seguente ordine la bambina gli occhiali da sole il pappagallo la bicicletta
			Tocca nel seguente ordine la bambina gli occhiali da sole il pappagallo l'ombrello
			Tocca nel seguente ordine la bambina gli occhiali da sole il polipo il leone
			Tocca nel seguente ordine la bambina gli occhiali da sole il polipo il pappagallo
			Tocca nel seguente ordine la bambina gli occhiali da sole il polipo il gelato
			Tocca nel seguente ordine la bambina gli occhiali da sole il polipo la scimmia
			Tocca nel seguente ordine la bambina gli occhiali da sole il polipo il monopattino
			Tocca nel seguente ordine la bambina gli occhiali da sole il polipo la bicicletta
			Tocca nel seguente ordine la bambina gli occhiali da sole il polipo l'ombrello
			Tocca nel seguente ordine la bambina gli occhiali da sole il gelato il polipo
			Tocca nel seguente ordine la bambina gli occhiali da sole il gelato il pappagallo
			Tocca nel seguente ordine la bambina gli occhiali da sole il gelato il leone
			Tocca nel seguente ordine la bambina gli occhiali da sole il gelato la scimmia
			Tocca nel seguente ordine la bambina gli occhiali da sole il gelato il monopattino
			Tocca nel seguente ordine la bambina gli occhiali da sole il gelato la bicicletta
			Tocca nel seguente ordine la bambina gli occhiali da sole il gelato l'ombrello
			Tocca nel seguente ordine la bambina gli occhiali da sole la bicicletta il polipo
			Tocca nel seguente ordine la bambina gli occhiali da sole la bicicletta il pappagallo
			Tocca nel seguente ordine la bambina gli occhiali da sole la bicicletta il gelato
			Tocca nel seguente ordine la bambina gli occhiali da sole la bicicletta la scimmia
			Tocca nel seguente ordine la bambina gli occhiali da sole la bicicletta il monopattino
			Tocca nel seguente ordine la bambina gli occhiali da sole la bicicletta il leone
			Tocca nel seguente ordine la bambina gli occhiali da sole la bicicletta l'ombrello
			Tocca nel seguente ordine la bambina gli occhiali da sole il monopattino il polipo
			Tocca nel seguente ordine la bambina gli occhiali da sole il monopattino il pappagallo
			Tocca nel seguente ordine la bambina gli occhiali da sole il monopattino il gelato
			Tocca nel seguente ordine la bambina gli occhiali da sole il monopattino la scimmia
			Tocca nel seguente ordine la bambina gli occhiali da sole il monopattino il leone
			Tocca nel seguente ordine la bambina gli occhiali da sole il monopattino la bicicletta
			Tocca nel seguente ordine la bambina gli occhiali da sole il monopattino l'ombrello
			Tocca nel seguente ordine la bambina gli occhiali da sole l'ombrello il polipo
			Tocca nel seguente ordine la bambina gli occhiali da sole l'ombrello il pappagallo
			Tocca nel seguente ordine la bambina gli occhiali da sole l'ombrello il gelato
			Tocca nel seguente ordine la bambina gli occhiali da sole l'ombrello la scimmia
			Tocca nel seguente ordine la bambina gli occhiali da sole l'ombrello il monopattino
			Tocca nel seguente ordine la bambina gli occhiali da sole l'ombrello la bicicletta
			Tocca nel seguente ordine la bambina gli occhiali da sole l'ombrello il leone
			Tocca nel seguente ordine la bambina la bicicletta il leone il polipo
			Tocca nel seguente ordine la bambina la bicicletta il leone il pappagallo
			Tocca nel seguente ordine la bambina la bicicletta il leone il gelato
			Tocca nel seguente ordine la bambina la bicicletta il leone gli occhiali da sole
			Tocca nel seguente ordine la bambina la bicicletta il leone il monopattino
			Tocca nel seguente ordine la bambina la bicicletta il leone la scimmia
			Tocca nel seguente ordine la bambina la bicicletta il leone l'ombrello
			Tocca nel seguente ordine la bambina la bicicletta la scimmia il polipo
			Tocca nel seguente ordine la bambina la bicicletta la scimmia il pappagallo
			Tocca nel seguente ordine la bambina la bicicletta la scimmia il gelato
			Tocca nel seguente ordine la bambina la bicicletta la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bambina la bicicletta la scimmia il monopattino
			Tocca nel seguente ordine la bambina la bicicletta la scimmia il leone
			Tocca nel seguente ordine la bambina la bicicletta la scimmia l'ombrello
			Tocca nel seguente ordine la bambina la bicicletta il pappagallo il polipo
			Tocca nel seguente ordine la bambina la bicicletta il pappagallo il leone
			Tocca nel seguente ordine la bambina la bicicletta il pappagallo il gelato
			Tocca nel seguente ordine la bambina la bicicletta il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bambina la bicicletta il pappagallo il monopattino
			Tocca nel seguente ordine la bambina la bicicletta il pappagallo la scimmia
			Tocca nel seguente ordine la bambina la bicicletta il pappagallo l'ombrello
			Tocca nel seguente ordine la bambina la bicicletta il polipo il leone
			Tocca nel seguente ordine la bambina la bicicletta il polipo il pappagallo
			Tocca nel seguente ordine la bambina la bicicletta il polipo il gelato
			Tocca nel seguente ordine la bambina la bicicletta il polipo gli occhiali da sole
			Tocca nel seguente ordine la bambina la bicicletta il polipo il monopattino
			Tocca nel seguente ordine la bambina la bicicletta il polipo la scimmia
			Tocca nel seguente ordine la bambina la bicicletta il polipo l'ombrello
			Tocca nel seguente ordine la bambina la bicicletta il gelato il polipo
			Tocca nel seguente ordine la bambina la bicicletta il gelato il pappagallo
			Tocca nel seguente ordine la bambina la bicicletta il gelato il leone
			Tocca nel seguente ordine la bambina la bicicletta il gelato gli occhiali da sole
			Tocca nel seguente ordine la bambina la bicicletta il gelato il monopattino
			Tocca nel seguente ordine la bambina la bicicletta il gelato la scimmia
			Tocca nel seguente ordine la bambina la bicicletta il gelato l'ombrello
			Tocca nel seguente ordine la bambina la bicicletta gli occhiali da sole il polipo
			Tocca nel seguente ordine la bambina la bicicletta gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bambina la bicicletta gli occhiali da sole il gelato
			Tocca nel seguente ordine la bambina la bicicletta gli occhiali da sole il leone
			Tocca nel seguente ordine la bambina la bicicletta gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bambina la bicicletta gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bambina la bicicletta gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bambina la bicicletta il monopattino il polipo
			Tocca nel seguente ordine la bambina la bicicletta il monopattino il pappagallo
			Tocca nel seguente ordine la bambina la bicicletta il monopattino il gelato
			Tocca nel seguente ordine la bambina la bicicletta il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bambina la bicicletta il monopattino il leone
			Tocca nel seguente ordine la bambina la bicicletta il monopattino la scimmia
			Tocca nel seguente ordine la bambina la bicicletta il monopattino l'ombrello
			Tocca nel seguente ordine la bambina la bicicletta l'ombrello il polipo
			Tocca nel seguente ordine la bambina la bicicletta l'ombrello il pappagallo
			Tocca nel seguente ordine la bambina la bicicletta l'ombrello il gelato
			Tocca nel seguente ordine la bambina la bicicletta l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bambina la bicicletta l'ombrello il monopattino
			Tocca nel seguente ordine la bambina la bicicletta l'ombrello la scimmia
			Tocca nel seguente ordine la bambina la bicicletta l'ombrello il leone
			Tocca nel seguente ordine la bambina il monopattino il leone il polipo
			Tocca nel seguente ordine la bambina il monopattino il leone il pappagallo
			Tocca nel seguente ordine la bambina il monopattino il leone il gelato
			Tocca nel seguente ordine la bambina il monopattino il leone gli occhiali da sole
			Tocca nel seguente ordine la bambina il monopattino il leone la scimmia
			Tocca nel seguente ordine la bambina il monopattino il leone la bicicletta
			Tocca nel seguente ordine la bambina il monopattino il leone l'ombrello
			Tocca nel seguente ordine la bambina il monopattino la scimmia il polipo
			Tocca nel seguente ordine la bambina il monopattino la scimmia il pappagallo
			Tocca nel seguente ordine la bambina il monopattino la scimmia il gelato
			Tocca nel seguente ordine la bambina il monopattino la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bambina il monopattino la scimmia il leone
			Tocca nel seguente ordine la bambina il monopattino la scimmia la bicicletta
			Tocca nel seguente ordine la bambina il monopattino la scimmia l'ombrello
			Tocca nel seguente ordine la bambina il monopattino il pappagallo il polipo
			Tocca nel seguente ordine la bambina il monopattino il pappagallo il leone
			Tocca nel seguente ordine la bambina il monopattino il pappagallo il gelato
			Tocca nel seguente ordine la bambina il monopattino il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bambina il monopattino il pappagallo la scimmia
			Tocca nel seguente ordine la bambina il monopattino il pappagallo la bicicletta
			Tocca nel seguente ordine la bambina il monopattino il pappagallo l'ombrello
			Tocca nel seguente ordine la bambina il monopattino il polipo il leone
			Tocca nel seguente ordine la bambina il monopattino il polipo il pappagallo
			Tocca nel seguente ordine la bambina il monopattino il polipo il gelato
			Tocca nel seguente ordine la bambina il monopattino il polipo gli occhiali da sole
			Tocca nel seguente ordine la bambina il monopattino il polipo la scimmia
			Tocca nel seguente ordine la bambina il monopattino il polipo la bicicletta
			Tocca nel seguente ordine la bambina il monopattino il polipo l'ombrello
			Tocca nel seguente ordine la bambina il monopattino il gelato il polipo
			Tocca nel seguente ordine la bambina il monopattino il gelato il pappagallo
			Tocca nel seguente ordine la bambina il monopattino il gelato il leone
			Tocca nel seguente ordine la bambina il monopattino il gelato gli occhiali da sole
			Tocca nel seguente ordine la bambina il monopattino il gelato la scimmia
			Tocca nel seguente ordine la bambina il monopattino il gelato la bicicletta
			Tocca nel seguente ordine la bambina il monopattino il gelato l'ombrello
			Tocca nel seguente ordine la bambina il monopattino gli occhiali da sole il polipo
			Tocca nel seguente ordine la bambina il monopattino gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bambina il monopattino gli occhiali da sole il gelato
			Tocca nel seguente ordine la bambina il monopattino gli occhiali da sole il leone
			Tocca nel seguente ordine la bambina il monopattino gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bambina il monopattino gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la bambina il monopattino gli occhiali da sole l'ombrello
			Tocca nel seguente ordine la bambina il monopattino la bicicletta il polipo
			Tocca nel seguente ordine la bambina il monopattino la bicicletta il pappagallo
			Tocca nel seguente ordine la bambina il monopattino la bicicletta il gelato
			Tocca nel seguente ordine la bambina il monopattino la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la bambina il monopattino la bicicletta la scimmia
			Tocca nel seguente ordine la bambina il monopattino la bicicletta il leone
			Tocca nel seguente ordine la bambina il monopattino la bicicletta l'ombrello
			Tocca nel seguente ordine la bambina il monopattino l'ombrello il polipo
			Tocca nel seguente ordine la bambina il monopattino l'ombrello il pappagallo
			Tocca nel seguente ordine la bambina il monopattino l'ombrello il gelato
			Tocca nel seguente ordine la bambina il monopattino l'ombrello gli occhiali da sole
			Tocca nel seguente ordine la bambina il monopattino l'ombrello la scimmia
			Tocca nel seguente ordine la bambina il monopattino l'ombrello la bicicletta
			Tocca nel seguente ordine la bambina il monopattino l'ombrello il leone
			Tocca nel seguente ordine la bambina l'ombrello il leone il polipo
			Tocca nel seguente ordine la bambina l'ombrello il leone il pappagallo
			Tocca nel seguente ordine la bambina l'ombrello il leone il gelato
			Tocca nel seguente ordine la bambina l'ombrello il leone gli occhiali da sole
			Tocca nel seguente ordine la bambina l'ombrello il leone il monopattino
			Tocca nel seguente ordine la bambina l'ombrello il leone la bicicletta
			Tocca nel seguente ordine la bambina l'ombrello il leone la scimmia
			Tocca nel seguente ordine la bambina l'ombrello la scimmia il polipo
			Tocca nel seguente ordine la bambina l'ombrello la scimmia il pappagallo
			Tocca nel seguente ordine la bambina l'ombrello la scimmia il gelato
			Tocca nel seguente ordine la bambina l'ombrello la scimmia gli occhiali da sole
			Tocca nel seguente ordine la bambina l'ombrello la scimmia il monopattino
			Tocca nel seguente ordine la bambina l'ombrello la scimmia la bicicletta
			Tocca nel seguente ordine la bambina l'ombrello la scimmia il leone
			Tocca nel seguente ordine la bambina l'ombrello il pappagallo il polipo
			Tocca nel seguente ordine la bambina l'ombrello il pappagallo il leone
			Tocca nel seguente ordine la bambina l'ombrello il pappagallo il gelato
			Tocca nel seguente ordine la bambina l'ombrello il pappagallo gli occhiali da sole
			Tocca nel seguente ordine la bambina l'ombrello il pappagallo il monopattino
			Tocca nel seguente ordine la bambina l'ombrello il pappagallo la bicicletta
			Tocca nel seguente ordine la bambina l'ombrello il pappagallo la scimmia
			Tocca nel seguente ordine la bambina l'ombrello il polipo il leone
			Tocca nel seguente ordine la bambina l'ombrello il polipo il pappagallo
			Tocca nel seguente ordine la bambina l'ombrello il polipo il gelato
			Tocca nel seguente ordine la bambina l'ombrello il polipo gli occhiali da sole
			Tocca nel seguente ordine la bambina l'ombrello il polipo il monopattino
			Tocca nel seguente ordine la bambina l'ombrello il polipo la bicicletta
			Tocca nel seguente ordine la bambina l'ombrello il polipo la scimmia
			Tocca nel seguente ordine la bambina l'ombrello il gelato il polipo
			Tocca nel seguente ordine la bambina l'ombrello il gelato il pappagallo
			Tocca nel seguente ordine la bambina l'ombrello il gelato il leone
			Tocca nel seguente ordine la bambina l'ombrello il gelato gli occhiali da sole
			Tocca nel seguente ordine la bambina l'ombrello il gelato il monopattino
			Tocca nel seguente ordine la bambina l'ombrello il gelato la bicicletta
			Tocca nel seguente ordine la bambina l'ombrello il gelato la scimmia
			Tocca nel seguente ordine la bambina l'ombrello gli occhiali da sole il polipo
			Tocca nel seguente ordine la bambina l'ombrello gli occhiali da sole il pappagallo
			Tocca nel seguente ordine la bambina l'ombrello gli occhiali da sole il gelato
			Tocca nel seguente ordine la bambina l'ombrello gli occhiali da sole il leone
			Tocca nel seguente ordine la bambina l'ombrello gli occhiali da sole il monopattino
			Tocca nel seguente ordine la bambina l'ombrello gli occhiali da sole la bicicletta
			Tocca nel seguente ordine la bambina l'ombrello gli occhiali da sole la scimmia
			Tocca nel seguente ordine la bambina l'ombrello la bicicletta il polipo
			Tocca nel seguente ordine la bambina l'ombrello la bicicletta il pappagallo
			Tocca nel seguente ordine la bambina l'ombrello la bicicletta il gelato
			Tocca nel seguente ordine la bambina l'ombrello la bicicletta gli occhiali da sole
			Tocca nel seguente ordine la bambina l'ombrello la bicicletta il monopattino
			Tocca nel seguente ordine la bambina l'ombrello la bicicletta il leone
			Tocca nel seguente ordine la bambina l'ombrello la bicicletta la scimmia
			Tocca nel seguente ordine la bambina l'ombrello il monopattino il polipo
			Tocca nel seguente ordine la bambina l'ombrello il monopattino il pappagallo
			Tocca nel seguente ordine la bambina l'ombrello il monopattino il gelato
			Tocca nel seguente ordine la bambina l'ombrello il monopattino gli occhiali da sole
			Tocca nel seguente ordine la bambina l'ombrello il monopattino il leone
			Tocca nel seguente ordine la bambina l'ombrello il monopattino la bicicletta
			Tocca nel seguente ordine la bambina l'ombrello il monopattino la scimmia
			Tocca nel seguente ordine l'ombrello il leone la scimmia il polipo
			Tocca nel seguente ordine l'ombrello il leone la scimmia il pappagallo
			Tocca nel seguente ordine l'ombrello il leone la scimmia il gelato
			Tocca nel seguente ordine l'ombrello il leone la scimmia gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il leone la scimmia la bicicletta
			Tocca nel seguente ordine l'ombrello il leone la scimmia il monopattino
			Tocca nel seguente ordine l'ombrello il leone la scimmia la bambina
			Tocca nel seguente ordine l'ombrello il leone il pappagallo il polipo
			Tocca nel seguente ordine l'ombrello il leone il pappagallo la scimmia
			Tocca nel seguente ordine l'ombrello il leone il pappagallo il gelato
			Tocca nel seguente ordine l'ombrello il leone il pappagallo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il leone il pappagallo la bicicletta
			Tocca nel seguente ordine l'ombrello il leone il pappagallo il monopattino
			Tocca nel seguente ordine l'ombrello il leone il pappagallo la bambina
			Tocca nel seguente ordine l'ombrello il leone il polipo la scimmia
			Tocca nel seguente ordine l'ombrello il leone il polipo il pappagallo
			Tocca nel seguente ordine l'ombrello il leone il polipo il gelato
			Tocca nel seguente ordine l'ombrello il leone il polipo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il leone il polipo la bicicletta
			Tocca nel seguente ordine l'ombrello il leone il polipo il monopattino
			Tocca nel seguente ordine l'ombrello il leone il polipo la bambina
			Tocca nel seguente ordine l'ombrello il leone il gelato il polipo
			Tocca nel seguente ordine l'ombrello il leone il gelato il pappagallo
			Tocca nel seguente ordine l'ombrello il leone il gelato la scimmia
			Tocca nel seguente ordine l'ombrello il leone il gelato gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il leone il gelato la bicicletta
			Tocca nel seguente ordine l'ombrello il leone il gelato il monopattino
			Tocca nel seguente ordine l'ombrello il leone il gelato la bambina
			Tocca nel seguente ordine l'ombrello il leone gli occhiali da sole il polipo
			Tocca nel seguente ordine l'ombrello il leone gli occhiali da sole il pappagallo
			Tocca nel seguente ordine l'ombrello il leone gli occhiali da sole il gelato
			Tocca nel seguente ordine l'ombrello il leone gli occhiali da sole la scimmia
			Tocca nel seguente ordine l'ombrello il leone gli occhiali da sole la bicicletta
			Tocca nel seguente ordine l'ombrello il leone gli occhiali da sole il monopattino
			Tocca nel seguente ordine l'ombrello il leone gli occhiali da sole la bambina
			Tocca nel seguente ordine l'ombrello il leone la bicicletta il polipo
			Tocca nel seguente ordine l'ombrello il leone la bicicletta il pappagallo
			Tocca nel seguente ordine l'ombrello il leone la bicicletta il gelato
			Tocca nel seguente ordine l'ombrello il leone la bicicletta gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il leone la bicicletta la scimmia
			Tocca nel seguente ordine l'ombrello il leone la bicicletta il monopattino
			Tocca nel seguente ordine l'ombrello il leone la bicicletta la bambina
			Tocca nel seguente ordine l'ombrello il leone il monopattino il polipo
			Tocca nel seguente ordine l'ombrello il leone il monopattino il pappagallo
			Tocca nel seguente ordine l'ombrello il leone il monopattino il gelato
			Tocca nel seguente ordine l'ombrello il leone il monopattino gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il leone il monopattino la bicicletta
			Tocca nel seguente ordine l'ombrello il leone il monopattino la scimmia
			Tocca nel seguente ordine l'ombrello il leone il monopattino la bambina
			Tocca nel seguente ordine l'ombrello il leone la bambina il polipo
			Tocca nel seguente ordine l'ombrello il leone la bambina il pappagallo
			Tocca nel seguente ordine l'ombrello il leone la bambina il gelato
			Tocca nel seguente ordine l'ombrello il leone la bambina gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il leone la bambina la bicicletta
			Tocca nel seguente ordine l'ombrello il leone la bambina il monopattino
			Tocca nel seguente ordine l'ombrello il leone la bambina la scimmia
			Tocca nel seguente ordine l'ombrello la scimmia il leone il polipo
			Tocca nel seguente ordine l'ombrello la scimmia il leone il pappagallo
			Tocca nel seguente ordine l'ombrello la scimmia il leone il gelato
			Tocca nel seguente ordine l'ombrello la scimmia il leone gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la scimmia il leone la bicicletta
			Tocca nel seguente ordine l'ombrello la scimmia il leone il monopattino
			Tocca nel seguente ordine l'ombrello la scimmia il leone la bambina
			Tocca nel seguente ordine l'ombrello la scimmia il pappagallo il polipo
			Tocca nel seguente ordine l'ombrello la scimmia il pappagallo il leone
			Tocca nel seguente ordine l'ombrello la scimmia il pappagallo il gelato
			Tocca nel seguente ordine l'ombrello la scimmia il pappagallo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la scimmia il pappagallo la bicicletta
			Tocca nel seguente ordine l'ombrello la scimmia il pappagallo il monopattino
			Tocca nel seguente ordine l'ombrello la scimmia il pappagallo la bambina
			Tocca nel seguente ordine l'ombrello la scimmia il polipo il leone
			Tocca nel seguente ordine l'ombrello la scimmia il polipo il pappagallo
			Tocca nel seguente ordine l'ombrello la scimmia il polipo il gelato
			Tocca nel seguente ordine l'ombrello la scimmia il polipo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la scimmia il polipo la bicicletta
			Tocca nel seguente ordine l'ombrello la scimmia il polipo il monopattino
			Tocca nel seguente ordine l'ombrello la scimmia il polipo la bambina
			Tocca nel seguente ordine l'ombrello la scimmia il gelato il polipo
			Tocca nel seguente ordine l'ombrello la scimmia il gelato il pappagallo
			Tocca nel seguente ordine l'ombrello la scimmia il gelato il leone
			Tocca nel seguente ordine l'ombrello la scimmia il gelato gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la scimmia il gelato la bicicletta
			Tocca nel seguente ordine l'ombrello la scimmia il gelato il monopattino
			Tocca nel seguente ordine l'ombrello la scimmia il gelato la bambina
			Tocca nel seguente ordine l'ombrello la scimmia gli occhiali da sole il polipo
			Tocca nel seguente ordine l'ombrello la scimmia gli occhiali da sole il pappagallo
			Tocca nel seguente ordine l'ombrello la scimmia gli occhiali da sole il gelato
			Tocca nel seguente ordine l'ombrello la scimmia gli occhiali da sole il leone
			Tocca nel seguente ordine l'ombrello la scimmia gli occhiali da sole la bicicletta
			Tocca nel seguente ordine l'ombrello la scimmia gli occhiali da sole il monopattino
			Tocca nel seguente ordine l'ombrello la scimmia gli occhiali da sole la bambina
			Tocca nel seguente ordine l'ombrello la scimmia la bicicletta il polipo
			Tocca nel seguente ordine l'ombrello la scimmia la bicicletta il pappagallo
			Tocca nel seguente ordine l'ombrello la scimmia la bicicletta il gelato
			Tocca nel seguente ordine l'ombrello la scimmia la bicicletta gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la scimmia la bicicletta il leone
			Tocca nel seguente ordine l'ombrello la scimmia la bicicletta il monopattino
			Tocca nel seguente ordine l'ombrello la scimmia la bicicletta la bambina
			Tocca nel seguente ordine l'ombrello la scimmia il monopattino il polipo
			Tocca nel seguente ordine l'ombrello la scimmia il monopattino il pappagallo
			Tocca nel seguente ordine l'ombrello la scimmia il monopattino il gelato
			Tocca nel seguente ordine l'ombrello la scimmia il monopattino gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la scimmia il monopattino la bicicletta
			Tocca nel seguente ordine l'ombrello la scimmia il monopattino il leone
			Tocca nel seguente ordine l'ombrello la scimmia il monopattino la bambina
			Tocca nel seguente ordine l'ombrello la scimmia la bambina il polipo
			Tocca nel seguente ordine l'ombrello la scimmia la bambina il pappagallo
			Tocca nel seguente ordine l'ombrello la scimmia la bambina il gelato
			Tocca nel seguente ordine l'ombrello la scimmia la bambina gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la scimmia la bambina la bicicletta
			Tocca nel seguente ordine l'ombrello la scimmia la bambina il monopattino
			Tocca nel seguente ordine l'ombrello la scimmia la bambina il leone
			Tocca nel seguente ordine l'ombrello il pappagallo il leone il polipo
			Tocca nel seguente ordine l'ombrello il pappagallo il leone la scimmia
			Tocca nel seguente ordine l'ombrello il pappagallo il leone il gelato
			Tocca nel seguente ordine l'ombrello il pappagallo il leone gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il pappagallo il leone la bicicletta
			Tocca nel seguente ordine l'ombrello il pappagallo il leone il monopattino
			Tocca nel seguente ordine l'ombrello il pappagallo il leone la bambina
			Tocca nel seguente ordine l'ombrello il pappagallo la scimmia il polipo
			Tocca nel seguente ordine l'ombrello il pappagallo la scimmia il leone
			Tocca nel seguente ordine l'ombrello il pappagallo la scimmia il gelato
			Tocca nel seguente ordine l'ombrello il pappagallo la scimmia gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il pappagallo la scimmia la bicicletta
			Tocca nel seguente ordine l'ombrello il pappagallo la scimmia il monopattino
			Tocca nel seguente ordine l'ombrello il pappagallo la scimmia la bambina
			Tocca nel seguente ordine l'ombrello il pappagallo il polipo il leone
			Tocca nel seguente ordine l'ombrello il pappagallo il polipo la scimmia
			Tocca nel seguente ordine l'ombrello il pappagallo il polipo il gelato
			Tocca nel seguente ordine l'ombrello il pappagallo il polipo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il pappagallo il polipo la bicicletta
			Tocca nel seguente ordine l'ombrello il pappagallo il polipo il monopattino
			Tocca nel seguente ordine l'ombrello il pappagallo il polipo la bambina
			Tocca nel seguente ordine l'ombrello il pappagallo il gelato il polipo
			Tocca nel seguente ordine l'ombrello il pappagallo il gelato la scimmia
			Tocca nel seguente ordine l'ombrello il pappagallo il gelato il leone
			Tocca nel seguente ordine l'ombrello il pappagallo il gelato gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il pappagallo il gelato la bicicletta
			Tocca nel seguente ordine l'ombrello il pappagallo il gelato il monopattino
			Tocca nel seguente ordine l'ombrello il pappagallo il gelato la bambina
			Tocca nel seguente ordine l'ombrello il pappagallo gli occhiali da sole il polipo
			Tocca nel seguente ordine l'ombrello il pappagallo gli occhiali da sole la scimmia
			Tocca nel seguente ordine l'ombrello il pappagallo gli occhiali da sole il gelato
			Tocca nel seguente ordine l'ombrello il pappagallo gli occhiali da sole il leone
			Tocca nel seguente ordine l'ombrello il pappagallo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine l'ombrello il pappagallo gli occhiali da sole il monopattino
			Tocca nel seguente ordine l'ombrello il pappagallo gli occhiali da sole la bambina
			Tocca nel seguente ordine l'ombrello il pappagallo la bicicletta il polipo
			Tocca nel seguente ordine l'ombrello il pappagallo la bicicletta il leone
			Tocca nel seguente ordine l'ombrello il pappagallo la bicicletta il gelato
			Tocca nel seguente ordine l'ombrello il pappagallo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il pappagallo la bicicletta la scimmia
			Tocca nel seguente ordine l'ombrello il pappagallo la bicicletta il monopattino
			Tocca nel seguente ordine l'ombrello il pappagallo la bicicletta la bambina
			Tocca nel seguente ordine l'ombrello il pappagallo il monopattino il polipo
			Tocca nel seguente ordine l'ombrello il pappagallo il monopattino il leone
			Tocca nel seguente ordine l'ombrello il pappagallo il monopattino il gelato
			Tocca nel seguente ordine l'ombrello il pappagallo il monopattino gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il pappagallo il monopattino la bicicletta
			Tocca nel seguente ordine l'ombrello il pappagallo il monopattino la scimmia
			Tocca nel seguente ordine l'ombrello il pappagallo il monopattino la bambina
			Tocca nel seguente ordine l'ombrello il pappagallo la bambina il polipo
			Tocca nel seguente ordine l'ombrello il pappagallo la bambina il leone
			Tocca nel seguente ordine l'ombrello il pappagallo la bambina il gelato
			Tocca nel seguente ordine l'ombrello il pappagallo la bambina gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il pappagallo la bambina la bicicletta
			Tocca nel seguente ordine l'ombrello il pappagallo la bambina il monopattino
			Tocca nel seguente ordine l'ombrello il pappagallo la bambina la scimmia
			Tocca nel seguente ordine l'ombrello il polipo il leone la scimmia
			Tocca nel seguente ordine l'ombrello il polipo il leone il pappagallo
			Tocca nel seguente ordine l'ombrello il polipo il leone il gelato
			Tocca nel seguente ordine l'ombrello il polipo il leone gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il polipo il leone la bicicletta
			Tocca nel seguente ordine l'ombrello il polipo il leone il monopattino
			Tocca nel seguente ordine l'ombrello il polipo il leone la bambina
			Tocca nel seguente ordine l'ombrello il polipo la scimmia il leone
			Tocca nel seguente ordine l'ombrello il polipo la scimmia il pappagallo
			Tocca nel seguente ordine l'ombrello il polipo la scimmia il gelato
			Tocca nel seguente ordine l'ombrello il polipo la scimmia gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il polipo la scimmia la bicicletta
			Tocca nel seguente ordine l'ombrello il polipo la scimmia il monopattino
			Tocca nel seguente ordine l'ombrello il polipo la scimmia la bambina
			Tocca nel seguente ordine l'ombrello il polipo il pappagallo il leone
			Tocca nel seguente ordine l'ombrello il polipo il pappagallo la scimmia
			Tocca nel seguente ordine l'ombrello il polipo il pappagallo il gelato
			Tocca nel seguente ordine l'ombrello il polipo il pappagallo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il polipo il pappagallo la bicicletta
			Tocca nel seguente ordine l'ombrello il polipo il pappagallo il monopattino
			Tocca nel seguente ordine l'ombrello il polipo il pappagallo la bambina
			Tocca nel seguente ordine l'ombrello il polipo il gelato il leone
			Tocca nel seguente ordine l'ombrello il polipo il gelato il pappagallo
			Tocca nel seguente ordine l'ombrello il polipo il gelato la scimmia
			Tocca nel seguente ordine l'ombrello il polipo il gelato gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il polipo il gelato la bicicletta
			Tocca nel seguente ordine l'ombrello il polipo il gelato il monopattino
			Tocca nel seguente ordine l'ombrello il polipo il gelato la bambina
			Tocca nel seguente ordine l'ombrello il polipo gli occhiali da sole il leone
			Tocca nel seguente ordine l'ombrello il polipo gli occhiali da sole il pappagallo
			Tocca nel seguente ordine l'ombrello il polipo gli occhiali da sole il gelato
			Tocca nel seguente ordine l'ombrello il polipo gli occhiali da sole la scimmia
			Tocca nel seguente ordine l'ombrello il polipo gli occhiali da sole la bicicletta
			Tocca nel seguente ordine l'ombrello il polipo gli occhiali da sole il monopattino
			Tocca nel seguente ordine l'ombrello il polipo gli occhiali da sole la bambina
			Tocca nel seguente ordine l'ombrello il polipo la bicicletta il leone
			Tocca nel seguente ordine l'ombrello il polipo la bicicletta il pappagallo
			Tocca nel seguente ordine l'ombrello il polipo la bicicletta il gelato
			Tocca nel seguente ordine l'ombrello il polipo la bicicletta gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il polipo la bicicletta la scimmia
			Tocca nel seguente ordine l'ombrello il polipo la bicicletta il monopattino
			Tocca nel seguente ordine l'ombrello il polipo la bicicletta la bambina
			Tocca nel seguente ordine l'ombrello il polipo il monopattino il leone
			Tocca nel seguente ordine l'ombrello il polipo il monopattino il pappagallo
			Tocca nel seguente ordine l'ombrello il polipo il monopattino il gelato
			Tocca nel seguente ordine l'ombrello il polipo il monopattino gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il polipo il monopattino la bicicletta
			Tocca nel seguente ordine l'ombrello il polipo il monopattino la scimmia
			Tocca nel seguente ordine l'ombrello il polipo il monopattino la bambina
			Tocca nel seguente ordine l'ombrello il polipo la bambina il leone
			Tocca nel seguente ordine l'ombrello il polipo la bambina il pappagallo
			Tocca nel seguente ordine l'ombrello il polipo la bambina il gelato
			Tocca nel seguente ordine l'ombrello il polipo la bambina gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il polipo la bambina la bicicletta
			Tocca nel seguente ordine l'ombrello il polipo la bambina il monopattino
			Tocca nel seguente ordine l'ombrello il polipo la bambina la scimmia
			Tocca nel seguente ordine l'ombrello il gelato il leone il polipo
			Tocca nel seguente ordine l'ombrello il gelato il leone il pappagallo
			Tocca nel seguente ordine l'ombrello il gelato il leone la scimmia
			Tocca nel seguente ordine l'ombrello il gelato il leone gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il gelato il leone la bicicletta
			Tocca nel seguente ordine l'ombrello il gelato il leone il monopattino
			Tocca nel seguente ordine l'ombrello il gelato il leone la bambina
			Tocca nel seguente ordine l'ombrello il gelato la scimmia il polipo
			Tocca nel seguente ordine l'ombrello il gelato la scimmia il pappagallo
			Tocca nel seguente ordine l'ombrello il gelato la scimmia il leone
			Tocca nel seguente ordine l'ombrello il gelato la scimmia gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il gelato la scimmia la bicicletta
			Tocca nel seguente ordine l'ombrello il gelato la scimmia il monopattino
			Tocca nel seguente ordine l'ombrello il gelato la scimmia la bambina
			Tocca nel seguente ordine l'ombrello il gelato il pappagallo il polipo
			Tocca nel seguente ordine l'ombrello il gelato il pappagallo la scimmia
			Tocca nel seguente ordine l'ombrello il gelato il pappagallo il leone
			Tocca nel seguente ordine l'ombrello il gelato il pappagallo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il gelato il pappagallo la bicicletta
			Tocca nel seguente ordine l'ombrello il gelato il pappagallo il monopattino
			Tocca nel seguente ordine l'ombrello il gelato il pappagallo la bambina
			Tocca nel seguente ordine l'ombrello il gelato il polipo la scimmia
			Tocca nel seguente ordine l'ombrello il gelato il polipo il pappagallo
			Tocca nel seguente ordine l'ombrello il gelato il polipo il leone
			Tocca nel seguente ordine l'ombrello il gelato il polipo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il gelato il polipo la bicicletta
			Tocca nel seguente ordine l'ombrello il gelato il polipo il monopattino
			Tocca nel seguente ordine l'ombrello il gelato il polipo la bambina
			Tocca nel seguente ordine l'ombrello il gelato gli occhiali da sole il polipo
			Tocca nel seguente ordine l'ombrello il gelato gli occhiali da sole il pappagallo
			Tocca nel seguente ordine l'ombrello il gelato gli occhiali da sole il leone
			Tocca nel seguente ordine l'ombrello il gelato gli occhiali da sole la scimmia
			Tocca nel seguente ordine l'ombrello il gelato gli occhiali da sole la bicicletta
			Tocca nel seguente ordine l'ombrello il gelato gli occhiali da sole il monopattino
			Tocca nel seguente ordine l'ombrello il gelato gli occhiali da sole la bambina
			Tocca nel seguente ordine l'ombrello il gelato la bicicletta il polipo
			Tocca nel seguente ordine l'ombrello il gelato la bicicletta il pappagallo
			Tocca nel seguente ordine l'ombrello il gelato la bicicletta il leone
			Tocca nel seguente ordine l'ombrello il gelato la bicicletta gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il gelato la bicicletta la scimmia
			Tocca nel seguente ordine l'ombrello il gelato la bicicletta il monopattino
			Tocca nel seguente ordine l'ombrello il gelato la bicicletta la bambina
			Tocca nel seguente ordine l'ombrello il gelato il monopattino il polipo
			Tocca nel seguente ordine l'ombrello il gelato il monopattino il pappagallo
			Tocca nel seguente ordine l'ombrello il gelato il monopattino il leone
			Tocca nel seguente ordine l'ombrello il gelato il monopattino gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il gelato il monopattino la bicicletta
			Tocca nel seguente ordine l'ombrello il gelato il monopattino la scimmia
			Tocca nel seguente ordine l'ombrello il gelato il monopattino la bambina
			Tocca nel seguente ordine l'ombrello il gelato la bambina il polipo
			Tocca nel seguente ordine l'ombrello il gelato la bambina il pappagallo
			Tocca nel seguente ordine l'ombrello il gelato la bambina il leone
			Tocca nel seguente ordine l'ombrello il gelato la bambina gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il gelato la bambina la bicicletta
			Tocca nel seguente ordine l'ombrello il gelato la bambina il monopattino
			Tocca nel seguente ordine l'ombrello il gelato la bambina la scimmia
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il leone il polipo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il leone il pappagallo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il leone il gelato
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il leone la scimmia
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il leone la bicicletta
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il leone il monopattino
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il leone la bambina
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la scimmia il polipo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la scimmia il pappagallo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la scimmia il gelato
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la scimmia il leone
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la scimmia la bicicletta
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la scimmia il monopattino
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la scimmia la bambina
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il pappagallo il polipo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il pappagallo il leone
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il pappagallo il gelato
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il pappagallo la scimmia
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il pappagallo la bicicletta
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il pappagallo il monopattino
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il pappagallo la bambina
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il polipo il leone
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il polipo il pappagallo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il polipo il gelato
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il polipo la scimmia
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il polipo la bicicletta
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il polipo il monopattino
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il polipo la bambina
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il gelato il polipo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il gelato il pappagallo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il gelato il leone
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il gelato la scimmia
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il gelato la bicicletta
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il gelato il monopattino
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il gelato la bambina
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bicicletta il polipo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bicicletta il pappagallo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bicicletta il gelato
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bicicletta la scimmia
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bicicletta il leone
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bicicletta il monopattino
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bicicletta la bambina
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il monopattino il polipo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il monopattino il pappagallo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il monopattino il gelato
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il monopattino la scimmia
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il monopattino la bicicletta
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il monopattino il leone
			Tocca nel seguente ordine l'ombrello gli occhiali da sole il monopattino la bambina
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bambina il polipo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bambina il pappagallo
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bambina il gelato
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bambina la scimmia
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bambina la bicicletta
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bambina il monopattino
			Tocca nel seguente ordine l'ombrello gli occhiali da sole la bambina il leone
			Tocca nel seguente ordine l'ombrello la bicicletta il leone il polipo
			Tocca nel seguente ordine l'ombrello la bicicletta il leone il pappagallo
			Tocca nel seguente ordine l'ombrello la bicicletta il leone il gelato
			Tocca nel seguente ordine l'ombrello la bicicletta il leone gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bicicletta il leone la scimmia
			Tocca nel seguente ordine l'ombrello la bicicletta il leone il monopattino
			Tocca nel seguente ordine l'ombrello la bicicletta il leone la bambina
			Tocca nel seguente ordine l'ombrello la bicicletta la scimmia il polipo
			Tocca nel seguente ordine l'ombrello la bicicletta la scimmia il pappagallo
			Tocca nel seguente ordine l'ombrello la bicicletta la scimmia il gelato
			Tocca nel seguente ordine l'ombrello la bicicletta la scimmia gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bicicletta la scimmia il leone
			Tocca nel seguente ordine l'ombrello la bicicletta la scimmia il monopattino
			Tocca nel seguente ordine l'ombrello la bicicletta la scimmia la bambina
			Tocca nel seguente ordine l'ombrello la bicicletta il pappagallo il polipo
			Tocca nel seguente ordine l'ombrello la bicicletta il pappagallo il leone
			Tocca nel seguente ordine l'ombrello la bicicletta il pappagallo il gelato
			Tocca nel seguente ordine l'ombrello la bicicletta il pappagallo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bicicletta il pappagallo la scimmia
			Tocca nel seguente ordine l'ombrello la bicicletta il pappagallo il monopattino
			Tocca nel seguente ordine l'ombrello la bicicletta il pappagallo la bambina
			Tocca nel seguente ordine l'ombrello la bicicletta il polipo il leone
			Tocca nel seguente ordine l'ombrello la bicicletta il polipo il pappagallo
			Tocca nel seguente ordine l'ombrello la bicicletta il polipo il gelato
			Tocca nel seguente ordine l'ombrello la bicicletta il polipo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bicicletta il polipo la scimmia
			Tocca nel seguente ordine l'ombrello la bicicletta il polipo il monopattino
			Tocca nel seguente ordine l'ombrello la bicicletta il polipo la bambina
			Tocca nel seguente ordine l'ombrello la bicicletta il gelato il polipo
			Tocca nel seguente ordine l'ombrello la bicicletta il gelato il pappagallo
			Tocca nel seguente ordine l'ombrello la bicicletta il gelato il leone
			Tocca nel seguente ordine l'ombrello la bicicletta il gelato gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bicicletta il gelato la scimmia
			Tocca nel seguente ordine l'ombrello la bicicletta il gelato il monopattino
			Tocca nel seguente ordine l'ombrello la bicicletta il gelato la bambina
			Tocca nel seguente ordine l'ombrello la bicicletta gli occhiali da sole il polipo
			Tocca nel seguente ordine l'ombrello la bicicletta gli occhiali da sole il pappagallo
			Tocca nel seguente ordine l'ombrello la bicicletta gli occhiali da sole il gelato
			Tocca nel seguente ordine l'ombrello la bicicletta gli occhiali da sole il leone
			Tocca nel seguente ordine l'ombrello la bicicletta gli occhiali da sole la scimmia
			Tocca nel seguente ordine l'ombrello la bicicletta gli occhiali da sole il monopattino
			Tocca nel seguente ordine l'ombrello la bicicletta gli occhiali da sole la bambina
			Tocca nel seguente ordine l'ombrello la bicicletta il monopattino il polipo
			Tocca nel seguente ordine l'ombrello la bicicletta il monopattino il pappagallo
			Tocca nel seguente ordine l'ombrello la bicicletta il monopattino il gelato
			Tocca nel seguente ordine l'ombrello la bicicletta il monopattino gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bicicletta il monopattino la scimmia
			Tocca nel seguente ordine l'ombrello la bicicletta il monopattino il leone
			Tocca nel seguente ordine l'ombrello la bicicletta il monopattino la bambina
			Tocca nel seguente ordine l'ombrello la bicicletta la bambina il polipo
			Tocca nel seguente ordine l'ombrello la bicicletta la bambina il pappagallo
			Tocca nel seguente ordine l'ombrello la bicicletta la bambina il gelato
			Tocca nel seguente ordine l'ombrello la bicicletta la bambina gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bicicletta la bambina la scimmia
			Tocca nel seguente ordine l'ombrello la bicicletta la bambina il monopattino
			Tocca nel seguente ordine l'ombrello la bicicletta la bambina il leone
			Tocca nel seguente ordine l'ombrello il monopattino il leone il polipo
			Tocca nel seguente ordine l'ombrello il monopattino il leone il pappagallo
			Tocca nel seguente ordine l'ombrello il monopattino il leone il gelato
			Tocca nel seguente ordine l'ombrello il monopattino il leone gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il monopattino il leone la bicicletta
			Tocca nel seguente ordine l'ombrello il monopattino il leone la scimmia
			Tocca nel seguente ordine l'ombrello il monopattino il leone la bambina
			Tocca nel seguente ordine l'ombrello il monopattino la scimmia il polipo
			Tocca nel seguente ordine l'ombrello il monopattino la scimmia il pappagallo
			Tocca nel seguente ordine l'ombrello il monopattino la scimmia il gelato
			Tocca nel seguente ordine l'ombrello il monopattino la scimmia gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il monopattino la scimmia la bicicletta
			Tocca nel seguente ordine l'ombrello il monopattino la scimmia il leone
			Tocca nel seguente ordine l'ombrello il monopattino la scimmia la bambina
			Tocca nel seguente ordine l'ombrello il monopattino il pappagallo il polipo
			Tocca nel seguente ordine l'ombrello il monopattino il pappagallo la scimmia
			Tocca nel seguente ordine l'ombrello il monopattino il pappagallo il gelato
			Tocca nel seguente ordine l'ombrello il monopattino il pappagallo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il monopattino il pappagallo la bicicletta
			Tocca nel seguente ordine l'ombrello il monopattino il pappagallo il leone
			Tocca nel seguente ordine l'ombrello il monopattino il pappagallo la bambina
			Tocca nel seguente ordine l'ombrello il monopattino il polipo il leone
			Tocca nel seguente ordine l'ombrello il monopattino il polipo il pappagallo
			Tocca nel seguente ordine l'ombrello il monopattino il polipo il gelato
			Tocca nel seguente ordine l'ombrello il monopattino il polipo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il monopattino il polipo la bicicletta
			Tocca nel seguente ordine l'ombrello il monopattino il polipo la scimmia
			Tocca nel seguente ordine l'ombrello il monopattino il polipo la bambina
			Tocca nel seguente ordine l'ombrello il monopattino il gelato il polipo
			Tocca nel seguente ordine l'ombrello il monopattino il gelato il pappagallo
			Tocca nel seguente ordine l'ombrello il monopattino il gelato il leone
			Tocca nel seguente ordine l'ombrello il monopattino il gelato gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il monopattino il gelato la bicicletta
			Tocca nel seguente ordine l'ombrello il monopattino il gelato la scimmia
			Tocca nel seguente ordine l'ombrello il monopattino il gelato la bambina
			Tocca nel seguente ordine l'ombrello il monopattino gli occhiali da sole il polipo
			Tocca nel seguente ordine l'ombrello il monopattino gli occhiali da sole il pappagallo
			Tocca nel seguente ordine l'ombrello il monopattino gli occhiali da sole il gelato
			Tocca nel seguente ordine l'ombrello il monopattino gli occhiali da sole il leone
			Tocca nel seguente ordine l'ombrello il monopattino gli occhiali da sole la bicicletta
			Tocca nel seguente ordine l'ombrello il monopattino gli occhiali da sole la scimmia
			Tocca nel seguente ordine l'ombrello il monopattino gli occhiali da sole la bambina
			Tocca nel seguente ordine l'ombrello il monopattino la bicicletta il polipo
			Tocca nel seguente ordine l'ombrello il monopattino la bicicletta il pappagallo
			Tocca nel seguente ordine l'ombrello il monopattino la bicicletta il gelato
			Tocca nel seguente ordine l'ombrello il monopattino la bicicletta gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il monopattino la bicicletta il leone
			Tocca nel seguente ordine l'ombrello il monopattino la bicicletta la scimmia
			Tocca nel seguente ordine l'ombrello il monopattino la bicicletta la bambina
			Tocca nel seguente ordine l'ombrello il monopattino la bambina il polipo
			Tocca nel seguente ordine l'ombrello il monopattino la bambina il pappagallo
			Tocca nel seguente ordine l'ombrello il monopattino la bambina il gelato
			Tocca nel seguente ordine l'ombrello il monopattino la bambina gli occhiali da sole
			Tocca nel seguente ordine l'ombrello il monopattino la bambina la bicicletta
			Tocca nel seguente ordine l'ombrello il monopattino la bambina il leone
			Tocca nel seguente ordine l'ombrello il monopattino la bambina la scimmia
			Tocca nel seguente ordine l'ombrello la bambina il leone il polipo
			Tocca nel seguente ordine l'ombrello la bambina il leone il pappagallo
			Tocca nel seguente ordine l'ombrello la bambina il leone il gelato
			Tocca nel seguente ordine l'ombrello la bambina il leone gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bambina il leone la bicicletta
			Tocca nel seguente ordine l'ombrello la bambina il leone il monopattino
			Tocca nel seguente ordine l'ombrello la bambina il leone la scimmia
			Tocca nel seguente ordine l'ombrello la bambina la scimmia il polipo
			Tocca nel seguente ordine l'ombrello la bambina la scimmia il pappagallo
			Tocca nel seguente ordine l'ombrello la bambina la scimmia il gelato
			Tocca nel seguente ordine l'ombrello la bambina la scimmia gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bambina la scimmia la bicicletta
			Tocca nel seguente ordine l'ombrello la bambina la scimmia il monopattino
			Tocca nel seguente ordine l'ombrello la bambina la scimmia il leone
			Tocca nel seguente ordine l'ombrello la bambina il pappagallo il polipo
			Tocca nel seguente ordine l'ombrello la bambina il pappagallo il leone
			Tocca nel seguente ordine l'ombrello la bambina il pappagallo il gelato
			Tocca nel seguente ordine l'ombrello la bambina il pappagallo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bambina il pappagallo la bicicletta
			Tocca nel seguente ordine l'ombrello la bambina il pappagallo il monopattino
			Tocca nel seguente ordine l'ombrello la bambina il pappagallo la scimmia
			Tocca nel seguente ordine l'ombrello la bambina il polipo il leone
			Tocca nel seguente ordine l'ombrello la bambina il polipo il pappagallo
			Tocca nel seguente ordine l'ombrello la bambina il polipo il gelato
			Tocca nel seguente ordine l'ombrello la bambina il polipo gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bambina il polipo la bicicletta
			Tocca nel seguente ordine l'ombrello la bambina il polipo il monopattino
			Tocca nel seguente ordine l'ombrello la bambina il polipo la scimmia
			Tocca nel seguente ordine l'ombrello la bambina il gelato il polipo
			Tocca nel seguente ordine l'ombrello la bambina il gelato il pappagallo
			Tocca nel seguente ordine l'ombrello la bambina il gelato il leone
			Tocca nel seguente ordine l'ombrello la bambina il gelato gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bambina il gelato la bicicletta
			Tocca nel seguente ordine l'ombrello la bambina il gelato il monopattino
			Tocca nel seguente ordine l'ombrello la bambina il gelato la scimmia
			Tocca nel seguente ordine l'ombrello la bambina gli occhiali da sole il polipo
			Tocca nel seguente ordine l'ombrello la bambina gli occhiali da sole il pappagallo
			Tocca nel seguente ordine l'ombrello la bambina gli occhiali da sole il gelato
			Tocca nel seguente ordine l'ombrello la bambina gli occhiali da sole il leone
			Tocca nel seguente ordine l'ombrello la bambina gli occhiali da sole la bicicletta
			Tocca nel seguente ordine l'ombrello la bambina gli occhiali da sole il monopattino
			Tocca nel seguente ordine l'ombrello la bambina gli occhiali da sole la scimmia
			Tocca nel seguente ordine l'ombrello la bambina la bicicletta il polipo
			Tocca nel seguente ordine l'ombrello la bambina la bicicletta il pappagallo
			Tocca nel seguente ordine l'ombrello la bambina la bicicletta il gelato
			Tocca nel seguente ordine l'ombrello la bambina la bicicletta gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bambina la bicicletta il leone
			Tocca nel seguente ordine l'ombrello la bambina la bicicletta il monopattino
			Tocca nel seguente ordine l'ombrello la bambina la bicicletta la scimmia
			Tocca nel seguente ordine l'ombrello la bambina il monopattino il polipo
			Tocca nel seguente ordine l'ombrello la bambina il monopattino il pappagallo
			Tocca nel seguente ordine l'ombrello la bambina il monopattino il gelato
			Tocca nel seguente ordine l'ombrello la bambina il monopattino gli occhiali da sole
			Tocca nel seguente ordine l'ombrello la bambina il monopattino la bicicletta
			Tocca nel seguente ordine l'ombrello la bambina il monopattino il leone
			Tocca nel seguente ordine l'ombrello la bambina il monopattino la scimmia
	    7OCO2 = Order color for 2 patches
			Tocca prima un disegno rosso poi un disegno verde
			Tocca prima un disegno rosso poi un disegno blu
			Tocca prima un disegno rosso poi un disegno giallo
			Tocca prima un disegno verde poi un disegno rosso
			Tocca prima un disegno verde poi un disegno giallo
			Tocca prima un disegno verde poi un disegno blu
			Tocca prima un disegno giallo poi un disegno rosso
			Tocca prima un disegno giallo poi un disegno verde
			Tocca prima un disegno giallo poi un disegno blu
			Tocca prima un disegno blu poi un disegno rosso
			Tocca prima un disegno blu poi un disegno verde
			Tocca prima un disegno blu poi un disegno giallo
	    7OCO3 = Order color for 3 patches
			Tocca per primo un disegno rosso poi un disegno verde dopo un disegno blu
			Tocca per primo un disegno rosso poi un disegno verde dopo un disegno giallo
			Tocca per primo un disegno rosso poi un disegno blu dopo un disegno verde
			Tocca per primo un disegno rosso poi un disegno blu dopo un disegno giallo
			Tocca per primo un disegno rosso poi un disegno giallo dopo un disegno verde
			Tocca per primo un disegno rosso poi un disegno giallo dopo un disegno blu
			Tocca per primo un disegno verde poi un disegno rosso dopo un disegno blu
			Tocca per primo un disegno verde poi un disegno rosso dopo un disegno giallo
			Tocca per primo un disegno verde poi un disegno blu dopo un disegno rosso
			Tocca per primo un disegno verde poi un disegno blu dopo un disegno giallo
			Tocca per primo un disegno verde poi un disegno giallo dopo un disegno rosso
			Tocca per primo un disegno verde poi un disegno giallo dopo un disegno blu
			Tocca per primo un disegno giallo poi un disegno rosso dopo un disegno il blu
			Tocca per primo un disegno giallo poi un disegno rosso dopo un disegno il verde
			Tocca per primo un disegno giallo poi un disegno blu dopo un disegno verde
			Tocca per primo un disegno giallo poi un disegno blu dopo un disegno rosso
			Tocca per primo un disegno giallo poi un disegno verde dopo un disegno rosso
			Tocca per primo un disegno giallo poi un disegno verde dopo un disegno blu
			Tocca per primo un disegno blu poi un disegno verde dopo un disegno rosso
			Tocca per primo un disegno blu poi un disegno verde dopo un disegno giallo
			Tocca per primo un disegno blu poi un disegno rosso dopo un disegno verde
			Tocca per primo un disegno blu poi un disegno rosso dopo un disegno giallo
			Tocca per primo un disegno blu poi un disegno giallo dopo un disegno verde
			Tocca per primo un disegno blu poi un disegno giallo dopo un disegno rosso
	    7OCO4 = Order color for 4 patches
			Tocca in ordine i colori prima il rosso poi il verde poi il blu poi il giallo
			Tocca in ordine i colori prima il rosso poi il verde poi il giallo poi il blu
			Tocca in ordine i colori prima il rosso poi il blu poi il verde poi il giallo
			Tocca in ordine i colori prima il rosso poi il blu poi il giallo poi il verde
			Tocca in ordine i colori prima il rosso poi il giallo poi il verde poi il blu
			Tocca in ordine i colori prima il rosso poi il giallo poi il blu poi il verde
			Tocca in ordine i colori prima il verde poi il rosso poi il blu poi il giallo
			Tocca in ordine i colori prima il verde poi il rosso poi il giallo poi il blu
			Tocca in ordine i colori prima il verde poi il blu poi il rosso poi il giallo
			Tocca in ordine i colori prima il verde poi il blu poi il giallo poi il rosso
			Tocca in ordine i colori prima il verde poi il giallo poi il rosso poi il blu
			Tocca in ordine i colori prima il verde poi il giallo poi il blu poi il rosso
			Tocca in ordine i colori prima il giallo poi il rosso poi il blu poi il verde
			Tocca in ordine i colori prima il giallo poi il rosso poi il verde poi il blu
			Tocca in ordine i colori prima il giallo poi il blu poi il verde poi il rosso
			Tocca in ordine i colori prima il giallo poi il blu poi il rosso poi il verde
			Tocca in ordine i colori prima il giallo poi il verde poi il rosso poi il blu
			Tocca in ordine i colori prima il giallo poi il verde poi il blu poi il rosso
			Tocca in ordine i colori prima il blu poi il verde poi il rosso poi il giallo
			Tocca in ordine i colori prima il blu poi il verde poi il giallo poi il rosso
			Tocca in ordine i colori prima il blu poi il rosso poi il verde poi il giallo
			Tocca in ordine i colori prima il blu poi il rosso poi il giallo poi il verde
			Tocca in ordine i colori prima il blu poi il giallo poi il verde poi il rosso
			Tocca in ordine i colori prima il blu poi il giallo poi il rosso poi il verde
		7OL2 = Order colorLed for 2 patches 
			Tocca per primo i disegni di questo colore, poi tocca per ultimo i disegni di questo colore
		7OL3 = Order colorLed three
			Tocca per primo i disegni di questo colore, poi tocca per secondo i disegni di questo colore, poi tocca per ultimo i disegni di questo colore
	######################################
	#8Q = Quiz game
		8QTSUSC1 = Quiz_True capacitive subject single one
			Tocca i leoni se c'è una scimmia
			Tocca i leoni se c'è un pappagallo
			Tocca i leoni se c'è un polipo
			Tocca i leoni se c'è un gelato
			Tocca i leoni se c'è un paio di occhiali da sole
			Tocca i leoni se c'è una bicicletta
			Tocca i leoni se c'è un monopattino
			Tocca i leoni se c'è un ombrello
			Tocca i leoni se c'è una bambina
			Tocca le scimmie se c'è un leone
			Tocca le scimmie se c'è un pappagallo
			Tocca le scimmie se c'è un polipo
			Tocca le scimmie se c'è un gelato
			Tocca le scimmie se c'è un paio di occhiali da sole
			Tocca le scimmie se c'è una bicicletta
			Tocca le scimmie se c'è un monopattino
			Tocca le scimmie se c'è un ombrello
			Tocca le scimmie se c'è una bambina
			Tocca i pappagalli se c'è un leone
			Tocca i pappagalli se c'è una scimmia
			Tocca i pappagalli se c'è un polipo
			Tocca i pappagalli se c'è un gelato
			Tocca i pappagalli se c'è un paio di occhiali da sole
			Tocca i pappagalli se c'è una bicicletta
			Tocca i pappagalli se c'è un monopattino
			Tocca i pappagalli se c'è un ombrello
			Tocca i pappagalli se c'è una bambina
			Tocca i polipi se c'è un leone
			Tocca i polipi se c'è una scimmia
			Tocca i polipi se c'è un pappagallo
			Tocca i polipi se c'è un gelato
			Tocca i polipi se c'è un paio di occhiali da sole
			Tocca i polipi se c'è una bicicletta
			Tocca i polipi se c'è un monopattino
			Tocca i polipi se c'è un ombrello
			Tocca i polipi se c'è una bambina
			Tocca i gelati se c'è un leone
			Tocca i gelati se c'è una scimmia
			Tocca i gelati se c'è un pappagallo
			Tocca i gelati se c'è un polipo
			Tocca i gelati se c'è un paio di occhiali da sole
			Tocca i gelati se c'è una bicicletta
			Tocca i gelati se c'è un monopattino
			Tocca i gelati se c'è un ombrello
			Tocca i gelati se c'è una bambina
			Tocca tutti gli occhiali da sole se c'è un leone
			Tocca tutti gli occhiali da sole se c'è una scimmia
			Tocca tutti gli occhiali da sole se c'è un pappagallo
			Tocca tutti gli occhiali da sole se c'è un polipo
			Tocca tutti gli occhiali da sole se c'è un gelato
			Tocca tutti gli occhiali da sole se c'è una bicicletta
			Tocca tutti gli occhiali da sole se c'è un monopattino
			Tocca tutti gli occhiali da sole se c'è un ombrello
			Tocca tutti gli occhiali da sole se c'è una bambina
			Tocca le biciclette se c'è un leone
			Tocca le biciclette se c'è una scimmia
			Tocca le biciclette se c'è un pappagallo
			Tocca le biciclette se c'è un polipo
			Tocca le biciclette se c'è un gelato
			Tocca le biciclette se c'è un paio di occhiali da sole
			Tocca le biciclette se c'è un monopattino
			Tocca le biciclette se c'è un ombrello
			Tocca le biciclette se c'è una bambina
			Tocca i monopattini se c'è un leone
			Tocca i monopattini se c'è una scimmia
			Tocca i monopattini se c'è un pappagallo
			Tocca i monopattini se c'è un polipo
			Tocca i monopattini se c'è un gelato
			Tocca i monopattini se c'è un paio di occhiali da sole
			Tocca i monopattini se c'è una bicicletta
			Tocca i monopattini se c'è un ombrello
			Tocca i monopattini se c'è una bambina
			Tocca gli ombrelli se c'è un leone
			Tocca gli ombrelli se c'è una scimmia
			Tocca gli ombrelli se c'è un pappagallo
			Tocca gli ombrelli se c'è un polipo
			Tocca gli ombrelli se c'è un gelato
			Tocca gli ombrelli se c'è un paio di occhiali da sole
			Tocca gli ombrelli se c'è una bicicletta
			Tocca gli ombrelli se c'è un monopattino
			Tocca gli ombrelli se c'è una bambina
			Tocca le bambine se c'è un leone
			Tocca le bambine se c'è una scimmia
			Tocca le bambine se c'è un pappagallo
			Tocca le bambine se c'è un polipo
			Tocca le bambine se c'è un gelato
			Tocca le bambine se c'è un paio di occhiali da sole
			Tocca le bambine se c'è una bicicletta
			Tocca le bambine se c'è un monopattino
			Tocca le bambine se c'è un ombrello
		8QTSUSC2 = Quiz_True capacitive subject single two
			Tocca i leoni se ci sono due scimmie
			Tocca i leoni se ci sono due pappagallo
			Tocca i leoni se ci sono due polipi
			Tocca i leoni se ci sono due gelati
			Tocca i leoni se ci sono due paia di occhiali da sole
			Tocca i leoni se ci sono due biciclette
			Tocca i leoni se ci sono due monopattini
			Tocca i leoni se ci sono due ombrelli
			Tocca i leoni se ci sono una bambine
			Tocca le scimmie se ci sono due leoni
			Tocca le scimmie se ci sono due pappagallo
			Tocca le scimmie se ci sono due polipi
			Tocca le scimmie se ci sono due gelati
			Tocca le scimmie se ci sono due paia di occhiali da sole
			Tocca le scimmie se ci sono due biciclette
			Tocca le scimmie se ci sono due monopattini
			Tocca le scimmie se ci sono due ombrelli
			Tocca le scimmie se ci sono una bambine
			Tocca i pappagalli se ci sono due leoni
			Tocca i pappagalli se ci sono due scimmie
			Tocca i pappagalli se ci sono due polipi
			Tocca i pappagalli se ci sono due gelati
			Tocca i pappagalli se ci sono due paia di occhiali da sole
			Tocca i pappagalli se ci sono due biciclette
			Tocca i pappagalli se ci sono due monopattini
			Tocca i pappagalli se ci sono due ombrelli
			Tocca i pappagalli se ci sono due bambine
			Tocca i polipi se ci sono due leoni
			Tocca i polipi se ci sono due scimmie
			Tocca i polipi se ci sono due pappagalli
			Tocca i polipi se ci sono due gelati
			Tocca i polipi se ci sono due paia di occhiali da sole
			Tocca i polipi se ci sono due biciclette
			Tocca i polipi se ci sono due monopattini
			Tocca i polipi se ci sono due ombrelli
			Tocca i polipi se ci sono una bambine
			Tocca i gelati se ci sono due leoni
			Tocca i gelati se ci sono due scimmie
			Tocca i gelati se ci sono due pappagalli
			Tocca i gelati se ci sono due polipi
			Tocca i gelati se ci sono due paia di occhiali da sole
			Tocca i gelati se ci sono due biciclette
			Tocca i gelati se ci sono due monopattini
			Tocca i gelati se ci sono due ombrelli
			Tocca i gelati se ci sono una bambine
			Tocca tutti gli occhiali da sole se ci sono due leoni
			Tocca tutti gli occhiali da sole se ci sono due scimmie
			Tocca tutti gli occhiali da sole se ci sono due pappagalli
			Tocca tutti gli occhiali da sole se ci sono due polipi
			Tocca tutti gli occhiali da sole se ci sono due gelati
			Tocca tutti gli occhiali da sole se ci sono due biciclette
			Tocca tutti gli occhiali da sole se ci sono due monopattini
			Tocca tutti gli occhiali da sole se ci sono due ombrelli
			Tocca tutti gli occhiali da sole se ci sono una bambine
			Tocca le biciclette se ci sono due leoni
			Tocca le biciclette se ci sono due scimmie
			Tocca le biciclette se ci sono due pappagalli
			Tocca le biciclette se ci sono due polipi
			Tocca le biciclette se ci sono due gelati
			Tocca le biciclette se ci sono due paia di occhiali da sole
			Tocca le biciclette se ci sono due monopattini
			Tocca le biciclette se ci sono due ombrelli
			Tocca le biciclette se ci sono una bambine
			Tocca i monopattini se ci sono due leoni
			Tocca i monopattini se ci sono due scimmie
			Tocca i monopattini se ci sono due pappagalli
			Tocca i monopattini se ci sono due polipi
			Tocca i monopattini se ci sono due gelati
			Tocca i monopattini se ci sono due paia di occhiali da sole
			Tocca i monopattini se ci sono due biciclette
			Tocca i monopattini se ci sono due ombrelli
			Tocca i monopattini se ci sono una bambine
			Tocca gli ombrelli se ci sono due leoni
			Tocca gli ombrelli se ci sono due scimmie
			Tocca gli ombrelli se ci sono due pappagalli
			Tocca gli ombrelli se ci sono due polipi
			Tocca gli ombrelli se ci sono due gelati
			Tocca gli ombrelli se ci sono due paia di occhiali da sole
			Tocca gli ombrelli se ci sono due biciclette
			Tocca gli ombrelli se ci sono due monopattini
			Tocca gli ombrelli se ci sono una bambine
			Tocca le bambine se ci sono due leoni
			Tocca le bambine se ci sono due scimmie
			Tocca le bambine se ci sono due pappagalli
			Tocca le bambine se ci sono due polipi
			Tocca le bambine se ci sono due gelati
			Tocca le bambine se ci sono due paia di occhiali da sole
			Tocca le bambine se ci sono due biciclette
			Tocca le bambine se ci sono due monopattini
			Tocca le bambine se ci sono due ombrelli
	    8QTCOSC1 = Quiz_True capacitive color single one 
			Tocca i leoni se c'è un disegno rosso
			Tocca i leoni se c'è un disegno verde
			Tocca i leoni se c'è un disegno giallo
			Tocca i leoni se c'è un disegno blu
			Tocca le scimmie se c'è un disegno rosso
			Tocca le scimmie se c'è un disegno verde
			Tocca le scimmie se c'è un disegno giallo
			Tocca le scimmie se c'è un disegno blu
			Tocca i pappagalli se c'è un disegno rosso
			Tocca i pappagalli se c'è un disegno verde
			Tocca i pappagalli se c'è un disegno giallo
			Tocca i pappagalli se c'è un disegno blu
			Tocca i polipi se c'è un disegno rosso
			Tocca i polipi se c'è un disegno verde
			Tocca i polipi se c'è un disegno giallo
			Tocca i polipi se c'è un disegno blu
			Tocca i gelati se c'è un disegno rosso
			Tocca i gelati se c'è un disegno verde
			Tocca i gelati se c'è un disegno giallo
			Tocca i gelati se c'è un disegno blu
			Tocca tutti gli occhiali da sole se c'è un disegno rosso
			Tocca tutti gli occhiali da sole se c'è un disegno verde
			Tocca tutti gli occhiali da sole se c'è un disegno giallo
			Tocca tutti gli occhiali da sole se c'è un disegno blu
			Tocca le biciclette se c'è un disegno rosso
			Tocca le biciclette se c'è un disegno verde
			Tocca le biciclette se c'è un disegno giallo
			Tocca le biciclette se c'è un disegno blu
			Tocca i monopattini se c'è un disegno rosso
			Tocca i monopattini se c'è un disegno verde
			Tocca i monopattini se c'è un disegno giallo
			Tocca i monopattini se c'è un disegno blu
			Tocca gli ombrelli se c'è un disegno rosso
			Tocca gli ombrelli se c'è un disegno verde
			Tocca gli ombrelli se c'è un disegno giallo
			Tocca gli ombrelli se c'è un disegno blu
			Tocca le bambine se c'è un disegno rosso
			Tocca le bambine se c'è un disegno verde
			Tocca le bambine se c'è un disegno giallo
			Tocca le bambine se c'è un disegno blu
	    8QTCOSC2 = Quiz_True capacitive color single two
			Tocca i leoni se ci sono due disegni rossi
			Tocca i leoni se ci sono due disegni verdi
			Tocca i leoni se ci sono due disegni gialli
			Tocca i leoni se ci sono due disegni blu
			Tocca le scimmie se ci sono due disegni rossi
			Tocca le scimmie se ci sono due disegni verdi
			Tocca le scimmie se ci sono due disegni gialli
			Tocca le scimmie se ci sono due disegni blu
			Tocca i pappagalli se ci sono due disegni rossi
			Tocca i pappagalli se ci sono due disegni verdi
			Tocca i pappagalli se ci sono due disegni gialli
			Tocca i pappagalli se ci sono due disegni blu
			Tocca i polipi se ci sono due disegni rossi
			Tocca i polipi se ci sono due disegni verdi
			Tocca i polipi se ci sono due disegni gialli
			Tocca i polipi se ci sono due disegni blu
			Tocca i gelati se ci sono due disegni rossi
			Tocca i gelati se ci sono due disegni verdi
			Tocca i gelati se ci sono due disegni gialli
			Tocca i gelati se ci sono due disegni blu
			Tocca tutti gli occhiali da sole se ci sono due disegni rossi
			Tocca tutti gli occhiali da sole se ci sono due disegni verdi
			Tocca tutti gli occhiali da sole se ci sono due disegni gialli
			Tocca tutti gli occhiali da sole se ci sono due disegni blu
			Tocca le biciclette se ci sono due disegni rossi
			Tocca le biciclette se ci sono due disegni verdi
			Tocca le biciclette se ci sono due disegni gialli
			Tocca le biciclette se ci sono due disegni blu
			Tocca i monopattini se ci sono due disegni rossi
			Tocca i monopattini se ci sono due disegni verdi
			Tocca i monopattini se ci sono due disegni gialli
			Tocca i monopattini se ci sono due disegni blu
			Tocca gli ombrelli se ci sono due disegni rossi
			Tocca gli ombrelli se ci sono due disegni verdi
			Tocca gli ombrelli se ci sono due disegni gialli
			Tocca gli ombrelli se ci sono due disegni blu
			Tocca le bambine se ci sono due disegni rossi
			Tocca le bambine se ci sono due disegni verdi
			Tocca le bambine se ci sono due disegni gialli
			Tocca le bambine se ci sono due disegni blu
	    8QTKNSC = Quiz_True capacitive knowledge single
			Tocca i leoni se c'è qualcosa che si può mangiare
			Tocca i leoni se c'è qualcosa che si può mettere sulla testa
			Tocca i leoni se c'è qualcosa che si può guidare
			Tocca i leoni se c'è qualcosa che si può indossare
			Tocca i leoni se c'è qualcosa cosa si può pedalare
			Tocca i leoni se c'è qualcosa che si usa quando piove
			Tocca i leoni se c'è qualcosa che può stare nella cameretta
			Tocca i leoni se c'è qualcuno che sa parlare
			Tocca i leoni se c'è qualcuno che non sa parlare
			Tocca i leoni se c'è qualcosa che sa volare
			Tocca i leoni se c'è qualcuno che non sa volare
			Tocca i leoni se c'è qualcosa ha le route
			Tocca i leoni se c'è qualcuno che sa nuotare
			Tocca i leoni se c'è qualcuno che non ha le zampe
			Tocca i leoni se c'è qualcuno che cammina su due zampe
			Tocca i leoni se c'è qualcuno che cammina a quattro zampe
			Tocca i leoni se c'è qualcuno che ruggisce
			Tocca i leoni se c'è qualcuno che ha il becco
			Tocca i leoni se c'è qualcuno che può vivere in casa
			Tocca i leoni se c'è qualcuno che non può vivere in città
			Tocca i leoni se c'è qualcuno cosa si può metttere sopra un tavolo
			Tocca i leoni se c'è qualcuno che sa correre
			Tocca i leoni se c'è qualcuno che può stare sugli alberi
			Tocca i leoni se c'è qualcuno che può arrampicarsi
			Tocca le scimmie se c'è qualcosa che si può mangiare
			Tocca le scimmie se c'è qualcosa che si può mettere sulla testa
			Tocca le scimmie se c'è qualcosa che si può guidare
			Tocca le scimmie se c'è qualcosa che si può indossare
			Tocca le scimmie se c'è qualcosa cosa si può pedalare
			Tocca le scimmie se c'è qualcosa che si usa quando piove
			Tocca le scimmie se c'è qualcosa che può stare nella cameretta
			Tocca le scimmie se c'è qualcuno che sa parlare
			Tocca le scimmie se c'è qualcuno che non sa parlare
			Tocca le scimmie se c'è qualcosa che sa volare
			Tocca le scimmie se c'è qualcuno che non sa volare
			Tocca le scimmie se c'è qualcosa ha le route
			Tocca le scimmie se c'è qualcuno che sa nuotare
			Tocca le scimmie se c'è qualcuno che non ha le zampe
			Tocca le scimmie se c'è qualcuno che cammina su due zampe
			Tocca le scimmie se c'è qualcuno che cammina a quattro zampe
			Tocca le scimmie se c'è qualcuno che ruggisce
			Tocca le scimmie se c'è qualcuno che ha il becco
			Tocca le scimmie se c'è qualcuno che può vivere in casa
			Tocca le scimmie se c'è qualcuno che non può vivere in città
			Tocca le scimmie se c'è qualcuno cosa si può metttere sopra un tavolo
			Tocca le scimmie se c'è qualcuno che sa correre
			Tocca le scimmie se c'è qualcuno che può stare sugli alberi
			Tocca le scimmie se c'è qualcuno che può arrampicarsi
			Tocca i pappagalli se c'è qualcosa che si può mangiare
			Tocca i pappagalli se c'è qualcosa che si può mettere sulla testa
			Tocca i pappagalli se c'è qualcosa che si può guidare
			Tocca i pappagalli se c'è qualcosa che si può indossare
			Tocca i pappagalli se c'è qualcosa cosa si può pedalare
			Tocca i pappagalli se c'è qualcosa che si usa quando piove
			Tocca i pappagalli se c'è qualcosa che può stare nella cameretta
			Tocca i pappagalli se c'è qualcuno che sa parlare
			Tocca i pappagalli se c'è qualcuno che non sa parlare
			Tocca i pappagalli se c'è qualcosa che sa volare
			Tocca i pappagalli se c'è qualcuno che non sa volare
			Tocca i pappagalli se c'è qualcosa ha le route
			Tocca i pappagalli se c'è qualcuno che sa nuotare
			Tocca i pappagalli se c'è qualcuno che non ha le zampe
			Tocca i pappagalli se c'è qualcuno che cammina su due zampe
			Tocca i pappagalli se c'è qualcuno che cammina a quattro zampe
			Tocca i pappagalli se c'è qualcuno che ruggisce
			Tocca i pappagalli se c'è qualcuno che ha il becco
			Tocca i pappagalli se c'è qualcuno che può vivere in casa
			Tocca i pappagalli se c'è qualcuno che non può vivere in città
			Tocca i pappagalli se c'è qualcuno cosa si può metttere sopra un tavolo
			Tocca i pappagalli se c'è qualcuno che sa correre
			Tocca i pappagalli se c'è qualcuno che può stare sugli alberi
			Tocca i pappagalli se c'è qualcuno che può arrampicarsi
			Tocca i polipi se c'è qualcosa che si può mangiare
			Tocca i polipi se c'è qualcosa che si può mettere sulla testa
			Tocca i polipi se c'è qualcosa che si può guidare
			Tocca i polipi se c'è qualcosa che si può indossare
			Tocca i polipi se c'è qualcosa cosa si può pedalare
			Tocca i polipi se c'è qualcosa che si usa quando piove
			Tocca i polipi se c'è qualcosa che può stare nella cameretta
			Tocca i polipi se c'è qualcuno che sa parlare
			Tocca i polipi se c'è qualcuno che non sa parlare
			Tocca i polipi se c'è qualcosa che sa volare
			Tocca i polipi se c'è qualcuno che non sa volare
			Tocca i polipi se c'è qualcosa ha le route
			Tocca i polipi se c'è qualcuno che sa nuotare
			Tocca i polipi se c'è qualcuno che non ha le zampe
			Tocca i polipi se c'è qualcuno che cammina su due zampe
			Tocca i polipi se c'è qualcuno che cammina a quattro zampe
			Tocca i polipi se c'è qualcuno che ruggisce
			Tocca i polipi se c'è qualcuno che ha il becco
			Tocca i polipi se c'è qualcuno che può vivere in casa
			Tocca i polipi se c'è qualcuno che non può vivere in città
			Tocca i polipi se c'è qualcuno cosa si può metttere sopra un tavolo
			Tocca i polipi se c'è qualcuno che sa correre
			Tocca i polipi se c'è qualcuno che può stare sugli alberi
			Tocca i polipi se c'è qualcuno che può arrampicarsi
			Tocca i gelati se c'è qualcosa che si può mangiare
			Tocca i gelati se c'è qualcosa che si può mettere sulla testa
			Tocca i gelati se c'è qualcosa che si può guidare
			Tocca i gelati se c'è qualcosa che si può indossare
			Tocca i gelati se c'è qualcosa cosa si può pedalare
			Tocca i gelati se c'è qualcosa che si usa quando piove
			Tocca i gelati se c'è qualcosa che può stare nella cameretta
			Tocca i gelati se c'è qualcuno che sa parlare
			Tocca i gelati se c'è qualcuno che non sa parlare
			Tocca i gelati se c'è qualcosa che sa volare
			Tocca i gelati se c'è qualcuno che non sa volare
			Tocca i gelati se c'è qualcosa ha le route
			Tocca i gelati se c'è qualcuno che sa nuotare
			Tocca i gelati se c'è qualcuno che non ha le zampe
			Tocca i gelati se c'è qualcuno che cammina su due zampe
			Tocca i gelati se c'è qualcuno che cammina a quattro zampe
			Tocca i gelati se c'è qualcuno che ruggisce
			Tocca i gelati se c'è qualcuno che ha il becco
			Tocca i gelati se c'è qualcuno che può vivere in casa
			Tocca i gelati se c'è qualcuno che non può vivere in città
			Tocca i gelati se c'è qualcuno cosa si può metttere sopra un tavolo
			Tocca i gelati se c'è qualcuno che sa correre
			Tocca tutti gli occhiali se c'è qualcosa che si può mangiare
			Tocca tutti gli occhiali se c'è qualcosa che si può mettere sulla testa
			Tocca tutti gli occhiali se c'è qualcosa che si può guidare
			Tocca tutti gli occhiali se c'è qualcosa che si può indossare
			Tocca tutti gli occhiali se c'è qualcosa cosa si può pedalare
			Tocca tutti gli occhiali se c'è qualcosa che si usa quando piove
			Tocca tutti gli occhiali se c'è qualcosa che può stare nella cameretta
			Tocca tutti gli occhiali se c'è qualcuno che sa parlare
			Tocca tutti gli occhiali se c'è qualcuno che non sa parlare
			Tocca tutti gli occhiali se c'è qualcosa che sa volare
			Tocca tutti gli occhiali se c'è qualcuno che non sa volare
			Tocca tutti gli occhiali se c'è qualcosa ha le route
			Tocca tutti gli occhiali se c'è qualcuno che sa nuotare
			Tocca tutti gli occhiali se c'è qualcuno che non ha le zampe
			Tocca tutti gli occhiali se c'è qualcuno che cammina su due zampe
			Tocca tutti gli occhiali se c'è qualcuno che cammina a quattro zampe
			Tocca tutti gli occhiali se c'è qualcuno che ruggisce
			Tocca tutti gli occhiali se c'è qualcuno che ha il becco
			Tocca tutti gli occhiali se c'è qualcuno che può vivere in casa
			Tocca tutti gli occhiali se c'è qualcuno che non può vivere in città
			Tocca tutti gli occhiali se c'è qualcuno cosa si può metttere sopra un tavolo
			Tocca tutti gli occhiali se c'è qualcuno che sa correre
			Tocca tutti gli occhiali se c'è qualcuno che può stare sugli alberi
			Tocca tutti gli occhiali se c'è qualcuno che può arrampicarsi
			Tocca le biciclette se c'è qualcosa che si può mettere sulla testa
			Tocca le biciclette se c'è qualcosa che si può guidare
			Tocca le biciclette se c'è qualcosa che si può indossare
			Tocca le biciclette se c'è qualcosa cosa si può pedalare
			Tocca le biciclette se c'è qualcosa che si usa quando piove
			Tocca le biciclette se c'è qualcosa che può stare nella cameretta
			Tocca le biciclette se c'è qualcuno che sa parlare
			Tocca le biciclette se c'è qualcuno che non sa parlare
			Tocca le biciclette se c'è qualcosa che sa volare
			Tocca le biciclette se c'è qualcuno che non sa volare
			Tocca le biciclette se c'è qualcosa ha le route
			Tocca le biciclette se c'è qualcuno che sa nuotare
			Tocca le biciclette se c'è qualcuno che non ha le zampe
			Tocca le biciclette se c'è qualcuno che cammina su due zampe
			Tocca le biciclette se c'è qualcuno che cammina a quattro zampe
			Tocca le biciclette se c'è qualcuno che ruggisce
			Tocca le biciclette se c'è qualcuno che ha il becco
			Tocca le biciclette se c'è qualcuno che può vivere in casa
			Tocca le biciclette se c'è qualcuno che non può vivere in città
			Tocca le biciclette se c'è qualcuno cosa si può metttere sopra un tavolo
			Tocca le biciclette se c'è qualcuno che sa correre
			Tocca le biciclette se c'è qualcuno che può stare sugli alberi
			Tocca le biciclette se c'è qualcuno che può arrampicarsi
			Tocca i monopattini se c'è qualcosa che si può mangiare
			Tocca i monopattini se c'è qualcosa che si può mettere sulla testa
			Tocca i monopattini se c'è qualcosa che si può guidare
			Tocca i monopattini se c'è qualcosa che si può indossare
			Tocca i monopattini se c'è qualcosa cosa si può pedalare
			Tocca i monopattini se c'è qualcosa che si usa quando piove
			Tocca i monopattini se c'è qualcosa che può stare nella cameretta
			Tocca i monopattini se c'è qualcuno che sa parlare
			Tocca i monopattini se c'è qualcuno che non sa parlare
			Tocca i monopattini se c'è qualcosa che sa volare
			Tocca i monopattini se c'è qualcuno che non sa volare
			Tocca i monopattini se c'è qualcosa ha le route
			Tocca i monopattini se c'è qualcuno che sa nuotare
			Tocca i monopattini se c'è qualcuno che non ha le zampe
			Tocca i monopattini se c'è qualcuno che cammina su due zampe
			Tocca i monopattini se c'è qualcuno che cammina a quattro zampe
			Tocca i monopattini se c'è qualcuno che ruggisce
			Tocca i monopattini se c'è qualcuno che ha il becco
			Tocca i monopattini se c'è qualcuno che può vivere in casa
			Tocca i monopattini se c'è qualcuno che non può vivere in città
			Tocca i monopattini se c'è qualcuno cosa si può metttere sopra un tavolo
			Tocca i monopattini se c'è qualcuno che sa correre
			Tocca i monopattini se c'è qualcuno che può stare sugli alberi
			Tocca i monopattini se c'è qualcuno che può arrampicarsi
			Tocca gli ombrelli se c'è qualcosa che si può mangiare
			Tocca gli ombrelli se c'è qualcosa che si può mettere sulla testa
			Tocca gli ombrelli se c'è qualcosa che si può guidare
			Tocca gli ombrelli se c'è qualcosa che si può indossare
			Tocca gli ombrelli se c'è qualcosa cosa si può pedalare
			Tocca gli ombrelli se c'è qualcosa che si usa quando piove
			Tocca gli ombrelli se c'è qualcosa che può stare nella cameretta
			Tocca gli ombrelli se c'è qualcuno che sa parlare
			Tocca gli ombrelli se c'è qualcuno che non sa parlare
			Tocca gli ombrelli se c'è qualcosa che sa volare
			Tocca gli ombrelli se c'è qualcuno che non sa volare
			Tocca gli ombrelli se c'è qualcosa ha le route
			Tocca gli ombrelli se c'è qualcuno che sa nuotare
			Tocca gli ombrelli se c'è qualcuno che non ha le zampe
			Tocca gli ombrelli se c'è qualcuno che cammina su due zampe
			Tocca gli ombrelli se c'è qualcuno che cammina a quattro zampe
			Tocca gli ombrelli se c'è qualcuno che ruggisce
			Tocca gli ombrelli se c'è qualcuno che ha il becco
			Tocca gli ombrelli se c'è qualcuno che può vivere in casa
			Tocca gli ombrelli se c'è qualcuno che non può vivere in città
			Tocca gli ombrelli se c'è qualcuno cosa si può metttere sopra un tavolo
			Tocca gli ombrelli se c'è qualcuno che sa correre
			Tocca gli ombrelli se c'è qualcuno che può stare sugli alberi
			Tocca gli ombrelli se c'è qualcuno che può arrampicarsi
			Tocca le bambine se c'è qualcosa che si può mangiare
			Tocca le bambine se c'è qualcosa che si può mettere sulla testa
			Tocca le bambine se c'è qualcosa che si può guidare
			Tocca le bambine se c'è qualcosa che si può indossare
			Tocca le bambine se c'è qualcosa cosa si può pedalare
			Tocca le bambine se c'è qualcosa che si usa quando piove
			Tocca le bambine se c'è qualcosa che può stare nella cameretta
			Tocca le bambine se c'è qualcuno che sa parlare
			Tocca le bambine se c'è qualcuno che non sa parlare
			Tocca le bambine se c'è qualcosa che sa volare
			Tocca le bambine se c'è qualcuno che non sa volare
			Tocca le bambine se c'è qualcosa ha le route
			Tocca le bambine se c'è qualcuno che sa nuotare
			Tocca le bambine se c'è qualcuno che non ha le zampe
			Tocca le bambine se c'è qualcuno che cammina su due zampe
			Tocca le bambine se c'è qualcuno che cammina a quattro zampe
			Tocca le bambine se c'è qualcuno che ruggisce
			Tocca le bambine se c'è qualcuno che ha il becco
			Tocca le bambine se c'è qualcuno che può vivere in casa
			Tocca le bambine se c'è qualcuno che non può vivere in città
			Tocca le bambine se c'è qualcuno cosa si può metttere sopra un tavolo
			Tocca le bambine se c'è qualcuno che sa correre
			Tocca le bambine se c'è qualcuno che può stare sugli alberi
			Tocca le bambine se c'è qualcuno che può arrampicarsi
		8QTKNSC = Quiz_True capacitive knowledge single
			Tocca i leoni se c'è qualcuno che sa correre e parlare
			Tocca i leoni se c'è qualcuno che cammina su due zampee non può volare
			Tocca le scimmie se c'è qualcuno che sa nuotare e correre
			Tocca le scimmie se c'è qualcuno che sa correre e parlare
			Tocca le scimmie se c'è qualcuno che cammina su due zampee non può volare
			Tocca i pappagalli se c'è qualcuno che sa nuotare e correre
			Tocca i pappagalli se c'è qualcuno che sa correre e parlare
			Tocca i pappagalli se c'è qualcuno che cammina su due zampee non può volare
			Tocca i polipi se c'è qualcuno che sa nuotare e correre
			Tocca i polipi se c'è qualcuno che sa correre e parlare
			Tocca i polipi se c'è qualcuno che cammina su due zampee non può volare
			Tocca i gelati se c'è qualcuno che sa nuotare e correre
			Tocca i gelati se c'è qualcuno che sa correre e parlare
			Tocca i gelati se c'è qualcuno che cammina su due zampee non può volare
			Tocca tutti gli occhiali se c'è qualcuno che sa nuotare e correre
			Tocca tutti gli occhiali se c'è qualcuno che sa correre e parlare
			Tocca tutti gli occhiali se c'è qualcuno che cammina su due zampee non può volare
			Tocca i monopattini se c'è qualcuno che sa nuotare e correre
			Tocca i monopattini se c'è qualcuno che sa correre e parlare
			Tocca i monopattini se c'è qualcuno che cammina su due zampee non può volare
			Tocca le biciclette se c'è qualcuno che sa nuotare e correre
			Tocca le biciclette se c'è qualcuno che sa correre e parlare
			Tocca le biciclette se c'è qualcuno che cammina su due zampee non può volare
			Tocca le bambine se c'è qualcuno che sa nuotare e correre
			Tocca le bambine se c'è qualcuno che sa correre e parlare
			Tocca le bambine se c'è qualcuno che cammina su due zampee non può volare
	    8QTPRSC = Quiz_True capacitive precise_ans single
			Tocca i leoni se c'è ci sono meno di due scimmie
			Tocca i leoni se c'è ci sono meno di due pappagalli
			Tocca i leoni se c'è ci sono meno di due polipi
			Tocca i leoni se c'è ci sono meno di due gelati
			Tocca i leoni se c'è ci sono meno di due paia di occhiali da sole
			Tocca i leoni se c'è ci sono meno di due biciclette
			Tocca i leoni se c'è ci sono meno di due monopattini
			Tocca i leoni se c'è ci sono meno di due ombrelli
			Tocca i leoni se c'è ci sono meno di due bambine
			Tocca le scimmie se c'è ci sono meno di due leoni
			Tocca le scimmie se c'è ci sono meno di due pappagalli
			Tocca le scimmie se c'è ci sono meno di due polipi
			Tocca le scimmie se c'è ci sono meno di due gelati
			Tocca le scimmie se c'è ci sono meno di due paia di occhiali da sole
			Tocca le scimmie se c'è ci sono meno di due biciclette
			Tocca le scimmie se c'è ci sono meno di due monopattini
			Tocca le scimmie se c'è ci sono meno di due ombrelli
			Tocca le scimmie se c'è ci sono meno di due bambine
			Tocca i pappagalli se c'è ci sono meno di due leoni
			Tocca i pappagalli se c'è ci sono meno di due scimmie
			Tocca i pappagalli se c'è ci sono meno di due polipi
			Tocca i pappagalli se c'è ci sono meno di due gelati
			Tocca i pappagalli se c'è ci sono meno di due paia di occhiali da sole
			Tocca i pappagalli se c'è ci sono meno di due biciclette
			Tocca i pappagalli se c'è ci sono meno di due monopattini
			Tocca i pappagalli se c'è ci sono meno di due ombrelli
			Tocca i pappagalli se c'è ci sono meno di due bambine
			Tocca i polipi se c'è ci sono meno di due leoni
			Tocca i polipi se c'è ci sono meno di due scimmie
			Tocca i polipi se c'è ci sono meno di due pappagalli
			Tocca i polipi se c'è ci sono meno di due gelato
			Tocca i polipi se c'è ci sono meno di due paia di occhiali da sole
			Tocca i polipi se c'è ci sono meno di due biciclette
			Tocca i polipi se c'è ci sono meno di due monopattini
			Tocca i polipi se c'è ci sono meno di due ombrelli
			Tocca i polipi se c'è ci sono meno di due bambine
			Tocca i gelati se c'è ci sono meno di due leoni
			Tocca i gelati se c'è ci sono meno di due scimmie
			Tocca i gelati se c'è ci sono meno di due pappagalli
			Tocca i gelati se c'è ci sono meno di due polipi
			Tocca i gelati se c'è ci sono meno di due paia di occhiali da sole
			Tocca i gelati se c'è ci sono meno di due biciclette
			Tocca i gelati se c'è ci sono meno di due monopattini
			Tocca i gelati se c'è ci sono meno di due ombrelli
			Tocca i gelati se c'è ci sono meno di due bambine
			Tocca tutti gli occhiali da sole se c'è ci sono meno di due leoni
			Tocca tutti gli occhiali da sole se c'è ci sono meno di due scimmie
			Tocca tutti gli occhiali da sole se c'è ci sono meno di due pappagalli
			Tocca tutti gli occhiali da sole se c'è ci sono meno di due polipi
			Tocca tutti gli occhiali da sole se c'è ci sono meno di due gelato
			Tocca tutti gli occhiali da sole se c'è ci sono meno di due biciclette
			Tocca tutti gli occhiali da sole se c'è ci sono meno di due monopattini
			Tocca tutti gli occhiali da sole se c'è ci sono meno di due ombrelli
			Tocca tutti gli occhiali da sole se c'è ci sono meno di due bambine
			Tocca le biciclette se c'è ci sono meno di due leoni
			Tocca le biciclette se c'è ci sono meno di due scimmie
			Tocca le biciclette se c'è ci sono meno di due pappagalli
			Tocca le biciclette se c'è ci sono meno di due polipi
			Tocca le biciclette se c'è ci sono meno di due gelato
			Tocca le biciclette se c'è ci sono meno di due paia di occhiali da sole
			Tocca le biciclette se c'è ci sono meno di due monopattini
			Tocca le biciclette se c'è ci sono meno di due ombrelli
			Tocca le biciclette se c'è ci sono meno di due bambine
			Tocca i monopattini se c'è ci sono meno di due leoni
			Tocca i monopattini se c'è ci sono meno di due scimmie
			Tocca i monopattini se c'è ci sono meno di due pappagalli
			Tocca i monopattini se c'è ci sono meno di due polipi
			Tocca i monopattini se c'è ci sono meno di due gelato
			Tocca i monopattini se c'è ci sono meno di due paia di occhiali da sole
			Tocca i monopattini se c'è ci sono meno di due biciclette
			Tocca i monopattini se c'è ci sono meno di due ombrelli
			Tocca i monopattini se c'è ci sono meno di due bambine
			Tocca gli ombrelli se c'è ci sono meno di due leoni
			Tocca gli ombrelli se c'è ci sono meno di due scimmie
			Tocca gli ombrelli se c'è ci sono meno di due pappagalli
			Tocca gli ombrelli se c'è ci sono meno di due polipi
			Tocca gli ombrelli se c'è ci sono meno di due gelato
			Tocca gli ombrelli se c'è ci sono meno di due paia di occhiali da sole
			Tocca gli ombrelli se c'è ci sono meno di due biciclette
			Tocca gli ombrelli se c'è ci sono meno di due monopattini
			Tocca gli ombrelli se c'è ci sono meno di due bambine
			Tocca le bambine se c'è ci sono meno di due leoni
			Tocca le bambine se c'è ci sono meno di due scimmie
			Tocca le bambine se c'è ci sono meno di due pappagalli
			Tocca le bambine se c'è ci sono meno di due polipi
			Tocca le bambine se c'è ci sono meno di due gelato
			Tocca le bambine se c'è ci sono meno di due paia di occhiali da sole
			Tocca le bambine se c'è ci sono meno di due biciclette
			Tocca le bambine se c'è ci sono meno di due monopattini
			Tocca le bambine se c'è ci sono meno di due ombrelli
			Tocca i leoni se c'è più di una scimmia
			Tocca i leoni se c'è più di un pappagallo
			Tocca i leoni se c'è più di un polipo
			Tocca i leoni se c'è più di un gelato
			Tocca i leoni se c'è più di un paio di occhiali da sole
			Tocca i leoni se c'è più di una bicicletta
			Tocca i leoni se c'è più di un monopattino
			Tocca i leoni se c'è più di un ombrello
			Tocca i leoni se c'è più di una bambina
			Tocca le scimmie se c'è più di un leone
			Tocca le scimmie se c'è più di un pappagallo
			Tocca le scimmie se c'è più di un polipo
			Tocca le scimmie se c'è più di un gelato
			Tocca le scimmie se c'è più di un paio di occhiali da sole
			Tocca le scimmie se c'è più di una bicicletta
			Tocca le scimmie se c'è più di un monopattino
			Tocca le scimmie se c'è più di un ombrello
			Tocca le scimmie se c'è più di una bambina
			Tocca i pappagalli se c'è più di un leone
			Tocca i pappagalli se c'è più di una scimmia
			Tocca i pappagalli se c'è più di un polipo
			Tocca i pappagalli se c'è più di un gelato
			Tocca i pappagalli se c'è più di un paio di occhiali da sole
			Tocca i pappagalli se c'è più di una bicicletta
			Tocca i pappagalli se c'è più di un monopattino
			Tocca i pappagalli se c'è più di un ombrello
			Tocca i pappagalli se c'è più di una bambina
			Tocca i polipi se c'è più di un leone
			Tocca i polipi se c'è più di una scimmia
			Tocca i polipi se c'è più di un pappagallo
			Tocca i polipi se c'è più di un gelato
			Tocca i polipi se c'è più di un paio di occhiali da sole
			Tocca i polipi se c'è più di una bicicletta
			Tocca i polipi se c'è più di un monopattino
			Tocca i polipi se c'è più di un ombrello
			Tocca i polipi se c'è più di una bambina
			Tocca i gelati se c'è più di un leone
			Tocca i gelati se c'è più di una scimmia
			Tocca i gelati se c'è più di un pappagallo
			Tocca i gelati se c'è più di un polipo
			Tocca i gelati se c'è più di un paio di occhiali da sole
			Tocca i gelati se c'è più di una bicicletta
			Tocca i gelati se c'è più di un monopattino
			Tocca i gelati se c'è più di un ombrello
			Tocca i gelati se c'è più di una bambina
			Tocca tutti gli occhiali da sole se c'è più di un leone
			Tocca tutti gli occhiali da sole se c'è più di una scimmia
			Tocca tutti gli occhiali da sole se c'è più di un pappagallo
			Tocca tutti gli occhiali da sole se c'è più di un polipo
			Tocca tutti gli occhiali da sole se c'è più di un gelato
			Tocca tutti gli occhiali da sole se c'è più di una bicicletta
			Tocca tutti gli occhiali da sole se c'è più di un monopattino
			Tocca tutti gli occhiali da sole se c'è più di un ombrello
			Tocca tutti gli occhiali da sole se c'è più di una bambina
			Tocca le biciclette se c'è più di un leone
			Tocca le biciclette se c'è più di una scimmia
			Tocca le biciclette se c'è più di un pappagallo
			Tocca le biciclette se c'è più di un polipo
			Tocca le biciclette se c'è più di un gelato
			Tocca le biciclette se c'è più di un paio di occhiali da sole
			Tocca le biciclette se c'è più di un monopattino
			Tocca le biciclette se c'è più di un ombrello
			Tocca le biciclette se c'è più di una bambina
			Tocca i monopattini se c'è più di un leone
			Tocca i monopattini se c'è più di una scimmia
			Tocca i monopattini se c'è più di un pappagallo
			Tocca i monopattini se c'è più di un polipo
			Tocca i monopattini se c'è più di un gelato
			Tocca i monopattini se c'è più di un paio di occhiali da sole
			Tocca i monopattini se c'è più di una bicicletta
			Tocca i monopattini se c'è più di un ombrello
			Tocca i monopattini se c'è più di una bambina
			Tocca gli ombrelli se c'è più di un leone
			Tocca gli ombrelli se c'è più di una scimmia
			Tocca gli ombrelli se c'è più di un pappagallo
			Tocca gli ombrelli se c'è più di un polipo
			Tocca gli ombrelli se c'è più di un gelato
			Tocca gli ombrelli se c'è più di un paio di occhiali da sole
			Tocca gli ombrelli se c'è più di una bicicletta
			Tocca gli ombrelli se c'è più di un monopattino
			Tocca gli ombrelli se c'è più di una bambina
			Tocca le bambine se c'è più di un leone
			Tocca le bambine se c'è più di una scimmia
			Tocca le bambine se c'è più di un pappagallo
			Tocca le bambine se c'è più di un polipo
			Tocca le bambine se c'è più di un gelato
			Tocca le bambine se c'è più di un paio di occhiali da sole
			Tocca le bambine se c'è più di una bicicletta
			Tocca le bambine se c'è più di un monopattino
			Tocca le bambine se c'è più di un ombrello
		8QTSUDC1 = Quiz_True capacitive subject double one
			Tocca i leoni se vedi una scimmia e un pappagallo
			Tocca i leoni se vedi una scimmia e un polipo
			Tocca i leoni se vedi una scimmia e un gelato
			Tocca i leoni se vedi una scimmia e un paio di occhiali da sole
			Tocca i leoni se vedi una scimmia e una bicicletta
			Tocca i leoni se vedi una scimmia e un monopattino
			Tocca i leoni se vedi una scimmia e un ombrello
			Tocca i leoni se vedi una scimmia e una bambina
			Tocca i leoni se vedi un pappagallo e un polipo
			Tocca i leoni se vedi un pappagallo e un gelato
			Tocca i leoni se vedi un pappagallo e un paio di occhiali da sole
			Tocca i leoni se vedi un pappagallo e una bicicletta
			Tocca i leoni se vedi un pappagallo e un monopattino
			Tocca i leoni se vedi un pappagallo e un ombrello
			Tocca i leoni se vedi un pappagallo e una bambina
			Tocca i leoni se vedi un polipo e un gelato
			Tocca i leoni se vedi un polipo e un paio di occhiali da sole
			Tocca i leoni se vedi un polipo e una bicicletta
			Tocca i leoni se vedi un polipo e un monopattino
			Tocca i leoni se vedi un polipo e un ombrello
			Tocca i leoni se vedi un polipo e una bambina
			Tocca i leoni se vedi un gelato e un paio di occhiali da sole
			Tocca i leoni se vedi un gelato e una bicicletta
			Tocca i leoni se vedi un gelato e un monopattino
			Tocca i leoni se vedi un gelato e un ombrello
			Tocca i leoni se vedi un gelato e una bambina
			Tocca i leoni se vedi un paio di occhiali da sole e una bicicletta
			Tocca i leoni se vedi un paio di occhiali da sole e un monopattino
			Tocca i leoni se vedi un paio di occhiali da sole e un ombrello
			Tocca i leoni se vedi un paio di occhiali da sole e una bambina
			Tocca i leoni se vedi una bicicletta e un monopattino
			Tocca i leoni se vedi una bicicletta e un ombrello
			Tocca i leoni se vedi una bicicletta e una bambina
			Tocca i leoni se vedi un monopattino e un ombrello
			Tocca i leoni se vedi un monopattino e una bambina
			Tocca i leoni se vedi un ombrello e una bambina
			Tocca le scimmie se vedi un leone e un pappagallo
			Tocca le scimmie se vedi un leone e un polipo
			Tocca le scimmie se vedi un leone e un gelato
			Tocca le scimmie se vedi un leone e un paio di occhiali da sole
			Tocca le scimmie se vedi un leone e una bicicletta
			Tocca le scimmie se vedi un leone e un monopattino
			Tocca le scimmie se vedi un leone e un ombrello
			Tocca le scimmie se vedi un leone e una bambina
			Tocca le scimmie se vedi un pappagallo e un polipo
			Tocca le scimmie se vedi un pappagallo e un gelato
			Tocca le scimmie se vedi un pappagallo e un paio di occhiali da sole
			Tocca le scimmie se vedi un pappagallo e una bicicletta
			Tocca le scimmie se vedi un pappagallo e un monopattino
			Tocca le scimmie se vedi un pappagallo e un ombrello
			Tocca le scimmie se vedi un pappagallo e una bambina
			Tocca le scimmie se vedi un polipo e un gelato
			Tocca le scimmie se vedi un polipo e un paio di occhiali da sole
			Tocca le scimmie se vedi un polipo e una bicicletta
			Tocca le scimmie se vedi un polipo e un monopattino
			Tocca le scimmie se vedi un polipo e un ombrello
			Tocca le scimmie se vedi un polipo e una bambina
			Tocca le scimmie se vedi un gelato e un paio di occhiali da sole
			Tocca le scimmie se vedi un gelato e una bicicletta
			Tocca le scimmie se vedi un gelato e un monopattino
			Tocca le scimmie se vedi un gelato e un ombrello
			Tocca le scimmie se vedi un gelato e una bambina
			Tocca le scimmie se vedi un paio di occhiali da sole e una bicicletta
			Tocca le scimmie se vedi un paio di occhiali da sole e un monopattino
			Tocca le scimmie se vedi un paio di occhiali da sole e un ombrello
			Tocca le scimmie se vedi un paio di occhiali da sole e una bambina
			Tocca le scimmie se vedi una bicicletta e un monopattino
			Tocca le scimmie se vedi una bicicletta e un ombrello
			Tocca le scimmie se vedi una bicicletta e una bambina
			Tocca le scimmie se vedi un monopattino e un ombrello
			Tocca le scimmie se vedi un monopattino e una bambina
			Tocca le scimmie se vedi un ombrello e una bambina
			Tocca i pappagalli se vedi un leone e una scimmia
			Tocca i pappagalli se vedi un leone e un polipo
			Tocca i pappagalli se vedi un leone e un gelato
			Tocca i pappagalli se vedi un leone e un paio di occhiali da sole
			Tocca i pappagalli se vedi un leone e una bicicletta
			Tocca i pappagalli se vedi un leone e un monopattino
			Tocca i pappagalli se vedi un leone e un ombrello
			Tocca i pappagalli se vedi un leone e una bambina
			Tocca i pappagalli se vedi una scimmia e un polipo
			Tocca i pappagalli se vedi una scimmia e un gelato
			Tocca i pappagalli se vedi una scimmia e un paio di occhiali da sole
			Tocca i pappagalli se vedi una scimmia e una bicicletta
			Tocca i pappagalli se vedi una scimmia e un monopattino
			Tocca i pappagalli se vedi una scimmia e un ombrello
			Tocca i pappagalli se vedi una scimmia e una bambina
			Tocca i pappagalli se vedi un polipo e un gelato
			Tocca i pappagalli se vedi un polipo e un paio di occhiali da sole
			Tocca i pappagalli se vedi un polipo e una bicicletta
			Tocca i pappagalli se vedi un polipo e un monopattino
			Tocca i pappagalli se vedi un polipo e un ombrello
			Tocca i pappagalli se vedi un polipo e una bambina
			Tocca i pappagalli se vedi un gelato e un paio di occhiali da sole
			Tocca i pappagalli se vedi un gelato e una bicicletta
			Tocca i pappagalli se vedi un gelato e un monopattino
			Tocca i pappagalli se vedi un gelato e un ombrello
			Tocca i pappagalli se vedi un gelato e una bambina
			Tocca i pappagalli se vedi un paio di occhiali da sole e una bicicletta
			Tocca i pappagalli se vedi un paio di occhiali da sole e un monopattino
			Tocca i pappagalli se vedi un paio di occhiali da sole e un ombrello
			Tocca i pappagalli se vedi un paio di occhiali da sole e una bambina
			Tocca i pappagalli se vedi una bicicletta e un monopattino
			Tocca i pappagalli se vedi una bicicletta e un ombrello
			Tocca i pappagalli se vedi una bicicletta e una bambina
			Tocca i pappagalli se vedi un monopattino e un ombrello
			Tocca i pappagalli se vedi un monopattino e una bambina
			Tocca i pappagalli se vedi un ombrello e una bambina
			Tocca i polipi se vedi un leone e una scimmia
			Tocca i polipi se vedi un leone e un pappagallo
			Tocca i polipi se vedi un leone e un gelato
			Tocca i polipi se vedi un leone e un paio di occhiali da sole
			Tocca i polipi se vedi un leone e una bicicletta
			Tocca i polipi se vedi un leone e un monopattino
			Tocca i polipi se vedi un leone e un ombrello
			Tocca i polipi se vedi un leone e una bambina
			Tocca i polipi se vedi una scimmia e un pappagallo
			Tocca i polipi se vedi una scimmia e un gelato
			Tocca i polipi se vedi una scimmia e un paio di occhiali da sole
			Tocca i polipi se vedi una scimmia e una bicicletta
			Tocca i polipi se vedi una scimmia e un monopattino
			Tocca i polipi se vedi una scimmia e un ombrello
			Tocca i polipi se vedi una scimmia e una bambina
			Tocca i polipi se vedi un pappagallo e un gelato
			Tocca i polipi se vedi un pappagallo e un paio di occhiali da sole
			Tocca i polipi se vedi un pappagallo e una bicicletta
			Tocca i polipi se vedi un pappagallo e un monopattino
			Tocca i polipi se vedi un pappagallo e un ombrello
			Tocca i polipi se vedi un pappagallo e una bambina
			Tocca i polipi se vedi un gelato e un paio di occhiali da sole
			Tocca i polipi se vedi un gelato e una bicicletta
			Tocca i polipi se vedi un gelato e un monopattino
			Tocca i polipi se vedi un gelato e un ombrello
			Tocca i polipi se vedi un gelato e una bambina
			Tocca i polipi se vedi un paio di occhiali da sole e una bicicletta
			Tocca i polipi se vedi un paio di occhiali da sole e un monopattino
			Tocca i polipi se vedi un paio di occhiali da sole e un ombrello
			Tocca i polipi se vedi un paio di occhiali da sole e una bambina
			Tocca i polipi se vedi una bicicletta e un monopattino
			Tocca i polipi se vedi una bicicletta e un ombrello
			Tocca i polipi se vedi una bicicletta e una bambina
			Tocca i polipi se vedi un monopattino e un ombrello
			Tocca i polipi se vedi un monopattino e una bambina
			Tocca i polipi se vedi un ombrello e una bambina
			Tocca i gelati se vedi un leone e una scimmia
			Tocca i gelati se vedi un leone e un pappagallo
			Tocca i gelati se vedi un leone e un polipo
			Tocca i gelati se vedi un leone e un paio di occhiali da sole
			Tocca i gelati se vedi un leone e una bicicletta
			Tocca i gelati se vedi un leone e un monopattino
			Tocca i gelati se vedi un leone e un ombrello
			Tocca i gelati se vedi un leone e una bambina
			Tocca i gelati se vedi una scimmia e un pappagallo
			Tocca i gelati se vedi una scimmia e un polipo
			Tocca i gelati se vedi una scimmia e un paio di occhiali da sole
			Tocca i gelati se vedi una scimmia e una bicicletta
			Tocca i gelati se vedi una scimmia e un monopattino
			Tocca i gelati se vedi una scimmia e un ombrello
			Tocca i gelati se vedi una scimmia e una bambina
			Tocca i gelati se vedi un pappagallo e un polipo
			Tocca i gelati se vedi un pappagallo e un paio di occhiali da sole
			Tocca i gelati se vedi un pappagallo e una bicicletta
			Tocca i gelati se vedi un pappagallo e un monopattino
			Tocca i gelati se vedi un pappagallo e un ombrello
			Tocca i gelati se vedi un pappagallo e una bambina
			Tocca i gelati se vedi un polipo e un paio di occhiali da sole
			Tocca i gelati se vedi un polipo e una bicicletta
			Tocca i gelati se vedi un polipo e un monopattino
			Tocca i gelati se vedi un polipo e un ombrello
			Tocca i gelati se vedi un polipo e una bambina
			Tocca i gelati se vedi un paio di occhiali da sole e una bicicletta
			Tocca i gelati se vedi un paio di occhiali da sole e un monopattino
			Tocca i gelati se vedi un paio di occhiali da sole e un ombrello
			Tocca i gelati se vedi un paio di occhiali da sole e una bambina
			Tocca i gelati se vedi una bicicletta e un monopattino
			Tocca i gelati se vedi una bicicletta e un ombrello
			Tocca i gelati se vedi una bicicletta e una bambina
			Tocca i gelati se vedi un monopattino e un ombrello
			Tocca i gelati se vedi un monopattino e una bambina
			Tocca i gelati se vedi un ombrello e una bambina
			Tocca tutti gli occhiali da sole se vedi un leone e una scimmia
			Tocca tutti gli occhiali da sole se vedi un leone e un pappagallo
			Tocca tutti gli occhiali da sole se vedi un leone e un polipo
			Tocca tutti gli occhiali da sole se vedi un leone e un gelato
			Tocca tutti gli occhiali da sole se vedi un leone e una bicicletta
			Tocca tutti gli occhiali da sole se vedi un leone e un monopattino
			Tocca tutti gli occhiali da sole se vedi un leone e un ombrello
			Tocca tutti gli occhiali da sole se vedi un leone e una bambina
			Tocca tutti gli occhiali da sole se vedi una scimmia e un pappagallo
			Tocca tutti gli occhiali da sole se vedi una scimmia e un polipo
			Tocca tutti gli occhiali da sole se vedi una scimmia e un gelato
			Tocca tutti gli occhiali da sole se vedi una scimmia e una bicicletta
			Tocca tutti gli occhiali da sole se vedi una scimmia e un monopattino
			Tocca tutti gli occhiali da sole se vedi una scimmia e un ombrello
			Tocca tutti gli occhiali da sole se vedi una scimmia e una bambina
			Tocca tutti gli occhiali da sole se vedi un pappagallo e un polipo
			Tocca tutti gli occhiali da sole se vedi un pappagallo e un gelato
			Tocca tutti gli occhiali da sole se vedi un pappagallo e una bicicletta
			Tocca tutti gli occhiali da sole se vedi un pappagallo e un monopattino
			Tocca tutti gli occhiali da sole se vedi un pappagallo e un ombrello
			Tocca tutti gli occhiali da sole se vedi un pappagallo e una bambina
			Tocca tutti gli occhiali da sole se vedi un polipo e un gelato
			Tocca tutti gli occhiali da sole se vedi un polipo e una bicicletta
			Tocca tutti gli occhiali da sole se vedi un polipo e un monopattino
			Tocca tutti gli occhiali da sole se vedi un polipo e un ombrello
			Tocca tutti gli occhiali da sole se vedi un polipo e una bambina
			Tocca tutti gli occhiali da sole se vedi un gelato e una bicicletta
			Tocca tutti gli occhiali da sole se vedi un gelato e un monopattino
			Tocca tutti gli occhiali da sole se vedi un gelato e un ombrello
			Tocca tutti gli occhiali da sole se vedi un gelato e una bambina
			Tocca tutti gli occhiali da sole se vedi una bicicletta e un monopattino
			Tocca tutti gli occhiali da sole se vedi una bicicletta e un ombrello
			Tocca tutti gli occhiali da sole se vedi una bicicletta e una bambina
			Tocca tutti gli occhiali da sole se vedi un monopattino e un ombrello
			Tocca tutti gli occhiali da sole se vedi un monopattino e una bambina
			Tocca tutti gli occhiali da sole se vedi un ombrello e una bambina
			Tocca le biciclette se vedi un leone e una scimmia
			Tocca le biciclette se vedi un leone e un pappagallo
			Tocca le biciclette se vedi un leone e un polipo
			Tocca le biciclette se vedi un leone e un gelato
			Tocca le biciclette se vedi un leone e un paio di occhiali da sole
			Tocca le biciclette se vedi un leone e un monopattino
			Tocca le biciclette se vedi un leone e un ombrello
			Tocca le biciclette se vedi un leone e una bambina
			Tocca le biciclette se vedi una scimmia e un pappagallo
			Tocca le biciclette se vedi una scimmia e un polipo
			Tocca le biciclette se vedi una scimmia e un gelato
			Tocca le biciclette se vedi una scimmia e un paio di occhiali da sole
			Tocca le biciclette se vedi una scimmia e un monopattino
			Tocca le biciclette se vedi una scimmia e un ombrello
			Tocca le biciclette se vedi una scimmia e una bambina
			Tocca le biciclette se vedi un pappagallo e un polipo
			Tocca le biciclette se vedi un pappagallo e un gelato
			Tocca le biciclette se vedi un pappagallo e un paio di occhiali da sole
			Tocca le biciclette se vedi un pappagallo e un monopattino
			Tocca le biciclette se vedi un pappagallo e un ombrello
			Tocca le biciclette se vedi un pappagallo e una bambina
			Tocca le biciclette se vedi un polipo e un gelato
			Tocca le biciclette se vedi un polipo e un paio di occhiali da sole
			Tocca le biciclette se vedi un polipo e un monopattino
			Tocca le biciclette se vedi un polipo e un ombrello
			Tocca le biciclette se vedi un polipo e una bambina
			Tocca le biciclette se vedi un gelato e un paio di occhiali da sole
			Tocca le biciclette se vedi un gelato e un monopattino
			Tocca le biciclette se vedi un gelato e un ombrello
			Tocca le biciclette se vedi un gelato e una bambina
			Tocca le biciclette se vedi un paio di occhiali da sole e un monopattino
			Tocca le biciclette se vedi un paio di occhiali da sole e un ombrello
			Tocca le biciclette se vedi un paio di occhiali da sole e una bambina
			Tocca le biciclette se vedi un monopattino e un ombrello
			Tocca le biciclette se vedi un monopattino e una bambina
			Tocca le biciclette se vedi un ombrello e una bambina
			Tocca i monopattini se vedi un leone e una scimmia
			Tocca i monopattini se vedi un leone e un pappagallo
			Tocca i monopattini se vedi un leone e un polipo
			Tocca i monopattini se vedi un leone e un gelato
			Tocca i monopattini se vedi un leone e un paio di occhiali da sole
			Tocca i monopattini se vedi un leone e una bicicletta
			Tocca i monopattini se vedi un leone e un ombrello
			Tocca i monopattini se vedi un leone e una bambina
			Tocca i monopattini se vedi una scimmia e un pappagallo
			Tocca i monopattini se vedi una scimmia e un polipo
			Tocca i monopattini se vedi una scimmia e un gelato
			Tocca i monopattini se vedi una scimmia e un paio di occhiali da sole
			Tocca i monopattini se vedi una scimmia e una bicicletta
			Tocca i monopattini se vedi una scimmia e un ombrello
			Tocca i monopattini se vedi una scimmia e una bambina
			Tocca i monopattini se vedi un pappagallo e un polipo
			Tocca i monopattini se vedi un pappagallo e un gelato
			Tocca i monopattini se vedi un pappagallo e un paio di occhiali da sole
			Tocca i monopattini se vedi un pappagallo e una bicicletta
			Tocca i monopattini se vedi un pappagallo e un ombrello
			Tocca i monopattini se vedi un pappagallo e una bambina
			Tocca i monopattini se vedi un polipo e un gelato
			Tocca i monopattini se vedi un polipo e un paio di occhiali da sole
			Tocca i monopattini se vedi un polipo e una bicicletta
			Tocca i monopattini se vedi un polipo e un ombrello
			Tocca i monopattini se vedi un polipo e una bambina
			Tocca i monopattini se vedi un gelato e un paio di occhiali da sole
			Tocca i monopattini se vedi un gelato e una bicicletta
			Tocca i monopattini se vedi un gelato e un ombrello
			Tocca i monopattini se vedi un gelato e una bambina
			Tocca i monopattini se vedi un paio di occhiali da sole e una bicicletta
			Tocca i monopattini se vedi un paio di occhiali da sole e un ombrello
			Tocca i monopattini se vedi un paio di occhiali da sole e una bambina
			Tocca i monopattini se vedi una bicicletta e un ombrello
			Tocca i monopattini se vedi una bicicletta e una bambina
			Tocca i monopattini se vedi un ombrello e una bambina
			Tocca gli ombrelli se vedi un leone e una scimmia
			Tocca gli ombrelli se vedi un leone e un pappagallo
			Tocca gli ombrelli se vedi un leone e un polipo
			Tocca gli ombrelli se vedi un leone e un gelato
			Tocca gli ombrelli se vedi un leone e un paio di occhiali da sole
			Tocca gli ombrelli se vedi un leone e una bicicletta
			Tocca gli ombrelli se vedi un leone e un monopattino
			Tocca gli ombrelli se vedi un leone e una bambina
			Tocca gli ombrelli se vedi una scimmia e un pappagallo
			Tocca gli ombrelli se vedi una scimmia e un polipo
			Tocca gli ombrelli se vedi una scimmia e un gelato
			Tocca gli ombrelli se vedi una scimmia e un paio di occhiali da sole
			Tocca gli ombrelli se vedi una scimmia e una bicicletta
			Tocca gli ombrelli se vedi una scimmia e un monopattino
			Tocca gli ombrelli se vedi una scimmia e una bambina
			Tocca gli ombrelli se vedi un pappagallo e un polipo
			Tocca gli ombrelli se vedi un pappagallo e un gelato
			Tocca gli ombrelli se vedi un pappagallo e un paio di occhiali da sole
			Tocca gli ombrelli se vedi un pappagallo e una bicicletta
			Tocca gli ombrelli se vedi un pappagallo e un monopattino
			Tocca gli ombrelli se vedi un pappagallo e una bambina
			Tocca gli ombrelli se vedi un polipo e un gelato
			Tocca gli ombrelli se vedi un polipo e un paio di occhiali da sole
			Tocca gli ombrelli se vedi un polipo e una bicicletta
			Tocca gli ombrelli se vedi un polipo e un monopattino
			Tocca gli ombrelli se vedi un polipo e una bambina
			Tocca gli ombrelli se vedi un gelato e un paio di occhiali da sole
			Tocca gli ombrelli se vedi un gelato e una bicicletta
			Tocca gli ombrelli se vedi un gelato e un monopattino
			Tocca gli ombrelli se vedi un gelato e una bambina
			Tocca gli ombrelli se vedi un paio di occhiali da sole e una bicicletta
			Tocca gli ombrelli se vedi un paio di occhiali da sole e un monopattino
			Tocca gli ombrelli se vedi un paio di occhiali da sole e una bambina
			Tocca gli ombrelli se vedi una bicicletta e un monopattino
			Tocca gli ombrelli se vedi una bicicletta e una bambina
			Tocca gli ombrelli se vedi un monopattino e una bambina
			Tocca le bambine se vedi un leone e una scimmia
			Tocca le bambine se vedi un leone e un pappagallo
			Tocca le bambine se vedi un leone e un polipo
			Tocca le bambine se vedi un leone e un gelato
			Tocca le bambine se vedi un leone e un paio di occhiali da sole
			Tocca le bambine se vedi un leone e una bicicletta
			Tocca le bambine se vedi un leone e un monopattino
			Tocca le bambine se vedi un leone e un ombrello
			Tocca le bambine se vedi una scimmia e un pappagallo
			Tocca le bambine se vedi una scimmia e un polipo
			Tocca le bambine se vedi una scimmia e un gelato
			Tocca le bambine se vedi una scimmia e un paio di occhiali da sole
			Tocca le bambine se vedi una scimmia e una bicicletta
			Tocca le bambine se vedi una scimmia e un monopattino
			Tocca le bambine se vedi una scimmia e un ombrello
			Tocca le bambine se vedi un pappagallo e un polipo
			Tocca le bambine se vedi un pappagallo e un gelato
			Tocca le bambine se vedi un pappagallo e un paio di occhiali da sole
			Tocca le bambine se vedi un pappagallo e una bicicletta
			Tocca le bambine se vedi un pappagallo e un monopattino
			Tocca le bambine se vedi un pappagallo e un ombrello
			Tocca le bambine se vedi un polipo e un gelato
			Tocca le bambine se vedi un polipo e un paio di occhiali da sole
			Tocca le bambine se vedi un polipo e una bicicletta
			Tocca le bambine se vedi un polipo e un monopattino
			Tocca le bambine se vedi un polipo e un ombrello
			Tocca le bambine se vedi un gelato e un paio di occhiali da sole
			Tocca le bambine se vedi un gelato e una bicicletta
			Tocca le bambine se vedi un gelato e un monopattino
			Tocca le bambine se vedi un gelato e un ombrello
			Tocca le bambine se vedi un paio di occhiali da sole e una bicicletta
			Tocca le bambine se vedi un paio di occhiali da sole e un monopattino
			Tocca le bambine se vedi un paio di occhiali da sole e un ombrello
			Tocca le bambine se vedi una bicicletta e un monopattino
			Tocca le bambine se vedi una bicicletta e un ombrello
			Tocca le bambine se vedi un monopattino e un ombrello
	    8QTSUDC2 = Quiz_True capacitive subject double two
			Tocca i leoni se ci sono due scimmie e un pappagallo
			Tocca i leoni se ci sono due scimmie e un polipo
			Tocca i leoni se ci sono due scimmie e un gelato
			Tocca i leoni se ci sono due scimmie e un paio di occhiali da sole
			Tocca i leoni se ci sono due scimmie e una bicicletta
			Tocca i leoni se ci sono due scimmie e un monopattino
			Tocca i leoni se ci sono due scimmie e un ombrello
			Tocca i leoni se ci sono due scimmie e una bambina
			Tocca i leoni se ci sono due pappagalli e una scimmia
			Tocca i leoni se ci sono due pappagalli e un polipo
			Tocca i leoni se ci sono due pappagalli e un gelato
			Tocca i leoni se ci sono due pappagalli e un paio di occhiali da sole
			Tocca i leoni se ci sono due pappagalli e una bicicletta
			Tocca i leoni se ci sono due pappagalli e un monopattino
			Tocca i leoni se ci sono due pappagalli e un ombrello
			Tocca i leoni se ci sono due pappagalli e una bambina
			Tocca i leoni se ci sono due polipi e una scimmia
			Tocca i leoni se ci sono due polipi e un pappagallo
			Tocca i leoni se ci sono due polipi e un gelato
			Tocca i leoni se ci sono due polipi e un paio di occhiali da sole
			Tocca i leoni se ci sono due polipi e una bicicletta
			Tocca i leoni se ci sono due polipi e un monopattino
			Tocca i leoni se ci sono due polipi e un ombrello
			Tocca i leoni se ci sono due polipi e una bambina
			Tocca i leoni se ci sono due gelati e una scimmia
			Tocca i leoni se ci sono due gelati e un pappagallo
			Tocca i leoni se ci sono due gelati e un polipo
			Tocca i leoni se ci sono due gelati e un paio di occhiali da sole
			Tocca i leoni se ci sono due gelati e una bicicletta
			Tocca i leoni se ci sono due gelati e un monopattino
			Tocca i leoni se ci sono due gelati e un ombrello
			Tocca i leoni se ci sono due gelati e una bambina
			Tocca i leoni se ci sono due paia di occhiali da sole e una scimmia
			Tocca i leoni se ci sono due paia di occhiali da sole e un pappagallo
			Tocca i leoni se ci sono due paia di occhiali da sole e un polipo
			Tocca i leoni se ci sono due paia di occhiali da sole e un gelato
			Tocca i leoni se ci sono due paia di occhiali da sole e una bicicletta
			Tocca i leoni se ci sono due paia di occhiali da sole e un monopattino
			Tocca i leoni se ci sono due paia di occhiali da sole e un ombrello
			Tocca i leoni se ci sono due paia di occhiali da sole e una bambina
			Tocca i leoni se ci sono due biciclette e una scimmia
			Tocca i leoni se ci sono due biciclette e un pappagallo
			Tocca i leoni se ci sono due biciclette e un polipo
			Tocca i leoni se ci sono due biciclette e un gelato
			Tocca i leoni se ci sono due biciclette e un paio di occhiali da sole
			Tocca i leoni se ci sono due biciclette e un monopattino
			Tocca i leoni se ci sono due biciclette e un ombrello
			Tocca i leoni se ci sono due biciclette e una bambina
			Tocca i leoni se ci sono due monopattini e una scimmia
			Tocca i leoni se ci sono due monopattini e un pappagallo
			Tocca i leoni se ci sono due monopattini e un polipo
			Tocca i leoni se ci sono due monopattini e un gelato
			Tocca i leoni se ci sono due monopattini e un paio di occhiali da sole
			Tocca i leoni se ci sono due monopattini e una bicicletta
			Tocca i leoni se ci sono due monopattini e un ombrello
			Tocca i leoni se ci sono due monopattini e una bambina
			Tocca i leoni se ci sono due ombrelli e una scimmia
			Tocca i leoni se ci sono due ombrelli e un pappagallo
			Tocca i leoni se ci sono due ombrelli e un polipo
			Tocca i leoni se ci sono due ombrelli e un gelato
			Tocca i leoni se ci sono due ombrelli e un paio di occhiali da sole
			Tocca i leoni se ci sono due ombrelli e una bicicletta
			Tocca i leoni se ci sono due ombrelli e un monopattino
			Tocca i leoni se ci sono due ombrelli e una bambina
			Tocca i leoni se ci sono due bambine e una scimmia
			Tocca i leoni se ci sono due bambine e un pappagallo
			Tocca i leoni se ci sono due bambine e un polipo
			Tocca i leoni se ci sono due bambine e un gelato
			Tocca i leoni se ci sono due bambine e un paio di occhiali da sole
			Tocca i leoni se ci sono due bambine e una bicicletta
			Tocca i leoni se ci sono due bambine e un monopattino
			Tocca i leoni se ci sono due bambine e un ombrello
			Tocca le scimmie se ci sono due leoni e un pappagallo
			Tocca le scimmie se ci sono due leoni e un polipo
			Tocca le scimmie se ci sono due leoni e un gelato
			Tocca le scimmie se ci sono due leoni e un paio di occhiali da sole
			Tocca le scimmie se ci sono due leoni e una bicicletta
			Tocca le scimmie se ci sono due leoni e un monopattino
			Tocca le scimmie se ci sono due leoni e un ombrello
			Tocca le scimmie se ci sono due leoni e una bambina
			Tocca le scimmie se ci sono due pappagalli e un leone
			Tocca le scimmie se ci sono due pappagalli e un polipo
			Tocca le scimmie se ci sono due pappagalli e un gelato
			Tocca le scimmie se ci sono due pappagalli e un paio di occhiali da sole
			Tocca le scimmie se ci sono due pappagalli e una bicicletta
			Tocca le scimmie se ci sono due pappagalli e un monopattino
			Tocca le scimmie se ci sono due pappagalli e un ombrello
			Tocca le scimmie se ci sono due pappagalli e una bambina
			Tocca le scimmie se ci sono due polipi e un leone
			Tocca le scimmie se ci sono due polipi e un pappagallo
			Tocca le scimmie se ci sono due polipi e un gelato
			Tocca le scimmie se ci sono due polipi e un paio di occhiali da sole
			Tocca le scimmie se ci sono due polipi e una bicicletta
			Tocca le scimmie se ci sono due polipi e un monopattino
			Tocca le scimmie se ci sono due polipi e un ombrello
			Tocca le scimmie se ci sono due polipi e una bambina
			Tocca le scimmie se ci sono due gelati e un leone
			Tocca le scimmie se ci sono due gelati e un pappagallo
			Tocca le scimmie se ci sono due gelati e un polipo
			Tocca le scimmie se ci sono due gelati e un paio di occhiali da sole
			Tocca le scimmie se ci sono due gelati e una bicicletta
			Tocca le scimmie se ci sono due gelati e un monopattino
			Tocca le scimmie se ci sono due gelati e un ombrello
			Tocca le scimmie se ci sono due gelati e una bambina
			Tocca le scimmie se ci sono due paia di occhiali da sole e un leone
			Tocca le scimmie se ci sono due paia di occhiali da sole e un pappagallo
			Tocca le scimmie se ci sono due paia di occhiali da sole e un polipo
			Tocca le scimmie se ci sono due paia di occhiali da sole e un gelato
			Tocca le scimmie se ci sono due paia di occhiali da sole e una bicicletta
			Tocca le scimmie se ci sono due paia di occhiali da sole e un monopattino
			Tocca le scimmie se ci sono due paia di occhiali da sole e un ombrello
			Tocca le scimmie se ci sono due paia di occhiali da sole e una bambina
			Tocca le scimmie se ci sono due biciclette e un leone
			Tocca le scimmie se ci sono due biciclette e un pappagallo
			Tocca le scimmie se ci sono due biciclette e un polipo
			Tocca le scimmie se ci sono due biciclette e un gelato
			Tocca le scimmie se ci sono due biciclette e un paio di occhiali da sole
			Tocca le scimmie se ci sono due biciclette e un monopattino
			Tocca le scimmie se ci sono due biciclette e un ombrello
			Tocca le scimmie se ci sono due biciclette e una bambina
			Tocca le scimmie se ci sono due monopattini e un leone
			Tocca le scimmie se ci sono due monopattini e un pappagallo
			Tocca le scimmie se ci sono due monopattini e un polipo
			Tocca le scimmie se ci sono due monopattini e un gelato
			Tocca le scimmie se ci sono due monopattini e un paio di occhiali da sole
			Tocca le scimmie se ci sono due monopattini e una bicicletta
			Tocca le scimmie se ci sono due monopattini e un ombrello
			Tocca le scimmie se ci sono due monopattini e una bambina
			Tocca le scimmie se ci sono due ombrelli e un leone
			Tocca le scimmie se ci sono due ombrelli e un pappagallo
			Tocca le scimmie se ci sono due ombrelli e un polipo
			Tocca le scimmie se ci sono due ombrelli e un gelato
			Tocca le scimmie se ci sono due ombrelli e un paio di occhiali da sole
			Tocca le scimmie se ci sono due ombrelli e una bicicletta
			Tocca le scimmie se ci sono due ombrelli e un monopattino
			Tocca le scimmie se ci sono due ombrelli e una bambina
			Tocca le scimmie se ci sono due bambine e un leone
			Tocca le scimmie se ci sono due bambine e un pappagallo
			Tocca le scimmie se ci sono due bambine e un polipo
			Tocca le scimmie se ci sono due bambine e un gelato
			Tocca le scimmie se ci sono due bambine e un paio di occhiali da sole
			Tocca le scimmie se ci sono due bambine e una bicicletta
			Tocca le scimmie se ci sono due bambine e un monopattino
			Tocca le scimmie se ci sono due bambine e un ombrello
			Tocca i pappagalli se ci sono due leoni e una scimmia
			Tocca i pappagalli se ci sono due leoni e un polipo
			Tocca i pappagalli se ci sono due leoni e un gelato
			Tocca i pappagalli se ci sono due leoni e un paio di occhiali da sole
			Tocca i pappagalli se ci sono due leoni e una bicicletta
			Tocca i pappagalli se ci sono due leoni e un monopattino
			Tocca i pappagalli se ci sono due leoni e un ombrello
			Tocca i pappagalli se ci sono due leoni e una bambina
			Tocca i pappagalli se ci sono due scimmie e una scimmia
			Tocca i pappagalli se ci sono due scimmie e un polipo
			Tocca i pappagalli se ci sono due scimmie e un gelato
			Tocca i pappagalli se ci sono due scimmie e un paio di occhiali da sole
			Tocca i pappagalli se ci sono due scimmie e una bicicletta
			Tocca i pappagalli se ci sono due scimmie e un monopattino
			Tocca i pappagalli se ci sono due scimmie e un ombrello
			Tocca i pappagalli se ci sono due scimmie e una bambina
			Tocca i pappagalli se ci sono due polipi e una scimmia
			Tocca i pappagalli se ci sono due polipi e un leone
			Tocca i pappagalli se ci sono due polipi e un gelato
			Tocca i pappagalli se ci sono due polipi e un paio di occhiali da sole
			Tocca i pappagalli se ci sono due polipi e una bicicletta
			Tocca i pappagalli se ci sono due polipi e un monopattino
			Tocca i pappagalli se ci sono due polipi e un ombrello
			Tocca i pappagalli se ci sono due polipi e una bambina
			Tocca i pappagalli se ci sono due gelati e una scimmia
			Tocca i pappagalli se ci sono due gelati e un leone
			Tocca i pappagalli se ci sono due gelati e un polipo
			Tocca i pappagalli se ci sono due gelati e un paio di occhiali da sole
			Tocca i pappagalli se ci sono due gelati e una bicicletta
			Tocca i pappagalli se ci sono due gelati e un monopattino
			Tocca i pappagalli se ci sono due gelati e un ombrello
			Tocca i pappagalli se ci sono due gelati e una bambina
			Tocca i pappagalli se ci sono due paia di occhiali da sole e una scimmia
			Tocca i pappagalli se ci sono due paia di occhiali da sole e un leone
			Tocca i pappagalli se ci sono due paia di occhiali da sole e un polipo
			Tocca i pappagalli se ci sono due paia di occhiali da sole e un gelato
			Tocca i pappagalli se ci sono due paia di occhiali da sole e una bicicletta
			Tocca i pappagalli se ci sono due paia di occhiali da sole e un monopattino
			Tocca i pappagalli se ci sono due paia di occhiali da sole e un ombrello
			Tocca i pappagalli se ci sono due paia di occhiali da sole e una bambina
			Tocca i pappagalli se ci sono due biciclette e una scimmia
			Tocca i pappagalli se ci sono due biciclette e un leone
			Tocca i pappagalli se ci sono due biciclette e un polipo
			Tocca i pappagalli se ci sono due biciclette e un gelato
			Tocca i pappagalli se ci sono due biciclette e un paio di occhiali da sole
			Tocca i pappagalli se ci sono due biciclette e un monopattino
			Tocca i pappagalli se ci sono due biciclette e un ombrello
			Tocca i pappagalli se ci sono due biciclette e una bambina
			Tocca i pappagalli se ci sono due monopattini e una scimmia
			Tocca i pappagalli se ci sono due monopattini e un leone
			Tocca i pappagalli se ci sono due monopattini e un polipo
			Tocca i pappagalli se ci sono due monopattini e un gelato
			Tocca i pappagalli se ci sono due monopattini e un paio di occhiali da sole
			Tocca i pappagalli se ci sono due monopattini e una bicicletta
			Tocca i pappagalli se ci sono due monopattini e un ombrello
			Tocca i pappagalli se ci sono due monopattini e una bambina
			Tocca i pappagalli se ci sono due ombrelli e una scimmia
			Tocca i pappagalli se ci sono due ombrelli e un leone
			Tocca i pappagalli se ci sono due ombrelli e un polipo
			Tocca i pappagalli se ci sono due ombrelli e un gelato
			Tocca i pappagalli se ci sono due ombrelli e un paio di occhiali da sole
			Tocca i pappagalli se ci sono due ombrelli e una bicicletta
			Tocca i pappagalli se ci sono due ombrelli e un monopattino
			Tocca i pappagalli se ci sono due ombrelli e una bambina
			Tocca i pappagalli se ci sono due bambine e una scimmia
			Tocca i pappagalli se ci sono due bambine e un leone
			Tocca i pappagalli se ci sono due bambine e un polipo
			Tocca i pappagalli se ci sono due bambine e un gelato
			Tocca i pappagalli se ci sono due bambine e un paio di occhiali da sole
			Tocca i pappagalli se ci sono due bambine e una bicicletta
			Tocca i pappagalli se ci sono due bambine e un monopattino
			Tocca i pappagalli se ci sono due bambine e un ombrello
			Tocca i polipi se ci sono due leoni e un pappagallo
			Tocca i polipi se ci sono due leoni e una scimmia
			Tocca i polipi se ci sono due leoni e un gelato
			Tocca i polipi se ci sono due leoni e un paio di occhiali da sole
			Tocca i polipi se ci sono due leoni e una bicicletta
			Tocca i polipi se ci sono due leoni e un monopattino
			Tocca i polipi se ci sono due leoni e un ombrello
			Tocca i polipi se ci sono due leoni e una bambina
			Tocca i polipi se ci sono due scimmie e una scimmia
			Tocca i polipi se ci sono due scimmie e un leone
			Tocca i polipi se ci sono due scimmie e un gelato
			Tocca i polipi se ci sono due scimmie e un paio di occhiali da sole
			Tocca i polipi se ci sono due scimmie e una bicicletta
			Tocca i polipi se ci sono due scimmie e un monopattino
			Tocca i polipi se ci sono due scimmie e un ombrello
			Tocca i polipi se ci sono due scimmie e una bambina
			Tocca i polipi se ci sono due pappagalli e una scimmia
			Tocca i polipi se ci sono due pappagalli e un leone
			Tocca i polipi se ci sono due pappagalli e un gelato
			Tocca i polipi se ci sono due pappagalli e un paio di occhiali da sole
			Tocca i polipi se ci sono due pappagalli e una bicicletta
			Tocca i polipi se ci sono due pappagalli e un monopattino
			Tocca i polipi se ci sono due pappagalli e un ombrello
			Tocca i polipi se ci sono due pappagalli e una bambina
			Tocca i polipi se ci sono due gelati e una scimmia
			Tocca i polipi se ci sono due gelati e un pappagallo
			Tocca i polipi se ci sono due gelati e un leone
			Tocca i polipi se ci sono due gelati e un paio di occhiali da sole
			Tocca i polipi se ci sono due gelati e una bicicletta
			Tocca i polipi se ci sono due gelati e un monopattino
			Tocca i polipi se ci sono due gelati e un ombrello
			Tocca i polipi se ci sono due gelati e una bambina
			Tocca i polipi se ci sono due paia di occhiali da sole e una scimmia
			Tocca i polipi se ci sono due paia di occhiali da sole e un pappagallo
			Tocca i polipi se ci sono due paia di occhiali da sole e un leone
			Tocca i polipi se ci sono due paia di occhiali da sole e un gelato
			Tocca i polipi se ci sono due paia di occhiali da sole e una bicicletta
			Tocca i polipi se ci sono due paia di occhiali da sole e un monopattino
			Tocca i polipi se ci sono due paia di occhiali da sole e un ombrello
			Tocca i polipi se ci sono due paia di occhiali da sole e una bambina
			Tocca i polipi se ci sono due biciclette e una scimmia
			Tocca i polipi se ci sono due biciclette e un pappagallo
			Tocca i polipi se ci sono due biciclette e un leone
			Tocca i polipi se ci sono due biciclette e un gelato
			Tocca i polipi se ci sono due biciclette e un paio di occhiali da sole
			Tocca i polipi se ci sono due biciclette e un monopattino
			Tocca i polipi se ci sono due biciclette e un ombrello
			Tocca i polipi se ci sono due biciclette e una bambina
			Tocca i polipi se ci sono due monopattini e una scimmia
			Tocca i polipi se ci sono due monopattini e un pappagallo
			Tocca i polipi se ci sono due monopattini e un leone
			Tocca i polipi se ci sono due monopattini e un gelato
			Tocca i polipi se ci sono due monopattini e un paio di occhiali da sole
			Tocca i polipi se ci sono due monopattini e una bicicletta
			Tocca i polipi se ci sono due monopattini e un ombrello
			Tocca i polipi se ci sono due monopattini e una bambina
			Tocca i polipi se ci sono due ombrelli e una scimmia
			Tocca i polipi se ci sono due ombrelli e un pappagallo
			Tocca i polipi se ci sono due ombrelli e un leone
			Tocca i polipi se ci sono due ombrelli e un gelato
			Tocca i polipi se ci sono due ombrelli e un paio di occhiali da sole
			Tocca i polipi se ci sono due ombrelli e una bicicletta
			Tocca i polipi se ci sono due ombrelli e un monopattino
			Tocca i polipi se ci sono due ombrelli e una bambina
			Tocca i polipi se ci sono due bambine e una scimmia
			Tocca i polipi se ci sono due bambine e un pappagallo
			Tocca i polipi se ci sono due bambine e un leone
			Tocca i polipi se ci sono due bambine e un gelato
			Tocca i polipi se ci sono due bambine e un paio di occhiali da sole
			Tocca i polipi se ci sono due bambine e una bicicletta
			Tocca i polipi se ci sono due bambine e un monopattino
			Tocca i polipi se ci sono due bambine e un ombrello
			Tocca i gelati se ci sono due leoni e un pappagallo
			Tocca i gelati se ci sono due leoni e un polipo
			Tocca i gelati se ci sono due leoni e una scimmia
			Tocca i gelati se ci sono due leoni e un paio di occhiali da sole
			Tocca i gelati se ci sono due leoni e una bicicletta
			Tocca i gelati se ci sono due leoni e un monopattino
			Tocca i gelati se ci sono due leoni e un ombrello
			Tocca i gelati se ci sono due leoni e una bambina
			Tocca i gelati se ci sono due scimmie e una scimmia
			Tocca i gelati se ci sono due scimmie e un polipo
			Tocca i gelati se ci sono due scimmie e un leone
			Tocca i gelati se ci sono due scimmie e un paio di occhiali da sole
			Tocca i gelati se ci sono due scimmie e una bicicletta
			Tocca i gelati se ci sono due scimmie e un monopattino
			Tocca i gelati se ci sono due scimmie e un ombrello
			Tocca i gelati se ci sono due scimmie e una bambina
			Tocca i gelati se ci sono due polipi e una scimmia
			Tocca i gelati se ci sono due polipi e un pappagallo
			Tocca i gelati se ci sono due polipi e un leone
			Tocca i gelati se ci sono due polipi e un paio di occhiali da sole
			Tocca i gelati se ci sono due polipi e una bicicletta
			Tocca i gelati se ci sono due polipi e un monopattino
			Tocca i gelati se ci sono due polipi e un ombrello
			Tocca i gelati se ci sono due polipi e una bambina
			Tocca i gelati se ci sono due pappagalli e una scimmia
			Tocca i gelati se ci sono due pappagalli e un pappagallo
			Tocca i gelati se ci sono due pappagalli e un polipo
			Tocca i gelati se ci sono due pappagalli e un paio di occhiali da sole
			Tocca i gelati se ci sono due pappagalli e una bicicletta
			Tocca i gelati se ci sono due pappagalli e un monopattino
			Tocca i gelati se ci sono due pappagalli e un ombrello
			Tocca i gelati se ci sono due pappagalli e una bambina
			Tocca i gelati se ci sono due paia di occhiali da sole e una scimmia
			Tocca i gelati se ci sono due paia di occhiali da sole e un pappagallo
			Tocca i gelati se ci sono due paia di occhiali da sole e un polipo
			Tocca i gelati se ci sono due paia di occhiali da sole e un leone
			Tocca i gelati se ci sono due paia di occhiali da sole e una bicicletta
			Tocca i gelati se ci sono due paia di occhiali da sole e un monopattino
			Tocca i gelati se ci sono due paia di occhiali da sole e un ombrello
			Tocca i gelati se ci sono due paia di occhiali da sole e una bambina
			Tocca i gelati se ci sono due biciclette e una scimmia
			Tocca i gelati se ci sono due biciclette e un pappagallo
			Tocca i gelati se ci sono due biciclette e un polipo
			Tocca i gelati se ci sono due biciclette e un leone
			Tocca i gelati se ci sono due biciclette e un paio di occhiali da sole
			Tocca i gelati se ci sono due biciclette e un monopattino
			Tocca i gelati se ci sono due biciclette e un ombrello
			Tocca i gelati se ci sono due biciclette e una bambina
			Tocca i gelati se ci sono due monopattini e una scimmia
			Tocca i gelati se ci sono due monopattini e un pappagallo
			Tocca i gelati se ci sono due monopattini e un polipo
			Tocca i gelati se ci sono due monopattini e un leone
			Tocca i gelati se ci sono due monopattini e un paio di occhiali da sole
			Tocca i gelati se ci sono due monopattini e una bicicletta
			Tocca i gelati se ci sono due monopattini e un ombrello
			Tocca i gelati se ci sono due monopattini e una bambina
			Tocca i gelati se ci sono due ombrelli e una scimmia
			Tocca i gelati se ci sono due ombrelli e un pappagallo
			Tocca i gelati se ci sono due ombrelli e un polipo
			Tocca i gelati se ci sono due ombrelli e un leone
			Tocca i gelati se ci sono due ombrelli e un paio di occhiali da sole
			Tocca i gelati se ci sono due ombrelli e una bicicletta
			Tocca i gelati se ci sono due ombrelli e un monopattino
			Tocca i gelati se ci sono due ombrelli e una bambina
			Tocca i gelati se ci sono due bambine e una scimmia
			Tocca i gelati se ci sono due bambine e un pappagallo
			Tocca i gelati se ci sono due bambine e un polipo
			Tocca i gelati se ci sono due bambine e un leone
			Tocca i gelati se ci sono due bambine e un paio di occhiali da sole
			Tocca i gelati se ci sono due bambine e una bicicletta
			Tocca i gelati se ci sono due bambine e un monopattino
			Tocca i gelati se ci sono due bambine e un ombrello
			Tocca tutti gli occhiali da sole se ci sono due leoni e un pappagallo
			Tocca tutti gli occhiali da sole se ci sono due leoni e un polipo
			Tocca tutti gli occhiali da sole se ci sono due leoni e una scimmia
			Tocca tutti gli occhiali da sole se ci sono due leoni e un gelato
			Tocca tutti gli occhiali da sole se ci sono due leoni e una bicicletta
			Tocca tutti gli occhiali da sole se ci sono due leoni e un monopattino
			Tocca tutti gli occhiali da sole se ci sono due leoni e un ombrello
			Tocca tutti gli occhiali da sole se ci sono due leoni e una bambina
			Tocca tutti gli occhiali da sole se ci sono due scimmie e una scimmia
			Tocca tutti gli occhiali da sole se ci sono due scimmie e un polipo
			Tocca tutti gli occhiali da sole se ci sono due scimmie e un leone
			Tocca tutti gli occhiali da sole se ci sono due scimmie e un gelato
			Tocca tutti gli occhiali da sole se ci sono due scimmie e una bicicletta
			Tocca tutti gli occhiali da sole se ci sono due scimmie e un monopattino
			Tocca tutti gli occhiali da sole se ci sono due scimmie e un ombrello
			Tocca tutti gli occhiali da sole se ci sono due scimmie e una bambina
			Tocca tutti gli occhiali da sole se ci sono due polipi e una scimmia
			Tocca tutti gli occhiali da sole se ci sono due polipi e un pappagallo
			Tocca tutti gli occhiali da sole se ci sono due polipi e un leone
			Tocca tutti gli occhiali da sole se ci sono due polipi e un gelato
			Tocca tutti gli occhiali da sole se ci sono due polipi e una bicicletta
			Tocca tutti gli occhiali da sole se ci sono due polipi e un monopattino
			Tocca tutti gli occhiali da sole se ci sono due polipi e un ombrello
			Tocca tutti gli occhiali da sole se ci sono due polipi e una bambina
			Tocca tutti gli occhiali da sole se ci sono due pappagalli e una scimmia
			Tocca tutti gli occhiali da sole se ci sono due pappagalli e un polipo
			Tocca tutti gli occhiali da sole se ci sono due pappagalli e un gelato
			Tocca tutti gli occhiali da sole se ci sono due pappagalli e un leone
			Tocca tutti gli occhiali da sole se ci sono due pappagalli e una bicicletta
			Tocca tutti gli occhiali da sole se ci sono due pappagalli e un monopattino
			Tocca tutti gli occhiali da sole se ci sono due pappagalli e un ombrello
			Tocca tutti gli occhiali da sole se ci sono due pappagalli e una bambina
			Tocca tutti gli occhiali da sole se ci sono due gelati e una scimmia
			Tocca tutti gli occhiali da sole se ci sono due gelati e un pappagallo
			Tocca tutti gli occhiali da sole se ci sono due gelati e un polipo
			Tocca tutti gli occhiali da sole se ci sono due gelati e un leone
			Tocca tutti gli occhiali da sole se ci sono due gelati e una bicicletta
			Tocca tutti gli occhiali da sole se ci sono due gelati e un monopattino
			Tocca tutti gli occhiali da sole se ci sono due gelati e un ombrello
			Tocca tutti gli occhiali da sole se ci sono due gelati e una bambina
			Tocca tutti gli occhiali da sole se ci sono due biciclette e una scimmia
			Tocca tutti gli occhiali da sole se ci sono due biciclette e un pappagallo
			Tocca tutti gli occhiali da sole se ci sono due biciclette e un polipo
			Tocca tutti gli occhiali da sole se ci sono due biciclette e un gelato
			Tocca tutti gli occhiali da sole se ci sono due biciclette e un leone
			Tocca tutti gli occhiali da sole se ci sono due biciclette e un monopattino
			Tocca tutti gli occhiali da sole se ci sono due biciclette e un ombrello
			Tocca tutti gli occhiali da sole se ci sono due biciclette e una bambina
			Tocca tutti gli occhiali da sole se ci sono due monopattini e una scimmia
			Tocca tutti gli occhiali da sole se ci sono due monopattini e un pappagallo
			Tocca tutti gli occhiali da sole se ci sono due monopattini e un polipo
			Tocca tutti gli occhiali da sole se ci sono due monopattini e un gelato
			Tocca tutti gli occhiali da sole se ci sono due monopattini e un leone
			Tocca tutti gli occhiali da sole se ci sono due monopattini e una bicicletta
			Tocca tutti gli occhiali da sole se ci sono due monopattini e un ombrello
			Tocca tutti gli occhiali da sole se ci sono due monopattini e una bambina
			Tocca tutti gli occhiali da sole se ci sono due ombrelli e una scimmia
			Tocca tutti gli occhiali da sole se ci sono due ombrelli e un pappagallo
			Tocca tutti gli occhiali da sole se ci sono due ombrelli e un polipo
			Tocca tutti gli occhiali da sole se ci sono due ombrelli e un gelato
			Tocca tutti gli occhiali da sole se ci sono due ombrelli e un leone
			Tocca tutti gli occhiali da sole se ci sono due ombrelli e una bicicletta
			Tocca tutti gli occhiali da sole se ci sono due ombrelli e un monopattino
			Tocca tutti gli occhiali da sole se ci sono due ombrelli e una bambina
			Tocca tutti gli occhiali da sole se ci sono due bambine e una scimmia
			Tocca tutti gli occhiali da sole se ci sono due bambine e un pappagallo
			Tocca tutti gli occhiali da sole se ci sono due bambine e un polipo
			Tocca tutti gli occhiali da sole se ci sono due bambine e un gelato
			Tocca tutti gli occhiali da sole se ci sono due bambine e un leone
			Tocca tutti gli occhiali da sole se ci sono due bambine e una bicicletta
			Tocca tutti gli occhiali da sole se ci sono due bambine e un monopattino
			Tocca tutti gli occhiali da sole se ci sono due bambine e un ombrello
			Tocca le biciclette se ci sono due leoni e una scimmia
			Tocca le biciclette se ci sono due leoni e un pappagallo
			Tocca le biciclette se ci sono due leoni e un polipo
			Tocca le biciclette se ci sono due leoni e un gelato
			Tocca le biciclette se ci sono due leoni e un paio di occhiali da sole
			Tocca le biciclette se ci sono due leoni e un monopattino
			Tocca le biciclette se ci sono due leoni e un ombrello
			Tocca le biciclette se ci sono due leoni e una bambina
			Tocca le biciclette se ci sono due scimmie e un leone
			Tocca le biciclette se ci sono due scimmie e un polipo
			Tocca le biciclette se ci sono due scimmie e un gelato
			Tocca le biciclette se ci sono due scimmie e un leone
			Tocca le biciclette se ci sono due scimmie e un paio di occhiali da sole
			Tocca le biciclette se ci sono due scimmie e un monopattino
			Tocca le biciclette se ci sono due scimmie e un ombrello
			Tocca le biciclette se ci sono due scimmie e una bambina
			Tocca le biciclette se ci sono due polipi e una scimmia
			Tocca le biciclette se ci sono due polipi e un pappagallo
			Tocca le biciclette se ci sono due polipi e un gelato
			Tocca le biciclette se ci sono due polipi e un leone
			Tocca le biciclette se ci sono due polipi e un paio di occhiali da sole
			Tocca le biciclette se ci sono due polipi e un monopattino
			Tocca le biciclette se ci sono due polipi e un ombrello
			Tocca le biciclette se ci sono due polipi e una bambina
			Tocca le biciclette se ci sono due pappagalli e una scimmia
			Tocca le biciclette se ci sono due pappagalli e un pappagallo
			Tocca le biciclette se ci sono due pappagalli e un polipo
			Tocca le biciclette se ci sono due pappagalli e un gelato
			Tocca le biciclette se ci sono due pappagalli e un paio di occhiali da sole
			Tocca le biciclette se ci sono due pappagalli e un monopattino
			Tocca le biciclette se ci sono due pappagalli e un ombrello
			Tocca le biciclette se ci sono due pappagalli e una bambina
			Tocca le biciclette se ci sono due gelati e una scimmia
			Tocca le biciclette se ci sono due gelati e un pappagallo
			Tocca le biciclette se ci sono due gelati e un polileonepo
			Tocca le biciclette se ci sono due gelati e un leone
			Tocca le biciclette se ci sono due gelati e un paio di occhiali da sole
			Tocca le biciclette se ci sono due gelati e un monopattino
			Tocca le biciclette se ci sono due gelati e un ombrello
			Tocca le biciclette se ci sono due gelati e una bambina
			Tocca le biciclette se ci sono due paia di occhiali da sole e una scimmia
			Tocca le biciclette se ci sono due paia di occhiali da sole e un pappagallo
			Tocca le biciclette se ci sono due paia di occhiali da sole e un polipo
			Tocca le biciclette se ci sono due paia di occhiali da sole e un leone
			Tocca le biciclette se ci sono due paia di occhiali da sole e un gelato
			Tocca le biciclette se ci sono due paia di occhiali da sole e un monopattino
			Tocca le biciclette se ci sono due paia di occhiali da sole e un ombrello
			Tocca le biciclette se ci sono due paia di occhiali da sole e una bambina
			Tocca le biciclette se ci sono due monopattini e una scimmia
			Tocca le biciclette se ci sono due monopattini e un pappagallo
			Tocca le biciclette se ci sono due monopattini e un polipo
			Tocca le biciclette se ci sono due monopattini e un leone
			Tocca le biciclette se ci sono due monopattini e un gelato
			Tocca le biciclette se ci sono due monopattini e un paio di occhiali da sole
			Tocca le biciclette se ci sono due monopattini e un ombrello
			Tocca le biciclette se ci sono due monopattini e una bambina
			Tocca le biciclette se ci sono due ombrelli e una scimmia
			Tocca le biciclette se ci sono due ombrelli e un pappagallo
			Tocca le biciclette se ci sono due ombrelli e un polipo
			Tocca le biciclette se ci sono due ombrelli e un leone
			Tocca le biciclette se ci sono due ombrelli e un gelato
			Tocca le biciclette se ci sono due ombrelli e un paio di occhiali da sole
			Tocca le biciclette se ci sono due ombrelli e un monopattino
			Tocca le biciclette se ci sono due ombrelli e una bambina
			Tocca le biciclette se ci sono due bambine e una scimmia
			Tocca le biciclette se ci sono due bambine e un pappagallo
			Tocca le biciclette se ci sono due bambine e un polipo
			Tocca le biciclette se ci sono due bambine e un leone
			Tocca le biciclette se ci sono due bambine e un gelato
			Tocca le biciclette se ci sono due bambine e un paio di occhiali da sole
			Tocca le biciclette se ci sono due bambine e un monopattino
			Tocca le biciclette se ci sono due bambine e un ombrello
			Tocca i monopattini se ci sono due leoni e una scimmia
			Tocca i monopattini se ci sono due leoni e un pappagallo
			Tocca i monopattini se ci sono due leoni e un polipo
			Tocca i monopattini se ci sono due leoni e un gelato
			Tocca i monopattini se ci sono due leoni e un paio di occhiali da sole
			Tocca i monopattini se ci sono due leoni e una bicicletta
			Tocca i monopattini se ci sono due leoni e un ombrello
			Tocca i monopattini se ci sono due leoni e una bambina
			Tocca i monopattini se ci sono due scimmie e un leone
			Tocca i monopattini se ci sono due scimmie e un polipo
			Tocca i monopattini se ci sono due scimmie e un gelato
			Tocca i monopattini se ci sono due scimmie e un leone
			Tocca i monopattini se ci sono due scimmie e un paio di occhiali da sole
			Tocca i monopattini se ci sono due scimmie e una bicicletta
			Tocca i monopattini se ci sono due scimmie e un ombrello
			Tocca i monopattini se ci sono due scimmie e una bambina
			Tocca i monopattini se ci sono due polipi e una scimmia
			Tocca i monopattini se ci sono due polipi e un pappagallo
			Tocca i monopattini se ci sono due polipi e un gelato
			Tocca i monopattini se ci sono due polipi e un leone
			Tocca i monopattini se ci sono due polipi e un paio di occhiali da sole
			Tocca i monopattini se ci sono due polipi e una bicicletta
			Tocca i monopattini se ci sono due polipi e un ombrello
			Tocca i monopattini se ci sono due polipi e una bambina
			Tocca i monopattini se ci sono due pappagalli e una scimmia
			Tocca i monopattini se ci sono due pappagalli e un pappagallo
			Tocca i monopattini se ci sono due pappagalli e un polipo
			Tocca i monopattini se ci sono due pappagalli e un gelato
			Tocca i monopattini se ci sono due pappagalli e un paio di occhiali da sole
			Tocca i monopattini se ci sono due pappagalli e una bicicletta
			Tocca i monopattini se ci sono due pappagalli e un ombrello
			Tocca i monopattini se ci sono due pappagalli e una bambina
			Tocca i monopattini se ci sono due gelati e una scimmia
			Tocca i monopattini se ci sono due gelati e un pappagallo
			Tocca i monopattini se ci sono due gelati e un polileonepo
			Tocca i monopattini se ci sono due gelati e un leone
			Tocca i monopattini se ci sono due gelati e un paio di occhiali da sole
			Tocca i monopattini se ci sono due gelati e una bicicletta
			Tocca i monopattini se ci sono due gelati e un ombrello
			Tocca i monopattini se ci sono due gelati e una bambina
			Tocca i monopattini se ci sono due paia di occhiali da sole e una scimmia
			Tocca i monopattini se ci sono due paia di occhiali da sole e un pappagallo
			Tocca i monopattini se ci sono due paia di occhiali da sole e un polipo
			Tocca i monopattini se ci sono due paia di occhiali da sole e un leone
			Tocca i monopattini se ci sono due paia di occhiali da sole e un gelato
			Tocca i monopattini se ci sono due paia di occhiali da sole e una bicicletta
			Tocca i monopattini se ci sono due paia di occhiali da sole e un ombrello
			Tocca i monopattini se ci sono due paia di occhiali da sole e una bambina
			Tocca i monopattini se ci sono due biciclette e una scimmia
			Tocca i monopattini se ci sono due biciclette e un pappagallo
			Tocca i monopattini se ci sono due biciclette e un polipo
			Tocca i monopattini se ci sono due biciclette e un leone
			Tocca i monopattini se ci sono due biciclette e un gelato
			Tocca i monopattini se ci sono due biciclette e un paio di occhiali da sole
			Tocca i monopattini se ci sono due biciclette e un ombrello
			Tocca i monopattini se ci sono due biciclette e una bambina
			Tocca i monopattini se ci sono due ombrelli e una scimmia
			Tocca i monopattini se ci sono due ombrelli e un pappagallo
			Tocca i monopattini se ci sono due ombrelli e un polipo
			Tocca i monopattini se ci sono due ombrelli e un leone
			Tocca i monopattini se ci sono due ombrelli e un gelato
			Tocca i monopattini se ci sono due ombrelli e un paio di occhiali da sole
			Tocca i monopattini se ci sono due ombrelli e una bicicletta
			Tocca i monopattini se ci sono due ombrelli e una bambina
			Tocca i monopattini se ci sono due bambine e una scimmia
			Tocca i monopattini se ci sono due bambine e un pappagallo
			Tocca i monopattini se ci sono due bambine e un polipo
			Tocca i monopattini se ci sono due bambine e un leone
			Tocca i monopattini se ci sono due bambine e un gelato
			Tocca i monopattini se ci sono due bambine e un paio di occhiali da sole
			Tocca i monopattini se ci sono due bambine e una bicicletta
			Tocca i monopattini se ci sono due bambine e un ombrello
			Tocca gli ombrelli se ci sono due leoni e una scimmia
			Tocca gli ombrelli se ci sono due leoni e un pappagallo
			Tocca gli ombrelli se ci sono due leoni e un polipo
			Tocca gli ombrelli se ci sono due leoni e un gelato
			Tocca gli ombrelli se ci sono due leoni e un paio di occhiali da sole
			Tocca gli ombrelli se ci sono due leoni e una bicicletta
			Tocca gli ombrelli se ci sono due leoni e un monopattino
			Tocca gli ombrelli se ci sono due leoni e una bambina
			Tocca gli ombrelli se ci sono due scimmie e un leone
			Tocca gli ombrelli se ci sono due scimmie e un polipo
			Tocca gli ombrelli se ci sono due scimmie e un gelato
			Tocca gli ombrelli se ci sono due scimmie e un leone
			Tocca gli ombrelli se ci sono due scimmie e un paio di occhiali da sole
			Tocca gli ombrelli se ci sono due scimmie e una bicicletta
			Tocca gli ombrelli se ci sono due scimmie e un monopattino
			Tocca gli ombrelli se ci sono due scimmie e una bambina
			Tocca gli ombrelli se ci sono due polipi e una scimmia
			Tocca gli ombrelli se ci sono due polipi e un pappagallo
			Tocca gli ombrelli se ci sono due polipi e un gelato
			Tocca gli ombrelli se ci sono due polipi e un leone
			Tocca gli ombrelli se ci sono due polipi e un paio di occhiali da sole
			Tocca gli ombrelli se ci sono due polipi e una bicicletta
			Tocca gli ombrelli se ci sono due polipi e un monopattino
			Tocca gli ombrelli se ci sono due polipi e una bambina
			Tocca gli ombrelli se ci sono due pappagalli e una scimmia
			Tocca gli ombrelli se ci sono due pappagalli e un pappagallo
			Tocca gli ombrelli se ci sono due pappagalli e un polipo
			Tocca gli ombrelli se ci sono due pappagalli e un gelato
			Tocca gli ombrelli se ci sono due pappagalli e un paio di occhiali da sole
			Tocca gli ombrelli se ci sono due pappagalli e una bicicletta
			Tocca gli ombrelli se ci sono due pappagalli e un monopattino
			Tocca gli ombrelli se ci sono due pappagalli e una bambina
			Tocca gli ombrelli se ci sono due gelati e una scimmia
			Tocca gli ombrelli se ci sono due gelati e un pappagallo
			Tocca gli ombrelli se ci sono due gelati e un polileonepo
			Tocca gli ombrelli se ci sono due gelati e un leone
			Tocca gli ombrelli se ci sono due gelati e un paio di occhiali da sole
			Tocca gli ombrelli se ci sono due gelati e una bicicletta
			Tocca gli ombrelli se ci sono due gelati e un monopattino
			Tocca gli ombrelli se ci sono due gelati e una bambina
			Tocca gli ombrelli se ci sono due paia di occhiali da sole e una scimmia
			Tocca gli ombrelli se ci sono due paia di occhiali da sole e un pappagallo
			Tocca gli ombrelli se ci sono due paia di occhiali da sole e un polipo
			Tocca gli ombrelli se ci sono due paia di occhiali da sole e un leone
			Tocca gli ombrelli se ci sono due paia di occhiali da sole e un gelato
			Tocca gli ombrelli se ci sono due paia di occhiali da sole e una bicicletta
			Tocca gli ombrelli se ci sono due paia di occhiali da sole e un monopattino
			Tocca gli ombrelli se ci sono due paia di occhiali da sole e una bambina
			Tocca gli ombrelli se ci sono due biciclette e una scimmia
			Tocca gli ombrelli se ci sono due biciclette e un pappagallo
			Tocca gli ombrelli se ci sono due biciclette e un polipo
			Tocca gli ombrelli se ci sono due biciclette e un leone
			Tocca gli ombrelli se ci sono due biciclette e un gelato
			Tocca gli ombrelli se ci sono due biciclette e un paio di occhiali da sole
			Tocca gli ombrelli se ci sono due biciclette e un monopattino
			Tocca gli ombrelli se ci sono due biciclette e una bambina
			Tocca gli ombrelli se ci sono due monopattini e una scimmia
			Tocca gli ombrelli se ci sono due monopattini e un pappagallo
			Tocca gli ombrelli se ci sono due monopattini e un polipo
			Tocca gli ombrelli se ci sono due monopattini e un leone
			Tocca gli ombrelli se ci sono due monopattini e un gelato
			Tocca gli ombrelli se ci sono due monopattini e un paio di occhiali da sole
			Tocca gli ombrelli se ci sono due monopattini e una bicicletta
			Tocca gli ombrelli se ci sono due monopattini e una bambina
			Tocca gli ombrelli se ci sono due bambine e una scimmia
			Tocca gli ombrelli se ci sono due bambine e un pappagallo
			Tocca gli ombrelli se ci sono due bambine e un polipo
			Tocca gli ombrelli se ci sono due bambine e un leone
			Tocca gli ombrelli se ci sono due bambine e un gelato
			Tocca gli ombrelli se ci sono due bambine e un paio di occhiali da sole
			Tocca gli ombrelli se ci sono due bambine e una bicicletta
			Tocca gli ombrelli se ci sono due bambine e un monopattino
			Tocca le bambine se ci sono due leoni e una scimmia
			Tocca le bambine se ci sono due leoni e un pappagallo
			Tocca le bambine se ci sono due leoni e un polipo
			Tocca le bambine se ci sono due leoni e un gelato
			Tocca le bambine se ci sono due leoni e un paio di occhiali da sole
			Tocca le bambine se ci sono due leoni e una bicicletta
			Tocca le bambine se ci sono due leoni e un monopattino
			Tocca le bambine se ci sono due leoni e un ombrello
			Tocca le bambine se ci sono due scimmie e un leone
			Tocca le bambine se ci sono due scimmie e un polipo
			Tocca le bambine se ci sono due scimmie e un gelato
			Tocca le bambine se ci sono due scimmie e un leone
			Tocca le bambine se ci sono due scimmie e un paio di occhiali da sole
			Tocca le bambine se ci sono due scimmie e una bicicletta
			Tocca le bambine se ci sono due scimmie e un monopattino
			Tocca le bambine se ci sono due scimmie e un ombrello
			Tocca le bambine se ci sono due polipi e una scimmia
			Tocca le bambine se ci sono due polipi e un pappagallo
			Tocca le bambine se ci sono due polipi e un gelato
			Tocca le bambine se ci sono due polipi e un leone
			Tocca le bambine se ci sono due polipi e un paio di occhiali da sole
			Tocca le bambine se ci sono due polipi e una bicicletta
			Tocca le bambine se ci sono due polipi e un monopattino
			Tocca le bambine se ci sono due polipi e un ombrello
			Tocca le bambine se ci sono due pappagalli e una scimmia
			Tocca le bambine se ci sono due pappagalli e un pappagallo
			Tocca le bambine se ci sono due pappagalli e un polipo
			Tocca le bambine se ci sono due pappagalli e un gelato
			Tocca le bambine se ci sono due pappagalli e un paio di occhiali da sole
			Tocca le bambine se ci sono due pappagalli e una bicicletta
			Tocca le bambine se ci sono due pappagalli e un monopattino
			Tocca le bambine se ci sono due pappagalli e un ombrello
			Tocca le bambine se ci sono due gelati e una scimmia
			Tocca le bambine se ci sono due gelati e un pappagallo
			Tocca le bambine se ci sono due gelati e un polileonepo
			Tocca le bambine se ci sono due gelati e un leone
			Tocca le bambine se ci sono due gelati e un paio di occhiali da sole
			Tocca le bambine se ci sono due gelati e una bicicletta
			Tocca le bambine se ci sono due gelati e un monopattino
			Tocca le bambine se ci sono due gelati e un ombrello
			Tocca le bambine se ci sono due paia di occhiali da sole e una scimmia
			Tocca le bambine se ci sono due paia di occhiali da sole e un pappagallo
			Tocca le bambine se ci sono due paia di occhiali da sole e un polipo
			Tocca le bambine se ci sono due paia di occhiali da sole e un leone
			Tocca le bambine se ci sono due paia di occhiali da sole e un gelato
			Tocca le bambine se ci sono due paia di occhiali da sole e una bicicletta
			Tocca le bambine se ci sono due paia di occhiali da sole e un monopattino
			Tocca le bambine se ci sono due paia di occhiali da sole e un ombrello
			Tocca le bambine se ci sono due biciclette e una scimmia
			Tocca le bambine se ci sono due biciclette e un pappagallo
			Tocca le bambine se ci sono due biciclette e un polipo
			Tocca le bambine se ci sono due biciclette e un leone
			Tocca le bambine se ci sono due biciclette e un gelato
			Tocca le bambine se ci sono due biciclette e un paio di occhiali da sole
			Tocca le bambine se ci sono due biciclette e un monopattino
			Tocca le bambine se ci sono due biciclette e un ombrello
			Tocca le bambine se ci sono due monopattini e una scimmia
			Tocca le bambine se ci sono due monopattini e un pappagallo
			Tocca le bambine se ci sono due monopattini e un polipo
			Tocca le bambine se ci sono due monopattini e un leone
			Tocca le bambine se ci sono due monopattini e un gelato
			Tocca le bambine se ci sono due monopattini e un paio di occhiali da sole
			Tocca le bambine se ci sono due monopattini e una bicicletta
			Tocca le bambine se ci sono due monopattini e un ombrello
			Tocca le bambine se ci sono due ombrelli e una scimmia
			Tocca le bambine se ci sono due ombrelli e un pappagallo
			Tocca le bambine se ci sono due ombrelli e un polipo
			Tocca le bambine se ci sono due ombrelli e un leone
			Tocca le bambine se ci sono due ombrelli e un gelato
			Tocca le bambine se ci sono due ombrelli e un paio di occhiali da sole
			Tocca le bambine se ci sono due ombrelli e una bicicletta
			Tocca le bambine se ci sono due ombrelli e un monopattino    


		    8QTCODC2 = Quiz_True capacitive color double two
			Tocca i leoni se ci sono un disegno blu e un disegno rosso
			Tocca i leoni se ci sono un disegno verde e un disegno rosso
			Tocca i leoni se ci sono un disegno giallo e un disegno rosso
			Tocca i leoni se ci sono un disegno blu e un disegno verde
			Tocca i leoni se ci sono un disegno blu e un disegno giallo
			Tocca i leoni se ci sono un disegno verde e un disegno giallo
			Tocca le scimmie se ci sono un disegno blu e un disegno rosso
			Tocca le scimmie se ci sono un disegno verde e un disegno rosso
			Tocca le scimmie se ci sono un disegno giallo e un disegno rosso
			Tocca le scimmie se ci sono un disegno blu e un disegno verde
			Tocca le scimmie se ci sono un disegno blu e un disegno giallo
			Tocca le scimmie se ci sono un disegno verde e un disegno giallo
			Tocca i pappagalli se ci sono un disegno blu e un disegno rosso
			Tocca i pappagalli se ci sono un disegno verde e un disegno rosso
			Tocca i pappagalli se ci sono un disegno giallo e un disegno rosso
			Tocca i pappagalli se ci sono un disegno blu e un disegno verde
			Tocca i pappagalli se ci sono un disegno blu e un disegno giallo
			Tocca i pappagalli se ci sono un disegno verde e un disegno giallo
			Tocca i polipi se ci sono un disegno blu e un disegno rosso
			Tocca i polipi se ci sono un disegno verde e un disegno rosso
			Tocca i polipi se ci sono un disegno giallo e un disegno rosso
			Tocca i polipi se ci sono un disegno blu e un disegno verde
			Tocca i polipi se ci sono un disegno blu e un disegno giallo
			Tocca i polipi se ci sono un disegno verde e un disegno giallo
			Tocca i gelati se ci sono un disegno blu e un disegno rosso
			Tocca i gelati se ci sono un disegno verde e un disegno rosso
			Tocca i gelati se ci sono un disegno giallo e un disegno rosso
			Tocca i gelati se ci sono un disegno blu e un disegno verde
			Tocca i gelati se ci sono un disegno blu e un disegno giallo
			Tocca i gelati se ci sono un disegno verde e un disegno giallo
			Tocca tutti gli occhiali da sole se ci sono un disegno blu e un disegno rosso
			Tocca tutti gli occhiali da sole se ci sono un disegno verde e un disegno rosso
			Tocca tutti gli occhiali da sole se ci sono un disegno giallo e un disegno rosso
			Tocca tutti gli occhiali da sole se ci sono un disegno blu e un disegno verde
			Tocca tutti gli occhiali da sole se ci sono un disegno blu e un disegno giallo
			Tocca tutti gli occhiali da sole se ci sono un disegno verde e un disegno giallo
			Tocca le biciclette se ci sono un disegno blu e un disegno rosso
			Tocca le biciclette se ci sono un disegno verde e un disegno rosso
			Tocca le biciclette se ci sono un disegno giallo e un disegno rosso
			Tocca le biciclette se ci sono un disegno blu e un disegno verde
			Tocca le biciclette se ci sono un disegno blu e un disegno giallo
			Tocca le biciclette se ci sono un disegno verde e un disegno giallo
			Tocca i monopattini se ci sono un disegno blu e un disegno rosso
			Tocca i monopattini se ci sono un disegno verde e un disegno rosso
			Tocca i monopattini se ci sono un disegno giallo e un disegno rosso
			Tocca i monopattini se ci sono un disegno blu e un disegno verde
			Tocca i monopattini se ci sono un disegno blu e un disegno giallo
			Tocca i monopattini se ci sono un disegno verde e un disegno giallo
			Tocca gli ombrelli se ci sono un disegno blu e un disegno rosso
			Tocca gli ombrelli se ci sono un disegno verde e un disegno rosso
			Tocca gli ombrelli se ci sono un disegno giallo e un disegno rosso
			Tocca gli ombrelli se ci sono un disegno blu e un disegno verde
			Tocca gli ombrelli se ci sono un disegno blu e un disegno giallo
			Tocca gli ombrelli se ci sono un disegno verde e un disegno giallo
			Tocca le bambine se ci sono un disegno blu e un disegno rosso
			Tocca le bambine se ci sono un disegno verde e un disegno rosso
			Tocca le bambine se ci sono un disegno giallo e un disegno rosso
			Tocca le bambine se ci sono un disegno blu e un disegno verde
			Tocca le bambine se ci sono un disegno blu e un disegno giallo
			Tocca le bambine se ci sono un disegno verde e un disegno giallo
			Tocca i leoni se ci sono un disegno blu e due rossi
			Tocca i leoni se ci sono un disegno verde e due rossi
			Tocca i leoni se ci sono un disegno giallo e due rossi
			Tocca i leoni se ci sono un disegno blu e due verdi
			Tocca i leoni se ci sono un disegno blu e due gialli
			Tocca i leoni se ci sono un disegno verde e due gialli
			Tocca le scimmie se ci sono un disegno blu e due rossi
			Tocca le scimmie se ci sono un disegno verde e due rossi
			Tocca le scimmie se ci sono un disegno giallo e due rossi
			Tocca le scimmie se ci sono un disegno blu e due verdi
			Tocca le scimmie se ci sono un disegno blu e due gialli
			Tocca le scimmie se ci sono un disegno verde e due gialli
			Tocca i pappagalli se ci sono un disegno blu e due rossi
			Tocca i pappagalli se ci sono un disegno verde e due rossi
			Tocca i pappagalli se ci sono un disegno giallo e due rossi
			Tocca i pappagalli se ci sono un disegno blu e due verdi
			Tocca i pappagalli se ci sono un disegno blu e due gialli
			Tocca i pappagalli se ci sono un disegno verde e due gialli
			Tocca i polipi se ci sono un disegno blu e due rossi
			Tocca i polipi se ci sono un disegno verde e due rossi
			Tocca i polipi se ci sono un disegno giallo e due rossi
			Tocca i polipi se ci sono un disegno blu e due verdi
			Tocca i polipi se ci sono un disegno blu e due gialli
			Tocca i polipi se ci sono un disegno verde e due gialli
			Tocca i gelati se ci sono un disegno blu e due rossi
			Tocca i gelati se ci sono un disegno verde e due rossi
			Tocca i gelati se ci sono un disegno giallo e due rossi
			Tocca i gelati se ci sono un disegno blu e due verdi
			Tocca i gelati se ci sono un disegno blu e due gialli
			Tocca i gelati se ci sono un disegno verde e due gialli
			Tocca tutti gli occhiali da sole se ci sono un disegno blu e due rossi
			Tocca tutti gli occhiali da sole se ci sono un disegno verde e due rossi
			Tocca tutti gli occhiali da sole se ci sono un disegno giallo e due rossi
			Tocca tutti gli occhiali da sole se ci sono un disegno blu e due verdi
			Tocca tutti gli occhiali da sole se ci sono un disegno blu e due giallis
			Tocca tutti gli occhiali da sole se ci sono un disegno verde e due gialli
			Tocca le biciclette se ci sono un disegno blu e due rossi
			Tocca le biciclette se ci sono un disegno verde e due rossi
			Tocca le biciclette se ci sono un disegno giallo e due rossi
			Tocca le biciclette se ci sono un disegno blu e due verdi
			Tocca le biciclette se ci sono un disegno blu e due gialli
			Tocca le biciclette se ci sono un disegno verde e due gialli
			Tocca i monopattini se ci sono un disegno blu e due rossi
			Tocca i monopattini se ci sono un disegno verde e due rossi
			Tocca i monopattini se ci sono un disegno giallo e due rossi
			Tocca i monopattini se ci sono un disegno blu e due verdi
			Tocca i monopattini se ci sono un disegno blu e due gialli
			Tocca i monopattini se ci sono un disegno verde e due gialli
			Tocca gli ombrelli se ci sono un disegno blu e due rossi
			Tocca gli ombrelli se ci sono un disegno verde e due rossi
			Tocca gli ombrelli se ci sono un disegno giallo e due rossi
			Tocca gli ombrelli se ci sono un disegno blu e due verdi
			Tocca gli ombrelli se ci sono un disegno blu e due gialli
			Tocca gli ombrelli se ci sono un disegno verde e due gialli
			Tocca le bambine se ci sono un disegno blu e due rossi
			Tocca le bambine se ci sono un disegno verde e due rossi
			Tocca le bambine se ci sono un disegno giallo e due rossi
			Tocca le bambine se ci sono un disegno blu e due verdi
			Tocca le bambine se ci sono un disegno blu e due gialli
			Tocca le bambine se ci sono un disegno verde e due gialli		
	    8QTSUSB1 = Quiz_True body subject single one
			Toccami la pancia se c'è un leone
			Toccami la pancia se c'è una scimmia
			Toccami la pancia se c'è un pappagallo
			Toccami la pancia se c'è un polipo
			Toccami la pancia se c'è un gelato
			Toccami la pancia se c'è un paio di occhiali da sole
			Toccami la pancia se c'è una bicicletta
			Toccami la pancia se c'è un monopattino
			Toccami la pancia se c'è un ombrello
			Toccami la pancia se c'è una bambina
	    8QTSUSB2 = Quiz_True body subject single two
			Toccami la pancia se ci sono due leoni
			Toccami la pancia se ci sono due scimmie
			Toccami la pancia se ci sono due pappagallo
			Toccami la pancia se ci sono due polipi
			Toccami la pancia se ci sono due gelati
			Toccami la pancia se ci sono due paia di occhiali
			Toccami la pancia se ci sono due biciclette
			Toccami la pancia se ci sono due monopattini
			Toccami la pancia se ci sono due ombrelli
			Toccami la pancia se ci sono due bambine
	    8QTCOSB1 = Quiz_True body color single one
			Toccami la pancia se cè un disegno rosso
			Toccami la pancia se cè un disegno verde
			Toccami la pancia se cè un disegno giallo
			Toccami la pancia se cè un disegno blu
	    8QTCOSB2 = Quiz_True body color single two
			Toccami la pancia se ci sono due disegni rossi
			Toccami la pancia se ci sono due disegni verdi
			Toccami la pancia se ci sono due disegni gialli
			Toccami la pancia se ci sono due disegni blu
	    8QTKNSB = Quiz_True body knowledge single
			Toccami la pancia se c'è qualcuno che sa parlare
			Toccami la pancia se c'è qualcuno che non sa parlare
			Toccami la pancia se c'è qualcosa che sa volare
			Toccami la pancia se c'è qualcuno che non sa volare
			Toccami la pancia se c'è qualcosa ha le route
			Toccami la pancia se c'è qualcuno che sa nuotare
			Toccami la pancia se c'è qualcuno che non ha le zampe
			Toccami la pancia se c'è qualcuno che cammina su due zampe
			Toccami la pancia se c'è qualcuno che cammina a quattro zampe
			Toccami la pancia se c'è qualcuno che ruggisce
			Toccami la pancia se c'è qualcuno che ha il becco
			Toccami la pancia se c'è qualcuno che può vivere in casa
			Toccami la pancia se c'è qualcuno che non può vivere in città
			Toccami la pancia se c'è qualcuno cosa si può metttere sopra un tavolo
			Toccami la pancia se c'è qualcuno che sa correre
			Toccami la pancia se c'è qualcuno che può stare sugli alberi
			Toccami la pancia se c'è qualcuno che può arrampicarsi
			Toccami la pancia se c'è qualcuno che sa nuotare e correre
			Toccami la pancia se c'è qualcuno che sa correre e parlare
			Toccami la pancia se c'è qualcuno che cammina su due zampee non può volare
		8QTPRSB = Quiz_True body precise_ans single
			Toccami la pancia se ci sono meno di due leoni
			Toccami la pancia se ci sono meno di due scimmie
			Toccami la pancia se ci sono meno di due pappagalli
			Toccami la pancia se ci sono meno di due polipi
			Toccami la pancia se ci sono meno di due gelati
			Toccami la pancia se ci sono meno di due paio di occhiali da sole
			Toccami la pancia se ci sono meno di due biciclette
			Toccami la pancia se ci sono meno di due monopattini
			Toccami la pancia se ci sono meno di due ombrelli
			Toccami la pancia se ci sono meno di due bambine
			Toccami la pancia se c'è più di un leone
			Toccami la pancia se c'è più di una scimmia
			Toccami la pancia se c'è più di un pappagallo
			Toccami la pancia se c'è più di un polipo
			Toccami la pancia se c'è più di un gelato
			Toccami la pancia se c'è più di un paio di occhiali da sole
			Toccami la pancia se c'è più di una bicicletta
			Toccami la pancia se c'è più di un monopattino
			Toccami la pancia se c'è più di un ombrello
			Toccami la pancia se c'è più di una bambina
	    8QTSUDB1 = Quiz_True body subject double one
			Toccami la pancia se vedi un leone e un pappagallo
			Toccami la pancia se vedi un leone e un polipo
			Toccami la pancia se vedi un leone e un gelato
			Toccami la pancia se vedi un leone e un paio di occhiali da sole
			Toccami la pancia se vedi un leone e una bicicletta
			Toccami la pancia se vedi un leone e un monopattino
			Toccami la pancia se vedi un leone e un ombrello
			Toccami la pancia se vedi un leone e una bambina
			Toccami la pancia se vedi una scimmia e un pappagallo
			Toccami la pancia se vedi una scimmia e un polipo
			Toccami la pancia se vedi una scimmia e un gelato
			Toccami la pancia se vedi una scimmia e un paio di occhiali da sole
			Toccami la pancia se vedi una scimmia e una bicicletta
			Toccami la pancia se vedi una scimmia e un monopattino
			Toccami la pancia se vedi una scimmia e un ombrello
			Toccami la pancia se vedi una scimmia e una bambina
			Toccami la pancia se vedi un pappagallo e un polipo
			Toccami la pancia se vedi un pappagallo e un gelato
			Toccami la pancia se vedi un pappagallo e un paio di occhiali da sole
			Toccami la pancia se vedi un pappagallo e una bicicletta
			Toccami la pancia se vedi un pappagallo e un monopattino
			Toccami la pancia se vedi un pappagallo e un ombrello
			Toccami la pancia se vedi un pappagallo e una bambina
			Toccami la pancia se vedi un polipo e un gelato
			Toccami la pancia se vedi un polipo e un paio di occhiali da sole
			Toccami la pancia se vedi un polipo e una bicicletta
			Toccami la pancia se vedi un polipo e un monopattino
			Toccami la pancia se vedi un polipo e un ombrello
			Toccami la pancia se vedi un polipo e una bambina
			Toccami la pancia se vedi un gelato e un paio di occhiali da sole
			Toccami la pancia se vedi un gelato e una bicicletta
			Toccami la pancia se vedi un gelato e un monopattino
			Toccami la pancia se vedi un gelato e un ombrello
			Toccami la pancia se vedi un gelato e una bambina
			Toccami la pancia se vedi un paio di occhiali da sole e una bicicletta
			Toccami la pancia se vedi un paio di occhiali da sole e un monopattino
			Toccami la pancia se vedi un paio di occhiali da sole e un ombrello
			Toccami la pancia se vedi un paio di occhiali da sole e una bambina
			Toccami la pancia se vedi un bicicletta e un monopattino
			Toccami la pancia se vedi un bicicletta e un ombrello
			Toccami la pancia se vedi un bicicletta e una bambina
			Toccami la pancia se vedi un monopattino e un ombrello
			Toccami la pancia se vedi un monopattino e una bambina
			Toccami la pancia se vedi un ombrello e una bambina
	    8QTSUDB2 = Quiz_True body subject double two 
			Toccami la pancia se ci sono due leoni e una scimmia
			Toccami la pancia se ci sono due leoni e un pappagallo
			Toccami la pancia se ci sono due leoni e un polipo
			Toccami la pancia se ci sono due leoni e un gelato
			Toccami la pancia se ci sono due leoni e un paio di occhiali
			Toccami la pancia se ci sono due leoni e una bicicletta
			Toccami la pancia se ci sono due leoni e un monopattino
			Toccami la pancia se ci sono due leoni e un ombrello
			Toccami la pancia se ci sono due leoni e una bambina
			Toccami la pancia se ci sono due scimmie e un leone
			Toccami la pancia se ci sono due scimmie e un polipo
			Toccami la pancia se ci sono due scimmie e un gelato
			Toccami la pancia se ci sono due scimmie e un leone
			Toccami la pancia se ci sono due scimmie e un paio di occhiali
			Toccami la pancia se ci sono due scimmie e una bicicletta
			Toccami la pancia se ci sono due scimmie e un monopattino
			Toccami la pancia se ci sono due scimmie e un ombrello
			Toccami la pancia se ci sono due scimmie e una bambina
			Toccami la pancia se ci sono due polipi e una scimmia
			Toccami la pancia se ci sono due polipi e un pappagallo
			Toccami la pancia se ci sono due polipi e un gelato
			Toccami la pancia se ci sono due polipi e un leone
			Toccami la pancia se ci sono due polipi e un paio di occhiali
			Toccami la pancia se ci sono due polipi e una bicicletta
			Toccami la pancia se ci sono due polipi e un monopattino
			Toccami la pancia se ci sono due polipi e un ombrello
			Toccami la pancia se ci sono due polipi e una bambina
			Toccami la pancia se ci sono due pappagalli e una scimmia
			Toccami la pancia se ci sono due pappagalli e un pappagallo
			Toccami la pancia se ci sono due pappagalli e un polipo
			Toccami la pancia se ci sono due pappagalli e un gelato
			Toccami la pancia se ci sono due pappagalli e un paio di occhiali
			Toccami la pancia se ci sono due pappagalli e una bicicletta
			Toccami la pancia se ci sono due pappagalli e un monopattino
			Toccami la pancia se ci sono due pappagalli e un ombrello
			Toccami la pancia se ci sono due pappagalli e una bambina
			Toccami la pancia se ci sono due gelati e una scimmia
			Toccami la pancia se ci sono due gelati e un pappagallo
			Toccami la pancia se ci sono due gelati e un polileonepo
			Toccami la pancia se ci sono due gelati e un leone
			Toccami la pancia se ci sono due gelati e un paio di occhiali
			Toccami la pancia se ci sono due gelati e una bicicletta
			Toccami la pancia se ci sono due gelati e un monopattino
			Toccami la pancia se ci sono due gelati e un ombrello
			Toccami la pancia se ci sono due gelati e una bambina
			Toccami la pancia se ci sono due paia di occhiali e una scimmia
			Toccami la pancia se ci sono due paia di occhiali e un pappagallo
			Toccami la pancia se ci sono due paia di occhiali e un polipo
			Toccami la pancia se ci sono due paia di occhiali e un leone
			Toccami la pancia se ci sono due paia di occhiali e un gelato
			Toccami la pancia se ci sono due paia di occhiali e una bicicletta
			Toccami la pancia se ci sono due paia di occhiali e un monopattino
			Toccami la pancia se ci sono due paia di occhiali e un ombrello
			Toccami la pancia se ci sono due paia di occhiali e una bambina
			Toccami la pancia se ci sono due biciclette e una scimmia
			Toccami la pancia se ci sono due biciclette e un pappagallo
			Toccami la pancia se ci sono due biciclette e un polipo
			Toccami la pancia se ci sono due biciclette e un leone
			Toccami la pancia se ci sono due biciclette e un gelato
			Toccami la pancia se ci sono due biciclette e un paio di occhiali
			Toccami la pancia se ci sono due biciclette e un monopattino
			Toccami la pancia se ci sono due biciclette e un ombrello
			Toccami la pancia se ci sono due biciclette e una bambina
			Toccami la pancia se ci sono due monopattini e una scimmia
			Toccami la pancia se ci sono due monopattini e un pappagallo
			Toccami la pancia se ci sono due monopattini e un polipo
			Toccami la pancia se ci sono due monopattini e un leone
			Toccami la pancia se ci sono due monopattini e un gelato
			Toccami la pancia se ci sono due monopattini e un paio di occhiali
			Toccami la pancia se ci sono due monopattini e una bicicletta
			Toccami la pancia se ci sono due monopattini e un ombrello
			Toccami la pancia se ci sono due monopattini e una bambina
			Toccami la pancia se ci sono due bambine e una scimmia
			Toccami la pancia se ci sono due bambine e un pappagallo
			Toccami la pancia se ci sono due bambine e un polipo
			Toccami la pancia se ci sono due bambine e un leone
			Toccami la pancia se ci sono due bambine e un gelato
			Toccami la pancia se ci sono due bambine e un paio di occhiali
			Toccami la pancia se ci sono due bambine e una bicicletta
			Toccami la pancia se ci sono due bambine e un monopattino
			Toccami la pancia se ci sono due bambine e un ombrello
	    8QTCODB1 = Quiz_True body color double one
			Toccami la pancia se ci sono un disegno blu e un disegno rosso
			Toccami la pancia se ci sono un disegno verde e un disegno rosso
			Toccami la pancia se ci sono un disegno giallo e un disegno rosso
			Toccami la pancia se ci sono un disegno blu e un disegno verde
			Toccami la pancia se ci sono un disegno blu e un disegno giallo
			Toccami la pancia se ci sono un disegno verde e un disegno giallo    
	    8QTCODB2 = Quiz_True body color double two
			Toccami la pancia se ci sono un disegno blu e due rossi
			Toccami la pancia se ci sono un disegno verde e due rossi
			Toccami la pancia se ci sono un disegno giallo e due rossi
			Toccami la pancia se ci sono un disegno blu e due verdi
			Toccami la pancia se ci sono un disegno blu e due gialli
			Toccami la pancia se ci sono un disegno verde e due gialli
		8QTSUSCOC1 = Quiz_True capacitive subject single one, otherwise capacitive 
			Tocca i leoni se c'è una scimmia altrimenti tocca i pappagalli
			Tocca i leoni se c'è una scimmia altrimenti tocca i polipi
			Tocca i leoni se c'è una scimmia altrimenti tocca i gelati
			Tocca i leoni se c'è una scimmia altrimenti tocca le bambine
			Tocca i leoni se c'è una scimmia altrimenti tocca le biciclette
			Tocca i leoni se c'è una scimmia altrimenti tocca i monopattini
			Tocca i leoni se c'è una scimmia altrimenti tocca tutti gli occhiali da sole
			Tocca i leoni se c'è una scimmia altrimenti tocca l'ombrello
			Tocca i leoni se c'è un pappagallo altrimenti tocca le scimmie
			Tocca i leoni se c'è un pappagallo altrimenti tocca i polipi
			Tocca i leoni se c'è un pappagallo altrimenti tocca i gelati
			Tocca i leoni se c'è un pappagallo altrimenti tocca le bambine
			Tocca i leoni se c'è un pappagallo altrimenti tocca le biciclette
			Tocca i leoni se c'è un pappagallo altrimenti tocca i monopattini
			Tocca i leoni se c'è un pappagallo altrimenti tocca tutti gli occhiali da sole
			Tocca i leoni se c'è un pappagallo altrimenti tocca l'ombrello
			Tocca i leoni se c'è un polipo altrimenti tocca le scimmie
			Tocca i leoni se c'è un polipo altrimenti tocca i leoni
			Tocca i leoni se c'è un polipo altrimenti tocca i gelati
			Tocca i leoni se c'è un polipo altrimenti tocca le bambine
			Tocca i leoni se c'è un polipo altrimenti tocca le biciclette
			Tocca i leoni se c'è un polipo altrimenti tocca i monopattini
			Tocca i leoni se c'è un polipo altrimenti tocca tutti gli occhiali da sole
			Tocca i leoni se c'è un polipo altrimenti tocca l'ombrello
			Tocca i leoni se c'è un gelato altrimenti tocca le scimmie
			Tocca i leoni se c'è un gelato altrimenti tocca i polipi
			Tocca i leoni se c'è un gelato altrimenti tocca le bambine
			Tocca i leoni se c'è un gelato altrimenti tocca le biciclette
			Tocca i leoni se c'è un gelato altrimenti tocca i monopattini
			Tocca i leoni se c'è un gelato altrimenti tocca tutti gli occhiali da sole
			Tocca i leoni se c'è un gelato altrimenti tocca l'ombrello
			Tocca i leoni se c'è un paio di occhiali da sole altrimenti tocca le scimmie
			Tocca i leoni se c'è un paio di occhiali da sole altrimenti tocca i polipi
			Tocca i leoni se c'è un paio di occhiali da sole altrimenti tocca i gelati
			Tocca i leoni se c'è un paio di occhiali da sole altrimenti tocca le bambine
			Tocca i leoni se c'è un paio di occhiali da sole altrimenti tocca le biciclette
			Tocca i leoni se c'è un paio di occhiali da sole altrimenti tocca i monopattini
			Tocca i leoni se c'è un paio di occhiali da sole altrimenti tocca l'ombrello
			Tocca i leoni se c'è una bicicletta altrimenti tocca le scimmie
			Tocca i leoni se c'è una bicicletta altrimenti tocca i polipi
			Tocca i leoni se c'è una bicicletta altrimenti tocca i gelati
			Tocca i leoni se c'è una bicicletta altrimenti tocca i monopattini
			Tocca i leoni se c'è una bicicletta altrimenti tocca tutti gli occhiali da sole
			Tocca i leoni se c'è una bicicletta altrimenti tocca le bambine
			Tocca i leoni se c'è una bicicletta altrimenti tocca l'ombrello
			Tocca i leoni se c'è un monopattino altrimenti tocca le scimmie
			Tocca i leoni se c'è un monopattino altrimenti tocca i polipi
			Tocca i leoni se c'è un monopattino altrimenti tocca i gelati
			Tocca i leoni se c'è un monopattino altrimenti tocca le bambine
			Tocca i leoni se c'è un monopattino altrimenti tocca le biciclette
			Tocca i leoni se c'è un monopattino altrimenti tocca tutti gli occhiali da sole
			Tocca i leoni se c'è un monopattino altrimenti tocca l'ombrello
			Tocca i leoni se c'è un ombrello altrimenti tocca le scimmie
			Tocca i leoni se c'è un ombrello altrimenti tocca i polipi
			Tocca i leoni se c'è un ombrello altrimenti tocca i gelati
			Tocca i leoni se c'è un ombrello altrimenti tocca le biciclette
			Tocca i leoni se c'è un ombrello altrimenti tocca i monopattini
			Tocca i leoni se c'è un ombrello altrimenti tocca tutti gli occhiali da sole
			Tocca i leoni se c'è un ombrello altrimenti tocca le bambine
			Tocca i leoni se c'è una bambina altrimenti tocca le scimmie
			Tocca i leoni se c'è una bambina altrimenti tocca i polipi
			Tocca i leoni se c'è una bambina altrimenti tocca i gelati
			Tocca i leoni se c'è una bambina altrimenti tocca le bambine
			Tocca i leoni se c'è una bambina altrimenti tocca le biciclette
			Tocca i leoni se c'è una bambina altrimenti tocca i monopattini
			Tocca i leoni se c'è una bambina altrimenti tocca tutti gli occhiali da sole
			Tocca i leoni se c'è una bambina altrimenti tocca l'ombrello
			Tocca le scimmie se c'è un leone altrimenti tocca i pappagalli
			Tocca le scimmie se c'è un leone altrimenti tocca i polipi
			Tocca le scimmie se c'è un leone altrimenti tocca i gelati
			Tocca le scimmie se c'è un leone altrimenti tocca le bambine
			Tocca le scimmie se c'è un leone altrimenti tocca le biciclette
			Tocca le scimmie se c'è un leone altrimenti tocca i monopattini
			Tocca le scimmie se c'è un leone altrimenti tocca tutti gli occhiali da sole
			Tocca le scimmie se c'è un leone altrimenti tocca l'ombrello
			Tocca le scimmie se c'è un pappagallo altrimenti tocca i leoni
			Tocca le scimmie se c'è un pappagallo altrimenti tocca i polipi
			Tocca le scimmie se c'è un pappagallo altrimenti tocca i gelati
			Tocca le scimmie se c'è un pappagallo altrimenti tocca le bambine
			Tocca le scimmie se c'è un pappagallo altrimenti tocca le biciclette
			Tocca le scimmie se c'è un pappagallo altrimenti tocca i monopattini
			Tocca le scimmie se c'è un pappagallo altrimenti tocca tutti gli occhiali da sole
			Tocca le scimmie se c'è un pappagallo altrimenti tocca l'ombrello
			Tocca le scimmie se c'è un polipo altrimenti tocca i leoni
			Tocca le scimmie se c'è un polipo altrimenti tocca i pappagalli
			Tocca le scimmie se c'è un polipo altrimenti tocca i gelati
			Tocca le scimmie se c'è un polipo altrimenti tocca le bambine
			Tocca le scimmie se c'è un polipo altrimenti tocca le biciclette
			Tocca le scimmie se c'è un polipo altrimenti tocca i monopattini
			Tocca le scimmie se c'è un polipo altrimenti tocca tutti gli occhiali da sole
			Tocca le scimmie se c'è un polipo altrimenti tocca l'ombrello
			Tocca le scimmie se c'è un gelato altrimenti tocca i leoni
			Tocca le scimmie se c'è un gelato altrimenti tocca i pappagalli
			Tocca le scimmie se c'è un gelato altrimenti tocca i polipi
			Tocca le scimmie se c'è un gelato altrimenti tocca le bambine
			Tocca le scimmie se c'è un gelato altrimenti tocca le biciclette
			Tocca le scimmie se c'è un gelato altrimenti tocca i monopattini
			Tocca le scimmie se c'è un gelato altrimenti tocca tutti gli occhiali da sole
			Tocca le scimmie se c'è un gelato altrimenti tocca l'ombrello
			Tocca le scimmie se c'è un paio di occhiali da sole altrimenti tocca i leoni
			Tocca le scimmie se c'è un paio di occhiali da sole altrimenti tocca i pappagalli
			Tocca le scimmie se c'è un paio di occhiali da sole altrimenti tocca i polipi
			Tocca le scimmie se c'è un paio di occhiali da sole altrimenti tocca i gelati
			Tocca le scimmie se c'è un paio di occhiali da sole altrimenti tocca le bambine
			Tocca le scimmie se c'è un paio di occhiali da sole altrimenti tocca le biciclette
			Tocca le scimmie se c'è un paio di occhiali da sole altrimenti tocca i monopattini
			Tocca le scimmie se c'è un paio di occhiali da sole altrimenti tocca l'ombrello
			Tocca le scimmie se c'è una bicicletta altrimenti tocca i leoni
			Tocca le scimmie se c'è una bicicletta altrimenti tocca i pappagalli
			Tocca le scimmie se c'è una bicicletta altrimenti tocca i polipi
			Tocca le scimmie se c'è una bicicletta altrimenti tocca i gelati
			Tocca le scimmie se c'è una bicicletta altrimenti tocca le bambine
			Tocca le scimmie se c'è una bicicletta altrimenti tocca i monopattini
			Tocca le scimmie se c'è una bicicletta altrimenti tocca tutti gli occhiali da sole
			Tocca le scimmie se c'è una bicicletta altrimenti tocca l'ombrello
			Tocca le scimmie se c'è un monopattino altrimenti tocca i leoni
			Tocca le scimmie se c'è un monopattino altrimenti tocca i pappagalli
			Tocca le scimmie se c'è un monopattino altrimenti tocca i polipi
			Tocca le scimmie se c'è un monopattino altrimenti tocca i gelati
			Tocca le scimmie se c'è un monopattino altrimenti tocca le bambine
			Tocca le scimmie se c'è un monopattino altrimenti tocca le biciclette
			Tocca le scimmie se c'è un monopattino altrimenti tocca tutti gli occhiali da sole
			Tocca le scimmie se c'è un monopattino altrimenti tocca l'ombrello
			Tocca le scimmie se c'è un ombrello altrimenti tocca i leoni
			Tocca le scimmie se c'è un ombrello altrimenti tocca i pappagalli
			Tocca le scimmie se c'è un ombrello altrimenti tocca i polipi
			Tocca le scimmie se c'è un ombrello altrimenti tocca i gelati
			Tocca le scimmie se c'è un ombrello altrimenti tocca le bambine
			Tocca le scimmie se c'è un ombrello altrimenti tocca le biciclette
			Tocca le scimmie se c'è un ombrello altrimenti tocca i monopattini
			Tocca le scimmie se c'è un ombrello altrimenti tocca tutti gli occhiali da sole
			Tocca le scimmie se c'è una bambina altrimenti tocca i leoni
			Tocca le scimmie se c'è una bambina altrimenti tocca i pappagalli
			Tocca le scimmie se c'è una bambina altrimenti tocca i polipi
			Tocca le scimmie se c'è una bambina altrimenti tocca tutti gli occhiali da sole
			Tocca le scimmie se c'è una bambina altrimenti tocca i monopattini
			Tocca le scimmie se c'è una bambina altrimenti tocca i gelati
			Tocca le scimmie se c'è una bambina altrimenti tocca le biciclette
			Tocca le scimmie se c'è una bambina altrimenti tocca l'ombrello
			Tocca i pappagalli se c'è un leone altrimenti tocca le scimmie
			Tocca i pappagalli se c'è un leone altrimenti tocca i polipi
			Tocca i pappagalli se c'è un leone altrimenti tocca i gelati
			Tocca i pappagalli se c'è un leone altrimenti tocca tutti gli occhiali da sole
			Tocca i pappagalli se c'è un leone altrimenti tocca le biciclette
			Tocca i pappagalli se c'è un leone altrimenti tocca i monopattini
			Tocca i pappagalli se c'è un leone altrimenti tocca l'ombrello
			Tocca i pappagalli se c'è un leone altrimenti tocca le bambine
			Tocca i pappagalli se c'è una scimmia altrimenti tocca i leoni
			Tocca i pappagalli se c'è una scimmia altrimenti tocca i polipi
			Tocca i pappagalli se c'è una scimmia altrimenti tocca i gelati
			Tocca i pappagalli se c'è una scimmia altrimenti tocca tutti gli occhiali da sole
			Tocca i pappagalli se c'è una scimmia altrimenti tocca le biciclette
			Tocca i pappagalli se c'è una scimmia altrimenti tocca i monopattini
			Tocca i pappagalli se c'è una scimmia altrimenti tocca l'ombrello
			Tocca i pappagalli se c'è una scimmia altrimenti tocca le bambine
			Tocca i pappagalli se c'è un polipo altrimenti tocca le scimmie
			Tocca i pappagalli se c'è un polipo altrimenti tocca i leoni
			Tocca i pappagalli se c'è un polipo altrimenti tocca i gelati
			Tocca i pappagalli se c'è un polipo altrimenti tocca tutti gli occhiali da sole
			Tocca i pappagalli se c'è un polipo altrimenti tocca le biciclette
			Tocca i pappagalli se c'è un polipo altrimenti tocca i monopattini
			Tocca i pappagalli se c'è un polipo altrimenti tocca le bambine
			Tocca i pappagalli se c'è un polipo altrimenti tocca l'ombrello
			Tocca i pappagalli se c'è un gelato altrimenti tocca le scimmie
			Tocca i pappagalli se c'è un gelato altrimenti tocca i polipi
			Tocca i pappagalli se c'è un gelato altrimenti tocca i leoni
			Tocca i pappagalli se c'è un gelato altrimenti tocca le bambine
			Tocca i pappagalli se c'è un gelato altrimenti tocca le biciclette
			Tocca i pappagalli se c'è un gelato altrimenti tocca i monopattini
			Tocca i pappagalli se c'è un gelato altrimenti tocca tutti gli occhiali da sole
			Tocca i pappagalli se c'è un gelato altrimenti tocca l'ombrello
			Tocca i pappagalli se c'è un paio di occhiali da sole altrimenti tocca le scimmie
			Tocca i pappagalli se c'è un paio di occhiali da sole altrimenti tocca i polipi
			Tocca i pappagalli se c'è un paio di occhiali da sole altrimenti tocca i gelati
			Tocca i pappagalli se c'è un paio di occhiali da sole altrimenti tocca le bambine
			Tocca i pappagalli se c'è un paio di occhiali da sole altrimenti tocca le biciclette
			Tocca i pappagalli se c'è un paio di occhiali da sole altrimenti tocca i monopattini
			Tocca i pappagalli se c'è un paio di occhiali da sole altrimenti tocca i leoni
			Tocca i pappagalli se c'è un paio di occhiali da sole altrimenti tocca l'ombrello
			Tocca i pappagalli se c'è una bicicletta altrimenti tocca le scimmie
			Tocca i pappagalli se c'è una bicicletta altrimenti tocca i polipi
			Tocca i pappagalli se c'è una bicicletta altrimenti tocca i gelati
			Tocca i pappagalli se c'è una bicicletta altrimenti tocca le bambine
			Tocca i pappagalli se c'è una bicicletta altrimenti tocca i leoni
			Tocca i pappagalli se c'è una bicicletta altrimenti tocca i monopattini
			Tocca i pappagalli se c'è una bicicletta altrimenti tocca tutti gli occhiali da sole
			Tocca i pappagalli se c'è una bicicletta altrimenti tocca l'ombrello
			Tocca i pappagalli se c'è un monopattino altrimenti tocca le scimmie
			Tocca i pappagalli se c'è un monopattino altrimenti tocca i polipi
			Tocca i pappagalli se c'è un monopattino altrimenti tocca i gelati
			Tocca i pappagalli se c'è un monopattino altrimenti tocca le bambine
			Tocca i pappagalli se c'è un monopattino altrimenti tocca le biciclette
			Tocca i pappagalli se c'è un monopattino altrimenti tocca i leoni
			Tocca i pappagalli se c'è un monopattino altrimenti tocca tutti gli occhiali da sole
			Tocca i pappagalli se c'è un monopattino altrimenti tocca l'ombrello
			Tocca i pappagalli se c'è un ombrello altrimenti tocca le scimmie
			Tocca i pappagalli se c'è un ombrello altrimenti tocca i polipi
			Tocca i pappagalli se c'è un ombrello altrimenti tocca i gelati
			Tocca i pappagalli se c'è un ombrello altrimenti tocca le bambine
			Tocca i pappagalli se c'è un ombrello altrimenti tocca le biciclette
			Tocca i pappagalli se c'è un ombrello altrimenti tocca i monopattini
			Tocca i pappagalli se c'è un ombrello altrimenti tocca tutti gli occhiali da sole
			Tocca i pappagalli se c'è un ombrello altrimenti tocca i leoni
			Tocca i pappagalli se c'è una bambina altrimenti tocca le scimmie
			Tocca i pappagalli se c'è una bambina altrimenti tocca i polipi
			Tocca i pappagalli se c'è una bambina altrimenti tocca i gelati
			Tocca i pappagalli se c'è una bambina altrimenti tocca i leoni
			Tocca i pappagalli se c'è una bambina altrimenti tocca le biciclette
			Tocca i pappagalli se c'è una bambina altrimenti tocca i monopattini
			Tocca i pappagalli se c'è una bambina altrimenti tocca tutti gli occhiali da sole
			Tocca i pappagalli se c'è una bambina altrimenti tocca l'ombrello
			Tocca i polipi se c'è un leone altrimenti tocca le scimmie
			Tocca i polipi se c'è un leone altrimenti tocca i pappagalli
			Tocca i polipi se c'è un leone altrimenti tocca i gelati
			Tocca i polipi se c'è un leone altrimenti tocca le bambine
			Tocca i polipi se c'è un leone altrimenti tocca le biciclette
			Tocca i polipi se c'è un leone altrimenti tocca i monopattini
			Tocca i polipi se c'è un leone altrimenti tocca tutti gli occhiali da sole
			Tocca i polipi se c'è un leone altrimenti tocca l'ombrello
			Tocca i polipi se c'è una scimmia altrimenti tocca i leoni
			Tocca i polipi se c'è una scimmia altrimenti tocca i pappagalli
			Tocca i polipi se c'è una scimmia altrimenti tocca i gelati
			Tocca i polipi se c'è una scimmia altrimenti tocca le bambine
			Tocca i polipi se c'è una scimmia altrimenti tocca le biciclette
			Tocca i polipi se c'è una scimmia altrimenti tocca i monopattini
			Tocca i polipi se c'è una scimmia altrimenti tocca tutti gli occhiali da sole
			Tocca i polipi se c'è una scimmia altrimenti tocca l'ombrello
			Tocca i polipi se c'è un pappagallo altrimenti tocca le scimmie
			Tocca i polipi se c'è un pappagallo altrimenti tocca i leoni
			Tocca i polipi se c'è un pappagallo altrimenti tocca i gelati
			Tocca i polipi se c'è un pappagallo altrimenti tocca le bambine
			Tocca i polipi se c'è un pappagallo altrimenti tocca le biciclette
			Tocca i polipi se c'è un pappagallo altrimenti tocca i monopattini
			Tocca i polipi se c'è un pappagallo altrimenti tocca tutti gli occhiali da sole
			Tocca i polipi se c'è un pappagallo altrimenti tocca l'ombrello
			Tocca i polipi se c'è un gelato altrimenti tocca le scimmie
			Tocca i polipi se c'è un gelato altrimenti tocca i pappagalli
			Tocca i polipi se c'è un gelato altrimenti tocca i leoni
			Tocca i polipi se c'è un gelato altrimenti tocca i gelati
			Tocca i polipi se c'è un gelato altrimenti tocca le bambine
			Tocca i polipi se c'è un gelato altrimenti tocca le biciclette
			Tocca i polipi se c'è un gelato altrimenti tocca i monopattini
			Tocca i polipi se c'è un gelato altrimenti tocca tutti gli occhiali da sole
			Tocca i polipi se c'è un gelato altrimenti tocca l'ombrello
			Tocca i polipi se c'è un paio di occhiali da sole altrimenti tocca le scimmie
			Tocca i polipi se c'è un paio di occhiali da sole altrimenti tocca i pappagalli
			Tocca i polipi se c'è un paio di occhiali da sole altrimenti tocca i gelati
			Tocca i polipi se c'è un paio di occhiali da sole altrimenti tocca le bambine
			Tocca i polipi se c'è un paio di occhiali da sole altrimenti tocca le biciclette
			Tocca i polipi se c'è un paio di occhiali da sole altrimenti tocca i monopattini
			Tocca i polipi se c'è un paio di occhiali da sole altrimenti tocca i leoni
			Tocca i polipi se c'è un paio di occhiali da sole altrimenti tocca l occhiali da sole
			Tocca i polipi se c'è una bicicletta altrimenti tocca le scimmie
			Tocca i polipi se c'è una bicicletta altrimenti tocca i pappagalli
			Tocca i polipi se c'è una bicicletta altrimenti tocca i gelati
			Tocca i polipi se c'è una bicicletta altrimenti tocca le bambine
			Tocca i polipi se c'è una bicicletta altrimenti tocca i leoni
			Tocca i polipi se c'è una bicicletta altrimenti tocca i monopattini
			Tocca i polipi se c'è una bicicletta altrimenti tocca tutti gli occhiali da sole
			Tocca i polipi se c'è una bicicletta altrimenti tocca l'ombrello
			Tocca i polipi se c'è un monopattino altrimenti tocca le scimmie
			Tocca i polipi se c'è un monopattino altrimenti tocca i pappagalli
			Tocca i polipi se c'è un monopattino altrimenti tocca i gelati
			Tocca i polipi se c'è un monopattino altrimenti tocca le bambine
			Tocca i polipi se c'è un monopattino altrimenti tocca le biciclette
			Tocca i polipi se c'è un monopattino altrimenti tocca i leoni
			Tocca i polipi se c'è un monopattino altrimenti tocca tutti gli occhiali da sole
			Tocca i polipi se c'è un monopattino altrimenti tocca l'ombrello
			Tocca i polipi se c'è un ombrello altrimenti tocca le scimmie
			Tocca i polipi se c'è un ombrello altrimenti tocca i pappagalli
			Tocca i polipi se c'è un ombrello altrimenti tocca i gelati
			Tocca i polipi se c'è un ombrello altrimenti tocca le bambine
			Tocca i polipi se c'è un ombrello altrimenti tocca le biciclette
			Tocca i polipi se c'è un ombrello altrimenti tocca i monopattini
			Tocca i polipi se c'è un ombrello altrimenti tocca tutti gli occhiali da sole
			Tocca i polipi se c'è un ombrello altrimenti tocca i leoni
			Tocca i polipi se c'è una bambina altrimenti tocca le scimmie
			Tocca i polipi se c'è una bambina altrimenti tocca i pappagalli
			Tocca i polipi se c'è una bambina altrimenti tocca i gelati
			Tocca i polipi se c'è una bambina altrimenti tocca i leoni
			Tocca i polipi se c'è una bambina altrimenti tocca le biciclette
			Tocca i polipi se c'è una bambina altrimenti tocca i monopattini
			Tocca i polipi se c'è una bambina altrimenti tocca tutti gli occhiali da sole
			Tocca i polipi se c'è una bambina altrimenti tocca l'ombrello
			Tocca i gelati se c'è un leone altrimenti tocca le scimmie
			Tocca i gelati se c'è un leone altrimenti tocca i polipi
			Tocca i gelati se c'è un leone altrimenti tocca i pappagalli
			Tocca i gelati se c'è un leone altrimenti tocca le bambine
			Tocca i gelati se c'è un leone altrimenti tocca le biciclette
			Tocca i gelati se c'è un leone altrimenti tocca i monopattini
			Tocca i gelati se c'è un leone altrimenti tocca tutti gli occhiali da sole
			Tocca i gelati se c'è un leone altrimenti tocca l'ombrello
			Tocca i gelati se c'è una scimmia altrimenti tocca i pappagalli
			Tocca i gelati se c'è una scimmia altrimenti tocca i polipi
			Tocca i gelati se c'è una scimmia altrimenti tocca i leoni
			Tocca i gelati se c'è una scimmia altrimenti tocca le bambine
			Tocca i gelati se c'è una scimmia altrimenti tocca le biciclette
			Tocca i gelati se c'è una scimmia altrimenti tocca i monopattini
			Tocca i gelati se c'è una scimmia altrimenti tocca tutti gli occhiali da sole
			Tocca i gelati se c'è una scimmia altrimenti tocca l'ombrello
			Tocca i gelati se c'è un pappagallo altrimenti tocca le scimmie
			Tocca i gelati se c'è un pappagallo altrimenti tocca i polipi
			Tocca i gelati se c'è un pappagallo altrimenti tocca i leoni
			Tocca i gelati se c'è un pappagallo altrimenti tocca le bambine
			Tocca i gelati se c'è un pappagallo altrimenti tocca le biciclette
			Tocca i gelati se c'è un pappagallo altrimenti tocca i monopattini
			Tocca i gelati se c'è un pappagallo altrimenti tocca tutti gli occhiali da sole
			Tocca i gelati se c'è un pappagallo altrimenti tocca l'ombrello
			Tocca i gelati se c'è un polipo altrimenti tocca le scimmie
			Tocca i gelati se c'è un polipo altrimenti tocca i pappagalli
			Tocca i gelati se c'è un polipo altrimenti tocca i leoni
			Tocca i gelati se c'è un polipo altrimenti tocca le bambine
			Tocca i gelati se c'è un polipo altrimenti tocca le biciclette
			Tocca i gelati se c'è un polipo altrimenti tocca i monopattini
			Tocca i gelati se c'è un polipo altrimenti tocca tutti gli occhiali da sole
			Tocca i gelati se c'è un polipo altrimenti tocca l'ombrello
			Tocca i gelati se c'è un paio di occhiali da sole altrimenti tocca le scimmie
			Tocca i gelati se c'è un paio di occhiali da sole altrimenti tocca i pappagalli
			Tocca i gelati se c'è un paio di occhiali da sole altrimenti tocca i polipi
			Tocca i gelati se c'è un paio di occhiali da sole altrimenti tocca i leoni
			Tocca i gelati se c'è un paio di occhiali da sole altrimenti tocca le bambine
			Tocca i gelati se c'è un paio di occhiali da sole altrimenti tocca le biciclette
			Tocca i gelati se c'è un paio di occhiali da sole altrimenti tocca i monopattini
			Tocca i gelati se c'è un paio di occhiali da sole altrimenti tocca l'ombrello
			Tocca i gelati se c'è una bicicletta altrimenti tocca le scimmie
			Tocca i gelati se c'è una bicicletta altrimenti tocca i polipi
			Tocca i gelati se c'è una bicicletta altrimenti tocca i pappagalli
			Tocca i gelati se c'è una bicicletta altrimenti tocca le bambine
			Tocca i gelati se c'è una bicicletta altrimenti tocca i leoni
			Tocca i gelati se c'è una bicicletta altrimenti tocca i monopattini
			Tocca i gelati se c'è una bicicletta altrimenti tocca tutti gli occhiali da sole
			Tocca i gelati se c'è una bicicletta altrimenti tocca l'ombrello
			Tocca i gelati se c'è un monopattino altrimenti tocca le scimmie
			Tocca i gelati se c'è un monopattino altrimenti tocca i pappagalli
			Tocca i gelati se c'è un monopattino altrimenti tocca i polipi
			Tocca i gelati se c'è un monopattino altrimenti tocca i leoni
			Tocca i gelati se c'è un monopattino altrimenti tocca le bambine
			Tocca i gelati se c'è un monopattino altrimenti tocca le biciclette
			Tocca i gelati se c'è un monopattino altrimenti tocca tutti gli occhiali da sole
			Tocca i gelati se c'è un monopattino altrimenti tocca l'ombrello
			Tocca i gelati se c'è un ombrello altrimenti tocca le scimmie
			Tocca i gelati se c'è un ombrello altrimenti tocca i pappagalli
			Tocca i gelati se c'è un ombrello altrimenti tocca i polipi
			Tocca i gelati se c'è un ombrello altrimenti tocca i leoni
			Tocca i gelati se c'è un ombrello altrimenti tocca le bambine
			Tocca i gelati se c'è un ombrello altrimenti tocca le biciclette
			Tocca i gelati se c'è un ombrello altrimenti tocca i monopattini
			Tocca i gelati se c'è un ombrello altrimenti tocca tutti gli occhiali da sole
			Tocca i gelati se c'è una bambina altrimenti tocca le scimmie
			Tocca i gelati se c'è una bambina altrimenti tocca i polipi
			Tocca i gelati se c'è una bambina altrimenti tocca i pappagalli
			Tocca i gelati se c'è una bambina altrimenti tocca i leoni
			Tocca i gelati se c'è una bambina altrimenti tocca le biciclette
			Tocca i gelati se c'è una bambina altrimenti tocca i monopattini
			Tocca i gelati se c'è una bambina altrimenti tocca tutti gli occhiali da sole
			Tocca i gelati se c'è una bambina altrimenti tocca l'ombrello
			Tocca tutti gli occhiali da sole se c'è un leone altrimenti tocca le scimmie
			Tocca tutti gli occhiali da sole se c'è un leone altrimenti tocca i pappagalli
			Tocca tutti gli occhiali da sole se c'è un leone altrimenti tocca i polipi
			Tocca tutti gli occhiali da sole se c'è un leone altrimenti tocca i gelati
			Tocca tutti gli occhiali da sole se c'è un leone altrimenti tocca le biciclette
			Tocca tutti gli occhiali da sole se c'è un leone altrimenti tocca i monopattini
			Tocca tutti gli occhiali da sole se c'è un leone altrimenti tocca l'ombrello
			Tocca tutti gli occhiali da sole se c'è un leone altrimenti tocca le bambine
			Tocca tutti gli occhiali da sole se c'è una scimmia altrimenti tocca i leoni
			Tocca tutti gli occhiali da sole se c'è una scimmia altrimenti tocca i pappagalli
			Tocca tutti gli occhiali da sole se c'è una scimmia altrimenti tocca i polipi
			Tocca tutti gli occhiali da sole se c'è una scimmia altrimenti tocca i gelati
			Tocca tutti gli occhiali da sole se c'è una scimmia altrimenti tocca le bambine
			Tocca tutti gli occhiali da sole se c'è una scimmia altrimenti tocca le biciclette
			Tocca tutti gli occhiali da sole se c'è una scimmia altrimenti tocca i monopattini
			Tocca tutti gli occhiali da sole se c'è una scimmia altrimenti tocca l'ombrello
			Tocca tutti gli occhiali da sole se c'è un pappagallo altrimenti tocca i leoni
			Tocca tutti gli occhiali da sole se c'è un pappagallo altrimenti tocca le scimmie
			Tocca tutti gli occhiali da sole se c'è un pappagallo altrimenti tocca i polipi
			Tocca tutti gli occhiali da sole se c'è un pappagallo altrimenti tocca i gelati
			Tocca tutti gli occhiali da sole se c'è un pappagallo altrimenti tocca le biciclette
			Tocca tutti gli occhiali da sole se c'è un pappagallo altrimenti tocca i monopattini
			Tocca tutti gli occhiali da sole se c'è un pappagallo altrimenti tocca le bambine
			Tocca tutti gli occhiali da sole se c'è un pappagallo altrimenti tocca l'ombrello
			Tocca tutti gli occhiali da sole se c'è un polipo altrimenti tocca i leoni
			Tocca tutti gli occhiali da sole se c'è un polipo altrimenti tocca le scimmie
			Tocca tutti gli occhiali da sole se c'è un polipo altrimenti tocca i pappagalli
			Tocca tutti gli occhiali da sole se c'è un polipo altrimenti tocca i gelati
			Tocca tutti gli occhiali da sole se c'è un polipo altrimenti tocca le biciclette
			Tocca tutti gli occhiali da sole se c'è un polipo altrimenti tocca i monopattini
			Tocca tutti gli occhiali da sole se c'è un polipo altrimenti tocca le bambine
			Tocca tutti gli occhiali da sole se c'è un polipo altrimenti tocca l'ombrello
			Tocca tutti gli occhiali da sole se c'è un gelato altrimenti tocca i leoni
			Tocca tutti gli occhiali da sole se c'è un gelato altrimenti tocca le scimmie
			Tocca tutti gli occhiali da sole se c'è un gelato altrimenti tocca i pappagalli
			Tocca tutti gli occhiali da sole se c'è un gelato altrimenti tocca i polipi
			Tocca tutti gli occhiali da sole se c'è un gelato altrimenti tocca le biciclette
			Tocca tutti gli occhiali da sole se c'è un gelato altrimenti tocca i monopattini
			Tocca tutti gli occhiali da sole se c'è un gelato altrimenti tocca le bambine
			Tocca tutti gli occhiali da sole se c'è un gelato altrimenti tocca l'ombrello
			Tocca tutti gli occhiali da sole se c'è una bicicletta altrimenti tocca i leoni
			Tocca tutti gli occhiali da sole se c'è una bicicletta altrimenti tocca le scimmie
			Tocca tutti gli occhiali da sole se c'è una bicicletta altrimenti tocca i pappagalli
			Tocca tutti gli occhiali da sole se c'è una bicicletta altrimenti tocca i polipi
			Tocca tutti gli occhiali da sole se c'è una bicicletta altrimenti tocca i gelati
			Tocca tutti gli occhiali da sole se c'è una bicicletta altrimenti tocca le bambine
			Tocca tutti gli occhiali da sole se c'è una bicicletta altrimenti tocca i monopattini
			Tocca tutti gli occhiali da sole se c'è una bicicletta altrimenti tocca l'ombrello
			Tocca tutti gli occhiali da sole se c'è un monopattino altrimenti tocca i leoni
			Tocca tutti gli occhiali da sole se c'è un monopattino altrimenti tocca le scimmie
			Tocca tutti gli occhiali da sole se c'è un monopattino altrimenti tocca i pappagalli
			Tocca tutti gli occhiali da sole se c'è un monopattino altrimenti tocca i polipi
			Tocca tutti gli occhiali da sole se c'è un monopattino altrimenti tocca i gelati
			Tocca tutti gli occhiali da sole se c'è un monopattino altrimenti tocca le bambine
			Tocca tutti gli occhiali da sole se c'è un monopattino altrimenti tocca le biciclette
			Tocca tutti gli occhiali da sole se c'è un monopattino altrimenti tocca l'ombrello
			Tocca tutti gli occhiali da sole se c'è un ombrello altrimenti tocca i leoni
			Tocca tutti gli occhiali da sole se c'è un ombrello altrimenti tocca le scimmie
			Tocca tutti gli occhiali da sole se c'è un ombrello altrimenti tocca i pappagalli
			Tocca tutti gli occhiali da sole se c'è un ombrello altrimenti tocca i polipi
			Tocca tutti gli occhiali da sole se c'è un ombrello altrimenti tocca i gelati
			Tocca tutti gli occhiali da sole se c'è un ombrello altrimenti tocca le bambine
			Tocca tutti gli occhiali da sole se c'è un ombrello altrimenti tocca le biciclette
			Tocca tutti gli occhiali da sole se c'è un ombrello altrimenti tocca i monopattini
			Tocca tutti gli occhiali da sole se c'è una bambina altrimenti tocca i leoni
			Tocca tutti gli occhiali da sole se c'è una bambina altrimenti tocca le scimmie
			Tocca tutti gli occhiali da sole se c'è una bambina altrimenti tocca i pappagalli
			Tocca tutti gli occhiali da sole se c'è una bambina altrimenti tocca i polipi
			Tocca tutti gli occhiali da sole se c'è una bambina altrimenti tocca i gelati
			Tocca tutti gli occhiali da sole se c'è una bambina altrimenti tocca le biciclette
			Tocca tutti gli occhiali da sole se c'è una bambina altrimenti tocca i monopattini
			Tocca tutti gli occhiali da sole se c'è una bambina altrimenti tocca l'ombrello
			Tocca le biciclette se c'è un leone altrimenti tocca le scimmie
			Tocca le biciclette se c'è un leone altrimenti tocca i pappagalli
			Tocca le biciclette se c'è un leone altrimenti tocca i polipi
			Tocca le biciclette se c'è un leone altrimenti tocca i gelati
			Tocca le biciclette se c'è un leone altrimenti tocca i monopattini
			Tocca le biciclette se c'è un leone altrimenti tocca tutti gli occhiali da sole
			Tocca le biciclette se c'è un leone altrimenti tocca le bambine
			Tocca le biciclette se c'è un leone altrimenti tocca l'ombrello
			Tocca le biciclette se c'è una scimmia altrimenti tocca i leoni
			Tocca le biciclette se c'è una scimmia altrimenti tocca i pappagalli
			Tocca le biciclette se c'è una scimmia altrimenti tocca i polipi
			Tocca le biciclette se c'è una scimmia altrimenti tocca i gelati
			Tocca le biciclette se c'è una scimmia altrimenti tocca i monopattini
			Tocca le biciclette se c'è una scimmia altrimenti tocca tutti gli occhiali da sole
			Tocca le biciclette se c'è una scimmia altrimenti tocca le bambine
			Tocca le biciclette se c'è una scimmia altrimenti tocca l'ombrello
			Tocca le biciclette se c'è un pappagallo altrimenti tocca i leoni
			Tocca le biciclette se c'è un pappagallo altrimenti tocca le scimmie
			Tocca le biciclette se c'è un pappagallo altrimenti tocca i polipi
			Tocca le biciclette se c'è un pappagallo altrimenti tocca i gelati
			Tocca le biciclette se c'è un pappagallo altrimenti tocca le bambine
			Tocca le biciclette se c'è un pappagallo altrimenti tocca i monopattini
			Tocca le biciclette se c'è un pappagallo altrimenti tocca tutti gli occhiali da sole
			Tocca le biciclette se c'è un pappagallo altrimenti tocca l'ombrello
			Tocca le biciclette se c'è un polipo altrimenti tocca i leoni
			Tocca le biciclette se c'è un polipo altrimenti tocca le scimmie
			Tocca le biciclette se c'è un polipo altrimenti tocca i pappagalli
			Tocca le biciclette se c'è un polipo altrimenti tocca i gelati
			Tocca le biciclette se c'è un polipo altrimenti tocca le bambine
			Tocca le biciclette se c'è un polipo altrimenti tocca i monopattini
			Tocca le biciclette se c'è un polipo altrimenti tocca tutti gli occhiali da sole
			Tocca le biciclette se c'è un polipo altrimenti tocca l occhiali da sole
			Tocca le biciclette se c'è un gelato altrimenti tocca i leoni
			Tocca le biciclette se c'è un gelato altrimenti tocca le scimmie
			Tocca le biciclette se c'è un gelato altrimenti tocca i pappagalli
			Tocca le biciclette se c'è un gelato altrimenti tocca i polipi
			Tocca le biciclette se c'è un gelato altrimenti tocca i monopattini
			Tocca le biciclette se c'è un gelato altrimenti tocca tutti gli occhiali da sole
			Tocca le biciclette se c'è un gelato altrimenti tocca le bambine
			Tocca le biciclette se c'è un gelato altrimenti tocca l'ombrello
			Tocca le biciclette se c'è un paio di occhiali da sole altrimenti tocca i leoni
			Tocca le biciclette se c'è un paio di occhiali da sole altrimenti tocca le scimmie
			Tocca le biciclette se c'è un paio di occhiali da sole altrimenti tocca i pappagalli
			Tocca le biciclette se c'è un paio di occhiali da sole altrimenti tocca i polipi
			Tocca le biciclette se c'è un paio di occhiali da sole altrimenti tocca i gelati
			Tocca le biciclette se c'è un paio di occhiali da sole altrimenti tocca le bambine
			Tocca le biciclette se c'è un paio di occhiali da sole altrimenti tocca i monopattini
			Tocca le biciclette se c'è un paio di occhiali da sole altrimenti tocca l occhiali da sole
			Tocca le biciclette se c'è un monopattino altrimenti tocca i leoni
			Tocca le biciclette se c'è un monopattino altrimenti tocca le scimmie
			Tocca le biciclette se c'è un monopattino altrimenti tocca i pappagalli
			Tocca le biciclette se c'è un monopattino altrimenti tocca i polipi
			Tocca le biciclette se c'è un monopattino altrimenti tocca i gelati
			Tocca le biciclette se c'è un monopattino altrimenti tocca le bambine
			Tocca le biciclette se c'è un monopattino altrimenti tocca tutti gli occhiali da sole
			Tocca le biciclette se c'è un monopattino altrimenti tocca l'ombrello
			Tocca le biciclette se c'è un ombrello altrimenti tocca i leoni
			Tocca le biciclette se c'è un ombrello altrimenti tocca le scimmie
			Tocca le biciclette se c'è un ombrello altrimenti tocca i pappagalli
			Tocca le biciclette se c'è un ombrello altrimenti tocca i polipi
			Tocca le biciclette se c'è un ombrello altrimenti tocca i gelati
			Tocca le biciclette se c'è un ombrello altrimenti tocca le bambine
			Tocca le biciclette se c'è un ombrello altrimenti tocca i monopattini
			Tocca le biciclette se c'è un ombrello altrimenti tocca tutti gli occhiali da sole
			Tocca le biciclette se c'è una bambina altrimenti tocca i leoni
			Tocca le biciclette se c'è una bambina altrimenti tocca le scimmie
			Tocca le biciclette se c'è una bambina altrimenti tocca i pappagalli
			Tocca le biciclette se c'è una bambina altrimenti tocca i polipi
			Tocca le biciclette se c'è una bambina altrimenti tocca i gelati
			Tocca le biciclette se c'è una bambina altrimenti tocca i monopattini
			Tocca le biciclette se c'è una bambina altrimenti tocca tutti gli occhiali da sole
			Tocca le biciclette se c'è una bambina altrimenti tocca l'ombrello
			Tocca i monopattini se c'è un leone altrimenti tocca le scimmie
			Tocca i monopattini se c'è un leone altrimenti tocca i polipi
			Tocca i monopattini se c'è un leone altrimenti tocca i pappagalli
			Tocca i monopattini se c'è un leone altrimenti tocca i gelati
			Tocca i monopattini se c'è un leone altrimenti tocca le bambine
			Tocca i monopattini se c'è un leone altrimenti tocca le biciclette
			Tocca i monopattini se c'è un leone altrimenti tocca tutti gli occhiali da sole
			Tocca i monopattini se c'è un leone altrimenti tocca l'ombrello
			Tocca i monopattini se c'è una scimmia altrimenti tocca i leoni
			Tocca i monopattini se c'è una scimmia altrimenti tocca i polipi
			Tocca i monopattini se c'è una scimmia altrimenti tocca i gelati
			Tocca i monopattini se c'è una scimmia altrimenti tocca le bambine
			Tocca i monopattini se c'è una scimmia altrimenti tocca le biciclette
			Tocca i monopattini se c'è una scimmia altrimenti tocca i pappagalli
			Tocca i monopattini se c'è una scimmia altrimenti tocca tutti gli occhiali da sole
			Tocca i monopattini se c'è una scimmia altrimenti tocca l'ombrello
			Tocca i monopattini se c'è un pappagallo altrimenti tocca i leoni
			Tocca i monopattini se c'è un pappagallo altrimenti tocca le scimmie
			Tocca i monopattini se c'è un pappagallo altrimenti tocca i polipi
			Tocca i monopattini se c'è un pappagallo altrimenti tocca i gelati
			Tocca i monopattini se c'è un pappagallo altrimenti tocca le biciclette
			Tocca i monopattini se c'è un pappagallo altrimenti tocca tutti gli occhiali da sole
			Tocca i monopattini se c'è un pappagallo altrimenti tocca le bambine
			Tocca i monopattini se c'è un pappagallo altrimenti tocca l'ombrello
			Tocca i monopattini se c'è un polipo altrimenti tocca i leoni
			Tocca i monopattini se c'è un polipo altrimenti tocca le scimmie
			Tocca i monopattini se c'è un polipo altrimenti tocca i gelati
			Tocca i monopattini se c'è un polipo altrimenti tocca le bambine
			Tocca i monopattini se c'è un polipo altrimenti tocca le biciclette
			Tocca i monopattini se c'è un polipo altrimenti tocca i pappagalli
			Tocca i monopattini se c'è un polipo altrimenti tocca tutti gli occhiali da sole
			Tocca i monopattini se c'è un polipo altrimenti tocca l'ombrello
			Tocca i monopattini se c'è un gelato altrimenti tocca i leoni
			Tocca i monopattini se c'è un gelato altrimenti tocca le scimmie
			Tocca i monopattini se c'è un gelato altrimenti tocca i polipi
			Tocca i monopattini se c'è un gelato altrimenti tocca le bambine
			Tocca i monopattini se c'è un gelato altrimenti tocca le biciclette
			Tocca i monopattini se c'è un gelato altrimenti tocca i pappagalli
			Tocca i monopattini se c'è un gelato altrimenti tocca tutti gli occhiali da sole
			Tocca i monopattini se c'è un gelato altrimenti tocca l'ombrello
			Tocca i monopattini se c'è un paio di occhiali da sole altrimenti tocca i leoni
			Tocca i monopattini se c'è un paio di occhiali da sole altrimenti tocca le scimmie
			Tocca i monopattini se c'è un paio di occhiali da sole altrimenti tocca i pappagalli
			Tocca i monopattini se c'è un paio di occhiali da sole altrimenti tocca i polipi
			Tocca i monopattini se c'è un paio di occhiali da sole altrimenti tocca i gelati
			Tocca i monopattini se c'è un paio di occhiali da sole altrimenti tocca le bambine
			Tocca i monopattini se c'è un paio di occhiali da sole altrimenti tocca le biciclette
			Tocca i monopattini se c'è un paio di occhiali da sole altrimenti tocca l'ombrello
			Tocca i monopattini se c'è una bicicletta altrimenti tocca le scimmie
			Tocca i monopattini se c'è una bicicletta altrimenti tocca i polipi
			Tocca i monopattini se c'è una bicicletta altrimenti tocca i gelati
			Tocca i monopattini se c'è una bicicletta altrimenti tocca le bambine
			Tocca i monopattini se c'è una bicicletta altrimenti tocca i leoni
			Tocca i monopattini se c'è una bicicletta altrimenti tocca i pappagalli
			Tocca i monopattini se c'è una bicicletta altrimenti tocca tutti gli occhiali da sole
			Tocca i monopattini se c'è una bicicletta altrimenti tocca l'ombrello
			Tocca i monopattini se c'è un ombrello altrimenti tocca i leoni
			Tocca i monopattini se c'è un ombrello altrimenti tocca le scimmie
			Tocca i monopattini se c'è un ombrello altrimenti tocca i pappagalli
			Tocca i monopattini se c'è un ombrello altrimenti tocca i polipi
			Tocca i monopattini se c'è un ombrello altrimenti tocca i gelati
			Tocca i monopattini se c'è un ombrello altrimenti tocca le bambine
			Tocca i monopattini se c'è un ombrello altrimenti tocca le biciclette
			Tocca i monopattini se c'è un ombrello altrimenti tocca tutti gli occhiali da sole
			Tocca i monopattini se c'è una bambina altrimenti tocca i leoni
			Tocca i monopattini se c'è una bambina altrimenti tocca le scimmie
			Tocca i monopattini se c'è una bambina altrimenti tocca i pappagalli
			Tocca i monopattini se c'è una bambina altrimenti tocca i polipi
			Tocca i monopattini se c'è una bambina altrimenti tocca i gelati
			Tocca i monopattini se c'è una bambina altrimenti tocca le biciclette
			Tocca i monopattini se c'è una bambina altrimenti tocca tutti gli occhiali da sole
			Tocca i monopattini se c'è una bambina altrimenti tocca l'ombrello
			Tocca gli ombrelli se c'è un leone altrimenti tocca le scimmie
			Tocca gli ombrelli se c'è un leone altrimenti tocca i pappagalli
			Tocca gli ombrelli se c'è un leone altrimenti tocca i polipi
			Tocca gli ombrelli se c'è un leone altrimenti tocca i gelati
			Tocca gli ombrelli se c'è un leone altrimenti tocca le bambine
			Tocca gli ombrelli se c'è un leone altrimenti tocca le biciclette
			Tocca gli ombrelli se c'è un leone altrimenti tocca i monopattini
			Tocca gli ombrelli se c'è un leone altrimenti tocca tutti gli occhiali da sole
			Tocca gli ombrelli se c'è una scimmia altrimenti tocca i leoni
			Tocca gli ombrelli se c'è una scimmia altrimenti tocca i polipi
			Tocca gli ombrelli se c'è una scimmia altrimenti tocca i gelati
			Tocca gli ombrelli se c'è una scimmia altrimenti tocca le bambine
			Tocca gli ombrelli se c'è una scimmia altrimenti tocca le biciclette
			Tocca gli ombrelli se c'è una scimmia altrimenti tocca i monopattini
			Tocca gli ombrelli se c'è una scimmia altrimenti tocca tutti gli occhiali da sole
			Tocca gli ombrelli se c'è una scimmia altrimenti tocca i pappagalli
			Tocca gli ombrelli se c'è un pappagallo altrimenti tocca i leoni
			Tocca gli ombrelli se c'è un pappagallo altrimenti tocca le scimmie
			Tocca gli ombrelli se c'è un pappagallo altrimenti tocca i polipi
			Tocca gli ombrelli se c'è un pappagallo altrimenti tocca i gelati
			Tocca gli ombrelli se c'è un pappagallo altrimenti tocca le bambine
			Tocca gli ombrelli se c'è un pappagallo altrimenti tocca le biciclette
			Tocca gli ombrelli se c'è un pappagallo altrimenti tocca i monopattini
			Tocca gli ombrelli se c'è un pappagallo altrimenti tocca tutti gli occhiali da sole
			Tocca gli ombrelli se c'è un polipo altrimenti tocca le scimmie
			Tocca gli ombrelli se c'è un polipo altrimenti tocca i pappagalli
			Tocca gli ombrelli se c'è un polipo altrimenti tocca i gelati
			Tocca gli ombrelli se c'è un polipo altrimenti tocca le bambine
			Tocca gli ombrelli se c'è un polipo altrimenti tocca le biciclette
			Tocca gli ombrelli se c'è un polipo altrimenti tocca i monopattini
			Tocca gli ombrelli se c'è un polipo altrimenti tocca tutti gli occhiali da sole
			Tocca gli ombrelli se c'è un polipo altrimenti tocca i leoni
			Tocca gli ombrelli se c'è un gelato altrimenti tocca i leoni
			Tocca gli ombrelli se c'è un gelato altrimenti tocca le scimmie
			Tocca gli ombrelli se c'è un gelato altrimenti tocca i pappagalli
			Tocca gli ombrelli se c'è un gelato altrimenti tocca i polipi
			Tocca gli ombrelli se c'è un gelato altrimenti tocca le bambine
			Tocca gli ombrelli se c'è un gelato altrimenti tocca le biciclette
			Tocca gli ombrelli se c'è un gelato altrimenti tocca i monopattini
			Tocca gli ombrelli se c'è un gelato altrimenti tocca tutti gli occhiali da sole
			Tocca gli ombrelli se c'è un paio di occhiali da sole altrimenti tocca le scimmie
			Tocca gli ombrelli se c'è un paio di occhiali da sole altrimenti tocca i polipi
			Tocca gli ombrelli se c'è un paio di occhiali da sole altrimenti tocca i gelati
			Tocca gli ombrelli se c'è un paio di occhiali da sole altrimenti tocca le bambine
			Tocca gli ombrelli se c'è un paio di occhiali da sole altrimenti tocca le biciclette
			Tocca gli ombrelli se c'è un paio di occhiali da sole altrimenti tocca i monopattini
			Tocca gli ombrelli se c'è un paio di occhiali da sole altrimenti tocca i leoni
			Tocca gli ombrelli se c'è un paio di occhiali da sole altrimenti tocca i pappagalli
			Tocca gli ombrelli se c'è una bicicletta altrimenti tocca le scimmie
			Tocca gli ombrelli se c'è una bicicletta altrimenti tocca i polipi
			Tocca gli ombrelli se c'è una bicicletta altrimenti tocca i gelati
			Tocca gli ombrelli se c'è una bicicletta altrimenti tocca le bambine
			Tocca gli ombrelli se c'è una bicicletta altrimenti tocca i leoni
			Tocca gli ombrelli se c'è una bicicletta altrimenti tocca i monopattini
			Tocca gli ombrelli se c'è una bicicletta altrimenti tocca tutti gli occhiali da sole
			Tocca gli ombrelli se c'è una bicicletta altrimenti tocca i pappagalli
			Tocca gli ombrelli se c'è un monopattino altrimenti tocca le scimmie
			Tocca gli ombrelli se c'è un monopattino altrimenti tocca i polipi
			Tocca gli ombrelli se c'è un monopattino altrimenti tocca i gelati
			Tocca gli ombrelli se c'è un monopattino altrimenti tocca le bambine
			Tocca gli ombrelli se c'è un monopattino altrimenti tocca le biciclette
			Tocca gli ombrelli se c'è un monopattino altrimenti tocca i leoni
			Tocca gli ombrelli se c'è un monopattino altrimenti tocca tutti gli occhiali da sole
			Tocca gli ombrelli se c'è un monopattino altrimenti tocca i pappagalli
			Tocca gli ombrelli se c'è una bambina altrimenti tocca le scimmie
			Tocca gli ombrelli se c'è una bambina altrimenti tocca i polipi
			Tocca gli ombrelli se c'è una bambina altrimenti tocca i gelati
			Tocca gli ombrelli se c'è una bambina altrimenti tocca i leoni
			Tocca gli ombrelli se c'è una bambina altrimenti tocca le biciclette
			Tocca gli ombrelli se c'è una bambina altrimenti tocca i monopattini
			Tocca gli ombrelli se c'è una bambina altrimenti tocca tutti gli occhiali da sole
			Tocca gli ombrelli se c'è una bambina altrimenti tocca i pappagalli
			Tocca le bambine se c'è un leone altrimenti tocca le scimmie
			Tocca le bambine se c'è un leone altrimenti tocca i pappagalli
			Tocca le bambine se c'è un leone altrimenti tocca i polipi
			Tocca le bambine se c'è un leone altrimenti tocca i gelati
			Tocca le bambine se c'è un leone altrimenti tocca le biciclette
			Tocca le bambine se c'è un leone altrimenti tocca i monopattini
			Tocca le bambine se c'è un leone altrimenti tocca tutti gli occhiali da sole
			Tocca le bambine se c'è un leone altrimenti tocca l'ombrello
			Tocca le bambine se c'è una scimmia altrimenti tocca i pappagalli
			Tocca le bambine se c'è una scimmia altrimenti tocca i polipi
			Tocca le bambine se c'è una scimmia altrimenti tocca i gelati
			Tocca le bambine se c'è una scimmia altrimenti tocca i leoni
			Tocca le bambine se c'è una scimmia altrimenti tocca le biciclette
			Tocca le bambine se c'è una scimmia altrimenti tocca i monopattini
			Tocca le bambine se c'è una scimmia altrimenti tocca tutti gli occhiali da sole
			Tocca le bambine se c'è una scimmia altrimenti tocca l'ombrello
			Tocca le bambine se c'è un pappagallo altrimenti tocca le scimmie
			Tocca le bambine se c'è un pappagallo altrimenti tocca i polipi
			Tocca le bambine se c'è un pappagallo altrimenti tocca i gelati
			Tocca le bambine se c'è un pappagallo altrimenti tocca i leoni
			Tocca le bambine se c'è un pappagallo altrimenti tocca le biciclette
			Tocca le bambine se c'è un pappagallo altrimenti tocca i monopattini
			Tocca le bambine se c'è un pappagallo altrimenti tocca tutti gli occhiali da sole
			Tocca le bambine se c'è un pappagallo altrimenti tocca l'ombrello
			Tocca le bambine se c'è un polipo altrimenti tocca le scimmie
			Tocca le bambine se c'è un polipo altrimenti tocca i pappagalli
			Tocca le bambine se c'è un polipo altrimenti tocca i gelati
			Tocca le bambine se c'è un polipo altrimenti tocca i leoni
			Tocca le bambine se c'è un polipo altrimenti tocca le biciclette
			Tocca le bambine se c'è un polipo altrimenti tocca i monopattini
			Tocca le bambine se c'è un polipo altrimenti tocca tutti gli occhiali da sole
			Tocca le bambine se c'è un polipo altrimenti tocca l'ombrello
			Tocca le bambine se c'è un gelato altrimenti tocca le scimmie
			Tocca le bambine se c'è un gelato altrimenti tocca i polipi
			Tocca le bambine se c'è un gelato altrimenti tocca i pappagalli
			Tocca le bambine se c'è un gelato altrimenti tocca i leoni
			Tocca le bambine se c'è un gelato altrimenti tocca le biciclette
			Tocca le bambine se c'è un gelato altrimenti tocca i monopattini
			Tocca le bambine se c'è un gelato altrimenti tocca tutti gli occhiali da sole
			Tocca le bambine se c'è un gelato altrimenti tocca l'ombrello
			Tocca le bambine se c'è un paio di occhiali da sole altrimenti tocca le scimmie
			Tocca le bambine se c'è un paio di occhiali da sole altrimenti tocca i polipi
			Tocca le bambine se c'è un paio di occhiali da sole altrimenti tocca i gelati
			Tocca le bambine se c'è un paio di occhiali da sole altrimenti tocca i leoni
			Tocca le bambine se c'è un paio di occhiali da sole altrimenti tocca le biciclette
			Tocca le bambine se c'è un paio di occhiali da sole altrimenti tocca i monopattini
			Tocca le bambine se c'è un paio di occhiali da sole altrimenti tocca i pappagalli
			Tocca le bambine se c'è un paio di occhiali da sole altrimenti tocca l'ombrello
			Tocca le bambine se c'è una bicicletta altrimenti tocca le scimmie
			Tocca le bambine se c'è una bicicletta altrimenti tocca i polipi
			Tocca le bambine se c'è una bicicletta altrimenti tocca i gelati
			Tocca le bambine se c'è una bicicletta altrimenti tocca i leoni
			Tocca le bambine se c'è una bicicletta altrimenti tocca le biciclette
			Tocca le bambine se c'è una bicicletta altrimenti tocca i monopattini
			Tocca le bambine se c'è una bicicletta altrimenti tocca tutti gli occhiali da sole
			Tocca le bambine se c'è un monopattino altrimenti tocca le scimmie
			Tocca le bambine se c'è un monopattino altrimenti tocca i polipi
			Tocca le bambine se c'è un monopattino altrimenti tocca i gelati
			Tocca le bambine se c'è un monopattino altrimenti tocca i leoni
			Tocca le bambine se c'è un monopattino altrimenti tocca le biciclette
			Tocca le bambine se c'è un monopattino altrimenti tocca i pappagalli
			Tocca le bambine se c'è un monopattino altrimenti tocca tutti gli occhiali da sole
			Tocca le bambine se c'è un monopattino altrimenti tocca l'ombrello
			Tocca le bambine se c'è un ombrello altrimenti tocca le scimmie
			Tocca le bambine se c'è un ombrello altrimenti tocca i polipi
			Tocca le bambine se c'è un ombrello altrimenti tocca i gelati
			Tocca le bambine se c'è un ombrello altrimenti tocca i leoni
			Tocca le bambine se c'è un ombrello altrimenti tocca le biciclette
			Tocca le bambine se c'è un ombrello altrimenti tocca i monopattini
			Tocca le bambine se c'è un ombrello altrimenti tocca tutti gli occhiali da sole
			Tocca le bambine se c'è un ombrello altrimenti tocca i pappagalli
	######################################
	#9C count
		9CNSU = 
				Quanti leoni ci sono? Toccami la pancia 1 volta se c è un leone
				Quanti leoni ci sono? Toccami la pancia 2 volte se ci sono due leoni
				Quanti leoni ci sono? Toccami la pancia 3 volte se ci sono tre leoni
				Quanti leoni ci sono? Toccami la pancia 4 volte se ci sono quattro leoni
				Quanti scimmie ci sono? Toccami la pancia 1 volta se c è un scimmia
				Quanti scimmie ci sono? Toccami la pancia 2 volte se ci sono due scimmie
				Quanti scimmie ci sono? Toccami la pancia 3 volte se ci sono tre scimmie
				Quanti scimmie ci sono? Toccami la pancia 4 volte se ci sono quattro scimmie
				Quanti pappagalli ci sono? Toccami la pancia 1 volta se c è un pappagallo
				Quanti pappagalli ci sono? Toccami la pancia 2 volte se ci sono due pappagalli
				Quanti pappagalli ci sono? Toccami la pancia 3 volte se ci sono tre pappagalli
				Quanti pappagalli ci sono? Toccami la pancia 4 volte se ci sono quattro pappagalli
				Quanti polipi ci sono? Toccami la pancia 1 volta se c è un polipo
				Quanti polipi ci sono? Toccami la pancia 2 volte se ci sono due polipi
				Quanti polipi ci sono? Toccami la pancia 3 volte se ci sono tre polipi
				Quanti polipi ci sono? Toccami la pancia 4 volte se ci sono quattro polipi
				Quanti galati ci sono? Toccami la pancia 1 volta se c è un galato
				Quanti galati ci sono? Toccami la pancia 2 volte se ci sono due galati
				Quanti galati ci sono? Toccami la pancia 3 volte se ci sono tre galati
				Quanti galati ci sono? Toccami la pancia 4 volte se ci sono quattro galati
				Quanti occhiali ci sono? Toccami la pancia 1 volta se c è un paio di occhiali
				Quanti occhiali ci sono? Toccami la pancia 2 volte se ci sono due occhiali
				Quanti occhiali ci sono? Toccami la pancia 3 volte se ci sono tre occhiali
				Quanti occhiali ci sono? Toccami la pancia 4 volte se ci sono quattro occhiali
				Quanti biciclette ci sono? Toccami la pancia 1 volta se c è una bicicletta
				Quanti biciclette ci sono? Toccami la pancia 2 volte se ci sono due biciclette
				Quanti biciclette ci sono? Toccami la pancia 3 volte se ci sono tre biciclette
				Quanti biciclette ci sono? Toccami la pancia 4 volte se ci sono quattro biciclette
				Quanti monopattini ci sono? Toccami la pancia 1 volta se c è un monopattino
				Quanti monopattini ci sono? Toccami la pancia 2 volte se ci sono due monopattini
				Quanti monopattini ci sono? Toccami la pancia 3 volte se ci sono tre monopattini
				Quanti monopattini ci sono? Toccami la pancia 4 volte se ci sono quattro monopattini
				Quanti ombrelli ci sono? Toccami la pancia 1 volta se c è un ombrello
				Quanti ombrelli ci sono? Toccami la pancia 2 volte se ci sono due ombrelli
				Quanti ombrelli ci sono? Toccami la pancia 3 volte se ci sono tre ombrelli
				Quanti ombrelli ci sono? Toccami la pancia 4 volte se ci sono quattro ombrelli
				Quante bambine ci sono? Toccami la pancia 1 volta se c è una bambina
				Quante bambine ci sono? Toccami la pancia 2 volte se ci sono due bambine
				Quante bambine ci sono? Toccami la pancia 3 volte se ci sono tre bambine
				Quante bambine ci sono? Toccami la pancia 4 volte se ci sono quattro bambine
		9CNCO = 
				Quanti disegni rossi ci sono? Toccami la pancia 2 volte se ci sono due rossi
				Quanti disegni rossi ci sono? Toccami la pancia 3 volte se ci sono tre rossi
				Quanti disegni rossi ci sono? Toccami la pancia 4 volte se ci sono quattro rossi
				Quanti disegni verdi ci sono? Toccami la pancia 2 volte se ci sono due verdi
				Quanti disegni verdi ci sono? Toccami la pancia 3 volte se ci sono tre verdi
				Quanti disegni verdi ci sono? Toccami la pancia 4 volte se ci sono quattro verdi
				Quanti disegni gialli ci sono? Toccami la pancia 2 volte se ci sono due gialli
				Quanti disegni gialli ci sono? Toccami la pancia 3 volte se ci sono tre gialli
				Quanti disegni gialli ci sono? Toccami la pancia 4 volte se ci sono quattro gialli
				Quanti disegni blu ci sono? Toccami la pancia 2 volte se ci sono due blu
				Quanti disegni blu ci sono? Toccami la pancia 3 volte se ci sono tre blu
				Quanti disegni blu ci sono? Toccami la pancia 4 volte se ci sono quattro blu
		9CNSSU =
				Tocca due volte il bottone tutto rosso
				Tocca tre volte il bottone tutto rosso
				Tocca quattro volte il bottone tutto rosso
				Tocca due volte il bottone tutto verde
				Tocca tre volte il bottone tutto verde
				Tocca quattro volte il bottone tutto verde
				Tocca due volte il bottone tutto giallo
				Tocca tre volte il bottone tutto giallo
				Tocca quattro volte il bottone tutto giallo
				Tocca due volte il bottone tutto blu
				Tocca tre volte il bottone tutto blu 
				Tocca quattro volte il bottone tutto blu
				Tocca due volte un leone 
				Tocca tre volte un leone
				Tocca quattro volte un leone
				Tocca due volte una scimmia
				Tocca tre volte una scimmia 
				Tocca quattro volte una scimmia 
				Tocca due volte il pappagallo
				Tocca tre volte un pappagallo 
				Tocca quattro volte un pappagallo 
				Tocca due volte il polipo
				Tocca tre volte un polipo
				Tocca quattro volte un polipo
				Tocca due volte il gelato
				Tocca tre volte un gelato
				Tocca quattro volte un gelato
				Tocca due volte gli occhiali da sole
				Tocca tre volte gli occhiali da sole
				Tocca quattro volte gli occhiali da sole
				Tocca due volte una bicicletta
				Tocca tre volte una bicicletta	
				Tocca quattro volte una bicicletta
				Tocca due volte il monopattino
				Tocca tre volte un monopattino
				Tocca quattro volte un monopattino
				Tocca due volte un ombrello
				Tocca tre volte un ombrello
				Tocca quattro volte un ombrello
				Tocca due volte la bambina
				Tocca tre volte una bambina
				Tocca quattro volte una bambina
		9CNSPA = 
				Tocca due volte il leone rosso
				Tocca due volte il leone verde
				Tocca due volte il leone giallo
				Tocca due volte il leone blu
				Tocca tre volte il leone rosso
				Tocca tre volte il leone verde
				Tocca tre volte il leone giallo
				Tocca tre volte il leone blu
				Tocca quattro volte il leone rosso
				Tocca quattro volte il leone verde
				Tocca quattro volte il leone giallo
				Tocca quattro volte il leone blu
				Tocca due volte la scimmia rossa
				Tocca due volte la scimmia verde
				Tocca due volte la scimmia gialla
				Tocca due volte la scimmia blu
				Tocca tre volte la scimmia rossa
				Tocca tre volte la scimmia verde
				Tocca tre volte la scimmia gialla
				Tocca tre volte la scimmia blu
				Tocca quattro volte la scimmia rossa
				Tocca quattro volte la scimmia verde
				Tocca quattro volte la scimmia gialla
				Tocca quattro volte la scimmia blu
				Tocca due volte il pappagallo rosso
				Tocca due volte il pappagallo verde
				Tocca due volte il pappagallo giallo
				Tocca due volte il pappagallo blu
				Tocca tre volte il pappagallo rosso
				Tocca tre volte il pappagallo verde
				Tocca tre volte il pappagallo giallo
				Tocca tre volte il pappagallo blu
				Tocca quattro volte il pappagallo rosso
				Tocca quattro volte il pappagallo verde
				Tocca quattro volte il pappagallo giallo
				Tocca quattro volte il pappagallo blu
				Tocca due volte il polipo rosso
				Tocca due volte il polipo verde
				Tocca due volte il polipo giallo
				Tocca due volte il polipo blu
				Tocca tre volte il polipo rosso
				Tocca tre volte il polipo verde
				Tocca tre volte il polipo giallo
				Tocca tre volte il polipo blu
				Tocca quattro volte il polipo rosso
				Tocca quattro volte il polipo verde
				Tocca quattro volte il polipo giallo
				Tocca quattro volte il polipo blu
				Tocca due volte il gelato rosso
				Tocca due volte il gelato verde
				Tocca due volte il gelato giallo
				Tocca due volte il gelato blu
				Tocca tre volte il gelato rosso
				Tocca tre volte il gelato verde
				Tocca tre volte il gelato giallo
				Tocca tre volte il gelato blu
				Tocca quattro volte il gelato rosso
				Tocca quattro volte il gelato verde
				Tocca quattro volte il gelato giallo
				Tocca quattro volte il gelato blu
				Tocca due volte gli occhiali da sole rossi
				Tocca due volte gli occhiali da sole verdi
				Tocca due volte gli occhiali da sole gialli
				Tocca due volte gli occhiali da sole blu
				Tocca tre volte gli occhiali da sole rossi
				Tocca tre volte gli occhiali da sole verdi
				Tocca tre volte gli occhiali da sole gialli
				Tocca tre volte gli occhiali da sole blu
				Tocca quattro volte gli occhiali da sole rossi
				Tocca quattro volte gli occhiali da sole verdi
				Tocca quattro volte gli occhiali da sole gialli
				Tocca quattro volte gli occhiali da sole blu
				Tocca due volte la bicicletta rossa
				Tocca due volte la bicicletta verde
				Tocca due volte la bicicletta gialla
				Tocca due volte la bicicletta blu
				Tocca tre volte la bicicletta rossa
				Tocca tre volte la bicicletta verde
				Tocca tre volte la bicicletta gialla
				Tocca tre volte la bicicletta blu
				Tocca quattro volte la bicicletta rossa
				Tocca quattro volte la bicicletta verde
				Tocca quattro volte la bicicletta gialla
				Tocca quattro volte la bicicletta blu
				Tocca due volte il monopattino rosso
				Tocca due volte il monopattino verde
				Tocca due volte il monopattino giallo
				Tocca due volte il monopattino blu
				Tocca tre volte il monopattino rosso
				Tocca tre volte il monopattino verde
				Tocca tre volte il monopattino giallo
				Tocca tre volte il monopattino blu
				Tocca quattro volte il monopattino rosso
				Tocca quattro volte il monopattino verde
				Tocca quattro volte il monopattino giallo
				Tocca quattro volte il monopattino blu
				Tocca due volte l'ombrello rosso
				Tocca due volte l'ombrello verde
				Tocca due volte l'ombrello giallo
				Tocca due volte l'ombrello blu
				Tocca tre volte l'ombrello rosso
				Tocca tre volte l'ombrello verde
				Tocca tre volte l'ombrello giallo
				Tocca tre volte l'ombrello blu
				Tocca quattro volte l'ombrello rosso
				Tocca quattro volte l'ombrello verde
				Tocca quattro volte l'ombrello giallo
				Tocca quattro volte l'ombrello blu
				Tocca due volte la bambina vestita in rosso
				Tocca due volte la bambina vestita in verde
				Tocca due volte la bambina vestita in giallo
				Tocca due volte la bambina vestita in blu
				Tocca tre volte la bambina vestita in rosso
				Tocca tre volte la bambina vestita in verde
				Tocca tre volte la bambina vestita in giallo
				Tocca tre volte la bambina vestita in blu
				Tocca quattro volte la bambina vestita in rosso
				Tocca quattro volte la bambina vestita in verde
				Tocca quattro volte la bambina vestita in giallo
				Tocca quattro volte la bambina vestita in blu
		9CNSPX = 
				Tocca una volta il disegno in alto a sinistra
				Tocca una volta il disegno in basso a sinistra
				Tocca una volta il disegno in alto a destra
				Tocca una volta il disegno in basso a destra
				Tocca due volte il disegno in alto a sinistra
				Tocca due volte il disegno in basso a sinistra
				Tocca due volte il disegno in alto a destra
				Tocca due volte il disegno in basso a destra
				Tocca tre volte il disegno in alto a sinistra
				Tocca tre volte il disegno in basso a sinistra
				Tocca tre volte il disegno in alto a destra
				Tocca tre volte il disegno in basso a destra
				Tocca quatro volte il disegno in alto a sinistra
				Tocca quatro volte il disegno in basso a sinistra
				Tocca quatro volte il disegno in alto a destra
				Tocca quatro volte il disegno in basso a destra
	######################################

Created by Colombo Giacomo Airlab Polimi 2020
"""
