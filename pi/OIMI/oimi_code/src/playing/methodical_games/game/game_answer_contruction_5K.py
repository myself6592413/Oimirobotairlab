############## KIND 5K
import faulthandler; faulthandler.enable()
def build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,same_col,same_ele):
	print("WE ARE IN KIND 5K")
	ali,ali_tmp,ali_1,tot,tot_1 = [],[],[],[],[]
	all_alternative_pos=[0,0,0,0]
	one_ans_is_enough=[0,0,0,0]
	multiple_branch_ans=[0,0,0,0]
	multiple_or_ans=[0,0,0,0]
	touch_together=[0,0,0,0]
	no_ans_to_create_only_body=[0,0,0,0]
	body_response=[0,0,0,0]
	body_and_patch=[0,0,0,0]
	need_light=[0,0,0,0]
	need_light1=[0,0,0,0]
	need_light2=[0,0,0,0]
	need_light3=[0,0,0,0]
	the_first=[0,0,0,0]
	the_second=[0,0,0,0]
	body_num_press=[0,0,0,0]
	all_num_touches_to_do=[0,0,0,0]
	ques_9_num = [0,0,0,0]
	limit = [0,0,0,0]
	limit1 = [0,0,0,0]
	limit2 = [0,0,0,0]
	limit3 = [0,0,0,0]
	limitRed = [0,0,0,0]
	limitGreen = [0,0,0,0]
	limitBlue = [0,0,0,0]
	limitYellow  = [0,0,0,0]
	limitLion = [0,0,0,0]
	limitMonkey = [0,0,0,0]
	limitParrot = [0,0,0,0]
	limitOctopus = [0,0,0,0]
	for k,kaudio in enumerate(listaudios):
		###5KD
		if kaudio=='audio_3048':
			#knowledge doppio nuotare e correre
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['lion','monkey','girl']]
		elif kaudio=='audio_3049':
			#correre e parlare
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
		elif kaudio=='audio_3050':
			#chi sa camminare su due zampe e non può volare
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
		###5KS
		elif kaudio=='audio_3021':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
		elif kaudio=='audio_3022':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
		elif kaudio=='audio_3023':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['bicycle', 'push_scooter']]
		elif kaudio=='audio_3024':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
		elif kaudio=='audio_3025':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
		elif kaudio=='audio_3026':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
		elif kaudio=='audio_3027':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
		elif kaudio=='audio_3028':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['monkey','parrot','octopus','lion']]
		elif kaudio=='audio_3029':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
		elif kaudio=='audio_3030':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['monkey','girl','octopus','lion']]
		elif kaudio=='audio_3031':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['bicycle', 'push_scooter']]
		elif kaudio=='audio_3032':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['monkey','girl','octopus','lion']]
		elif kaudio=='audio_3033':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['octopus','girl']]
		elif kaudio=='audio_3034':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['monkey','parrot']]
		elif kaudio=='audio_3035':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['monkey','parrot']]
		elif kaudio=='audio_3036':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
		elif kaudio=='audio_3037':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
		elif kaudio=='audio_3038':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['parrot','girl']]
		elif kaudio=='audio_3039':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['lion','monkey','octopus']]
		elif kaudio=='audio_3040':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['glasses','umbrella']]
		elif kaudio=='audio_3041':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['glasses','bicycle','push_scooter','umbrella']]
		elif kaudio=='audio_3042':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['monkey','girl','lion']]
		elif kaudio=='audio_3043':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['parrot','monkey']]
		elif kaudio=='audio_3044':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element in ['parrot','girl']]
		elif kaudio=='audio_3045': #il più veloce
			if same_ele['lion']>0:
				alil = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
			elif same_ele['monkey']>0:
				alim = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
			elif same_ele['push_scooter']>0:
				alip = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
			elif same_ele['bicycle']>0:
				alib = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
			elif same_ele['girl']>0:
				alig = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
			ali = alil + alim + alip + alib + alig
			one_ans_is_enough[k] = 1

		if len(ali_1) or len(tot_1):
			tot.append(ali)
			if all_alternative_pos[k] in (0,1):
				tot_1.append(ali) #not ali_1
			else:
				tot_1.append(ali_1)
		else:
			print("partial_ali {}".format(ali))
			tot.append(ali)
		ali=[]
		ali_1=[]
	print("tot --> {}".format(tot))
	print("tot_1 --> {}".format(tot_1))
	print("all_alternative_pos={}".format(all_alternative_pos))

	all_properties = []
	all_properties.append(tot)
	all_properties.append(tot_1)
	all_properties.append(all_alternative_pos)
	all_properties.append(one_ans_is_enough)
	all_properties.append(multiple_branch_ans)
	all_properties.append(multiple_or_ans)
	all_properties.append(touch_together)
	all_properties.append(no_ans_to_create_only_body)
	all_properties.append(body_response)
	all_properties.append(body_and_patch)
	all_properties.append(need_light)
	all_properties.append(need_light1)
	all_properties.append(need_light2)
	all_properties.append(need_light3)
	all_properties.append(the_first)
	all_properties.append(the_second)
	all_properties.append(body_num_press)
	all_properties.append(all_num_touches_to_do)
	all_properties.append(ques_9_num)
	all_properties.append(limit)
	all_properties.append(limit1)
	all_properties.append(limit2)
	all_properties.append(limit3)
	all_properties.append(limitRed)
	all_properties.append(limitGreen)
	all_properties.append(limitBlue)
	all_properties.append(limitYellow)
	all_properties.append(limitLion)
	all_properties.append(limitMonkey)
	all_properties.append(limitParrot)
	all_properties.append(limitOctopus)
	return all_properties
