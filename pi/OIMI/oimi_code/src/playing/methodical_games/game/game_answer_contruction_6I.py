from collections import Counter

def build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,same_col,same_ele):
	print("WE ARE IN KIND 6I")
	found_for_color = 0
	ali,ali_tmp,ali_1,tot,tot_1 = [],[],[],[],[]
	all_alternative_pos=[0,0,0,0]
	one_ans_is_enough=[0,0,0,0]
	multiple_branch_ans=[0,0,0,0]
	multiple_or_ans=[0,0,0,0]
	touch_together=[0,0,0,0]
	no_ans_to_create_only_body=[0,0,0,0]
	body_response=[0,0,0,0]
	body_and_patch=[0,0,0,0]
	need_light=[0,0,0,0]
	need_light1=[0,0,0,0]
	need_light2=[0,0,0,0]
	need_light3=[0,0,0,0]
	the_first=[0,0,0,0]
	the_second=[0,0,0,0]
	body_num_press=[0,0,0,0]
	all_num_touches_to_do=[0,0,0,0]
	ques_9_num = [0,0,0,0]
	limit = [0,0,0,0]
	limit1 = [0,0,0,0]
	limit2 = [0,0,0,0]
	limit3 = [0,0,0,0]
	limitRed = [0,0,0,0]
	limitGreen = [0,0,0,0]
	limitBlue = [0,0,0,0]
	limitYellow  = [0,0,0,0]
	limitLion = [0,0,0,0]
	limitMonkey = [0,0,0,0]
	limitParrot = [0,0,0,0]
	limitOctopus = [0,0,0,0]
	for kaudio in listaudios:
		if kaudio=='audio_3051':
			###############################################################################
			if same_col['Red']==3 and same_col['Green']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
				found_for_color=1
			if same_col['Red']==3 and same_col['Yellow']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
				found_for_color=1
			if same_col['Red']==3 and same_col['Blue']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
				found_for_color=1
			if same_col['Green']>3 and same_col['Red']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
				found_for_color=1
			if same_col['Green']>3 and same_col['Yellow']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
				found_for_color=1
			if same_col['Green']>3 and same_col['Blue']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
				found_for_color=1
			if same_col['Yellow']==3 and same_col['Red']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
				found_for_color=1
			if same_col['Yellow']==3 and same_col['Green']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
				found_for_color=1
			if same_col['Yellow']==3 and same_col['Blue']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Blue']
				found_for_color=1
			if same_col['Blue']==3 and same_col['Red']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Red']
				found_for_color=1
			if same_col['Blue']==3 and same_col['Yellow']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Yellow']
				found_for_color=1
			if same_col['Blue']==3 and same_col['Green']==1:
				ali = [i+1 for i,colo in enumerate(patpat_col) if colo=='Green']
				found_for_color=1
			###############################################################################
			if same_ele['lion']==1 and same_ele['monkey']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			if same_ele['lion']==1 and same_ele['parrot']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			if same_ele['lion']==1 and same_ele['octopus']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			if same_ele['monkey']==1 and same_ele['lion']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			if same_ele['monkey']==1 and same_ele['parrot']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			if same_ele['monkey']==1 and same_ele['octopus']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			if same_ele['parrot']==1 and same_ele['monkey']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			if same_ele['parrot']==1 and same_ele['lion']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			if same_ele['parrot']==1 and same_ele['octopus']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			if same_ele['octopus']==1 and same_ele['lion']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			if same_ele['octopus']==1 and same_ele['monkey']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			if same_ele['octopus']==1 and same_ele['parrot']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			if same_ele['icecream']==1 and same_ele['lion']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['monkey']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['parrot']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['octopus']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['glasses']==1 and same_ele['lion']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['monkey']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['parrot']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['octopus']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['bicycle']==1 and same_ele['lion']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['monkey']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['parrot']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['octopus']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['push_scooter']==1 and same_ele['lion']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['monkey']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['parrot']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['octopus']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['umbrella']==1 and same_ele['lion']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['monkey']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['parrot']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['octopus']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['girl']==1 and same_ele['lion']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['monkey']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['parrot']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['octopus']==3:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			###############################################################################
			#glasses considered as dress!! weird but ok !! no changed!!!
			#if same_ele['lion']==1 and same_ele['glasses']>=1 and same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			#if same_ele['lion']==1 and same_ele['glasses']>=1 and same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			#if same_ele['lion']==1 and same_ele['glasses']>=1 and same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			#if same_ele['monkey']==1 and same_ele['glasses']>=1 and same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			#if same_ele['monkey']==1 and same_ele['glasses']>=1 and same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			#if same_ele['monkey']==1 and same_ele['glasses']>=1 and same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			#if same_ele['parrot']==1 and same_ele['glasses']>=1 and same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			#if same_ele['parrot']==1 and same_ele['glasses']>=1 and same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			#if same_ele['parrot']==1 and same_ele['glasses']>=1 and same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			#if same_ele['octopus']==1 and same_ele['glasses']>=1 and same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			#if same_ele['octopus']==1 and same_ele['glasses']>=1 and same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			#if same_ele['octopus']==1 and same_ele['glasses']>=1 and same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			#if same_ele['girl']==1 and same_ele['glasses']>=1 and same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			#if same_ele['girl']==1 and same_ele['glasses']>=1 and same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			#if same_ele['girl']==1 and same_ele['glasses']>=1 and same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['lion']==1 and same_ele['umbrella']>=1 and same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			if same_ele['monkey']==1 and same_ele['umbrella']>=1 and same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			if same_ele['parrot']==1 and same_ele['umbrella']>=1 and same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			if same_ele['octopus']==1 and same_ele['umbrella']>=1 and same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			#################################################################################################################
			if same_ele['girl']==1 and same_ele['umbrella']>=1 and same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['glasses']==1 and same_ele['monkey']>=1 and same_ele['octopus']>=1 and same_ele['parrot']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['monkey']>=1 and same_ele['octopus']>=1 and same_ele['lion']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['monkey']>=1 and same_ele['parrot']>=1 and same_ele['lion']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['parrot']>=1 and same_ele['lion']>=1 and same_ele['octopus']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['bicycle']==1 and same_ele['monkey']>=1 and same_ele['octopus']>=1 and same_ele['parrot']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['monkey']>=1 and same_ele['octopus']>=1 and same_ele['lion']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['monkey']>=1 and same_ele['parrot']>=1 and same_ele['lion']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['parrot']>=1 and same_ele['lion']>=1 and same_ele['octopus']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['push_scooter']==1 and same_ele['monkey']>=1 and same_ele['octopus']>=1 and same_ele['parrot']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['monkey']>=1 and same_ele['octopus']>=1 and same_ele['lion']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['monkey']>=1 and same_ele['parrot']>=1 and same_ele['lion']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['parrot']>=1 and same_ele['lion']>=1 and same_ele['octopus']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['umbrella']==1 and same_ele['monkey']>=1 and same_ele['octopus']>=1 and same_ele['parrot']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['monkey']>=1 and same_ele['octopus']>=1 and same_ele['lion']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['monkey']>=1 and same_ele['parrot']>=1 and same_ele['lion']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['parrot']>=1 and same_ele['lion']>=1 and same_ele['octopus']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['girl']==1 and same_ele['monkey']>=1 and same_ele['octopus']>=1 and same_ele['parrot']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['monkey']>=1 and same_ele['octopus']>=1 and same_ele['lion']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['monkey']>=1 and same_ele['parrot']>=1 and same_ele['lion']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['parrot']>=1 and same_ele['lion']>=1 and same_ele['octopus']>=1:
				if found_for_color:
					ali_1 = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
				else:
					ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			############################################################################
			if same_ele['glasses']==1 and same_ele['lion']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['lion']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['lion']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['monkey']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['monkey']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['monkey']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['parrot']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['parrot']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['parrot']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['octopus']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['octopus']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['glasses']==1 and same_ele['octopus']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='glasses']
			if same_ele['girl']==1 and same_ele['lion']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['lion']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['lion']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['monkey']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['monkey']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['monkey']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['parrot']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['parrot']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['parrot']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['octopus']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['octopus']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['girl']==1 and same_ele['octopus']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='girl']
			if same_ele['umbrella']==1 and same_ele['lion']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['lion']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['lion']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['monkey']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['monkey']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['monkey']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['parrot']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['parrot']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['parrot']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['octopus']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['octopus']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['umbrella']==1 and same_ele['octopus']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='umbrella']
			if same_ele['push_scooter']==1 and same_ele['lion']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['lion']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['lion']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['monkey']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['monkey']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['monkey']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['parrot']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['parrot']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['parrot']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['octopus']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['octopus']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['push_scooter']==1 and same_ele['octopus']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='push_scooter']
			if same_ele['bicycle']==1 and same_ele['lion']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['lion']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['lion']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['monkey']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['monkey']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['monkey']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['parrot']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['parrot']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['parrot']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['octopus']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['octopus']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['bicycle']==1 and same_ele['octopus']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='bicycle']
			if same_ele['icecream']==1 and same_ele['lion']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['lion']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['lion']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['monkey']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['monkey']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['monkey']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['parrot']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['parrot']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['parrot']>=2 and same_ele['octopus']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['octopus']>=2 and same_ele['lion']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['octopus']>=2 and same_ele['monkey']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			if same_ele['icecream']==1 and same_ele['octopus']>=2 and same_ele['parrot']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='icecream']
			############################################################################
			if same_ele['lion']==1 and same_ele['bybicle']>=2 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			#if same_ele['lion']==1 and same_ele['bybicle']>=2 and same_ele['glasses']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			if same_ele['lion']==1 and same_ele['bybicle']>=2 and same_ele['umbrella']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			if same_ele['lion']==1 and same_ele['push_scooter']>=2 and same_ele['bybicle']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			#if same_ele['lion']==1 and same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			if same_ele['lion']==1 and same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			#if same_ele['lion']==1 and same_ele['glasses']>=2 and same_ele['bybicle']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			#if same_ele['lion']==1 and same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			#if same_ele['lion']==1 and same_ele['glasses']>=2 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			if same_ele['lion']==1 and same_ele['umbrella']>=2 and same_ele['bybicle']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			if same_ele['lion']==1 and same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			#if same_ele['lion']==1 and same_ele['umbrella']>=2 and same_ele['glasses']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='lion']
			if same_ele['monkey']==1 and same_ele['bybicle']>=2 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			#if same_ele['monkey']==1 and same_ele['bybicle']>=2 and same_ele['glasses']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			if same_ele['monkey']==1 and same_ele['bybicle']>=2 and same_ele['umbrella']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			if same_ele['monkey']==1 and same_ele['push_scooter']>=2 and same_ele['bybicle']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			#if same_ele['monkey']==1 and same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			if same_ele['monkey']==1 and same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			#if same_ele['monkey']==1 and same_ele['glasses']>=2 and same_ele['bybicle']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			#if same_ele['monkey']==1 and same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			#if same_ele['monkey']==1 and same_ele['glasses']>=2 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			if same_ele['monkey']==1 and same_ele['umbrella']>=2 and same_ele['bybicle']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			if same_ele['monkey']==1 and same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			#if same_ele['monkey']==1 and same_ele['umbrella']>=2 and same_ele['glasses']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='monkey']
			if same_ele['parrot']==1 and same_ele['bybicle']>=2 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			#if same_ele['parrot']==1 and same_ele['bybicle']>=2 and same_ele['glasses']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			if same_ele['parrot']==1 and same_ele['bybicle']>=2 and same_ele['umbrella']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			if same_ele['parrot']==1 and same_ele['push_scooter']>=2 and same_ele['bybicle']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			#if same_ele['parrot']==1 and same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			if same_ele['parrot']==1 and same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			#if same_ele['parrot']==1 and same_ele['glasses']>=2 and same_ele['bybicle']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			#if same_ele['parrot']==1 and same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			#if same_ele['parrot']==1 and same_ele['glasses']>=2 and same_ele['umbrella']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			if same_ele['parrot']==1 and same_ele['umbrella']>=2 and same_ele['bybicle']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			if same_ele['parrot']==1 and same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			#if same_ele['parrot']==1 and same_ele['umbrella']>=2 and same_ele['glasses']>=1:
			#		ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='parrot']
			if same_ele['ocotpus']==1 and same_ele['bybicle']>=2 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			#if same_ele['ocotpus']==1 and same_ele['bybicle']>=2 and same_ele['glasses']>=1:
			#		if patpat_ele[q]=='ocotpus':
			if same_ele['ocotpus']==1 and same_ele['bybicle']>=2 and same_ele['umbrella']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			if same_ele['ocotpus']==1 and same_ele['push_scooter']>=2 and same_ele['bybicle']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			#if same_ele['ocotpus']==1 and same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
			#		if patpat_ele[q]=='ocotpus':
			if same_ele['ocotpus']==1 and same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			#if same_ele['ocotpus']==1 and same_ele['glasses']>=2 and same_ele['bybicle']>=1:
			#		if patpat_ele[q]=='ocotpus':
			#if same_ele['ocotpus']==1 and same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
			#		if patpat_ele[q]=='ocotpus':
			#if same_ele['ocotpus']==1 and same_ele['glasses']>=2 and same_ele['umbrella']>=1:
			#		if patpat_ele[q]=='ocotpus':
			if same_ele['ocotpus']==1 and same_ele['umbrella']>=2 and same_ele['bybicle']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			if same_ele['ocotpus']==1 and same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
				ali = [i+1 for i,elem in enumerate(patpat_ele) if elem=='octopus']
			#if same_ele['ocotpus']==1 and same_ele['umbrella']>=2 and same_ele['glasses']>=1:
			#		if patpat_ele[q]=='ocotpus':

		if len(ali_1) or len(tot_1):
			tot.append(ali)
			if all_alternative_pos[k] in (0,1):
				tot_1.append(ali) #not ali_1
			else:
				tot_1.append(ali_1)
		else:
			print("partial_ali {}".format(ali))
			tot.append(ali)
		ali=[]
		ali_1=[]

	all_properties = []
	all_properties.append(tot)
	all_properties.append(tot_1)
	all_properties.append(all_alternative_pos)
	all_properties.append(one_ans_is_enough)
	all_properties.append(multiple_branch_ans)
	all_properties.append(multiple_or_ans)
	all_properties.append(touch_together)
	all_properties.append(no_ans_to_create_only_body)
	all_properties.append(body_response)
	all_properties.append(body_and_patch)
	all_properties.append(need_light)
	all_properties.append(need_light1)
	all_properties.append(need_light2)
	all_properties.append(need_light3)
	all_properties.append(the_first)
	all_properties.append(the_second)
	all_properties.append(body_num_press)
	all_properties.append(all_num_touches_to_do)
	all_properties.append(ques_9_num)
	all_properties.append(limit)
	all_properties.append(limit1)
	all_properties.append(limit2)
	all_properties.append(limit3)
	all_properties.append(limitRed)
	all_properties.append(limitGreen)
	all_properties.append(limitBlue)
	all_properties.append(limitYellow)
	all_properties.append(limitLion)
	all_properties.append(limitMonkey)
	all_properties.append(limitParrot)
	all_properties.append(limitOctopus)
	return all_properties
