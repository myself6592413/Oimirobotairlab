""" Oimi Game principal module, contains all methods involving robot game. 
	At runtime this .pyx works only together with corresponding .pxd extension file
	All details can be found looking at game documentation game_docs.py ; check info also in game.pxd file
	Check DB for having a global view of Game attributes 
	
	runme_game() uses the state machine with events and status
"""
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/game') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/game')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/scenario') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/scenario')
if '/home/pi/OIMI/oimi_code/src/oimi_state_machine/') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/oimi_state_machine/')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/score') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/score')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/games_reactions/') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/games_reactions/')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/game_letsplay/') not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/game_letsplay/')

import myfsmmain as my
import fsm_event
from fsm_event import Trigger, Target
import time
from typing import Optional
import random
import copy
import numpy as np
import pandas as pd
from collections import Counter
cimport games_reactions as gare
cimport games_calcs as gaca
cimport modify_default_lists as mdl
import add_custom_pred_2 as acp2
import faulthandler; faulthandler.enable()
import database_manager as db
from games_classes cimport PyQeA
from games_classes cimport QeAWrappingList
from games_classes cimport PyPatch
from games_classes cimport PatchesWrappingList
from games_classes cimport PyQuestionType
from games_classes cimport PyQuestion
from games_classes cimport create_current_question
from games_classes cimport create_current_inquiry_d1
from games_classes cimport create_current_inquiry_d2
from games_classes cimport create_current_inquiry_d3
from games_classes cimport create_current_inquiry_d4
from games_classes cimport create_current_inquiry_d12
from games_classes cimport create_current_inquiry_d13
from games_classes cimport create_current_inquiry_d14
from games_classes cimport create_current_inquiry_d23
from games_classes cimport create_current_inquiry_d24
from games_classes cimport create_current_inquiry_d34
from games_classes cimport create_current_inquiry_d123
from games_classes cimport create_current_inquiry_d124
from games_classes cimport create_current_inquiry_d134
from games_classes cimport create_current_inquiry_d234
from games_classes cimport create_current_inquiry_d1234
from games_classes cimport create_current_inquiry
from games_classes cimport create_current_inquiry_list
from games_classes cimport create_current_patches
from games_classes cimport change_required_time
from enums_play cimport PatchCategory
from enums_play cimport GameCategory
from enums_play cimport Subject
from enums_play cimport Color
from enums_play cimport MatchDifficulty
from enums_play cimport GameDifficulty
from enums_play cimport SessionDifficulty
from enums_play cimport Feedback
from enums_play cimport QuestionKind
from enums_play cimport QuestionType
from enums_play cimport KidLevel
import game_answer_total_mixed gatm
import game_answer_contruction_1F as gc1
import game_answer_contruction_2L as gc2
import game_answer_contruction_3S as gc3
import game_answer_contruction_4P as gc4
import game_answer_contruction_5K as gc5
import game_answer_contruction_6I as gc6
import game_answer_contruction_7O as gc7
import game_answer_contruction_8Q as gc8
import game_answer_contruction_9C as gc9
cimport scenario as sc
cimport score as res
cimport utils_play as up
cimport game_letsplay as glp
cimport game
# Set max depth of recursion
import threading
sys.setrecursionlimit(10**7) 
threading.stack_size(2**27) 
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class Game:
	def __cinit__(self,int id_game, int num_questions, QuestionKind kind_of_game,QuestionType type_of_game,int ideal_necessary_time,
	GameDifficulty difficulty_of_game,int extra_time,sc.Scenario scenario, order_difficulty_questions, audio_g_intro, audio_g_exit, audio_rules, db_conn):
		self.id_game=id_game #=0 or not!
		self.num_questions=num_questions
		self.kind_of_game=kind_of_game
		self.type_of_game=type_of_game
		
		self.order_difficulty_questions = order_difficulty_questions
		self.audio_g_intro = audio_g_intro
		self.audio_g_exit = audio_g_exit
		self.audio_rules = audio_rules

		self.ideal_necessary_time=self.ideal_necessary_time
		self.difficulty_of_game= difficulty_of_game
		self.extra_time=extra_time
		self.scenario=scenario
		self.current_game_subjects=[] #to create QeA 
		self.current_game_colors=[] #to create QeA
		self.current_game_genres=[] #to create QeA
		self.num_responses=0 #runtime 
		self.user_dict_game={"task": self.preliminals,"check1":self.play_the_game,"check2":self.check_res, 
			'task_save_results':self.finally_calc_results} #states for fsm
		self.db_conn=db_conn
		#self.db_conn=db.DatabaseManager()
		
		self.one_ans_is_enough = []
		self.multiple_branch_ans = []
		self.multiple_or_ans = []
		self.touch_together = []
		self.no_ans_to_create_only_body = []
		self.body_response = []
		self.body_and_patch = []
		self.need_light = []
		self.need_light1 = []
		self.need_light2 = []
		self.need_light3 = []
		self.the_first = []
		self.the_second = []
		self.body_num_press = []
		self.all_num_touches_to_do = []
		self.ques_9_num = []
		self.limit = []
		self.limit1 = []
		self.limit2 = []
		self.limit3 = []
		self.limitRed = []
		self.limitGreen = []
		self.limitBlue = []
		self.limitYellow = []
		self.limitLion = []
		self.limitMonkey = []
		self.limitParrot = []
		self.limitOctopus = []
		self.listaudios_int = []
		self.set_necessary_time()
		self.select_questions() #to create QeA..
	def __enter__(self):
		print("Starting Games creation and initialization...")
	def __exit__(self,exc_type,exc_val,exc_tb):
		print("All games are ready to start.")
	def __eq__(self,other):
		cdef:
			Py_ssize_t i=0
			Py_ssize_t lenght=len(self.results)
			bint compare=1 
		for i in range (lenght):
			if self.ques_ans[i]!=other.ques_ans[i]:
				compare=0
				break
		if not isinstance(other,Game): 
			return NotImplemented
		return other.kind_of_game and self.required_time==other.required_time and \
			self.audio_intro_kind==other.audio_intro_kind and self.ques_value==other.ques_value and compare
	
	# -----------------------------------------------------------------------------
	# getters,setters 
	# -----------------------------------------------------------------------------
	@property
	def id_game(self):
		return self.id_game
	@id_game.setter
	def id_game(self,value):
		self.id_game=value
	@property
	def kind_of_game(self):
		return self.kind_of_game
	@kind_of_game.setter
	def kind_of_game(self,value):
		self.kind_of_game=value
	@property
	def audio_intro_kind(self):
		return self.audio_intro_kind
	@audio_intro_kind.setter
	def audio_intro_kind(self,value):
		self.audio_intro_kind=value
	@property
	def required_time(self):
		return self.required_time
	@required_time.setter
	def required_time(self,value):
		self.required_time=value
	@property
	def necessary_time(self):
		return self.necessary_time
	@necessary_time.setter
	def necessary_time(self,value):
		self.necessary_time=value
	@property
	def ideal_necessary_time(self):
		return self.ideal_necessary_time
	@ideal_necessary_time.setter
	def ideal_necessary_time(self,val):
		self.ideal_necessary_time=val
	@property
	def ques_value(self):
		return self.ques_value
	@ques_value.setter
	def ques_value(self,val):
		self.ques_value=val
	@property
	def num_questions(self):
		return self.num_questions
	@num_questions.setter
	def num_questions(self,value):
		self.num_questions=value	
	@property
	def scenario(self):
		return self.scenario
	@property
	def one_ans_is_enough(self):
		return self.one_ans_is_enough
	@one_ans_is_enough.setter
	def one_ans_is_enough(self,val):
		self.one_ans_is_enough=val
	@property
	def multiple_branch_ans(self):
		return self.multiple_branch_ans
	@multiple_branch_ans.setter
	def multiple_branch_ans(self,val):
		self.multiple_branch_ans=val		
	@property
	def multiple_or_ans(self):
		return self.multiple_or_ans
	@multiple_or_ans.setter
	def multiple_or_ans(self,val):
		self.multiple_or_ans=val		
	@property
	def touch_together(self):
		return self.touch_together
	@touch_together.setter
	def touch_together(self,val):
		self.touch_together=val
	@property
	def no_ans_to_create_only_body(self):
		return self.no_ans_to_create_only_body
	@no_ans_to_create_only_body.setter
	def no_ans_to_create_only_body(self,val):
		self.no_ans_to_create_only_body=val
	@property
	def body_response(self):
		return self.body_response
	@body_response.setter
	def body_response(self,val):
		self.body_response=val
	@property
	def body_and_patch(self):
		return self.body_and_patch
	@body_and_patch.setter
	def body_and_patch(self,val):
		self.body_and_patch=val
	@property
	def need_light(self):
		return self.need_light
	@need_light.setter
	def need_light(self,val):
		self.need_light=val
	@property
	def need_light1(self):
		return self.need_light1
	@need_light1.setter
	def need_light1(self,val):
		self.need_light1=val
	@property
	def need_light2(self):
		return self.need_light2
	@need_light2.setter
	def need_light2(self,val):
		self.need_light2=val
	@property
	def the_first(self):
		return self.the_first
	@the_first.setter
	def the_first(self,val):
		self.the_first=val
	@property
	def the_second(self):
		return self.the_second
	@the_second.setter
	def the_second(self,val):
		self.the_second=val
	@property
	def body_num_press(self):
		return self.body_num_press
	@body_num_press.setter
	def body_num_press(self,val):
		self.body_num_press=val
	@property
	def results(self):
		return self.results
	@results.setter
	def results(self,val):
		self.results=val	
	@property
	def resp_in_body(self):
		return self.resp_in_body
	@resp_in_body.setter
	def resp_in_body(self,val):
		self.resp_in_body=val
	@property
	def this_kind(self):
		return self.this_kind
	@this_kind.setter
	def this_kind(self,val):
		self.this_kind=val
	@property
	def this_type(self):
		return self.this_type
	@this_type.setter
	def this_type(self,val):
		self.this_type=val
	@property
	def one_ans_is_enough(self):
		return self.one_ans_is_enough
	@one_ans_is_enough.setter
	def one_ans_is_enough(self,val):
		self.one_ans_is_enough=val
	@property
	def multiple_branch_ans(self):
		return self.multiple_branch_ans
	@multiple_branch_ans.setter
	def multiple_branch_ans(self,val):
		self.multiple_branch_ans=val
	@property
	def multiple_or_ans(self):
		return self.multiple_or_ans
	@multiple_or_ans.setter
	def multiple_or_ans(self,val):
		self.multiple_or_ans=val
	@property
	def touch_together(self):
		return self.touch_together
	@touch_together.setter
	def touch_together(self,val):
		self.touch_together=val
	@property
	def no_ans_to_create_only_body(self):
		return self.no_ans_to_create_only_body
	@no_ans_to_create_only_body.setter
	def no_ans_to_create_only_body(self,val):
		self.no_ans_to_create_only_body=val
	@property
	def body_response(self):
		return self.body_response
	@body_response.setter
	def body_response(self,val):
		self.body_response=val
	@property
	def body_and_patch(self):
		return self.body_and_patch
	@body_and_patch.setter
	def body_and_patch(self,val):
		self.body_and_patch=val
	@property
	def need_light(self):
		return self.need_light
	@need_light.setter
	def need_light(self,val):
		self.need_light=val
	@property
	def need_light1(self):
		return self.need_light1
	@need_light1.setter
	def need_light1(self,val):
		self.need_light1=val
	@property
	def need_light2(self):
		return self.need_light2
	@need_light2.setter
	def need_light2(self,val):
		self.need_light2=val
	@property
	def need_light3(self):
		return self.need_light3
	@need_light3.setter
	def need_light3(self,val):
		self.need_light3=val
	@property
	def need_light4(self):
		return self.need_light4
	@need_light4.setter
	def need_light4(self,val):
		self.need_light4=val
	@property
	def the_first(self):
		return self.the_first
	@the_first.setter
	def the_first(self,val):
		self.the_first=val
	@property
	def the_second(self):
		return self.the_second
	@the_second.setter
	def the_second(self,val):
		self.the_second=val
	@property
	def all_num_touches_to_do(self):
		return self.all_num_touches_to_do
	@all_num_touches_to_do.setter
	def all_num_touches_to_do(self,val):
		self.all_num_touches_to_do=val
	@property
	def ques_9_num(self):
		return self.ques_9_num
	@ques_9_num.setter
	def ques_9_num(self,val):
		self.ques_9_num=val
	@property
	def same_ele(self):
		return self.same_ele
	@same_ele.setter
	def same_ele(self,val):
		self.same_ele=val
	@property
	def same_col(self):
		return self.same_col
	@same_col.setter
	def same_col(self,val):
		self.same_col=val
	@property
	def limit(self):
		return self.limit
	@limit.setter
	def limit(self,val):
		self.limit=val
	@property
	def limit1(self):
		return self.limit1
	@limit1.setter
	def limit1(self,val):
		self.limit1=val
	@property
	def limit2(self):
		return self.limit2
	@limit2.setter
	def limit2(self,val):
		self.limit2=val
	@property
	def limit3(self):
		return self.limit3
	@limit3.setter
	def limit3(self,val):
		self.limit3=val
	@property
	def limit4(self):
		return self.limit4
	@limit4.setter
	def limit4(self,val):
		self.limit4=val
	@property
	def limitRed(self):
		return self.limitRed
	@limitRed.setter
	def limitRed(self,val):
		self.limitRed=val
	@property
	def limitGreen(self):
		return self.limitGreen
	@limitGreen.setter
	def limitGreen(self,val):
		self.limitGreen=val
	@property
	def limitBlue(self):
		return self.limitBlue
	@limitBlue.setter
	def limitBlue(self,val):
		self.limitBlue=val
	@property
	def limitYellow(self):
		return self.limitYellow
	@limitYellow.setter
	def limitYellow(self,val):
		self.limitYellow=val
	@property
	def limitLion(self):
		return self.limitLion
	@limitLion.setter
	def limitLion(self,val):
		self.limitLion=val
	@property
	def limitMonkey(self):
		return self.limitMonkey
	@limitMonkey.setter
	def limitMonkey(self,val):
		self.limitMonkey=val
	@property
	def limitParrot(self):
		return self.limitParrot
	@limitParrot.setter
	def limitParrot(self,val):
		self.limitParrot=val
	@property
	def limitOctopus(self):
		return self.limitOctopus
	@limitOctopus.setter
	def limitOctopus(self,val):
		self.limitOctopus=val
	@property
	def difficulty_of_game(self):
		return self.difficulty_of_game
	@difficulty_of_game.setter
	def difficulty_of_game(self,val):
		self.difficulty_of_game=val
	@property
	def current_games_subjects(self):
		return self.current_games_subjects
	@current_games_subjects.setter
	def current_games_subjects(self,val):
		self.current_games_subjects=val
	@property
	def current_game_colors(self):
		return self.current_game_colors
	@current_game_colors.setter
	def current_game_colors(self,val):
		self.current_game_colors=val
	@property
	def current_game_genres(self):
		return self.current_game_genres
	@current_game_genres.setter
	def current_game_genres(self,val):
		self.current_game_genres=val
	@property
	def listaudios(self):
		return self.listaudios
	@listaudios.setter
	def listaudios(self,val):
		self.listaudios=val	
	@property
	def listaudios_int(self):
		return self.listaudios_int
	@listaudios_int.setter
	def listaudios_int(self,val):
		self.listaudios_int=val			
	@property
	def listaTypes(self):
		return self.listaTypes
	@listaTypes.setter
	def listaTypes(self,val):
		self.listaTypes=val
	@property
	def ques_ans(self):
		return self.ques_ans
	@ques_ans.setter
	def ques_ans(self,val):
		self.ques_ans=val
	@property
	def ques_ans_1(self):
		return self.ques_ans_1
	@ques_ans_1.setter
	def ques_ans_1(self,val):
		self.ques_ans_1=val		
	# -----------------------------------------------------------------------------
	# Game methods 
	# -----------------------------------------------------------------------------
	cpdef set_audio_introkind(self):
		self.audio_intro_kind=up.choose_audiokind(self.kind_of_game)
	
	cpdef set_necessary_time(self): 
		"""Idead total time of a game"""
		print("kind of game is {}".format(self.kind_of_game))
		if self.kind_of_game=='k1F' and self.extra_time:
			if self.ideal_necessary_time<=120:
				self.necessary_time=140
			else:
				self.necessary_time=self.ideal_necessary_time + 10
		if self.kind_of_game=='k2L':
			if self.ideal_necessary_time<=120:
				self.necessary_time=140
			else:
				self.necessary_time=self.ideal_necessary_time + 10
		if self.kind_of_game=='k3S':
			if self.ideal_necessary_time<=120:
				self.necessary_time=140
			else:
				self.necessary_time=self.ideal_necessary_time + 10
		if self.kind_of_game=='k4Q':
			if self.ideal_necessary_time<=120:
				self.necessary_time=140
			else:
				self.necessary_time=self.ideal_necessary_time + 10
		if self.kind_of_game=='k5K':
			if self.ideal_necessary_time<=120:
				self.necessary_time=140
			else:
				self.necessary_time=self.ideal_necessary_time + 10
		if self.kind_of_game=='k6I':
			if self.ideal_necessary_time<=120:
				self.necessary_time=140
			else:
				self.necessary_time=self.ideal_necessary_time + 10
		if self.kind_of_game=='k7O':
			if self.ideal_necessary_time<=120:
				self.necessary_time=140
			else:
				self.necessary_time=self.ideal_necessary_time + 10
		if self.kind_of_game=='k8Q':
			if self.ideal_necessary_time<=120:
				self.necessary_time=140
			else:
				self.necessary_time=self.ideal_necessary_time + 10
		if self.kind_of_game=='k9C':
			if self.ideal_necessary_time<=120:
				self.necessary_time=140
			else:
				self.necessary_time=self.ideal_necessary_time + 10
		print("final necessary_time {}".format(self.necessary_time))
		#self.necessary_time=self.required_time * self.num_questions + 5 
			
	cpdef set_num_questions(self, num_of_q): 
		self.num_questions=copy.copy(num_of_q)
		#print("self.num_questions (IN SET NUM QUESTION) are {}".format(self.num_questions))
	
	cdef build_answers(self, length, tot, l1, l2, l3, l4, all_alternative_pos, self.ques_9_num, num_touches_to_do):
		""" Build the correct answer based on the current scenario, All limits attributes are the number of possible patch that correspond to answer of question1/2/3/4 
			depending on the question limit*Color or limit*Subject represent the num of different patch on the scanario 
		"""
		print("------------------------------------->>> here are the attributes to build ans------------------------------------->>>")
		print(self.one_ans_is_enough)
		print(self.multiple_branch_ans)
		print(self.multiple_or_ans)
		print(self.touch_together)
		print(self.no_ans_to_create_only_body)
		print(self.body_response)
		print(self.body_and_patch)
		print(self.need_light)
		print(self.need_light1)
		print(self.need_light2)
		print(self.need_light3)
		print(self.the_first)
		print(self.the_second)
		print(self.body_num_press)
		print(self.all_num_touches_to_do)
		print(self.ques_9_num)
		print(self.limit)
		print(self.limit1)
		print(self.limit2)
		print(self.limit3)
		print(self.limitRed)
		print(self.limitGreen)
		print(self.limitBlue)
		print(self.limitYellow)
		print(self.limitLion)
		print(self.limitMonkey)
		print(self.limitParrot)
		print(self.limitOctopus)
		max_ques_9 = max(self.ques_9_num)
		max_num_touches = max(num_touches_to_do)
		if length==1:
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 not in all_alternative_pos: 
				# print("len tot =========== 1")
				l1.append(tot[0][0])
				l2.append(5)
				l3.append(5)
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==1: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append(5)
				l3.append(5)
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==1: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append(5)
				l3.append(5)
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==1: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append(5)
				l3.append(5)
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==1: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append(5)
				l3.append(5)
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==2: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([5])
				l3.append(5)
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==2: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([5,5])
				l3.append(5)
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==2: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([5,5,5])
				l3.append(5)
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==2: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([5,5,5,5])
				l3.append(5)
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==3: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([5])
				l3.append([5])
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==3: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([5,5])
				l3.append([5,5])
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==3: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([5,5,5])
				l3.append([5,5,5])
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==3: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([5,5,5,5])
				l3.append([5,5,5,5])
				l4.append(5)
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==4: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([5])
				l3.append([5])
				l4.append([5])
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==4: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([5,5])
				l3.append([5,5])
				l4.append([5,5])
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==4: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([5,5,5])
				l3.append([5,5,5])
				l4.append([5,5,5])
			if len(tot[0])==1 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==4: 
				# print("len tot =========== 1 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([5,5,5,5])
				l3.append([5,5,5,5])
				l4.append([5,5,5,5])								

			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 not in all_alternative_pos:
				# print("len tot =========== 2")
				l1.append(tot[0][0])
				l2.append(tot[0][1])
				l3.append(5)
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==1:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append(tot[0][1])
				l3.append(5)
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==1:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append(tot[0][1])
				l3.append(5)
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==1:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append(tot[0][1])
				l3.append(5)
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==1:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append(tot[0][1])
				l3.append(5)
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==2:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([tot[0][1]])
				l3.append(5)
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==2:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1]])
				l3.append(5)
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==2:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1]])
				l3.append(5)
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==2:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1],tot[0][1]])
				l3.append(5)
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==3:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([tot[0][1]])
				l3.append([5])
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==3:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1]])
				l3.append([5,5])
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==3:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1]])
				l3.append([5,5,5])
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==3:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1],tot[0][1]])
				l3.append([5,5,5,5])
				l4.append(5)
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==4:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([tot[0][1]])
				l3.append([5])
				l4.append([5])
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==4:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1]])
				l3.append([5,5])
				l4.append([5,5])
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==4:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1]])
				l3.append([5,5,5])
				l4.append([5,5,5])
			elif len(tot[0])==2 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==4:
				# print("len tot =========== 2 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1],tot[0][1]])
				l3.append([5,5,5,5])
				l4.append([5,5,5,5])						
																
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 not in all_alternative_pos:
				# print("len tot =========== 3")
				l1.append(tot[0][0])
				l2.append(tot[0][1])
				l3.append(tot[0][2])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==1:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append(tot[0][1])
				l3.append(tot[0][2])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==1:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append(tot[0][1])
				l3.append(tot[0][2])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==1:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append(tot[0][1])
				l3.append(tot[0][2])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==1:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append(tot[0][1])
				l3.append(tot[0][2])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==2:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([tot[0][1]])
				l3.append(tot[0][2])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==2:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1]])
				l3.append(tot[0][2])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==2:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1]])
				l3.append(tot[0][2])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==2:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1],tot[0][1]])
				l3.append(tot[0][2])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==3:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([tot[0][1]])
				l3.append([tot[0][2]])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==3:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2]])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==3:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2],tot[0][2]])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==3:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2],tot[0][2],tot[0][2]])
				l4.append(5)
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==4:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([tot[0][1]])
				l3.append([tot[0][2]])
				l4.append([5])
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==4:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2]])
				l4.append([5,5])
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==4:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2],tot[0][2]])
				l4.append([5,5,5])
			elif len(tot[0])==3 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==4:
				# print("len tot =========== 3 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2],tot[0][2],tot[0][2]])
				l4.append([5,5,5,5])								

			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 not in all_alternative_pos:
				# print("len tot =========== 4")
				l1.append(tot[0][0])
				l2.append(tot[0][1])
				l3.append(tot[0][2])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==1:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append(tot[0][1])
				l3.append(tot[0][2])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==1:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append(tot[0][1])
				l3.append(tot[0][2])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==1:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append(tot[0][1])
				l3.append(tot[0][2])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==1:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append(tot[0][1])
				l3.append(tot[0][2])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==2:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([tot[0][1]])
				l3.append(tot[0][2])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==2:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1]])
				l3.append(tot[0][2])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==2:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1]])
				l3.append(tot[0][2])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==2:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1],tot[0][1]])
				l3.append(tot[0][2])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==3:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([tot[0][1]])
				l3.append([tot[0][2]])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==3:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2]])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==3:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2],tot[0][2]])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==3:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2],tot[0][2],tot[0][2]])
				l4.append(tot[0][3])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==4:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0]])
				l2.append([tot[0][1]])
				l3.append([tot[0][2]])
				l4.append([tot[0][3]])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==4:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2]])
				l4.append([tot[0][3],tot[0][3]])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==4:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0]])
				l2.append([tot[0][1],tot[0][1],tot[0][1]])
				l3.append([tot[0][2],tot[0][2],tot[0][2]])
				l4.append([tot[0][3],tot[0][3],tot[0][3]])
			elif len(tot[0])==4 and all_alternative_pos[0] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==4:
				# print("len tot =========== 4 and sooner or later 1 and q9n={} and nmtdo={}".format(max_ques_9,max_num_touches))
				l1.append([tot[0][0],tot[0][0],tot[0][0],tot[0][0]])
				l2.append(tot[0][1],tot[0][1],tot[0][1],tot[0][1])
				l3.append(tot[0][2],tot[0][2],tot[0][2],tot[0][2])
				l4.append(tot[0][3],tot[0][3],tot[0][3],tot[0][3])
			#################################################
			if all_alternative_pos[0]==1 and max_num_touches==1:
				# print("single case!! num_audios=1 ; all_alternative_pos =========== 1 ;; num_touches_to_do=1")
				l1.append(tot[0])
				l2.append(5)
				l3.append(5)
				l4.append(5)
			elif all_alternative_pos[0]==1 and max_num_touches==2:
				# print("single case!! num_audios=1 ; all_alternative_pos =========== 2 ;; num_touches_to_do=2")
				l1.append(tot[0])
				l2.append(tot[0])
				l3.append(5)
				l4.append(5)
			elif all_alternative_pos[0]==1 and max_num_touches==3:
				# print("single case!! num_audios=1 ; all_alternative_pos =========== 3 ;; num_touches_to_do=3")
				l1.append(tot[0])
				l2.append(tot[0])
				l3.append(tot[0])
				l4.append(5)
			elif all_alternative_pos[0]==1 and max_num_touches==4:
				# print("single case!! num_audios=1 ; all_alternative_pos =========== 4 ;; num_touches_to_do=4")
				l1.append(tot[0])
				l2.append(tot[0])
				l3.append(tot[0])
				l4.append(tot[0])
		###############
		if length>=2:
			# print("all all_alternative_pos build_answers!! {}".format(all_alternative_pos))
			# print("tot build_answers!! {}".format(tot))
			for a in range(len(tot)):
				b=0
				# print("Indexes:")
				print("a is is {}".format(a))
				print("b is is {}".format(b))
				print("tot[a] {}".format(tot[a]))
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 not in all_alternative_pos:
					# print("len tot =========== 1 case 0")
					l1.append(tot[a][b])
					l2.append(5)
					l3.append(5)
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==1:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append(5)
					l3.append(5)
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==1:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append(5)
					l3.append(5)
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==1:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append(5)
					l3.append(5)
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==1:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append(5)
					l3.append(5)
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==2:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([5])
					l3.append(5)
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==2:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([5,5])
					l3.append(5)
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==2:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([5,5,5])
					l3.append(5)
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==2:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([5,5,5,5])
					l3.append(5)
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==3:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([5])
					l3.append([5])
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==3:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([5,5])
					l3.append([5,5])
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==3:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([5,5,5])
					l3.append([5,5,5])
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==3:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([5,5,5,5])
					l3.append([5,5,5,5])
					l4.append(5)
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==4:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([5])
					l3.append([5])
					l4.append([5])
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==4:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([5,5])
					l3.append([5,5])
					l4.append([5,5])
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==4:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([5,5,5])
					l3.append([5,5,5])
					l4.append([5,5,5])
				if len(tot[a])==1 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==4:
					# print("len tot =========== 1 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([5,5,5,5])
					l3.append([5,5,5,5])
					l4.append([5,5,5,5])																			


				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 not in all_alternative_pos:
					# print("len tot =========== 2 case 0")
					l1.append(tot[a][b])
					l2.append(tot[a][b+1])
					l3.append(5)
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==1:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(5)
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==1:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(5)
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==1:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(5)
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==1:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(5)
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==2:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([tot[a][b+1]])
					l3.append(5)
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==2:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1]])
					l3.append(5)
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==2:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append(5)
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==2:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append(5)
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==3:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([tot[a][b+1]])
					l3.append([5])
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==3:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1]])
					l3.append([5,5])
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==3:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([5,5,5])
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==3:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([5,5,5,5])
					l4.append(5)
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==4:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([tot[a][b+1]])
					l3.append([5])
					l4.append([5])
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==4:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1]])
					l3.append([5,5])
					l4.append([5,5])
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==4:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([5,5,5])
					l4.append([5,5,5])
				elif len(tot[a])==2 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==4:
					# print("len tot =========== 2 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([5,5,5,5])
					l4.append([5,5,5,5])

				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 not in all_alternative_pos:
					# print("len tot =========== 3 case 0")
					l1.append(tot[a][b])
					l2.append(tot[a][b+1])
					l3.append(tot[a][b+2])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==1:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(tot[a][b+2])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==1:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(tot[a][b+2])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==1:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(tot[a][b+2])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==1:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(tot[a][b+2])
					l4.append(5)															
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==2:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([tot[a][b+1]])
					l3.append(tot[a][b+2])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==2:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1]])
					l3.append(tot[a][b+2])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==2:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append(tot[a][b+2])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==2:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append(tot[a][b+2])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==3:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([tot[a][b+1]])
					l3.append([tot[a][b+2]])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==3:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2]])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==3:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2],tot[a][b+2]])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==3:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2],tot[a][b+2]])
					l4.append(5)
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==4:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([tot[a][b+1]])
					l3.append([tot[a][b+2]])
					l4.append([5])
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==4:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2]])
					l4.append([5,5])
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==4:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2],tot[a][b+2]])
					l4.append([5,5,5])
				elif len(tot[a])==3 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==4:
					# print("len tot =========== 3 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2],tot[a][b+2],tot[a][b+2]])
					l4.append([5,5,5,5])										

				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 not in all_alternative_pos:
					# print("len tot	=========== 4 case 0")
					l1.append(tot[a][b])
					l2.append(tot[a][b+1])
					l3.append(tot[a][b+2])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==1:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(tot[a][b+2])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==1:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(tot[a][b+2])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==1:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(tot[a][b+2])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==1:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append(tot[a][b+1])
					l3.append(tot[a][b+2])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==2:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([tot[a][b+1]])
					l3.append(tot[a][b+2])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==2:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1]])
					l3.append(tot[a][b+2])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==2:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append(tot[a][b+2])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==2:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append(tot[a][b+2])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==3:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2]])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==3:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2]])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==3:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2]])
					l4.append(tot[a][b+3])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==3:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2],tot[a][b+2],tot[a][b+2]])
					l4.append(tot[a][b+3])					
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==1 and max_num_touches==4:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b]])
					l2.append([tot[a][b+1]])
					l3.append([tot[a][b+2]])
					l4.append([tot[a][b+3]])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==2 and max_num_touches==4:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2]])
					l4.append([tot[a][b+3],tot[a][b+3]])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==3 and max_num_touches==4:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2],tot[a][b+2]])
					l4.append([tot[a][b+3],tot[a][b+3],tot[a][b+3]])
				elif len(tot[a])==4 and all_alternative_pos[a] in (0,2) and 1 in all_alternative_pos and max_ques_9==4 and max_num_touches==4:
					# print("len tot =========== 4 and sooner or later 1 and MAX q9n={} and MAX nmtdo={}".format(max_ques_9,max_num_touches))
					l1.append([tot[a][b],tot[a][b],tot[a][b],tot[a][b]])
					l2.append([tot[a][b+1],tot[a][b+1],tot[a][b+1],tot[a][b+1]])
					l3.append([tot[a][b+2],tot[a][b+2],tot[a][b+2],tot[a][b+2]])
					l4.append([tot[a][b+3],tot[a][b+3],tot[a][b+3],tot[a][b+3]])
				
				############################################################################################ all_alternative_pos[a]==1
				#len tot[a]==1 ; and num_touches_to_do[a]==1 
				if all_alternative_pos[a]==1:
					lentotx=len(tot[a])
					if lentotx==1:
						if num_touches_to_do[a]==1:
							if max_ques_9==1:
								if max_num_touches==1:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(5)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append([5])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append([5])
									l3.append([5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append([5])
									l3.append([5])
									l4.append([5])
							if max_ques_9==2:
								if max_num_touches==1:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append(5)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5])
									l3.append([5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5])
									l3.append([5,5])
									l4.append([5,5])
							if max_ques_9==3:
								if max_num_touches==1:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append(5)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5])
									l3.append([5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5])
									l3.append([5,5,5])
									l4.append([5,5,5])
							if max_ques_9==4:
								if max_num_touches==1:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append(5)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5,5])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5,5])
									l3.append([5,5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5,5])
									l3.append([5,5,5,5])
									l4.append([5,5,5,5])

						if num_touches_to_do[a]==2:
							if max_ques_9==1:
								if max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append([5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append([5])
									l4.append([5])
							if max_ques_9==2:
								if max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5])
									l4.append([5,5])
							if max_ques_9==3:
								if max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l2.append(l2_tmp)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5,5])
									l4.append([5,5,5])
							if max_ques_9==4:
								if max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5,5,5])
									l4.append([5,5,5,5])

						if num_touches_to_do[a]==3:
							if max_ques_9==1:
								if max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append(tot[a])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append(tot[a])
									l4.append([5])
							if max_ques_9==2:
								if max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append([5,5])
							if max_ques_9==3:
								if max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append([5,5,5])
							if max_ques_9==4:
								if max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp.append(55)
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp.append(55)
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append([5,5,5,5])

						if num_touches_to_do[a]==4:
							if max_ques_9==1:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append(tot[a])
								l3.append(tot[a])
								l4.append(tot[a])
							if max_ques_9==2:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1_tmp=[]
								l1_tmp.append(tot[a])
								l1_tmp.append(55)
								l1_tmp=list(acp2.iterFlatten(l1_tmp))
								l1.append(l1_tmp)
								l2_tmp=[]
								l2_tmp.append(tot[a])
								l2_tmp.append(55)
								l2_tmp=list(acp2.iterFlatten(l2_tmp))
								l2.append(l2_tmp)
								l3_tmp=[]
								l3_tmp.append(tot[a])
								l3_tmp.append(55)
								l3_tmp=list(acp2.iterFlatten(l3_tmp))
								l3.append(l3_tmp)
								l4_tmp=[]
								l4_tmp.append(tot[a])
								l4_tmp.append(55)
								l4_tmp=list(acp2.iterFlatten(l4_tmp))
								l4.append(l4_tmp)
							if max_ques_9==3:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1_tmp=[]
								l1_tmp.append(tot[a])
								l1_tmp.append(55)
								l1_tmp.append(55)
								l1_tmp=list(acp2.iterFlatten(l1_tmp))
								l1.append(l1_tmp)
								l2_tmp=[]
								l2_tmp.append(tot[a])
								l2_tmp.append(55)
								l2_tmp.append(55)
								l2_tmp=list(acp2.iterFlatten(l2_tmp))
								l2.append(l2_tmp)
								l3_tmp=[]
								l3_tmp.append(tot[a])
								l3_tmp.append(55)
								l3_tmp.append(55)
								l3_tmp=list(acp2.iterFlatten(l3_tmp))
								l3.append(l3_tmp)
								l4_tmp=[]
								l4_tmp.append(tot[a])
								l4_tmp.append(55)
								l4_tmp.append(55)
								l4_tmp=list(acp2.iterFlatten(l4_tmp))
								l4.append(l4_tmp)
							if max_ques_9==4:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1_tmp=[]
								l1_tmp.append(tot[a])
								l1_tmp.append(55)
								l1_tmp.append(55)
								l1_tmp.append(55)
								l1_tmp=list(acp2.iterFlatten(l1_tmp))
								l1.append(l1_tmp)
								l2_tmp=[]
								l2_tmp.append(tot[a])
								l2_tmp.append(55)
								l2_tmp.append(55)
								l2_tmp.append(55)
								l2_tmp=list(acp2.iterFlatten(l2_tmp))
								l2.append(l2_tmp)
								l3_tmp=[]
								l3_tmp.append(tot[a])
								l3_tmp.append(55)
								l3_tmp.append(55)
								l3_tmp.append(55)
								l3_tmp=list(acp2.iterFlatten(l3_tmp))
								l3.append(l3_tmp)
								l4_tmp=[]
								l4_tmp.append(tot[a])
								l4_tmp.append(55)
								l4_tmp.append(55)
								l4_tmp.append(55)
								l4_tmp=list(acp2.iterFlatten(l4_tmp))
								l4.append(l4_tmp)
					if lentotx==2:
						if num_touches_to_do[a]==1:
							if max_ques_9==2:
								if max_num_touches==1:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(5)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append([5,5])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append([5,5])
									l3.append([5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append([5,5])
									l3.append([5,5])
									l4.append([5,5])
							if max_ques_9==3:
								if max_num_touches==1:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append(5)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5])
									l3.append([5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5])
									l3.append([5,5,5])
									l4.append([5,5,5])
							if max_ques_9==4:
								if max_num_touches==1:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append(5)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5,5])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5,5])
									l3.append([5,5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5,5])
									l3.append([5,5,5,5])
									l4.append([5,5,5,5])

						if num_touches_to_do[a]==2:
							if max_ques_9==2:
								if max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append([5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append([5,5])
									l4.append([5,5])
							if max_ques_9==3:
								if max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5,5])
									l4.append([5,5,5])
							if max_ques_9==4:
								if max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5,5,5])
									l4.append([5,5,5,5])

						if num_touches_to_do[a]==3:
							if max_ques_9==2:
								if max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append(tot[a])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append(tot[a])
									l4.append([5,5])
							if max_ques_9==3:
								if max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append([5,5,5])
							if max_ques_9==4:
								if max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append([5,5,5,5])

						if num_touches_to_do[a]==4:
							if max_ques_9==2:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append(tot[a])
								l3.append(tot[a])
								l4.append(tot[a])
							if max_ques_9==3:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1_tmp=[]
								l1_tmp.append(tot[a])
								l1_tmp.append(55)
								l1_tmp=list(acp2.iterFlatten(l1_tmp))
								l1.append(l1_tmp)
								l2_tmp=[]
								l2_tmp.append(tot[a])
								l2_tmp.append(55)
								l2_tmp=list(acp2.iterFlatten(l2_tmp))
								l2.append(l2_tmp)
								l3_tmp=[]
								l3_tmp.append(tot[a])
								l3_tmp.append(55)
								l3_tmp=list(acp2.iterFlatten(l3_tmp))
								l3.append(l3_tmp)
								l4_tmp=[]
								l4_tmp.append(tot[a])
								l4_tmp.append(55)
								l4_tmp=list(acp2.iterFlatten(l4_tmp))
								l4.append(l4_tmp)								
							if max_ques_9==4:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1_tmp=[]
								l1_tmp.append(tot[a])
								l1_tmp.append(55)
								l1_tmp.append(55)
								l1_tmp=list(acp2.iterFlatten(l1_tmp))
								l1.append(l1_tmp)
								l2_tmp=[]
								l2_tmp.append(tot[a])
								l2_tmp.append(55)
								l2_tmp.append(55)
								l2_tmp=list(acp2.iterFlatten(l2_tmp))
								l2.append(l2_tmp)
								l3_tmp=[]
								l3_tmp.append(tot[a])
								l3_tmp.append(55)
								l3_tmp.append(55)
								l3_tmp=list(acp2.iterFlatten(l3_tmp))
								l3.append(l3_tmp)
								l4_tmp=[]
								l4_tmp.append(tot[a])
								l4_tmp.append(55)
								l4_tmp.append(55)
								l4_tmp=list(acp2.iterFlatten(l4_tmp))
								l4.append(l4_tmp)
					if lentotx==3:
						if num_touches_to_do[a]==1:
							if max_ques_9==3:
								if max_num_touches==1:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(5)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append([5,5,5])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append([5,5,5])
									l3.append([5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append([5,5,5])
									l3.append([5,5,5])
									l4.append([5,5,5])
							if max_ques_9==4:
								if max_num_touches==1:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append(5)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5,5])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5,5])
									l3.append([5,5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2.append([5,5,5,5])
									l3.append([5,5,5,5])
									l4.append([5,5,5,5])

						if num_touches_to_do[a]==2:
							if max_ques_9==3:
								if max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append([5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append([5,5,5])
									l4.append([5,5,5])
							if max_ques_9==4:
								if max_num_touches==2:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append(5)
									l4.append(5)
								elif max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5,5,5])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3.append([5,5,5,5])
									l4.append([5,5,5,5])

						if num_touches_to_do[a]==3:
							if max_ques_9==3:
								if max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append(tot[a])
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1.append(tot[a])
									l2.append(tot[a])
									l3.append(tot[a])
									l4.append([5,5,5])
							if max_ques_9==4:
								if max_num_touches==3:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append(5)
								elif max_num_touches==4:
									print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
									l1_tmp=[]
									l1_tmp.append(tot[a])
									l1_tmp.append(55)
									l1_tmp=list(acp2.iterFlatten(l1_tmp))
									l1.append(l1_tmp)
									l2_tmp=[]
									l2_tmp.append(tot[a])
									l2_tmp.append(55)
									l2_tmp=list(acp2.iterFlatten(l2_tmp))
									l2.append(l2_tmp)
									l3_tmp=[]
									l3_tmp.append(tot[a])
									l3_tmp.append(55)
									l3_tmp=list(acp2.iterFlatten(l3_tmp))
									l3.append(l3_tmp)
									l4.append([5,5,5,5])

						if num_touches_to_do[a]==4:
							if max_ques_9==3:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append(tot[a])
								l3.append(tot[a])
								l4.append(tot[a])
							if max_ques_9==4:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1_tmp=[]
								l1_tmp.append(tot[a])
								l1_tmp.append(55)
								l1_tmp=list(acp2.iterFlatten(l1_tmp))
								l1.append(l1_tmp)
								l2_tmp=[]
								l2_tmp.append(tot[a])
								l2_tmp.append(55)
								l2_tmp=list(acp2.iterFlatten(l2_tmp))
								l2.append(l2_tmp)
								l3_tmp=[]
								l3_tmp.append(tot[a])
								l3_tmp.append(55)
								l3_tmp=list(acp2.iterFlatten(l3_tmp))
								l3.append(l3_tmp)
								l3_tmp=[]
								l3_tmp.append(tot[a])
								l3_tmp.append(55)
								l3_tmp=list(acp2.iterFlatten(l3_tmp))
								l3.append(l3_tmp)
								l4_tmp=[]
								l4_tmp.append(tot[a])
								l4_tmp.append(55)
								l4_tmp=list(acp2.iterFlatten(l4_tmp))
								l4.append(l4_tmp)
					if lentotx==4:
						if num_touches_to_do[a]==1:
							if max_num_touches==1:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append(5)
								l3.append(5)
								l4.append(5)
							elif max_num_touches==2:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append([5,5,5,5])
								l3.append(5)
								l4.append(5)
							elif max_num_touches==3:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append([5,5,5,5])
								l3.append([5,5,5,5])
								l4.append(5)
							elif max_num_touches==4:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append([5,5,5,5])
								l3.append([5,5,5,5])
								l4.append([5,5,5,5])

						if num_touches_to_do[a]==2:
							if max_num_touches==2:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append(tot[a])
								l3.append(5)
								l4.append(5)
							elif max_num_touches==3:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append(tot[a])
								l3.append([5,5,5,5])
								l4.append(5)
							elif max_num_touches==4:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append(tot[a])
								l3.append([5,5,5,5])
								l4.append([5,5,5,5])

						if num_touches_to_do[a]==3:
							if max_num_touches==3:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append(tot[a])
								l3.append(tot[a])
								l4.append(5)
							elif max_num_touches==4:
								print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
								l1.append(tot[a])
								l2.append(tot[a])
								l3.append(tot[a])
								l4.append([5,5,5,5])

						if num_touches_to_do[a]==4:
							print("LEN TOT => {} ;; num_touches_to_do ==> {} ;; max_ques_9 ===> {} ;; max_num_touches ====> {}".format(lentotx,num_touches_to_do[a],max_ques_9,max_num_touches))
							l1.append(tot[a])
							l2.append(tot[a])
							l3.append(tot[a])
							l4.append(tot[a])

		print("000000000000000000000000000000000000000000000000000000000000000")
		print("listans1 ==> ==> {}".format(l1))
		print("listans2 ==> ==> {}".format(l2))
		print("listans3 ==> ==> {}".format(l3))
		print("listans4 ==> ==> {}".format(l4))
		print("000000000000000000000000000000000000000000000000000000000000000")
		return l1,l2,l3,l4
	
	cdef write_responses(self,patpat_ele,patpat_col,patpat_gen,listaudios,listaKinds,listaTypes):
		cdef:
			list ali=[]
			list ali_tmp=[]
			list ali_1=[]
			list tot=[]
			list tot_1=[]
			list all_alternative_pos=[0,0,0,0]
			list listans1=[]
			list listans2=[]
			list listans3=[]
			list listans4=[]
			list listans11=[]
			list listans12=[]
			list listans13=[]
			list listans14=[]
		self.listaTypes = copy.copy(listaTypes)
		self.same_col=Counter(patpat_col)
		self.same_ele=Counter(patpat_ele)
		print("########################### SAME COL, SAME ELE")
		print("same_col {}".format(self.same_col))
		print("same_ele {}".format(self.same_ele))
		print("###########################")
		print("LSITA ADSADASDAS={}".format(listaudios))
		print("LISTA kinds LISTA kinds LISTA kinds LISTA kinds LISTA kinds LISTA kinds LISTA kinds={}".format(listaKinds))
		take_whole_attributes=[]
		if 'MIX' in listaKinds:
			print("MIX founded now!!!")
			take_whole_attributes=gatm.build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,listaTypes,self.same_col,self.same_ele)
		if '1F' in listaKinds:
			print("1F founded now!!!")
			take_whole_attributes=gc1.build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,self.same_col,self.same_ele)
		elif '2L' in listaKinds:
			print("2L founded now!!!")
			take_whole_attributes=gc2.build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,self.same_col,self.same_ele)
		elif '3S' in listaKinds:
			print("3S founded now!!!")
			take_whole_attributes=gc3.build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,self.same_col,self.same_ele)
		elif '4P' in listaKinds:
			print("4P founded now!!!")
			take_whole_attributes=gc4.build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,self.same_col,self.same_ele)
		elif '5K' in listaKinds:
			print("5K founded now!!!")
			take_whole_attributes=gc5.build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,self.same_col,self.same_ele)
		elif '6I' in listaKinds:
			print("6I founded now!!!")
			take_whole_attributes=gc6.build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,self.same_col,self.same_ele)
		elif '7O' in listaKinds:
			print("7O founded now!!!")
			take_whole_attributes=gc7.build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,listaTypes,self.same_col,self.same_ele)
		elif '8Q' in listaKinds:
			print("8Q founded now!!!")
			take_whole_attributes=gc8.build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,self.same_col,self.same_ele)
		elif '9C' in listaKinds:
			print("9C founded now!!!")
			take_whole_attributes=gc9.build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,self.same_col,self.same_ele)
			#tot,tot_1,all_alternative_pos,self.one_ans_is_enough,self.multiple_branch_ans,self.multiple_or_ans, self.touch_together, \\\\
			#self.no_ans_to_create_only_body,self.body_response,self.head_response,self.body_and_patch,self.need_light,self.need_light1, \\\
			#self.need_light2, self.need_light3,self.the_first,self.the_second,self.body_num_press=gc9.build_Resp(patpat_ele,patpat_col, \
			#patpat_gen,listaudios,self.same_col,self.same_ele)
		print("take_whole_attributes {}".format(take_whole_attributes))
		print("length take_whole_attributes {}".format(len(take_whole_attributes)))
		tot=copy.copy(take_whole_attributes[0])
		print(tot)
		tot_1=copy.copy(take_whole_attributes[1])
		print(tot_1)
		all_alternative_pos=copy.copy(take_whole_attributes[2])
		print(all_alternative_pos)
		self.one_ans_is_enough=copy.copy(take_whole_attributes[3])
		print(self.one_ans_is_enough)
		self.multiple_branch_ans=copy.copy(take_whole_attributes[4])
		print(self.multiple_branch_ans)
		self.multiple_or_ans=copy.copy(take_whole_attributes[5])
		print(self.multiple_or_ans)
		self.touch_together=copy.copy(take_whole_attributes[6])
		print(self.touch_together)
		self.no_ans_to_create_only_body=copy.copy(take_whole_attributes[7])
		print(self.no_ans_to_create_only_body)
		self.body_response=copy.copy(take_whole_attributes[8])
		print(self.body_response)
		self.body_and_patch=copy.copy(take_whole_attributes[9])
		print(self.body_and_patch)
		self.need_light=copy.copy(take_whole_attributes[10])
		print(self.need_light)
		self.need_light1=copy.copy(take_whole_attributes[11])
		print(self.need_light1)
		self.need_light2=copy.copy(take_whole_attributes[12])
		print(self.need_light2)
		self.need_light3=copy.copy(take_whole_attributes[13])
		print(self.need_light3)
		self.the_first=copy.copy(take_whole_attributes[14])
		print(self.the_first)
		self.the_second=copy.copy(take_whole_attributes[15])
		print(self.the_second)
		self.body_num_press=copy.copy(take_whole_attributes[16])
		print(self.body_num_press)
		self.all_num_touches_to_do=copy.copy(take_whole_attributes[17])
		print(self.all_num_touches_to_do)
		self.ques_9_num=copy.copy(take_whole_attributes[18])
		print(self.ques_9_num)
		self.limit=copy.copy(take_whole_attributes[19])
		print(self.limit)
		self.limit1=copy.copy(take_whole_attributes[20])
		print(self.limit1)
		self.limit2=copy.copy(take_whole_attributes[21])
		print(self.limit2)
		self.limit3=copy.copy(take_whole_attributes[22])
		print(self.limit3)
		self.limitRed=copy.copy(take_whole_attributes[23])
		print(self.limitRed)
		self.limitGreen=copy.copy(take_whole_attributes[24])
		print(self.limitGreen)
		self.limitBlue=copy.copy(take_whole_attributes[25])
		print(self.limitBlue)
		self.limitYellow=copy.copy(take_whole_attributes[26])
		print(self.limitYellow)
		self.limitLion=copy.copy(take_whole_attributes[27])
		print(self.limitLion)
		self.limitMonkey=copy.copy(take_whole_attributes[28])
		print(self.limitMonkey)
		self.limitParrot=copy.copy(take_whole_attributes[29])
		print(self.limitParrot)
		self.limitOctopus=copy.copy(take_whole_attributes[30])		
		#print("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT")
		#print("IM BACK WHERE I BELONG!!!")
		print("tot => {}".format(tot))
		print("tot_1 => {}".format(tot_1))
		print("all_alternative_pos {}".format(all_alternative_pos))
		length=len(listaudios)

		print("Length of listaudios is  {}".format(length))
		listans1,listans2,listans3,listans4=self.build_answers(length,tot,listans1,listans2,listans3,listans4,all_alternative_pos,self.ques_9_num,self.all_num_touches_to_do)
		if 1 in self.multiple_branch_ans:
			#print("The are two responses here")
			listans11,listans12,listans13,listans14=self.build_answers(length,tot_1,listans11,listans12,listans13,listans14,all_alternative_pos,self.ques_9_num,self.all_num_touches_to_do)
			return listans1,listans2,listans3,listans4,listans11,listans12,listans13,listans14
		return listans1,listans2,listans3,listans4
	
	cpdef create_QeA(self):
		#print('1) remember that the difficulty is {}'.format(self.difficulty_of_game))
		#print(self.scenario.patches[0].genre)
		#print(self.scenario.patches[1].genre)
		#print(self.scenario.patches[2].genre)
		#print(self.scenario.patches[3].genre)
		cdef:
			str group=self.scenario.group_scenario
			str element1=up.convert_subject(self.scenario.patches[0].subject)
			str color1=up.convert_color(self.scenario.patches[0].color)
			str genre1=up.convert_genre(self.scenario.patches[0].genre)
			str element2=up.convert_subject(self.scenario.patches[1].subject)
			str color2=up.convert_color(self.scenario.patches[1].color)
			str genre2=up.convert_genre(self.scenario.patches[1].genre)
			str element3=up.convert_subject(self.scenario.patches[2].subject)
			str color3=up.convert_color(self.scenario.patches[2].color)
			str genre3=up.convert_genre(self.scenario.patches[2].genre)
			str element4=up.convert_subject(self.scenario.patches[3].subject)
			str color4=up.convert_color(self.scenario.patches[3].color)
			str genre4=up.convert_genre(self.scenario.patches[3].genre)
			list listans1=[]
			list listans2=[]
			list listans3=[]
			list listans4=[]
			list listans11=[]
			list listans12=[]
			list listans13=[]
			list listans14=[]
		patpat_ele=[element1,element2,element3,element4]
		patpat_col=[color1,color2,color3,color4]
		patpat_gen=[genre1,genre2,genre3,genre4]
		#print(patpat_ele)
		#print(patpat_col)
		#print(patpat_gen)
		self.current_game_subjects=patpat_ele
		self.current_game_colors=patpat_col
		self.current_game_genres=patpat_gen
		cdef list pats=[]
		cdef list listrequiredtimes=[]
		self.this_kind=up.convert_kind(self.kind_of_game)
		self.this_type=up.convert_type(self.type_of_game)
		#print('kind is {}'.format(self.this_kind))
		#print('type is {}'.format(self.this_type))
		#print('type of variable is {}'.format(type(self.this_type)))
		#print('element1 is {}'.format(element1))
		#print('color1 is {}'.format(color1))
		#print('element2 is {}'.format(element2))
		#print('tipe of element2{} is'.format(type(element2)))
		#print('color2 is {}'.format(color2))
		#print('element3 tipo is {}'.format(type(element3)))
		#print('element3 is {}'.format(element3))
		#print('color3 is {}'.format(color3))
		#print('element4 is {}'.format(element4))
		#print('color4 is {}'.format(color4))
		cdef int length=0
		listaudios=[]
		listaKinds=[]
		listaTypes=[]
		params=(self.id_game)
		sql_select_ava="SELECT question_id,audio,kind,type,description,value \
			FROM Questions JOIN Games_Questions USING (question_id) \
			WHERE game_id=?"
		#with db.DatabaseManager():
		fetched_3=self.db_conn.execute_param_query(sql_select_ava,params)
				print("fetched_3  is {}".format(fetched_3))
		try:
			if fetched_3[0][0]: #if lung=1 --> go to exception!! lung=1
				length=len(fetched_3)
				print("lengthlengthlengthlengthlength :::: {}".format(length))
				#fetched_3=random.sample(fetched_3,len(fetched_3))
				for i in range(len(fetched_3)):
					listaudios.append(fetched_3[i][1])
					listaKinds.append(fetched_3[i][2])
					listaTypes.append(fetched_3[i][3])
		except Exception:
			print("Exception occured while creating QeA")
			length=1
			listaudios.append(fetched_3[1])
			listaKinds.append(fetched_3[2])
			listaTypes.append(fetched_3[3])
		
		print("listaudios")
		print(listaudios)
		print("listaKinds")
		print(listaKinds)
		print("listaTypes")
		print(listaTypes)

		if len(listaudios)!=len(list(set(listaudios))):
			copies_dupl=list(set(listaudios[::2]) & set(listaudios[1::2]))
			primook=0
			for i in range(len(copies_dupl)):
				for k in range(len(listaudios)):
					if listaudios[k]==copies_dupl[i] and primook==0:
						del listaTypes[i]
						del listaKinds[i]
						primook=1
				primook=0	 

			listaudios=copy.copy(list(set(listaudios)))
			copies_dupl=[]
			print("listaudios NEW")
			print(listaudios)
			print("listaKinds NEW")
			print(listaKinds)
			print("listaTypes NEW")
			print(listaTypes)

		print("self.num_questions ADESSO!!! ",self.num_questions)
		if len(listaudios)!=self.num_questions:
			old_quest=copy.copy(self.num_questions)
			print("old_quest --> {}".format(old_quest))
			print("$$$$$$$$ length lower than")
			# Update games num of questions
			self.num_questions=copy.copy(len(listaudios))
			print("99 new. NUM QUEST after modifications {}".format(self.num_questions))
			# Modifying NUM GAMES already present in tables 
			self.db_conn.change_questions_num(self.num_questions, self.id_game)
			# Modifying NUM GAMES IN FILE DB_DEFUALT
			tokenpa=(self.id_game)
			taketoken="SELECT token FROM Games WHERE game_id=?"
			token_sql=self.db_conn.execute_param_query(taketoken,tokenpa)
			print("SQLtoken_sqltoken_sqltoken_sqltoken_sql!!! {}".format(token_sql))
			if len(token_sql)==1:
				print("tokenIF")
				token=token_sql[0]
			else:
				print("tokenELSE")
				token=[item for sublist in token_sql for item in sublist]
				print("temp token {}".format(token))
				token=token[0]
			print("per both!! token!!!! da passare!!! {}".format(token))
			mdl.modify_with_update_in_lines(token, old_quest, self.num_questions)
			print("FINE MODIFICA!!!!")

		if ('2LDSU2' in listaTypes) or ('2LDSUOC' in listaTypes): #('9CNSSU' in listaTypes) or 
			listans1,listans2,listans3,listans4,listans11,listans12,listans13,listans14=self.write_responses(patpat_ele,patpat_col,patpat_gen,listaudios,listaKinds,listaTypes)
		else:
			listans1,listans2,listans3,listans4=self.write_responses(patpat_ele,patpat_col,patpat_gen,listaudios,listaKinds,listaTypes)
		
		listrequiredtimes=[]
		for q in range(len(listaudios)):
			listrequiredtimes.append(5)
			time_tmp=change_required_time(listrequiredtimes[q],5)
			listrequiredtimes[q]=time_tmp
		print("listrequiredtimes listrequiredtimes listrequiredtimes listrequiredtimes {}".format(listrequiredtimes))
		
		#for p in range(len(listaudios)):
		#	print("inside for---------")
		#	print(p)
		#	print(listaudios[p])
		#	audio_tmp=self.extract_question(listaudios[p])
		#	print(audio_tmp)
		#	self.listaudios_int.append(audio_tmp)
		
		for p,v in enumerate(listaudios):
			print("inside for---------")
			print("p", p)
			print("p", v)
			print(listaudios)
			audio_tmp=self.extract_question(v)		
			print(audio_tmp)
			self.listaudios_int.append(audio_tmp)
		print("self.listaudios_intself.listaudios_intself.listaudios_intself.listaudios_intself.listaudios_int {}".format(self.listaudios_int))

		print("RECAP RECAP RECAP RECAP RECAP RECAP RECAP RECAP")
		print(listans1)
		print(listans2)
		print(listans3)
		print(listans4)
		print("RECAP RECAP RECAP RECAP RECAP RECAP RECAP RECAP")
		choose_two_dimensional,choose11_two_dimensional=0,0
		use_d1=0
		use_d2=0
		use_d3=0
		use_d4=0
		use_d12=0
		use_d13=0
		use_d14=0
		use_d23=0
		use_d24=0
		use_d34=0
		use_d123=0
		use_d124=0
		use_d134=0
		use_d234=0
		use_d1234=0
		use11_d1=0
		use11_d2=0
		use11_d3=0
		use11_d4=0
		use11_d12=0
		use11_d13=0
		use11_d14=0
		use11_d23=0
		use11_d24=0
		use11_d34=0
		use11_d123=0
		use11_d124=0
		use11_d134=0
		use11_d234=0
		use11_d1234=0
		#if isnt simple 1dim list but is a list of list ---> so type 9cnssu and multiple_or_ans=true!
		comeback1=self.find_listoflist(listans1)
		comeback2=self.find_listoflist(listans2)
		comeback3=self.find_listoflist(listans3)
		comeback4=self.find_listoflist(listans4)
		print("-------------check returns-------------")
		print(comeback1)
		print(comeback2)
		print(comeback3)
		print(comeback4)
		print("-------------check returns-------------")
		if comeback1:
			#listans1=list(acp2.iterFlatten(listans1))
			use_d1=1
			choose_two_dimensional=1
			print("LA PRIMA should be doubled!! LISTOFLIST")
		if comeback2:
			#listans2=list(acp2.iterFlatten(listans2))
			use_d2=1
			choose_two_dimensional=1
			print("LA SECOND should be doubled!! LISTOFLIST")
		if comeback3:
			#listans3=list(acp2.iterFlatten(listans3))
			use_d3=1
			choose_two_dimensional=1
			print("LA TERZA should be doubled!! LISTOFLIST")
		if comeback4:
			#listans4=list(acp2.iterFlatten(listans4))
			use_d4=1
			choose_two_dimensional=1
			print("LA QUARTA should be doubled!! LISTOFLIST")
		
		if use_d1==1 and use_d2==1 and use_d3==0 and use_d4==0:
			print("it's the case 12")
			use_d1=0
			use_d2=0
			use_d12=1
		elif use_d1==1 and use_d2==0 and use_d3==1 and use_d4==0:
			print("it's the case 13")
			use_d1=0
			use_d3=0
			use_d13=1
		elif use_d1==1 and use_d2==0 and use_d3==0 and use_d4==1:
			print("it's the case 14")
			use_d1=0
			use_d4=0
			use_d14=1
		elif use_d1==0 and use_d2==1 and use_d3==1 and use_d4==0:
			print("it's the case 23")
			use_d2=0
			use_d3=0
			use_d23=1
		elif use_d1==0 and use_d2==1 and use_d3==0 and use_d4==1:
			print("it's the case 24")
			use_d2=0
			use_d4=0
			use_d24=1
		elif use_d1==0 and use_d2==0 and use_d3==1 and use_d4==1:
			print("it's the case 34")
			use_d3=0
			use_d4=0
			use_d34=1
		elif use_d1==1 and use_d2==1 and use_d3==1 and use_d4==0:
			print("it's the case 123")
			use_d1=0
			use_d2=0
			use_d3=0
			use_d123=1
		elif use_d1==1 and use_d2==1 and use_d3==0 and use_d4==1:
			print("it's the case 134")
			use_d1=0
			use_d2=0
			use_d4=0
			use_d134=1
		elif use_d1==1 and use_d2==0 and use_d3==1 and use_d4==1:
			print("it's the case 124")
			use_d1=0
			use_d3=0
			use_d4=0
			use_d134=1
		elif use_d1==0 and use_d2==1 and use_d3==1 and use_d4==1:
			print("it's the case 234")
			use_d2=0
			use_d3=0
			use_d4=0
			use_d234=1
		elif use_d1==1 and use_d2==1 and use_d3==1 and use_d4==1:
			print("it's the case 1234")
			use_d1=0
			use_d2=0
			use_d3=0
			use_d4=0
			use_d1234=1
		audiossarray=np.asarray(self.listaudios_int,dtype=np.int32)
		answers1sarray=np.asarray(listans1,dtype=np.int32)
		answers2sarray=np.asarray(listans2,dtype=np.int32)
		answers3sarray=np.asarray(listans3,dtype=np.int32)
		answers4sarray=np.asarray(listans4,dtype=np.int32)
		timesarray=np.asarray(listrequiredtimes,dtype=np.int32)
		#print("AAAA Arrays")
		#print(audiossarray)
		#print(answers1sarray)
		#print(answers2sarray)
		#print(answers3sarray)
		#print(answers4sarray)
		#print(timesarray)
		#print("AAAA Arrays")
		#with QeA2: call
		#answers=np.asarray(listanswers,dtype=np.int32)
		#timesarray=np.asarray(listrequiredtimes,dtype=np.int32)
		#self.ques_ans=create_current_inquiry(audiossarray,timesarray,answers)
		print("choose_two_dimensional=={}".format(choose_two_dimensional))
		if not choose_two_dimensional:
			self.ques_ans=create_current_inquiry(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)

		else:
			if use_d1==1:
				print("using use_d1")
				self.ques_ans=create_current_inquiry_d1(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)			
			elif use_d2==1:
				print("using use_d2")
				self.ques_ans=create_current_inquiry_d2(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)			
			elif use_d3==1:
				print("using use_d3")
				self.ques_ans=create_current_inquiry_d3(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d4==1:
				print("using use_d4")
				self.ques_ans=create_current_inquiry_d4(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d12==1:
				print("using use_d12")
				self.ques_ans=create_current_inquiry_d12(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d13==1:
				print("using use_d13")
				self.ques_ans=create_current_inquiry_d13(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d14==1:
				print("using use_d14")
				self.ques_ans=create_current_inquiry_d14(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d23==1:
				print("using use_d23")
				self.ques_ans=create_current_inquiry_d23(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d24==1:
				print("using use_d24")
				self.ques_ans=create_current_inquiry_d24(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d34==1:
				print("using use_d34")
				self.ques_ans=create_current_inquiry_d34(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d123==1:
				print("using use_d123")
				self.ques_ans=create_current_inquiry_d123(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d124==1:
				print("using use_d124")
				self.ques_ans=create_current_inquiry_d124(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d134==1:
				print("using use_d134")
				self.ques_ans=create_current_inquiry_d134(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d234==1:
				print("using use_d234")
				self.ques_ans=create_current_inquiry_d234(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			elif use_d1234==1:
				print("using use_d1234")
				self.ques_ans=create_current_inquiry_d1234(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
		print("ques_ansques_ansques_ansques_ans {}".format(self.ques_ans))
		print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
		#try:
		#	print("self.ques_ans[0] {}".format(self.ques_ans[0]))
		#	print("self.ques_ans[1] {}".format(self.ques_ans[1]))
		#	print("self.ques_ans[2] {}".format(self.ques_ans[2]))
		#	print("self.ques_ans[3] {}".format(self.ques_ans[3]))
		#except Exception:
		#	print("Pay attention to Exception printing, segm fault depende da length")
		print("QeA object AUDIO ---> {}".format(self.ques_ans[0].audio))
		print("QeA object ANSWER 1---> {}".format(self.ques_ans[0].answer1))
		print("QeA object ANSWER 2---> {}".format(self.ques_ans[0].answer2))
		print("QeA object ANSWER 3---> {}".format(self.ques_ans[0].answer3))
		print("QeA object ANSWER 4---> {}".format(self.ques_ans[0].answer4))
		new_qea_len=1
		try:
			print("1!!!")
			#print("length QeA objectQeA object QeA ans0{}".format(len(self.ques_ans[0])))
			#print("length QeA objectQeA object QeA ans1{}".format(len(self.ques_ans[1])))
			print("QeA object AUDIO ---> {}".format(self.ques_ans[1].audio))
			print("QeA object ANSWER 1---> {}".format(self.ques_ans[1].answer1))
			print("QeA object ANSWER 2---> {}".format(self.ques_ans[1].answer2))
			print("QeA object ANSWER 3---> {}".format(self.ques_ans[1].answer3))
			print("QeA object ANSWER 4---> {}".format(self.ques_ans[1].answer4))
			new_qea_len=new_qea_len+1
			print("2!!!")
			#print("length QeA objectQeA object QeA 2 {}".format(len(self.ques_ans[2])))
			print("QeA object AUDIO ---> {}".format(self.ques_ans[2].audio))
			print("QeA object ANSWER 1---> {}".format(self.ques_ans[2].answer1))
			print("QeA object ANSWER 2---> {}".format(self.ques_ans[2].answer2))
			print("QeA object ANSWER 3---> {}".format(self.ques_ans[2].answer3))
			print("QeA object ANSWER 4---> {}".format(self.ques_ans[2].answer4))
			new_qea_len=new_qea_len+1
			print("3!!!")
			#print("length QeA objectQeA object QeA 3 {}".format(len(self.ques_ans[3])))
			print("QeA object AUDIO ---> {}".format(self.ques_ans[3].audio))
			print("QeA object ANSWER 1---> {}".format(self.ques_ans[3].answer1))
			print("QeA object ANSWER 2---> {}".format(self.ques_ans[3].answer2))
			print("QeA object ANSWER 3---> {}".format(self.ques_ans[3].answer3))
			print("QeA object ANSWER 4---> {}".format(self.ques_ans[3].answer4))
			new_qea_len=new_qea_len+1

		except Exception:
			print("ce stata un eccezzione madornale it's the case 1 listans1!!!")	
			print("length qea!!! {}".format(new_qea_len))
		print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
		
		if len(listans11): #if there are two list of available answers create also that ! 
			print("Special case, there is one more available answer!")
			#if isnt simple "1dim" list, but is a list of list ---> so type 9cnssu and multiple_or_ans=true!
			comeback11=self.find_listoflist(listans11)
			comeback22=self.find_listoflist(listans12)
			comeback33=self.find_listoflist(listans13)
			comeback44=self.find_listoflist(listans14)
			print("-------------retruns of listans11 case-------------")
			print(comeback11)
			print(comeback22)
			print(comeback33)
			print(comeback44)
			print("-------------retruns of listans11 case-------------")
			
			if comeback11:
				#listans1=list(acp2.iterFlatten(listans1))
				use11_d1=1
				choose11_two_dimensional=1
				print("The first should be doubled!! LISTOFLIST")
			if comeback22:
				#listans2=list(acp2.iterFlatten(listans2))
				use11_d2=1
				choose11_two_dimensional=1
				print("The second should be doubled!! LISTOFLIST")
			if comeback33:
				#listans3=list(acp2.iterFlatten(listans3))
				use11_d3=1
				choose11_two_dimensional=1
				print("The third should be doubled!! LISTOFLIST")
			if comeback44:
				#listans4=list(acp2.iterFlatten(listans4))
				use11_d4=1
				choose11_two_dimensional=1
				print("The fourth should be doubled!! LISTOFLIST")
			
			if use11_d1==1 and use11_d2==1 and use11_d3==0 and use11_d4==0:
				print("it's the case 12")
				use11_d1=0
				use11_d2=0
				use11_d12=1
			elif use11_d1==1 and use11_d2==0 and use11_d3==1 and use11_d4==0:
				print("it's the case 13")
				use11_d1=0
				use11_d3=0
				use11_d13=1
			elif use11_d1==1 and use11_d2==0 and use11_d3==0 and use11_d4==1:
				print("it's the case 14")
				use11_d1=0
				use11_d4=0
				use11_d14=1
			elif use11_d1==0 and use11_d2==1 and use11_d3==1 and use11_d4==0:
				print("it's the case 23")
				use11_d2=0
				use11_d3=0
				use11_d23=1
			elif use11_d1==0 and use11_d2==1 and use11_d3==0 and use11_d4==1:
				print("it's the case 24")
				use11_d2=0
				use11_d4=0
				use11_d24=1
			elif use11_d1==0 and use11_d2==0 and use11_d3==1 and use11_d4==1:
				print("it's the case 34")
				use11_d3=0
				use11_d4=0
				use11_d34=1
			elif use11_d1==1 and use11_d2==1 and use11_d3==1 and use11_d4==0:
				print("it's the case 123")
				use11_d1=0
				use11_d2=0
				use11_d3=0
				use11_d123=1
			elif use11_d1==1 and use11_d2==1 and use11_d3==0 and use11_d4==1:
				print("it's the case 134")
				use11_d1=0
				use11_d2=0
				use11_d4=0
				use11_d134=1
			elif use11_d1==1 and use11_d2==0 and use11_d3==1 and use11_d4==1:
				print("it's the case 124")
				use11_d1=0
				use11_d3=0
				use11_d4=0
				use11_d134=1
			elif use11_d1==0 and use11_d2==1 and use11_d3==1 and use11_d4==1:
				print("it's the case 234")
				use11_d2=0
				use11_d3=0
				use11_d4=0
				use11_d234=1
			elif use11_d1==1 and use11_d2==1 and use11_d3==1 and use11_d4==1:
				print("it's the case 1234")
				use11_d1=0
				use11_d2=0
				use11_d3=0
				use11_d4=0
				use11_d1234=1
			answers11sarray=np.asarray(listans11,dtype=np.int32) 
			answers12sarray=np.asarray(listans12,dtype=np.int32) 
			answers13sarray=np.asarray(listans13,dtype=np.int32)
			answers14sarray=np.asarray(listans14,dtype=np.int32)
			print("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB")
			print(audiossarray)
			print(answers1sarray)
			print(answers2sarray)
			print(answers3sarray)
			print(answers4sarray)
			print(timesarray)
			print("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB")	
			#self.ques_ans_1=create_current_inquiry(audiossarray,timesarray,answers11sarray,answers12sarray,answers13sarray,answers14sarray)
			if not choose11_two_dimensional:
				self.ques_ans_1=create_current_inquiry(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
			else:
				if use11_d1==1:
					print("using use11_d1")
					self.ques_ans_1=create_current_inquiry_d1(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)			
				elif use11_d2==1:
					print("using use11_d2")
					self.ques_ans_1=create_current_inquiry_d2(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)			
				elif use11_d3==1:
					print("using use11_d3")
					self.ques_ans_1=create_current_inquiry_d3(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d4==1:
					print("using use11_d4")
					self.ques_ans_1=create_current_inquiry_d4(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d12==1:
					print("using use11_d12")
					self.ques_ans_1=create_current_inquiry_d12(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d13==1:
					print("using use11_d13")
					self.ques_ans_1=create_current_inquiry_d13(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d14==1:
					print("using use11_d14")
					self.ques_ans_1=create_current_inquiry_d14(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d23==1:
					print("using use11_d23")
					self.ques_ans_1=create_current_inquiry_d23(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d24==1:
					print("using use11_d24")
					self.ques_ans_1=create_current_inquiry_d24(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d34==1:
					print("using use11_d34")
					self.ques_ans_1=create_current_inquiry_d34(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d123==1:
					print("using use11_d123")
					self.ques_ans_1=create_current_inquiry_d123(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d124==1:
					print("using use11_d124")
					self.ques_ans_1=create_current_inquiry_d124(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d134==1:
					print("using use11_d134")
					self.ques_ans_1=create_current_inquiry_d134(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d234==1:
					print("using use11_d234")
					self.ques_ans_1=create_current_inquiry_d234(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)
				elif use11_d1234==1:
					print("using use11_d1234")
					self.ques_ans_1=create_current_inquiry_d1234(audiossarray,timesarray,answers1sarray,answers2sarray,answers3sarray,answers4sarray)

			print("THERE IS ANOTHER QeA THERE IS ANOTHER QeA THERE IS ANOTHER QeA THERE IS ANOTHER QeA THERE IS ANOTHER QeA")
			print("ques_ansques_ansques_ansques_ans {}".format(self.ques_ans_1))
			print("ques_ansques_ansques_ansques_ans {}".format(self.ques_ans))

			print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
			print("QeA object AUDIO ---> {}".format(self.ques_ans[0].audio))
			print("QeA object ANSWER 1---> {}".format(self.ques_ans[0].answer1))
			print("QeA object ANSWER 2---> {}".format(self.ques_ans[0].answer2))
			print("QeA object ANSWER 3---> {}".format(self.ques_ans[0].answer3))
			print("QeA object ANSWER 4---> {}".format(self.ques_ans[0].answer4))
			print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
			new_qea_len=1
			try:
				print("1!!!")
				#print("length QeA objectQeA object QeA 1 {}".format(len(self.ques_ans[1])))
				print("QeA object AUDIO ---> {}".format(self.ques_ans[1].audio))
				print("QeA object ANSWER 1---> {}".format(self.ques_ans[1].answer1))
				print("QeA object ANSWER 2---> {}".format(self.ques_ans[1].answer2))
				print("QeA object ANSWER 3---> {}".format(self.ques_ans[1].answer3))
				print("QeA object ANSWER 4---> {}".format(self.ques_ans[1].answer4))
				new_qea_len=new_qea_len+1
				print("2!!!")
				#print("length QeA objectQeA object QeA 2 {}".format(len(self.ques_ans[2])))
				print("QeA object AUDIO ---> {}".format(self.ques_ans[2].audio))
				print("QeA object ANSWER 1---> {}".format(self.ques_ans[2].answer1))
				print("QeA object ANSWER 2---> {}".format(self.ques_ans[2].answer2))
				print("QeA object ANSWER 3---> {}".format(self.ques_ans[2].answer3))
				print("QeA object ANSWER 4---> {}".format(self.ques_ans[2].answer4))
				new_qea_len=new_qea_len+1
				print("3!!!")
				#print("length QeA objectQeA object QeA 3 {}".format(len(self.ques_ans[3])))
				print("QeA object AUDIO ---> {}".format(self.ques_ans[3].audio))
				print("QeA object ANSWER 1---> {}".format(self.ques_ans[3].answer1))
				print("QeA object ANSWER 2---> {}".format(self.ques_ans[3].answer2))
				print("QeA object ANSWER 3---> {}".format(self.ques_ans[3].answer3))
				print("QeA object ANSWER 4---> {}".format(self.ques_ans[3].answer4))
				new_qea_len=new_qea_len+1
			except Exception:
				print("Exception occured! it's the case 2 listans11!!!")
				print("length QeA!!! {}".format(new_qea_len))

	cpdef select_questions(self):
		self.create_QeA()
	
	cpdef play_the_game(self):
		# print("------------------------------------->>> print again params for this game in PLAY THE GAME ------------------------------------->>>")
		# print(self.one_ans_is_enough)
		# print(self.multiple_branch_ans)
		# print(self.multiple_or_ans)
		# print(self.touch_together)
		# print(self.no_ans_to_create_only_body)
		# print(self.body_response)
		# print(self.body_and_patch)
		# print(self.need_light)
		# print(self.need_light1)
		# print(self.need_light2)
		# print(self.need_light3)
		# print(self.the_first)
		# print(self.the_second)
		# print(self.body_num_press)
		# print(self.all_num_touches_to_do)
		# print(self.ques_9_num)
		# print(self.limit)
		# print(self.limit1)
		# print(self.limit2)
		# print(self.limit3)
		# print(self.limitRed)
		# print(self.limitGreen)
		# print(self.limitBlue)
		# print(self.limitYellow)
		# print(self.limitLion)
		# print(self.limitMonkey)
		# print(self.limitParrot)
		# print(self.limitOctopus)
		everything_well = glp.play_game(self)
		#print("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\Im back from play_game!!!!")
		#print("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\The game was successfully executed??? {}".format(everything_well))

	cpdef check_if_modifications_take_place(self):
		for q in range(4):
			print("PRIMERO!!!! ROUND NUM {}".format(q))
			print("LENGHT results is {}".format(len(self.results)))
			print("self.results[q].set_complexity_sess {}".format(self.results[q].complexity_sess))
			print("self.results[q].set_difficulty_match{}".format(self.results[q].difficulty_match))
			print("self.results[q].set_difficulty_game {}".format(self.results[q].difficulty_game))
			print("self.results[q].set_category {}".format(self.results[q].category))
			print("num_indecisions!! {}".format(self.results[q].num_indecisions))
			print("NEW NEW NUM ERRORI di scoee in array multiple!!!!!! {}".format(self.results[q].num_errors))
			print("NEW NEW NUM CORRETTE di scoc!!!!!! {}".format(self.results[q].num_corrects))

	def extract_question(self, txt):
		converted=''.join(char for char in txt if char.isdigit())
		converted=int(converted)
		print("converted is {}".format(converted))
		return converted
	
	def find_listoflist(self, listans):
		if any(isinstance(el, list) for el in listans):
			return 1
		else: 
			return 0

	def runme_game(self):
		state0=my.PlayGameState("play_game_xyz")
		evt1=fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.BEGIN_GAM, user_data=self.user_dict_game,)
		evt2=fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.GO_TO_MIDDLE_G, user_data=self.user_dict_game,)
		evt3=fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.GO_TO_MIDDLE2_G, user_data=self.user_dict_game,)
		mainfsm=my.OimiFSMGame()
		mainfsm.update_state_fsm(evt1)
		mainfsm.update_state_fsm(evt2)
		mainfsm.update_state_fsm(evt3)

	def preliminals(self):
		print("put here game preliminals as first operation of state machine! ..uneccesary for now.")

	def check_res(self):	
		print("--> CHECK_result!!")
		print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
		print("QeA object AUDIO ---> {}".format(self.ques_ans[0].audio))
		print("QeA object ANSWER 1---> {}".format(self.ques_ans[0].answer1))
		print("QeA object ANSWER 2---> {}".format(self.ques_ans[0].answer2))
		print("QeA object ANSWER 3---> {}".format(self.ques_ans[0].answer3))
		print("QeA object ANSWER 4---> {}".format(self.ques_ans[0].answer4))
		print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")

	cpdef finally_calc_results(self):
		for j,og in enumerate(self.results):
			print("risultati ERRORS {} , {}".format(j,og.num_errors))
			print("risultati CORRECT{} , {}".format(j,og.num_corrects))
			print("risultati INDECISION{} ,{}".format(j,og.num_indecisions))
		
		name_pickle_file = 'game{}_final_tmp_scores_file{}'.format(self.id_game)
		filename_pk = '/home/pi/OIMI/oimi_code/src/playing/methodical_games/game/{}.pk'.format(name_pickle_file)
		with open(filename_pk, 'wb') as fi:
			for j,og in enumerate(self.results):
				pickle.dump(self.og.num_errors, fi)
				pickle.dump(self.og.num_corrects, fi)
				pickle.dump(self.og.num_indecisions, fi)
				pickle.dump(self.tim, fi)