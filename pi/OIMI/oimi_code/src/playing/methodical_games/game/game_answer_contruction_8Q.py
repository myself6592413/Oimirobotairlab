############## KIND 8Q
import faulthandler; faulthandler.enable()

def get_ali_8QTCODB1(pc1,pc2,patpat_col,kidx,no_ans_to_create_only_body,body_response,resp_in_body):
    alitmp1 = [i+1 for i,element in enumerate(patpat_col) if element==pc1 or element==pc2]
    lim=len(alitmp1)
    alitmp2 = [i+1 for i,element in enumerate(patpat_col) if element==pc2]
    lim1=len(alitmp2)
    ali = alitmp1+alitmp2
    limit[kidx]=lim
    limit1[kidx]=lim1
    touch_together[kidx]=1
    return ali,limit,limit1,touch_together

def build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,same_col,same_ele):
    print("WE ARE IN KIND 8Q")
    ali,ali_tmp,ali_1,tot,tot_1 = [],[],[],[],[]
    all_alternative_pos=[0,0,0,0]
    one_ans_is_enough=[0,0,0,0]
    multiple_branch_ans=[0,0,0,0]
    multiple_or_ans=[0,0,0,0]
    touch_together=[0,0,0,0]
    no_ans_to_create_only_body=[0,0,0,0]
    body_response=[0,0,0,0]
    body_and_patch=[0,0,0,0]
    need_light=[0,0,0,0]
    need_light1=[0,0,0,0]
    need_light2=[0,0,0,0]
    need_light3=[0,0,0,0]
    the_first=[0,0,0,0]
    the_second=[0,0,0,0]
    body_num_press=[0,0,0,0]
    all_num_touches_to_do=[0,0,0,0]
    ques_9_num = [0,0,0,0]
    limit = [0,0,0,0]
    limit1 = [0,0,0,0]
    limit2 = [0,0,0,0]
    limit3 = [0,0,0,0]
    limitRed = [0,0,0,0]
    limitGreen = [0,0,0,0]
    limitBlue = [0,0,0,0]
    limitYellow  = [0,0,0,0]
    limitLion = [0,0,0,0]
    limitMonkey = [0,0,0,0]
    limitParrot = [0,0,0,0]
    limitOctopus = [0,0,0,0]
    for k,kaudio in enumerate(listaudios):
    ###8QTCODB1
        if kaudio=='audio_11156':
            no_ans_to_create_only_body[k]=1
            for pacol in patpat_col:
                if pacol=='Red' or pacol=='Blue':
                    body_response[k] = 1
                    resp_in_body[k] = 1 #belly
                    ali.append(11)
        elif kaudio=='audio_11157':
            no_ans_to_create_only_body[k]=1
            for pacol in patpat_col:
                if pacol=='Red' or pacol=='Green':
                    body_response[k] = 1
                    resp_in_body[k] = 1 #belly
                    ali.append(11)
        elif kaudio=='audio_11158':
            no_ans_to_create_only_body[k]=1
            for pacol in patpat_col:
                if pacol=='Red' or pacol=='Yellow':
                    body_response[k] = 1
                    resp_in_body[k] = 1 #belly
                    ali.append(11)
        elif kaudio=='audio_11159':
            no_ans_to_create_only_body[k]=1
            for pacol in patpat_col:
                if pacol=='Green' or pacol=='Blue':
                    body_response[k] = 1
                    resp_in_body[k] = 1 #belly
                    ali.append(11)
        elif kaudio=='audio_11160':
            no_ans_to_create_only_body[k]=1
            for pacol in patpat_col:
                if pacol=='Green' or pacol=='Yellow':
                    body_response[k] = 1
                    resp_in_body[k] = 1 #belly
                    ali.append(11)
        elif kaudio=='audio_11161':
            no_ans_to_create_only_body[k]=1
            for pacol in patpat_col:
                if pacol=='Yellow' or pacol=='Blue':
                    body_response[k] = 1
                    resp_in_body[k] = 1 #belly
                    ali.append(11)
    ###8QTCODB2
        elif kaudio=='audio_11162':
            no_ans_to_create_only_body[k]=1
            if same_col['Green']>=2:
                for pacol in patpat_col:
                    if pacol=='Red':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break       
        elif kaudio=='audio_11163':
            no_ans_to_create_only_body[k]=1
            if same_col['Yellow']>=2:
                for pacol in patpat_col:
                    if pacol=='Red':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break       
        elif kaudio=='audio_11164':
            no_ans_to_create_only_body[k]=1
            if same_col['Blue']>=2:
                for pacol in patpat_col:
                    if pacol=='Red':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break
        elif kaudio=='audio_11165':
            no_ans_to_create_only_body[k]=1
            if same_col['Red']>=2:
                for pacol in patpat_col:
                    if pacol=='Green':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break
        elif kaudio=='audio_11166':
            no_ans_to_create_only_body[k]=1
            if same_col['Yellow']>=2:
                for pacol in patpat_col:
                    if pacol=='Green':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break       
        elif kaudio=='audio_11167':
            no_ans_to_create_only_body[k]=1
            if same_col['Blue']>=2:
                for pacol in patpat_col:
                    if pacol=='Green':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break       
        elif kaudio=='audio_11168':
            no_ans_to_create_only_body[k]=1
            if same_col['Red']>=2:
                for pacol in patpat_col:
                    if pacol=='Blue':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break       
        elif kaudio=='audio_11169':
            no_ans_to_create_only_body[k]=1
            if same_col['Green']>=2:
                for pacol in patpat_col:
                    if pacol=='Blue':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break       
        elif kaudio=='audio_11170':
            no_ans_to_create_only_body[k]=1
            if same_col['Yellow']>=2:
                for pacol in patpat_col:
                    if pacol=='Blue':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break       
        elif kaudio=='audio_11171':
            no_ans_to_create_only_body[k]=1
            if same_col['Red']>=2:
                for pacol in patpat_col:
                    if pacol=='Yellow':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break       
        elif kaudio=='audio_11172':
            no_ans_to_create_only_body[k]=1
            if same_col['Green']>=2:
                for pacol in patpat_col:
                    if pacol=='Yellow':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break       
        elif kaudio=='audio_11173':
            no_ans_to_create_only_body[k]=1
            if same_col['Blue']>=2:
                for pacol in patpat_col:
                    if pacol=='Yellow':
                        body_response[k] = 1
                        resp_in_body[k] = 1 #belly
                        ali.append(11)
                        break   
    ###8QTCODC1
        elif kaudio=='audio_10778':
            if same_col['Red']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10779':
            if same_col['Red']>=1 and same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10780':
            if same_col['Red']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10781':
            if same_col['Green']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10782':
            if same_col['Blue']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10783':
            if same_col['Green']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10784':
            if same_col['Red']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10785':
            if same_col['Red']>=1 and same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10786':
            if same_col['Red']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10787':
            if same_col['Green']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10788':
            if same_col['Blue']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10789':
            if same_col['Green']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10790':
            if same_col['Red']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10791':
            if same_col['Red']>=1 and same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10792':
            if same_col['Red']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10793':
            if same_col['Green']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10794':
            if same_col['Blue']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10795':
            if same_col['Green']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10796':
            if same_col['Red']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10797':
            if same_col['Red']>=1 and same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10798':
            if same_col['Red']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10799':
            if same_col['Green']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10800':
            if same_col['Blue']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10801':
            if same_col['Green']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10802':
            if same_col['Red']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10803':
            if same_col['Red']>=1 and same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10804':
            if same_col['Red']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10805':
            if same_col['Green']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10806':
            if same_col['Blue']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10807':
            if same_col['Green']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10808':
            if same_col['Red']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10809':
            if same_col['Red']>=1 and same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10810':
            if same_col['Red']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10811':
            if same_col['Green']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10812':
            if same_col['Blue']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10813':
            if same_col['Green']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10814':
            if same_col['Red']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10815':
            if same_col['Red']>=1 and same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10816':
            if same_col['Red']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10817':
            if same_col['Green']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10818':
            if same_col['Blue']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10819':
            if same_col['Green']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10820':
            if same_col['Red']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10821':
            if same_col['Red']>=1 and same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10822':
            if same_col['Red']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10823':
            if same_col['Green']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10824':
            if same_col['Blue']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10825':
            if same_col['Green']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10826':
            if same_col['Red']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10827':
            if same_col['Red']>=1 and same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10828':
            if same_col['Red']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10829':
            if same_col['Green']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10830':
            if same_col['Blue']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10831':
            if same_col['Green']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10832':
            if same_col['Red']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10833':
            if same_col['Red']>=1 and same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10834':
            if same_col['Red']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10835':
            if same_col['Green']>=1 and same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10836':
            if same_col['Blue']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10837':
            if same_col['Green']>=1 and same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
    ###8QTCODC2
        elif kaudio=='audio_10838':
            if same_col['Red']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10839':
            if same_col['Red']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10840':
            if same_col['Red']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10841':
            if same_col['Blue']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10842':
            if same_col['Blue']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10843':
            if same_col['Blue']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10844':
            if same_col['Green']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10845':
            if same_col['Green']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10846':
            if same_col['Green']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10847':
            if same_col['Yellow']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10848':
            if same_col['Yellow']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10849':
            if same_col['Yellow']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10850':
            if same_col['Red']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10851':
            if same_col['Red']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10852':
            if same_col['Red']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10853':
            if same_col['Blue']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10854':
            if same_col['Blue']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10855':
            if same_col['Blue']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10856':
            if same_col['Green']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10857':
            if same_col['Green']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10858':
            if same_col['Green']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10859':
            if same_col['Yellow']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10860':
            if same_col['Yellow']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10861':
            if same_col['Yellow']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10862':
            if same_col['Red']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10863':
            if same_col['Red']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10864':
            if same_col['Red']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10865':
            if same_col['Blue']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10866':
            if same_col['Blue']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10867':
            if same_col['Blue']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10868':
            if same_col['Green']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10869':
            if same_col['Green']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10870':
            if same_col['Green']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10871':
            if same_col['Yellow']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10872':
            if same_col['Yellow']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10873':
            if same_col['Yellow']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10874':
            if same_col['Red']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10875':
            if same_col['Red']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10876':
            if same_col['Red']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10877':
            if same_col['Blue']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10878':
            if same_col['Blue']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10879':
            if same_col['Blue']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10880':
            if same_col['Green']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10881':
            if same_col['Green']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10882':
            if same_col['Green']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10883':
            if same_col['Yellow']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10884':
            if same_col['Yellow']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10885':
            if same_col['Yellow']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10886':
            if same_col['Red']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10887':
            if same_col['Red']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10888':
            if same_col['Red']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10889':
            if same_col['Blue']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10890':
            if same_col['Blue']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10891':
            if same_col['Blue']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10892':
            if same_col['Green']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10893':
            if same_col['Green']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10894':
            if same_col['Green']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10895':
            if same_col['Yellow']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10896':
            if same_col['Yellow']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10897':
            if same_col['Yellow']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10898':
            if same_col['Red']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10899':
            if same_col['Red']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10900':
            if same_col['Red']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10901':
            if same_col['Blue']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10902':
            if same_col['Blue']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10903':
            if same_col['Blue']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10904':
            if same_col['Green']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10905':
            if same_col['Green']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10906':
            if same_col['Green']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10907':
            if same_col['Yellow']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10908':
            if same_col['Yellow']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10909':
            if same_col['Yellow']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10910':
            if same_col['Red']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10911':
            if same_col['Red']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10912':
            if same_col['Red']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10913':
            if same_col['Blue']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10914':
            if same_col['Blue']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10915':
            if same_col['Blue']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10916':
            if same_col['Green']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10917':
            if same_col['Green']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10918':
            if same_col['Green']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10919':
            if same_col['Yellow']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10920':
            if same_col['Yellow']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10921':
            if same_col['Yellow']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10922':
            if same_col['Red']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10923':
            if same_col['Red']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10924':
            if same_col['Red']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10925':
            if same_col['Blue']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10926':
            if same_col['Blue']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10927':
            if same_col['Blue']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10928':
            if same_col['Green']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10929':
            if same_col['Green']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10930':
            if same_col['Green']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10931':
            if same_col['Yellow']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10932':
            if same_col['Yellow']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10933':
            if same_col['Yellow']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10934':
            if same_col['Red']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10935':
            if same_col['Red']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10936':
            if same_col['Red']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10937':
            if same_col['Blue']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10938':
            if same_col['Blue']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10939':
            if same_col['Blue']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10940':
            if same_col['Green']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10941':
            if same_col['Green']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10942':
            if same_col['Green']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10943':
            if same_col['Yellow']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10944':
            if same_col['Yellow']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10945':
            if same_col['Yellow']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10946':
            if same_col['Red']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10947':
            if same_col['Red']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10948':
            if same_col['Red']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10949':
            if same_col['Blue']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10950':
            if same_col['Blue']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10951':
            if same_col['Blue']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10952':
            if same_col['Green']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10953':
            if same_col['Green']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10954':
            if same_col['Green']>=1 and same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10955':
            if same_col['Yellow']>=1 and same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10956':
            if same_col['Yellow']>=1 and same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10957':
            if same_col['Yellow']>=1 and same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']         
        ###8QTCOSB1
        elif kaudio=='audio_10978':
            no_ans_to_create_only_body[k]=1
            if same_col['Red']>=1:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10979':
            no_ans_to_create_only_body[k]=1
            if same_col['Green']>=1:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10980':
            no_ans_to_create_only_body[k]=1
            if same_col['Yellow']>=1:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10981':
            no_ans_to_create_only_body[k]=1
            if same_col['Blue']>=1:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        ###8QTCOSB2
        elif kaudio=='audio_10982':
            no_ans_to_create_only_body[k]=1
            if same_col['Red']>=2:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10983':
            no_ans_to_create_only_body[k]=1
            if same_col['Green']>=2:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10984':
            no_ans_to_create_only_body[k]=1
            if same_col['Yellow']>=2:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10985':
            no_ans_to_create_only_body[k]=1
            if same_col['Blue']>=2:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        ###8QTCOSC1
        elif kaudio=='audio_9171':
            if same_col['Red']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9172':
            if same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9173':
            if same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9174':
            if same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9175':
            if same_col['Red']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9176':
            if same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9177':
            if same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9178':
            if same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey'] 
        elif kaudio=='audio_9179':
            if same_col['Red']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9180':
            if same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9181':
            if same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9182':
            if same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9183':
            if same_col['Red']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9184':
            if same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9185':
            if same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9186':
            if same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9187':
            if same_col['Red']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9188':
            if same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9189':
            if same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9190':
            if same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9191':
            if same_col['Red']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9192':
            if same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9193':
            if same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9194':
            if same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9195':
            if same_col['Red']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9196':
            if same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9197':
            if same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9198':
            if same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9199':
            if same_col['Red']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9200':
            if same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9201':
            if same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9202':
            if same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9203':
            if same_col['Red']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9204':
            if same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9205':
            if same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9206':
            if same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9207':
            if same_col['Red']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9208':
            if same_col['Green']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9209':
            if same_col['Yellow']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9210':
            if same_col['Blue']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        ###8QTCOSC2
        elif kaudio=='audio_9211':
            if same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion'] 
        elif kaudio=='audio_9212':
            if same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9213':
            if same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion'] 
        elif kaudio=='audio_9214':
            if same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion'] 
        elif kaudio=='audio_9215':
            if same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey'] 
        elif kaudio=='audio_9216':
            if same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9217':
            if same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey'] 
        elif kaudio=='audio_9218':
            if same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9219':
            if same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot'] 
        elif kaudio=='audio_9220':
            if same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9221':
            if same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot'] 
        elif kaudio=='audio_9222':
            if same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot'] 
        elif kaudio=='audio_9223':
            if same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus'] 
        elif kaudio=='audio_9224':
            if same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9225':
            if same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus'] 
        elif kaudio=='audio_9226':
            if same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus'] 
        elif kaudio=='audio_9227':
            if same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream'] 
        elif kaudio=='audio_9228':
            if same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9229':
            if same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream'] 
        elif kaudio=='audio_9230':
            if same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream'] 
        elif kaudio=='audio_9231':
            if same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses'] 
        elif kaudio=='audio_9232':
            if same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9233':
            if same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses'] 
        elif kaudio=='audio_9234':
            if same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses'] 
        elif kaudio=='audio_9235':
            if same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle'] 
        elif kaudio=='audio_9236':
            if same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9237':
            if same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle'] 
        elif kaudio=='audio_9238':
            if same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle'] 
        elif kaudio=='audio_9239':
            if same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter'] 
        elif kaudio=='audio_9240':
            if same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9241':
            if same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter'] 
        elif kaudio=='audio_9242':
            if same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter'] 
        elif kaudio=='audio_9243':
            if same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella'] 
        elif kaudio=='audio_9244':
            if same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9245':
            if same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella'] 
        elif kaudio=='audio_9246':
            if same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella'] 
        elif kaudio=='audio_9247':
            if same_col['Red']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl'] 
        elif kaudio=='audio_9248':
            if same_col['Green']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9249':
            if same_col['Yellow']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl'] 
        elif kaudio=='audio_9250':
            if same_col['Blue']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
    ###8QTKNDC
        elif kaudio=='audio_9491':
            if same_ele['lion']>0 or same_ele['monkey']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9492':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9493':
            if same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9494':
            if same_ele['monkey']>0 or same_ele['monkey']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9495':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9496':
            if same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9497':
            if same_ele['parrot']>0 or same_ele['parrot']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9498':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9499':
            if same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9500':
            if same_ele['octopus']>0 or same_ele['octopus']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9501':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9502':
            if same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9503':
            if same_ele['icecream']>0 or same_ele['icecream']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9504':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9505':
            if same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9506':
            if same_ele['glasses']>0 or same_ele['glasses']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9507':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9508':
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9509':
            if same_ele['push_scooter']>0 or same_ele['push_scooter']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9510':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9511':
            if same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']         
        elif kaudio=='audio_9512':
            if same_ele['bicycle']>0 or same_ele['bicycle']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9513':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9514':
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9515':
            if same_ele['girl']>0 or same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9516':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9517':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
    ###8QTKNSB
        elif kaudio=='audio_10986':
            no_ans_to_create_only_body[k] = 1
            if same_ele['icecream']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10987':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10988':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10989':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['glasses']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10990':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['bicycle']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10991':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['umbrella']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10992':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['umbrella']>0 or same_ele['bicycle']>0 or same_ele['glasses'] or same_ele['push_scooter']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10993':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['girl']>0 or same_ele['parrot']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10994':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['lion']>0 or same_ele['monkey']>0 or same_ele['octopus']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10995':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['parrot']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10996':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['girl']>0 or same_ele['lion']>0 or same_ele['monkey']>0 or same_ele['octopus']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10997':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10998':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['monkey']>0 or same_ele['girl']>0 or same_ele['lion']>0 or same_ele['octopus']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10999':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['girl']>0 or same_ele['octopus']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11000':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['parrot']>0 or same_ele['monkey']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11001':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['lion']>0 or same_ele['monkey']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11002':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['lion']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11003':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['parrot']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11004':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['parrot']>0 or same_ele['girl']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11005':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['lion']>0 or same_ele['octopus']>0 or same_ele['monkey']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11006':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11007':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['lion']>0 or same_ele['girl']>0 or same_ele['monkey']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11008':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['parrot']>0 or same_ele['monkey']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11009':  
            no_ans_to_create_only_body[k] = 1
            if same_ele['girl']>0 or same_ele['monkey']>0:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
    ###8QTKNSC
        elif kaudio=='audio_9251':
            if same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9252':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9253':   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9254':                   
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']                 
        elif kaudio=='audio_9255':   
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']                 
        elif kaudio=='audio_9256':   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9257':   
            if same_ele['umbrella']>0 or same_ele['bicycle']>0 or same_ele['glasses'] or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9258':                   
            if same_ele['girl']>0 or same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9259':   
            if same_ele['lion']>0 or same_ele['monkey']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9260':                   
            if same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9261':                   
            if same_ele['girl']>0 or same_ele['lion']>0 or same_ele['monkey']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9262':                   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9263':                   
            if same_ele['monkey']>0 or same_ele['girl']>0 or same_ele['lion']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9264':                   
            if same_ele['girl']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9265':                   
            if same_ele['parrot']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9266':
            if same_ele['lion']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9267':
            if same_ele['lion']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9268':                   
            if same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9269':                   
            if same_ele['parrot']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9270':                   
            if same_ele['lion']>0 or same_ele['octopus']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9271':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9272':   
            if same_ele['lion']>0 or same_ele['girl']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9273':                   
            if same_ele['parrot']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9274':                   
            if same_ele['girl']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9275':
            if same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9276':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9277':   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9278':                   
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']                 
        elif kaudio=='audio_9279':   
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']                 
        elif kaudio=='audio_9280':   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9281':   
            if same_ele['umbrella']>0 or same_ele['bicycle']>0 or same_ele['glasses'] or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9282':                   
            if same_ele['girl']>0 or same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9283':   
            if same_ele['monkey']>0 or same_ele['monkey']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9284':                   
            if same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9285':                   
            if same_ele['girl']>0 or same_ele['monkey']>0 or same_ele['monkey']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9286':                   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9287':                   
            if same_ele['monkey']>0 or same_ele['girl']>0 or same_ele['monkey']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9288':                   
            if same_ele['girl']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9289':                   
            if same_ele['parrot']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9290':
            if same_ele['monkey']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9291':
            if same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9292':                   
            if same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9293':                   
            if same_ele['parrot']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9294':                   
            if same_ele['monkey']>0 or same_ele['octopus']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9295':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9296':   
            if same_ele['monkey']>0 or same_ele['girl']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9297':                   
            if same_ele['parrot']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9298':                   
            if same_ele['girl']>0 or same_ele['monkey']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9299':
            if same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9300':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9301':   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9302':                   
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']                 
        elif kaudio=='audio_9303':   
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']                 
        elif kaudio=='audio_9304':   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9305':   
            if same_ele['umbrella']>0 or same_ele['bicycle']>0 or same_ele['glasses'] or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9306':                   
            if same_ele['girl']>0 or same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9307':   
            if same_ele['parrot']>0 or same_ele['parrot']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9308':                   
            if same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9309':                   
            if same_ele['girl']>0 or same_ele['parrot']>0 or same_ele['parrot']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9310':                   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9311':                   
            if same_ele['parrot']>0 or same_ele['girl']>0 or same_ele['parrot']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9312':                   
            if same_ele['girl']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9313':                   
            if same_ele['parrot']>0 or same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9314':
            if same_ele['parrot']>0 or same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9315':
            if same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9316':                   
            if same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9317':                   
            if same_ele['parrot']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9318':                   
            if same_ele['parrot']>0 or same_ele['octopus']>0 or same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9319':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9320':   
            if same_ele['parrot']>0 or same_ele['girl']>0 or same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9321':                   
            if same_ele['parrot']>0 or same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9322':                   
            if same_ele['girl']>0 or same_ele['parrot']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9323':
            if same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9324':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9325':   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9326':                   
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']                 
        elif kaudio=='audio_9327':   
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']                 
        elif kaudio=='audio_9328':   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9329':   
            if same_ele['umbrella']>0 or same_ele['bicycle']>0 or same_ele['glasses'] or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9330':                   
            if same_ele['girl']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9331':   
            if same_ele['octopus']>0 or same_ele['octopus']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9332':                   
            if same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9333':                   
            if same_ele['girl']>0 or same_ele['octopus']>0 or same_ele['octopus']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9334':                   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9335':                   
            if same_ele['octopus']>0 or same_ele['girl']>0 or same_ele['octopus']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9336':                   
            if same_ele['girl']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9337':                   
            if same_ele['octopus']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9338':
            if same_ele['octopus']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9339':
            if same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9340':                   
            if same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9341':                   
            if same_ele['octopus']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9342':                   
            if same_ele['octopus']>0 or same_ele['octopus']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9343':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9344':   
            if same_ele['octopus']>0 or same_ele['girl']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9345':                   
            if same_ele['octopus']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9346':                   
            if same_ele['girl']>0 or same_ele['octopus']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9347':
            if same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9348':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9349':   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9350':                   
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']                 
        elif kaudio=='audio_9351':   
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']                 
        elif kaudio=='audio_9352':   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9353':   
            if same_ele['umbrella']>0 or same_ele['bicycle']>0 or same_ele['glasses'] or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9354':                   
            if same_ele['girl']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9355':   
            if same_ele['icecream']>0 or same_ele['icecream']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9356':                   
            if same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9357':                   
            if same_ele['girl']>0 or same_ele['icecream']>0 or same_ele['icecream']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9358':                   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9359':                   
            if same_ele['icecream']>0 or same_ele['girl']>0 or same_ele['icecream']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9360':                   
            if same_ele['girl']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9361':                   
            if same_ele['icecream']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9362':
            if same_ele['icecream']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9363':
            if same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9364':                   
            if same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9365':                   
            if same_ele['icecream']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9366':                   
            if same_ele['icecream']>0 or same_ele['icecream']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9367':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9368':   
            if same_ele['icecream']>0 or same_ele['girl']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9369':                   
            if same_ele['icecream']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9370':                   
            if same_ele['girl']>0 or same_ele['icecream']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9371':
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9372':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9373':   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9374':                   
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']                 
        elif kaudio=='audio_9375':   
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']                 
        elif kaudio=='audio_9376':   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9377':   
            if same_ele['umbrella']>0 or same_ele['bicycle']>0 or same_ele['glasses'] or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9378':                   
            if same_ele['girl']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9379':   
            if same_ele['glasses']>0 or same_ele['glasses']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9380':                   
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9381':                   
            if same_ele['girl']>0 or same_ele['glasses']>0 or same_ele['glasses']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9382':                   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9383':                   
            if same_ele['glasses']>0 or same_ele['girl']>0 or same_ele['glasses']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9384':                   
            if same_ele['girl']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9385':                   
            if same_ele['glasses']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9386':
            if same_ele['glasses']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9387':
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9388':                   
            if same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9389':                   
            if same_ele['glasses']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9390':                   
            if same_ele['glasses']>0 or same_ele['glasses']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9391':                   
            if same_ele['glasses']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9392':   
            if same_ele['glasses']>0 or same_ele['girl']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9393':                   
            if same_ele['glasses']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9394':                   
            if same_ele['girl']>0 or same_ele['glasses']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9395':
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9396':                   
            if same_ele['bicycle']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9397':   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9398':                   
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']                 
        elif kaudio=='audio_9399':   
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']                 
        elif kaudio=='audio_9400':   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9401':   
            if same_ele['umbrella']>0 or same_ele['bicycle']>0 or same_ele['bicycle'] or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9402':                   
            if same_ele['girl']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9403':   
            if same_ele['bicycle']>0 or same_ele['bicycle']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9404':                   
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9405':                   
            if same_ele['girl']>0 or same_ele['bicycle']>0 or same_ele['bicycle']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9406':                   
            if same_ele['bicycle']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9407':                   
            if same_ele['bicycle']>0 or same_ele['girl']>0 or same_ele['bicycle']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9408':                   
            if same_ele['girl']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9409':                   
            if same_ele['bicycle']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9410':
            if same_ele['bicycle']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9411':
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9412':                   
            if same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9413':                   
            if same_ele['bicycle']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9414':                   
            if same_ele['bicycle']>0 or same_ele['bicycle']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9415':                   
            if same_ele['bicycle']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9416':   
            if same_ele['bicycle']>0 or same_ele['girl']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9417':                   
            if same_ele['bicycle']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9418':                   
            if same_ele['girl']>0 or same_ele['bicycle']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9419':
            if same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9420':                   
            if same_ele['push_scooter']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9421':   
            if same_ele['push_scooter']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9422':                   
            if same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']                 
        elif kaudio=='audio_9423':   
            if same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']                 
        elif kaudio=='audio_9424':   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9425':   
            if same_ele['umbrella']>0 or same_ele['push_scooter']>0 or same_ele['push_scooter'] or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9426':                   
            if same_ele['girl']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9427':   
            if same_ele['push_scooter']>0 or same_ele['push_scooter']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9428':                   
            if same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9429':                   
            if same_ele['girl']>0 or same_ele['push_scooter']>0 or same_ele['push_scooter']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9430':                   
            if same_ele['push_scooter']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9431':                   
            if same_ele['push_scooter']>0 or same_ele['girl']>0 or same_ele['push_scooter']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9432':                   
            if same_ele['girl']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9433':                   
            if same_ele['push_scooter']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9434':
            if same_ele['push_scooter']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9435':
            if same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9436':                   
            if same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9437':                   
            if same_ele['push_scooter']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9438':                   
            if same_ele['push_scooter']>0 or same_ele['push_scooter']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9439':                   
            if same_ele['push_scooter']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9440':   
            if same_ele['push_scooter']>0 or same_ele['girl']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9441':                   
            if same_ele['push_scooter']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9442':                   
            if same_ele['girl']>0 or same_ele['push_scooter']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9443':
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9444':                   
            if same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9445':   
            if same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9446':                   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']                 
        elif kaudio=='audio_9447':   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']                 
        elif kaudio=='audio_9448':   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9449':   
            if same_ele['umbrella']>0 or same_ele['umbrella']>0 or same_ele['umbrella'] or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9450':                   
            if same_ele['girl']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9451':   
            if same_ele['umbrella']>0 or same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9452':                   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9453':                   
            if same_ele['girl']>0 or same_ele['umbrella']>0 or same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9454':                   
            if same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9455':                   
            if same_ele['umbrella']>0 or same_ele['girl']>0 or same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9456':                   
            if same_ele['girl']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9457':                   
            if same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9458':
            if same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9459':
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9460':                   
            if same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9461':                   
            if same_ele['umbrella']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9462':                   
            if same_ele['umbrella']>0 or same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9463':                   
            if same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9464':   
            if same_ele['umbrella']>0 or same_ele['girl']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9465':                   
            if same_ele['umbrella']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9466':                   
            if same_ele['girl']>0 or same_ele['umbrella']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9467':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9468':                   
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9469':   
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9470':                   
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']                 
        elif kaudio=='audio_9471':   
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']                 
        elif kaudio=='audio_9472':   
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9473':   
            if same_ele['girl']>0 or same_ele['girl']>0 or same_ele['girl'] or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9474':                   
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9475':   
            if same_ele['girl']>0 or same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9476':                   
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9477':                   
            if same_ele['girl']>0 or same_ele['girl']>0 or same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9478':                   
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9479':                   
            if same_ele['girl']>0 or same_ele['girl']>0 or same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9480':                   
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9481':                   
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9482':
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9483':
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9484':                   
            if same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9485':                   
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9486':                   
            if same_ele['girl']>0 or same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9487':                   
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9488':   
            if same_ele['girl']>0 or same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9489':                   
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9490':                   
            if same_ele['girl']>0 or same_ele['girl']>0:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
    ###8QTPRSB
        elif kaudio=='audio_11010':
            if same_ele['lion']<2:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)  
        elif kaudio=='audio_11011':
            if same_ele['monkey']<2:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11012':
            if same_ele['parrot']<2:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11013':
            if same_ele['octopus']<2:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11014':
            if same_ele['icecream']<2:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11015':
            if same_ele['glasses']<2:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11016':
            if same_ele['bicycle']<2:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11017':
            if same_ele['push_scooter']<2:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11018':
            if same_ele['umbrella']<2:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11019':
            if same_ele['girl']<2:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11020':
            if same_ele['lion']>1:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly 
                ali.append(11)      
        elif kaudio=='audio_11021':
            if same_ele['monkey']>1:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11022':
            if same_ele['parrot']>1:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11023':
            if same_ele['octopus']>1:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11024':
            if same_ele['icecream']>1:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11025':
            if same_ele['glasses']>1:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11026':
            if same_ele['bicycle']>1:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11027':
            if same_ele['push_scooter']>1:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11028':
            if same_ele['umbrella']>1:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11029':
            if same_ele['girl']>1:
                body_response[k] = 1
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
    ###8QTPRSC
        elif kaudio=='audio_9518':
            if same_ele['monkey']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9519':
            if same_ele['parrot']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9520':
            if same_ele['octopus']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9521':
            if same_ele['icecream']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9522':
            if same_ele['glasses']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9523':
            if same_ele['bicycle']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9524':
            if same_ele['push_scooter']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9525':
            if same_ele['umbrella']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9526':
            if same_ele['girl']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion'] 
        elif kaudio=='audio_9527':
            if same_ele['lion']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9528':
            if same_ele['parrot']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9529':
            if same_ele['octopus']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9530':
            if same_ele['icecream']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9531':
            if same_ele['glasses']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9532':
            if same_ele['bicycle']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9533':
            if same_ele['push_scooter']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9534':
            if same_ele['umbrella']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9535':
            if same_ele['girl']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9536':
            if same_ele['lion']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9537':
            if same_ele['monkey']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9538':
            if same_ele['octopus']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9539':
            if same_ele['icecream']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9540':
            if same_ele['glasses']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9541':
            if same_ele['bicycle']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9542':
            if same_ele['push_scooter']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9543':
            if same_ele['umbrella']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9544':
            if same_ele['girl']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']         
        elif kaudio=='audio_9545':
            if same_ele['lion']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9546':
            if same_ele['monkey']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9547':
            if same_ele['parrot']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9548':
            if same_ele['icecream']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9549':
            if same_ele['glasses']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9550':
            if same_ele['bicycle']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9551':
            if same_ele['push_scooter']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9552':
            if same_ele['umbrella']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9553':
            if same_ele['girl']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9554':
            if same_ele['lion']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9555':
            if same_ele['monkey']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9556':
            if same_ele['parrot']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9557':
            if same_ele['octopus']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9558':
            if same_ele['glasses']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9559':
            if same_ele['bicycle']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9560':
            if same_ele['push_scooter']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9561':
            if same_ele['umbrella']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9562':
            if same_ele['girl']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9563':
            if same_ele['lion']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9564':
            if same_ele['monkey']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9565':
            if same_ele['parrot']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9566':
            if same_ele['octopus']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9567':
            if same_ele['icecream']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9568':
            if same_ele['bicycle']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9569':
            if same_ele['push_scooter']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9570':
            if same_ele['umbrella']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9571':
            if same_ele['girl']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9572':
            if same_ele['lion']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9573':
            if same_ele['monkey']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9574':
            if same_ele['parrot']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9575':
            if same_ele['octopus']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9576':
            if same_ele['icecream']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9577':
            if same_ele['glasses']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9578':
            if same_ele['push_scooter']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9579':
            if same_ele['umbrella']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9580':
            if same_ele['girl']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9581':
            if same_ele['lion']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9582':
            if same_ele['monkey']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9583':
            if same_ele['parrot']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9584':
            if same_ele['octopus']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9585':
            if same_ele['icecream']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9586':
            if same_ele['glasses']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9587':
            if same_ele['bicycle']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9588':
            if same_ele['umbrella']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9589':
            if same_ele['girl']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9590':
            if same_ele['lion']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9591':
            if same_ele['monkey']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9592':
            if same_ele['parrot']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9593':
            if same_ele['octopus']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9594':
            if same_ele['icecream']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9595':
            if same_ele['glasses']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9596':
            if same_ele['bicycle']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9597':
            if same_ele['push_scooter']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9598':
            if same_ele['girl']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']                         
        elif kaudio=='audio_9599':
            if same_ele['lion']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9600':
            if same_ele['monkey']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9601':
            if same_ele['parrot']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9602':
            if same_ele['octopus']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9603':
            if same_ele['icecream']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9604':
            if same_ele['glasses']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9605':
            if same_ele['bicycle']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9606':
            if same_ele['push_scooter']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9607':
            if same_ele['umbrella']<2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9608':
            if same_ele['monkey']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9609':
            if same_ele['parrot']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9610':
            if same_ele['octopus']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9611':
            if same_ele['icecream']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9612':
            if same_ele['glasses']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9613':
            if same_ele['bicycle']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9614':
            if same_ele['push_scooter']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9615':
            if same_ele['umbrella']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9616':
            if same_ele['girl']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion'] 
        elif kaudio=='audio_9617':
            if same_ele['lion']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9618':
            if same_ele['parrot']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9619':
            if same_ele['octopus']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9620':
            if same_ele['icecream']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9621':
            if same_ele['glasses']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9622':
            if same_ele['bicycle']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9623':
            if same_ele['push_scooter']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9624':
            if same_ele['umbrella']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9625':
            if same_ele['girl']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9626':
            if same_ele['lion']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9627':
            if same_ele['monkey']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9628':
            if same_ele['octopus']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9629':
            if same_ele['icecream']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9630':
            if same_ele['glasses']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9631':
            if same_ele['bicycle']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9632':
            if same_ele['push_scooter']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9633':
            if same_ele['umbrella']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9634':
            if same_ele['girl']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']         
        elif kaudio=='audio_9635':
            if same_ele['lion']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9636':
            if same_ele['monkey']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9637':
            if same_ele['parrot']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9638':
            if same_ele['icecream']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9639':
            if same_ele['glasses']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9640':
            if same_ele['bicycle']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9641':
            if same_ele['push_scooter']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9642':
            if same_ele['umbrella']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9643':
            if same_ele['girl']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9644':
            if same_ele['lion']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9645':
            if same_ele['monkey']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9646':
            if same_ele['parrot']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9647':
            if same_ele['octopus']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9648':
            if same_ele['glasses']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9649':
            if same_ele['bicycle']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9650':
            if same_ele['push_scooter']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9651':
            if same_ele['umbrella']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9652':
            if same_ele['girl']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9653':
            if same_ele['lion']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9654':
            if same_ele['monkey']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9655':
            if same_ele['parrot']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9656':
            if same_ele['octopus']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9657':
            if same_ele['icecream']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9658':
            if same_ele['bicycle']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9659':
            if same_ele['push_scooter']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9660':
            if same_ele['umbrella']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9661':
            if same_ele['girl']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9662':
            if same_ele['lion']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9663':
            if same_ele['monkey']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9664':
            if same_ele['parrot']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9665':
            if same_ele['octopus']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9666':
            if same_ele['icecream']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9667':
            if same_ele['glasses']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9668':
            if same_ele['push_scooter']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9669':
            if same_ele['umbrella']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9670':
            if same_ele['girl']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9671':
            if same_ele['lion']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9672':
            if same_ele['monkey']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9673':
            if same_ele['parrot']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9674':
            if same_ele['octopus']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9675':
            if same_ele['icecream']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9676':
            if same_ele['glasses']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9677':
            if same_ele['bicycle']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9678':
            if same_ele['umbrella']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9679':
            if same_ele['girl']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9680':
            if same_ele['lion']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9681':
            if same_ele['monkey']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9682':
            if same_ele['parrot']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9683':
            if same_ele['octopus']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9684':
            if same_ele['icecream']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9685':
            if same_ele['glasses']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9686':
            if same_ele['bicycle']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9687':
            if same_ele['push_scooter']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9688':
            if same_ele['girl']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']                         
        elif kaudio=='audio_9689':
            if same_ele['lion']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9690':
            if same_ele['monkey']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9691':
            if same_ele['parrot']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9692':
            if same_ele['octopus']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9693':
            if same_ele['icecream']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9694':
            if same_ele['glasses']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9695':
            if same_ele['bicycle']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9696':
            if same_ele['push_scooter']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9697':
            if same_ele['umbrella']>1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
    ###8QTSUDB1
        elif kaudio=='audio_11030':
            if same_ele['lion']>=1 and same_ele['monkey']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11031':
            if same_ele['lion']>=1 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11032':
            if same_ele['lion']>=1 and same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11033':
            if same_ele['lion']>=1 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11034':
            if same_ele['lion']>=1 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11035':
            if same_ele['lion']>=1 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11036':
            if same_ele['lion']>=1 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11037':
            if same_ele['lion']>=1 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11038':
            if same_ele['lion']>=1 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11039':
            if same_ele['monkey']>=1 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11040':
            if same_ele['monkey']>=1 and same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11041':
            if same_ele['monkey']>=1 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11042':
            if same_ele['monkey']>=1 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11043':
            if same_ele['monkey']>=1 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11044':
            if same_ele['monkey']>=1 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11045':
            if same_ele['monkey']>=1 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11046':
            if same_ele['monkey']>=1 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11047':
            if same_ele['parrot']>=1 and same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11048':
            if same_ele['parrot']>=1 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11049':
            if same_ele['parrot']>=1 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11050':
            if same_ele['parrot']>=1 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1   
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11051':
            if same_ele['parrot']>=1 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11052':
            if same_ele['parrot']>=1 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11053':
            if same_ele['parrot']>=1 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11054':
            if same_ele['octopus']>=1 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11055':
            if same_ele['octopus']>=1 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11056':
            if same_ele['octopus']>=1 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1   
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11057':
            if same_ele['octopus']>=1 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11058':
            if same_ele['octopus']>=1 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11059':
            if same_ele['octopus']>=1 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11060':
            if same_ele['icecream']>=1 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11061':
            if same_ele['icecream']>=1 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1   
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11062':
            if same_ele['icecream']>=1 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11063':
            if same_ele['icecream']>=1 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11064':
            if same_ele['icecream']>=1 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11065':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11066':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11067':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11068':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11069':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11070':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11071':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1   
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11072':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11073':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11074':
            if same_ele['umbrella']>=1 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        ###8QTSUDB2
        elif kaudio=='audio_11075':
            if same_ele['lion']>=2 and same_ele['monkey']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11076':
            if same_ele['lion']>=2 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11077':
            if same_ele['lion']>=2 and same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11078':
            if same_ele['lion']>=2 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11079':
            if same_ele['lion']>=2 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11080':
            if same_ele['lion']>=2 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1                       
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11081':
            if same_ele['lion']>=2 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11082':
            if same_ele['lion']>=2 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11083':
            if same_ele['lion']>=2 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11084':
            if same_ele['monkey']>=2 and same_ele['lion']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11085':
            if same_ele['monkey']>=2 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11086':
            if same_ele['monkey']>=2 and same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11087':
            if same_ele['monkey']>=2 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
                body_response[k] = 1
        elif kaudio=='audio_11088':
            if same_ele['monkey']>=2 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11089':
            if same_ele['monkey']>=2 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1                       
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11090':
            if same_ele['monkey']>=2 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11091':
            if same_ele['monkey']>=2 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11092':
            if same_ele['monkey']>=2 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11093':
            if same_ele['parrot']>=2 and same_ele['monkey']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11094':
            if same_ele['parrot']>=2 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11095':
            if same_ele['parrot']>=2 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11096':
            if same_ele['parrot']>=2 and same_ele['lion']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11097':
            if same_ele['parrot']>=2 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11098':
            if same_ele['parrot']>=2 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1                       
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11099':
            if same_ele['parrot']>=2 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11100':
            if same_ele['parrot']>=2 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11101':
            if same_ele['parrot']>=2 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11102':
            if same_ele['octopus']>=2 and same_ele['monkey']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11103':
            if same_ele['octopus']>=2 and same_ele['lion']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11104':
            if same_ele['octopus']>=2 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11105':
            if same_ele['octopus']>=2 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11106':
            if same_ele['octopus']>=2 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11107':
            if same_ele['octopus']>=2 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1                       
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11108':
            if same_ele['octopus']>=2 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11109':
            if same_ele['octopus']>=2 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11110':
            if same_ele['octopus']>=2 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11111':
            if same_ele['icecream']>=2 and same_ele['monkey']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11112':
            if same_ele['icecream']>=2 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11113':
            if same_ele['icecream']>=2 and same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)              
        elif kaudio=='audio_11114':
            if same_ele['icecream']>=2 and same_ele['lion']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11115':
            if same_ele['icecream']>=2 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11116':
            if same_ele['icecream']>=2 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1                       
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11117':
            if same_ele['icecream']>=2 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11118':
            if same_ele['icecream']>=2 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11119':
            if same_ele['icecream']>=2 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11120':
            if same_ele['glasses']>=2 and same_ele['monkey']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11121':
            if same_ele['glasses']>=2 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11122':
            if same_ele['glasses']>=2 and same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11123':
            if same_ele['glasses']>=2 and same_ele['lion']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11124':
            if same_ele['glasses']>=2 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11125':
            if same_ele['glasses']>=2 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1                       
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11126':
            if same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11127':
            if same_ele['glasses']>=2 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11128':
            if same_ele['glasses']>=2 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11129':
            if same_ele['bicycle']>=2 and same_ele['monkey']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
        elif kaudio=='audio_11130':
            if same_ele['bicycle']>=2 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11131':
            if same_ele['bicycle']>=2 and same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11132':
            if same_ele['bicycle']>=2 and same_ele['lion']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11133':
            if same_ele['bicycle']>=2 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11134':
            if same_ele['bicycle']>=2 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1                       
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11135':
            if same_ele['bicycle']>=2 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11136':
            if same_ele['bicycle']>=2 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11137':
            if same_ele['bicycle']>=2 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11138':
            if same_ele['push_scooter']>=2 and same_ele['monkey']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11139':
            if same_ele['push_scooter']>=2 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11140':
            if same_ele['push_scooter']>=2 and same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11141':
            if same_ele['push_scooter']>=2 and same_ele['lion']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11142':
            if same_ele['push_scooter']>=2 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11143':
            if same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1                       
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11144':
            if same_ele['push_scooter']>=2 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11145':
            if same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11146':
            if same_ele['push_scooter']>=2 and same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11147':
            if same_ele['girl']>=2 and same_ele['monkey']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11148':
            if same_ele['girl']>=2 and same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11149':
            if same_ele['girl']>=2 and same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11150':
            if same_ele['girl']>=2 and same_ele['lion']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11151':
            if same_ele['girl']>=2 and same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11152':
            if same_ele['girl']>=2 and same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1                       
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11153':
            if same_ele['girl']>=2 and same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11154':
            if same_ele['girl']>=2 and same_ele['push_scooter']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_11155':
            if same_ele['girl']>=2 and same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
    ###8QTSUDC1
        elif kaudio=='audio_9698':
            if same_ele['monkey']>=1 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9699':
            if same_ele['monkey']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9700':
            if same_ele['monkey']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9701':
            if same_ele['monkey']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9702':
            if same_ele['monkey']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9703':
            if same_ele['monkey']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9704':
            if same_ele['monkey']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9705':
            if same_ele['monkey']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9706':
            if same_ele['parrot']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9707':
            if same_ele['parrot']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9708':
            if same_ele['parrot']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9709':
            if same_ele['parrot']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9710':
            if same_ele['parrot']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9711':
            if same_ele['parrot']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9712':
            if same_ele['parrot']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9713':
            if same_ele['octopus']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9714':
            if same_ele['octopus']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9715':
            if same_ele['octopus']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9716':
            if same_ele['octopus']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9717':
            if same_ele['octopus']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9718':
            if same_ele['octopus']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9719':
            if same_ele['icecream']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9720':
            if same_ele['icecream']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9721':
            if same_ele['icecream']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9722':
            if same_ele['icecream']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9723':
            if same_ele['icecream']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9724':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9725':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9726':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9727':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9728':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9729':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9730':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9731':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9732':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9733':
            if same_ele['umbrella']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9734':
            if same_ele['lion']>=1 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9735':
            if same_ele['lion']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9736':
            if same_ele['lion']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9737':
            if same_ele['lion']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9738':
            if same_ele['lion']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9739':
            if same_ele['lion']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9740':
            if same_ele['lion']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9741':
            if same_ele['lion']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9742':
            if same_ele['parrot']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9743':
            if same_ele['parrot']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9744':
            if same_ele['parrot']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9745':
            if same_ele['parrot']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9746':
            if same_ele['parrot']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9747':
            if same_ele['parrot']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9748':
            if same_ele['parrot']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9749':
            if same_ele['octopus']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9750':
            if same_ele['octopus']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9751':
            if same_ele['octopus']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9752':
            if same_ele['octopus']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9753':
            if same_ele['octopus']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9754':
            if same_ele['octopus']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9755':
            if same_ele['icecream']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9756':
            if same_ele['icecream']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9757':
            if same_ele['icecream']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9758':
            if same_ele['icecream']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9759':
            if same_ele['icecream']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9760':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9761':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9762':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9763':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9764':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9765':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9766':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9767':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9768':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9769':
            if same_ele['umbrella']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9770':
            if same_ele['lion']>=1 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9771':
            if same_ele['lion']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9772':
            if same_ele['lion']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9773':
            if same_ele['lion']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9774':
            if same_ele['lion']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9775':
            if same_ele['lion']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9776':
            if same_ele['lion']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9777':
            if same_ele['lion']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9778':
            if same_ele['monkey']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9779':
            if same_ele['monkey']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9780':
            if same_ele['monkey']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9781':
            if same_ele['monkey']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9782':
            if same_ele['monkey']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9783':
            if same_ele['monkey']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9784':
            if same_ele['monkey']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9785':
            if same_ele['octopus']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9786':
            if same_ele['octopus']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9787':
            if same_ele['octopus']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9788':
            if same_ele['octopus']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9789':
            if same_ele['octopus']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9790':
            if same_ele['octopus']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9791':
            if same_ele['icecream']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9792':
            if same_ele['icecream']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9793':
            if same_ele['icecream']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9794':
            if same_ele['icecream']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9795':
            if same_ele['icecream']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9796':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9797':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9798':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9799':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9800':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9801':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9802':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9803':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9804':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9805':
            if same_ele['umbrella']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9806':
            if same_ele['lion']>=1 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9807':
            if same_ele['lion']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9808':
            if same_ele['lion']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9809':
            if same_ele['lion']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9810':
            if same_ele['lion']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9811':
            if same_ele['lion']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9812':
            if same_ele['lion']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9813':
            if same_ele['lion']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9814':
            if same_ele['monkey']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9815':
            if same_ele['monkey']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9816':
            if same_ele['monkey']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9817':
            if same_ele['monkey']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9818':
            if same_ele['monkey']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9819':
            if same_ele['monkey']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9820':
            if same_ele['monkey']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9821':
            if same_ele['parrot']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9822':
            if same_ele['parrot']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9823':
            if same_ele['parrot']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9824':
            if same_ele['parrot']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9825':
            if same_ele['parrot']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9826':
            if same_ele['parrot']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9827':
            if same_ele['icecream']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9828':
            if same_ele['icecream']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9829':
            if same_ele['icecream']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9830':
            if same_ele['icecream']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9831':
            if same_ele['icecream']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9832':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9833':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9834':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9835':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9836':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9837':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9838':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9839':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9840':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9841':
            if same_ele['umbrella']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9842':
            if same_ele['lion']>=1 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9843':
            if same_ele['lion']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9844':
            if same_ele['lion']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9845':
            if same_ele['lion']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9846':
            if same_ele['lion']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9847':
            if same_ele['lion']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9848':
            if same_ele['lion']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9849':
            if same_ele['lion']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9850':
            if same_ele['monkey']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9851':
            if same_ele['monkey']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9852':
            if same_ele['monkey']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9853':
            if same_ele['monkey']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9854':
            if same_ele['monkey']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9855':
            if same_ele['monkey']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9856':
            if same_ele['monkey']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9857':
            if same_ele['parrot']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9858':
            if same_ele['parrot']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9859':
            if same_ele['parrot']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9860':
            if same_ele['parrot']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9861':
            if same_ele['parrot']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9862':
            if same_ele['parrot']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9863':
            if same_ele['octopus']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9864':
            if same_ele['octopus']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9865':
            if same_ele['octopus']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9866':
            if same_ele['octopus']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9867':
            if same_ele['octopus']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9868':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9869':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9870':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9871':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9872':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9873':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9874':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9875':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9876':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9877':
            if same_ele['umbrella']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9878':
            if same_ele['glasses']>=1 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9879':
            if same_ele['glasses']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9880':
            if same_ele['glasses']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9881':
            if same_ele['glasses']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9882':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9883':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9884':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9885':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9886':
            if same_ele['monkey']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9887':
            if same_ele['monkey']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9888':
            if same_ele['monkey']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9889':
            if same_ele['monkey']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9890':
            if same_ele['monkey']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9891':
            if same_ele['monkey']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9892':
            if same_ele['monkey']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9893':
            if same_ele['parrot']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9894':
            if same_ele['parrot']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9895':
            if same_ele['parrot']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9896':
            if same_ele['parrot']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9897':
            if same_ele['parrot']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9898':
            if same_ele['parrot']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9899':
            if same_ele['octopus']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9900':
            if same_ele['octopus']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9901':
            if same_ele['octopus']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9902':
            if same_ele['octopus']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9903':
            if same_ele['octopus']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9904':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9905':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9906':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9907':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9908':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9909':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9910':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9911':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9912':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9913':
            if same_ele['umbrella']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9914':
            if same_ele['glasses']>=1 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9915':
            if same_ele['glasses']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9916':
            if same_ele['glasses']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9917':
            if same_ele['glasses']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9918':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9919':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9920':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9921':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9922':
            if same_ele['bicycle']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9923':
            if same_ele['bicycle']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9924':
            if same_ele['bicycle']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9925':
            if same_ele['bicycle']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9926':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9927':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9928':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9929':
            if same_ele['parrot']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9930':
            if same_ele['parrot']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9931':
            if same_ele['parrot']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9932':
            if same_ele['parrot']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9933':
            if same_ele['parrot']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9934':
            if same_ele['parrot']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9935':
            if same_ele['octopus']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9936':
            if same_ele['octopus']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9937':
            if same_ele['octopus']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9938':
            if same_ele['octopus']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9939':
            if same_ele['octopus']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9940':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9941':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9942':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9943':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9944':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9945':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9946':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9947':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9948':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9949':
            if same_ele['umbrella']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9950':
            if same_ele['push_scooter']>=1 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9951':
            if same_ele['push_scooter']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9952':
            if same_ele['push_scooter']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9953':
            if same_ele['push_scooter']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9954':
            if same_ele['push_scooter']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9955':
            if same_ele['push_scooter']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9956':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9957':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9958':
            if same_ele['bicycle']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9959':
            if same_ele['bicycle']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9960':
            if same_ele['bicycle']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9961':
            if same_ele['bicycle']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9962':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9963':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9964':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9965':
            if same_ele['parrot']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9966':
            if same_ele['parrot']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9967':
            if same_ele['parrot']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9968':
            if same_ele['parrot']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9969':
            if same_ele['parrot']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9970':
            if same_ele['parrot']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9971':
            if same_ele['octopus']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9972':
            if same_ele['octopus']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9973':
            if same_ele['octopus']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9974':
            if same_ele['octopus']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9975':
            if same_ele['octopus']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9976':
            if same_ele['push_scooter']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9977':
            if same_ele['push_scooter']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9978':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9979':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9980':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9981':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9982':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9983':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9984':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9985':
            if same_ele['umbrella']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9986':
            if same_ele['push_scooter']>=1 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9987':
            if same_ele['push_scooter']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9988':
            if same_ele['push_scooter']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9989':
            if same_ele['push_scooter']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9990':
            if same_ele['push_scooter']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9991':
            if same_ele['push_scooter']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9992':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9993':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9994':
            if same_ele['bicycle']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9995':
            if same_ele['bicycle']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9996':
            if same_ele['bicycle']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9997':
            if same_ele['bicycle']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9998':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9999':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10000':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10001':
            if same_ele['parrot']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10002':
            if same_ele['parrot']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10003':
            if same_ele['parrot']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10004':
            if same_ele['parrot']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10005':
            if same_ele['parrot']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10006':
            if same_ele['parrot']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10007':
            if same_ele['octopus']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10008':
            if same_ele['octopus']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10009':
            if same_ele['octopus']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10010':
            if same_ele['octopus']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10011':
            if same_ele['octopus']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10012':
            if same_ele['push_scooter']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10013':
            if same_ele['push_scooter']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10014':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10015':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10016':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10017':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10018':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10019':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10020':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10021':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10022':
            if same_ele['lion']>=1 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10023':
            if same_ele['lion']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10024':
            if same_ele['lion']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10025':
            if same_ele['lion']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10026':
            if same_ele['lion']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10027':
            if same_ele['lion']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10028':
            if same_ele['lion']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10029':
            if same_ele['lion']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10030':
            if same_ele['parrot']>=1 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10031':
            if same_ele['parrot']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10032':
            if same_ele['parrot']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10033':
            if same_ele['parrot']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10034':
            if same_ele['parrot']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10035':
            if same_ele['parrot']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10036':
            if same_ele['parrot']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10037':
            if same_ele['octopus']>=1 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10038':
            if same_ele['octopus']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10039':
            if same_ele['octopus']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10040':
            if same_ele['octopus']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10041':
            if same_ele['octopus']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10042':
            if same_ele['octopus']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10043':
            if same_ele['icecream']>=1 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10044':
            if same_ele['icecream']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10045':
            if same_ele['icecream']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10046':
            if same_ele['icecream']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10047':
            if same_ele['icecream']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10048':
            if same_ele['glasses']>=1 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10049':
            if same_ele['glasses']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10050':
            if same_ele['glasses']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10051':
            if same_ele['glasses']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10052':
            if same_ele['bicycle']>=1 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10053':
            if same_ele['bicycle']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10054':
            if same_ele['bicycle']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10055':
            if same_ele['push_scooter']>=1 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10056':
            if same_ele['push_scooter']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10057':
            if same_ele['umbrella']>=1 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
    ###8QTSUDC2
        elif kaudio=='audio_10058':
            if same_ele['monkey']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10059':
            if same_ele['monkey']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10060':
            if same_ele['monkey']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10061':
            if same_ele['monkey']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10062':
            if same_ele['monkey']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10063':
            if same_ele['monkey']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10064':
            if same_ele['monkey']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10065':
            if same_ele['monkey']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10066':
            if same_ele['parrot']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10067':
            if same_ele['parrot']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10068':
            if same_ele['parrot']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10069':
            if same_ele['parrot']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10070':
            if same_ele['parrot']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10071':
            if same_ele['parrot']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10072':
            if same_ele['parrot']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10073':
            if same_ele['parrot']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10074':
            if same_ele['octopus']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10075':
            if same_ele['octopus']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10076':
            if same_ele['octopus']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10077':
            if same_ele['octopus']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10078':
            if same_ele['octopus']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10079':
            if same_ele['octopus']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10080':
            if same_ele['octopus']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10081':
            if same_ele['octopus']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10082':
            if same_ele['icecream']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10083':
            if same_ele['icecream']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10084':
            if same_ele['icecream']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10085':
            if same_ele['icecream']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10086':
            if same_ele['icecream']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10087':
            if same_ele['icecream']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10088':
            if same_ele['icecream']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10089':
            if same_ele['icecream']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10090':
            if same_ele['glasses']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10091':
            if same_ele['glasses']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10092':
            if same_ele['glasses']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10093':
            if same_ele['glasses']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10094':
            if same_ele['glasses']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10095':
            if same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10096':
            if same_ele['glasses']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10097':
            if same_ele['glasses']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10098':
            if same_ele['bicycle']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10099':
            if same_ele['bicycle']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10100':
            if same_ele['bicycle']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10101':
            if same_ele['bicycle']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10102':
            if same_ele['bicycle']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10103':
            if same_ele['bicycle']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10104':
            if same_ele['bicycle']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10105':
            if same_ele['bicycle']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10106':
            if same_ele['push_scooter']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10107':
            if same_ele['push_scooter']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10108':
            if same_ele['push_scooter']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10109':
            if same_ele['push_scooter']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10110':
            if same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10111':
            if same_ele['push_scooter']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10112':
            if same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10113':
            if same_ele['push_scooter']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10114':
            if same_ele['umbrella']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10115':
            if same_ele['umbrella']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10116':
            if same_ele['umbrella']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10117':
            if same_ele['umbrella']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10118':
            if same_ele['umbrella']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10119':
            if same_ele['umbrella']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10120':
            if same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10121':
            if same_ele['umbrella']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10122':
            if same_ele['girl']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10123':
            if same_ele['girl']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10124':
            if same_ele['girl']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10125':
            if same_ele['girl']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10126':
            if same_ele['girl']>=2 and same_ele['glassses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10127':
            if same_ele['girl']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10128':
            if same_ele['girl']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10129':
            if same_ele['girl']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_10130':
            if same_ele['lion']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10131':
            if same_ele['lion']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10132':
            if same_ele['lion']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10133':
            if same_ele['lion']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10134':
            if same_ele['lion']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10135':
            if same_ele['lion']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10136':
            if same_ele['lion']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10137':
            if same_ele['lion']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10138':
            if same_ele['parrot']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10139':
            if same_ele['parrot']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10140':
            if same_ele['parrot']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10141':
            if same_ele['parrot']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10142':
            if same_ele['parrot']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10143':
            if same_ele['parrot']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10144':
            if same_ele['parrot']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10145':
            if same_ele['parrot']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10146':
            if same_ele['octopus']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10147':
            if same_ele['octopus']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10148':
            if same_ele['octopus']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10149':
            if same_ele['octopus']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10150':
            if same_ele['octopus']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10151':
            if same_ele['octopus']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10152':
            if same_ele['octopus']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10153':
            if same_ele['octopus']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10154':
            if same_ele['icecream']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10155':
            if same_ele['icecream']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10156':
            if same_ele['icecream']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10157':
            if same_ele['icecream']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10158':
            if same_ele['icecream']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10159':
            if same_ele['icecream']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10160':
            if same_ele['icecream']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10161':
            if same_ele['icecream']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10162':
            if same_ele['glasses']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10163':
            if same_ele['glasses']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10164':
            if same_ele['glasses']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10165':
            if same_ele['glasses']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10166':
            if same_ele['glasses']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10167':
            if same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10168':
            if same_ele['glasses']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10169':
            if same_ele['glasses']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10170':
            if same_ele['bicycle']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10171':
            if same_ele['bicycle']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10172':
            if same_ele['bicycle']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10173':
            if same_ele['bicycle']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10174':
            if same_ele['bicycle']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10175':
            if same_ele['bicycle']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10176':
            if same_ele['bicycle']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10177':
            if same_ele['bicycle']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10178':
            if same_ele['push_scooter']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10179':
            if same_ele['push_scooter']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10180':
            if same_ele['push_scooter']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10181':
            if same_ele['push_scooter']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10182':
            if same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10183':
            if same_ele['push_scooter']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10184':
            if same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10185':
            if same_ele['push_scooter']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10186':
            if same_ele['umbrella']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10187':
            if same_ele['umbrella']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10188':
            if same_ele['umbrella']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10189':
            if same_ele['umbrella']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10190':
            if same_ele['umbrella']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10191':
            if same_ele['umbrella']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10192':
            if same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10193':
            if same_ele['umbrella']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10194':
            if same_ele['girl']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10195':
            if same_ele['girl']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10196':
            if same_ele['girl']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10197':
            if same_ele['girl']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10198':
            if same_ele['girl']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10199':
            if same_ele['girl']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10200':
            if same_ele['girl']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10201':
            if same_ele['girl']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_10202':
            if same_ele['lion']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10203':
            if same_ele['lion']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10204':
            if same_ele['lion']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10205':
            if same_ele['lion']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10206':
            if same_ele['lion']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10207':
            if same_ele['lion']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10208':
            if same_ele['lion']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10209':
            if same_ele['lion']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10210':
            if same_ele['monkey']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10211':
            if same_ele['monkey']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10212':
            if same_ele['monkey']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10213':
            if same_ele['monkey']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10214':
            if same_ele['monkey']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10215':
            if same_ele['monkey']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10216':
            if same_ele['monkey']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10217':
            if same_ele['monkey']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10218':
            if same_ele['octopus']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10219':
            if same_ele['octopus']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10220':
            if same_ele['octopus']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10221':
            if same_ele['octopus']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10222':
            if same_ele['octopus']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10223':
            if same_ele['octopus']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10224':
            if same_ele['octopus']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10225':
            if same_ele['octopus']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10226':
            if same_ele['icecream']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10227':
            if same_ele['icecream']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10228':
            if same_ele['icecream']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10229':
            if same_ele['icecream']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10230':
            if same_ele['icecream']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10231':
            if same_ele['icecream']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10232':
            if same_ele['icecream']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10233':
            if same_ele['icecream']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10234':
            if same_ele['glasses']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10235':
            if same_ele['glasses']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10236':
            if same_ele['glasses']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10237':
            if same_ele['glasses']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10238':
            if same_ele['glasses']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10239':
            if same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10240':
            if same_ele['glasses']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10241':
            if same_ele['glasses']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10242':
            if same_ele['bicycle']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10243':
            if same_ele['bicycle']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10244':
            if same_ele['bicycle']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10245':
            if same_ele['bicycle']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10246':
            if same_ele['bicycle']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10247':
            if same_ele['bicycle']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10248':
            if same_ele['bicycle']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10249':
            if same_ele['bicycle']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10250':
            if same_ele['push_scooter']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10251':
            if same_ele['push_scooter']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10252':
            if same_ele['push_scooter']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10253':
            if same_ele['push_scooter']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10254':
            if same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10255':
            if same_ele['push_scooter']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10256':
            if same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10257':
            if same_ele['push_scooter']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10258':
            if same_ele['umbrella']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10259':
            if same_ele['umbrella']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10260':
            if same_ele['umbrella']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10261':
            if same_ele['umbrella']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10262':
            if same_ele['umbrella']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10263':
            if same_ele['umbrella']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10264':
            if same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10265':
            if same_ele['umbrella']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10266':
            if same_ele['girl']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10267':
            if same_ele['girl']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10268':
            if same_ele['girl']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10269':
            if same_ele['girl']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10270':
            if same_ele['girl']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10271':
            if same_ele['girl']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10272':
            if same_ele['girl']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10273':
            if same_ele['girl']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_10274':
            if same_ele['lion']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10275':
            if same_ele['lion']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10276':
            if same_ele['lion']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10277':
            if same_ele['lion']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10278':
            if same_ele['lion']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10279':
            if same_ele['lion']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10280':
            if same_ele['lion']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10281':
            if same_ele['lion']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10282':
            if same_ele['monkey']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10283':
            if same_ele['monkey']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10284':
            if same_ele['monkey']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10285':
            if same_ele['monkey']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10286':
            if same_ele['monkey']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10287':
            if same_ele['monkey']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10288':
            if same_ele['monkey']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10289':
            if same_ele['monkey']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10290':
            if same_ele['parrot']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10291':
            if same_ele['parrot']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10292':
            if same_ele['parrot']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10293':
            if same_ele['parrot']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10294':
            if same_ele['parrot']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10295':
            if same_ele['parrot']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10296':
            if same_ele['parrot']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10297':
            if same_ele['parrot']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10298':
            if same_ele['icecream']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10299':
            if same_ele['icecream']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10300':
            if same_ele['icecream']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10301':
            if same_ele['icecream']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10302':
            if same_ele['icecream']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10303':
            if same_ele['icecream']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10304':
            if same_ele['icecream']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10305':
            if same_ele['icecream']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10306':
            if same_ele['glasses']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10307':
            if same_ele['glasses']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10308':
            if same_ele['glasses']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10309':
            if same_ele['glasses']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10310':
            if same_ele['glasses']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10311':
            if same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10312':
            if same_ele['glasses']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10313':
            if same_ele['glasses']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10314':
            if same_ele['bicycle']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10315':
            if same_ele['bicycle']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10316':
            if same_ele['bicycle']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10317':
            if same_ele['bicycle']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10318':
            if same_ele['bicycle']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10319':
            if same_ele['bicycle']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10320':
            if same_ele['bicycle']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10321':
            if same_ele['bicycle']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10322':
            if same_ele['push_scooter']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10323':
            if same_ele['push_scooter']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10324':
            if same_ele['push_scooter']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10325':
            if same_ele['push_scooter']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10326':
            if same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10327':
            if same_ele['push_scooter']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10328':
            if same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10329':
            if same_ele['push_scooter']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10330':
            if same_ele['umbrella']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10331':
            if same_ele['umbrella']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10332':
            if same_ele['umbrella']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10333':
            if same_ele['umbrella']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10334':
            if same_ele['umbrella']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10335':
            if same_ele['umbrella']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10336':
            if same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10337':
            if same_ele['umbrella']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10338':
            if same_ele['girl']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10339':
            if same_ele['girl']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10340':
            if same_ele['girl']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10341':
            if same_ele['girl']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10342':
            if same_ele['girl']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10343':
            if same_ele['girl']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10344':
            if same_ele['girl']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10345':
            if same_ele['girl']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_10346':
            if same_ele['lion']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10347':
            if same_ele['lion']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10348':
            if same_ele['lion']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10349':
            if same_ele['lion']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10350':
            if same_ele['lion']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10351':
            if same_ele['lion']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10352':
            if same_ele['lion']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10353':
            if same_ele['lion']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10354':
            if same_ele['monkey']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10355':
            if same_ele['monkey']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10356':
            if same_ele['monkey']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10357':
            if same_ele['monkey']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10358':
            if same_ele['monkey']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10359':
            if same_ele['monkey']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10360':
            if same_ele['monkey']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10361':
            if same_ele['monkey']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10362':
            if same_ele['octopus']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10363':
            if same_ele['octopus']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10364':
            if same_ele['octopus']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10365':
            if same_ele['octopus']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10366':
            if same_ele['octopus']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10367':
            if same_ele['octopus']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10368':
            if same_ele['octopus']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10369':
            if same_ele['octopus']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10370':
            if same_ele['parrot']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10371':
            if same_ele['parrot']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10372':
            if same_ele['parrot']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10373':
            if same_ele['parrot']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10374':
            if same_ele['parrot']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10375':
            if same_ele['parrot']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10376':
            if same_ele['parrot']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10377':
            if same_ele['parrot']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10378':
            if same_ele['glasses']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10379':
            if same_ele['glasses']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10380':
            if same_ele['glasses']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10381':
            if same_ele['glasses']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10382':
            if same_ele['glasses']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10383':
            if same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10384':
            if same_ele['glasses']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10385':
            if same_ele['glasses']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10386':
            if same_ele['bicycle']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10387':
            if same_ele['bicycle']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10388':
            if same_ele['bicycle']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10389':
            if same_ele['bicycle']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10390':
            if same_ele['bicycle']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10391':
            if same_ele['bicycle']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10392':
            if same_ele['bicycle']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10393':
            if same_ele['bicycle']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10394':
            if same_ele['push_scooter']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10395':
            if same_ele['push_scooter']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10396':
            if same_ele['push_scooter']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10397':
            if same_ele['push_scooter']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10398':
            if same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10399':
            if same_ele['push_scooter']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10400':
            if same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10401':
            if same_ele['push_scooter']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10402':
            if same_ele['umbrella']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10403':
            if same_ele['umbrella']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10404':
            if same_ele['umbrella']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10405':
            if same_ele['umbrella']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10406':
            if same_ele['umbrella']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10407':
            if same_ele['umbrella']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10408':
            if same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10409':
            if same_ele['umbrella']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10410':
            if same_ele['girl']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10411':
            if same_ele['girl']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10412':
            if same_ele['girl']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10413':
            if same_ele['girl']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10414':
            if same_ele['girl']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10415':
            if same_ele['girl']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10416':
            if same_ele['girl']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10417':
            if same_ele['girl']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_10418':
            if same_ele['lion']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10419':
            if same_ele['lion']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10420':
            if same_ele['lion']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10421':
            if same_ele['lion']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10422':
            if same_ele['lion']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10423':
            if same_ele['lion']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10424':
            if same_ele['lion']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10425':
            if same_ele['lion']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10426':
            if same_ele['monkey']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10427':
            if same_ele['monkey']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10428':
            if same_ele['monkey']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10429':
            if same_ele['monkey']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10430':
            if same_ele['monkey']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10431':
            if same_ele['monkey']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10432':
            if same_ele['monkey']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10433':
            if same_ele['monkey']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10434':
            if same_ele['octopus']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10435':
            if same_ele['octopus']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10436':
            if same_ele['octopus']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10437':
            if same_ele['octopus']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10438':
            if same_ele['octopus']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10439':
            if same_ele['octopus']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10440':
            if same_ele['octopus']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10441':
            if same_ele['octopus']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10442':
            if same_ele['parrot']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10443':
            if same_ele['parrot']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10444':
            if same_ele['parrot']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10445':
            if same_ele['parrot']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10446':
            if same_ele['parrot']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10447':
            if same_ele['parrot']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10448':
            if same_ele['parrot']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10449':
            if same_ele['parrot']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10450':
            if same_ele['icecream']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10451':
            if same_ele['icecream']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10452':
            if same_ele['icecream']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10453':
            if same_ele['icecream']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10454':
            if same_ele['icecream']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10455':
            if same_ele['icecream']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10456':
            if same_ele['icecream']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10457':
            if same_ele['icecream']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10458':
            if same_ele['bicycle']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10459':
            if same_ele['bicycle']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10460':
            if same_ele['bicycle']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10461':
            if same_ele['bicycle']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10462':
            if same_ele['bicycle']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10463':
            if same_ele['bicycle']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10464':
            if same_ele['bicycle']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10465':
            if same_ele['bicycle']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10466':
            if same_ele['push_scooter']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10467':
            if same_ele['push_scooter']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10468':
            if same_ele['push_scooter']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10469':
            if same_ele['push_scooter']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10470':
            if same_ele['push_scooter']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10471':
            if same_ele['push_scooter']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10472':
            if same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10473':
            if same_ele['push_scooter']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10474':
            if same_ele['umbrella']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10475':
            if same_ele['umbrella']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10476':
            if same_ele['umbrella']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10477':
            if same_ele['umbrella']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10478':
            if same_ele['umbrella']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10479':
            if same_ele['umbrella']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10480':
            if same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10481':
            if same_ele['umbrella']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10482':
            if same_ele['girl']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10483':
            if same_ele['girl']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10484':
            if same_ele['girl']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10485':
            if same_ele['girl']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10486':
            if same_ele['girl']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10487':
            if same_ele['girl']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10488':
            if same_ele['girl']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_10489':
            if same_ele['girl']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']                                                                                 
        elif kaudio=='audio_10490':
            if same_ele['lion']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10491':
            if same_ele['lion']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10492':
            if same_ele['lion']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10493':
            if same_ele['lion']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10494':
            if same_ele['lion']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10495':
            if same_ele['lion']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10496':
            if same_ele['lion']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10497':
            if same_ele['lion']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10498':
            if same_ele['monkey']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10499':
            if same_ele['monkey']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10500':
            if same_ele['monkey']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10501':
            if same_ele['monkey']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10502':
            if same_ele['monkey']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10503':
            if same_ele['monkey']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10504':
            if same_ele['monkey']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10505':
            if same_ele['monkey']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10506':
            if same_ele['octopus']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10507':
            if same_ele['octopus']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10508':
            if same_ele['octopus']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10509':
            if same_ele['octopus']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10510':
            if same_ele['octopus']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10511':
            if same_ele['octopus']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10512':
            if same_ele['octopus']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10513':
            if same_ele['octopus']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10514':
            if same_ele['parrot']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10515':
            if same_ele['parrot']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10516':
            if same_ele['parrot']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10517':
            if same_ele['parrot']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10518':
            if same_ele['parrot']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10519':
            if same_ele['parrot']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10520':
            if same_ele['parrot']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10521':
            if same_ele['parrot']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10522':
            if same_ele['icecream']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10523':
            if same_ele['icecream']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10524':
            if same_ele['icecream']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10525':
            if same_ele['icecream']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10526':
            if same_ele['icecream']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10527':
            if same_ele['icecream']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10528':
            if same_ele['icecream']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10529':
            if same_ele['icecream']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10530':
            if same_ele['glasses']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10531':
            if same_ele['glasses']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10532':
            if same_ele['glasses']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10533':
            if same_ele['glasses']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10534':
            if same_ele['glasses']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10535':
            if same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10536':
            if same_ele['glasses']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10537':
            if same_ele['glasses']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10538':
            if same_ele['push_scooter']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10539':
            if same_ele['push_scooter']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10540':
            if same_ele['push_scooter']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10541':
            if same_ele['push_scooter']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10542':
            if same_ele['push_scooter']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10543':
            if same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10544':
            if same_ele['push_scooter']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10545':
            if same_ele['push_scooter']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10546':
            if same_ele['umbrella']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10547':
            if same_ele['umbrella']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10548':
            if same_ele['umbrella']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10549':
            if same_ele['umbrella']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10550':
            if same_ele['umbrella']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10551':
            if same_ele['umbrella']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10552':
            if same_ele['umbrella']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10553':
            if same_ele['umbrella']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10554':
            if same_ele['girl']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10555':
            if same_ele['girl']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10556':
            if same_ele['girl']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10557':
            if same_ele['girl']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10558':
            if same_ele['girl']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10559':
            if same_ele['girl']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10560':
            if same_ele['girl']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10561':
            if same_ele['girl']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_10562':
            if same_ele['lion']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10563':
            if same_ele['lion']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10564':
            if same_ele['lion']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10565':
            if same_ele['lion']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10566':
            if same_ele['lion']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10567':
            if same_ele['lion']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10568':
            if same_ele['lion']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10569':
            if same_ele['lion']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10570':
            if same_ele['monkey']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10571':
            if same_ele['monkey']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10572':
            if same_ele['monkey']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10573':
            if same_ele['monkey']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10574':
            if same_ele['monkey']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10575':
            if same_ele['monkey']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10576':
            if same_ele['monkey']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10577':
            if same_ele['monkey']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10578':
            if same_ele['octopus']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10579':
            if same_ele['octopus']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10580':
            if same_ele['octopus']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10581':
            if same_ele['octopus']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10582':
            if same_ele['octopus']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10583':
            if same_ele['octopus']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10584':
            if same_ele['octopus']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10585':
            if same_ele['octopus']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10586':
            if same_ele['parrot']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10587':
            if same_ele['parrot']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10588':
            if same_ele['parrot']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10589':
            if same_ele['parrot']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10590':
            if same_ele['parrot']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10591':
            if same_ele['parrot']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10592':
            if same_ele['parrot']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10593':
            if same_ele['parrot']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10594':
            if same_ele['icecream']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10595':
            if same_ele['icecream']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10596':
            if same_ele['icecream']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10597':
            if same_ele['icecream']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10598':
            if same_ele['icecream']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10599':
            if same_ele['icecream']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10600':
            if same_ele['icecream']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10601':
            if same_ele['icecream']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10602':
            if same_ele['glasses']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10603':
            if same_ele['glasses']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10604':
            if same_ele['glasses']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10605':
            if same_ele['glasses']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10606':
            if same_ele['glasses']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10607':
            if same_ele['glasses']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10608':
            if same_ele['glasses']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10609':
            if same_ele['glasses']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10610':
            if same_ele['bicycle']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10611':
            if same_ele['bicycle']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10612':
            if same_ele['bicycle']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10613':
            if same_ele['bicycle']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10614':
            if same_ele['bicycle']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10615':
            if same_ele['bicycle']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10616':
            if same_ele['bicycle']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10617':
            if same_ele['bicycle']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10618':
            if same_ele['umbrella']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10619':
            if same_ele['umbrella']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10620':
            if same_ele['umbrella']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10621':
            if same_ele['umbrella']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10622':
            if same_ele['umbrella']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10623':
            if same_ele['umbrella']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10624':
            if same_ele['umbrella']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10625':
            if same_ele['umbrella']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10626':
            if same_ele['girl']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10627':
            if same_ele['girl']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10628':
            if same_ele['girl']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10629':
            if same_ele['girl']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10630':
            if same_ele['girl']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10631':
            if same_ele['girl']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10632':
            if same_ele['girl']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_10633':
            if same_ele['girl']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']                         
        elif kaudio=='audio_10634':
            if same_ele['lion']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10635':
            if same_ele['lion']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10636':
            if same_ele['lion']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10637':
            if same_ele['lion']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10638':
            if same_ele['lion']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10639':
            if same_ele['lion']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10640':
            if same_ele['lion']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10641':
            if same_ele['lion']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10642':
            if same_ele['monkey']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10643':
            if same_ele['monkey']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10644':
            if same_ele['monkey']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10645':
            if same_ele['monkey']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10646':
            if same_ele['monkey']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10647':
            if same_ele['monkey']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10648':
            if same_ele['monkey']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10649':
            if same_ele['monkey']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10650':
            if same_ele['octopus']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10651':
            if same_ele['octopus']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10652':
            if same_ele['octopus']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10653':
            if same_ele['octopus']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10654':
            if same_ele['octopus']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10655':
            if same_ele['octopus']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10656':
            if same_ele['octopus']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10657':
            if same_ele['octopus']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10658':
            if same_ele['parrot']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10659':
            if same_ele['parrot']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10660':
            if same_ele['parrot']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10661':
            if same_ele['parrot']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10662':
            if same_ele['parrot']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10663':
            if same_ele['parrot']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10664':
            if same_ele['parrot']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10665':
            if same_ele['parrot']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10666':
            if same_ele['icecream']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10667':
            if same_ele['icecream']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10668':
            if same_ele['icecream']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10669':
            if same_ele['icecream']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10670':
            if same_ele['icecream']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10671':
            if same_ele['icecream']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10672':
            if same_ele['icecream']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10673':
            if same_ele['icecream']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10674':
            if same_ele['glasses']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10675':
            if same_ele['glasses']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10676':
            if same_ele['glasses']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10677':
            if same_ele['glasses']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10678':
            if same_ele['glasses']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10679':
            if same_ele['glasses']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10680':
            if same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10681':
            if same_ele['glasses']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10682':
            if same_ele['bicycle']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10683':
            if same_ele['bicycle']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10684':
            if same_ele['bicycle']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10685':
            if same_ele['bicycle']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10686':
            if same_ele['bicycle']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10687':
            if same_ele['bicycle']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10688':
            if same_ele['bicycle']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10689':
            if same_ele['bicycle']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10690':
            if same_ele['push_scooter']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10691':
            if same_ele['push_scooter']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10692':
            if same_ele['push_scooter']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10693':
            if same_ele['push_scooter']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10694':
            if same_ele['push_scooter']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10695':
            if same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10696':
            if same_ele['push_scooter']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10697':
            if same_ele['push_scooter']>=2 and same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10698':
            if same_ele['girl']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10699':
            if same_ele['girl']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10700':
            if same_ele['girl']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10701':
            if same_ele['girl']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10702':
            if same_ele['girl']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10703':
            if same_ele['girl']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10704':
            if same_ele['girl']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10705':
            if same_ele['girl']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_10706':
            if same_ele['lion']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10707':
            if same_ele['lion']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10708':
            if same_ele['lion']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10709':
            if same_ele['lion']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10710':
            if same_ele['lion']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10711':
            if same_ele['lion']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10712':
            if same_ele['lion']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10713':
            if same_ele['lion']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10714':
            if same_ele['monkey']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10715':
            if same_ele['monkey']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10716':
            if same_ele['monkey']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10717':
            if same_ele['monkey']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10718':
            if same_ele['monkey']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10719':
            if same_ele['monkey']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10720':
            if same_ele['monkey']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10721':
            if same_ele['monkey']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10722':
            if same_ele['octopus']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10723':
            if same_ele['octopus']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10724':
            if same_ele['octopus']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10725':
            if same_ele['octopus']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10726':
            if same_ele['octopus']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10727':
            if same_ele['octopus']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10728':
            if same_ele['octopus']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10729':
            if same_ele['octopus']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10730':
            if same_ele['parrot']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10731':
            if same_ele['parrot']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10732':
            if same_ele['parrot']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10733':
            if same_ele['parrot']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10734':
            if same_ele['parrot']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10735':
            if same_ele['parrot']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10736':
            if same_ele['parrot']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10737':
            if same_ele['parrot']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10738':
            if same_ele['icecream']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10739':
            if same_ele['icecream']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10740':
            if same_ele['icecream']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10741':
            if same_ele['icecream']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10742':
            if same_ele['icecream']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10743':
            if same_ele['icecream']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10744':
            if same_ele['icecream']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10745':
            if same_ele['icecream']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10746':
            if same_ele['glasses']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10747':
            if same_ele['glasses']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10748':
            if same_ele['glasses']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10749':
            if same_ele['glasses']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10750':
            if same_ele['glasses']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10751':
            if same_ele['glasses']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10752':
            if same_ele['glasses']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10753':
            if same_ele['glasses']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10754':
            if same_ele['bicycle']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10755':
            if same_ele['bicycle']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10756':
            if same_ele['bicycle']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10757':
            if same_ele['bicycle']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10758':
            if same_ele['bicycle']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10759':
            if same_ele['bicycle']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10760':
            if same_ele['bicycle']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10761':
            if same_ele['bicycle']>=2 and same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10762':
            if same_ele['push_scooter']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10763':
            if same_ele['push_scooter']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10764':
            if same_ele['push_scooter']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10765':
            if same_ele['push_scooter']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10766':
            if same_ele['push_scooter']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10767':
            if same_ele['push_scooter']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10768':
            if same_ele['push_scooter']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10769':
            if same_ele['push_scooter']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10770':
            if same_ele['umbrella']>=2 and same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10771':
            if same_ele['umbrella']>=2 and same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10772':
            if same_ele['umbrella']>=2 and same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10773':
            if same_ele['umbrella']>=2 and same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10774':
            if same_ele['umbrella']>=2 and same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10775':
            if same_ele['umbrella']>=2 and same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10776':
            if same_ele['umbrella']>=2 and same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_10777':
            if same_ele['umbrella']>=2 and same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
    ###8QTSUSB1
        elif kaudio=='audio_10958':
            if same_ele['lion']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10959':
            if same_ele['monkey']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10960':
            if same_ele['parrot']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10961':
            if same_ele['octopus']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10962':
            if same_ele['icecream']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10963':
            if same_ele['glasses']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10964':
            if same_ele['bicycle']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10965':
            if same_ele['push_scooter']>=1:
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
                no_ans_to_create_only_body[k] = 1
        elif kaudio=='audio_10966':
            if same_ele['umbrella']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10967':
            if same_ele['girl']>=1:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
    ###8QTSUSB2
        elif kaudio=='audio_10968':
            if same_ele['lion']>=2:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10969':
            if same_ele['monkey']>=2:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10970':
            if same_ele['parrot']>=2:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10971':
            if same_ele['octopus']>=2:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10971':
            if same_ele['icecream']>=2:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10973':
            if same_ele['glasses']>=2:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10974':
            if same_ele['bicycle']>=2:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10975':
            if same_ele['push_scooter']>=2:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10976':
            if same_ele['umbrella']>=2:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
        elif kaudio=='audio_10977':
            if same_ele['girl']>=2:
                no_ans_to_create_only_body[k] = 1
                body_response[k] = 1
                resp_in_body[k] = 1 #belly
                ali.append(11)
    ###8QTSUSC1
        elif kaudio=='audio_8991':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']         
        elif kaudio=='audio_8992':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_8993':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']         
        elif kaudio=='audio_8994':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_8995':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']         
        elif kaudio=='audio_8996':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_8997':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']         
        elif kaudio=='audio_8998':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_8999':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9000':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']         
        elif kaudio=='audio_9001':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9002':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']         
        elif kaudio=='audio_9003':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9004':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']         
        elif kaudio=='audio_9005':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9006':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']         
        elif kaudio=='audio_9007':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9008':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9009':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']         
        elif kaudio=='audio_9010':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9011':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']         
        elif kaudio=='audio_9012':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9013':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']         
        elif kaudio=='audio_9014':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9015':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']         
        elif kaudio=='audio_9016':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9017':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9018':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']         
        elif kaudio=='audio_9019':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9020':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']         
        elif kaudio=='audio_9021':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9022':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']         
        elif kaudio=='audio_9023':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9024':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']         
        elif kaudio=='audio_9025':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9026':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9027':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']         
        elif kaudio=='audio_9028':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9029':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']         
        elif kaudio=='audio_9030':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9031':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']         
        elif kaudio=='audio_9032':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9033':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']         
        elif kaudio=='audio_9034':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9035':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9036':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']         
        elif kaudio=='audio_9037':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9038':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']         
        elif kaudio=='audio_9039':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9040':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']         
        elif kaudio=='audio_9041':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9042':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']         
        elif kaudio=='audio_9043':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9044':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9045':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']         
        elif kaudio=='audio_9046':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9047':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']         
        elif kaudio=='audio_9048':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9049':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']         
        elif kaudio=='audio_9050':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9051':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']         
        elif kaudio=='audio_9052':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9053':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']                                                     
        elif kaudio=='audio_9054':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']         
        elif kaudio=='audio_9055':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9056':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']         
        elif kaudio=='audio_9057':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9058':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']         
        elif kaudio=='audio_9059':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9060':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']         
        elif kaudio=='audio_9061':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9062':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9063':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella'] 
        elif kaudio=='audio_9064':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9065':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']         
        elif kaudio=='audio_9066':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9067':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']         
        elif kaudio=='audio_9068':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9069':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']         
        elif kaudio=='audio_9070':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9071':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9072':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl'] 
        elif kaudio=='audio_9073':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9074':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']         
        elif kaudio=='audio_9075':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9076':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']         
        elif kaudio=='audio_9077':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9078':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']         
        elif kaudio=='audio_9079':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9080':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
    ###8QTSUSC2
        elif kaudio=='audio_9081':
            if same_ele['monkey']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']         
        elif kaudio=='audio_9082':
            if same_ele['parrot']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9083':
            if same_ele['octopus']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']         
        elif kaudio=='audio_9084':
            if same_ele['icecream']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9085':
            if same_ele['glasses']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']         
        elif kaudio=='audio_9086':
            if same_ele['bicycle']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9087':
            if same_ele['push_scooter']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']         
        elif kaudio=='audio_9088':
            if same_ele['umbrella']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9089':
            if same_ele['girl']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
        elif kaudio=='audio_9090':
            if same_ele['lion']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']         
        elif kaudio=='audio_9091':
            if same_ele['parrot']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9092':
            if same_ele['octopus']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']         
        elif kaudio=='audio_9093':
            if same_ele['icecream']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9094':
            if same_ele['glasses']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']         
        elif kaudio=='audio_9095':
            if same_ele['bicycle']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9096':
            if same_ele['push_scooter']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']         
        elif kaudio=='audio_9097':
            if same_ele['umbrella']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9098':
            if same_ele['girl']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
        elif kaudio=='audio_9099':
            if same_ele['lion']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']         
        elif kaudio=='audio_9100':
            if same_ele['monkey']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9101':
            if same_ele['octopus']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']         
        elif kaudio=='audio_9102':
            if same_ele['icecream']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9103':
            if same_ele['glasses']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']         
        elif kaudio=='audio_9104':
            if same_ele['bicycle']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9105':
            if same_ele['push_scooter']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']         
        elif kaudio=='audio_9106':
            if same_ele['umbrella']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9107':
            if same_ele['girl']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
        elif kaudio=='audio_9108':
            if same_ele['lion']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']         
        elif kaudio=='audio_9109':
            if same_ele['monkey']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9110':
            if same_ele['parrot']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']         
        elif kaudio=='audio_9111':
            if same_ele['icecream']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9112':
            if same_ele['glasses']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']         
        elif kaudio=='audio_9113':
            if same_ele['bicycle']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9114':
            if same_ele['push_scooter']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']         
        elif kaudio=='audio_9115':
            if same_ele['umbrella']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9116':
            if same_ele['girl']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
        elif kaudio=='audio_9117':
            if same_ele['lion']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']         
        elif kaudio=='audio_9118':
            if same_ele['monkey']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9119':
            if same_ele['parrot']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']         
        elif kaudio=='audio_9120':
            if same_ele['octopus']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9121':
            if same_ele['glasses']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']         
        elif kaudio=='audio_9122':
            if same_ele['bicycle']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9123':
            if same_ele['push_scooter']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']         
        elif kaudio=='audio_9124':
            if same_ele['umbrella']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9125':
            if same_ele['girl']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
        elif kaudio=='audio_9126':
            if same_ele['lion']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']         
        elif kaudio=='audio_9127':
            if same_ele['monkey']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9128':
            if same_ele['parrot']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']         
        elif kaudio=='audio_9129':
            if same_ele['octopus']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9130':
            if same_ele['icecream']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']         
        elif kaudio=='audio_9131':
            if same_ele['bicycle']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9132':
            if same_ele['push_scooter']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']         
        elif kaudio=='audio_9133':
            if same_ele['umbrella']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9134':
            if same_ele['girl']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
        elif kaudio=='audio_9135':
            if same_ele['lion']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']         
        elif kaudio=='audio_9136':
            if same_ele['monkey']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9137':
            if same_ele['parrot']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']         
        elif kaudio=='audio_9138':
            if same_ele['octopus']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9139':
            if same_ele['icecream']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']         
        elif kaudio=='audio_9140':
            if same_ele['glasses']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9141':
            if same_ele['push_scooter']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']         
        elif kaudio=='audio_9142':
            if same_ele['umbrella']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
        elif kaudio=='audio_9143':
            if same_ele['girl']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']                                                     
        elif kaudio=='audio_9144':
            if same_ele['lion']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']         
        elif kaudio=='audio_9145':
            if same_ele['monkey']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9146':
            if same_ele['parrot']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']         
        elif kaudio=='audio_9147':
            if same_ele['octopus']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9148':
            if same_ele['icecream']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']         
        elif kaudio=='audio_9149':
            if same_ele['glasses']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9150':
            if same_ele['bicycle']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']         
        elif kaudio=='audio_9151':
            if same_ele['umbrella']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9152':
            if same_ele['girl']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
        elif kaudio=='audio_9153':
            if same_ele['lion']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella'] 
        elif kaudio=='audio_9154':
            if same_ele['monkey']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9155':
            if same_ele['parrot']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']         
        elif kaudio=='audio_9156':
            if same_ele['octopus']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9157':
            if same_ele['icecream']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']         
        elif kaudio=='audio_9158':
            if same_ele['glasses']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9159':
            if same_ele['bicycle']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']         
        elif kaudio=='audio_9160':
            if same_ele['push_scooter']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9161':
            if same_ele['girl']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
        elif kaudio=='audio_9162':
            if same_ele['lion']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl'] 
        elif kaudio=='audio_9163':
            if same_ele['monkey']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9164':
            if same_ele['parrot']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9165':
            if same_ele['octopus']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9166':
            if same_ele['icecream']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']         
        elif kaudio=='audio_9167':
            if same_ele['glasses']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9168':
            if same_ele['push_scooter']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']         
        elif kaudio=='audio_9169':
            if same_ele['push_scooter']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
        elif kaudio=='audio_9170':
            if same_ele['umbrella']>=2:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
    ###8QTSUSCOC1
        elif kaudio=='audio_11174':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11175':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11176':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11177':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11178':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11179':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11180':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11181':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11182':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11183':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11184':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11185':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11186':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11187':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11188':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11189':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11190':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11191':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11192':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11193':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11194':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11195':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11196':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11197':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11198':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11199':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11200':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11201':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11202':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11203':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11204':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11205':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11206':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                for paele in patpat_ele:
                    if patpat_ele[q]=='ocotpus':
                        ali_1.append(q+1)
                the_second[k]=1
        elif kaudio=='audio_11207':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11208':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11209':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11210':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11211':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11212':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11213':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11214':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11215':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11216':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11217':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11218':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11219':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11220':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11221':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11222':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11223':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11224':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11225':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11226':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11227':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11228':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11229':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11230':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11231':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11232':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11233':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11234':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11235':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11236':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11237':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11238':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11239':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11240':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11241':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11242':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11243':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11244':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11245':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11246':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11247':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11248':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11249':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11250':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11251':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11252':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11253':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11254':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11255':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11256':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11257':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11258':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11259':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11260':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11261':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11262':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11263':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11264':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11265':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11266':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11267':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11268':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11269':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11270':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11271':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11272':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11273':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11274':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11275':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11276':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11277':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11278':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11279':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11280':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11281':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11282':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11283':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11284':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11285':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11286':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11287':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11288':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11289':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11290':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11291':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11292':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11293':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11294':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11295':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11296':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11297':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11298':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11299':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11300':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11301':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11302':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11303':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11304':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11305':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11306':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11307':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11308':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11309':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11310':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11311':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11312':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11313':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11314':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11315':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11316':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11317':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11318':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11319':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11320':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11321':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11322':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11323':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11324':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11325':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11326':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11327':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11328':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11329':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11330':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11331':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11332':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11333':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11334':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11335':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11336':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11337':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11338':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11339':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11340':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11341':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11342':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11343':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11344':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11345':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11346':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11347':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11348':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11349':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11350':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11351':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11352':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11353':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11354':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11355':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11356':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11357':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11358':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11359':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11360':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11361':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11362':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11363':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11364':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11365':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11366':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11367':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11368':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11369':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11370':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11371':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11372':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11373':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11374':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11375':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11376':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11377':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11378':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11379':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11380':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11381':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11382':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11383':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11384':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11385':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11386':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11387':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11388':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11389':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11390':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11391':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11392':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11393':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11394':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11395':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11396':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11397':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11398':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11399':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11400':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11401':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11402':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11403':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11404':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11405':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11406':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11407':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11408':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11409':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11410':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11411':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11412':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11413':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11414':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11415':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11416':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11417':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11418':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11419':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11420':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11421':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11422':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11423':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11424':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11425':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11426':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11427':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11428':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11429':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11430':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11431':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11432':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11433':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11434':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11435':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11436':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11437':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11438':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11439':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11440':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11441':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11442':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11443':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11444':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11445':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11446':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11447':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11448':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11449':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11450':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11451':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11452':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11453':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11454':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11455':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11456':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11457':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11458':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11459':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11460':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11461':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11462':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11463':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11464':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11465':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11466':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11467':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11468':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11469':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11470':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11471':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11472':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11473':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11474':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11475':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11476':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11477':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11478':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11479':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11480':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11481':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11482':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11483':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11484':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11485':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11486':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11487':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11488':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11489':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11490':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11491':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11492':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11493':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11494':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11495':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11496':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11497':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11498':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11499':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11500':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11501':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11502':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11503':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11504':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11505':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11506':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11507':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11508':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11509':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11510':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11511':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11512':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11513':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11514':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11515':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11516':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11517':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11518':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11519':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11520':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11521':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11522':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11523':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11524':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11525':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11526':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11527':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11528':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11529':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11530':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11531':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11532':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11533':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11534':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11535':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11536':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11537':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11538':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11539':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11540':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11541':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11542':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11543':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11544':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11545':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11546':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11547':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11548':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11549':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11550':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11551':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11552':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11553':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11554':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11555':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11556':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11557':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11558':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11559':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11560':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11561':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11562':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11563':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11564':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11565':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11566':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11567':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11568':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11569':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11570':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11571':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11572':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11573':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11574':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11575':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11576':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11577':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11578':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11579':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11580':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11581':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11582':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11583':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11584':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11585':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11586':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11587':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11588':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11589':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11590':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11591':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11592':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11593':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11594':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11595':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11596':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11597':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11598':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11599':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11600':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11601':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11602':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11603':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11604':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11605':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11606':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11607':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11608':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11609':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11610':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11611':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11612':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11613':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11614':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11615':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11616':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11617':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11618':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11619':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11620':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11621':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11622':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11623':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11624':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11625':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11626':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11627':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11628':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11629':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11630':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11631':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11632':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11633':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11634':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11635':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11636':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11637':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11638':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11639':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11640':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11641':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11642':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11643':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11644':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11645':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11646':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11647':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11648':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11649':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11650':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11651':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11652':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11653':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11654':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11655':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11656':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11657':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11658':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11659':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11660':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11661':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11662':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11663':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11664':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11665':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11666':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11667':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11668':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11669':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11670':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11671':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11672':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11673':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11674':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11675':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11676':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11677':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11678':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11679':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11680':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11681':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11682':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11683':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11684':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11685':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11686':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11687':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11688':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11689':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11690':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11691':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11692':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11693':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11694':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11695':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11696':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11697':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11698':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11699':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11700':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11701':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11702':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11703':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11704':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11705':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11706':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11707':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11708':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11709':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11710':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11711':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11712':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11713':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11714':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11715':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11716':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11717':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11718':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11719':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11720':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11721':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11722':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11723':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11724':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11725':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11726':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11727':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11728':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11729':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11730':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11731':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11732':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11733':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11734':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11735':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11736':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11737':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11738':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11739':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11740':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11741':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11742':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11743':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11744':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11745':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11746':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11747':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11748':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11749':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11750':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11751':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11752':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11753':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11754':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11755':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11756':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11757':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11758':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11759':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11760':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11761':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11762':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11763':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11764':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11765':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11766':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11767':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11768':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11769':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11770':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11771':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11772':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11773':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11774':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11775':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11776':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11777':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11778':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11779':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11780':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11781':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11782':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11783':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11784':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11785':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11786':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11787':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11788':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11789':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11790':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11791':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11792':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11793':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11794':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11795':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11796':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11797':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11798':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11799':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11800':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11801':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11802':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11803':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11804':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11805':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_second[k]=1
        elif kaudio=='audio_11806':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11807':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11808':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11809':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11810':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11811':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11812':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11813':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11814':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11815':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11816':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11817':
            if same_ele['girl']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11818':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11819':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11820':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11821':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11822':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11823':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11824':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11825':
            if same_ele['lion']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11826':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11827':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11828':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11829':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11830':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11831':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11832':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11833':
            if same_ele['monkey']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11834':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11835':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11836':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11837':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11838':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11839':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11840':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11841':
            if same_ele['parrot']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11842':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11843':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11844':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11845':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11846':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11847':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11848':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11849':
            if same_ele['octopus']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11850':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11851':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11852':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11853':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11854':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11855':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11856':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11857':
            if same_ele['icecream']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11858':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11859':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11860':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11861':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11862':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11863':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11864':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11865':
            if same_ele['glasses']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11866':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11867':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11868':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11869':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11870':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11871':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11872':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11873':
            if same_ele['bicycle']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11874':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11875':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11876':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11877':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11878':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        elif kaudio=='audio_11879':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11880':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']
                the_second[k]=1
        elif kaudio=='audio_11881':
            if same_ele['push_scooter']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11882':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
                the_second[k]=1
        elif kaudio=='audio_11883':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']
                the_second[k]=1
        elif kaudio=='audio_11884':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']
                the_second[k]=1
        elif kaudio=='audio_11885':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']
                the_second[k]=1
        elif kaudio=='audio_11886':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']
                the_second[k]=1
        elif kaudio=='audio_11887':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']
                the_second[k]=1
        elif kaudio=='audio_11888':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']
                the_second[k]=1
        elif kaudio=='audio_11889':
            if same_ele['umbrella']>=1:
                ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']
                the_first[k]=1
            else:
                ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']
                the_second[k]=1
        
        if len(ali_1) or len(tot_1):
            tot.append(ali)
            if all_alternative_pos[k] in (0,1):
                tot_1.append(ali) #not ali_1
            else:
                tot_1.append(ali_1)
        else:
            print("partial_ali {}".format(ali))
            tot.append(ali)
        ali=[]
        ali_1=[]
        print("all_alternative_pos={}".format(all_alternative_pos))

 
    all_properties = []
    all_properties.append(tot)
    all_properties.append(tot_1)
    all_properties.append(all_alternative_pos)
    all_properties.append(one_ans_is_enough)
    all_properties.append(multiple_branch_ans)
    all_properties.append(multiple_or_ans)
    all_properties.append(touch_together)
    all_properties.append(no_ans_to_create_only_body)
    all_properties.append(body_response)
    all_properties.append(body_and_patch)
    all_properties.append(need_light)
    all_properties.append(need_light1)
    all_properties.append(need_light2)
    all_properties.append(need_light3)
    all_properties.append(the_first)
    all_properties.append(the_second)
    all_properties.append(body_num_press)
    all_properties.append(all_num_touches_to_do)
    all_properties.append(ques_9_num)
    all_properties.append(limit)
    all_properties.append(limit1)
    all_properties.append(limit2)
    all_properties.append(limit3)
    all_properties.append(limitRed)
    all_properties.append(limitGreen)
    all_properties.append(limitBlue)
    all_properties.append(limitYellow)
    all_properties.append(limitLion)
    all_properties.append(limitMonkey)
    all_properties.append(limitParrot)
    all_properties.append(limitOctopus)
    return all_properties
