############## KIND 2L
def get_ali_2LDSUOC(pe1,pe2,patpat_ele,kidx,all_alternative_pos,need_light,lig,one_ans_is_enough,multiple_branch_ans):
	ali = [i+1 for i,element in enumerate(patpat_ele) if element==pe1]
	ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element==pe2]
	need_light[kidx]=lig
	one_ans_is_enough[kidx]=1
	multiple_branch_ans[kidx]=1
	all_alternative_pos[kidx]=2
	return ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans

def get_ali_2LDBOC(pe,patpat_ele,kidx,body_and_patch,resp_in_body,val_rib,need_light,lig):
	ali = [i+1 for i,element in enumerate(patpat_ele) if element==pe]
	need_light[kidx]=lig
	body_and_patch[kidx]=1
	resp_in_body[kidx]=val_rib
	return ali,body_and_patch,need_light,resp_in_body

def get_ali_2LDSUOB(pe,patpat_ele,kidx,one_ans_is_enough,body_and_patch,resp_in_body,val_rib,need_light,lig):
	ali = [i+1 for i,element in enumerate(patpat_ele) if element==pe]
	need_light[kidx]=lig
	body_and_patch[kidx]=1
	resp_in_body[kidx]=val_rib
	one_ans_is_enough[kidx]=1
	return ali,one_ans_is_enough,need_light,body_and_patch,resp_in_body

def get_ali_2LTWO(pc,patpat_col,kidx,one_ans_is_enough,the_first,val_fir,the_second,val_sec,need_light,lig):
	ali = [i+1 for i,element in enumerate(patpat_col) if element==pc]
	need_light[kidx]=lig
	body_and_patch[kidx]=1
	the_first[kidx]=val_fir
	the_second[kidx]=val_sec
	one_ans_is_enough[kidx]=1
	return ali,one_ans_is_enough,need_light,the_first,the_second

def get_ali_2LDSU2(pe1,pe2,patpat_ele,kidx,all_alternative_pos,need_light,lig,need_light1,lig1,one_ans_is_enough,multiple_branch_ans):
	ali = [i+1 for i,element in enumerate(patpat_ele) if element==pe1]
	ali_1 = [i+1 for i,element in enumerate(patpat_ele) if element==pe2]
	need_light[kidx]=lig
	need_light1[kidx]=lig1
	one_ans_is_enough[kidx]=1
	all_alternative_pos[kidx]=2
	multiple_branch_ans[kidx]=1
	return ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans

def build_Resp(patpat_ele,patpat_col,patpat_gen,listaudios,same_col,same_ele):
	print("WE ARE IN KIND 2L")
	ali,ali_tmp,ali_1,tot,tot_1 = [],[],[],[],[]
	all_alternative_pos=[0,0,0,0]
	one_ans_is_enough=[0,0,0,0]
	multiple_branch_ans=[0,0,0,0]
	multiple_or_ans=[0,0,0,0]
	touch_together=[0,0,0,0]
	no_ans_to_create_only_body=[0,0,0,0]
	body_response=[0,0,0,0]
	body_and_patch=[0,0,0,0]
	need_light=[0,0,0,0]
	need_light1=[0,0,0,0]
	need_light2=[0,0,0,0]
	need_light3=[0,0,0,0]
	the_first=[0,0,0,0]
	the_second=[0,0,0,0]
	body_num_press=[0,0,0,0]
	all_num_touches_to_do=[0,0,0,0]
	ques_9_num = [0,0,0,0]
	limit = [0,0,0,0]
	limit1 = [0,0,0,0]
	limit2 = [0,0,0,0]
	limit3 = [0,0,0,0]
	limitRed = [0,0,0,0]
	limitGreen = [0,0,0,0]
	limitBlue = [0,0,0,0]
	limitYellow  = [0,0,0,0]
	limitLion = [0,0,0,0]
	limitMonkey = [0,0,0,0]
	limitParrot = [0,0,0,0]
	limitOctopus = [0,0,0,0]
	for k,kaudio in enumerate(listaudios):

	###2LDSUOC
		########################################### monkey
		if kaudio=='audio_1895':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1896':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1897':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1898':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1899':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1900':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1901':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1902':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1903':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### monkey
		elif kaudio=='audio_1904':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1905':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1906':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1907':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1908':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1909':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1910':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1911':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1912':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
	
		########################################### monkeygirl	
		elif kaudio=='audio_1913':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1914':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1915':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1916':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1917':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1918':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1919':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1920':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1921':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		########################################### monkey
		elif kaudio=='audio_1922':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1923':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1924':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1925':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1926':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1927':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1928':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1929':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1930':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		################################################################### lion
		elif kaudio=='audio_1931':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','monkey',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1932':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1933':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1934':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1935':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1936':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1937':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1938':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1939':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','girl',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### lion
		elif kaudio=='audio_1940':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','monkey',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1941':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1942':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1943':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1944':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1945':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1946':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1947':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1948':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','girl',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		########################################### lion
		elif kaudio=='audio_1949':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','monkey',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1950':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1951':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1952':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1953':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1954':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1955':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1956':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1957':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','girl',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		########################################### lion
		elif kaudio=='audio_1958':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','monkey',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1959':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1960':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1961':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1962':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1963':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1964':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1965':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1966':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('lion','girl',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		###################################################################################### parrot
		elif kaudio=='audio_1967':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','lion',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1968':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','monkey',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1969':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','octopus',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1970':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1971':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1972':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1973':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1974':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1975':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### parrot
		elif kaudio=='audio_1976':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','lion',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1977':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','monkey',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1978':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','octopus',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1979':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1980':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1981':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1982':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1983':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1984':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		########################################### parrot
		elif kaudio=='audio_1985':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','lion',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1986':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','monkey',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1987':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','octopus',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1988':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1989':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1990':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1991':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1992':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1993':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		########################################### parrot
		elif kaudio=='audio_1994':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','lion',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1995':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','monkey',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1996':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','octopus',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1997':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1998':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_1999':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2000':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2001':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2002':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		###################################################################################### octopus
		elif kaudio=='audio_2003':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','lion',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2004':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','monkey',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2005':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2006':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2007':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2008':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2009':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2010':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2011':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### octopus
		elif kaudio=='audio_2012':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','lion',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2013':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','monkey',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2014':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2015':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2016':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2017':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2018':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2019':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2020':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		########################################### octopus
		elif kaudio=='audio_2021':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','lion',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2022':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','monkey',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2023':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2024':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2025':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2026':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2027':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2028':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2029':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		########################################### octopus
		elif kaudio=='audio_2030':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','lion',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2031':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','monkey',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2032':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2033':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2034':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2035':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2036':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2037':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2038':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		########################################### icecream
		elif kaudio=='audio_2039':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','lion',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2040':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','monkey',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2041':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','parrot',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2042':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','octopus',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2043':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2044':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2045':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2046':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2047':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### icecream
		elif kaudio=='audio_2048':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','lion',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2049':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','monkey',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2050':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','parrot',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2051':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','octopus',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2052':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2053':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2054':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2055':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2056':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		########################################### icecream
		elif kaudio=='audio_2057':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','lion',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2058':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','monkey',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2059':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','parrot',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2060':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','octopus',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2061':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2062':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2063':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2064':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2065':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		########################################### icecream
		elif kaudio=='audio_2066':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','lion',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2067':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','monkey',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2068':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','parrot',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2069':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','octopus',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2070':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2071':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2072':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2073':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2074':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		###################################################################################### glasses
		elif kaudio=='audio_2075':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','lion',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2076':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','monkey',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2077':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','parrot',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2078':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','octopus',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2079':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','icecream',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2080':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2081':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2082':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2083':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','girl',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### glasses
		elif kaudio=='audio_2084':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','lion',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2085':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','monkey',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2086':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','parrot',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2087':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','octopus',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2088':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','icecream',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2089':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2090':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2091':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2092':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','girl',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		########################################### glasses
		elif kaudio=='audio_2093':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','lion',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2094':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','monkey',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2095':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','parrot',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2096':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','octopus',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2097':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','icecream',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2098':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2099':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2100':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2101':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','girl',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		########################################### glasses
		elif kaudio=='audio_2102':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','lion',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2103':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','monkey',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2104':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','parrot',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2105':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','octopus',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2106':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','icecream',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2107':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2108':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2109':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2110':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('glasses','girl',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		###################################################################################### bicycle
		elif kaudio=='audio_2111':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','lion',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2112':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','monkey',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2113':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','parrot',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2114':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','octopus',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2115':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','icecream',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2116':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','glasses',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2117':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2118':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2119':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','girl',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### bicycle
		elif kaudio=='audio_2120':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','lion',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2121':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','monkey',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2122':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','parrot',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2123':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','octopus',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2124':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','icecream',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2125':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','glasses',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2126':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2127':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2128':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','girl',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### bicycle
		elif kaudio=='audio_2129':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','lion',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2130':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','monkey',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2131':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','parrot',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2132':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','octopus',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2133':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','icecream',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2134':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','glasses',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2135':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2136':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2137':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','girl',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		########################################### bicycle
		elif kaudio=='audio_2138':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','lion',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2139':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','monkey',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2140':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','parrot',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2141':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','octopus',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2142':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','icecream',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2143':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','glasses',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2144':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2145':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2146':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('bicycle','girl',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		###################################################################################### push_scooter
		elif kaudio=='audio_2147':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','lion',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2148':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','monkey',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2149':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','parrot',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2150':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','octopus',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2151':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','icecream',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2152':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','glasses',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2153':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2154':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2155':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','girl',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### push_scooter
		elif kaudio=='audio_2156':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','lion',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2157':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','monkey',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2158':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','parrot',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2159':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','octopus',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2160':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','icecream',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2161':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','glasses',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2162':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2163':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2164':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','girl',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		########################################### push_scooter
		elif kaudio=='audio_2165':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','lion',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2166':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','monkey',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2167':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','parrot',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2168':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','octopus',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2169':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','icecream',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2170':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','glasses',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2171':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2172':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2173':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','girl',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		########################################### push_scooter
		elif kaudio=='audio_2174':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','lion',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2175':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','monkey',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2176':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','parrot',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2177':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','octopus',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2178':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','icecream',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2179':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','glasses',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2180':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2181':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2182':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('push_scooter','girl',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		###################################################################################### umbrella
		elif kaudio=='audio_2183':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','lion',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2184':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','monkey',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2185':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','parrot',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2186':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','octopus',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2187':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','icecream',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2188':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2189':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2190':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2191':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### umbrella
		elif kaudio=='audio_2192':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','lion',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2193':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','monkey',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2194':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','parrot',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2195':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','octopus',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2196':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','icecream',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2197':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2198':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2199':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2200':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		########################################### umbrella
		elif kaudio=='audio_2201':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','lion',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2202':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','monkey',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2203':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','parrot',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2204':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','octopus',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2205':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','icecream',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2206':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2207':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2208':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2209':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		########################################### umbrella
		elif kaudio=='audio_2210':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','lion',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2211':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','monkey',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2212':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','parrot',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2213':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','octopus',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2214':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','icecream',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2215':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2216':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2217':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2218':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		###################################################################################### girl
		elif kaudio=='audio_2219':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','lion',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2220':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','monkey',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2221':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','parrot',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2222':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','octopus',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2223':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','icecream',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2224':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2225':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2226':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2227':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,one_ans_is_enough,multiple_branch_ans)
		########################################### girl
		elif kaudio=='audio_2228':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','lion',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2229':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','monkey',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2230':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','parrot',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2231':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','octopus',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2232':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','icecream',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2233':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2234':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2235':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2236':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,one_ans_is_enough,multiple_branch_ans)
		########################################### girl
		elif kaudio=='audio_2237':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','lion',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2238':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','monkey',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2239':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','parrot',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2240':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','octopus',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2241':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','icecream',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2242':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2243':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2244':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2245':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,one_ans_is_enough,multiple_branch_ans)
		########################################### girl
		elif kaudio=='audio_2246':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','lion',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2247':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','monkey',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2248':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','parrot',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2249':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','octopus',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2250':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','icecream',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2251':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2252':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2253':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2254':
			ali,ali_1,need_light,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSUOC('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,one_ans_is_enough,multiple_branch_ans)
	###2LDBOB
		elif kaudio=='audio_2415':
			no_ans_to_create_only_body=1
			need_light[k] = 0
			ali.append(11)
		elif kaudio=='audio_2416':
			no_ans_to_create_only_body=1
			need_light[k] = 1
			ali.append(11)
		elif kaudio=='audio_2417':
			no_ans_to_create_only_body=1
			need_light[k] = 3
			ali.append(11)	
		elif kaudio=='audio_2418':
			no_ans_to_create_only_body=1
			need_light[k] = 2
			ali.append(11)
		elif kaudio=='audio_2419':
			no_ans_to_create_only_body=1
			need_light[k] = 0
			ali.append(11)
		elif kaudio=='audio_2420':
			no_ans_to_create_only_body=1
			need_light[k] = 1
			ali.append(11)
		elif kaudio=='audio_2421':
			no_ans_to_create_only_body=1
			need_light[k] = 3
			ali.append(11)
		elif kaudio=='audio_2422':
			no_ans_to_create_only_body=1
			need_light[k] = 2
			ali.append(11)
	###2LDBOC
		elif kaudio=='audio_2255':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('monkey',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2256':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('lion',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2257':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('parrot',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2258':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('octopus',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2259':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('icecream',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2260':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('glasses',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2261':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('bicycle',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2262':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('push_scooter',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2263':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('umbrella',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2264':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('girl',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2265':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('monkey',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2266':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('lion',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2267':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('parrot',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2268':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('octopus',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2269':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('icecream',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2270':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('glasses',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2271':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('bicycle',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2272':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('push_scooter',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2273':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('umbrella',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2274':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('girl',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2275':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('monkey',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2276':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('lion',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2277':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('parrot',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2278':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('octopus',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2279':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('icecream',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2280':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('glasses',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2281':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('bicycle',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2282':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('push_scooter',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2283':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('umbrella',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2284':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('girl',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2285':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('monkey',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2286':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('lion',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2287':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('parrot',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2288':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('octopus',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2289':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('icecream',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2290':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('glasses',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2291':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('bicycle',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2292':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('push_scooter',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2293':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('umbrella',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2294':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('girl',patpat_ele,k,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2295':	
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('monkey',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2296':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('lion',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2297':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('parrot',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2298':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('octopus',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2299':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('icecream',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2300':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('glasses',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2301':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('bicycle',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2302':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('push_scooter',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2303':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('umbrella',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2304':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('girl',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2305':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('monkey',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2306':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('lion',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2307':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('parrot',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2308':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('octopus',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2309':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('icecream',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2310':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('glasses',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2311':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('bicycle',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2312':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('push_scooter',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2313':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('umbrella',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2314':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('girl',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2315':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('monkey',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2316':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('lion',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2317':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('parrot',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2318':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('octopus',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2319':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('icecream',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2320':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('glasses',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2321':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('bicycle',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2322':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('push_scooter',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2323':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('umbrella',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2324':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('girl',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2325':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('monkey',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2326':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('lion',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2327':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('parrot',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2328':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('octopus',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2329':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('icecream',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2330':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('glasses',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2331':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('bicycle',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2332':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('push_scooter',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2333':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('umbrella',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2334':
			ali,body_and_patch,need_light,resp_in_body = get_ali_2LDBOC('girl',patpat_ele,k,body_and_patch,resp_in_body,0,need_light,2)
	###2LDSU
		elif kaudio=='audio_1855':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
			need_light[k] = 0 
		elif kaudio=='audio_1856':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
			need_light[k] = 1 
		elif kaudio=='audio_1857':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
			need_light[k] = 2 
		elif kaudio=='audio_1858':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='monkey']
			need_light[k] = 3 
		elif kaudio=='audio_1859':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']	
			need_light[k] = 0 
		elif kaudio=='audio_1860':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']			
			need_light[k] = 1
		elif kaudio=='audio_1861':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']			
			need_light[k] = 2
		elif kaudio=='audio_1862':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='lion']			
			need_light[k] = 3
		elif kaudio=='audio_1863':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']			
			need_light[k] = 0
		elif kaudio=='audio_1864':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']			
			need_light[k] = 1
		elif kaudio=='audio_1865':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']			
			need_light[k] = 2
		elif kaudio=='audio_1866':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='parrot']			
			need_light[k] = 3
		elif kaudio=='audio_1867':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']			
			need_light[k] = 0
		elif kaudio=='audio_1868':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']			
			need_light[k] = 1
		elif kaudio=='audio_1869':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']			
			need_light[k] = 2		
		elif kaudio=='audio_1870':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='octopus']			
			need_light[k] = 3
		elif kaudio=='audio_1871':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']			
			need_light[k] = 0
		elif kaudio=='audio_1872':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']			
			need_light[k] = 1
		elif kaudio=='audio_1873':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']			
			need_light[k] = 2
		elif kaudio=='audio_1874':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='icecream']			
			need_light[k] = 3
		elif kaudio=='audio_1875':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']			
			need_light[k] = 0 
		elif kaudio=='audio_1876':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']			
			need_light[k] = 1
		elif kaudio=='audio_1877':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']			
			need_light[k] = 2
		elif kaudio=='audio_1878':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='glasses']			
			need_light[k] = 3
		elif kaudio=='audio_1879':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']			
			need_light[k] = 0
		elif kaudio=='audio_1880':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']			
			need_light[k] = 1
		elif kaudio=='audio_1881':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']			
			need_light[k] = 2
		elif kaudio=='audio_1882':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='bicycle']			
			need_light[k] = 3
		elif kaudio=='audio_1883':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']			
			need_light[k] = 0
		elif kaudio=='audio_1884':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']			
			need_light[k] = 1
		elif kaudio=='audio_1885':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']			
			need_light[k] = 2
		elif kaudio=='audio_1886':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='push_scooter']			
			need_light[k] = 3
		elif kaudio=='audio_1887':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']			
			need_light[k] = 0 
		elif kaudio=='audio_1888':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']			
			need_light[k] = 1
		elif kaudio=='audio_1889':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']			
			need_light[k] = 2
		elif kaudio=='audio_1890':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='umbrella']			
			need_light[k] = 3
		elif kaudio=='audio_1891':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']			
			need_light[k] = 0
		elif kaudio=='audio_1892':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']			
			need_light[k] = 1
		elif kaudio=='audio_1893':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']			
			need_light[k] = 2
		elif kaudio=='audio_1894':
			ali = [i+1 for i,element in enumerate(patpat_ele) if element=='girl']			
			need_light[k] = 3
	###2LDSUOB
		elif kaudio=='audio_2335':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('monkey',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2336':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('monkey',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2337':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('monkey',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2338':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('monkey',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2339':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('monkey',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2340':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('monkey',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2341':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('monkey',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2342':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('monkey',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2343':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('lion',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2344':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('lion',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2345':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('lion',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2346':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('lion',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2347':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('lion',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2348':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('lion',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2349':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('lion',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2350':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('lion',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2351':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('parrot',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2352':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('parrot',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2353':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('parrot',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2354':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('parrot',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2355':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('parrot',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2356':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('parrot',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2357':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('parrot',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2358':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('parrot',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2359':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('octopus',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2360':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('octopus',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2361':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('octopus',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2362':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('octopus',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2363':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('octopus',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2364':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('octopus',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2365':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('octopus',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2366':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('octopus',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2367':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('icecream',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2368':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('icecream',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2369':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('icecream',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2370':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('icecream',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2371':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('icecream',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2372':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('icecream',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2373':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('icecream',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2374':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('icecream',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2375':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('glasses',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2376':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('glasses',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2377':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('glasses',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2378':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('glasses',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2379':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('glasses',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2380':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('glasses',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2381':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('glasses',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2382':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('glasses',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2383':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('bicycle',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2384':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('bicycle',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2385':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('bicycle',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2386':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('bicycle',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2387':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('bicycle',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2388':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('bicycle',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2389':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('bicycle',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2390':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('bicycle',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2391':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('push_scooter',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2392':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('push_scooter',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2393':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('push_scooter',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2394':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('push_scooter',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2395':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('push_scooter',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2396':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('push_scooter',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2397':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('push_scooter',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2398':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('push_scooter',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2399':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('umbrella',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2400':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('umbrella',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2401':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('umbrella',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2402':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('umbrella',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2403':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('umbrella',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2404':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('umbrella',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2405':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('umbrella',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2406':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('umbrella',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,2)
		elif kaudio=='audio_2407':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('girl',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,0)
		elif kaudio=='audio_2408':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('girl',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,0)
		elif kaudio=='audio_2409':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('girl',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,1)
		elif kaudio=='audio_2410':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('girl',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,1)
		elif kaudio=='audio_2411':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('girl',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,3)
		elif kaudio=='audio_2412':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('girl',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,3)
		elif kaudio=='audio_2413':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('girl',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,0,need_light,2)
		elif kaudio=='audio_2414':
			ali,one_ans_is_enough,body_and_patch,need_light = get_ali_2LDBOC('girl',patpat_ele,k,one_ans_is_enough,body_and_patch,resp_in_body,1,need_light,2)
	###2LST
		elif kaudio=='audio_1803':
			ali = [i+1 for i,col in enumerate(patpat_col) if col=='Red']
			need_light[k]=0
		elif kaudio=='audio_1804':
			ali = [i+1 for i,col in enumerate(patpat_col) if col=='Green']
			need_light[k]=1
		elif kaudio=='audio_1805':
			ali = [i+1 for i,col in enumerate(patpat_col) if col=='Yellow']
			need_light[k]=2
		elif kaudio=='audio_1806':
			ali = [i+1 for i,col in enumerate(patpat_col) if col=='Blue']
			need_light[k]=3
	###2LSU
		elif kaudio=='audio_1807':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Red']
			need_light[k]=0
		elif kaudio=='audio_1808':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Green']
			need_light[k]=1
		elif kaudio=='audio_1809':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Blue']
			need_light[k]=2
		elif kaudio=='audio_1810':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='monkey' and pc=='Yellow']
			need_light[k]=3
		elif kaudio=='audio_1811':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Red']
			need_light[k]=0
		elif kaudio=='audio_1812':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Green']
			need_light[k]=1
		elif kaudio=='audio_1813':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Blue']
			need_light[k]=2
		elif kaudio=='audio_1814':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='lion' and pc=='Yellow']
			need_light[k]=3
		elif kaudio=='audio_1815':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Red']
			need_light[k]=0
		elif kaudio=='audio_1816':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Green']
			need_light[k]=1
		elif kaudio=='audio_1817':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Blue']
			need_light[k]=2
		elif kaudio=='audio_1818':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='parrot' and pc=='Yellow']
			need_light[k]=3
		elif kaudio=='audio_1819':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Red']
			need_light[k]=0
		elif kaudio=='audio_1820':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Green']
			need_light[k]=1
		elif kaudio=='audio_1821':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Blue']
			need_light[k]=2
		elif kaudio=='audio_1822':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='octopus' and pc=='Yellow']
			need_light[k]=3
		elif kaudio=='audio_1823':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Red']
			need_light[k]=0
		elif kaudio=='audio_1824':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Green']
			need_light[k]=1
		elif kaudio=='audio_1825':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Blue']
			need_light[k]=2
		elif kaudio=='audio_1826':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='icecream' and pc=='Yellow']
			need_light[k]=3
		elif kaudio=='audio_1827':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Red']
			need_light[k]=0
		elif kaudio=='audio_1828':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Green']
			need_light[k]=1
		elif kaudio=='audio_1829':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Blue']
			need_light[k]=2
		elif kaudio=='audio_1830':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='glasses' and pc=='Yellow']
			need_light[k]=3
		elif kaudio=='audio_1831':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Red']
			need_light[k]=0
		elif kaudio=='audio_1832':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Green']
			need_light[k]=1
		elif kaudio=='audio_1833':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Blue']
			need_light[k]=2
		elif kaudio=='audio_1834':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='bicycle' and pc=='Yellow']
			need_light[k]=3
		elif kaudio=='audio_1835':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Red']
			need_light[k]=0
		elif kaudio=='audio_1836':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Green']
			need_light[k]=1
		elif kaudio=='audio_1837':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Blue']
			need_light[k]=2
		elif kaudio=='audio_1838':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='push_scooter' and pc=='Yellow']
			need_light[k]=3
		elif kaudio=='audio_1839':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Red']
			need_light[k]=0
		elif kaudio=='audio_1840':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Green']
			need_light[k]=1
		elif kaudio=='audio_1841':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Blue']
			need_light[k]=2
		elif kaudio=='audio_1842':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='umbrella' and pc=='Yellow']
			need_light[k]=3
		elif kaudio=='audio_1843':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Red']
			need_light[k]=0
		elif kaudio=='audio_1844':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Green']
			need_light[k]=1
		elif kaudio=='audio_1845':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Blue']
			need_light[k]=2
		elif kaudio=='audio_1846':
			ali = [i+1 for i, (pe, pc) in enumerate(zip(patpat_ele,patpat_col)) if pe=='girl' and pc=='Yellow']
			need_light[k]=3
	###2LTWO
		elif kaudio=='audio_1847':
			ali,one_ans_is_enough,need_light,the_first,the_second = get_ali_2LTWO('Red',patpat_col,k,one_ans_is_enough,the_first,1,the_second,0,need_light,0)
		elif kaudio=='audio_1848':
			ali,one_ans_is_enough,need_light,the_first,the_second = get_ali_2LTWO('Green',patpat_col,k,one_ans_is_enough,the_first,1,the_second,0,need_light,1)
		elif kaudio=='audio_1849':
			ali,one_ans_is_enough,need_light,the_first,the_second = get_ali_2LTWO('Blue',patpat_col,k,one_ans_is_enough,the_first,1,the_second,0,need_light,2)
		elif kaudio=='audio_1850':
			ali,one_ans_is_enough,need_light,the_first,the_second = get_ali_2LTWO('Yellow',patpat_col,k,one_ans_is_enough,the_first,1,the_second,0,need_light,3)
		elif kaudio=='audio_1851':
			ali,one_ans_is_enough,need_light,the_first,the_second = get_ali_2LTWO('Red',patpat_col,k,one_ans_is_enough,the_first,0,the_second,1,need_light,0)
		elif kaudio=='audio_1852':
			ali,one_ans_is_enough,need_light,the_first,the_second = get_ali_2LTWO('Green',patpat_col,k,one_ans_is_enough,the_first,0,the_second,1,need_light,1)
		elif kaudio=='audio_1853':
			ali,one_ans_is_enough,need_light,the_first,the_second = get_ali_2LTWO('Blue',patpat_col,k,one_ans_is_enough,the_first,0,the_second,1,need_light,2)
		elif kaudio=='audio_1854':
			ali,one_ans_is_enough,need_light,the_first,the_second = get_ali_2LTWO('Yellow',patpat_col,k,one_ans_is_enough,the_first,0,the_second,1,need_light,3)
	###2LDSU2
		############################################################ monkey - lion
		elif kaudio=='audio_2423':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2424':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2425':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2426':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2427':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2428':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2429':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2430':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2431':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2432':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2433':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2434':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','lion',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ monkey - parrot
		elif kaudio=='audio_2435':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2436':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2437':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2438':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2439':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2440':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2441':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2442':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2443':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2444':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2445':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2446':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','parrot',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ monkey - octopus
		elif kaudio=='audio_2447':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2448':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2449':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2450':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2451':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2452':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2453':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2454':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2455':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2456':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2457':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2458':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','octopus',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ monkey - icecream
		elif kaudio=='audio_2459':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2460':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2461':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2462':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2463':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2464':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2465':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2466':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2467':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2468':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2469':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2470':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ monkey - girl
		elif kaudio=='audio_2471':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2472':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2473':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2474':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2475':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2476':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2477':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2478':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2479':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2480':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2481':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2482':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ monkey - glasses
		elif kaudio=='audio_2483':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2484':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2485':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2486':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2487':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2488':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2489':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2490':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2491':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2492':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2493':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2494':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ monkey - bicycle
		elif kaudio=='audio_2495':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2496':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2497':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2498':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2499':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2500':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2501':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2502':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2503':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2504':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2505':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2506':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ monkey - push_scooter
		elif kaudio=='audio_2507':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2508':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2509':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2510':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2511':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2512':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2513':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2514':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2515':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2516':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2517':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2518':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ monkey - umbrella
		elif kaudio=='audio_2519':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2520':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2521':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2522':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2523':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2524':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2525':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2526':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2527':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2528':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2529':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2530':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('monkey','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ lion - parrot
		elif kaudio=='audio_2531':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2532':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2533':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2534':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2535':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2536':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2537':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2538':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2539':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2540':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2541':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2542':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','parrot',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ lion - octopus
		elif kaudio=='audio_2543':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2544':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2545':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2546':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2547':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2548':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2549':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2550':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2551':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2552':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2553':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2554':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','octopus',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ lion - icecream
		elif kaudio=='audio_2555':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2556':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2557':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2558':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2559':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2560':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2561':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2562':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2563':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2564':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2565':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2566':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ lion - girl
		elif kaudio=='audio_2567':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2568':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2569':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2570':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2571':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2572':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2573':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2574':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2575':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2576':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2577':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2578':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ lion - glasses
		elif kaudio=='audio_2579':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2580':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2581':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2582':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2583':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2584':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2585':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2586':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2587':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2588':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2589':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2590':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ lion - bicycle
		elif kaudio=='audio_2591':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2592':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2593':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2594':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2595':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2596':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2597':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2598':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2599':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2600':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2601':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2602':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ lion - push_scooter
		elif kaudio=='audio_2603':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2604':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2605':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2606':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2607':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2608':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2609':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2610':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2611':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2612':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2613':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2614':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ lion - umbrella
		elif kaudio=='audio_2615':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2616':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2617':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2618':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2619':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2620':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2621':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2622':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2623':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2624':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2625':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2626':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('lion','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ octopus - parrot
		elif kaudio=='audio_2627':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2628':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2629':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2630':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2631':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2632':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2633':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2634':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2635':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2636':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2637':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2638':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','parrot',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ octopus - icecream
		elif kaudio=='audio_2639':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2640':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2641':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2642':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2643':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2644':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2645':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2646':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2647':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2648':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2649':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2650':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ octopus - girl
		elif kaudio=='audio_2651':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2652':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2653':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2654':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2655':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2656':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2657':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2658':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2659':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2660':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2661':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2662':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ octopus - glasses
		elif kaudio=='audio_2663':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2664':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2665':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2666':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2667':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2668':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2669':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2670':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2671':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2672':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2673':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2674':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ octopus - bicycle
		elif kaudio=='audio_2675':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2676':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2677':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2678':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2679':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2680':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2681':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2682':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2683':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2684':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2685':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2686':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		################################################ octopus - push_scooter
		if kaudio=='audio_2687':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2688':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2689':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2690':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2691':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2692':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2693':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2694':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)		
		elif kaudio=='audio_2695':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)		
		elif kaudio=='audio_2696':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)		
		elif kaudio=='audio_2697':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)		
		elif kaudio=='audio_2698':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ octopus - umbrella
		elif kaudio=='audio_2699':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2700':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2701':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2702':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2703':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2704':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2705':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2706':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2707':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2708':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2709':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2710':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('octopus','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ parrot - icecream
		elif kaudio=='audio_2711':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2712':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2713':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2714':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2715':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2716':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2717':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2718':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2719':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2720':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2721':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2722':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','icecream',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ parrot - girl
		elif kaudio=='audio_2723':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2724':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2725':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2726':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2727':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2728':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2729':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2730':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2731':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2732':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2733':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2734':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ parrot - glasses
		elif kaudio=='audio_2735':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2736':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2737':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2738':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2739':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2740':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2741':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2742':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2743':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2744':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2745':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2746':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ parrot - bicycle
		elif kaudio=='audio_2747':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2748':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2749':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2750':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2751':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2752':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2753':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2754':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2755':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2756':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2757':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2758':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ parrot - push_scooter
		elif kaudio=='audio_2759':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2760':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2761':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2762':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2763':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2764':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2765':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2766':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2767':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2768':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2769':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2770':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ parrot - umbrella
		elif kaudio=='audio_2771':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2772':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2773':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2774':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2775':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2776':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2777':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2778':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2779':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2780':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2781':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2782':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('parrot','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ icecream - girl
		elif kaudio=='audio_2783':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2784':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2785':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2786':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2787':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2788':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2789':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2790':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2791':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2792':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2793':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2794':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ icecream - glasses
		elif kaudio=='audio_2795':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2796':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2797':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2798':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2799':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2800':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2801':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2802':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2803':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2804':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2805':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2806':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ icecream - bicycle
		elif kaudio=='audio_2807':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2808':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2809':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2810':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2811':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2812':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2813':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2814':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2815':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2816':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2817':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2818':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ icecream - push_scooter
		elif kaudio=='audio_2819':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2820':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2821':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2822':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2823':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2824':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2825':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2826':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2827':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2828':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2829':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2830':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ icecream - umbrella
		elif kaudio=='audio_2831':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2832':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2833':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2834':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2835':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2836':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2837':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2838':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2839':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2840':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2841':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2842':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('icecream','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ glasses - bicycle
		elif kaudio=='audio_2843':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2844':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2845':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2846':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2847':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2848':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2849':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2850':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2851':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2852':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2853':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2854':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ glasses - push_scooter
		elif kaudio=='audio_2855':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2856':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2857':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2858':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2859':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2860':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2861':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2862':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2863':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2864':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2865':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2866':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ glasses - umbrella
		elif kaudio=='audio_2867':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2868':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2869':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2870':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2871':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2872':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2873':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2874':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2875':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2876':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2877':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2878':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('glasses','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ bicycle - push_scooter
		elif kaudio=='audio_2879':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2880':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2881':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2882':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2883':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2884':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2885':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2886':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2887':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2888':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2889':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2890':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ bicycle - umbrella
		elif kaudio=='audio_2891':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2892':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2893':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2894':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2895':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2896':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2897':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2898':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2899':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2900':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2901':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2902':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('bicycle','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ push_scooter - umbrella
		elif kaudio=='audio_2903':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2904':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2905':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2906':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2907':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2908':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2909':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2910':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2911':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2912':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2913':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2914':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('push_scooter','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ umbrella - glasses
		elif kaudio=='audio_2915':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2916':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2917':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2918':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2919':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2920':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2921':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2922':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2923':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2924':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2925':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2926':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ umbrella - bicycle
		elif kaudio=='audio_2927':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2928':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2929':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2930':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2931':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2932':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2933':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2934':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2935':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2936':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2937':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2938':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ umbrella - push_scooter
		elif kaudio=='audio_2939':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2940':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2941':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2942':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2943':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2944':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2945':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2946':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2947':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2948':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2949':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2950':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ umbrella - girl
		elif kaudio=='audio_2951':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2952':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2953':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2954':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2955':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2956':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2957':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2958':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2959':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2960':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2961':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2962':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('umbrella','girl',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ girl - glasses
		elif kaudio=='audio_2963':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2964':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2965':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2966':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2967':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2968':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2969':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2970':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2971':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2972':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2973':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2974':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','glasses',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ girl - bicycle
		elif kaudio=='audio_2975':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2976':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2977':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2978':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2979':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2980':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2981':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2982':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2983':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2984':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2985':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2986':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','bicycle',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ girl - push_scooter
		elif kaudio=='audio_2987':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2988':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2989':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2990':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2991':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2992':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2993':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2994':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2995':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2996':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2997':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_2998':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','push_scooter',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		############################################################ girl - umbrella
		elif kaudio=='audio_2999':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3000':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3001':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,0,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3002':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3003':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3004':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,1,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3005':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3006':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,1,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3007':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,3,need_light1,2,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3008':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,0,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3009':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,3,one_ans_is_enough,multiple_branch_ans)
		elif kaudio=='audio_3010':
			ali,ali_1,need_light,need_light1,all_alternative_pos,one_ans_is_enough,multiple_branch_ans = get_ali_2LDSU2('girl','umbrella',patpat_ele,k,all_alternative_pos,need_light,2,need_light1,1,one_ans_is_enough,multiple_branch_ans)

		if len(ali_1) or len(tot_1):
			tot.append(ali)
			if all_alternative_pos[k] in (0,1):
				tot_1.append(ali) #not ali_1
			else:
				tot_1.append(ali_1)
		else:
			print("partial_ali {}".format(ali))
			tot.append(ali)
		ali=[]
		ali_1=[]

	all_properties = []
	all_properties.append(tot)
	all_properties.append(tot_1)
	all_properties.append(all_alternative_pos)
	all_properties.append(one_ans_is_enough)
	all_properties.append(multiple_branch_ans)
	all_properties.append(multiple_or_ans)
	all_properties.append(touch_together)
	all_properties.append(no_ans_to_create_only_body)
	all_properties.append(body_response)
	all_properties.append(body_and_patch)
	all_properties.append(need_light)
	all_properties.append(need_light1)
	all_properties.append(need_light2)
	all_properties.append(need_light3)
	all_properties.append(the_first)
	all_properties.append(the_second)
	all_properties.append(body_num_press)
	all_properties.append(all_num_touches_to_do)
	all_properties.append(ques_9_num)
	all_properties.append(limit)
	all_properties.append(limit1)
	all_properties.append(limit2)
	all_properties.append(limit3)
	all_properties.append(limitRed)
	all_properties.append(limitGreen)
	all_properties.append(limitBlue)
	all_properties.append(limitYellow)
	all_properties.append(limitLion)
	all_properties.append(limitMonkey)
	all_properties.append(limitParrot)
	all_properties.append(limitOctopus)
	return all_properties
