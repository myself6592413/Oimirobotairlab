# ==========================================================================================================================================================
#  Imports:
# ==========================================================================================================================================================
import os
import path
import io
import sys
import shutil
from distutils.core import setup
import setuptools
from distutils.extension import Extension
from Cython.Build import build_ext
from Cython.Build import cythonize
import numpy
from multiprocessing import Pool

#readme_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), './README.md')
included_path_0 = os.path.abspath('./session/')
included_path_1 = os.path.abspath('./scenario/')
included_path_2 = os.path.abspath('./match/')
included_path_3 = os.path.abspath('./game/')
included_path_4 = os.path.abspath('./games_classes/')
included_path_5 = os.path.abspath('./score/')
included_path_6 = os.path.abspath('.')
included_path_7 = os.path.abspath('/home/pi/OIMI/oimi_code/src/managers/database_manager/')
included_path_8 = os.path.abspath('../managers/database_manager/oimi_state_machine/')
included_path_9 = os.path.abspath('./games_reactions/')
included_path_10 = os.path.abspath('./game_letsplay/')

if 'SRC_PATH' not in os.environ:
	os.environ['included_path_0'] = included_path_0
	os.environ['included_path_1'] = included_path_1
	os.environ['included_path_2'] = included_path_2
	os.environ['included_path_3'] = included_path_3
	os.environ['included_path_4'] = included_path_4
	os.environ['included_path_5'] = included_path_5
	os.environ['included_path_6'] = included_path_6
	os.environ['included_path_7'] = included_path_7
	os.environ['included_path_8'] = included_path_8
	os.environ['included_path_9'] = included_path_9
	os.environ['included_path_10'] = included_path_10
else:
	included_path_0 = os.environ['included_path_0']
	included_path_1 = os.environ['included_path_1']
	included_path_2 = os.environ['included_path_2']
	included_path_3 = os.environ['included_path_3']
	included_path_4 = os.environ['included_path_4']
	included_path_5 = os.environ['included_path_5']
	included_path_6 = os.environ['included_path_6']
	included_path_7 = os.environ['included_path_7']
	included_path_8 = os.environ['included_path_8']
	included_path_9 = os.environ['included_path_9']
	included_path_10 = os.environ['included_path_10']
# ==========================================================================================================================================================
#  Modules:
# ==========================================================================================================================================================
ext_modules = [
	Extension("session", ["session/session.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1, included_path_2, included_path_3, included_path_4, included_path_5, included_path_6, included_path_7, included_path_8, included_path_9, included_path_10],
		language='c++'),
	Extension("match", ["match/match.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1, included_path_2, included_path_3, included_path_4, included_path_5, included_path_6, included_path_7, included_path_8, included_path_9, included_path_10],
		language='c++'),
	Extension("game", ["game/game.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1, included_path_2, included_path_3, included_path_4, included_path_5, included_path_6, included_path_7, included_path_8, included_path_9, included_path_10],
		language='c++'),
	Extension("scenario", ["scenario/scenario.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1, included_path_2, included_path_3, included_path_4, included_path_5, included_path_6, included_path_7, included_path_8, included_path_9, included_path_10],
		language='c++'),
	Extension("games_classes", ["games_classes/games_classes.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1, included_path_2, included_path_3, included_path_4, included_path_5, included_path_6, included_path_7, included_path_8, included_path_9, included_path_10],
		language='c++'),
	Extension("score", ["score/score.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1, included_path_2, included_path_3, included_path_4, included_path_5, included_path_6, included_path_7, included_path_8, included_path_9, included_path_10],
		language='c++'),
	Extension("game_letsplay", ["game_letsplay/game_letsplay.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1, included_path_2, included_path_3, included_path_4, included_path_5, included_path_6, included_path_7, included_path_8, included_path_9],
		language='c++'),
	Extension("games_reactions", ["games_reactions/games_reactions.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1, included_path_2, included_path_3, included_path_4, included_path_5, included_path_6, included_path_7, included_path_8, included_path_9],
		language='c++'),
	]		
# ==========================================================================================================================================================
#  Class:
# ==========================================================================================================================================================
class BuildExt(build_ext):
	""" Extend Ctyhon build_ext for removing annoying warnings"""
	def build_extensions(self):
		if '-Wstrict-prototypes' in self.compiler.compiler_so:
			self.compiler.compiler_so.remove('-Wstrict-prototypes')
		super().build_extensions()
# ==========================================================================================================================================================
#  Methods:
# ==========================================================================================================================================================
def setup_function():
	if sys.version_info < (3, 5):
		sys.exit("Have you activated the corrent python environment? oimi doesnt support a py version < 3.7")
	#clean()
	for e in ext_modules:
		e.cython_directives = {'embedsignature': True, 'boundscheck': False, 'wraparound': False, 'linetrace': True, 'language_level': "3"}

	setup(
		name='Specific Oimi_Games building package. Compiling and preparing sources file. Binding cython to python...',
		version='2.1.0',
		author='Colombo Giacomo',
		#long_description=io.open(readme_file, 'rt', encoding='utf-8').read(),
		ext_modules=ext_modules,
		cmdclass = {'build_ext': BuildExt},
		)
# ==========================================================================================================================================================
#  Main:
# ==========================================================================================================================================================
if __name__=="__main__":
	#setup_function()
	with Pool(processes=10) as pool:
		res = pool.apply(setup_function)