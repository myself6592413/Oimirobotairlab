cimport scenario as sc
cimport score as res
cimport game as gm
from games_classes cimport PatchesWrappingList
from enums_play cimport MatchDifficulty

cdef class Match:
	cdef:
		int id_match
		int num_of_games
		gm.Game[:] games_of_this_match
		#old str audio_intro_category
		str audio_m_intro
		#old str audio_exit_match
		str audio_m_exit
		MatchDifficulty match_difficulty
		str variety
		str order_difficulty_games
		str order_quantity_questions	
		str criteria_arrangement	
		sc.Scenario scenario
		list match_feedbacks
		id_scenario
		int advisable_time
		db_conn
		int extra_time
		dict user_dict_match
		list ready_games

	cdef int set_intruder_requirement(self, PatchesWrappingList patches)
	cdef int set_different_requirement(self, PatchesWrappingList patches)
	cdef getDuplicatesWithCount(self, listOfElems)
	cdef okcheck_if_colors(self, PatchesWrappingList patches)
	cpdef set_audio_introcategory(self)
	cpdef choose_an_existing_game(self, int id_game)
	cpdef create_games_of_this_match(self)
	cpdef start_match(self)
	cpdef store_match_in_progress(self)
	cpdef restore_match_in_progress(self)


