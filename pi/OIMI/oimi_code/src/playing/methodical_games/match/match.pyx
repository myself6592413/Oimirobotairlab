""" Oimi Match class getting existing Match created previously in the DB, contains all methods involving matches.
	Defines scenario, games. 
	At runtime this .pyx works only together with corresponding .pxd extension file
	Details:
		considering dress and thing as the same for sunglasses
"""
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
#cimport Match
import sys
cimport scenario as sc
import game as gm
if '/home/pi/OIMI/oimi_code/src/playing/managers/database_manager':
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager')
if '/home/pi/OIMI/oimi_code/src/playing/oimi_state_machine':
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/oimi_state_machine')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games':
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games')
if '/home/pi/OIMI/oimi_code/src/playing/methodical_games/score':
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/methodical_games/score')
import database_manager as db
import pickle
cimport utils_play as up
from cpython.array cimport array, clone
import numpy as np
import copy
import myfsmmain as my
import fsm_event
from fsm_event import Trigger, Target

from enums_play cimport PatchCategory
from games_classes cimport PatchesWrappingList
from enums_play cimport GameCategory
from enums_play cimport MatchDifficulty
from enums_play cimport GameDifficulty
from enums_play cimport QuestionKind
from enums_play cimport QuestionType
from enums_play cimport KidLevel
# =============================================================================
# Class
# =============================================================================
cdef class Match:
	def __cinit__(self, int id_match, int num_of_games, MatchDifficulty match_difficulty, id_scenario, bint way_scenario, 
	bint ask_pos_scenario, bint insertion_scen_difficulty, int extra_time, db_conn):
		self.id_match = id_match
		self.num_of_games = num_of_games
		self.match_feedbacks = []
		self.match_difficulty = match_difficulty
		self.extra_time = extra_time
		self.scenario = sc.Scenario(id_scenario, way_scenario, ask_pos_scenario, insertion_scen_difficulty)
		self.db_conn = db_conn
		self.audio_m_exit = 'audio_199'
		#self.set_audio_introcategory()
		self.user_dict_match = {"task_preli": self.preliminary_activities_match,"task_begin_play":self.go_to_game,} #for fsm! 
		#print(self.id_match)	
		#print(self.num_of_games)	
		#print(self.match_feedbacks)	
		#print(self.match_difficulty)	
		#print(self.extra_time)
		#print(self.audio_m_exit)
	def __enter__(self):
		print("Starting Matches creation and initialization...")

	def __exit__(self, exc_type, exc_val, exc_tb):
		print("All matches for this session have been created.")

	def __eq__(self, other): 
		cdef:
			Py_ssize_t i = 0
			Py_ssize_t lenght = copy.copy(len(self.games_of_this_match))
			bint compare = 1
		for i in range(lenght):
			if self.games_of_this_match[i] != other.games[i]:
				compare = 0
				break
		if not isinstance(other, Match): return NotImplemented
		return len(self.games_of_this_match) == len(other.games) and compare
	# -----------------------------------------------------------------------------
	# getters, setters 
	# -----------------------------------------------------------------------------
	@property
	def id_match(self):
		return self.id_match
	@id_match.setter
	def id_match(self, value):
		self.id_match = value
	@property
	def num_of_games(self):
		return self.num_of_games
	@num_of_games.setter
	def num_of_games(self, value):
		self.num_of_games = value
	@property
	def audio_m_intro(self):
		return self.audio_m_intro
	@audio_m_intro.setter
	def audio_m_intro(self, value):
		self.audio_m_intro = value
	@property
	def games_of_this_match(self):
		return self.games_of_this_match
	@property
	def match_difficulty(self):
		return self.match_difficulty
	@match_difficulty.setter
	def match_difficulty(self, value):
		self.match_difficulty = value
	@property
	def scenario(self):
		return self.scenario
	@property
	def advisable_time(self):
		return self.advisable_time
	@advisable_time.setter
	def advisable_time(self, value):
		self.advisable_time = value
	@property
	def way_scenario(self):
		return self.way_scenario
	@way_scenario.setter
	def way_scenario(self, value):
		self.way_scenario = value
	@property
	def ask_pos_scenario(self):
		return self.ask_pos_scenario
	@ask_pos_scenario.setter
	def ask_pos_scenario(self, value):
		self.ask_pos_scenario= value
	@property
	def insertion_scen_difficulty(self):
		return self.insertion_scen_difficulty
	@insertion_scen_difficulty.setter
	def insertion_scen_difficulty(self, value):
		self.insertion_scen_difficulty = value
	@property
	def audio_intro(self):
		return self.audio_intro
	@audio_intro.setter
	def audio_intro(self, value):
		self.audio_intro = value
	@property
	def audio_m_exit(self):
		return self.audio_m_exit
	@audio_m_exit.setter
	def audio_m_exit(self, value):
		self.audio_m_exit = value
	@property
	def variety(self):
		return self.variety
	@variety.setter
	def variety(self, value):
		self.variety = value
	@property
	def order_difficulty_games(self):
		return self.order_difficulty_games
	@order_difficulty_games.setter
	def order_difficulty_games(self, value):
		self.order_difficulty_games = value
	@property
	def order_quantity_questions(self):
		return self.order_quantity_questions
	@order_quantity_questions.setter
	def order_quantity_questions(self, value):
		self.order_quantity_questions = value
	@property
	def criteria_arrangement(self):
		return self.criteria_arrangement
	@criteria_arrangement.setter
	def criteria_arrangement(self, value):
		self.criteria_arrangement = value
	##################################################################################################################################
	def runme_match(self):
		state0 = my.PlayMatchState("play_session")
		evt1 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.BEGIN_MAT, user_data=self.user_dict_match,)
		evt2 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.GO_TO_MIDDLE_M, user_data=self.user_dict_match,)
		mainfsm = my.OimiFSMMatch()
		mainfsm.update_state_fsm(evt1)
		mainfsm.update_state_fsm(evt2)

	def preliminary_activities_match(self):
		print("preliminary_activities !")
		self.set_audio_introcategory()

	def go_to_game(self):
		print("Games of current matches {}".format(self.games_of_this_match))
		print("LEN {}".format(len(self.games_of_this_match)))
		for i in range(self.num_of_games):
			print("range round gotogame {}".format(i))
			self.games_of_this_match[i].runme_game()
	# -----------------------------------------------------------------------------
	# Checking requirements
	# -----------------------------------------------------------------------------
	cdef int set_intruder_requirement(self, PatchesWrappingList patches):
		'''Works only if 3 genre are equal and 1 is different'''
		cdef:
			bint intruderok = 0
			list pat_gen = []
		for i in range(4):
			pat_gen.append(patches[i].genre)
		pat_gen = list(dict.fromkeys(pat_gen))
		if len(pat_gen)==2:
			intruderok = 1
		
		return intruderok

	cdef int set_different_requirement(self, PatchesWrappingList patches):
		cdef:
			bint differnceok = 0
			list pat_sub = []
		for i in range(4):
			pat_sub.append(patches[i].subject)
		sub = list(dict.fromkeys(pat_sub))
		if len(sub)==len(pat_sub):
			differenceok = 1
		return differenceok

	cdef getDuplicatesWithCount(self, listOfElems):
		''' Get frequency count of duplicate elements in the given list '''
		dictOfElems = dict()
		# Iterates over each element in list
		for elem in listOfElems:
			# If element exists in dict then increment its value else add it in dict
			if elem in dictOfElems:
				dictOfElems[elem] += 1
			else:
				dictOfElems[elem] = 1
		# Filter key-value pairs in dictionary. Keep pairs whose value is greater than 1 i.e. only duplicate elements from list.
		dictOfElems = { key:value for key, value in dictOfElems.items() if value > 1}
		# Returns a dict of duplicate elements and thier frequency count
		return dictOfElems
		
	cdef okcheck_if_colors(self, PatchesWrappingList patches):
		cdef int colorflagok = 0
		cdef list new_colors_list = []
		cdef list lili = []
		for i in range(4):
			new_colors_list.append(patches[i].color)
		dict_result = self.getDuplicatesWithCount(new_colors_list)
		empty = not dict_result 
		print("COLOR LIST SISI {}".format(new_colors_list))
		print("EMPTYNESS {}".format(empty))
		if not empty:
			lili = list(dict_result.keys())
			colorflagok = 1
		print("COLOR duplicate lili {}".format(lili))
		print("COLORFLAG STAMPA{}".format(colorflagok))
		print("COLOR stampare len {}".format(len(dict_result)))
		return colorflagok,lili,len(dict_result)
	# -----------------------------------------------------------------------------
	# Internal additional
	# -----------------------------------------------------------------------------
	cpdef set_audio_introcategory(self):
		''' Set introductive audio for specifying and tell which is the main subject of the patches that doesn't change in a match '''
		cdef list listgenres = []
		cdef int cont_animals = 0
		cdef int cont_colors = 0
		cdef int cont_humans = 0
		cdef int cont_things = 0
		cdef int cont_foods = 0
		cdef int cont_dresses = 0
		cdef Py_ssize_t i = 0
		cdef Py_ssize_t lenght = 4	
		for i in range(lenght):
			listgenres.append(self.scenario.patches[i].genre)
		listgenres_def = list(dict.fromkeys(listgenres))
		if len(listgenres) == len(listgenres_def):
			if listgenres[0] == GameCategory.animals:
				self.audio_m_intro = 'audio_93' #audio giochiamo con animali 
			elif listgenres[0] == GameCategory.humans:
				self.audio_m_intro = 'audio_10' #audio giochiamo con persone
			elif listgenres[0] == GameCategory.things: 
				self.audio_m_intro = 'audio_11' #audio giochiamo con oggetti
			elif listgenres[0] == GameCategory.dresses:
				self.audio_m_intro = 'audio_90' #audio giochiamo con i colori
			elif listgenres[0] == GameCategory.foods:
				self.audio_m_intro = 'audio_12' #audio giochiamo con il cibo
		else:
			for i in range(lenght):
				if listgenres[i] != PatchCategory.thing and listgenres[i] != PatchCategory.dress: 
					ok_flag = 1
					break
			if ok_flag == 0: self.audio_m_intro = 'audio_11' #audio giochiamo con oggetti
			else:
				self.audio_m_intro = 'audio_13' #miscellanous!	
	# -----------------------------------------------------------------------------
	# Defines games
	# -----------------------------------------------------------------------------
	cpdef create_games_of_this_match(self):
		""" Adds other column and all set of game attribute of game list """
		self.ready_games = []
		sql_take_games = "SELECT game_id FROM Matches_Games WHERE match_id = ?"
		result_game = self.db_conn.execute_param_query(sql_take_games, self.id_match)
		print("result_game {}".format(result_game))
		print("self.num_of_games:")
		print(self.num_of_games)
		if self.num_of_games==1:
			id_game = result_game[0]
			print("type game {}".format(type(id_game)))
			print("id game num {} = {}".format(1,id_game))
			one_game = self.choose_an_existing_game(id_game)
			self.ready_games.append(one_game)
		else:
			#print("NUM OF GAMES: --> INSIDE CREATE GAMES OF THIS MATCH {}".format(self.num_of_games))
			for i in range(self.num_of_games):
				id_game = result_game[i][0]
				print("tipotipo {}".format(type(id_game)))
				print("id game num {} = {}".format(i,id_game))
				one_game = self.choose_an_existing_game(id_game)
				self.ready_games.append(one_game)
				print("---fine self.ready_games")
		#print("READYREADYREADYREADYREADYREADYREADYREADYREADYREADYREADYREADY {}".format(self.ready_games))
		print(self.ready_games)
		for i in range(self.num_of_games):
			print(self.ready_games[i])
		cdef gm.Game[:] games_arr
		gtemp = np.asarray(self.ready_games, dtype = gm.Game)
		games_arr = gtemp
		self.games_of_this_match = games_arr
		#print("games_temp is {}".format(gtemp))
		#print("games_arr is {}".format(games_arr))
		#print("----fine crate_games_of_this_match")

	cpdef choose_an_existing_game(self, int id_game): 
		""" Check if the game exists """
		sql_take_info_game = "SELECT * FROM Games WHERE game_id = ?"
		result_sel = self.db_conn.execute_param_query(sql_take_info_game, id_game)
		#print("result_sel 2 {}".format(result_sel))
		#print("result_sel[1]")
		#print(result_sel[1])
		#print("result_sel[2]")
		#print(result_sel[2])
		#print("result_sel[3]")
		#print(result_sel[3])
		#print("result_sel[4]")
		#print(result_sel[4])
		#print("result_sel[6]")
		#print(result_sel[6])
		cdef:
			int num_of_questions = result_sel[1]
			int g_kind = up.opposite_conversion(result_sel[2])
			int g_type = up.opposite_conversion(result_sel[3])
			int g_diff = up.opposite_conversion(result_sel[4])
			QuestionKind ga_kind = up.transform_question_kind(g_kind)
			QuestionType ga_type = up.transform_question_type(g_type)
			GameDifficulty ga_diff = up.transform_difficulty_game(g_diff)
			int ga_time = result_sel[6]
		print("////////////////////7")
		print(num_of_questions)
		print(ga_kind)
		print(ga_type)
		print(ga_diff)
		print(ga_time)
		print("////////////////////7")
		cdef gm.Game game_new = gm.Game(id_game, num_of_questions, ga_kind, ga_type, ga_diff, ga_time, self.extra_time, self.scenario, self.db_conn)
		game_new.set_necessary_time()
		print("this is the game: {}".format(game_new.id_game))
		return game_new
	
	#cpdef store_session_in_progress(self)
	#cpdef restore_session_in_progress(self)
