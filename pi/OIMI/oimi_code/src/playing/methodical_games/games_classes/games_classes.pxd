# ===================================== # ===================================== #
# Extern Imports
# ===================================== # ===================================== #
cdef extern from"Python.h":
	object PyList(float *s, Py_ssize_t leng)

cdef extern from "support_play.h":
	ctypedef struct QuestionType:
		int kind
		int the_type
	ctypedef struct Question:
		int size
		int number 
		int value
		int required_time
		QuestionType *types
	Question makeQuestion(int size, int number, int value, int required_time, int*kinds, int*types)
	ctypedef struct QeA:		
		int audio,required_time,answer1,answer2,answer3,answer4
	ctypedef struct QeAArray:
		int size
		QeA *arr
	QeAArray makeQeAArray(int size,int*audios, int*required_times,int*answers1, int*answers2, int*answers3, int*answers4)
	QeAArray2 makeQeAArray2(int size,int*audios, int*required_times,int*answers1, int*answers2, int*answers3, int*answers4)
	ctypedef struct Patch:
		int subject, color, genre
	ctypedef struct PatchesArray:
		int size
		Patch *arr
	PatchesArray makePatchesArray(int size, int* subjects, int* colors, int*genre)
# ===================================== # ===================================== #
# Classes for wrapping cpp structs:
# ===================================== # ===================================== #
cdef class PyQeA:
	"""Python wrapper for cpp struct QeA 
	
	Attributes
	----------
	:audio 
	:4 answers
	:value = question is easy or hard?
	:type
	:kind
	:ptr pointer for being called by WrappingList
	
	To do:
	------
		--remove 4 answers, add a list of answers ...difficult, I cannot
	"""
	cdef QeA *ptr

cdef class PyQeA2:
	"""
	list of pointers without limits 
	"""
	cdef QeA2 *ptr

cdef class QeAWrappingList:
	"""Python wrapper for cpp struct of struct QeAarray 
	
	Attributes
	----------
	:size = necessary for malloc
	:arr = array of QeA (pointer to QeA)
	
	NB:
	------
	For now is impossible to valueare directly the WrappingList, valueare instead single QeAs
	Thankd to __getitem__ arr cannot be seen in Python (but only in Cython) ... in order to access to arr elements use [] 
	"""
	cdef int size
	cdef QeA *arr


cdef class PyPatch:
	"""Python wrapper for cpp struct Patch
	
	Attributes
	----------
	:subject 
	:color 
	:shape 
	:genre 
	
	NB:
	------

	"""
	cdef Patch *ptr 
	

cdef class PatchesWrappingList:
	"""Python wrapper for cpp struct of struct QeAarray 
	
	Attributes
	----------
	:size = necessary for malloc
	:arr = array of Patches 
	
	NB:
	------
	For now is impossible to valueare directly the WrappingList, valueare instead single Patches
	Thankd to __getitem__ arr cannot be seen in Python (but only in Cython) ... in order to access to arr elements use [] 
	"""
	cdef int size
	cdef Patch *arr
	
cdef class PyQuestionType:
	"""Python wrapper for cpp struct QuestionType
	
	Attributes
	----------
	:kind = each question belongs to a specific game or more than one
	:type =  each question belongs to a specific type (among animals,humans,miscellanouss) 
		or more than one (both things and dresses or both animals and humans = eg. who is able to run?) 
	
	NB:
	------

	"""
	cdef QuestionType *ptr 

cdef class PyQuestion:
	"""Python wrapper for cpp struct Question
	Contain an array of QuestionType 
	
	Attributes
	----------
	:size = necessary for malloc
	:number 
	:value
	:types = array of QuestionTypes
	
	NB:
	------
	For now is impossible to valueare directly the WrappingQuestionTypeList, valueare instead single QuestionType
	"""
	cdef int size
	cdef int number
	cdef int value
	cdef int required_time
	cdef QuestionType *types
	
# ===================================== # ===================================== #
# Ausiliary methods for using cpp functions
# ===================================== # ===================================== #
cdef inline create_current_question(int num, int value, int time, int[::1] input_vals1, int[::1] input_vals2):
	cdef Question res=makeQuestion(len(input_vals1), num, value, time, &input_vals1[0], &input_vals2[0])
	ques=PyQuestion()
	ques.size, ques.number, ques.value, ques.required_time, ques.types = res.size, res.number, res.value, res.required_time, res.types
	return ques
#domanda,tempo,risp1,risp2,risp3,risp4
cdef inline create_current_inquiry(int[::1] input_vals1, int[::1] input_vals2, int[::1] input_vals3, int[::1] input_vals4, int[::1] input_vals5, int[::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0], &input_vals4[0], &input_vals5[0], &input_vals6[0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst
#####################
cdef inline create_current_inquiry_d1(int[::1] input_vals1, int[::1] input_vals2, int[:,::1] input_vals3, int[::1] input_vals4, int[::1] input_vals5, int[::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0,0], &input_vals4[0], &input_vals5[0], &input_vals6[0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst
cdef inline create_current_inquiry_d2(int[::1] input_vals1, int[::1] input_vals2, int[::1] input_vals3, int[:,::1] input_vals4, int[::1] input_vals5, int[::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0], &input_vals4[0,0], &input_vals5[0], &input_vals6[0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst
cdef inline create_current_inquiry_d3(int[::1] input_vals1, int[::1] input_vals2, int[::1] input_vals3, int[::1] input_vals4, int[:,::1] input_vals5, int[::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0], &input_vals4[0], &input_vals5[0,0], &input_vals6[0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst
cdef inline create_current_inquiry_d4(int[::1] input_vals1, int[::1] input_vals2, int[::1] input_vals3, int[::1] input_vals4, int[::1] input_vals5, int[:,::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0], &input_vals4[0], &input_vals5[0], &input_vals6[0,0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst
#####################
cdef inline create_current_inquiry_d12(int[::1] input_vals1, int[::1] input_vals2, int[:,::1] input_vals3, int[:,::1] input_vals4, int[::1] input_vals5, int[::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0,0], &input_vals4[0,0], &input_vals5[0], &input_vals6[0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst

cdef inline create_current_inquiry_d13(int[::1] input_vals1, int[::1] input_vals2, int[:,::1] input_vals3, int[::1] input_vals4, int[:,::1] input_vals5, int[::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0,0], &input_vals4[0], &input_vals5[0,0], &input_vals6[0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst

cdef inline create_current_inquiry_d14(int[::1] input_vals1, int[::1] input_vals2, int[:,::1] input_vals3, int[::1] input_vals4, int[::1] input_vals5, int[:,::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0,0], &input_vals4[0], &input_vals5[0], &input_vals6[0,0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst

cdef inline create_current_inquiry_d23(int[::1] input_vals1, int[::1] input_vals2, int[::1] input_vals3, int[:,::1] input_vals4, int[:,::1] input_vals5, int[::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0], &input_vals4[0,0], &input_vals5[0,0], &input_vals6[0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst

cdef inline create_current_inquiry_d24(int[::1] input_vals1, int[::1] input_vals2, int[::1] input_vals3, int[:,::1] input_vals4, int[::1] input_vals5, int[:,::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0], &input_vals4[0,0], &input_vals5[0], &input_vals6[0,0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst

cdef inline create_current_inquiry_d34(int[::1] input_vals1, int[::1] input_vals2, int[::1] input_vals3, int[::1] input_vals4, int[:,::1] input_vals5, int[:,::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0], &input_vals4[0], &input_vals5[0,0], &input_vals6[0,0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst
############################################
cdef inline create_current_inquiry_d123(int[::1] input_vals1, int[::1] input_vals2, int[:,::1] input_vals3, int[:,::1] input_vals4, int[:,::1] input_vals5, int[::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0,0], &input_vals4[0,0], &input_vals5[0,0], &input_vals6[0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst

cdef inline create_current_inquiry_d124(int[::1] input_vals1, int[::1] input_vals2, int[:,::1] input_vals3, int[:,::1] input_vals4, int[::1] input_vals5, int[:,::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0,0], &input_vals4[0,0], &input_vals5[0], &input_vals6[0,0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst

cdef inline create_current_inquiry_d134(int[::1] input_vals1, int[::1] input_vals2, int[:,::1] input_vals3, int[::1] input_vals4, int[:,::1] input_vals5, int[:,::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0,0], &input_vals4[0], &input_vals5[0,0], &input_vals6[0,0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst

cdef inline create_current_inquiry_d234(int[::1] input_vals1, int[::1] input_vals2, int[::1] input_vals3, int[:,::1] input_vals4, int[:,::1] input_vals5, int[:,::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0], &input_vals4[0,0], &input_vals5[0,0], &input_vals6[0,0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst
########################################
cdef inline create_current_inquiry_d1234(int[::1] input_vals1, int[::1] input_vals2, int[:,::1] input_vals3, int[:,::1] input_vals4, int[:,::1] input_vals5, int[:,::1] input_vals6):
	cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0,0], &input_vals4[0,0], &input_vals5[0,0], &input_vals6[0,0])
	lst=QeAWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst

cdef inline create_current_patches(int[::1] input_vals1, int[::1] input_vals2, int[::1] input_vals3):
	cdef PatchesArray res=makePatchesArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0])
	lst=PatchesWrappingList()
	lst.size, lst.arr = res.size, res.arr
	return lst

cdef inline change_required_time(int time, int add_extra_time):
	required_time = time + add_extra_time
	return required_time
#####################################################################################################################################################################
cdef inline create_current_inquiry_list(int[::1] input_vals1, int[::1] input_vals2, int[:,::1] input_vals3):
    cdef QeAArray res=makeQeAArray(len(input_vals1), &input_vals1[0], &input_vals2[0], &input_vals3[0,0])
    lst=QeAWrappingList()
    lst.size, lst.arr = res.size, res.arr
    return lst