'''Info:
		Games classes using the support_play.h module, which contains Question + Answer object created in realtime, same for Patches
		exploting c methods to speed up computation (see pxd corresponded file)
	Details:
		create_current_inquiry_list using PyQeA2 with malloc and no limit know a priori about num of answers 
Classes for managing the memory of the C array and providing a Pythonic way of accessing its elements (through the __getitem__ method). 
'''
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
from libc.stdlib cimport malloc, free
cimport games_classes
# ===================================== # ===================================== #
# Classes for wrapping cpp structs:
# ===================================== # ===================================== #
cdef class PyQeA:
	def __cinit__(self):
		self.ptr=NULL

	@property
	def audio(self):
		return self.ptr.audio
	@audio.setter
	def audio(self, value):
		self.ptr.audio = value
	@property
	def required_time(self):
		return self.ptr.required_time
	@required_time.setter
	def required_time(self, value):
		self.ptr.required_time = value
	@property
	def answer1(self):
		return self.ptr.answer1
	@answer1.setter
	def answer1(self, value):
		self.ptr.answer1 = value
	@property
	def answer2(self):
		return self.ptr.answer2
	@answer2.setter
	def answer2(self, value):
		self.ptr.answer2 = value	
	@property
	def answer3(self):
		return self.ptr.answer3
	@answer3.setter
	def answer3(self, value):
		self.ptr.answer3 = value
	@property
	def answer4(self):
		return self.ptr.answer4
	@answer4.setter
	def answer4(self, value):
		self.ptr.answer4 = value

	def __eq__(self, other):
		if not isinstance(other, PyQeA): 
			return NotImplemented 
		return self.audio == other.audio and self.answer1 == other.answer1 and self.answer2 == other.answer2 \
			and self.answer3 == other.answer3 and self.answer4 == other.answer4

cdef class QeAWrappingList:
	def __cinit__(self):
		self.size=0
		self.arr=NULL

	def __dealloc__(self):
		free(self.arr)
		print("Question memory deallocated")
	
	def __getitem__(self, index):
		#cdef PyQeA res = PyQeA()
		if index>=self.size:
			raise IndexError("out of range")
		res = PyQeA()
		res.ptr=&self.arr[index]
		return res

	def __eq__(self, other): 
		if not isinstance(other, QeAWrappingList): 
			return NotImplemented
		return self.size == other.size 

cdef class PyPatch:
	def __init__(self):
		self.ptr=NULL
	
	@property
	def subject(self):
		return self.ptr.subject
	@subject.setter
	def subject(self, value):
		self.ptr.subject = value
	@property
	def color(self):
		return self.ptr.color		
	@color.setter
	def color(self, value):
		self.ptr.color = value
	@property
	def genre(self):
		return self.ptr.genre		
	@genre.setter
	def genre(self, value):
		self.ptr.genre = value

	def __eq__(self, other): 
		if not isinstance(other, PyPatch):
			return NotImplemented
		return self.subject == other.subject and self.color == other.color

cdef class PatchesWrappingList:
	def __cinit__(self):
		self.size=0
		self.arr=NULL

	def __dealloc__(self):
		free(self.arr)
		print("Patches memory deallocated")
	
	def __getitem__(self, index):
		if index>=self.size:
			raise IndexError("list index out of range")
		res = PyPatch()
		res.ptr=&self.arr[index]
		return res
	def __eq__(self, other): 
		if not isinstance(other, PatchesWrappingList): 
			return NotImplemented
		return self.size == other.size

cdef class PyQuestionType:
	def __init__(self):
		self.ptr=NULL
	
	@property
	def kind(self):
		return self.ptr.kind
	@kind.setter
	def kind(self, value):
		self.ptr.kind = value
	@property
	def the_type(self):
		return self.ptr.the_type
	@the_type.setter
	def the_type(self, value):
		self.ptr.the_type = value

	def __eq__(self, other): 
		if not isinstance(other, PyQuestionType): 
			return NotImplemented
		return self.kind == other.kind and self.value == other.value

cdef class PyQuestion:
	def __cinit__(self):
		self.size=0
		self.number=0
		self.value=0
		self.required_time=0
		self.types=NULL

	def __dealloc__(self):
		free(self.types)
		print("Question_type memory deallocated")
	
	def __getitem__(self, index):
		if index>=self.size:
			raise IndexError("list index out of range")
		res = PyQuestionType()
		res.ptr=&self.types[index]
		return res

	def __eq__(self, other): 
		if not isinstance(other, PyQuestion):
			return NotImplemented
		return self.size == other.size and self.number == other.number and self.complexity == other.complexity

cdef class PyQeA2:
    def __cinit__(self):
        self.ptr = NULL
        self.answers = []
        
    @property
    def audio(self):
        return self.ptr.audio
    
    @audio.setter
    def audio(self, value):
        self.ptr.audio = value
        
    @property
    def required_time(self):
        return self.ptr.required_time
    
    @required_time.setter
    def required_time(self, value):
        self.ptr.required_time = value
        
    def add_answer(self, value):
        self.answers.append(value)
        
    def __eq__(self, other):
        if not isinstance(other, PyQeA2): 
            return NotImplemented 
        return self.audio == other.audio and self.answers == other.answers and

	def __getitem__(self, index):
	    if index >= self.size:
	        raise IndexError("out of range")
	    res = PyQeA2()
	    res.ptr = &self.arr[index]
	    res.number_of_answers = self.arr[index].number_of_answers
	    res.answers = self.arr[index].answers
	    return res