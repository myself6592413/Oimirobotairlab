"""Info:
    Oimi body IMU class for make IMU sensors work 
Notes:
    For detailed info, look at ./code_docs_of_playing
Author:
    Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import smbus
from imu_support import rebuild
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class IMU_sensor:
    def __cinit__(self, int device_Address):
        self.device_Address = device_Address
        self.setup_mpu()

    def __enter__(self):
        print("Initializing the stage reaction...")

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("The stage have been created successfully.")
        print("Im exiting from stage")
    

    def __reduce__(self): 
        '''for handling with success reduction when dumping or call object from Pool --> thanks to simply rebuild method in imu_support module'''
        return rebuild, (self.device_Address,)


    # -----------------------------------------------------------------------------
    # Getters, setters 
    # -----------------------------------------------------------------------------
    @property
    def device_Address(self):
        return self.device_Address
    @device_Address.setter
    def device_Address(self, value):
        self.device_Address = value
    @property
    def PWR_MGMT_1(self):
        return self.PWR_MGMT_1
    @PWR_MGMT_1.setter
    def PWR_MGMT_1(self, value):
        self.PWR_MGMT_1 = value
    @property
    def SMPLRT_DIV(self):
        return self.SMPLRT_DIV
    @SMPLRT_DIV.setter
    def SMPLRT_DIV(self, value):
        self.SMPLRT_DIV = value
    @property
    def CONFIG(self):
        return self.CONFIG
    @CONFIG.setter
    def CONFIG(self, value):
        self.CONFIG = value
    @property
    def GYRO_CONFIG(self):
        return self.GYRO_CONFIG
    @GYRO_CONFIG.setter
    def GYRO_CONFIG(self, value):
        self.GYRO_CONFIG = value
    @property
    def INT_ENABLE(self):
        return self.INT_ENABLE
    @INT_ENABLE.setter
    def INT_ENABLE(self, value):
        self.INT_ENABLE = value
    @property
    def ACCEL_XOUT_H(self):
        return self.ACCEL_XOUT_H
    @ACCEL_XOUT_H.setter
    def ACCEL_XOUT_H(self, value):
        self.ACCEL_XOUT_H = value
    @property
    def ACCEL_YOUT_H(self):
        return self.ACCEL_YOUT_H
    @ACCEL_YOUT_H.setter
    def ACCEL_YOUT_H(self, value):
        self.ACCEL_YOUT_H = value
    @property
    def ACCEL_ZOUT_H(self):
        return self.ACCEL_ZOUT_H
    @ACCEL_ZOUT_H.setter
    def ACCEL_ZOUT_H(self, value):
        self.ACCEL_ZOUT_H = value
    @property
    def GYRO_XOUT_H(self):
        return self.GYRO_XOUT_H
    @GYRO_XOUT_H.setter
    def GYRO_XOUT_H(self, value):
        self.GYRO_XOUT_H = value
    @property
    def GYRO_YOUT_H(self):
        return self.GYRO_YOUT_H
    @GYRO_YOUT_H.setter
    def GYRO_YOUT_H(self, value):
        self.GYRO_YOUT_H = value
    @property
    def GYRO_ZOUT_H(self):
        return self.GYRO_ZOUT_H
    @GYRO_ZOUT_H.setter
    def GYRO_ZOUT_H(self, value):
        self.GYRO_ZOUT_H = value
    @property
    def bus(self):
        return self.bus
    @bus.setter
    def bus(self, value):
        self.bus = value
    # ==========================================================================================================================================================
    # Methods
    # ==========================================================================================================================================================
    cpdef setup_mpu(self):
        self.PWR_MGMT_1   = 0x6B
        self.SMPLRT_DIV   = 0x19
        self.CONFIG       = 0x1A
        self.GYRO_CONFIG  = 0x1B
        self.INT_ENABLE   = 0x38
        self.ACCEL_XOUT_H = 0x3B
        self.ACCEL_YOUT_H = 0x3D
        self.ACCEL_ZOUT_H = 0x3F
        self.GYRO_XOUT_H  = 0x43
        self.GYRO_YOUT_H  = 0x45
        self.GYRO_ZOUT_H  = 0x47

        self.bus = smbus.SMBus(1)

        self.MPU_Init()
    #------------------------------------------------------------------------------------
    # Enablers
    #------------------------------------------------------------------------------------
    cpdef MPU_Init(self):
        self.bus.write_byte_data(self.device_Address, self.SMPLRT_DIV, 7)    #write to sample rate register
        self.bus.write_byte_data(self.device_Address, self.PWR_MGMT_1, 1)    #write to power management register
        self.bus.write_byte_data(self.device_Address, self.CONFIG, 0)        #write to Configuration register
        self.bus.write_byte_data(self.device_Address, self.GYRO_CONFIG, 24)  #write to Gyro configuration register
        self.bus.write_byte_data(self.device_Address, self.INT_ENABLE, 1)    #write to interrupt enable register
        
    cpdef read_raw_data(self, addr):
        '''Acceleration and Gyroscopes value are 16-bit
            ---> concatenate higher and lower value to get signed value from mpu6050'''
        high = self.bus.read_byte_data(self.device_Address, addr) 
        low = self.bus.read_byte_data(self.device_Address, addr+1)
        
        value = ((high << 8) | low)
        if(value > 32768):
            value = value - 65536
        return value
    
    cpdef get_gyroX(self):
        return self.read_raw_data(self.GYRO_XOUT_H)
    cpdef get_gyroY(self):
        return self.read_raw_data(self.GYRO_YOUT_H)
    cpdef get_gyroZ(self):
        return self.read_raw_data(self.GYRO_ZOUT_H)