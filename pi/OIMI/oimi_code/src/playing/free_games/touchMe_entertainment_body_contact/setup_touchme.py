"""Compiling cython source with a separated cythoning file from the main one
    Usage:
        python setup_touchme oimi.py build_ext --inplace (or alias setta_touch)
"""
# ============================================================================================
#  Imports:
# ============================================================================================
#try:
#    from setuptools import setup
#    from setuptools import Extension
#except ImportError:

from distutils.core import setup
import setuptools
from distutils.extension import Extension
from Cython.Build import build_ext
from Cython.Build import cythonize
import numpy
import os.path
import io
import sys
import shutil
# ===========================================================================================
#  Elements:
# ===========================================================================================
readme_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'README.md')

ext_modules = [
	Extension("contact_manager", ["contact_manager.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include()],
		language='c++'),	
	#Extension("body_calculate_features", ["body_calculate_features.pyx"],
	#	extra_compile_args=['-O3','-w','-fopenmp'],
	#	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	#	include_dirs=[numpy.get_include()],
	#	language='c++'),	
	Extension("body_create_sample", ["body_create_sample.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include()],
		language='c++'),	
	Extension("IMU_sensor", ["IMU_sensor.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include()],
		language='c++'),
	]
# =================================================================================================================================
#  Methods:
# =================================================================================================================================
def clean():
    """ Cleaning """
    for root, dirs, files in os.walk(".", topdown=False):   # For all files in the whole project tree, walking bottom-up
        for name in files:
            if (name.startswith("contact_manager") and not(name.endswith(".pyx")) and not(name.endswith(".pxd"))) or \
                (name.startswith("body_calculate_features") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))   or \
                (name.startswith("body_create_sample") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))   or \
                (name.startswith("body_take_gyroscopes") and not(name.endswith(".pyx")) and not(name.endswith(".pxd"))):
                os.remove(os.path.join(root, name))  # Remove all .cpp and .so files with a certain name
        for name in dirs:
            if (name == "build"):
                shutil.rmtree(name)  # Remove also build directory
# =======================================================================
#  Class:
# =======================================================================
class BuildExt(build_ext):
    """ Extend Ctyhon build_ext for removing annoying warnings"""
    def build_extensions(self):
        if '-Wstrict-prototypes' in self.compiler.compiler_so:
            self.compiler.compiler_so.remove('-Wstrict-prototypes')
        super().build_extensions()
# =====================================================================================================================================
#  Main:
# =====================================================================================================================================
#clean()
for e in ext_modules:
    e.cython_directives = {'embedsignature': True,'boundscheck': False,'wraparound': False,'linetrace': True, 'language_level': "3"}

setup(
    name='oimi-robot source compilation for touchme_game',
    version='2.1.0',
    author='Colombo Giacomo',
    long_description=io.open(readme_file, 'rt', encoding='utf-8').read(),
    ext_modules=ext_modules,
    cmdclass = {'build_ext': BuildExt},
    )
