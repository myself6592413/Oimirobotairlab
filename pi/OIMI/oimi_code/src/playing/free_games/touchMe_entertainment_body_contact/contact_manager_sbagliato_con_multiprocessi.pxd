"""Info:
	Oimi body calculate features for the logistic regression model = aka column of the dataset
	-Max
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
	Pressure signals of 10 / 12 samples 
	
	Min value eliminated
	Acce_std eliminated

	Final sav file imported = > created by "only_training_logistic_regression.py" file
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
cpdef queue_MPU_values(mpu1_left, mpu2_right, event_start_get_mpu, qu_allow_start_mpu_loop, event_lifo_mpu_filled, lifo_mpus)
cpdef extract_MPU_values_from_lifo(lifo_mpus)
cpdef take_10_MPU_values(mpu1_left, mpu2_right, event_take_mpus_from_lifo, qu_allow_take_mpus, qu_last_mpu10, lifo_mpus)
#cpdef take_pressure_values(serNano2, event_ok_start_taking_press, event_continue_take_press, lifo_press)
cpdef take_pressure_values(serNano2, event_ok_start_taking_press, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press)
cdef stop_taking_mpx(serNano2, event_continue_take_press, event_ok_start_taking_press, event_continue_take_press22222222,event_lifo_press_filled,event_lifo_mpu_filled)
#cdef prendi_press(serNano2,event_start_get_press)
#cpdef prendi_mpu(mpu1_left,mpu2_right,event_start_get_mpu)
#cpdef prendi_mpu(mpu1_left,mpu2_right)
