"""Info:
	Oimi body calculate features for the logistic regression model = aka column of the dataset
	-Max
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
	Pressure signals of 10 / 12 samples 
	
	Min value eliminated
	Acce_std eliminated

	Final sav file imported = > created by "only_training_logistic_regression.py" file
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import time, sys, math, os
import multiprocessing
import inspect
from multiprocessing import Pool
import timedelta
import queue
import csv
from contextlib import closing #to test!!! --> using #with closing(bimu.IMU_sensor(device_Address_l)) as mpu1_left:
import joblib
import IMU_sensor as bimu
import body_create_sample as bcs
#import body_calculate_features as bcf
import serial_manager as serr
from multiprocessing import Process, Pool, Pipe, Event, Lock, Value
from oimi_process import OimiProcess, OimiStartProcess, OimiQueue, OimiManager
from queue import LifoQueue
import reactions as rea
# ==========================================================================================================================================================
# Variables
# ==========================================================================================================================================================
mpx0, mpx1, mpx2, mpx3, mpx4, giro_l, giro_r = [],[],[],[],[],[],[] #useful????
start_time = time.time()
n, cycle=0,0
num_quiet=0
result = 'initial'
fla_result = 0
fla_funny = 0
device_Address_l = 0x69   # MPU6050 device address 1
device_Address_r = 0x68			# MPU6050 device address 2
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cpdef queue_MPU_values(mpu1_left, mpu2_right, event_start_get_mpu, qu_allow_start_mpu_loop, lifo_mpus):
	#print(">>>>>>>>>>>>>>>> 1 queue_MPU_values")
	i = 0
	while True:
		if event_start_get_mpu.is_set():
			#print("111111111111111111111 entro nel while queue!!!")
			#addictive control...is necessary??? lets start with no choice
			gyroxl, gyrozl, gyroxr, gyrozr = bcs.scan_MPUs(mpu1_left, mpu2_right)
			#one_single_elem or many??
			#single because all should be taken together
			lifo_mpus.put([gyroxl,gyrozl,gyroxr,gyrozr])
			#or??? many in several queue nodes
			#qu_mpu.put(gyroxl1)	
			#qu_mpu.put(gyrozl1)
			#qu_mpu.put(gyroxr1)
			#qu_mpu.put(gyrozr1)	
			#qu_mpu.put(gyroxl2)	
			#qu_mpu.put(gyrozl2)	
			#qu_mpu.put(gyroxr2)	
			#qu_mpu.put(gyrozr2)
			#get size!!
			
			#i = i+1
			#if i < 10:
			#	print("size lifo_mpus????size lifo_mpus????size lifo_mpus????size lifo_mpus????size lifo_mpus???? is ", lifo_mpus.size())


'''
cpdef take_MPU_double(mpu1_left, mpu2_right, qu_mpu2):
	while True:
		event_start_get_mpu
		gyroxl1,gyrozl1,gyroxr1,gyrozr1,gyroxl2,gyrozl2,gyroxr2,gyrozr2    = bcs.taketwoMPU(mpu1_left, mpu2_right)
		#one_single_elem or many??
		#single
		qu_mpu.put([gyroxl1,gyrozl1,gyroxr1,gyrozr1,gyroxl2,gyrozl2,gyroxr2,gyrozr2])
		#many in several queue nodes
		#qu_mpu.put(gyroxl1)	
		#qu_mpu.put(gyrozl1)	
		#qu_mpu.put(gyroxr1)	
		#qu_mpu.put(gyrozr1)	
		#qu_mpu.put(gyroxl2)	
		#qu_mpu.put(gyrozl2)	
		#qu_mpu.put(gyroxr2)	
		#qu_mpu.put(gyrozr2)
'''

cpdef extract_MPU_values_from_lifo(lifo_mpus):
	#it make sense take a step again to encapsulate and return values??
	#print(">>>>>>>>>>>>>>>> 2 enter in extract_MPU_values_from_lifo")
	list_mpus = lifo_mpus.get()
	#print("list_mpus list_mpus list_mpuslist_mpus list_mpus ", list_mpus)
	#print("list_mpus list_mpus list_mpuslist_mpus list_mpus ", list_mpus)
	#print("list_mpus list_mpus list_mpuslist_mpus list_mpus ", list_mpus)
	#print("list_mpus list_mpus list_mpuslist_mpus list_mpus ", list_mpus)
	#print("type type list_mpus ", type(list_mpus))
	#gyroxl, gyrozl, gyroxr, gyrozr = 1,1,1,1
	gyroxl, gyrozl, gyroxr, gyrozr = list_mpus[0], list_mpus[1], list_mpus[2], list_mpus[3]
	return gyroxl, gyrozl, gyroxr, gyrozr

cpdef take_10_MPU_values(mpu1_left, mpu2_right, event_take_mpus_from_lifo, qu_allow_take_mpus, qu_last_mpu10, lifo_mpus):
	#print(">>>>>>>>>>>>>>>> 3 enter in take_10_MPU_values")
	print(type(mpu1_left))
	print(type(mpu2_right))
	cdef:
		Py_ssize_t i = 0
		Py_ssize_t leng = 10
	gyro_left_list, gyro_right_list= [],[] ##?? useful or not????
	while True:
		if event_take_mpus_from_lifo.is_set():
			#print(">>>>>>>>>>>>>>>> 3333333 while inside ok!!! ")
			#serve????
			#res = qu_allow_take_mpus.get()
			#if res == 1:
			for i in range(leng):
				#print(">>>>>>>>>>>>>>>> 3333333 3333333 3333333 3333333 ")
				gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(lifo_mpus)
				gyro_left_list.append(gyroxl)
				gyro_right_list.append(gyroxr)
				#print("3333333333333333333 gyroxl", gyroxl)
				#print("3333333333333333333 gyroxl", gyrozl)
				#print("3333333333333333333 gyroxl", gyroxr)
				#print("3333333333333333333 gyroxl", gyrozr)

				#queue also these???
				gyro_z_l = mpu1_left.get_gyroZ()
				gyro_z_r = mpu2_right.get_gyroZ()
			#print("3333333 gyro_left_list ", gyro_left_list)
			#print("3333333 gyro_right_list ", gyro_right_list)
			qu_last_mpu10.put([gyro_left_list,gyro_right_list,gyro_z_l,gyro_z_r])
			#qu_last_mpu10.put(gyro_right_list)
			#qu_last_mpu10.put(gyro_z_l)
			#qu_last_mpu10.put(gyro_z_r)

			gyro_left_list = []
			gyro_right_list = []


##################################################################################################################################
#ne metto uno solo direttamente in serial???
cpdef take_pressure_values(serNano2, event_ok_start_taking_press, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press):
	cdef bint first_after_stop = False
	cdef int i = 0
	print(">>>>>>>>>>>>>>>> 4 enter in take_pressures")
	while True:
		if event_ok_start_taking_press.is_set():
			print(">>>>>>>>>>>>>>>> 444 enter in the while ok take_pressures")
			event_continue_take_press.set()
			event_continue_take_press22222222.clear()
			#useless!! start automatically??? 
			serNano2.take_pressures_forever(event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press)
			first_after_stop = True
			
			#print("no maria io esco!!")
			#print("ciclo numero ciclo numero ciclo numero ciclo numero ", i)
			#print("no maria io esco!!")
			#print("ciclo numero ciclo numero ciclo numero ciclo numero ", i)
			
			#print("no maria io esco!!")
			i = i + 1			
			#event_ok_start_taking_press.clear()
			#mpx0,mpx1,mpx2,mpx3,mpx4 = [],[],[],[],[] #????
			#qu_press.put([mpx0,mpx1,mpx2,mpx3,mpx4])
			#event_ok_press_ready.clear()
		else:
			if first_after_stop:
				print("no maria io sto facendo il mega danno!!")
				first_after_stop = False
				#print("no maria io sto facendo il mega danno!!")
				#print("no maria io sto facendo il mega danno!!")
				#print("no maria io sto facendo il mega danno!!")
				#print("no maria io sto facendo il mega danno!!")

cdef stop_taking_mpx(serNano2, event_continue_take_press, event_ok_start_taking_press, event_continue_take_press22222222):
	event_continue_take_press.clear()
	event_continue_take_press22222222.set()
	event_ok_start_taking_press.clear()

def empty_lifoqueue(lifo_press):
	"""when is not reading...after hit or shove reaction"""
	if lifo_press.size()>100:
		while not lifo_press.empty():
			trash = lifo_press.get()

##################################################################################################################################
def multi_takescans(lifo_press, qu_last_mpu10, event_continue_take_press, event_lifo_press_filled):
	print(">>>>>>>>>>>>>>>> 6 enter in multi_takescans")
	''' start getting mpu and pressure outputs
	'''
	mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,gyro_z_l,gyro_z_r,pressures_q,mpus_q = [],[],[],[],[],[],[],[],[],[],[]

	if event_continue_take_press.is_set():
		if event_lifo_press_filled.is_set():
			#print(">>>>>>>>>>>>>>>> 666666666 666666666 666666666 666666666")
			#if qu_last_mpu10.empty():
			#	with open('output.txt', 'a') as f:
			#		print("lifo empty", file=f)
			#else:
			#	with open('output.txt', 'a') as f:
			#		print(">>>>>>>>>>>>>>>> 666666666", file=f)
			#print(">>>>>>>>>>>>>>>> 666666666 666666666 666666666 666666Z")
			pp = lifo_press.get()
			#print("pp pp pp pp pp pp pp pp =", pp)
			qq = qu_last_mpu10.get()
			#print("qq qq qq qq qq qq qq qq = ", qq)
			pressures_q.append(pp)
			#print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][0])
			#print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][1])
			#print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][2])
			#print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][3])
			#print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][4])
			#print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][0])
			#print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][1])
			#print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][2])
			#print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][3])
			#print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][4])
			
			mpus_q.append(qq)
			#print("6666 66666 mpus_q mpus_q mpus_q mpus_q  =", mpus_q)
			#print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q)
			#print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q)
			#print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q[1])
			#print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q[2])
			#print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q[3])

			lenlen = len(pressures_q[0][0])-10 
			mpx00 = pressures_q[0][0]
			mpx11 = pressures_q[0][1]
			mpx22 = pressures_q[0][2]
			mpx33 = pressures_q[0][3]
			mpx44 = pressures_q[0][4]

			if lenlen!=0:
				popped_0 = mpx00[:lenlen]
				del mpx00[:lenlen]
				popped_1 = mpx11[:lenlen]
				del mpx11[:lenlen]
				popped_2 = mpx22[:lenlen]
				del mpx22[:lenlen]
				popped_3 = mpx33[:lenlen]
				del mpx33[:lenlen]
				popped_4 = mpx44[:lenlen]
				del mpx44[:lenlen]

			mpx0_t = list(map(float, mpx00))
			mpx1_t = list(map(float, mpx11))
			mpx2_t = list(map(float, mpx22))
			mpx3_t = list(map(float, mpx33))
			mpx4_t = list(map(float, mpx44))
			mpx0 = [round(x, 2) for x in mpx0_t]
			mpx1 = [round(x, 2) for x in mpx1_t]
			mpx2 = [round(x, 2) for x in mpx2_t]
			mpx3 = [round(x, 2) for x in mpx3_t]
			mpx4 = [round(x, 2) for x in mpx4_t]
			giro_l = mpus_q[0][0]
			giro_r = mpus_q[0][1]
			gyro_z_l = mpus_q[0][2]
			gyro_z_r = mpus_q[0][3]

			#print("sono in multi_takescans, sono in multi_takescans, mpx0 =", mpx0)
			#print("sono in multi_takescans, sono in multi_takescans, mpx1 =", mpx1)
			#print("sono in multi_takescans, sono in multi_takescans, mpx2 =", mpx2)
			#print("sono in multi_takescans, sono in multi_takescans, mpx3 =", mpx3)
			#print("sono in multi_takescans, sono in multi_takescans, mpx4 =", mpx4)
			#print("sono in multi_takescans, sono in multi_takescans, giro_l =", giro_l)
			#print("sono in multi_takescans, sono in multi_takescans, giro_r =", giro_r)
			#print("sono in multi_takescans, sono in multi_takescans, gyro_z_l =", gyro_z_l)
			#print("sono in multi_takescans, sono in multi_takescans, gyro_z_r =", gyro_z_r)
			
			#if not event_ok_mpu_ready.is_set() and not event_ok_press_ready.is_set():
			pressures_q = []
			mpus_q = []
			return mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,gyro_z_l,gyro_z_r

def predict_contact(mpu1_left, mpu2_right, event_continue_take_press, event_lifo_press_filled, lifo_press, qu_last_mpu10):
	print(">>>>>>>>>>>>>>>> 7 enter in predict_contact")
	#creao liste????
	mpx0,mpx1,mpx2,mpx3,mpx4, giro_l,giro_r,girozl,girozr = multi_takescans(lifo_press, qu_last_mpu10, event_continue_take_press, event_lifo_press_filled)
	sample, flag_result, flag_fun = bcs.produce_input_sample(mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r)
	sample = bcs.produce_input_sample(mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r)
	loaded_model = joblib.load("/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/training_body_17genn_2023.sav")
	perceived_interaction = loaded_model.predict(sample)
	print('check uguale funny {}'.format(flag_fun))
	print("perceived_interaction is  ", perceived_interaction)
	print("perceived_interaction is  ", perceived_interaction)
	print("perceived_interaction is  ", perceived_interaction)
	print("perceived_interaction is  ", perceived_interaction)
	print("perceived_interaction is  ", perceived_interaction)
	print("perceived_interaction is  ", perceived_interaction)
	print("perceived_interaction is  ", perceived_interaction)
	print("perceived_interaction is  ", perceived_interaction)
	print("perceived_interaction is  ", perceived_interaction)
	return perceived_interaction, flag_result, flag_fun

def detect_touch(mpu1_left, mpu2_right, event_continue_take_press, event_take_touch, qu_start_prediction, qu_res_pred, lifo_press, qu_last_mpu10, event_lifo_press_filled, event_continue_take_press22222222):
	print(">>>>>>>>>>>>>>>> 8 enter in detect_touch")
	#flag_funny = False
	#flag_result = False
	while True:
		if event_take_touch.is_set():
			if not event_continue_take_press22222222.is_set():
				print(">>>>>>>>>>>>>>>> 88888 while inside ok!!! ")
				#print('entrato in multitake')
				#num_on_queue = qu_start_prediction.get()
				#if num_on_queue == 1:
				result_pred, flag_result, flag_fun= predict_contact(mpu1_left, mpu2_right, event_continue_take_press, event_lifo_press_filled, lifo_press, qu_last_mpu10)
				qu_res_pred.put([result_pred, flag_result, flag_fun, giro_l, giro_r])
				#here??? or later?? TESTA!!!
				#event_take_touch.clear()

def extract_touch(mpu1_left, mpu2_right, event_continue_take_press, event_take_touch, qu_start_prediction, qu_res_pred, lifo_press, qu_last_mpu10, event_lifo_press_filled, event_continue_take_press22222222):
	print(">>>>>>>>>>>>>>>> bella bella bella bella enter in detect_touch")
	#flag_funny = False
	#flag_result = False
	while True:
		if event_take_touch.is_set():
			if not event_continue_take_press22222222.is_set():
				print(">>>>>>>>>>>>>>>> bella bella bella bella while inside ok!!! ")
				#print('entrato in multitake')
				#num_on_queue = qu_start_prediction.get()
				#if num_on_queue == 1:
				mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,gyro_z_l,gyro_z_r = multi_takescans(lifo_press, qu_last_mpu10, event_continue_take_press, event_lifo_press_filled)
				
				print("==============================================================")
				print("sono in extract_touch, sono in multi_takescans, mpx0 =", mpx0)
				print("sono in extract_touch, sono in multi_takescans, mpx1 =", mpx1)
				print("sono in extract_touch, sono in multi_takescans, mpx2 =", mpx2)
				print("sono in extract_touch, sono in multi_takescans, mpx3 =", mpx3)
				print("sono in extract_touch, sono in multi_takescans, mpx4 =", mpx4)
				print("sono in extract_touch, sono in multi_takescans, giro_l =", giro_l)
				print("sono in extract_touch, sono in multi_takescans, giro_r =", giro_r)
				print("sono in extract_touch, sono in multi_takescans, gyro_z_l =", gyro_z_l)
				print("sono in extract_touch, sono in multi_takescans, gyro_z_r =", gyro_z_r)
				print("==============================================================")
				
				#other subprocess?????
				with open('/home/pi/quiet_prova.csv', mode='a') as ser_output:
					ser_writer = csv.writer(ser_output, delimiter=';')
					for n in range(10):
						#if first_line or os.stat('/home/pi/quiet_prova.csv').st_size == 0:
						# ser_writer.writerow(['Data recieved: '])
						# ser_writer.writerow([array0]','[array1]','[array2]','[array3]','[array4])
						# ok for reading better csvRow = [array0[n], array1[n], array2[n], array3[n], array4[n], "-->", giro_l[n], '',giro_r[n]]
						csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], giro_l[n], giro_r[n]]
						ser_writer.writerow(csvRow)
						#first_line = False
						if n==9:
							csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], giro_l[n], giro_r[n]]
							ser_writer.writerow(csvRow)
							ser_writer.writerow([])


##################################################################################################################################
#controllo!! ma non ora!!!
#creao event_main e q_reaction...
##################################################################################################################################
def react_to_touch(result_touch, event_main, num_quiet, qu_reaction, stopInterrupt, primary_lock, queue_lock):
	print(">>>>>>>>>>>>>>>> 10 enter in react_to_touch")
	command = serr.choose_command_touch(result_touch, num_quiet)
	comm_t=str(command,'utf-8')
	print("comm_t is")
	print(comm_t)
	#redo this if!!!!
	if((comm_t != '<qu1>' and comm_t != '<qu2>') or (comm_t == '<qu1>' and num_quiet==100) or (comm_t == '<qu2>' and num_quiet==100)):
		#call stage !
		serr.transmit(command) #ok last! devo mettere byte!! sbagliato!!!
		#play audio ... 
		#play(command)
		#sistemo!!
		#time.sleep(3)
		qu_reaction.put(1)
		event_main.clear()
		with rea.Stage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock) as reaction1:
			print("uga chaca")
##################################################################################################################################



def cancelme(event_cancel, lifo_press, event_continue_take_press22222222):
	while True:
		if event_cancel.is_set():
			if not event_continue_take_press22222222.is_set():
				list_lifo_finale = []
				arra = lifo_press.get()
				list_lifo_finale.append(arra)
				print("ARRA ARRA ARRA ARRA ARRA ARRA ARRA ARRA ARRA ARRA ARRA ARRA  ", arra)
	
	
	#while lifo_press.empty():
	#	arra = lifo_press.get()
	#	list_lifo_finale.append(arra)

	#for a in lifo_finale():
	#	print(a)

# ===============================================================================================================
#  Main
# ===============================================================================================================
def contact_manager_main(stopInterrupt, primary_lock, queue_lock):
	print(">>>>>>>>>>>>>>>> 0 enter in contact_manager_main contact_manager_main contact_manager_main ")
	#instatiating mpus object
	
	mpu1_left = bimu.IMU_sensor(device_Address_l)
	mpu2_right = bimu.IMU_sensor(device_Address_r)
	print(type(mpu1_left))
	print(type(mpu2_right))
	inspect.isclass(mpu1_left)
	inspect.isclass(mpu2_right)
	
	#with bimu.IMU_sensor(device_Address_l) as mpu1_left:
	#	print("getting mpu value...")
	#	print(type(mpu1_left))
	#with bimu.IMU_sensor(device_Address_r) as mpu2_right:
	#	print("getting mpu value...")
	#	print(type(mpu2_right))
	serNano2 = serr.setup_serial_nano2()
	serNano2.await_arduino()
	#ret_val = serNano2.await_arduinospecial()
	#print("ret_val ret_val ret_val", ret_val)
	#print("deca ret_val deca ret_val", ret_val.decode('UTF-8', 'strict'))

	time.sleep(5)

	gyrox_r1 = -150
	gyrox_l1 = 50
	gyroz_l1 = 4
	gyroz_r1 = -35
	gyrox_r2 = -150
	gyrox_l2 = 50
	gyroz_l2 = 4
	gyroz_r2 = -35
	#self ??? evento e code ancora???
	#gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(mpu1_left, mpu2_right)
	#gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(mpu1_left, mpu2_right)

	man = OimiManager()
	man.start()
	lifo_mpus = man.LifoQueue()
	OimiManager.register('lifo_press', LifoQueue)
	
	man1 = OimiManager()
	man1.start()
	OimiManager.register('lifo_mpus', LifoQueue)
	lifo_press = man1.LifoQueue()
	
	#It is ok to put size in OimiQueue??? no!! just name
	qu_start_prediction = OimiQueue("fifo qu_start_prediction")
	qu_res_pred = OimiQueue("fifo qu_res_pred")
	qu_last_mpu10 = OimiQueue("fifo qu_last_mpu10")
	qu_take_mpus = OimiQueue("fifo qu_take_mpus") 
	qu_allow_start_mpu_loop = OimiQueue("fifo qu_allow_start_mpu_loop") 
	qu_allow_take_mpus = OimiQueue("fifo qu_allow_take_mpus") 
	event_ok_start_taking_press = Event()
	event_start_get_mpu = Event()
	event_take_mpus_from_lifo = Event()
	event_ok_single_ready = Event()
	event_take_touch = Event()
	event_continue_take_press = Event()
	event_continue_take_press22222222 = Event()

	event_cancel = Event()
	event_lifo_press_filled = Event()

	###################### to fix ###################### to fix ###################### to fix ###################### to fix 
	job1_begin_to_scan_pressures_continuosly = multiprocessing.Process(name='job1_begin_to_scan_pressures_continuosly', target=take_pressure_values, args=(serNano2, event_ok_start_taking_press, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press,))
	
	job2_begin_to_scan_mpu_continuosly = multiprocessing.Process(name='job2_begin_to_scan_mpu_continuosly', target=queue_MPU_values, args=(mpu1_left, mpu2_right, event_start_get_mpu, qu_allow_start_mpu_loop, lifo_mpus,))
	
	#poi canecello job8_take_press
	#job8_take_press = multiprocessing.Process(name='job8_take_press', target=cancelme, args=(event_cancel, lifo_press, event_continue_take_press22222222,))
	job22_take_mpus = multiprocessing.Process(name='job22_take_mpus', target=take_10_MPU_values, args=(mpu1_left, mpu2_right, event_take_mpus_from_lifo, qu_allow_take_mpus, qu_last_mpu10, lifo_mpus,))
	job3_detect_touch = multiprocessing.Process(name='job3_detect_touch', target=detect_touch, args=(mpu1_left, mpu2_right, event_continue_take_press, event_take_touch, qu_start_prediction, qu_res_pred, lifo_press, qu_last_mpu10, event_lifo_press_filled, event_continue_take_press22222222,))
	
	job1_begin_to_scan_pressures_continuosly.start()
	job2_begin_to_scan_mpu_continuosly.start()
	#job8_take_press.start()
	job22_take_mpus.start()
	job3_detect_touch.start()
	print("inizio seconda parte")
	event_ok_start_taking_press.set()
	event_start_get_mpu.set()
	qu_allow_start_mpu_loop.put(1)
	time.sleep(2)
	event_take_mpus_from_lifo.set()
	qu_allow_take_mpus.put(1)
	event_take_touch.set()
	qu_start_prediction.put(1)
	#aaa = qu_res_pred.get()
	print("inizio terza parte")
	time.sleep(10)
	#event_cancel.set()
	#time.sleep(10)
	stop_taking_mpx(serNano2, event_continue_take_press, event_ok_start_taking_press, event_continue_take_press22222222)
	time.sleep(3)

	job1_begin_to_scan_pressures_continuosly.join()
	job2_begin_to_scan_mpu_continuosly.join()
	job22_take_mpus.join()
	job3_detect_touch.join()

	print("esco!!! ho finito!!!")

	job1_begin_to_scan_pressures_continuosly.close()
	job2_begin_to_scan_mpu_continuosly.close()
	job22_take_mpus.close()
	job3_detect_touch.close()
	print("esco!!! ho finito!!!")


	'''
	job1_begin_to_scan_pressures_continuosly.join()
	job2_begin_to_scan_mpu_continuosly.join()
	
		

	
	



	#job4 = multiprocessing.Process(name='capat', target=capathread, args=(qu5,qu6,event3,))
	#job4.start()
	'''



	## create a list proxy and append a mutable object (a dictionary)
	#lproxy = manager.list()
	#lproxy.append({})
	## now mutate the dictionary
	#d = lproxy[0]
	#d['a'] = 1
	#d['b'] = 2
	## at this point, the changes to d are not yet synced, but by
	## reassigning the dictionary, the proxy is notified of the change
	#lproxy[0] = d

def contact_manager_for_data_acquisition():
	print(">>>>>>>>>>>>>>>> 0 enter in contact_manager_main contact_manager_main contact_manager_main ")
	#instatiating mpus object
	
	mpu1_left = bimu.IMU_sensor(device_Address_l)
	mpu2_right = bimu.IMU_sensor(device_Address_r)
	print(type(mpu1_left))
	print(type(mpu2_right))
	inspect.isclass(mpu1_left)
	inspect.isclass(mpu2_right)
	
	#with bimu.IMU_sensor(device_Address_l) as mpu1_left:
	#	print("getting mpu value...")
	#	print(type(mpu1_left))
	#with bimu.IMU_sensor(device_Address_r) as mpu2_right:
	#	print("getting mpu value...")
	#	print(type(mpu2_right))
	serNano2 = serr.setup_serial_nano2()
	serNano2.await_arduino()
	#ret_val = serNano2.await_arduinospecial()
	#print("ret_val ret_val ret_val", ret_val)
	#print("deca ret_val deca ret_val", ret_val.decode('UTF-8', 'strict'))

	time.sleep(5)

	gyrox_r1 = -150
	gyrox_l1 = 50
	gyroz_l1 = 4
	gyroz_r1 = -35
	gyrox_r2 = -150
	gyrox_l2 = 50
	gyroz_l2 = 4
	gyroz_r2 = -35
	#self ??? evento e code ancora???
	#gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(mpu1_left, mpu2_right)
	#gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(mpu1_left, mpu2_right)

	man = OimiManager()
	man.start()
	lifo_mpus = man.LifoQueue()
	OimiManager.register('lifo_press', LifoQueue)
	
	man1 = OimiManager()
	man1.start()
	OimiManager.register('lifo_mpus', LifoQueue)
	lifo_press = man1.LifoQueue()

	man2 = OimiManager()
	man2.start()
	OimiManager.register('qu_last_mpu10', LifoQueue)
	qu_last_mpu10 = man1.LifoQueue()

	
	#It is ok to put size in OimiQueue??? no!! just name
	qu_start_prediction = OimiQueue("fifo qu_start_prediction")
	qu_res_pred = OimiQueue("fifo qu_res_pred")
	qu_take_mpus = OimiQueue("fifo qu_take_mpus") 
	qu_allow_start_mpu_loop = OimiQueue("fifo qu_allow_start_mpu_loop") 
	qu_allow_take_mpus = OimiQueue("fifo qu_allow_take_mpus") 
	event_ok_start_taking_press = Event()
	event_start_get_mpu = Event()
	event_take_mpus_from_lifo = Event()
	event_ok_single_ready = Event()
	event_take_touch = Event()
	event_continue_take_press = Event()
	event_continue_take_press22222222 = Event()

	event_cancel = Event()
	event_lifo_press_filled = Event()

	###################### to fix ###################### to fix ###################### to fix ###################### to fix 
	job1_begin_to_scan_pressures_continuosly = multiprocessing.Process(name='job1_begin_to_scan_pressures_continuosly', target=take_pressure_values, args=(serNano2, event_ok_start_taking_press, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press,))
	
	job2_begin_to_scan_mpu_continuosly = multiprocessing.Process(name='job2_begin_to_scan_mpu_continuosly', target=queue_MPU_values, args=(mpu1_left, mpu2_right, event_start_get_mpu, qu_allow_start_mpu_loop, lifo_mpus,))
	
	#poi canecello job8_take_press
	#job8_take_press = multiprocessing.Process(name='job8_take_press', target=cancelme, args=(event_cancel, lifo_press, event_continue_take_press22222222,))
	job22_take_mpus = multiprocessing.Process(name='job22_take_mpus', target=take_10_MPU_values, args=(mpu1_left, mpu2_right, event_take_mpus_from_lifo, qu_allow_take_mpus, qu_last_mpu10, lifo_mpus,))
	job3_extract_touch = multiprocessing.Process(name='job3_extract_touch', target=extract_touch, args=(mpu1_left, mpu2_right, event_continue_take_press, event_take_touch, qu_start_prediction, qu_res_pred, lifo_press, qu_last_mpu10, event_lifo_press_filled, event_continue_take_press22222222,))
	
	job1_begin_to_scan_pressures_continuosly.start()
	job2_begin_to_scan_mpu_continuosly.start()
	#job8_take_press.start()
	job22_take_mpus.start()
	job3_extract_touch.start()
	print("inizio seconda parte")
	event_ok_start_taking_press.set()
	event_start_get_mpu.set()
	qu_allow_start_mpu_loop.put(1)
	time.sleep(2)
	event_take_mpus_from_lifo.set()
	qu_allow_take_mpus.put(1)
	event_take_touch.set()
	qu_start_prediction.put(1)
	#aaa = qu_res_pred.get()
	print("inizio terza parte")
	time.sleep(300)
	#event_cancel.set()
	#time.sleep(10)
	stop_taking_mpx(serNano2, event_continue_take_press, event_ok_start_taking_press, event_continue_take_press22222222)
	#time.sleep(3)

	job1_begin_to_scan_pressures_continuosly.join()
	job2_begin_to_scan_mpu_continuosly.join()
	job22_take_mpus.join()
	job3_extract_touch.join()

	print("esco!!! ho finito!!!")

	job1_begin_to_scan_pressures_continuosly.close()
	job2_begin_to_scan_mpu_continuosly.close()
	job22_take_mpus.close()
	job3_extract_touch.close()
	print("esco!!! ho finito!!!")