cdef class IMU_sensor:
	cdef:
		int PWR_MGMT_1  
		int SMPLRT_DIV  
		int CONFIG      
		int GYRO_CONFIG 
		int INT_ENABLE  
		int ACCEL_XOUT_H
		int ACCEL_YOUT_H
		int ACCEL_ZOUT_H
		int GYRO_XOUT_H 
		int GYRO_YOUT_H 
		int GYRO_ZOUT_H 

		int device_Address
		bus

	cpdef setup_mpu(self)
	cpdef MPU_Init(self)
	cpdef read_raw_data(self, addr)
	cpdef get_gyroX(self)
	cpdef get_gyroY(self)
	cpdef get_gyroZ(self)
