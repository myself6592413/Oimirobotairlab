import IMU_sensor as bimu
import body_create_sample as bcs
import inspect, time

device_Address_l = 0x69   # MPU6050 device address 1
device_Address_r = 0x68   # MPU6050 device address 2


mpu1_left = bimu.IMU_sensor(device_Address_l)
mpu2_right = bimu.IMU_sensor(device_Address_r)

print(type(mpu1_left))
print(type(mpu2_right))
inspect.isclass(mpu1_left)
inspect.isclass(mpu2_right)
n = 0
while n<100:
    n+=1

    gyroxl, gyrozl, gyroxr, gyrozr = bcs.scan_MPUs(mpu1_left, mpu2_right)
    #gyroxl = mpu1_left.get_gyroX()
    #gyrozl = mpu1_left.get_gyroZ()
    #gyroxr = mpu2_right.get_gyroX()
    #gyrozr = mpu2_right.get_gyroZ()

    print('gyro1 inside takempus [{} , {}, {}, {}]'.format(gyroxl,gyrozl,gyroxr,gyrozr))

    time.sleep(1)

