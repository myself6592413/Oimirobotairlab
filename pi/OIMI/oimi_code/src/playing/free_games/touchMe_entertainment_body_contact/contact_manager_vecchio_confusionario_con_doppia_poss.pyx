"""Info:
	Oimi body calculate features for the logistic regression model = aka column of the dataset
	-Max
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
	Pressure signals of 10 / 12 samples 
	
	Min value eliminated
	Acce_std eliminated

	Final sav file imported = > created by "only_training_logistic_regression.py" file
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import time, sys, math
#import numpy as np
#import pyximport; pyximport.install()
#from array import array
import multiprocessing
from multiprocessing import Pool
import timedelta
import queue
from contextlib import closing #to test!!! --> using #with closing(bimu.IMU_sensor(device_Address_l)) as mpu1_left:

import IMU_sensor as bimu
import body_create_sample as bcs
import body_calculate_features as bcf
import serial_manager as serr
from oimi_process import OimiProcess, OimiStartProcess, OimiQueue, OimiManager
from queue import LifoQueue
# ==========================================================================================================================================================
# Variables
# ==========================================================================================================================================================
mpx0, mpx1, mpx2, mpx3, mpx4, giro_l, giro_r = [],[],[],[],[],[],[] #useful????
mpx0, mpx1_prev, mpx2_prev, mpx3_prev, mpx4_prev, girox_prev, mean_gir_prev = [],[],[],[],[],[],[] #useful????
start_time = time.time()
n, cycle=0,0
num_quiet=0
result = 'initial'
fla_result = 0
fla_funny = 0
device_Address_l = 0x69   # MPU6050 device address 1
device_Address_r = 0x68	  # MPU6050 device address 2

# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cpdef queue_MPU_values(mpu1_left, mpu2_right, event_start_get_mpu, lifo_mpus):
	print(">>>>>>>>>>>>>>>> 1 queue_MPU_values")
	i = 0
	while True:
		event_start_get_mpu.wait()
		#addictive control...is necessary??? lets start with no choice
		#res = qu_allow_start_mpu_loop.get()
		#if res == [3]:
		gyroxl, gyrozl, gyroxr, gyrozr = bcs.scan_MPUs(mpu1_left, mpu2_right)
		#one_single_elem or many??
		#single because all should be taken together
		lifo_mpus.put([gyroxl,gyrozl,gyroxr,gyrozr])
		#or??? many in several queue nodes
		#qu_mpu.put(gyroxl1)	
		#qu_mpu.put(gyrozl1)
		#qu_mpu.put(gyroxr1)
		#qu_mpu.put(gyrozr1)	
		#qu_mpu.put(gyroxl2)	
		#qu_mpu.put(gyrozl2)	
		#qu_mpu.put(gyroxr2)	
		#qu_mpu.put(gyrozr2)
		#get size!!
		i = i+1
		if i < 10:
			print("size lifo_mpus????size lifo_mpus????size lifo_mpus????size lifo_mpus????size lifo_mpus???? is ", lifo_mpus.size())

'''
cpdef take_MPU_double(mpu1_left, mpu2_right, qu_mpu2):
	while True:
		event_start_get_mpu
		gyroxl1,gyrozl1,gyroxr1,gyrozr1,gyroxl2,gyrozl2,gyroxr2,gyrozr2	= bcs.taketwoMPU(mpu1_left, mpu2_right)
		#one_single_elem or many??
		#single
		qu_mpu.put([gyroxl1,gyrozl1,gyroxr1,gyrozr1,gyroxl2,gyrozl2,gyroxr2,gyrozr2])
		#many in several queue nodes
		#qu_mpu.put(gyroxl1)	
		#qu_mpu.put(gyrozl1)	
		#qu_mpu.put(gyroxr1)	
		#qu_mpu.put(gyrozr1)	
		#qu_mpu.put(gyroxl2)	
		#qu_mpu.put(gyrozl2)	
		#qu_mpu.put(gyroxr2)	
		#qu_mpu.put(gyrozr2)
'''

cpdef extract_MPU_values_from_lifo(event_take_mpus_from_lifo, lifo_mpus):
	#it make sense take a step again to encapsulate and return values??
	print(">>>>>>>>>>>>>>>> 2 extract_MPU_values_from_lifo")
	i = 0
	while True:
		event_take_mpus_from_lifo.wait()
		#res = qu1.get()
		#if res == [3]:
		mpu12 = lifo_mpus
		gyroxl, gyrozl, gyroxr, gyrozr = lifo_mpus[0], lifo_mpus[1], lifo_mpus[2], lifo_mpus[3]
		return gyroxl, gyrozl, gyroxr, gyrozr

cpdef take_10_MPU_values(mpu1_left, mpu2_right, qu_last_mpu10, event_ok_mpu_ready):
	print(">>>>>>>>>>>>>>>> 3 enter in take_10_MPU_values")
	cdef:
		Py_ssize_t i = 0
		Py_ssize_t leng = 10
	gyro_left_list, gyro_right_list= [],[] ##?? useful or not????

	for i in range(leng):
		#old 
		#gyro_x_l = mpu1_left.get_gyroX()
		#gyro_x_r = mpu2_right.get_gyroX()
		gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo()
		gyro_left_list.append(gyroxl)
		gyro_right_list.append(gyroxr)

		#queue also these???
		gyro_z_l = mpu1_left.get_gyroZ()
		gyro_z_r = mpu2_right.get_gyroZ()

	qu_last_mpu10.put(gyro_left_list)
	qu_last_mpu10.put(gyro_right_list)
	qu_last_mpu10.put(gyro_z_l)
	qu_last_mpu10.put(gyro_z_r)
	event_ok_mpu_ready.clear()

sooner or later...
transmit(presstop)
##################################################################################################################################
cpdef take_pressures(qu_press, serMega, event_ok_press_ready, event_continue_take_press):
	print(">>>>>>>>>>>>>>>> 4 enter in take_pressures")
	event_continue_take_press.set()
	#mpx0,mpx1,mpx2,mpx3,mpx4 = [],[],[],[],[] #????
	mpx0,mpx1,mpx2,mpx3,mpx4 = serMega.take_pressures_forever(event_continue_take_press)
	qu_press.put([mpx0,mpx1,mpx2,mpx3,mpx4])
	event_ok_press_ready.clear()
##################################################################################################################################
def onetakescan(start_marker,end_marker,ser,qu1,qu2,event1):
	print(">>>>>>>>>>>>>>>> 5 enter in onetakescan")
	n=0
	for n in range(1000000):
		event1.wait()
		#print('entrato in onetake')
		res = qu1.get()
		if res == [3]:
			mpx0,mpx1,mpx2,mpx3,mpx4 = sc.takeonescan(start_marker,end_marker,ser)
			qu2.put([mpx0,mpx1,mpx2,mpx3,mpx4])

def multi_takescans(mpu1_left, mpu2_right):
	print(">>>>>>>>>>>>>>>> 6 enter in multi_takescans")
	''' start getting mpu and pressure outputs
	'''
	qu_mpu2 = Queue(1000)
	qu_mpu10 = Queue(1000)
	qu_press = Queue(10)

	event_ok_mpu_ready = Event()
	event_ok_press_ready = Event()
	job_mpu = multiprocessing.Process(name='job_mpu', target=take_MPU_values, args=(mpu1_left, mpu2_right, qu_mpu10, event_ok_mpu_ready)) #N.B. last comma!
	job_mpu.start()
	job_press = multiprocessing.Process(name='job_press', target=take_pressures, args=(qu_press, serMega, event_ok_press_ready,))
	job_press.start()

	#join?? not necessary right???
	#job_press.join()
	#job_press.join()
	if not event_ok_mpu_ready.is_set() and not event_ok_press_ready.is_set():
		return mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,gyro_z_l,gyro_z_r

def predict_contact(mpu1_left, mpu2_right):
	print(">>>>>>>>>>>>>>>> 7 enter in predict_contact")
	#creao liste????
	mpx0,mpx1,mpx2,mpx3,mpx4, giro_l,giro_r,girozl,girozr = multi_takescans(mpu1_left, mpu2_right)
	sample, flag_result, flag_fun = bcs.produce_input_sample(mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,girozl,girozr)
	loaded_model = joblib.load("/home/pi/train_13apr.sav")
	perceived_interaction = loaded_model.predict(sample)
	print('check uguale funny {}'.format(flag_fun))
	print("perceived_interaction is  ", perceived_interaction)
	return perceived_interaction
##################################################################################################################################
def detect_touch(mpu1_left, mpu2_right, qu3_start_prediction, qu4_res_pred, event_take_touch):
	print(">>>>>>>>>>>>>>>> 8 enter in detect_touch")
	n=0
	#flag_funny = False
	#flag_result = False
	while True:
		event_take_touch.wait()
		#print('entrato in multitake')
		num_on_queue = qu3_start_prediction.get()
		if num_on_queue == [2]:
			result_pred = predict_contact(mpu1_left, mpu2_right)
			qu4_res_pred.put([result_pred, flag_result, flag_funny, giro_l, giro_r])
			#here??? or later?? TESTA!!!
			#event_take_touch.clear()

def detect_single_scan_pressure(event_start_single_scan, event_ok_single_ready, qu1, qu2, qu_single, serMega):
	print(">>>>>>>>>>>>>>>> 9 enter in detect_single_scan_pressure")
	while True:
		event_start_single_scan.wait()
		#print('entrato in onetake')
		res = qu1.get()
		if res == [3]:
			mpx0,mpx1,mpx2,mpx3,mpx4 = serMega.take_one_scan_pressure()
			#or with parameters???
			#mpx0,mpx1,mpx2,mpx3,mpx4 = serMega.take_one_scan_pressure(mp0,mp1,mp2,mp3,mp4)
			qu2.put([mpx0,mpx1,mpx2,mpx3,mpx4])
			event_ok_single_ready.clear()

##################################################################################################################################
def react_to_touch():
	print(">>>>>>>>>>>>>>>> 10 enter in react_to_touch")
	command = serr.choose_command_touch(result_touch, num_quiet)
	comm_t=str(command,'utf-8')	
	print("comm_t is")
	print(comm_t)
	#redo this if!!!!
	if((comm_t != '<qu1>' and comm_t != '<qu2>') or (comm_t == '<qu1>' and num_quiet==100) or (comm_t == '<qu2>' and num_quiet==100)):
		#call stage !
		serr.transmit(command) #ok last!
		#play audio ... 
		play(command)
		#sistemo!!
        #time.sleep(3)
        coda.put(1)
        event_main.clear()
        with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            print("uga chaca")
# ===============================================================================================================
#  Main
# ===============================================================================================================
def contact_manager_main(stopInterrupt, primary_lock, queue_lock):
	print(">>>>>>>>>>>>>>>> 0 enter in contact_manager_main contact_manager_main contact_manager_main ")
    #instatiating mpus object
    with bimu.IMU_sensor(device_Address_l) as mpu1_left:
    	print("getting mpu value...")
    with bimu.IMU_sensor(device_Address_r) as mpu2_right:
    	print("getting mpu value...")

	
	gyrox_r1 = -150
	gyrox_l1 = 50
	gyroz_l1 = 4
	gyroz_r1 = -35
	gyrox_r2 = -150
	gyrox_l2 = 50
	gyroz_l2 = 4
	gyroz_r2 = -35
	#self ??? evento e code ancora???
	gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(mpu1_left, mpu2_right)
	gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(mpu1_left, mpu2_right)

    man = OimiManager()
    manager.start()
    lifo_mpus = man.LifoQueue()
    OimiManager.register('LifoQueue', LifoQueue)
    manager.start()
    #It is ok to put size in OimiQueue??? no!! just name
	qu1 = OimiQueue("fifo ")
	qu2 = OimiQueue("fifo ")
	qu3 = OimiQueue("fifo ")
	qu4 = OimiQueue("fifo ")
	qu5 = OimiQueue("fifo ")
	qu6 = OimiQueue("fifo ")
	qu_mpu = = OimiQueue("fifo ")
	qu_mpu2 = = OimiQueue("fifo ")
	qu_mpu_continue = = OimiQueue("fifo ")
	qu_last_mpu10 = OimiQueue("fifo ")
	
	event_start_single_scan = Event()
	event_start_get_mpu = Event()
	event_ok_mpu_ready = Event()
	event_ok_single_ready = Event()
	event_take_touch = Event()
	

	###################### to fix ###################### to fix ###################### to fix ###################### to fix 
	job1_extract_single_pressure_scan = multiprocessing.Process(name='takingpressone', target=onetakescan, args=(start_marker,end_marker,ser,qu1,qu2,event1,))
	job1_extract_single_pressure_scan.start()
	job2_detect_touch = multiprocessing.Process(name='takingpressmulti', target=detect_touch, args=(start_marker,end_marker,ser,qu3,qu4,event2,))
	job2_extract_single_pressure_scan.start()
	#job3 = multiprocessing.Process(name='capat', target=capathread, args=(qu5,qu6,event3,))
	#job3.start()

	sys.exit()

def contact_manager_vero_but_too_complicate_for_now(stopInterrupt, primary_lock, queue_lock):
	print(">>>>>>>>>>>>>>>> 0 enter in contact_manager_main contact_manager_main contact_manager_main ")
    #instatiating mpus object
    with bimu.IMU_sensor(device_Address_l) as mpu1_left:
    	print("getting mpu value...")
    with bimu.IMU_sensor(device_Address_r) as mpu2_right:
    	print("getting mpu value...")

	gyrox_r1 = -150
	gyrox_l1 = 50
	gyroz_l1 = 4
	gyroz_r1 = -35
	gyrox_r2 = -150
	gyrox_l2 = 50
	gyroz_l2 = 4
	gyroz_r2 = -35

	#old...need the array right???
	#giroxl1,girozl1,giroxr1,girozr1,giroxl2,girozl2,giroxr2,girozr2 = sc.taketwoMPU(gyrox_l1,gyroz_l1,gyrox_r1,gyroz_r1,gyrox_l2,gyroz_l2,gyrox_r2,gyroz_r2)
	#giroxl1,girozl1,giroxr1,girozr1,giroxl2,girozl2,giroxr2,girozr2 = sc.taketwoMPU(gyrox_l1,gyroz_l1,gyrox_r1,gyroz_r1,gyrox_l2,gyroz_l2,gyrox_r2,gyroz_r2)
	gyroxl1,gyrozl1,gyroxr1,gyrozr1,gyroxl2,gyrozl2,gyroxr2,gyrozr2	= bcs.taketwoMPU(mpu1_left, mpu2_right)
	gyroxl1,gyrozl1,gyroxr1,gyrozr1,gyroxl2,gyrozl2,gyroxr2,gyrozr2	= bcs.taketwoMPU(mpu1_left, mpu2_right)


    man = OimiManager()
    manager.start()
    lifo_mpus = man.LifoQueue()
    OimiManager.register('LifoQueue', LifoQueue)
    manager.start()
    #It is ok to put size in OimiQueue??? no!! just name
	qu1 = OimiQueue("fifo ")
	qu2 = OimiQueue("fifo ")
	qu3 = OimiQueue("fifo ")
	qu4 = OimiQueue("fifo ")
	qu5 = OimiQueue("fifo ")
	qu6 = OimiQueue("fifo ")
	qu_mpu = = OimiQueue("fifo ")
	qu_mpu2 = = OimiQueue("fifo ")
	qu_mpu_continue = = OimiQueue("fifo ")
	qu_last_mpu10 = OimiQueue("fifo ")
	
	event_start_single_scan = Event()
	event_start_get_mpu = Event()
	event_ok_mpu_ready = Event()
	event_ok_single_ready = Event()
	event_take_touch = Event()
	

	###################### to fix ###################### to fix ###################### to fix ###################### to fix 
	job1 = multiprocessing.Process(name='takingpressone', target=onetakescan, args=(start_marker,end_marker,ser,qu1,qu2,event1,))
	job1.start()
	job2 = multiprocessing.Process(name='takingpressmulti', target=detect_touch, args=(start_marker,end_marker,ser,qu3,qu4,event2,))
	job2.start()
	#job3 = multiprocessing.Process(name='capat', target=capathread, args=(qu5,qu6,event3,))
	#job3.start()
	fla=False
	for n in range(1000000):
		#sc.capa()
		event3.set()
		if fla==False:
			qu5.put([5])
			re = qu6.get()
			if re==[1]:
				fla=True
				print("capatouched is {} ".format(re))
				break
		print('num_quiet {}'.format(num_quiet))
		if result=='initial':
			giroxl1,girozl1,giroxr1,girozr1,giroxl2,girozl2,giroxr2,girozr2 = sc.taketwoMPU(gyrox_l1,gyroz_l1,gyrox_r1,gyroz_r1,gyrox_l2,gyroz_l2,gyrox_r2,gyroz_r2)
		elif result == 'quiet' or result=='contact':
			time.sleep(0.2)
			event1.set()
			qu1.put([3])
			current_press = qu2.get()
			event1.clear()
			giroxl1,girozl1,giroxr1,girozr1,giroxl2,girozl2,giroxr2,girozr2 = sc.taketwoMPU(gyrox_l1,gyroz_l1,gyrox_r1,gyroz_r1,gyrox_l2,gyroz_l2,gyrox_r2,gyroz_r2)
		elif result == 'hug':
			#mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,girozl1,girozr1= sc.serial_takepressure_withgiro_both(start_marker, end_marker, ser)
			event2.set()
			qu3.put([2])
			resu = qu4.get()
			print('resuuuuu is {}'.format(resu))
			result = resu[0]
			fla_result = resu[1]
			fla_funny = resu[2]
			gir_l = resu[3]
			gir_r = resu[4]
			print('resulttt {}'.format(result))
			print('resmpu  {}'.format(fla_funny))
			event2.clear()
			giroxl1 = gir_l[9]
			giroxr1 = gir_r[9]
		elif result == 'touch':
			time.sleep(2)
			giroxl1,girozl1,giroxr1,girozr1,giroxl2,girozl2,giroxr2,girozr2 = sc.taketwoMPU(gyrox_l1,gyroz_l1,gyrox_r1,gyroz_r1,gyrox_l2,gyroz_l2,gyrox_r2,gyroz_r2)
		elif result == 'hit_back' or result == 'hit_front' or result == 'shove' or result == 'caress' or result=='squeeze' or result=='sgigot':
			giroxl1,girozl1,giroxr1,girozr1,giroxl2,girozl2,giroxr2,girozr2 = sc.taketwoMPU(gyrox_l1,gyroz_l1,gyrox_r1,gyroz_r1,gyrox_l2,gyroz_l2,gyrox_r2,gyroz_r2)
			time.sleep(1.8)
			giroxr1 = -150
			giroxl1 = 50
			girozl1 = 4
			girozr1 = -35
			giroxr2 = -150
			giroxl2 = 50
			girozl2 = 4
			girozr2 = -35

		if (((((abs(giroxr1)<143 or abs(giroxr1)>159) and (abs(girozr1)<32 or abs(girozr1)>40)) or ((giroxl1<38 or giroxl1>60) and (girozl1<0 or girozl1>9))) \
			or \
			(((abs(giroxr2)<143 or abs(giroxr2)>159) and (abs(girozr2)<32 or abs(girozr2)>40)) or ((giroxl2<38 or giroxl2>60) and (girozl2<0 or girozl2>9)))) \
			and (current_press[0]>0.26 or current_press[1]>0.22 or current_press[2]>0.31 or current_press[3]>0.26 or current_press[4]>0.25)):

			if abs(giroxr1)>=1000 or abs(giroxl1)>=650 or abs(girozr1)>=800 or abs(girozl1)>=1200 or abs(giroxr2)>=1300 or abs(giroxl2)>=1300 or abs(girozr2)>=450 or abs(girozl2)>=1000 :
				result='shove'
				print("SHOVEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE")
				#time.sleep(0.02)
				event1.set()
				qu1.put([3])
				current_press = qu2.get()
				event1.clear()
				if (current_press[0]>=0.25 or current_press[1]>=0.23 or current_press[3]>=0.27 or current_press[2]>=0.31 or current_press[4]>=0.27):
					#print('current is{}'.format(current_press))
					cucu0 = math.fabs(0.21 - current_press[0])
					cucu1 = math.fabs(0.20 - current_press[1])
					cucu2 = math.fabs(0.28 - current_press[2])
					cucu3 = math.fabs(0.23 - current_press[3])
					cucu4 = math.fabs(0.23 - current_press[4])
					print('cuccuu[[{},{},{},{},{}]]'.format(cucu0,cucu1,cucu2,cucu3,cucu4))
					mass =max(cucu0,cucu1,cucu2,cucu3,cucu4)
					if mass == cucu0:
						result = 'hit_back'
					elif  mass==cucu1:
						result = 'hit_right'
					elif  mass==cucu3:
						reult = 'hit_left'
					elif mass==cucu2 or mass==cucu4:
						result = 'hit_front'
				#if (current_press[0]>=0.24) or (current_press[1]>=0.22) or (current_press[3]>=0.26):
				#	result = 'hit_back'
				#elif (current_press[2]>=0.30) or (current_press[4]>=0.26):
				#	result = 'hit_front'
			else:
				if result != 'hug':
					time.sleep(0.9)
					event1.clear()
					event2.set()
					qu3.put([2])
					resu = qu4.get()
					print('resuuuuu is {}'.format(resu))
					result = resu[0]
					fla_result = resu[1]
					fla_funny = resu[2]
					print('resulttt {}'.format(result))
					print('resmpu {}'.format(fla_funny))
					event2.clear()
					#mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,girozl,girozl= sc.serial_takepressure_withgiro_both(start_marker, end_marker, ser)
					#print(giro_l)
					#print(giro_r)
					#print('[{},{},{},{},{}]'.format(mpx0,mpx1,mpx2,mpx3,mpx4))
					#result = sc.calcgibo(mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r)
		else:
			result = 'quiet'
		duration = time.time()-start_time
		#print("definitive result is:")
		#print(result)
		#print('Time is {}'.format(duration))
		#print('Cycle is {}'.format(n))
		print('flag_result is {}'.format(fla_result))
		print('flag_funny  is {}'.format(fla_funny))
		if result == 'caress' and fla_result==1:
			result = 'contact'
			print("definitive result is:{}".format(result))
			fla_result = 0
		if result == 'touch' and fla_funny==1 and fla_result==0:
			result = 'caress'
			print("definitive result is	:{}".format(result))
			fla_funny = 0
		if result == 'touch' and fla_funny==1 and fla_result==1:
			result = 'contact'
			print("definitive result is:{}".format(result))
			fla_funny = 0
			fla_result = 0
		command = sc.choose_comm(result,num_quiet)
		commando = str(command,'utf-8')
		print("commando is")
		print(commando)
		if((commando != '<qu1>' and commando != '<qu2>') or (commando == '<qu1>' and num_quiet==100) or (commando == '<qu2>' and num_quiet==100)):
			pool = Pool(processes=2)
			pred = pool.apply_async(sc.serial_printprediction, [start_marker,end_marker,ser,commando])
			pla  = pool.apply_async(sc.play, [commando])

			pool.close()
			pool.join()
			#play(command)
			#serial_printprediction(startma, endma, ser, command)
			#sc.play(command)
		#if commando == '<ca1>' or commando == '<ca2>':
		#	play(command)
		sc.react(start_marker,end_marker,ser,result,num_quiet)
		if result == 'quiet':
			num_quiet+=1
		if num_quiet>100:
			num_quiet=0
		if result != 'quiet' and num_quiet>50:
			num_quiet=0
		print ('quiet n is {}'.format(num_quiet))
		n+=1
		event3.clear()
	ser.close()

	sys.exit()
