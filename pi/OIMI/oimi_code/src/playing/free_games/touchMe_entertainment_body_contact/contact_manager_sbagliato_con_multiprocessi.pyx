"""Info:
    Oimi body calculate features for the logistic regression model = aka column of the dataset
    -Max
Notes:
    For detailed info, look at ./code_docs_of_dabatase_manager
    Pressure signals of 10 / 12 samples 
    
    Min value eliminated
    Acce_std eliminated

    Final sav file imported = > created by "only_training_logistic_regression.py" file
Author:
    Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
#remove? since useless at this point...
import time, sys, math, os, random
if '/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/')
if '/home/pi/OIMI/oimi_code/src/managers/serial_manager/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/serial_manager/')
import asyncio
import multiprocessing
import inspect
import serial
#from multiprocessing import Pool
from multiprocessing.pool import ThreadPool as Pool
import timedelta
import queue
import csv
from contextlib import closing #to test!!! --> using #with closing(bimu.IMU_sensor(device_Address_l)) as mpu1_left:
import joblib
import IMU_sensor as bimu
import body_create_sample as bcs
#import body_calculate_features as bcf
import serial_manager as serr
import multiprocessing
from multiprocessing import Process, Pool, Pipe, Event, Lock, Value
from oimi_process import OimiProcess, OimiStartProcess, OimiQueue, OimiManager
from queue import LifoQueue

import prediction_body_contact_definitive as pbcd
cdef extern from"Python.h":
    object PyList(float *s, Py_ssize_t leng)

from subprocess import run
import shared as shar
import sounds_playback as sp
import serial_manager as serr
import moving.std_navigation.std_navigation as move
# ==========================================================================================================================================================
# Variables
# ==========================================================================================================================================================
mpx0, mpx1, mpx2, mpx3, mpx4, giro_l, giro_r = [],[],[],[],[],[],[] #useful????
start_time = time.time()
n, cycle=0,0
num_quiet=0
fla_result = 0
fla_funny = 0
device_Address_l = 0x69   # MPU6050 device address 1
device_Address_r = 0x68   # MPU6050 device address 2
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cpdef queue_MPU_values(mpu1_left, mpu2_right, event_start_get_mpu, qu_allow_start_mpu_loop, event_lifo_mpu_filled, lifo_mpus):
    #print(">>>>>>>>>>>>>>>> 1 queue_MPU_values")
    gyroxl, gyrozl, gyroxr, gyrozr = [],[],[],[]
    while True:
        if event_start_get_mpu.is_set():
            #print("111111111111111111111 entro nel while queue!!!")
            #addictive control...is necessary??? lets start with no choice
            gxl, gzl, gxr, gr = bcs.scan_MPUs(mpu1_left, mpu2_right)
            gyroxl.append(gxl)
            gyrozl.append(gzl)
            gyroxr.append(gxr)
            gyrozr.append(gr)
            #one_single_elem or many??
            #single because all should be taken together
            #print("gyroxl gyroxl gyroxl gyroxl gyroxl ", gyroxl)
            if len(gyroxl)==10:
                lifo_mpus.put([gyroxl,gyrozl,gyroxr,gyrozr])
                #print("maggiore!!!! maggiore!!!!")
                gyroxl, gyrozl, gyroxr, gyrozr = [],[],[],[]    
                event_lifo_mpu_filled.set()
            #if lifo_mpus.qsize() > 1:
            #if lifo_mpus.qsize()== lifo_mpus.qsize() 10:
            #    aaa = lifo_mpus.get()
            #    print("11111111111111111 ", aaa)



            #or??? many in several queue nodes
            #qu_mpu.put(gyroxl1)    
            #qu_mpu.put(gyrozl1)
            #qu_mpu.put(gyroxr1)
            #qu_mpu.put(gyrozr1)    
            #qu_mpu.put(gyroxl2)    
            #qu_mpu.put(gyrozl2)    
            #qu_mpu.put(gyroxr2)    
            #qu_mpu.put(gyrozr2)
    

cpdef extract_MPU_values_from_lifo(lifo_mpus):
    #it make sense take a step again to encapsulate and return values??
    #print(">>>>>>>>>>>>>>>> 2 enter in extract_MPU_values_from_lifo")
    list_mpus = lifo_mpus.get()
    print("list_mpus list_mpus list_mpuslist_mpus list_mpus ", list_mpus)
    #print("list_mpus list_mpus list_mpuslist_mpus list_mpus ", list_mpus)
    #print("list_mpus list_mpus list_mpuslist_mpus list_mpus ", list_mpus)
    #print("list_mpus list_mpus list_mpuslist_mpus list_mpus ", list_mpus)
    #print("type type list_mpus ", type(list_mpus))
    #gyroxl, gyrozl, gyroxr, gyrozr = 1,1,1,1
    gyroxl, gyrozl, gyroxr, gyrozr = list_mpus[0], list_mpus[1], list_mpus[2], list_mpus[3]
    return gyroxl, gyrozl, gyroxr, gyrozr

cpdef take_10_MPU_values(mpu1_left, mpu2_right, event_take_mpus_from_lifo, qu_allow_take_mpus, qu_last_mpu10, lifo_mpus):
    #print(">>>>>>>>>>>>>>>> 3 enter in take_10_MPU_values")
    print(type(mpu1_left))
    print(type(mpu2_right))
    cdef:
        Py_ssize_t i = 0
        Py_ssize_t leng = 10
    gyro_left_list, gyro_right_list= [],[] ##?? useful or not????
    while True:
        if event_take_mpus_from_lifo.is_set():
            #print(">>>>>>>>>>>>>>>> 3333333 while inside ok!!! ")
            #serve????
            #res = qu_allow_take_mpus.get()
            #if res == 1:
            for i in range(leng):
                #print(">>>>>>>>>>>>>>>> 3333333 3333333 3333333 3333333 ")
                gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(lifo_mpus)
                gyro_left_list.append(gyroxl)
                gyro_right_list.append(gyroxr)
                #print("3333333333333333333 gyroxl", gyroxl)
                #print("3333333333333333333 gyroxl", gyrozl)
                #print("3333333333333333333 gyroxl", gyroxr)
                #print("3333333333333333333 gyroxl", gyrozr)

                #queue also these???
                gyro_z_l = mpu1_left.get_gyroZ()
                gyro_z_r = mpu2_right.get_gyroZ()
            #print("3333333 gyro_left_list ", gyro_left_list)
            #print("3333333 gyro_right_list ", gyro_right_list)
            qu_last_mpu10.put([gyro_left_list,gyro_right_list,gyro_z_l,gyro_z_r])
            #qu_last_mpu10.put(gyro_right_list)
            #qu_last_mpu10.put(gyro_z_l)
            #qu_last_mpu10.put(gyro_z_r)

            gyro_left_list = []
            gyro_right_list = []


##################################################################################################################################
#ne metto uno solo direttamente in serial???
cpdef take_pressure_values(serNano2, event_ok_start_taking_press, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press):
    cdef bint first_after_stop = False
    cdef int i = 0
    print(">>>>>>>>>>>>>>>> 4 enter in take_pressures")
    while True:
        if event_ok_start_taking_press.is_set():
            print(">>>>>>>>>>>>>>>> 444 enter in the while ok take_pressures")
            event_continue_take_press.set()
            event_continue_take_press22222222.clear()
            #useless!! start automatically??? 
            serNano2.take_pressures_forever(event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press)
            first_after_stop = True
            
            #print("no maria io esco!!")
            #print("ciclo numero ciclo numero ciclo numero ciclo numero ", i)
            #print("no maria io esco!!")
            #print("ciclo numero ciclo numero ciclo numero ciclo numero ", i)
            
            #print("no maria io esco!!")
            i = i + 1
            #event_ok_start_taking_press.clear()
            #mpx0,mpx1,mpx2,mpx3,mpx4 = [],[],[],[],[] #????
            #qu_press.put([mpx0,mpx1,mpx2,mpx3,mpx4])
            #event_ok_press_ready.clear()
        else:
            if first_after_stop:
                print("no maria io sto facendo il mega danno!!")
                first_after_stop = False
                #print("no maria io sto facendo il mega danno!!")
                #print("no maria io sto facendo il mega danno!!")
                #print("no maria io sto facendo il mega danno!!")
                #print("no maria io sto facendo il mega danno!!")

cdef stop_taking_mpx(serNano2, event_continue_take_press, event_ok_start_taking_press, event_continue_take_press22222222,event_lifo_press_filled,event_lifo_mpu_filled):
    event_continue_take_press.clear()
    event_continue_take_press22222222.set()
    event_ok_start_taking_press.clear()
    event_lifo_press_filled.clear()
    event_lifo_mpu_filled.clear()

def empty_lifoqueue(lifo_press):
    """when is not reading...after hit or shove reaction"""
    if lifo_press.size()>100:
        while not lifo_press.empty():
            trash = lifo_press.get()

##################################################################################################################################
def multi_takescans_vecchio(lifo_press, qu_last_mpu10, event_continue_take_press, event_lifo_press_filled):
    print(">>>>>>>>>>>>>>>> 6 enter in multi_takescans")
    ''' start getting mpu and pressure outputs
    '''
    mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,gyro_z_l,gyro_z_r,pressures_q,mpus_q = [],[],[],[],[],[],[],[],[],[],[]

    if event_continue_take_press.is_set():
        if event_lifo_press_filled.is_set():
            #print(">>>>>>>>>>>>>>>> 666666666 666666666 666666666 666666666")
            #if qu_last_mpu10.empty():
            #    with open('output.txt', 'a') as f:
            #        print("lifo empty", file=f)
            #else:
            #    with open('output.txt', 'a') as f:
            #        print(">>>>>>>>>>>>>>>> 666666666", file=f)
            #print(">>>>>>>>>>>>>>>> 666666666 666666666 666666666 666666Z")
            pp = lifo_press.get()
            #print("pp pp pp pp pp pp pp pp =", pp)
            qq = qu_last_mpu10.get()
            #print("qq qq qq qq qq qq qq qq = ", qq)
            pressures_q.append(pp)
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][0])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][1])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][2])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][3])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][4])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][0])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][1])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][2])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][3])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][4])
            
            mpus_q.append(qq)
            #print("6666 66666 mpus_q mpus_q mpus_q mpus_q    =", mpus_q)
            #print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q)
            #print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q)
            #print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q[1])
            #print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q[2])
            #print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q[3])

            lenlen = len(pressures_q[0][0])-10 
            mpx00 = pressures_q[0][0]
            mpx11 = pressures_q[0][1]
            mpx22 = pressures_q[0][2]
            mpx33 = pressures_q[0][3]
            mpx44 = pressures_q[0][4]

            if lenlen!=0:
                popped_0 = mpx00[:lenlen]
                del mpx00[:lenlen]
                popped_1 = mpx11[:lenlen]
                del mpx11[:lenlen]
                popped_2 = mpx22[:lenlen]
                del mpx22[:lenlen]
                popped_3 = mpx33[:lenlen]
                del mpx33[:lenlen]
                popped_4 = mpx44[:lenlen]
                del mpx44[:lenlen]

            mpx0_t = list(map(float, mpx00))
            mpx1_t = list(map(float, mpx11))
            mpx2_t = list(map(float, mpx22))
            mpx3_t = list(map(float, mpx33))
            mpx4_t = list(map(float, mpx44))
            mpx0 = [round(x, 2) for x in mpx0_t]
            mpx1 = [round(x, 2) for x in mpx1_t]
            mpx2 = [round(x, 2) for x in mpx2_t]
            mpx3 = [round(x, 2) for x in mpx3_t]
            mpx4 = [round(x, 2) for x in mpx4_t]
            giro_l = mpus_q[0][0]
            giro_r = mpus_q[0][1]
            gyro_z_l = mpus_q[0][2]
            gyro_z_r = mpus_q[0][3]

            #print("sono in multi_takescans, sono in multi_takescans, mpx0 =", mpx0)
            #print("sono in multi_takescans, sono in multi_takescans, mpx1 =", mpx1)
            #print("sono in multi_takescans, sono in multi_takescans, mpx2 =", mpx2)
            #print("sono in multi_takescans, sono in multi_takescans, mpx3 =", mpx3)
            #print("sono in multi_takescans, sono in multi_takescans, mpx4 =", mpx4)
            #print("sono in multi_takescans, sono in multi_takescans, giro_l =", giro_l)
            #print("sono in multi_takescans, sono in multi_takescans, giro_r =", giro_r)
            #print("sono in multi_takescans, sono in multi_takescans, gyro_z_l =", gyro_z_l)
            #print("sono in multi_takescans, sono in multi_takescans, gyro_z_r =", gyro_z_r)
            
            #if not event_ok_mpu_ready.is_set() and not event_ok_press_ready.is_set():
            pressures_q = []
            mpus_q = []
            return mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,gyro_z_l,gyro_z_r

def multi_takescans(lifo_press, lifo_mpus, event_continue_take_press, event_lifo_press_filled, event_lifo_mpu_filled):
    #print(">>>>>>>>>>>>>>>> 6 enter in multi_takescans")
    ''' start getting mpu and pressure outputs
    '''
    mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,gyro_z_l,gyro_z_r,pressures_q,mpus_q = [],[],[],[],[],[],[],[],[],[],[]

    if event_continue_take_press.is_set():
        if event_lifo_press_filled.is_set() and event_lifo_mpu_filled.is_set():
            print(">>>>>>>>>>>>>>>> 666666666 666666666 666666666 666666666")
            if lifo_press.empty():
                with open('output.txt', 'a') as f:
                    print("lifo prpr empty", file=f)
            else:
                with open('output.txt', 'a') as f:
                    print(">>>>>>>>>>>>>>>> 666666666", file=f)
            if lifo_mpus.empty():
                with open('output2.txt', 'a') as f:
                    print("lifo mmm empty", file=f)
            else:
                with open('output2.txt', 'a') as f:
                    print(">>>>>>>>>>>>>>>> 666666666", file=f)                    
            #print(">>>>>>>>>>>>>>>> 666666666 666666666 666666666 666666Z")
            pp = lifo_press.get()
            print("pp pp pp pp pp pp pp pp =", pp)
            qq = lifo_mpus.get()
            print("qq qq qq qq qq qq qq qq = ", qq)
            pressures_q.append(pp)
            mpus_q.append(qq)
            event_lifo_press_filled.clear()
            event_lifo_mpu_filled.clear()
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][0])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][1])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][2])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][3])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][4])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][0])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][1])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][2])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][3])
            #print("pressures_q pressures_q pressures_q pressures_q  =", pressures_q[0][4])
            #print("6666 66666 mpus_q mpus_q mpus_q mpus_q    =", mpus_q)
            
            #print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q)
            #print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q[1])
            #print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q[2])
            #print("mpus_q mpus_q mpus_q mpus_q  =", mpus_q[3])

            '''
            lenlen_p = len(pressures_q[0][0])-10 
            mpx00 = pressures_q[0][0]
            mpx11 = pressures_q[0][1]
            mpx22 = pressures_q[0][2]
            mpx33 = pressures_q[0][3]
            mpx44 = pressures_q[0][4]

            if lenlen_p!=0:
                popped_0 = mpx00[:lenlen_p]
                del mpx00[:lenlen_p]
                popped_1 = mpx11[:lenlen_p]
                del mpx11[:lenlen_p]
                popped_2 = mpx22[:lenlen_p]
                del mpx22[:lenlen_p]
                popped_3 = mpx33[:lenlen_p]
                del mpx33[:lenlen_p]
                popped_4 = mpx44[:lenlen_p]
                del mpx44[:lenlen_p]

            mpx0_t = list(map(float, mpx00))
            mpx1_t = list(map(float, mpx11))
            mpx2_t = list(map(float, mpx22))
            mpx3_t = list(map(float, mpx33))
            mpx4_t = list(map(float, mpx44))
            mpx0 = [round(x, 2) for x in mpx0_t]
            mpx1 = [round(x, 2) for x in mpx1_t]
            mpx2 = [round(x, 2) for x in mpx2_t]
            mpx3 = [round(x, 2) for x in mpx3_t]
            mpx4 = [round(x, 2) for x in mpx4_t]
            
            lenlen_m = len(mpus_q[0][0])-10 
            giro_l = mpus_q[0][0]
            giro_r = mpus_q[0][1]
            gyro_z_l = mpus_q[0][2]
            gyro_z_r = mpus_q[0][3]
            
            if lenlen_m!=0:
                popped_0 = giro_l[:lenlen_m]
                del giro_l[:lenlen_m]
                popped_1 = giro_r[:lenlen_m]
                del giro_r[:lenlen_m]
                popped_2 = gyro_z_l[:lenlen_m]
                del gyro_z_l[:lenlen_m]
                popped_3 = gyro_z_r[:lenlen_m]
                del gyro_z_r[:lenlen_m]

            print("sono in multi_takescans, sono in multi_takescans, mpx0 =", mpx0)
            print("sono in multi_takescans, sono in multi_takescans, mpx1 =", mpx1)
            print("sono in multi_takescans, sono in multi_takescans, mpx2 =", mpx2)
            print("sono in multi_takescans, sono in multi_takescans, mpx3 =", mpx3)
            print("sono in multi_takescans, sono in multi_takescans, mpx4 =", mpx4)
            print("sono in multi_takescans, sono in multi_takescans, giro_l =", giro_l)
            print("sono in multi_takescans, sono in multi_takescans, giro_r =", giro_r)
            print("sono in multi_takescans, sono in multi_takescans, gyro_z_l =", gyro_z_l)
            print("sono in multi_takescans, sono in multi_takescans, gyro_z_r =", gyro_z_r)
            
            #if not event_ok_mpu_ready.is_set() and not event_ok_press_ready.is_set():
            pressures_q = []
            mpus_q = []
            return mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,gyro_z_l,gyro_z_r
            '''
##################################################################################################################################
def predict_contact(mpx0,mpx1,mpx2,mpx3,mpx4, giro_l,giro_r):
    print(">>>>>>>>>>>>>>>> 7 enter in predict_contact")
    #sample, flag_result, flag_fun = bcs.produce_input_sample(mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r)
    perceived_interaction = bcs.produce_input_sample_and_predict(mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r)
    print("perceived_interaction is  ", perceived_interaction)
    print("perceived_interaction is  ", perceived_interaction)
    print("perceived_interaction is  ", perceived_interaction)
    print("perceived_interaction is  ", perceived_interaction)
    print("perceived_interaction is  ", perceived_interaction)
    print("perceived_interaction is  ", perceived_interaction)
    print("perceived_interaction is  ", perceived_interaction)
    print("perceived_interaction is  ", perceived_interaction)
    print("perceived_interaction is  ", perceived_interaction)
    return perceived_interaction

def detect_touch(mpu1_left, mpu2_right, event_continue_take_press, event_take_touch, qu_start_prediction, qu_res_pred, 
        lifo_press, qu_last_mpu10, event_lifo_press_filled, event_continue_take_press22222222):
    print(">>>>>>>>>>>>>>>> 8 enter in detect_touch")
    #flag_funny = False
    #flag_result = False
    while True:
        if event_take_touch.is_set():
            if not event_continue_take_press22222222.is_set():
                print(">>>>>>>>>>>>>>>> 88888 while inside ok!!! ")
                #print('entrato in multitake')
                #num_on_queue = qu_start_prediction.get()
                #if num_on_queue == 1:
                result_prediction, flag_result, flag_fun=predict_contact(mpu1_left, mpu2_right, event_continue_take_press, event_lifo_press_filled, lifo_press, qu_last_mpu10)
                qu_res_pred.put([result_prediction, flag_result, flag_fun, giro_l, giro_r])
                #here??? or later?? TESTA!!!
                #event_take_touch.clear()

def extract_touch(mpu1_left, mpu2_right, event_continue_take_press, event_take_touch, qu_start_prediction, qu_res_pred, lifo_press, lifo_mpus, qu_last_mpu10, event_lifo_press_filled, event_lifo_mpu_filled, event_continue_take_press22222222):
    #print(">>>>>>>>>>>>>>>> bella bella bella bella enter in detect_touch")
    #flag_funny = False
    #flag_result = False
    while True:
        if event_take_touch.is_set():
            if not event_continue_take_press22222222.is_set():
                #print(">>>>>>>>>>>>>>>> bella bella bella bella while inside ok!!! ")
                #print('entrato in multitake')
                #num_on_queue = qu_start_prediction.get()
                #if num_on_queue == 1:
                #mpx0,mpx1,mpx2,mpx3,mpx4,giro_l,giro_r,gyro_z_l,gyro_z_r = multi_takescans(lifo_press, lifo_mpus, event_continue_take_press, event_lifo_press_filled, event_lifo_mpu_filled)
                multi_takescans(lifo_press, lifo_mpus, event_continue_take_press, event_lifo_press_filled, event_lifo_mpu_filled)
                '''
                print("==============================================================")
                print("sono in extract_touch, sono in multi_takescans, mpx0 =", mpx0)
                print("sono in extract_touch, sono in multi_takescans, mpx1 =", mpx1)
                print("sono in extract_touch, sono in multi_takescans, mpx2 =", mpx2)
                print("sono in extract_touch, sono in multi_takescans, mpx3 =", mpx3)
                print("sono in extract_touch, sono in multi_takescans, mpx4 =", mpx4)
                print("sono in extract_touch, sono in multi_takescans, giro_l =", giro_l)
                print("sono in extract_touch, sono in multi_takescans, giro_r =", giro_r)
                print("sono in extract_touch, sono in multi_takescans, gyro_z_l =", gyro_z_l)
                print("sono in extract_touch, sono in multi_takescans, gyro_z_r =", gyro_z_r)
                print("==============================================================")
                '''
                #other subprocess?????
                #with open('/home/pi/quiet_prova.csv', mode='a') as ser_output:
                #    ser_writer = csv.writer(ser_output, delimiter=';')
                #    for n in range(10):
                #        #if first_line or os.stat('/home/pi/quiet_prova.csv').st_size == 0:
                #        # ser_writer.writerow(['Data recieved: '])
                #        # ser_writer.writerow([array0]','[array1]','[array2]','[array3]','[array4])
                #        # ok for reading better csvRow = [array0[n], array1[n], array2[n], array3[n], array4[n], "-->", giro_l[n], '',giro_r[n]]
                #        csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], giro_l[n], giro_r[n]]
                #        ser_writer.writerow(csvRow)
                #        #first_line = False
                #        if n==9:
                #            csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], giro_l[n], giro_r[n]]
                #            ser_writer.writerow(csvRow)
                #            ser_writer.writerow([])


##################################################################################################################################
#controllo!! ma non ora!!!
#creao event_main e q_reaction...
##################################################################################################################################
def react_to_touch(result_touch, event_main, num_quiet, stopInterrupt, primary_lock, queue_lock, serNano2, coda, event_end_main):
    print(">>>>>>>>>>>>>>>> 10 enter in react_to_touch")
    command = serNano2.choose_command_touch(result_touch, num_quiet)
    comm_t=str(command,'utf-8')
    print("comm_t is")
    print(comm_t)
    #redo this if!!!!
    #if((comm_t != '<qu1>' and comm_t != '<qu2>') or (comm_t == '<qu1>' and num_quiet==100) or (comm_t == '<qu2>' and num_quiet==100)):
    #    #call stage !
    #    serr.transmit(command) #ok last! devo mettere byte!! sbagliato!!!
    #    #play audio ... 
    #    #play(command)
    #    #sistemo!!
    #    #time.sleep(3)
    
    #qu_reaction.put(1)
    #event_main.clear()
    start_stage_exhibition(coda, event_main)
    while True:
        if event_end_main.is_set():
            print("POSSO USCIREEEEEEEEEEEEEEEEEEEEEEE")
            break
##################################################################################################################################

# ===============================================================================================================
#  Main
# ===============================================================================================================
def contact_manager_main_too_old(stopInterrupt, primary_lock, queue_lock):
    print(">>>>>>>>>>>>>>>> 0 enter in contact_manager_main contact_manager_main contact_manager_main ")
    #instatiating mpus object
    
    mpu1_left = bimu.IMU_sensor(device_Address_l)
    mpu2_right = bimu.IMU_sensor(device_Address_r)
    print(type(mpu1_left))
    print(type(mpu2_right))
    inspect.isclass(mpu1_left)
    inspect.isclass(mpu2_right)
    
    #with bimu.IMU_sensor(device_Address_l) as mpu1_left:
    #    print("getting mpu value...")
    #    print(type(mpu1_left))
    #with bimu.IMU_sensor(device_Address_r) as mpu2_right:
    #    print("getting mpu value...")
    #    print(type(mpu2_right))
    serNano2 = serr.setup_serial_nano2()
    serNano2.await_arduino()
    #ret_val = serNano2.await_arduinospecial()
    #print("ret_val ret_val ret_val", ret_val)
    #print("deca ret_val deca ret_val", ret_val.decode('UTF-8', 'strict'))

    time.sleep(5)

    gyrox_r1 = -150
    gyrox_l1 = 50
    gyroz_l1 = 4
    gyroz_r1 = -35
    gyrox_r2 = -150
    gyrox_l2 = 50
    gyroz_l2 = 4
    gyroz_r2 = -35
    #self ??? evento e code ancora???
    #gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(mpu1_left, mpu2_right)
    #gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(mpu1_left, mpu2_right)

    man = OimiManager()
    man.start()
    lifo_mpus = man.LifoQueue()
    OimiManager.register('lifo_press', LifoQueue)
    
    man1 = OimiManager()
    man1.start()
    OimiManager.register('lifo_mpus', LifoQueue)
    lifo_press = man1.LifoQueue()
    
    #It is ok to put size in OimiQueue??? no!! just name
    qu_start_prediction = OimiQueue("fifo qu_start_prediction")
    qu_res_pred = OimiQueue("fifo qu_res_pred")
    qu_last_mpu10 = OimiQueue("fifo qu_last_mpu10")
    qu_take_mpus = OimiQueue("fifo qu_take_mpus") 
    qu_allow_start_mpu_loop = OimiQueue("fifo qu_allow_start_mpu_loop") 
    qu_allow_take_mpus = OimiQueue("fifo qu_allow_take_mpus") 
    event_ok_start_taking_press = Event()
    event_start_get_mpu = Event()
    event_take_mpus_from_lifo = Event()
    event_ok_single_ready = Event()
    event_take_touch = Event()
    event_continue_take_press = Event()
    event_continue_take_press22222222 = Event()

    event_cancel = Event()
    event_lifo_press_filled = Event()

    ###################### to fix ###################### to fix ###################### to fix ###################### to fix 
    job1_begin_to_scan_pressures_continuosly = multiprocessing.Process(name='job1_begin_to_scan_pressures_continuosly', target=take_pressure_values, args=(serNano2, event_ok_start_taking_press, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press,))
    
    job2_begin_to_scan_mpu_continuosly = multiprocessing.Process(name='job2_begin_to_scan_mpu_continuosly', target=queue_MPU_values, args=(mpu1_left, mpu2_right, event_start_get_mpu, qu_allow_start_mpu_loop, lifo_mpus,))
    
    #poi canecello job8_take_press
    #job8_take_press = multiprocessing.Process(name='job8_take_press', target=cancelme, args=(event_cancel, lifo_press, event_continue_take_press22222222,))
    job22_take_mpus = multiprocessing.Process(name='job22_take_mpus', target=take_10_MPU_values, args=(mpu1_left, mpu2_right, event_take_mpus_from_lifo, qu_allow_take_mpus, qu_last_mpu10, lifo_mpus,))
    job3_detect_touch = multiprocessing.Process(name='job3_detect_touch', target=detect_touch, args=(mpu1_left, mpu2_right, event_continue_take_press, event_take_touch, qu_start_prediction, qu_res_pred, lifo_press, qu_last_mpu10, event_lifo_press_filled, event_continue_take_press22222222,))
    
    job1_begin_to_scan_pressures_continuosly.start()
    job2_begin_to_scan_mpu_continuosly.start()
    #job8_take_press.start()
    job22_take_mpus.start()
    job3_detect_touch.start()
    print("inizio seconda parte")
    event_ok_start_taking_press.set()
    event_start_get_mpu.set()
    qu_allow_start_mpu_loop.put(1)
    time.sleep(2)
    event_take_mpus_from_lifo.set()
    qu_allow_take_mpus.put(1)
    event_take_touch.set()
    qu_start_prediction.put(1)
    #aaa = qu_res_pred.get()
    print("inizio terza parte")
    time.sleep(10)
    #event_cancel.set()
    #time.sleep(10)
    ##àstop_taking_mpx(serNano2, event_continue_take_press, event_ok_start_taking_press, event_continue_take_press22222222) to avoid setup rompe
    #time.sleep(3)

    job1_begin_to_scan_pressures_continuosly.join()
    job2_begin_to_scan_mpu_continuosly.join()
    job22_take_mpus.join()
    job3_detect_touch.join()

    print("esco!!! ho finito!!!")

    job1_begin_to_scan_pressures_continuosly.close()
    job2_begin_to_scan_mpu_continuosly.close()
    job22_take_mpus.close()
    job3_detect_touch.close()
    print("esco!!! ho finito!!!")


    '''
    job1_begin_to_scan_pressures_continuosly.join()
    job2_begin_to_scan_mpu_continuosly.join()
    
        

    
    



    #job4 = multiprocessing.Process(name='capat', target=capathread, args=(qu5,qu6,event3,))
    #job4.start()
    '''



    ## create a list proxy and append a mutable object (a dictionary)
    #lproxy = manager.list()
    #lproxy.append({})
    ## now mutate the dictionary
    #d = lproxy[0]
    #d['a'] = 1
    #d['b'] = 2
    ## at this point, the changes to d are not yet synced, but by
    ## reassigning the dictionary, the proxy is notified of the change
    #lproxy[0] = d

def contact_manager_for_data_acquisition_vecchio_con_coda_lento():
    print(">>>>>>>>>>>>>>>> 0 enter in contact_manager_main contact_manager_main contact_manager_main ")
    #instatiating mpus object
    
    mpu1_left = bimu.IMU_sensor(device_Address_l)
    mpu2_right = bimu.IMU_sensor(device_Address_r)
    print(type(mpu1_left))
    print(type(mpu2_right))
    inspect.isclass(mpu1_left)
    inspect.isclass(mpu2_right)
    
    #with bimu.IMU_sensor(device_Address_l) as mpu1_left:
    #    print("getting mpu value...")
    #    print(type(mpu1_left))
    #with bimu.IMU_sensor(device_Address_r) as mpu2_right:
    #    print("getting mpu value...")
    #    print(type(mpu2_right))
    serNano2 = serr.setup_serial_nano2()
    serNano2.await_arduino()
    #ret_val = serNano2.await_arduinospecial()
    #print("ret_val ret_val ret_val", ret_val)
    #print("deca ret_val deca ret_val", ret_val.decode('UTF-8', 'strict'))

    time.sleep(5)

    gyrox_r1 = -150
    gyrox_l1 = 50
    gyroz_l1 = 4
    gyroz_r1 = -35
    gyrox_r2 = -150
    gyrox_l2 = 50
    gyroz_l2 = 4
    gyroz_r2 = -35
    #self ??? evento e code ancora???
    #gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(mpu1_left, mpu2_right)
    #gyroxl, gyrozl, gyroxr, gyrozr = extract_MPU_values_from_lifo(mpu1_left, mpu2_right)

    man = OimiManager()
    man.start()
    lifo_mpus = man.LifoQueue()
    OimiManager.register('lifo_press', LifoQueue)
    
    man1 = OimiManager()
    man1.start()
    OimiManager.register('lifo_mpus', LifoQueue)
    lifo_press = man1.LifoQueue()

    man2 = OimiManager()
    man2.start()
    OimiManager.register('qu_last_mpu10', LifoQueue)
    qu_last_mpu10 = man1.LifoQueue()

    
    #It is ok to put size in OimiQueue??? no!! just name
    qu_start_prediction = OimiQueue("fifo qu_start_prediction")
    qu_res_pred = OimiQueue("fifo qu_res_pred")
    qu_take_mpus = OimiQueue("fifo qu_take_mpus") 
    qu_allow_start_mpu_loop = OimiQueue("fifo qu_allow_start_mpu_loop") 
    qu_allow_take_mpus = OimiQueue("fifo qu_allow_take_mpus") 
    event_ok_start_taking_press = Event()
    event_start_get_mpu = Event()
    event_take_mpus_from_lifo = Event()
    event_ok_single_ready = Event()
    event_take_touch = Event()
    event_continue_take_press = Event()
    event_continue_take_press22222222 = Event()
    event_cancel = Event()
    event_lifo_press_filled = Event()
    event_lifo_mpu_filled = Event()

    ###################### to fix ###################### to fix ###################### to fix ###################### to fix 
    job1_begin_to_scan_pressures_continuosly = multiprocessing.Process(name='job1_begin_to_scan_pressures_continuosly', target=take_pressure_values, args=(serNano2, event_ok_start_taking_press, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press,))
    
    job2_begin_to_scan_mpu_continuosly = multiprocessing.Process(name='job2_begin_to_scan_mpu_continuosly', target=queue_MPU_values, args=(mpu1_left, mpu2_right, event_start_get_mpu, qu_allow_start_mpu_loop, event_lifo_mpu_filled, lifo_mpus,))
    
    #poi canecello job8_take_press
    #job8_take_press = multiprocessing.Process(name='job8_take_press', target=cancelme, args=(event_cancel, lifo_press, event_continue_take_press22222222,))
    #job22_take_mpus = multiprocessing.Process(name='job22_take_mpus', target=take_10_MPU_values, args=(mpu1_left, mpu2_right, event_take_mpus_from_lifo, qu_allow_take_mpus, qu_last_mpu10, lifo_mpus,))
    job3_extract_touch = multiprocessing.Process(name='job3_extract_touch', target=extract_touch, args=(mpu1_left, mpu2_right, event_continue_take_press, event_take_touch, qu_start_prediction, qu_res_pred, lifo_press, lifo_mpus, qu_last_mpu10, event_lifo_press_filled, event_lifo_mpu_filled, event_continue_take_press22222222,))
    
    job1_begin_to_scan_pressures_continuosly.start()
    job2_begin_to_scan_mpu_continuosly.start()

    event_ok_start_taking_press.set()
    event_start_get_mpu.set()
    qu_allow_start_mpu_loop.put(1)


    #job22_take_mpus.start()
    job3_extract_touch.start()
    print("inizio seconda parte")
    event_ok_start_taking_press.set()
    event_start_get_mpu.set()
    qu_allow_start_mpu_loop.put(1)
    
    time.sleep(2)
    event_take_mpus_from_lifo.set()
    qu_allow_take_mpus.put(1)
    event_take_touch.set()
    qu_start_prediction.put(1)
    #aaa = qu_res_pred.get()
    print("inizio terza parte")
    time.sleep(16000)
    #event_cancel.set()
    #time.sleep(10)
    stop_taking_mpx(serNano2, event_continue_take_press, event_ok_start_taking_press, event_continue_take_press22222222,event_lifo_press_filled,event_lifo_mpu_filled)
    #time.sleep(3)

    job1_begin_to_scan_pressures_continuosly.join()
    job2_begin_to_scan_mpu_continuosly.join()
    #job22_take_mpus.join()
    job3_extract_touch.join()

    print("esco!!! ho finito!!!")

    job1_begin_to_scan_pressures_continuosly.close()
    job2_begin_to_scan_mpu_continuosly.close()
    #job22_take_mpus.close()
    job3_extract_touch.close()
    print("esco!!! ho finito!!!")

def prendi_press(serNano2):
    print("___aaaa____prendi_press___aaaa____prendi_press___aaaa____prendi_press___aaaa____prendi_press")
    #mpx0,mpx1,mpx2,mpx3,mpx4 = [],[],[],[]
    mpx0,mpx1,mpx2,mpx3,mpx4 = serNano2.take_10_pressures()
    return mpx0,mpx1,mpx2,mpx3,mpx4

def prendi_mpu(mpu1_left, mpu2_right):
    print("___bbbb____prendi_mpu___bbbb____prendi_mpu___bbbb____prendi_mpu___bbbb____prendi_mpu___bbbb____prendi_mpu")
    cdef Py_ssize_t i = 0
    cdef Py_ssize_t leng = 10
    gyroxl, gyrozl, gyroxr, gyrozr = [],[],[],[]
    for i in range(leng):
        gxl, gzl, gxr, gr = bcs.scan_MPUs(mpu1_left, mpu2_right)
        gyroxl.append(gxl)
        gyrozl.append(gzl)
        gyroxr.append(gxr)
        gyrozr.append(gr)
        time.sleep(0.4)
    return gyroxl, gyrozl, gyroxr, gyrozr

def contact_manager_for_data_acquisition_sequential():
    mpu1_left = bimu.IMU_sensor(device_Address_l)
    mpu2_right = bimu.IMU_sensor(device_Address_r)
    print(type(mpu1_left))
    print(type(mpu2_right))
    inspect.isclass(mpu1_left)
    inspect.isclass(mpu2_right)
    serNano2 = serr.setup_serial_nano2()
    serNano2.await_arduino()
    time.sleep(1)
    print(" START!!!!! START!!!!!")
    ##########ok 
    #NB cannot pass events to pool functions!!!! Condition objects should only be shared between processes through inheritance!!!!
    #pool.apply(prendi_press, [serNano2])
    '''
    i = 0
    while i<=5:
        pool = Pool(processes=2)
        mpx0, mpx1, mpx2, mpx3, mpx4 = pool.apply_async(prendi_press, [serNano2]).get()
        #ok!!!
        gyroxl, gyrozl, gyroxr, gyrozr = pool.apply_async(prendi_mpu, [mpu1_left, mpu2_right]).get()
        #pool.apply(prendi_mpu, [mpu1_left, mpu2_right])
        print("mpx0, mpx1, mpx2, mpx3, mpx4 ====> {} {} {} {} {} ".format(mpx0,mpx1,mpx2,mpx3,mpx4))
        print("gyroxl, gyrozl, gyroxr, gyrozr ====> {} {} {} {} ".format(gyroxl, gyrozl, gyroxr, gyrozr))
        pool.close()
        pool.join()
        i+=1
        with open('/home/pi/quiet_prova.csv', mode='a') as ser_output:
            ser_writer = csv.writer(ser_output, delimiter=';')
            for n in range(10):
                #if first_line or os.stat('/home/pi/quiet_prova.csv').st_size == 0:
                # ser_writer.writerow(['Data recieved: '])
                # ser_writer.writerow([array0]','[array1]','[array2]','[array3]','[array4])
                # ok for reading better csvRow = [array0[n], array1[n], array2[n], array3[n], array4[n], "-->", giro_l[n], '',giro_r[n]]
                csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                ser_writer.writerow(csvRow)
                #first_line = False
                if n==9:
                    csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                    ser_writer.writerow(csvRow)
                    ser_writer.writerow([])
        '''
    
    #pool.close()
    #result = calcprediction(mpx0,mpx1,mpx2,mpx3,mpx4,gyroxl,gyroxr)
    i = 0
    while i <= 5:
        pool = Pool(processes=2)
        mpx0, mpx1, mpx2, mpx3, mpx4 = pool.apply_async(prendi_press, [serNano2]).get()
        #ok!!!
        gyroxl, gyrozl, gyroxr, gyrozr = pool.apply_async(prendi_mpu, [mpu1_left, mpu2_right]).get()
        #pool.apply(prendi_mpu, [mpu1_left, mpu2_right])
        print("mpx0, mpx1, mpx2, mpx3, mpx4 ====> {} {} {} {} {} ".format(mpx0,mpx1,mpx2,mpx3,mpx4))
        print("gyroxl, gyrozl, gyroxr, gyrozr ====> {} {} {} {} ".format(gyroxl, gyrozl, gyroxr, gyrozr))

        print("mpx0 0 =", mpx0[0])
        print("mpx1 0 =", mpx1[0])
        print("mpx2 0 =", mpx2[0])
        print("mpx3 0 =", mpx3[0])
        print("mpx4 0 =", mpx4[0])
        print("gyroxl 0 =", gyroxl[0])
        print("gyroxr 0 =", gyroxr[0])

        with open('/home/pi/quiet_prova.csv', mode='a') as ser_output:
            ser_writer = csv.writer(ser_output, delimiter=';')
            for n in range(10):
                #if first_line or os.stat('/home/pi/quiet_prova.csv').st_size == 0:
                # ser_writer.writerow(['Data recieved: '])
                # ser_writer.writerow([array0]','[array1]','[array2]','[array3]','[array4])
                # ok for reading better csvRow = [array0[n], array1[n], array2[n], array3[n], array4[n], "-->", giro_l[n], '',giro_r[n]]
                csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                ser_writer.writerow(csvRow)
                #first_line = False
                if n==9:
                    csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                    ser_writer.writerow(csvRow)
                    ser_writer.writerow([])

        pool.close()
        pool.join()
        print("HO FINITO!!!!")


def contact_manager_for_data_acquisition_con_pool():
    mpu1_left = bimu.IMU_sensor(device_Address_l)
    mpu2_right = bimu.IMU_sensor(device_Address_r)
    print(type(mpu1_left))
    print(type(mpu2_right))
    inspect.isclass(mpu1_left)
    inspect.isclass(mpu2_right)
    serNano2 = serr.setup_serial_nano2()
    serNano2.await_arduino()
    time.sleep(1)
    print(" START!!!!! START!!!!!")
    ##########ok 
    #NB cannot pass events to pool functions!!!! Condition objects should only be shared between processes through inheritance!!!!
    #pool.apply(prendi_press, [serNano2])
    '''
    i = 0
    while i<=5:
        pool = Pool(processes=2)
        mpx0, mpx1, mpx2, mpx3, mpx4 = pool.apply_async(prendi_press, [serNano2]).get()
        #ok!!!
        gyroxl, gyrozl, gyroxr, gyrozr = pool.apply_async(prendi_mpu, [mpu1_left, mpu2_right]).get()
        #pool.apply(prendi_mpu, [mpu1_left, mpu2_right])
        print("mpx0, mpx1, mpx2, mpx3, mpx4 ====> {} {} {} {} {} ".format(mpx0,mpx1,mpx2,mpx3,mpx4))
        print("gyroxl, gyrozl, gyroxr, gyrozr ====> {} {} {} {} ".format(gyroxl, gyrozl, gyroxr, gyrozr))
        pool.close()
        pool.join()
        i+=1
        with open('/home/pi/quiet_prova.csv', mode='a') as ser_output:
            ser_writer = csv.writer(ser_output, delimiter=';')
            for n in range(10):
                #if first_line or os.stat('/home/pi/quiet_prova.csv').st_size == 0:
                # ser_writer.writerow(['Data recieved: '])
                # ser_writer.writerow([array0]','[array1]','[array2]','[array3]','[array4])
                # ok for reading better csvRow = [array0[n], array1[n], array2[n], array3[n], array4[n], "-->", giro_l[n], '',giro_r[n]]
                csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                ser_writer.writerow(csvRow)
                #first_line = False
                if n==9:
                    csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                    ser_writer.writerow(csvRow)
                    ser_writer.writerow([])
        '''
    
    #pool.close()
    #result = calcprediction(mpx0,mpx1,mpx2,mpx3,mpx4,gyroxl,gyroxr)
    i = 0

    while i <= 5:
        #async return a result...while apply not!
        pool = Pool(processes=2)
        mpx0, mpx1, mpx2, mpx3, mpx4 = pool.apply_async(prendi_press, [serNano2]).get() #get() wait for the function to finish
        #ok!!!
        gyroxl, gyrozl, gyroxr, gyrozr = pool.apply_async(prendi_mpu, [mpu1_left, mpu2_right]).get() 
        #pool.apply(prendi_mpu, [mpu1_left, mpu2_right])
        print("mpx0, mpx1, mpx2, mpx3, mpx4 ====> {} {} {} {} {} ".format(mpx0,mpx1,mpx2,mpx3,mpx4))
        print("gyroxl, gyrozl, gyroxr, gyrozr ====> {} {} {} {} ".format(gyroxl, gyrozl, gyroxr, gyrozr))

        print("mpx0 0 =", mpx0[0])
        print("mpx1 0 =", mpx1[0])
        print("mpx2 0 =", mpx2[0])
        print("mpx3 0 =", mpx3[0])
        print("mpx4 0 =", mpx4[0])
        print("gyroxl 0 =", gyroxl[0])
        print("gyroxr 0 =", gyroxr[0])

        with open('/home/pi/quiet_prova.csv', mode='a') as ser_output:
            ser_writer = csv.writer(ser_output, delimiter=';')
            for n in range(10):
                #if first_line or os.stat('/home/pi/quiet_prova.csv').st_size == 0:
                # ser_writer.writerow(['Data recieved: '])
                # ser_writer.writerow([array0]','[array1]','[array2]','[array3]','[array4])
                # ok for reading better csvRow = [array0[n], array1[n], array2[n], array3[n], array4[n], "-->", giro_l[n], '',giro_r[n]]
                csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                ser_writer.writerow(csvRow)
                #first_line = False
                if n==9:
                    csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                    ser_writer.writerow(csvRow)
                    ser_writer.writerow([])
        i+=1
        pool.close() # Prevents any more tasks from being submitted to the pool. Once all the tasks have been completed the worker processes will exit.
        pool.join() #Wait for the worker processes to exit. close() or terminate() must be called before using join().
        print("HO FINITO!!!!")


async def prendi_press2(serNano2):
    print("___aaaa__async____prendi_press___aaaa__async____prendi_press___aaaa__async____prendi_press___aaaa__async____prendi_press")
    #mpx0,mpx1,mpx2,mpx3,mpx4 = [],[],[],[]
    mpx0,mpx1,mpx2,mpx3,mpx4 = serNano2.take_10_pressures()
    return mpx0,mpx1,mpx2,mpx3,mpx4

async def prendi_mpu2(mpu1_left, mpu2_right):
    print("___bbbb_async____prendi_mpu___bbbb__async____prendi_mpu___bbbb____prendi_mpu___bbbb__async____prendi_mpu___bbbb__async____prendi_mpu")
    cdef Py_ssize_t i = 0
    cdef Py_ssize_t leng = 10
    gyroxl, gyrozl, gyroxr, gyrozr = [],[],[],[]
    for i in range(leng):
        gxl, gzl, gxr, gr = bcs.scan_MPUs(mpu1_left, mpu2_right)
        gyroxl.append(gxl)
        gyrozl.append(gzl)
        gyroxr.append(gxr)
        gyrozr.append(gr)
        time.sleep(0.4)
    return gyroxl, gyrozl, gyroxr, gyrozr


async def contact_manager_for_data_acquisition_asyncio():
    mpu1_left = bimu.IMU_sensor(device_Address_l)
    mpu2_right = bimu.IMU_sensor(device_Address_r)
    print(type(mpu1_left))
    print(type(mpu2_right))
    inspect.isclass(mpu1_left)
    inspect.isclass(mpu2_right)
    serNano2 = serr.setup_serial_nano2()
    serNano2.await_arduino()
    time.sleep(1)
    print(" START!!!!! START!!!!!")
    ##########ok 
    #NB cannot pass events to pool functions!!!! Condition objects should only be shared between processes through inheritance!!!!
    #pool.apply(prendi_press, [serNano2])
    i = 0

    #while i <= 5:
    #async return a result...while apply not!
    while i <= 5:
        task1 = asyncio.create_task(
            prendi_press2(serNano2))

        task2 = asyncio.create_task(
            prendi_mpu2(mpu1_left, mpu2_right))

        print(f"started at {time.strftime('%X')}")

        # Wait until both tasks are completed (should take
        # around 2 seconds.)
        mpx0, mpx1, mpx2, mpx3, mpx4 = await task1
        gyroxl, gyrozl, gyroxr, gyrozr = await task2

        print(f"finished at {time.strftime('%X')}")

        #pool.apply(prendi_mpu, [mpu1_left, mpu2_right])
        print("mpx0, mpx1, mpx2, mpx3, mpx4 ====> {} {} {} {} {} ".format(mpx0,mpx1,mpx2,mpx3,mpx4))
        print("gyroxl, gyrozl, gyroxr, gyrozr ====> {} {} {} {} ".format(gyroxl, gyrozl, gyroxr, gyrozr))

        print("mpx0 0 =", mpx0[0])
        print("mpx1 0 =", mpx1[0])
        print("mpx2 0 =", mpx2[0])
        print("mpx3 0 =", mpx3[0])
        print("mpx4 0 =", mpx4[0])
        print("gyroxl 0 =", gyroxl[0])
        print("gyroxr 0 =", gyroxr[0])

        with open('/home/pi/quiet_prova.csv', mode='a') as ser_output:
            ser_writer = csv.writer(ser_output, delimiter=';')
            for n in range(10):
                #if first_line or os.stat('/home/pi/quiet_prova.csv').st_size == 0:
                # ser_writer.writerow(['Data recieved: '])
                # ser_writer.writerow([array0]','[array1]','[array2]','[array3]','[array4])
                # ok for reading better csvRow = [array0[n], array1[n], array2[n], array3[n], array4[n], "-->", giro_l[n], '',giro_r[n]]
                csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                ser_writer.writerow(csvRow)
                #first_line = False
                if n==9:
                    csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                    ser_writer.writerow(csvRow)
                    ser_writer.writerow([])
        i+=1
    print("HO FINITO async async async !!!!")

"""ray!!!!! cannot install wheel!!!! problem!!!!
@ray.remote(num_returns=2)
def prendi_press_ray(serNano2):
    print("___aaaa__ray__prendi_press___aaaa____prendi_press___aaaa____prendi_press___aaaa____prendi_press")
    #mpx0,mpx1,mpx2,mpx3,mpx4 = [],[],[],[]
    mpx0,mpx1,mpx2,mpx3,mpx4 = serNano2.take_10_pressures()
    return mpx0,mpx1,mpx2,mpx3,mpx4
@ray.remote(num_returns=2)
def prendi_mpu_ray(mpu1_left, mpu2_right):
    print("___bbbb__ray__prendi_mpu___bbbb____prendi_mpu___bbbb____prendi_mpu___bbbb____prendi_mpu___bbbb____prendi_mpu")
    cdef Py_ssize_t i = 0
    cdef Py_ssize_t leng = 10
    gyroxl, gyrozl, gyroxr, gyrozr = [],[],[],[]
    for i in range(leng):
        gxl, gzl, gxr, gr = bcs.scan_MPUs(mpu1_left, mpu2_right)
        gyroxl.append(gxl)
        gyrozl.append(gzl)
        gyroxr.append(gxr)
        gyrozr.append(gr)
        time.sleep(0.4)
    return gyroxl, gyrozl, gyroxr, gyrozr

################################################################################################################################################
def contact_manager_for_data_acquisition_ray_need_wheel_to_be_installed():
    mpu1_left = bimu.IMU_sensor(device_Address_l)
    mpu2_right = bimu.IMU_sensor(device_Address_r)
    print(type(mpu1_left))
    print(type(mpu2_right))
    inspect.isclass(mpu1_left)
    inspect.isclass(mpu2_right)
    serNano2 = serr.setup_serial_nano2()
    serNano2.await_arduino()
    time.sleep(1)
    print(" START!!!!! START!!!!!")

    i = 0

    #while i <= 5:
    ret_func1 = prendi_press_ray.remote(serNano2)
    ret_func2 = prendi_mpu_ray.remote(mpu1_left, mpu2_right)
    ret1, ret2 = ray.get([ret_func1, ret_func2])

    print("ret_func1 ============= ", ret_func1)
    print("ret_func2 ============= ", ret_func2)
    print("ret1 ============= ", ret1)
    print("ret2 ============= ", ret2)
    '''
    print("mpx0, mpx1, mpx2, mpx3, mpx4 ====> {} {} {} {} {} ".format(mpx0,mpx1,mpx2,mpx3,mpx4))
    print("gyroxl, gyrozl, gyroxr, gyrozr ====> {} {} {} {} ".format(gyroxl, gyrozl, gyroxr, gyrozr))
    print("mpx0 0 =", mpx0[0])
    print("mpx1 0 =", mpx1[0])
    print("mpx2 0 =", mpx2[0])
    print("mpx3 0 =", mpx3[0])
    print("mpx4 0 =", mpx4[0])
    print("gyroxl 0 =", gyroxl[0])
    print("gyroxr 0 =", gyroxr[0])

    with open('/home/pi/quiet_prova.csv', mode='a') as ser_output:
        ser_writer = csv.writer(ser_output, delimiter=';')
        for n in range(10):
            #if first_line or os.stat('/home/pi/quiet_prova.csv').st_size == 0:
            # ser_writer.writerow(['Data recieved: '])
            # ser_writer.writerow([array0]','[array1]','[array2]','[array3]','[array4])
            # ok for reading better csvRow = [array0[n], array1[n], array2[n], array3[n], array4[n], "-->", giro_l[n], '',giro_r[n]]
            csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
            ser_writer.writerow(csvRow)
            #first_line = False
            if n==9:
                csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                ser_writer.writerow(csvRow)
                ser_writer.writerow([])
    i+=1
    '''
    print("HO FINITO!!!!")
"""
############################################################################################################################################
def contact_manager_for_data_acquisition(rightcsv):
    mpu1_left = bimu.IMU_sensor(device_Address_l)
    mpu2_right = bimu.IMU_sensor(device_Address_r)
    print(type(mpu1_left))
    print(type(mpu2_right))
    inspect.isclass(mpu1_left)
    inspect.isclass(mpu2_right)
    serNano2 = serr.setup_serial_nano2()
    serNano2.await_arduino()
    time.sleep(1)
    print(" START!!!!! START!!!!!")

    i = 0
    while i<=500:
        mpx0,mpx1,mpx2,mpx3,mpx4,gyroxl,gyrozl,gyroxr,gyrozr = serNano2.take_10_pressures_and_mpus(mpu1_left, mpu2_right)
        #print("mpx0, mpx1, mpx2, mpx3, mpx4 ====> {} {} {} {} {} ".format(mpx0,mpx1,mpx2,mpx3,mpx4))
        #print("gyroxl, gyrozl, gyroxr, gyrozr ====> {} {} {} {} ".format(gyroxl, gyrozl, gyroxr, gyrozr))
        
        #with open('/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/data_acquisition/sensor_outputs_to_learn/shove.csv', mode='a') as ser_output:
        with open(rightcsv, mode='a') as ser_output:
            ser_writer = csv.writer(ser_output, delimiter=';')
            for n in range(10):
                csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                ser_writer.writerow(csvRow)
                #first_line = False
                if n==9:
                    csvRow = [mpx0[n], mpx1[n], mpx2[n], mpx3[n], mpx4[n], gyroxl[n], gyroxr[n]]
                    ser_writer.writerow(csvRow)
                    ser_writer.writerow([])
        i+=1
    print("HO FINITO!!!!")

def function_for_getting_data_for_predicting(serNano2, mpu1_left, mpu2_right):
    mpx0,mpx1,mpx2,mpx3,mpx4,gyroxl,gyrozl,gyroxr,gyrozr = serNano2.take_10_pressures_and_mpus(mpu1_left, mpu2_right)
    #print("mpx0, mpx1, mpx2, mpx3, mpx4 ====> {} {} {} {} {} ".format(mpx0,mpx1,mpx2,mpx3,mpx4))
    #print("gyroxl, gyrozl, gyroxr, gyrozr ====> {} {} {} {} ".format(gyroxl, gyrozl, gyroxr, gyrozr))
    return mpx0,mpx1,mpx2,mpx3,mpx4,gyroxl,gyrozl,gyroxr,gyrozr

def stage_start_animation(global_status,coda,event_led,event_move,led_command,led_command_base,event_led_base,event_start_play_audio,right_audio_to_play, event_main, event_end_main):
    while True:
        if not event_main.is_set():
            print("COMINCIA ADESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSOOOOOOOOOOOOOOOOOOOOOOOOOOO")
            coda_el = coda.get()
            if coda_el==1:
                print("COMINCIA dopoooooooooooooooooooooooooooooooooo")
                event_move.clear()
                led_command.put(9)
                led_command_base.put(3)
                event_led.clear()
                #stt = b'bella'
                event_led_base.clear()
                stt1 = "no"
                stt = sp.sounds_dict.get(stt1,"This sound doesn't exists!")
                right_audio_to_play.put(stt)
                event_start_play_audio.set()
                print("ok ho impostato tutto in stage_start_animation")
                event_main.set()
                #break
        #else:
        #    print("event_main_settato!!!")

def goLED_body(ser, event_led_body, led_command):
    print("!!enter in goled_body!!")
    #while True:
        #try:
    if not event_led_body.is_set():
        print("event body not set, quindi posso entrare")
        #ser = led_command.get()
        #print("ser nella coda = ", ser)
        conferma = led_command.get()
        print("led numero nella coda = ", conferma)
        ser.sendled(conferma)
        time.sleep(3.5)
        ser.sendled(8)
        #ser.sendreset() #to remove ?=!?!!?!?? to take single press from nano2
        aa = 'aa'
        return aa

def activateLED(global_status, ser, event_startledanimation, event_led_body, led_command):
    print("enter in led animation body, waiting")
    while True:
        event_startledanimation.wait() #with set wait method works ; with not set better use if but not in this case
        print("!!event_startledanimation!!")
        aa = goLED_body(ser, event_led_body, led_command)
        if aa=='aa':
            print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa body")
            event_led_body.set() 
            event_startledanimation.clear()
            #break
            

def dispatcher_led(global_status, ser, event_startledanimation, event_led_body, event_led):
    print("dispatcher_led is waiting ...........................")
    if not event_startledanimation.is_set():
        print("CIAOOOOOOOOOOOOOOOOOOOOO")
    while True:
        if not event_led.is_set():
            print("dispatcher_led body in body!!!!!!!!!!!!!!!!!!!!!!!!")
            #time.sleep(5)
            #print("tempo_passato!!!")
            #led_command.put(1)
            event_startledanimation.set()
            event_led_body.clear()
            #led_command.put(ser)
            #controller(ser1,ser2,ser3)
            event_led.set() #lo stesso di adesso senno va avanti all'inifinito
            
            #occhio che qui fa casino anche adesso mi sa se aggiungo movimento!!!!!
            #per adesso commento...
            #while not led_command.empty():
            #    led_command.get()
            #print("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOK SVUOTATOOOOOOOOOOOOOOOO")
            

            #time.sleep(20)
            #led_command.put(2)
            #event_startledanimation.set()
            #break

def goLED_base(ser, event_led_basebase, led_command_base):
    print("!!enter in _base!!")
    #while True:
        #try:
    if not event_led_basebase.is_set():
        print("event _base not set,_base quindi posso entrare")
        #ser = led_command.get()
        #print("ser nella coda = ", ser)
        conferma = led_command_base.get()
        print("led numero nella coda della basedella basedella basedella base della basedella base della base = ", conferma)
        ser.sendled(conferma)
        time.sleep(3.5)
        ser.sendled(8)
        #ser.sendreset()
        aa = 'aa'
        return aa

def activateLED_base(global_status, ser, event_startledanimation_base, event_led_basebase, led_command_base):
    print("enter in led animation base, waiting")
    while True:
        event_startledanimation_base.wait() #with set wait method works ; with not set better use if but not in this case
        print("!!event_startledanimation _base !!")
        aa = goLED_base(ser, event_led_basebase, led_command_base)
        if aa=='aa':
            print("BBBBBBBBBBBBBBBBBBBBBBB _base _base BBBBBBBBBBBBBBBBBBBBBBB")
            #ser.close()
            event_led_basebase.set()
            event_startledanimation_base.clear()
            #occhio qui quando dovrai copiare quella per joypad...crea nuovo metodo
            #break

def dispatcher_led_base(global_status, ser, event_led_base, event_startledanimation_base, event_led_basebase):
    print("dispatcher_led _base is waiting ...........................")
    while True:
        if not event_led_base.is_set():
            print("dispatcher_led base in action!!!!!!!!!!!!!!!!!!!!!!!!")
            #time.sleep(5)
            #print("tempo_passato!!!")
            #led_command.put(1)
            event_startledanimation_base.set()
            event_led_basebase.clear()
            #led_command.put(ser)
            #controller(ser1,ser2,ser3)
            event_led_base.set()
            
            #while not led_command_base.empty():
            #    led_command_base.get()
            #print("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOK _base _base SVUOTATOOOOOOOOOOOOOOOO")
            
            #time.sleep(20)
            #led_command.put(2)
            #event_startledanimation.set()
            #break

def play_first_on_queue(global_status, event_start_play_audio, right_audio_to_play):
    print("entro il first play queue!!")
    while True:
        event_start_play_audio.wait() #with set wait method works ; with not set better use if but not in this case
        print("START EVENTO SETTATO PER SUONARE PER LA PRIMA VOLTA.... @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ first play queue!!")
        play_this = right_audio_to_play.get()
        playthis = sp.sounds_dict.get(play_this,"This sound doesn't exist!")
        sp.play_audio(play_this)
        event_start_play_audio.clear()
        #break

def move_pattern(global_status, stopInterrupt, serMega, coda, primary_lock, queue_lock, event_move, event_principal, event_end_main):
    print("enter in move pattern waiting....waiting....waiting....waiting....waiting....")
    while True:
        #if not coda.empty():
        #    print("coda piena come è giusto che sia!!!")
        #else:
        #    print("coda vuota!!!")
        if not event_move.is_set():
            print("move move move inside !!!!!!!!!!!!!!!!!!!!!!!! move move move inside !!!!!!!!!!")
            #led_command.put(ser)
            #controller(ser1,ser2,ser3)

            move_type = "sadness"
            #move_type = "agree"
            #move_type = "disagree"
            
            ##status = shar.PyGlobalStatus(b'moving')
            ##print("status is {}".format(status.get_globalstatus()))
            ##global_status.put(status)
            
            #move.move_pattern(global_status, stopInterrupt, move_type, primary_lock, queue_lock)
            
            move.move_robot22(stopInterrupt, serMega, move_type, primary_lock, queue_lock)
            print("OK MI SONO MOSSO, POSSO CHIUDERE")
            print("OK MI SONO MOSSO, POSSO CHIUDERE")
            print("OK MI SONO MOSSO, POSSO CHIUDERE")
            print("OK MI SONO MOSSO, POSSO CHIUDERE")
            print("OK MI SONO MOSSO, POSSO CHIUDERE")
            print("OK MI SONO MOSSO, POSSO CHIUDERE")
            event_move.set() #lo stesso di adesso senno va avanti all'inifinito
            event_principal.clear()
            event_end_main.set()
            #serMega.sendreset()  #??
            #command = b'<quiet>'
            #serMega.transmit(command)  #??
            #serMega.close()  #?? 
            #serMega.open()
            #while not led_command.empty():
            #    led_command.get()
            #print("move move move move move move move move move move end")
            
            #time.sleep(20)
            #led_command.put(2)
            #event_startledanimation.set()
            #break
        #else:
        #    print("è settato!!!!")

def contact_manager_go(global_status, stopInterrupt, primary_lock, queue_lock, serNano2, coda, event_main, mpu1_left, mpu2_right, event_principal, result_prediction, event_end_main):
    #removed!!!!
    #reaction1 = rea.Stage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock)
    #reaction1 = rea.Stage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock)
    #reaction1.stage_exhibition(stopInterrupt, primary_lock, queue_lock)
    #with reaction1:
    #    print("uga chaca")
    num_quiet = 0
    while True:
        if not event_principal.is_set():
            event_principal.set()
            gyrox_l = 57
            gyroz_l = 8
            gyrox_r = -145
            gyroz_r = -33
            current_press0, current_press1, current_press2, current_press3, current_press4 = 0.22, 0.20, 0.26, 0.21, 0.19
            #while True:
            n = 0 
            while n<500:
                n+=1
                gyrox_l, gyroz_l, gyrox_r, gyroz_r = bcs.scan_MPUs(mpu1_left, mpu2_right)
                #if result_prediction=='initial':    
                if result_prediction in ['touch','squeeze']:
                    pass
                elif result_prediction in ['quiet']:
                    if num_quiet==100:
                        mpx0,mpx1,mpx2,mpx3,mpx4,gyroxl,gyrozl,gyroxr,gyrozr = function_for_getting_data_for_predicting(serNano2, mpu1_left, mpu2_right)
                        pp = pbcd.prediction(name="pred#1")
                        ddp2, flag_discrimination_1 = pp.calculate_single_sample(mpx0, mpx1, mpx2, mpx3, mpx4, gyroxl, gyroxr)
                        result_prediction = pp.predict_current_sample(ddp2)
                        react_to_touch(result_prediction, event_main, num_quiet, stopInterrupt, primary_lock, queue_lock, serNano2, coda, event_end_main)
                elif result_prediction in ['hug','choke']:
                    #reset gyro
                    gyrox_l = 57
                    gyroz_l = 8
                    gyrox_r = -145
                    gyroz_r = -33
                    time.sleep(2)
                elif result_prediction == 'hit_back' or result_prediction == 'hit_front' or result_prediction == 'shove':
                    #reset gyro
                    gyrox_l = 57
                    gyroz_l = 8
                    gyrox_r = -145
                    gyroz_r = -33

                if (((((abs(gyrox_r)<140 or abs(gyrox_r)>152) and (abs(gyroz_r)<28 or abs(gyroz_r)>40)) or ((gyrox_l<45 or gyrox_l>60) and (gyroz_l<0 or gyroz_l>11)))) \
                or (current_press0>0.24 or current_press1>0.25 or current_press2>0.30 or current_press3>0.24 or current_press4>0.23)):
                    print("!!!!!!! tocco trovato!! mpu!!!!!!!!!!!!!! tocco trovato!! mpu!!!!!!!")
                    #or multiprocessing????
                    current_press0, current_press1, current_press2, current_press3, current_press4 =  serNano2.takeoneMpxScan()
                    current_press0, current_press1, current_press2, current_press3, current_press4
                    print(" -------------------------------- current_press0, current_press1, current_press2, current_press3, current_press4 --------------------------------")
                    print(current_press0, current_press1, current_press2, current_press3, current_press4)
                    if abs(gyrox_r)>=300 or abs(gyrox_l)>=300 or abs(gyroz_r)>=300 or abs(gyrox_l)>=300:
                        result_prediction='shove'
                        if (current_press0>=0.24) or (current_press1>=0.23):
                            result_prediction = 'hit_front' 
                        if (current_press2>=0.31):
                            result_prediction = 'hit_left'
                        if (current_press3>=0.26):
                            result_prediction = 'hit_back'
                        if (current_press4>=0.26):
                            result_prediction = 'hit_right' 
                    else:
                        mpx0,mpx1,mpx2,mpx3,mpx4,gyroxl,gyrozl,gyroxr,gyrozr = function_for_getting_data_for_predicting(serNano2, mpu1_left, mpu2_right)
                        pp = pbcd.prediction(name="pred#1")
                        ddp2, flag_discrimination_1 = pp.calculate_single_sample(mpx0, mpx1, mpx2, mpx3, mpx4, gyroxl, gyroxr)
                        result_prediction = pp.predict_current_sample(ddp2)
                        react_to_touch(result_prediction, event_main, num_quiet, stopInterrupt, primary_lock, queue_lock, serNano2, coda, event_end_main)
                else:
                    result_prediction = 'quiet'

                if result_prediction == 'quiet':
                    num_quiet+=1
                if num_quiet>100:
                    num_quiet=0
                if result_prediction != 'quiet' and num_quiet>50:
                    num_quiet=0
                print ('quiet n is {}'.format(num_quiet))
            print("FINE PROVA MAIN!!!!")
            print("FINE PROVA MAIN!!!!")
            print("FINE PROVA MAIN!!!!")
            print("FINE PROVA MAIN!!!!")
            print("FINE PROVA MAIN!!!!")

def start_stage_exhibition(coda, event_main): 
    print("ok sono in stage_exhibition!!")
    coda.put(1)
    event_main.clear()
############################################################################################
# Main
############################################################################################
def contact_main_new_multiproc(stopInterrupt, primary_lock, queue_lock, serMega, serNano1, serNano2):
    mpu1_left = bimu.IMU_sensor(device_Address_l)
    mpu2_right = bimu.IMU_sensor(device_Address_r)
    print(type(mpu1_left))
    print(type(mpu2_right))
    inspect.isclass(mpu1_left)
    inspect.isclass(mpu2_right)
    #serNano2 = serr.setup_serial_nano2()
    #serNano2.await_arduino()
    #time.sleep(1)
    print(" START!!!!! START!!!!! inside contact manager main!!!!")
    result_prediction = 'initial'
    #time.sleep(1)
    num_quiet = 0
    man = OimiManager()
    man.start()
    lifo_pending_reactions = man.LifoQueue()
    OimiManager.register('pending_reactions', LifoQueue)

    qu_reaction = OimiQueue("fifo qu_allow_take_mpus") 
    event_main = Event()

    print("ok sono in stage main!!")
    print("ok sono in stage main!!")
    print("ok sono in stage main!!")
    print("ok sono in stage main!!")
    print()
    #if not coda.empty():
    #    print("coda piena come è giusto che sia!!!")
    #else:
    #    print("coda vuota!!!")
    #print()
    print("ok sono in stage main!!")
    print("ok sono in stage main!!")
    print("ok sono in stage main!!")
    parent_id = os.getpid()
    main_connection, sub_connection = Pipe()
    coda = OimiQueue("Fifo coda")
    led_command = OimiQueue("Fifo led commands")
    led_command_base = OimiQueue("Fifo led commands1")
    right_audio_to_play = OimiQueue("Fifo play audio command")
    event_startledanimation = Event()
    event_startledanimation_base = Event()
    event_start_play_audio = Event()
    event_led = Event()
    event_principal = Event()
    event_led_body = Event()
    event_led_base = Event()
    event_led_basebase = Event()
    event_move = Event()
    event_end_main = Event()
    manager = OimiManager()
    OimiManager.register('LifoQueue', LifoQueue)
    manager.start()
    global_status = manager.LifoQueue()
    status = shar.PyGlobalStatus(b'beginning')
    global_status.put(status)
    event_main.set() #fundamental!!! for starting correctly exhibition later when its called
    '''erano qui!!!
    ser1, ser2, ser3 = serr.setup_serial()
    #ser1.await_arduinoMega()
    #ser2.await_arduinoNano1()
    #ser3.await_arduinoNano2()
    print("comincio ad aspettare seriali vari")
    ser1.await_arduino()
    ser2.await_arduino()
    ser3.await_arduino()
    '''
    parent_id = os.getpid()
    event_led.set()
    event_led_body.set()
    event_led_base.set()
    event_led_basebase.set()
    event_move.set()
    event_principal.set()
    led_diplay_job = OimiStartProcess(name = 'led_diplay_job',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = activateLED,
                                    daemon = False,
                                    ser = serNano2,
                                    event_startledanimation = event_startledanimation,
                                    event_led_body = event_led_body,
                                    led_command = led_command)
    dispatcher_job = OimiStartProcess(name = 'dispatcher_job',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = dispatcher_led,
                                    daemon = False,
                                    ser = serNano2,
                                    event_startledanimation = event_startledanimation,
                                    event_led_body = event_led_body,
                                    event_led = event_led)

    led_diplay_job_base = OimiStartProcess(name = 'led_diplay_job_base',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = activateLED_base,
                                    daemon = False,
                                    ser = serNano1,
                                    event_startledanimation_base = event_startledanimation_base,
                                    event_led_basebase = event_led_basebase,
                                    led_command_base = led_command_base) 
    
    dispatcher_job_base = OimiStartProcess(name = 'dispatcher_job_base',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = dispatcher_led_base,
                                    daemon = False,
                                    ser = serNano1,
                                    event_led_base = event_led_base,
                                    event_startledanimation_base = event_startledanimation_base,
                                    event_led_basebase = event_led_basebase)

    play_audio_job = OimiStartProcess(name = 'play_audio_job',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = play_first_on_queue,
                                    daemon = False,
                                    event_start_play_audio = event_start_play_audio,
                                    right_audio_to_play = right_audio_to_play)

    start_animation_job = OimiStartProcess(name = 'start_animation_job',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = stage_start_animation,
                                    daemon = False,
                                    coda = coda,
                                    event_led = event_led,
                                    event_move = event_move,
                                    led_command = led_command,
                                    led_command_base = led_command_base,
                                    event_led_base = event_led_base,
                                    event_start_play_audio = event_start_play_audio,
                                    right_audio_to_play = right_audio_to_play, 
                                    event_main = event_main,
                                    event_end_main = event_end_main)

    movement_pattern_job = OimiStartProcess(name = 'movement_pattern_job',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = move_pattern,
                                    daemon = False,
                                    stopInterrupt = stopInterrupt,
                                    serMega = serMega,
                                    coda = coda,
                                    primary_lock = primary_lock,
                                    queue_lock = queue_lock,
                                    event_move = event_move,
                                    event_principal = event_principal,
                                    event_end_main = event_end_main)

    detect_touch_job = OimiStartProcess(name = 'detect_touch_job',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = contact_manager_go,
                                    daemon = False,
                                    stopInterrupt = stopInterrupt,
                                    primary_lock = primary_lock,
                                    queue_lock = queue_lock,
                                    serNano2 = serNano2,
                                    coda = coda,
                                    event_main = event_main, 
                                    mpu1_left = mpu1_left,
                                    mpu2_right = mpu2_right,
                                    event_principal = event_principal,
                                    result_prediction = result_prediction,
                                    event_end_main = event_end_main)

    print("ok sono in stage_exhibition!!")
    led_diplay_job.start()
    dispatcher_job.start()
    led_diplay_job_base.start()
    dispatcher_job_base.start()
    start_animation_job.start()
    play_audio_job.start()
    movement_pattern_job.start()
    detect_touch_job.start()
    #time.sleep(2)
    event_principal.clear()

    print("comincio jointtttsss!")
    led_diplay_job.join()
    dispatcher_job.join()
    led_diplay_job_base.join()
    dispatcher_job_base.join() 
    start_animation_job.join()   
    play_audio_job.join()
    movement_pattern_job.join()
    detect_touch_job.join()

    
    
    #####################################à#sooner or later....by the way...when it is better??
    #time.sleep(5)
    
    print("provo a chiudere!")
    #led_diplay_job.suspend()
    led_diplay_job.shutdown()
    dispatcher_job.shutdown()
    led_diplay_job_base.shutdown()
    dispatcher_job_base.shutdown() 
    start_animation_job.shutdown()
    play_audio_job.shutdown()
    movement_pattern_job.shutdown()
    detect_touch_job.shutdown()
