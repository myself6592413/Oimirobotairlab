"""Info:
	Oimi body calculate sample for the logistic regression prediction in real time during touchMe game
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import joblib
import numpy as np 
cimport numpy as np
import pandas as pd
from cpython cimport array
import time
#cimport cython
from sklearn.linear_model import LogisticRegression
import IMU_sensor
#import body_calculate_features as bcf

cdef extern from"Python.h":
	object PyList(float *s, Py_ssize_t leng)
	
cdef extern from "math.h":
	float sqrt(float m)
	float fabs(float m)
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================

cdef funny(int[:] a, int[:] b):
	cdef Py_ssize_t siza = len(a)
	cdef Py_ssize_t sizb = len(b)
	cdef Py_ssize_t i = 0
	cdef bint fluff = 0
	for i  in range(siza):
		if ((a[i]<40) or (a[i]>67)):
			fluff=1
	if fluff==0:
		for j in range(sizb):
			if ((abs(b[j])<40) or (abs(b[j])>170)):
				fluff=1	
	return fluff
	
cdef calc_flag_fun(int[:] giro_xl, int[:] giro_xr):
	cdef Py_ssize_t j = 6
	cdef Py_ssize_t lenl = len(giro_xl)
	cdef Py_ssize_t lenr = len(giro_xr)
	cdef bint flag_ok = 0
	if ((143<abs(giro_xr[5])<=159) and (38<=giro_xl[5]<=60)):
		flag_ok = 1
	for j in range(j,lenl):
		if ((143<abs(giro_xr[j])<=159) and (38<giro_xl[j]<=60) and flag_ok==1):
			flag_ok = 1
		else:
			flag_ok = 0	
	return flag_ok

def dtw(s, t):
	n = len(s)
	m = len(t)
	dtw_matrix = np.zeros((n+1, m+1))
	for i in range(n+1):
		for j in range(m+1):
			dtw_matrix[i, j] = np.inf
	dtw_matrix[0, 0] = 0
	
	for i in range(1, n+1):
		for j in range(1, m+1):
			cost = fabs(s[i-1] - t[j-1])
			# take last min from a square box
			last_min = np.min([dtw_matrix[i-1, j], dtw_matrix[i, j-1], dtw_matrix[i-1, j-1]])
			dtw_matrix[i, j] = cost + last_min
	return dtw_matrix

def warp(s,t):
	ma = dtw(s,t)
	return ma[len(ma)-1][len(ma)-1]

##########################################
cdef float cmin(float[:] a):
	cdef:
		int c
		float mini
		Py_ssize_t la = len(a)
	mini = a[0]
	with nogil:
		for c in range(1,la):
			if (a[c] < mini):
				mini = a[c]
	return mini
##########################################
cdef float cmax(float[:] a):
	cdef:
		int c 
		float maxi
		Py_ssize_t la = len(a)
	maxi = a[0]
	with nogil:
		for c in range(1,la):
			if (a[c] > maxi):
				maxi = a[c]
	return maxi
##########################################
cdef float cavg(float[:] a):
	cdef:
		float sum = 0
		int N = len(a)
	with nogil:
		for i in range(N):
			sum += a[i]
	return sum/N
##########################################
cdef float cstdDev(float[:] a):
	cdef:
		Py_ssize_t i
		Py_ssize_t n = len(a)
		float m = 0.0
		float v = 0.0
	with nogil:
		for i in range(n):
			m+=a[i]
	m /= n
	with nogil:
		for i in range(n):
			v += (a[i] - m)**2
	return sqrt(v / n)
##########################################
cdef dict cplat(float[:] b):
	cdef:
		Py_ssize_t siz = len(b)
		int i = 0
		int j = 0  
		int k = 0  
		int cont = 0  
		bint up = 0
		dict res = dict()
		float prev
		float val
	for j in range(1, siz):
		prev, val = b[j - 1], b[j]
		if up and val < prev:
			res[k]=cont
			k+=1
			up = 0
			cont = 0
		if (val > prev and fabs(val - prev) >= 0.2 and cont==0) or \
			(val > prev and up) or (val == prev and cont != 0):
			cont += 1
			i = j
			up = 1
		if j == siz - 1:
			if up:
				res[k]=cont
			if not res:
				res[k]=cont
			return res		
##########################################
cdef array.array counterpeak(dict urca):
	cdef array.array num_peaks = array.array('i', [0,0,0,0,0])
	for k in urca.keys():
		if urca[k]==1:
			num_peaks[0]+=1
		elif urca[k]==2:
			num_peaks[1]+=1
		elif urca[k]==3: 
			num_peaks[2]+=1
		elif urca[k]==4:
			num_peaks[3]+=1
		elif urca[k]>=5:
			num_peaks[4]+=1
	return num_peaks
##########################################
cdef conv(dict di):
	cdef bint fla= 0
	d = dict({0: 0})
	if di is None:
		fla = 1 
	if fla == 1: 
		return fla, d
	if fla == 0:
		return fla, di	
##########################################
cdef float crange(float[:] numbers, int amount):
	cdef int i = 1
	cdef float mini=0.0
	cdef maxi=0.0
	cdef diff=0.0 
	mini = numbers[0]
	maxi = numbers[0]
	for i in range(amount):
		if numbers[i] < mini:
			mini = numbers[i]
		if maxi < numbers[i]:
			maxi = numbers[i]
	diff = maxi - mini
	return diff
#########################################
cdef dict cplat_mpu(int[:] b):
	cdef Py_ssize_t siz = len(b)
	cdef int i = 0
	cdef int j = 0	
	cdef int k = 0	
	cdef int cont = 0  
	cdef bint up = 0
	cdef dict res = dict()
	cdef float prev
	cdef float val	
	for j in range(1, siz):
		prev, val = b[j - 1], b[j]
		if up and val < prev:
			res[k]=cont
			k+=1
			up = 0
			cont = 0
		if (val > prev and fabs(val - prev) >= 50 and cont==0) or \
			(val > prev and up) or (val == prev and cont != 0):
			cont += 1
			i = j
			up = 1
		if j == siz - 1:
			if up:
				res[k]=cont 
			if not res:
				res[k]=cont
	return res
##########################################
cdef float cavg2(int[:] a):
	cdef int sum = 0
	cdef int N = len(a)
	with nogil:
		for i in range(N):
			sum += a[i]
	return sum/N
##########################################		  
cdef float cstdDev2(int[:] a):
	cdef Py_ssize_t i
	cdef Py_ssize_t n = len(a)
	cdef int m = 0
	cdef int v = 0
	with nogil:
		for i in range(n):
			m+=a[i]
	m /= n	  
	with nogil:
		for i in range(n):
			v += (a[i] - m)**2
	return sqrt(v / n)
	##########################################
# ==========================================================================================================================================================
# Variables
# ==========================================================================================================================================================
sample_quiet0=[0.21,0.22,0.21,0.21,0.21,0.22,0.21,0.22,0.22,0.21]
sample_quiet1=[0.23,0.22,0.22,0.21,0.22,0.22,0.22,0.21,0.21,0.22]
sample_quiet2=[0.28,0.29,0.28,0.29,0.29,0.28,0.29,0.28,0.28,0.28]
sample_quiet3=[0.20,0.20,0.21,0.21,0.21,0.20,0.20,0.21,0.20,0.21]
sample_quiet4=[0.20,0.21,0.20,0.20,0.20,0.20,0.21,0.21,0.21,0.20]

# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cpdef scan_MPUs(mpu1_left, mpu2_right):
	"""simple function that take the values of X gyro and Z gyro in both sensors (l and r), 
	asking it to the class method that read the proper register
	This method is running continuosly in the subprocess that execute TakeMpu
	"""
	gyroxl = mpu1_left.get_gyroX()
	gyrozl = mpu1_left.get_gyroZ()
	gyroxr = mpu2_right.get_gyroX()
	gyrozr = mpu2_right.get_gyroZ()
	#print('gyro1 inside takeMPUs [{} , {}, {}, {}]'.format(gyroxl,gyrozl,gyroxr,gyrozr))

	return gyroxl,gyrozl,gyroxr,gyrozr

cpdef scan_twoMPUs(mpu1_left, mpu2_right):
	"""simple function that take the values of X gyro and Z gyro in both sensors (l and r), 
	for two consecutive times"""
	for i in range(2):
		gyroxl = mpu1_left.get_gyroX()
		gyrozl = mpu1_left.get_gyroZ()
		gyroxr = mpu2_right.get_gyroX()
		gyrozr = mpu2_right.get_gyroZ()
		time.sleep(0.3)
	return gyroxl,gyrozl,gyroxr,gyrozr

cpdef produce_input_sample_and_predict(new1,new2,new3,new4,new5,giro_l,giro_r):
	print("enter in produce_input_sample =E.X. calculate")
	girox_l = np.asarray(giro_l, dtype=np.int32)	
	girox_r = np.asarray(giro_r, dtype=np.int32)	
	cdef int[:] giro_xl = girox_l
	cdef int[:] giro_xr = girox_r	
	prot0 = np.asarray(new1, dtype=np.float32)
	prot1 = np.asarray(new2, dtype=np.float32)
	prot2 = np.asarray(new3, dtype=np.float32)
	prot3 = np.asarray(new4, dtype=np.float32)
	prot4 = np.asarray(new5, dtype=np.float32)
	
	# a bytes-like object is required, not 'list'
	cdef float[:] prot0_view = prot0
	cdef float[:] prot1_view = prot1
	cdef float[:] prot2_view = prot2
	cdef float[:] prot3_view = prot3
	cdef float[:] prot4_view = prot4
	
	min0,min1,min2,min3,min4 = cmin(prot0_view), cmin(prot1_view), cmin(prot2_view), cmin(prot3_view), cmin(prot4_view)
	max0,max1,max2,max3,max4 = cmax(prot0_view), cmax(prot1_view), cmax(prot2_view), cmax(prot3_view), cmax(prot4_view)
	avg0,avg1,avg2,avg3,avg4 = cavg(prot0_view), cavg(prot1_view), cavg(prot2_view), cavg(prot3_view), cavg(prot4_view) 
	std0,std1,std2,std3,std4 = cstdDev(prot0_view), cstdDev(prot1_view), cstdDev(prot2_view), cstdDev(prot3_view), cstdDev(prot4_view)
	range0,range1,range2,range3,range4 = crange(prot0_view,10),crange(prot1_view,10),crange(prot2_view,10),crange(prot3_view,10),crange(prot4_view,10)	
	perc0,perc1,perc2,perc3,perc4 = np.percentile(prot0_view,97),np.percentile(prot1_view,97),np.percentile(prot2_view,97),np.percentile(prot3_view,97),np.percentile(prot4_view,97),
	
	
	pla0, pla1, pla2, pla3, pla4 = cplat(prot0_view), cplat(prot1_view), cplat(prot2_view) , cplat(prot3_view), cplat(prot4_view)
	pla_left = cplat_mpu(giro_xl)
	pla_right = cplat_mpu(giro_xr)
	
	resp_left, da_left = conv(pla_left)
	resp_right, da_right = conv(pla_right)
	
	num_peaks_l = counterpeak(da_left)
	num_peaks_r = counterpeak(da_right)
	
	resp0,da0 = conv(pla0)
	resp1,da1 = conv(pla1)
	resp2,da2 = conv(pla2)
	resp3,da3 = conv(pla3)
	resp4,da4 = conv(pla4)

	num_peaks0 = counterpeak(da0) 
	num_peaks1 = counterpeak(da1) 
	num_peaks2 = counterpeak(da2)
	num_peaks3 = counterpeak(da3)
	num_peaks4 = counterpeak(da4)
	
	'''ddp2 = np.array([[max0,max1,max2,max3,max4,avg0,avg1,avg2,avg3,avg4,std0,std1,std2,std3,std4,
		num_peaks0[0],num_peaks0[1],num_peaks0[2],num_peaks0[3],num_peaks0[4], 
		num_peaks1[0],num_peaks1[1],num_peaks1[2],num_peaks1[3],num_peaks1[4], 
		num_peaks2[0],num_peaks2[1],num_peaks2[2],num_peaks2[3],num_peaks2[4], 
		num_peaks3[0],num_peaks3[1],num_peaks3[2],num_peaks3[3],num_peaks3[4], 
		num_peaks4[0],num_peaks4[1],num_peaks4[2],num_peaks4[3],num_peaks4[4]]])'''
	tw0=warp(sample_quiet0,prot0_view)
	tw1=warp(sample_quiet1,prot1_view)
	tw2=warp(sample_quiet2,prot2_view)
	tw3=warp(sample_quiet3,prot3_view)
	tw4=warp(sample_quiet4,prot4_view)
	
	std_gir_l = cstdDev2(giro_xl)
	std_gir_r = cstdDev2(giro_xr)
	avg_gir_l = cavg2(giro_xl)
	avg_gir_r = cavg2(giro_xr)

	np.set_printoptions(formatter={'float_kind':'{:f}'.format})
	
	#NO
	#ddp2 = np.array([[max0,max1,max2,max3,max4,avg0,avg1,avg2,avg3,avg4,std0,std1,std2,std3,std4]])
	#NO
	#ddp2 = np.array([[max0,max1,max2,max3,max4,avg0,avg1,avg2,avg3,avg4,
	#std0,std1,std2,std3,std4,
	#num_peaks0[0],num_peaks0[4],
	#num_peaks1[0],num_peaks1[4],
	#num_peaks2[0],num_peaks2[4],
	#num_peaks3[0],num_peaks3[4],
	#num_peaks4[0],num_peaks4[4],
	#std_gir_l, std_gir_r]])
	#NEW with dtw!??????????
	'''
	ddp2 = np.array([[
		min0,min1,min2,min3,min4,
		max0,max1,max2,max3,max4,
		avg0,avg1,avg2,avg3,avg4,
		std0,std1,std2,std3,std4,
		range0,range1,range2,range3,range4,
		tw0,tw1,tw2,tw3,tw4,
		num_peaks0[0],num_peaks0[1],
		num_peaks1[0],num_peaks1[1],
		num_peaks2[0],num_peaks2[1],
		num_peaks3[0],num_peaks3[1],
		num_peaks4[0],num_peaks4[1],	 
		avg_gir_l, avg_gir_r,
		std_gir_l, std_gir_r
		]])
	'''
	#YES standard
	ddp2 = np.array([[
		min0,min1,min2,min3,min4,
		max0,max1,max2,max3,max4,
		avg0,avg1,avg2,avg3,avg4,
		std0,std1,std2,std3,std4,
		range0,range1,range2,range3,range4,
		num_peaks0[0],num_peaks0[1],
		num_peaks1[0],num_peaks1[1],
		num_peaks2[0],num_peaks2[1],
		num_peaks3[0],num_peaks3[1],
		num_peaks4[0],num_peaks4[1],	 
		avg_gir_l, avg_gir_r,
		std_gir_l, std_gir_r
		]])

	print("Im calculating ddp2 ddp2")
	print(ddp2)
	print()
	loaded_model = joblib.load("/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/training_body_30genn_2023.sav")
	perceived_interaction = loaded_model.predict(ddp2)
	print("perceived_interaction!")
	print(perceived_interaction)
	return perceived_interaction
