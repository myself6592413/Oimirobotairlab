"""Info:
	Oimi body calculate features for the logistic regression model = aka column of the dataset
	-Max
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
	Pressure signals of 10 / 12 samples 
	
	Min value eliminated
	Acce_std eliminated

	Final sav file imported = > created by "only_training_logistic_regression.py" file
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
from cpython cimport array
import numpy as np 
cimport numpy as np
#cimport cython
cdef extern from"Python.h":
	object PyList(float *s, Py_ssize_t leng)
	
cdef extern from "math.h":
	float sqrt(float m)
	float fabs(float m)
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================

cdef funny(int[:] a, int[:] b):
	cdef Py_ssize_t siza = len(a)
	cdef Py_ssize_t sizb = len(b)
	cdef Py_ssize_t i = 0
	cdef bint fluff = 0
	for i  in range(siza):
		if ((a[i]<40) or (a[i]>67)):
			fluff=1
	if fluff==0:
		for j in range(sizb):
			if ((abs(b[j])<40) or (abs(b[j])>170)):
				fluff=1	
	return fluff
	
	
cdef calc_flag_fun(int[:] std_g_l, int[:] std_g_r):
	cdef Py_ssize_t j = 6
	cdef Py_ssize_t lenl = len(std_g_l)
	cdef Py_ssize_t lenr = len(std_g_r)
	cdef bint flag_ok = 0
	if ((143<abs(std_g_r[5])<=159) and (38<=std_g_l[5]<=60)):
		flag_ok = 1
	for j in range(j,lenl):
		if ((143<abs(std_g_r[j])<=159) and (38<std_g_l[j]<=60) and flag_ok==1):
			flag_ok = 1
		else:
			flag_ok = 0	
	return flag_ok

cdef float crange(float[:] numbers, int amount):
	cdef int i = 1
	cdef float mini=0.0
	cdef maxi=0.0
	cdef diff=0.0 
	mini = numbers[0]
	maxi = numbers[0]
	for i in range(amount):
		if numbers[i] < mini:
			mini = numbers[i]
		if maxi < numbers[i]:
			maxi = numbers[i]
	diff = maxi - mini
	return diff

cdef float cmin(float[:] a):
	cdef int c 
	cdef float mini
	cdef Py_ssize_t la = len(a)
	mini = a[0]
	with nogil:
		for c in range(1,la):
			if (a[c] < mini):
				mini = a[c]
	return mini


cdef float cmax(float[:] a):
	cdef int c 
	cdef float maxi
	cdef Py_ssize_t la = len(a)
	maxi = a[0]
	with nogil:
		for c in range(1,la):
			if (a[c] > maxi):
				maxi = a[c]
	return maxi


cdef float cavg(float[:] a):
	#cdef float [:] x = a
	cdef float sum = 0
	cdef int N = len(a)
	with nogil:
		for i in range(N):
			sum += a[i]
	return sum/N

	
cdef float cstdDev(float[:] a):
	cdef Py_ssize_t i
	cdef Py_ssize_t n = len(a)
	cdef float m = 0.0
	cdef float v = 0.0
	with nogil:
		for i in range(n):
			m+=a[i]
	m /= n    
	with nogil:
		for i in range(n):
			v += (a[i] - m)**2
	return sqrt(v / n)


cdef float cstdDev_gyro(int[:] a):
	cdef Py_ssize_t i
	cdef Py_ssize_t n = len(a)
	cdef float m = 0.0
	cdef float v = 0.0
	with nogil:
		for i in range(n):
			m+=a[i]
	m /= n    
	with nogil:
		for i in range(n):
			v += (a[i] - m)**2
	return sqrt(v / n)


cdef float cavg2(int[:] a):
	#cdef float [:] x = a
	cdef float sum = 0
	cdef int N = len(a)
	with nogil:
		for i in range(N):
			sum += a[i]
	return sum/N


cdef int cmode(int[:] arr, int n):
	cdef int i = 0
	cdef int k = 0
	cdef int j = 0
	cdef int h = 0
	cdef int count = 0
	cdef int maxcount = 0
	cdef int result
	#cdef int[:] m 
	while (i < n):
		j=i+1
		count = 1
		while j<n and arr[i]==arr[j]:
			count+=1
		if (maxcount < count):
			maxcount = count
			result = arr[i]
		i = i + count
		
	#i=0
	#while (i < n):
	#	j=i+1
	#	count = 1
	#	while j<n and arr[i] == arr[h]:
	#		count+=1
	#	if (maxcount == count):
	#		m[k] = arr[i]
	#		k+=1
	#	i=i+count
	return result


cdef dict cplat(float[:] b):
	cdef Py_ssize_t siz = len(b)	
	cdef int i = 0
	cdef int j = 0  
	cdef int k = 0  
	cdef int cont = 0  
	cdef bint up = 0
	cdef dict res = dict()
	cdef float prev
	cdef float val	
	for j in range(1, siz):
		prev, val = b[j - 1], b[j]
		if up and val < prev:
			res[k]=cont
			k+=1
			up = 0
			cont = 0
		if (val > prev and fabs(val - prev) >= 0.02 and cont==0) or (val > prev and up) or (val == prev and cont != 0):
			cont += 1
			i = j
			up = 1
		if j == siz - 1:
			if up:
				res[k]=cont	
			if not res:
				res[k]=cont	
	return res


cdef dict cplat_mpu(int[:] b):
	cdef Py_ssize_t siz = len(b)	
	cdef int i = 0
	cdef int j = 0  
	cdef int k = 0  
	cdef int cont = 0  
	cdef bint up = 0
	cdef dict res = dict()
	cdef float prev
	cdef float val	
	for j in range(1, siz):
		prev, val = b[j - 1], b[j]
		if up and val < prev:
			res[k]=cont
			k+=1
			up = 0
			cont = 0
		if (val > prev and fabs(val - prev) >= 50 and cont==0) or (val > prev and up) or (val == prev and cont != 0):
			cont += 1
			i = j
			up = 1
		if j == siz - 1:
			if up:
				res[k]=cont	
			if not res:
				res[k]=cont	
	return res



cdef array.array counterpeak(dict urca):
	cdef array.array num_peaks = array.array('i', [0,0,0,0,0])
	for k in urca.keys():
		if urca[k]==1:
			num_peaks[0]+=1
		elif urca[k]==2:
			num_peaks[0]+=1
		elif urca[k]==3: 
			num_peaks[2]+=1
		elif urca[k]==4:
			num_peaks[3]+=1
		elif urca[k]>=5:
			num_peaks[4]+=1
	return num_peaks


cdef conv(dict di):
	cdef bint fla= 0
	d = dict({0: 0})
	if dict is None:
		fla = 1 
	if fla == 1: 
		return fla,d
	if fla == 0:
		return fla,di


def dtw(s, t):
	n = len(s)
	m = len(t)
	dtw_matrix = np.zeros((n+1, m+1))
	for i in range(n+1):
		for j in range(m+1):
			dtw_matrix[i, j] = np.inf
	dtw_matrix[0, 0] = 0
	
	for i in range(1, n+1):
		for j in range(1, m+1):
			cost = fabs(s[i-1] - t[j-1])
			# take last min from a square box
			last_min = np.min([dtw_matrix[i-1, j], dtw_matrix[i, j-1], dtw_matrix[i-1, j-1]])
			dtw_matrix[i, j] = cost + last_min
	return dtw_matrix

def warp(s,t):
	ma = dtw(s,t)
	return ma[len(ma)-1][len(ma)-1]