"""Info:
    Oimi body sound detection [Decibel] with sparkfun sensor
Notes:
    For detailed info, look at ./code_docs_of_playing
Author:
    Created by Colombo Giacomo, Politecnico di Milano 2022 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import time
import board
import busio
import math
import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
# ==========================================================================================================================================================
# Variables
# ==========================================================================================================================================================
gain = 1
startime = time.time()
# Create the I2C bus
i2c = busio.I2C(board.SCL, board.SDA)
# Create the ADC object using the I2C bus
ads = ADS.ADS1015(i2c)
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
def sound_detection_main():
	print("{:>5}\t{:>5}".format('raw', 'v'))
	while True:
		chan = AnalogIn(ads, ADS.P0)
		print("{:>5}\t{:>5.3f}".format(chan.value, chan.voltage))
		time.sleep(0.5)

		val1 = 16.801*math.log(math.e,chan.value/1023) + 9.872
		val2 = 20.0  * math.log10 (chan.value) +9+1.76
		val3 = 20.0 * math.log10(chan.value)
		val4 = 10*math.log10(10/6)+20.0 * math.log10(chan.value)
		dB = (chan.value + 83.2073) / 11.003;
		#print(val1)
		#print(val2)
		#print("val3 eccolo{}".format(val3))
		print("val4 == ", val4) #sono i decibel!!!!!!!!
		print("decibel == ", dB) #sono i decibel!!!!!!!!
		time.sleep(.2)
		timenow = time.time()
		diff=timenow-startime
		if(diff>50):
			break
	print("stop")
	print(diff)
	ads.stop_adc()