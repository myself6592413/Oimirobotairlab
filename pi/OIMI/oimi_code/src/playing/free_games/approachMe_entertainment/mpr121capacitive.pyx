"""Info:
    Oimi body capacitive sensors
Notes:
    For detailed info, look at ./code_docs_of_playing
Author:
    Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import time, board, busio, adafruit_mpr121
from datetime import datetime, timedelta
# Create I2C bus.
i2c = busio.I2C(board.SCL, board.SDA)
# Create MPR121 object.
mpr121 = adafruit_mpr121.MPR121(i2c)
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef int fun_capa(mpr121, sec):
	print("CAPA CAPA CAPA CAPA CAPA CAPA CAPA CAPA")
	maxtime = datetime.now()+timedelta(seconds=sec)
	cdef int j = 0 
	while datetime.now()<maxtime:
		beg_time = time.time()
		if i==2:
			return i
		for i in range(4):
			# Call is_touched and pass it then number of the input.	If it's touched
			# it will return True, otherwise it will return False.
			if mpr121[i].value:
				print('Input CAPA CAPA CAPA CAPA CAPA CAPA {} touched!'.format(i))
				break 
		time.sleep(0.25) # Small delay to keep from spamming output messages.
	return 5 
	
def fun_mpr121(sec):
	i = fun_capa(mpr121, sec)
	return i 