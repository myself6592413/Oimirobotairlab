"""Info:
	Oimi approachMe entertainment
	called by free_games_modality
Notes:
	For detailed info, look at ./code_docs_of_playing_free_games
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2022 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import os, sys, time

import multiprocessing
from oimi_process import OimiProcess, OimiStartProcess, OimiQueue, OimiManager
from multiprocessing import Process, Pool, Pipe, Event, Lock, Value
from queue import LifoQueue
import moving.std_navigation.std_navigation as move

#if '/home/pi/OIMI/oimi_code/src/managers/database_manager/' not in sys.path:
#    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/')
#import database_manager as db
import contact_manager as cm
import mpr121capacitive as capa
import sound_detection as sd
import shared as shar
import sounds_playback as sp


#if '/home/pi/OIMI/oimi_code/src/managers/database_manager/' not in sys.path:
#	 sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/')

import moving.std_navigation.std_navigation as move
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
def start_entertainment_process_0(global_status, queue0_choice,queue5_sound,event0_controller,event1_touch,event2_capa,event3_move,event4_capa,event5_sound):
	while True:
		if event0_controller.is_set():
			print("approachMe entertainment is officially started")
			q_element = queue0_choice.get()
			if q_element=="begin":
				print("start_entertainment_process job 0")
				event1_touch.set()
				event2_capa.set()

def take_touch_process_1(global_status, stopInterrupt, primary_lock, queue_lock, serNano2, exact_stages_list_required, event1_touch, event2_capa,event3_move,event_bloccatutto):
	time.sleep(2)
	while True:
		if event1_touch.is_set():
			#if event_bloccatutto.is_set():
			print("enter with success in move_autonomously_process take_touch_process")
			result = cm.contact_manager_approachme(stopInterrupt, primary_lock, queue_lock, serNano2, exact_stages_list_required)
			if result=='shove':
				event3_move.set()
				event1_touch.clear()
				event2_capa.clear()

def take_capacitive_process_2(global_status, stopInterrupt, primary_lock, queue_lock, exact_stages_list_required, event1_touch,event2_capa, event3_move):
	time.sleep(2)	 
	while True:
		if event2_capa.is_set():
			print("enter in take_capacitive_process222")
			i = capa.fun_mpr121(5000)
			if i==2:
				right_reaction = exact_stages_list_required[35]
				print("right_reaction capacitive 22222222222 = ", right_reaction)
				right_reaction.stage_exhibition(stopInterrupt, primary_lock, queue_lock)
				event3_move.set()
				event2_capa.clear()
				event1_touch.clear()

def move_autonomously_process_3(global_status, stopInterrupt, primary_lock, queue_lock, serMega, serNano, serNano2, event3_move, event4_capa):
	time.sleep(2)
	while True:
		if event3_move.is_set():
			move_type = 'autonomous'
			print("333 enter with success in move_autonomously_process")
			move.move_robot_oimi_auto(stopInterrupt, move_type, primary_lock,queue_lock, serMega, serNano, serNano2)
			event4_capa.set()

def take_capacitive_process_4(global_status, stopInterrupt, primary_lock, queue_lock, exact_stages_list_required, queue0_choice, event3_move, event4_capa):
	while True:
		if event4_capa.is_set():
			print("44444444 enter in take_capacitive_process 444")
			i = capa.fun_mpr121(20000)
			if i==2:
				right_reaction = exact_stages_list_required[35]
				print("right_reaction capacitive 44444444444444444 = ", right_reaction)
				right_reaction.stage_exhibition(stopInterrupt, primary_lock, queue_lock)
				#serMega.send_stop() #???? stop movement from here if the event is not enough??
				event3_move.clear()
				event4_capa.clear()
				time.sleep(2)
				queue0_choice.put("begin")

def take_sound_process_5(global_status, queue0_choice, queue5_sound, event1_touch, event2_capa, event3_move, event4_capa, event5_sound):
	while True:
		if event5_sound.is_set():
			permission = queue5_sound.get()
			if permission=="go_sound":
				print("enter in take_sound_process_5")					
				### add return?
				sd.sound_detection_main()
				event5_sound.clear()


def approachMe_main(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, exact_stages_list_required):
	print("ok sono in approachMe_main approachMe_main approachMe_main !!")
	parent_id = os.getpid()
	main_connection, sub_connection = Pipe()
	queue0_choice = OimiQueue("Fifo subroutines of events to begin for job0 controller")
	queue5_sound = OimiQueue("Fifo sound put status")
	
	event0_controller = Event()
	event1_touch = Event()
	event2_capa = Event()
	event3_move = Event()
	event4_capa = Event()
	event5_sound = Event()
	event_bloccatutto = Event()
	manager = OimiManager()
	OimiManager.register('LifoQueue', LifoQueue)
	manager.start()
	global_status = manager.LifoQueue()
	status = shar.PyGlobalStatus(b'beginning')
	global_status.put(status)
	start_entertainment_job_0 = OimiStartProcess(name = 'start_entertainment_job_0',
									sub_connection = sub_connection,
									global_status = global_status,
									parent_id = parent_id,
									execute = start_entertainment_process_0,
									daemon = False,
									queue0_choice = queue0_choice,
									queue5_sound = queue5_sound,
									event0_controller = event0_controller,
									event1_touch = event1_touch,
									event2_capa = event2_capa,
									event3_move = event3_move,
									event4_capa = event4_capa,
									event5_sound = event5_sound)
	touch_listener_job_1 = OimiStartProcess(name = 'touch_listener_job_1',
									sub_connection = sub_connection,
									global_status = global_status,
									parent_id = parent_id,
									execute = take_touch_process_1,
									daemon = False,
									stopInterrupt = stopInterrupt,
									primary_lock = primary_lock,
									queue_lock = queue_lock,
									serNano2 = ser3,
									exact_stages_list_required = exact_stages_list_required,
									event1_touch = event1_touch,
									event2_capa = event2_capa,
									event3_move = event3_move,
									event_bloccatutto = event_bloccatutto)
	capacitive_listener_job_2 = OimiStartProcess(name = 'capacitive_listener_job_2',
									sub_connection = sub_connection,
									global_status = global_status,
									parent_id = parent_id,
									execute = take_capacitive_process_2,
									daemon = False,
									stopInterrupt = stopInterrupt,
									primary_lock = primary_lock,
									queue_lock = queue_lock,
									exact_stages_list_required = exact_stages_list_required,
									event1_touch = event1_touch,
									event2_capa = event2_capa,
									event3_move = event3_move)
	move_auto_job_3 = OimiStartProcess(name = 'move_auto_job_3',
									sub_connection = sub_connection,
									global_status = global_status,
									parent_id = parent_id,
									execute = move_autonomously_process_3,
									daemon = False,
									stopInterrupt = stopInterrupt,
									primary_lock = primary_lock,
									queue_lock = queue_lock,
									serMega = ser1,
									serNano = ser2,
									serNano2 = ser3,
									event3_move = event3_move,
									event4_capa = event4_capa)
	capacitive_listener_job_4 = OimiStartProcess(name = 'capacitive_listener_job_4',
									sub_connection = sub_connection,
									global_status = global_status,
									parent_id = parent_id,
									execute = take_capacitive_process_4,
									daemon = False,
									stopInterrupt = stopInterrupt,
									primary_lock = primary_lock,
									queue_lock = queue_lock,
									exact_stages_list_required = exact_stages_list_required,
									queue0_choice = queue0_choice,
									event3_move = event3_move,
									event4_capa = event4_capa)
	sound_listener_job_5 = OimiStartProcess(name = 'sound_listener_job_5',
									sub_connection = sub_connection,
									global_status = global_status,
									parent_id = parent_id,
									execute = take_sound_process_5,
									daemon = False,
									queue0_choice = queue0_choice,
									queue5_sound = queue5_sound,
									event1_touch = event1_touch,
									event2_capa = event2_capa,
									event3_move = event3_move,
									event4_capa = event4_capa,
									event5_sound = event5_sound)

	start_audio='audio_1'
	play_this = sp.sounds_dict.get(start_audio,"This sound doesn't exist!")
	print("play_this")
	sp.play_audio(play_this)
	start_entertainment_job_0.start()
	touch_listener_job_1.start()
	capacitive_listener_job_2.start()
	move_auto_job_3.start()
	capacitive_listener_job_4.start()

	#to start main!!!
	queue0_choice.put("begin")
	event0_controller.set()
	
	
	print("comincio jointtttsss!")

	start_entertainment_job_0.join()
	touch_listener_job_1.join()
	capacitive_listener_job_2.join()
	move_auto_job_3.join()
	capacitive_listener_job_4.join()

	print("provo a chiudere approachMe!")
	print("provo a chiudere approachMe!")
	print("provo a chiudere approachMe!")
	print("provo a chiudere approachMe!")
	#start_entertainment_job_0.shutdown()
	#touch_listener_job_1.shutdown()
	#capacitive_listener_job_2.shutdown()
	#move_auto_job_3.shutdown()
	#capacitive_listener_job_4.shutdown()


