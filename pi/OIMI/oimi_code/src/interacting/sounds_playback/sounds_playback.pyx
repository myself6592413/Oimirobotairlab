"""Info:
    Oimi all available Audios 
Notes:
    For detailed info, look at ./code_docs_of_entertainments
    --Last version based on pygame audio instead of omxplayer 

    #### access to elements with sounds_dict["si"] / if list => sounds_dict['sad'][0]
To-do:
    check #change to write down all missing audios
    
Author:
    Created by Colombo Giacomo, Politecnico di Milano 2022 """    
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================='
#  Imports:
# ==========================================================================================================================='
import signal, os
from subprocess import run
# ==========================================================================================================================='
#  Elements:
# ==========================================================================================================================='
sounds_dict = {
    ############################################################################################ test 
    "greeting_prova": b'bella',
    "bella": b'bella',
    "audio_1":b'ale_oh_oh',
    "autonomous":b'autonomous_game',
    "following":b'following_game',
    "joypad":b'mi_faccio_un_giro', #change
    "no":b'no',
    "si":b'si',
    "nopat":b'obstacle',
    "blo":b'bella',
    "turn":b'bella',
    "alpa":b'bella',
    "albpa":b'bella',
    "bpat":b'bella',
    "cpat":b'bella',
    "lpat":b'bella',
    "fpat":b'bella',
    "rpat":b'bella',
    "sad": [b'sad_speech1',b'sad_speech2'],
    ############################################################################################ ok
    "greeting_onboot_1": b'ciao_mi_presento_sono_oimi',
    "greeting_start_1": b'ti_aspettavo',
    "greeting_start_2": b'che_bello_vederti',
    "greeting_start_3": b'ciao_bentornato',
    "greeting_start_4": b'come_stai',
    "greeting_start_5": b'e_da_un_po_che_non_ci_vediamo',
    "greeting_start_6": b'mi_chiamo_piacere',
    "greeting_end_1": b'ciao_ci_vediamo_la_prossima_volta',
    "greeting_end_2": b'Abbiamo_finito_per_oggi',
    "greeting_end_3": b'Ci_vediamo_la_prossima_volta',
    "greeting_end_4": b'che_bella_partita_abbiamo_fatto',
    "promt_simple_1": b'allora',
    "prompt_simple_2": b'ti_ho_trovato',
    "compliments_1": b'sei_simpatico',
    "compliments_2": b'sei_un_genio',
    "compliments_3": b'wow_bravo',
    "congratulation_1": b'ottimo_lavoro',
    "congratulation_2": b'bravissimo',
    "congratulation_3": b'grande',
    "cheers_startsession_3": b'vuoi_giocare_oggi',
    "cheers_startgame_1": b'dai_giochiamo_insieme',
    "cheers_startgame_2": b'impariamo_insieme',
    "cheers_endgame_1": b'ci_siamo_divertiti_vero',
    "cheers_endgame_2": b'urra',
    "pleased_squeeze_3": b'mi_fai_il_solletico',
    "pleased_mood_hug_3": b'abbracciami_ancora',
    "pleased_mood_hug_2": b'viva_gli_abbracci',
    "pleased_caress_2": b'anchio_ti_voglio_bene',
    "happy_celebration_1": b'ale_oh_oh',
    "happy_celebration_2": b'che_bello_evviva',
    "happy_celebration_3": b'che_bello',
    "happy_celebration_4": b'che_felicita',
    "happy_celebration_5": b'che_goduria',
    "happy_celebration_6": b'come_sono_contento',
    "happy_celebration_7": b'evviva',
    "raise_overturning_1": b'aiutami_a_rialzarmi',
    "fear_mood_1": b'che_paura',
    "fear_mood_2": b'che rumore spaventoso',
    "angry_mood_1": b'uffa',
    "angry_mood_2": b'che_rabbia',
    "angry_mood_4": b'uffa_che_rabbia',
    "surprise_mood_1": b'chi_va_la',
    "surprise_mood_2": b'che_sorpresa',
    "surprise_mood_3": b'eccoti_qua',
    "sad_mood_1": b'che_tristezza',
    "sad_mood_3": b'che_tristezza_uffa_che_rabbia',
    "enthusiast_mood_1": b'grande_ale_oh_oh',
    "moan_hit_1": b'ahia_che_botta',
    "scold_sad_hit_1": b'non_fare_cosi_mi_fai_male',
    "scold_sad_hit_2": b'non_si_fa_non_e_bello',
    "scold_sad_hit_3": b'smettila',
    "free_fami_game_1": b'mi_faccio_un_giro',
    "free_fami_game_2": b'oh_che_bella_dormita',
    "free_fami_game_3": b'ora_devo_andare',
    "free_fami_game_4": b'seguimi',
    "free_fami_game_5": b'sono_qui_vieni',
    "free_touch_game_1": b'dammi_una_carezza',
    "free_touch_game_2": b'dammi_un_abbraccio',
    "free_touch_game_3": b'scuotimi',
    "prompt_game_1": b'continua_cosi',
    "prompt_game_2": b'dove_sei',
    "prompt_game_3": b'e_il_mio_turno',
    "game_already_1": b'mi_hai_gia_dato_questa_risposta',
    "game_indecision_1": b'sei_indeciso_ti_ripeto_la_domanda_2',
    "game_indecision_2": b'sei_indeciso_ti_ripeto_la_domanda',
    "game_error_1": b'che_peccato_non_e_giusto',
    "game_error_2": b'che_peccato_non_e_giusto_riprova',
    "game_error_3": b'no_mi_spiace_non_e_la_risposta_giusta',
    "game_error_4": b'riprova',
    "game_error_5": b'riproviamo',
    "game_error_6": b'risposta_sbagliata_riprova',
    "game_waiting_interaction_1": b'ti_aspetto_rispondimi',
    "top_right_position": b'alto_a_destra',
    "top_left_position": b'alto_a_sinistra',
    "down_right_position": b'basso_a_destra',
    "down_left_position": b'basso_a_sinistra',
    "blu": b'blu',
    "giallo": b'giallo',
    "rosso": b'rosso',
    "verde": b'verde',
    "patch_bottone": b'bottone',
    "patches_bottoni": b'bottoni',
    "occhiali": b'gli_occhiali_da_sole',
    "ombrelli": b'gli_ombrelli',
    "bottone_blu": b'il_bottone_blu',
    "bottone_giallo": b'il_bottone_giallo',
    "bottone_rosso": b'il_bottone_rosso',
    "bottone_verde": b'il_bottone_verde',
    "leoni": b'i_leoni',
    "leone": b'il_leone',
    "monopattino": b'il_monopattino',
    "pappagallo": b'il_pappagallo',
    "polipo": b'il_polipo',
    "monopattini": b'i_monopattini',
    "fear_mood_3": b'io_scappo',
    "pappagalli": b'i_pappagalli',
    "polipi": b'i_polipi',
    "bambina": b'la_bambina',
    "bicicletta": b'la_bicicletta',
    "scimmia": b'la_scimmia',
    "bambine": b'le_bambine',
    "biciclette": b'le_biciclette',
    "scimmie": b'le_scimmie',
    "ombrello": b'lombrello',
    ############################################################################################ questions
    "question_game_11": b'Premi_testa_se_luce_arancione_alt_tocca_leone',
    "question_game_12": b'Premi_testa_se_luce_blu_alt_tocca_polipo',
    "question_game_13": b'Premi_testa_se_luce_rossa_alt_tocca_scimmia',
    "question_game_14": b'Premi_testa_se_luce_verde_alt_tocca_leone',
    "question_game_15": b'Premi_testa_se_luce_viola_alt_tocca_ombrello',
    "question_game_11": b'Tocca_chi_arrampica',
    "question_game_11": b'Tocca_chi_no_citta',
    "question_game_11": b'Tocca_chi_no_zampe',
    "question_game_11": b'Tocca_chi_ruggisce',
    "question_game_11": b'Tocca_chi_sa_nuotare',
    "question_game_11": b'Tocca_disegno_colore_uguale_al_primo_colore',
    "question_game_11": b'Tocca_due_volte',
    "question_game_11": b'Tocca__gli_ombrelli',
    "question_game_11": b'Tocca_i_disegni_blu_e_gialli',
    "question_game_11": b'Tocca_i_disegni_blu',
    "question_game_11": b'Tocca_i_disegni_gialli',
    "question_game_11": b'Tocca_i_disegni_rossi_e_blu',
    "question_game_11": b'Tocca_i_disegni_rossi_e_gialli',
    "question_game_11": b'Tocca_i_disegni_rossi_e_verdi',
    "question_game_11": b'Tocca_i_disegni_rossi',
    "question_game_11": b'Tocca_i_disegni_verdi_e_blu',
    "question_game_11": b'Tocca_i_disegni_verdi_e_gialli',
    "question_game_11": b'Tocca_i_disegni_verdi',
    "question_game_11": b'Tocca_i_leoni_e_gli_ombrelli',
    "question_game_11": b'Tocca_i_leoni_e_i_polipi',
    "question_game_11": b'Tocca_i_leoni_e_le_scimmie',
    "question_game_11": b'Tocca_i_leoni',
    "question_game_11": b'Tocca_il_leone_di_questo_colore',
    "question_game_11": b'Tocca_il_leone_rosso_e_ombrello_giallo',
    "question_game_11": b'Tocca_il_leone_rosso_e_polipo_blu',
    "question_game_11": b'Tocca_il_leone_rosso',
    "question_game_11": b'Tocca_il_leone_se_la_luce_e_blu',
    "question_game_11": b'Tocca_il_polipo_blu_e_ombrello_giallo',
    "question_game_11": b'Tocca_il_polipo_blu',
    "question_game_11": b'Tocca_il_polipo_di_questo_colore',
    "question_game_11": b'Tocca_in ordine il pappagallo, la scimmia e il leone',
    "question_game_11": b'Tocca_insieme_leone_rosso_ombrello_giallo',
    "question_game_11": b'Tocca_insieme_leone_rosso_polipo_blu',
    "question_game_11": b'Tocca_insieme_polipo_blu_ombrello_giallo',
    "question_game_11": b'Tocca_insieme_scimmia_rossa_leone_rosso',
    "question_game_11": b'Tocca_insieme_scimmia_rossa_ombrello_giallo',
    "question_game_11": b'Tocca_insieme_scimmia_rossa_polipo_blu',
    "question_game_11": b'Tocca_insieme_un_disegno_blue_e_giallo',
    "question_game_11": b'Tocca_insieme_un_disegno_blue_e_verde',
    "question_game_11": b'Tocca_insieme_un_disegno_rosso_e_blu',
    "question_game_11": b'Tocca_insieme_un_disegno_rosso_e_giallo',
    "question_game_11": b'Tocca_insieme_un_disegno_rosso_e_verde',
    "question_game_11": b'Tocca_insieme_un_disegno_verde_e_blu',
    "question_game_11": b'Tocca_insieme_un_disegno_verde_e_giallo',
    "question_game_11": b'Tocca_i_polipi_gli_ombrelli',
    "question_game_11": b'Tocca_i_polipi',
    "question_game_11": b'Tocca_la_scimmia_di_questo_colore',
    "question_game_11": b'Tocca_la_scimmia_rossa_leone_rosso',
    "question_game_11": b'Tocca_la_scimmia_rossa',
    "question_game_11": b'Tocca_la_scimmia_rossa_ombrello_giallo',
    "question_game_11": b'Tocca_la_scimmia_rossa_polipo_blu',
    "question_game_11": b'Tocca_leone_se_luce_blu_alt_ombrello',
    "question_game_11": b'Tocca_leone_se_luce_blu_alt_scimmia',
    "question_game_11": b'Tocca_leone_se_luce_verde_alt_ombrello',
    "question_game_11": b'Tocca_leone_se_luce_viola_alt_polipo',
    "question_game_11": b'Tocca_leoni_se_luce_blu',
    "question_game_11": b'Tocca_leoni_se_luce_gialla',
    "question_game_11": b'Tocca_le_scimmie_e_gli_ombrelli',
    "question_game_11": b'Tocca_le_scimmie_e_i_polipi',
    "question_game_11": b'Tocca_le_scimmie',
    "question_game_11": b'toccami_la_pancia',
    "question_game_11": b'toccami_la_testa',
    "question_game_11": b'Tocca_nello_stesso_momento_',
    "question_game_11": b'Tocca_ombrelli_se_luce_blu',
    "question_game_11": b'Tocca_ombrelli_se_luce_verde',
    "question_game_11": b'Tocca_ombrello_di_questo_colore',
    "question_game_11": b'Tocca_ombrello_giallo',
    "question_game_11": b'Tocca_ombrello_se_luce_arancione_alt_leone',
    "question_game_11": b'Tocca_ombrello_se_luce_blu_alt_polipo',
    "question_game_11": b'Tocca_ombrello_se_luce_rossa_alt_scimmia',
    "question_game_11": b'Tocca_pancia_3_volte_se_scimmia',
    "question_game_11": b'Tocca_pancia_luce_gialla_o_pappagallo',
    "question_game_11": b'Tocca_polipi_se_dis_giallo_e_blu',
    "question_game_11": b'Tocca_polipi_se_luce_rossa',
    "question_game_11": b'Tocca_polipi_se_luce_verde',
    "question_game_11": b'Tocca_polipo_se_giallo_o_scimmia_se_verde',
    "question_game_11": b'Tocca_polipo_se_luce_blu_alt_scimmia',
    "question_game_11": b'Tocca_polipo_se_luce_rossa_alt_leone',
    "question_game_11": b'Tocca_polipo_se_luce_verde_alt_ombrello',
    "question_game_11": b'Tocca_prima_un_disegno_giallo_e_poi_un_disegno_rosso',
    "question_game_11": b'Tocca_scimmia_se_luce_blu_alt_ombrello',
    "question_game_11": b'Tocca_scimmia_se_luce_rossa_alt_leone',
    "question_game_11": b'Tocca_scimmia_se_luce_verde_alt_polipo',
    "question_game_11": b'Tocca_scimmie_se_luce_rossa',
    "question_game_11": b'Tocca_scimmie_se_luce_verde',
    "question_game_11": b'Tocca_tutti_gli_animali',
    "question_game_11": b'Tocca_tutti_gli_animali_rossi',
    "question_game_11": b'Tocca_tutti_gli_animali_verdi',
    "question_game_11": b'Tocca_tutti_gli_oggetti',
    "question_game_11": b'Tocca_un_animale_rosso',
    "question_game_11": b'Tocca_un_leone_se_mi_coloro_di_giallo',
    "question_game_11": b'Tocca_un_oggetto_giallo',
    ################################################################################################## OLD different voice
    "old_cheers_endgame_3": b't_abbiamo_finito_con_questo_esercizio', 
    "old_angry_moan_1": b't_ahia',
    "old_free_game_obstacle_1": b't_aiuto_ostacolo',
    "old_propt_game_5": b't_andiamo_avanti_con_la_prossima',
    "old_scold_sad_hit_4": b't_basta_mi_fai_male',
    "old_greeting_onboot_2": b't_bene_conosciamoci_meglio_con_qualche_gioco',
    "old_cheers_startsession_1": b't_bene_tempo_di_imparare',
    "old_cheers_endmatch_1": b't_bravo_prossima_volta_ancora_meglio',
    "old_game_right_1": b't_ci_siamo_e_esatto',
    "old_game_right_2": b't_corretto_stiamo_imparando',
    "old_game_right_3": b't_complimenti_prossima_volta',
    "old_noise_fear_1": b't_cosa_e_stato_rumore',
    "old_cheers_startsession_2": b't_di_cosa_parliamo_oggi',
    "old_free_game_obstacle_2": b't_di_qui_non_si_passa',
    "old_pleased_touch_2": b't_ecco_ti_ho_visto',
    "old_free_game_obstacle_3": b't_e_meglio_cambiare_direzione',
    "old_game_error_7": b't_e_sbagliato_riproviamo',
    "old_game_right_4": b't_esercizio_perfetto_spaziale',
    "old_match_category_game_1": b't_giochiamo_con_animali_e_colori',
    "old_match_category_game_2": b't_giochiamo_con_oggetti_o_persone',
    "old_match_category_game_3": b't_impariamo_colori',
    "old_game_right_5": b't_grande_esercizio_complimenti',
    "old_pleased_caress_1": b't_grazie_mi_piacciono_le_carezze',
    "old_game_already_2": b't_hai_gia_scelto_questo',
    "old_game_scenario_already_1": b't_hai_scelto_scenario',
    "old_cheers_startmatch_1": b't_iniziamo_a_fare_esercizi_toccami_pancia',
    "old_pleased_hug_1": b't_mi_piace',
    "old_game_error_8": b't_mmm_non_e_la_risp_giusta_prova_ancora',
    "old_quiet_interaction_1": b't_non_ti_vedo',
    "old_free_fami_game_6": b't_ops_ti_ho_perso_non_ti_vedo',
    "old_quiet_hum_1": b't_parapaparapappapa',
    "old_quiet_hum_2": b't_peppeperepeppe',
    "old_method_game_scenario_1": b't_posiziona_immagini_correttamente',
    "old_method_game_scenario_2": b't_quale_scenario',
    "old_game_error_9": b't_riprova_non_e_giusto',
    "old_cheers_startmatch_2": b't_scegliamo_cosa_fare_oggi',
    "old_battery_charge": b't_sono_molto_stanco_mi_serve_una_ricarica',
    "old_noise_fear_2": b't_spavento_rumore',
    "old_free_game_obstacle_4": b't_stavo_per_schiantarmi',
    "old_game_indecision_3": b't_vuoi_pensarci_ancora_un_po_ricominciamo',
    "old_pleased_hug_2": b't_wow_anchio_ti_voglio_bene',
    "old_match_category_game_4": b't_giochiamo_conoscenza',
    ################################################################################################## OLD questions different voice
    "old_question_metgame_500": b't_tocca_animale_cammina_due_zampe_non_puo_volare.mp3',
    "old_question_metgame_501": b't_tocca_animale_cammina_quattro_zampe.mp3',
    "old_question_metgame_502": b't_tocca_animale_che_ha_il_becco.mp3',
    "old_question_metgame_503": b't_tocca_animale_che_sa_nuotare.mp3',
    "old_question_metgame_504": b't_tocca_animale_no_zampe.mp3',
    "old_question_metgame_505": b't_tocca_chi_sa_correre.mp3',
    "old_question_metgame_506": b't_tocca_chi_sa_parlare.mp3',
    "old_question_metgame_507": b't_tocca_cosa_ha_le_route.mp3',
    "old_question_metgame_508": b't_tocca_cosa_non_parla.mp3',
    "old_question_metgame_509": b't_tocca_cosa_puoi_indossare.mp3',
    "old_question_metgame_500": b't_tocca_cosa_si_puo_mettere_sulla_testa.mp3',
    "old_question_metgame_511": b't_tocca_tocca_cosa_si_puo_mangiare.mp3',
    "old_question_metgame_140": b't_tocca_gli_animali_blu.mp3',
    "old_question_metgame_141": b't_tocca_gli_animali_dello_stesso_colore.mp3',
    "old_question_metgame_142": b't_tocca_gli_animali_gialli.mp3',
    "old_question_metgame_143": b't_tocca_gli_animali.mp3',
    "old_question_metgame_144": b't_tocca_gli_animali_rossi.mp3',
    "old_question_metgame_145": b't_tocca_gli_animali_verdi.mp3',
    "old_question_metgame_146": b't_tocca_gli_occhiali.mp3',
    "old_question_metgame_147": b't_tocca_gli_oggetti.mp3',
    "old_question_metgame_148": b't_tocca_il_bambino.mp3',
    "old_question_metgame_149": b't_tocca_il_cappello.mp3',
    "old_question_metgame_150": b't_tocca_il_cavallo.mp3',
    "old_question_metgame_151": b't_tocca_il_colore_come_questo.mp3',
    "old_question_metgame_152": b't_tocca_il_gatto.mp3',
    "old_question_metgame_153": b't_tocca_il_gelato.mp3',
    "old_question_metgame_154": b't_tocca_il_leone.mp3',
    "old_question_metgame_155": b't_tocca_il_panino.mp3',
    "old_question_metgame_156": b't_tocca_il_pappagallo.mp3',
    "old_question_metgame_157": b't_tocca_il_polipo.mp3',
    "old_question_metgame_158": b't_tocca_il_signore.mp3',
    "old_question_metgame_159": b't_tocca_intruso.mp3',
    "old_question_metgame_160": b't_tocca_la_macchina.mp3',
    "old_question_metgame_161": b't_tocca_la_scimmia.mp3',
    "old_question_metgame_162": b't_tocca_la_sedia.mp3',
    "old_question_metgame_163": b't_tocca_la_signora.mp3',
    "old_question_metgame_164": b't_tocca_lombrello.mp3'
}
# ============================================================================================================================
#  Methods:
# ============================================================================================================================
cdef speak(char* comm):
    """ Play proper audio without printing anything on std output, calling a subprocess daemon that run pygame audio player (no more omxplayer!)"""
    print("In the meantime I start a speech, playing an audio")
    if comm == b'bella':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_bello_evviva.mp3' > /dev/null 2>&1", shell=True)
    elif comm==b'abbiamo_finito_per_oggi': 
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/abbiamo_finito_per_oggi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'abbracciami_ancora': 
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/abbracciami_ancora.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ahia_che_botta':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ahia_che_botta.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'aiutami_a_rialzarmi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/aiutami_a_rialzarmi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ale_oh_oh':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ale_oh_oh.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'allora':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/allora.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'alto_a_destra':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/alto_a_destra.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'alto_a_sinistra':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/alto_a_sinistra.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'anchio_ti_voglio_bene':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/anchio_ti_voglio_bene.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'basso_a_destra':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/basso_a_destra.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'basso_a_sinistra':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/basso_a_sinistra.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'bottone':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/bottone.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'bottoni':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/bottoni.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'bravissimo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/bravissimo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_bella_partita_abbiamo_fatto':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_bella_partita_abbiamo_fatto.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_bello_evviva':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_bello_evviva.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_bello':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_bello.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_bello_vederti':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_bello_vederti.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_felicita':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_felicita.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_goduria':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_goduria.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_paura':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_paura.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_peccato_non_e_giusto':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_peccato_non_e_giusto.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_peccato_non_e_giusto_riprova':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_peccato_non_e_giusto_riprova.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_rabbia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_rabbia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che rumore spaventoso':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che rumore spaventoso.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_sorpresa':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_sorpresa.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_tristezza':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_tristezza.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'che_tristezza_uffa_che_rabbia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_tristezza_uffa_che_rabbia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'chi_va_la':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/chi_va_la.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ciao_bentornato':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ciao_bentornato.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ciao_ci_vediamo_la_prossima_volta':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ciao_ci_vediamo_la_prossima_volta.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ciao_mi_presento_sono_oimi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ciao_mi_presento_sono_oimi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ci_siamo_divertiti_vero':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ci_siamo_divertiti_vero.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ci_vediamo_la_prossima_volta':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ci_vediamo_la_prossima_volta.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'come_sono_contento':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/come_sono_contento.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'come_stai':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/come_stai.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'continua_cosi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/continua_cosi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'dai_giochiamo_insieme':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/dai_giochiamo_insieme.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'dammi_un_abbraccio':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/dammi_un_abbraccio.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'dammi_una_carezza':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/dammi_una_carezza.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'dove_sei':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/dove_sei.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'eccoti_qua':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/eccoti_qua.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'e_da_un_po_che_non_ci_vediamo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/e_da_un_po_che_non_ci_vediamo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'e_il_mio_turno':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/e_il_mio_turno.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'evviva':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/evviva.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'gli_occhiali_da_sole':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/gli_occhiali_da_sole.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'gli_ombrelli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/gli_ombrelli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'grande_ale_oh_oh':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/grande_ale_oh_oh.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'grande':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/grande.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'il_bottone_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/il_bottone_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'il_bottone_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/il_bottone_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'il_bottone_rosso':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/il_bottone_rosso.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'il_bottone_verde':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/il_bottone_verde.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'i_leoni':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/i_leoni.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'il_leone':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/il_leone.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'il_monopattino':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/il_monopattino.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'il_pappagallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/il_pappagallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'il_polipo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/il_polipo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'i_monopattini':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/i_monopattini.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'impariamo_insieme':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/impariamo_insieme.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'io_scappo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/io_scappo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'i_pappagalli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/i_pappagalli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'i_polipi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/i_polipi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'la_bambina':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/la_bambina.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'la_bicicletta':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/la_bicicletta.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'la_scimmia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/la_scimmia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'le_bambine':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/le_bambine.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'le_biciclette':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/le_biciclette.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'le_scimmie':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/le_scimmie.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'lombrello':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/lombrello.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'mi_chiamo_piacere':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/mi_chiamo_piacere.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'mi_faccio_un_giro':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/mi_faccio_un_giro.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'mi_fai_il_solletico':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/mi_fai_il_solletico.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'mi_hai_gia_dato_questa_risposta':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/mi_hai_gia_dato_questa_risposta.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'no_mi_spiace_non_e_la_risposta_giusta':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/no_mi_spiace_non_e_la_risposta_giusta.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'non_fare_cosi_mi_fai_male': 
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/non_fare_cosi_mi_fai_male.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'non_si_fa_non_e_bello': 
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/non_si_fa_non_e_bello.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'oh_che_bella_dormita': 
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/oh_che_bella_dormita.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ora_devo_andare':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ora_devo_andare.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ottimo_lavoro':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ottimo_lavoro.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Premi_testa_se_luce_arancione_alt_tocca_leone': 
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Premi_testa_se_luce_arancione_alt_tocca_leone.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Premi_testa_se_luce_blu_alt_tocca_polipo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Premi_testa_se_luce_blu_alt_tocca_polipo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Premi_testa_se_luce_rossa_alt_tocca_scimmia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Premi_testa_se_luce_rossa_alt_tocca_scimmia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Premi_testa_se_luce_verde_alt_tocca_leone':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Premi_testa_se_luce_verde_alt_tocca_leone.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Premi_testa_se_luce_viola_alt_tocca_ombrello':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Premi_testa_se_luce_viola_alt_tocca_ombrello.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'riprova':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/riprova.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'riproviamo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/riproviamo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'risposta_sbagliata_riprova':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/risposta_sbagliata_riprova.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'rosso':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/rosso.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'scuotimi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/scuotimi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'seguimi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/seguimi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'sei_indeciso_ti_ripeto_la_domanda_2':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/sei_indeciso_ti_ripeto_la_domanda_2.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'sei_indeciso_ti_ripeto_la_domanda':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/sei_indeciso_ti_ripeto_la_domanda.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'sei_simpatico':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/sei_simpatico.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'sei_un_genio':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/sei_un_genio.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'smettila':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/smettila.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'sono_qui_vieni':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/sono_qui_vieni.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ti_aspettavo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ti_aspettavo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ti_aspetto_rispondimi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ti_aspetto_rispondimi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'ti_ho_trovato':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/ti_ho_trovato.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_chi_arrampica':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_chi_arrampica.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_chi_no_citta':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_chi_no_citta.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_chi_no_zampe':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_chi_no_zampe.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_chi_ruggisce':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_chi_ruggisce.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_chi_sa_nuotare':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_chi_sa_nuotare.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_disegno_colore_uguale_al_primo_colore':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_disegno_colore_uguale_al_primo_colore.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_due_volte':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_due_volte.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca__gli_ombrelli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca__gli_ombrelli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_disegni_blu_e_gialli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_disegni_blu_e_gialli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_disegni_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_disegni_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_disegni_gialli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_disegni_gialli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_disegni_rossi_e_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_disegni_rossi_e_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_disegni_rossi_e_gialli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_disegni_rossi_e_gialli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_disegni_rossi_e_verdi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_disegni_rossi_e_verdi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_disegni_rossi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_disegni_rossi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_disegni_verdi_e_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_disegni_verdi_e_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_disegni_verdi_e_gialli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_disegni_verdi_e_gialli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_disegni_verdi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_disegni_verdi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_leoni_e_gli_ombrelli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_leoni_e_gli_ombrelli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_leoni_e_i_polipi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_leoni_e_i_polipi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_leoni_e_le_scimmie':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_leoni_e_le_scimmie.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_leoni':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_leoni.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_il_leone_di_questo_colore':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_il_leone_di_questo_colore.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_il_leone_rosso_e_ombrello_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_il_leone_rosso_e_ombrello_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_il_leone_rosso_e_polipo_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_il_leone_rosso_e_polipo_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_il_leone_rosso':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_il_leone_rosso.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_il_leone_se_la_luce_e_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_il_leone_se_la_luce_e_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_il_polipo_blu_e_ombrello_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_il_polipo_blu_e_ombrello_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_il_polipo_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_il_polipo_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_il_polipo_di_questo_colore':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_il_polipo_di_questo_colore.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_in ordine il pappagallo, la scimmia e il leone':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_in ordine il pappagallo, la scimmia e il leone.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_leone_rosso_ombrello_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_leone_rosso_ombrello_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_leone_rosso_polipo_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_leone_rosso_polipo_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_polipo_blu_ombrello_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_polipo_blu_ombrello_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_scimmia_rossa_leone_rosso':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_scimmia_rossa_leone_rosso.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_scimmia_rossa_ombrello_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_scimmia_rossa_ombrello_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_scimmia_rossa_polipo_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_scimmia_rossa_polipo_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_un_disegno_blue_e_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_un_disegno_blue_e_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_un_disegno_blue_e_verde':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_un_disegno_blue_e_verde.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_un_disegno_rosso_e_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_un_disegno_rosso_e_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_un_disegno_rosso_e_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_un_disegno_rosso_e_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_un_disegno_rosso_e_verde':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_un_disegno_rosso_e_verde.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_un_disegno_verde_e_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_un_disegno_verde_e_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_insieme_un_disegno_verde_e_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_insieme_un_disegno_verde_e_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_polipi_gli_ombrelli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_polipi_gli_ombrelli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_i_polipi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_i_polipi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_la_scimmia_di_questo_colore':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_la_scimmia_di_questo_colore.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_la_scimmia_rossa_leone_rosso':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_la_scimmia_rossa_leone_rosso.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_la_scimmia_rossa':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_la_scimmia_rossa.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_la_scimmia_rossa_ombrello_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_la_scimmia_rossa_ombrello_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_la_scimmia_rossa_polipo_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_la_scimmia_rossa_polipo_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_leone_se_luce_blu_alt_ombrello':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_leone_se_luce_blu_alt_ombrello.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_leone_se_luce_blu_alt_scimmia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_leone_se_luce_blu_alt_scimmia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_leone_se_luce_verde_alt_ombrello':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_leone_se_luce_verde_alt_ombrello.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_leone_se_luce_viola_alt_polipo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_leone_se_luce_viola_alt_polipo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_leoni_se_luce_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_leoni_se_luce_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_leoni_se_luce_gialla':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_leoni_se_luce_gialla.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_le_scimmie_e_gli_ombrelli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_le_scimmie_e_gli_ombrelli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_le_scimmie_e_i_polipi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_le_scimmie_e_i_polipi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_le_scimmie':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_le_scimmie.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'toccami_la_pancia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/toccami_la_pancia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'toccami_la_testa':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/toccami_la_testa.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_nello_stesso_momento_':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_nello_stesso_momento_.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_ombrelli_se_luce_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_ombrelli_se_luce_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_ombrelli_se_luce_verde':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_ombrelli_se_luce_verde.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_ombrello_di_questo_colore':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_ombrello_di_questo_colore.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_ombrello_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_ombrello_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_ombrello_se_luce_arancione_alt_leone':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_ombrello_se_luce_arancione_alt_leone.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_ombrello_se_luce_blu_alt_polipo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_ombrello_se_luce_blu_alt_polipo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_ombrello_se_luce_rossa_alt_scimmia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_ombrello_se_luce_rossa_alt_scimmia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_pancia_3_volte_se_scimmia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_pancia_3_volte_se_scimmia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_pancia_luce_gialla_o_pappagallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_pancia_luce_gialla_o_pappagallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_polipi_se_dis_giallo_e_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_polipi_se_dis_giallo_e_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_polipi_se_luce_rossa':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_polipi_se_luce_rossa.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_polipi_se_luce_verde':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_polipi_se_luce_verde.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_polipo_se_giallo_o_scimmia_se_verde':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_polipo_se_giallo_o_scimmia_se_verde.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_polipo_se_luce_blu_alt_scimmia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_polipo_se_luce_blu_alt_scimmia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_polipo_se_luce_rossa_alt_leone':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_polipo_se_luce_rossa_alt_leone.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_polipo_se_luce_verde_alt_ombrello':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_polipo_se_luce_verde_alt_ombrello.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_prima_un_disegno_giallo_e_poi_un_disegno_rosso':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_prima_un_disegno_giallo_e_poi_un_disegno_rosso.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_scimmia_se_luce_blu_alt_ombrello':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_scimmia_se_luce_blu_alt_ombrello.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_scimmia_se_luce_rossa_alt_leone':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_scimmia_se_luce_rossa_alt_leone.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_scimmia_se_luce_verde_alt_polipo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_scimmia_se_luce_verde_alt_polipo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_scimmie_se_luce_rossa':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_scimmie_se_luce_rossa.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_scimmie_se_luce_verde':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_scimmie_se_luce_verde.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_tutti_gli_animali':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_tutti_gli_animali.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_tutti_gli_animali_rossi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_tutti_gli_animali_rossi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_tutti_gli_animali_verdi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_tutti_gli_animali_verdi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_tutti_gli_oggetti':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_tutti_gli_oggetti.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_un_animale_rosso':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_un_animale_rosso.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_un_leone_se_mi_coloro_di_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_un_leone_se_mi_coloro_di_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'Tocca_un_oggetto_giallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/Tocca_un_oggetto_giallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'uffa_che_rabbia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/uffa_che_rabbia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'uffa':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/uffa.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'urra':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/urra.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'verde':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/verde.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'viva_gli_abbracci':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/viva_gli_abbracci.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'vuoi_giocare_oggi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/vuoi_giocare_oggi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b'wow_bravo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/wow_bravo.mp3' > /dev/null 2>1", shell=True)

    elif comm==b't_abbiamo_finito_con_questo_esercizio':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_abbiamo_finito_con_questo_esercizio.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_ahia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_ahia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_aiuto_ostacolo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_aiuto_ostacolo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_andiamo_avanti_con_la_prossima':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_andiamo_avanti_con_la_prossima.mp3' > /dev/null 2>1", shell=True)
    elif comm== b't_basta_mi_fai_male':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_basta_mi_fai_male.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_bene_conosciamoci_meglio_con_qualche_gioco':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_bene_conosciamoci_meglio_con_qualche_gioco.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_bene_tempo_di_imparare':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_bene_tempo_di_imparare.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_bravo_prossima_volta_ancora_meglio':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_bravo_prossima_volta_ancora_meglio.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_ci_siamo_e_esatto':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_ci_siamo_e_esatto.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_corretto_stiamo_imparando':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_corretto_stiamo_imparando.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_complimenti_prossima_volta':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_complimenti_prossima_volta.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_cosa_e_stato_rumore':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_cosa_e_stato_rumore.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_di_cosa_parliamo_oggi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_di_cosa_parliamo_oggi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_di_qui_non_si_passa':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_di_qui_non_si_passa.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_ecco_ti_ho_visto':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_ecco_ti_ho_visto.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_e_meglio_cambiare_direzione':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_e_meglio_cambiare_direzione.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_e_sbagliato_riproviamo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_e_sbagliato_riproviamo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_esercizio_perfetto_spaziale':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_esercizio_perfetto_spaziale.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_giochiamo_con_animali_e_colori':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_giochiamo_con_animali_e_colori.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_giochiamo_con_oggetti_o_persone':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_giochiamo_con_oggetti_o_persone.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_impariamo_colori':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_impariamo_colori.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_grande_esercizio_complimenti':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_grande_esercizio_complimenti.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_grazie_mi_piacciono_le_carezze':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_grazie_mi_piacciono_le_carezze.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_hai_gia_scelto_questo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_hai_gia_scelto_questo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_hai_scelto_scenario':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_hai_scelto_scenario.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_iniziamo_a_fare_esercizi_toccami_pancia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_iniziamo_a_fare_esercizi_toccami_pancia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_mi_piace':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_mi_piace.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_mmm_non_e_la_risp_giusta_prova_ancora':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_mmm_non_e_la_risp_giusta_prova_ancora.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_non_ti_vedo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_non_ti_vedo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_ops_ti_ho_perso_non_ti_vedo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_ops_ti_ho_perso_non_ti_vedo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_parapaparapappapa':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_parapaparapappapa.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_peppeperepeppe':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_peppeperepeppe.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_posiziona_immagini_correttamente':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_posiziona_immagini_correttamente.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_quale_scenario':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_quale_scenario.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_riprova_non_e_giusto':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_riprova_non_e_giusto.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_scegliamo_cosa_fare_oggi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_scegliamo_cosa_fare_oggi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_sono_molto_stanco_mi_serve_una_ricarica':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_sono_molto_stanco_mi_serve_una_ricarica.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_spavento_rumore':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_spavento_rumore.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_stavo_per_schiantarmi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_stavo_per_schiantarmi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_vuoi_pensarci_ancora_un_po_ricominciamo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_vuoi_pensarci_ancora_un_po_ricominciamo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_wow_anchio_ti_voglio_bene':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_wow_anchio_ti_voglio_bene.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_giochiamo_conoscenza':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_giochiamo_conoscenza.mp3' > /dev/null 2>1", shell=True)

    elif comm==b't_tocca_animale_cammina_due_zampe_non_puo_volare':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_animale_cammina_due_zampe_non_puo_volare.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_animale_cammina_quattro_zampe':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_animale_cammina_quattro_zampe.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_animale_che_ha_il_becco':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_animale_che_ha_il_becco.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_animale_che_sa_nuotare':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_animale_che_sa_nuotare.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_animale_no_zampe':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_animale_no_zampe.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_chi_sa_correre':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_chi_sa_correre.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_chi_sa_parlare':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_chi_sa_parlare.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_cosa_ha_le_route':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_cosa_ha_le_route.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_cosa_non_parla':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_cosa_non_parla.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_cosa_puoi_indossare':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_cosa_puoi_indossare.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_cosa_si_puo_mettere_sulla_testa':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_cosa_si_puo_mettere_sulla_testa.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_tocca_cosa_si_puo_mangiare':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_tocca_cosa_si_puo_mangiare.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_gli_animali_blu':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_gli_animali_blu.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_gli_animali_dello_stesso_colore':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_gli_animali_dello_stesso_colore.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_gli_animali_gialli':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_gli_animali_gialli.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_gli_animali':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_gli_animali.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_gli_animali_rossi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_gli_animali_rossi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_gli_animali_verdi':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_gli_animali_verdi.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_gli_occhiali':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_gli_occhiali.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_gli_oggetti':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_gli_oggetti.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_bambino':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_bambino.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_cappello':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_cappello.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_cavallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_cavallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_colore_come_questo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_colore_come_questo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_gatto':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_gatto.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_gelato':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_gelato.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_leone':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_leone.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_panino':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_panino.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_pappagallo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_pappagallo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_polipo':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_polipo.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_il_signore':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_il_signore.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_intruso':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_intruso.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_la_macchina':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_la_macchina.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_la_scimmia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_la_scimmia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_la_sedia':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_la_sedia.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_la_signora':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_la_signora.mp3' > /dev/null 2>1", shell=True)
    elif comm==b't_tocca_lombrello':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/mp3_gained_volume/t_tocca_lombrello.mp3' > /dev/null 2>1", shell=True)

    



def play_audio(comm):
    cdef char* audio = comm
    speak(comm)

"""
import signal, os
from subprocess import run 

sounds_dict = {"greeting_1": b'bella', 'autonomous':b'bella', 'following':b'bella', 
                "left1":b'bella',
                "left2":b'bella',
                "right1":b'bella',
                "right2":b'bella',
                "front1":b'bella',
                "back1":b'bella',
                "no":b'bella',
                "nopat":b'bella',
                "blo":b'bella',
                "turn":b'bella',
                "alpa":b'bella',
                "albpa":b'bella',
                "bpat":b'bella',
                "cpat":b'bella',
                "lpat":b'bella',
                "fpat":b'bella',
                "rpat":b'bella',
                "sadness": b'bella',
                "audio_1": b'bella',
                "audio_2": b'bella',
                "audio_3": b'bella',
                "audio_4": b'bella',
                "sad": [b'sad_speech1',b'sad_speech2']}

cdef speak(char* comm):
    print("44444444444444444444444 ECCOCI IN SPEAK 444444444444444444444444444444444444444444 ")
    if comm == b'bella':
        print("5555555555555555555555555555555555 COMMM = BELLA 5555555555555555555555555 ")
        #run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/0086.wav' > /dev/null 2>&1", shell=True)
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/OIMI/oimi_media/audio_files_volume/che_bello_evviva.mp3' > /dev/null 2>&1", shell=True)
        #pg.mixer.init()
        ##pg.mixer.music.load("/home/pi/0086.wav")
        #print("aa = ", sys.argv[1])
        #pg.mixer.music.load(sys.argv[1])
        #pg.mixer.music.play()
        #while pg.mixer.music.get_busy()==True:
        #    continue
    elif comm == b'zio':
        run("python /home/pi/OIMI/oimi_code/src/interacting/sounds_playback/reproduce_single_audio.py '/home/pi/0086.wav' > /dev/null 2>&1", shell=True)

def play_audio(comm):
    cdef char* audio = comm
    speak(comm)
"""


