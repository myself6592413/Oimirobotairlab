""" Oimi reactions class
    --stages
    --
    todo: 
    #add into db time_to_wait_ba/bo for kids...it's an important feature for the reaction

    Created by Colombo Giacomo Airlab Polimi 2022
"""
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys, os, time, random
import serial
from re import search
from subprocess import run
import shared as shar
import multiprocessing
from oimi_process import OimiProcess, OimiStartProcess, OimiQueue, OimiManager
from multiprocessing import Process, Pool, Pipe, Event, Lock, Value
from queue import LifoQueue
import sounds_playback as sp
import serial_manager as serr
import moving.std_navigation.std_navigation as move
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/')
#import sqlite3 as lite useless no??? is inside db
import database_manager as db
# ==========================================================================================================================================================
# Classes
# ==========================================================================================================================================================
cdef class Stage:
    def __cinit__(self, int id_stage, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3):
        self.id_stage = id_stage
        self.coda = coda
        self.event_main = event_main
        self.ser1 = ser1
        self.ser2 = ser2
        self.ser3 = ser3
        #state machine??? FUTURE???
        #self.user_dict_sess = {"task_preli": self.preliminary_activities_session,"task_begin_play":self.go_to_match,"task_finals":self.final_activities_session,"task_update_child":self.update_kid_stats} #for fsm! 
        self.stage_main(stopInterrupt, primary_lock, queue_lock) #da spostare giu???
    def __enter__(self):
        print("Initializing the stage reaction...")

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("The stage have been created successfully.")
        print("Im exiting from stage")
    
    # -----------------------------------------------------------------------------
    # Getters, setters 
    # -----------------------------------------------------------------------------
    @property
    def id_stage(self):
        return self.id_stage
    @id_stage.setter
    def id_stage(self, value):
        self.id_stage = value
    @property
    def category(self):
        return self.category
    @category.setter
    def category(self, value):
        self.category = value
    @property
    def event_main(self):
        return self.event_main
    @event_main.setter
    def event_main(self, value):
        self.event_main = value   
    @property
    def event_startledanimation(self):
        return self.event_startledanimation
    @event_startledanimation.setter
    def event_startledanimation(self, value):
        self.event_startledanimation = value
    @property
    def event_led_body(self):
        return self.event_led_body
    @event_led_body.setter
    def event_led_body(self, value):
        self.event_led_body = value
    @property
    def event_led_bodybody(self):
        return self.event_led_bodybody
    @event_led_bodybody.setter
    def event_led_bodybody(self, value):
        self.event_led_bodybody = value
    @property
    def event_startledanimation_base(self):
        return self.event_startledanimation_base
    @event_startledanimation_base.setter
    def event_startledanimation_base(self, value):
        self.event_startledanimation_base = value
    @property
    def event_led_basebase(self):
        return self.event_led_basebase
    @event_led_basebase.setter
    def event_led_basebase(self, value):
        self.event_led_basebase = value
    @property
    def event_led_base(self):
        return self.event_led_base
    @event_led_base.setter
    def event_led_base(self, value):
        self.event_led_base = value
    @property
    def event_start_play_audio(self):
        return self.event_start_play_audio
    @event_start_play_audio.setter
    def event_start_play_audio(self, value):
        self.event_start_play_audio = value
    @property
    def led_command_body(self):
        return self.led_command_body
    @led_command_body.setter
    def led_command_body(self, value):
        self.led_command_body = value
    @property
    def led_command_base(self):
        return self.led_command_base
    @led_command_base.setter
    def led_command_base(self, value):
        self.led_command_base = value        
    @property
    def right_audio_to_play(self):
        return self.right_audio_to_play
    @right_audio_to_play.setter
    def right_audio_to_play(self, value):
        self.right_audio_to_play = value

    @property
    def ser1(self):
        return self.ser1
    @ser1.setter
    def ser1(self, value):
        self.ser1 = value
    @property
    def ser3(self):
        return self.ser3
    @ser3.setter
    def ser3(self, value):
        self.ser3 = value        
    @property
    def ser2(self):
        return self.ser2
    @ser2.setter
    def ser2(self, value):
        self.ser2 = value        
    @property
    def global_status(self):
        return self.global_status
    @global_status.setter
    def global_status(self, value):
        self.global_status = value        
    @property
    def sub_connection(self):
        return self.sub_connection
    @sub_connection.setter
    def sub_connection(self, value):
        self.sub_connection = value        

    def stage_start_animation(self, global_status, ):
        while True:
            if not self.event_main.is_set():
                print("COMINCIA ADESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSOOOOOOOOOOOOOOOOOOOOOOOOOOO")
                coda_el = self.coda.get()
                if coda_el==1:
                    print("COMINCIA dopoooooooooooooooooooooooooooooooooo")
                    ''' prima = ordine move + led base + led base + audio
                    self.event_move.clear()
                    self.led_command_body.put(self.stage_selected_LED_body)
                    self.led_command_base.put(self.stage_selected_LED_base)
                    self.event_led_body.clear()
                    self.event_led_base.clear()
                    '''
                    self.led_command_base.put(self.stage_selected_LED_base)
                    self.led_command_body.put(self.stage_selected_LED_body)
                    self.event_led_base.clear()
                    self.event_led_body.clear()
                    self.event_move.clear()
                    #stt1 = "no"
                    print("-----------------------------------------------------------------------")
                    print("stage_selected_audio === ", self.stage_selected_audio)
                    stt = sp.sounds_dict.get(self.stage_selected_audio,"This sound doesn't exists!")
                    print("stt, stt, stt", stt)
                    print("stt, stt, stt", stt)
                    print("stt, stt, stt", stt)
                    self.right_audio_to_play.put(stt)
                    self.event_start_play_audio.set()
                    print("ok ho impostato tutto in stage_start_animation")
                    break
            #else:
            #    print("event_main_settato!!!")

    def goLED_body(self, ser, time_to_wait_bo):
        #print("!!enter in goled_body!!")
        #while True:
            #try:
        if not self.event_led_bodybody.is_set():
            print("event body not set, quindi posso entrare in go_led_body")
            #ser = led_command_body.get()
            #print("ser nella coda = ", ser)
            conferma = self.led_command_body.get()
            print("led str with info nella coda = ", conferma)
            #ser.sendled(conferma)
            #if conferma=='none' or conferma is Null: come si fa null?????
            #conferma = "stable_blue"
            if conferma=='none' or conferma is None:
                print("conferma is NULL!!")
                #for test....reput pass later!
                conferma = "stable_blue"
                ser.sendSpecLED(conferma)
                time.sleep(time_to_wait_bo)
                ser.sendled(8) #stop as was before with int??                                
                #pass     
            else:
                substring = "_"
                if search(substring, conferma): #its a Stage which specified LED and pattern indication
                    ser.sendSpecLED(conferma)
                    time.sleep(3.5)
                    ser.sendled(8) #stop necessary not in simple color animation...with react_this
                    #ser.sendreset() #to remove ?=!?!!?!?? to take single press from nano2
                else: #command of type quiet => reaction
                    conf = ser.take_proper_msg_body_touch_reaction(conferma)
                    print("conf conf conf = ",conf)
                    print("conf conf conf = ",conf)
                    print("conf conf conf = ",conf)
                    print("conf conf conf = ",conf)
                    ser.send_led_anim(conf)
                    time.sleep(6.5)
                    ser.sendled(8) #stop as was before with int???? 
                    #ser.sendreset() #to remove ?=!?!!?!?? to take single press from nano2
            aa = 'aa'
            return aa

    def activateLED_body(self, global_status, ser, time_to_wait_bo):
        #print("enter in led animation body, waiting")
        while True:
            self.event_startledanimation.wait() #with set wait method works ; with not set better use if but not in this case
            print("!!event_startledanimation!!")
            aa = self.goLED_body(ser, time_to_wait_bo)
            if aa=='aa':
                print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa body")
                self.event_led_bodybody.set() 
                self.event_startledanimation.clear()
                break
                

    def dispatcher_led_body(self, global_status, ser):
        #print("dispatcher_led is waiting ...........................")
        if not self.event_startledanimation.is_set():
            print("CIAOOOOOOOOOOOOOOOOOOOOO")
        while True:
            if not self.event_led_body.is_set():
                print("dispatcher_led body sono entrato in in body!!!!!!!!!!!!!!!!!!!!!!!!")
                #time.sleep(5)
                #print("tempo_passato!!!")
                #led_command_body.put(1)
                self.event_startledanimation.set()
                self.event_led_bodybody.clear()
                #led_command_body.put(ser)
                #controller(ser1,ser2,ser3)
                self.event_led_body.set() #lo stesso di adesso senno va avanti all'inifinito
                
                #occhio che qui fa casino anche adesso mi sa se aggiungo movimento!!!!!
                #per adesso commento...
                #while not self.led_command_body.empty():
                #    self.led_command_body.get()
                #print("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOK SVUOTATOOOOOOOOOOOOOOOO")
                

                #time.sleep(20)
                #led_command_body.put(2)
                #event_startledanimation.set()
                break

    def goLED_base(self, ser, time_to_wait_ba):
        #print("!!enter in goLed_base!!")
        #while True:
            #try:
        if not self.event_led_basebase.is_set():
            print("event _base not set, quindi posso entrare in goled_base")
            #ser = led_command_body.get()
            #print("ser nella coda = ", ser)
            
            #conferma = self.led_command_base.get()
            #print("led numero nella coda della basedella basedella basedella base della basedella base della base = ", conferma)
            #ser.sendled(conferma)
            #time.sleep(3.5)
            #ser.sendled(8)
            ##ser.sendreset()

            conferma = self.led_command_base.get()
            print("led numero nella coda = ", conferma)
            #ser.sendled(conferma)
            if conferma=='none' or conferma is None:
                print("conferma is NULL!!")
                #for test....reput pass later!
                conferma = "stable_blue"
                ser.sendSpecLED(conferma)
                time.sleep(time_to_wait_ba) #more the animation last more is the time needed ... need to passed as parameter!! 
                ser.sendled(8) #stop as was before with int??                
                #pass     
            else:                
                substring = "_"
                if search(substring, conferma): #its a Stage which specified LED and pattern indication
                    ser.sendSpecLED(conferma)
                    time.sleep(3)
                    ser.sendled(8) #stop necessary not in simple color animation...with react_this
                    #ser.sendreset() #to remove ?=!?!!?!?? to take single press from nano2
                else: #command of type quiet => reaction
                    conf = ser.take_proper_msg_body_touch_reaction(conferma)
                    ser.send_led_anim(conf)
                    time.sleep(3)
                    ser.sendled(8) #stop as was before with int???? 
                    #ser.sendreset() #to remove ?=!?!!?!?? to take single press from nano2
            aa = 'aa'
            return aa

    def activateLED_base(self, global_status, ser, time_to_wait_ba):
        #print("enter in led animation base, waiting")
        while True:
            self.event_startledanimation_base.wait() #with set wait method works ; with not set better use if but not in this case
            #print("!!event_startledanimation _base !!")
            aa = self.goLED_base(ser, time_to_wait_ba)
            if aa=='aa':
                print("BBBBBBBBBBBBBBBBBBBBBBB _base _base BBBBBBBBBBBBBBBBBBBBBBB")
                #ser.close()
                self.event_led_basebase.set()
                self.event_startledanimation_base.clear()
                #occhio qui quando dovrai copiare quella per joypad...crea nuovo metodo
                break

    def dispatcher_led_base(self, global_status, ser):
        #print("dispatcher_led _base is waiting ...........................")
        while True:
            if not self.event_led_base.is_set():
                print("dispatcher_led base in action!!!!!!!!!!!!!!!!!!!!!!!!")
                #time.sleep(5)
                #print("tempo_passato!!!")
                #led_command_body.put(1)
                self.event_startledanimation_base.set()
                self.event_led_basebase.clear()
                #led_command_body.put(ser)
                #controller(ser1,ser2,ser3)
                self.event_led_base.set()
                
                #while not self.led_command_base.empty():
                #    self.led_command_base.get()
                #print("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOK _base _base SVUOTATOOOOOOOOOOOOOOOO")
                
                #time.sleep(20)
                #led_command_body.put(2)
                #event_startledanimation.set()
                break

    def play_first_on_queue(self, global_status):
        #print("entro il first play queue!!")
        while True:
            self.event_start_play_audio.wait() #with set wait method works ; with not set better use if but not in this case
            print("START EVENTO SETTATO PER SUONARE PER LA PRIMA VOLTA.... @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ first play queue!!")
            play_this = self.right_audio_to_play.get()
            #playthis = sp.sounds_dict.get(play_this,"This sound doesn't exist!")
            print("playthis playthis playthis playthis reaction = = = ", play_this)
            print("playthis playthis playthis playthis reaction tipo tipo tipo = = = ", type(play_this))
            sp.play_audio(play_this)
            self.event_start_play_audio.clear()
            break
    
    def move_pattern(self, global_status, stopInterrupt, serMega, primary_lock, queue_lock):
        #print("enter in move pattern waiting....waiting....waiting....waiting....waiting....")
        while True:
            if not self.coda.empty():
                print("coda piena come è giusto che sia!!!")
            #else:
            #    #print("coda vuota!!!")
            if not self.event_move.is_set():
                print("move move move inside !!!!!!!!!!!!!!!!!!!!!!!! move move move inside !!!!!!!!!!")
                #led_command_body.put(ser)
                #controller(ser1,ser2,ser3)
                #until the content of the database is wrong...
                #works without queue???
                print("self.stage_selected_movement = ", self.stage_selected_movement)
                print("self.stage_selected_movement = ", self.stage_selected_movement)
                print("self.stage_selected_movement = ", self.stage_selected_movement)
                print("self.stage_selected_movement = ", self.stage_selected_movement)
                if self.stage_selected_movement=='movement_1': #no_more!! cancel?
                    #move_type = "sadness"
                    #move_type = "happy"
                    #move_type = "anger"
                    #move_type = "scare"
                    #move_type = "enthusiast"
                    move_type = "surprised_1"
                elif self.stage_selected_movement=='movement_2': #no_more!! cancel?
                    #move_type = "sadness"
                    #move_type = "happy"
                    #move_type = "anger"
                    #move_type = "scare"
                    #move_type = "enthusiast"
                    move_type = "surprised_2"
                elif self.stage_selected_movement=='movement_3': #no_more!! cancel?
                    #move_type = "sadness"    
                    #move_type = "happy"
                    #move_type = "anger"
                    #move_type = "scare"
                    #move_type = "enthusiast"
                    move_type = "surprised_3"
                else: ## goes here!!
                    move_type = self.stage_selected_movement
                    #move_type = "sadness"    
                    #move_type = "happy"
                #if self.stage_selected_movement=='movement_1':
                #    move_type = "disagree"
                
                ##status = shar.PyGlobalStatus(b'moving')
                ##print("status is {}".format(status.get_globalstatus()))
                ##global_status.put(status)
                if move_type in ['sadness']:
                    #move.move_pattern(global_status, stopInterrupt, move_type, primary_lock, queue_lock)
                    move.move_robot2(stopInterrupt, serMega, move_type, primary_lock, queue_lock)
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")
                    self.event_move.set() #lo stesso di adesso senno va avanti all'inifinito
                    #serMega.sendreset()  #??
                    #command = b'<quiet>'
                    #serMega.transmit(command)  #??
                    #serMega.close()  #?? 
                    #serMega.open()
                    #while not self.led_command_body.empty():
                    #    self.led_command_body.get()
                    #print("move move move move move move move move move move end")
                    
                    #time.sleep(20)
                    #led_command_body.put(2)
                    #event_startledanimation.set()
                    break
                if move_type in ['scare','anger','enthusiast']:
                    #move.move_pattern(global_status, stopInterrupt, move_type, primary_lock, queue_lock)
                    move.move_robot22(stopInterrupt, serMega, move_type, primary_lock, queue_lock)
                    print("OK MI SONO MOSSO, POSSO CHIUDERE move22")
                    print("OK MI SONO MOSSO, POSSO CHIUDERE move22")
                    print("OK MI SONO MOSSO, POSSO CHIUDERE move22")
                    print("OK MI SONO MOSSO, POSSO CHIUDERE move22")
                    self.event_move.set() #lo stesso di adesso senno va avanti all'inifinito
                    #serMega.sendreset()  #??
                    #command = b'<quiet>'
                    #serMega.transmit(command)  #??
                    #serMega.close()  #?? 
                    #serMega.open()
                    #while not self.led_command_body.empty():
                    #    self.led_command_body.get()
                    #print("move move move move move move move move move move end")
                    #time.sleep(20)
                    #led_command_body.put(2)
                    #event_startledanimation.set()
                    break
                if move_type in ['quiet','surprised_1','surprised_2','surprised_3', 
                        'reactst_neg_1', 'reactst_neg_2', 'reactst_neg_3','reactst_neg_4', 'reactst_neg_5', 'reactst_neg_6',
                        'reactst_pos_1', 'reactst_pos_2', 'reactst_pos_3','reactst_pos_4', 'reactst_pos_5', 'reactst_pos_6',
                        'attention_mov1','attention_mov2','attention_mov3',
                        'encourage_mov1','encourage_mov2','encourage_mov3',
                        'greetingst_mov1','greetingst_mov2','greetingst_mov3',
                        'greetingen_mov1','greetingen_mov2','greetingen_mov3',
                        'rewardright_mov1','rewardright_mov2','rewardright_mov3',
                        'rewardwrong_mov1','rewardwrong_mov2','rewardwrong_mov3',
                        'rewardgame_mov1','rewardgame_mov2','rewardgame_mov3',
                        'rewardmatch_mov1','rewardmatch_mov2',
                        'rewardmatch_mov3','rewardsess_mov1','rewardsess_mov2','rewardsess_mov3',
                        'anger_mov2','anger_mov1','anger_mov2','anger_mov3','enthu_mov1','enthu_mov2','enthu_mov3',
                        'happy_mov1','happy_mov2','happy_mov3','sad_mov1','sad_mov2','sad_mov3','surp_mov1',
                        'surp_mov2','surp_mov3','scare_mov1','scare_mov2','scare_mov3']:
                    move.move_robot4(stopInterrupt, serMega, move_type, primary_lock, queue_lock)
                    print("OK 444444444444, POSSO CHIUDERE move44444444444444444444")
                    print("OK 444444444444, POSSO CHIUDERE move44444444444444444444")
                    print("OK 444444444444, POSSO CHIUDERE move44444444444444444444")
                    print("OK 444444444444, POSSO CHIUDERE move44444444444444444444")
                    self.event_move.set() 
                    break       
                ###########sistemoooooooooooooooo
                stra_speed, for_speed, ang_speed = 0.05, 20.0, 0.5
                if move_type=='happy':
                    move.move_robot3(stopInterrupt, serMega, move_type, primary_lock, queue_lock, stra_speed, for_speed, ang_speed)
                    move.move_robot3(stopInterrupt, serMega, move_type, primary_lock, queue_lock, stra_speed, for_speed, ang_speed)
                    move.move_robot3(stopInterrupt, serMega, move_type, primary_lock, queue_lock, stra_speed, for_speed, ang_speed)
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")                    
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")                    
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")                    
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")                    
                    print("OK MI SONO MOSSO, POSSO CHIUDERE")                    
                    self.event_move.set()
                    break
            #else:
            #    print("è settato!!!!")

    #-------------------------#--------------------------#--------------------------#--------------------------#--------------------------
    # main
    #--------------------------#--------------------------#--------------------------#--------------------------#-------------------------
    def stage_main(self, stopInterrupt, primary_lock, queue_lock): #activation stage display
        print("ok sono in stage main!!")
        print("ok sono in stage main!!")
        print("ok sono in stage main!!")
        print("ok sono in stage main!!")
        print()
        if not self.coda.empty():
            print("coda piena come è giusto che sia!!!")
        #else:
        #    print("coda vuota!!!")
        print()
        print("ok sono in stage main!!")
        print("ok sono in stage main!!")
        print("ok sono in stage main!!")
        self.parent_id = os.getpid()
        main_connection, self.sub_connection = Pipe()
        self.led_command_body = OimiQueue("Fifo led commands")
        self.led_command_base = OimiQueue("Fifo led commands1")
        self.right_audio_to_play = OimiQueue("Fifo play audio command")
        self.event_startledanimation = Event()
        self.event_startledanimation_base = Event()
        self.event_start_play_audio = Event()
        self.event_led_body = Event()
        self.event_led_bodybody = Event()
        self.event_led_base = Event()
        self.event_led_basebase = Event()
        self.event_move = Event()
        manager = OimiManager()
        OimiManager.register('LifoQueue', LifoQueue)
        manager.start()
        self.global_status = manager.LifoQueue()
        status = shar.PyGlobalStatus(b'beginning')
        self.global_status.put(status)

        #self.event_main.set() #fundamental!!! for starting correctly exhibition later when its called

        '''erano qui!!!
        self.ser1, self.ser2, self.ser3 = serr.setup_serial()
        #ser1.await_arduinoMega()
        #ser2.await_arduinoNano1()
        #ser3.await_arduinoNano2()
        print("comincio ad aspettare seriali vari")
        self.ser1.await_arduino()
        self.ser2.await_arduino()
        self.ser3.await_arduino()
        '''
    
        #ser1.prova1() da mettere poi!!!
        self.event_led_body.set()
        self.event_led_bodybody.set()
        self.event_led_base.set()
        self.event_led_basebase.set()
        self.event_move.set()

    def stage_exhibition(self, stopInterrupt, primary_lock, queue_lock):
        
        self.led_diplay_job_body = OimiStartProcess(name = 'led_diplay_job_body',
                                        sub_connection = self.sub_connection,
                                        global_status = self.global_status,
                                        parent_id = self.parent_id,
                                        execute = self.activateLED_body,
                                        daemon = False,
                                        ser = self.ser3,
                                        time_to_wait_bo = self.time_to_wait_bo)
        self.dispatcher_job_body = OimiStartProcess(name = 'dispatcher_job_body',
                                        sub_connection = self.sub_connection,
                                        global_status = self.global_status,
                                        parent_id = self.parent_id,
                                        execute = self.dispatcher_led_body,
                                        daemon = False,
                                        ser = self.ser3)
        self.led_diplay_job_base = OimiStartProcess(name = 'led_diplay_job_base',
                                        sub_connection = self.sub_connection,
                                        global_status = self.global_status,
                                        parent_id = self.parent_id,
                                        execute = self.activateLED_base,
                                        daemon = False,
                                        ser = self.ser2,
                                        time_to_wait_ba = self.time_to_wait_ba)
        self.dispatcher_job_base = OimiStartProcess(name = 'dispatcher_job_base',
                                        sub_connection = self.sub_connection,
                                        global_status = self.global_status,
                                        parent_id = self.parent_id,
                                        execute = self.dispatcher_led_base,
                                        daemon = False,
                                        ser = self.ser2)     

        self.play_audio_job = OimiStartProcess(name = 'play_audio_job',
                                        sub_connection = self.sub_connection,
                                        global_status = self.global_status,
                                        parent_id = self.parent_id,
                                        execute = self.play_first_on_queue,
                                        daemon = False)

        self.start_animation_job = OimiStartProcess(name = 'start_animation_job',
                                        sub_connection = self.sub_connection,
                                        global_status = self.global_status,
                                        parent_id = self.parent_id,
                                        execute = self.stage_start_animation,
                                        daemon = False)

        self.movement_pattern_job = OimiStartProcess(name = 'movement_pattern_job',
                                        sub_connection = self.sub_connection,
                                        global_status = self.global_status,
                                        parent_id = self.parent_id,
                                        execute = self.move_pattern,
                                        daemon = False,
                                        stopInterrupt = stopInterrupt,
                                        serMega = self.ser1,
                                        primary_lock = primary_lock,
                                        queue_lock = queue_lock)
    
        print("ok sono in stage_exhibition!!")
        self.led_diplay_job_body.start()
        self.dispatcher_job_body.start()
        self.led_diplay_job_base.start()
        self.dispatcher_job_base.start()
        self.start_animation_job.start()
        self.play_audio_job.start()
        self.movement_pattern_job.start()
        #time.sleep(2)
        self.coda.put(1)
        self.event_main.clear()
        
        
        print("comincio jointtttsss!")
        self.led_diplay_job_body.join()
        self.dispatcher_job_body.join()
        self.led_diplay_job_base.join()
        self.dispatcher_job_base.join() 
        self.start_animation_job.join()   
        self.play_audio_job.join()
        self.movement_pattern_job.join()
        
        #time.sleep(5)
        
        print("provo a chiudere!")
        print("provo a chiudere!")
        print("provo a chiudere!")
        print("provo a chiudere!")
        print("provo a chiudere!")
        print("provo a chiudere!")
        print("provo a chiudere!")
        print("provo a chiudere!")
        print("provo a chiudere!")
        #self.led_diplay_job_body.suspend()
        self.led_diplay_job_body.shutdown()
        self.dispatcher_job_body.shutdown()
        self.led_diplay_job_base.shutdown()
        self.dispatcher_job_base.shutdown() 
        self.start_animation_job.shutdown()
        self.play_audio_job.shutdown()
        self.movement_pattern_job.shutdown()

cdef class SubStage(Stage):
    def __cinit__(self, int id_stage, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3):
        self.id_stage = id_stage
        self.coda = coda
        self.event_main = event_main
        self.ser1 = ser1
        self.ser2 = ser2
        self.ser3 = ser3
        #self.initilize_substage() #extern not inside???
        self.stage_main(stopInterrupt, primary_lock, queue_lock) #da spostare giu???
    
        print("self.id_stage ricevuto all'inizio!!!!! =>>> ", self.id_stage)
    # -----------------------------------------------------------------------------
    # Getters, setters 
    # -----------------------------------------------------------------------------
    @property
    def subtype(self):
        return self.subtype
    @subtype.setter
    def subtype(self, value):
        self.subtype = value
    @property
    def time_to_wait_ba(self):
        return self.time_to_wait_ba
    @time_to_wait_ba.setter
    def time_to_wait_ba(self, value):
        self.time_to_wait_ba = value        
    @property
    def time_to_wait_bo(self):
        return self.time_to_wait_bo
    @time_to_wait_bo.setter
    def time_to_wait_bo(self, value):
        self.time_to_wait_bo = value        
    @property
    def stage_selected_LED_base(self):
        return self.stage_selected_LED_base
    @stage_selected_LED_base.setter
    def stage_selected_LED_base(self, value):
        self.stage_selected_LED_base = value        
    @property
    def stage_selected_LED_body(self):
        return self.stage_selected_LED_body
    @stage_selected_LED_body.setter
    def stage_selected_LED_body(self, value):
        self.stage_selected_LED_body = value      
    @property
    def stage_selected_movement(self):
        return self.stage_selected_movement
    @stage_selected_movement.setter
    def stage_selected_movement(self, value):
        self.stage_selected_movement = value      
    @property
    def stage_selected_audio(self):
        return self.stage_selected_audio
    @stage_selected_audio.setter
    def stage_selected_audio(self, value):
        self.stage_selected_audio = value
    @property
    def stage_reference(self):
        return self.stage_reference
    @stage_reference.setter
    def stage_reference(self, value):
        self.stage_reference = value  
    @property
    def stage_flow(self):
        return self.stage_flow
    @stage_flow.setter
    def stage_flow(self, value):
        self.stage_flow = value                                               

    cpdef set_substage_attr(self, command_led_1, command_led_2, movem_to_do, audio_in_question, time_ba, time_bo):
        print("enter in initialize_substage_without_indicating_the_color_and_pattern audio_in_question === ", audio_in_question)
        self.subtype=='fixed_display' #change!!

        self.stage_selected_LED_base = command_led_1
        self.stage_selected_LED_body = command_led_2
        self.stage_selected_movement = movem_to_do
        self.stage_selected_audio = audio_in_question
        self.time_to_wait_ba = time_ba
        self.time_to_wait_bo = time_bo

        print("stampo pochiiiiiipochiiiiiipochiiiiiipochiiiiii pochiiiiii")
        print("self.stage_selected_LED_base " , self.stage_selected_LED_base)
        print("self.stage_selected_LED_body " , self.stage_selected_LED_body)
        print("self.stage_selected_movement " , self.stage_selected_movement)
        print("self.stage_selected_audio " , self.stage_selected_audio)
        print("self.time_to_wait_ba " , self.time_to_wait_ba)
        print("self.time_to_wait_bo " , self.time_to_wait_bo)

    cpdef initialize_substage(self, oimidb):
        print("enter in initialize_substage")
        print("stavolta stavolta stavolta stavolta self.id_stage = ", self.id_stage)
        #oimidb = db.DatabaseManager() #culprit!!!!
        #with oimidb:
        subtype = oimidb.execute_param_query('''SELECT sub_name FROM Substages WHERE substage_id = ?''',(self.id_stage))
        print("subtype subtype ", subtype)
        print(subtype)
        self.subtype = subtype[0]
        print("self.subtype ", self.subtype)
        print("self.subtype ", self.subtype)
        print("self.subtype ", self.subtype)
        print("self.subtype ", self.subtype)
        print("self.subtype ", self.subtype)
        print("ok initialize_substage in corso....................")
        if 'subpositive' in self.subtype or 'subnegative' in self.subtype:
            self.category=='reaction'
            sort = oimidb.execute_param_query('''SELECT sort FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
            stage_reference = oimidb.execute_param_query('''SELECT stage_reference FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
            stage_flow = oimidb.execute_param_query('''SELECT stage_flow FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
            choice = random.randint(0,2)
            if choice==0:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_1 FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_1 FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_1 FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
            if choice==1:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_2 FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_2 FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_2 FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
            if choice==2:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_1 FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_2 FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_3 FROM Reaction_Substages WHERE substage_id = ?''',(self.id_stage))                

        elif 'emo_level' in self.subtype:
            self.category=='mood'
            sort = oimidb.execute_param_query('''SELECT sort FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
            stage_reference = oimidb.execute_param_query('''SELECT stage_reference FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
            stage_flow = oimidb.execute_param_query('''SELECT stage_flow FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
            
            choice = random.randint(0,2)
            if choice==0:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_1 FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_1 FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_1 FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
            if choice==1:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_2 FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_2 FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_2 FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
            if choice==2:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_2 FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_2 FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_3 FROM Mood_Substages WHERE substage_id = ?''',(self.id_stage))                

        elif 'extent' in self.subtype:
            self.category=='reaction'
            sort = oimidb.execute_param_query('''SELECT sort FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
            stage_reference = oimidb.execute_param_query('''SELECT stage_reference FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
            stage_flow = oimidb.execute_param_query('''SELECT stage_flow FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
            choice = random.randint(0,2)
            if choice==0:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_1 FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_1 FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_1 FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
            if choice==1:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_2 FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_2 FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_2 FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
            if choice==2:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_2 FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_2 FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_3 FROM Reward_Substages WHERE substage_id = ?''',(self.id_stage))                

        elif 'group' in self.subtype:
            self.category=='prompt'
            sort = oimidb.execute_param_query('''SELECT sort FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
            stage_reference = oimidb.execute_param_query('''SELECT stage_reference FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
            stage_flow = oimidb.execute_param_query('''SELECT stage_flow FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
            choice = random.randint(0,2)
            if choice==0:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_1 FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_1 FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_1 FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
            if choice==1:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_2 FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_2 FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_2 FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
            if choice==2:
                print("choice choicechoice choicechoice choicechoice choice === ", choice)
                stage_selected_LED_base = oimidb.execute_param_query('''SELECT stage_output_lights_base_2 FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_LED_body = oimidb.execute_param_query('''SELECT stage_output_lights_body_2 FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_movement = oimidb.execute_param_query('''SELECT stage_movement_nick FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
                stage_selected_audio = oimidb.execute_param_query('''SELECT stage_output_speeches_3 FROM Prompt_Substages WHERE substage_id = ?''',(self.id_stage))
        
        self.stage_reference = stage_reference[0]
        self.sort = sort[0]
        self.stage_flow = stage_flow[0]

        self.stage_selected_LED_base = stage_selected_LED_base[0]
        self.stage_selected_LED_body = stage_selected_LED_body[0]
        self.stage_selected_movement = stage_selected_movement[0]
        self.stage_selected_audio = stage_selected_audio[0]

        if 'specific' in self.subtype:
            #self.category=='custom' #create new category database?? 
            self.sort = 'custom'
            self.stage_reference = 'custom'
            self.stage_flow = 'custom'
            self.stage_selected_LED_base = 'wipe_red'
            self.stage_selected_LED_body = 'wipe_red'
            self.stage_selected_movement = 'movement_1'
            self.stage_selected_audio = 'audio_1'
            self.time_to_wait_ba = 5 
            self.time_to_wait_bo = 5

        print("stampo tuttiiiiiiiiiiiiiiiiiiiii tuttiiiiiiiiiiiiiiiiiiiii")
        print("self.stage_reference " , self.stage_reference)
        print("self.sort " , self.sort)
        print("self.stage_flow " , self.stage_flow)
        print("self.stage_selected_LED_base " , self.stage_selected_LED_base)
        print("self.stage_selected_LED_body " , self.stage_selected_LED_body)
        print("self.stage_selected_movement " , self.stage_selected_movement)
        print("self.stage_selected_audio " , self.stage_selected_audio)