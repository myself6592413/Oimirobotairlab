cdef class Stage:
	cdef:
		str style 
		int id_stage 
		str category
		str sort
		int time_to_wait_ba
		int time_to_wait_bo
		coda
		event_main
		#global_status
		#e
		event_startledanimation
		event_led_body
		event_led_bodybody
		event_startledanimation_base
		event_led_basebase
		event_led_base
		event_start_play_audio
		event_move
		#q
		led_command_body
		led_command_base
		right_audio_to_play
		led_diplay_job_body
		dispatcher_job_body
		led_diplay_job_base
		dispatcher_job_base
		start_animation_job
		play_audio_job
		movement_pattern_job
		parent_id
		#cancello poi tutto !!!
		#int num_of_matches
		#SessionDifficulty complexity
		#int extra_time
		#int desiderable_time
		#bint mvmnt_ok
		#bint body_ok
		#bint audio_slow_ok
		#bint audio_rules_ok
		#str led_brightness
		#bint exhort
		#bint repeat_ques
		#bint repeat_intro
		#str audio_s_intro
		#str audio_s_exit
		#mt.Match[:] matches_of_this_session
		#int current_match
		#list session_feedbacks
		#sc.Score session_scores
		#db_conn
		#list impositions_list
		#dict user_dict_sess
		#bint stop_or_not
		
		#new!!		
		ser1
		ser2
		ser3
		global_status
		sub_connection


cdef class SubStage(Stage):
	cdef:
		str stage_reference
		str subtype
		str stage_flow
		str stage_selected_LED_base
		str stage_selected_LED_body
		str stage_selected_movement
		str stage_selected_audio

	cpdef initialize_substage(self, oimidb)
	cpdef set_substage_attr(self, command_led_1, command_led_2, movem_to_do, audio_in_question, time_ba, time_bo)