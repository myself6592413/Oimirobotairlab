cdef class Feedback:
	cdef:
		db_conn
		str quantity_feedback_questions
		str grade_feedback_questions
		int value_feedback_questions
		str quantity_feedback_games
		str grade_feedback_games
		int value_feedback_games
		str quantity_feedback_matches
		str grade_feedback_matches
		int value_feedback_matches
		str quantity_feedback_session
		str grade_feedback_session
		int value_feedback_session
		
		kid_age
		kid_level
		every_question
		every_num_answers
		every_correct_ans
		every_errors_ans
		every_indecisions
		every_question_times
		all_values_questions
		every_question_kinds
		every_question_percentage_errs

		every_game_times
		every_expected_game_times
		every_difficulties_game
		every_quan_feedback_ques
		every_grade_feedback_ques
		every_val_feedback_ques
		every_time_percentage_game
		every_kind_games

		every_match_times
		every_expected_match_times
		every_difficulty_matches
		every_quan_feedback_game
		every_grade_feedback_game
		every_val_feedback_game
		every_time_percentage_match

		complex_ses
		time_ses
		expected_time_ses
		percentage_ses
		time_percentage_ses
		every_quan_feedback_match
		every_grade_feedback_match
		every_val_feedback_match

		the_session_feeds
		matches_feeds
		games_feeds
		questions_feeds
		
	cpdef takeout_feeds_question(self, int which_kid)
	
	cpdef takeout_feeds_game(self, int which_kid)
	cpdef takeout_feeds_match(self, int which_kid)
	cpdef takeout_feeds_session(self, int which_kid)
	
	cpdef evaluate_whole_feedbacks(self)
	
	cpdef feedbacks_insertion(self, int which_kid)
