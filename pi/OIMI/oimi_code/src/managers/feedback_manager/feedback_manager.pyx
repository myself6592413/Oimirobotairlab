# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/')
from typing import Optional
import database_manager as db
cimport fuzzy_controller_takeout as fct
cimport fuzzy_controller_evaluate_feedbacks_whole_session as fcefws
cimport fuzzy_controller_evaluate_question as fceq
cimport fuzzy_controller_evaluate_match as fcem
cimport fuzzy_controller_evaluate_game as fceg
cimport fuzzy_controller_insert as fci
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class Feedback:
	def __cinit__(self):
		self.db_conn = db.DatabaseManager()
		self.every_question = []
		self.every_num_answers = []
		self.every_correct_ans = []
		self.every_errors_ans = []
		self.every_indecisions = []
		self.all_values_questions = []
		self.every_question_times = []
		self.every_question_kinds = []
		self.every_game_times = []
		self.every_difficulties_game = []
		self.every_val_feedback_ques = []
		self.every_quan_feedback_ques = []
		self.every_grade_feedback_ques = []
		self.every_match_times = []
		self.every_difficulty_matches = []
		self.every_quan_feedback_game = []
		self.every_grade_feedback_game = []
		self.every_val_feedback_game = []
		self.every_quan_feedback_match = []
		self.every_grade_feedback_match = []
		self.every_val_feedback_match = []
		self.every_question_percentage_errs = []
		self.every_time_percentage_game = []
		self.every_time_percentage_match = []
		self.every_expected_game_times = []
		self.every_expected_match_times = []
		self.the_session_feeds = []
		self.matches_feeds = []
		self.games_feeds = []
		self.questions_feeds = []
	def __enter__(self):
		print("Creating Feeds...")

	def __exit__(self, exc_type, exc_val, exc_tb):
		print("All feedbacks were created successfully.")

	def __eq__(self, other): 
		return self.quantity_feedback_questions==other.quantity_feedback_questions \
		and self.grade_feedback_questions==other.grade_feedback_questions \
		and self.value_feedback_questions==other.value_feedback_questions \
		and self.quantity_feedback_games==other.quantity_feedback_games \
		and self.grade_feedback_games==other.grade_feedback_games \
		and self.value_feedback_games==other.value_feedback_games \
		and self.quantity_feedback_matches==other.quantity_feedback_matches \
		and self.grade_feedback_matches==other.grade_feedback_matches \
		and self.value_feedback_matches==other.value_feedback_matches \
		and self.quantity_feedback_matches==other.quantity_feedback_matches \
		and self.grade_feedback_matches==other.grade_feedback_matches \
		and self.value_feedback_matches==other.value_feedback_matches \
		and self.quantity_feedback_session==other.quantity_feedback_session \
		and self.grade_feedback_session==other.grade_feedback_session \
		and self.value_feedback_session==other.value_feedback_session 
	# -----------------------------------------------------------------------------
	# Getters, setters 
	# -----------------------------------------------------------------------------	
	@property
	def quantity_feedback_questions(self):
		return self.quantity_feedback_questions
	@quantity_feedback_questions.setter
	def quantity_feedback_questions(self, value):
		self.quantity_feedback_questions = value
	@property
	def quantity_feedback_games(self):
		return self.quantity_feedback_games
	@quantity_feedback_games.setter
	def quantity_feedback_games(self, value):
		self.quantity_feedback_games = value
	@property
	def quantity_feedback_matches(self):
		return self.quantity_feedback_matches
	@quantity_feedback_matches.setter
	def quantity_feedback_matches(self, value):
		self.quantity_feedback_matches = value
	@property
	def quantity_feedback_session(self):
		return self.quantity_feedback_session
	@quantity_feedback_session.setter
	def quantity_feedback_session(self, value):
		self.quantity_feedback_session = value
	@property
	def grade_feedback_questions(self):
		return self.grade_feedback_questions
	@grade_feedback_questions.setter
	def grade_feedback_questions(self, value):
		self.grade_feedback_questions = value
	@property
	def grade_feedback_games(self):
		return self.grade_feedback_games
	@grade_feedback_games.setter
	def grade_feedback_games(self, value):
		self.grade_feedback_games = value
	@property
	def grade_feedback_matches(self):
		return self.grade_feedback_matches
	@grade_feedback_matches.setter
	def grade_feedback_matches(self, value):
		self.grade_feedback_matches = value
	@property
	def grade_feedback_session(self):
		return self.grade_feedback_session
	@grade_feedback_session.setter
	def grade_feedback_session(self, value):
		self.grade_feedback_session = value
	@property
	def value_feedback_questions(self):
		return self.value_feedback_questions
	@value_feedback_questions.setter
	def value_feedback_questions(self, value):
		self.value_feedback_questions = value
	@property
	def value_feedback_games(self):
		return self.value_feedback_games
	@value_feedback_games.setter
	def value_feedback_games(self, value):
		self.value_feedback_games = value
	@property
	def value_feedback_matches(self):
		return self.value_feedback_matches
	@value_feedback_matches.setter
	def value_feedback_matches(self, value):
		self.value_feedback_matches = value
	@property
	def value_feedback_session(self):
		return self.value_feedback_session
	@value_feedback_session.setter
	def value_feedback_session(self, value):
		self.value_feedback_session = value
	@property
	def db_conn(self):
		return self.db_conn
	@db_conn.setter
	def db_conn(self, value):
		self.db_conn = value

	@property
	def kid_age(self):
		return self.kid_age
	@kid_age.setter
	def kid_age(self, value):
		self.kid_age = value
	@property
	def kid_level(self):
		return self.kid_level
	@kid_level.setter
	def kid_level(self, value):
		self.kid_level = value
	@property
	def every_question(self):
		return self.every_question
	@every_question.setter
	def every_question(self, value):
		self.every_question = value
	@property
	def every_num_answers(self):
		return self.every_num_answers
	@every_num_answers.setter
	def every_num_answers(self, value):
		self.every_num_answers = value
	@property
	def every_correct_ans(self):
		return self.every_correct_ans
	@every_correct_ans.setter
	def every_correct_ans(self, value):
		self.every_correct_ans = value
	@property
	def every_errors_ans(self):
		return self.every_errors_ans
	@every_errors_ans.setter
	def every_errors_ans(self, value):
		self.every_errors_ans = value
	@property
	def every_indecisions(self):
		return self.every_indecisions
	@every_indecisions.setter
	def every_indecisions(self, value):
		self.every_indecisions = value
	@property
	def every_question_times(self):
		return self.every_question_times
	@every_question_times.setter
	def every_question_times(self, value):
		self.every_question_times = value
	@property
	def all_values_questions(self):
		return self.all_values_questions
	@all_values_questions.setter
	def all_values_questions(self, value):
		self.all_values_questions = value
	@property
	def every_game_times(self):
		return self.every_game_times
	@every_game_times.setter
	def every_game_times(self, value):
		self.every_game_times = value
	@property
	def every_difficulties_game(self):
		return self.every_difficulties_game
	@every_difficulties_game.setter
	def every_difficulties_game(self, value):
		self.every_difficulties_game = value	
	@property
	def every_quan_feedback_ques(self):
		return self.every_quan_feedback_ques
	@every_quan_feedback_ques.setter
	def every_quan_feedback_ques(self, value):
		self.every_quan_feedback_ques = value
	@property
	def every_grade_feedback_ques(self):
		return self.every_grade_feedback_ques
	@every_grade_feedback_ques.setter
	def every_grade_feedback_ques(self, value):
		self.every_grade_feedback_ques = value	
	@property
	def every_val_feedback_ques(self):
		return self.every_val_feedback_ques
	@every_val_feedback_ques.setter
	def every_val_feedback_ques(self, value):
		self.every_val_feedback_ques = value
	@property
	def every_match_times(self):
		return self.every_match_times
	@every_match_times.setter
	def every_match_times(self, value):
		self.every_match_times = value
	@property
	def every_difficulty_matches(self):
		return self.every_difficulty_matches
	@every_difficulty_matches.setter
	def every_difficulty_matches(self, value):
		self.every_difficulty_matches = value
	@property
	def every_quan_feedback_game(self):
		return self.every_quan_feedback_game
	@every_quan_feedback_game.setter
	def every_quan_feedback_game(self, value):
		self.every_quan_feedback_game = value
	@property
	def every_grade_feedback_game(self):
		return self.every_grade_feedback_game
	@every_grade_feedback_game.setter
	def every_grade_feedback_game(self, value):
		self.every_grade_feedback_game = value
	@property
	def every_val_feedback_game(self):
		return self.every_val_feedback_game
	@every_val_feedback_game.setter
	def every_val_feedback_game(self, value):
		self.every_val_feedback_game = value
	@property
	def complex_ses(self):
		return self.complex_ses
	@complex_ses.setter
	def complex_ses(self, value):
		self.complex_ses = value
	@property
	def time_ses(self):
		return self.time_ses
	@time_ses.setter
	def time_ses(self, value):
		self.time_ses = value
	@property
	def time_percentage_ses(self):
		return self.time_percentage_ses
	@time_percentage_ses.setter
	def time_percentage_ses(self, value):
		self.time_percentage_ses = value
	@property
	def expected_time_ses(self):
		return self.expected_time_ses
	@expected_time_ses.setter
	def expected_time_ses(self, value):
		self.expected_time_ses = value
	@property
	def every_quan_feedback_match(self):
		return self.every_quan_feedback_match
	@every_quan_feedback_match.setter
	def every_quan_feedback_match(self, value):
		self.every_quan_feedback_match = value
	@property
	def every_grade_feedback_match(self):
		return self.every_grade_feedback_match
	@every_grade_feedback_match.setter
	def every_grade_feedback_match(self, value):
		self.every_grade_feedback_match = value
	@property
	def every_val_feedback_match(self):
		return self.every_val_feedback_match
	@every_val_feedback_match.setter
	def every_val_feedback_match(self, value):
		self.every_val_feedback_match = value								
	@property
	def every_question_kinds(self):
		return self.every_question_kinds
	@every_question_kinds.setter
	def every_question_kinds(self, value):
		self.every_question_kinds = value	
	@property
	def every_question_percentage_errs(self):
		return self.every_question_percentage_errs
	@every_question_percentage_errs.setter
	def every_question_percentage_errs(self, value):
		self.every_question_percentage_errs = value	
	@property
	def every_time_percentage_game(self):
		return self.every_time_percentage_game
	@every_time_percentage_game.setter
	def every_time_percentage_game(self, value):
		self.every_time_percentage_game = value	
	@property
	def every_time_percentage_match(self):
		return self.every_time_percentage_match
	@every_time_percentage_match.setter
	def every_time_percentage_match(self, value):
		self.every_time_percentage_match = value	
	@property
	def percentage_ses(self):
		return self.percentage_ses
	@percentage_ses.setter
	def percentage_ses(self, value):
		self.percentage_ses = value
	@property
	def every_expected_match_times(self):
		return self.every_expected_match_times
	@every_expected_match_times.setter
	def every_expected_match_times(self, value):
		self.every_expected_match_times = value	
	@property
	def every_expected_game_times(self):
		return self.every_expected_game_times
	@every_expected_game_times.setter
	def every_expected_game_times(self, value):
		self.every_expected_game_times = value			
	@property
	def every_kind_games(self):
		return self.every_kind_games
	@every_kind_games.setter
	def every_kind_games(self, value):
		self.every_kind_games = value
	@property
	def questions_feeds(self):
		return self.questions_feeds
	@questions_feeds.setter
	def questions_feeds(self, value):
		self.questions_feeds = value
	@property
	def matches_feeds(self):
		return self.matches_feeds
	@matches_feeds.setter
	def matches_feeds(self, value):
		self.matches_feeds = value
	@property
	def games_feeds(self):
		return self.games_feeds
	@games_feeds.setter
	def games_feeds(self, value):
		self.games_feeds = value
	@property
	def the_session_feeds(self):
		return self.the_session_feeds
	@the_session_feeds.setter
	def the_session_feeds(self, value):
		self.the_session_feeds = value								
	# ==========================================================================================================================================================
	# Methods
	# ==========================================================================================================================================================
	cpdef takeout_feeds_question(self, int which_kid):
		fct.takeout_feeds_from_db_question(self,which_kid)

	cpdef takeout_feeds_game(self, int which_kid):
		fct.takeout_feeds_from_db_game(self,which_kid)
	cpdef takeout_feeds_match(self, int which_kid):
		fct.takeout_feeds_from_db_match(self,which_kid)
	cpdef takeout_feeds_session(self, int which_kid):
		fct.takeout_feeds_from_db_session(self,which_kid)

	cpdef evaluate_whole_feedbacks(self):
		fcefws.evaluate_feedbacks_whole_session(self)
	
	cpdef feedbacks_insertion(self, int which_kid):
		s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
		this_session = self.db_conn.execute_new_query(s_sql,which_kid)
		session_id = this_session[0]
		print("questions_feeds ", self.questions_feeds)
		print("the_session_feeds ", self.the_session_feeds)
		print("matches_feeds ", self.matches_feeds)
		print("games_feeds ", self.games_feeds)
		self.db_conn.include_all_feeds_in_once_given_session(which_kid, session_id, self.questions_feeds, self.the_session_feeds, self.matches_feeds, self.games_feeds)