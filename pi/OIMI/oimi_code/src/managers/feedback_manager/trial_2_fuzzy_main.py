import sys
if '/home/pi/OIMI/oimi_code/src/managers/feedback_manager' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/feedback_manager/feedback_manager') 
import feedback_manager as fm
import database_manager as db

if __name__=="__main__":
	feed = fm.Feedback()
	with fm.Feedback():
		kid = 1001
		feed.takeout_feeds_question(kid)
		feed.takeout_feeds_game(kid)
		feed.takeout_feeds_match(kid)
		feed.takeout_feeds_session(kid)
		feed.evaluate_whole_feedbacks()
		feed.feedbacks_insertion(kid)