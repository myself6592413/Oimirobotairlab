Oimi fuzzy system recap: Details can be found in the specific method code comments

Main modules for defining a generic fuzzy system for Oimi scores:
	_variables = creation of the FuzzyVariable  Sugeno type
	_rules = defines concept of rules and FuzzyCondition
	_terms = specific obect membership function
	_rule_parser = defines keywords, lexems, tokens, expressions, parser for all terms/rules in strings
	_mf = definition of all available classes of membership functions (shapes = singleton, triangular, trepezoidal. composite)
	_generic_fs = oimi fuzzy model definition GenericFuzzySystem with evaluate_condition methods (base class)
	_mamdani_fs = oimi fuzzy mamdani model where is specified the flow of basic operations (implication, aggragation, fuzzification, defuzzification ) (childern class)
	_f_types = creation of all possible methods of operation in the fuzzy system as classes ..or/and/implication/aggragation/defuzz methods...

Specific Oimi fuzzy modules:
	# To insert and extract from DB results
	feedback_manager.pyx 
	call the modules in fuzzy_controller folder:
		--> fuzzy_controller: where results are calculated at runtime 
	
	# To create specific mamdani function for inputs and ouputs ; define also specific rules 
	session_fuzzy_administrator.py
	match_fuzzy_administrator.py
	game_fuzzy_administrator.py
	question_fuzzy_administrator.py
Rules list containers:
	#sessions:
		rules_to_use_1_session_fuzzy_1 ..2..3..4
	#matches:
		rules_to_use_1_matches_fuzzy_1 ..2..3..4
	#games:
		rules_to_use_1_games_fuzzy_1 ..2..3..4
	#questions:
		rules_to_use_1_fuzzy_1
		rules_to_use_1_fuzzy_2
		rules_to_use_1_fuzzy_3
		rules_to_use_1_fuzzy_4

To plot the membership funct with matplotlib:
	plotting_definitive_result_session
	plotting_definitive_result_match
	plotting_definitive_result_game
	plotting_definitive_result_question

images_input_outputs FOLDER contains the plotted images in various phases 
##########################################################################
Usage: (to prepare the code to be used in the games_oimi methods at runtime)
build code with setup
	$. home/pi/oimi-venv/bin/activate
	$python setup_for_fuzzy_sys.py build_ext --inplace

MAIN METHODS TO CREATE AND CALC FEEDBACKs:
	- main_fuzzy_output_is_result_games
	- main_fuzzy_output_is_result_match
	- main_fuzzy_output_is_result_question
	- main_fuzzy_output_is_result_session
	with same methods as administrator files

Just for testing during coding phase:
	trials_1_fuzzy_main.py
	trials_2_fuzzy_main.py 

Created by Colombo Giacomo, Politecnico di Milano 2021
