# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/feedback_manager/fuzzy_logic' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/feedback_manager/fuzzy_logic')
from pprint import pprint
from terms import Term
from variables import FuzzyVariable
from mamdani_fs import MamdaniFuzzySystem
from mf import TriangularMF, TrapezoidMF, ConstantMF
import matplotlib.pyplot as plt
import numpy as np
import session_fuzzy_administrator as sfa
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef evaluate_feeds_session(how_many, selected_feed_matches, selected_session_complexity, selected_time_perc_sess):
	if selected_session_complexity[0]=='Easy':
		sel_session_compl = 1
	elif selected_session_complexity[0]=='Normal':
		sel_session_compl = 2
	elif selected_session_complexity[0]=='Medium':
		sel_session_compl = 3
	elif selected_session_complexity[0]=='Hard':
		sel_session_compl = 4
	if len(selected_feed_matches)==1:
		selected_feedback_match1 = selected_feed_matches[0]
		mamf = sfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({sfa.input1_feedback_match1: selected_feedback_match1, 
			sfa.input5_session_complexity: sel_session_compl, 
			sfa.input6_time_perc: selected_time_perc_sess})

	if len(selected_feed_matches)==2:
		selected_feedback_match1 = selected_feed_matches[0]
		selected_feedback_match2 = selected_feed_matches[1]
		mamf = sfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({sfa.input1_feedback_match1: selected_feedback_match1, 
			sfa.input2_feedback_match2: selected_feedback_match2, 
			sfa.input5_session_complexity: sel_session_compl, 
			sfa.input6_time_perc: selected_time_perc_sess})

	if len(selected_feed_matches)==3:
		selected_feedback_match1 = selected_feed_matches[0]
		selected_feedback_match2 = selected_feed_matches[1]
		selected_feedback_match3 = selected_feed_matches[2]
		mamf = sfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({sfa.input1_feedback_match1: selected_feedback_match1, 
			sfa.input2_feedback_match2: selected_feedback_match2, 
			sfa.input3_feedback_match3: selected_feedback_match3, 
			sfa.input5_session_complexity: sel_session_compl, 
			sfa.input6_time_perc: selected_time_perc_sess})

	if len(selected_feed_matches)==4:
		selected_feedback_match1 = selected_feed_matches[0]
		selected_feedback_match2 = selected_feed_matches[1]
		selected_feedback_match3 = selected_feed_matches[2]
		selected_feedback_match4 = selected_feed_matches[3]
		mamf = sfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({sfa.input1_feedback_match1: selected_feedback_match1, 
			sfa.input2_feedback_match2: selected_feedback_match2, 
			sfa.input3_feedback_match3: selected_feedback_match3, 
			sfa.input4_feedback_match4: selected_feedback_match4, 
			sfa.input5_session_complexity: sel_session_compl, 
			sfa.input6_time_perc: selected_time_perc_sess})

	out_val = [result[k] for k in result]
	value_output = out_val[0]
	print("value_output session ===========> ", value_output)
	gen_name_output = []
	name_output = []
	quantity_output = []
	if 0 < value_output <= 0.2:
		gen_name_output.append('Awful')
	if 0.2 < value_output <= 0.5:
		gen_name_output.append('Bad')
	if 0.3 < value_output <= 0.7:
		gen_name_output.append('Good')
	if 0.6 < value_output <= 0.8:
		gen_name_output.append('Excellent')
	if value_output > 0.8:
		gen_name_output.append('Perfect')
	if 0 < value_output <= 0.08:
		quantity_output.append('heavily')
		name_output.append('Awful')
	if 0.08 < value_output <= 0.15:
		quantity_output.append('precisely')
		name_output.append('Awful')
	if 0.15 < value_output <= 0.2:
		quantity_output.append('marginally')
		name_output.append('Awful')
	if 0.1 < value_output <= 0.2:
		quantity_output.append('heavily')
		name_output.append('Bad')
	if 0.2 < value_output <= 0.3:
		quantity_output.append('precisely')
		name_output.append('Bad')
	if 0.3 < value_output <= 0.4:
		quantity_output.append('marginally')
		name_output.append('Bad')
	if 0.3 < value_output <= 0.4:
		quantity_output.append('marginally')
		name_output.append('Good')
	if 0.4 < value_output <= 0.6:
		quantity_output.append('precisely')
		name_output.append('Good')
	if 0.6 < value_output <= 0.7:
		quantity_output.append('heavily')
		name_output.append('Good')
	if 0.6 < value_output <= 0.7:
		quantity_output.append('marginally')
		name_output.append('Excellent')
	if 0.7 < value_output <= 0.8:
		quantity_output.append('precisely')
		name_output.append('Excellent')
	if 0.8 < value_output <= 0.9:
		quantity_output.append('heavily')    
		name_output.append('Excellent')
	if 0.8 < value_output <= 0.85:
		quantity_output.append('marginally')
		name_output.append('Perfect')
	if 0.85 < value_output <= 0.9:
		quantity_output.append('precisely')
		name_output.append('Perfect')
	if 0.9 < value_output <= 1:
		quantity_output.append('heavily')
		name_output.append('Perfect')

	print(name_output)
	print(quantity_output)

	total = 11 
	if total > 9:
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[0]
			del quantity_output[0]
	print(quantity_output)
	print(name_output)

	return quantity_output,name_output,value_output

