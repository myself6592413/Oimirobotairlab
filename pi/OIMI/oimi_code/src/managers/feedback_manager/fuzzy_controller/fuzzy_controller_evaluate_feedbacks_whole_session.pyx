""" Principal module to calculate fuuzzy results of whole session, it calls single controller_evaluation """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os
from copy import copy
cimport fuzzy_controller_evaluate_question as fceq
cimport fuzzy_controller_evaluate_game as fceg
cimport fuzzy_controller_evaluate_match as fcem
cimport fuzzy_controller_evaluate_session as fces
import fuzzy_controller_takeout as fct
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cpdef evaluate_feedbacks_whole_session(feed_self):
	#print("every_question")
	#print(feed_self.every_question)
	#print("every_num_answers")
	#print(feed_self.every_num_answers)
	#print("every_correct_ans")
	#print(feed_self.every_correct_ans)
	#print("every_errors_ans")
	#print(feed_self.every_errors_ans)
	#print("every_indecisions")
	#print(feed_self.every_indecisions)
	#print("every_question_times")
	#print(feed_self.every_question_times)
	#print("every_question_kinds")
	#print(feed_self.every_question_kinds)
	#print("every_question_percentage_errs")
	#print(feed_self.every_question_percentage_errs)
	the_first,the_second,the_third,the_fourth = [],[],[],[]
	prima_game,seconda_game,terza_game,quarta_game = [],[],[],[]
	prima_match,seconda_match,terza_match,quarta_match = [],[],[],[]
	first_to_take,first_to_take1,first_to_take2,first_to_take3 = [],[],[],[]
	second_to_take,second_to_take1,second_to_take2,second_to_take3 = [],[],[],[]
	third_to_take,third_to_take1,third_to_take2,third_to_take3 = [],[],[],[]
	fourth_to_take,fourth_to_take1,fourth_to_take2,fourth_to_take3 = [],[],[],[]
	first_to_take_game,first_to_take_game1,first_to_take_game2,first_to_take_game3 = [],[],[],[]
	second_to_take_game,second_to_take_game1,second_to_take_game2,second_to_take_game3 = [],[],[],[]
	third_to_take_game,third_to_take_game1,third_to_take_game2,third_to_take_game3 = [],[],[],[]
	fourth_to_take_game,fourth_to_take_game1,fourth_to_take_game2,fourth_to_take_game3 = [],[],[],[]
	first_to_take_match,first_to_take_match1,first_to_take_match2,first_to_take_match3 = [],[],[],[]
	second_to_take_match,second_to_take_match1,second_to_take_match2,second_to_take_match3 = [],[],[],[]
	third_to_take_match,third_to_take_match1,third_to_take_match2,third_to_take_match3 = [],[],[],[]
	fourth_to_take_match,fourth_to_take_match1,fourth_to_take_match2,fourth_to_take_match3 = [],[],[],[]
	how_many_all_ques_totale = []
	total_value_feedbacks_for_game = []
	supertotal_final_grade_tot_ques = []
	how_many_all_games_totale = []
	total_value_feedbacks_for_match = []
	supertotal_final_grade_tot_game = []
	TOTAL_matches_final_grade = []
	TOTAL_matches_values_feedbacks = []
	TOTAL_matches_how_many_matches = []
	total_mat = []
	all_total = []
	total_mat_2 = []
	tottotal_2 = []
	total_mat_3 = []
	supertotal_3 = []
	all_err_perc = feed_self.every_question_percentage_errs
	all_indeci = feed_self.every_indecisions
	all_val = feed_self.all_values_questions
	all_que = feed_self.every_question_times
	print("start all_err_perc = ", all_err_perc)
	print("start all_indeci = ", all_indeci)
	print("start all_val = ", all_val)
	print("start all_que = ", all_que)
	zipped_inputs = list(zip(all_err_perc,all_indeci,all_val,all_que))
	print(zipped_inputs)
	value_feedbacks_TOTALI_game = []
	for hh in range(len(zipped_inputs)):
		print("enter into primo match h = ", hh)
		for i in range(hh,len(zipped_inputs)):
			print("loop i {}".format(i))
			print("zipped_inputs[i] ==> ", zipped_inputs[i])
			for k in range(len(zipped_inputs[i])):
				print("loop k {}".format(k))
				print("zipped_inputs[i][k] ==> ", zipped_inputs[i][k])
				for j in range(len(zipped_inputs[i][k])):
					p=j
					while p < len(zipped_inputs[i][k]):
						print("loop p {}".format(p))
						p+=1    
						if j==0:
							print("loop j {}".format(j))
							for z in range(len(zipped_inputs[i][k][j])):
								print("loop z {}".format(z))
								print("zipped_inputs[i][k][j][z] is ",zipped_inputs[i][k][j][z])
								try:
									first_to_take.append(zipped_inputs[i][k][j][z])
								except:
									pass
								try:
									second_to_take.append(zipped_inputs[i][k][j][z+1])
								except:
									pass
								try:
									third_to_take.append(zipped_inputs[i][k][j][z+2])
								except:
									pass
								try:
									fourth_to_take.append(zipped_inputs[i][k][j][z+3])
								except:
									pass
								break
							print("j is ", j)    
							break
						if j==1:
							print("loop j {}".format(j))
							for z in range(len(zipped_inputs[i][k][j])):
								print("loop z {}".format(z))
								print(zipped_inputs[i][k][j][z])
								try:
									first_to_take1.append(zipped_inputs[i][k][j][z])
								except:
									pass
								try:
									second_to_take1.append(zipped_inputs[i][k][j][z+1])
								except:
									pass
								try:
									third_to_take1.append(zipped_inputs[i][k][j][z+2])
								except:
									pass
								try:
									fourth_to_take1.append(zipped_inputs[i][k][j][z+3])
								except:
									pass
								break
							print("j is ", j)    
							break
						if j==2:
							print("loop j {}".format(j))
							for z in range(len(zipped_inputs[i][k][j])):
								print("loop z {}".format(z))
								print(zipped_inputs[i][k][j][z])
								try:
									first_to_take2.append(zipped_inputs[i][k][j][z])
								except:
									pass
								try:
									second_to_take2.append(zipped_inputs[i][k][j][z+1])
								except:
									pass
								try:
									third_to_take2.append(zipped_inputs[i][k][j][z+2])
								except:
									pass
								try:
									fourth_to_take2.append(zipped_inputs[i][k][j][z+3])
								except:
									pass
								break
							print("j is ", j)    
							break
						if j==3:
							print("loop j {}".format(j))
							for z in range(len(zipped_inputs[i][k][j])):
								print("loop z {}".format(z))
								print(zipped_inputs[i][k][j][z])
								try:
									first_to_take3.append(zipped_inputs[i][k][j][z])
								except:
									pass
								try:
									second_to_take3.append(zipped_inputs[i][k][j][z+1])
								except:
									pass
								try:
									third_to_take3.append(zipped_inputs[i][k][j][z+2])
								except:
									pass
								try:
									fourth_to_take3.append(zipped_inputs[i][k][j][z+3])
								except:
									pass
								break
							print("j is ", j)    
							break
			break
		the_first.append(first_to_take)
		the_first.append(second_to_take)
		the_first.append(third_to_take)
		the_first.append(fourth_to_take)
		the_second.append(first_to_take1)
		the_second.append(second_to_take1)
		the_second.append(third_to_take1)
		the_second.append(fourth_to_take1)
		the_third.append(first_to_take2)
		the_third.append(second_to_take2)
		the_third.append(third_to_take2)
		the_third.append(fourth_to_take2)
		the_fourth.append(first_to_take3)
		the_fourth.append(second_to_take3)
		the_fourth.append(third_to_take3)
		the_fourth.append(fourth_to_take3)
		the_first = list(filter(None, the_first))
		print("the_first AFTER {}".format(the_first))
		the_second = list(filter(None, the_second))
		print("the_second AFTER {}".format(the_second))
		the_third = list(filter(None, the_third))
		print("the_third AFTER {}".format(the_third))
		the_fourth = list(filter(None, the_fourth))
		print("the_fourth AFTER {}".format(the_fourth))
		total_mat.append(the_first)
		total_mat.append(the_second)
		total_mat.append(the_third)
		total_mat.append(the_fourth)
		total_mat = list(filter(None, total_mat))
		print(total_mat)
		all_total.append(total_mat)
		print(all_total)
		y, i = 0,0
		final_grade = []
		final_grade_all_Games = []
		res_part = []
		############# the_first 
		#################the_first QUES
		selected_kid_age = feed_self.kid_age
		selected_kid_level = feed_self.kid_level
		selected_interruptions = 0
		###################### evaluate feedback all questions first game
		final_grade_tot_ques  = []
		print("##### comincio #####")
		print("##### comincio #####")
		print("##### comincio #####")
		print("##### comincio #####")
		print("##### comincio #####")
		print("##### comincio #####")
		if len(the_first)>0:
			for z in range(len(the_first)):
				print("the_first[z] ", the_first[z])
				for i in range(len(the_first[z])-3):
					print(the_first[z][y])
					selected_correct_ans = the_first[z][y]
					selected_indecisions = the_first[z][y+1]
					selected_value_question  = the_first[z][y+2]
					selected_time_spent  = the_first[z][y+3]
					print("loop num z ",z)
					print("loop num i ",i)
					print("###### SELECT ######")
					print("selected_correct_ans")
					print(selected_correct_ans)
					print("selected_indecisions")
					print(selected_indecisions)
					print("selected_value_question")
					print(selected_value_question)
					print("selected_time_spent")
					print(selected_time_spent)
					print("###### SELECT ######")
					res,fres,conc = fceq.evaluate_feeds_question(selected_kid_age, selected_kid_level, selected_value_question, selected_time_spent, selected_correct_ans, selected_indecisions, selected_interruptions)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part.append(res[0])
					res_part.append(fres[0])
					res_part.append(conc)
				final_grade.append(res_part)
				res_part = []
			print("final_grade middle = ", final_grade)
			final_grade_tot_ques.append(final_grade)
		if len(the_second)>0:
			print("len the_second c'è eccome")
			res_part = []
			final_grade = []
			final_grade_2 = []
			for z in range(len(the_second)):
				print("the_second[z] ", the_second[z])
				for i in range(len(the_second[z])-3):
					print(the_second[z][y])
					selected_correct_ans = the_second[z][y]
					selected_indecisions = the_second[z][y+1]
					selected_value_question  = the_second[z][y+2]
					selected_time_spent  = the_second[z][y+3]
					print("loop num z ",z)
					print("loop num i ",i)
					print("###### SELECT 2 the_second ######")
					print("selected_correct_ans 2")
					print(selected_correct_ans)
					print("selected_indecisions 2")
					print(selected_indecisions)
					print("selected_value_question 2")
					print(selected_value_question)
					print("selected_time_spent 2")
					print(selected_time_spent)
					print("###### SELECT 2 the_second ######")
					res,fres,conc = fceq.evaluate_feeds_question(selected_kid_age, selected_kid_level, selected_value_question, selected_time_spent, selected_correct_ans, selected_indecisions, selected_interruptions)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part.append(res[0])
					res_part.append(fres[0])
					res_part.append(conc)
				final_grade.append(res_part)
				res_part = []
			print("BEFORE final grade question!!!")
			print(final_grade) 
			final_grade_tot_ques.append(final_grade)
			print("final grade middle 2 ",final_grade_tot_ques)
		if len(the_third)>0:
			print("len the_third c'è eccome")
			res_part = []
			final_grade = []
			final_grade_2 = []
			for z in range(len(the_third)):
				print("the_third[z] ", the_third[z])
				for i in range(len(the_third[z])-3):
					print(the_third[z][y])
					selected_correct_ans = the_third[z][y]
					selected_indecisions = the_third[z][y+1]
					selected_value_question  = the_third[z][y+2]
					selected_time_spent  = the_third[z][y+3]
					print("loop num z ",z)
					print("###### SELECT 3 the_third ######")
					print("selected_correct_ans 2 ", selected_correct_ans)
					print("selected_indecisions 2 ", selected_indecisions)
					print("selected_value_question 2 ", selected_value_question)
					print("selected_time_spent 2", selected_time_spent)
					print("###### SELECT 3 the_third ######")
					res, fres, conc = fceq.evaluate_feeds_question(selected_kid_age, selected_kid_level, selected_value_question, 
						selected_time_spent, selected_correct_ans, selected_indecisions, selected_interruptions)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part.append(res[0])
					res_part.append(fres[0])
					res_part.append(conc)
				final_grade.append(res_part)
				res_part = []
			final_grade_tot_ques.append(final_grade)
			print(" tre middle ",final_grade_tot_ques)
		if len(the_fourth)>0:
			print("len the_fourth c'è eccome")
			res_part = []
			final_grade = []
			final_grade_2 = []
			for z in range(len(the_fourth)):
				print("the_fourth[z] ", the_fourth[z])
				for i in range(len(the_fourth[z])-3):
					print(the_fourth[z][y])
					selected_correct_ans = the_fourth[z][y]
					selected_indecisions = the_fourth[z][y+1]
					selected_value_question  = the_fourth[z][y+2]
					selected_time_spent  = the_fourth[z][y+3]
					print("loop num z ",z)
					print("###### SELECT 4 the_fourth ######")
					print("selected_correct_ans 2 ", selected_correct_ans)
					print("selected_indecisions 2", selected_indecisions)
					print("selected_value_question 2", selected_value_question)
					print("selected_time_spent 2", selected_time_spent)
					print("###### SELECT 4 the_fourth ######")
					res,fres,conc = fceq.evaluate_feeds_question(selected_kid_age, selected_kid_level, selected_value_question, 
						selected_time_spent, selected_correct_ans, selected_indecisions, selected_interruptions)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part.append(res[0])
					res_part.append(fres[0])
					res_part.append(conc)
				final_grade.append(res_part)
				res_part = []
			final_grade_tot_ques.append(final_grade)
			print(" quattro middle e finale ",final_grade_tot_ques)
		print("final_grade_tot_ques ",final_grade_tot_ques)
		############ before ===> the GAME preparation game 1
		value_feedbacks = [] 
		v1 = []
		
		selected_feed_questions = [] #to pass to fgec.... game!
		final_grade_tot_ques = list(filter(None, final_grade_tot_ques))
		print("AFTER remove [] and none .... final_grade question ",final_grade_tot_ques)
		for f in final_grade_tot_ques:
			print("what is f? ", f)
		how_many_all_ques = []
		for f in final_grade_tot_ques:
			how_many_ques = len(f)
			how_many_all_ques.append(how_many_ques)
			for l in f:
				print("what was l? ", l)
				data = l[2]
				v1.append(data)
			value_feedbacks.append(v1)
			v1 = []
		print("value_feedbacks value_feedbacks value_feedbacks value_feedbacks   ",value_feedbacks)
		print("how_many_all_queshow_many_all_queshow_many_all_ques    ", how_many_all_ques)
		how_many_all_ques_totale.append(how_many_all_ques)
		total_value_feedbacks_for_game.append(value_feedbacks)
		supertotal_final_grade_tot_ques.append(final_grade_tot_ques)
		the_first = []
		the_second = []
		the_third = []
		the_fourth = []
		first_to_take,first_to_take1,first_to_take2,first_to_take3 = [],[],[],[]
		second_to_take,second_to_take1,second_to_take2,second_to_take3 = [],[],[],[]
		third_to_take,third_to_take1,third_to_take2,third_to_take3 = [],[],[],[]
		fourth_to_take,fourth_to_take1,fourth_to_take2,fourth_to_take3 = [],[],[],[]
	print("how_many_all_ques_totale how_many_all_ques_totale how_many_all_ques_totale ===> ",how_many_all_ques_totale)
	print("total_value_feedbacks_for_game total_value_feedbacks_for_game total_value_feedbacks_for_game ===> ",total_value_feedbacks_for_game)
	print("supertotal_final_grade_tot_quessupertotal_final_grade_tot_quessupertotal_final_grade_tot_ques ===> ",supertotal_final_grade_tot_ques)
	###################### GAME GAME GAME GAME GAME
	all_err_perc = feed_self.every_difficulties_game
	all_indeci = feed_self.every_time_percentage_game
	print("all_err_perc all_err_perc ",all_err_perc)
	print("all_indeci all_indeci" ,all_indeci)
	zipped_inputs_2 = list(zip(all_err_perc,all_indeci))
	print(zipped_inputs_2)
	for h in range(len(zipped_inputs_2)):
		for i in range(h,len(zipped_inputs_2)):
			print("loop i {}".format(i))
			for k in range(len(zipped_inputs_2[i])):
				print("loop k {}".format(k))
				for j in range(len(zipped_inputs_2[i][k])):
					p=j
					while p < len(zipped_inputs_2[i][k]):
						print("loop p {}".format(p))
						p+=1    
						if j==0:
							print("loop j {}".format(j))
							for z in range(len(zipped_inputs_2[i][k][j])):
								print("loop z {}".format(z))
								print(zipped_inputs_2[i][k][j][z])
								try:
									first_to_take_game.append(zipped_inputs_2[i][k][j][z])
								except:
									pass
								try:
									second_to_take_game.append(zipped_inputs_2[i][k][j][z+1])
								except:
									pass
								try:
									third_to_take_game.append(zipped_inputs[i][k][j][z+2])
								except:
									pass
								try:
									fourth_to_take_game.append(zipped_inputs[i][k][j][z+3])
								except:
									pass
								break
							print("j is ", j)    
							break
						if j==1:
							print("loop j {}".format(j))
							for z in range(len(zipped_inputs_2[i][k][j])):
								print("loop z {}".format(z))
								print(zipped_inputs_2[i][k][j][z])
								try:
									first_to_take_game1.append(zipped_inputs_2[i][k][j][z])
								except:
									pass
								try:
									second_to_take_game1.append(zipped_inputs_2[i][k][j][z+1])
								except:
									pass
								try:
									third_to_take_game1.append(zipped_inputs[i][k][j][z+2])
								except:
									pass
								try:
									fourth_to_take_game1.append(zipped_inputs[i][k][j][z+3])
								except:
									pass
								break
							print("j is ", j)    
							break
						if j==2:
							print("loop j {}".format(j))
							for z in range(len(zipped_inputs_2[i][k][j])):
								print("loop z {}".format(z))
								print(zipped_inputs_2[i][k][j][z])
								try:
									first_to_take_game2.append(zipped_inputs_2[i][k][j][z])
								except:
									pass
								try:
									second_to_take_game2.append(zipped_inputs_2[i][k][j][z+1])
								except:
									pass
								try:
									third_to_take_game2.append(zipped_inputs[i][k][j][z+2])
								except:
									pass
								try:
									fourth_to_take_game2.append(zipped_inputs[i][k][j][z+3])
								except:
									pass
								break										
							print("j is ", j)    
							break
						if j==3:
							print("loop j {}".format(j))
							for z in range(len(zipped_inputs_2[i][k][j])):
								print("loop z {}".format(z))
								print(zipped_inputs_2[i][k][j][z])
								try:
									first_to_take_game3.append(zipped_inputs_2[i][k][j][z])
								except:
									pass
								try:
									second_to_take_game3.append(zipped_inputs_2[i][k][j][z+1])
								except:
									pass
								try:
									third_to_take_game3.append(zipped_inputs[i][k][j][z+2])
								except:
									pass
								try:
									fourth_to_take_game3.append(zipped_inputs[i][k][j][z+3])
								except:
									pass
								break
							print("j is ", j)    
							break
			break
		prima_game.append(first_to_take_game)
		#prima_game.append(second_to_take_game)
		#prima_game.append(third_to_take_game)
		#prima_game.append(fourth_to_take_game)
		seconda_game.append(first_to_take_game1)
		#seconda_game.append(second_to_take_game1)
		#seconda_game.append(third_to_take_game1)
		#seconda_game.append(fourth_to_take_game1)
		terza_game.append(first_to_take_game2)
		#terza_game.append(second_to_take_game2)
		#terza_game.append(third_to_take_game2)
		#terza_game.append(fourth_to_take_game2)
		quarta_game.append(first_to_take_game3)
		#quarta_game.append(second_to_take_game3)
		#quarta_game.append(third_to_take_game3)
		#quarta_game.append(fourth_to_take_game3)
		prima_game = list(filter(None, prima_game))
		seconda_game = list(filter(None, seconda_game))
		terza_game = list(filter(None, terza_game))
		quarta_game = list(filter(None, quarta_game))
		#print(prima_game)
		#print(seconda_game)
		#print(terza_game)
		#print(quarta_game)
		total_mat_2.append(prima_game)
		total_mat_2.append(seconda_game)
		total_mat_2.append(terza_game)
		total_mat_2.append(quarta_game)
		print("################# total_mat_2 ", total_mat_2)
		total_mat_2 = list(filter(None, total_mat_2))
		print("################# total_mat after_2 ", total_mat_2)
		tottotal_2.append(total_mat_2)
		print("################# tottotal_2 ", tottotal_2)
		total_mat_2 = []
		y, i, p = 0,0,0
		final_grade_2 = []
		res_part_2 = []
		print("Before the first ==> selected_feed_questions", total_value_feedbacks_for_game)
		if len(prima_game)>0:
			for z in range(len(prima_game)):
				print("value_feedbacks in use [p] = ",total_value_feedbacks_for_game[p])
				how_many = how_many_all_ques_totale[h][p]
				print("how_many how_many tipo tipo ", how_many)
				selected_feed_questions = total_value_feedbacks_for_game[h][p]
				print("selected_feed_questions in use fir ",selected_feed_questions)
				for i in range(len(prima_game[z])-1):
					print(prima_game[z][y])
					selected_game_diff = prima_game[z][y]
					selected_time_perc_game = prima_game[z][y+1]
					print("###### SELECT GAME the_first ######")
					print("selected_game_diff {}".format(selected_game_diff))
					print("selected_time_perc_game {}".format(selected_time_perc_game))
					res,fres,conc = fceg.evaluate_feeds_game(how_many, selected_feed_questions, selected_game_diff, selected_time_perc_game)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part_2.append(res[0])
					res_part_2.append(fres[0])
					res_part_2.append(conc)
				final_grade_2.append(res_part_2)
				res_part_2 = []
		if len(seconda_game)>0:
			p += 1
			for z in range(len(seconda_game)):
				print("value_feedbacks in use[p] = ",total_value_feedbacks_for_game[p])
				how_many = how_many_all_ques_totale[h][p]
				print("how_many how_many tipo tipo ", how_many)
				selected_feed_questions = total_value_feedbacks_for_game[h][p]
				print("selected_feed_questions in use sec ",selected_feed_questions)
				for i in range(len(seconda_game[z])-1):
					print(seconda_game[z][y])
					selected_game_diff = seconda_game[z][y]
					selected_time_perc_game = seconda_game[z][y+1]
					print("selected_game_diff {}".format(selected_game_diff))
					print("selected_time_perc_game {}".format(selected_time_perc_game))
					res,fres,conc = fceg.evaluate_feeds_game(how_many, selected_feed_questions, selected_game_diff, selected_time_perc_game)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part_2.append(res[0])
					res_part_2.append(fres[0])
					res_part_2.append(conc)
				final_grade_2.append(res_part_2)
				res_part_2 = []
		if len(terza_game)>0:
			p += 1 
			for z in range(len(terza_game)):
				print("value_feedbacks in use[p] = ",total_value_feedbacks_for_game[p])
				how_many = how_many_all_ques_totale[h][p]
				print("how_many how_many tipo tipo ", how_many)
				selected_feed_questions = total_value_feedbacks_for_game[h][p]
				print("selected_feed_questions in use sec ",selected_feed_questions)
				for i in range(len(terza_game[z])-1):
					print(terza_game[z][y])
					selected_game_diff = terza_game[z][y]
					selected_time_perc_game = terza_game[z][y+1]
					#print("selected_game_diff ", selected_game_diff)
					#print("selected_time_perc_game ", selected_time_perc_game)
					res,fres,conc = fceg.evaluate_feeds_game(how_many, selected_feed_questions, selected_game_diff, selected_time_perc_game)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part_2.append(res[0])
					res_part_2.append(fres[0])
					res_part_2.append(conc)
				final_grade_2.append(res_part_2)
				res_part_2 = []
		if len(quarta_game)>0:
			p += 1
			for z in range(len(quarta_game)):
				print("value_feedbacks in use[p] = ",total_value_feedbacks_for_game[p])
				how_many = how_many_all_ques_totale[h][p]
				print("how_many how_many tipo tipo ", how_many)
				selected_feed_questions = total_value_feedbacks_for_game[h][p]
				print("selected_feed_questions in use sec ",selected_feed_questions)
				for i in range(len(quarta_game[z])-1):
					print(quarta_game[z][y])
					selected_game_diff = quarta_game[z][y]
					selected_time_perc_game = quarta_game[z][y+1]
					print("selected_game_diff ", selected_game_diff)
					print("selected_time_perc_game ", selected_time_perc_game)
					res,fres,conc = fceg.evaluate_feeds_game(how_many, selected_feed_questions, selected_game_diff, selected_time_perc_game)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part_2.append(res[0])
					res_part_2.append(fres[0])
					res_part_2.append(conc)
				final_grade_2.append(res_part_2)
				res_part_2 = []
		#print("BEFORE 22 final_grade 22")
		print(final_grade_2) 
		final_grade_2 = list(filter(None, final_grade_2))
		print(all_total)
		print(tottotal_2)
		total_mat2 = list(filter(None, total_mat))
		all_total = list(filter(None, all_total))
		tottotal_2 = list(filter(None, tottotal_2))
		#print([[x if len(x)>0 else 1 for x in y] for y in total_mat]) 
		#print("FINAL GradeS::::")
		#print("ques")
		#print(final_grade_tot_ques)
		#print("gam")
		#print(final_grade_2)
		value_feedbacks_game = [] 
		v2 = []
		selected_feed_questions = []
		final_grade_2 = list(filter(None, final_grade_2))
		print("AFTER remove [] and none .... final_grade question ",final_grade_2)
		how_many_g = len(final_grade_2)
		how_many_all_games = []
		how_many_all_games.append(how_many_g)
		for f in final_grade_2:
			print("what is f?? ", f)
			data = f[2]
			print("data = ", data)
			v2.append(data)
			value_feedbacks_game.append(v2)
			v2 = []
		print("value_feedbacks_game ",value_feedbacks_game)
		print("how_many_all_games ",how_many_all_games)
		how_many_all_games_totale.append(how_many_all_games)
		total_value_feedbacks_for_match.append(value_feedbacks_game)
		supertotal_final_grade_tot_game.append(final_grade_2)
		prima_game,seconda_game,terza_game,quarta_game = [],[],[],[]
		first_to_take_game,first_to_take_game1,first_to_take_game2,first_to_take_game3 = [],[],[],[]
		second_to_take_game,second_to_take_game1,second_to_take_game2,second_to_take_game3 = [],[],[],[]
		third_to_take_game,third_to_take_game1,third_to_take_game2,third_to_take_game3 = [],[],[],[]
		fourth_to_take_game,fourth_to_take_game1,fourth_to_take_game2,fourth_to_take_game3 = [],[],[],[]
	print("how_many_all_games_totale how_many_all_games_totale   ",how_many_all_games_totale)
	print("total_value_feedbacks_for_match total_value_feedbacks_for_match   ",total_value_feedbacks_for_match)
	print("supertotal_final_grade_tot_game supertotal_final_grade_tot_game   ",supertotal_final_grade_tot_game)
	################# match
	aum = feed_self.every_difficulty_matches
	bum = feed_self.every_time_percentage_match
	zipped_inputs_3 = list(zip(aum,bum))
	print("all_err_perc 3 == ",aum)
	print("all_indeci 3 == ",bum)
	print("ZIPPED 3 {}".format(zipped_inputs_3))
	print("len(zipped_inputs_3  === ) ",len(zipped_inputs_3))
	for h in range(len(zipped_inputs_3)):
		for i in range(len(zipped_inputs_3[h])):
			print("i ",i)
			print(zipped_inputs_3[h][i])
			try:
				if i==0:
					first_to_take_match.append(zipped_inputs_3[h][i])
			except:
				pass
			try:
				if i==1:
					second_to_take_match.append(zipped_inputs_3[h][i])
			except:
				pass
			try:
				if i==2:
					third_to_take_match.append(zipped_inputs_3[h][i])
			except:
				pass
			try:
				if i==3:
					fourth_to_take_match.append(zipped_inputs_3[h][i])
			except:
				pass

		y, p, i = 0,0,0
		final_grade_3 = []
		res_part_3 = []
		############# the_first 
		print("Before the first matches!!! ==> total_value_feedbacks_for_match", total_value_feedbacks_for_match)
		for z in range(len(first_to_take_match)):
			print("value_feedbacks game in use [p] = ",total_value_feedbacks_for_match[h])
			how_many_games = how_many_all_games_totale[h][p]
			print("how_many how_many how_many_gameshow_many_games  ", how_many_games)
			sel = total_value_feedbacks_for_match[h]
			print("selected_feed_questions che sto usando = the_first di flatten = ", sel)
			selected_feed_games = list(fct.iterFlatten(sel))
			print("selected_feed_questions in use fir ",selected_feed_games)
		print("len(first_to_take_match)", len(first_to_take_match))
		print("len(first_to_take_match)", len(first_to_take_match))
		print("len(first_to_take_match)", len(first_to_take_match))
		if len(first_to_take_match)==1:
			selected_matches_diff = first_to_take_match[0][0]
			selected_time_perc_matches = second_to_take_match[0][0]
			print("selected_matches_diff {}".format(selected_matches_diff))
			print("selected_time_perc_matches {}".format(selected_time_perc_matches))
			res,fres,conc = fcem.evaluate_feeds_match(how_many_games, selected_feed_games, selected_matches_diff, selected_time_perc_matches)
			print(res)
			print(fres)
			print(conc)
			res_part_3.append(res)
			res_part_3.append(fres)
			res_part_3.append(conc)
			final_grade_3.append(res_part_3)
			res_part_3 = []
		else:
			if len(first_to_take_match)>0:
				for z in range(len(first_to_take_match)):
					print(first_to_take_match[z])
					selected_matches_diff = first_to_take_match[z]
					selected_time_perc_matches = first_to_take_match[z+1]
					print("loop num z ",z)
					print("##### SELECT MATCH ######")
					print("selected_matches_diff {}".format(selected_matches_diff))
					print("selected_time_perc_matches {}".format(selected_time_perc_matches))
					res,fres,conc = fcem.evaluate_feeds_match(how_many_games, selected_feed_games, selected_matches_diff, selected_time_perc_matches)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part_3.append(res)
					res_part_3.append(fres)
					res_part_3.append(conc)
					final_grade_3.append(res_part_3)
					res_part_3 = []
			if len(second_to_take_match)>0:
				for z in range(len(second_to_take_match)-1):
					print(second_to_take_match[z])
					selected_matches_diff = second_to_take_match[z]
					selected_time_perc_matches = second_to_take_match[z+1]
					print("loop num z ",z)
					print("###### SELECT MATCH######")
					print("selected_matches_diff {}".format(selected_matches_diff))
					print("selected_time_perc_matches {}".format(selected_time_perc_matches))
					res,fres,conc = fcem.evaluate_feeds_match(how_many_games, selected_feed_games, selected_matches_diff, selected_time_perc_matches)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part_3.append(res)
					res_part_3.append(fres)
					res_part_3.append(conc)
					final_grade_3.append(res_part_3)
					res_part_3 = []	
			if len(third_to_take_match)>0:
				for z in range(len(third_to_take_match)-1):
					print(third_to_take_match[z])
					selected_matches_diff = third_to_take_match[z]
					selected_time_perc_matches = third_to_take_match[z+1]
					print("loop num z ",z)
					print("###### SELECT MATCH ######")
					print("selected_matches_diff {}".format(selected_matches_diff))
					print("selected_time_perc_matches {}".format(selected_time_perc_matches))
					res,fres,conc = fcem.evaluate_feeds_match(how_many_games, selected_feed_games, selected_matches_diff, selected_time_perc_matches)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part_3.append(res)
					res_part_3.append(fres)
					res_part_3.append(conc)
					final_grade_3.append(res_part_3)
					res_part_3 = []
			if len(fourth_to_take_match)>0:
				for z in range(len(fourth_to_take_match)-1):
					print(fourth_to_take_match[z])
					selected_matches_diff = fourth_to_take_match[z]
					selected_time_perc_matches = fourth_to_take_match[z+1]
					print("loop num z ",z)
					print("###### SELECT MATCH ######")
					print("selected_matches_diff {}".format(selected_matches_diff))
					print("selected_time_perc_matches {}".format(selected_time_perc_matches))
					res,fres,conc = fcem.evaluate_feeds_match(how_many_games, selected_feed_games, selected_matches_diff, selected_time_perc_matches)
					print("###### loop num ", z)
					print(res)
					print(fres)
					print(conc)
					res_part_3.append(res)
					res_part_3.append(fres)
					res_part_3.append(conc)
					final_grade_3.append(res_part_3)
					res_part_3 = []
		print("final_grade_3 {}".format(final_grade_3))	
		print("BEFORE 33 final_grade 33 {}".format(final_grade_3))
		final_grade_3 = list(filter(None, final_grade_3))
		print("AFTER 33 final_grade 33 {}".format(final_grade_3))
		final_grade_3 = [list(fct.iterFlatten(q)) for q in final_grade_3]
		value_feedbacks_m = []
		selected_feed_matches = []
		for f in final_grade_3:
			print("f mat", f)
			datam = f[2]
			print("datam ",datam)
			value_feedbacks_m.append(datam)
		selected_feed_matches = copy(value_feedbacks_m)
		TOTAL_matches_final_grade.append(final_grade_3)
		TOTAL_matches_values_feedbacks.append(value_feedbacks_m)
		first_to_take_match = []
		second_to_take_match = []
		third_to_take_match = []
		fourth_to_take_match = []		
	print("TOTAL_matches_final_grade  ",TOTAL_matches_final_grade)
	print("TOTAL_matches_values_feedbacks  ",TOTAL_matches_values_feedbacks)
	TOTAL_matches_how_many_matches = len(TOTAL_matches_final_grade)
	print("TOTAL_matches_how_many_matches  ", TOTAL_matches_how_many_matches)
	###################### session session session		
	###pronto per evalutation session fuzzy
	fgrade_4 = []
	selected_session_complexity = feed_self.complex_ses
	selected_time_perc_sess = feed_self.percentage_ses
	selected_feed_matches = list(fct.iterFlatten(TOTAL_matches_values_feedbacks))
	print("selected_session_complexity  ",selected_session_complexity)
	print("selected_time_perc_sess  ",selected_time_perc_sess)
	print("selected_feed_matches  ",selected_feed_matches)
	print("###### SELECT session ######")
	res,fres,conc = fces.evaluate_feeds_session(TOTAL_matches_how_many_matches, selected_feed_matches, selected_session_complexity, selected_time_perc_sess)
	fgrade_4.append(res)
	fgrade_4.append(fres)
	fgrade_4.append(conc)
	print("fgrade_4 ---> ",fgrade_4)
	print("fgrade_4 primo e unico---> ",fgrade_4[0])
	#print("@@@@@@@@@@@@@@@@@")
	print("ques final_grade_tot_ques ", supertotal_final_grade_tot_ques)
	print("games final_grade_2 ", supertotal_final_grade_tot_game)
	print("match final_grade_3 ", TOTAL_matches_final_grade)
	print("session final_grade_4 ", fgrade_4)
	fgrade_4 = [list(fct.iterFlatten(q)) for q in fgrade_4]
	fgrade_4 = [item for sublist in fgrade_4 for item in sublist]
	supertotal_final_grade_tot_ques = [item for sublist in supertotal_final_grade_tot_ques for item in sublist]
	supertotal_final_grade_tot_ques = [item for sublist in supertotal_final_grade_tot_ques for item in sublist]
	supertotal_final_grade_tot_game = [item for sublist in supertotal_final_grade_tot_game for item in sublist]
	TOTAL_matches_final_grade = [item for sublist in TOTAL_matches_final_grade for item in sublist]
	#print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
	print("ques final_grade_tot_ques ", supertotal_final_grade_tot_ques)
	print("games final_grade_2 ", supertotal_final_grade_tot_game)
	print("match final_grade_3 ", TOTAL_matches_final_grade)
	print("session final_grade_4 ", fgrade_4)
	feed_self.the_session_feeds = copy(fgrade_4)
	feed_self.matches_feeds = copy(TOTAL_matches_final_grade)
	feed_self.games_feeds = copy(supertotal_final_grade_tot_game)
	feed_self.questions_feeds = copy(supertotal_final_grade_tot_ques)
	print("Finally, feed_self.the_session_feeds    ", feed_self.the_session_feeds)
	print("Finally, feed_self.matches_feeds    ", feed_self.matches_feeds)
	print("Finally, feed_self.games_feeds    ", feed_self.games_feeds)
	print("Finally, feed_self.questions_feeds    ", feed_self.questions_feeds)