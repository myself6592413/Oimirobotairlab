# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/feedback_manager/fuzzy_logic' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/feedback_manager/fuzzy_logic')
import match_fuzzy_administrator as mfa
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef evaluate_feeds_match(how_many, selected_feed_games, selected_match_diff, selected_time_perc_match):
	if selected_match_diff[0]=='Easy_1':
		sel_match_diff = 1
	elif selected_match_diff[0]=='Easy_2':
		sel_match_diff = 2
	elif selected_match_diff[0]=='Normal_1':
		sel_match_diff = 3
	elif selected_match_diff[0]=='Normal_2':
		sel_match_diff = 4
	elif selected_match_diff[0]=='Medium_1':
		sel_match_diff = 5
	elif selected_match_diff[0]=='Medium_2':
		sel_match_diff = 6
	elif selected_match_diff[0]=='Hard_1':
		sel_match_diff = 7
	elif selected_match_diff[0]=='Hard_2':
		sel_match_diff = 8
	if len(selected_feed_games)==1:
		selected_feedback_game1 = selected_feed_games[0]
		mamf = mfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({mfa.input1_feedback_game1: selected_feedback_game1, 
			mfa.input5_match_difficulty: sel_match_diff, 
			mfa.input6_time_perc: selected_time_perc_match})

	if len(selected_feed_games)==2:
		selected_feedback_game1 = selected_feed_games[0]
		selected_feedback_game2 = selected_feed_games[1]
		mamf = mfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({mfa.input1_feedback_game1: selected_feedback_game1, 
			mfa.input2_feedback_game2: selected_feedback_game2, 
			mfa.input5_match_difficulty: sel_match_diff, 
			mfa.input6_time_perc: selected_time_perc_match})

	if len(selected_feed_games)==3:
		selected_feedback_game1 = selected_feed_games[0]
		selected_feedback_game2 = selected_feed_games[1]
		selected_feedback_game3 = selected_feed_games[2]
		mamf = mfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({mfa.input1_feedback_game1: selected_feedback_game1, 
			mfa.input2_feedback_game2: selected_feedback_game2, 
			mfa.input3_feedback_game3: selected_feedback_game3, 
			mfa.input5_match_difficulty: sel_match_diff, 
			mfa.input6_time_perc: selected_time_perc_match})

	if len(selected_feed_games)==4:
		selected_feedback_game1 = selected_feed_games[0]
		selected_feedback_game2 = selected_feed_games[1]
		selected_feedback_game3 = selected_feed_games[2]
		selected_feedback_game4 = selected_feed_games[3]
		mamf = mfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({mfa.input1_feedback_game1: selected_feedback_game1, 
			mfa.input2_feedback_game2: selected_feedback_game2, 
			mfa.input3_feedback_game3: selected_feedback_game3, 
			mfa.input4_feedback_game4: selected_feedback_game4, 
			mfa.input5_match_difficulty: sel_match_diff, 
			mfa.input6_time_perc: selected_time_perc_match})

	out_val = [result[k] for k in result]
	value_output = out_val[0]
	print("value_output match ===========> ", value_output)
	gen_name_output = []
	name_output = []
	quantity_output = []
	if 0 < value_output <= 0.2:
		gen_name_output.append('Awful')
	if 0.2 < value_output <= 0.5:
		gen_name_output.append('Bad')
	if 0.3 < value_output <= 0.7:
		gen_name_output.append('Good')
	if 0.6 < value_output <= 0.8:
		gen_name_output.append('Excellent')
	if value_output > 0.8:
		gen_name_output.append('Perfect')
	if 0 < value_output <= 0.08:
		quantity_output.append('heavily')
		name_output.append('Awful')
	if 0.08 < value_output <= 0.15:
		quantity_output.append('precisely')
		name_output.append('Awful')
	if 0.15 < value_output <= 0.2:
		quantity_output.append('marginally')
		name_output.append('Awful')
	if 0.1 < value_output <= 0.2:
		quantity_output.append('heavily')
		name_output.append('Bad')
	if 0.2 < value_output <= 0.3:
		quantity_output.append('precisely')
		name_output.append('Bad')
	if 0.3 < value_output <= 0.4:
		quantity_output.append('marginally')
		name_output.append('Bad')
	if 0.3 < value_output <= 0.4:
		quantity_output.append('marginally')
		name_output.append('Good')
	if 0.4 < value_output <= 0.6:
		quantity_output.append('precisely')
		name_output.append('Good')
	if 0.6 < value_output <= 0.7:
		quantity_output.append('heavily')
		name_output.append('Good')
	if 0.6 < value_output <= 0.7:
		quantity_output.append('marginally')
		name_output.append('Excellent')
	if 0.7 < value_output <= 0.8:
		quantity_output.append('precisely')
		name_output.append('Excellent')
	if 0.8 < value_output <= 0.9:
		quantity_output.append('heavily')    
		name_output.append('Excellent')
	if 0.8 < value_output <= 0.85:
		quantity_output.append('marginally')
		name_output.append('Perfect')
	if 0.85 < value_output <= 0.9:
		quantity_output.append('precisely')
		name_output.append('Perfect')
	if 0.9 < value_output <= 1:
		quantity_output.append('heavily')
		name_output.append('Perfect')

	print(name_output)
	print(quantity_output)

	total = 11
	if total > 9:
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[0]
			del quantity_output[0]
	print(quantity_output)
	print(name_output)


	return quantity_output,name_output,value_output