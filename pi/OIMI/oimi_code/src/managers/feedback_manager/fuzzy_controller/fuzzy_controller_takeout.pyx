# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef percentage(part, whole):
	if whole==0:
		return 0
	else:
		return round(part/whole,2)
		
def unique(sequence):
	seen = set()
	return [x for x in sequence if not (x in seen or seen.add(x))]

def iterFlatten(a_list):
	if isinstance(a_list, (list, tuple)):
		for element in a_list:
			for e in iterFlatten(element):
				yield e
	else:
		yield a_list

def match_reduction(a_list):
	for i in a_list:
		b = list(iterFlatten(i))
		c = unique(b)
	return c
# ------------------------------------------------------------------------------------------------------------------------------------------------
# Questions
# ------------------------------------------------------------------------------------------------------------------------------------------------
cdef takeout_feeds_from_db_question(feed_self, int which_kid):
	print("entro in takeout ques")
	s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
	this_session = feed_self.db_conn.execute_new_query(s_sql,which_kid)
	this_session = this_session[0]
	m_sql = "SELECT match_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id = ?"
	these_matches = feed_self.db_conn.execute_new_query(m_sql,(which_kid,this_session))
	g_sql = "SELECT game_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id = ? "	
	#########################################
	print("res_match before", these_matches)
	these_matches = list(set(these_matches))
	print("res_match after", these_matches)	
	#########################################
	gama = []
	for mm in these_matches:
		g_sql = "SELECT game_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id= ? AND match_id = ?"
		these_games = feed_self.db_conn.execute_new_query(g_sql,(which_kid,this_session,mm))
		gama.append(these_games)
		print("these_games ", these_games)
	print("gama {}".format(gama))
	games_for_each_match = []
	for g in gama:
		gm = list(set(g))
		print("for gmpoe", gm)
		gm_sort = sorted(gm)
		games_for_each_match.append(gm_sort)
	print("games_for_each_match ", games_for_each_match)
	age_sql = "SELECT age FROM Kids where kid_id = ?"
	kid_age = feed_self.db_conn.execute_new_query(age_sql,(which_kid))
	lev_sql = "SELECT current_level FROM Kids where kid_id = ?"
	kid_level = feed_self.db_conn.execute_new_query(lev_sql,(which_kid))
	feed_self.kid_age = kid_age[0]
	kid_level = kid_level[0]
	if kid_level=='beginner':
		feed_self.kid_level = 1
	if kid_level=='elementary':
		feed_self.kid_level = 2
	if kid_level=='intermediate':
		feed_self.kid_level = 3
	if kid_level=='advanced':
		feed_self.kid_level = 4
	print("feed_self.kid_age ", feed_self.kid_age)
	print("feed_self.kid_level", feed_self.kid_level)
	every_question = []
	every_num_answers = []
	every_correct_ans = []
	every_errors_ans = []
	every_indecisions = []
	every_question_times = []
	all_values_questions= []
	all_values_questions_tmp = []
	every_question_kinds = []
	every_question_percentage_errs = []
	for mm in these_matches:
		for gg in games_for_each_match:
			for gg1 in gg:
				q_sql = "SELECT question_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id= ? AND match_id = ? AND game_id = ?"
				qq_que = feed_self.db_conn.execute_new_query(q_sql,(which_kid,this_session,mm,gg1))
				every_question.append(qq_que)
				na_sql = "SELECT num_answers_question FROM Full_played_recap_giga WHERE kid_id = ? AND session_id= ? AND match_id = ? AND game_id = ?"
				naq_que = feed_self.db_conn.execute_new_query(na_sql,(which_kid,this_session,mm,gg1))
				every_num_answers.append(naq_que)
				co_sql = "SELECT num_corrects_question FROM Full_played_recap_giga WHERE kid_id = ? AND session_id= ? AND match_id = ? AND game_id = ?"
				coq_que = feed_self.db_conn.execute_new_query(co_sql,(which_kid,this_session,mm,gg1))
				every_correct_ans.append(coq_que)
				er_sql = "SELECT num_errors_question FROM Full_played_recap_giga WHERE kid_id = ? AND session_id= ? AND match_id = ? AND game_id = ?"
				err_que = feed_self.db_conn.execute_new_query(er_sql,(which_kid,this_session,mm,gg1))
				every_errors_ans.append(err_que)
				in_sql = "SELECT num_indecisions_question FROM Full_played_recap_giga WHERE kid_id = ? AND session_id= ? AND match_id = ? AND game_id = ?"
				indq_que = feed_self.db_conn.execute_new_query(in_sql,(which_kid,this_session,mm,gg1))
				every_indecisions.append(indq_que)
				ti_sql = "SELECT time_of_question FROM Full_played_recap_giga WHERE kid_id = ? AND session_id= ? AND match_id = ? AND game_id = ?"
				timeq_q = feed_self.db_conn.execute_new_query(ti_sql,(which_kid,this_session,mm,gg1))
				every_question_times.append(timeq_q)
	every_question = list(filter(None, every_question))
	every_num_answers = list(filter(None, every_num_answers))
	every_correct_ans = list(filter(None, every_correct_ans))
	every_errors_ans = list(filter(None, every_errors_ans))
	every_indecisions = list(filter(None, every_indecisions))
	every_question_times = list(filter(None, every_question_times))
	ca = []
	for eq in every_question:
		for b in eq:
			ki_sql = "SELECT kind FROM Questions WHERE question_id = ?"
			kind_ma = feed_self.db_conn.execute_new_query(ki_sql,(b))
			kind_ma = kind_ma[0]
			print("kind_ma kind_ma kind_ma kind_ma kind_ma kind_ma ==> ",kind_ma)
			if kind_ma=='1F':
				kind_n = 1
			elif kind_ma=='2L':
				kind_n = 2
			elif kind_ma=='3S':
				kind_n = 3
			elif kind_ma=='4P':
				kind_n = 4
			elif kind_ma=='5K':
				kind_n = 5
			elif kind_ma=='6I':
				kind_n = 6
			elif kind_ma=='7O':
				kind_n = 7
			elif kind_ma=='8Q':
				kind_n = 8
			elif kind_ma=='9C':
				kind_n = 9
			print("kind_n ==> ", kind_n)
			ca.append(kind_n)
		every_question_kinds.append(ca)
		ca = []
	print(every_question_kinds)
	ca = []
	if every_correct_ans[0][0]==1: #controllo che vada bene senno tolog e decido
		for a in range(len(every_num_answers)):
			for b in range(len(every_correct_ans[a])):
				denominator = every_correct_ans[a][b]+every_errors_ans[a][b]
				perc = percentage(every_correct_ans[a][b], denominator)
				print("perc perc ==> ", perc)
				ca.append(perc)
			every_question_percentage_errs.append(ca)
			ca = []
	print("every_question_percentage_errs ," every_question_percentage_errs)
	print("every_question ", every_question)
	print("every_num_answers ", every_num_answers)
	print("every_correct_ans ", every_correct_ans)
	print("every_errors_ans ", every_errors_ans)
	print("every_indecisions ", every_indecisions)
	print("every_question_times ", every_question_times)
	print("every_question_kinds ", every_question_kinds)
	#############################################
	j,k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_question[k])
			cc.append(every_question[k])
			j+=1
			k+=1
		j=0
		feed_self.every_question.append(cc)
		cc = []
	#############################################
	#############################################
	j,k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_num_answers[k])
			cc.append(every_num_answers[k])
			j+=1
			k+=1
		j=0
		feed_self.every_num_answers.append(cc)
		cc = []
	#############################################	
	#############################################
	j,k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_correct_ans[k])
			cc.append(every_correct_ans[k])
			j+=1
			k+=1
		j=0
		feed_self.every_correct_ans.append(cc)
		cc = []
	#############################################	
	#############################################
	j,k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_errors_ans[k])
			cc.append(every_errors_ans[k])
			j+=1
			k+=1
		j=0
		feed_self.every_errors_ans.append(cc)
		cc = []
	#############################################	
	#############################################
	j,k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_indecisions[k])
			cc.append(every_indecisions[k])
			j+=1
			k+=1
		j=0
		feed_self.every_indecisions.append(cc)
		cc = []
	#############################################
	#############################################
	j,k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_question_times[k])
			cc.append(every_question_times[k])
			j+=1
			k+=1
		j=0
		feed_self.every_question_times.append(cc)
		cc = []
	#############################################	
	#############################################
	j,k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_question_kinds[k])
			cc.append(every_question_kinds[k])
			j+=1
			k+=1
		j=0
		feed_self.every_question_kinds.append(cc)
		cc = []
	#############################################	
	#############################################
	j,k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_question_percentage_errs[k])
			cc.append(every_question_percentage_errs[k])
			j+=1
			k+=1
		j=0
		feed_self.every_question_percentage_errs.append(cc)
		cc = []
	#############################################
	#print("after con feed question")
	#print("determinant takeout every_question")
	#print(feed_self.every_question)
	#print("determinant takeout every_num_answers")
	#print(feed_self.every_num_answers)
	#print("determinant takeout every_correct_ans")
	#print(feed_self.every_correct_ans)
	#print("determinant takeout every_errors_ans")
	#print(feed_self.every_errors_ans)
	#print("determinant takeout every_indecisions")
	#print(feed_self.every_indecisions)
	#print("determinant takeout every_question_times")
	#print(feed_self.every_question_times)
	#print("determinant takeout every_question_kinds")
	#print(feed_self.every_question_kinds)
	#print("determinant takeout every_question_percentage_errs")
	#print(feed_self.every_question_percentage_errs)
	val_lis = []
	vall = []
	for qq in range(len(feed_self.every_question)):
		#print("qq ", feed_self.every_question[qq])
		for qq1 in range(len(feed_self.every_question[qq])):
			#print("qq1 ", feed_self.every_question[qq][qq1])
			for qqq in range(len(feed_self.every_question[qq][qq1])):
				#print("qqq ===> ", feed_self.every_question[qq][qq1][qqq])
				val_sql = "SELECT value FROM Questions where question_id=?"
				v_q = feed_self.db_conn.execute_new_query(val_sql,(feed_self.every_question[qq][qq1][qqq]))
				#print("v_q is ", v_q)
				val_lis.append(v_q[0])
				print("val_lis = ", val_lis)
			vall.append(val_lis)
			#print("vall ", vall)
			val_lis = []
		feed_self.all_values_questions.append(vall)
		vall = []
	print(feed_self.all_values_questions)	
# ------------------------------------------------------------------------------------------------------------------------------------------------
# Games
# ------------------------------------------------------------------------------------------------------------------------------------------------
cdef takeout_feeds_from_db_game(feed_self, int which_kid):
	s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
	this_session = feed_self.db_conn.execute_new_query(s_sql,which_kid)
	this_session = this_session[0]
	m_sql = "SELECT match_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id = ?"
	these_matches = feed_self.db_conn.execute_new_query(m_sql,(which_kid,this_session))
	g_sql = "SELECT game_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id = ? "	
	#########################################
	print("res_match before", these_matches)
	these_matches = list(set(these_matches))
	print("res_match after", these_matches)	
	#########################################
	gama = []
	for mm in these_matches:
		g_sql = "SELECT game_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id= ? AND match_id = ?"
		these_games = feed_self.db_conn.execute_new_query(g_sql,(which_kid,this_session,mm))
		gama.append(these_games)
		print("these_games ", these_games)
	print("gama ", gama)
	games_for_each_match = []
	for g in gama:
		gm = list(set(g))
		print("for gmpoe", gm)
		gm_sort = sorted(gm)
		games_for_each_match.append(gm_sort)
	print("games_for_each_match {}".format(games_for_each_match))
	every_game_times = []
	every_expected_game_times = []
	every_difficulties_game = []
	every_val_feedback_ques = []
	every_quan_feedback_ques = []
	every_grade_feedback_ques = []
	every_time_percentage_game = []
	every_kind_games = []
	for mm in these_matches:
		for gg in games_for_each_match:
			for gg1 in gg:
				tg_sql = "SELECT time_of_game FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				ti_ga = feed_self.db_conn.execute_new_query(tg_sql,(which_kid,this_session,mm,gg1))
				every_game_times.append(ti_ga)
				etg_sql = "SELECT necessary_time FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				et_ga = feed_self.db_conn.execute_new_query(etg_sql,(which_kid,this_session,mm,gg1))
				every_expected_game_times.append(et_ga)
				dg_sql = "SELECT g_difficulty FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				di_ga = feed_self.db_conn.execute_new_query(dg_sql,(which_kid,this_session,mm,gg1))
				every_difficulties_game.append(di_ga)
				qua_qu_sql = "SELECT quantity_feedback_question FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				qua_ga = feed_self.db_conn.execute_new_query(qua_qu_sql,(which_kid,this_session,mm,gg1))
				every_quan_feedback_ques.append(qua_ga)
				gra_qu_sql = "SELECT grade_feedback_question FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				gra_ga = feed_self.db_conn.execute_new_query(gra_qu_sql,(which_kid,this_session,mm,gg1))
				every_grade_feedback_ques.append(gra_ga)
				val_qu_sql = "SELECT value_feedback_question FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				val_ga = feed_self.db_conn.execute_new_query(val_qu_sql,(which_kid,this_session,mm,gg1))
				every_val_feedback_ques.append(val_ga)
				ki_sql = "SELECT kind FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				ki_ga = feed_self.db_conn.execute_new_query(ki_sql,(which_kid,this_session,mm,gg1))
				every_kind_games.append(ki_ga)
	every_game_times = list(filter(None, every_game_times))
	every_expected_game_times = list(filter(None, every_expected_game_times))
	every_difficulties_game = list(filter(None, every_difficulties_game))
	every_val_feedback_ques = list(filter(None, every_val_feedback_ques))
	every_quan_feedback_ques = list(filter(None, every_quan_feedback_ques))
	every_grade_feedback_ques = list(filter(None, every_grade_feedback_ques))
	every_kind_games = list(filter(None, every_kind_games))
	ca = []
	for a in range(len(every_game_times)):
		for b in range(len(every_expected_game_times[a])):
			perc = percentage(every_game_times[a][b],every_expected_game_times[a][b])
			print("perc perc time game ==> ", perc)
			if perc > 1:
				print("adjusted percentage session to 1")
				perc = 1
			ca.append(perc)
		every_time_percentage_game.append(ca)
		ca = []
	#print("before without FEED GAME")
	#print("every_game_times")
	#print(every_game_times)
	#print("every_expected_game_times")
	#print(every_expected_game_times)
	#print("every_difficulties_game")
	#print(every_difficulties_game)
	#print("every_val_feedback_ques")
	#print(every_val_feedback_ques)
	#print("every_quan_feedback_ques")
	#print(every_quan_feedback_ques)
	#print("every_grade_feedback_ques")
	#print(every_grade_feedback_ques)
	#print("every_time_percentage_game")
	#print(every_time_percentage_game)
	#############################################
	j, k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_game_times[k])
			cc.append(every_game_times[k])
			j+=1
			k+=1
		j=0
		feed_self.every_game_times.append(cc)
		cc = []
	#############################################
	#############################################
	j, k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_difficulties_game[k])
			cc.append(every_difficulties_game[k])
			j+=1
			k+=1
		j=0
		feed_self.every_difficulties_game.append(cc)
		cc = []
	#############################################	
	#############################################
	j,k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_val_feedback_ques[k])
			cc.append(every_val_feedback_ques[k])
			j+=1
			k+=1
		j=0
		feed_self.every_val_feedback_ques.append(cc)
		cc = []
	#############################################	
	#############################################
	j, k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_quan_feedback_ques[k])
			cc.append(every_quan_feedback_ques[k])
			j+=1
			k+=1
		j=0
		feed_self.every_quan_feedback_ques.append(cc)
		cc = []
	#############################################	
	#############################################
	j, k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_grade_feedback_ques[k])
			cc.append(every_grade_feedback_ques[k])
			j+=1
			k+=1
		j=0
		feed_self.every_grade_feedback_ques.append(cc)
		cc = []
	#############################################
	#############################################
	j, k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_time_percentage_game[k])
			cc.append(every_time_percentage_game[k])
			j+=1
			k+=1
		j=0
		feed_self.every_time_percentage_game.append(cc)
		cc = []
	#############################################						
	#print("feed_self.every_game_times")
	#print(feed_self.every_game_times)
	#print("feed_self.every_expected_game_times")
	#print(feed_self.every_expected_game_times)
	#print("feed_self.every_difficulties_game")
	#print(feed_self.every_difficulties_game)
	#print("feed_self.every_val_feedback_ques")
	#print(feed_self.every_val_feedback_ques)
	#print("feed_self.every_quan_feedback_ques")
	#print(feed_self.every_quan_feedback_ques)
	#print("feed_self.every_grade_feedback_ques")
	#print(feed_self.every_grade_feedback_ques)
	#print("every_time_percentage_game")
	#print(feed_self.every_time_percentage_game)
# ------------------------------------------------------------------------------------------------------------------------------------------------
# Matches
# ------------------------------------------------------------------------------------------------------------------------------------------------
cdef takeout_feeds_from_db_match(feed_self, int which_kid):
	s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
	this_session = feed_self.db_conn.execute_new_query(s_sql,which_kid)
	this_session = this_session[0]
	m_sql = "SELECT match_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id = ?"
	these_matches = feed_self.db_conn.execute_new_query(m_sql,(which_kid,this_session))
	g_sql = "SELECT game_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id = ? "	
	#########################################
	print("res_match before", these_matches)
	these_matches = list(set(these_matches))
	print("res_match after", these_matches)	
	#########################################
	gama = []
	for mm in these_matches:
		g_sql = "SELECT game_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id= ? AND match_id = ?"
		these_games = feed_self.db_conn.execute_new_query(g_sql,(which_kid,this_session,mm))
		gama.append(these_games)
		print("these_games")
		print(these_games)
	print()
	print("gama")
	print(gama)
	games_for_each_match = []
	for g in gama:
		gm = list(set(g))
		print("for gmpoe", gm)
		gm_sort = sorted(gm)
		games_for_each_match.append(gm_sort)
	print("games_for_each_match ", games_for_each_match)
	every_match_times = []
	every_expected_match_times = []
	every_difficulty_matches = []
	every_quan_feedback_game = []
	every_grade_feedback_game = []
	every_val_feedback_game = []
	every_time_percentage_match = []
	for mm in these_matches:
		for gg in games_for_each_match:
			for gg1 in gg:
				tm_sql = "SELECT time_of_match FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				ti_ma = feed_self.db_conn.execute_new_query(tm_sql,(which_kid,this_session,mm,gg1))
				every_match_times.append(ti_ma)
				atm_sql = "SELECT advisable_time FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				ati_ma = feed_self.db_conn.execute_new_query(atm_sql,(which_kid,this_session,mm,gg1))
				every_expected_match_times.append(ati_ma)
				dm_sql = "SELECT m_difficulty FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				di_ma = feed_self.db_conn.execute_new_query(dm_sql,(which_kid,this_session,mm,gg1))
				every_difficulty_matches.append(di_ma)
				qua_ga_sql = "SELECT quantity_feedback_game FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				qua_ma = feed_self.db_conn.execute_new_query(qua_ga_sql,(which_kid,this_session,mm,gg1))
				every_quan_feedback_game.append(qua_ma)
				gra_ga_sql = "SELECT grade_feedback_game FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				gra_ma = feed_self.db_conn.execute_new_query(gra_ga_sql,(which_kid,this_session,mm,gg1))
				every_grade_feedback_game.append(gra_ma)
				val_ga_sql = "SELECT value_feedback_game FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				val_ma = feed_self.db_conn.execute_new_query(val_ga_sql,(which_kid,this_session,mm,gg1))
				every_val_feedback_game.append(val_ma)
	every_match_times = list(filter(None, every_match_times))
	every_expected_match_times = list(filter(None, every_expected_match_times))
	every_difficulty_matches = list(filter(None, every_difficulty_matches))
	every_val_feedback_game = list(filter(None, every_val_feedback_game))
	every_quan_feedback_game = list(filter(None, every_quan_feedback_game))
	every_grade_feedback_game = list(filter(None, every_grade_feedback_game))
	ca = []
	for a in range(len(every_match_times)):
		for b in range(len(every_expected_match_times[a])):
			perc = percentage(every_match_times[a][b],every_expected_match_times[a][b])
			print("perc perc time match ==> ", perc)
			if perc > 1:
				perc = 1
				print("adjusted percentage match to 1")
			ca.append(perc)
		every_time_percentage_match.append(ca)
		ca = []
	#print("before without FEEED MATCH")
	#print("every_match_times")
	#print(every_match_times)
	#print("every_expected_match_times")
	#print(every_expected_match_times)
	#print("every_time_percentage_match")
	#print(every_time_percentage_match)
	#print("every_difficulty_matches")
	#print(every_difficulty_matches)
	#print("every_val_feedback_game")
	#print(every_val_feedback_game)
	#print("every_quan_feedback_game")
	#print(every_quan_feedback_game)
	#print("every_grade_feedback_game")
	#print(every_grade_feedback_game)
	#############################################
	j, k = 0, 0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_match_times[k])
			cc.append(every_match_times[k])
			j+=1
			k+=1
		j=0
		feed_self.every_match_times.append(cc)
		cc = []
	#############################################
	#############################################
	j, k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_difficulty_matches[k])
			cc.append(every_difficulty_matches[k])
			j+=1
			k+=1
		j=0
		feed_self.every_difficulty_matches.append(cc)
		cc = []
	#############################################	
	#############################################
	j, k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_val_feedback_game[k])
			cc.append(every_val_feedback_game[k])
			j+=1
			k+=1
		j=0
		feed_self.every_val_feedback_game.append(cc)
		cc = []
	#############################################	
	#############################################
	j, k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_quan_feedback_game[k])
			cc.append(every_quan_feedback_game[k])
			j+=1
			k+=1
		j=0
		feed_self.every_quan_feedback_game.append(cc)
		cc = []
	#############################################	
	#############################################
	j, k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_grade_feedback_game[k])
			cc.append(every_grade_feedback_game[k])
			j+=1
			k+=1
		j=0
		feed_self.every_grade_feedback_game.append(cc)
		cc = []
	#############################################
	#############################################
	j, k = 0,0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_time_percentage_match[k])
			cc.append(every_time_percentage_match[k])
			j+=1
			k+=1
		j=0
		feed_self.every_time_percentage_match.append(cc)
		cc = []
	#############################################			
	#print("after FEEED MATCH fix, but before di unique e flatten")
	#print("feed_self.every_match_times")
	#print(feed_self.every_match_times)
	#print("feed_self.every_difficulty_matches")
	#print(feed_self.every_difficulty_matches)
	#print("feed_self.every_val_feedback_game")
	#print(feed_self.every_val_feedback_game)
	#print("feed_self.every_quan_feedback_game")
	#print(feed_self.every_quan_feedback_game)
	#print("feed_self.every_grade_feedback_game")
	#print(feed_self.every_grade_feedback_game)
	#print("every_time_percentage_match")
	#print(feed_self.every_time_percentage_match)
	feed_self.every_match_times = [match_reduction(i) for i in feed_self.every_match_times]
	feed_self.every_difficulty_matches = [match_reduction(i) for i in feed_self.every_difficulty_matches]
	feed_self.every_time_percentage_match = [match_reduction(i) for i in feed_self.every_time_percentage_match]
	#print("after CON FEEED MATCH")
	#print("feed_self.every_match_times")
	#print(feed_self.every_match_times)
	#print("feed_self.every_difficulty_matches")
	#print(feed_self.every_difficulty_matches)
	#print("feed_self.every_val_feedback_game")
	#print(feed_self.every_val_feedback_game)
	#print("feed_self.every_quan_feedback_game")
	#print(feed_self.every_quan_feedback_game)
	#print("feed_self.every_grade_feedback_game")
	#print(feed_self.every_grade_feedback_game)
	#print("every_time_percentage_match")
	#print(feed_self.every_time_percentage_match)
	#print("every_match_times")
	#print(feed_self.every_match_times)
# ------------------------------------------------------------------------------------------------------------------------------------------------
# Session
# ------------------------------------------------------------------------------------------------------------------------------------------------
cdef takeout_feeds_from_db_session(feed_self, int which_kid):
	s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
	this_session = feed_self.db_conn.execute_new_query(s_sql,which_kid)
	this_session = this_session[0]
	m_sql = "SELECT match_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id = ?"
	these_matches = feed_self.db_conn.execute_new_query(m_sql,(which_kid,this_session))
	g_sql = "SELECT game_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id = ? "	
	print("res_match before", these_matches)
	these_matches = list(set(these_matches))
	print("res_match after", these_matches)	
	#########################################
	gama = []
	for mm in these_matches:
		g_sql = "SELECT game_id FROM Full_played_recap_giga WHERE kid_id = ? AND session_id= ? AND match_id = ?"
		these_games = feed_self.db_conn.execute_new_query(g_sql,(which_kid,this_session,mm))
		gama.append(these_games)
		print("these_games ", these_games)
	print("gama ", gama)
	games_for_each_match = []
	for g in gama:
		gm = list(set(g))
		print("for gmpoe", gm)
		gm_sort = sorted(gm)
		games_for_each_match.append(gm_sort)
	print("games_for_each_match {}".format(games_for_each_match))
	com_sql = "SELECT complexity FROM Full_played_recap_giga where session_id= ? LIMIT 1"
	feed_self.complex_ses = feed_self.db_conn.execute_new_query(com_sql,(this_session))
	ts_sql = "SELECT time_of_session FROM Full_played_recap_giga where session_id= ? LIMIT 1"
	feed_self.time_ses = feed_self.db_conn.execute_new_query(ts_sql,(this_session))
	ets_sql = "SELECT desiderable_time FROM Full_played_recap_giga where session_id= ? LIMIT 1"
	feed_self.expected_time_ses = feed_self.db_conn.execute_new_query(ets_sql,(this_session))
	print("perc perc complex_ses complex_ses ==> ", feed_self.complex_ses)
	print("perc perc time_ses session ==> ", feed_self.time_ses)
	print("perc perc expected_time_ses session ==> ", feed_self.expected_time_ses)
	feed_self.percentage_ses = percentage(int(feed_self.time_ses[0]),feed_self.expected_time_ses[0])
	print("perc perc time session ==> ", feed_self.percentage_ses)
	if feed_self.percentage_ses > 1:
		print("adjusted percentage session to 1")
		feed_self.percentage_ses = 1
	every_quan_feedback_match = []
	every_grade_feedback_match = []
	every_val_feedback_match = []
	every_quan_feedback_match_1 = []
	every_grade_feedback_match_1 = []
	every_val_feedback_match_1 = []
	every_quan_fm = []
	every_grade_fm = []
	every_val_fm = []
	for mm in these_matches:
		for gg in games_for_each_match:
			for gg1 in gg:
				qua_ga_sql = "SELECT quantity_feedback_match FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				qua_se = feed_self.db_conn.execute_new_query(qua_ga_sql,(which_kid,this_session,mm,gg1))
				every_quan_feedback_match_1.append(qua_se)
				gra_ga_sql = "SELECT grade_feedback_match FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				gra_se = feed_self.db_conn.execute_new_query(gra_ga_sql,(which_kid,this_session,mm,gg1))
				every_grade_feedback_match_1.append(gra_se)
				val_ga_sql = "SELECT value_feedback_match FROM Full_played_recap_giga where kid_id = ? and session_id= ? and match_id = ? and game_id = ?"
				val_se = feed_self.db_conn.execute_new_query(val_ga_sql,(which_kid,this_session,mm,gg1))
				every_val_feedback_match_1.append(val_se)
	print("before without feed session")
	print("every_val_feedback_match {}".format(every_val_feedback_match_1))
	print("every_quan_feedback_match {}".format(every_quan_feedback_match_1))
	print("every_grade_feedback_match {}".format(every_grade_feedback_match_1))
	#############################################
	j, k = 0, 0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_val_feedback_match_1[k])
			cc.append(every_val_feedback_match_1[k])
			j+=1
			k+=1
		j=0
		every_val_fm.append(cc)
		cc = []
	#############################################	
	#############################################
	j, k = 0, 0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_quan_feedback_match_1[k])
			cc.append(every_quan_feedback_match_1[k])
			j+=1
			k+=1
		j=0
		every_quan_fm.append(cc)
		cc = []
	#############################################	
	#############################################
	j, k = 0, 0
	cc = []
	for i in range(len(games_for_each_match)):
		while j < len(games_for_each_match[i]):
			print("i ",i)
			print("j ",j)
			print("k ",k)
			print(every_grade_feedback_match_1[k])
			cc.append(every_grade_feedback_match_1[k])
			j+=1
			k+=1
		j=0
		every_grade_fm.append(cc)
		cc = []
	#############################################
	#print("after con feed session")
	#print("feed_self.complex_ses")
	#print(feed_self.complex_ses)
	#print("feed_self.time_ses")
	#print(feed_self.time_ses)
	#print("feed_self.every_val_feedback_match")
	#print(every_val_fm)
	#print("feed_self.every_quan_feedback_match")
	#print(every_quan_fm)
	#print("feed_self.every_grade_feedback_match")
	#print(every_grade_fm)
	every_val_feedback_match_2 = list(filter(None, every_val_fm))
	every_quan_feedback_match_2 = list(filter(None, every_quan_fm))
	every_grade_feedback_match_2 = list(filter(None, every_grade_fm))
	print("every_val_feedback_match_2 ============ ", every_val_feedback_match_2)
	print("every_quan_feedback_match_2 ============ ", every_quan_feedback_match_2)
	print("every_grade_feedback_match_2 ============ ", every_grade_feedback_match_2)
	every_val_feedback_match_2 = list(iterFlatten(every_val_feedback_match_2))
	every_quan_feedback_match_2 = list(iterFlatten(every_quan_feedback_match_2))
	every_grade_feedback_match_2 = list(iterFlatten(every_grade_feedback_match_2))
	print("every_val_feedback_match_2 ============ ", every_val_feedback_match_2)
	print("every_quan_feedback_match_2 ============ ", every_quan_feedback_match_2)
	print("every_grade_feedback_match_2 ============ ", every_grade_feedback_match_2)
	feed_self.every_val_feedback_match = unique(every_val_feedback_match_2)
	feed_self.every_quan_feedback_match = unique(every_quan_feedback_match_2)
	feed_self.every_grade_feedback_match = unique(every_grade_feedback_match_2)
	#############################################
	#print("after con feed session")
	#print("feed_self.complex_ses")
	#print(feed_self.complex_ses)
	#print("feed_self.time_ses")
	#print(feed_self.time_ses)
	#print("feed_self.every_val_feedback_match")
	#print(feed_self.every_val_feedback_match)
	#print("feed_self.every_quan_feedback_match")
	#print(feed_self.every_quan_feedback_match)
	#print("feed_self.every_grade_feedback_match")
	#print(feed_self.every_grade_feedback_match)
