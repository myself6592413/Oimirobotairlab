# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/feedback_manager/fuzzy_logic' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/feedback_manager/fuzzy_logic')
import question_fuzzy_administrator as qfa
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef evaluate_feeds_question(selected_kid_age, selected_kid_level, selected_value_question, selected_time_spent, selected_correct_ans, 
	selected_indecisions, selected_interruptions):
	print(selected_kid_age)
	print(selected_kid_level)
	print(selected_value_question)
	print(selected_time_spent)
	print(selected_correct_ans)
	print(selected_indecisions)
	print(selected_interruptions)
	print(type(selected_kid_age))
	print(type(selected_kid_level))
	print(type(selected_value_question))
	print(type(selected_time_spent))
	print(type(selected_correct_ans))
	print(type(selected_indecisions))
	print(type(selected_interruptions))
	
	mamf = qfa.define_mf()
	result,fuzzy_result,conclusions = mamf.calculate({qfa.input1_kid_age: selected_kid_age, 
		qfa.input2_kid_level: selected_kid_level, 
		qfa.input3_value_question: selected_value_question, 
		qfa.input4_time_spent: selected_time_spent, 
		qfa.input5_correct_ans: selected_correct_ans, 
		qfa.input6_indecisions: selected_indecisions})
	print("result")
	print(result)

	out_val = [result[k] for k in result]
	value_output = out_val[0]
	print("value_output question ===========> ", value_output)
	gen_name_output = []
	name_output = []
	quantity_output = []
	
	if 0 < value_output <= 0.2:
		gen_name_output.append('Awful')
	if 0.2 < value_output <= 0.5:
		gen_name_output.append('Bad')
	if 0.3 < value_output <= 0.7:
		gen_name_output.append('Good')
	if 0.6 < value_output <= 0.8:
		gen_name_output.append('Excellent')
	if value_output > 0.8:
		gen_name_output.append('Perfect')
	if value_output==0:
		quantity_output.append('heavily')
		name_output.append('Awful')
	if 0 < value_output <= 0.08:
		quantity_output.append('heavily')
		name_output.append('Awful')
	if 0.08 < value_output <= 0.15:
		quantity_output.append('precisely')
		name_output.append('Awful')
	if 0.15 < value_output <= 0.2:
		quantity_output.append('marginally')
		name_output.append('Awful')
	if 0.1 < value_output <= 0.2:
		quantity_output.append('heavily')
		name_output.append('Bad')
	if 0.2 < value_output <= 0.3:
		quantity_output.append('precisely')
		name_output.append('Bad')
	if 0.3 < value_output <= 0.4:
		quantity_output.append('marginally')
		name_output.append('Bad')
	if 0.3 < value_output <= 0.4:
		quantity_output.append('marginally')
		name_output.append('Good')
	if 0.4 < value_output <= 0.6:
		quantity_output.append('precisely')
		name_output.append('Good')
	if 0.6 < value_output <= 0.7:
		quantity_output.append('heavily')
		name_output.append('Good')
	if 0.6 < value_output <= 0.7:
		quantity_output.append('marginally')
		name_output.append('Excellent')
	if 0.7 < value_output <= 0.8:
		quantity_output.append('precisely')
		name_output.append('Excellent')
	if 0.8 < value_output <= 0.9:
		quantity_output.append('heavily')    
		name_output.append('Excellent')
	if 0.8 < value_output <= 0.85:
		quantity_output.append('marginally')
		name_output.append('Perfect')
	if 0.85 < value_output <= 0.9:
		quantity_output.append('precisely')
		name_output.append('Perfect')
	if 0.9 < value_output <= 1:
		quantity_output.append('heavily')
		name_output.append('Perfect')

	print(name_output)
	print(quantity_output)

	total = 11 #total_num_question...

	if total > 9:
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[0]
			del quantity_output[0]
	print(quantity_output)
	print(name_output)

	return quantity_output,name_output,value_output

