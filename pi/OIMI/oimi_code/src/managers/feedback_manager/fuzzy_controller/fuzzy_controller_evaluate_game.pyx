# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/feedback_manager/fuzzy_logic' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/feedback_manager/fuzzy_logic')
import game_fuzzy_administrator as gfa
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef evaluate_feeds_game(how_many, selected_feed_questions, selected_game_diff, selected_time_perc_game):
	selected_feedback_ques1 = selected_feed_questions[0]
	if selected_game_diff[0]=='Easy_1':
		sel_game_diff = 1
	elif selected_game_diff[0]=='Easy_2':
		sel_game_diff = 2
	elif selected_game_diff[0]=='Normal_1':
		sel_game_diff = 3
	elif selected_game_diff[0]=='Normal_2':
		sel_game_diff = 4
	elif selected_game_diff[0]=='Medium_1':
		sel_game_diff = 5
	elif selected_game_diff[0]=='Medium_2':
		sel_game_diff = 6
	elif selected_game_diff[0]=='Hard_1':
		sel_game_diff = 7
	elif selected_game_diff[0]=='Hard_2':
		sel_game_diff = 8
	if len(selected_feed_questions)==1:
		mamf = gfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({gfa.input1_feedback_ques1: selected_feedback_ques1, 
			gfa.input5_game_difficulty: sel_game_diff, 
			gfa.input6_time_perc: selected_time_perc_game})

	if len(selected_feed_questions)==2:
		selected_feedback_ques1 = selected_feed_questions[0]
		selected_feedback_ques2 = selected_feed_questions[1]
		mamf = gfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({gfa.input1_feedback_ques1: selected_feedback_ques1, 
			gfa.input2_feedback_ques2: selected_feedback_ques2, 
			gfa.input5_game_difficulty: sel_game_diff, 
			gfa.input6_time_perc: selected_time_perc_game})

	if len(selected_feed_questions)==3:
		selected_feedback_ques1 = selected_feed_questions[0]
		selected_feedback_ques2 = selected_feed_questions[1]
		selected_feedback_ques3 = selected_feed_questions[2]
		mamf = gfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({gfa.input1_feedback_ques1: selected_feedback_ques1, 
			gfa.input2_feedback_ques2: selected_feedback_ques2, 
			gfa.input3_feedback_ques3: selected_feedback_ques3, 
			gfa.input5_game_difficulty: sel_game_diff, 
			gfa.input6_time_perc: selected_time_perc_game})

	if len(selected_feed_questions)==4:
		selected_feedback_ques1 = selected_feed_questions[0]
		selected_feedback_ques2 = selected_feed_questions[1]
		selected_feedback_ques3 = selected_feed_questions[2]
		selected_feedback_ques4 = selected_feed_questions[3]
		mamf = gfa.define_mf(how_many)
		result,fuzzy_result,conclusions = mamf.calculate({gfa.input1_feedback_ques1: selected_feedback_ques1, 
			gfa.input2_feedback_ques2: selected_feedback_ques2, 
			gfa.input3_feedback_ques3: selected_feedback_ques3, 
			gfa.input4_feedback_ques4: selected_feedback_ques4, 
			gfa.input5_game_difficulty: sel_game_diff, 
			gfa.input6_time_perc: selected_time_perc_game})

	out_val = [result[k] for k in result]
	value_output = out_val[0]
	print("value_output game ===========> ", value_output)
	gen_name_output = []
	name_output = []
	quantity_output = []
	if 0 < value_output <= 0.2:
		gen_name_output.append('Awful')
	if 0.2 < value_output <= 0.5:
		gen_name_output.append('Bad')
	if 0.3 < value_output <= 0.7:
		gen_name_output.append('Good')
	if 0.6 < value_output <= 0.8:
		gen_name_output.append('Excellent')
	if value_output > 0.8:
		gen_name_output.append('Perfect')
	if 0 < value_output <= 0.08:
		quantity_output.append('heavily')
		name_output.append('Awful')
	if 0.08 < value_output <= 0.15:
		quantity_output.append('precisely')
		name_output.append('Awful')
	if 0.15 < value_output <= 0.2:
		quantity_output.append('marginally')
		name_output.append('Awful')
	if 0.1 < value_output <= 0.2:
		quantity_output.append('heavily')
		name_output.append('Bad')
	if 0.2 < value_output <= 0.3:
		quantity_output.append('precisely')
		name_output.append('Bad')
	if 0.3 < value_output <= 0.4:
		quantity_output.append('marginally')
		name_output.append('Bad')
	if 0.3 < value_output <= 0.4:
		quantity_output.append('marginally')
		name_output.append('Good')
	if 0.4 < value_output <= 0.6:
		quantity_output.append('precisely')
		name_output.append('Good')
	if 0.6 < value_output <= 0.7:
		quantity_output.append('heavily')
		name_output.append('Good')
	if 0.6 < value_output <= 0.7:
		quantity_output.append('marginally')
		name_output.append('Excellent')
	if 0.7 < value_output <= 0.8:
		quantity_output.append('precisely')
		name_output.append('Excellent')
	if 0.8 < value_output <= 0.9:
		quantity_output.append('heavily')    
		name_output.append('Excellent')
	if 0.8 < value_output <= 0.85:
		quantity_output.append('marginally')
		name_output.append('Perfect')
	if 0.85 < value_output <= 0.9:
		quantity_output.append('precisely')
		name_output.append('Perfect')
	if 0.9 < value_output <= 1:
		quantity_output.append('heavily')
		name_output.append('Perfect')
	#####################################################################
	kind = '1F'
	fake_kind = '1F'

	if kind=='1F' or fake_kind=='1F': #remove greater
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[1]
			del quantity_output[1]
			if resp1 or resp2:
				quantity_output[0] = 'precisely'
			else:
				quantity_output[0] = 'heavily'

	elif kind=='2L' or fake_kind=='2L':#remove greater
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[1]
			del quantity_output[1]
			if resp1 or resp2:
				quantity_output[0] = 'precisely'
			else:
				quantity_output[0] = 'heavily'

	elif kind=='3S' or fake_kind=='3S':#remove greater
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[1]
			del quantity_output[1]
			if resp1 or resp2:
				quantity_output[0] = 'precisely'
			else:
				quantity_output[0] = 'heavily'

	elif kind=='4P' or fake_kind=='4P':#remove lower
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[0]
			del quantity_output[0]
			if resp1 or resp2:
				quantity_output[0] = 'precisely'

	elif kind=='5K' or fake_kind=='5K':#remove lower
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[0]
			del quantity_output[0]
			if resp1 or resp2:
				quantity_output[0] = 'marginally'
			else:
				quantity_output[0] = 'precisely'

	elif kind=='6I' or fake_kind=='6I':#remove lower
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[0]
			del quantity_output[0]
			if resp1:
				quantity_output[0] = 'marginally'
			if resp2:
				quantity_output[0] = 'precisely'
			else:
				quantity_output[0] = 'heavily'

	elif kind=='7O' or fake_kind=='7O':#remove lower
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[0]
			del quantity_output[0]
			if resp1 or resp2:
				quantity_output[0] = 'marginally'
			else:
				quantity_output[0] = 'precisely'

	elif kind=='8Q' or fake_kind=='8Q':
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[0]
			del quantity_output[0]
			if resp1:
				quantity_output[0] = 'marginally'
			if resp2:
				quantity_output[0] = 'precisely'
			else:
				quantity_output[0] = 'heavily'

	elif kind=='9C' or fake_kind=='9C': 
		resp1 = all(x in name_output for x in ['Awful', 'Bad'])
		resp2 = all(x in name_output for x in ['Bad', 'Good'])
		resp3 = all(x in name_output for x in ['Good', 'Excellent'])
		resp4 = all(x in name_output for x in ['Excellent', 'Perfect'])
		print("resp1", resp1)
		print("resp2", resp2)
		print("resp3", resp3)
		print("resp4", resp4)
		if resp1 or resp2 or resp3 or resp4:
			del name_output[0]
			del quantity_output[0]
			if resp1:
				quantity_output[0] = 'marginally'
			if resp2 or resp3:
				quantity_output[0] = 'precisely'
			else:
				quantity_output[0] = 'heavily'

	return quantity_output,name_output,value_output