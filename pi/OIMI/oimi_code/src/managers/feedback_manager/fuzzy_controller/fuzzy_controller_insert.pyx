# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/database_manager' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager')
from typing import Optional
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef insert_feeds_into_db_question(feed_feed_self):
	print("insert")
cdef insert_feeds_into_db_game(feed_self):
	print("insert")
cdef insert_feeds_into_db_match(feed_self):
	print("insert")
cdef insert_feeds_into_db_session(feed_self):
	print("insert")

