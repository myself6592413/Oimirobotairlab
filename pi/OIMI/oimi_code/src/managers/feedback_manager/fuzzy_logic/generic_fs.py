# ==========================================================================================================================================================
#  Imports:
# ==========================================================================================================================================================
from typing import List, Dict
from collections import defaultdict
from variables import FuzzyVariable
from rules import FuzzyRule
from terms import Term
from rules import Conditions, FuzzyCondition
from f_types import AndMethod, OrMethod, OperatorType, HedgeType
# ==========================================================================================================================================================
#  Class:
# ==========================================================================================================================================================
class GenericFuzzySystem:
    """ Fuzzy Model """
    def __init__(self,
                 inp: List[FuzzyVariable],
                 am: AndMethod = AndMethod.PROD,
                 om: OrMethod = OrMethod.MAX):
        self.inp: List[FuzzyVariable] = inp
        self.rules: List[FuzzyRule] = []
        self.and_method: AndMethod = am
        self.or_method: OrMethod = om

    def input_by_name(self, name: str) -> FuzzyVariable:
        """ Search param by name """
        for inp in self.inp:
            if inp.name == name:
                return inp
        raise Exception(f'Output variable called "{name}" not found')

    def evaluate_conditions(self, fi: Dict[FuzzyVariable, Dict[Term, float]]) -> Dict[FuzzyRule, float]:
        """ Calculate conclusion according to fuzzy rules """
        return {rule: self.evaluate_condition(rule.condition, fi) for rule in self.rules}

    def fuzzify(self, inp: Dict[FuzzyVariable, float]) -> Dict[FuzzyVariable, Dict[Term, float]]:
        """ Transform into fuzzy values """
        self.validate_input_values(inp)
        result: Dict[FuzzyVariable, Dict[Term, float]] = defaultdict(Dict[Term, float])
        for variable in self.inp:
            result[variable] = defaultdict(float)
            for term in variable.terms:
                result[variable][term] = term.mf.get_value(inp[variable])
        return result

    def evaluate_condition(self,
                           condition: [Conditions, FuzzyCondition],
                           fi: Dict[FuzzyVariable, Dict[Term, float]]) -> float:
        """ Calculate condition """
        if isinstance(condition, Conditions):
            if len(condition.conditions) == 0: 
                raise Exception('No states')
            elif len(condition.conditions) == 1:
                result: float = self.evaluate_condition(condition.conditions[0], fi)
            else:
                result: float = self.evaluate_condition(condition.conditions[0], fi)
                for i in range(1, len(condition.conditions)):
                    result = self.evaluate_condition_pair(
                        result,
                        self.evaluate_condition(condition.conditions[i], fi),
                        condition.op)
            return 1.0 - result if condition.not_ else result
        elif isinstance(condition, FuzzyCondition):
            result: float = fi[condition.variable][condition.term.term]
            # Applying the modificator
            if condition.hedge == HedgeType.SLIGHTLY:
                result = result ** (1. / 3.)
            elif condition.hedge == HedgeType.SOMEWHAT:
                result = result ** .5
            elif condition.hedge == HedgeType.VERY:
                result *= result
            elif condition.hedge == HedgeType.EXTREMELY:
                result = result ** 3
            return 1.0 - result if condition.not_ else result
        else:
            raise Exception('Nessuna condizione trovata nella regola fuzzy')

    def evaluate_condition_pair(self, condition1: float, condition2: float, op: OperatorType) -> float:
        if op == OperatorType.AND:
            if self.and_method == AndMethod.MIN:
                return min(condition1, condition2)
            elif self.and_method == AndMethod.PROD:
                return condition1 * condition2
            else:
                raise Exception('AND Operator not found')
        elif op == OperatorType.OR:
            if self.or_method == OrMethod.MAX:
                return max(condition1, condition2)
            elif self.or_method == OrMethod.PROB:
                return condition1 + condition2 - condition1 * condition2
            else:
                raise Exception('OR Operator not found')
        else:
            raise Exception('Indicated Operator type not exist')

    def validate_input_values(self, inp: Dict[FuzzyVariable, float]):
        """ Check validity input variables """
        if len(inp) != len(self.inp):
            raise Exception('The number of inputs in not correct')
        for variable in self.inp:
            if variable in inp:
                value: float = inp[variable]
                if not variable.min_value <= value <= variable.max_value:
                    raise Exception('Variable values outside of allowed boundaries')
            else:
                raise Exception(f'The value {variable.name} not exists')