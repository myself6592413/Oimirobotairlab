""" Just for plotting the fuzzy variables"""
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt  
from pylab import show
########################################################################################################### in case of 4 questions
x_feedback_game = np.arange(0, 1.1, 0.1)
x_time_perc = np.arange(0, 2.1, 0.1)
x_num_questions_total = np.arange(1,17, 1)
################################################################ INPUTS 
feedback_game1_input_awful = fuzz.trapmf(x_feedback_game,[0, 0, 0.1, 0.2])
feedback_game1_input_bad = fuzz.trapmf(x_feedback_game,[0.1,0.2,0.3,0.4])
feedback_game1_input_good = fuzz.trapmf(x_feedback_game,[0.3,0.4,0.6,0.7])
feedback_game1_input_excellent = fuzz.trapmf(x_feedback_game,[0.6,0.7,0.8,0.9])
feedback_game1_input_perfect = fuzz.trapmf(x_feedback_game,[0.8,0.9,1,1])

feedback_game2_input_awful = fuzz.trapmf(x_feedback_game,[0, 0, 0.1, 0.2])
feedback_game2_input_bad = fuzz.trapmf(x_feedback_game,[0.1,0.2,0.3,0.4])
feedback_game2_input_good = fuzz.trapmf(x_feedback_game,[0.3,0.4,0.6,0.7])
feedback_game2_input_excellent = fuzz.trapmf(x_feedback_game,[0.6,0.7,0.8,0.9])
feedback_game2_input_perfect = fuzz.trapmf(x_feedback_game,[0.8,0.9,1,1])

feedback_game3_input_awful = fuzz.trapmf(x_feedback_game,[0, 0, 0.1, 0.2])
feedback_game3_input_bad = fuzz.trapmf(x_feedback_game,[0.1,0.2,0.3,0.4])
feedback_game3_input_good = fuzz.trapmf(x_feedback_game,[0.3,0.4,0.6,0.7])
feedback_game3_input_excellent = fuzz.trapmf(x_feedback_game,[0.6,0.7,0.8,0.9])
feedback_game3_input_perfect = fuzz.trapmf(x_feedback_game,[0.8,0.9,1,1])

feedback_game4_input_awful = fuzz.trapmf(x_feedback_game,[0, 0, 0.1, 0.2])
feedback_game4_input_bad = fuzz.trapmf(x_feedback_game,[0.1,0.2,0.3,0.4])
feedback_game4_input_good = fuzz.trapmf(x_feedback_game,[0.3,0.4,0.6,0.7])
feedback_game4_input_excellent = fuzz.trapmf(x_feedback_game,[0.6,0.7,0.8,0.9])
feedback_game4_input_perfect = fuzz.trapmf(x_feedback_game,[0.8,0.9,1,1])

feedback_match_output_awful = fuzz.trapmf(x_feedback_game,[0, 0, 0.1, 0.2])
feedback_match_output_bad = fuzz.trapmf(x_feedback_game,[0.1,0.2,0.3,0.4])
feedback_match_output_good = fuzz.trapmf(x_feedback_game,[0.3,0.4,0.6,0.7])
feedback_match_output_excellent = fuzz.trapmf(x_feedback_game,[0.6,0.7,0.8,0.9])
feedback_match_output_perfect = fuzz.trapmf(x_feedback_game,[0.8,0.9,1,1])

num_questions_few = fuzz.trapmf(x_num_questions_total,[1,1,4,5])
num_questions_enough = fuzz.trapmf(x_num_questions_total,[4,5,8,9])
num_questions_some = fuzz.trapmf(x_num_questions_total,[8,9,12,13])
num_questions_many = fuzz.trapmf(x_num_questions_total,[12,13,16,16])

advisable_time_output_quick = fuzz.trapmf(x_time_perc,[0, 0, 0.7, 0.8])
advisable_time_output_reasonable = fuzz.trapmf(x_time_perc,[0.7,0.8,1.1,1.2])
advisable_time_output_acceptable = fuzz.trapmf(x_time_perc,[1,1.2,1.3,1.5])
advisable_time_output_slow = fuzz.trapmf(x_time_perc,[1.3,1.5,1.7,1.8])
advisable_time_output_too_much = fuzz.trapmf(x_time_perc,[1.7,1.8,2,2])


"""
fig, (ax1, ax2, ax3, ax4) = plt.subplots(4,figsize=(10, 15))

ax1.set_title('Feedback_game 1 ',loc='left')  
ax1.plot(x_feedback_game, feedback_game1_input_awful, 'gold', linewidth=1.5, label='Awful')
ax1.plot(x_feedback_game, feedback_game1_input_bad, 'black', linewidth=1.5, label='Bad')
ax1.plot(x_feedback_game, feedback_game1_input_good, 'lightcoral', linewidth=1.5, label='Good')
ax1.plot(x_feedback_game, feedback_game1_input_excellent, 'navy', linewidth=1.5, label='Excellent')
ax1.plot(x_feedback_game, feedback_game1_input_perfect, 'palegreen', linewidth=1.5, label='Perfect')
ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax2.set_title('Feedback_game 2 ',loc='left')  
ax2.plot(x_feedback_game, feedback_game2_input_awful, 'gold', linewidth=1.5, label='Awful')
ax2.plot(x_feedback_game, feedback_game2_input_bad, 'black', linewidth=1.5, label='Bad')
ax2.plot(x_feedback_game, feedback_game2_input_good, 'lightcoral', linewidth=1.5, label='Good')
ax2.plot(x_feedback_game, feedback_game2_input_excellent, 'navy', linewidth=1.5, label='Excellent')
ax2.plot(x_feedback_game, feedback_game2_input_perfect, 'palegreen', linewidth=1.5, label='Perfect')
ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax3.set_title('Feedback_game 3',loc='left')  
ax3.plot(x_feedback_game, feedback_game3_input_awful, 'gold', linewidth=1.5, label='Awful')
ax3.plot(x_feedback_game, feedback_game3_input_bad, 'black', linewidth=1.5, label='Bad')
ax3.plot(x_feedback_game, feedback_game3_input_good, 'lightcoral', linewidth=1.5, label='Good')
ax3.plot(x_feedback_game, feedback_game3_input_excellent, 'navy', linewidth=1.5, label='Excellent')
ax3.plot(x_feedback_game, feedback_game3_input_perfect, 'palegreen', linewidth=1.5, label='Perfect')
ax3.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax4.set_title('Feedback_game 4',loc='left')  
ax4.plot(x_feedback_game, feedback_game4_input_awful, 'gold', linewidth=1.5, label='Awful')
ax4.plot(x_feedback_game, feedback_game4_input_bad, 'black', linewidth=1.5, label='Bad')
ax4.plot(x_feedback_game, feedback_game4_input_good, 'lightcoral', linewidth=1.5, label='Good')
ax4.plot(x_feedback_game, feedback_game4_input_excellent, 'navy', linewidth=1.5, label='Excellent')
ax4.plot(x_feedback_game, feedback_game4_input_perfect, 'palegreen', linewidth=1.5, label='Perfect')
ax4.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

for ax in (ax1, ax2, ax3, ax4):
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)
  ax.get_xaxis().tick_bottom()
  ax.get_yaxis().tick_left()
  plt.tight_layout()

fig.savefig('Fuzzy_match_1_2_3_4.png')
plt.close()

"""
fig2, (ax0, ax5, ax6, ax7) = plt.subplots(4,figsize=(10, 15))

ax0.set_title('Match difficulty', loc='left')
ax0.axvline(x=1, color='lime', label='Easy_1')
ax0.axvline(x=2, color='black', label='Easy_2')
ax0.axvline(x=3, color='cyan', label='Normal_1')
ax0.axvline(x=4, color='magenta', label='Normal_2')
ax0.axvline(x=5, color='darkorange', label='Medium_1')
ax0.axvline(x=6, color='blue', label='Medium_2')
ax0.axvline(x=7, color='goldenrod', label='Hard_1')
ax0.axvline(x=8, color='red', label='Hard_2')
ax0.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)

ax5.set_title('Time passed',loc='left')  
ax5.plot(x_time_perc, advisable_time_output_quick, 'fuchsia', linewidth=1.5, label='Quick')
ax5.plot(x_time_perc, advisable_time_output_reasonable, 'black', linewidth=1.5, label='Reasonable')
ax5.plot(x_time_perc, advisable_time_output_acceptable, 'red', linewidth=1.5, label='Acceptable')
ax5.plot(x_time_perc, advisable_time_output_slow, 'lime', linewidth=1.5, label='Slow')
ax5.plot(x_time_perc, advisable_time_output_too_much, 'blueviolet', linewidth=1.5, label='Too_much')
ax5.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)

ax6.set_title('Total questions',loc='left')  
ax6.plot(x_num_questions_total, num_questions_few, 'blue', linewidth=1.5, label='Few')
ax6.plot(x_num_questions_total, num_questions_enough, 'black', linewidth=1.5, label='Enough')
ax6.plot(x_num_questions_total, num_questions_some, 'green', linewidth=1.5, label='Some')
ax6.plot(x_num_questions_total, num_questions_many, 'grey', linewidth=1.5, label='Many')
ax6.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)

ax7.set_title('Feedback_match output',loc='left')  
ax7.plot(x_feedback_game, feedback_match_output_awful, 'gold', linewidth=1.5, label='Awful')
ax7.plot(x_feedback_game, feedback_match_output_bad, 'seagreen', linewidth=1.5, label='Bad')
ax7.plot(x_feedback_game, feedback_match_output_good, 'cyan', linewidth=1.5, label='Good')
ax7.plot(x_feedback_game, feedback_match_output_excellent, 'fuchsia', linewidth=1.5, label='Excellent')
ax7.plot(x_feedback_game, feedback_match_output_perfect, 'slategray', linewidth=1.5, label='Perfect')
ax7.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)

#plt.subplots_adjust(hspace=1.5)

for ax in (ax0, ax5, ax6, ax7):
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)
  ax.get_xaxis().tick_bottom()
  ax.get_yaxis().tick_left()
  plt.tight_layout()

fig2.savefig('new_Fuzzy_match_0_5.png')
plt.close()

show()
