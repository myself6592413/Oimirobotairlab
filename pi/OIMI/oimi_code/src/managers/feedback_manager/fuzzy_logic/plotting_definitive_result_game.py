""" Just for plotting the fuzzy variables"""
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt  
from pylab import show

########################################################################################################### in case of 4 questions
x_feedback_question = np.arange(0, 1.1, 0.1)
x_time_perc = np.arange(0, 2.1, 0.1)
################################################################ INPUTS 
feedback_question1_input_awful = fuzz.trapmf(x_feedback_question,[0, 0, 0.1, 0.2])
feedback_question1_input_bad = fuzz.trapmf(x_feedback_question,[0.1,0.2,0.3,0.4])
feedback_question1_input_good = fuzz.trapmf(x_feedback_question,[0.3,0.4,0.6,0.7])
feedback_question1_input_excellent = fuzz.trapmf(x_feedback_question,[0.6,0.7,0.8,0.9])
feedback_question1_input_perfect = fuzz.trapmf(x_feedback_question,[0.8,0.9,1,1])

feedback_question2_input_awful = fuzz.trapmf(x_feedback_question,[0, 0, 0.1, 0.2])
feedback_question2_input_bad = fuzz.trapmf(x_feedback_question,[0.1,0.2,0.3,0.4])
feedback_question2_input_good = fuzz.trapmf(x_feedback_question,[0.3,0.4,0.6,0.7])
feedback_question2_input_excellent = fuzz.trapmf(x_feedback_question,[0.6,0.7,0.8,0.9])
feedback_question2_input_perfect = fuzz.trapmf(x_feedback_question,[0.8,0.9,1,1])

feedback_question3_input_awful = fuzz.trapmf(x_feedback_question,[0, 0, 0.1, 0.2])
feedback_question3_input_bad = fuzz.trapmf(x_feedback_question,[0.1,0.2,0.3,0.4])
feedback_question3_input_good = fuzz.trapmf(x_feedback_question,[0.3,0.4,0.6,0.7])
feedback_question3_input_excellent = fuzz.trapmf(x_feedback_question,[0.6,0.7,0.8,0.9])
feedback_question3_input_perfect = fuzz.trapmf(x_feedback_question,[0.8,0.9,1,1])

feedback_question4_input_awful = fuzz.trapmf(x_feedback_question,[0, 0, 0.1, 0.2])
feedback_question4_input_bad = fuzz.trapmf(x_feedback_question,[0.1,0.2,0.3,0.4])
feedback_question4_input_good = fuzz.trapmf(x_feedback_question,[0.3,0.4,0.6,0.7])
feedback_question4_input_excellent = fuzz.trapmf(x_feedback_question,[0.6,0.7,0.8,0.9])
feedback_question4_input_perfect = fuzz.trapmf(x_feedback_question,[0.8,0.9,1,1])

feedback_game_output_awful = fuzz.trapmf(x_feedback_question,[0, 0, 0.1, 0.2])
feedback_game_output_bad = fuzz.trapmf(x_feedback_question,[0.1,0.2,0.3,0.4])
feedback_game_output_good = fuzz.trapmf(x_feedback_question,[0.3,0.4,0.6,0.7])
feedback_game_output_excellent = fuzz.trapmf(x_feedback_question,[0.6,0.7,0.8,0.9])
feedback_game_output_perfect = fuzz.trapmf(x_feedback_question,[0.8,0.9,1,1])

necessary_time_output_quick = fuzz.trapmf(x_time_perc,[0, 0, 0.7, 0.8])
necessary_time_output_reasonable = fuzz.trapmf(x_time_perc,[0.7,0.8,1.1,1.2])
necessary_time_output_acceptable = fuzz.trapmf(x_time_perc,[1,1.2,1.3,1.5])
necessary_time_output_slow = fuzz.trapmf(x_time_perc,[1.3,1.5,1.7,1.8])
necessary_time_output_too_much = fuzz.trapmf(x_time_perc,[1.7,1.8,2,2])

"""
fig, (ax1, ax2, ax3, ax4) = plt.subplots(4,figsize=(10, 15))

ax1.set_title('Feedback_question 1 ',loc='left')  
ax1.plot(x_feedback_question, feedback_question1_input_awful, 'g', linewidth=1.5, label='Awful')
ax1.plot(x_feedback_question, feedback_question1_input_bad, 'b', linewidth=1.5, label='Bad')
ax1.plot(x_feedback_question, feedback_question1_input_good, 'c', linewidth=1.5, label='Good')
ax1.plot(x_feedback_question, feedback_question1_input_excellent, 'y', linewidth=1.5, label='Excellent')
ax1.plot(x_feedback_question, feedback_question1_input_perfect, 'r', linewidth=1.5, label='Perfect')
ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax2.set_title('Feedback_question 2 ',loc='left')  
ax2.plot(x_feedback_question, feedback_question2_input_awful, 'g', linewidth=1.5, label='Awful')
ax2.plot(x_feedback_question, feedback_question2_input_bad, 'b', linewidth=1.5, label='Bad')
ax2.plot(x_feedback_question, feedback_question2_input_good, 'c', linewidth=1.5, label='Good')
ax2.plot(x_feedback_question, feedback_question2_input_excellent, 'y', linewidth=1.5, label='Excellent')
ax2.plot(x_feedback_question, feedback_question2_input_perfect, 'r', linewidth=1.5, label='Perfect')
ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax3.set_title('Feedback_question 3',loc='left')  
ax3.plot(x_feedback_question, feedback_question3_input_awful, 'g', linewidth=1.5, label='Awful')
ax3.plot(x_feedback_question, feedback_question3_input_bad, 'b', linewidth=1.5, label='Bad')
ax3.plot(x_feedback_question, feedback_question3_input_good, 'c', linewidth=1.5, label='Good')
ax3.plot(x_feedback_question, feedback_question3_input_excellent, 'y', linewidth=1.5, label='Excellent')
ax3.plot(x_feedback_question, feedback_question3_input_perfect, 'r', linewidth=1.5, label='Perfect')
ax3.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax4.set_title('Feedback_question 4',loc='left')  
ax4.plot(x_feedback_question, feedback_question4_input_awful, 'g', linewidth=1.5, label='Awful')
ax4.plot(x_feedback_question, feedback_question4_input_bad, 'b', linewidth=1.5, label='Bad')
ax4.plot(x_feedback_question, feedback_question4_input_good, 'c', linewidth=1.5, label='Good')
ax4.plot(x_feedback_question, feedback_question4_input_excellent, 'y', linewidth=1.5, label='Excellent')
ax4.plot(x_feedback_question, feedback_question4_input_perfect, 'r', linewidth=1.5, label='Perfect')
ax4.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

for ax in (ax1, ax2, ax3, ax4):
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)
  ax.get_xaxis().tick_bottom()
  ax.get_yaxis().tick_left()
  plt.tight_layout()

fig.savefig('Fuzzy_game_1_2_3_4.png')
plt.close()
"""
"""
fig2, (ax0, ax5, ax6) = plt.subplots(3,figsize=(10, 15))

ax0.set_title('Game difficulty', loc='left')
ax0.axvline(x=1, color='lime', label='Easy_1')
ax0.axvline(x=2, color='darkorange', label='Easy_2')
ax0.axvline(x=3, color='cyan', label='Normal_1')
ax0.axvline(x=4, color='magenta', label='Normal_2')
ax0.axvline(x=5, color='green', label='Medium_1')
ax0.axvline(x=6, color='pink', label='Medium_2')
ax0.axvline(x=7, color='gold', label='Hard_1')
ax0.axvline(x=8, color='red', label='Hard_2')
ax0.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)

ax5.set_title('Time spent',loc='left')  
ax5.plot(x_time_perc, necessary_time_output_quick, 'turquoise', linewidth=1.5, label='Quick')
ax5.plot(x_time_perc, necessary_time_output_reasonable, 'black', linewidth=1.5, label='Reasonable')
ax5.plot(x_time_perc, necessary_time_output_acceptable, 'red', linewidth=1.5, label='Acceptable')
ax5.plot(x_time_perc, necessary_time_output_slow, 'lime', linewidth=1.5, label='Slow')
ax5.plot(x_time_perc, necessary_time_output_too_much, 'blueviolet', linewidth=1.5, label='Too_much')
ax5.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)

ax6.set_title('Feedback_game ouput',loc='left')  
ax6.plot(x_feedback_question, feedback_game_output_awful, 'maroon', linewidth=1.5, label='Awful')
ax6.plot(x_feedback_question, feedback_game_output_bad, 'fuchsia', linewidth=1.5, label='Bad')
ax6.plot(x_feedback_question, feedback_game_output_good, 'orange', linewidth=1.5, label='Good')
ax6.plot(x_feedback_question, feedback_game_output_excellent, 'green', linewidth=1.5, label='Excellent')
ax6.plot(x_feedback_question, feedback_game_output_perfect, 'darkviolet', linewidth=1.5, label='Perfect')
ax6.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)

plt.subplots_adjust(hspace=1.5)

fig2.savefig('Fuzzy_game_0_5.png')
plt.close()
"""

############################# new!!! groups of 4 mf in one page

fig2, (ax0, ax5, ax4, ax6) = plt.subplots(4,figsize=(10, 15))

ax0.set_title('Game difficulty', loc='left')
ax0.axvline(x=1, color='lime', label='Easy_1')
ax0.axvline(x=2, color='darkorange', label='Easy_2')
ax0.axvline(x=3, color='cyan', label='Normal_1')
ax0.axvline(x=4, color='magenta', label='Normal_2')
ax0.axvline(x=5, color='green', label='Medium_1')
ax0.axvline(x=6, color='pink', label='Medium_2')
ax0.axvline(x=7, color='gold', label='Hard_1')
ax0.axvline(x=8, color='red', label='Hard_2')
ax0.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)

ax5.set_title('Time spent',loc='left')  
ax5.plot(x_time_perc, necessary_time_output_quick, 'turquoise', linewidth=1.5, label='Quick')
ax5.plot(x_time_perc, necessary_time_output_reasonable, 'black', linewidth=1.5, label='Reasonable')
ax5.plot(x_time_perc, necessary_time_output_acceptable, 'red', linewidth=1.5, label='Acceptable')
ax5.plot(x_time_perc, necessary_time_output_slow, 'lime', linewidth=1.5, label='Slow')
ax5.plot(x_time_perc, necessary_time_output_too_much, 'blueviolet', linewidth=1.5, label='Too_much')
ax5.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)

ax6.set_title('Feedback_game ouput',loc='left')  
ax6.plot(x_feedback_question, feedback_game_output_awful, 'maroon', linewidth=1.5, label='Awful')
ax6.plot(x_feedback_question, feedback_game_output_bad, 'fuchsia', linewidth=1.5, label='Bad')
ax6.plot(x_feedback_question, feedback_game_output_good, 'orange', linewidth=1.5, label='Good')
ax6.plot(x_feedback_question, feedback_game_output_excellent, 'green', linewidth=1.5, label='Excellent')
ax6.plot(x_feedback_question, feedback_game_output_perfect, 'darkviolet', linewidth=1.5, label='Perfect')
ax6.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)


ax4.set_title('Feedback_question 4',loc='left')  
ax4.plot(x_feedback_question, feedback_question4_input_awful, 'g', linewidth=1.5, label='Awful')
ax4.plot(x_feedback_question, feedback_question4_input_bad, 'b', linewidth=1.5, label='Bad')
ax4.plot(x_feedback_question, feedback_question4_input_good, 'c', linewidth=1.5, label='Good')
ax4.plot(x_feedback_question, feedback_question4_input_excellent, 'y', linewidth=1.5, label='Excellent')
ax4.plot(x_feedback_question, feedback_question4_input_perfect, 'r', linewidth=1.5, label='Perfect')
ax4.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

#for ax in (ax0, ax5, ax4, ax6):
#  ax.spines['top'].set_visible(False)
#  ax.spines['right'].set_visible(False)
#  ax.get_xaxis().tick_bottom()
#  ax.get_yaxis().tick_left()
#  plt.tight_layout()

plt.subplots_adjust(hspace=1.5)
fig2.savefig('new_GAMEFuzzy_0_5_4_6.png')
plt.close()

show()