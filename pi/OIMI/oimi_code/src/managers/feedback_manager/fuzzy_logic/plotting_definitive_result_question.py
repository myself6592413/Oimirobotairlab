""" Just for plotting the fuzzy variables"""
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt  
from pylab import show
# ==========================================================================================================================================================
# Vars
# ==========================================================================================================================================================
#Inputs
x_age= np.arange(5, 11, 1)
#level singleton
x_value_question = np.arange(1, 10.5, 0.5)  
x_time = np.arange(1, 15.5, 0.5)  
x_corrects = np.arange(0, 1.1, 0.1)
x_indecisions = np.arange(0, 5.5, 0.5)
x_interruptions = np.arange(0, 5.5, 0.5)
#Output
x_feedback_question = np.arange(0, 1.1, 0.1)

################################################################ INPUTS 
age_baby = fuzz.trapmf(x_age,[5, 5, 5, 7]) 
age_infant = fuzz.trapmf(x_age,[6, 7, 8, 9])
age_junior = fuzz.trapmf(x_age,[8, 10, 10, 10])

value_low = fuzz.trapmf(x_value_question,[1, 1, 2, 3])
value_moderate = fuzz.trapmf(x_value_question,[2, 3, 4.5, 5])
value_significant = fuzz.trapmf(x_value_question,[4.5, 6, 7, 8])
value_high = fuzz.trapmf(x_value_question,[7, 8, 10, 10.5])

time_quick = fuzz.trapmf(x_time,[0,0,4.5,5])
time_reasonable = fuzz.trapmf(x_time,[4,5,8,9.5])
time_slow = fuzz.trapmf(x_time,[8.5,10,12,13])
time_too_much = fuzz.trapmf(x_time,[12,13,15,15.5])

n_corrects_small = fuzz.trapmf(x_corrects,[0, 0, 0.35, 0.4]) 
n_corrects_acceptable = fuzz.trapmf(x_corrects,[0.35,0.4,0.55,0.6])
n_corrects_fine = fuzz.trapmf(x_corrects,[0.55,0.6,0.8,0.85])
n_corrects_big = fuzz.trapmf(x_corrects,[0.8,0.9,1,1])

n_indecisions_none = fuzz.trapmf(x_indecisions, [0,0,0,1])
n_indecisions_minor = fuzz.trapmf(x_indecisions, [0.5,1,2,3])
n_indecisions_some = fuzz.trapmf(x_indecisions, [2,3,5,5.5])

n_interruptions_none = fuzz.trapmf(x_interruptions,[0,0,0,1])
n_interruptions_minor = fuzz.trapmf(x_interruptions,[0.5,1,2,3])
n_interruptions_some = fuzz.trapmf(x_interruptions,[2,3,5,5.5])

feedback_question_output_awful = fuzz.trapmf(x_feedback_question,[0, 0, 0.1, 0.2])
feedback_question_output_bad = fuzz.trapmf(x_feedback_question,[0.1,0.2,0.3,0.4])
feedback_question_output_good = fuzz.trapmf(x_feedback_question,[0.3,0.4,0.6,0.7])
feedback_question_output_excellent = fuzz.trapmf(x_feedback_question,[0.6,0.7,0.8,0.9])
feedback_question_output_perfect = fuzz.trapmf(x_feedback_question,[0.8,0.9,1,1])

# ==========================================================================================================================================================
# Vars
# ==========================================================================================================================================================
fig, (ax1, ax2, ax3, ax4) = plt.subplots(4,figsize=(10, 15))

ax1.set_title('Level child', loc='left')
ax1.axvline(x=1, color='lime', label='Beginner')
ax1.axvline(x=2, color='darkorange', label='Elementary')
ax1.axvline(x=3, color='cyan', label='Intermediate')
ax1.axvline(x=4, color='magenta', label='Adavanced')
ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax2.set_title('Age child', loc='left')
ax2.plot(x_age, age_baby, 'b', linewidth=1.5, label='Baby')  
ax2.plot(x_age, age_infant, 'g', linewidth=1.5, label='Infant')  
ax2.plot(x_age, age_junior, 'r', linewidth=1.5, label='Junior')  
ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax3.set_title('Value question (intrinsic difficulty)',loc='left')  
ax3.plot(x_value_question, value_low, 'darkblue', linewidth=1.5, label='Low')  
ax3.plot(x_value_question, value_moderate, 'lightsteelblue', linewidth=1.5, label='Moderate')  
ax3.plot(x_value_question, value_significant, 'darkviolet', linewidth=1.5, label='Significant')  
ax3.plot(x_value_question, value_high, 'pink', linewidth=1.5, label='High')  
ax3.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax4.set_title('Time passed', loc='left')  
ax4.plot(x_time, time_quick, 'yellow', linewidth=1.5, label='Quick')  
ax4.plot(x_time, time_reasonable, 'darkorange', linewidth=1.5, label='Reasonable')  
ax4.plot(x_time, time_slow, 'saddlebrown', linewidth=1.5, label='Slow')  
ax4.plot(x_time, time_too_much, 'gold', linewidth=1.5, label='Too_much')  
ax4.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

show()
#for ax in (ax1, ax2, ax3, ax4):
#  ax.spines['top'].set_visible(False)
#  ax.spines['right'].set_visible(False)
#  ax.get_xaxis().tick_bottom()
#  ax.get_yaxis().tick_left()
#  plt.tight_layout()

#plt.subplots_adjust(hspace=4)
#plt.subplots_adjust(top=2)

# Put a legend below current axis
#ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
#          fancybox=True, shadow=True, ncol=5)


fig.savefig('new_Fuzzy_plots_1_2_3_4_question.png')
plt.close()
"""

fig2, (ax5, ax6, ax7, ax8) = plt.subplots(4,figsize=(10, 15))

ax5.set_title('Correct responses percentage',loc='left') 
ax5.plot(x_corrects, n_corrects_small, 'y', linewidth=1.5, label='Small')  
ax5.plot(x_corrects, n_corrects_acceptable, 'b', linewidth=1.5, label='Acceptable')  
ax5.plot(x_corrects, n_corrects_fine, 'g', linewidth=1.5, label='Fine')  
ax5.plot(x_corrects, n_corrects_big, 'r', linewidth=1.5, label='Big')  
ax5.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax6.set_title('Indecision',loc='left')
ax6.plot(x_indecisions, n_indecisions_none, 'green', linewidth=1.5, label='None')  
ax6.plot(x_indecisions, n_indecisions_minor, 'darkolivegreen', linewidth=1.5, label='Minor')  
ax6.plot(x_indecisions, n_indecisions_some, 'lime', linewidth=1.5, label='Some')  
ax6.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax7.set_title('Interruptions',loc='left')
ax7.plot(x_interruptions, n_interruptions_none, 'black', linewidth=1.5, label='None')  
ax7.plot(x_interruptions, n_interruptions_minor, 'gray', linewidth=1.5, label='Minor')  
ax7.plot(x_interruptions, n_interruptions_some, 'saddlebrown', linewidth=1.5, label='Some')  
ax7.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax8.set_title('Kind of question', loc='left')
ax8.axvline(x=1, color='darkorange', label='1F')
ax8.axvline(x=2, color='cyan', label='2L')
ax8.axvline(x=3, color='lawngreen', label='3S')
ax8.axvline(x=4, color='navy', label='4P')
ax8.axvline(x=5, color='magenta', label='5K')
ax8.axvline(x=6, color='olive', label='6I')
ax8.axvline(x=7, color='pink', label='7O')
ax8.axvline(x=8, color='darkviolet', label='8Q')
ax8.axvline(x=9, color='maroon', label='9C')
ax8.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

plt.subplots_adjust(hspace=2)

#for ax in (ax5, ax6, ax7, ax8):
#  ax.spines['top'].set_visible(False)
#  ax.spines['right'].set_visible(False)
#  ax.get_xaxis().tick_bottom()
#  ax.get_yaxis().tick_left()
#  plt.tight_layout()

fig2.savefig('NEW_QUIFuzzy5_6_7_8.png')
plt.close()


"""
"""
fig3, (ax9) = plt.subplots(1,figsize=(10, 15))

ax9.set_title('Feedback_question output',loc='left')  
ax9.plot(x_feedback_question, feedback_question_output_awful, 'g', linewidth=1.5, label='Awful')
ax9.plot(x_feedback_question, feedback_question_output_bad, 'b', linewidth=1.5, label='Bad')
ax9.plot(x_feedback_question, feedback_question_output_good, 'c', linewidth=1.5, label='Good')
ax9.plot(x_feedback_question, feedback_question_output_excellent, 'y', linewidth=1.5, label='Excellent')
ax9.plot(x_feedback_question, feedback_question_output_perfect, 'r', linewidth=1.5, label='Perfect')
ax9.legend(loc='upper center', bbox_to_anchor=(0.5, -0.075),
          fancybox=True, shadow=True, ncol=5)

ax9.spines['top'].set_visible(False)
ax9.spines['right'].set_visible(False)
ax9.get_xaxis().tick_bottom()
ax9.get_yaxis().tick_left()

fig3.savefig('Fuzzy9_output.png')
plt.close()

from pylab import show
show()
"""
"""

#for showing little figure output not alone....copy other mem function input and then cut image manually


fig3, (ax5, ax6, ax7, ax9) = plt.subplots(4,figsize=(10, 15))

ax5.set_title('Correct responses percentage',loc='left') 
ax5.plot(x_corrects, n_corrects_small, 'y', linewidth=1.5, label='Small')  
ax5.plot(x_corrects, n_corrects_acceptable, 'b', linewidth=1.5, label='Acceptable')  
ax5.plot(x_corrects, n_corrects_fine, 'g', linewidth=1.5, label='Fine')  
ax5.plot(x_corrects, n_corrects_big, 'r', linewidth=1.5, label='Big')  
ax5.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax6.set_title('Indecision',loc='left')
ax6.plot(x_indecisions, n_indecisions_none, 'green', linewidth=1.5, label='None')  
ax6.plot(x_indecisions, n_indecisions_minor, 'darkolivegreen', linewidth=1.5, label='Minor')  
ax6.plot(x_indecisions, n_indecisions_some, 'lime', linewidth=1.5, label='Some')  
ax6.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)

ax7.set_title('Interruptions',loc='left')
ax7.plot(x_interruptions, n_interruptions_none, 'black', linewidth=1.5, label='None')  
ax7.plot(x_interruptions, n_interruptions_minor, 'gray', linewidth=1.5, label='Minor')  
ax7.plot(x_interruptions, n_interruptions_some, 'saddlebrown', linewidth=1.5, label='Some')  
ax7.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)


ax9.set_title('Feedback_question output',loc='left')  
ax9.plot(x_feedback_question, feedback_question_output_awful, 'g', linewidth=1.5, label='Awful')
ax9.plot(x_feedback_question, feedback_question_output_bad, 'b', linewidth=1.5, label='Bad')
ax9.plot(x_feedback_question, feedback_question_output_good, 'c', linewidth=1.5, label='Good')
ax9.plot(x_feedback_question, feedback_question_output_excellent, 'y', linewidth=1.5, label='Excellent')
ax9.plot(x_feedback_question, feedback_question_output_perfect, 'r', linewidth=1.5, label='Perfect')
ax9.legend(loc='upper center', bbox_to_anchor=(0.5, -0.075),
          fancybox=True, shadow=True, ncol=5)

ax9.spines['top'].set_visible(False)
ax9.spines['right'].set_visible(False)
ax9.get_xaxis().tick_bottom()
ax9.get_yaxis().tick_left()

plt.subplots_adjust(hspace=2)

#for ax in (ax5, ax6, ax7, ax9):
#  ax.spines['top'].set_visible(False)
#  ax.spines['right'].set_visible(False)
#  ax.get_xaxis().tick_bottom()
#  ax.get_yaxis().tick_left()
#  plt.tight_layout()

fig3.savefig('OUTPUT_FINALE.png')
plt.close()
"""