""" Just for plotting the fuzzy variables"""
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt  
from pylab import show

x_feedback_match = np.arange(0, 1.1, 0.1)
x_num_games_total = np.arange(1,17, 1)
# ==========================================================================================================================================================
# VArs
# ==========================================================================================================================================================
################################################################ INPUTS 
feedback_match1_input_awful = fuzz.trapmf(x_feedback_match,[0, 0, 0.1, 0.2])
feedback_match1_input_bad = fuzz.trapmf(x_feedback_match,[0.1,0.2,0.3,0.4])
feedback_match1_input_good = fuzz.trapmf(x_feedback_match,[0.3,0.4,0.6,0.7])
feedback_match1_input_excellent = fuzz.trapmf(x_feedback_match,[0.6,0.7,0.8,0.9])
feedback_match1_input_perfect = fuzz.trapmf(x_feedback_match,[0.8,0.9,1,1])

feedback_match2_input_awful = fuzz.trapmf(x_feedback_match,[0, 0, 0.1, 0.2])
feedback_match2_input_bad = fuzz.trapmf(x_feedback_match,[0.1,0.2,0.3,0.4])
feedback_match2_input_good = fuzz.trapmf(x_feedback_match,[0.3,0.4,0.6,0.7])
feedback_match2_input_excellent = fuzz.trapmf(x_feedback_match,[0.6,0.7,0.8,0.9])
feedback_match2_input_perfect = fuzz.trapmf(x_feedback_match,[0.8,0.9,1,1])

feedback_match3_input_awful = fuzz.trapmf(x_feedback_match,[0, 0, 0.1, 0.2])
feedback_match3_input_bad = fuzz.trapmf(x_feedback_match,[0.1,0.2,0.3,0.4])
feedback_match3_input_good = fuzz.trapmf(x_feedback_match,[0.3,0.4,0.6,0.7])
feedback_match3_input_excellent = fuzz.trapmf(x_feedback_match,[0.6,0.7,0.8,0.9])
feedback_match3_input_perfect = fuzz.trapmf(x_feedback_match,[0.8,0.9,1,1])

feedback_match4_input_awful = fuzz.trapmf(x_feedback_match,[0, 0, 0.1, 0.2])
feedback_match4_input_bad = fuzz.trapmf(x_feedback_match,[0.1,0.2,0.3,0.4])
feedback_match4_input_good = fuzz.trapmf(x_feedback_match,[0.3,0.4,0.6,0.7])
feedback_match4_input_excellent = fuzz.trapmf(x_feedback_match,[0.6,0.7,0.8,0.9])
feedback_match4_input_perfect = fuzz.trapmf(x_feedback_match,[0.8,0.9,1,1])

feedback_session_output_awful = fuzz.trapmf(x_feedback_match,[0, 0, 0.1, 0.2])
feedback_session_output_bad = fuzz.trapmf(x_feedback_match,[0.1,0.2,0.3,0.4])
feedback_session_output_good = fuzz.trapmf(x_feedback_match,[0.3,0.4,0.6,0.7])
feedback_session_output_excellent = fuzz.trapmf(x_feedback_match,[0.6,0.7,0.8,0.9])
feedback_session_output_perfect = fuzz.trapmf(x_feedback_match,[0.8,0.9,1,1])

num_games_few = fuzz.trapmf(x_num_games_total,[1,1,4,5])
num_games_enough = fuzz.trapmf(x_num_games_total,[4,5,8,9])
num_games_some = fuzz.trapmf(x_num_games_total,[8,9,12,13])
num_games_many = fuzz.trapmf(x_num_games_total,[12,13,16,16])

fig2, (ax0, ax5, ax6, ax4) = plt.subplots(4,figsize=(10, 15))

ax0.set_title('Session complexity', loc='left')
ax0.axvline(x=1, color='green', label='Easy')
ax0.axvline(x=2, color='red', label='Normal')
ax0.axvline(x=3, color='blue', label='Medium')
ax0.axvline(x=4, color='orange', label='Hard')
ax0.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)

ax5.set_title('Total games',loc='left')  
ax5.plot(x_num_games_total, num_games_few, 'fuchsia', linewidth=1.5, label='Few')
ax5.plot(x_num_games_total, num_games_enough, 'black', linewidth=1.5, label='Enough')
ax5.plot(x_num_games_total, num_games_some, 'lime', linewidth=1.5, label='Some')
ax5.plot(x_num_games_total, num_games_many, 'blueviolet', linewidth=1.5, label='Many')
ax5.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)


ax6.set_title('Feedback_session_output',loc='left')  
ax6.plot(x_feedback_match, feedback_session_output_awful, 'gold', linewidth=1.5, label='Awful')
ax6.plot(x_feedback_match, feedback_session_output_bad, 'seagreen', linewidth=1.5, label='Bad')
ax6.plot(x_feedback_match, feedback_session_output_good, 'cyan', linewidth=1.5, label='Good')
ax6.plot(x_feedback_match, feedback_session_output_excellent, 'fuchsia', linewidth=1.5, label='Excellent')
ax6.plot(x_feedback_match, feedback_session_output_perfect, 'slategray', linewidth=1.5, label='Perfect')
ax6.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),
          fancybox=True, shadow=True, ncol=5)


ax4.set_title('Feedback_match 4',loc='left')  
ax4.plot(x_feedback_match, feedback_match4_input_awful, 'gold', linewidth=1.5, label='Awful')
ax4.plot(x_feedback_match, feedback_match4_input_bad, 'black', linewidth=1.5, label='Bad')
ax4.plot(x_feedback_match, feedback_match4_input_good, 'lightcoral', linewidth=1.5, label='Good')
ax4.plot(x_feedback_match, feedback_match4_input_excellent, 'navy', linewidth=1.5, label='Excellent')
ax4.plot(x_feedback_match, feedback_match4_input_perfect, 'palegreen', linewidth=1.5, label='Perfect')
ax4.legend(loc='upper center', bbox_to_anchor=(0.5, -0.09),
          fancybox=True, shadow=True, ncol=5)
# ==========================================================================================================================================================
# Main 
# ==========================================================================================================================================================
##############remove ax4 repeated cuttin image but conserving same dimesions
for ax in (ax0, ax5, ax6, ax4):
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)
  ax.get_xaxis().tick_bottom()
  ax.get_yaxis().tick_left()
  plt.tight_layout()

#plt.subplots_adjust(hspace=1.5)
fig2.savefig('NEW_Fuzzy_session_0_5.png')
plt.close()

show()