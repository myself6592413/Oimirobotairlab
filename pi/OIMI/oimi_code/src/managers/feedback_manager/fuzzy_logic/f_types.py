# ==========================================================================================================================================================
#  Imports:
# ==========================================================================================================================================================
from enum import Enum
# ==========================================================================================================================================================
#  Classes:
# ==========================================================================================================================================================
class MfCompositionType(Enum):
    """ Type for composition of mf """
    MIN = 1
    MAX = 2
    PROD = 3
    SUM = 4

class AndMethod(Enum):
    """ Method for AND """
    MIN = 1     # min(a, b)
    PROD = 2    # a * b
    MAX = 3 

class ImplicationMethod(Enum):
    """ Method for Implication """
    MIN = 1     # Truncation output fuzzy set
    PROD = 2    # Inference reduction fuzzy set
    MAX = 3

class OrMethod(Enum):
    """ Method for OR """
    MAX = 1     # max(a, b)
    PROB = 2    # a + b - a * b

class AggregationMethod(Enum):
    """ Aggregation methods """
    MAX = 1
    SUM = 2

class DefuzzificationMethod(Enum):
    """ Types of defuzzification """
    CENTROID = 1
    BISECTOR = 2
    AVERAGE_MAXIMUM = 3

class OperatorType(Enum):
    """
    Type of operator in fuzzy rule base
    """
    AND = 1
    OR = 2

class HedgeType(Enum):
    """ Hedge modified for terms """
    NULL = 0
    SLIGHTLY = 1
    SOMEWHAT = 2
    VERY = 3
    EXTREMELY = 4

class DefazzificationMethod(Enum):
    """ Defuzzification methods """
    CENTROID = 1
    BISECTOR = 2
    AVERAGE_MAXIMUM = 3