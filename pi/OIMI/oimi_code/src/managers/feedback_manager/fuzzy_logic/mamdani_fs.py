# ==========================================================================================================================================================
#  Imports:
# ==========================================================================================================================================================
from typing import List, Dict
from generic_fs import GenericFuzzySystem
from rules import FuzzyRule
from variables import FuzzyVariable
from rule_parser import RuleParser
from mf import MembershipFunction, CompositeMF, ConstantMF
from terms import Term
from f_types import AndMethod, \
    OrMethod, \
    ImplicationMethod, \
    AggregationMethod, \
    DefuzzificationMethod, \
    MfCompositionType
# ==========================================================================================================================================================
#  Class:
# ==========================================================================================================================================================
class MamdaniFuzzySystem(GenericFuzzySystem):
    def __init__(self,
                 inp=None,
                 out=None,
                 am: AndMethod = AndMethod.MIN,
                 om: OrMethod = OrMethod.MAX,
                 im: ImplicationMethod = ImplicationMethod.MIN,
                 ag: AggregationMethod = AggregationMethod.MAX,
                 dm: DefuzzificationMethod = DefuzzificationMethod.CENTROID):
        # Costructor
        self.out: List[FuzzyVariable] = out if out is not None else []
        self.implication_method: ImplicationMethod = im
        self.aggregation_method: AggregationMethod = ag
        self.def_method: DefuzzificationMethod = dm
        super().__init__(inp if inp is not None else [], am, om)

    def output_by_name(self, name: str) -> FuzzyVariable:
        """ Search output by name """
        for out in self.out:
            if out.name == name:
                return out
        raise Exception(f'Output variable named "{name}" does not exist')

    def parse_rule(self, rule: str) -> FuzzyRule:
        """ Start parsing text for rules """
        return RuleParser.parse(rule, self.inp, self.out)

    def calculate(self, input_values: Dict[FuzzyVariable, float]) -> Dict[FuzzyVariable, float]:
        if len(self.rules) == 0:
            raise Exception('At least one rule must exist')
        fi: Dict[FuzzyVariable, Dict[Term, float]] = self.fuzzify(input_values)
        #print("INSIDE calculate----> ", fi)
        conditions: Dict[FuzzyRule, float] = self.evaluate_conditions(fi)                
        conclusions: Dict[FuzzyRule, MembershipFunction] = self.implicate(conditions)
        fuzzy_result: Dict[FuzzyVariable, MembershipFunction] = self.aggregate(conclusions)
        result: Dict[FuzzyVariable, float] = self.defuzzify(fuzzy_result)                       
        return result,fuzzy_result, conclusions

    def implicate(self, conditions: Dict[FuzzyRule, float]) -> Dict[FuzzyRule, MembershipFunction]:
        def implicate_type(it: ImplicationMethod) -> MfCompositionType:
            if it == ImplicationMethod.MIN:
                return MfCompositionType.MIN
            elif it == ImplicationMethod.PROD:
                return MfCompositionType.PROD
            raise Exception(f'Implication type {it} not found')
        return {
            rule: CompositeMF(
                implicate_type(self.implication_method),
                ConstantMF(value),
                rule.conclusion.term.mf) for rule, value in conditions.items()
        }

    def aggregate(self, conclusions: Dict[FuzzyRule, MembershipFunction]) -> Dict[FuzzyVariable, MembershipFunction]:
        def composite_type(am: AggregationMethod) -> MfCompositionType:
            if am == AggregationMethod.MAX:
                return MfCompositionType.MAX
            elif am == AggregationMethod.SUM:
                return MfCompositionType.SUM
            raise Exception(f'Aggregation type {am} not found')
        return {
            variable: CompositeMF(
                composite_type(self.aggregation_method),
                *[mf for rule, mf in conclusions.items() if rule.conclusion.variable == variable]
            ) for variable in self.out
        }

    def defuzzify(self, fr: Dict[FuzzyVariable, MembershipFunction]) -> Dict[FuzzyVariable, float]:
        return {variable: self.__defuzzify(mf, variable.min_value, variable.max_value) for variable, mf in fr.items()}

    def __defuzzify(self, mf: MembershipFunction, min_value: float, max_value: float) -> float:
        if self.def_method == DefuzzificationMethod.CENTROID:
            k: int = 101   # defuzzification step
            step = (max_value - min_value) / k
            numerator: float = 0
            denominator: float = 0
            for i in range(k):
                pt_center = min_value + step * float(i)
                val_center = mf.get_value(pt_center)
                val2_center = pt_center * val_center
                numerator += val2_center
                denominator += val_center
            return round(numerator / denominator, 8) if denominator != 0 else 0.0
        else:
            raise Exception(f'Defuzzification method {self.def_method} not implemented')