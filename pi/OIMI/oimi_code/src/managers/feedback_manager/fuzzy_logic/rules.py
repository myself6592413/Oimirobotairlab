# ==========================================================================================================================================================
#  Imports:
# ==========================================================================================================================================================
from typing import List
from terms import Term
from f_types import OperatorType, HedgeType
from variables import FuzzyVariable, SugenoVariable, SugenoFunction
# ==========================================================================================================================================================
#  Classes:
# ==========================================================================================================================================================
class SingleCondition:
    def __init__(self, variable: [FuzzyVariable, SugenoVariable], term: [Term, SugenoFunction], not_: bool = False):
        self.variable: [FuzzyVariable, SugenoVariable] = variable
        self.term: [Term, SugenoFunction] = term
        self.not_: bool = not_

class Conditions:
    def __init__(self, conditions: [List, None] = None, op: OperatorType = OperatorType.AND, not_: bool = False):
        self.conditions: List = conditions if conditions is not None else []
        self.op: OperatorType = op
        self.__not: bool = not_

    @property
    def not_(self):
        return self.__not

class FuzzyCondition(SingleCondition):
    def __init__(self,
                 variable: [FuzzyVariable, SugenoVariable],
                 term: [Term, SugenoFunction],
                 not_: bool = False,
                 hedge: HedgeType = HedgeType.NULL):

        super().__init__(variable, term, not_)
        self.hedge: HedgeType = hedge

class FuzzyRule:
    def __init__(self, condition: Conditions, conclusion: SingleCondition, weight: float = 1.):
        """ Rule design Mamdani fuzzy
        :param condition: IF clause
        :param conclusion: THEN clause
        :param weight: extra weight rules
        """
        self.condition: Conditions = condition
        self.conclusion: SingleCondition = conclusion
        self.weight: float = weight
        
        @property
        def val(self): #like in all other dictionaries
            return self.conclusion
