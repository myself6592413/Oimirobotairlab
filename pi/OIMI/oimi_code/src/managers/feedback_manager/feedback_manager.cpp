#ifndef PY_SSIZE_T_CLEAN
#define PY_SSIZE_T_CLEAN
#endif /* PY_SSIZE_T_CLEAN */
#include "Python.h"
#ifndef Py_PYTHON_H
    #error Python headers needed to compile C extensions, please install development version of Python.
#elif PY_VERSION_HEX < 0x02060000 || (0x03000000 <= PY_VERSION_HEX && PY_VERSION_HEX < 0x03030000)
    #error Cython requires Python 2.6+ or Python 3.3+.
#else
#define CYTHON_ABI "0_29_24"
#define CYTHON_HEX_VERSION 0x001D18F0
#define CYTHON_FUTURE_DIVISION 1
#include <stddef.h>
#ifndef offsetof
  #define offsetof(type, member) ( (size_t) & ((type*)0) -> member )
#endif
#if !defined(WIN32) && !defined(MS_WINDOWS)
  #ifndef __stdcall
    #define __stdcall
  #endif
  #ifndef __cdecl
    #define __cdecl
  #endif
  #ifndef __fastcall
    #define __fastcall
  #endif
#endif
#ifndef DL_IMPORT
  #define DL_IMPORT(t) t
#endif
#ifndef DL_EXPORT
  #define DL_EXPORT(t) t
#endif
#define __PYX_COMMA ,
#ifndef HAVE_LONG_LONG
  #if PY_VERSION_HEX >= 0x02070000
    #define HAVE_LONG_LONG
  #endif
#endif
#ifndef PY_LONG_LONG
  #define PY_LONG_LONG LONG_LONG
#endif
#ifndef Py_HUGE_VAL
  #define Py_HUGE_VAL HUGE_VAL
#endif
#ifdef PYPY_VERSION
  #define CYTHON_COMPILING_IN_PYPY 1
  #define CYTHON_COMPILING_IN_PYSTON 0
  #define CYTHON_COMPILING_IN_CPYTHON 0
  #undef CYTHON_USE_TYPE_SLOTS
  #define CYTHON_USE_TYPE_SLOTS 0
  #undef CYTHON_USE_PYTYPE_LOOKUP
  #define CYTHON_USE_PYTYPE_LOOKUP 0
  #if PY_VERSION_HEX < 0x03050000
    #undef CYTHON_USE_ASYNC_SLOTS
    #define CYTHON_USE_ASYNC_SLOTS 0
  #elif !defined(CYTHON_USE_ASYNC_SLOTS)
    #define CYTHON_USE_ASYNC_SLOTS 1
  #endif
  #undef CYTHON_USE_PYLIST_INTERNALS
  #define CYTHON_USE_PYLIST_INTERNALS 0
  #undef CYTHON_USE_UNICODE_INTERNALS
  #define CYTHON_USE_UNICODE_INTERNALS 0
  #undef CYTHON_USE_UNICODE_WRITER
  #define CYTHON_USE_UNICODE_WRITER 0
  #undef CYTHON_USE_PYLONG_INTERNALS
  #define CYTHON_USE_PYLONG_INTERNALS 0
  #undef CYTHON_AVOID_BORROWED_REFS
  #define CYTHON_AVOID_BORROWED_REFS 1
  #undef CYTHON_ASSUME_SAFE_MACROS
  #define CYTHON_ASSUME_SAFE_MACROS 0
  #undef CYTHON_UNPACK_METHODS
  #define CYTHON_UNPACK_METHODS 0
  #undef CYTHON_FAST_THREAD_STATE
  #define CYTHON_FAST_THREAD_STATE 0
  #undef CYTHON_FAST_PYCALL
  #define CYTHON_FAST_PYCALL 0
  #undef CYTHON_PEP489_MULTI_PHASE_INIT
  #define CYTHON_PEP489_MULTI_PHASE_INIT 0
  #undef CYTHON_USE_TP_FINALIZE
  #define CYTHON_USE_TP_FINALIZE 0
  #undef CYTHON_USE_DICT_VERSIONS
  #define CYTHON_USE_DICT_VERSIONS 0
  #undef CYTHON_USE_EXC_INFO_STACK
  #define CYTHON_USE_EXC_INFO_STACK 0
#elif defined(PYSTON_VERSION)
  #define CYTHON_COMPILING_IN_PYPY 0
  #define CYTHON_COMPILING_IN_PYSTON 1
  #define CYTHON_COMPILING_IN_CPYTHON 0
  #ifndef CYTHON_USE_TYPE_SLOTS
    #define CYTHON_USE_TYPE_SLOTS 1
  #endif
  #undef CYTHON_USE_PYTYPE_LOOKUP
  #define CYTHON_USE_PYTYPE_LOOKUP 0
  #undef CYTHON_USE_ASYNC_SLOTS
  #define CYTHON_USE_ASYNC_SLOTS 0
  #undef CYTHON_USE_PYLIST_INTERNALS
  #define CYTHON_USE_PYLIST_INTERNALS 0
  #ifndef CYTHON_USE_UNICODE_INTERNALS
    #define CYTHON_USE_UNICODE_INTERNALS 1
  #endif
  #undef CYTHON_USE_UNICODE_WRITER
  #define CYTHON_USE_UNICODE_WRITER 0
  #undef CYTHON_USE_PYLONG_INTERNALS
  #define CYTHON_USE_PYLONG_INTERNALS 0
  #ifndef CYTHON_AVOID_BORROWED_REFS
    #define CYTHON_AVOID_BORROWED_REFS 0
  #endif
  #ifndef CYTHON_ASSUME_SAFE_MACROS
    #define CYTHON_ASSUME_SAFE_MACROS 1
  #endif
  #ifndef CYTHON_UNPACK_METHODS
    #define CYTHON_UNPACK_METHODS 1
  #endif
  #undef CYTHON_FAST_THREAD_STATE
  #define CYTHON_FAST_THREAD_STATE 0
  #undef CYTHON_FAST_PYCALL
  #define CYTHON_FAST_PYCALL 0
  #undef CYTHON_PEP489_MULTI_PHASE_INIT
  #define CYTHON_PEP489_MULTI_PHASE_INIT 0
  #undef CYTHON_USE_TP_FINALIZE
  #define CYTHON_USE_TP_FINALIZE 0
  #undef CYTHON_USE_DICT_VERSIONS
  #define CYTHON_USE_DICT_VERSIONS 0
  #undef CYTHON_USE_EXC_INFO_STACK
  #define CYTHON_USE_EXC_INFO_STACK 0
#else
  #define CYTHON_COMPILING_IN_PYPY 0
  #define CYTHON_COMPILING_IN_PYSTON 0
  #define CYTHON_COMPILING_IN_CPYTHON 1
  #ifndef CYTHON_USE_TYPE_SLOTS
    #define CYTHON_USE_TYPE_SLOTS 1
  #endif
  #if PY_VERSION_HEX < 0x02070000
    #undef CYTHON_USE_PYTYPE_LOOKUP
    #define CYTHON_USE_PYTYPE_LOOKUP 0
  #elif !defined(CYTHON_USE_PYTYPE_LOOKUP)
    #define CYTHON_USE_PYTYPE_LOOKUP 1
  #endif
  #if PY_MAJOR_VERSION < 3
    #undef CYTHON_USE_ASYNC_SLOTS
    #define CYTHON_USE_ASYNC_SLOTS 0
  #elif !defined(CYTHON_USE_ASYNC_SLOTS)
    #define CYTHON_USE_ASYNC_SLOTS 1
  #endif
  #if PY_VERSION_HEX < 0x02070000
    #undef CYTHON_USE_PYLONG_INTERNALS
    #define CYTHON_USE_PYLONG_INTERNALS 0
  #elif !defined(CYTHON_USE_PYLONG_INTERNALS)
    #define CYTHON_USE_PYLONG_INTERNALS 1
  #endif
  #ifndef CYTHON_USE_PYLIST_INTERNALS
    #define CYTHON_USE_PYLIST_INTERNALS 1
  #endif
  #ifndef CYTHON_USE_UNICODE_INTERNALS
    #define CYTHON_USE_UNICODE_INTERNALS 1
  #endif
  #if PY_VERSION_HEX < 0x030300F0
    #undef CYTHON_USE_UNICODE_WRITER
    #define CYTHON_USE_UNICODE_WRITER 0
  #elif !defined(CYTHON_USE_UNICODE_WRITER)
    #define CYTHON_USE_UNICODE_WRITER 1
  #endif
  #ifndef CYTHON_AVOID_BORROWED_REFS
    #define CYTHON_AVOID_BORROWED_REFS 0
  #endif
  #ifndef CYTHON_ASSUME_SAFE_MACROS
    #define CYTHON_ASSUME_SAFE_MACROS 1
  #endif
  #ifndef CYTHON_UNPACK_METHODS
    #define CYTHON_UNPACK_METHODS 1
  #endif
  #ifndef CYTHON_FAST_THREAD_STATE
    #define CYTHON_FAST_THREAD_STATE 1
  #endif
  #ifndef CYTHON_FAST_PYCALL
    #define CYTHON_FAST_PYCALL 1
  #endif
  #ifndef CYTHON_PEP489_MULTI_PHASE_INIT
    #define CYTHON_PEP489_MULTI_PHASE_INIT (PY_VERSION_HEX >= 0x03050000)
  #endif
  #ifndef CYTHON_USE_TP_FINALIZE
    #define CYTHON_USE_TP_FINALIZE (PY_VERSION_HEX >= 0x030400a1)
  #endif
  #ifndef CYTHON_USE_DICT_VERSIONS
    #define CYTHON_USE_DICT_VERSIONS (PY_VERSION_HEX >= 0x030600B1)
  #endif
  #ifndef CYTHON_USE_EXC_INFO_STACK
    #define CYTHON_USE_EXC_INFO_STACK (PY_VERSION_HEX >= 0x030700A3)
  #endif
#endif
#if !defined(CYTHON_FAST_PYCCALL)
#define CYTHON_FAST_PYCCALL  (CYTHON_FAST_PYCALL && PY_VERSION_HEX >= 0x030600B1)
#endif
#if CYTHON_USE_PYLONG_INTERNALS
  #include "longintrepr.h"
  #undef SHIFT
  #undef BASE
  #undef MASK
  #ifdef SIZEOF_VOID_P
    enum { __pyx_check_sizeof_voidp = 1 / (int)(SIZEOF_VOID_P == sizeof(void*)) };
  #endif
#endif
#ifndef __has_attribute
  #define __has_attribute(x) 0
#endif
#ifndef __has_cpp_attribute
  #define __has_cpp_attribute(x) 0
#endif
#ifndef CYTHON_RESTRICT
  #if defined(__GNUC__)
    #define CYTHON_RESTRICT __restrict__
  #elif defined(_MSC_VER) && _MSC_VER >= 1400
    #define CYTHON_RESTRICT __restrict
  #elif defined (__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
    #define CYTHON_RESTRICT restrict
  #else
    #define CYTHON_RESTRICT
  #endif
#endif
#ifndef CYTHON_UNUSED
# if defined(__GNUC__)
#   if !(defined(__cplusplus)) || (__GNUC__ > 3 || (__GNUC__ == 3 && __GNUC_MINOR__ >= 4))
#     define CYTHON_UNUSED __attribute__ ((__unused__))
#   else
#     define CYTHON_UNUSED
#   endif
# elif defined(__ICC) || (defined(__INTEL_COMPILER) && !defined(_MSC_VER))
#   define CYTHON_UNUSED __attribute__ ((__unused__))
# else
#   define CYTHON_UNUSED
# endif
#endif
#ifndef CYTHON_MAYBE_UNUSED_VAR
#  if defined(__cplusplus)
     template<class T> void CYTHON_MAYBE_UNUSED_VAR( const T& ) { }
#  else
#    define CYTHON_MAYBE_UNUSED_VAR(x) (void)(x)
#  endif
#endif
#ifndef CYTHON_NCP_UNUSED
# if CYTHON_COMPILING_IN_CPYTHON
#  define CYTHON_NCP_UNUSED
# else
#  define CYTHON_NCP_UNUSED CYTHON_UNUSED
# endif
#endif
#define __Pyx_void_to_None(void_result) ((void)(void_result), Py_INCREF(Py_None), Py_None)
#ifdef _MSC_VER
    #ifndef _MSC_STDINT_H_
        #if _MSC_VER < 1300
           typedef unsigned char     uint8_t;
           typedef unsigned int      uint32_t;
        #else
           typedef unsigned __int8   uint8_t;
           typedef unsigned __int32  uint32_t;
        #endif
    #endif
#else
   #include <stdint.h>
#endif
#ifndef CYTHON_FALLTHROUGH
  #if defined(__cplusplus) && __cplusplus >= 201103L
    #if __has_cpp_attribute(fallthrough)
      #define CYTHON_FALLTHROUGH [[fallthrough]]
    #elif __has_cpp_attribute(clang::fallthrough)
      #define CYTHON_FALLTHROUGH [[clang::fallthrough]]
    #elif __has_cpp_attribute(gnu::fallthrough)
      #define CYTHON_FALLTHROUGH [[gnu::fallthrough]]
    #endif
  #endif
  #ifndef CYTHON_FALLTHROUGH
    #if __has_attribute(fallthrough)
      #define CYTHON_FALLTHROUGH __attribute__((fallthrough))
    #else
      #define CYTHON_FALLTHROUGH
    #endif
  #endif
  #if defined(__clang__ ) && defined(__apple_build_version__)
    #if __apple_build_version__ < 7000000
      #undef  CYTHON_FALLTHROUGH
      #define CYTHON_FALLTHROUGH
    #endif
  #endif
#endif

#ifndef __cplusplus
  #error "Cython files generated with the C++ option must be compiled with a C++ compiler."
#endif
#ifndef CYTHON_INLINE
  #if defined(__clang__)
    #define CYTHON_INLINE __inline__ __attribute__ ((__unused__))
  #else
    #define CYTHON_INLINE inline
  #endif
#endif
template<typename T>
void __Pyx_call_destructor(T& x) {
    x.~T();
}
template<typename T>
class __Pyx_FakeReference {
  public:
    __Pyx_FakeReference() : ptr(NULL) { }
    __Pyx_FakeReference(const T& ref) : ptr(const_cast<T*>(&ref)) { }
    T *operator->() { return ptr; }
    T *operator&() { return ptr; }
    operator T&() { return *ptr; }
    template<typename U> bool operator ==(U other) { return *ptr == other; }
    template<typename U> bool operator !=(U other) { return *ptr != other; }
  private:
    T *ptr;
};

#if CYTHON_COMPILING_IN_PYPY && PY_VERSION_HEX < 0x02070600 && !defined(Py_OptimizeFlag)
  #define Py_OptimizeFlag 0
#endif
#define __PYX_BUILD_PY_SSIZE_T "n"
#define CYTHON_FORMAT_SSIZE_T "z"
#if PY_MAJOR_VERSION < 3
  #define __Pyx_BUILTIN_MODULE_NAME "__builtin__"
  #define __Pyx_PyCode_New(a, k, l, s, f, code, c, n, v, fv, cell, fn, name, fline, lnos)\
          PyCode_New(a+k, l, s, f, code, c, n, v, fv, cell, fn, name, fline, lnos)
  #define __Pyx_DefaultClassType PyClass_Type
#else
  #define __Pyx_BUILTIN_MODULE_NAME "builtins"
#if PY_VERSION_HEX >= 0x030800A4 && PY_VERSION_HEX < 0x030800B2
  #define __Pyx_PyCode_New(a, k, l, s, f, code, c, n, v, fv, cell, fn, name, fline, lnos)\
          PyCode_New(a, 0, k, l, s, f, code, c, n, v, fv, cell, fn, name, fline, lnos)
#else
  #define __Pyx_PyCode_New(a, k, l, s, f, code, c, n, v, fv, cell, fn, name, fline, lnos)\
          PyCode_New(a, k, l, s, f, code, c, n, v, fv, cell, fn, name, fline, lnos)
#endif
  #define __Pyx_DefaultClassType PyType_Type
#endif
#ifndef Py_TPFLAGS_CHECKTYPES
  #define Py_TPFLAGS_CHECKTYPES 0
#endif
#ifndef Py_TPFLAGS_HAVE_INDEX
  #define Py_TPFLAGS_HAVE_INDEX 0
#endif
#ifndef Py_TPFLAGS_HAVE_NEWBUFFER
  #define Py_TPFLAGS_HAVE_NEWBUFFER 0
#endif
#ifndef Py_TPFLAGS_HAVE_FINALIZE
  #define Py_TPFLAGS_HAVE_FINALIZE 0
#endif
#ifndef METH_STACKLESS
  #define METH_STACKLESS 0
#endif
#if PY_VERSION_HEX <= 0x030700A3 || !defined(METH_FASTCALL)
  #ifndef METH_FASTCALL
     #define METH_FASTCALL 0x80
  #endif
  typedef PyObject *(*__Pyx_PyCFunctionFast) (PyObject *self, PyObject *const *args, Py_ssize_t nargs);
  typedef PyObject *(*__Pyx_PyCFunctionFastWithKeywords) (PyObject *self, PyObject *const *args,
                                                          Py_ssize_t nargs, PyObject *kwnames);
#else
  #define __Pyx_PyCFunctionFast _PyCFunctionFast
  #define __Pyx_PyCFunctionFastWithKeywords _PyCFunctionFastWithKeywords
#endif
#if CYTHON_FAST_PYCCALL
#define __Pyx_PyFastCFunction_Check(func)\
    ((PyCFunction_Check(func) && (METH_FASTCALL == (PyCFunction_GET_FLAGS(func) & ~(METH_CLASS | METH_STATIC | METH_COEXIST | METH_KEYWORDS | METH_STACKLESS)))))
#else
#define __Pyx_PyFastCFunction_Check(func) 0
#endif
#if CYTHON_COMPILING_IN_PYPY && !defined(PyObject_Malloc)
  #define PyObject_Malloc(s)   PyMem_Malloc(s)
  #define PyObject_Free(p)     PyMem_Free(p)
  #define PyObject_Realloc(p)  PyMem_Realloc(p)
#endif
#if CYTHON_COMPILING_IN_CPYTHON && PY_VERSION_HEX < 0x030400A1
  #define PyMem_RawMalloc(n)           PyMem_Malloc(n)
  #define PyMem_RawRealloc(p, n)       PyMem_Realloc(p, n)
  #define PyMem_RawFree(p)             PyMem_Free(p)
#endif
#if CYTHON_COMPILING_IN_PYSTON
  #define __Pyx_PyCode_HasFreeVars(co)  PyCode_HasFreeVars(co)
  #define __Pyx_PyFrame_SetLineNumber(frame, lineno) PyFrame_SetLineNumber(frame, lineno)
#else
  #define __Pyx_PyCode_HasFreeVars(co)  (PyCode_GetNumFree(co) > 0)
  #define __Pyx_PyFrame_SetLineNumber(frame, lineno)  (frame)->f_lineno = (lineno)
#endif
#if !CYTHON_FAST_THREAD_STATE || PY_VERSION_HEX < 0x02070000
  #define __Pyx_PyThreadState_Current PyThreadState_GET()
#elif PY_VERSION_HEX >= 0x03060000
  #define __Pyx_PyThreadState_Current _PyThreadState_UncheckedGet()
#elif PY_VERSION_HEX >= 0x03000000
  #define __Pyx_PyThreadState_Current PyThreadState_GET()
#else
  #define __Pyx_PyThreadState_Current _PyThreadState_Current
#endif
#if PY_VERSION_HEX < 0x030700A2 && !defined(PyThread_tss_create) && !defined(Py_tss_NEEDS_INIT)
#include "pythread.h"
#define Py_tss_NEEDS_INIT 0
typedef int Py_tss_t;
static CYTHON_INLINE int PyThread_tss_create(Py_tss_t *key) {
  *key = PyThread_create_key();
  return 0;
}
static CYTHON_INLINE Py_tss_t * PyThread_tss_alloc(void) {
  Py_tss_t *key = (Py_tss_t *)PyObject_Malloc(sizeof(Py_tss_t));
  *key = Py_tss_NEEDS_INIT;
  return key;
}
static CYTHON_INLINE void PyThread_tss_free(Py_tss_t *key) {
  PyObject_Free(key);
}
static CYTHON_INLINE int PyThread_tss_is_created(Py_tss_t *key) {
  return *key != Py_tss_NEEDS_INIT;
}
static CYTHON_INLINE void PyThread_tss_delete(Py_tss_t *key) {
  PyThread_delete_key(*key);
  *key = Py_tss_NEEDS_INIT;
}
static CYTHON_INLINE int PyThread_tss_set(Py_tss_t *key, void *value) {
  return PyThread_set_key_value(*key, value);
}
static CYTHON_INLINE void * PyThread_tss_get(Py_tss_t *key) {
  return PyThread_get_key_value(*key);
}
#endif
#if CYTHON_COMPILING_IN_CPYTHON || defined(_PyDict_NewPresized)
#define __Pyx_PyDict_NewPresized(n)  ((n <= 8) ? PyDict_New() : _PyDict_NewPresized(n))
#else
#define __Pyx_PyDict_NewPresized(n)  PyDict_New()
#endif
#if PY_MAJOR_VERSION >= 3 || CYTHON_FUTURE_DIVISION
  #define __Pyx_PyNumber_Divide(x,y)         PyNumber_TrueDivide(x,y)
  #define __Pyx_PyNumber_InPlaceDivide(x,y)  PyNumber_InPlaceTrueDivide(x,y)
#else
  #define __Pyx_PyNumber_Divide(x,y)         PyNumber_Divide(x,y)
  #define __Pyx_PyNumber_InPlaceDivide(x,y)  PyNumber_InPlaceDivide(x,y)
#endif
#if CYTHON_COMPILING_IN_CPYTHON && PY_VERSION_HEX >= 0x030500A1 && CYTHON_USE_UNICODE_INTERNALS
#define __Pyx_PyDict_GetItemStr(dict, name)  _PyDict_GetItem_KnownHash(dict, name, ((PyASCIIObject *) name)->hash)
#else
#define __Pyx_PyDict_GetItemStr(dict, name)  PyDict_GetItem(dict, name)
#endif
#if PY_VERSION_HEX > 0x03030000 && defined(PyUnicode_KIND)
  #define CYTHON_PEP393_ENABLED 1
  #if defined(PyUnicode_IS_READY)
  #define __Pyx_PyUnicode_READY(op)       (likely(PyUnicode_IS_READY(op)) ?\
                                              0 : _PyUnicode_Ready((PyObject *)(op)))
  #else
  #define __Pyx_PyUnicode_READY(op)       (0)
  #endif
  #define __Pyx_PyUnicode_GET_LENGTH(u)   PyUnicode_GET_LENGTH(u)
  #define __Pyx_PyUnicode_READ_CHAR(u, i) PyUnicode_READ_CHAR(u, i)
  #define __Pyx_PyUnicode_MAX_CHAR_VALUE(u)   PyUnicode_MAX_CHAR_VALUE(u)
  #define __Pyx_PyUnicode_KIND(u)         PyUnicode_KIND(u)
  #define __Pyx_PyUnicode_DATA(u)         PyUnicode_DATA(u)
  #define __Pyx_PyUnicode_READ(k, d, i)   PyUnicode_READ(k, d, i)
  #define __Pyx_PyUnicode_WRITE(k, d, i, ch)  PyUnicode_WRITE(k, d, i, ch)
  #if defined(PyUnicode_IS_READY) && defined(PyUnicode_GET_SIZE)
  #if CYTHON_COMPILING_IN_CPYTHON && PY_VERSION_HEX >= 0x03090000
  #define __Pyx_PyUnicode_IS_TRUE(u)      (0 != (likely(PyUnicode_IS_READY(u)) ? PyUnicode_GET_LENGTH(u) : ((PyCompactUnicodeObject *)(u))->wstr_length))
  #else
  #define __Pyx_PyUnicode_IS_TRUE(u)      (0 != (likely(PyUnicode_IS_READY(u)) ? PyUnicode_GET_LENGTH(u) : PyUnicode_GET_SIZE(u)))
  #endif
  #else
  #define __Pyx_PyUnicode_IS_TRUE(u)      (0 != PyUnicode_GET_LENGTH(u))
  #endif
#else
  #define CYTHON_PEP393_ENABLED 0
  #define PyUnicode_1BYTE_KIND  1
  #define PyUnicode_2BYTE_KIND  2
  #define PyUnicode_4BYTE_KIND  4
  #define __Pyx_PyUnicode_READY(op)       (0)
  #define __Pyx_PyUnicode_GET_LENGTH(u)   PyUnicode_GET_SIZE(u)
  #define __Pyx_PyUnicode_READ_CHAR(u, i) ((Py_UCS4)(PyUnicode_AS_UNICODE(u)[i]))
  #define __Pyx_PyUnicode_MAX_CHAR_VALUE(u)   ((sizeof(Py_UNICODE) == 2) ? 65535 : 1114111)
  #define __Pyx_PyUnicode_KIND(u)         (sizeof(Py_UNICODE))
  #define __Pyx_PyUnicode_DATA(u)         ((void*)PyUnicode_AS_UNICODE(u))
  #define __Pyx_PyUnicode_READ(k, d, i)   ((void)(k), (Py_UCS4)(((Py_UNICODE*)d)[i]))
  #define __Pyx_PyUnicode_WRITE(k, d, i, ch)  (((void)(k)), ((Py_UNICODE*)d)[i] = ch)
  #define __Pyx_PyUnicode_IS_TRUE(u)      (0 != PyUnicode_GET_SIZE(u))
#endif
#if CYTHON_COMPILING_IN_PYPY
  #define __Pyx_PyUnicode_Concat(a, b)      PyNumber_Add(a, b)
  #define __Pyx_PyUnicode_ConcatSafe(a, b)  PyNumber_Add(a, b)
#else
  #define __Pyx_PyUnicode_Concat(a, b)      PyUnicode_Concat(a, b)
  #define __Pyx_PyUnicode_ConcatSafe(a, b)  ((unlikely((a) == Py_None) || unlikely((b) == Py_None)) ?\
      PyNumber_Add(a, b) : __Pyx_PyUnicode_Concat(a, b))
#endif
#if CYTHON_COMPILING_IN_PYPY && !defined(PyUnicode_Contains)
  #define PyUnicode_Contains(u, s)  PySequence_Contains(u, s)
#endif
#if CYTHON_COMPILING_IN_PYPY && !defined(PyByteArray_Check)
  #define PyByteArray_Check(obj)  PyObject_TypeCheck(obj, &PyByteArray_Type)
#endif
#if CYTHON_COMPILING_IN_PYPY && !defined(PyObject_Format)
  #define PyObject_Format(obj, fmt)  PyObject_CallMethod(obj, "__format__", "O", fmt)
#endif
#define __Pyx_PyString_FormatSafe(a, b)   ((unlikely((a) == Py_None || (PyString_Check(b) && !PyString_CheckExact(b)))) ? PyNumber_Remainder(a, b) : __Pyx_PyString_Format(a, b))
#define __Pyx_PyUnicode_FormatSafe(a, b)  ((unlikely((a) == Py_None || (PyUnicode_Check(b) && !PyUnicode_CheckExact(b)))) ? PyNumber_Remainder(a, b) : PyUnicode_Format(a, b))
#if PY_MAJOR_VERSION >= 3
  #define __Pyx_PyString_Format(a, b)  PyUnicode_Format(a, b)
#else
  #define __Pyx_PyString_Format(a, b)  PyString_Format(a, b)
#endif
#if PY_MAJOR_VERSION < 3 && !defined(PyObject_ASCII)
  #define PyObject_ASCII(o)            PyObject_Repr(o)
#endif
#if PY_MAJOR_VERSION >= 3
  #define PyBaseString_Type            PyUnicode_Type
  #define PyStringObject               PyUnicodeObject
  #define PyString_Type                PyUnicode_Type
  #define PyString_Check               PyUnicode_Check
  #define PyString_CheckExact          PyUnicode_CheckExact
#ifndef PyObject_Unicode
  #define PyObject_Unicode             PyObject_Str
#endif
#endif
#if PY_MAJOR_VERSION >= 3
  #define __Pyx_PyBaseString_Check(obj) PyUnicode_Check(obj)
  #define __Pyx_PyBaseString_CheckExact(obj) PyUnicode_CheckExact(obj)
#else
  #define __Pyx_PyBaseString_Check(obj) (PyString_Check(obj) || PyUnicode_Check(obj))
  #define __Pyx_PyBaseString_CheckExact(obj) (PyString_CheckExact(obj) || PyUnicode_CheckExact(obj))
#endif
#ifndef PySet_CheckExact
  #define PySet_CheckExact(obj)        (Py_TYPE(obj) == &PySet_Type)
#endif
#if PY_VERSION_HEX >= 0x030900A4
  #define __Pyx_SET_REFCNT(obj, refcnt) Py_SET_REFCNT(obj, refcnt)
  #define __Pyx_SET_SIZE(obj, size) Py_SET_SIZE(obj, size)
#else
  #define __Pyx_SET_REFCNT(obj, refcnt) Py_REFCNT(obj) = (refcnt)
  #define __Pyx_SET_SIZE(obj, size) Py_SIZE(obj) = (size)
#endif
#if CYTHON_ASSUME_SAFE_MACROS
  #define __Pyx_PySequence_SIZE(seq)  Py_SIZE(seq)
#else
  #define __Pyx_PySequence_SIZE(seq)  PySequence_Size(seq)
#endif
#if PY_MAJOR_VERSION >= 3
  #define PyIntObject                  PyLongObject
  #define PyInt_Type                   PyLong_Type
  #define PyInt_Check(op)              PyLong_Check(op)
  #define PyInt_CheckExact(op)         PyLong_CheckExact(op)
  #define PyInt_FromString             PyLong_FromString
  #define PyInt_FromUnicode            PyLong_FromUnicode
  #define PyInt_FromLong               PyLong_FromLong
  #define PyInt_FromSize_t             PyLong_FromSize_t
  #define PyInt_FromSsize_t            PyLong_FromSsize_t
  #define PyInt_AsLong                 PyLong_AsLong
  #define PyInt_AS_LONG                PyLong_AS_LONG
  #define PyInt_AsSsize_t              PyLong_AsSsize_t
  #define PyInt_AsUnsignedLongMask     PyLong_AsUnsignedLongMask
  #define PyInt_AsUnsignedLongLongMask PyLong_AsUnsignedLongLongMask
  #define PyNumber_Int                 PyNumber_Long
#endif
#if PY_MAJOR_VERSION >= 3
  #define PyBoolObject                 PyLongObject
#endif
#if PY_MAJOR_VERSION >= 3 && CYTHON_COMPILING_IN_PYPY
  #ifndef PyUnicode_InternFromString
    #define PyUnicode_InternFromString(s) PyUnicode_FromString(s)
  #endif
#endif
#if PY_VERSION_HEX < 0x030200A4
  typedef long Py_hash_t;
  #define __Pyx_PyInt_FromHash_t PyInt_FromLong
  #define __Pyx_PyInt_AsHash_t   PyInt_AsLong
#else
  #define __Pyx_PyInt_FromHash_t PyInt_FromSsize_t
  #define __Pyx_PyInt_AsHash_t   PyInt_AsSsize_t
#endif
#if PY_MAJOR_VERSION >= 3
  #define __Pyx_PyMethod_New(func, self, klass) ((self) ? ((void)(klass), PyMethod_New(func, self)) : __Pyx_NewRef(func))
#else
  #define __Pyx_PyMethod_New(func, self, klass) PyMethod_New(func, self, klass)
#endif
#if CYTHON_USE_ASYNC_SLOTS
  #if PY_VERSION_HEX >= 0x030500B1
    #define __Pyx_PyAsyncMethodsStruct PyAsyncMethods
    #define __Pyx_PyType_AsAsync(obj) (Py_TYPE(obj)->tp_as_async)
  #else
    #define __Pyx_PyType_AsAsync(obj) ((__Pyx_PyAsyncMethodsStruct*) (Py_TYPE(obj)->tp_reserved))
  #endif
#else
  #define __Pyx_PyType_AsAsync(obj) NULL
#endif
#ifndef __Pyx_PyAsyncMethodsStruct
    typedef struct {
        unaryfunc am_await;
        unaryfunc am_aiter;
        unaryfunc am_anext;
    } __Pyx_PyAsyncMethodsStruct;
#endif

#if defined(WIN32) || defined(MS_WINDOWS)
  #define _USE_MATH_DEFINES
#endif
#include <math.h>
#ifdef NAN
#define __PYX_NAN() ((float) NAN)
#else
static CYTHON_INLINE float __PYX_NAN() {
  float value;
  memset(&value, 0xFF, sizeof(value));
  return value;
}
#endif
#if defined(__CYGWIN__) && defined(_LDBL_EQ_DBL)
#define __Pyx_truncl trunc
#else
#define __Pyx_truncl truncl
#endif

#define __PYX_MARK_ERR_POS(f_index, lineno) \
    { __pyx_filename = __pyx_f[f_index]; (void)__pyx_filename; __pyx_lineno = lineno; (void)__pyx_lineno; __pyx_clineno = __LINE__; (void)__pyx_clineno; }
#define __PYX_ERR(f_index, lineno, Ln_error) \
    { __PYX_MARK_ERR_POS(f_index, lineno) goto Ln_error; }

#ifndef __PYX_EXTERN_C
  #ifdef __cplusplus
    #define __PYX_EXTERN_C extern "C"
  #else
    #define __PYX_EXTERN_C extern
  #endif
#endif

#define __PYX_HAVE__feedback_manager
#define __PYX_HAVE_API__feedback_manager
/* Early includes */
#ifdef _OPENMP
#include <omp.h>
#endif /* _OPENMP */

#if defined(PYREX_WITHOUT_ASSERTIONS) && !defined(CYTHON_WITHOUT_ASSERTIONS)
#define CYTHON_WITHOUT_ASSERTIONS
#endif

typedef struct {PyObject **p; const char *s; const Py_ssize_t n; const char* encoding;
                const char is_unicode; const char is_str; const char intern; } __Pyx_StringTabEntry;

#define __PYX_DEFAULT_STRING_ENCODING_IS_ASCII 0
#define __PYX_DEFAULT_STRING_ENCODING_IS_UTF8 0
#define __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT (PY_MAJOR_VERSION >= 3 && __PYX_DEFAULT_STRING_ENCODING_IS_UTF8)
#define __PYX_DEFAULT_STRING_ENCODING ""
#define __Pyx_PyObject_FromString __Pyx_PyBytes_FromString
#define __Pyx_PyObject_FromStringAndSize __Pyx_PyBytes_FromStringAndSize
#define __Pyx_uchar_cast(c) ((unsigned char)c)
#define __Pyx_long_cast(x) ((long)x)
#define __Pyx_fits_Py_ssize_t(v, type, is_signed)  (\
    (sizeof(type) < sizeof(Py_ssize_t))  ||\
    (sizeof(type) > sizeof(Py_ssize_t) &&\
          likely(v < (type)PY_SSIZE_T_MAX ||\
                 v == (type)PY_SSIZE_T_MAX)  &&\
          (!is_signed || likely(v > (type)PY_SSIZE_T_MIN ||\
                                v == (type)PY_SSIZE_T_MIN)))  ||\
    (sizeof(type) == sizeof(Py_ssize_t) &&\
          (is_signed || likely(v < (type)PY_SSIZE_T_MAX ||\
                               v == (type)PY_SSIZE_T_MAX)))  )
static CYTHON_INLINE int __Pyx_is_valid_index(Py_ssize_t i, Py_ssize_t limit) {
    return (size_t) i < (size_t) limit;
}
#if defined (__cplusplus) && __cplusplus >= 201103L
    #include <cstdlib>
    #define __Pyx_sst_abs(value) std::abs(value)
#elif SIZEOF_INT >= SIZEOF_SIZE_T
    #define __Pyx_sst_abs(value) abs(value)
#elif SIZEOF_LONG >= SIZEOF_SIZE_T
    #define __Pyx_sst_abs(value) labs(value)
#elif defined (_MSC_VER)
    #define __Pyx_sst_abs(value) ((Py_ssize_t)_abs64(value))
#elif defined (__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
    #define __Pyx_sst_abs(value) llabs(value)
#elif defined (__GNUC__)
    #define __Pyx_sst_abs(value) __builtin_llabs(value)
#else
    #define __Pyx_sst_abs(value) ((value<0) ? -value : value)
#endif
static CYTHON_INLINE const char* __Pyx_PyObject_AsString(PyObject*);
static CYTHON_INLINE const char* __Pyx_PyObject_AsStringAndSize(PyObject*, Py_ssize_t* length);
#define __Pyx_PyByteArray_FromString(s) PyByteArray_FromStringAndSize((const char*)s, strlen((const char*)s))
#define __Pyx_PyByteArray_FromStringAndSize(s, l) PyByteArray_FromStringAndSize((const char*)s, l)
#define __Pyx_PyBytes_FromString        PyBytes_FromString
#define __Pyx_PyBytes_FromStringAndSize PyBytes_FromStringAndSize
static CYTHON_INLINE PyObject* __Pyx_PyUnicode_FromString(const char*);
#if PY_MAJOR_VERSION < 3
    #define __Pyx_PyStr_FromString        __Pyx_PyBytes_FromString
    #define __Pyx_PyStr_FromStringAndSize __Pyx_PyBytes_FromStringAndSize
#else
    #define __Pyx_PyStr_FromString        __Pyx_PyUnicode_FromString
    #define __Pyx_PyStr_FromStringAndSize __Pyx_PyUnicode_FromStringAndSize
#endif
#define __Pyx_PyBytes_AsWritableString(s)     ((char*) PyBytes_AS_STRING(s))
#define __Pyx_PyBytes_AsWritableSString(s)    ((signed char*) PyBytes_AS_STRING(s))
#define __Pyx_PyBytes_AsWritableUString(s)    ((unsigned char*) PyBytes_AS_STRING(s))
#define __Pyx_PyBytes_AsString(s)     ((const char*) PyBytes_AS_STRING(s))
#define __Pyx_PyBytes_AsSString(s)    ((const signed char*) PyBytes_AS_STRING(s))
#define __Pyx_PyBytes_AsUString(s)    ((const unsigned char*) PyBytes_AS_STRING(s))
#define __Pyx_PyObject_AsWritableString(s)    ((char*) __Pyx_PyObject_AsString(s))
#define __Pyx_PyObject_AsWritableSString(s)    ((signed char*) __Pyx_PyObject_AsString(s))
#define __Pyx_PyObject_AsWritableUString(s)    ((unsigned char*) __Pyx_PyObject_AsString(s))
#define __Pyx_PyObject_AsSString(s)    ((const signed char*) __Pyx_PyObject_AsString(s))
#define __Pyx_PyObject_AsUString(s)    ((const unsigned char*) __Pyx_PyObject_AsString(s))
#define __Pyx_PyObject_FromCString(s)  __Pyx_PyObject_FromString((const char*)s)
#define __Pyx_PyBytes_FromCString(s)   __Pyx_PyBytes_FromString((const char*)s)
#define __Pyx_PyByteArray_FromCString(s)   __Pyx_PyByteArray_FromString((const char*)s)
#define __Pyx_PyStr_FromCString(s)     __Pyx_PyStr_FromString((const char*)s)
#define __Pyx_PyUnicode_FromCString(s) __Pyx_PyUnicode_FromString((const char*)s)
static CYTHON_INLINE size_t __Pyx_Py_UNICODE_strlen(const Py_UNICODE *u) {
    const Py_UNICODE *u_end = u;
    while (*u_end++) ;
    return (size_t)(u_end - u - 1);
}
#define __Pyx_PyUnicode_FromUnicode(u)       PyUnicode_FromUnicode(u, __Pyx_Py_UNICODE_strlen(u))
#define __Pyx_PyUnicode_FromUnicodeAndLength PyUnicode_FromUnicode
#define __Pyx_PyUnicode_AsUnicode            PyUnicode_AsUnicode
#define __Pyx_NewRef(obj) (Py_INCREF(obj), obj)
#define __Pyx_Owned_Py_None(b) __Pyx_NewRef(Py_None)
static CYTHON_INLINE PyObject * __Pyx_PyBool_FromLong(long b);
static CYTHON_INLINE int __Pyx_PyObject_IsTrue(PyObject*);
static CYTHON_INLINE int __Pyx_PyObject_IsTrueAndDecref(PyObject*);
static CYTHON_INLINE PyObject* __Pyx_PyNumber_IntOrLong(PyObject* x);
#define __Pyx_PySequence_Tuple(obj)\
    (likely(PyTuple_CheckExact(obj)) ? __Pyx_NewRef(obj) : PySequence_Tuple(obj))
static CYTHON_INLINE Py_ssize_t __Pyx_PyIndex_AsSsize_t(PyObject*);
static CYTHON_INLINE PyObject * __Pyx_PyInt_FromSize_t(size_t);
#if CYTHON_ASSUME_SAFE_MACROS
#define __pyx_PyFloat_AsDouble(x) (PyFloat_CheckExact(x) ? PyFloat_AS_DOUBLE(x) : PyFloat_AsDouble(x))
#else
#define __pyx_PyFloat_AsDouble(x) PyFloat_AsDouble(x)
#endif
#define __pyx_PyFloat_AsFloat(x) ((float) __pyx_PyFloat_AsDouble(x))
#if PY_MAJOR_VERSION >= 3
#define __Pyx_PyNumber_Int(x) (PyLong_CheckExact(x) ? __Pyx_NewRef(x) : PyNumber_Long(x))
#else
#define __Pyx_PyNumber_Int(x) (PyInt_CheckExact(x) ? __Pyx_NewRef(x) : PyNumber_Int(x))
#endif
#define __Pyx_PyNumber_Float(x) (PyFloat_CheckExact(x) ? __Pyx_NewRef(x) : PyNumber_Float(x))
#if PY_MAJOR_VERSION < 3 && __PYX_DEFAULT_STRING_ENCODING_IS_ASCII
static int __Pyx_sys_getdefaultencoding_not_ascii;
static int __Pyx_init_sys_getdefaultencoding_params(void) {
    PyObject* sys;
    PyObject* default_encoding = NULL;
    PyObject* ascii_chars_u = NULL;
    PyObject* ascii_chars_b = NULL;
    const char* default_encoding_c;
    sys = PyImport_ImportModule("sys");
    if (!sys) goto bad;
    default_encoding = PyObject_CallMethod(sys, (char*) "getdefaultencoding", NULL);
    Py_DECREF(sys);
    if (!default_encoding) goto bad;
    default_encoding_c = PyBytes_AsString(default_encoding);
    if (!default_encoding_c) goto bad;
    if (strcmp(default_encoding_c, "ascii") == 0) {
        __Pyx_sys_getdefaultencoding_not_ascii = 0;
    } else {
        char ascii_chars[128];
        int c;
        for (c = 0; c < 128; c++) {
            ascii_chars[c] = c;
        }
        __Pyx_sys_getdefaultencoding_not_ascii = 1;
        ascii_chars_u = PyUnicode_DecodeASCII(ascii_chars, 128, NULL);
        if (!ascii_chars_u) goto bad;
        ascii_chars_b = PyUnicode_AsEncodedString(ascii_chars_u, default_encoding_c, NULL);
        if (!ascii_chars_b || !PyBytes_Check(ascii_chars_b) || memcmp(ascii_chars, PyBytes_AS_STRING(ascii_chars_b), 128) != 0) {
            PyErr_Format(
                PyExc_ValueError,
                "This module compiled with c_string_encoding=ascii, but default encoding '%.200s' is not a superset of ascii.",
                default_encoding_c);
            goto bad;
        }
        Py_DECREF(ascii_chars_u);
        Py_DECREF(ascii_chars_b);
    }
    Py_DECREF(default_encoding);
    return 0;
bad:
    Py_XDECREF(default_encoding);
    Py_XDECREF(ascii_chars_u);
    Py_XDECREF(ascii_chars_b);
    return -1;
}
#endif
#if __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT && PY_MAJOR_VERSION >= 3
#define __Pyx_PyUnicode_FromStringAndSize(c_str, size) PyUnicode_DecodeUTF8(c_str, size, NULL)
#else
#define __Pyx_PyUnicode_FromStringAndSize(c_str, size) PyUnicode_Decode(c_str, size, __PYX_DEFAULT_STRING_ENCODING, NULL)
#if __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT
static char* __PYX_DEFAULT_STRING_ENCODING;
static int __Pyx_init_sys_getdefaultencoding_params(void) {
    PyObject* sys;
    PyObject* default_encoding = NULL;
    char* default_encoding_c;
    sys = PyImport_ImportModule("sys");
    if (!sys) goto bad;
    default_encoding = PyObject_CallMethod(sys, (char*) (const char*) "getdefaultencoding", NULL);
    Py_DECREF(sys);
    if (!default_encoding) goto bad;
    default_encoding_c = PyBytes_AsString(default_encoding);
    if (!default_encoding_c) goto bad;
    __PYX_DEFAULT_STRING_ENCODING = (char*) malloc(strlen(default_encoding_c) + 1);
    if (!__PYX_DEFAULT_STRING_ENCODING) goto bad;
    strcpy(__PYX_DEFAULT_STRING_ENCODING, default_encoding_c);
    Py_DECREF(default_encoding);
    return 0;
bad:
    Py_XDECREF(default_encoding);
    return -1;
}
#endif
#endif


/* Test for GCC > 2.95 */
#if defined(__GNUC__)     && (__GNUC__ > 2 || (__GNUC__ == 2 && (__GNUC_MINOR__ > 95)))
  #define likely(x)   __builtin_expect(!!(x), 1)
  #define unlikely(x) __builtin_expect(!!(x), 0)
#else /* !__GNUC__ or GCC < 2.95 */
  #define likely(x)   (x)
  #define unlikely(x) (x)
#endif /* __GNUC__ */
static CYTHON_INLINE void __Pyx_pretend_to_initialize(void* ptr) { (void)ptr; }

static PyObject *__pyx_m = NULL;
static PyObject *__pyx_d;
static PyObject *__pyx_b;
static PyObject *__pyx_cython_runtime = NULL;
static PyObject *__pyx_empty_tuple;
static PyObject *__pyx_empty_bytes;
static PyObject *__pyx_empty_unicode;
static int __pyx_lineno;
static int __pyx_clineno = 0;
static const char * __pyx_cfilenm= __FILE__;
static const char *__pyx_filename;


static const char *__pyx_f[] = {
  "feedback_manager.pyx",
  "stringsource",
};

/*--- Type declarations ---*/
struct __pyx_obj_16feedback_manager_Feedback;

/* "feedback_manager.pxd":1
 * cdef class Feedback:             # <<<<<<<<<<<<<<
 * 	cdef:
 * 		db_conn
 */
struct __pyx_obj_16feedback_manager_Feedback {
  PyObject_HEAD
  struct __pyx_vtabstruct_16feedback_manager_Feedback *__pyx_vtab;
  PyObject *db_conn;
  PyObject *quantity_feedback_questions;
  PyObject *grade_feedback_questions;
  int value_feedback_questions;
  PyObject *quantity_feedback_games;
  PyObject *grade_feedback_games;
  int value_feedback_games;
  PyObject *quantity_feedback_matches;
  PyObject *grade_feedback_matches;
  int value_feedback_matches;
  PyObject *quantity_feedback_session;
  PyObject *grade_feedback_session;
  int value_feedback_session;
  PyObject *kid_age;
  PyObject *kid_level;
  PyObject *every_question;
  PyObject *every_num_answers;
  PyObject *every_correct_ans;
  PyObject *every_errors_ans;
  PyObject *every_indecisions;
  PyObject *every_question_times;
  PyObject *all_values_questions;
  PyObject *every_question_kinds;
  PyObject *every_question_percentage_errs;
  PyObject *every_game_times;
  PyObject *every_expected_game_times;
  PyObject *every_difficulties_game;
  PyObject *every_quan_feedback_ques;
  PyObject *every_grade_feedback_ques;
  PyObject *every_val_feedback_ques;
  PyObject *every_time_percentage_game;
  PyObject *every_kind_games;
  PyObject *every_match_times;
  PyObject *every_expected_match_times;
  PyObject *every_difficulty_matches;
  PyObject *every_quan_feedback_game;
  PyObject *every_grade_feedback_game;
  PyObject *every_val_feedback_game;
  PyObject *every_time_percentage_match;
  PyObject *complex_ses;
  PyObject *time_ses;
  PyObject *expected_time_ses;
  PyObject *percentage_ses;
  PyObject *time_percentage_ses;
  PyObject *every_quan_feedback_match;
  PyObject *every_grade_feedback_match;
  PyObject *every_val_feedback_match;
  PyObject *the_session_feeds;
  PyObject *matches_feeds;
  PyObject *games_feeds;
  PyObject *questions_feeds;
};



/* "feedback_manager.pyx":42
 * # Class
 * # ==========================================================================================================================================================
 * cdef class Feedback:             # <<<<<<<<<<<<<<
 * 	def __cinit__(self):
 * 		self.db_conn = db.DatabaseManager()
 */

struct __pyx_vtabstruct_16feedback_manager_Feedback {
  PyObject *(*takeout_feeds_question)(struct __pyx_obj_16feedback_manager_Feedback *, int, int __pyx_skip_dispatch);
  PyObject *(*takeout_feeds_game)(struct __pyx_obj_16feedback_manager_Feedback *, int, int __pyx_skip_dispatch);
  PyObject *(*takeout_feeds_match)(struct __pyx_obj_16feedback_manager_Feedback *, int, int __pyx_skip_dispatch);
  PyObject *(*takeout_feeds_session)(struct __pyx_obj_16feedback_manager_Feedback *, int, int __pyx_skip_dispatch);
  PyObject *(*evaluate_whole_feedbacks)(struct __pyx_obj_16feedback_manager_Feedback *, int __pyx_skip_dispatch);
  PyObject *(*feedbacks_insertion)(struct __pyx_obj_16feedback_manager_Feedback *, int, int __pyx_skip_dispatch);
};
static struct __pyx_vtabstruct_16feedback_manager_Feedback *__pyx_vtabptr_16feedback_manager_Feedback;

/* --- Runtime support code (head) --- */
/* Refnanny.proto */
#ifndef CYTHON_REFNANNY
  #define CYTHON_REFNANNY 0
#endif
#if CYTHON_REFNANNY
  typedef struct {
    void (*INCREF)(void*, PyObject*, int);
    void (*DECREF)(void*, PyObject*, int);
    void (*GOTREF)(void*, PyObject*, int);
    void (*GIVEREF)(void*, PyObject*, int);
    void* (*SetupContext)(const char*, int, const char*);
    void (*FinishContext)(void**);
  } __Pyx_RefNannyAPIStruct;
  static __Pyx_RefNannyAPIStruct *__Pyx_RefNanny = NULL;
  static __Pyx_RefNannyAPIStruct *__Pyx_RefNannyImportAPI(const char *modname);
  #define __Pyx_RefNannyDeclarations void *__pyx_refnanny = NULL;
#ifdef WITH_THREAD
  #define __Pyx_RefNannySetupContext(name, acquire_gil)\
          if (acquire_gil) {\
              PyGILState_STATE __pyx_gilstate_save = PyGILState_Ensure();\
              __pyx_refnanny = __Pyx_RefNanny->SetupContext((name), __LINE__, __FILE__);\
              PyGILState_Release(__pyx_gilstate_save);\
          } else {\
              __pyx_refnanny = __Pyx_RefNanny->SetupContext((name), __LINE__, __FILE__);\
          }
#else
  #define __Pyx_RefNannySetupContext(name, acquire_gil)\
          __pyx_refnanny = __Pyx_RefNanny->SetupContext((name), __LINE__, __FILE__)
#endif
  #define __Pyx_RefNannyFinishContext()\
          __Pyx_RefNanny->FinishContext(&__pyx_refnanny)
  #define __Pyx_INCREF(r)  __Pyx_RefNanny->INCREF(__pyx_refnanny, (PyObject *)(r), __LINE__)
  #define __Pyx_DECREF(r)  __Pyx_RefNanny->DECREF(__pyx_refnanny, (PyObject *)(r), __LINE__)
  #define __Pyx_GOTREF(r)  __Pyx_RefNanny->GOTREF(__pyx_refnanny, (PyObject *)(r), __LINE__)
  #define __Pyx_GIVEREF(r) __Pyx_RefNanny->GIVEREF(__pyx_refnanny, (PyObject *)(r), __LINE__)
  #define __Pyx_XINCREF(r)  do { if((r) != NULL) {__Pyx_INCREF(r); }} while(0)
  #define __Pyx_XDECREF(r)  do { if((r) != NULL) {__Pyx_DECREF(r); }} while(0)
  #define __Pyx_XGOTREF(r)  do { if((r) != NULL) {__Pyx_GOTREF(r); }} while(0)
  #define __Pyx_XGIVEREF(r) do { if((r) != NULL) {__Pyx_GIVEREF(r);}} while(0)
#else
  #define __Pyx_RefNannyDeclarations
  #define __Pyx_RefNannySetupContext(name, acquire_gil)
  #define __Pyx_RefNannyFinishContext()
  #define __Pyx_INCREF(r) Py_INCREF(r)
  #define __Pyx_DECREF(r) Py_DECREF(r)
  #define __Pyx_GOTREF(r)
  #define __Pyx_GIVEREF(r)
  #define __Pyx_XINCREF(r) Py_XINCREF(r)
  #define __Pyx_XDECREF(r) Py_XDECREF(r)
  #define __Pyx_XGOTREF(r)
  #define __Pyx_XGIVEREF(r)
#endif
#define __Pyx_XDECREF_SET(r, v) do {\
        PyObject *tmp = (PyObject *) r;\
        r = v; __Pyx_XDECREF(tmp);\
    } while (0)
#define __Pyx_DECREF_SET(r, v) do {\
        PyObject *tmp = (PyObject *) r;\
        r = v; __Pyx_DECREF(tmp);\
    } while (0)
#define __Pyx_CLEAR(r)    do { PyObject* tmp = ((PyObject*)(r)); r = NULL; __Pyx_DECREF(tmp);} while(0)
#define __Pyx_XCLEAR(r)   do { if((r) != NULL) {PyObject* tmp = ((PyObject*)(r)); r = NULL; __Pyx_DECREF(tmp);}} while(0)

/* PyObjectGetAttrStr.proto */
#if CYTHON_USE_TYPE_SLOTS
static CYTHON_INLINE PyObject* __Pyx_PyObject_GetAttrStr(PyObject* obj, PyObject* attr_name);
#else
#define __Pyx_PyObject_GetAttrStr(o,n) PyObject_GetAttr(o,n)
#endif

/* GetBuiltinName.proto */
static PyObject *__Pyx_GetBuiltinName(PyObject *name);

/* RaiseArgTupleInvalid.proto */
static void __Pyx_RaiseArgtupleInvalid(const char* func_name, int exact,
    Py_ssize_t num_min, Py_ssize_t num_max, Py_ssize_t num_found);

/* KeywordStringCheck.proto */
static int __Pyx_CheckKeywordStrings(PyObject *kwdict, const char* function_name, int kw_allowed);

/* PyThreadStateGet.proto */
#if CYTHON_FAST_THREAD_STATE
#define __Pyx_PyThreadState_declare  PyThreadState *__pyx_tstate;
#define __Pyx_PyThreadState_assign  __pyx_tstate = __Pyx_PyThreadState_Current;
#define __Pyx_PyErr_Occurred()  __pyx_tstate->curexc_type
#else
#define __Pyx_PyThreadState_declare
#define __Pyx_PyThreadState_assign
#define __Pyx_PyErr_Occurred()  PyErr_Occurred()
#endif

/* PyErrFetchRestore.proto */
#if CYTHON_FAST_THREAD_STATE
#define __Pyx_PyErr_Clear() __Pyx_ErrRestore(NULL, NULL, NULL)
#define __Pyx_ErrRestoreWithState(type, value, tb)  __Pyx_ErrRestoreInState(PyThreadState_GET(), type, value, tb)
#define __Pyx_ErrFetchWithState(type, value, tb)    __Pyx_ErrFetchInState(PyThreadState_GET(), type, value, tb)
#define __Pyx_ErrRestore(type, value, tb)  __Pyx_ErrRestoreInState(__pyx_tstate, type, value, tb)
#define __Pyx_ErrFetch(type, value, tb)    __Pyx_ErrFetchInState(__pyx_tstate, type, value, tb)
static CYTHON_INLINE void __Pyx_ErrRestoreInState(PyThreadState *tstate, PyObject *type, PyObject *value, PyObject *tb);
static CYTHON_INLINE void __Pyx_ErrFetchInState(PyThreadState *tstate, PyObject **type, PyObject **value, PyObject **tb);
#if CYTHON_COMPILING_IN_CPYTHON
#define __Pyx_PyErr_SetNone(exc) (Py_INCREF(exc), __Pyx_ErrRestore((exc), NULL, NULL))
#else
#define __Pyx_PyErr_SetNone(exc) PyErr_SetNone(exc)
#endif
#else
#define __Pyx_PyErr_Clear() PyErr_Clear()
#define __Pyx_PyErr_SetNone(exc) PyErr_SetNone(exc)
#define __Pyx_ErrRestoreWithState(type, value, tb)  PyErr_Restore(type, value, tb)
#define __Pyx_ErrFetchWithState(type, value, tb)  PyErr_Fetch(type, value, tb)
#define __Pyx_ErrRestoreInState(tstate, type, value, tb)  PyErr_Restore(type, value, tb)
#define __Pyx_ErrFetchInState(tstate, type, value, tb)  PyErr_Fetch(type, value, tb)
#define __Pyx_ErrRestore(type, value, tb)  PyErr_Restore(type, value, tb)
#define __Pyx_ErrFetch(type, value, tb)  PyErr_Fetch(type, value, tb)
#endif

/* Profile.proto */
#ifndef CYTHON_PROFILE
#if CYTHON_COMPILING_IN_PYPY || CYTHON_COMPILING_IN_PYSTON
  #define CYTHON_PROFILE 0
#else
  #define CYTHON_PROFILE 1
#endif
#endif
#ifndef CYTHON_TRACE_NOGIL
  #define CYTHON_TRACE_NOGIL 0
#else
  #if CYTHON_TRACE_NOGIL && !defined(CYTHON_TRACE)
    #define CYTHON_TRACE 1
  #endif
#endif
#ifndef CYTHON_TRACE
  #define CYTHON_TRACE 0
#endif
#if CYTHON_TRACE
  #undef CYTHON_PROFILE_REUSE_FRAME
#endif
#ifndef CYTHON_PROFILE_REUSE_FRAME
  #define CYTHON_PROFILE_REUSE_FRAME 0
#endif
#if CYTHON_PROFILE || CYTHON_TRACE
  #include "compile.h"
  #include "frameobject.h"
  #include "traceback.h"
  #if CYTHON_PROFILE_REUSE_FRAME
    #define CYTHON_FRAME_MODIFIER static
    #define CYTHON_FRAME_DEL(frame)
  #else
    #define CYTHON_FRAME_MODIFIER
    #define CYTHON_FRAME_DEL(frame) Py_CLEAR(frame)
  #endif
  #define __Pyx_TraceDeclarations\
      static PyCodeObject *__pyx_frame_code = NULL;\
      CYTHON_FRAME_MODIFIER PyFrameObject *__pyx_frame = NULL;\
      int __Pyx_use_tracing = 0;
  #define __Pyx_TraceFrameInit(codeobj)\
      if (codeobj) __pyx_frame_code = (PyCodeObject*) codeobj;
#if PY_VERSION_HEX >= 0x030a00b1
  #define __Pyx_IsTracing(tstate, check_tracing, check_funcs)\
     (unlikely((tstate)->cframe->use_tracing) &&\
         (!(check_tracing) || !(tstate)->tracing) &&\
         (!(check_funcs) || (tstate)->c_profilefunc || (CYTHON_TRACE && (tstate)->c_tracefunc)))
  #define __Pyx_SetTracing(tstate, enable)\
      (tstate)->cframe->use_tracing = (enable)
#else
  #define __Pyx_IsTracing(tstate, check_tracing, check_funcs)\
     (unlikely((tstate)->use_tracing) &&\
         (!(check_tracing) || !(tstate)->tracing) &&\
         (!(check_funcs) || (tstate)->c_profilefunc || (CYTHON_TRACE && (tstate)->c_tracefunc)))
  #define __Pyx_SetTracing(tstate, enable)\
      (tstate)->use_tracing = (enable)
#endif
  #ifdef WITH_THREAD
  #define __Pyx_TraceCall(funcname, srcfile, firstlineno, nogil, goto_error)\
  if (nogil) {\
      if (CYTHON_TRACE_NOGIL) {\
          PyThreadState *tstate;\
          PyGILState_STATE state = PyGILState_Ensure();\
          tstate = __Pyx_PyThreadState_Current;\
          if (__Pyx_IsTracing(tstate, 1, 1)) {\
              __Pyx_use_tracing = __Pyx_TraceSetupAndCall(&__pyx_frame_code, &__pyx_frame, tstate, funcname, srcfile, firstlineno);\
          }\
          PyGILState_Release(state);\
          if (unlikely(__Pyx_use_tracing < 0)) goto_error;\
      }\
  } else {\
      PyThreadState* tstate = PyThreadState_GET();\
      if (__Pyx_IsTracing(tstate, 1, 1)) {\
          __Pyx_use_tracing = __Pyx_TraceSetupAndCall(&__pyx_frame_code, &__pyx_frame, tstate, funcname, srcfile, firstlineno);\
          if (unlikely(__Pyx_use_tracing < 0)) goto_error;\
      }\
  }
  #else
  #define __Pyx_TraceCall(funcname, srcfile, firstlineno, nogil, goto_error)\
  {   PyThreadState* tstate = PyThreadState_GET();\
      if (__Pyx_IsTracing(tstate, 1, 1)) {\
          __Pyx_use_tracing = __Pyx_TraceSetupAndCall(&__pyx_frame_code, &__pyx_frame, tstate, funcname, srcfile, firstlineno);\
          if (unlikely(__Pyx_use_tracing < 0)) goto_error;\
      }\
  }
  #endif
  #define __Pyx_TraceException()\
  if (likely(!__Pyx_use_tracing)); else {\
      PyThreadState* tstate = __Pyx_PyThreadState_Current;\
      if (__Pyx_IsTracing(tstate, 0, 1)) {\
          tstate->tracing++;\
          __Pyx_SetTracing(tstate, 0);\
          PyObject *exc_info = __Pyx_GetExceptionTuple(tstate);\
          if (exc_info) {\
              if (CYTHON_TRACE && tstate->c_tracefunc)\
                  tstate->c_tracefunc(\
                      tstate->c_traceobj, __pyx_frame, PyTrace_EXCEPTION, exc_info);\
              tstate->c_profilefunc(\
                  tstate->c_profileobj, __pyx_frame, PyTrace_EXCEPTION, exc_info);\
              Py_DECREF(exc_info);\
          }\
          __Pyx_SetTracing(tstate, 1);\
          tstate->tracing--;\
      }\
  }
  static void __Pyx_call_return_trace_func(PyThreadState *tstate, PyFrameObject *frame, PyObject *result) {
      PyObject *type, *value, *traceback;
      __Pyx_ErrFetchInState(tstate, &type, &value, &traceback);
      tstate->tracing++;
      __Pyx_SetTracing(tstate, 0);
      if (CYTHON_TRACE && tstate->c_tracefunc)
          tstate->c_tracefunc(tstate->c_traceobj, frame, PyTrace_RETURN, result);
      if (tstate->c_profilefunc)
          tstate->c_profilefunc(tstate->c_profileobj, frame, PyTrace_RETURN, result);
      CYTHON_FRAME_DEL(frame);
      __Pyx_SetTracing(tstate, 1);
      tstate->tracing--;
      __Pyx_ErrRestoreInState(tstate, type, value, traceback);
  }
  #ifdef WITH_THREAD
  #define __Pyx_TraceReturn(result, nogil)\
  if (likely(!__Pyx_use_tracing)); else {\
      if (nogil) {\
          if (CYTHON_TRACE_NOGIL) {\
              PyThreadState *tstate;\
              PyGILState_STATE state = PyGILState_Ensure();\
              tstate = __Pyx_PyThreadState_Current;\
              if (__Pyx_IsTracing(tstate, 0, 0)) {\
                  __Pyx_call_return_trace_func(tstate, __pyx_frame, (PyObject*)result);\
              }\
              PyGILState_Release(state);\
          }\
      } else {\
          PyThreadState* tstate = __Pyx_PyThreadState_Current;\
          if (__Pyx_IsTracing(tstate, 0, 0)) {\
              __Pyx_call_return_trace_func(tstate, __pyx_frame, (PyObject*)result);\
          }\
      }\
  }
  #else
  #define __Pyx_TraceReturn(result, nogil)\
  if (likely(!__Pyx_use_tracing)); else {\
      PyThreadState* tstate = __Pyx_PyThreadState_Current;\
      if (__Pyx_IsTracing(tstate, 0, 0)) {\
          __Pyx_call_return_trace_func(tstate, __pyx_frame, (PyObject*)result);\
      }\
  }
  #endif
  static PyCodeObject *__Pyx_createFrameCodeObject(const char *funcname, const char *srcfile, int firstlineno);
  static int __Pyx_TraceSetupAndCall(PyCodeObject** code, PyFrameObject** frame, PyThreadState* tstate, const char *funcname, const char *srcfile, int firstlineno);
#else
  #define __Pyx_TraceDeclarations
  #define __Pyx_TraceFrameInit(codeobj)
  #define __Pyx_TraceCall(funcname, srcfile, firstlineno, nogil, goto_error)   if ((1)); else goto_error;
  #define __Pyx_TraceException()
  #define __Pyx_TraceReturn(result, nogil)
#endif
#if CYTHON_TRACE
  static int __Pyx_call_line_trace_func(PyThreadState *tstate, PyFrameObject *frame, int lineno) {
      int ret;
      PyObject *type, *value, *traceback;
      __Pyx_ErrFetchInState(tstate, &type, &value, &traceback);
      __Pyx_PyFrame_SetLineNumber(frame, lineno);
      tstate->tracing++;
      __Pyx_SetTracing(tstate, 0);
      ret = tstate->c_tracefunc(tstate->c_traceobj, frame, PyTrace_LINE, NULL);
      __Pyx_SetTracing(tstate, 1);
      tstate->tracing--;
      if (likely(!ret)) {
          __Pyx_ErrRestoreInState(tstate, type, value, traceback);
      } else {
          Py_XDECREF(type);
          Py_XDECREF(value);
          Py_XDECREF(traceback);
      }
      return ret;
  }
  #ifdef WITH_THREAD
  #define __Pyx_TraceLine(lineno, nogil, goto_error)\
  if (likely(!__Pyx_use_tracing)); else {\
      if (nogil) {\
          if (CYTHON_TRACE_NOGIL) {\
              int ret = 0;\
              PyThreadState *tstate;\
              PyGILState_STATE state = PyGILState_Ensure();\
              tstate = __Pyx_PyThreadState_Current;\
              if (__Pyx_IsTracing(tstate, 0, 0) && tstate->c_tracefunc && __pyx_frame->f_trace) {\
                  ret = __Pyx_call_line_trace_func(tstate, __pyx_frame, lineno);\
              }\
              PyGILState_Release(state);\
              if (unlikely(ret)) goto_error;\
          }\
      } else {\
          PyThreadState* tstate = __Pyx_PyThreadState_Current;\
          if (__Pyx_IsTracing(tstate, 0, 0) && tstate->c_tracefunc && __pyx_frame->f_trace) {\
              int ret = __Pyx_call_line_trace_func(tstate, __pyx_frame, lineno);\
              if (unlikely(ret)) goto_error;\
          }\
      }\
  }
  #else
  #define __Pyx_TraceLine(lineno, nogil, goto_error)\
  if (likely(!__Pyx_use_tracing)); else {\
      PyThreadState* tstate = __Pyx_PyThreadState_Current;\
      if (__Pyx_IsTracing(tstate, 0, 0) && tstate->c_tracefunc && __pyx_frame->f_trace) {\
          int ret = __Pyx_call_line_trace_func(tstate, __pyx_frame, lineno);\
          if (unlikely(ret)) goto_error;\
      }\
  }
  #endif
#else
  #define __Pyx_TraceLine(lineno, nogil, goto_error)   if ((1)); else goto_error;
#endif

/* PyDictVersioning.proto */
#if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_TYPE_SLOTS
#define __PYX_DICT_VERSION_INIT  ((PY_UINT64_T) -1)
#define __PYX_GET_DICT_VERSION(dict)  (((PyDictObject*)(dict))->ma_version_tag)
#define __PYX_UPDATE_DICT_CACHE(dict, value, cache_var, version_var)\
    (version_var) = __PYX_GET_DICT_VERSION(dict);\
    (cache_var) = (value);
#define __PYX_PY_DICT_LOOKUP_IF_MODIFIED(VAR, DICT, LOOKUP) {\
    static PY_UINT64_T __pyx_dict_version = 0;\
    static PyObject *__pyx_dict_cached_value = NULL;\
    if (likely(__PYX_GET_DICT_VERSION(DICT) == __pyx_dict_version)) {\
        (VAR) = __pyx_dict_cached_value;\
    } else {\
        (VAR) = __pyx_dict_cached_value = (LOOKUP);\
        __pyx_dict_version = __PYX_GET_DICT_VERSION(DICT);\
    }\
}
static CYTHON_INLINE PY_UINT64_T __Pyx_get_tp_dict_version(PyObject *obj);
static CYTHON_INLINE PY_UINT64_T __Pyx_get_object_dict_version(PyObject *obj);
static CYTHON_INLINE int __Pyx_object_dict_version_matches(PyObject* obj, PY_UINT64_T tp_dict_version, PY_UINT64_T obj_dict_version);
#else
#define __PYX_GET_DICT_VERSION(dict)  (0)
#define __PYX_UPDATE_DICT_CACHE(dict, value, cache_var, version_var)
#define __PYX_PY_DICT_LOOKUP_IF_MODIFIED(VAR, DICT, LOOKUP)  (VAR) = (LOOKUP);
#endif

/* GetModuleGlobalName.proto */
#if CYTHON_USE_DICT_VERSIONS
#define __Pyx_GetModuleGlobalName(var, name)  {\
    static PY_UINT64_T __pyx_dict_version = 0;\
    static PyObject *__pyx_dict_cached_value = NULL;\
    (var) = (likely(__pyx_dict_version == __PYX_GET_DICT_VERSION(__pyx_d))) ?\
        (likely(__pyx_dict_cached_value) ? __Pyx_NewRef(__pyx_dict_cached_value) : __Pyx_GetBuiltinName(name)) :\
        __Pyx__GetModuleGlobalName(name, &__pyx_dict_version, &__pyx_dict_cached_value);\
}
#define __Pyx_GetModuleGlobalNameUncached(var, name)  {\
    PY_UINT64_T __pyx_dict_version;\
    PyObject *__pyx_dict_cached_value;\
    (var) = __Pyx__GetModuleGlobalName(name, &__pyx_dict_version, &__pyx_dict_cached_value);\
}
static PyObject *__Pyx__GetModuleGlobalName(PyObject *name, PY_UINT64_T *dict_version, PyObject **dict_cached_value);
#else
#define __Pyx_GetModuleGlobalName(var, name)  (var) = __Pyx__GetModuleGlobalName(name)
#define __Pyx_GetModuleGlobalNameUncached(var, name)  (var) = __Pyx__GetModuleGlobalName(name)
static CYTHON_INLINE PyObject *__Pyx__GetModuleGlobalName(PyObject *name);
#endif

/* PyFunctionFastCall.proto */
#if CYTHON_FAST_PYCALL
#define __Pyx_PyFunction_FastCall(func, args, nargs)\
    __Pyx_PyFunction_FastCallDict((func), (args), (nargs), NULL)
#if 1 || PY_VERSION_HEX < 0x030600B1
static PyObject *__Pyx_PyFunction_FastCallDict(PyObject *func, PyObject **args, Py_ssize_t nargs, PyObject *kwargs);
#else
#define __Pyx_PyFunction_FastCallDict(func, args, nargs, kwargs) _PyFunction_FastCallDict(func, args, nargs, kwargs)
#endif
#define __Pyx_BUILD_ASSERT_EXPR(cond)\
    (sizeof(char [1 - 2*!(cond)]) - 1)
#ifndef Py_MEMBER_SIZE
#define Py_MEMBER_SIZE(type, member) sizeof(((type *)0)->member)
#endif
  static size_t __pyx_pyframe_localsplus_offset = 0;
  #include "frameobject.h"
  #define __Pxy_PyFrame_Initialize_Offsets()\
    ((void)__Pyx_BUILD_ASSERT_EXPR(sizeof(PyFrameObject) == offsetof(PyFrameObject, f_localsplus) + Py_MEMBER_SIZE(PyFrameObject, f_localsplus)),\
     (void)(__pyx_pyframe_localsplus_offset = ((size_t)PyFrame_Type.tp_basicsize) - Py_MEMBER_SIZE(PyFrameObject, f_localsplus)))
  #define __Pyx_PyFrame_GetLocalsplus(frame)\
    (assert(__pyx_pyframe_localsplus_offset), (PyObject **)(((char *)(frame)) + __pyx_pyframe_localsplus_offset))
#endif

/* PyObjectCall.proto */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_Call(PyObject *func, PyObject *arg, PyObject *kw);
#else
#define __Pyx_PyObject_Call(func, arg, kw) PyObject_Call(func, arg, kw)
#endif

/* PyObjectCallMethO.proto */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallMethO(PyObject *func, PyObject *arg);
#endif

/* PyObjectCallNoArg.proto */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallNoArg(PyObject *func);
#else
#define __Pyx_PyObject_CallNoArg(func) __Pyx_PyObject_Call(func, __pyx_empty_tuple, NULL)
#endif

/* PyCFunctionFastCall.proto */
#if CYTHON_FAST_PYCCALL
static CYTHON_INLINE PyObject *__Pyx_PyCFunction_FastCall(PyObject *func, PyObject **args, Py_ssize_t nargs);
#else
#define __Pyx_PyCFunction_FastCall(func, args, nargs)  (assert(0), NULL)
#endif

/* PyObjectCallOneArg.proto */
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallOneArg(PyObject *func, PyObject *arg);

/* RaiseDoubleKeywords.proto */
static void __Pyx_RaiseDoubleKeywordsError(const char* func_name, PyObject* kw_name);

/* ParseKeywords.proto */
static int __Pyx_ParseOptionalKeywords(PyObject *kwds, PyObject **argnames[],\
    PyObject *kwds2, PyObject *values[], Py_ssize_t num_pos_args,\
    const char* function_name);

/* PyObjectCall2Args.proto */
static CYTHON_UNUSED PyObject* __Pyx_PyObject_Call2Args(PyObject* function, PyObject* arg1, PyObject* arg2);

/* GetItemInt.proto */
#define __Pyx_GetItemInt(o, i, type, is_signed, to_py_func, is_list, wraparound, boundscheck)\
    (__Pyx_fits_Py_ssize_t(i, type, is_signed) ?\
    __Pyx_GetItemInt_Fast(o, (Py_ssize_t)i, is_list, wraparound, boundscheck) :\
    (is_list ? (PyErr_SetString(PyExc_IndexError, "list index out of range"), (PyObject*)NULL) :\
               __Pyx_GetItemInt_Generic(o, to_py_func(i))))
#define __Pyx_GetItemInt_List(o, i, type, is_signed, to_py_func, is_list, wraparound, boundscheck)\
    (__Pyx_fits_Py_ssize_t(i, type, is_signed) ?\
    __Pyx_GetItemInt_List_Fast(o, (Py_ssize_t)i, wraparound, boundscheck) :\
    (PyErr_SetString(PyExc_IndexError, "list index out of range"), (PyObject*)NULL))
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_List_Fast(PyObject *o, Py_ssize_t i,
                                                              int wraparound, int boundscheck);
#define __Pyx_GetItemInt_Tuple(o, i, type, is_signed, to_py_func, is_list, wraparound, boundscheck)\
    (__Pyx_fits_Py_ssize_t(i, type, is_signed) ?\
    __Pyx_GetItemInt_Tuple_Fast(o, (Py_ssize_t)i, wraparound, boundscheck) :\
    (PyErr_SetString(PyExc_IndexError, "tuple index out of range"), (PyObject*)NULL))
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_Tuple_Fast(PyObject *o, Py_ssize_t i,
                                                              int wraparound, int boundscheck);
static PyObject *__Pyx_GetItemInt_Generic(PyObject *o, PyObject* j);
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_Fast(PyObject *o, Py_ssize_t i,
                                                     int is_list, int wraparound, int boundscheck);

/* RaiseException.proto */
static void __Pyx_Raise(PyObject *type, PyObject *value, PyObject *tb, PyObject *cause);

/* PyObject_GenericGetAttrNoDict.proto */
#if CYTHON_USE_TYPE_SLOTS && CYTHON_USE_PYTYPE_LOOKUP && PY_VERSION_HEX < 0x03070000
static CYTHON_INLINE PyObject* __Pyx_PyObject_GenericGetAttrNoDict(PyObject* obj, PyObject* attr_name);
#else
#define __Pyx_PyObject_GenericGetAttrNoDict PyObject_GenericGetAttr
#endif

/* PyObject_GenericGetAttr.proto */
#if CYTHON_USE_TYPE_SLOTS && CYTHON_USE_PYTYPE_LOOKUP && PY_VERSION_HEX < 0x03070000
static PyObject* __Pyx_PyObject_GenericGetAttr(PyObject* obj, PyObject* attr_name);
#else
#define __Pyx_PyObject_GenericGetAttr PyObject_GenericGetAttr
#endif

/* SetVTable.proto */
static int __Pyx_SetVtable(PyObject *dict, void *vtable);

/* PyErrExceptionMatches.proto */
#if CYTHON_FAST_THREAD_STATE
#define __Pyx_PyErr_ExceptionMatches(err) __Pyx_PyErr_ExceptionMatchesInState(__pyx_tstate, err)
static CYTHON_INLINE int __Pyx_PyErr_ExceptionMatchesInState(PyThreadState* tstate, PyObject* err);
#else
#define __Pyx_PyErr_ExceptionMatches(err)  PyErr_ExceptionMatches(err)
#endif

/* PyObjectGetAttrStrNoError.proto */
static CYTHON_INLINE PyObject* __Pyx_PyObject_GetAttrStrNoError(PyObject* obj, PyObject* attr_name);

/* SetupReduce.proto */
static int __Pyx_setup_reduce(PyObject* type_obj);

/* Import.proto */
static PyObject *__Pyx_Import(PyObject *name, PyObject *from_list, int level);

/* PySequenceContains.proto */
static CYTHON_INLINE int __Pyx_PySequence_ContainsTF(PyObject* item, PyObject* seq, int eq) {
    int result = PySequence_Contains(seq, item);
    return unlikely(result < 0) ? result : (result == (eq == Py_EQ));
}

/* ListAppend.proto */
#if CYTHON_USE_PYLIST_INTERNALS && CYTHON_ASSUME_SAFE_MACROS
static CYTHON_INLINE int __Pyx_PyList_Append(PyObject* list, PyObject* x) {
    PyListObject* L = (PyListObject*) list;
    Py_ssize_t len = Py_SIZE(list);
    if (likely(L->allocated > len) & likely(len > (L->allocated >> 1))) {
        Py_INCREF(x);
        PyList_SET_ITEM(list, len, x);
        __Pyx_SET_SIZE(list, len + 1);
        return 0;
    }
    return PyList_Append(list, x);
}
#else
#define __Pyx_PyList_Append(L,x) PyList_Append(L,x)
#endif

/* PyObjectGetMethod.proto */
static int __Pyx_PyObject_GetMethod(PyObject *obj, PyObject *name, PyObject **method);

/* PyObjectCallMethod1.proto */
static PyObject* __Pyx_PyObject_CallMethod1(PyObject* obj, PyObject* method_name, PyObject* arg);

/* append.proto */
static CYTHON_INLINE int __Pyx_PyObject_Append(PyObject* L, PyObject* x);

/* ImportFrom.proto */
static PyObject* __Pyx_ImportFrom(PyObject* module, PyObject* name);

/* CLineInTraceback.proto */
#ifdef CYTHON_CLINE_IN_TRACEBACK
#define __Pyx_CLineForTraceback(tstate, c_line)  (((CYTHON_CLINE_IN_TRACEBACK)) ? c_line : 0)
#else
static int __Pyx_CLineForTraceback(PyThreadState *tstate, int c_line);
#endif

/* CodeObjectCache.proto */
typedef struct {
    PyCodeObject* code_object;
    int code_line;
} __Pyx_CodeObjectCacheEntry;
struct __Pyx_CodeObjectCache {
    int count;
    int max_count;
    __Pyx_CodeObjectCacheEntry* entries;
};
static struct __Pyx_CodeObjectCache __pyx_code_cache = {0,0,NULL};
static int __pyx_bisect_code_objects(__Pyx_CodeObjectCacheEntry* entries, int count, int code_line);
static PyCodeObject *__pyx_find_code_object(int code_line);
static void __pyx_insert_code_object(int code_line, PyCodeObject* code_object);

/* AddTraceback.proto */
static void __Pyx_AddTraceback(const char *funcname, int c_line,
                               int py_line, const char *filename);

/* GCCDiagnostics.proto */
#if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6))
#define __Pyx_HAS_GCC_DIAGNOSTIC
#endif

/* CIntFromPy.proto */
static CYTHON_INLINE int __Pyx_PyInt_As_int(PyObject *);

/* CIntToPy.proto */
static CYTHON_INLINE PyObject* __Pyx_PyInt_From_int(int value);

/* CIntToPy.proto */
static CYTHON_INLINE PyObject* __Pyx_PyInt_From_long(long value);

/* CIntFromPy.proto */
static CYTHON_INLINE long __Pyx_PyInt_As_long(PyObject *);

/* FastTypeChecks.proto */
#if CYTHON_COMPILING_IN_CPYTHON
#define __Pyx_TypeCheck(obj, type) __Pyx_IsSubtype(Py_TYPE(obj), (PyTypeObject *)type)
static CYTHON_INLINE int __Pyx_IsSubtype(PyTypeObject *a, PyTypeObject *b);
static CYTHON_INLINE int __Pyx_PyErr_GivenExceptionMatches(PyObject *err, PyObject *type);
static CYTHON_INLINE int __Pyx_PyErr_GivenExceptionMatches2(PyObject *err, PyObject *type1, PyObject *type2);
#else
#define __Pyx_TypeCheck(obj, type) PyObject_TypeCheck(obj, (PyTypeObject *)type)
#define __Pyx_PyErr_GivenExceptionMatches(err, type) PyErr_GivenExceptionMatches(err, type)
#define __Pyx_PyErr_GivenExceptionMatches2(err, type1, type2) (PyErr_GivenExceptionMatches(err, type1) || PyErr_GivenExceptionMatches(err, type2))
#endif
#define __Pyx_PyException_Check(obj) __Pyx_TypeCheck(obj, PyExc_Exception)

/* CheckBinaryVersion.proto */
static int __Pyx_check_binary_version(void);

/* FunctionImport.proto */
static int __Pyx_ImportFunction(PyObject *module, const char *funcname, void (**f)(void), const char *sig);

/* InitStrings.proto */
static int __Pyx_InitStrings(__Pyx_StringTabEntry *t);

static PyObject *__pyx_f_16feedback_manager_8Feedback_takeout_feeds_question(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid, int __pyx_skip_dispatch); /* proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_takeout_feeds_game(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid, int __pyx_skip_dispatch); /* proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_takeout_feeds_match(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid, int __pyx_skip_dispatch); /* proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_takeout_feeds_session(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid, int __pyx_skip_dispatch); /* proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_evaluate_whole_feedbacks(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_skip_dispatch); /* proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_feedbacks_insertion(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid, int __pyx_skip_dispatch); /* proto*/

/* Module declarations from 'fuzzy_controller_takeout' */
static PyObject *(*__pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_question)(PyObject *, int); /*proto*/
static PyObject *(*__pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_game)(PyObject *, int); /*proto*/
static PyObject *(*__pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_match)(PyObject *, int); /*proto*/
static PyObject *(*__pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_session)(PyObject *, int); /*proto*/

/* Module declarations from 'fuzzy_controller_evaluate_feedbacks_whole_session' */
static PyObject *(*__pyx_f_49fuzzy_controller_evaluate_feedbacks_whole_session_evaluate_feedbacks_whole_session)(PyObject *, int __pyx_skip_dispatch); /*proto*/

/* Module declarations from 'fuzzy_controller_evaluate_question' */

/* Module declarations from 'fuzzy_controller_evaluate_match' */

/* Module declarations from 'fuzzy_controller_evaluate_game' */

/* Module declarations from 'fuzzy_controller_insert' */

/* Module declarations from 'feedback_manager' */
static PyTypeObject *__pyx_ptype_16feedback_manager_Feedback = 0;
#define __Pyx_MODULE_NAME "feedback_manager"
extern int __pyx_module_is_main_feedback_manager;
int __pyx_module_is_main_feedback_manager = 0;

/* Implementation of 'feedback_manager' */
static PyObject *__pyx_builtin_print;
static PyObject *__pyx_builtin_TypeError;
static const char __pyx_k_db[] = "db";
static const char __pyx_k_os[] = "os";
static const char __pyx_k_sys[] = "sys";
static const char __pyx_k_main[] = "__main__";
static const char __pyx_k_name[] = "__name__";
static const char __pyx_k_path[] = "path";
static const char __pyx_k_test[] = "__test__";
static const char __pyx_k_print[] = "print";
static const char __pyx_k_append[] = "append";
static const char __pyx_k_exc_tb[] = "exc_tb";
static const char __pyx_k_import[] = "__import__";
static const char __pyx_k_reduce[] = "__reduce__";
static const char __pyx_k_typing[] = "typing";
static const char __pyx_k_exc_val[] = "exc_val";
static const char __pyx_k_Feedback[] = "Feedback";
static const char __pyx_k_Optional[] = "Optional";
static const char __pyx_k_exc_type[] = "exc_type";
static const char __pyx_k_getstate[] = "__getstate__";
static const char __pyx_k_setstate[] = "__setstate__";
static const char __pyx_k_TypeError[] = "TypeError";
static const char __pyx_k_reduce_ex[] = "__reduce_ex__";
static const char __pyx_k_pyx_vtable[] = "__pyx_vtable__";
static const char __pyx_k_games_feeds[] = "games_feeds ";
static const char __pyx_k_matches_feeds[] = "matches_feeds ";
static const char __pyx_k_reduce_cython[] = "__reduce_cython__";
static const char __pyx_k_Creating_Feeds[] = "Creating Feeds...";
static const char __pyx_k_DatabaseManager[] = "DatabaseManager";
static const char __pyx_k_questions_feeds[] = "questions_feeds ";
static const char __pyx_k_setstate_cython[] = "__setstate_cython__";
static const char __pyx_k_database_manager[] = "database_manager";
static const char __pyx_k_execute_new_query[] = "execute_new_query";
static const char __pyx_k_the_session_feeds[] = "the_session_feeds ";
static const char __pyx_k_cline_in_traceback[] = "cline_in_traceback";
static const char __pyx_k_takeout_feeds_game[] = "takeout_feeds_game";
static const char __pyx_k_feedbacks_insertion[] = "feedbacks_insertion";
static const char __pyx_k_takeout_feeds_match[] = "takeout_feeds_match";
static const char __pyx_k_grade_feedback_games[] = "grade_feedback_games";
static const char __pyx_k_value_feedback_games[] = "value_feedback_games";
static const char __pyx_k_takeout_feeds_session[] = "takeout_feeds_session";
static const char __pyx_k_grade_feedback_matches[] = "grade_feedback_matches";
static const char __pyx_k_grade_feedback_session[] = "grade_feedback_session";
static const char __pyx_k_takeout_feeds_question[] = "takeout_feeds_question";
static const char __pyx_k_value_feedback_matches[] = "value_feedback_matches";
static const char __pyx_k_value_feedback_session[] = "value_feedback_session";
static const char __pyx_k_quantity_feedback_games[] = "quantity_feedback_games";
static const char __pyx_k_evaluate_whole_feedbacks[] = "evaluate_whole_feedbacks";
static const char __pyx_k_grade_feedback_questions[] = "grade_feedback_questions";
static const char __pyx_k_value_feedback_questions[] = "value_feedback_questions";
static const char __pyx_k_quantity_feedback_matches[] = "quantity_feedback_matches";
static const char __pyx_k_quantity_feedback_session[] = "quantity_feedback_session";
static const char __pyx_k_quantity_feedback_questions[] = "quantity_feedback_questions";
static const char __pyx_k_home_notto4_Desktop_thesis_offi[] = "/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/";
static const char __pyx_k_include_all_feeds_in_once_given[] = "include_all_feeds_in_once_given_session";
static const char __pyx_k_All_feedbacks_were_created_succe[] = "All feedbacks were created successfully.";
static const char __pyx_k_Info_Oimi_robot_fuzzy_controller[] = "Info:\n\tOimi robot fuzzy_controller_extract \n\tTake data from db to evaluate ques/gam/mast/sess feedbacks\nNotes:\n\tFor detailed info, look at ./code_docs_of_dabatase_manager database_controller_add_into\nAuthor:\n\tCreated by Colombo Giacomo, Politecnico di Milano 2021 ";
static const char __pyx_k_SELECT_session_id_FROM_Kids_Sess[] = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1";
static const char __pyx_k_VEDIAMO_SE_LE_HA_MODIFICATE_GIUS[] = "VEDIAMO SE LE HA MODIFICATE GIUSTE.....";
static const char __pyx_k_no_default___reduce___due_to_non[] = "no default __reduce__ due to non-trivial __cinit__";
static const char __pyx_k_home_notto4_Desktop_thesis_offi_2[] = "/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/";
static PyObject *__pyx_kp_u_All_feedbacks_were_created_succe;
static PyObject *__pyx_kp_u_Creating_Feeds;
static PyObject *__pyx_n_s_DatabaseManager;
static PyObject *__pyx_n_s_Feedback;
static PyObject *__pyx_n_s_Optional;
static PyObject *__pyx_kp_u_SELECT_session_id_FROM_Kids_Sess;
static PyObject *__pyx_n_s_TypeError;
static PyObject *__pyx_kp_u_VEDIAMO_SE_LE_HA_MODIFICATE_GIUS;
static PyObject *__pyx_n_s_append;
static PyObject *__pyx_n_s_cline_in_traceback;
static PyObject *__pyx_n_s_database_manager;
static PyObject *__pyx_n_s_db;
static PyObject *__pyx_n_s_evaluate_whole_feedbacks;
static PyObject *__pyx_n_s_exc_tb;
static PyObject *__pyx_n_s_exc_type;
static PyObject *__pyx_n_s_exc_val;
static PyObject *__pyx_n_s_execute_new_query;
static PyObject *__pyx_n_s_feedbacks_insertion;
static PyObject *__pyx_kp_u_games_feeds;
static PyObject *__pyx_n_s_getstate;
static PyObject *__pyx_n_s_grade_feedback_games;
static PyObject *__pyx_n_s_grade_feedback_matches;
static PyObject *__pyx_n_s_grade_feedback_questions;
static PyObject *__pyx_n_s_grade_feedback_session;
static PyObject *__pyx_kp_u_home_notto4_Desktop_thesis_offi;
static PyObject *__pyx_kp_u_home_notto4_Desktop_thesis_offi_2;
static PyObject *__pyx_n_s_import;
static PyObject *__pyx_n_s_include_all_feeds_in_once_given;
static PyObject *__pyx_n_s_main;
static PyObject *__pyx_kp_u_matches_feeds;
static PyObject *__pyx_n_s_name;
static PyObject *__pyx_kp_s_no_default___reduce___due_to_non;
static PyObject *__pyx_n_s_os;
static PyObject *__pyx_n_s_path;
static PyObject *__pyx_n_s_print;
static PyObject *__pyx_n_s_pyx_vtable;
static PyObject *__pyx_n_s_quantity_feedback_games;
static PyObject *__pyx_n_s_quantity_feedback_matches;
static PyObject *__pyx_n_s_quantity_feedback_questions;
static PyObject *__pyx_n_s_quantity_feedback_session;
static PyObject *__pyx_kp_u_questions_feeds;
static PyObject *__pyx_n_s_reduce;
static PyObject *__pyx_n_s_reduce_cython;
static PyObject *__pyx_n_s_reduce_ex;
static PyObject *__pyx_n_s_setstate;
static PyObject *__pyx_n_s_setstate_cython;
static PyObject *__pyx_n_s_sys;
static PyObject *__pyx_n_s_takeout_feeds_game;
static PyObject *__pyx_n_s_takeout_feeds_match;
static PyObject *__pyx_n_s_takeout_feeds_question;
static PyObject *__pyx_n_s_takeout_feeds_session;
static PyObject *__pyx_n_s_test;
static PyObject *__pyx_kp_u_the_session_feeds;
static PyObject *__pyx_n_s_typing;
static PyObject *__pyx_n_s_value_feedback_games;
static PyObject *__pyx_n_s_value_feedback_matches;
static PyObject *__pyx_n_s_value_feedback_questions;
static PyObject *__pyx_n_s_value_feedback_session;
static int __pyx_pf_16feedback_manager_8Feedback___cinit__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_2__enter__(CYTHON_UNUSED struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_4__exit__(CYTHON_UNUSED struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, CYTHON_UNUSED PyObject *__pyx_v_exc_type, CYTHON_UNUSED PyObject *__pyx_v_exc_val, CYTHON_UNUSED PyObject *__pyx_v_exc_tb); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_6__eq__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_other); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_27quantity_feedback_questions___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_27quantity_feedback_questions_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_23quantity_feedback_games___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_23quantity_feedback_games_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_matches___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_matches_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_session___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_session_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_24grade_feedback_questions___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_24grade_feedback_questions_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_20grade_feedback_games___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_20grade_feedback_games_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_22grade_feedback_matches___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_22grade_feedback_matches_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_22grade_feedback_session___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_22grade_feedback_session_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_24value_feedback_questions___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_24value_feedback_questions_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_20value_feedback_games___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_20value_feedback_games_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_22value_feedback_matches___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_22value_feedback_matches_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_22value_feedback_session___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_22value_feedback_session_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_7db_conn___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_7db_conn_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_7kid_age___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_7kid_age_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_9kid_level___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_9kid_level_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_14every_question___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_14every_question_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_17every_num_answers___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_17every_num_answers_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_17every_correct_ans___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_17every_correct_ans_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_16every_errors_ans___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_16every_errors_ans_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_17every_indecisions___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_17every_indecisions_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_20every_question_times___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_20every_question_times_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_20all_values_questions___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_20all_values_questions_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_16every_game_times___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_16every_game_times_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_23every_difficulties_game___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_23every_difficulties_game_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_ques___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_ques_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_ques___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_ques_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_ques___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_ques_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_17every_match_times___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_17every_match_times_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_24every_difficulty_matches___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_24every_difficulty_matches_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_game___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_game_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_game___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_game_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_game___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_game_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_11complex_ses___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_11complex_ses_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_8time_ses___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_8time_ses_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_19time_percentage_ses___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_19time_percentage_ses_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_17expected_time_ses___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_17expected_time_ses_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_25every_quan_feedback_match___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_25every_quan_feedback_match_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_26every_grade_feedback_match___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_26every_grade_feedback_match_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_24every_val_feedback_match___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_24every_val_feedback_match_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_20every_question_kinds___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_20every_question_kinds_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_30every_question_percentage_errs___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_30every_question_percentage_errs_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_26every_time_percentage_game___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_26every_time_percentage_game_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_27every_time_percentage_match___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_27every_time_percentage_match_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_14percentage_ses___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_14percentage_ses_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_26every_expected_match_times___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_26every_expected_match_times_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_25every_expected_game_times___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_25every_expected_game_times_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_16every_kind_games___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_16every_kind_games_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_15questions_feeds___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_15questions_feeds_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_13matches_feeds___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_13matches_feeds_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_11games_feeds___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_11games_feeds_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_17the_session_feeds___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static int __pyx_pf_16feedback_manager_8Feedback_17the_session_feeds_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_8takeout_feeds_question(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_10takeout_feeds_game(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_12takeout_feeds_match(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_14takeout_feeds_session(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_16evaluate_whole_feedbacks(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_18feedbacks_insertion(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_20__reduce_cython__(CYTHON_UNUSED struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_16feedback_manager_8Feedback_22__setstate_cython__(CYTHON_UNUSED struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, CYTHON_UNUSED PyObject *__pyx_v___pyx_state); /* proto */
static PyObject *__pyx_tp_new_16feedback_manager_Feedback(PyTypeObject *t, PyObject *a, PyObject *k); /*proto*/
static PyObject *__pyx_tuple_;
static PyObject *__pyx_tuple__2;
static PyObject *__pyx_tuple__3;
static PyObject *__pyx_tuple__4;
static PyObject *__pyx_tuple__5;
/* Late includes */

/* "feedback_manager.pyx":43
 * # ==========================================================================================================================================================
 * cdef class Feedback:
 * 	def __cinit__(self):             # <<<<<<<<<<<<<<
 * 		self.db_conn = db.DatabaseManager()
 * 
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_1__cinit__(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_1__cinit__(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__cinit__ (wrapper)", 0);
  if (unlikely(PyTuple_GET_SIZE(__pyx_args) > 0)) {
    __Pyx_RaiseArgtupleInvalid("__cinit__", 1, 0, 0, PyTuple_GET_SIZE(__pyx_args)); return -1;}
  if (unlikely(__pyx_kwds) && unlikely(PyDict_Size(__pyx_kwds) > 0) && unlikely(!__Pyx_CheckKeywordStrings(__pyx_kwds, "__cinit__", 0))) return -1;
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback___cinit__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback___cinit__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__cinit__", 0);
  __Pyx_TraceCall("__cinit__", __pyx_f[0], 43, 0, __PYX_ERR(0, 43, __pyx_L1_error));

  /* "feedback_manager.pyx":44
 * cdef class Feedback:
 * 	def __cinit__(self):
 * 		self.db_conn = db.DatabaseManager()             # <<<<<<<<<<<<<<
 * 
 * 		self.every_question = []
 */
  __Pyx_TraceLine(44,0,__PYX_ERR(0, 44, __pyx_L1_error))
  __Pyx_GetModuleGlobalName(__pyx_t_2, __pyx_n_s_db); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 44, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_DatabaseManager); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 44, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_2 = NULL;
  if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_3))) {
    __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_3);
    if (likely(__pyx_t_2)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
      __Pyx_INCREF(__pyx_t_2);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_3, function);
    }
  }
  __pyx_t_1 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 44, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->db_conn);
  __Pyx_DECREF(__pyx_v_self->db_conn);
  __pyx_v_self->db_conn = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":46
 * 		self.db_conn = db.DatabaseManager()
 * 
 * 		self.every_question = []             # <<<<<<<<<<<<<<
 * 		self.every_num_answers = []
 * 		self.every_correct_ans = []
 */
  __Pyx_TraceLine(46,0,__PYX_ERR(0, 46, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 46, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_question);
  __Pyx_DECREF(__pyx_v_self->every_question);
  __pyx_v_self->every_question = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":47
 * 
 * 		self.every_question = []
 * 		self.every_num_answers = []             # <<<<<<<<<<<<<<
 * 		self.every_correct_ans = []
 * 		self.every_errors_ans = []
 */
  __Pyx_TraceLine(47,0,__PYX_ERR(0, 47, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 47, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_num_answers);
  __Pyx_DECREF(__pyx_v_self->every_num_answers);
  __pyx_v_self->every_num_answers = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":48
 * 		self.every_question = []
 * 		self.every_num_answers = []
 * 		self.every_correct_ans = []             # <<<<<<<<<<<<<<
 * 		self.every_errors_ans = []
 * 		self.every_indecisions = []
 */
  __Pyx_TraceLine(48,0,__PYX_ERR(0, 48, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 48, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_correct_ans);
  __Pyx_DECREF(__pyx_v_self->every_correct_ans);
  __pyx_v_self->every_correct_ans = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":49
 * 		self.every_num_answers = []
 * 		self.every_correct_ans = []
 * 		self.every_errors_ans = []             # <<<<<<<<<<<<<<
 * 		self.every_indecisions = []
 * 		self.all_values_questions = []
 */
  __Pyx_TraceLine(49,0,__PYX_ERR(0, 49, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 49, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_errors_ans);
  __Pyx_DECREF(__pyx_v_self->every_errors_ans);
  __pyx_v_self->every_errors_ans = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":50
 * 		self.every_correct_ans = []
 * 		self.every_errors_ans = []
 * 		self.every_indecisions = []             # <<<<<<<<<<<<<<
 * 		self.all_values_questions = []
 * 		self.every_question_times = []
 */
  __Pyx_TraceLine(50,0,__PYX_ERR(0, 50, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 50, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_indecisions);
  __Pyx_DECREF(__pyx_v_self->every_indecisions);
  __pyx_v_self->every_indecisions = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":51
 * 		self.every_errors_ans = []
 * 		self.every_indecisions = []
 * 		self.all_values_questions = []             # <<<<<<<<<<<<<<
 * 		self.every_question_times = []
 * 		self.every_question_kinds = []
 */
  __Pyx_TraceLine(51,0,__PYX_ERR(0, 51, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 51, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->all_values_questions);
  __Pyx_DECREF(__pyx_v_self->all_values_questions);
  __pyx_v_self->all_values_questions = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":52
 * 		self.every_indecisions = []
 * 		self.all_values_questions = []
 * 		self.every_question_times = []             # <<<<<<<<<<<<<<
 * 		self.every_question_kinds = []
 * 		self.every_game_times = []
 */
  __Pyx_TraceLine(52,0,__PYX_ERR(0, 52, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 52, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_question_times);
  __Pyx_DECREF(__pyx_v_self->every_question_times);
  __pyx_v_self->every_question_times = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":53
 * 		self.all_values_questions = []
 * 		self.every_question_times = []
 * 		self.every_question_kinds = []             # <<<<<<<<<<<<<<
 * 		self.every_game_times = []
 * 		self.every_difficulties_game = []
 */
  __Pyx_TraceLine(53,0,__PYX_ERR(0, 53, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 53, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_question_kinds);
  __Pyx_DECREF(__pyx_v_self->every_question_kinds);
  __pyx_v_self->every_question_kinds = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":54
 * 		self.every_question_times = []
 * 		self.every_question_kinds = []
 * 		self.every_game_times = []             # <<<<<<<<<<<<<<
 * 		self.every_difficulties_game = []
 * 		self.every_val_feedback_ques = []
 */
  __Pyx_TraceLine(54,0,__PYX_ERR(0, 54, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 54, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_game_times);
  __Pyx_DECREF(__pyx_v_self->every_game_times);
  __pyx_v_self->every_game_times = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":55
 * 		self.every_question_kinds = []
 * 		self.every_game_times = []
 * 		self.every_difficulties_game = []             # <<<<<<<<<<<<<<
 * 		self.every_val_feedback_ques = []
 * 		self.every_quan_feedback_ques = []
 */
  __Pyx_TraceLine(55,0,__PYX_ERR(0, 55, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 55, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_difficulties_game);
  __Pyx_DECREF(__pyx_v_self->every_difficulties_game);
  __pyx_v_self->every_difficulties_game = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":56
 * 		self.every_game_times = []
 * 		self.every_difficulties_game = []
 * 		self.every_val_feedback_ques = []             # <<<<<<<<<<<<<<
 * 		self.every_quan_feedback_ques = []
 * 		self.every_grade_feedback_ques = []
 */
  __Pyx_TraceLine(56,0,__PYX_ERR(0, 56, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 56, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_val_feedback_ques);
  __Pyx_DECREF(__pyx_v_self->every_val_feedback_ques);
  __pyx_v_self->every_val_feedback_ques = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":57
 * 		self.every_difficulties_game = []
 * 		self.every_val_feedback_ques = []
 * 		self.every_quan_feedback_ques = []             # <<<<<<<<<<<<<<
 * 		self.every_grade_feedback_ques = []
 * 		self.every_match_times = []
 */
  __Pyx_TraceLine(57,0,__PYX_ERR(0, 57, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 57, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_quan_feedback_ques);
  __Pyx_DECREF(__pyx_v_self->every_quan_feedback_ques);
  __pyx_v_self->every_quan_feedback_ques = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":58
 * 		self.every_val_feedback_ques = []
 * 		self.every_quan_feedback_ques = []
 * 		self.every_grade_feedback_ques = []             # <<<<<<<<<<<<<<
 * 		self.every_match_times = []
 * 		self.every_difficulty_matches = []
 */
  __Pyx_TraceLine(58,0,__PYX_ERR(0, 58, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 58, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_grade_feedback_ques);
  __Pyx_DECREF(__pyx_v_self->every_grade_feedback_ques);
  __pyx_v_self->every_grade_feedback_ques = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":59
 * 		self.every_quan_feedback_ques = []
 * 		self.every_grade_feedback_ques = []
 * 		self.every_match_times = []             # <<<<<<<<<<<<<<
 * 		self.every_difficulty_matches = []
 * 		self.every_quan_feedback_game = []
 */
  __Pyx_TraceLine(59,0,__PYX_ERR(0, 59, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 59, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_match_times);
  __Pyx_DECREF(__pyx_v_self->every_match_times);
  __pyx_v_self->every_match_times = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":60
 * 		self.every_grade_feedback_ques = []
 * 		self.every_match_times = []
 * 		self.every_difficulty_matches = []             # <<<<<<<<<<<<<<
 * 		self.every_quan_feedback_game = []
 * 		self.every_grade_feedback_game = []
 */
  __Pyx_TraceLine(60,0,__PYX_ERR(0, 60, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 60, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_difficulty_matches);
  __Pyx_DECREF(__pyx_v_self->every_difficulty_matches);
  __pyx_v_self->every_difficulty_matches = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":61
 * 		self.every_match_times = []
 * 		self.every_difficulty_matches = []
 * 		self.every_quan_feedback_game = []             # <<<<<<<<<<<<<<
 * 		self.every_grade_feedback_game = []
 * 		self.every_val_feedback_game = []
 */
  __Pyx_TraceLine(61,0,__PYX_ERR(0, 61, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 61, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_quan_feedback_game);
  __Pyx_DECREF(__pyx_v_self->every_quan_feedback_game);
  __pyx_v_self->every_quan_feedback_game = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":62
 * 		self.every_difficulty_matches = []
 * 		self.every_quan_feedback_game = []
 * 		self.every_grade_feedback_game = []             # <<<<<<<<<<<<<<
 * 		self.every_val_feedback_game = []
 * 		self.every_quan_feedback_match = []
 */
  __Pyx_TraceLine(62,0,__PYX_ERR(0, 62, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 62, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_grade_feedback_game);
  __Pyx_DECREF(__pyx_v_self->every_grade_feedback_game);
  __pyx_v_self->every_grade_feedback_game = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":63
 * 		self.every_quan_feedback_game = []
 * 		self.every_grade_feedback_game = []
 * 		self.every_val_feedback_game = []             # <<<<<<<<<<<<<<
 * 		self.every_quan_feedback_match = []
 * 		self.every_grade_feedback_match = []
 */
  __Pyx_TraceLine(63,0,__PYX_ERR(0, 63, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 63, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_val_feedback_game);
  __Pyx_DECREF(__pyx_v_self->every_val_feedback_game);
  __pyx_v_self->every_val_feedback_game = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":64
 * 		self.every_grade_feedback_game = []
 * 		self.every_val_feedback_game = []
 * 		self.every_quan_feedback_match = []             # <<<<<<<<<<<<<<
 * 		self.every_grade_feedback_match = []
 * 		self.every_val_feedback_match = []
 */
  __Pyx_TraceLine(64,0,__PYX_ERR(0, 64, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 64, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_quan_feedback_match);
  __Pyx_DECREF(__pyx_v_self->every_quan_feedback_match);
  __pyx_v_self->every_quan_feedback_match = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":65
 * 		self.every_val_feedback_game = []
 * 		self.every_quan_feedback_match = []
 * 		self.every_grade_feedback_match = []             # <<<<<<<<<<<<<<
 * 		self.every_val_feedback_match = []
 * 		self.every_question_percentage_errs = []
 */
  __Pyx_TraceLine(65,0,__PYX_ERR(0, 65, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 65, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_grade_feedback_match);
  __Pyx_DECREF(__pyx_v_self->every_grade_feedback_match);
  __pyx_v_self->every_grade_feedback_match = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":66
 * 		self.every_quan_feedback_match = []
 * 		self.every_grade_feedback_match = []
 * 		self.every_val_feedback_match = []             # <<<<<<<<<<<<<<
 * 		self.every_question_percentage_errs = []
 * 		self.every_time_percentage_game = []
 */
  __Pyx_TraceLine(66,0,__PYX_ERR(0, 66, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 66, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_val_feedback_match);
  __Pyx_DECREF(__pyx_v_self->every_val_feedback_match);
  __pyx_v_self->every_val_feedback_match = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":67
 * 		self.every_grade_feedback_match = []
 * 		self.every_val_feedback_match = []
 * 		self.every_question_percentage_errs = []             # <<<<<<<<<<<<<<
 * 		self.every_time_percentage_game = []
 * 		self.every_time_percentage_match = []
 */
  __Pyx_TraceLine(67,0,__PYX_ERR(0, 67, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 67, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_question_percentage_errs);
  __Pyx_DECREF(__pyx_v_self->every_question_percentage_errs);
  __pyx_v_self->every_question_percentage_errs = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":68
 * 		self.every_val_feedback_match = []
 * 		self.every_question_percentage_errs = []
 * 		self.every_time_percentage_game = []             # <<<<<<<<<<<<<<
 * 		self.every_time_percentage_match = []
 * 		self.every_expected_game_times = []
 */
  __Pyx_TraceLine(68,0,__PYX_ERR(0, 68, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 68, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_time_percentage_game);
  __Pyx_DECREF(__pyx_v_self->every_time_percentage_game);
  __pyx_v_self->every_time_percentage_game = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":69
 * 		self.every_question_percentage_errs = []
 * 		self.every_time_percentage_game = []
 * 		self.every_time_percentage_match = []             # <<<<<<<<<<<<<<
 * 		self.every_expected_game_times = []
 * 		self.every_expected_match_times = []
 */
  __Pyx_TraceLine(69,0,__PYX_ERR(0, 69, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 69, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_time_percentage_match);
  __Pyx_DECREF(__pyx_v_self->every_time_percentage_match);
  __pyx_v_self->every_time_percentage_match = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":70
 * 		self.every_time_percentage_game = []
 * 		self.every_time_percentage_match = []
 * 		self.every_expected_game_times = []             # <<<<<<<<<<<<<<
 * 		self.every_expected_match_times = []
 * 		self.the_session_feeds = []
 */
  __Pyx_TraceLine(70,0,__PYX_ERR(0, 70, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 70, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_expected_game_times);
  __Pyx_DECREF(__pyx_v_self->every_expected_game_times);
  __pyx_v_self->every_expected_game_times = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":71
 * 		self.every_time_percentage_match = []
 * 		self.every_expected_game_times = []
 * 		self.every_expected_match_times = []             # <<<<<<<<<<<<<<
 * 		self.the_session_feeds = []
 * 		self.matches_feeds = []
 */
  __Pyx_TraceLine(71,0,__PYX_ERR(0, 71, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 71, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->every_expected_match_times);
  __Pyx_DECREF(__pyx_v_self->every_expected_match_times);
  __pyx_v_self->every_expected_match_times = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":72
 * 		self.every_expected_game_times = []
 * 		self.every_expected_match_times = []
 * 		self.the_session_feeds = []             # <<<<<<<<<<<<<<
 * 		self.matches_feeds = []
 * 		self.games_feeds = []
 */
  __Pyx_TraceLine(72,0,__PYX_ERR(0, 72, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 72, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->the_session_feeds);
  __Pyx_DECREF(__pyx_v_self->the_session_feeds);
  __pyx_v_self->the_session_feeds = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":73
 * 		self.every_expected_match_times = []
 * 		self.the_session_feeds = []
 * 		self.matches_feeds = []             # <<<<<<<<<<<<<<
 * 		self.games_feeds = []
 * 		self.questions_feeds = []
 */
  __Pyx_TraceLine(73,0,__PYX_ERR(0, 73, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 73, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->matches_feeds);
  __Pyx_DECREF(__pyx_v_self->matches_feeds);
  __pyx_v_self->matches_feeds = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":74
 * 		self.the_session_feeds = []
 * 		self.matches_feeds = []
 * 		self.games_feeds = []             # <<<<<<<<<<<<<<
 * 		self.questions_feeds = []
 * 		#self.user_dict_sess = {"task_preli": self.preliminary_activities_session,"task_begin_play":self.go_to_match,"task_finals":self.final_activities_session,"task_update_child":self.update_kid_stats} #for fsm!
 */
  __Pyx_TraceLine(74,0,__PYX_ERR(0, 74, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 74, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->games_feeds);
  __Pyx_DECREF(__pyx_v_self->games_feeds);
  __pyx_v_self->games_feeds = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":75
 * 		self.matches_feeds = []
 * 		self.games_feeds = []
 * 		self.questions_feeds = []             # <<<<<<<<<<<<<<
 * 		#self.user_dict_sess = {"task_preli": self.preliminary_activities_session,"task_begin_play":self.go_to_match,"task_finals":self.final_activities_session,"task_update_child":self.update_kid_stats} #for fsm!
 * 	def __enter__(self):
 */
  __Pyx_TraceLine(75,0,__PYX_ERR(0, 75, __pyx_L1_error))
  __pyx_t_1 = PyList_New(0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 75, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->questions_feeds);
  __Pyx_DECREF(__pyx_v_self->questions_feeds);
  __pyx_v_self->questions_feeds = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":43
 * # ==========================================================================================================================================================
 * cdef class Feedback:
 * 	def __cinit__(self):             # <<<<<<<<<<<<<<
 * 		self.db_conn = db.DatabaseManager()
 * 
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_AddTraceback("feedback_manager.Feedback.__cinit__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":77
 * 		self.questions_feeds = []
 * 		#self.user_dict_sess = {"task_preli": self.preliminary_activities_session,"task_begin_play":self.go_to_match,"task_finals":self.final_activities_session,"task_update_child":self.update_kid_stats} #for fsm!
 * 	def __enter__(self):             # <<<<<<<<<<<<<<
 * 		print("Creating Feeds...")
 * 
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_3__enter__(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_16feedback_manager_8Feedback_2__enter__[] = "Feedback.__enter__(self)";
static PyObject *__pyx_pw_16feedback_manager_8Feedback_3__enter__(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__enter__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_2__enter__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_2__enter__(CYTHON_UNUSED struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__enter__", 0);
  __Pyx_TraceCall("__enter__", __pyx_f[0], 77, 0, __PYX_ERR(0, 77, __pyx_L1_error));

  /* "feedback_manager.pyx":78
 * 		#self.user_dict_sess = {"task_preli": self.preliminary_activities_session,"task_begin_play":self.go_to_match,"task_finals":self.final_activities_session,"task_update_child":self.update_kid_stats} #for fsm!
 * 	def __enter__(self):
 * 		print("Creating Feeds...")             # <<<<<<<<<<<<<<
 * 
 * 	def __exit__(self, exc_type, exc_val, exc_tb):
 */
  __Pyx_TraceLine(78,0,__PYX_ERR(0, 78, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_Call(__pyx_builtin_print, __pyx_tuple_, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 78, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":77
 * 		self.questions_feeds = []
 * 		#self.user_dict_sess = {"task_preli": self.preliminary_activities_session,"task_begin_play":self.go_to_match,"task_finals":self.final_activities_session,"task_update_child":self.update_kid_stats} #for fsm!
 * 	def __enter__(self):             # <<<<<<<<<<<<<<
 * 		print("Creating Feeds...")
 * 
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.__enter__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":80
 * 		print("Creating Feeds...")
 * 
 * 	def __exit__(self, exc_type, exc_val, exc_tb):             # <<<<<<<<<<<<<<
 * 		print("All feedbacks were created successfully.")
 * 
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_5__exit__(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static char __pyx_doc_16feedback_manager_8Feedback_4__exit__[] = "Feedback.__exit__(self, exc_type, exc_val, exc_tb)";
static PyObject *__pyx_pw_16feedback_manager_8Feedback_5__exit__(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  CYTHON_UNUSED PyObject *__pyx_v_exc_type = 0;
  CYTHON_UNUSED PyObject *__pyx_v_exc_val = 0;
  CYTHON_UNUSED PyObject *__pyx_v_exc_tb = 0;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__exit__ (wrapper)", 0);
  {
    static PyObject **__pyx_pyargnames[] = {&__pyx_n_s_exc_type,&__pyx_n_s_exc_val,&__pyx_n_s_exc_tb,0};
    PyObject* values[3] = {0,0,0};
    if (unlikely(__pyx_kwds)) {
      Py_ssize_t kw_args;
      const Py_ssize_t pos_args = PyTuple_GET_SIZE(__pyx_args);
      switch (pos_args) {
        case  3: values[2] = PyTuple_GET_ITEM(__pyx_args, 2);
        CYTHON_FALLTHROUGH;
        case  2: values[1] = PyTuple_GET_ITEM(__pyx_args, 1);
        CYTHON_FALLTHROUGH;
        case  1: values[0] = PyTuple_GET_ITEM(__pyx_args, 0);
        CYTHON_FALLTHROUGH;
        case  0: break;
        default: goto __pyx_L5_argtuple_error;
      }
      kw_args = PyDict_Size(__pyx_kwds);
      switch (pos_args) {
        case  0:
        if (likely((values[0] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_exc_type)) != 0)) kw_args--;
        else goto __pyx_L5_argtuple_error;
        CYTHON_FALLTHROUGH;
        case  1:
        if (likely((values[1] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_exc_val)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("__exit__", 1, 3, 3, 1); __PYX_ERR(0, 80, __pyx_L3_error)
        }
        CYTHON_FALLTHROUGH;
        case  2:
        if (likely((values[2] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_exc_tb)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("__exit__", 1, 3, 3, 2); __PYX_ERR(0, 80, __pyx_L3_error)
        }
      }
      if (unlikely(kw_args > 0)) {
        if (unlikely(__Pyx_ParseOptionalKeywords(__pyx_kwds, __pyx_pyargnames, 0, values, pos_args, "__exit__") < 0)) __PYX_ERR(0, 80, __pyx_L3_error)
      }
    } else if (PyTuple_GET_SIZE(__pyx_args) != 3) {
      goto __pyx_L5_argtuple_error;
    } else {
      values[0] = PyTuple_GET_ITEM(__pyx_args, 0);
      values[1] = PyTuple_GET_ITEM(__pyx_args, 1);
      values[2] = PyTuple_GET_ITEM(__pyx_args, 2);
    }
    __pyx_v_exc_type = values[0];
    __pyx_v_exc_val = values[1];
    __pyx_v_exc_tb = values[2];
  }
  goto __pyx_L4_argument_unpacking_done;
  __pyx_L5_argtuple_error:;
  __Pyx_RaiseArgtupleInvalid("__exit__", 1, 3, 3, PyTuple_GET_SIZE(__pyx_args)); __PYX_ERR(0, 80, __pyx_L3_error)
  __pyx_L3_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.__exit__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __Pyx_RefNannyFinishContext();
  return NULL;
  __pyx_L4_argument_unpacking_done:;
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_4__exit__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), __pyx_v_exc_type, __pyx_v_exc_val, __pyx_v_exc_tb);

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_4__exit__(CYTHON_UNUSED struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, CYTHON_UNUSED PyObject *__pyx_v_exc_type, CYTHON_UNUSED PyObject *__pyx_v_exc_val, CYTHON_UNUSED PyObject *__pyx_v_exc_tb) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__exit__", 0);
  __Pyx_TraceCall("__exit__", __pyx_f[0], 80, 0, __PYX_ERR(0, 80, __pyx_L1_error));

  /* "feedback_manager.pyx":81
 * 
 * 	def __exit__(self, exc_type, exc_val, exc_tb):
 * 		print("All feedbacks were created successfully.")             # <<<<<<<<<<<<<<
 * 
 * 
 */
  __Pyx_TraceLine(81,0,__PYX_ERR(0, 81, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_Call(__pyx_builtin_print, __pyx_tuple__2, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 81, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":80
 * 		print("Creating Feeds...")
 * 
 * 	def __exit__(self, exc_type, exc_val, exc_tb):             # <<<<<<<<<<<<<<
 * 		print("All feedbacks were created successfully.")
 * 
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.__exit__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":84
 * 
 * 
 * 	def __eq__(self, other):             # <<<<<<<<<<<<<<
 * 		return self.quantity_feedback_questions==other.quantity_feedback_questions \
 * 		and self.grade_feedback_questions==other.grade_feedback_questions \
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_7__eq__(PyObject *__pyx_v_self, PyObject *__pyx_v_other); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_7__eq__(PyObject *__pyx_v_self, PyObject *__pyx_v_other) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__eq__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_6__eq__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_other));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_6__eq__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_other) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  int __pyx_t_4;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__eq__", 0);
  __Pyx_TraceCall("__eq__", __pyx_f[0], 84, 0, __PYX_ERR(0, 84, __pyx_L1_error));

  /* "feedback_manager.pyx":85
 * 
 * 	def __eq__(self, other):
 * 		return self.quantity_feedback_questions==other.quantity_feedback_questions \             # <<<<<<<<<<<<<<
 * 		and self.grade_feedback_questions==other.grade_feedback_questions \
 * 		and self.value_feedback_questions==other.value_feedback_questions \
 */
  __Pyx_TraceLine(85,0,__PYX_ERR(0, 85, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);

  /* "feedback_manager.pyx":86
 * 	def __eq__(self, other):
 * 		return self.quantity_feedback_questions==other.quantity_feedback_questions \
 * 		and self.grade_feedback_questions==other.grade_feedback_questions \             # <<<<<<<<<<<<<<
 * 		and self.value_feedback_questions==other.value_feedback_questions \
 * 		and self.quantity_feedback_games==other.quantity_feedback_games \
 */
  __Pyx_TraceLine(86,0,__PYX_ERR(0, 86, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_quantity_feedback_questions); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 85, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = PyObject_RichCompare(__pyx_v_self->quantity_feedback_questions, __pyx_t_2, Py_EQ); __Pyx_XGOTREF(__pyx_t_3); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 85, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 85, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_3);
    __pyx_t_1 = __pyx_t_3;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":87
 * 		return self.quantity_feedback_questions==other.quantity_feedback_questions \
 * 		and self.grade_feedback_questions==other.grade_feedback_questions \
 * 		and self.value_feedback_questions==other.value_feedback_questions \             # <<<<<<<<<<<<<<
 * 		and self.quantity_feedback_games==other.quantity_feedback_games \
 * 		and self.grade_feedback_games==other.grade_feedback_games \
 */
  __Pyx_TraceLine(87,0,__PYX_ERR(0, 87, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_grade_feedback_questions); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 86, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_2 = PyObject_RichCompare(__pyx_v_self->grade_feedback_questions, __pyx_t_3, Py_EQ); __Pyx_XGOTREF(__pyx_t_2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 86, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 86, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_2);
    __pyx_t_1 = __pyx_t_2;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":88
 * 		and self.grade_feedback_questions==other.grade_feedback_questions \
 * 		and self.value_feedback_questions==other.value_feedback_questions \
 * 		and self.quantity_feedback_games==other.quantity_feedback_games \             # <<<<<<<<<<<<<<
 * 		and self.grade_feedback_games==other.grade_feedback_games \
 * 		and self.value_feedback_games==other.value_feedback_games \
 */
  __Pyx_TraceLine(88,0,__PYX_ERR(0, 88, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyInt_From_int(__pyx_v_self->value_feedback_questions); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 87, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);

  /* "feedback_manager.pyx":87
 * 		return self.quantity_feedback_questions==other.quantity_feedback_questions \
 * 		and self.grade_feedback_questions==other.grade_feedback_questions \
 * 		and self.value_feedback_questions==other.value_feedback_questions \             # <<<<<<<<<<<<<<
 * 		and self.quantity_feedback_games==other.quantity_feedback_games \
 * 		and self.grade_feedback_games==other.grade_feedback_games \
 */
  __Pyx_TraceLine(87,0,__PYX_ERR(0, 87, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_value_feedback_questions); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 87, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_5 = PyObject_RichCompare(__pyx_t_2, __pyx_t_3, Py_EQ); __Pyx_XGOTREF(__pyx_t_5); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 87, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_5); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 87, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_5);
    __pyx_t_1 = __pyx_t_5;
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":88
 * 		and self.grade_feedback_questions==other.grade_feedback_questions \
 * 		and self.value_feedback_questions==other.value_feedback_questions \
 * 		and self.quantity_feedback_games==other.quantity_feedback_games \             # <<<<<<<<<<<<<<
 * 		and self.grade_feedback_games==other.grade_feedback_games \
 * 		and self.value_feedback_games==other.value_feedback_games \
 */
  __Pyx_TraceLine(88,0,__PYX_ERR(0, 88, __pyx_L1_error))
  __pyx_t_5 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_quantity_feedback_games); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 88, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_5);
  __pyx_t_3 = PyObject_RichCompare(__pyx_v_self->quantity_feedback_games, __pyx_t_5, Py_EQ); __Pyx_XGOTREF(__pyx_t_3); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 88, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 88, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_3);
    __pyx_t_1 = __pyx_t_3;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":89
 * 		and self.value_feedback_questions==other.value_feedback_questions \
 * 		and self.quantity_feedback_games==other.quantity_feedback_games \
 * 		and self.grade_feedback_games==other.grade_feedback_games \             # <<<<<<<<<<<<<<
 * 		and self.value_feedback_games==other.value_feedback_games \
 * 		and self.quantity_feedback_matches==other.quantity_feedback_matches \
 */
  __Pyx_TraceLine(89,0,__PYX_ERR(0, 89, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_grade_feedback_games); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 89, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_5 = PyObject_RichCompare(__pyx_v_self->grade_feedback_games, __pyx_t_3, Py_EQ); __Pyx_XGOTREF(__pyx_t_5); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 89, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_5); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 89, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_5);
    __pyx_t_1 = __pyx_t_5;
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":90
 * 		and self.quantity_feedback_games==other.quantity_feedback_games \
 * 		and self.grade_feedback_games==other.grade_feedback_games \
 * 		and self.value_feedback_games==other.value_feedback_games \             # <<<<<<<<<<<<<<
 * 		and self.quantity_feedback_matches==other.quantity_feedback_matches \
 * 		and self.grade_feedback_matches==other.grade_feedback_matches \
 */
  __Pyx_TraceLine(90,0,__PYX_ERR(0, 90, __pyx_L1_error))
  __pyx_t_5 = __Pyx_PyInt_From_int(__pyx_v_self->value_feedback_games); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 90, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_5);
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_value_feedback_games); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 90, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_2 = PyObject_RichCompare(__pyx_t_5, __pyx_t_3, Py_EQ); __Pyx_XGOTREF(__pyx_t_2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 90, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 90, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_2);
    __pyx_t_1 = __pyx_t_2;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":91
 * 		and self.grade_feedback_games==other.grade_feedback_games \
 * 		and self.value_feedback_games==other.value_feedback_games \
 * 		and self.quantity_feedback_matches==other.quantity_feedback_matches \             # <<<<<<<<<<<<<<
 * 		and self.grade_feedback_matches==other.grade_feedback_matches \
 * 		and self.value_feedback_matches==other.value_feedback_matches \
 */
  __Pyx_TraceLine(91,0,__PYX_ERR(0, 91, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_quantity_feedback_matches); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 91, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = PyObject_RichCompare(__pyx_v_self->quantity_feedback_matches, __pyx_t_2, Py_EQ); __Pyx_XGOTREF(__pyx_t_3); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 91, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 91, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_3);
    __pyx_t_1 = __pyx_t_3;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":92
 * 		and self.value_feedback_games==other.value_feedback_games \
 * 		and self.quantity_feedback_matches==other.quantity_feedback_matches \
 * 		and self.grade_feedback_matches==other.grade_feedback_matches \             # <<<<<<<<<<<<<<
 * 		and self.value_feedback_matches==other.value_feedback_matches \
 * 		and self.quantity_feedback_matches==other.quantity_feedback_matches \
 */
  __Pyx_TraceLine(92,0,__PYX_ERR(0, 92, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_grade_feedback_matches); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 92, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_2 = PyObject_RichCompare(__pyx_v_self->grade_feedback_matches, __pyx_t_3, Py_EQ); __Pyx_XGOTREF(__pyx_t_2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 92, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 92, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_2);
    __pyx_t_1 = __pyx_t_2;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":93
 * 		and self.quantity_feedback_matches==other.quantity_feedback_matches \
 * 		and self.grade_feedback_matches==other.grade_feedback_matches \
 * 		and self.value_feedback_matches==other.value_feedback_matches \             # <<<<<<<<<<<<<<
 * 		and self.quantity_feedback_matches==other.quantity_feedback_matches \
 * 		and self.grade_feedback_matches==other.grade_feedback_matches \
 */
  __Pyx_TraceLine(93,0,__PYX_ERR(0, 93, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyInt_From_int(__pyx_v_self->value_feedback_matches); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 93, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_value_feedback_matches); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 93, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_5 = PyObject_RichCompare(__pyx_t_2, __pyx_t_3, Py_EQ); __Pyx_XGOTREF(__pyx_t_5); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 93, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_5); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 93, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_5);
    __pyx_t_1 = __pyx_t_5;
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":94
 * 		and self.grade_feedback_matches==other.grade_feedback_matches \
 * 		and self.value_feedback_matches==other.value_feedback_matches \
 * 		and self.quantity_feedback_matches==other.quantity_feedback_matches \             # <<<<<<<<<<<<<<
 * 		and self.grade_feedback_matches==other.grade_feedback_matches \
 * 		and self.value_feedback_matches==other.value_feedback_matches \
 */
  __Pyx_TraceLine(94,0,__PYX_ERR(0, 94, __pyx_L1_error))
  __pyx_t_5 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_quantity_feedback_matches); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 94, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_5);
  __pyx_t_3 = PyObject_RichCompare(__pyx_v_self->quantity_feedback_matches, __pyx_t_5, Py_EQ); __Pyx_XGOTREF(__pyx_t_3); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 94, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 94, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_3);
    __pyx_t_1 = __pyx_t_3;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":95
 * 		and self.value_feedback_matches==other.value_feedback_matches \
 * 		and self.quantity_feedback_matches==other.quantity_feedback_matches \
 * 		and self.grade_feedback_matches==other.grade_feedback_matches \             # <<<<<<<<<<<<<<
 * 		and self.value_feedback_matches==other.value_feedback_matches \
 * 		and self.quantity_feedback_session==other.quantity_feedback_session \
 */
  __Pyx_TraceLine(95,0,__PYX_ERR(0, 95, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_grade_feedback_matches); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 95, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_5 = PyObject_RichCompare(__pyx_v_self->grade_feedback_matches, __pyx_t_3, Py_EQ); __Pyx_XGOTREF(__pyx_t_5); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 95, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_5); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 95, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_5);
    __pyx_t_1 = __pyx_t_5;
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":96
 * 		and self.quantity_feedback_matches==other.quantity_feedback_matches \
 * 		and self.grade_feedback_matches==other.grade_feedback_matches \
 * 		and self.value_feedback_matches==other.value_feedback_matches \             # <<<<<<<<<<<<<<
 * 		and self.quantity_feedback_session==other.quantity_feedback_session \
 * 		and self.grade_feedback_session==other.grade_feedback_session \
 */
  __Pyx_TraceLine(96,0,__PYX_ERR(0, 96, __pyx_L1_error))
  __pyx_t_5 = __Pyx_PyInt_From_int(__pyx_v_self->value_feedback_matches); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 96, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_5);
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_value_feedback_matches); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 96, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_2 = PyObject_RichCompare(__pyx_t_5, __pyx_t_3, Py_EQ); __Pyx_XGOTREF(__pyx_t_2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 96, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 96, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_2);
    __pyx_t_1 = __pyx_t_2;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":97
 * 		and self.grade_feedback_matches==other.grade_feedback_matches \
 * 		and self.value_feedback_matches==other.value_feedback_matches \
 * 		and self.quantity_feedback_session==other.quantity_feedback_session \             # <<<<<<<<<<<<<<
 * 		and self.grade_feedback_session==other.grade_feedback_session \
 * 		and self.value_feedback_session==other.value_feedback_session
 */
  __Pyx_TraceLine(97,0,__PYX_ERR(0, 97, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_quantity_feedback_session); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 97, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = PyObject_RichCompare(__pyx_v_self->quantity_feedback_session, __pyx_t_2, Py_EQ); __Pyx_XGOTREF(__pyx_t_3); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 97, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 97, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_3);
    __pyx_t_1 = __pyx_t_3;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":98
 * 		and self.value_feedback_matches==other.value_feedback_matches \
 * 		and self.quantity_feedback_session==other.quantity_feedback_session \
 * 		and self.grade_feedback_session==other.grade_feedback_session \             # <<<<<<<<<<<<<<
 * 		and self.value_feedback_session==other.value_feedback_session
 * 	# -----------------------------------------------------------------------------
 */
  __Pyx_TraceLine(98,0,__PYX_ERR(0, 98, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_grade_feedback_session); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 98, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_2 = PyObject_RichCompare(__pyx_v_self->grade_feedback_session, __pyx_t_3, Py_EQ); __Pyx_XGOTREF(__pyx_t_2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 98, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 98, __pyx_L1_error)
  if (__pyx_t_4) {
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  } else {
    __Pyx_INCREF(__pyx_t_2);
    __pyx_t_1 = __pyx_t_2;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    goto __pyx_L3_bool_binop_done;
  }

  /* "feedback_manager.pyx":99
 * 		and self.quantity_feedback_session==other.quantity_feedback_session \
 * 		and self.grade_feedback_session==other.grade_feedback_session \
 * 		and self.value_feedback_session==other.value_feedback_session             # <<<<<<<<<<<<<<
 * 	# -----------------------------------------------------------------------------
 * 	# Getters, setters
 */
  __Pyx_TraceLine(99,0,__PYX_ERR(0, 99, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyInt_From_int(__pyx_v_self->value_feedback_session); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 99, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_other, __pyx_n_s_value_feedback_session); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 99, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_5 = PyObject_RichCompare(__pyx_t_2, __pyx_t_3, Py_EQ); __Pyx_XGOTREF(__pyx_t_5); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 99, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __Pyx_INCREF(__pyx_t_5);
  __pyx_t_1 = __pyx_t_5;
  __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  __pyx_L3_bool_binop_done:;
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* "feedback_manager.pyx":84
 * 
 * 
 * 	def __eq__(self, other):             # <<<<<<<<<<<<<<
 * 		return self.quantity_feedback_questions==other.quantity_feedback_questions \
 * 		and self.grade_feedback_questions==other.grade_feedback_questions \
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("feedback_manager.Feedback.__eq__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":104
 * 	# -----------------------------------------------------------------------------
 * 	@property
 * 	def quantity_feedback_questions(self):             # <<<<<<<<<<<<<<
 * 		return self.quantity_feedback_questions
 * 	@quantity_feedback_questions.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_27quantity_feedback_questions_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_27quantity_feedback_questions_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_27quantity_feedback_questions___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_27quantity_feedback_questions___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 104, 0, __PYX_ERR(0, 104, __pyx_L1_error));

  /* "feedback_manager.pyx":105
 * 	@property
 * 	def quantity_feedback_questions(self):
 * 		return self.quantity_feedback_questions             # <<<<<<<<<<<<<<
 * 	@quantity_feedback_questions.setter
 * 	def quantity_feedback_questions(self, value):
 */
  __Pyx_TraceLine(105,0,__PYX_ERR(0, 105, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->quantity_feedback_questions);
  __pyx_r = __pyx_v_self->quantity_feedback_questions;
  goto __pyx_L0;

  /* "feedback_manager.pyx":104
 * 	# -----------------------------------------------------------------------------
 * 	@property
 * 	def quantity_feedback_questions(self):             # <<<<<<<<<<<<<<
 * 		return self.quantity_feedback_questions
 * 	@quantity_feedback_questions.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.quantity_feedback_questions.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":107
 * 		return self.quantity_feedback_questions
 * 	@quantity_feedback_questions.setter
 * 	def quantity_feedback_questions(self, value):             # <<<<<<<<<<<<<<
 * 		self.quantity_feedback_questions = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_27quantity_feedback_questions_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_27quantity_feedback_questions_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_27quantity_feedback_questions_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_27quantity_feedback_questions_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 107, 0, __PYX_ERR(0, 107, __pyx_L1_error));

  /* "feedback_manager.pyx":108
 * 	@quantity_feedback_questions.setter
 * 	def quantity_feedback_questions(self, value):
 * 		self.quantity_feedback_questions = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def quantity_feedback_games(self):
 */
  __Pyx_TraceLine(108,0,__PYX_ERR(0, 108, __pyx_L1_error))
  if (!(likely(PyUnicode_CheckExact(__pyx_v_value))||((__pyx_v_value) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "unicode", Py_TYPE(__pyx_v_value)->tp_name), 0))) __PYX_ERR(0, 108, __pyx_L1_error)
  __pyx_t_1 = __pyx_v_value;
  __Pyx_INCREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->quantity_feedback_questions);
  __Pyx_DECREF(__pyx_v_self->quantity_feedback_questions);
  __pyx_v_self->quantity_feedback_questions = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":107
 * 		return self.quantity_feedback_questions
 * 	@quantity_feedback_questions.setter
 * 	def quantity_feedback_questions(self, value):             # <<<<<<<<<<<<<<
 * 		self.quantity_feedback_questions = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.quantity_feedback_questions.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":110
 * 		self.quantity_feedback_questions = value
 * 	@property
 * 	def quantity_feedback_games(self):             # <<<<<<<<<<<<<<
 * 		return self.quantity_feedback_games
 * 	@quantity_feedback_games.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_23quantity_feedback_games_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_23quantity_feedback_games_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_23quantity_feedback_games___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_23quantity_feedback_games___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 110, 0, __PYX_ERR(0, 110, __pyx_L1_error));

  /* "feedback_manager.pyx":111
 * 	@property
 * 	def quantity_feedback_games(self):
 * 		return self.quantity_feedback_games             # <<<<<<<<<<<<<<
 * 	@quantity_feedback_games.setter
 * 	def quantity_feedback_games(self, value):
 */
  __Pyx_TraceLine(111,0,__PYX_ERR(0, 111, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->quantity_feedback_games);
  __pyx_r = __pyx_v_self->quantity_feedback_games;
  goto __pyx_L0;

  /* "feedback_manager.pyx":110
 * 		self.quantity_feedback_questions = value
 * 	@property
 * 	def quantity_feedback_games(self):             # <<<<<<<<<<<<<<
 * 		return self.quantity_feedback_games
 * 	@quantity_feedback_games.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.quantity_feedback_games.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":113
 * 		return self.quantity_feedback_games
 * 	@quantity_feedback_games.setter
 * 	def quantity_feedback_games(self, value):             # <<<<<<<<<<<<<<
 * 		self.quantity_feedback_games = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_23quantity_feedback_games_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_23quantity_feedback_games_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_23quantity_feedback_games_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_23quantity_feedback_games_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 113, 0, __PYX_ERR(0, 113, __pyx_L1_error));

  /* "feedback_manager.pyx":114
 * 	@quantity_feedback_games.setter
 * 	def quantity_feedback_games(self, value):
 * 		self.quantity_feedback_games = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def quantity_feedback_matches(self):
 */
  __Pyx_TraceLine(114,0,__PYX_ERR(0, 114, __pyx_L1_error))
  if (!(likely(PyUnicode_CheckExact(__pyx_v_value))||((__pyx_v_value) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "unicode", Py_TYPE(__pyx_v_value)->tp_name), 0))) __PYX_ERR(0, 114, __pyx_L1_error)
  __pyx_t_1 = __pyx_v_value;
  __Pyx_INCREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->quantity_feedback_games);
  __Pyx_DECREF(__pyx_v_self->quantity_feedback_games);
  __pyx_v_self->quantity_feedback_games = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":113
 * 		return self.quantity_feedback_games
 * 	@quantity_feedback_games.setter
 * 	def quantity_feedback_games(self, value):             # <<<<<<<<<<<<<<
 * 		self.quantity_feedback_games = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.quantity_feedback_games.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":116
 * 		self.quantity_feedback_games = value
 * 	@property
 * 	def quantity_feedback_matches(self):             # <<<<<<<<<<<<<<
 * 		return self.quantity_feedback_matches
 * 	@quantity_feedback_matches.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_matches_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_matches_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_matches___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_matches___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 116, 0, __PYX_ERR(0, 116, __pyx_L1_error));

  /* "feedback_manager.pyx":117
 * 	@property
 * 	def quantity_feedback_matches(self):
 * 		return self.quantity_feedback_matches             # <<<<<<<<<<<<<<
 * 	@quantity_feedback_matches.setter
 * 	def quantity_feedback_matches(self, value):
 */
  __Pyx_TraceLine(117,0,__PYX_ERR(0, 117, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->quantity_feedback_matches);
  __pyx_r = __pyx_v_self->quantity_feedback_matches;
  goto __pyx_L0;

  /* "feedback_manager.pyx":116
 * 		self.quantity_feedback_games = value
 * 	@property
 * 	def quantity_feedback_matches(self):             # <<<<<<<<<<<<<<
 * 		return self.quantity_feedback_matches
 * 	@quantity_feedback_matches.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.quantity_feedback_matches.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":119
 * 		return self.quantity_feedback_matches
 * 	@quantity_feedback_matches.setter
 * 	def quantity_feedback_matches(self, value):             # <<<<<<<<<<<<<<
 * 		self.quantity_feedback_matches = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_matches_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_matches_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_matches_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_matches_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 119, 0, __PYX_ERR(0, 119, __pyx_L1_error));

  /* "feedback_manager.pyx":120
 * 	@quantity_feedback_matches.setter
 * 	def quantity_feedback_matches(self, value):
 * 		self.quantity_feedback_matches = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def quantity_feedback_session(self):
 */
  __Pyx_TraceLine(120,0,__PYX_ERR(0, 120, __pyx_L1_error))
  if (!(likely(PyUnicode_CheckExact(__pyx_v_value))||((__pyx_v_value) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "unicode", Py_TYPE(__pyx_v_value)->tp_name), 0))) __PYX_ERR(0, 120, __pyx_L1_error)
  __pyx_t_1 = __pyx_v_value;
  __Pyx_INCREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->quantity_feedback_matches);
  __Pyx_DECREF(__pyx_v_self->quantity_feedback_matches);
  __pyx_v_self->quantity_feedback_matches = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":119
 * 		return self.quantity_feedback_matches
 * 	@quantity_feedback_matches.setter
 * 	def quantity_feedback_matches(self, value):             # <<<<<<<<<<<<<<
 * 		self.quantity_feedback_matches = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.quantity_feedback_matches.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":122
 * 		self.quantity_feedback_matches = value
 * 	@property
 * 	def quantity_feedback_session(self):             # <<<<<<<<<<<<<<
 * 		return self.quantity_feedback_session
 * 	@quantity_feedback_session.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_session_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_session_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_session___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_session___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 122, 0, __PYX_ERR(0, 122, __pyx_L1_error));

  /* "feedback_manager.pyx":123
 * 	@property
 * 	def quantity_feedback_session(self):
 * 		return self.quantity_feedback_session             # <<<<<<<<<<<<<<
 * 	@quantity_feedback_session.setter
 * 	def quantity_feedback_session(self, value):
 */
  __Pyx_TraceLine(123,0,__PYX_ERR(0, 123, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->quantity_feedback_session);
  __pyx_r = __pyx_v_self->quantity_feedback_session;
  goto __pyx_L0;

  /* "feedback_manager.pyx":122
 * 		self.quantity_feedback_matches = value
 * 	@property
 * 	def quantity_feedback_session(self):             # <<<<<<<<<<<<<<
 * 		return self.quantity_feedback_session
 * 	@quantity_feedback_session.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.quantity_feedback_session.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":125
 * 		return self.quantity_feedback_session
 * 	@quantity_feedback_session.setter
 * 	def quantity_feedback_session(self, value):             # <<<<<<<<<<<<<<
 * 		self.quantity_feedback_session = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_session_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_session_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_session_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_25quantity_feedback_session_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 125, 0, __PYX_ERR(0, 125, __pyx_L1_error));

  /* "feedback_manager.pyx":126
 * 	@quantity_feedback_session.setter
 * 	def quantity_feedback_session(self, value):
 * 		self.quantity_feedback_session = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def grade_feedback_questions(self):
 */
  __Pyx_TraceLine(126,0,__PYX_ERR(0, 126, __pyx_L1_error))
  if (!(likely(PyUnicode_CheckExact(__pyx_v_value))||((__pyx_v_value) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "unicode", Py_TYPE(__pyx_v_value)->tp_name), 0))) __PYX_ERR(0, 126, __pyx_L1_error)
  __pyx_t_1 = __pyx_v_value;
  __Pyx_INCREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->quantity_feedback_session);
  __Pyx_DECREF(__pyx_v_self->quantity_feedback_session);
  __pyx_v_self->quantity_feedback_session = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":125
 * 		return self.quantity_feedback_session
 * 	@quantity_feedback_session.setter
 * 	def quantity_feedback_session(self, value):             # <<<<<<<<<<<<<<
 * 		self.quantity_feedback_session = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.quantity_feedback_session.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":128
 * 		self.quantity_feedback_session = value
 * 	@property
 * 	def grade_feedback_questions(self):             # <<<<<<<<<<<<<<
 * 		return self.grade_feedback_questions
 * 	@grade_feedback_questions.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24grade_feedback_questions_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24grade_feedback_questions_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24grade_feedback_questions___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_24grade_feedback_questions___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 128, 0, __PYX_ERR(0, 128, __pyx_L1_error));

  /* "feedback_manager.pyx":129
 * 	@property
 * 	def grade_feedback_questions(self):
 * 		return self.grade_feedback_questions             # <<<<<<<<<<<<<<
 * 	@grade_feedback_questions.setter
 * 	def grade_feedback_questions(self, value):
 */
  __Pyx_TraceLine(129,0,__PYX_ERR(0, 129, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->grade_feedback_questions);
  __pyx_r = __pyx_v_self->grade_feedback_questions;
  goto __pyx_L0;

  /* "feedback_manager.pyx":128
 * 		self.quantity_feedback_session = value
 * 	@property
 * 	def grade_feedback_questions(self):             # <<<<<<<<<<<<<<
 * 		return self.grade_feedback_questions
 * 	@grade_feedback_questions.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.grade_feedback_questions.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":131
 * 		return self.grade_feedback_questions
 * 	@grade_feedback_questions.setter
 * 	def grade_feedback_questions(self, value):             # <<<<<<<<<<<<<<
 * 		self.grade_feedback_questions = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_24grade_feedback_questions_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_24grade_feedback_questions_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24grade_feedback_questions_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_24grade_feedback_questions_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 131, 0, __PYX_ERR(0, 131, __pyx_L1_error));

  /* "feedback_manager.pyx":132
 * 	@grade_feedback_questions.setter
 * 	def grade_feedback_questions(self, value):
 * 		self.grade_feedback_questions = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def grade_feedback_games(self):
 */
  __Pyx_TraceLine(132,0,__PYX_ERR(0, 132, __pyx_L1_error))
  if (!(likely(PyUnicode_CheckExact(__pyx_v_value))||((__pyx_v_value) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "unicode", Py_TYPE(__pyx_v_value)->tp_name), 0))) __PYX_ERR(0, 132, __pyx_L1_error)
  __pyx_t_1 = __pyx_v_value;
  __Pyx_INCREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->grade_feedback_questions);
  __Pyx_DECREF(__pyx_v_self->grade_feedback_questions);
  __pyx_v_self->grade_feedback_questions = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":131
 * 		return self.grade_feedback_questions
 * 	@grade_feedback_questions.setter
 * 	def grade_feedback_questions(self, value):             # <<<<<<<<<<<<<<
 * 		self.grade_feedback_questions = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.grade_feedback_questions.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":134
 * 		self.grade_feedback_questions = value
 * 	@property
 * 	def grade_feedback_games(self):             # <<<<<<<<<<<<<<
 * 		return self.grade_feedback_games
 * 	@grade_feedback_games.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_20grade_feedback_games_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_20grade_feedback_games_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20grade_feedback_games___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_20grade_feedback_games___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 134, 0, __PYX_ERR(0, 134, __pyx_L1_error));

  /* "feedback_manager.pyx":135
 * 	@property
 * 	def grade_feedback_games(self):
 * 		return self.grade_feedback_games             # <<<<<<<<<<<<<<
 * 	@grade_feedback_games.setter
 * 	def grade_feedback_games(self, value):
 */
  __Pyx_TraceLine(135,0,__PYX_ERR(0, 135, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->grade_feedback_games);
  __pyx_r = __pyx_v_self->grade_feedback_games;
  goto __pyx_L0;

  /* "feedback_manager.pyx":134
 * 		self.grade_feedback_questions = value
 * 	@property
 * 	def grade_feedback_games(self):             # <<<<<<<<<<<<<<
 * 		return self.grade_feedback_games
 * 	@grade_feedback_games.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.grade_feedback_games.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":137
 * 		return self.grade_feedback_games
 * 	@grade_feedback_games.setter
 * 	def grade_feedback_games(self, value):             # <<<<<<<<<<<<<<
 * 		self.grade_feedback_games = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_20grade_feedback_games_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_20grade_feedback_games_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20grade_feedback_games_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_20grade_feedback_games_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 137, 0, __PYX_ERR(0, 137, __pyx_L1_error));

  /* "feedback_manager.pyx":138
 * 	@grade_feedback_games.setter
 * 	def grade_feedback_games(self, value):
 * 		self.grade_feedback_games = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def grade_feedback_matches(self):
 */
  __Pyx_TraceLine(138,0,__PYX_ERR(0, 138, __pyx_L1_error))
  if (!(likely(PyUnicode_CheckExact(__pyx_v_value))||((__pyx_v_value) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "unicode", Py_TYPE(__pyx_v_value)->tp_name), 0))) __PYX_ERR(0, 138, __pyx_L1_error)
  __pyx_t_1 = __pyx_v_value;
  __Pyx_INCREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->grade_feedback_games);
  __Pyx_DECREF(__pyx_v_self->grade_feedback_games);
  __pyx_v_self->grade_feedback_games = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":137
 * 		return self.grade_feedback_games
 * 	@grade_feedback_games.setter
 * 	def grade_feedback_games(self, value):             # <<<<<<<<<<<<<<
 * 		self.grade_feedback_games = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.grade_feedback_games.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":140
 * 		self.grade_feedback_games = value
 * 	@property
 * 	def grade_feedback_matches(self):             # <<<<<<<<<<<<<<
 * 		return self.grade_feedback_matches
 * 	@grade_feedback_matches.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_22grade_feedback_matches_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_22grade_feedback_matches_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_22grade_feedback_matches___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_22grade_feedback_matches___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 140, 0, __PYX_ERR(0, 140, __pyx_L1_error));

  /* "feedback_manager.pyx":141
 * 	@property
 * 	def grade_feedback_matches(self):
 * 		return self.grade_feedback_matches             # <<<<<<<<<<<<<<
 * 	@grade_feedback_matches.setter
 * 	def grade_feedback_matches(self, value):
 */
  __Pyx_TraceLine(141,0,__PYX_ERR(0, 141, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->grade_feedback_matches);
  __pyx_r = __pyx_v_self->grade_feedback_matches;
  goto __pyx_L0;

  /* "feedback_manager.pyx":140
 * 		self.grade_feedback_games = value
 * 	@property
 * 	def grade_feedback_matches(self):             # <<<<<<<<<<<<<<
 * 		return self.grade_feedback_matches
 * 	@grade_feedback_matches.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.grade_feedback_matches.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":143
 * 		return self.grade_feedback_matches
 * 	@grade_feedback_matches.setter
 * 	def grade_feedback_matches(self, value):             # <<<<<<<<<<<<<<
 * 		self.grade_feedback_matches = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_22grade_feedback_matches_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_22grade_feedback_matches_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_22grade_feedback_matches_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_22grade_feedback_matches_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 143, 0, __PYX_ERR(0, 143, __pyx_L1_error));

  /* "feedback_manager.pyx":144
 * 	@grade_feedback_matches.setter
 * 	def grade_feedback_matches(self, value):
 * 		self.grade_feedback_matches = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def grade_feedback_session(self):
 */
  __Pyx_TraceLine(144,0,__PYX_ERR(0, 144, __pyx_L1_error))
  if (!(likely(PyUnicode_CheckExact(__pyx_v_value))||((__pyx_v_value) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "unicode", Py_TYPE(__pyx_v_value)->tp_name), 0))) __PYX_ERR(0, 144, __pyx_L1_error)
  __pyx_t_1 = __pyx_v_value;
  __Pyx_INCREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->grade_feedback_matches);
  __Pyx_DECREF(__pyx_v_self->grade_feedback_matches);
  __pyx_v_self->grade_feedback_matches = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":143
 * 		return self.grade_feedback_matches
 * 	@grade_feedback_matches.setter
 * 	def grade_feedback_matches(self, value):             # <<<<<<<<<<<<<<
 * 		self.grade_feedback_matches = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.grade_feedback_matches.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":146
 * 		self.grade_feedback_matches = value
 * 	@property
 * 	def grade_feedback_session(self):             # <<<<<<<<<<<<<<
 * 		return self.grade_feedback_session
 * 	@grade_feedback_session.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_22grade_feedback_session_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_22grade_feedback_session_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_22grade_feedback_session___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_22grade_feedback_session___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 146, 0, __PYX_ERR(0, 146, __pyx_L1_error));

  /* "feedback_manager.pyx":147
 * 	@property
 * 	def grade_feedback_session(self):
 * 		return self.grade_feedback_session             # <<<<<<<<<<<<<<
 * 	@grade_feedback_session.setter
 * 	def grade_feedback_session(self, value):
 */
  __Pyx_TraceLine(147,0,__PYX_ERR(0, 147, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->grade_feedback_session);
  __pyx_r = __pyx_v_self->grade_feedback_session;
  goto __pyx_L0;

  /* "feedback_manager.pyx":146
 * 		self.grade_feedback_matches = value
 * 	@property
 * 	def grade_feedback_session(self):             # <<<<<<<<<<<<<<
 * 		return self.grade_feedback_session
 * 	@grade_feedback_session.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.grade_feedback_session.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":149
 * 		return self.grade_feedback_session
 * 	@grade_feedback_session.setter
 * 	def grade_feedback_session(self, value):             # <<<<<<<<<<<<<<
 * 		self.grade_feedback_session = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_22grade_feedback_session_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_22grade_feedback_session_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_22grade_feedback_session_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_22grade_feedback_session_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 149, 0, __PYX_ERR(0, 149, __pyx_L1_error));

  /* "feedback_manager.pyx":150
 * 	@grade_feedback_session.setter
 * 	def grade_feedback_session(self, value):
 * 		self.grade_feedback_session = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def value_feedback_questions(self):
 */
  __Pyx_TraceLine(150,0,__PYX_ERR(0, 150, __pyx_L1_error))
  if (!(likely(PyUnicode_CheckExact(__pyx_v_value))||((__pyx_v_value) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "unicode", Py_TYPE(__pyx_v_value)->tp_name), 0))) __PYX_ERR(0, 150, __pyx_L1_error)
  __pyx_t_1 = __pyx_v_value;
  __Pyx_INCREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->grade_feedback_session);
  __Pyx_DECREF(__pyx_v_self->grade_feedback_session);
  __pyx_v_self->grade_feedback_session = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":149
 * 		return self.grade_feedback_session
 * 	@grade_feedback_session.setter
 * 	def grade_feedback_session(self, value):             # <<<<<<<<<<<<<<
 * 		self.grade_feedback_session = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.grade_feedback_session.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":152
 * 		self.grade_feedback_session = value
 * 	@property
 * 	def value_feedback_questions(self):             # <<<<<<<<<<<<<<
 * 		return self.value_feedback_questions
 * 	@value_feedback_questions.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24value_feedback_questions_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24value_feedback_questions_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24value_feedback_questions___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_24value_feedback_questions___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 152, 0, __PYX_ERR(0, 152, __pyx_L1_error));

  /* "feedback_manager.pyx":153
 * 	@property
 * 	def value_feedback_questions(self):
 * 		return self.value_feedback_questions             # <<<<<<<<<<<<<<
 * 	@value_feedback_questions.setter
 * 	def value_feedback_questions(self, value):
 */
  __Pyx_TraceLine(153,0,__PYX_ERR(0, 153, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __Pyx_PyInt_From_int(__pyx_v_self->value_feedback_questions); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 153, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* "feedback_manager.pyx":152
 * 		self.grade_feedback_session = value
 * 	@property
 * 	def value_feedback_questions(self):             # <<<<<<<<<<<<<<
 * 		return self.value_feedback_questions
 * 	@value_feedback_questions.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.value_feedback_questions.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":155
 * 		return self.value_feedback_questions
 * 	@value_feedback_questions.setter
 * 	def value_feedback_questions(self, value):             # <<<<<<<<<<<<<<
 * 		self.value_feedback_questions = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_24value_feedback_questions_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_24value_feedback_questions_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24value_feedback_questions_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_24value_feedback_questions_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 155, 0, __PYX_ERR(0, 155, __pyx_L1_error));

  /* "feedback_manager.pyx":156
 * 	@value_feedback_questions.setter
 * 	def value_feedback_questions(self, value):
 * 		self.value_feedback_questions = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def value_feedback_games(self):
 */
  __Pyx_TraceLine(156,0,__PYX_ERR(0, 156, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyInt_As_int(__pyx_v_value); if (unlikely((__pyx_t_1 == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 156, __pyx_L1_error)
  __pyx_v_self->value_feedback_questions = __pyx_t_1;

  /* "feedback_manager.pyx":155
 * 		return self.value_feedback_questions
 * 	@value_feedback_questions.setter
 * 	def value_feedback_questions(self, value):             # <<<<<<<<<<<<<<
 * 		self.value_feedback_questions = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.value_feedback_questions.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":158
 * 		self.value_feedback_questions = value
 * 	@property
 * 	def value_feedback_games(self):             # <<<<<<<<<<<<<<
 * 		return self.value_feedback_games
 * 	@value_feedback_games.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_20value_feedback_games_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_20value_feedback_games_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20value_feedback_games___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_20value_feedback_games___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 158, 0, __PYX_ERR(0, 158, __pyx_L1_error));

  /* "feedback_manager.pyx":159
 * 	@property
 * 	def value_feedback_games(self):
 * 		return self.value_feedback_games             # <<<<<<<<<<<<<<
 * 	@value_feedback_games.setter
 * 	def value_feedback_games(self, value):
 */
  __Pyx_TraceLine(159,0,__PYX_ERR(0, 159, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __Pyx_PyInt_From_int(__pyx_v_self->value_feedback_games); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 159, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* "feedback_manager.pyx":158
 * 		self.value_feedback_questions = value
 * 	@property
 * 	def value_feedback_games(self):             # <<<<<<<<<<<<<<
 * 		return self.value_feedback_games
 * 	@value_feedback_games.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.value_feedback_games.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":161
 * 		return self.value_feedback_games
 * 	@value_feedback_games.setter
 * 	def value_feedback_games(self, value):             # <<<<<<<<<<<<<<
 * 		self.value_feedback_games = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_20value_feedback_games_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_20value_feedback_games_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20value_feedback_games_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_20value_feedback_games_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 161, 0, __PYX_ERR(0, 161, __pyx_L1_error));

  /* "feedback_manager.pyx":162
 * 	@value_feedback_games.setter
 * 	def value_feedback_games(self, value):
 * 		self.value_feedback_games = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def value_feedback_matches(self):
 */
  __Pyx_TraceLine(162,0,__PYX_ERR(0, 162, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyInt_As_int(__pyx_v_value); if (unlikely((__pyx_t_1 == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 162, __pyx_L1_error)
  __pyx_v_self->value_feedback_games = __pyx_t_1;

  /* "feedback_manager.pyx":161
 * 		return self.value_feedback_games
 * 	@value_feedback_games.setter
 * 	def value_feedback_games(self, value):             # <<<<<<<<<<<<<<
 * 		self.value_feedback_games = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.value_feedback_games.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":164
 * 		self.value_feedback_games = value
 * 	@property
 * 	def value_feedback_matches(self):             # <<<<<<<<<<<<<<
 * 		return self.value_feedback_matches
 * 	@value_feedback_matches.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_22value_feedback_matches_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_22value_feedback_matches_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_22value_feedback_matches___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_22value_feedback_matches___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 164, 0, __PYX_ERR(0, 164, __pyx_L1_error));

  /* "feedback_manager.pyx":165
 * 	@property
 * 	def value_feedback_matches(self):
 * 		return self.value_feedback_matches             # <<<<<<<<<<<<<<
 * 	@value_feedback_matches.setter
 * 	def value_feedback_matches(self, value):
 */
  __Pyx_TraceLine(165,0,__PYX_ERR(0, 165, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __Pyx_PyInt_From_int(__pyx_v_self->value_feedback_matches); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 165, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* "feedback_manager.pyx":164
 * 		self.value_feedback_games = value
 * 	@property
 * 	def value_feedback_matches(self):             # <<<<<<<<<<<<<<
 * 		return self.value_feedback_matches
 * 	@value_feedback_matches.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.value_feedback_matches.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":167
 * 		return self.value_feedback_matches
 * 	@value_feedback_matches.setter
 * 	def value_feedback_matches(self, value):             # <<<<<<<<<<<<<<
 * 		self.value_feedback_matches = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_22value_feedback_matches_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_22value_feedback_matches_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_22value_feedback_matches_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_22value_feedback_matches_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 167, 0, __PYX_ERR(0, 167, __pyx_L1_error));

  /* "feedback_manager.pyx":168
 * 	@value_feedback_matches.setter
 * 	def value_feedback_matches(self, value):
 * 		self.value_feedback_matches = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def value_feedback_session(self):
 */
  __Pyx_TraceLine(168,0,__PYX_ERR(0, 168, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyInt_As_int(__pyx_v_value); if (unlikely((__pyx_t_1 == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 168, __pyx_L1_error)
  __pyx_v_self->value_feedback_matches = __pyx_t_1;

  /* "feedback_manager.pyx":167
 * 		return self.value_feedback_matches
 * 	@value_feedback_matches.setter
 * 	def value_feedback_matches(self, value):             # <<<<<<<<<<<<<<
 * 		self.value_feedback_matches = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.value_feedback_matches.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":170
 * 		self.value_feedback_matches = value
 * 	@property
 * 	def value_feedback_session(self):             # <<<<<<<<<<<<<<
 * 		return self.value_feedback_session
 * 	@value_feedback_session.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_22value_feedback_session_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_22value_feedback_session_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_22value_feedback_session___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_22value_feedback_session___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 170, 0, __PYX_ERR(0, 170, __pyx_L1_error));

  /* "feedback_manager.pyx":171
 * 	@property
 * 	def value_feedback_session(self):
 * 		return self.value_feedback_session             # <<<<<<<<<<<<<<
 * 	@value_feedback_session.setter
 * 	def value_feedback_session(self, value):
 */
  __Pyx_TraceLine(171,0,__PYX_ERR(0, 171, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __Pyx_PyInt_From_int(__pyx_v_self->value_feedback_session); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 171, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* "feedback_manager.pyx":170
 * 		self.value_feedback_matches = value
 * 	@property
 * 	def value_feedback_session(self):             # <<<<<<<<<<<<<<
 * 		return self.value_feedback_session
 * 	@value_feedback_session.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.value_feedback_session.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":173
 * 		return self.value_feedback_session
 * 	@value_feedback_session.setter
 * 	def value_feedback_session(self, value):             # <<<<<<<<<<<<<<
 * 		self.value_feedback_session = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_22value_feedback_session_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_22value_feedback_session_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_22value_feedback_session_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_22value_feedback_session_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 173, 0, __PYX_ERR(0, 173, __pyx_L1_error));

  /* "feedback_manager.pyx":174
 * 	@value_feedback_session.setter
 * 	def value_feedback_session(self, value):
 * 		self.value_feedback_session = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def db_conn(self):
 */
  __Pyx_TraceLine(174,0,__PYX_ERR(0, 174, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyInt_As_int(__pyx_v_value); if (unlikely((__pyx_t_1 == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 174, __pyx_L1_error)
  __pyx_v_self->value_feedback_session = __pyx_t_1;

  /* "feedback_manager.pyx":173
 * 		return self.value_feedback_session
 * 	@value_feedback_session.setter
 * 	def value_feedback_session(self, value):             # <<<<<<<<<<<<<<
 * 		self.value_feedback_session = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.value_feedback_session.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":176
 * 		self.value_feedback_session = value
 * 	@property
 * 	def db_conn(self):             # <<<<<<<<<<<<<<
 * 		return self.db_conn
 * 	@db_conn.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_7db_conn_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_7db_conn_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_7db_conn___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_7db_conn___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 176, 0, __PYX_ERR(0, 176, __pyx_L1_error));

  /* "feedback_manager.pyx":177
 * 	@property
 * 	def db_conn(self):
 * 		return self.db_conn             # <<<<<<<<<<<<<<
 * 	@db_conn.setter
 * 	def db_conn(self, value):
 */
  __Pyx_TraceLine(177,0,__PYX_ERR(0, 177, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->db_conn);
  __pyx_r = __pyx_v_self->db_conn;
  goto __pyx_L0;

  /* "feedback_manager.pyx":176
 * 		self.value_feedback_session = value
 * 	@property
 * 	def db_conn(self):             # <<<<<<<<<<<<<<
 * 		return self.db_conn
 * 	@db_conn.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.db_conn.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":179
 * 		return self.db_conn
 * 	@db_conn.setter
 * 	def db_conn(self, value):             # <<<<<<<<<<<<<<
 * 		self.db_conn = value
 * 
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_7db_conn_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_7db_conn_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_7db_conn_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_7db_conn_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 179, 0, __PYX_ERR(0, 179, __pyx_L1_error));

  /* "feedback_manager.pyx":180
 * 	@db_conn.setter
 * 	def db_conn(self, value):
 * 		self.db_conn = value             # <<<<<<<<<<<<<<
 * 
 * 	@property
 */
  __Pyx_TraceLine(180,0,__PYX_ERR(0, 180, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->db_conn);
  __Pyx_DECREF(__pyx_v_self->db_conn);
  __pyx_v_self->db_conn = __pyx_v_value;

  /* "feedback_manager.pyx":179
 * 		return self.db_conn
 * 	@db_conn.setter
 * 	def db_conn(self, value):             # <<<<<<<<<<<<<<
 * 		self.db_conn = value
 * 
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.db_conn.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":183
 * 
 * 	@property
 * 	def kid_age(self):             # <<<<<<<<<<<<<<
 * 		return self.kid_age
 * 	@kid_age.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_7kid_age_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_7kid_age_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_7kid_age___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_7kid_age___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 183, 0, __PYX_ERR(0, 183, __pyx_L1_error));

  /* "feedback_manager.pyx":184
 * 	@property
 * 	def kid_age(self):
 * 		return self.kid_age             # <<<<<<<<<<<<<<
 * 	@kid_age.setter
 * 	def kid_age(self, value):
 */
  __Pyx_TraceLine(184,0,__PYX_ERR(0, 184, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->kid_age);
  __pyx_r = __pyx_v_self->kid_age;
  goto __pyx_L0;

  /* "feedback_manager.pyx":183
 * 
 * 	@property
 * 	def kid_age(self):             # <<<<<<<<<<<<<<
 * 		return self.kid_age
 * 	@kid_age.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.kid_age.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":186
 * 		return self.kid_age
 * 	@kid_age.setter
 * 	def kid_age(self, value):             # <<<<<<<<<<<<<<
 * 		self.kid_age = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_7kid_age_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_7kid_age_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_7kid_age_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_7kid_age_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 186, 0, __PYX_ERR(0, 186, __pyx_L1_error));

  /* "feedback_manager.pyx":187
 * 	@kid_age.setter
 * 	def kid_age(self, value):
 * 		self.kid_age = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def kid_level(self):
 */
  __Pyx_TraceLine(187,0,__PYX_ERR(0, 187, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->kid_age);
  __Pyx_DECREF(__pyx_v_self->kid_age);
  __pyx_v_self->kid_age = __pyx_v_value;

  /* "feedback_manager.pyx":186
 * 		return self.kid_age
 * 	@kid_age.setter
 * 	def kid_age(self, value):             # <<<<<<<<<<<<<<
 * 		self.kid_age = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.kid_age.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":189
 * 		self.kid_age = value
 * 	@property
 * 	def kid_level(self):             # <<<<<<<<<<<<<<
 * 		return self.kid_level
 * 	@kid_level.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_9kid_level_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_9kid_level_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_9kid_level___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_9kid_level___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 189, 0, __PYX_ERR(0, 189, __pyx_L1_error));

  /* "feedback_manager.pyx":190
 * 	@property
 * 	def kid_level(self):
 * 		return self.kid_level             # <<<<<<<<<<<<<<
 * 	@kid_level.setter
 * 	def kid_level(self, value):
 */
  __Pyx_TraceLine(190,0,__PYX_ERR(0, 190, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->kid_level);
  __pyx_r = __pyx_v_self->kid_level;
  goto __pyx_L0;

  /* "feedback_manager.pyx":189
 * 		self.kid_age = value
 * 	@property
 * 	def kid_level(self):             # <<<<<<<<<<<<<<
 * 		return self.kid_level
 * 	@kid_level.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.kid_level.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":192
 * 		return self.kid_level
 * 	@kid_level.setter
 * 	def kid_level(self, value):             # <<<<<<<<<<<<<<
 * 		self.kid_level = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_9kid_level_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_9kid_level_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_9kid_level_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_9kid_level_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 192, 0, __PYX_ERR(0, 192, __pyx_L1_error));

  /* "feedback_manager.pyx":193
 * 	@kid_level.setter
 * 	def kid_level(self, value):
 * 		self.kid_level = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_question(self):
 */
  __Pyx_TraceLine(193,0,__PYX_ERR(0, 193, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->kid_level);
  __Pyx_DECREF(__pyx_v_self->kid_level);
  __pyx_v_self->kid_level = __pyx_v_value;

  /* "feedback_manager.pyx":192
 * 		return self.kid_level
 * 	@kid_level.setter
 * 	def kid_level(self, value):             # <<<<<<<<<<<<<<
 * 		self.kid_level = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.kid_level.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":195
 * 		self.kid_level = value
 * 	@property
 * 	def every_question(self):             # <<<<<<<<<<<<<<
 * 		return self.every_question
 * 	@every_question.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_14every_question_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_14every_question_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_14every_question___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_14every_question___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 195, 0, __PYX_ERR(0, 195, __pyx_L1_error));

  /* "feedback_manager.pyx":196
 * 	@property
 * 	def every_question(self):
 * 		return self.every_question             # <<<<<<<<<<<<<<
 * 	@every_question.setter
 * 	def every_question(self, value):
 */
  __Pyx_TraceLine(196,0,__PYX_ERR(0, 196, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_question);
  __pyx_r = __pyx_v_self->every_question;
  goto __pyx_L0;

  /* "feedback_manager.pyx":195
 * 		self.kid_level = value
 * 	@property
 * 	def every_question(self):             # <<<<<<<<<<<<<<
 * 		return self.every_question
 * 	@every_question.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_question.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":198
 * 		return self.every_question
 * 	@every_question.setter
 * 	def every_question(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_question = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_14every_question_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_14every_question_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_14every_question_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_14every_question_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 198, 0, __PYX_ERR(0, 198, __pyx_L1_error));

  /* "feedback_manager.pyx":199
 * 	@every_question.setter
 * 	def every_question(self, value):
 * 		self.every_question = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_num_answers(self):
 */
  __Pyx_TraceLine(199,0,__PYX_ERR(0, 199, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_question);
  __Pyx_DECREF(__pyx_v_self->every_question);
  __pyx_v_self->every_question = __pyx_v_value;

  /* "feedback_manager.pyx":198
 * 		return self.every_question
 * 	@every_question.setter
 * 	def every_question(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_question = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_question.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":201
 * 		self.every_question = value
 * 	@property
 * 	def every_num_answers(self):             # <<<<<<<<<<<<<<
 * 		return self.every_num_answers
 * 	@every_num_answers.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17every_num_answers_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17every_num_answers_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17every_num_answers___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_17every_num_answers___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 201, 0, __PYX_ERR(0, 201, __pyx_L1_error));

  /* "feedback_manager.pyx":202
 * 	@property
 * 	def every_num_answers(self):
 * 		return self.every_num_answers             # <<<<<<<<<<<<<<
 * 	@every_num_answers.setter
 * 	def every_num_answers(self, value):
 */
  __Pyx_TraceLine(202,0,__PYX_ERR(0, 202, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_num_answers);
  __pyx_r = __pyx_v_self->every_num_answers;
  goto __pyx_L0;

  /* "feedback_manager.pyx":201
 * 		self.every_question = value
 * 	@property
 * 	def every_num_answers(self):             # <<<<<<<<<<<<<<
 * 		return self.every_num_answers
 * 	@every_num_answers.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_num_answers.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":204
 * 		return self.every_num_answers
 * 	@every_num_answers.setter
 * 	def every_num_answers(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_num_answers = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_17every_num_answers_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_17every_num_answers_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17every_num_answers_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_17every_num_answers_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 204, 0, __PYX_ERR(0, 204, __pyx_L1_error));

  /* "feedback_manager.pyx":205
 * 	@every_num_answers.setter
 * 	def every_num_answers(self, value):
 * 		self.every_num_answers = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_correct_ans(self):
 */
  __Pyx_TraceLine(205,0,__PYX_ERR(0, 205, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_num_answers);
  __Pyx_DECREF(__pyx_v_self->every_num_answers);
  __pyx_v_self->every_num_answers = __pyx_v_value;

  /* "feedback_manager.pyx":204
 * 		return self.every_num_answers
 * 	@every_num_answers.setter
 * 	def every_num_answers(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_num_answers = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_num_answers.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":207
 * 		self.every_num_answers = value
 * 	@property
 * 	def every_correct_ans(self):             # <<<<<<<<<<<<<<
 * 		return self.every_correct_ans
 * 	@every_correct_ans.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17every_correct_ans_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17every_correct_ans_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17every_correct_ans___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_17every_correct_ans___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 207, 0, __PYX_ERR(0, 207, __pyx_L1_error));

  /* "feedback_manager.pyx":208
 * 	@property
 * 	def every_correct_ans(self):
 * 		return self.every_correct_ans             # <<<<<<<<<<<<<<
 * 	@every_correct_ans.setter
 * 	def every_correct_ans(self, value):
 */
  __Pyx_TraceLine(208,0,__PYX_ERR(0, 208, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_correct_ans);
  __pyx_r = __pyx_v_self->every_correct_ans;
  goto __pyx_L0;

  /* "feedback_manager.pyx":207
 * 		self.every_num_answers = value
 * 	@property
 * 	def every_correct_ans(self):             # <<<<<<<<<<<<<<
 * 		return self.every_correct_ans
 * 	@every_correct_ans.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_correct_ans.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":210
 * 		return self.every_correct_ans
 * 	@every_correct_ans.setter
 * 	def every_correct_ans(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_correct_ans = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_17every_correct_ans_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_17every_correct_ans_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17every_correct_ans_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_17every_correct_ans_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 210, 0, __PYX_ERR(0, 210, __pyx_L1_error));

  /* "feedback_manager.pyx":211
 * 	@every_correct_ans.setter
 * 	def every_correct_ans(self, value):
 * 		self.every_correct_ans = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_errors_ans(self):
 */
  __Pyx_TraceLine(211,0,__PYX_ERR(0, 211, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_correct_ans);
  __Pyx_DECREF(__pyx_v_self->every_correct_ans);
  __pyx_v_self->every_correct_ans = __pyx_v_value;

  /* "feedback_manager.pyx":210
 * 		return self.every_correct_ans
 * 	@every_correct_ans.setter
 * 	def every_correct_ans(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_correct_ans = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_correct_ans.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":213
 * 		self.every_correct_ans = value
 * 	@property
 * 	def every_errors_ans(self):             # <<<<<<<<<<<<<<
 * 		return self.every_errors_ans
 * 	@every_errors_ans.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_16every_errors_ans_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_16every_errors_ans_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_16every_errors_ans___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_16every_errors_ans___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 213, 0, __PYX_ERR(0, 213, __pyx_L1_error));

  /* "feedback_manager.pyx":214
 * 	@property
 * 	def every_errors_ans(self):
 * 		return self.every_errors_ans             # <<<<<<<<<<<<<<
 * 	@every_errors_ans.setter
 * 	def every_errors_ans(self, value):
 */
  __Pyx_TraceLine(214,0,__PYX_ERR(0, 214, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_errors_ans);
  __pyx_r = __pyx_v_self->every_errors_ans;
  goto __pyx_L0;

  /* "feedback_manager.pyx":213
 * 		self.every_correct_ans = value
 * 	@property
 * 	def every_errors_ans(self):             # <<<<<<<<<<<<<<
 * 		return self.every_errors_ans
 * 	@every_errors_ans.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_errors_ans.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":216
 * 		return self.every_errors_ans
 * 	@every_errors_ans.setter
 * 	def every_errors_ans(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_errors_ans = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_16every_errors_ans_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_16every_errors_ans_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_16every_errors_ans_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_16every_errors_ans_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 216, 0, __PYX_ERR(0, 216, __pyx_L1_error));

  /* "feedback_manager.pyx":217
 * 	@every_errors_ans.setter
 * 	def every_errors_ans(self, value):
 * 		self.every_errors_ans = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_indecisions(self):
 */
  __Pyx_TraceLine(217,0,__PYX_ERR(0, 217, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_errors_ans);
  __Pyx_DECREF(__pyx_v_self->every_errors_ans);
  __pyx_v_self->every_errors_ans = __pyx_v_value;

  /* "feedback_manager.pyx":216
 * 		return self.every_errors_ans
 * 	@every_errors_ans.setter
 * 	def every_errors_ans(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_errors_ans = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_errors_ans.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":219
 * 		self.every_errors_ans = value
 * 	@property
 * 	def every_indecisions(self):             # <<<<<<<<<<<<<<
 * 		return self.every_indecisions
 * 	@every_indecisions.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17every_indecisions_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17every_indecisions_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17every_indecisions___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_17every_indecisions___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 219, 0, __PYX_ERR(0, 219, __pyx_L1_error));

  /* "feedback_manager.pyx":220
 * 	@property
 * 	def every_indecisions(self):
 * 		return self.every_indecisions             # <<<<<<<<<<<<<<
 * 	@every_indecisions.setter
 * 	def every_indecisions(self, value):
 */
  __Pyx_TraceLine(220,0,__PYX_ERR(0, 220, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_indecisions);
  __pyx_r = __pyx_v_self->every_indecisions;
  goto __pyx_L0;

  /* "feedback_manager.pyx":219
 * 		self.every_errors_ans = value
 * 	@property
 * 	def every_indecisions(self):             # <<<<<<<<<<<<<<
 * 		return self.every_indecisions
 * 	@every_indecisions.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_indecisions.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":222
 * 		return self.every_indecisions
 * 	@every_indecisions.setter
 * 	def every_indecisions(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_indecisions = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_17every_indecisions_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_17every_indecisions_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17every_indecisions_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_17every_indecisions_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 222, 0, __PYX_ERR(0, 222, __pyx_L1_error));

  /* "feedback_manager.pyx":223
 * 	@every_indecisions.setter
 * 	def every_indecisions(self, value):
 * 		self.every_indecisions = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_question_times(self):
 */
  __Pyx_TraceLine(223,0,__PYX_ERR(0, 223, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_indecisions);
  __Pyx_DECREF(__pyx_v_self->every_indecisions);
  __pyx_v_self->every_indecisions = __pyx_v_value;

  /* "feedback_manager.pyx":222
 * 		return self.every_indecisions
 * 	@every_indecisions.setter
 * 	def every_indecisions(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_indecisions = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_indecisions.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":225
 * 		self.every_indecisions = value
 * 	@property
 * 	def every_question_times(self):             # <<<<<<<<<<<<<<
 * 		return self.every_question_times
 * 	@every_question_times.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_20every_question_times_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_20every_question_times_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20every_question_times___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_20every_question_times___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 225, 0, __PYX_ERR(0, 225, __pyx_L1_error));

  /* "feedback_manager.pyx":226
 * 	@property
 * 	def every_question_times(self):
 * 		return self.every_question_times             # <<<<<<<<<<<<<<
 * 	@every_question_times.setter
 * 	def every_question_times(self, value):
 */
  __Pyx_TraceLine(226,0,__PYX_ERR(0, 226, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_question_times);
  __pyx_r = __pyx_v_self->every_question_times;
  goto __pyx_L0;

  /* "feedback_manager.pyx":225
 * 		self.every_indecisions = value
 * 	@property
 * 	def every_question_times(self):             # <<<<<<<<<<<<<<
 * 		return self.every_question_times
 * 	@every_question_times.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_question_times.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":228
 * 		return self.every_question_times
 * 	@every_question_times.setter
 * 	def every_question_times(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_question_times = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_20every_question_times_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_20every_question_times_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20every_question_times_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_20every_question_times_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 228, 0, __PYX_ERR(0, 228, __pyx_L1_error));

  /* "feedback_manager.pyx":229
 * 	@every_question_times.setter
 * 	def every_question_times(self, value):
 * 		self.every_question_times = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def all_values_questions(self):
 */
  __Pyx_TraceLine(229,0,__PYX_ERR(0, 229, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_question_times);
  __Pyx_DECREF(__pyx_v_self->every_question_times);
  __pyx_v_self->every_question_times = __pyx_v_value;

  /* "feedback_manager.pyx":228
 * 		return self.every_question_times
 * 	@every_question_times.setter
 * 	def every_question_times(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_question_times = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_question_times.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":231
 * 		self.every_question_times = value
 * 	@property
 * 	def all_values_questions(self):             # <<<<<<<<<<<<<<
 * 		return self.all_values_questions
 * 	@all_values_questions.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_20all_values_questions_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_20all_values_questions_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20all_values_questions___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_20all_values_questions___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 231, 0, __PYX_ERR(0, 231, __pyx_L1_error));

  /* "feedback_manager.pyx":232
 * 	@property
 * 	def all_values_questions(self):
 * 		return self.all_values_questions             # <<<<<<<<<<<<<<
 * 	@all_values_questions.setter
 * 	def all_values_questions(self, value):
 */
  __Pyx_TraceLine(232,0,__PYX_ERR(0, 232, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->all_values_questions);
  __pyx_r = __pyx_v_self->all_values_questions;
  goto __pyx_L0;

  /* "feedback_manager.pyx":231
 * 		self.every_question_times = value
 * 	@property
 * 	def all_values_questions(self):             # <<<<<<<<<<<<<<
 * 		return self.all_values_questions
 * 	@all_values_questions.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.all_values_questions.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":234
 * 		return self.all_values_questions
 * 	@all_values_questions.setter
 * 	def all_values_questions(self, value):             # <<<<<<<<<<<<<<
 * 		self.all_values_questions = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_20all_values_questions_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_20all_values_questions_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20all_values_questions_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_20all_values_questions_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 234, 0, __PYX_ERR(0, 234, __pyx_L1_error));

  /* "feedback_manager.pyx":235
 * 	@all_values_questions.setter
 * 	def all_values_questions(self, value):
 * 		self.all_values_questions = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_game_times(self):
 */
  __Pyx_TraceLine(235,0,__PYX_ERR(0, 235, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->all_values_questions);
  __Pyx_DECREF(__pyx_v_self->all_values_questions);
  __pyx_v_self->all_values_questions = __pyx_v_value;

  /* "feedback_manager.pyx":234
 * 		return self.all_values_questions
 * 	@all_values_questions.setter
 * 	def all_values_questions(self, value):             # <<<<<<<<<<<<<<
 * 		self.all_values_questions = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.all_values_questions.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":237
 * 		self.all_values_questions = value
 * 	@property
 * 	def every_game_times(self):             # <<<<<<<<<<<<<<
 * 		return self.every_game_times
 * 	@every_game_times.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_16every_game_times_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_16every_game_times_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_16every_game_times___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_16every_game_times___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 237, 0, __PYX_ERR(0, 237, __pyx_L1_error));

  /* "feedback_manager.pyx":238
 * 	@property
 * 	def every_game_times(self):
 * 		return self.every_game_times             # <<<<<<<<<<<<<<
 * 	@every_game_times.setter
 * 	def every_game_times(self, value):
 */
  __Pyx_TraceLine(238,0,__PYX_ERR(0, 238, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_game_times);
  __pyx_r = __pyx_v_self->every_game_times;
  goto __pyx_L0;

  /* "feedback_manager.pyx":237
 * 		self.all_values_questions = value
 * 	@property
 * 	def every_game_times(self):             # <<<<<<<<<<<<<<
 * 		return self.every_game_times
 * 	@every_game_times.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_game_times.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":240
 * 		return self.every_game_times
 * 	@every_game_times.setter
 * 	def every_game_times(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_game_times = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_16every_game_times_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_16every_game_times_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_16every_game_times_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_16every_game_times_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 240, 0, __PYX_ERR(0, 240, __pyx_L1_error));

  /* "feedback_manager.pyx":241
 * 	@every_game_times.setter
 * 	def every_game_times(self, value):
 * 		self.every_game_times = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_difficulties_game(self):
 */
  __Pyx_TraceLine(241,0,__PYX_ERR(0, 241, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_game_times);
  __Pyx_DECREF(__pyx_v_self->every_game_times);
  __pyx_v_self->every_game_times = __pyx_v_value;

  /* "feedback_manager.pyx":240
 * 		return self.every_game_times
 * 	@every_game_times.setter
 * 	def every_game_times(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_game_times = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_game_times.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":243
 * 		self.every_game_times = value
 * 	@property
 * 	def every_difficulties_game(self):             # <<<<<<<<<<<<<<
 * 		return self.every_difficulties_game
 * 	@every_difficulties_game.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_23every_difficulties_game_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_23every_difficulties_game_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_23every_difficulties_game___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_23every_difficulties_game___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 243, 0, __PYX_ERR(0, 243, __pyx_L1_error));

  /* "feedback_manager.pyx":244
 * 	@property
 * 	def every_difficulties_game(self):
 * 		return self.every_difficulties_game             # <<<<<<<<<<<<<<
 * 	@every_difficulties_game.setter
 * 	def every_difficulties_game(self, value):
 */
  __Pyx_TraceLine(244,0,__PYX_ERR(0, 244, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_difficulties_game);
  __pyx_r = __pyx_v_self->every_difficulties_game;
  goto __pyx_L0;

  /* "feedback_manager.pyx":243
 * 		self.every_game_times = value
 * 	@property
 * 	def every_difficulties_game(self):             # <<<<<<<<<<<<<<
 * 		return self.every_difficulties_game
 * 	@every_difficulties_game.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_difficulties_game.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":246
 * 		return self.every_difficulties_game
 * 	@every_difficulties_game.setter
 * 	def every_difficulties_game(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_difficulties_game = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_23every_difficulties_game_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_23every_difficulties_game_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_23every_difficulties_game_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_23every_difficulties_game_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 246, 0, __PYX_ERR(0, 246, __pyx_L1_error));

  /* "feedback_manager.pyx":247
 * 	@every_difficulties_game.setter
 * 	def every_difficulties_game(self, value):
 * 		self.every_difficulties_game = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_quan_feedback_ques(self):
 */
  __Pyx_TraceLine(247,0,__PYX_ERR(0, 247, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_difficulties_game);
  __Pyx_DECREF(__pyx_v_self->every_difficulties_game);
  __pyx_v_self->every_difficulties_game = __pyx_v_value;

  /* "feedback_manager.pyx":246
 * 		return self.every_difficulties_game
 * 	@every_difficulties_game.setter
 * 	def every_difficulties_game(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_difficulties_game = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_difficulties_game.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":249
 * 		self.every_difficulties_game = value
 * 	@property
 * 	def every_quan_feedback_ques(self):             # <<<<<<<<<<<<<<
 * 		return self.every_quan_feedback_ques
 * 	@every_quan_feedback_ques.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_ques_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_ques_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_ques___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_ques___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 249, 0, __PYX_ERR(0, 249, __pyx_L1_error));

  /* "feedback_manager.pyx":250
 * 	@property
 * 	def every_quan_feedback_ques(self):
 * 		return self.every_quan_feedback_ques             # <<<<<<<<<<<<<<
 * 	@every_quan_feedback_ques.setter
 * 	def every_quan_feedback_ques(self, value):
 */
  __Pyx_TraceLine(250,0,__PYX_ERR(0, 250, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_quan_feedback_ques);
  __pyx_r = __pyx_v_self->every_quan_feedback_ques;
  goto __pyx_L0;

  /* "feedback_manager.pyx":249
 * 		self.every_difficulties_game = value
 * 	@property
 * 	def every_quan_feedback_ques(self):             # <<<<<<<<<<<<<<
 * 		return self.every_quan_feedback_ques
 * 	@every_quan_feedback_ques.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_quan_feedback_ques.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":252
 * 		return self.every_quan_feedback_ques
 * 	@every_quan_feedback_ques.setter
 * 	def every_quan_feedback_ques(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_quan_feedback_ques = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_ques_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_ques_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_ques_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_ques_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 252, 0, __PYX_ERR(0, 252, __pyx_L1_error));

  /* "feedback_manager.pyx":253
 * 	@every_quan_feedback_ques.setter
 * 	def every_quan_feedback_ques(self, value):
 * 		self.every_quan_feedback_ques = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_grade_feedback_ques(self):
 */
  __Pyx_TraceLine(253,0,__PYX_ERR(0, 253, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_quan_feedback_ques);
  __Pyx_DECREF(__pyx_v_self->every_quan_feedback_ques);
  __pyx_v_self->every_quan_feedback_ques = __pyx_v_value;

  /* "feedback_manager.pyx":252
 * 		return self.every_quan_feedback_ques
 * 	@every_quan_feedback_ques.setter
 * 	def every_quan_feedback_ques(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_quan_feedback_ques = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_quan_feedback_ques.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":255
 * 		self.every_quan_feedback_ques = value
 * 	@property
 * 	def every_grade_feedback_ques(self):             # <<<<<<<<<<<<<<
 * 		return self.every_grade_feedback_ques
 * 	@every_grade_feedback_ques.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_ques_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_ques_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_ques___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_ques___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 255, 0, __PYX_ERR(0, 255, __pyx_L1_error));

  /* "feedback_manager.pyx":256
 * 	@property
 * 	def every_grade_feedback_ques(self):
 * 		return self.every_grade_feedback_ques             # <<<<<<<<<<<<<<
 * 	@every_grade_feedback_ques.setter
 * 	def every_grade_feedback_ques(self, value):
 */
  __Pyx_TraceLine(256,0,__PYX_ERR(0, 256, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_grade_feedback_ques);
  __pyx_r = __pyx_v_self->every_grade_feedback_ques;
  goto __pyx_L0;

  /* "feedback_manager.pyx":255
 * 		self.every_quan_feedback_ques = value
 * 	@property
 * 	def every_grade_feedback_ques(self):             # <<<<<<<<<<<<<<
 * 		return self.every_grade_feedback_ques
 * 	@every_grade_feedback_ques.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_grade_feedback_ques.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":258
 * 		return self.every_grade_feedback_ques
 * 	@every_grade_feedback_ques.setter
 * 	def every_grade_feedback_ques(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_grade_feedback_ques = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_ques_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_ques_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_ques_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_ques_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 258, 0, __PYX_ERR(0, 258, __pyx_L1_error));

  /* "feedback_manager.pyx":259
 * 	@every_grade_feedback_ques.setter
 * 	def every_grade_feedback_ques(self, value):
 * 		self.every_grade_feedback_ques = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_val_feedback_ques(self):
 */
  __Pyx_TraceLine(259,0,__PYX_ERR(0, 259, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_grade_feedback_ques);
  __Pyx_DECREF(__pyx_v_self->every_grade_feedback_ques);
  __pyx_v_self->every_grade_feedback_ques = __pyx_v_value;

  /* "feedback_manager.pyx":258
 * 		return self.every_grade_feedback_ques
 * 	@every_grade_feedback_ques.setter
 * 	def every_grade_feedback_ques(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_grade_feedback_ques = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_grade_feedback_ques.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":261
 * 		self.every_grade_feedback_ques = value
 * 	@property
 * 	def every_val_feedback_ques(self):             # <<<<<<<<<<<<<<
 * 		return self.every_val_feedback_ques
 * 	@every_val_feedback_ques.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_ques_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_ques_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_ques___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_ques___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 261, 0, __PYX_ERR(0, 261, __pyx_L1_error));

  /* "feedback_manager.pyx":262
 * 	@property
 * 	def every_val_feedback_ques(self):
 * 		return self.every_val_feedback_ques             # <<<<<<<<<<<<<<
 * 	@every_val_feedback_ques.setter
 * 	def every_val_feedback_ques(self, value):
 */
  __Pyx_TraceLine(262,0,__PYX_ERR(0, 262, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_val_feedback_ques);
  __pyx_r = __pyx_v_self->every_val_feedback_ques;
  goto __pyx_L0;

  /* "feedback_manager.pyx":261
 * 		self.every_grade_feedback_ques = value
 * 	@property
 * 	def every_val_feedback_ques(self):             # <<<<<<<<<<<<<<
 * 		return self.every_val_feedback_ques
 * 	@every_val_feedback_ques.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_val_feedback_ques.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":264
 * 		return self.every_val_feedback_ques
 * 	@every_val_feedback_ques.setter
 * 	def every_val_feedback_ques(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_val_feedback_ques = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_ques_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_ques_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_ques_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_ques_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 264, 0, __PYX_ERR(0, 264, __pyx_L1_error));

  /* "feedback_manager.pyx":265
 * 	@every_val_feedback_ques.setter
 * 	def every_val_feedback_ques(self, value):
 * 		self.every_val_feedback_ques = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_match_times(self):
 */
  __Pyx_TraceLine(265,0,__PYX_ERR(0, 265, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_val_feedback_ques);
  __Pyx_DECREF(__pyx_v_self->every_val_feedback_ques);
  __pyx_v_self->every_val_feedback_ques = __pyx_v_value;

  /* "feedback_manager.pyx":264
 * 		return self.every_val_feedback_ques
 * 	@every_val_feedback_ques.setter
 * 	def every_val_feedback_ques(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_val_feedback_ques = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_val_feedback_ques.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":267
 * 		self.every_val_feedback_ques = value
 * 	@property
 * 	def every_match_times(self):             # <<<<<<<<<<<<<<
 * 		return self.every_match_times
 * 	@every_match_times.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17every_match_times_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17every_match_times_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17every_match_times___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_17every_match_times___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 267, 0, __PYX_ERR(0, 267, __pyx_L1_error));

  /* "feedback_manager.pyx":268
 * 	@property
 * 	def every_match_times(self):
 * 		return self.every_match_times             # <<<<<<<<<<<<<<
 * 	@every_match_times.setter
 * 	def every_match_times(self, value):
 */
  __Pyx_TraceLine(268,0,__PYX_ERR(0, 268, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_match_times);
  __pyx_r = __pyx_v_self->every_match_times;
  goto __pyx_L0;

  /* "feedback_manager.pyx":267
 * 		self.every_val_feedback_ques = value
 * 	@property
 * 	def every_match_times(self):             # <<<<<<<<<<<<<<
 * 		return self.every_match_times
 * 	@every_match_times.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_match_times.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":270
 * 		return self.every_match_times
 * 	@every_match_times.setter
 * 	def every_match_times(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_match_times = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_17every_match_times_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_17every_match_times_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17every_match_times_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_17every_match_times_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 270, 0, __PYX_ERR(0, 270, __pyx_L1_error));

  /* "feedback_manager.pyx":271
 * 	@every_match_times.setter
 * 	def every_match_times(self, value):
 * 		self.every_match_times = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_difficulty_matches(self):
 */
  __Pyx_TraceLine(271,0,__PYX_ERR(0, 271, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_match_times);
  __Pyx_DECREF(__pyx_v_self->every_match_times);
  __pyx_v_self->every_match_times = __pyx_v_value;

  /* "feedback_manager.pyx":270
 * 		return self.every_match_times
 * 	@every_match_times.setter
 * 	def every_match_times(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_match_times = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_match_times.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":273
 * 		self.every_match_times = value
 * 	@property
 * 	def every_difficulty_matches(self):             # <<<<<<<<<<<<<<
 * 		return self.every_difficulty_matches
 * 	@every_difficulty_matches.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24every_difficulty_matches_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24every_difficulty_matches_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24every_difficulty_matches___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_24every_difficulty_matches___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 273, 0, __PYX_ERR(0, 273, __pyx_L1_error));

  /* "feedback_manager.pyx":274
 * 	@property
 * 	def every_difficulty_matches(self):
 * 		return self.every_difficulty_matches             # <<<<<<<<<<<<<<
 * 	@every_difficulty_matches.setter
 * 	def every_difficulty_matches(self, value):
 */
  __Pyx_TraceLine(274,0,__PYX_ERR(0, 274, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_difficulty_matches);
  __pyx_r = __pyx_v_self->every_difficulty_matches;
  goto __pyx_L0;

  /* "feedback_manager.pyx":273
 * 		self.every_match_times = value
 * 	@property
 * 	def every_difficulty_matches(self):             # <<<<<<<<<<<<<<
 * 		return self.every_difficulty_matches
 * 	@every_difficulty_matches.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_difficulty_matches.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":276
 * 		return self.every_difficulty_matches
 * 	@every_difficulty_matches.setter
 * 	def every_difficulty_matches(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_difficulty_matches = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_24every_difficulty_matches_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_24every_difficulty_matches_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24every_difficulty_matches_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_24every_difficulty_matches_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 276, 0, __PYX_ERR(0, 276, __pyx_L1_error));

  /* "feedback_manager.pyx":277
 * 	@every_difficulty_matches.setter
 * 	def every_difficulty_matches(self, value):
 * 		self.every_difficulty_matches = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_quan_feedback_game(self):
 */
  __Pyx_TraceLine(277,0,__PYX_ERR(0, 277, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_difficulty_matches);
  __Pyx_DECREF(__pyx_v_self->every_difficulty_matches);
  __pyx_v_self->every_difficulty_matches = __pyx_v_value;

  /* "feedback_manager.pyx":276
 * 		return self.every_difficulty_matches
 * 	@every_difficulty_matches.setter
 * 	def every_difficulty_matches(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_difficulty_matches = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_difficulty_matches.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":279
 * 		self.every_difficulty_matches = value
 * 	@property
 * 	def every_quan_feedback_game(self):             # <<<<<<<<<<<<<<
 * 		return self.every_quan_feedback_game
 * 	@every_quan_feedback_game.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_game_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_game_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_game___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_game___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 279, 0, __PYX_ERR(0, 279, __pyx_L1_error));

  /* "feedback_manager.pyx":280
 * 	@property
 * 	def every_quan_feedback_game(self):
 * 		return self.every_quan_feedback_game             # <<<<<<<<<<<<<<
 * 	@every_quan_feedback_game.setter
 * 	def every_quan_feedback_game(self, value):
 */
  __Pyx_TraceLine(280,0,__PYX_ERR(0, 280, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_quan_feedback_game);
  __pyx_r = __pyx_v_self->every_quan_feedback_game;
  goto __pyx_L0;

  /* "feedback_manager.pyx":279
 * 		self.every_difficulty_matches = value
 * 	@property
 * 	def every_quan_feedback_game(self):             # <<<<<<<<<<<<<<
 * 		return self.every_quan_feedback_game
 * 	@every_quan_feedback_game.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_quan_feedback_game.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":282
 * 		return self.every_quan_feedback_game
 * 	@every_quan_feedback_game.setter
 * 	def every_quan_feedback_game(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_quan_feedback_game = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_game_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_game_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_game_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_24every_quan_feedback_game_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 282, 0, __PYX_ERR(0, 282, __pyx_L1_error));

  /* "feedback_manager.pyx":283
 * 	@every_quan_feedback_game.setter
 * 	def every_quan_feedback_game(self, value):
 * 		self.every_quan_feedback_game = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_grade_feedback_game(self):
 */
  __Pyx_TraceLine(283,0,__PYX_ERR(0, 283, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_quan_feedback_game);
  __Pyx_DECREF(__pyx_v_self->every_quan_feedback_game);
  __pyx_v_self->every_quan_feedback_game = __pyx_v_value;

  /* "feedback_manager.pyx":282
 * 		return self.every_quan_feedback_game
 * 	@every_quan_feedback_game.setter
 * 	def every_quan_feedback_game(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_quan_feedback_game = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_quan_feedback_game.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":285
 * 		self.every_quan_feedback_game = value
 * 	@property
 * 	def every_grade_feedback_game(self):             # <<<<<<<<<<<<<<
 * 		return self.every_grade_feedback_game
 * 	@every_grade_feedback_game.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_game_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_game_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_game___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_game___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 285, 0, __PYX_ERR(0, 285, __pyx_L1_error));

  /* "feedback_manager.pyx":286
 * 	@property
 * 	def every_grade_feedback_game(self):
 * 		return self.every_grade_feedback_game             # <<<<<<<<<<<<<<
 * 	@every_grade_feedback_game.setter
 * 	def every_grade_feedback_game(self, value):
 */
  __Pyx_TraceLine(286,0,__PYX_ERR(0, 286, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_grade_feedback_game);
  __pyx_r = __pyx_v_self->every_grade_feedback_game;
  goto __pyx_L0;

  /* "feedback_manager.pyx":285
 * 		self.every_quan_feedback_game = value
 * 	@property
 * 	def every_grade_feedback_game(self):             # <<<<<<<<<<<<<<
 * 		return self.every_grade_feedback_game
 * 	@every_grade_feedback_game.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_grade_feedback_game.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":288
 * 		return self.every_grade_feedback_game
 * 	@every_grade_feedback_game.setter
 * 	def every_grade_feedback_game(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_grade_feedback_game = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_game_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_game_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_game_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_25every_grade_feedback_game_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 288, 0, __PYX_ERR(0, 288, __pyx_L1_error));

  /* "feedback_manager.pyx":289
 * 	@every_grade_feedback_game.setter
 * 	def every_grade_feedback_game(self, value):
 * 		self.every_grade_feedback_game = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_val_feedback_game(self):
 */
  __Pyx_TraceLine(289,0,__PYX_ERR(0, 289, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_grade_feedback_game);
  __Pyx_DECREF(__pyx_v_self->every_grade_feedback_game);
  __pyx_v_self->every_grade_feedback_game = __pyx_v_value;

  /* "feedback_manager.pyx":288
 * 		return self.every_grade_feedback_game
 * 	@every_grade_feedback_game.setter
 * 	def every_grade_feedback_game(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_grade_feedback_game = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_grade_feedback_game.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":291
 * 		self.every_grade_feedback_game = value
 * 	@property
 * 	def every_val_feedback_game(self):             # <<<<<<<<<<<<<<
 * 		return self.every_val_feedback_game
 * 	@every_val_feedback_game.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_game_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_game_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_game___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_game___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 291, 0, __PYX_ERR(0, 291, __pyx_L1_error));

  /* "feedback_manager.pyx":292
 * 	@property
 * 	def every_val_feedback_game(self):
 * 		return self.every_val_feedback_game             # <<<<<<<<<<<<<<
 * 	@every_val_feedback_game.setter
 * 	def every_val_feedback_game(self, value):
 */
  __Pyx_TraceLine(292,0,__PYX_ERR(0, 292, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_val_feedback_game);
  __pyx_r = __pyx_v_self->every_val_feedback_game;
  goto __pyx_L0;

  /* "feedback_manager.pyx":291
 * 		self.every_grade_feedback_game = value
 * 	@property
 * 	def every_val_feedback_game(self):             # <<<<<<<<<<<<<<
 * 		return self.every_val_feedback_game
 * 	@every_val_feedback_game.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_val_feedback_game.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":294
 * 		return self.every_val_feedback_game
 * 	@every_val_feedback_game.setter
 * 	def every_val_feedback_game(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_val_feedback_game = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_game_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_game_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_game_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_23every_val_feedback_game_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 294, 0, __PYX_ERR(0, 294, __pyx_L1_error));

  /* "feedback_manager.pyx":295
 * 	@every_val_feedback_game.setter
 * 	def every_val_feedback_game(self, value):
 * 		self.every_val_feedback_game = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def complex_ses(self):
 */
  __Pyx_TraceLine(295,0,__PYX_ERR(0, 295, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_val_feedback_game);
  __Pyx_DECREF(__pyx_v_self->every_val_feedback_game);
  __pyx_v_self->every_val_feedback_game = __pyx_v_value;

  /* "feedback_manager.pyx":294
 * 		return self.every_val_feedback_game
 * 	@every_val_feedback_game.setter
 * 	def every_val_feedback_game(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_val_feedback_game = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_val_feedback_game.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":297
 * 		self.every_val_feedback_game = value
 * 	@property
 * 	def complex_ses(self):             # <<<<<<<<<<<<<<
 * 		return self.complex_ses
 * 	@complex_ses.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_11complex_ses_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_11complex_ses_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_11complex_ses___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_11complex_ses___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 297, 0, __PYX_ERR(0, 297, __pyx_L1_error));

  /* "feedback_manager.pyx":298
 * 	@property
 * 	def complex_ses(self):
 * 		return self.complex_ses             # <<<<<<<<<<<<<<
 * 	@complex_ses.setter
 * 	def complex_ses(self, value):
 */
  __Pyx_TraceLine(298,0,__PYX_ERR(0, 298, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->complex_ses);
  __pyx_r = __pyx_v_self->complex_ses;
  goto __pyx_L0;

  /* "feedback_manager.pyx":297
 * 		self.every_val_feedback_game = value
 * 	@property
 * 	def complex_ses(self):             # <<<<<<<<<<<<<<
 * 		return self.complex_ses
 * 	@complex_ses.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.complex_ses.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":300
 * 		return self.complex_ses
 * 	@complex_ses.setter
 * 	def complex_ses(self, value):             # <<<<<<<<<<<<<<
 * 		self.complex_ses = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_11complex_ses_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_11complex_ses_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_11complex_ses_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_11complex_ses_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 300, 0, __PYX_ERR(0, 300, __pyx_L1_error));

  /* "feedback_manager.pyx":301
 * 	@complex_ses.setter
 * 	def complex_ses(self, value):
 * 		self.complex_ses = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def time_ses(self):
 */
  __Pyx_TraceLine(301,0,__PYX_ERR(0, 301, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->complex_ses);
  __Pyx_DECREF(__pyx_v_self->complex_ses);
  __pyx_v_self->complex_ses = __pyx_v_value;

  /* "feedback_manager.pyx":300
 * 		return self.complex_ses
 * 	@complex_ses.setter
 * 	def complex_ses(self, value):             # <<<<<<<<<<<<<<
 * 		self.complex_ses = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.complex_ses.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":303
 * 		self.complex_ses = value
 * 	@property
 * 	def time_ses(self):             # <<<<<<<<<<<<<<
 * 		return self.time_ses
 * 	@time_ses.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_8time_ses_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_8time_ses_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_8time_ses___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_8time_ses___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 303, 0, __PYX_ERR(0, 303, __pyx_L1_error));

  /* "feedback_manager.pyx":304
 * 	@property
 * 	def time_ses(self):
 * 		return self.time_ses             # <<<<<<<<<<<<<<
 * 	@time_ses.setter
 * 	def time_ses(self, value):
 */
  __Pyx_TraceLine(304,0,__PYX_ERR(0, 304, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->time_ses);
  __pyx_r = __pyx_v_self->time_ses;
  goto __pyx_L0;

  /* "feedback_manager.pyx":303
 * 		self.complex_ses = value
 * 	@property
 * 	def time_ses(self):             # <<<<<<<<<<<<<<
 * 		return self.time_ses
 * 	@time_ses.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.time_ses.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":306
 * 		return self.time_ses
 * 	@time_ses.setter
 * 	def time_ses(self, value):             # <<<<<<<<<<<<<<
 * 		self.time_ses = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_8time_ses_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_8time_ses_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_8time_ses_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_8time_ses_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 306, 0, __PYX_ERR(0, 306, __pyx_L1_error));

  /* "feedback_manager.pyx":307
 * 	@time_ses.setter
 * 	def time_ses(self, value):
 * 		self.time_ses = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def time_percentage_ses(self):
 */
  __Pyx_TraceLine(307,0,__PYX_ERR(0, 307, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->time_ses);
  __Pyx_DECREF(__pyx_v_self->time_ses);
  __pyx_v_self->time_ses = __pyx_v_value;

  /* "feedback_manager.pyx":306
 * 		return self.time_ses
 * 	@time_ses.setter
 * 	def time_ses(self, value):             # <<<<<<<<<<<<<<
 * 		self.time_ses = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.time_ses.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":309
 * 		self.time_ses = value
 * 	@property
 * 	def time_percentage_ses(self):             # <<<<<<<<<<<<<<
 * 		return self.time_percentage_ses
 * 	@time_percentage_ses.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_19time_percentage_ses_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_19time_percentage_ses_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_19time_percentage_ses___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_19time_percentage_ses___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 309, 0, __PYX_ERR(0, 309, __pyx_L1_error));

  /* "feedback_manager.pyx":310
 * 	@property
 * 	def time_percentage_ses(self):
 * 		return self.time_percentage_ses             # <<<<<<<<<<<<<<
 * 	@time_percentage_ses.setter
 * 	def time_percentage_ses(self, value):
 */
  __Pyx_TraceLine(310,0,__PYX_ERR(0, 310, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->time_percentage_ses);
  __pyx_r = __pyx_v_self->time_percentage_ses;
  goto __pyx_L0;

  /* "feedback_manager.pyx":309
 * 		self.time_ses = value
 * 	@property
 * 	def time_percentage_ses(self):             # <<<<<<<<<<<<<<
 * 		return self.time_percentage_ses
 * 	@time_percentage_ses.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.time_percentage_ses.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":312
 * 		return self.time_percentage_ses
 * 	@time_percentage_ses.setter
 * 	def time_percentage_ses(self, value):             # <<<<<<<<<<<<<<
 * 		self.time_percentage_ses = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_19time_percentage_ses_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_19time_percentage_ses_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_19time_percentage_ses_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_19time_percentage_ses_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 312, 0, __PYX_ERR(0, 312, __pyx_L1_error));

  /* "feedback_manager.pyx":313
 * 	@time_percentage_ses.setter
 * 	def time_percentage_ses(self, value):
 * 		self.time_percentage_ses = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def expected_time_ses(self):
 */
  __Pyx_TraceLine(313,0,__PYX_ERR(0, 313, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->time_percentage_ses);
  __Pyx_DECREF(__pyx_v_self->time_percentage_ses);
  __pyx_v_self->time_percentage_ses = __pyx_v_value;

  /* "feedback_manager.pyx":312
 * 		return self.time_percentage_ses
 * 	@time_percentage_ses.setter
 * 	def time_percentage_ses(self, value):             # <<<<<<<<<<<<<<
 * 		self.time_percentage_ses = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.time_percentage_ses.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":315
 * 		self.time_percentage_ses = value
 * 	@property
 * 	def expected_time_ses(self):             # <<<<<<<<<<<<<<
 * 		return self.expected_time_ses
 * 	@expected_time_ses.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17expected_time_ses_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17expected_time_ses_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17expected_time_ses___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_17expected_time_ses___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 315, 0, __PYX_ERR(0, 315, __pyx_L1_error));

  /* "feedback_manager.pyx":316
 * 	@property
 * 	def expected_time_ses(self):
 * 		return self.expected_time_ses             # <<<<<<<<<<<<<<
 * 	@expected_time_ses.setter
 * 	def expected_time_ses(self, value):
 */
  __Pyx_TraceLine(316,0,__PYX_ERR(0, 316, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->expected_time_ses);
  __pyx_r = __pyx_v_self->expected_time_ses;
  goto __pyx_L0;

  /* "feedback_manager.pyx":315
 * 		self.time_percentage_ses = value
 * 	@property
 * 	def expected_time_ses(self):             # <<<<<<<<<<<<<<
 * 		return self.expected_time_ses
 * 	@expected_time_ses.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.expected_time_ses.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":318
 * 		return self.expected_time_ses
 * 	@expected_time_ses.setter
 * 	def expected_time_ses(self, value):             # <<<<<<<<<<<<<<
 * 		self.expected_time_ses = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_17expected_time_ses_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_17expected_time_ses_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17expected_time_ses_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_17expected_time_ses_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 318, 0, __PYX_ERR(0, 318, __pyx_L1_error));

  /* "feedback_manager.pyx":319
 * 	@expected_time_ses.setter
 * 	def expected_time_ses(self, value):
 * 		self.expected_time_ses = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_quan_feedback_match(self):
 */
  __Pyx_TraceLine(319,0,__PYX_ERR(0, 319, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->expected_time_ses);
  __Pyx_DECREF(__pyx_v_self->expected_time_ses);
  __pyx_v_self->expected_time_ses = __pyx_v_value;

  /* "feedback_manager.pyx":318
 * 		return self.expected_time_ses
 * 	@expected_time_ses.setter
 * 	def expected_time_ses(self, value):             # <<<<<<<<<<<<<<
 * 		self.expected_time_ses = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.expected_time_ses.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":321
 * 		self.expected_time_ses = value
 * 	@property
 * 	def every_quan_feedback_match(self):             # <<<<<<<<<<<<<<
 * 		return self.every_quan_feedback_match
 * 	@every_quan_feedback_match.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25every_quan_feedback_match_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25every_quan_feedback_match_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25every_quan_feedback_match___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_25every_quan_feedback_match___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 321, 0, __PYX_ERR(0, 321, __pyx_L1_error));

  /* "feedback_manager.pyx":322
 * 	@property
 * 	def every_quan_feedback_match(self):
 * 		return self.every_quan_feedback_match             # <<<<<<<<<<<<<<
 * 	@every_quan_feedback_match.setter
 * 	def every_quan_feedback_match(self, value):
 */
  __Pyx_TraceLine(322,0,__PYX_ERR(0, 322, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_quan_feedback_match);
  __pyx_r = __pyx_v_self->every_quan_feedback_match;
  goto __pyx_L0;

  /* "feedback_manager.pyx":321
 * 		self.expected_time_ses = value
 * 	@property
 * 	def every_quan_feedback_match(self):             # <<<<<<<<<<<<<<
 * 		return self.every_quan_feedback_match
 * 	@every_quan_feedback_match.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_quan_feedback_match.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":324
 * 		return self.every_quan_feedback_match
 * 	@every_quan_feedback_match.setter
 * 	def every_quan_feedback_match(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_quan_feedback_match = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_25every_quan_feedback_match_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_25every_quan_feedback_match_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25every_quan_feedback_match_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_25every_quan_feedback_match_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 324, 0, __PYX_ERR(0, 324, __pyx_L1_error));

  /* "feedback_manager.pyx":325
 * 	@every_quan_feedback_match.setter
 * 	def every_quan_feedback_match(self, value):
 * 		self.every_quan_feedback_match = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_grade_feedback_match(self):
 */
  __Pyx_TraceLine(325,0,__PYX_ERR(0, 325, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_quan_feedback_match);
  __Pyx_DECREF(__pyx_v_self->every_quan_feedback_match);
  __pyx_v_self->every_quan_feedback_match = __pyx_v_value;

  /* "feedback_manager.pyx":324
 * 		return self.every_quan_feedback_match
 * 	@every_quan_feedback_match.setter
 * 	def every_quan_feedback_match(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_quan_feedback_match = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_quan_feedback_match.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":327
 * 		self.every_quan_feedback_match = value
 * 	@property
 * 	def every_grade_feedback_match(self):             # <<<<<<<<<<<<<<
 * 		return self.every_grade_feedback_match
 * 	@every_grade_feedback_match.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_26every_grade_feedback_match_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_26every_grade_feedback_match_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_26every_grade_feedback_match___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_26every_grade_feedback_match___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 327, 0, __PYX_ERR(0, 327, __pyx_L1_error));

  /* "feedback_manager.pyx":328
 * 	@property
 * 	def every_grade_feedback_match(self):
 * 		return self.every_grade_feedback_match             # <<<<<<<<<<<<<<
 * 	@every_grade_feedback_match.setter
 * 	def every_grade_feedback_match(self, value):
 */
  __Pyx_TraceLine(328,0,__PYX_ERR(0, 328, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_grade_feedback_match);
  __pyx_r = __pyx_v_self->every_grade_feedback_match;
  goto __pyx_L0;

  /* "feedback_manager.pyx":327
 * 		self.every_quan_feedback_match = value
 * 	@property
 * 	def every_grade_feedback_match(self):             # <<<<<<<<<<<<<<
 * 		return self.every_grade_feedback_match
 * 	@every_grade_feedback_match.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_grade_feedback_match.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":330
 * 		return self.every_grade_feedback_match
 * 	@every_grade_feedback_match.setter
 * 	def every_grade_feedback_match(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_grade_feedback_match = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_26every_grade_feedback_match_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_26every_grade_feedback_match_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_26every_grade_feedback_match_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_26every_grade_feedback_match_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 330, 0, __PYX_ERR(0, 330, __pyx_L1_error));

  /* "feedback_manager.pyx":331
 * 	@every_grade_feedback_match.setter
 * 	def every_grade_feedback_match(self, value):
 * 		self.every_grade_feedback_match = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_val_feedback_match(self):
 */
  __Pyx_TraceLine(331,0,__PYX_ERR(0, 331, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_grade_feedback_match);
  __Pyx_DECREF(__pyx_v_self->every_grade_feedback_match);
  __pyx_v_self->every_grade_feedback_match = __pyx_v_value;

  /* "feedback_manager.pyx":330
 * 		return self.every_grade_feedback_match
 * 	@every_grade_feedback_match.setter
 * 	def every_grade_feedback_match(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_grade_feedback_match = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_grade_feedback_match.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":333
 * 		self.every_grade_feedback_match = value
 * 	@property
 * 	def every_val_feedback_match(self):             # <<<<<<<<<<<<<<
 * 		return self.every_val_feedback_match
 * 	@every_val_feedback_match.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24every_val_feedback_match_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_24every_val_feedback_match_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24every_val_feedback_match___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_24every_val_feedback_match___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 333, 0, __PYX_ERR(0, 333, __pyx_L1_error));

  /* "feedback_manager.pyx":334
 * 	@property
 * 	def every_val_feedback_match(self):
 * 		return self.every_val_feedback_match             # <<<<<<<<<<<<<<
 * 	@every_val_feedback_match.setter
 * 	def every_val_feedback_match(self, value):
 */
  __Pyx_TraceLine(334,0,__PYX_ERR(0, 334, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_val_feedback_match);
  __pyx_r = __pyx_v_self->every_val_feedback_match;
  goto __pyx_L0;

  /* "feedback_manager.pyx":333
 * 		self.every_grade_feedback_match = value
 * 	@property
 * 	def every_val_feedback_match(self):             # <<<<<<<<<<<<<<
 * 		return self.every_val_feedback_match
 * 	@every_val_feedback_match.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_val_feedback_match.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":336
 * 		return self.every_val_feedback_match
 * 	@every_val_feedback_match.setter
 * 	def every_val_feedback_match(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_val_feedback_match = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_24every_val_feedback_match_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_24every_val_feedback_match_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_24every_val_feedback_match_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_24every_val_feedback_match_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 336, 0, __PYX_ERR(0, 336, __pyx_L1_error));

  /* "feedback_manager.pyx":337
 * 	@every_val_feedback_match.setter
 * 	def every_val_feedback_match(self, value):
 * 		self.every_val_feedback_match = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_question_kinds(self):
 */
  __Pyx_TraceLine(337,0,__PYX_ERR(0, 337, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_val_feedback_match);
  __Pyx_DECREF(__pyx_v_self->every_val_feedback_match);
  __pyx_v_self->every_val_feedback_match = __pyx_v_value;

  /* "feedback_manager.pyx":336
 * 		return self.every_val_feedback_match
 * 	@every_val_feedback_match.setter
 * 	def every_val_feedback_match(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_val_feedback_match = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_val_feedback_match.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":339
 * 		self.every_val_feedback_match = value
 * 	@property
 * 	def every_question_kinds(self):             # <<<<<<<<<<<<<<
 * 		return self.every_question_kinds
 * 	@every_question_kinds.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_20every_question_kinds_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_20every_question_kinds_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20every_question_kinds___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_20every_question_kinds___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 339, 0, __PYX_ERR(0, 339, __pyx_L1_error));

  /* "feedback_manager.pyx":340
 * 	@property
 * 	def every_question_kinds(self):
 * 		return self.every_question_kinds             # <<<<<<<<<<<<<<
 * 	@every_question_kinds.setter
 * 	def every_question_kinds(self, value):
 */
  __Pyx_TraceLine(340,0,__PYX_ERR(0, 340, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_question_kinds);
  __pyx_r = __pyx_v_self->every_question_kinds;
  goto __pyx_L0;

  /* "feedback_manager.pyx":339
 * 		self.every_val_feedback_match = value
 * 	@property
 * 	def every_question_kinds(self):             # <<<<<<<<<<<<<<
 * 		return self.every_question_kinds
 * 	@every_question_kinds.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_question_kinds.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":342
 * 		return self.every_question_kinds
 * 	@every_question_kinds.setter
 * 	def every_question_kinds(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_question_kinds = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_20every_question_kinds_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_20every_question_kinds_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20every_question_kinds_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_20every_question_kinds_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 342, 0, __PYX_ERR(0, 342, __pyx_L1_error));

  /* "feedback_manager.pyx":343
 * 	@every_question_kinds.setter
 * 	def every_question_kinds(self, value):
 * 		self.every_question_kinds = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_question_percentage_errs(self):
 */
  __Pyx_TraceLine(343,0,__PYX_ERR(0, 343, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_question_kinds);
  __Pyx_DECREF(__pyx_v_self->every_question_kinds);
  __pyx_v_self->every_question_kinds = __pyx_v_value;

  /* "feedback_manager.pyx":342
 * 		return self.every_question_kinds
 * 	@every_question_kinds.setter
 * 	def every_question_kinds(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_question_kinds = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_question_kinds.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":345
 * 		self.every_question_kinds = value
 * 	@property
 * 	def every_question_percentage_errs(self):             # <<<<<<<<<<<<<<
 * 		return self.every_question_percentage_errs
 * 	@every_question_percentage_errs.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_30every_question_percentage_errs_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_30every_question_percentage_errs_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_30every_question_percentage_errs___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_30every_question_percentage_errs___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 345, 0, __PYX_ERR(0, 345, __pyx_L1_error));

  /* "feedback_manager.pyx":346
 * 	@property
 * 	def every_question_percentage_errs(self):
 * 		return self.every_question_percentage_errs             # <<<<<<<<<<<<<<
 * 	@every_question_percentage_errs.setter
 * 	def every_question_percentage_errs(self, value):
 */
  __Pyx_TraceLine(346,0,__PYX_ERR(0, 346, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_question_percentage_errs);
  __pyx_r = __pyx_v_self->every_question_percentage_errs;
  goto __pyx_L0;

  /* "feedback_manager.pyx":345
 * 		self.every_question_kinds = value
 * 	@property
 * 	def every_question_percentage_errs(self):             # <<<<<<<<<<<<<<
 * 		return self.every_question_percentage_errs
 * 	@every_question_percentage_errs.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_question_percentage_errs.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":348
 * 		return self.every_question_percentage_errs
 * 	@every_question_percentage_errs.setter
 * 	def every_question_percentage_errs(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_question_percentage_errs = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_30every_question_percentage_errs_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_30every_question_percentage_errs_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_30every_question_percentage_errs_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_30every_question_percentage_errs_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 348, 0, __PYX_ERR(0, 348, __pyx_L1_error));

  /* "feedback_manager.pyx":349
 * 	@every_question_percentage_errs.setter
 * 	def every_question_percentage_errs(self, value):
 * 		self.every_question_percentage_errs = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_time_percentage_game(self):
 */
  __Pyx_TraceLine(349,0,__PYX_ERR(0, 349, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_question_percentage_errs);
  __Pyx_DECREF(__pyx_v_self->every_question_percentage_errs);
  __pyx_v_self->every_question_percentage_errs = __pyx_v_value;

  /* "feedback_manager.pyx":348
 * 		return self.every_question_percentage_errs
 * 	@every_question_percentage_errs.setter
 * 	def every_question_percentage_errs(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_question_percentage_errs = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_question_percentage_errs.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":351
 * 		self.every_question_percentage_errs = value
 * 	@property
 * 	def every_time_percentage_game(self):             # <<<<<<<<<<<<<<
 * 		return self.every_time_percentage_game
 * 	@every_time_percentage_game.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_26every_time_percentage_game_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_26every_time_percentage_game_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_26every_time_percentage_game___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_26every_time_percentage_game___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 351, 0, __PYX_ERR(0, 351, __pyx_L1_error));

  /* "feedback_manager.pyx":352
 * 	@property
 * 	def every_time_percentage_game(self):
 * 		return self.every_time_percentage_game             # <<<<<<<<<<<<<<
 * 	@every_time_percentage_game.setter
 * 	def every_time_percentage_game(self, value):
 */
  __Pyx_TraceLine(352,0,__PYX_ERR(0, 352, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_time_percentage_game);
  __pyx_r = __pyx_v_self->every_time_percentage_game;
  goto __pyx_L0;

  /* "feedback_manager.pyx":351
 * 		self.every_question_percentage_errs = value
 * 	@property
 * 	def every_time_percentage_game(self):             # <<<<<<<<<<<<<<
 * 		return self.every_time_percentage_game
 * 	@every_time_percentage_game.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_time_percentage_game.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":354
 * 		return self.every_time_percentage_game
 * 	@every_time_percentage_game.setter
 * 	def every_time_percentage_game(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_time_percentage_game = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_26every_time_percentage_game_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_26every_time_percentage_game_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_26every_time_percentage_game_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_26every_time_percentage_game_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 354, 0, __PYX_ERR(0, 354, __pyx_L1_error));

  /* "feedback_manager.pyx":355
 * 	@every_time_percentage_game.setter
 * 	def every_time_percentage_game(self, value):
 * 		self.every_time_percentage_game = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_time_percentage_match(self):
 */
  __Pyx_TraceLine(355,0,__PYX_ERR(0, 355, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_time_percentage_game);
  __Pyx_DECREF(__pyx_v_self->every_time_percentage_game);
  __pyx_v_self->every_time_percentage_game = __pyx_v_value;

  /* "feedback_manager.pyx":354
 * 		return self.every_time_percentage_game
 * 	@every_time_percentage_game.setter
 * 	def every_time_percentage_game(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_time_percentage_game = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_time_percentage_game.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":357
 * 		self.every_time_percentage_game = value
 * 	@property
 * 	def every_time_percentage_match(self):             # <<<<<<<<<<<<<<
 * 		return self.every_time_percentage_match
 * 	@every_time_percentage_match.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_27every_time_percentage_match_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_27every_time_percentage_match_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_27every_time_percentage_match___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_27every_time_percentage_match___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 357, 0, __PYX_ERR(0, 357, __pyx_L1_error));

  /* "feedback_manager.pyx":358
 * 	@property
 * 	def every_time_percentage_match(self):
 * 		return self.every_time_percentage_match             # <<<<<<<<<<<<<<
 * 	@every_time_percentage_match.setter
 * 	def every_time_percentage_match(self, value):
 */
  __Pyx_TraceLine(358,0,__PYX_ERR(0, 358, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_time_percentage_match);
  __pyx_r = __pyx_v_self->every_time_percentage_match;
  goto __pyx_L0;

  /* "feedback_manager.pyx":357
 * 		self.every_time_percentage_game = value
 * 	@property
 * 	def every_time_percentage_match(self):             # <<<<<<<<<<<<<<
 * 		return self.every_time_percentage_match
 * 	@every_time_percentage_match.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_time_percentage_match.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":360
 * 		return self.every_time_percentage_match
 * 	@every_time_percentage_match.setter
 * 	def every_time_percentage_match(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_time_percentage_match = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_27every_time_percentage_match_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_27every_time_percentage_match_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_27every_time_percentage_match_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_27every_time_percentage_match_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 360, 0, __PYX_ERR(0, 360, __pyx_L1_error));

  /* "feedback_manager.pyx":361
 * 	@every_time_percentage_match.setter
 * 	def every_time_percentage_match(self, value):
 * 		self.every_time_percentage_match = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def percentage_ses(self):
 */
  __Pyx_TraceLine(361,0,__PYX_ERR(0, 361, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_time_percentage_match);
  __Pyx_DECREF(__pyx_v_self->every_time_percentage_match);
  __pyx_v_self->every_time_percentage_match = __pyx_v_value;

  /* "feedback_manager.pyx":360
 * 		return self.every_time_percentage_match
 * 	@every_time_percentage_match.setter
 * 	def every_time_percentage_match(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_time_percentage_match = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_time_percentage_match.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":363
 * 		self.every_time_percentage_match = value
 * 	@property
 * 	def percentage_ses(self):             # <<<<<<<<<<<<<<
 * 		return self.percentage_ses
 * 	@percentage_ses.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_14percentage_ses_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_14percentage_ses_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_14percentage_ses___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_14percentage_ses___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 363, 0, __PYX_ERR(0, 363, __pyx_L1_error));

  /* "feedback_manager.pyx":364
 * 	@property
 * 	def percentage_ses(self):
 * 		return self.percentage_ses             # <<<<<<<<<<<<<<
 * 	@percentage_ses.setter
 * 	def percentage_ses(self, value):
 */
  __Pyx_TraceLine(364,0,__PYX_ERR(0, 364, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->percentage_ses);
  __pyx_r = __pyx_v_self->percentage_ses;
  goto __pyx_L0;

  /* "feedback_manager.pyx":363
 * 		self.every_time_percentage_match = value
 * 	@property
 * 	def percentage_ses(self):             # <<<<<<<<<<<<<<
 * 		return self.percentage_ses
 * 	@percentage_ses.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.percentage_ses.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":366
 * 		return self.percentage_ses
 * 	@percentage_ses.setter
 * 	def percentage_ses(self, value):             # <<<<<<<<<<<<<<
 * 		self.percentage_ses = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_14percentage_ses_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_14percentage_ses_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_14percentage_ses_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_14percentage_ses_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 366, 0, __PYX_ERR(0, 366, __pyx_L1_error));

  /* "feedback_manager.pyx":367
 * 	@percentage_ses.setter
 * 	def percentage_ses(self, value):
 * 		self.percentage_ses = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_expected_match_times(self):
 */
  __Pyx_TraceLine(367,0,__PYX_ERR(0, 367, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->percentage_ses);
  __Pyx_DECREF(__pyx_v_self->percentage_ses);
  __pyx_v_self->percentage_ses = __pyx_v_value;

  /* "feedback_manager.pyx":366
 * 		return self.percentage_ses
 * 	@percentage_ses.setter
 * 	def percentage_ses(self, value):             # <<<<<<<<<<<<<<
 * 		self.percentage_ses = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.percentage_ses.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":369
 * 		self.percentage_ses = value
 * 	@property
 * 	def every_expected_match_times(self):             # <<<<<<<<<<<<<<
 * 		return self.every_expected_match_times
 * 	@every_expected_match_times.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_26every_expected_match_times_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_26every_expected_match_times_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_26every_expected_match_times___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_26every_expected_match_times___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 369, 0, __PYX_ERR(0, 369, __pyx_L1_error));

  /* "feedback_manager.pyx":370
 * 	@property
 * 	def every_expected_match_times(self):
 * 		return self.every_expected_match_times             # <<<<<<<<<<<<<<
 * 	@every_expected_match_times.setter
 * 	def every_expected_match_times(self, value):
 */
  __Pyx_TraceLine(370,0,__PYX_ERR(0, 370, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_expected_match_times);
  __pyx_r = __pyx_v_self->every_expected_match_times;
  goto __pyx_L0;

  /* "feedback_manager.pyx":369
 * 		self.percentage_ses = value
 * 	@property
 * 	def every_expected_match_times(self):             # <<<<<<<<<<<<<<
 * 		return self.every_expected_match_times
 * 	@every_expected_match_times.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_expected_match_times.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":372
 * 		return self.every_expected_match_times
 * 	@every_expected_match_times.setter
 * 	def every_expected_match_times(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_expected_match_times = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_26every_expected_match_times_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_26every_expected_match_times_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_26every_expected_match_times_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_26every_expected_match_times_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 372, 0, __PYX_ERR(0, 372, __pyx_L1_error));

  /* "feedback_manager.pyx":373
 * 	@every_expected_match_times.setter
 * 	def every_expected_match_times(self, value):
 * 		self.every_expected_match_times = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_expected_game_times(self):
 */
  __Pyx_TraceLine(373,0,__PYX_ERR(0, 373, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_expected_match_times);
  __Pyx_DECREF(__pyx_v_self->every_expected_match_times);
  __pyx_v_self->every_expected_match_times = __pyx_v_value;

  /* "feedback_manager.pyx":372
 * 		return self.every_expected_match_times
 * 	@every_expected_match_times.setter
 * 	def every_expected_match_times(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_expected_match_times = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_expected_match_times.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":375
 * 		self.every_expected_match_times = value
 * 	@property
 * 	def every_expected_game_times(self):             # <<<<<<<<<<<<<<
 * 		return self.every_expected_game_times
 * 	@every_expected_game_times.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25every_expected_game_times_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_25every_expected_game_times_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25every_expected_game_times___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_25every_expected_game_times___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 375, 0, __PYX_ERR(0, 375, __pyx_L1_error));

  /* "feedback_manager.pyx":376
 * 	@property
 * 	def every_expected_game_times(self):
 * 		return self.every_expected_game_times             # <<<<<<<<<<<<<<
 * 	@every_expected_game_times.setter
 * 	def every_expected_game_times(self, value):
 */
  __Pyx_TraceLine(376,0,__PYX_ERR(0, 376, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_expected_game_times);
  __pyx_r = __pyx_v_self->every_expected_game_times;
  goto __pyx_L0;

  /* "feedback_manager.pyx":375
 * 		self.every_expected_match_times = value
 * 	@property
 * 	def every_expected_game_times(self):             # <<<<<<<<<<<<<<
 * 		return self.every_expected_game_times
 * 	@every_expected_game_times.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_expected_game_times.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":378
 * 		return self.every_expected_game_times
 * 	@every_expected_game_times.setter
 * 	def every_expected_game_times(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_expected_game_times = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_25every_expected_game_times_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_25every_expected_game_times_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_25every_expected_game_times_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_25every_expected_game_times_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 378, 0, __PYX_ERR(0, 378, __pyx_L1_error));

  /* "feedback_manager.pyx":379
 * 	@every_expected_game_times.setter
 * 	def every_expected_game_times(self, value):
 * 		self.every_expected_game_times = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def every_kind_games(self):
 */
  __Pyx_TraceLine(379,0,__PYX_ERR(0, 379, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_expected_game_times);
  __Pyx_DECREF(__pyx_v_self->every_expected_game_times);
  __pyx_v_self->every_expected_game_times = __pyx_v_value;

  /* "feedback_manager.pyx":378
 * 		return self.every_expected_game_times
 * 	@every_expected_game_times.setter
 * 	def every_expected_game_times(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_expected_game_times = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_expected_game_times.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":381
 * 		self.every_expected_game_times = value
 * 	@property
 * 	def every_kind_games(self):             # <<<<<<<<<<<<<<
 * 		return self.every_kind_games
 * 	@every_kind_games.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_16every_kind_games_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_16every_kind_games_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_16every_kind_games___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_16every_kind_games___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 381, 0, __PYX_ERR(0, 381, __pyx_L1_error));

  /* "feedback_manager.pyx":382
 * 	@property
 * 	def every_kind_games(self):
 * 		return self.every_kind_games             # <<<<<<<<<<<<<<
 * 	@every_kind_games.setter
 * 	def every_kind_games(self, value):
 */
  __Pyx_TraceLine(382,0,__PYX_ERR(0, 382, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->every_kind_games);
  __pyx_r = __pyx_v_self->every_kind_games;
  goto __pyx_L0;

  /* "feedback_manager.pyx":381
 * 		self.every_expected_game_times = value
 * 	@property
 * 	def every_kind_games(self):             # <<<<<<<<<<<<<<
 * 		return self.every_kind_games
 * 	@every_kind_games.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_kind_games.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":384
 * 		return self.every_kind_games
 * 	@every_kind_games.setter
 * 	def every_kind_games(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_kind_games = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_16every_kind_games_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_16every_kind_games_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_16every_kind_games_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_16every_kind_games_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 384, 0, __PYX_ERR(0, 384, __pyx_L1_error));

  /* "feedback_manager.pyx":385
 * 	@every_kind_games.setter
 * 	def every_kind_games(self, value):
 * 		self.every_kind_games = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def questions_feeds(self):
 */
  __Pyx_TraceLine(385,0,__PYX_ERR(0, 385, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->every_kind_games);
  __Pyx_DECREF(__pyx_v_self->every_kind_games);
  __pyx_v_self->every_kind_games = __pyx_v_value;

  /* "feedback_manager.pyx":384
 * 		return self.every_kind_games
 * 	@every_kind_games.setter
 * 	def every_kind_games(self, value):             # <<<<<<<<<<<<<<
 * 		self.every_kind_games = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.every_kind_games.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":387
 * 		self.every_kind_games = value
 * 	@property
 * 	def questions_feeds(self):             # <<<<<<<<<<<<<<
 * 		return self.questions_feeds
 * 	@questions_feeds.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_15questions_feeds_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_15questions_feeds_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_15questions_feeds___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_15questions_feeds___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 387, 0, __PYX_ERR(0, 387, __pyx_L1_error));

  /* "feedback_manager.pyx":388
 * 	@property
 * 	def questions_feeds(self):
 * 		return self.questions_feeds             # <<<<<<<<<<<<<<
 * 	@questions_feeds.setter
 * 	def questions_feeds(self, value):
 */
  __Pyx_TraceLine(388,0,__PYX_ERR(0, 388, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->questions_feeds);
  __pyx_r = __pyx_v_self->questions_feeds;
  goto __pyx_L0;

  /* "feedback_manager.pyx":387
 * 		self.every_kind_games = value
 * 	@property
 * 	def questions_feeds(self):             # <<<<<<<<<<<<<<
 * 		return self.questions_feeds
 * 	@questions_feeds.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.questions_feeds.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":390
 * 		return self.questions_feeds
 * 	@questions_feeds.setter
 * 	def questions_feeds(self, value):             # <<<<<<<<<<<<<<
 * 		self.questions_feeds = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_15questions_feeds_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_15questions_feeds_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_15questions_feeds_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_15questions_feeds_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 390, 0, __PYX_ERR(0, 390, __pyx_L1_error));

  /* "feedback_manager.pyx":391
 * 	@questions_feeds.setter
 * 	def questions_feeds(self, value):
 * 		self.questions_feeds = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def matches_feeds(self):
 */
  __Pyx_TraceLine(391,0,__PYX_ERR(0, 391, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->questions_feeds);
  __Pyx_DECREF(__pyx_v_self->questions_feeds);
  __pyx_v_self->questions_feeds = __pyx_v_value;

  /* "feedback_manager.pyx":390
 * 		return self.questions_feeds
 * 	@questions_feeds.setter
 * 	def questions_feeds(self, value):             # <<<<<<<<<<<<<<
 * 		self.questions_feeds = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.questions_feeds.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":393
 * 		self.questions_feeds = value
 * 	@property
 * 	def matches_feeds(self):             # <<<<<<<<<<<<<<
 * 		return self.matches_feeds
 * 	@matches_feeds.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_13matches_feeds_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_13matches_feeds_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_13matches_feeds___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_13matches_feeds___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 393, 0, __PYX_ERR(0, 393, __pyx_L1_error));

  /* "feedback_manager.pyx":394
 * 	@property
 * 	def matches_feeds(self):
 * 		return self.matches_feeds             # <<<<<<<<<<<<<<
 * 	@matches_feeds.setter
 * 	def matches_feeds(self, value):
 */
  __Pyx_TraceLine(394,0,__PYX_ERR(0, 394, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->matches_feeds);
  __pyx_r = __pyx_v_self->matches_feeds;
  goto __pyx_L0;

  /* "feedback_manager.pyx":393
 * 		self.questions_feeds = value
 * 	@property
 * 	def matches_feeds(self):             # <<<<<<<<<<<<<<
 * 		return self.matches_feeds
 * 	@matches_feeds.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.matches_feeds.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":396
 * 		return self.matches_feeds
 * 	@matches_feeds.setter
 * 	def matches_feeds(self, value):             # <<<<<<<<<<<<<<
 * 		self.matches_feeds = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_13matches_feeds_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_13matches_feeds_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_13matches_feeds_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_13matches_feeds_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 396, 0, __PYX_ERR(0, 396, __pyx_L1_error));

  /* "feedback_manager.pyx":397
 * 	@matches_feeds.setter
 * 	def matches_feeds(self, value):
 * 		self.matches_feeds = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def games_feeds(self):
 */
  __Pyx_TraceLine(397,0,__PYX_ERR(0, 397, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->matches_feeds);
  __Pyx_DECREF(__pyx_v_self->matches_feeds);
  __pyx_v_self->matches_feeds = __pyx_v_value;

  /* "feedback_manager.pyx":396
 * 		return self.matches_feeds
 * 	@matches_feeds.setter
 * 	def matches_feeds(self, value):             # <<<<<<<<<<<<<<
 * 		self.matches_feeds = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.matches_feeds.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":399
 * 		self.matches_feeds = value
 * 	@property
 * 	def games_feeds(self):             # <<<<<<<<<<<<<<
 * 		return self.games_feeds
 * 	@games_feeds.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_11games_feeds_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_11games_feeds_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_11games_feeds___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_11games_feeds___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 399, 0, __PYX_ERR(0, 399, __pyx_L1_error));

  /* "feedback_manager.pyx":400
 * 	@property
 * 	def games_feeds(self):
 * 		return self.games_feeds             # <<<<<<<<<<<<<<
 * 	@games_feeds.setter
 * 	def games_feeds(self, value):
 */
  __Pyx_TraceLine(400,0,__PYX_ERR(0, 400, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->games_feeds);
  __pyx_r = __pyx_v_self->games_feeds;
  goto __pyx_L0;

  /* "feedback_manager.pyx":399
 * 		self.matches_feeds = value
 * 	@property
 * 	def games_feeds(self):             # <<<<<<<<<<<<<<
 * 		return self.games_feeds
 * 	@games_feeds.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.games_feeds.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":402
 * 		return self.games_feeds
 * 	@games_feeds.setter
 * 	def games_feeds(self, value):             # <<<<<<<<<<<<<<
 * 		self.games_feeds = value
 * 	@property
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_11games_feeds_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_11games_feeds_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_11games_feeds_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_11games_feeds_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 402, 0, __PYX_ERR(0, 402, __pyx_L1_error));

  /* "feedback_manager.pyx":403
 * 	@games_feeds.setter
 * 	def games_feeds(self, value):
 * 		self.games_feeds = value             # <<<<<<<<<<<<<<
 * 	@property
 * 	def the_session_feeds(self):
 */
  __Pyx_TraceLine(403,0,__PYX_ERR(0, 403, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->games_feeds);
  __Pyx_DECREF(__pyx_v_self->games_feeds);
  __pyx_v_self->games_feeds = __pyx_v_value;

  /* "feedback_manager.pyx":402
 * 		return self.games_feeds
 * 	@games_feeds.setter
 * 	def games_feeds(self, value):             # <<<<<<<<<<<<<<
 * 		self.games_feeds = value
 * 	@property
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.games_feeds.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":405
 * 		self.games_feeds = value
 * 	@property
 * 	def the_session_feeds(self):             # <<<<<<<<<<<<<<
 * 		return self.the_session_feeds
 * 	@the_session_feeds.setter
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17the_session_feeds_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17the_session_feeds_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17the_session_feeds___get__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_17the_session_feeds___get__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 405, 0, __PYX_ERR(0, 405, __pyx_L1_error));

  /* "feedback_manager.pyx":406
 * 	@property
 * 	def the_session_feeds(self):
 * 		return self.the_session_feeds             # <<<<<<<<<<<<<<
 * 	@the_session_feeds.setter
 * 	def the_session_feeds(self, value):
 */
  __Pyx_TraceLine(406,0,__PYX_ERR(0, 406, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->the_session_feeds);
  __pyx_r = __pyx_v_self->the_session_feeds;
  goto __pyx_L0;

  /* "feedback_manager.pyx":405
 * 		self.games_feeds = value
 * 	@property
 * 	def the_session_feeds(self):             # <<<<<<<<<<<<<<
 * 		return self.the_session_feeds
 * 	@the_session_feeds.setter
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.the_session_feeds.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":408
 * 		return self.the_session_feeds
 * 	@the_session_feeds.setter
 * 	def the_session_feeds(self, value):             # <<<<<<<<<<<<<<
 * 		self.the_session_feeds = value
 * 	# ==========================================================================================================================================================
 */

/* Python wrapper */
static int __pyx_pw_16feedback_manager_8Feedback_17the_session_feeds_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_16feedback_manager_8Feedback_17the_session_feeds_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_17the_session_feeds_2__set__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_16feedback_manager_8Feedback_17the_session_feeds_2__set__(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 408, 0, __PYX_ERR(0, 408, __pyx_L1_error));

  /* "feedback_manager.pyx":409
 * 	@the_session_feeds.setter
 * 	def the_session_feeds(self, value):
 * 		self.the_session_feeds = value             # <<<<<<<<<<<<<<
 * 	# ==========================================================================================================================================================
 * 	# Methods
 */
  __Pyx_TraceLine(409,0,__PYX_ERR(0, 409, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->the_session_feeds);
  __Pyx_DECREF(__pyx_v_self->the_session_feeds);
  __pyx_v_self->the_session_feeds = __pyx_v_value;

  /* "feedback_manager.pyx":408
 * 		return self.the_session_feeds
 * 	@the_session_feeds.setter
 * 	def the_session_feeds(self, value):             # <<<<<<<<<<<<<<
 * 		self.the_session_feeds = value
 * 	# ==========================================================================================================================================================
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.the_session_feeds.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":415
 * 	#cpdef takeout_feeds_question(self, int which_kid):
 * 	#	print("uccc")
 * 	cpdef takeout_feeds_question(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_question(self,which_kid)
 * 
 */

static PyObject *__pyx_pw_16feedback_manager_8Feedback_9takeout_feeds_question(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid); /*proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_takeout_feeds_question(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid, int __pyx_skip_dispatch) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("takeout_feeds_question", 0);
  __Pyx_TraceCall("takeout_feeds_question", __pyx_f[0], 415, 0, __PYX_ERR(0, 415, __pyx_L1_error));
  /* Check if called by wrapper */
  if (unlikely(__pyx_skip_dispatch)) ;
  /* Check if overridden in Python */
  else if (unlikely((Py_TYPE(((PyObject *)__pyx_v_self))->tp_dictoffset != 0) || (Py_TYPE(((PyObject *)__pyx_v_self))->tp_flags & (Py_TPFLAGS_IS_ABSTRACT | Py_TPFLAGS_HEAPTYPE)))) {
    #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    static PY_UINT64_T __pyx_tp_dict_version = __PYX_DICT_VERSION_INIT, __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
    if (unlikely(!__Pyx_object_dict_version_matches(((PyObject *)__pyx_v_self), __pyx_tp_dict_version, __pyx_obj_dict_version))) {
      PY_UINT64_T __pyx_type_dict_guard = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      #endif
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(((PyObject *)__pyx_v_self), __pyx_n_s_takeout_feeds_question); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 415, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      if (!PyCFunction_Check(__pyx_t_1) || (PyCFunction_GET_FUNCTION(__pyx_t_1) != (PyCFunction)(void*)__pyx_pw_16feedback_manager_8Feedback_9takeout_feeds_question)) {
        __Pyx_XDECREF(__pyx_r);
        __pyx_t_3 = __Pyx_PyInt_From_int(__pyx_v_which_kid); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 415, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_1);
        __pyx_t_4 = __pyx_t_1; __pyx_t_5 = NULL;
        if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_4))) {
          __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_4);
          if (likely(__pyx_t_5)) {
            PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
            __Pyx_INCREF(__pyx_t_5);
            __Pyx_INCREF(function);
            __Pyx_DECREF_SET(__pyx_t_4, function);
          }
        }
        __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_t_3);
        __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
        __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
        if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 415, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_2);
        __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
        __pyx_r = __pyx_t_2;
        __pyx_t_2 = 0;
        __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
        goto __pyx_L0;
      }
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
      __pyx_tp_dict_version = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      __pyx_obj_dict_version = __Pyx_get_object_dict_version(((PyObject *)__pyx_v_self));
      if (unlikely(__pyx_type_dict_guard != __pyx_tp_dict_version)) {
        __pyx_tp_dict_version = __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
      }
      #endif
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    }
    #endif
  }

  /* "feedback_manager.pyx":416
 * 	#	print("uccc")
 * 	cpdef takeout_feeds_question(self, int which_kid):
 * 		fct.takeout_feeds_from_db_question(self,which_kid)             # <<<<<<<<<<<<<<
 * 
 * 	cpdef takeout_feeds_game(self, int which_kid):
 */
  __Pyx_TraceLine(416,0,__PYX_ERR(0, 416, __pyx_L1_error))
  __pyx_t_1 = __pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_question(((PyObject *)__pyx_v_self), __pyx_v_which_kid); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 416, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":415
 * 	#cpdef takeout_feeds_question(self, int which_kid):
 * 	#	print("uccc")
 * 	cpdef takeout_feeds_question(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_question(self,which_kid)
 * 
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_question", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_9takeout_feeds_question(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid); /*proto*/
static char __pyx_doc_16feedback_manager_8Feedback_8takeout_feeds_question[] = "Feedback.takeout_feeds_question(self, int which_kid)";
static PyObject *__pyx_pw_16feedback_manager_8Feedback_9takeout_feeds_question(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid) {
  int __pyx_v_which_kid;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("takeout_feeds_question (wrapper)", 0);
  assert(__pyx_arg_which_kid); {
    __pyx_v_which_kid = __Pyx_PyInt_As_int(__pyx_arg_which_kid); if (unlikely((__pyx_v_which_kid == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 415, __pyx_L3_error)
  }
  goto __pyx_L4_argument_unpacking_done;
  __pyx_L3_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_question", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __Pyx_RefNannyFinishContext();
  return NULL;
  __pyx_L4_argument_unpacking_done:;
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_8takeout_feeds_question(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((int)__pyx_v_which_kid));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_8takeout_feeds_question(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("takeout_feeds_question", 0);
  __Pyx_TraceCall("takeout_feeds_question (wrapper)", __pyx_f[0], 415, 0, __PYX_ERR(0, 415, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __pyx_f_16feedback_manager_8Feedback_takeout_feeds_question(__pyx_v_self, __pyx_v_which_kid, 1); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 415, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_question", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":418
 * 		fct.takeout_feeds_from_db_question(self,which_kid)
 * 
 * 	cpdef takeout_feeds_game(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_game(self,which_kid)
 * 	cpdef takeout_feeds_match(self, int which_kid):
 */

static PyObject *__pyx_pw_16feedback_manager_8Feedback_11takeout_feeds_game(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid); /*proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_takeout_feeds_game(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid, int __pyx_skip_dispatch) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("takeout_feeds_game", 0);
  __Pyx_TraceCall("takeout_feeds_game", __pyx_f[0], 418, 0, __PYX_ERR(0, 418, __pyx_L1_error));
  /* Check if called by wrapper */
  if (unlikely(__pyx_skip_dispatch)) ;
  /* Check if overridden in Python */
  else if (unlikely((Py_TYPE(((PyObject *)__pyx_v_self))->tp_dictoffset != 0) || (Py_TYPE(((PyObject *)__pyx_v_self))->tp_flags & (Py_TPFLAGS_IS_ABSTRACT | Py_TPFLAGS_HEAPTYPE)))) {
    #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    static PY_UINT64_T __pyx_tp_dict_version = __PYX_DICT_VERSION_INIT, __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
    if (unlikely(!__Pyx_object_dict_version_matches(((PyObject *)__pyx_v_self), __pyx_tp_dict_version, __pyx_obj_dict_version))) {
      PY_UINT64_T __pyx_type_dict_guard = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      #endif
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(((PyObject *)__pyx_v_self), __pyx_n_s_takeout_feeds_game); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 418, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      if (!PyCFunction_Check(__pyx_t_1) || (PyCFunction_GET_FUNCTION(__pyx_t_1) != (PyCFunction)(void*)__pyx_pw_16feedback_manager_8Feedback_11takeout_feeds_game)) {
        __Pyx_XDECREF(__pyx_r);
        __pyx_t_3 = __Pyx_PyInt_From_int(__pyx_v_which_kid); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 418, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_1);
        __pyx_t_4 = __pyx_t_1; __pyx_t_5 = NULL;
        if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_4))) {
          __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_4);
          if (likely(__pyx_t_5)) {
            PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
            __Pyx_INCREF(__pyx_t_5);
            __Pyx_INCREF(function);
            __Pyx_DECREF_SET(__pyx_t_4, function);
          }
        }
        __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_t_3);
        __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
        __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
        if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 418, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_2);
        __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
        __pyx_r = __pyx_t_2;
        __pyx_t_2 = 0;
        __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
        goto __pyx_L0;
      }
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
      __pyx_tp_dict_version = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      __pyx_obj_dict_version = __Pyx_get_object_dict_version(((PyObject *)__pyx_v_self));
      if (unlikely(__pyx_type_dict_guard != __pyx_tp_dict_version)) {
        __pyx_tp_dict_version = __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
      }
      #endif
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    }
    #endif
  }

  /* "feedback_manager.pyx":419
 * 
 * 	cpdef takeout_feeds_game(self, int which_kid):
 * 		fct.takeout_feeds_from_db_game(self,which_kid)             # <<<<<<<<<<<<<<
 * 	cpdef takeout_feeds_match(self, int which_kid):
 * 		fct.takeout_feeds_from_db_match(self,which_kid)
 */
  __Pyx_TraceLine(419,0,__PYX_ERR(0, 419, __pyx_L1_error))
  __pyx_t_1 = __pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_game(((PyObject *)__pyx_v_self), __pyx_v_which_kid); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 419, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":418
 * 		fct.takeout_feeds_from_db_question(self,which_kid)
 * 
 * 	cpdef takeout_feeds_game(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_game(self,which_kid)
 * 	cpdef takeout_feeds_match(self, int which_kid):
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_game", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_11takeout_feeds_game(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid); /*proto*/
static char __pyx_doc_16feedback_manager_8Feedback_10takeout_feeds_game[] = "Feedback.takeout_feeds_game(self, int which_kid)";
static PyObject *__pyx_pw_16feedback_manager_8Feedback_11takeout_feeds_game(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid) {
  int __pyx_v_which_kid;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("takeout_feeds_game (wrapper)", 0);
  assert(__pyx_arg_which_kid); {
    __pyx_v_which_kid = __Pyx_PyInt_As_int(__pyx_arg_which_kid); if (unlikely((__pyx_v_which_kid == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 418, __pyx_L3_error)
  }
  goto __pyx_L4_argument_unpacking_done;
  __pyx_L3_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_game", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __Pyx_RefNannyFinishContext();
  return NULL;
  __pyx_L4_argument_unpacking_done:;
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_10takeout_feeds_game(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((int)__pyx_v_which_kid));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_10takeout_feeds_game(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("takeout_feeds_game", 0);
  __Pyx_TraceCall("takeout_feeds_game (wrapper)", __pyx_f[0], 418, 0, __PYX_ERR(0, 418, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __pyx_f_16feedback_manager_8Feedback_takeout_feeds_game(__pyx_v_self, __pyx_v_which_kid, 1); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 418, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_game", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":420
 * 	cpdef takeout_feeds_game(self, int which_kid):
 * 		fct.takeout_feeds_from_db_game(self,which_kid)
 * 	cpdef takeout_feeds_match(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_match(self,which_kid)
 * 	cpdef takeout_feeds_session(self, int which_kid):
 */

static PyObject *__pyx_pw_16feedback_manager_8Feedback_13takeout_feeds_match(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid); /*proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_takeout_feeds_match(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid, int __pyx_skip_dispatch) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("takeout_feeds_match", 0);
  __Pyx_TraceCall("takeout_feeds_match", __pyx_f[0], 420, 0, __PYX_ERR(0, 420, __pyx_L1_error));
  /* Check if called by wrapper */
  if (unlikely(__pyx_skip_dispatch)) ;
  /* Check if overridden in Python */
  else if (unlikely((Py_TYPE(((PyObject *)__pyx_v_self))->tp_dictoffset != 0) || (Py_TYPE(((PyObject *)__pyx_v_self))->tp_flags & (Py_TPFLAGS_IS_ABSTRACT | Py_TPFLAGS_HEAPTYPE)))) {
    #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    static PY_UINT64_T __pyx_tp_dict_version = __PYX_DICT_VERSION_INIT, __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
    if (unlikely(!__Pyx_object_dict_version_matches(((PyObject *)__pyx_v_self), __pyx_tp_dict_version, __pyx_obj_dict_version))) {
      PY_UINT64_T __pyx_type_dict_guard = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      #endif
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(((PyObject *)__pyx_v_self), __pyx_n_s_takeout_feeds_match); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 420, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      if (!PyCFunction_Check(__pyx_t_1) || (PyCFunction_GET_FUNCTION(__pyx_t_1) != (PyCFunction)(void*)__pyx_pw_16feedback_manager_8Feedback_13takeout_feeds_match)) {
        __Pyx_XDECREF(__pyx_r);
        __pyx_t_3 = __Pyx_PyInt_From_int(__pyx_v_which_kid); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 420, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_1);
        __pyx_t_4 = __pyx_t_1; __pyx_t_5 = NULL;
        if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_4))) {
          __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_4);
          if (likely(__pyx_t_5)) {
            PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
            __Pyx_INCREF(__pyx_t_5);
            __Pyx_INCREF(function);
            __Pyx_DECREF_SET(__pyx_t_4, function);
          }
        }
        __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_t_3);
        __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
        __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
        if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 420, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_2);
        __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
        __pyx_r = __pyx_t_2;
        __pyx_t_2 = 0;
        __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
        goto __pyx_L0;
      }
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
      __pyx_tp_dict_version = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      __pyx_obj_dict_version = __Pyx_get_object_dict_version(((PyObject *)__pyx_v_self));
      if (unlikely(__pyx_type_dict_guard != __pyx_tp_dict_version)) {
        __pyx_tp_dict_version = __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
      }
      #endif
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    }
    #endif
  }

  /* "feedback_manager.pyx":421
 * 		fct.takeout_feeds_from_db_game(self,which_kid)
 * 	cpdef takeout_feeds_match(self, int which_kid):
 * 		fct.takeout_feeds_from_db_match(self,which_kid)             # <<<<<<<<<<<<<<
 * 	cpdef takeout_feeds_session(self, int which_kid):
 * 		fct.takeout_feeds_from_db_session(self,which_kid)
 */
  __Pyx_TraceLine(421,0,__PYX_ERR(0, 421, __pyx_L1_error))
  __pyx_t_1 = __pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_match(((PyObject *)__pyx_v_self), __pyx_v_which_kid); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 421, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":420
 * 	cpdef takeout_feeds_game(self, int which_kid):
 * 		fct.takeout_feeds_from_db_game(self,which_kid)
 * 	cpdef takeout_feeds_match(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_match(self,which_kid)
 * 	cpdef takeout_feeds_session(self, int which_kid):
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_match", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_13takeout_feeds_match(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid); /*proto*/
static char __pyx_doc_16feedback_manager_8Feedback_12takeout_feeds_match[] = "Feedback.takeout_feeds_match(self, int which_kid)";
static PyObject *__pyx_pw_16feedback_manager_8Feedback_13takeout_feeds_match(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid) {
  int __pyx_v_which_kid;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("takeout_feeds_match (wrapper)", 0);
  assert(__pyx_arg_which_kid); {
    __pyx_v_which_kid = __Pyx_PyInt_As_int(__pyx_arg_which_kid); if (unlikely((__pyx_v_which_kid == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 420, __pyx_L3_error)
  }
  goto __pyx_L4_argument_unpacking_done;
  __pyx_L3_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_match", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __Pyx_RefNannyFinishContext();
  return NULL;
  __pyx_L4_argument_unpacking_done:;
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_12takeout_feeds_match(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((int)__pyx_v_which_kid));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_12takeout_feeds_match(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("takeout_feeds_match", 0);
  __Pyx_TraceCall("takeout_feeds_match (wrapper)", __pyx_f[0], 420, 0, __PYX_ERR(0, 420, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __pyx_f_16feedback_manager_8Feedback_takeout_feeds_match(__pyx_v_self, __pyx_v_which_kid, 1); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 420, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_match", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":422
 * 	cpdef takeout_feeds_match(self, int which_kid):
 * 		fct.takeout_feeds_from_db_match(self,which_kid)
 * 	cpdef takeout_feeds_session(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_session(self,which_kid)
 * 
 */

static PyObject *__pyx_pw_16feedback_manager_8Feedback_15takeout_feeds_session(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid); /*proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_takeout_feeds_session(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid, int __pyx_skip_dispatch) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("takeout_feeds_session", 0);
  __Pyx_TraceCall("takeout_feeds_session", __pyx_f[0], 422, 0, __PYX_ERR(0, 422, __pyx_L1_error));
  /* Check if called by wrapper */
  if (unlikely(__pyx_skip_dispatch)) ;
  /* Check if overridden in Python */
  else if (unlikely((Py_TYPE(((PyObject *)__pyx_v_self))->tp_dictoffset != 0) || (Py_TYPE(((PyObject *)__pyx_v_self))->tp_flags & (Py_TPFLAGS_IS_ABSTRACT | Py_TPFLAGS_HEAPTYPE)))) {
    #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    static PY_UINT64_T __pyx_tp_dict_version = __PYX_DICT_VERSION_INIT, __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
    if (unlikely(!__Pyx_object_dict_version_matches(((PyObject *)__pyx_v_self), __pyx_tp_dict_version, __pyx_obj_dict_version))) {
      PY_UINT64_T __pyx_type_dict_guard = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      #endif
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(((PyObject *)__pyx_v_self), __pyx_n_s_takeout_feeds_session); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 422, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      if (!PyCFunction_Check(__pyx_t_1) || (PyCFunction_GET_FUNCTION(__pyx_t_1) != (PyCFunction)(void*)__pyx_pw_16feedback_manager_8Feedback_15takeout_feeds_session)) {
        __Pyx_XDECREF(__pyx_r);
        __pyx_t_3 = __Pyx_PyInt_From_int(__pyx_v_which_kid); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 422, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_1);
        __pyx_t_4 = __pyx_t_1; __pyx_t_5 = NULL;
        if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_4))) {
          __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_4);
          if (likely(__pyx_t_5)) {
            PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
            __Pyx_INCREF(__pyx_t_5);
            __Pyx_INCREF(function);
            __Pyx_DECREF_SET(__pyx_t_4, function);
          }
        }
        __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_t_3);
        __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
        __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
        if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 422, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_2);
        __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
        __pyx_r = __pyx_t_2;
        __pyx_t_2 = 0;
        __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
        goto __pyx_L0;
      }
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
      __pyx_tp_dict_version = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      __pyx_obj_dict_version = __Pyx_get_object_dict_version(((PyObject *)__pyx_v_self));
      if (unlikely(__pyx_type_dict_guard != __pyx_tp_dict_version)) {
        __pyx_tp_dict_version = __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
      }
      #endif
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    }
    #endif
  }

  /* "feedback_manager.pyx":423
 * 		fct.takeout_feeds_from_db_match(self,which_kid)
 * 	cpdef takeout_feeds_session(self, int which_kid):
 * 		fct.takeout_feeds_from_db_session(self,which_kid)             # <<<<<<<<<<<<<<
 * 
 * 	cpdef evaluate_whole_feedbacks(self):
 */
  __Pyx_TraceLine(423,0,__PYX_ERR(0, 423, __pyx_L1_error))
  __pyx_t_1 = __pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_session(((PyObject *)__pyx_v_self), __pyx_v_which_kid); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 423, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":422
 * 	cpdef takeout_feeds_match(self, int which_kid):
 * 		fct.takeout_feeds_from_db_match(self,which_kid)
 * 	cpdef takeout_feeds_session(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_session(self,which_kid)
 * 
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_session", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_15takeout_feeds_session(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid); /*proto*/
static char __pyx_doc_16feedback_manager_8Feedback_14takeout_feeds_session[] = "Feedback.takeout_feeds_session(self, int which_kid)";
static PyObject *__pyx_pw_16feedback_manager_8Feedback_15takeout_feeds_session(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid) {
  int __pyx_v_which_kid;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("takeout_feeds_session (wrapper)", 0);
  assert(__pyx_arg_which_kid); {
    __pyx_v_which_kid = __Pyx_PyInt_As_int(__pyx_arg_which_kid); if (unlikely((__pyx_v_which_kid == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 422, __pyx_L3_error)
  }
  goto __pyx_L4_argument_unpacking_done;
  __pyx_L3_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_session", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __Pyx_RefNannyFinishContext();
  return NULL;
  __pyx_L4_argument_unpacking_done:;
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_14takeout_feeds_session(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((int)__pyx_v_which_kid));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_14takeout_feeds_session(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("takeout_feeds_session", 0);
  __Pyx_TraceCall("takeout_feeds_session (wrapper)", __pyx_f[0], 422, 0, __PYX_ERR(0, 422, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __pyx_f_16feedback_manager_8Feedback_takeout_feeds_session(__pyx_v_self, __pyx_v_which_kid, 1); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 422, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.takeout_feeds_session", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":425
 * 		fct.takeout_feeds_from_db_session(self,which_kid)
 * 
 * 	cpdef evaluate_whole_feedbacks(self):             # <<<<<<<<<<<<<<
 * 		#fcefws.evaluate_feedbacks_whole_session(self)
 * 		fcefws.evaluate_feedbacks_whole_session(self)
 */

static PyObject *__pyx_pw_16feedback_manager_8Feedback_17evaluate_whole_feedbacks(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_evaluate_whole_feedbacks(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_skip_dispatch) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("evaluate_whole_feedbacks", 0);
  __Pyx_TraceCall("evaluate_whole_feedbacks", __pyx_f[0], 425, 0, __PYX_ERR(0, 425, __pyx_L1_error));
  /* Check if called by wrapper */
  if (unlikely(__pyx_skip_dispatch)) ;
  /* Check if overridden in Python */
  else if (unlikely((Py_TYPE(((PyObject *)__pyx_v_self))->tp_dictoffset != 0) || (Py_TYPE(((PyObject *)__pyx_v_self))->tp_flags & (Py_TPFLAGS_IS_ABSTRACT | Py_TPFLAGS_HEAPTYPE)))) {
    #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    static PY_UINT64_T __pyx_tp_dict_version = __PYX_DICT_VERSION_INIT, __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
    if (unlikely(!__Pyx_object_dict_version_matches(((PyObject *)__pyx_v_self), __pyx_tp_dict_version, __pyx_obj_dict_version))) {
      PY_UINT64_T __pyx_type_dict_guard = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      #endif
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(((PyObject *)__pyx_v_self), __pyx_n_s_evaluate_whole_feedbacks); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 425, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      if (!PyCFunction_Check(__pyx_t_1) || (PyCFunction_GET_FUNCTION(__pyx_t_1) != (PyCFunction)(void*)__pyx_pw_16feedback_manager_8Feedback_17evaluate_whole_feedbacks)) {
        __Pyx_XDECREF(__pyx_r);
        __Pyx_INCREF(__pyx_t_1);
        __pyx_t_3 = __pyx_t_1; __pyx_t_4 = NULL;
        if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_3))) {
          __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
          if (likely(__pyx_t_4)) {
            PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
            __Pyx_INCREF(__pyx_t_4);
            __Pyx_INCREF(function);
            __Pyx_DECREF_SET(__pyx_t_3, function);
          }
        }
        __pyx_t_2 = (__pyx_t_4) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
        __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
        if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 425, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_2);
        __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
        __pyx_r = __pyx_t_2;
        __pyx_t_2 = 0;
        __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
        goto __pyx_L0;
      }
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
      __pyx_tp_dict_version = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      __pyx_obj_dict_version = __Pyx_get_object_dict_version(((PyObject *)__pyx_v_self));
      if (unlikely(__pyx_type_dict_guard != __pyx_tp_dict_version)) {
        __pyx_tp_dict_version = __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
      }
      #endif
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    }
    #endif
  }

  /* "feedback_manager.pyx":427
 * 	cpdef evaluate_whole_feedbacks(self):
 * 		#fcefws.evaluate_feedbacks_whole_session(self)
 * 		fcefws.evaluate_feedbacks_whole_session(self)             # <<<<<<<<<<<<<<
 * 
 * 	cpdef feedbacks_insertion(self, int which_kid):
 */
  __Pyx_TraceLine(427,0,__PYX_ERR(0, 427, __pyx_L1_error))
  __pyx_t_1 = __pyx_f_49fuzzy_controller_evaluate_feedbacks_whole_session_evaluate_feedbacks_whole_session(((PyObject *)__pyx_v_self), 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 427, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":425
 * 		fct.takeout_feeds_from_db_session(self,which_kid)
 * 
 * 	cpdef evaluate_whole_feedbacks(self):             # <<<<<<<<<<<<<<
 * 		#fcefws.evaluate_feedbacks_whole_session(self)
 * 		fcefws.evaluate_feedbacks_whole_session(self)
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_AddTraceback("feedback_manager.Feedback.evaluate_whole_feedbacks", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17evaluate_whole_feedbacks(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_16feedback_manager_8Feedback_16evaluate_whole_feedbacks[] = "Feedback.evaluate_whole_feedbacks(self)";
static PyObject *__pyx_pw_16feedback_manager_8Feedback_17evaluate_whole_feedbacks(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("evaluate_whole_feedbacks (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_16evaluate_whole_feedbacks(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_16evaluate_whole_feedbacks(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("evaluate_whole_feedbacks", 0);
  __Pyx_TraceCall("evaluate_whole_feedbacks (wrapper)", __pyx_f[0], 425, 0, __PYX_ERR(0, 425, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __pyx_f_16feedback_manager_8Feedback_evaluate_whole_feedbacks(__pyx_v_self, 1); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 425, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.evaluate_whole_feedbacks", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "feedback_manager.pyx":429
 * 		fcefws.evaluate_feedbacks_whole_session(self)
 * 
 * 	cpdef feedbacks_insertion(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		#self.db_conn.dcu.include_all_feeds_in_once_given_session(db_self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
 * 		s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
 */

static PyObject *__pyx_pw_16feedback_manager_8Feedback_19feedbacks_insertion(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid); /*proto*/
static PyObject *__pyx_f_16feedback_manager_8Feedback_feedbacks_insertion(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid, int __pyx_skip_dispatch) {
  PyObject *__pyx_v_s_sql = NULL;
  PyObject *__pyx_v_this_session = NULL;
  PyObject *__pyx_v_session_id = NULL;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_t_6;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("feedbacks_insertion", 0);
  __Pyx_TraceCall("feedbacks_insertion", __pyx_f[0], 429, 0, __PYX_ERR(0, 429, __pyx_L1_error));
  /* Check if called by wrapper */
  if (unlikely(__pyx_skip_dispatch)) ;
  /* Check if overridden in Python */
  else if (unlikely((Py_TYPE(((PyObject *)__pyx_v_self))->tp_dictoffset != 0) || (Py_TYPE(((PyObject *)__pyx_v_self))->tp_flags & (Py_TPFLAGS_IS_ABSTRACT | Py_TPFLAGS_HEAPTYPE)))) {
    #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    static PY_UINT64_T __pyx_tp_dict_version = __PYX_DICT_VERSION_INIT, __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
    if (unlikely(!__Pyx_object_dict_version_matches(((PyObject *)__pyx_v_self), __pyx_tp_dict_version, __pyx_obj_dict_version))) {
      PY_UINT64_T __pyx_type_dict_guard = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      #endif
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(((PyObject *)__pyx_v_self), __pyx_n_s_feedbacks_insertion); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 429, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      if (!PyCFunction_Check(__pyx_t_1) || (PyCFunction_GET_FUNCTION(__pyx_t_1) != (PyCFunction)(void*)__pyx_pw_16feedback_manager_8Feedback_19feedbacks_insertion)) {
        __Pyx_XDECREF(__pyx_r);
        __pyx_t_3 = __Pyx_PyInt_From_int(__pyx_v_which_kid); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 429, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_1);
        __pyx_t_4 = __pyx_t_1; __pyx_t_5 = NULL;
        if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_4))) {
          __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_4);
          if (likely(__pyx_t_5)) {
            PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
            __Pyx_INCREF(__pyx_t_5);
            __Pyx_INCREF(function);
            __Pyx_DECREF_SET(__pyx_t_4, function);
          }
        }
        __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_t_3);
        __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
        __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
        if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 429, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_2);
        __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
        __pyx_r = __pyx_t_2;
        __pyx_t_2 = 0;
        __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
        goto __pyx_L0;
      }
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
      __pyx_tp_dict_version = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      __pyx_obj_dict_version = __Pyx_get_object_dict_version(((PyObject *)__pyx_v_self));
      if (unlikely(__pyx_type_dict_guard != __pyx_tp_dict_version)) {
        __pyx_tp_dict_version = __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
      }
      #endif
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    }
    #endif
  }

  /* "feedback_manager.pyx":431
 * 	cpdef feedbacks_insertion(self, int which_kid):
 * 		#self.db_conn.dcu.include_all_feeds_in_once_given_session(db_self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
 * 		s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"             # <<<<<<<<<<<<<<
 * 		this_session = self.db_conn.execute_new_query(s_sql,which_kid)
 * 		session_id = this_session[0]
 */
  __Pyx_TraceLine(431,0,__PYX_ERR(0, 431, __pyx_L1_error))
  __Pyx_INCREF(__pyx_kp_u_SELECT_session_id_FROM_Kids_Sess);
  __pyx_v_s_sql = __pyx_kp_u_SELECT_session_id_FROM_Kids_Sess;

  /* "feedback_manager.pyx":432
 * 		#self.db_conn.dcu.include_all_feeds_in_once_given_session(db_self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
 * 		s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
 * 		this_session = self.db_conn.execute_new_query(s_sql,which_kid)             # <<<<<<<<<<<<<<
 * 		session_id = this_session[0]
 * 		print("VEDIAMO SE LE HA MODIFICATE GIUSTE.....")
 */
  __Pyx_TraceLine(432,0,__PYX_ERR(0, 432, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->db_conn, __pyx_n_s_execute_new_query); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 432, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_4 = __Pyx_PyInt_From_int(__pyx_v_which_kid); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 432, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_4);
  __pyx_t_3 = NULL;
  __pyx_t_6 = 0;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
    __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_2);
    if (likely(__pyx_t_3)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
      __Pyx_INCREF(__pyx_t_3);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_2, function);
      __pyx_t_6 = 1;
    }
  }
  #if CYTHON_FAST_PYCALL
  if (PyFunction_Check(__pyx_t_2)) {
    PyObject *__pyx_temp[3] = {__pyx_t_3, __pyx_v_s_sql, __pyx_t_4};
    __pyx_t_1 = __Pyx_PyFunction_FastCall(__pyx_t_2, __pyx_temp+1-__pyx_t_6, 2+__pyx_t_6); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 432, __pyx_L1_error)
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
  } else
  #endif
  #if CYTHON_FAST_PYCCALL
  if (__Pyx_PyFastCFunction_Check(__pyx_t_2)) {
    PyObject *__pyx_temp[3] = {__pyx_t_3, __pyx_v_s_sql, __pyx_t_4};
    __pyx_t_1 = __Pyx_PyCFunction_FastCall(__pyx_t_2, __pyx_temp+1-__pyx_t_6, 2+__pyx_t_6); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 432, __pyx_L1_error)
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
  } else
  #endif
  {
    __pyx_t_5 = PyTuple_New(2+__pyx_t_6); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 432, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    if (__pyx_t_3) {
      __Pyx_GIVEREF(__pyx_t_3); PyTuple_SET_ITEM(__pyx_t_5, 0, __pyx_t_3); __pyx_t_3 = NULL;
    }
    __Pyx_INCREF(__pyx_v_s_sql);
    __Pyx_GIVEREF(__pyx_v_s_sql);
    PyTuple_SET_ITEM(__pyx_t_5, 0+__pyx_t_6, __pyx_v_s_sql);
    __Pyx_GIVEREF(__pyx_t_4);
    PyTuple_SET_ITEM(__pyx_t_5, 1+__pyx_t_6, __pyx_t_4);
    __pyx_t_4 = 0;
    __pyx_t_1 = __Pyx_PyObject_Call(__pyx_t_2, __pyx_t_5, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 432, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  }
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_v_this_session = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":433
 * 		s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
 * 		this_session = self.db_conn.execute_new_query(s_sql,which_kid)
 * 		session_id = this_session[0]             # <<<<<<<<<<<<<<
 * 		print("VEDIAMO SE LE HA MODIFICATE GIUSTE.....")
 * 		print("questions_feeds ", self.questions_feeds)
 */
  __Pyx_TraceLine(433,0,__PYX_ERR(0, 433, __pyx_L1_error))
  __pyx_t_1 = __Pyx_GetItemInt(__pyx_v_this_session, 0, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 433, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_v_session_id = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "feedback_manager.pyx":434
 * 		this_session = self.db_conn.execute_new_query(s_sql,which_kid)
 * 		session_id = this_session[0]
 * 		print("VEDIAMO SE LE HA MODIFICATE GIUSTE.....")             # <<<<<<<<<<<<<<
 * 		print("questions_feeds ", self.questions_feeds)
 * 		print("the_session_feeds ", self.the_session_feeds)
 */
  __Pyx_TraceLine(434,0,__PYX_ERR(0, 434, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_Call(__pyx_builtin_print, __pyx_tuple__3, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 434, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":435
 * 		session_id = this_session[0]
 * 		print("VEDIAMO SE LE HA MODIFICATE GIUSTE.....")
 * 		print("questions_feeds ", self.questions_feeds)             # <<<<<<<<<<<<<<
 * 		print("the_session_feeds ", self.the_session_feeds)
 * 		print("matches_feeds ", self.matches_feeds)
 */
  __Pyx_TraceLine(435,0,__PYX_ERR(0, 435, __pyx_L1_error))
  __pyx_t_1 = PyTuple_New(2); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 435, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_INCREF(__pyx_kp_u_questions_feeds);
  __Pyx_GIVEREF(__pyx_kp_u_questions_feeds);
  PyTuple_SET_ITEM(__pyx_t_1, 0, __pyx_kp_u_questions_feeds);
  __Pyx_INCREF(__pyx_v_self->questions_feeds);
  __Pyx_GIVEREF(__pyx_v_self->questions_feeds);
  PyTuple_SET_ITEM(__pyx_t_1, 1, __pyx_v_self->questions_feeds);
  __pyx_t_2 = __Pyx_PyObject_Call(__pyx_builtin_print, __pyx_t_1, NULL); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 435, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

  /* "feedback_manager.pyx":436
 * 		print("VEDIAMO SE LE HA MODIFICATE GIUSTE.....")
 * 		print("questions_feeds ", self.questions_feeds)
 * 		print("the_session_feeds ", self.the_session_feeds)             # <<<<<<<<<<<<<<
 * 		print("matches_feeds ", self.matches_feeds)
 * 		print("games_feeds ", self.games_feeds)
 */
  __Pyx_TraceLine(436,0,__PYX_ERR(0, 436, __pyx_L1_error))
  __pyx_t_2 = PyTuple_New(2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 436, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_INCREF(__pyx_kp_u_the_session_feeds);
  __Pyx_GIVEREF(__pyx_kp_u_the_session_feeds);
  PyTuple_SET_ITEM(__pyx_t_2, 0, __pyx_kp_u_the_session_feeds);
  __Pyx_INCREF(__pyx_v_self->the_session_feeds);
  __Pyx_GIVEREF(__pyx_v_self->the_session_feeds);
  PyTuple_SET_ITEM(__pyx_t_2, 1, __pyx_v_self->the_session_feeds);
  __pyx_t_1 = __Pyx_PyObject_Call(__pyx_builtin_print, __pyx_t_2, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 436, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":437
 * 		print("questions_feeds ", self.questions_feeds)
 * 		print("the_session_feeds ", self.the_session_feeds)
 * 		print("matches_feeds ", self.matches_feeds)             # <<<<<<<<<<<<<<
 * 		print("games_feeds ", self.games_feeds)
 * 		self.db_conn.include_all_feeds_in_once_given_session(which_kid, session_id, self.questions_feeds, self.the_session_feeds, self.matches_feeds, self.games_feeds)
 */
  __Pyx_TraceLine(437,0,__PYX_ERR(0, 437, __pyx_L1_error))
  __pyx_t_1 = PyTuple_New(2); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 437, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_INCREF(__pyx_kp_u_matches_feeds);
  __Pyx_GIVEREF(__pyx_kp_u_matches_feeds);
  PyTuple_SET_ITEM(__pyx_t_1, 0, __pyx_kp_u_matches_feeds);
  __Pyx_INCREF(__pyx_v_self->matches_feeds);
  __Pyx_GIVEREF(__pyx_v_self->matches_feeds);
  PyTuple_SET_ITEM(__pyx_t_1, 1, __pyx_v_self->matches_feeds);
  __pyx_t_2 = __Pyx_PyObject_Call(__pyx_builtin_print, __pyx_t_1, NULL); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 437, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

  /* "feedback_manager.pyx":438
 * 		print("the_session_feeds ", self.the_session_feeds)
 * 		print("matches_feeds ", self.matches_feeds)
 * 		print("games_feeds ", self.games_feeds)             # <<<<<<<<<<<<<<
 * 		self.db_conn.include_all_feeds_in_once_given_session(which_kid, session_id, self.questions_feeds, self.the_session_feeds, self.matches_feeds, self.games_feeds)
 * 
 */
  __Pyx_TraceLine(438,0,__PYX_ERR(0, 438, __pyx_L1_error))
  __pyx_t_2 = PyTuple_New(2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 438, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_INCREF(__pyx_kp_u_games_feeds);
  __Pyx_GIVEREF(__pyx_kp_u_games_feeds);
  PyTuple_SET_ITEM(__pyx_t_2, 0, __pyx_kp_u_games_feeds);
  __Pyx_INCREF(__pyx_v_self->games_feeds);
  __Pyx_GIVEREF(__pyx_v_self->games_feeds);
  PyTuple_SET_ITEM(__pyx_t_2, 1, __pyx_v_self->games_feeds);
  __pyx_t_1 = __Pyx_PyObject_Call(__pyx_builtin_print, __pyx_t_2, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 438, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":439
 * 		print("matches_feeds ", self.matches_feeds)
 * 		print("games_feeds ", self.games_feeds)
 * 		self.db_conn.include_all_feeds_in_once_given_session(which_kid, session_id, self.questions_feeds, self.the_session_feeds, self.matches_feeds, self.games_feeds)             # <<<<<<<<<<<<<<
 * 
 * 	"""
 */
  __Pyx_TraceLine(439,0,__PYX_ERR(0, 439, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->db_conn, __pyx_n_s_include_all_feeds_in_once_given); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 439, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_5 = __Pyx_PyInt_From_int(__pyx_v_which_kid); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 439, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_5);
  __pyx_t_4 = NULL;
  __pyx_t_6 = 0;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
    __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_2);
    if (likely(__pyx_t_4)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
      __Pyx_INCREF(__pyx_t_4);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_2, function);
      __pyx_t_6 = 1;
    }
  }
  #if CYTHON_FAST_PYCALL
  if (PyFunction_Check(__pyx_t_2)) {
    PyObject *__pyx_temp[7] = {__pyx_t_4, __pyx_t_5, __pyx_v_session_id, __pyx_v_self->questions_feeds, __pyx_v_self->the_session_feeds, __pyx_v_self->matches_feeds, __pyx_v_self->games_feeds};
    __pyx_t_1 = __Pyx_PyFunction_FastCall(__pyx_t_2, __pyx_temp+1-__pyx_t_6, 6+__pyx_t_6); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 439, __pyx_L1_error)
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  } else
  #endif
  #if CYTHON_FAST_PYCCALL
  if (__Pyx_PyFastCFunction_Check(__pyx_t_2)) {
    PyObject *__pyx_temp[7] = {__pyx_t_4, __pyx_t_5, __pyx_v_session_id, __pyx_v_self->questions_feeds, __pyx_v_self->the_session_feeds, __pyx_v_self->matches_feeds, __pyx_v_self->games_feeds};
    __pyx_t_1 = __Pyx_PyCFunction_FastCall(__pyx_t_2, __pyx_temp+1-__pyx_t_6, 6+__pyx_t_6); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 439, __pyx_L1_error)
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  } else
  #endif
  {
    __pyx_t_3 = PyTuple_New(6+__pyx_t_6); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 439, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    if (__pyx_t_4) {
      __Pyx_GIVEREF(__pyx_t_4); PyTuple_SET_ITEM(__pyx_t_3, 0, __pyx_t_4); __pyx_t_4 = NULL;
    }
    __Pyx_GIVEREF(__pyx_t_5);
    PyTuple_SET_ITEM(__pyx_t_3, 0+__pyx_t_6, __pyx_t_5);
    __Pyx_INCREF(__pyx_v_session_id);
    __Pyx_GIVEREF(__pyx_v_session_id);
    PyTuple_SET_ITEM(__pyx_t_3, 1+__pyx_t_6, __pyx_v_session_id);
    __Pyx_INCREF(__pyx_v_self->questions_feeds);
    __Pyx_GIVEREF(__pyx_v_self->questions_feeds);
    PyTuple_SET_ITEM(__pyx_t_3, 2+__pyx_t_6, __pyx_v_self->questions_feeds);
    __Pyx_INCREF(__pyx_v_self->the_session_feeds);
    __Pyx_GIVEREF(__pyx_v_self->the_session_feeds);
    PyTuple_SET_ITEM(__pyx_t_3, 3+__pyx_t_6, __pyx_v_self->the_session_feeds);
    __Pyx_INCREF(__pyx_v_self->matches_feeds);
    __Pyx_GIVEREF(__pyx_v_self->matches_feeds);
    PyTuple_SET_ITEM(__pyx_t_3, 4+__pyx_t_6, __pyx_v_self->matches_feeds);
    __Pyx_INCREF(__pyx_v_self->games_feeds);
    __Pyx_GIVEREF(__pyx_v_self->games_feeds);
    PyTuple_SET_ITEM(__pyx_t_3, 5+__pyx_t_6, __pyx_v_self->games_feeds);
    __pyx_t_5 = 0;
    __pyx_t_1 = __Pyx_PyObject_Call(__pyx_t_2, __pyx_t_3, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 439, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  }
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":429
 * 		fcefws.evaluate_feedbacks_whole_session(self)
 * 
 * 	cpdef feedbacks_insertion(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		#self.db_conn.dcu.include_all_feeds_in_once_given_session(db_self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
 * 		s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("feedback_manager.Feedback.feedbacks_insertion", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_s_sql);
  __Pyx_XDECREF(__pyx_v_this_session);
  __Pyx_XDECREF(__pyx_v_session_id);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_19feedbacks_insertion(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid); /*proto*/
static char __pyx_doc_16feedback_manager_8Feedback_18feedbacks_insertion[] = "Feedback.feedbacks_insertion(self, int which_kid)";
static PyObject *__pyx_pw_16feedback_manager_8Feedback_19feedbacks_insertion(PyObject *__pyx_v_self, PyObject *__pyx_arg_which_kid) {
  int __pyx_v_which_kid;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("feedbacks_insertion (wrapper)", 0);
  assert(__pyx_arg_which_kid); {
    __pyx_v_which_kid = __Pyx_PyInt_As_int(__pyx_arg_which_kid); if (unlikely((__pyx_v_which_kid == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 429, __pyx_L3_error)
  }
  goto __pyx_L4_argument_unpacking_done;
  __pyx_L3_error:;
  __Pyx_AddTraceback("feedback_manager.Feedback.feedbacks_insertion", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __Pyx_RefNannyFinishContext();
  return NULL;
  __pyx_L4_argument_unpacking_done:;
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_18feedbacks_insertion(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((int)__pyx_v_which_kid));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_18feedbacks_insertion(struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, int __pyx_v_which_kid) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("feedbacks_insertion", 0);
  __Pyx_TraceCall("feedbacks_insertion (wrapper)", __pyx_f[0], 429, 0, __PYX_ERR(0, 429, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __pyx_f_16feedback_manager_8Feedback_feedbacks_insertion(__pyx_v_self, __pyx_v_which_kid, 1); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 429, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.feedbacks_insertion", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "(tree fragment)":1
 * def __reduce_cython__(self):             # <<<<<<<<<<<<<<
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_21__reduce_cython__(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_16feedback_manager_8Feedback_20__reduce_cython__[] = "Feedback.__reduce_cython__(self)";
static PyObject *__pyx_pw_16feedback_manager_8Feedback_21__reduce_cython__(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__reduce_cython__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_20__reduce_cython__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_20__reduce_cython__(CYTHON_UNUSED struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__reduce_cython__", 0);
  __Pyx_TraceCall("__reduce_cython__", __pyx_f[1], 1, 0, __PYX_ERR(1, 1, __pyx_L1_error));

  /* "(tree fragment)":2
 * def __reduce_cython__(self):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")             # <<<<<<<<<<<<<<
 * def __setstate_cython__(self, __pyx_state):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 */
  __Pyx_TraceLine(2,0,__PYX_ERR(1, 2, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_Call(__pyx_builtin_TypeError, __pyx_tuple__4, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(1, 2, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_Raise(__pyx_t_1, 0, 0, 0);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __PYX_ERR(1, 2, __pyx_L1_error)

  /* "(tree fragment)":1
 * def __reduce_cython__(self):             # <<<<<<<<<<<<<<
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.__reduce_cython__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "(tree fragment)":3
 * def __reduce_cython__(self):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):             # <<<<<<<<<<<<<<
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 */

/* Python wrapper */
static PyObject *__pyx_pw_16feedback_manager_8Feedback_23__setstate_cython__(PyObject *__pyx_v_self, PyObject *__pyx_v___pyx_state); /*proto*/
static char __pyx_doc_16feedback_manager_8Feedback_22__setstate_cython__[] = "Feedback.__setstate_cython__(self, __pyx_state)";
static PyObject *__pyx_pw_16feedback_manager_8Feedback_23__setstate_cython__(PyObject *__pyx_v_self, PyObject *__pyx_v___pyx_state) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__setstate_cython__ (wrapper)", 0);
  __pyx_r = __pyx_pf_16feedback_manager_8Feedback_22__setstate_cython__(((struct __pyx_obj_16feedback_manager_Feedback *)__pyx_v_self), ((PyObject *)__pyx_v___pyx_state));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_16feedback_manager_8Feedback_22__setstate_cython__(CYTHON_UNUSED struct __pyx_obj_16feedback_manager_Feedback *__pyx_v_self, CYTHON_UNUSED PyObject *__pyx_v___pyx_state) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__setstate_cython__", 0);
  __Pyx_TraceCall("__setstate_cython__", __pyx_f[1], 3, 0, __PYX_ERR(1, 3, __pyx_L1_error));

  /* "(tree fragment)":4
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")             # <<<<<<<<<<<<<<
 */
  __Pyx_TraceLine(4,0,__PYX_ERR(1, 4, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_Call(__pyx_builtin_TypeError, __pyx_tuple__5, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(1, 4, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_Raise(__pyx_t_1, 0, 0, 0);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __PYX_ERR(1, 4, __pyx_L1_error)

  /* "(tree fragment)":3
 * def __reduce_cython__(self):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):             # <<<<<<<<<<<<<<
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("feedback_manager.Feedback.__setstate_cython__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}
static struct __pyx_vtabstruct_16feedback_manager_Feedback __pyx_vtable_16feedback_manager_Feedback;

static PyObject *__pyx_tp_new_16feedback_manager_Feedback(PyTypeObject *t, CYTHON_UNUSED PyObject *a, CYTHON_UNUSED PyObject *k) {
  struct __pyx_obj_16feedback_manager_Feedback *p;
  PyObject *o;
  if (likely((t->tp_flags & Py_TPFLAGS_IS_ABSTRACT) == 0)) {
    o = (*t->tp_alloc)(t, 0);
  } else {
    o = (PyObject *) PyBaseObject_Type.tp_new(t, __pyx_empty_tuple, 0);
  }
  if (unlikely(!o)) return 0;
  p = ((struct __pyx_obj_16feedback_manager_Feedback *)o);
  p->__pyx_vtab = __pyx_vtabptr_16feedback_manager_Feedback;
  p->db_conn = Py_None; Py_INCREF(Py_None);
  p->quantity_feedback_questions = ((PyObject*)Py_None); Py_INCREF(Py_None);
  p->grade_feedback_questions = ((PyObject*)Py_None); Py_INCREF(Py_None);
  p->quantity_feedback_games = ((PyObject*)Py_None); Py_INCREF(Py_None);
  p->grade_feedback_games = ((PyObject*)Py_None); Py_INCREF(Py_None);
  p->quantity_feedback_matches = ((PyObject*)Py_None); Py_INCREF(Py_None);
  p->grade_feedback_matches = ((PyObject*)Py_None); Py_INCREF(Py_None);
  p->quantity_feedback_session = ((PyObject*)Py_None); Py_INCREF(Py_None);
  p->grade_feedback_session = ((PyObject*)Py_None); Py_INCREF(Py_None);
  p->kid_age = Py_None; Py_INCREF(Py_None);
  p->kid_level = Py_None; Py_INCREF(Py_None);
  p->every_question = Py_None; Py_INCREF(Py_None);
  p->every_num_answers = Py_None; Py_INCREF(Py_None);
  p->every_correct_ans = Py_None; Py_INCREF(Py_None);
  p->every_errors_ans = Py_None; Py_INCREF(Py_None);
  p->every_indecisions = Py_None; Py_INCREF(Py_None);
  p->every_question_times = Py_None; Py_INCREF(Py_None);
  p->all_values_questions = Py_None; Py_INCREF(Py_None);
  p->every_question_kinds = Py_None; Py_INCREF(Py_None);
  p->every_question_percentage_errs = Py_None; Py_INCREF(Py_None);
  p->every_game_times = Py_None; Py_INCREF(Py_None);
  p->every_expected_game_times = Py_None; Py_INCREF(Py_None);
  p->every_difficulties_game = Py_None; Py_INCREF(Py_None);
  p->every_quan_feedback_ques = Py_None; Py_INCREF(Py_None);
  p->every_grade_feedback_ques = Py_None; Py_INCREF(Py_None);
  p->every_val_feedback_ques = Py_None; Py_INCREF(Py_None);
  p->every_time_percentage_game = Py_None; Py_INCREF(Py_None);
  p->every_kind_games = Py_None; Py_INCREF(Py_None);
  p->every_match_times = Py_None; Py_INCREF(Py_None);
  p->every_expected_match_times = Py_None; Py_INCREF(Py_None);
  p->every_difficulty_matches = Py_None; Py_INCREF(Py_None);
  p->every_quan_feedback_game = Py_None; Py_INCREF(Py_None);
  p->every_grade_feedback_game = Py_None; Py_INCREF(Py_None);
  p->every_val_feedback_game = Py_None; Py_INCREF(Py_None);
  p->every_time_percentage_match = Py_None; Py_INCREF(Py_None);
  p->complex_ses = Py_None; Py_INCREF(Py_None);
  p->time_ses = Py_None; Py_INCREF(Py_None);
  p->expected_time_ses = Py_None; Py_INCREF(Py_None);
  p->percentage_ses = Py_None; Py_INCREF(Py_None);
  p->time_percentage_ses = Py_None; Py_INCREF(Py_None);
  p->every_quan_feedback_match = Py_None; Py_INCREF(Py_None);
  p->every_grade_feedback_match = Py_None; Py_INCREF(Py_None);
  p->every_val_feedback_match = Py_None; Py_INCREF(Py_None);
  p->the_session_feeds = Py_None; Py_INCREF(Py_None);
  p->matches_feeds = Py_None; Py_INCREF(Py_None);
  p->games_feeds = Py_None; Py_INCREF(Py_None);
  p->questions_feeds = Py_None; Py_INCREF(Py_None);
  if (unlikely(__pyx_pw_16feedback_manager_8Feedback_1__cinit__(o, __pyx_empty_tuple, NULL) < 0)) goto bad;
  return o;
  bad:
  Py_DECREF(o); o = 0;
  return NULL;
}

static void __pyx_tp_dealloc_16feedback_manager_Feedback(PyObject *o) {
  struct __pyx_obj_16feedback_manager_Feedback *p = (struct __pyx_obj_16feedback_manager_Feedback *)o;
  #if CYTHON_USE_TP_FINALIZE
  if (unlikely(PyType_HasFeature(Py_TYPE(o), Py_TPFLAGS_HAVE_FINALIZE) && Py_TYPE(o)->tp_finalize) && !_PyGC_FINALIZED(o)) {
    if (PyObject_CallFinalizerFromDealloc(o)) return;
  }
  #endif
  PyObject_GC_UnTrack(o);
  Py_CLEAR(p->db_conn);
  Py_CLEAR(p->quantity_feedback_questions);
  Py_CLEAR(p->grade_feedback_questions);
  Py_CLEAR(p->quantity_feedback_games);
  Py_CLEAR(p->grade_feedback_games);
  Py_CLEAR(p->quantity_feedback_matches);
  Py_CLEAR(p->grade_feedback_matches);
  Py_CLEAR(p->quantity_feedback_session);
  Py_CLEAR(p->grade_feedback_session);
  Py_CLEAR(p->kid_age);
  Py_CLEAR(p->kid_level);
  Py_CLEAR(p->every_question);
  Py_CLEAR(p->every_num_answers);
  Py_CLEAR(p->every_correct_ans);
  Py_CLEAR(p->every_errors_ans);
  Py_CLEAR(p->every_indecisions);
  Py_CLEAR(p->every_question_times);
  Py_CLEAR(p->all_values_questions);
  Py_CLEAR(p->every_question_kinds);
  Py_CLEAR(p->every_question_percentage_errs);
  Py_CLEAR(p->every_game_times);
  Py_CLEAR(p->every_expected_game_times);
  Py_CLEAR(p->every_difficulties_game);
  Py_CLEAR(p->every_quan_feedback_ques);
  Py_CLEAR(p->every_grade_feedback_ques);
  Py_CLEAR(p->every_val_feedback_ques);
  Py_CLEAR(p->every_time_percentage_game);
  Py_CLEAR(p->every_kind_games);
  Py_CLEAR(p->every_match_times);
  Py_CLEAR(p->every_expected_match_times);
  Py_CLEAR(p->every_difficulty_matches);
  Py_CLEAR(p->every_quan_feedback_game);
  Py_CLEAR(p->every_grade_feedback_game);
  Py_CLEAR(p->every_val_feedback_game);
  Py_CLEAR(p->every_time_percentage_match);
  Py_CLEAR(p->complex_ses);
  Py_CLEAR(p->time_ses);
  Py_CLEAR(p->expected_time_ses);
  Py_CLEAR(p->percentage_ses);
  Py_CLEAR(p->time_percentage_ses);
  Py_CLEAR(p->every_quan_feedback_match);
  Py_CLEAR(p->every_grade_feedback_match);
  Py_CLEAR(p->every_val_feedback_match);
  Py_CLEAR(p->the_session_feeds);
  Py_CLEAR(p->matches_feeds);
  Py_CLEAR(p->games_feeds);
  Py_CLEAR(p->questions_feeds);
  (*Py_TYPE(o)->tp_free)(o);
}

static int __pyx_tp_traverse_16feedback_manager_Feedback(PyObject *o, visitproc v, void *a) {
  int e;
  struct __pyx_obj_16feedback_manager_Feedback *p = (struct __pyx_obj_16feedback_manager_Feedback *)o;
  if (p->db_conn) {
    e = (*v)(p->db_conn, a); if (e) return e;
  }
  if (p->kid_age) {
    e = (*v)(p->kid_age, a); if (e) return e;
  }
  if (p->kid_level) {
    e = (*v)(p->kid_level, a); if (e) return e;
  }
  if (p->every_question) {
    e = (*v)(p->every_question, a); if (e) return e;
  }
  if (p->every_num_answers) {
    e = (*v)(p->every_num_answers, a); if (e) return e;
  }
  if (p->every_correct_ans) {
    e = (*v)(p->every_correct_ans, a); if (e) return e;
  }
  if (p->every_errors_ans) {
    e = (*v)(p->every_errors_ans, a); if (e) return e;
  }
  if (p->every_indecisions) {
    e = (*v)(p->every_indecisions, a); if (e) return e;
  }
  if (p->every_question_times) {
    e = (*v)(p->every_question_times, a); if (e) return e;
  }
  if (p->all_values_questions) {
    e = (*v)(p->all_values_questions, a); if (e) return e;
  }
  if (p->every_question_kinds) {
    e = (*v)(p->every_question_kinds, a); if (e) return e;
  }
  if (p->every_question_percentage_errs) {
    e = (*v)(p->every_question_percentage_errs, a); if (e) return e;
  }
  if (p->every_game_times) {
    e = (*v)(p->every_game_times, a); if (e) return e;
  }
  if (p->every_expected_game_times) {
    e = (*v)(p->every_expected_game_times, a); if (e) return e;
  }
  if (p->every_difficulties_game) {
    e = (*v)(p->every_difficulties_game, a); if (e) return e;
  }
  if (p->every_quan_feedback_ques) {
    e = (*v)(p->every_quan_feedback_ques, a); if (e) return e;
  }
  if (p->every_grade_feedback_ques) {
    e = (*v)(p->every_grade_feedback_ques, a); if (e) return e;
  }
  if (p->every_val_feedback_ques) {
    e = (*v)(p->every_val_feedback_ques, a); if (e) return e;
  }
  if (p->every_time_percentage_game) {
    e = (*v)(p->every_time_percentage_game, a); if (e) return e;
  }
  if (p->every_kind_games) {
    e = (*v)(p->every_kind_games, a); if (e) return e;
  }
  if (p->every_match_times) {
    e = (*v)(p->every_match_times, a); if (e) return e;
  }
  if (p->every_expected_match_times) {
    e = (*v)(p->every_expected_match_times, a); if (e) return e;
  }
  if (p->every_difficulty_matches) {
    e = (*v)(p->every_difficulty_matches, a); if (e) return e;
  }
  if (p->every_quan_feedback_game) {
    e = (*v)(p->every_quan_feedback_game, a); if (e) return e;
  }
  if (p->every_grade_feedback_game) {
    e = (*v)(p->every_grade_feedback_game, a); if (e) return e;
  }
  if (p->every_val_feedback_game) {
    e = (*v)(p->every_val_feedback_game, a); if (e) return e;
  }
  if (p->every_time_percentage_match) {
    e = (*v)(p->every_time_percentage_match, a); if (e) return e;
  }
  if (p->complex_ses) {
    e = (*v)(p->complex_ses, a); if (e) return e;
  }
  if (p->time_ses) {
    e = (*v)(p->time_ses, a); if (e) return e;
  }
  if (p->expected_time_ses) {
    e = (*v)(p->expected_time_ses, a); if (e) return e;
  }
  if (p->percentage_ses) {
    e = (*v)(p->percentage_ses, a); if (e) return e;
  }
  if (p->time_percentage_ses) {
    e = (*v)(p->time_percentage_ses, a); if (e) return e;
  }
  if (p->every_quan_feedback_match) {
    e = (*v)(p->every_quan_feedback_match, a); if (e) return e;
  }
  if (p->every_grade_feedback_match) {
    e = (*v)(p->every_grade_feedback_match, a); if (e) return e;
  }
  if (p->every_val_feedback_match) {
    e = (*v)(p->every_val_feedback_match, a); if (e) return e;
  }
  if (p->the_session_feeds) {
    e = (*v)(p->the_session_feeds, a); if (e) return e;
  }
  if (p->matches_feeds) {
    e = (*v)(p->matches_feeds, a); if (e) return e;
  }
  if (p->games_feeds) {
    e = (*v)(p->games_feeds, a); if (e) return e;
  }
  if (p->questions_feeds) {
    e = (*v)(p->questions_feeds, a); if (e) return e;
  }
  return 0;
}

static int __pyx_tp_clear_16feedback_manager_Feedback(PyObject *o) {
  PyObject* tmp;
  struct __pyx_obj_16feedback_manager_Feedback *p = (struct __pyx_obj_16feedback_manager_Feedback *)o;
  tmp = ((PyObject*)p->db_conn);
  p->db_conn = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->kid_age);
  p->kid_age = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->kid_level);
  p->kid_level = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_question);
  p->every_question = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_num_answers);
  p->every_num_answers = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_correct_ans);
  p->every_correct_ans = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_errors_ans);
  p->every_errors_ans = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_indecisions);
  p->every_indecisions = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_question_times);
  p->every_question_times = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->all_values_questions);
  p->all_values_questions = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_question_kinds);
  p->every_question_kinds = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_question_percentage_errs);
  p->every_question_percentage_errs = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_game_times);
  p->every_game_times = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_expected_game_times);
  p->every_expected_game_times = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_difficulties_game);
  p->every_difficulties_game = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_quan_feedback_ques);
  p->every_quan_feedback_ques = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_grade_feedback_ques);
  p->every_grade_feedback_ques = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_val_feedback_ques);
  p->every_val_feedback_ques = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_time_percentage_game);
  p->every_time_percentage_game = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_kind_games);
  p->every_kind_games = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_match_times);
  p->every_match_times = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_expected_match_times);
  p->every_expected_match_times = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_difficulty_matches);
  p->every_difficulty_matches = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_quan_feedback_game);
  p->every_quan_feedback_game = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_grade_feedback_game);
  p->every_grade_feedback_game = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_val_feedback_game);
  p->every_val_feedback_game = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_time_percentage_match);
  p->every_time_percentage_match = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->complex_ses);
  p->complex_ses = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->time_ses);
  p->time_ses = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->expected_time_ses);
  p->expected_time_ses = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->percentage_ses);
  p->percentage_ses = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->time_percentage_ses);
  p->time_percentage_ses = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_quan_feedback_match);
  p->every_quan_feedback_match = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_grade_feedback_match);
  p->every_grade_feedback_match = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->every_val_feedback_match);
  p->every_val_feedback_match = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->the_session_feeds);
  p->the_session_feeds = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->matches_feeds);
  p->matches_feeds = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->games_feeds);
  p->games_feeds = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  tmp = ((PyObject*)p->questions_feeds);
  p->questions_feeds = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  return 0;
}

static PyObject *__pyx_tp_richcompare_16feedback_manager_Feedback(PyObject *o1, PyObject *o2, int op) {
  switch (op) {
    case Py_EQ: {
      return __pyx_pw_16feedback_manager_8Feedback_7__eq__(o1, o2);
    }
    case Py_NE: {
      PyObject *ret;
      ret = __pyx_pw_16feedback_manager_8Feedback_7__eq__(o1, o2);
      if (likely(ret && ret != Py_NotImplemented)) {
        int b = __Pyx_PyObject_IsTrue(ret); Py_DECREF(ret);
        if (unlikely(b < 0)) return NULL;
        ret = (b) ? Py_False : Py_True;
        Py_INCREF(ret);
      }
      return ret;
    }
    default: {
      return __Pyx_NewRef(Py_NotImplemented);
    }
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_quantity_feedback_questions(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_27quantity_feedback_questions_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_quantity_feedback_questions(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_27quantity_feedback_questions_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_quantity_feedback_games(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_23quantity_feedback_games_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_quantity_feedback_games(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_23quantity_feedback_games_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_quantity_feedback_matches(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_matches_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_quantity_feedback_matches(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_matches_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_quantity_feedback_session(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_session_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_quantity_feedback_session(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_25quantity_feedback_session_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_grade_feedback_questions(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_24grade_feedback_questions_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_grade_feedback_questions(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_24grade_feedback_questions_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_grade_feedback_games(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_20grade_feedback_games_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_grade_feedback_games(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_20grade_feedback_games_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_grade_feedback_matches(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_22grade_feedback_matches_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_grade_feedback_matches(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_22grade_feedback_matches_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_grade_feedback_session(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_22grade_feedback_session_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_grade_feedback_session(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_22grade_feedback_session_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_value_feedback_questions(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_24value_feedback_questions_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_value_feedback_questions(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_24value_feedback_questions_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_value_feedback_games(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_20value_feedback_games_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_value_feedback_games(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_20value_feedback_games_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_value_feedback_matches(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_22value_feedback_matches_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_value_feedback_matches(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_22value_feedback_matches_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_value_feedback_session(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_22value_feedback_session_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_value_feedback_session(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_22value_feedback_session_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_db_conn(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_7db_conn_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_db_conn(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_7db_conn_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_kid_age(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_7kid_age_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_kid_age(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_7kid_age_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_kid_level(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_9kid_level_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_kid_level(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_9kid_level_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_question(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_14every_question_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_question(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_14every_question_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_num_answers(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_17every_num_answers_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_num_answers(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_17every_num_answers_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_correct_ans(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_17every_correct_ans_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_correct_ans(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_17every_correct_ans_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_errors_ans(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_16every_errors_ans_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_errors_ans(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_16every_errors_ans_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_indecisions(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_17every_indecisions_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_indecisions(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_17every_indecisions_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_question_times(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_20every_question_times_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_question_times(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_20every_question_times_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_all_values_questions(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_20all_values_questions_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_all_values_questions(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_20all_values_questions_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_game_times(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_16every_game_times_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_game_times(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_16every_game_times_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_difficulties_game(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_23every_difficulties_game_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_difficulties_game(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_23every_difficulties_game_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_quan_feedback_ques(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_ques_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_quan_feedback_ques(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_ques_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_grade_feedback_ques(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_ques_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_grade_feedback_ques(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_ques_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_val_feedback_ques(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_ques_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_val_feedback_ques(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_ques_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_match_times(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_17every_match_times_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_match_times(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_17every_match_times_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_difficulty_matches(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_24every_difficulty_matches_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_difficulty_matches(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_24every_difficulty_matches_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_quan_feedback_game(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_game_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_quan_feedback_game(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_24every_quan_feedback_game_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_grade_feedback_game(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_game_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_grade_feedback_game(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_25every_grade_feedback_game_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_val_feedback_game(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_game_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_val_feedback_game(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_23every_val_feedback_game_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_complex_ses(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_11complex_ses_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_complex_ses(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_11complex_ses_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_time_ses(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_8time_ses_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_time_ses(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_8time_ses_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_time_percentage_ses(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_19time_percentage_ses_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_time_percentage_ses(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_19time_percentage_ses_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_expected_time_ses(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_17expected_time_ses_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_expected_time_ses(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_17expected_time_ses_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_quan_feedback_match(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_25every_quan_feedback_match_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_quan_feedback_match(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_25every_quan_feedback_match_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_grade_feedback_match(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_26every_grade_feedback_match_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_grade_feedback_match(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_26every_grade_feedback_match_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_val_feedback_match(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_24every_val_feedback_match_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_val_feedback_match(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_24every_val_feedback_match_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_question_kinds(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_20every_question_kinds_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_question_kinds(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_20every_question_kinds_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_question_percentage_errs(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_30every_question_percentage_errs_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_question_percentage_errs(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_30every_question_percentage_errs_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_time_percentage_game(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_26every_time_percentage_game_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_time_percentage_game(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_26every_time_percentage_game_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_time_percentage_match(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_27every_time_percentage_match_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_time_percentage_match(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_27every_time_percentage_match_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_percentage_ses(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_14percentage_ses_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_percentage_ses(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_14percentage_ses_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_expected_match_times(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_26every_expected_match_times_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_expected_match_times(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_26every_expected_match_times_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_expected_game_times(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_25every_expected_game_times_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_expected_game_times(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_25every_expected_game_times_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_every_kind_games(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_16every_kind_games_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_every_kind_games(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_16every_kind_games_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_questions_feeds(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_15questions_feeds_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_questions_feeds(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_15questions_feeds_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_matches_feeds(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_13matches_feeds_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_matches_feeds(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_13matches_feeds_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_games_feeds(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_11games_feeds_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_games_feeds(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_11games_feeds_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_16feedback_manager_8Feedback_the_session_feeds(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_16feedback_manager_8Feedback_17the_session_feeds_1__get__(o);
}

static int __pyx_setprop_16feedback_manager_8Feedback_the_session_feeds(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_16feedback_manager_8Feedback_17the_session_feeds_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyMethodDef __pyx_methods_16feedback_manager_Feedback[] = {
  {"__enter__", (PyCFunction)__pyx_pw_16feedback_manager_8Feedback_3__enter__, METH_NOARGS, __pyx_doc_16feedback_manager_8Feedback_2__enter__},
  {"__exit__", (PyCFunction)(void*)(PyCFunctionWithKeywords)__pyx_pw_16feedback_manager_8Feedback_5__exit__, METH_VARARGS|METH_KEYWORDS, __pyx_doc_16feedback_manager_8Feedback_4__exit__},
  {"takeout_feeds_question", (PyCFunction)__pyx_pw_16feedback_manager_8Feedback_9takeout_feeds_question, METH_O, __pyx_doc_16feedback_manager_8Feedback_8takeout_feeds_question},
  {"takeout_feeds_game", (PyCFunction)__pyx_pw_16feedback_manager_8Feedback_11takeout_feeds_game, METH_O, __pyx_doc_16feedback_manager_8Feedback_10takeout_feeds_game},
  {"takeout_feeds_match", (PyCFunction)__pyx_pw_16feedback_manager_8Feedback_13takeout_feeds_match, METH_O, __pyx_doc_16feedback_manager_8Feedback_12takeout_feeds_match},
  {"takeout_feeds_session", (PyCFunction)__pyx_pw_16feedback_manager_8Feedback_15takeout_feeds_session, METH_O, __pyx_doc_16feedback_manager_8Feedback_14takeout_feeds_session},
  {"evaluate_whole_feedbacks", (PyCFunction)__pyx_pw_16feedback_manager_8Feedback_17evaluate_whole_feedbacks, METH_NOARGS, __pyx_doc_16feedback_manager_8Feedback_16evaluate_whole_feedbacks},
  {"feedbacks_insertion", (PyCFunction)__pyx_pw_16feedback_manager_8Feedback_19feedbacks_insertion, METH_O, __pyx_doc_16feedback_manager_8Feedback_18feedbacks_insertion},
  {"__reduce_cython__", (PyCFunction)__pyx_pw_16feedback_manager_8Feedback_21__reduce_cython__, METH_NOARGS, __pyx_doc_16feedback_manager_8Feedback_20__reduce_cython__},
  {"__setstate_cython__", (PyCFunction)__pyx_pw_16feedback_manager_8Feedback_23__setstate_cython__, METH_O, __pyx_doc_16feedback_manager_8Feedback_22__setstate_cython__},
  {0, 0, 0, 0}
};

static struct PyGetSetDef __pyx_getsets_16feedback_manager_Feedback[] = {
  {(char *)"quantity_feedback_questions", __pyx_getprop_16feedback_manager_8Feedback_quantity_feedback_questions, __pyx_setprop_16feedback_manager_8Feedback_quantity_feedback_questions, (char *)0, 0},
  {(char *)"quantity_feedback_games", __pyx_getprop_16feedback_manager_8Feedback_quantity_feedback_games, __pyx_setprop_16feedback_manager_8Feedback_quantity_feedback_games, (char *)0, 0},
  {(char *)"quantity_feedback_matches", __pyx_getprop_16feedback_manager_8Feedback_quantity_feedback_matches, __pyx_setprop_16feedback_manager_8Feedback_quantity_feedback_matches, (char *)0, 0},
  {(char *)"quantity_feedback_session", __pyx_getprop_16feedback_manager_8Feedback_quantity_feedback_session, __pyx_setprop_16feedback_manager_8Feedback_quantity_feedback_session, (char *)0, 0},
  {(char *)"grade_feedback_questions", __pyx_getprop_16feedback_manager_8Feedback_grade_feedback_questions, __pyx_setprop_16feedback_manager_8Feedback_grade_feedback_questions, (char *)0, 0},
  {(char *)"grade_feedback_games", __pyx_getprop_16feedback_manager_8Feedback_grade_feedback_games, __pyx_setprop_16feedback_manager_8Feedback_grade_feedback_games, (char *)0, 0},
  {(char *)"grade_feedback_matches", __pyx_getprop_16feedback_manager_8Feedback_grade_feedback_matches, __pyx_setprop_16feedback_manager_8Feedback_grade_feedback_matches, (char *)0, 0},
  {(char *)"grade_feedback_session", __pyx_getprop_16feedback_manager_8Feedback_grade_feedback_session, __pyx_setprop_16feedback_manager_8Feedback_grade_feedback_session, (char *)0, 0},
  {(char *)"value_feedback_questions", __pyx_getprop_16feedback_manager_8Feedback_value_feedback_questions, __pyx_setprop_16feedback_manager_8Feedback_value_feedback_questions, (char *)0, 0},
  {(char *)"value_feedback_games", __pyx_getprop_16feedback_manager_8Feedback_value_feedback_games, __pyx_setprop_16feedback_manager_8Feedback_value_feedback_games, (char *)0, 0},
  {(char *)"value_feedback_matches", __pyx_getprop_16feedback_manager_8Feedback_value_feedback_matches, __pyx_setprop_16feedback_manager_8Feedback_value_feedback_matches, (char *)0, 0},
  {(char *)"value_feedback_session", __pyx_getprop_16feedback_manager_8Feedback_value_feedback_session, __pyx_setprop_16feedback_manager_8Feedback_value_feedback_session, (char *)0, 0},
  {(char *)"db_conn", __pyx_getprop_16feedback_manager_8Feedback_db_conn, __pyx_setprop_16feedback_manager_8Feedback_db_conn, (char *)0, 0},
  {(char *)"kid_age", __pyx_getprop_16feedback_manager_8Feedback_kid_age, __pyx_setprop_16feedback_manager_8Feedback_kid_age, (char *)0, 0},
  {(char *)"kid_level", __pyx_getprop_16feedback_manager_8Feedback_kid_level, __pyx_setprop_16feedback_manager_8Feedback_kid_level, (char *)0, 0},
  {(char *)"every_question", __pyx_getprop_16feedback_manager_8Feedback_every_question, __pyx_setprop_16feedback_manager_8Feedback_every_question, (char *)0, 0},
  {(char *)"every_num_answers", __pyx_getprop_16feedback_manager_8Feedback_every_num_answers, __pyx_setprop_16feedback_manager_8Feedback_every_num_answers, (char *)0, 0},
  {(char *)"every_correct_ans", __pyx_getprop_16feedback_manager_8Feedback_every_correct_ans, __pyx_setprop_16feedback_manager_8Feedback_every_correct_ans, (char *)0, 0},
  {(char *)"every_errors_ans", __pyx_getprop_16feedback_manager_8Feedback_every_errors_ans, __pyx_setprop_16feedback_manager_8Feedback_every_errors_ans, (char *)0, 0},
  {(char *)"every_indecisions", __pyx_getprop_16feedback_manager_8Feedback_every_indecisions, __pyx_setprop_16feedback_manager_8Feedback_every_indecisions, (char *)0, 0},
  {(char *)"every_question_times", __pyx_getprop_16feedback_manager_8Feedback_every_question_times, __pyx_setprop_16feedback_manager_8Feedback_every_question_times, (char *)0, 0},
  {(char *)"all_values_questions", __pyx_getprop_16feedback_manager_8Feedback_all_values_questions, __pyx_setprop_16feedback_manager_8Feedback_all_values_questions, (char *)0, 0},
  {(char *)"every_game_times", __pyx_getprop_16feedback_manager_8Feedback_every_game_times, __pyx_setprop_16feedback_manager_8Feedback_every_game_times, (char *)0, 0},
  {(char *)"every_difficulties_game", __pyx_getprop_16feedback_manager_8Feedback_every_difficulties_game, __pyx_setprop_16feedback_manager_8Feedback_every_difficulties_game, (char *)0, 0},
  {(char *)"every_quan_feedback_ques", __pyx_getprop_16feedback_manager_8Feedback_every_quan_feedback_ques, __pyx_setprop_16feedback_manager_8Feedback_every_quan_feedback_ques, (char *)0, 0},
  {(char *)"every_grade_feedback_ques", __pyx_getprop_16feedback_manager_8Feedback_every_grade_feedback_ques, __pyx_setprop_16feedback_manager_8Feedback_every_grade_feedback_ques, (char *)0, 0},
  {(char *)"every_val_feedback_ques", __pyx_getprop_16feedback_manager_8Feedback_every_val_feedback_ques, __pyx_setprop_16feedback_manager_8Feedback_every_val_feedback_ques, (char *)0, 0},
  {(char *)"every_match_times", __pyx_getprop_16feedback_manager_8Feedback_every_match_times, __pyx_setprop_16feedback_manager_8Feedback_every_match_times, (char *)0, 0},
  {(char *)"every_difficulty_matches", __pyx_getprop_16feedback_manager_8Feedback_every_difficulty_matches, __pyx_setprop_16feedback_manager_8Feedback_every_difficulty_matches, (char *)0, 0},
  {(char *)"every_quan_feedback_game", __pyx_getprop_16feedback_manager_8Feedback_every_quan_feedback_game, __pyx_setprop_16feedback_manager_8Feedback_every_quan_feedback_game, (char *)0, 0},
  {(char *)"every_grade_feedback_game", __pyx_getprop_16feedback_manager_8Feedback_every_grade_feedback_game, __pyx_setprop_16feedback_manager_8Feedback_every_grade_feedback_game, (char *)0, 0},
  {(char *)"every_val_feedback_game", __pyx_getprop_16feedback_manager_8Feedback_every_val_feedback_game, __pyx_setprop_16feedback_manager_8Feedback_every_val_feedback_game, (char *)0, 0},
  {(char *)"complex_ses", __pyx_getprop_16feedback_manager_8Feedback_complex_ses, __pyx_setprop_16feedback_manager_8Feedback_complex_ses, (char *)0, 0},
  {(char *)"time_ses", __pyx_getprop_16feedback_manager_8Feedback_time_ses, __pyx_setprop_16feedback_manager_8Feedback_time_ses, (char *)0, 0},
  {(char *)"time_percentage_ses", __pyx_getprop_16feedback_manager_8Feedback_time_percentage_ses, __pyx_setprop_16feedback_manager_8Feedback_time_percentage_ses, (char *)0, 0},
  {(char *)"expected_time_ses", __pyx_getprop_16feedback_manager_8Feedback_expected_time_ses, __pyx_setprop_16feedback_manager_8Feedback_expected_time_ses, (char *)0, 0},
  {(char *)"every_quan_feedback_match", __pyx_getprop_16feedback_manager_8Feedback_every_quan_feedback_match, __pyx_setprop_16feedback_manager_8Feedback_every_quan_feedback_match, (char *)0, 0},
  {(char *)"every_grade_feedback_match", __pyx_getprop_16feedback_manager_8Feedback_every_grade_feedback_match, __pyx_setprop_16feedback_manager_8Feedback_every_grade_feedback_match, (char *)0, 0},
  {(char *)"every_val_feedback_match", __pyx_getprop_16feedback_manager_8Feedback_every_val_feedback_match, __pyx_setprop_16feedback_manager_8Feedback_every_val_feedback_match, (char *)0, 0},
  {(char *)"every_question_kinds", __pyx_getprop_16feedback_manager_8Feedback_every_question_kinds, __pyx_setprop_16feedback_manager_8Feedback_every_question_kinds, (char *)0, 0},
  {(char *)"every_question_percentage_errs", __pyx_getprop_16feedback_manager_8Feedback_every_question_percentage_errs, __pyx_setprop_16feedback_manager_8Feedback_every_question_percentage_errs, (char *)0, 0},
  {(char *)"every_time_percentage_game", __pyx_getprop_16feedback_manager_8Feedback_every_time_percentage_game, __pyx_setprop_16feedback_manager_8Feedback_every_time_percentage_game, (char *)0, 0},
  {(char *)"every_time_percentage_match", __pyx_getprop_16feedback_manager_8Feedback_every_time_percentage_match, __pyx_setprop_16feedback_manager_8Feedback_every_time_percentage_match, (char *)0, 0},
  {(char *)"percentage_ses", __pyx_getprop_16feedback_manager_8Feedback_percentage_ses, __pyx_setprop_16feedback_manager_8Feedback_percentage_ses, (char *)0, 0},
  {(char *)"every_expected_match_times", __pyx_getprop_16feedback_manager_8Feedback_every_expected_match_times, __pyx_setprop_16feedback_manager_8Feedback_every_expected_match_times, (char *)0, 0},
  {(char *)"every_expected_game_times", __pyx_getprop_16feedback_manager_8Feedback_every_expected_game_times, __pyx_setprop_16feedback_manager_8Feedback_every_expected_game_times, (char *)0, 0},
  {(char *)"every_kind_games", __pyx_getprop_16feedback_manager_8Feedback_every_kind_games, __pyx_setprop_16feedback_manager_8Feedback_every_kind_games, (char *)0, 0},
  {(char *)"questions_feeds", __pyx_getprop_16feedback_manager_8Feedback_questions_feeds, __pyx_setprop_16feedback_manager_8Feedback_questions_feeds, (char *)0, 0},
  {(char *)"matches_feeds", __pyx_getprop_16feedback_manager_8Feedback_matches_feeds, __pyx_setprop_16feedback_manager_8Feedback_matches_feeds, (char *)0, 0},
  {(char *)"games_feeds", __pyx_getprop_16feedback_manager_8Feedback_games_feeds, __pyx_setprop_16feedback_manager_8Feedback_games_feeds, (char *)0, 0},
  {(char *)"the_session_feeds", __pyx_getprop_16feedback_manager_8Feedback_the_session_feeds, __pyx_setprop_16feedback_manager_8Feedback_the_session_feeds, (char *)0, 0},
  {0, 0, 0, 0, 0}
};

static PyTypeObject __pyx_type_16feedback_manager_Feedback = {
  PyVarObject_HEAD_INIT(0, 0)
  "feedback_manager.Feedback", /*tp_name*/
  sizeof(struct __pyx_obj_16feedback_manager_Feedback), /*tp_basicsize*/
  0, /*tp_itemsize*/
  __pyx_tp_dealloc_16feedback_manager_Feedback, /*tp_dealloc*/
  #if PY_VERSION_HEX < 0x030800b4
  0, /*tp_print*/
  #endif
  #if PY_VERSION_HEX >= 0x030800b4
  0, /*tp_vectorcall_offset*/
  #endif
  0, /*tp_getattr*/
  0, /*tp_setattr*/
  #if PY_MAJOR_VERSION < 3
  0, /*tp_compare*/
  #endif
  #if PY_MAJOR_VERSION >= 3
  0, /*tp_as_async*/
  #endif
  0, /*tp_repr*/
  0, /*tp_as_number*/
  0, /*tp_as_sequence*/
  0, /*tp_as_mapping*/
  0, /*tp_hash*/
  0, /*tp_call*/
  0, /*tp_str*/
  0, /*tp_getattro*/
  0, /*tp_setattro*/
  0, /*tp_as_buffer*/
  Py_TPFLAGS_DEFAULT|Py_TPFLAGS_HAVE_VERSION_TAG|Py_TPFLAGS_CHECKTYPES|Py_TPFLAGS_HAVE_NEWBUFFER|Py_TPFLAGS_BASETYPE|Py_TPFLAGS_HAVE_GC, /*tp_flags*/
  0, /*tp_doc*/
  __pyx_tp_traverse_16feedback_manager_Feedback, /*tp_traverse*/
  __pyx_tp_clear_16feedback_manager_Feedback, /*tp_clear*/
  __pyx_tp_richcompare_16feedback_manager_Feedback, /*tp_richcompare*/
  0, /*tp_weaklistoffset*/
  0, /*tp_iter*/
  0, /*tp_iternext*/
  __pyx_methods_16feedback_manager_Feedback, /*tp_methods*/
  0, /*tp_members*/
  __pyx_getsets_16feedback_manager_Feedback, /*tp_getset*/
  0, /*tp_base*/
  0, /*tp_dict*/
  0, /*tp_descr_get*/
  0, /*tp_descr_set*/
  0, /*tp_dictoffset*/
  0, /*tp_init*/
  0, /*tp_alloc*/
  __pyx_tp_new_16feedback_manager_Feedback, /*tp_new*/
  0, /*tp_free*/
  0, /*tp_is_gc*/
  0, /*tp_bases*/
  0, /*tp_mro*/
  0, /*tp_cache*/
  0, /*tp_subclasses*/
  0, /*tp_weaklist*/
  0, /*tp_del*/
  0, /*tp_version_tag*/
  #if PY_VERSION_HEX >= 0x030400a1
  0, /*tp_finalize*/
  #endif
  #if PY_VERSION_HEX >= 0x030800b1
  0, /*tp_vectorcall*/
  #endif
  #if PY_VERSION_HEX >= 0x030800b4 && PY_VERSION_HEX < 0x03090000
  0, /*tp_print*/
  #endif
};

static PyMethodDef __pyx_methods[] = {
  {0, 0, 0, 0}
};

#if PY_MAJOR_VERSION >= 3
#if CYTHON_PEP489_MULTI_PHASE_INIT
static PyObject* __pyx_pymod_create(PyObject *spec, PyModuleDef *def); /*proto*/
static int __pyx_pymod_exec_feedback_manager(PyObject* module); /*proto*/
static PyModuleDef_Slot __pyx_moduledef_slots[] = {
  {Py_mod_create, (void*)__pyx_pymod_create},
  {Py_mod_exec, (void*)__pyx_pymod_exec_feedback_manager},
  {0, NULL}
};
#endif

static struct PyModuleDef __pyx_moduledef = {
    PyModuleDef_HEAD_INIT,
    "feedback_manager",
    __pyx_k_Info_Oimi_robot_fuzzy_controller, /* m_doc */
  #if CYTHON_PEP489_MULTI_PHASE_INIT
    0, /* m_size */
  #else
    -1, /* m_size */
  #endif
    __pyx_methods /* m_methods */,
  #if CYTHON_PEP489_MULTI_PHASE_INIT
    __pyx_moduledef_slots, /* m_slots */
  #else
    NULL, /* m_reload */
  #endif
    NULL, /* m_traverse */
    NULL, /* m_clear */
    NULL /* m_free */
};
#endif
#ifndef CYTHON_SMALL_CODE
#if defined(__clang__)
    #define CYTHON_SMALL_CODE
#elif defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 3))
    #define CYTHON_SMALL_CODE __attribute__((cold))
#else
    #define CYTHON_SMALL_CODE
#endif
#endif

static __Pyx_StringTabEntry __pyx_string_tab[] = {
  {&__pyx_kp_u_All_feedbacks_were_created_succe, __pyx_k_All_feedbacks_were_created_succe, sizeof(__pyx_k_All_feedbacks_were_created_succe), 0, 1, 0, 0},
  {&__pyx_kp_u_Creating_Feeds, __pyx_k_Creating_Feeds, sizeof(__pyx_k_Creating_Feeds), 0, 1, 0, 0},
  {&__pyx_n_s_DatabaseManager, __pyx_k_DatabaseManager, sizeof(__pyx_k_DatabaseManager), 0, 0, 1, 1},
  {&__pyx_n_s_Feedback, __pyx_k_Feedback, sizeof(__pyx_k_Feedback), 0, 0, 1, 1},
  {&__pyx_n_s_Optional, __pyx_k_Optional, sizeof(__pyx_k_Optional), 0, 0, 1, 1},
  {&__pyx_kp_u_SELECT_session_id_FROM_Kids_Sess, __pyx_k_SELECT_session_id_FROM_Kids_Sess, sizeof(__pyx_k_SELECT_session_id_FROM_Kids_Sess), 0, 1, 0, 0},
  {&__pyx_n_s_TypeError, __pyx_k_TypeError, sizeof(__pyx_k_TypeError), 0, 0, 1, 1},
  {&__pyx_kp_u_VEDIAMO_SE_LE_HA_MODIFICATE_GIUS, __pyx_k_VEDIAMO_SE_LE_HA_MODIFICATE_GIUS, sizeof(__pyx_k_VEDIAMO_SE_LE_HA_MODIFICATE_GIUS), 0, 1, 0, 0},
  {&__pyx_n_s_append, __pyx_k_append, sizeof(__pyx_k_append), 0, 0, 1, 1},
  {&__pyx_n_s_cline_in_traceback, __pyx_k_cline_in_traceback, sizeof(__pyx_k_cline_in_traceback), 0, 0, 1, 1},
  {&__pyx_n_s_database_manager, __pyx_k_database_manager, sizeof(__pyx_k_database_manager), 0, 0, 1, 1},
  {&__pyx_n_s_db, __pyx_k_db, sizeof(__pyx_k_db), 0, 0, 1, 1},
  {&__pyx_n_s_evaluate_whole_feedbacks, __pyx_k_evaluate_whole_feedbacks, sizeof(__pyx_k_evaluate_whole_feedbacks), 0, 0, 1, 1},
  {&__pyx_n_s_exc_tb, __pyx_k_exc_tb, sizeof(__pyx_k_exc_tb), 0, 0, 1, 1},
  {&__pyx_n_s_exc_type, __pyx_k_exc_type, sizeof(__pyx_k_exc_type), 0, 0, 1, 1},
  {&__pyx_n_s_exc_val, __pyx_k_exc_val, sizeof(__pyx_k_exc_val), 0, 0, 1, 1},
  {&__pyx_n_s_execute_new_query, __pyx_k_execute_new_query, sizeof(__pyx_k_execute_new_query), 0, 0, 1, 1},
  {&__pyx_n_s_feedbacks_insertion, __pyx_k_feedbacks_insertion, sizeof(__pyx_k_feedbacks_insertion), 0, 0, 1, 1},
  {&__pyx_kp_u_games_feeds, __pyx_k_games_feeds, sizeof(__pyx_k_games_feeds), 0, 1, 0, 0},
  {&__pyx_n_s_getstate, __pyx_k_getstate, sizeof(__pyx_k_getstate), 0, 0, 1, 1},
  {&__pyx_n_s_grade_feedback_games, __pyx_k_grade_feedback_games, sizeof(__pyx_k_grade_feedback_games), 0, 0, 1, 1},
  {&__pyx_n_s_grade_feedback_matches, __pyx_k_grade_feedback_matches, sizeof(__pyx_k_grade_feedback_matches), 0, 0, 1, 1},
  {&__pyx_n_s_grade_feedback_questions, __pyx_k_grade_feedback_questions, sizeof(__pyx_k_grade_feedback_questions), 0, 0, 1, 1},
  {&__pyx_n_s_grade_feedback_session, __pyx_k_grade_feedback_session, sizeof(__pyx_k_grade_feedback_session), 0, 0, 1, 1},
  {&__pyx_kp_u_home_notto4_Desktop_thesis_offi, __pyx_k_home_notto4_Desktop_thesis_offi, sizeof(__pyx_k_home_notto4_Desktop_thesis_offi), 0, 1, 0, 0},
  {&__pyx_kp_u_home_notto4_Desktop_thesis_offi_2, __pyx_k_home_notto4_Desktop_thesis_offi_2, sizeof(__pyx_k_home_notto4_Desktop_thesis_offi_2), 0, 1, 0, 0},
  {&__pyx_n_s_import, __pyx_k_import, sizeof(__pyx_k_import), 0, 0, 1, 1},
  {&__pyx_n_s_include_all_feeds_in_once_given, __pyx_k_include_all_feeds_in_once_given, sizeof(__pyx_k_include_all_feeds_in_once_given), 0, 0, 1, 1},
  {&__pyx_n_s_main, __pyx_k_main, sizeof(__pyx_k_main), 0, 0, 1, 1},
  {&__pyx_kp_u_matches_feeds, __pyx_k_matches_feeds, sizeof(__pyx_k_matches_feeds), 0, 1, 0, 0},
  {&__pyx_n_s_name, __pyx_k_name, sizeof(__pyx_k_name), 0, 0, 1, 1},
  {&__pyx_kp_s_no_default___reduce___due_to_non, __pyx_k_no_default___reduce___due_to_non, sizeof(__pyx_k_no_default___reduce___due_to_non), 0, 0, 1, 0},
  {&__pyx_n_s_os, __pyx_k_os, sizeof(__pyx_k_os), 0, 0, 1, 1},
  {&__pyx_n_s_path, __pyx_k_path, sizeof(__pyx_k_path), 0, 0, 1, 1},
  {&__pyx_n_s_print, __pyx_k_print, sizeof(__pyx_k_print), 0, 0, 1, 1},
  {&__pyx_n_s_pyx_vtable, __pyx_k_pyx_vtable, sizeof(__pyx_k_pyx_vtable), 0, 0, 1, 1},
  {&__pyx_n_s_quantity_feedback_games, __pyx_k_quantity_feedback_games, sizeof(__pyx_k_quantity_feedback_games), 0, 0, 1, 1},
  {&__pyx_n_s_quantity_feedback_matches, __pyx_k_quantity_feedback_matches, sizeof(__pyx_k_quantity_feedback_matches), 0, 0, 1, 1},
  {&__pyx_n_s_quantity_feedback_questions, __pyx_k_quantity_feedback_questions, sizeof(__pyx_k_quantity_feedback_questions), 0, 0, 1, 1},
  {&__pyx_n_s_quantity_feedback_session, __pyx_k_quantity_feedback_session, sizeof(__pyx_k_quantity_feedback_session), 0, 0, 1, 1},
  {&__pyx_kp_u_questions_feeds, __pyx_k_questions_feeds, sizeof(__pyx_k_questions_feeds), 0, 1, 0, 0},
  {&__pyx_n_s_reduce, __pyx_k_reduce, sizeof(__pyx_k_reduce), 0, 0, 1, 1},
  {&__pyx_n_s_reduce_cython, __pyx_k_reduce_cython, sizeof(__pyx_k_reduce_cython), 0, 0, 1, 1},
  {&__pyx_n_s_reduce_ex, __pyx_k_reduce_ex, sizeof(__pyx_k_reduce_ex), 0, 0, 1, 1},
  {&__pyx_n_s_setstate, __pyx_k_setstate, sizeof(__pyx_k_setstate), 0, 0, 1, 1},
  {&__pyx_n_s_setstate_cython, __pyx_k_setstate_cython, sizeof(__pyx_k_setstate_cython), 0, 0, 1, 1},
  {&__pyx_n_s_sys, __pyx_k_sys, sizeof(__pyx_k_sys), 0, 0, 1, 1},
  {&__pyx_n_s_takeout_feeds_game, __pyx_k_takeout_feeds_game, sizeof(__pyx_k_takeout_feeds_game), 0, 0, 1, 1},
  {&__pyx_n_s_takeout_feeds_match, __pyx_k_takeout_feeds_match, sizeof(__pyx_k_takeout_feeds_match), 0, 0, 1, 1},
  {&__pyx_n_s_takeout_feeds_question, __pyx_k_takeout_feeds_question, sizeof(__pyx_k_takeout_feeds_question), 0, 0, 1, 1},
  {&__pyx_n_s_takeout_feeds_session, __pyx_k_takeout_feeds_session, sizeof(__pyx_k_takeout_feeds_session), 0, 0, 1, 1},
  {&__pyx_n_s_test, __pyx_k_test, sizeof(__pyx_k_test), 0, 0, 1, 1},
  {&__pyx_kp_u_the_session_feeds, __pyx_k_the_session_feeds, sizeof(__pyx_k_the_session_feeds), 0, 1, 0, 0},
  {&__pyx_n_s_typing, __pyx_k_typing, sizeof(__pyx_k_typing), 0, 0, 1, 1},
  {&__pyx_n_s_value_feedback_games, __pyx_k_value_feedback_games, sizeof(__pyx_k_value_feedback_games), 0, 0, 1, 1},
  {&__pyx_n_s_value_feedback_matches, __pyx_k_value_feedback_matches, sizeof(__pyx_k_value_feedback_matches), 0, 0, 1, 1},
  {&__pyx_n_s_value_feedback_questions, __pyx_k_value_feedback_questions, sizeof(__pyx_k_value_feedback_questions), 0, 0, 1, 1},
  {&__pyx_n_s_value_feedback_session, __pyx_k_value_feedback_session, sizeof(__pyx_k_value_feedback_session), 0, 0, 1, 1},
  {0, 0, 0, 0, 0, 0, 0}
};
static CYTHON_SMALL_CODE int __Pyx_InitCachedBuiltins(void) {
  __pyx_builtin_print = __Pyx_GetBuiltinName(__pyx_n_s_print); if (!__pyx_builtin_print) __PYX_ERR(0, 78, __pyx_L1_error)
  __pyx_builtin_TypeError = __Pyx_GetBuiltinName(__pyx_n_s_TypeError); if (!__pyx_builtin_TypeError) __PYX_ERR(1, 2, __pyx_L1_error)
  return 0;
  __pyx_L1_error:;
  return -1;
}

static CYTHON_SMALL_CODE int __Pyx_InitCachedConstants(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_InitCachedConstants", 0);

  /* "feedback_manager.pyx":78
 * 		#self.user_dict_sess = {"task_preli": self.preliminary_activities_session,"task_begin_play":self.go_to_match,"task_finals":self.final_activities_session,"task_update_child":self.update_kid_stats} #for fsm!
 * 	def __enter__(self):
 * 		print("Creating Feeds...")             # <<<<<<<<<<<<<<
 * 
 * 	def __exit__(self, exc_type, exc_val, exc_tb):
 */
  __pyx_tuple_ = PyTuple_Pack(1, __pyx_kp_u_Creating_Feeds); if (unlikely(!__pyx_tuple_)) __PYX_ERR(0, 78, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_tuple_);
  __Pyx_GIVEREF(__pyx_tuple_);

  /* "feedback_manager.pyx":81
 * 
 * 	def __exit__(self, exc_type, exc_val, exc_tb):
 * 		print("All feedbacks were created successfully.")             # <<<<<<<<<<<<<<
 * 
 * 
 */
  __pyx_tuple__2 = PyTuple_Pack(1, __pyx_kp_u_All_feedbacks_were_created_succe); if (unlikely(!__pyx_tuple__2)) __PYX_ERR(0, 81, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_tuple__2);
  __Pyx_GIVEREF(__pyx_tuple__2);

  /* "feedback_manager.pyx":434
 * 		this_session = self.db_conn.execute_new_query(s_sql,which_kid)
 * 		session_id = this_session[0]
 * 		print("VEDIAMO SE LE HA MODIFICATE GIUSTE.....")             # <<<<<<<<<<<<<<
 * 		print("questions_feeds ", self.questions_feeds)
 * 		print("the_session_feeds ", self.the_session_feeds)
 */
  __pyx_tuple__3 = PyTuple_Pack(1, __pyx_kp_u_VEDIAMO_SE_LE_HA_MODIFICATE_GIUS); if (unlikely(!__pyx_tuple__3)) __PYX_ERR(0, 434, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_tuple__3);
  __Pyx_GIVEREF(__pyx_tuple__3);

  /* "(tree fragment)":2
 * def __reduce_cython__(self):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")             # <<<<<<<<<<<<<<
 * def __setstate_cython__(self, __pyx_state):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 */
  __pyx_tuple__4 = PyTuple_Pack(1, __pyx_kp_s_no_default___reduce___due_to_non); if (unlikely(!__pyx_tuple__4)) __PYX_ERR(1, 2, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_tuple__4);
  __Pyx_GIVEREF(__pyx_tuple__4);

  /* "(tree fragment)":4
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")             # <<<<<<<<<<<<<<
 */
  __pyx_tuple__5 = PyTuple_Pack(1, __pyx_kp_s_no_default___reduce___due_to_non); if (unlikely(!__pyx_tuple__5)) __PYX_ERR(1, 4, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_tuple__5);
  __Pyx_GIVEREF(__pyx_tuple__5);
  __Pyx_RefNannyFinishContext();
  return 0;
  __pyx_L1_error:;
  __Pyx_RefNannyFinishContext();
  return -1;
}

static CYTHON_SMALL_CODE int __Pyx_InitGlobals(void) {
  if (__Pyx_InitStrings(__pyx_string_tab) < 0) __PYX_ERR(0, 1, __pyx_L1_error);
  return 0;
  __pyx_L1_error:;
  return -1;
}

static CYTHON_SMALL_CODE int __Pyx_modinit_global_init_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_variable_export_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_function_export_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_type_init_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_type_import_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_variable_import_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_function_import_code(void); /*proto*/

static int __Pyx_modinit_global_init_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_global_init_code", 0);
  /*--- Global init code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}

static int __Pyx_modinit_variable_export_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_variable_export_code", 0);
  /*--- Variable export code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}

static int __Pyx_modinit_function_export_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_function_export_code", 0);
  /*--- Function export code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}

static int __Pyx_modinit_type_init_code(void) {
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__Pyx_modinit_type_init_code", 0);
  /*--- Type init code ---*/
  __pyx_vtabptr_16feedback_manager_Feedback = &__pyx_vtable_16feedback_manager_Feedback;
  __pyx_vtable_16feedback_manager_Feedback.takeout_feeds_question = (PyObject *(*)(struct __pyx_obj_16feedback_manager_Feedback *, int, int __pyx_skip_dispatch))__pyx_f_16feedback_manager_8Feedback_takeout_feeds_question;
  __pyx_vtable_16feedback_manager_Feedback.takeout_feeds_game = (PyObject *(*)(struct __pyx_obj_16feedback_manager_Feedback *, int, int __pyx_skip_dispatch))__pyx_f_16feedback_manager_8Feedback_takeout_feeds_game;
  __pyx_vtable_16feedback_manager_Feedback.takeout_feeds_match = (PyObject *(*)(struct __pyx_obj_16feedback_manager_Feedback *, int, int __pyx_skip_dispatch))__pyx_f_16feedback_manager_8Feedback_takeout_feeds_match;
  __pyx_vtable_16feedback_manager_Feedback.takeout_feeds_session = (PyObject *(*)(struct __pyx_obj_16feedback_manager_Feedback *, int, int __pyx_skip_dispatch))__pyx_f_16feedback_manager_8Feedback_takeout_feeds_session;
  __pyx_vtable_16feedback_manager_Feedback.evaluate_whole_feedbacks = (PyObject *(*)(struct __pyx_obj_16feedback_manager_Feedback *, int __pyx_skip_dispatch))__pyx_f_16feedback_manager_8Feedback_evaluate_whole_feedbacks;
  __pyx_vtable_16feedback_manager_Feedback.feedbacks_insertion = (PyObject *(*)(struct __pyx_obj_16feedback_manager_Feedback *, int, int __pyx_skip_dispatch))__pyx_f_16feedback_manager_8Feedback_feedbacks_insertion;
  if (PyType_Ready(&__pyx_type_16feedback_manager_Feedback) < 0) __PYX_ERR(0, 42, __pyx_L1_error)
  #if PY_VERSION_HEX < 0x030800B1
  __pyx_type_16feedback_manager_Feedback.tp_print = 0;
  #endif
  if ((CYTHON_USE_TYPE_SLOTS && CYTHON_USE_PYTYPE_LOOKUP) && likely(!__pyx_type_16feedback_manager_Feedback.tp_dictoffset && __pyx_type_16feedback_manager_Feedback.tp_getattro == PyObject_GenericGetAttr)) {
    __pyx_type_16feedback_manager_Feedback.tp_getattro = __Pyx_PyObject_GenericGetAttr;
  }
  if (__Pyx_SetVtable(__pyx_type_16feedback_manager_Feedback.tp_dict, __pyx_vtabptr_16feedback_manager_Feedback) < 0) __PYX_ERR(0, 42, __pyx_L1_error)
  if (PyObject_SetAttr(__pyx_m, __pyx_n_s_Feedback, (PyObject *)&__pyx_type_16feedback_manager_Feedback) < 0) __PYX_ERR(0, 42, __pyx_L1_error)
  if (__Pyx_setup_reduce((PyObject*)&__pyx_type_16feedback_manager_Feedback) < 0) __PYX_ERR(0, 42, __pyx_L1_error)
  __pyx_ptype_16feedback_manager_Feedback = &__pyx_type_16feedback_manager_Feedback;
  __Pyx_RefNannyFinishContext();
  return 0;
  __pyx_L1_error:;
  __Pyx_RefNannyFinishContext();
  return -1;
}

static int __Pyx_modinit_type_import_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_type_import_code", 0);
  /*--- Type import code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}

static int __Pyx_modinit_variable_import_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_variable_import_code", 0);
  /*--- Variable import code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}

static int __Pyx_modinit_function_import_code(void) {
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__Pyx_modinit_function_import_code", 0);
  /*--- Function import code ---*/
  __pyx_t_1 = PyImport_ImportModule("fuzzy_controller_takeout"); if (!__pyx_t_1) __PYX_ERR(0, 1, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (__Pyx_ImportFunction(__pyx_t_1, "takeout_feeds_from_db_question", (void (**)(void))&__pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_question, "PyObject *(PyObject *, int)") < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  if (__Pyx_ImportFunction(__pyx_t_1, "takeout_feeds_from_db_game", (void (**)(void))&__pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_game, "PyObject *(PyObject *, int)") < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  if (__Pyx_ImportFunction(__pyx_t_1, "takeout_feeds_from_db_match", (void (**)(void))&__pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_match, "PyObject *(PyObject *, int)") < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  if (__Pyx_ImportFunction(__pyx_t_1, "takeout_feeds_from_db_session", (void (**)(void))&__pyx_f_24fuzzy_controller_takeout_takeout_feeds_from_db_session, "PyObject *(PyObject *, int)") < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_1 = PyImport_ImportModule("fuzzy_controller_evaluate_feedbacks_whole_session"); if (!__pyx_t_1) __PYX_ERR(0, 1, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (__Pyx_ImportFunction(__pyx_t_1, "evaluate_feedbacks_whole_session", (void (**)(void))&__pyx_f_49fuzzy_controller_evaluate_feedbacks_whole_session_evaluate_feedbacks_whole_session, "PyObject *(PyObject *, int __pyx_skip_dispatch)") < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_RefNannyFinishContext();
  return 0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_RefNannyFinishContext();
  return -1;
}


#ifndef CYTHON_NO_PYINIT_EXPORT
#define __Pyx_PyMODINIT_FUNC PyMODINIT_FUNC
#elif PY_MAJOR_VERSION < 3
#ifdef __cplusplus
#define __Pyx_PyMODINIT_FUNC extern "C" void
#else
#define __Pyx_PyMODINIT_FUNC void
#endif
#else
#ifdef __cplusplus
#define __Pyx_PyMODINIT_FUNC extern "C" PyObject *
#else
#define __Pyx_PyMODINIT_FUNC PyObject *
#endif
#endif


#if PY_MAJOR_VERSION < 3
__Pyx_PyMODINIT_FUNC initfeedback_manager(void) CYTHON_SMALL_CODE; /*proto*/
__Pyx_PyMODINIT_FUNC initfeedback_manager(void)
#else
__Pyx_PyMODINIT_FUNC PyInit_feedback_manager(void) CYTHON_SMALL_CODE; /*proto*/
__Pyx_PyMODINIT_FUNC PyInit_feedback_manager(void)
#if CYTHON_PEP489_MULTI_PHASE_INIT
{
  return PyModuleDef_Init(&__pyx_moduledef);
}
static CYTHON_SMALL_CODE int __Pyx_check_single_interpreter(void) {
    #if PY_VERSION_HEX >= 0x030700A1
    static PY_INT64_T main_interpreter_id = -1;
    PY_INT64_T current_id = PyInterpreterState_GetID(PyThreadState_Get()->interp);
    if (main_interpreter_id == -1) {
        main_interpreter_id = current_id;
        return (unlikely(current_id == -1)) ? -1 : 0;
    } else if (unlikely(main_interpreter_id != current_id))
    #else
    static PyInterpreterState *main_interpreter = NULL;
    PyInterpreterState *current_interpreter = PyThreadState_Get()->interp;
    if (!main_interpreter) {
        main_interpreter = current_interpreter;
    } else if (unlikely(main_interpreter != current_interpreter))
    #endif
    {
        PyErr_SetString(
            PyExc_ImportError,
            "Interpreter change detected - this module can only be loaded into one interpreter per process.");
        return -1;
    }
    return 0;
}
static CYTHON_SMALL_CODE int __Pyx_copy_spec_to_module(PyObject *spec, PyObject *moddict, const char* from_name, const char* to_name, int allow_none) {
    PyObject *value = PyObject_GetAttrString(spec, from_name);
    int result = 0;
    if (likely(value)) {
        if (allow_none || value != Py_None) {
            result = PyDict_SetItemString(moddict, to_name, value);
        }
        Py_DECREF(value);
    } else if (PyErr_ExceptionMatches(PyExc_AttributeError)) {
        PyErr_Clear();
    } else {
        result = -1;
    }
    return result;
}
static CYTHON_SMALL_CODE PyObject* __pyx_pymod_create(PyObject *spec, CYTHON_UNUSED PyModuleDef *def) {
    PyObject *module = NULL, *moddict, *modname;
    if (__Pyx_check_single_interpreter())
        return NULL;
    if (__pyx_m)
        return __Pyx_NewRef(__pyx_m);
    modname = PyObject_GetAttrString(spec, "name");
    if (unlikely(!modname)) goto bad;
    module = PyModule_NewObject(modname);
    Py_DECREF(modname);
    if (unlikely(!module)) goto bad;
    moddict = PyModule_GetDict(module);
    if (unlikely(!moddict)) goto bad;
    if (unlikely(__Pyx_copy_spec_to_module(spec, moddict, "loader", "__loader__", 1) < 0)) goto bad;
    if (unlikely(__Pyx_copy_spec_to_module(spec, moddict, "origin", "__file__", 1) < 0)) goto bad;
    if (unlikely(__Pyx_copy_spec_to_module(spec, moddict, "parent", "__package__", 1) < 0)) goto bad;
    if (unlikely(__Pyx_copy_spec_to_module(spec, moddict, "submodule_search_locations", "__path__", 0) < 0)) goto bad;
    return module;
bad:
    Py_XDECREF(module);
    return NULL;
}


static CYTHON_SMALL_CODE int __pyx_pymod_exec_feedback_manager(PyObject *__pyx_pyinit_module)
#endif
#endif
{
  __Pyx_TraceDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  int __pyx_t_3;
  int __pyx_t_4;
  int __pyx_t_5;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannyDeclarations
  #if CYTHON_PEP489_MULTI_PHASE_INIT
  if (__pyx_m) {
    if (__pyx_m == __pyx_pyinit_module) return 0;
    PyErr_SetString(PyExc_RuntimeError, "Module 'feedback_manager' has already been imported. Re-initialisation is not supported.");
    return -1;
  }
  #elif PY_MAJOR_VERSION >= 3
  if (__pyx_m) return __Pyx_NewRef(__pyx_m);
  #endif
  #if CYTHON_REFNANNY
__Pyx_RefNanny = __Pyx_RefNannyImportAPI("refnanny");
if (!__Pyx_RefNanny) {
  PyErr_Clear();
  __Pyx_RefNanny = __Pyx_RefNannyImportAPI("Cython.Runtime.refnanny");
  if (!__Pyx_RefNanny)
      Py_FatalError("failed to import 'refnanny' module");
}
#endif
  __Pyx_RefNannySetupContext("__Pyx_PyMODINIT_FUNC PyInit_feedback_manager(void)", 0);
  if (__Pyx_check_binary_version() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #ifdef __Pxy_PyFrame_Initialize_Offsets
  __Pxy_PyFrame_Initialize_Offsets();
  #endif
  __pyx_empty_tuple = PyTuple_New(0); if (unlikely(!__pyx_empty_tuple)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_empty_bytes = PyBytes_FromStringAndSize("", 0); if (unlikely(!__pyx_empty_bytes)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_empty_unicode = PyUnicode_FromStringAndSize("", 0); if (unlikely(!__pyx_empty_unicode)) __PYX_ERR(0, 1, __pyx_L1_error)
  #ifdef __Pyx_CyFunction_USED
  if (__pyx_CyFunction_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  #ifdef __Pyx_FusedFunction_USED
  if (__pyx_FusedFunction_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  #ifdef __Pyx_Coroutine_USED
  if (__pyx_Coroutine_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  #ifdef __Pyx_Generator_USED
  if (__pyx_Generator_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  #ifdef __Pyx_AsyncGen_USED
  if (__pyx_AsyncGen_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  #ifdef __Pyx_StopAsyncIteration_USED
  if (__pyx_StopAsyncIteration_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  /*--- Library function declarations ---*/
  /*--- Threads initialization code ---*/
  #if defined(WITH_THREAD) && PY_VERSION_HEX < 0x030700F0 && defined(__PYX_FORCE_INIT_THREADS) && __PYX_FORCE_INIT_THREADS
  PyEval_InitThreads();
  #endif
  /*--- Module creation code ---*/
  #if CYTHON_PEP489_MULTI_PHASE_INIT
  __pyx_m = __pyx_pyinit_module;
  Py_INCREF(__pyx_m);
  #else
  #if PY_MAJOR_VERSION < 3
  __pyx_m = Py_InitModule4("feedback_manager", __pyx_methods, __pyx_k_Info_Oimi_robot_fuzzy_controller, 0, PYTHON_API_VERSION); Py_XINCREF(__pyx_m);
  #else
  __pyx_m = PyModule_Create(&__pyx_moduledef);
  #endif
  if (unlikely(!__pyx_m)) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  __pyx_d = PyModule_GetDict(__pyx_m); if (unlikely(!__pyx_d)) __PYX_ERR(0, 1, __pyx_L1_error)
  Py_INCREF(__pyx_d);
  __pyx_b = PyImport_AddModule(__Pyx_BUILTIN_MODULE_NAME); if (unlikely(!__pyx_b)) __PYX_ERR(0, 1, __pyx_L1_error)
  Py_INCREF(__pyx_b);
  __pyx_cython_runtime = PyImport_AddModule((char *) "cython_runtime"); if (unlikely(!__pyx_cython_runtime)) __PYX_ERR(0, 1, __pyx_L1_error)
  Py_INCREF(__pyx_cython_runtime);
  if (PyObject_SetAttrString(__pyx_m, "__builtins__", __pyx_b) < 0) __PYX_ERR(0, 1, __pyx_L1_error);
  /*--- Initialize various global constants etc. ---*/
  if (__Pyx_InitGlobals() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #if PY_MAJOR_VERSION < 3 && (__PYX_DEFAULT_STRING_ENCODING_IS_ASCII || __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT)
  if (__Pyx_init_sys_getdefaultencoding_params() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  if (__pyx_module_is_main_feedback_manager) {
    if (PyObject_SetAttr(__pyx_m, __pyx_n_s_name, __pyx_n_s_main) < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  }
  #if PY_MAJOR_VERSION >= 3
  {
    PyObject *modules = PyImport_GetModuleDict(); if (unlikely(!modules)) __PYX_ERR(0, 1, __pyx_L1_error)
    if (!PyDict_GetItemString(modules, "feedback_manager")) {
      if (unlikely(PyDict_SetItemString(modules, "feedback_manager", __pyx_m) < 0)) __PYX_ERR(0, 1, __pyx_L1_error)
    }
  }
  #endif
  /*--- Builtin init code ---*/
  if (__Pyx_InitCachedBuiltins() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  /*--- Constants init code ---*/
  if (__Pyx_InitCachedConstants() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  /*--- Global type/function init code ---*/
  (void)__Pyx_modinit_global_init_code();
  (void)__Pyx_modinit_variable_export_code();
  (void)__Pyx_modinit_function_export_code();
  if (unlikely(__Pyx_modinit_type_init_code() < 0)) __PYX_ERR(0, 1, __pyx_L1_error)
  (void)__Pyx_modinit_type_import_code();
  (void)__Pyx_modinit_variable_import_code();
  if (unlikely(__Pyx_modinit_function_import_code() < 0)) __PYX_ERR(0, 1, __pyx_L1_error)
  /*--- Execution code ---*/
  #if defined(__Pyx_Generator_USED) || defined(__Pyx_Coroutine_USED)
  if (__Pyx_patch_abc() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  __Pyx_TraceCall("__Pyx_PyMODINIT_FUNC PyInit_feedback_manager(void)", __pyx_f[0], 1, 0, __PYX_ERR(0, 1, __pyx_L1_error));

  /* "feedback_manager.pyx":21
 * # Imports
 * # ==========================================================================================================================================================
 * import sys             # <<<<<<<<<<<<<<
 * import os
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/' not in sys.path:
 */
  __Pyx_TraceLine(21,0,__PYX_ERR(0, 21, __pyx_L1_error))
  __pyx_t_1 = __Pyx_Import(__pyx_n_s_sys, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 21, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_sys, __pyx_t_1) < 0) __PYX_ERR(0, 21, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":22
 * # ==========================================================================================================================================================
 * import sys
 * import os             # <<<<<<<<<<<<<<
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/' not in sys.path:
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/')
 */
  __Pyx_TraceLine(22,0,__PYX_ERR(0, 22, __pyx_L1_error))
  __pyx_t_1 = __Pyx_Import(__pyx_n_s_os, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 22, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_os, __pyx_t_1) < 0) __PYX_ERR(0, 22, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "feedback_manager.pyx":23
 * import sys
 * import os
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/' not in sys.path:             # <<<<<<<<<<<<<<
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/')
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/' not in sys.path:
 */
  __Pyx_TraceLine(23,0,__PYX_ERR(0, 23, __pyx_L1_error))
  __Pyx_GetModuleGlobalName(__pyx_t_1, __pyx_n_s_sys); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 23, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_t_1, __pyx_n_s_path); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 23, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_3 = (__Pyx_PySequence_ContainsTF(__pyx_kp_u_home_notto4_Desktop_thesis_offi, __pyx_t_2, Py_NE)); if (unlikely(__pyx_t_3 < 0)) __PYX_ERR(0, 23, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_4 = (__pyx_t_3 != 0);
  if (__pyx_t_4) {

    /* "feedback_manager.pyx":24
 * import os
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/' not in sys.path:
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/')             # <<<<<<<<<<<<<<
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/' not in sys.path:
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/')
 */
    __Pyx_TraceLine(24,0,__PYX_ERR(0, 24, __pyx_L1_error))
    __Pyx_GetModuleGlobalName(__pyx_t_2, __pyx_n_s_sys); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 24, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_path); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 24, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_5 = __Pyx_PyObject_Append(__pyx_t_1, __pyx_kp_u_home_notto4_Desktop_thesis_offi); if (unlikely(__pyx_t_5 == ((int)-1))) __PYX_ERR(0, 24, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

    /* "feedback_manager.pyx":23
 * import sys
 * import os
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/' not in sys.path:             # <<<<<<<<<<<<<<
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/')
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/' not in sys.path:
 */
  }

  /* "feedback_manager.pyx":25
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/' not in sys.path:
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/')
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/' not in sys.path:             # <<<<<<<<<<<<<<
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/')
 * 
 */
  __Pyx_TraceLine(25,0,__PYX_ERR(0, 25, __pyx_L1_error))
  __Pyx_GetModuleGlobalName(__pyx_t_1, __pyx_n_s_sys); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 25, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_t_1, __pyx_n_s_path); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 25, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_4 = (__Pyx_PySequence_ContainsTF(__pyx_kp_u_home_notto4_Desktop_thesis_offi_2, __pyx_t_2, Py_NE)); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 25, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_3 = (__pyx_t_4 != 0);
  if (__pyx_t_3) {

    /* "feedback_manager.pyx":26
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/')
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/' not in sys.path:
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/')             # <<<<<<<<<<<<<<
 * 
 * #sys.path.insert(1, os.path.join(sys.path[0], '.'))
 */
    __Pyx_TraceLine(26,0,__PYX_ERR(0, 26, __pyx_L1_error))
    __Pyx_GetModuleGlobalName(__pyx_t_2, __pyx_n_s_sys); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 26, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_path); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 26, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_5 = __Pyx_PyObject_Append(__pyx_t_1, __pyx_kp_u_home_notto4_Desktop_thesis_offi_2); if (unlikely(__pyx_t_5 == ((int)-1))) __PYX_ERR(0, 26, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

    /* "feedback_manager.pyx":25
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/' not in sys.path:
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/database_manager/')
 * if '/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/' not in sys.path:             # <<<<<<<<<<<<<<
 * 	sys.path.append('/home/notto4/Desktop/thesis_official/oimi_robot/src/managers/feedback_manager/')
 * 
 */
  }

  /* "feedback_manager.pyx":29
 * 
 * #sys.path.insert(1, os.path.join(sys.path[0], '.'))
 * from typing import Optional             # <<<<<<<<<<<<<<
 * 
 * import database_manager as db
 */
  __Pyx_TraceLine(29,0,__PYX_ERR(0, 29, __pyx_L1_error))
  __pyx_t_1 = PyList_New(1); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 29, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_INCREF(__pyx_n_s_Optional);
  __Pyx_GIVEREF(__pyx_n_s_Optional);
  PyList_SET_ITEM(__pyx_t_1, 0, __pyx_n_s_Optional);
  __pyx_t_2 = __Pyx_Import(__pyx_n_s_typing, __pyx_t_1, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 29, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_1 = __Pyx_ImportFrom(__pyx_t_2, __pyx_n_s_Optional); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 29, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_Optional, __pyx_t_1) < 0) __PYX_ERR(0, 29, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

  /* "feedback_manager.pyx":31
 * from typing import Optional
 * 
 * import database_manager as db             # <<<<<<<<<<<<<<
 * 
 * cimport fuzzy_controller_takeout as fct
 */
  __Pyx_TraceLine(31,0,__PYX_ERR(0, 31, __pyx_L1_error))
  __pyx_t_2 = __Pyx_Import(__pyx_n_s_database_manager, 0, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 31, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_db, __pyx_t_2) < 0) __PYX_ERR(0, 31, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

  /* "feedback_manager.pyx":415
 * 	#cpdef takeout_feeds_question(self, int which_kid):
 * 	#	print("uccc")
 * 	cpdef takeout_feeds_question(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_question(self,which_kid)
 * 
 */
  __Pyx_TraceLine(415,0,__PYX_ERR(0, 415, __pyx_L1_error))


  /* "feedback_manager.pyx":418
 * 		fct.takeout_feeds_from_db_question(self,which_kid)
 * 
 * 	cpdef takeout_feeds_game(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_game(self,which_kid)
 * 	cpdef takeout_feeds_match(self, int which_kid):
 */
  __Pyx_TraceLine(418,0,__PYX_ERR(0, 418, __pyx_L1_error))


  /* "feedback_manager.pyx":420
 * 	cpdef takeout_feeds_game(self, int which_kid):
 * 		fct.takeout_feeds_from_db_game(self,which_kid)
 * 	cpdef takeout_feeds_match(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_match(self,which_kid)
 * 	cpdef takeout_feeds_session(self, int which_kid):
 */
  __Pyx_TraceLine(420,0,__PYX_ERR(0, 420, __pyx_L1_error))


  /* "feedback_manager.pyx":422
 * 	cpdef takeout_feeds_match(self, int which_kid):
 * 		fct.takeout_feeds_from_db_match(self,which_kid)
 * 	cpdef takeout_feeds_session(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		fct.takeout_feeds_from_db_session(self,which_kid)
 * 
 */
  __Pyx_TraceLine(422,0,__PYX_ERR(0, 422, __pyx_L1_error))


  /* "feedback_manager.pyx":425
 * 		fct.takeout_feeds_from_db_session(self,which_kid)
 * 
 * 	cpdef evaluate_whole_feedbacks(self):             # <<<<<<<<<<<<<<
 * 		#fcefws.evaluate_feedbacks_whole_session(self)
 * 		fcefws.evaluate_feedbacks_whole_session(self)
 */
  __Pyx_TraceLine(425,0,__PYX_ERR(0, 425, __pyx_L1_error))


  /* "feedback_manager.pyx":429
 * 		fcefws.evaluate_feedbacks_whole_session(self)
 * 
 * 	cpdef feedbacks_insertion(self, int which_kid):             # <<<<<<<<<<<<<<
 * 		#self.db_conn.dcu.include_all_feeds_in_once_given_session(db_self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
 * 		s_sql = "SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1"
 */
  __Pyx_TraceLine(429,0,__PYX_ERR(0, 429, __pyx_L1_error))


  /* "feedback_manager.pyx":1
 * """Info:             # <<<<<<<<<<<<<<
 * 	Oimi robot fuzzy_controller_extract
 * 	Take data from db to evaluate ques/gam/mast/sess feedbacks
 */
  __Pyx_TraceLine(1,0,__PYX_ERR(0, 1, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyDict_NewPresized(0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 1, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_test, __pyx_t_2) < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_TraceReturn(Py_None, 0);

  /*--- Wrapped vars code ---*/

  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  if (__pyx_m) {
    if (__pyx_d) {
      __Pyx_AddTraceback("init feedback_manager", __pyx_clineno, __pyx_lineno, __pyx_filename);
    }
    Py_CLEAR(__pyx_m);
  } else if (!PyErr_Occurred()) {
    PyErr_SetString(PyExc_ImportError, "init feedback_manager");
  }
  __pyx_L0:;
  __Pyx_RefNannyFinishContext();
  #if CYTHON_PEP489_MULTI_PHASE_INIT
  return (__pyx_m != NULL) ? 0 : -1;
  #elif PY_MAJOR_VERSION >= 3
  return __pyx_m;
  #else
  return;
  #endif
}

/* --- Runtime support code --- */
/* Refnanny */
#if CYTHON_REFNANNY
static __Pyx_RefNannyAPIStruct *__Pyx_RefNannyImportAPI(const char *modname) {
    PyObject *m = NULL, *p = NULL;
    void *r = NULL;
    m = PyImport_ImportModule(modname);
    if (!m) goto end;
    p = PyObject_GetAttrString(m, "RefNannyAPI");
    if (!p) goto end;
    r = PyLong_AsVoidPtr(p);
end:
    Py_XDECREF(p);
    Py_XDECREF(m);
    return (__Pyx_RefNannyAPIStruct *)r;
}
#endif

/* PyObjectGetAttrStr */
#if CYTHON_USE_TYPE_SLOTS
static CYTHON_INLINE PyObject* __Pyx_PyObject_GetAttrStr(PyObject* obj, PyObject* attr_name) {
    PyTypeObject* tp = Py_TYPE(obj);
    if (likely(tp->tp_getattro))
        return tp->tp_getattro(obj, attr_name);
#if PY_MAJOR_VERSION < 3
    if (likely(tp->tp_getattr))
        return tp->tp_getattr(obj, PyString_AS_STRING(attr_name));
#endif
    return PyObject_GetAttr(obj, attr_name);
}
#endif

/* GetBuiltinName */
static PyObject *__Pyx_GetBuiltinName(PyObject *name) {
    PyObject* result = __Pyx_PyObject_GetAttrStr(__pyx_b, name);
    if (unlikely(!result)) {
        PyErr_Format(PyExc_NameError,
#if PY_MAJOR_VERSION >= 3
            "name '%U' is not defined", name);
#else
            "name '%.200s' is not defined", PyString_AS_STRING(name));
#endif
    }
    return result;
}

/* RaiseArgTupleInvalid */
static void __Pyx_RaiseArgtupleInvalid(
    const char* func_name,
    int exact,
    Py_ssize_t num_min,
    Py_ssize_t num_max,
    Py_ssize_t num_found)
{
    Py_ssize_t num_expected;
    const char *more_or_less;
    if (num_found < num_min) {
        num_expected = num_min;
        more_or_less = "at least";
    } else {
        num_expected = num_max;
        more_or_less = "at most";
    }
    if (exact) {
        more_or_less = "exactly";
    }
    PyErr_Format(PyExc_TypeError,
                 "%.200s() takes %.8s %" CYTHON_FORMAT_SSIZE_T "d positional argument%.1s (%" CYTHON_FORMAT_SSIZE_T "d given)",
                 func_name, more_or_less, num_expected,
                 (num_expected == 1) ? "" : "s", num_found);
}

/* KeywordStringCheck */
static int __Pyx_CheckKeywordStrings(
    PyObject *kwdict,
    const char* function_name,
    int kw_allowed)
{
    PyObject* key = 0;
    Py_ssize_t pos = 0;
#if CYTHON_COMPILING_IN_PYPY
    if (!kw_allowed && PyDict_Next(kwdict, &pos, &key, 0))
        goto invalid_keyword;
    return 1;
#else
    while (PyDict_Next(kwdict, &pos, &key, 0)) {
        #if PY_MAJOR_VERSION < 3
        if (unlikely(!PyString_Check(key)))
        #endif
            if (unlikely(!PyUnicode_Check(key)))
                goto invalid_keyword_type;
    }
    if ((!kw_allowed) && unlikely(key))
        goto invalid_keyword;
    return 1;
invalid_keyword_type:
    PyErr_Format(PyExc_TypeError,
        "%.200s() keywords must be strings", function_name);
    return 0;
#endif
invalid_keyword:
    PyErr_Format(PyExc_TypeError,
    #if PY_MAJOR_VERSION < 3
        "%.200s() got an unexpected keyword argument '%.200s'",
        function_name, PyString_AsString(key));
    #else
        "%s() got an unexpected keyword argument '%U'",
        function_name, key);
    #endif
    return 0;
}

/* PyErrFetchRestore */
#if CYTHON_FAST_THREAD_STATE
static CYTHON_INLINE void __Pyx_ErrRestoreInState(PyThreadState *tstate, PyObject *type, PyObject *value, PyObject *tb) {
    PyObject *tmp_type, *tmp_value, *tmp_tb;
    tmp_type = tstate->curexc_type;
    tmp_value = tstate->curexc_value;
    tmp_tb = tstate->curexc_traceback;
    tstate->curexc_type = type;
    tstate->curexc_value = value;
    tstate->curexc_traceback = tb;
    Py_XDECREF(tmp_type);
    Py_XDECREF(tmp_value);
    Py_XDECREF(tmp_tb);
}
static CYTHON_INLINE void __Pyx_ErrFetchInState(PyThreadState *tstate, PyObject **type, PyObject **value, PyObject **tb) {
    *type = tstate->curexc_type;
    *value = tstate->curexc_value;
    *tb = tstate->curexc_traceback;
    tstate->curexc_type = 0;
    tstate->curexc_value = 0;
    tstate->curexc_traceback = 0;
}
#endif

/* Profile */
#if CYTHON_PROFILE
static int __Pyx_TraceSetupAndCall(PyCodeObject** code,
                                   PyFrameObject** frame,
                                   PyThreadState* tstate,
                                   const char *funcname,
                                   const char *srcfile,
                                   int firstlineno) {
    PyObject *type, *value, *traceback;
    int retval;
    if (*frame == NULL || !CYTHON_PROFILE_REUSE_FRAME) {
        if (*code == NULL) {
            *code = __Pyx_createFrameCodeObject(funcname, srcfile, firstlineno);
            if (*code == NULL) return 0;
        }
        *frame = PyFrame_New(
            tstate,                          /*PyThreadState *tstate*/
            *code,                           /*PyCodeObject *code*/
            __pyx_d,                  /*PyObject *globals*/
            0                                /*PyObject *locals*/
        );
        if (*frame == NULL) return 0;
        if (CYTHON_TRACE && (*frame)->f_trace == NULL) {
            Py_INCREF(Py_None);
            (*frame)->f_trace = Py_None;
        }
#if PY_VERSION_HEX < 0x030400B1
    } else {
        (*frame)->f_tstate = tstate;
#endif
    }
    __Pyx_PyFrame_SetLineNumber(*frame, firstlineno);
    retval = 1;
    tstate->tracing++;
    __Pyx_SetTracing(tstate, 0);
    __Pyx_ErrFetchInState(tstate, &type, &value, &traceback);
    #if CYTHON_TRACE
    if (tstate->c_tracefunc)
        retval = tstate->c_tracefunc(tstate->c_traceobj, *frame, PyTrace_CALL, NULL) == 0;
    if (retval && tstate->c_profilefunc)
    #endif
        retval = tstate->c_profilefunc(tstate->c_profileobj, *frame, PyTrace_CALL, NULL) == 0;
    __Pyx_SetTracing(tstate, (tstate->c_profilefunc || (CYTHON_TRACE && tstate->c_tracefunc)));
    tstate->tracing--;
    if (retval) {
        __Pyx_ErrRestoreInState(tstate, type, value, traceback);
        return __Pyx_IsTracing(tstate, 0, 0) && retval;
    } else {
        Py_XDECREF(type);
        Py_XDECREF(value);
        Py_XDECREF(traceback);
        return -1;
    }
}
static PyCodeObject *__Pyx_createFrameCodeObject(const char *funcname, const char *srcfile, int firstlineno) {
    PyCodeObject *py_code = 0;
#if PY_MAJOR_VERSION >= 3
    py_code = PyCode_NewEmpty(srcfile, funcname, firstlineno);
    if (likely(py_code)) {
        py_code->co_flags |= CO_OPTIMIZED | CO_NEWLOCALS;
    }
#else
    PyObject *py_srcfile = 0;
    PyObject *py_funcname = 0;
    py_funcname = PyString_FromString(funcname);
    if (unlikely(!py_funcname)) goto bad;
    py_srcfile = PyString_FromString(srcfile);
    if (unlikely(!py_srcfile)) goto bad;
    py_code = PyCode_New(
        0,
        0,
        0,
        CO_OPTIMIZED | CO_NEWLOCALS,
        __pyx_empty_bytes,     /*PyObject *code,*/
        __pyx_empty_tuple,     /*PyObject *consts,*/
        __pyx_empty_tuple,     /*PyObject *names,*/
        __pyx_empty_tuple,     /*PyObject *varnames,*/
        __pyx_empty_tuple,     /*PyObject *freevars,*/
        __pyx_empty_tuple,     /*PyObject *cellvars,*/
        py_srcfile,       /*PyObject *filename,*/
        py_funcname,      /*PyObject *name,*/
        firstlineno,
        __pyx_empty_bytes      /*PyObject *lnotab*/
    );
bad:
    Py_XDECREF(py_srcfile);
    Py_XDECREF(py_funcname);
#endif
    return py_code;
}
#endif

/* PyDictVersioning */
#if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_TYPE_SLOTS
static CYTHON_INLINE PY_UINT64_T __Pyx_get_tp_dict_version(PyObject *obj) {
    PyObject *dict = Py_TYPE(obj)->tp_dict;
    return likely(dict) ? __PYX_GET_DICT_VERSION(dict) : 0;
}
static CYTHON_INLINE PY_UINT64_T __Pyx_get_object_dict_version(PyObject *obj) {
    PyObject **dictptr = NULL;
    Py_ssize_t offset = Py_TYPE(obj)->tp_dictoffset;
    if (offset) {
#if CYTHON_COMPILING_IN_CPYTHON
        dictptr = (likely(offset > 0)) ? (PyObject **) ((char *)obj + offset) : _PyObject_GetDictPtr(obj);
#else
        dictptr = _PyObject_GetDictPtr(obj);
#endif
    }
    return (dictptr && *dictptr) ? __PYX_GET_DICT_VERSION(*dictptr) : 0;
}
static CYTHON_INLINE int __Pyx_object_dict_version_matches(PyObject* obj, PY_UINT64_T tp_dict_version, PY_UINT64_T obj_dict_version) {
    PyObject *dict = Py_TYPE(obj)->tp_dict;
    if (unlikely(!dict) || unlikely(tp_dict_version != __PYX_GET_DICT_VERSION(dict)))
        return 0;
    return obj_dict_version == __Pyx_get_object_dict_version(obj);
}
#endif

/* GetModuleGlobalName */
#if CYTHON_USE_DICT_VERSIONS
static PyObject *__Pyx__GetModuleGlobalName(PyObject *name, PY_UINT64_T *dict_version, PyObject **dict_cached_value)
#else
static CYTHON_INLINE PyObject *__Pyx__GetModuleGlobalName(PyObject *name)
#endif
{
    PyObject *result;
#if !CYTHON_AVOID_BORROWED_REFS
#if CYTHON_COMPILING_IN_CPYTHON && PY_VERSION_HEX >= 0x030500A1
    result = _PyDict_GetItem_KnownHash(__pyx_d, name, ((PyASCIIObject *) name)->hash);
    __PYX_UPDATE_DICT_CACHE(__pyx_d, result, *dict_cached_value, *dict_version)
    if (likely(result)) {
        return __Pyx_NewRef(result);
    } else if (unlikely(PyErr_Occurred())) {
        return NULL;
    }
#else
    result = PyDict_GetItem(__pyx_d, name);
    __PYX_UPDATE_DICT_CACHE(__pyx_d, result, *dict_cached_value, *dict_version)
    if (likely(result)) {
        return __Pyx_NewRef(result);
    }
#endif
#else
    result = PyObject_GetItem(__pyx_d, name);
    __PYX_UPDATE_DICT_CACHE(__pyx_d, result, *dict_cached_value, *dict_version)
    if (likely(result)) {
        return __Pyx_NewRef(result);
    }
    PyErr_Clear();
#endif
    return __Pyx_GetBuiltinName(name);
}

/* PyFunctionFastCall */
#if CYTHON_FAST_PYCALL
static PyObject* __Pyx_PyFunction_FastCallNoKw(PyCodeObject *co, PyObject **args, Py_ssize_t na,
                                               PyObject *globals) {
    PyFrameObject *f;
    PyThreadState *tstate = __Pyx_PyThreadState_Current;
    PyObject **fastlocals;
    Py_ssize_t i;
    PyObject *result;
    assert(globals != NULL);
    /* XXX Perhaps we should create a specialized
       PyFrame_New() that doesn't take locals, but does
       take builtins without sanity checking them.
       */
    assert(tstate != NULL);
    f = PyFrame_New(tstate, co, globals, NULL);
    if (f == NULL) {
        return NULL;
    }
    fastlocals = __Pyx_PyFrame_GetLocalsplus(f);
    for (i = 0; i < na; i++) {
        Py_INCREF(*args);
        fastlocals[i] = *args++;
    }
    result = PyEval_EvalFrameEx(f,0);
    ++tstate->recursion_depth;
    Py_DECREF(f);
    --tstate->recursion_depth;
    return result;
}
#if 1 || PY_VERSION_HEX < 0x030600B1
static PyObject *__Pyx_PyFunction_FastCallDict(PyObject *func, PyObject **args, Py_ssize_t nargs, PyObject *kwargs) {
    PyCodeObject *co = (PyCodeObject *)PyFunction_GET_CODE(func);
    PyObject *globals = PyFunction_GET_GLOBALS(func);
    PyObject *argdefs = PyFunction_GET_DEFAULTS(func);
    PyObject *closure;
#if PY_MAJOR_VERSION >= 3
    PyObject *kwdefs;
#endif
    PyObject *kwtuple, **k;
    PyObject **d;
    Py_ssize_t nd;
    Py_ssize_t nk;
    PyObject *result;
    assert(kwargs == NULL || PyDict_Check(kwargs));
    nk = kwargs ? PyDict_Size(kwargs) : 0;
    if (Py_EnterRecursiveCall((char*)" while calling a Python object")) {
        return NULL;
    }
    if (
#if PY_MAJOR_VERSION >= 3
            co->co_kwonlyargcount == 0 &&
#endif
            likely(kwargs == NULL || nk == 0) &&
            co->co_flags == (CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE)) {
        if (argdefs == NULL && co->co_argcount == nargs) {
            result = __Pyx_PyFunction_FastCallNoKw(co, args, nargs, globals);
            goto done;
        }
        else if (nargs == 0 && argdefs != NULL
                 && co->co_argcount == Py_SIZE(argdefs)) {
            /* function called with no arguments, but all parameters have
               a default value: use default values as arguments .*/
            args = &PyTuple_GET_ITEM(argdefs, 0);
            result =__Pyx_PyFunction_FastCallNoKw(co, args, Py_SIZE(argdefs), globals);
            goto done;
        }
    }
    if (kwargs != NULL) {
        Py_ssize_t pos, i;
        kwtuple = PyTuple_New(2 * nk);
        if (kwtuple == NULL) {
            result = NULL;
            goto done;
        }
        k = &PyTuple_GET_ITEM(kwtuple, 0);
        pos = i = 0;
        while (PyDict_Next(kwargs, &pos, &k[i], &k[i+1])) {
            Py_INCREF(k[i]);
            Py_INCREF(k[i+1]);
            i += 2;
        }
        nk = i / 2;
    }
    else {
        kwtuple = NULL;
        k = NULL;
    }
    closure = PyFunction_GET_CLOSURE(func);
#if PY_MAJOR_VERSION >= 3
    kwdefs = PyFunction_GET_KW_DEFAULTS(func);
#endif
    if (argdefs != NULL) {
        d = &PyTuple_GET_ITEM(argdefs, 0);
        nd = Py_SIZE(argdefs);
    }
    else {
        d = NULL;
        nd = 0;
    }
#if PY_MAJOR_VERSION >= 3
    result = PyEval_EvalCodeEx((PyObject*)co, globals, (PyObject *)NULL,
                               args, (int)nargs,
                               k, (int)nk,
                               d, (int)nd, kwdefs, closure);
#else
    result = PyEval_EvalCodeEx(co, globals, (PyObject *)NULL,
                               args, (int)nargs,
                               k, (int)nk,
                               d, (int)nd, closure);
#endif
    Py_XDECREF(kwtuple);
done:
    Py_LeaveRecursiveCall();
    return result;
}
#endif
#endif

/* PyObjectCall */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_Call(PyObject *func, PyObject *arg, PyObject *kw) {
    PyObject *result;
    ternaryfunc call = Py_TYPE(func)->tp_call;
    if (unlikely(!call))
        return PyObject_Call(func, arg, kw);
    if (unlikely(Py_EnterRecursiveCall((char*)" while calling a Python object")))
        return NULL;
    result = (*call)(func, arg, kw);
    Py_LeaveRecursiveCall();
    if (unlikely(!result) && unlikely(!PyErr_Occurred())) {
        PyErr_SetString(
            PyExc_SystemError,
            "NULL result without error in PyObject_Call");
    }
    return result;
}
#endif

/* PyObjectCallMethO */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallMethO(PyObject *func, PyObject *arg) {
    PyObject *self, *result;
    PyCFunction cfunc;
    cfunc = PyCFunction_GET_FUNCTION(func);
    self = PyCFunction_GET_SELF(func);
    if (unlikely(Py_EnterRecursiveCall((char*)" while calling a Python object")))
        return NULL;
    result = cfunc(self, arg);
    Py_LeaveRecursiveCall();
    if (unlikely(!result) && unlikely(!PyErr_Occurred())) {
        PyErr_SetString(
            PyExc_SystemError,
            "NULL result without error in PyObject_Call");
    }
    return result;
}
#endif

/* PyObjectCallNoArg */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallNoArg(PyObject *func) {
#if CYTHON_FAST_PYCALL
    if (PyFunction_Check(func)) {
        return __Pyx_PyFunction_FastCall(func, NULL, 0);
    }
#endif
#ifdef __Pyx_CyFunction_USED
    if (likely(PyCFunction_Check(func) || __Pyx_CyFunction_Check(func)))
#else
    if (likely(PyCFunction_Check(func)))
#endif
    {
        if (likely(PyCFunction_GET_FLAGS(func) & METH_NOARGS)) {
            return __Pyx_PyObject_CallMethO(func, NULL);
        }
    }
    return __Pyx_PyObject_Call(func, __pyx_empty_tuple, NULL);
}
#endif

/* PyCFunctionFastCall */
#if CYTHON_FAST_PYCCALL
static CYTHON_INLINE PyObject * __Pyx_PyCFunction_FastCall(PyObject *func_obj, PyObject **args, Py_ssize_t nargs) {
    PyCFunctionObject *func = (PyCFunctionObject*)func_obj;
    PyCFunction meth = PyCFunction_GET_FUNCTION(func);
    PyObject *self = PyCFunction_GET_SELF(func);
    int flags = PyCFunction_GET_FLAGS(func);
    assert(PyCFunction_Check(func));
    assert(METH_FASTCALL == (flags & ~(METH_CLASS | METH_STATIC | METH_COEXIST | METH_KEYWORDS | METH_STACKLESS)));
    assert(nargs >= 0);
    assert(nargs == 0 || args != NULL);
    /* _PyCFunction_FastCallDict() must not be called with an exception set,
       because it may clear it (directly or indirectly) and so the
       caller loses its exception */
    assert(!PyErr_Occurred());
    if ((PY_VERSION_HEX < 0x030700A0) || unlikely(flags & METH_KEYWORDS)) {
        return (*((__Pyx_PyCFunctionFastWithKeywords)(void*)meth)) (self, args, nargs, NULL);
    } else {
        return (*((__Pyx_PyCFunctionFast)(void*)meth)) (self, args, nargs);
    }
}
#endif

/* PyObjectCallOneArg */
#if CYTHON_COMPILING_IN_CPYTHON
static PyObject* __Pyx__PyObject_CallOneArg(PyObject *func, PyObject *arg) {
    PyObject *result;
    PyObject *args = PyTuple_New(1);
    if (unlikely(!args)) return NULL;
    Py_INCREF(arg);
    PyTuple_SET_ITEM(args, 0, arg);
    result = __Pyx_PyObject_Call(func, args, NULL);
    Py_DECREF(args);
    return result;
}
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallOneArg(PyObject *func, PyObject *arg) {
#if CYTHON_FAST_PYCALL
    if (PyFunction_Check(func)) {
        return __Pyx_PyFunction_FastCall(func, &arg, 1);
    }
#endif
    if (likely(PyCFunction_Check(func))) {
        if (likely(PyCFunction_GET_FLAGS(func) & METH_O)) {
            return __Pyx_PyObject_CallMethO(func, arg);
#if CYTHON_FAST_PYCCALL
        } else if (__Pyx_PyFastCFunction_Check(func)) {
            return __Pyx_PyCFunction_FastCall(func, &arg, 1);
#endif
        }
    }
    return __Pyx__PyObject_CallOneArg(func, arg);
}
#else
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallOneArg(PyObject *func, PyObject *arg) {
    PyObject *result;
    PyObject *args = PyTuple_Pack(1, arg);
    if (unlikely(!args)) return NULL;
    result = __Pyx_PyObject_Call(func, args, NULL);
    Py_DECREF(args);
    return result;
}
#endif

/* RaiseDoubleKeywords */
static void __Pyx_RaiseDoubleKeywordsError(
    const char* func_name,
    PyObject* kw_name)
{
    PyErr_Format(PyExc_TypeError,
        #if PY_MAJOR_VERSION >= 3
        "%s() got multiple values for keyword argument '%U'", func_name, kw_name);
        #else
        "%s() got multiple values for keyword argument '%s'", func_name,
        PyString_AsString(kw_name));
        #endif
}

/* ParseKeywords */
static int __Pyx_ParseOptionalKeywords(
    PyObject *kwds,
    PyObject **argnames[],
    PyObject *kwds2,
    PyObject *values[],
    Py_ssize_t num_pos_args,
    const char* function_name)
{
    PyObject *key = 0, *value = 0;
    Py_ssize_t pos = 0;
    PyObject*** name;
    PyObject*** first_kw_arg = argnames + num_pos_args;
    while (PyDict_Next(kwds, &pos, &key, &value)) {
        name = first_kw_arg;
        while (*name && (**name != key)) name++;
        if (*name) {
            values[name-argnames] = value;
            continue;
        }
        name = first_kw_arg;
        #if PY_MAJOR_VERSION < 3
        if (likely(PyString_Check(key))) {
            while (*name) {
                if ((CYTHON_COMPILING_IN_PYPY || PyString_GET_SIZE(**name) == PyString_GET_SIZE(key))
                        && _PyString_Eq(**name, key)) {
                    values[name-argnames] = value;
                    break;
                }
                name++;
            }
            if (*name) continue;
            else {
                PyObject*** argname = argnames;
                while (argname != first_kw_arg) {
                    if ((**argname == key) || (
                            (CYTHON_COMPILING_IN_PYPY || PyString_GET_SIZE(**argname) == PyString_GET_SIZE(key))
                             && _PyString_Eq(**argname, key))) {
                        goto arg_passed_twice;
                    }
                    argname++;
                }
            }
        } else
        #endif
        if (likely(PyUnicode_Check(key))) {
            while (*name) {
                int cmp = (**name == key) ? 0 :
                #if !CYTHON_COMPILING_IN_PYPY && PY_MAJOR_VERSION >= 3
                    (__Pyx_PyUnicode_GET_LENGTH(**name) != __Pyx_PyUnicode_GET_LENGTH(key)) ? 1 :
                #endif
                    PyUnicode_Compare(**name, key);
                if (cmp < 0 && unlikely(PyErr_Occurred())) goto bad;
                if (cmp == 0) {
                    values[name-argnames] = value;
                    break;
                }
                name++;
            }
            if (*name) continue;
            else {
                PyObject*** argname = argnames;
                while (argname != first_kw_arg) {
                    int cmp = (**argname == key) ? 0 :
                    #if !CYTHON_COMPILING_IN_PYPY && PY_MAJOR_VERSION >= 3
                        (__Pyx_PyUnicode_GET_LENGTH(**argname) != __Pyx_PyUnicode_GET_LENGTH(key)) ? 1 :
                    #endif
                        PyUnicode_Compare(**argname, key);
                    if (cmp < 0 && unlikely(PyErr_Occurred())) goto bad;
                    if (cmp == 0) goto arg_passed_twice;
                    argname++;
                }
            }
        } else
            goto invalid_keyword_type;
        if (kwds2) {
            if (unlikely(PyDict_SetItem(kwds2, key, value))) goto bad;
        } else {
            goto invalid_keyword;
        }
    }
    return 0;
arg_passed_twice:
    __Pyx_RaiseDoubleKeywordsError(function_name, key);
    goto bad;
invalid_keyword_type:
    PyErr_Format(PyExc_TypeError,
        "%.200s() keywords must be strings", function_name);
    goto bad;
invalid_keyword:
    PyErr_Format(PyExc_TypeError,
    #if PY_MAJOR_VERSION < 3
        "%.200s() got an unexpected keyword argument '%.200s'",
        function_name, PyString_AsString(key));
    #else
        "%s() got an unexpected keyword argument '%U'",
        function_name, key);
    #endif
bad:
    return -1;
}

/* PyObjectCall2Args */
static CYTHON_UNUSED PyObject* __Pyx_PyObject_Call2Args(PyObject* function, PyObject* arg1, PyObject* arg2) {
    PyObject *args, *result = NULL;
    #if CYTHON_FAST_PYCALL
    if (PyFunction_Check(function)) {
        PyObject *args[2] = {arg1, arg2};
        return __Pyx_PyFunction_FastCall(function, args, 2);
    }
    #endif
    #if CYTHON_FAST_PYCCALL
    if (__Pyx_PyFastCFunction_Check(function)) {
        PyObject *args[2] = {arg1, arg2};
        return __Pyx_PyCFunction_FastCall(function, args, 2);
    }
    #endif
    args = PyTuple_New(2);
    if (unlikely(!args)) goto done;
    Py_INCREF(arg1);
    PyTuple_SET_ITEM(args, 0, arg1);
    Py_INCREF(arg2);
    PyTuple_SET_ITEM(args, 1, arg2);
    Py_INCREF(function);
    result = __Pyx_PyObject_Call(function, args, NULL);
    Py_DECREF(args);
    Py_DECREF(function);
done:
    return result;
}

/* GetItemInt */
static PyObject *__Pyx_GetItemInt_Generic(PyObject *o, PyObject* j) {
    PyObject *r;
    if (!j) return NULL;
    r = PyObject_GetItem(o, j);
    Py_DECREF(j);
    return r;
}
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_List_Fast(PyObject *o, Py_ssize_t i,
                                                              CYTHON_NCP_UNUSED int wraparound,
                                                              CYTHON_NCP_UNUSED int boundscheck) {
#if CYTHON_ASSUME_SAFE_MACROS && !CYTHON_AVOID_BORROWED_REFS
    Py_ssize_t wrapped_i = i;
    if (wraparound & unlikely(i < 0)) {
        wrapped_i += PyList_GET_SIZE(o);
    }
    if ((!boundscheck) || likely(__Pyx_is_valid_index(wrapped_i, PyList_GET_SIZE(o)))) {
        PyObject *r = PyList_GET_ITEM(o, wrapped_i);
        Py_INCREF(r);
        return r;
    }
    return __Pyx_GetItemInt_Generic(o, PyInt_FromSsize_t(i));
#else
    return PySequence_GetItem(o, i);
#endif
}
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_Tuple_Fast(PyObject *o, Py_ssize_t i,
                                                              CYTHON_NCP_UNUSED int wraparound,
                                                              CYTHON_NCP_UNUSED int boundscheck) {
#if CYTHON_ASSUME_SAFE_MACROS && !CYTHON_AVOID_BORROWED_REFS
    Py_ssize_t wrapped_i = i;
    if (wraparound & unlikely(i < 0)) {
        wrapped_i += PyTuple_GET_SIZE(o);
    }
    if ((!boundscheck) || likely(__Pyx_is_valid_index(wrapped_i, PyTuple_GET_SIZE(o)))) {
        PyObject *r = PyTuple_GET_ITEM(o, wrapped_i);
        Py_INCREF(r);
        return r;
    }
    return __Pyx_GetItemInt_Generic(o, PyInt_FromSsize_t(i));
#else
    return PySequence_GetItem(o, i);
#endif
}
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_Fast(PyObject *o, Py_ssize_t i, int is_list,
                                                     CYTHON_NCP_UNUSED int wraparound,
                                                     CYTHON_NCP_UNUSED int boundscheck) {
#if CYTHON_ASSUME_SAFE_MACROS && !CYTHON_AVOID_BORROWED_REFS && CYTHON_USE_TYPE_SLOTS
    if (is_list || PyList_CheckExact(o)) {
        Py_ssize_t n = ((!wraparound) | likely(i >= 0)) ? i : i + PyList_GET_SIZE(o);
        if ((!boundscheck) || (likely(__Pyx_is_valid_index(n, PyList_GET_SIZE(o))))) {
            PyObject *r = PyList_GET_ITEM(o, n);
            Py_INCREF(r);
            return r;
        }
    }
    else if (PyTuple_CheckExact(o)) {
        Py_ssize_t n = ((!wraparound) | likely(i >= 0)) ? i : i + PyTuple_GET_SIZE(o);
        if ((!boundscheck) || likely(__Pyx_is_valid_index(n, PyTuple_GET_SIZE(o)))) {
            PyObject *r = PyTuple_GET_ITEM(o, n);
            Py_INCREF(r);
            return r;
        }
    } else {
        PySequenceMethods *m = Py_TYPE(o)->tp_as_sequence;
        if (likely(m && m->sq_item)) {
            if (wraparound && unlikely(i < 0) && likely(m->sq_length)) {
                Py_ssize_t l = m->sq_length(o);
                if (likely(l >= 0)) {
                    i += l;
                } else {
                    if (!PyErr_ExceptionMatches(PyExc_OverflowError))
                        return NULL;
                    PyErr_Clear();
                }
            }
            return m->sq_item(o, i);
        }
    }
#else
    if (is_list || PySequence_Check(o)) {
        return PySequence_GetItem(o, i);
    }
#endif
    return __Pyx_GetItemInt_Generic(o, PyInt_FromSsize_t(i));
}

/* RaiseException */
#if PY_MAJOR_VERSION < 3
static void __Pyx_Raise(PyObject *type, PyObject *value, PyObject *tb,
                        CYTHON_UNUSED PyObject *cause) {
    __Pyx_PyThreadState_declare
    Py_XINCREF(type);
    if (!value || value == Py_None)
        value = NULL;
    else
        Py_INCREF(value);
    if (!tb || tb == Py_None)
        tb = NULL;
    else {
        Py_INCREF(tb);
        if (!PyTraceBack_Check(tb)) {
            PyErr_SetString(PyExc_TypeError,
                "raise: arg 3 must be a traceback or None");
            goto raise_error;
        }
    }
    if (PyType_Check(type)) {
#if CYTHON_COMPILING_IN_PYPY
        if (!value) {
            Py_INCREF(Py_None);
            value = Py_None;
        }
#endif
        PyErr_NormalizeException(&type, &value, &tb);
    } else {
        if (value) {
            PyErr_SetString(PyExc_TypeError,
                "instance exception may not have a separate value");
            goto raise_error;
        }
        value = type;
        type = (PyObject*) Py_TYPE(type);
        Py_INCREF(type);
        if (!PyType_IsSubtype((PyTypeObject *)type, (PyTypeObject *)PyExc_BaseException)) {
            PyErr_SetString(PyExc_TypeError,
                "raise: exception class must be a subclass of BaseException");
            goto raise_error;
        }
    }
    __Pyx_PyThreadState_assign
    __Pyx_ErrRestore(type, value, tb);
    return;
raise_error:
    Py_XDECREF(value);
    Py_XDECREF(type);
    Py_XDECREF(tb);
    return;
}
#else
static void __Pyx_Raise(PyObject *type, PyObject *value, PyObject *tb, PyObject *cause) {
    PyObject* owned_instance = NULL;
    if (tb == Py_None) {
        tb = 0;
    } else if (tb && !PyTraceBack_Check(tb)) {
        PyErr_SetString(PyExc_TypeError,
            "raise: arg 3 must be a traceback or None");
        goto bad;
    }
    if (value == Py_None)
        value = 0;
    if (PyExceptionInstance_Check(type)) {
        if (value) {
            PyErr_SetString(PyExc_TypeError,
                "instance exception may not have a separate value");
            goto bad;
        }
        value = type;
        type = (PyObject*) Py_TYPE(value);
    } else if (PyExceptionClass_Check(type)) {
        PyObject *instance_class = NULL;
        if (value && PyExceptionInstance_Check(value)) {
            instance_class = (PyObject*) Py_TYPE(value);
            if (instance_class != type) {
                int is_subclass = PyObject_IsSubclass(instance_class, type);
                if (!is_subclass) {
                    instance_class = NULL;
                } else if (unlikely(is_subclass == -1)) {
                    goto bad;
                } else {
                    type = instance_class;
                }
            }
        }
        if (!instance_class) {
            PyObject *args;
            if (!value)
                args = PyTuple_New(0);
            else if (PyTuple_Check(value)) {
                Py_INCREF(value);
                args = value;
            } else
                args = PyTuple_Pack(1, value);
            if (!args)
                goto bad;
            owned_instance = PyObject_Call(type, args, NULL);
            Py_DECREF(args);
            if (!owned_instance)
                goto bad;
            value = owned_instance;
            if (!PyExceptionInstance_Check(value)) {
                PyErr_Format(PyExc_TypeError,
                             "calling %R should have returned an instance of "
                             "BaseException, not %R",
                             type, Py_TYPE(value));
                goto bad;
            }
        }
    } else {
        PyErr_SetString(PyExc_TypeError,
            "raise: exception class must be a subclass of BaseException");
        goto bad;
    }
    if (cause) {
        PyObject *fixed_cause;
        if (cause == Py_None) {
            fixed_cause = NULL;
        } else if (PyExceptionClass_Check(cause)) {
            fixed_cause = PyObject_CallObject(cause, NULL);
            if (fixed_cause == NULL)
                goto bad;
        } else if (PyExceptionInstance_Check(cause)) {
            fixed_cause = cause;
            Py_INCREF(fixed_cause);
        } else {
            PyErr_SetString(PyExc_TypeError,
                            "exception causes must derive from "
                            "BaseException");
            goto bad;
        }
        PyException_SetCause(value, fixed_cause);
    }
    PyErr_SetObject(type, value);
    if (tb) {
#if CYTHON_COMPILING_IN_PYPY
        PyObject *tmp_type, *tmp_value, *tmp_tb;
        PyErr_Fetch(&tmp_type, &tmp_value, &tmp_tb);
        Py_INCREF(tb);
        PyErr_Restore(tmp_type, tmp_value, tb);
        Py_XDECREF(tmp_tb);
#else
        PyThreadState *tstate = __Pyx_PyThreadState_Current;
        PyObject* tmp_tb = tstate->curexc_traceback;
        if (tb != tmp_tb) {
            Py_INCREF(tb);
            tstate->curexc_traceback = tb;
            Py_XDECREF(tmp_tb);
        }
#endif
    }
bad:
    Py_XDECREF(owned_instance);
    return;
}
#endif

/* PyObject_GenericGetAttrNoDict */
#if CYTHON_USE_TYPE_SLOTS && CYTHON_USE_PYTYPE_LOOKUP && PY_VERSION_HEX < 0x03070000
static PyObject *__Pyx_RaiseGenericGetAttributeError(PyTypeObject *tp, PyObject *attr_name) {
    PyErr_Format(PyExc_AttributeError,
#if PY_MAJOR_VERSION >= 3
                 "'%.50s' object has no attribute '%U'",
                 tp->tp_name, attr_name);
#else
                 "'%.50s' object has no attribute '%.400s'",
                 tp->tp_name, PyString_AS_STRING(attr_name));
#endif
    return NULL;
}
static CYTHON_INLINE PyObject* __Pyx_PyObject_GenericGetAttrNoDict(PyObject* obj, PyObject* attr_name) {
    PyObject *descr;
    PyTypeObject *tp = Py_TYPE(obj);
    if (unlikely(!PyString_Check(attr_name))) {
        return PyObject_GenericGetAttr(obj, attr_name);
    }
    assert(!tp->tp_dictoffset);
    descr = _PyType_Lookup(tp, attr_name);
    if (unlikely(!descr)) {
        return __Pyx_RaiseGenericGetAttributeError(tp, attr_name);
    }
    Py_INCREF(descr);
    #if PY_MAJOR_VERSION < 3
    if (likely(PyType_HasFeature(Py_TYPE(descr), Py_TPFLAGS_HAVE_CLASS)))
    #endif
    {
        descrgetfunc f = Py_TYPE(descr)->tp_descr_get;
        if (unlikely(f)) {
            PyObject *res = f(descr, obj, (PyObject *)tp);
            Py_DECREF(descr);
            return res;
        }
    }
    return descr;
}
#endif

/* PyObject_GenericGetAttr */
#if CYTHON_USE_TYPE_SLOTS && CYTHON_USE_PYTYPE_LOOKUP && PY_VERSION_HEX < 0x03070000
static PyObject* __Pyx_PyObject_GenericGetAttr(PyObject* obj, PyObject* attr_name) {
    if (unlikely(Py_TYPE(obj)->tp_dictoffset)) {
        return PyObject_GenericGetAttr(obj, attr_name);
    }
    return __Pyx_PyObject_GenericGetAttrNoDict(obj, attr_name);
}
#endif

/* SetVTable */
static int __Pyx_SetVtable(PyObject *dict, void *vtable) {
#if PY_VERSION_HEX >= 0x02070000
    PyObject *ob = PyCapsule_New(vtable, 0, 0);
#else
    PyObject *ob = PyCObject_FromVoidPtr(vtable, 0);
#endif
    if (!ob)
        goto bad;
    if (PyDict_SetItem(dict, __pyx_n_s_pyx_vtable, ob) < 0)
        goto bad;
    Py_DECREF(ob);
    return 0;
bad:
    Py_XDECREF(ob);
    return -1;
}

/* PyErrExceptionMatches */
#if CYTHON_FAST_THREAD_STATE
static int __Pyx_PyErr_ExceptionMatchesTuple(PyObject *exc_type, PyObject *tuple) {
    Py_ssize_t i, n;
    n = PyTuple_GET_SIZE(tuple);
#if PY_MAJOR_VERSION >= 3
    for (i=0; i<n; i++) {
        if (exc_type == PyTuple_GET_ITEM(tuple, i)) return 1;
    }
#endif
    for (i=0; i<n; i++) {
        if (__Pyx_PyErr_GivenExceptionMatches(exc_type, PyTuple_GET_ITEM(tuple, i))) return 1;
    }
    return 0;
}
static CYTHON_INLINE int __Pyx_PyErr_ExceptionMatchesInState(PyThreadState* tstate, PyObject* err) {
    PyObject *exc_type = tstate->curexc_type;
    if (exc_type == err) return 1;
    if (unlikely(!exc_type)) return 0;
    if (unlikely(PyTuple_Check(err)))
        return __Pyx_PyErr_ExceptionMatchesTuple(exc_type, err);
    return __Pyx_PyErr_GivenExceptionMatches(exc_type, err);
}
#endif

/* PyObjectGetAttrStrNoError */
static void __Pyx_PyObject_GetAttrStr_ClearAttributeError(void) {
    __Pyx_PyThreadState_declare
    __Pyx_PyThreadState_assign
    if (likely(__Pyx_PyErr_ExceptionMatches(PyExc_AttributeError)))
        __Pyx_PyErr_Clear();
}
static CYTHON_INLINE PyObject* __Pyx_PyObject_GetAttrStrNoError(PyObject* obj, PyObject* attr_name) {
    PyObject *result;
#if CYTHON_COMPILING_IN_CPYTHON && CYTHON_USE_TYPE_SLOTS && PY_VERSION_HEX >= 0x030700B1
    PyTypeObject* tp = Py_TYPE(obj);
    if (likely(tp->tp_getattro == PyObject_GenericGetAttr)) {
        return _PyObject_GenericGetAttrWithDict(obj, attr_name, NULL, 1);
    }
#endif
    result = __Pyx_PyObject_GetAttrStr(obj, attr_name);
    if (unlikely(!result)) {
        __Pyx_PyObject_GetAttrStr_ClearAttributeError();
    }
    return result;
}

/* SetupReduce */
static int __Pyx_setup_reduce_is_named(PyObject* meth, PyObject* name) {
  int ret;
  PyObject *name_attr;
  name_attr = __Pyx_PyObject_GetAttrStr(meth, __pyx_n_s_name);
  if (likely(name_attr)) {
      ret = PyObject_RichCompareBool(name_attr, name, Py_EQ);
  } else {
      ret = -1;
  }
  if (unlikely(ret < 0)) {
      PyErr_Clear();
      ret = 0;
  }
  Py_XDECREF(name_attr);
  return ret;
}
static int __Pyx_setup_reduce(PyObject* type_obj) {
    int ret = 0;
    PyObject *object_reduce = NULL;
    PyObject *object_reduce_ex = NULL;
    PyObject *reduce = NULL;
    PyObject *reduce_ex = NULL;
    PyObject *reduce_cython = NULL;
    PyObject *setstate = NULL;
    PyObject *setstate_cython = NULL;
#if CYTHON_USE_PYTYPE_LOOKUP
    if (_PyType_Lookup((PyTypeObject*)type_obj, __pyx_n_s_getstate)) goto __PYX_GOOD;
#else
    if (PyObject_HasAttr(type_obj, __pyx_n_s_getstate)) goto __PYX_GOOD;
#endif
#if CYTHON_USE_PYTYPE_LOOKUP
    object_reduce_ex = _PyType_Lookup(&PyBaseObject_Type, __pyx_n_s_reduce_ex); if (!object_reduce_ex) goto __PYX_BAD;
#else
    object_reduce_ex = __Pyx_PyObject_GetAttrStr((PyObject*)&PyBaseObject_Type, __pyx_n_s_reduce_ex); if (!object_reduce_ex) goto __PYX_BAD;
#endif
    reduce_ex = __Pyx_PyObject_GetAttrStr(type_obj, __pyx_n_s_reduce_ex); if (unlikely(!reduce_ex)) goto __PYX_BAD;
    if (reduce_ex == object_reduce_ex) {
#if CYTHON_USE_PYTYPE_LOOKUP
        object_reduce = _PyType_Lookup(&PyBaseObject_Type, __pyx_n_s_reduce); if (!object_reduce) goto __PYX_BAD;
#else
        object_reduce = __Pyx_PyObject_GetAttrStr((PyObject*)&PyBaseObject_Type, __pyx_n_s_reduce); if (!object_reduce) goto __PYX_BAD;
#endif
        reduce = __Pyx_PyObject_GetAttrStr(type_obj, __pyx_n_s_reduce); if (unlikely(!reduce)) goto __PYX_BAD;
        if (reduce == object_reduce || __Pyx_setup_reduce_is_named(reduce, __pyx_n_s_reduce_cython)) {
            reduce_cython = __Pyx_PyObject_GetAttrStrNoError(type_obj, __pyx_n_s_reduce_cython);
            if (likely(reduce_cython)) {
                ret = PyDict_SetItem(((PyTypeObject*)type_obj)->tp_dict, __pyx_n_s_reduce, reduce_cython); if (unlikely(ret < 0)) goto __PYX_BAD;
                ret = PyDict_DelItem(((PyTypeObject*)type_obj)->tp_dict, __pyx_n_s_reduce_cython); if (unlikely(ret < 0)) goto __PYX_BAD;
            } else if (reduce == object_reduce || PyErr_Occurred()) {
                goto __PYX_BAD;
            }
            setstate = __Pyx_PyObject_GetAttrStr(type_obj, __pyx_n_s_setstate);
            if (!setstate) PyErr_Clear();
            if (!setstate || __Pyx_setup_reduce_is_named(setstate, __pyx_n_s_setstate_cython)) {
                setstate_cython = __Pyx_PyObject_GetAttrStrNoError(type_obj, __pyx_n_s_setstate_cython);
                if (likely(setstate_cython)) {
                    ret = PyDict_SetItem(((PyTypeObject*)type_obj)->tp_dict, __pyx_n_s_setstate, setstate_cython); if (unlikely(ret < 0)) goto __PYX_BAD;
                    ret = PyDict_DelItem(((PyTypeObject*)type_obj)->tp_dict, __pyx_n_s_setstate_cython); if (unlikely(ret < 0)) goto __PYX_BAD;
                } else if (!setstate || PyErr_Occurred()) {
                    goto __PYX_BAD;
                }
            }
            PyType_Modified((PyTypeObject*)type_obj);
        }
    }
    goto __PYX_GOOD;
__PYX_BAD:
    if (!PyErr_Occurred())
        PyErr_Format(PyExc_RuntimeError, "Unable to initialize pickling for %s", ((PyTypeObject*)type_obj)->tp_name);
    ret = -1;
__PYX_GOOD:
#if !CYTHON_USE_PYTYPE_LOOKUP
    Py_XDECREF(object_reduce);
    Py_XDECREF(object_reduce_ex);
#endif
    Py_XDECREF(reduce);
    Py_XDECREF(reduce_ex);
    Py_XDECREF(reduce_cython);
    Py_XDECREF(setstate);
    Py_XDECREF(setstate_cython);
    return ret;
}

/* Import */
static PyObject *__Pyx_Import(PyObject *name, PyObject *from_list, int level) {
    PyObject *empty_list = 0;
    PyObject *module = 0;
    PyObject *global_dict = 0;
    PyObject *empty_dict = 0;
    PyObject *list;
    #if PY_MAJOR_VERSION < 3
    PyObject *py_import;
    py_import = __Pyx_PyObject_GetAttrStr(__pyx_b, __pyx_n_s_import);
    if (!py_import)
        goto bad;
    #endif
    if (from_list)
        list = from_list;
    else {
        empty_list = PyList_New(0);
        if (!empty_list)
            goto bad;
        list = empty_list;
    }
    global_dict = PyModule_GetDict(__pyx_m);
    if (!global_dict)
        goto bad;
    empty_dict = PyDict_New();
    if (!empty_dict)
        goto bad;
    {
        #if PY_MAJOR_VERSION >= 3
        if (level == -1) {
            if ((1) && (strchr(__Pyx_MODULE_NAME, '.'))) {
                module = PyImport_ImportModuleLevelObject(
                    name, global_dict, empty_dict, list, 1);
                if (!module) {
                    if (!PyErr_ExceptionMatches(PyExc_ImportError))
                        goto bad;
                    PyErr_Clear();
                }
            }
            level = 0;
        }
        #endif
        if (!module) {
            #if PY_MAJOR_VERSION < 3
            PyObject *py_level = PyInt_FromLong(level);
            if (!py_level)
                goto bad;
            module = PyObject_CallFunctionObjArgs(py_import,
                name, global_dict, empty_dict, list, py_level, (PyObject *)NULL);
            Py_DECREF(py_level);
            #else
            module = PyImport_ImportModuleLevelObject(
                name, global_dict, empty_dict, list, level);
            #endif
        }
    }
bad:
    #if PY_MAJOR_VERSION < 3
    Py_XDECREF(py_import);
    #endif
    Py_XDECREF(empty_list);
    Py_XDECREF(empty_dict);
    return module;
}

/* PyObjectGetMethod */
static int __Pyx_PyObject_GetMethod(PyObject *obj, PyObject *name, PyObject **method) {
    PyObject *attr;
#if CYTHON_UNPACK_METHODS && CYTHON_COMPILING_IN_CPYTHON && CYTHON_USE_PYTYPE_LOOKUP
    PyTypeObject *tp = Py_TYPE(obj);
    PyObject *descr;
    descrgetfunc f = NULL;
    PyObject **dictptr, *dict;
    int meth_found = 0;
    assert (*method == NULL);
    if (unlikely(tp->tp_getattro != PyObject_GenericGetAttr)) {
        attr = __Pyx_PyObject_GetAttrStr(obj, name);
        goto try_unpack;
    }
    if (unlikely(tp->tp_dict == NULL) && unlikely(PyType_Ready(tp) < 0)) {
        return 0;
    }
    descr = _PyType_Lookup(tp, name);
    if (likely(descr != NULL)) {
        Py_INCREF(descr);
#if PY_MAJOR_VERSION >= 3
        #ifdef __Pyx_CyFunction_USED
        if (likely(PyFunction_Check(descr) || (Py_TYPE(descr) == &PyMethodDescr_Type) || __Pyx_CyFunction_Check(descr)))
        #else
        if (likely(PyFunction_Check(descr) || (Py_TYPE(descr) == &PyMethodDescr_Type)))
        #endif
#else
        #ifdef __Pyx_CyFunction_USED
        if (likely(PyFunction_Check(descr) || __Pyx_CyFunction_Check(descr)))
        #else
        if (likely(PyFunction_Check(descr)))
        #endif
#endif
        {
            meth_found = 1;
        } else {
            f = Py_TYPE(descr)->tp_descr_get;
            if (f != NULL && PyDescr_IsData(descr)) {
                attr = f(descr, obj, (PyObject *)Py_TYPE(obj));
                Py_DECREF(descr);
                goto try_unpack;
            }
        }
    }
    dictptr = _PyObject_GetDictPtr(obj);
    if (dictptr != NULL && (dict = *dictptr) != NULL) {
        Py_INCREF(dict);
        attr = __Pyx_PyDict_GetItemStr(dict, name);
        if (attr != NULL) {
            Py_INCREF(attr);
            Py_DECREF(dict);
            Py_XDECREF(descr);
            goto try_unpack;
        }
        Py_DECREF(dict);
    }
    if (meth_found) {
        *method = descr;
        return 1;
    }
    if (f != NULL) {
        attr = f(descr, obj, (PyObject *)Py_TYPE(obj));
        Py_DECREF(descr);
        goto try_unpack;
    }
    if (descr != NULL) {
        *method = descr;
        return 0;
    }
    PyErr_Format(PyExc_AttributeError,
#if PY_MAJOR_VERSION >= 3
                 "'%.50s' object has no attribute '%U'",
                 tp->tp_name, name);
#else
                 "'%.50s' object has no attribute '%.400s'",
                 tp->tp_name, PyString_AS_STRING(name));
#endif
    return 0;
#else
    attr = __Pyx_PyObject_GetAttrStr(obj, name);
    goto try_unpack;
#endif
try_unpack:
#if CYTHON_UNPACK_METHODS
    if (likely(attr) && PyMethod_Check(attr) && likely(PyMethod_GET_SELF(attr) == obj)) {
        PyObject *function = PyMethod_GET_FUNCTION(attr);
        Py_INCREF(function);
        Py_DECREF(attr);
        *method = function;
        return 1;
    }
#endif
    *method = attr;
    return 0;
}

/* PyObjectCallMethod1 */
static PyObject* __Pyx__PyObject_CallMethod1(PyObject* method, PyObject* arg) {
    PyObject *result = __Pyx_PyObject_CallOneArg(method, arg);
    Py_DECREF(method);
    return result;
}
static PyObject* __Pyx_PyObject_CallMethod1(PyObject* obj, PyObject* method_name, PyObject* arg) {
    PyObject *method = NULL, *result;
    int is_method = __Pyx_PyObject_GetMethod(obj, method_name, &method);
    if (likely(is_method)) {
        result = __Pyx_PyObject_Call2Args(method, obj, arg);
        Py_DECREF(method);
        return result;
    }
    if (unlikely(!method)) return NULL;
    return __Pyx__PyObject_CallMethod1(method, arg);
}

/* append */
static CYTHON_INLINE int __Pyx_PyObject_Append(PyObject* L, PyObject* x) {
    if (likely(PyList_CheckExact(L))) {
        if (unlikely(__Pyx_PyList_Append(L, x) < 0)) return -1;
    } else {
        PyObject* retval = __Pyx_PyObject_CallMethod1(L, __pyx_n_s_append, x);
        if (unlikely(!retval))
            return -1;
        Py_DECREF(retval);
    }
    return 0;
}

/* ImportFrom */
static PyObject* __Pyx_ImportFrom(PyObject* module, PyObject* name) {
    PyObject* value = __Pyx_PyObject_GetAttrStr(module, name);
    if (unlikely(!value) && PyErr_ExceptionMatches(PyExc_AttributeError)) {
        PyErr_Format(PyExc_ImportError,
        #if PY_MAJOR_VERSION < 3
            "cannot import name %.230s", PyString_AS_STRING(name));
        #else
            "cannot import name %S", name);
        #endif
    }
    return value;
}

/* CLineInTraceback */
#ifndef CYTHON_CLINE_IN_TRACEBACK
static int __Pyx_CLineForTraceback(CYTHON_NCP_UNUSED PyThreadState *tstate, int c_line) {
    PyObject *use_cline;
    PyObject *ptype, *pvalue, *ptraceback;
#if CYTHON_COMPILING_IN_CPYTHON
    PyObject **cython_runtime_dict;
#endif
    if (unlikely(!__pyx_cython_runtime)) {
        return c_line;
    }
    __Pyx_ErrFetchInState(tstate, &ptype, &pvalue, &ptraceback);
#if CYTHON_COMPILING_IN_CPYTHON
    cython_runtime_dict = _PyObject_GetDictPtr(__pyx_cython_runtime);
    if (likely(cython_runtime_dict)) {
        __PYX_PY_DICT_LOOKUP_IF_MODIFIED(
            use_cline, *cython_runtime_dict,
            __Pyx_PyDict_GetItemStr(*cython_runtime_dict, __pyx_n_s_cline_in_traceback))
    } else
#endif
    {
      PyObject *use_cline_obj = __Pyx_PyObject_GetAttrStr(__pyx_cython_runtime, __pyx_n_s_cline_in_traceback);
      if (use_cline_obj) {
        use_cline = PyObject_Not(use_cline_obj) ? Py_False : Py_True;
        Py_DECREF(use_cline_obj);
      } else {
        PyErr_Clear();
        use_cline = NULL;
      }
    }
    if (!use_cline) {
        c_line = 0;
        PyObject_SetAttr(__pyx_cython_runtime, __pyx_n_s_cline_in_traceback, Py_False);
    }
    else if (use_cline == Py_False || (use_cline != Py_True && PyObject_Not(use_cline) != 0)) {
        c_line = 0;
    }
    __Pyx_ErrRestoreInState(tstate, ptype, pvalue, ptraceback);
    return c_line;
}
#endif

/* CodeObjectCache */
static int __pyx_bisect_code_objects(__Pyx_CodeObjectCacheEntry* entries, int count, int code_line) {
    int start = 0, mid = 0, end = count - 1;
    if (end >= 0 && code_line > entries[end].code_line) {
        return count;
    }
    while (start < end) {
        mid = start + (end - start) / 2;
        if (code_line < entries[mid].code_line) {
            end = mid;
        } else if (code_line > entries[mid].code_line) {
             start = mid + 1;
        } else {
            return mid;
        }
    }
    if (code_line <= entries[mid].code_line) {
        return mid;
    } else {
        return mid + 1;
    }
}
static PyCodeObject *__pyx_find_code_object(int code_line) {
    PyCodeObject* code_object;
    int pos;
    if (unlikely(!code_line) || unlikely(!__pyx_code_cache.entries)) {
        return NULL;
    }
    pos = __pyx_bisect_code_objects(__pyx_code_cache.entries, __pyx_code_cache.count, code_line);
    if (unlikely(pos >= __pyx_code_cache.count) || unlikely(__pyx_code_cache.entries[pos].code_line != code_line)) {
        return NULL;
    }
    code_object = __pyx_code_cache.entries[pos].code_object;
    Py_INCREF(code_object);
    return code_object;
}
static void __pyx_insert_code_object(int code_line, PyCodeObject* code_object) {
    int pos, i;
    __Pyx_CodeObjectCacheEntry* entries = __pyx_code_cache.entries;
    if (unlikely(!code_line)) {
        return;
    }
    if (unlikely(!entries)) {
        entries = (__Pyx_CodeObjectCacheEntry*)PyMem_Malloc(64*sizeof(__Pyx_CodeObjectCacheEntry));
        if (likely(entries)) {
            __pyx_code_cache.entries = entries;
            __pyx_code_cache.max_count = 64;
            __pyx_code_cache.count = 1;
            entries[0].code_line = code_line;
            entries[0].code_object = code_object;
            Py_INCREF(code_object);
        }
        return;
    }
    pos = __pyx_bisect_code_objects(__pyx_code_cache.entries, __pyx_code_cache.count, code_line);
    if ((pos < __pyx_code_cache.count) && unlikely(__pyx_code_cache.entries[pos].code_line == code_line)) {
        PyCodeObject* tmp = entries[pos].code_object;
        entries[pos].code_object = code_object;
        Py_DECREF(tmp);
        return;
    }
    if (__pyx_code_cache.count == __pyx_code_cache.max_count) {
        int new_max = __pyx_code_cache.max_count + 64;
        entries = (__Pyx_CodeObjectCacheEntry*)PyMem_Realloc(
            __pyx_code_cache.entries, ((size_t)new_max) * sizeof(__Pyx_CodeObjectCacheEntry));
        if (unlikely(!entries)) {
            return;
        }
        __pyx_code_cache.entries = entries;
        __pyx_code_cache.max_count = new_max;
    }
    for (i=__pyx_code_cache.count; i>pos; i--) {
        entries[i] = entries[i-1];
    }
    entries[pos].code_line = code_line;
    entries[pos].code_object = code_object;
    __pyx_code_cache.count++;
    Py_INCREF(code_object);
}

/* AddTraceback */
#include "compile.h"
#include "frameobject.h"
#include "traceback.h"
static PyCodeObject* __Pyx_CreateCodeObjectForTraceback(
            const char *funcname, int c_line,
            int py_line, const char *filename) {
    PyCodeObject *py_code = 0;
    PyObject *py_srcfile = 0;
    PyObject *py_funcname = 0;
    #if PY_MAJOR_VERSION < 3
    py_srcfile = PyString_FromString(filename);
    #else
    py_srcfile = PyUnicode_FromString(filename);
    #endif
    if (!py_srcfile) goto bad;
    if (c_line) {
        #if PY_MAJOR_VERSION < 3
        py_funcname = PyString_FromFormat( "%s (%s:%d)", funcname, __pyx_cfilenm, c_line);
        #else
        py_funcname = PyUnicode_FromFormat( "%s (%s:%d)", funcname, __pyx_cfilenm, c_line);
        #endif
    }
    else {
        #if PY_MAJOR_VERSION < 3
        py_funcname = PyString_FromString(funcname);
        #else
        py_funcname = PyUnicode_FromString(funcname);
        #endif
    }
    if (!py_funcname) goto bad;
    py_code = __Pyx_PyCode_New(
        0,
        0,
        0,
        0,
        0,
        __pyx_empty_bytes, /*PyObject *code,*/
        __pyx_empty_tuple, /*PyObject *consts,*/
        __pyx_empty_tuple, /*PyObject *names,*/
        __pyx_empty_tuple, /*PyObject *varnames,*/
        __pyx_empty_tuple, /*PyObject *freevars,*/
        __pyx_empty_tuple, /*PyObject *cellvars,*/
        py_srcfile,   /*PyObject *filename,*/
        py_funcname,  /*PyObject *name,*/
        py_line,
        __pyx_empty_bytes  /*PyObject *lnotab*/
    );
    Py_DECREF(py_srcfile);
    Py_DECREF(py_funcname);
    return py_code;
bad:
    Py_XDECREF(py_srcfile);
    Py_XDECREF(py_funcname);
    return NULL;
}
static void __Pyx_AddTraceback(const char *funcname, int c_line,
                               int py_line, const char *filename) {
    PyCodeObject *py_code = 0;
    PyFrameObject *py_frame = 0;
    PyThreadState *tstate = __Pyx_PyThreadState_Current;
    if (c_line) {
        c_line = __Pyx_CLineForTraceback(tstate, c_line);
    }
    py_code = __pyx_find_code_object(c_line ? -c_line : py_line);
    if (!py_code) {
        py_code = __Pyx_CreateCodeObjectForTraceback(
            funcname, c_line, py_line, filename);
        if (!py_code) goto bad;
        __pyx_insert_code_object(c_line ? -c_line : py_line, py_code);
    }
    py_frame = PyFrame_New(
        tstate,            /*PyThreadState *tstate,*/
        py_code,           /*PyCodeObject *code,*/
        __pyx_d,    /*PyObject *globals,*/
        0                  /*PyObject *locals*/
    );
    if (!py_frame) goto bad;
    __Pyx_PyFrame_SetLineNumber(py_frame, py_line);
    PyTraceBack_Here(py_frame);
bad:
    Py_XDECREF(py_code);
    Py_XDECREF(py_frame);
}

/* CIntFromPyVerify */
#define __PYX_VERIFY_RETURN_INT(target_type, func_type, func_value)\
    __PYX__VERIFY_RETURN_INT(target_type, func_type, func_value, 0)
#define __PYX_VERIFY_RETURN_INT_EXC(target_type, func_type, func_value)\
    __PYX__VERIFY_RETURN_INT(target_type, func_type, func_value, 1)
#define __PYX__VERIFY_RETURN_INT(target_type, func_type, func_value, exc)\
    {\
        func_type value = func_value;\
        if (sizeof(target_type) < sizeof(func_type)) {\
            if (unlikely(value != (func_type) (target_type) value)) {\
                func_type zero = 0;\
                if (exc && unlikely(value == (func_type)-1 && PyErr_Occurred()))\
                    return (target_type) -1;\
                if (is_unsigned && unlikely(value < zero))\
                    goto raise_neg_overflow;\
                else\
                    goto raise_overflow;\
            }\
        }\
        return (target_type) value;\
    }

/* CIntFromPy */
static CYTHON_INLINE int __Pyx_PyInt_As_int(PyObject *x) {
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#endif
    const int neg_one = (int) -1, const_zero = (int) 0;
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic pop
#endif
    const int is_unsigned = neg_one > const_zero;
#if PY_MAJOR_VERSION < 3
    if (likely(PyInt_Check(x))) {
        if (sizeof(int) < sizeof(long)) {
            __PYX_VERIFY_RETURN_INT(int, long, PyInt_AS_LONG(x))
        } else {
            long val = PyInt_AS_LONG(x);
            if (is_unsigned && unlikely(val < 0)) {
                goto raise_neg_overflow;
            }
            return (int) val;
        }
    } else
#endif
    if (likely(PyLong_Check(x))) {
        if (is_unsigned) {
#if CYTHON_USE_PYLONG_INTERNALS
            const digit* digits = ((PyLongObject*)x)->ob_digit;
            switch (Py_SIZE(x)) {
                case  0: return (int) 0;
                case  1: __PYX_VERIFY_RETURN_INT(int, digit, digits[0])
                case 2:
                    if (8 * sizeof(int) > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) >= 2 * PyLong_SHIFT) {
                            return (int) (((((int)digits[1]) << PyLong_SHIFT) | (int)digits[0]));
                        }
                    }
                    break;
                case 3:
                    if (8 * sizeof(int) > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) >= 3 * PyLong_SHIFT) {
                            return (int) (((((((int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0]));
                        }
                    }
                    break;
                case 4:
                    if (8 * sizeof(int) > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) >= 4 * PyLong_SHIFT) {
                            return (int) (((((((((int)digits[3]) << PyLong_SHIFT) | (int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0]));
                        }
                    }
                    break;
            }
#endif
#if CYTHON_COMPILING_IN_CPYTHON
            if (unlikely(Py_SIZE(x) < 0)) {
                goto raise_neg_overflow;
            }
#else
            {
                int result = PyObject_RichCompareBool(x, Py_False, Py_LT);
                if (unlikely(result < 0))
                    return (int) -1;
                if (unlikely(result == 1))
                    goto raise_neg_overflow;
            }
#endif
            if (sizeof(int) <= sizeof(unsigned long)) {
                __PYX_VERIFY_RETURN_INT_EXC(int, unsigned long, PyLong_AsUnsignedLong(x))
#ifdef HAVE_LONG_LONG
            } else if (sizeof(int) <= sizeof(unsigned PY_LONG_LONG)) {
                __PYX_VERIFY_RETURN_INT_EXC(int, unsigned PY_LONG_LONG, PyLong_AsUnsignedLongLong(x))
#endif
            }
        } else {
#if CYTHON_USE_PYLONG_INTERNALS
            const digit* digits = ((PyLongObject*)x)->ob_digit;
            switch (Py_SIZE(x)) {
                case  0: return (int) 0;
                case -1: __PYX_VERIFY_RETURN_INT(int, sdigit, (sdigit) (-(sdigit)digits[0]))
                case  1: __PYX_VERIFY_RETURN_INT(int,  digit, +digits[0])
                case -2:
                    if (8 * sizeof(int) - 1 > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, long, -(long) (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 2 * PyLong_SHIFT) {
                            return (int) (((int)-1)*(((((int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
                case 2:
                    if (8 * sizeof(int) > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 2 * PyLong_SHIFT) {
                            return (int) ((((((int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
                case -3:
                    if (8 * sizeof(int) - 1 > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, long, -(long) (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 3 * PyLong_SHIFT) {
                            return (int) (((int)-1)*(((((((int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
                case 3:
                    if (8 * sizeof(int) > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 3 * PyLong_SHIFT) {
                            return (int) ((((((((int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
                case -4:
                    if (8 * sizeof(int) - 1 > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, long, -(long) (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 4 * PyLong_SHIFT) {
                            return (int) (((int)-1)*(((((((((int)digits[3]) << PyLong_SHIFT) | (int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
                case 4:
                    if (8 * sizeof(int) > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 4 * PyLong_SHIFT) {
                            return (int) ((((((((((int)digits[3]) << PyLong_SHIFT) | (int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
            }
#endif
            if (sizeof(int) <= sizeof(long)) {
                __PYX_VERIFY_RETURN_INT_EXC(int, long, PyLong_AsLong(x))
#ifdef HAVE_LONG_LONG
            } else if (sizeof(int) <= sizeof(PY_LONG_LONG)) {
                __PYX_VERIFY_RETURN_INT_EXC(int, PY_LONG_LONG, PyLong_AsLongLong(x))
#endif
            }
        }
        {
#if CYTHON_COMPILING_IN_PYPY && !defined(_PyLong_AsByteArray)
            PyErr_SetString(PyExc_RuntimeError,
                            "_PyLong_AsByteArray() not available in PyPy, cannot convert large numbers");
#else
            int val;
            PyObject *v = __Pyx_PyNumber_IntOrLong(x);
 #if PY_MAJOR_VERSION < 3
            if (likely(v) && !PyLong_Check(v)) {
                PyObject *tmp = v;
                v = PyNumber_Long(tmp);
                Py_DECREF(tmp);
            }
 #endif
            if (likely(v)) {
                int one = 1; int is_little = (int)*(unsigned char *)&one;
                unsigned char *bytes = (unsigned char *)&val;
                int ret = _PyLong_AsByteArray((PyLongObject *)v,
                                              bytes, sizeof(val),
                                              is_little, !is_unsigned);
                Py_DECREF(v);
                if (likely(!ret))
                    return val;
            }
#endif
            return (int) -1;
        }
    } else {
        int val;
        PyObject *tmp = __Pyx_PyNumber_IntOrLong(x);
        if (!tmp) return (int) -1;
        val = __Pyx_PyInt_As_int(tmp);
        Py_DECREF(tmp);
        return val;
    }
raise_overflow:
    PyErr_SetString(PyExc_OverflowError,
        "value too large to convert to int");
    return (int) -1;
raise_neg_overflow:
    PyErr_SetString(PyExc_OverflowError,
        "can't convert negative value to int");
    return (int) -1;
}

/* CIntToPy */
static CYTHON_INLINE PyObject* __Pyx_PyInt_From_int(int value) {
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#endif
    const int neg_one = (int) -1, const_zero = (int) 0;
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic pop
#endif
    const int is_unsigned = neg_one > const_zero;
    if (is_unsigned) {
        if (sizeof(int) < sizeof(long)) {
            return PyInt_FromLong((long) value);
        } else if (sizeof(int) <= sizeof(unsigned long)) {
            return PyLong_FromUnsignedLong((unsigned long) value);
#ifdef HAVE_LONG_LONG
        } else if (sizeof(int) <= sizeof(unsigned PY_LONG_LONG)) {
            return PyLong_FromUnsignedLongLong((unsigned PY_LONG_LONG) value);
#endif
        }
    } else {
        if (sizeof(int) <= sizeof(long)) {
            return PyInt_FromLong((long) value);
#ifdef HAVE_LONG_LONG
        } else if (sizeof(int) <= sizeof(PY_LONG_LONG)) {
            return PyLong_FromLongLong((PY_LONG_LONG) value);
#endif
        }
    }
    {
        int one = 1; int little = (int)*(unsigned char *)&one;
        unsigned char *bytes = (unsigned char *)&value;
        return _PyLong_FromByteArray(bytes, sizeof(int),
                                     little, !is_unsigned);
    }
}

/* CIntToPy */
static CYTHON_INLINE PyObject* __Pyx_PyInt_From_long(long value) {
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#endif
    const long neg_one = (long) -1, const_zero = (long) 0;
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic pop
#endif
    const int is_unsigned = neg_one > const_zero;
    if (is_unsigned) {
        if (sizeof(long) < sizeof(long)) {
            return PyInt_FromLong((long) value);
        } else if (sizeof(long) <= sizeof(unsigned long)) {
            return PyLong_FromUnsignedLong((unsigned long) value);
#ifdef HAVE_LONG_LONG
        } else if (sizeof(long) <= sizeof(unsigned PY_LONG_LONG)) {
            return PyLong_FromUnsignedLongLong((unsigned PY_LONG_LONG) value);
#endif
        }
    } else {
        if (sizeof(long) <= sizeof(long)) {
            return PyInt_FromLong((long) value);
#ifdef HAVE_LONG_LONG
        } else if (sizeof(long) <= sizeof(PY_LONG_LONG)) {
            return PyLong_FromLongLong((PY_LONG_LONG) value);
#endif
        }
    }
    {
        int one = 1; int little = (int)*(unsigned char *)&one;
        unsigned char *bytes = (unsigned char *)&value;
        return _PyLong_FromByteArray(bytes, sizeof(long),
                                     little, !is_unsigned);
    }
}

/* CIntFromPy */
static CYTHON_INLINE long __Pyx_PyInt_As_long(PyObject *x) {
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#endif
    const long neg_one = (long) -1, const_zero = (long) 0;
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic pop
#endif
    const int is_unsigned = neg_one > const_zero;
#if PY_MAJOR_VERSION < 3
    if (likely(PyInt_Check(x))) {
        if (sizeof(long) < sizeof(long)) {
            __PYX_VERIFY_RETURN_INT(long, long, PyInt_AS_LONG(x))
        } else {
            long val = PyInt_AS_LONG(x);
            if (is_unsigned && unlikely(val < 0)) {
                goto raise_neg_overflow;
            }
            return (long) val;
        }
    } else
#endif
    if (likely(PyLong_Check(x))) {
        if (is_unsigned) {
#if CYTHON_USE_PYLONG_INTERNALS
            const digit* digits = ((PyLongObject*)x)->ob_digit;
            switch (Py_SIZE(x)) {
                case  0: return (long) 0;
                case  1: __PYX_VERIFY_RETURN_INT(long, digit, digits[0])
                case 2:
                    if (8 * sizeof(long) > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) >= 2 * PyLong_SHIFT) {
                            return (long) (((((long)digits[1]) << PyLong_SHIFT) | (long)digits[0]));
                        }
                    }
                    break;
                case 3:
                    if (8 * sizeof(long) > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) >= 3 * PyLong_SHIFT) {
                            return (long) (((((((long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0]));
                        }
                    }
                    break;
                case 4:
                    if (8 * sizeof(long) > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) >= 4 * PyLong_SHIFT) {
                            return (long) (((((((((long)digits[3]) << PyLong_SHIFT) | (long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0]));
                        }
                    }
                    break;
            }
#endif
#if CYTHON_COMPILING_IN_CPYTHON
            if (unlikely(Py_SIZE(x) < 0)) {
                goto raise_neg_overflow;
            }
#else
            {
                int result = PyObject_RichCompareBool(x, Py_False, Py_LT);
                if (unlikely(result < 0))
                    return (long) -1;
                if (unlikely(result == 1))
                    goto raise_neg_overflow;
            }
#endif
            if (sizeof(long) <= sizeof(unsigned long)) {
                __PYX_VERIFY_RETURN_INT_EXC(long, unsigned long, PyLong_AsUnsignedLong(x))
#ifdef HAVE_LONG_LONG
            } else if (sizeof(long) <= sizeof(unsigned PY_LONG_LONG)) {
                __PYX_VERIFY_RETURN_INT_EXC(long, unsigned PY_LONG_LONG, PyLong_AsUnsignedLongLong(x))
#endif
            }
        } else {
#if CYTHON_USE_PYLONG_INTERNALS
            const digit* digits = ((PyLongObject*)x)->ob_digit;
            switch (Py_SIZE(x)) {
                case  0: return (long) 0;
                case -1: __PYX_VERIFY_RETURN_INT(long, sdigit, (sdigit) (-(sdigit)digits[0]))
                case  1: __PYX_VERIFY_RETURN_INT(long,  digit, +digits[0])
                case -2:
                    if (8 * sizeof(long) - 1 > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, long, -(long) (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 2 * PyLong_SHIFT) {
                            return (long) (((long)-1)*(((((long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
                case 2:
                    if (8 * sizeof(long) > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 2 * PyLong_SHIFT) {
                            return (long) ((((((long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
                case -3:
                    if (8 * sizeof(long) - 1 > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, long, -(long) (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 3 * PyLong_SHIFT) {
                            return (long) (((long)-1)*(((((((long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
                case 3:
                    if (8 * sizeof(long) > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 3 * PyLong_SHIFT) {
                            return (long) ((((((((long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
                case -4:
                    if (8 * sizeof(long) - 1 > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, long, -(long) (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 4 * PyLong_SHIFT) {
                            return (long) (((long)-1)*(((((((((long)digits[3]) << PyLong_SHIFT) | (long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
                case 4:
                    if (8 * sizeof(long) > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 4 * PyLong_SHIFT) {
                            return (long) ((((((((((long)digits[3]) << PyLong_SHIFT) | (long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
            }
#endif
            if (sizeof(long) <= sizeof(long)) {
                __PYX_VERIFY_RETURN_INT_EXC(long, long, PyLong_AsLong(x))
#ifdef HAVE_LONG_LONG
            } else if (sizeof(long) <= sizeof(PY_LONG_LONG)) {
                __PYX_VERIFY_RETURN_INT_EXC(long, PY_LONG_LONG, PyLong_AsLongLong(x))
#endif
            }
        }
        {
#if CYTHON_COMPILING_IN_PYPY && !defined(_PyLong_AsByteArray)
            PyErr_SetString(PyExc_RuntimeError,
                            "_PyLong_AsByteArray() not available in PyPy, cannot convert large numbers");
#else
            long val;
            PyObject *v = __Pyx_PyNumber_IntOrLong(x);
 #if PY_MAJOR_VERSION < 3
            if (likely(v) && !PyLong_Check(v)) {
                PyObject *tmp = v;
                v = PyNumber_Long(tmp);
                Py_DECREF(tmp);
            }
 #endif
            if (likely(v)) {
                int one = 1; int is_little = (int)*(unsigned char *)&one;
                unsigned char *bytes = (unsigned char *)&val;
                int ret = _PyLong_AsByteArray((PyLongObject *)v,
                                              bytes, sizeof(val),
                                              is_little, !is_unsigned);
                Py_DECREF(v);
                if (likely(!ret))
                    return val;
            }
#endif
            return (long) -1;
        }
    } else {
        long val;
        PyObject *tmp = __Pyx_PyNumber_IntOrLong(x);
        if (!tmp) return (long) -1;
        val = __Pyx_PyInt_As_long(tmp);
        Py_DECREF(tmp);
        return val;
    }
raise_overflow:
    PyErr_SetString(PyExc_OverflowError,
        "value too large to convert to long");
    return (long) -1;
raise_neg_overflow:
    PyErr_SetString(PyExc_OverflowError,
        "can't convert negative value to long");
    return (long) -1;
}

/* FastTypeChecks */
#if CYTHON_COMPILING_IN_CPYTHON
static int __Pyx_InBases(PyTypeObject *a, PyTypeObject *b) {
    while (a) {
        a = a->tp_base;
        if (a == b)
            return 1;
    }
    return b == &PyBaseObject_Type;
}
static CYTHON_INLINE int __Pyx_IsSubtype(PyTypeObject *a, PyTypeObject *b) {
    PyObject *mro;
    if (a == b) return 1;
    mro = a->tp_mro;
    if (likely(mro)) {
        Py_ssize_t i, n;
        n = PyTuple_GET_SIZE(mro);
        for (i = 0; i < n; i++) {
            if (PyTuple_GET_ITEM(mro, i) == (PyObject *)b)
                return 1;
        }
        return 0;
    }
    return __Pyx_InBases(a, b);
}
#if PY_MAJOR_VERSION == 2
static int __Pyx_inner_PyErr_GivenExceptionMatches2(PyObject *err, PyObject* exc_type1, PyObject* exc_type2) {
    PyObject *exception, *value, *tb;
    int res;
    __Pyx_PyThreadState_declare
    __Pyx_PyThreadState_assign
    __Pyx_ErrFetch(&exception, &value, &tb);
    res = exc_type1 ? PyObject_IsSubclass(err, exc_type1) : 0;
    if (unlikely(res == -1)) {
        PyErr_WriteUnraisable(err);
        res = 0;
    }
    if (!res) {
        res = PyObject_IsSubclass(err, exc_type2);
        if (unlikely(res == -1)) {
            PyErr_WriteUnraisable(err);
            res = 0;
        }
    }
    __Pyx_ErrRestore(exception, value, tb);
    return res;
}
#else
static CYTHON_INLINE int __Pyx_inner_PyErr_GivenExceptionMatches2(PyObject *err, PyObject* exc_type1, PyObject *exc_type2) {
    int res = exc_type1 ? __Pyx_IsSubtype((PyTypeObject*)err, (PyTypeObject*)exc_type1) : 0;
    if (!res) {
        res = __Pyx_IsSubtype((PyTypeObject*)err, (PyTypeObject*)exc_type2);
    }
    return res;
}
#endif
static int __Pyx_PyErr_GivenExceptionMatchesTuple(PyObject *exc_type, PyObject *tuple) {
    Py_ssize_t i, n;
    assert(PyExceptionClass_Check(exc_type));
    n = PyTuple_GET_SIZE(tuple);
#if PY_MAJOR_VERSION >= 3
    for (i=0; i<n; i++) {
        if (exc_type == PyTuple_GET_ITEM(tuple, i)) return 1;
    }
#endif
    for (i=0; i<n; i++) {
        PyObject *t = PyTuple_GET_ITEM(tuple, i);
        #if PY_MAJOR_VERSION < 3
        if (likely(exc_type == t)) return 1;
        #endif
        if (likely(PyExceptionClass_Check(t))) {
            if (__Pyx_inner_PyErr_GivenExceptionMatches2(exc_type, NULL, t)) return 1;
        } else {
        }
    }
    return 0;
}
static CYTHON_INLINE int __Pyx_PyErr_GivenExceptionMatches(PyObject *err, PyObject* exc_type) {
    if (likely(err == exc_type)) return 1;
    if (likely(PyExceptionClass_Check(err))) {
        if (likely(PyExceptionClass_Check(exc_type))) {
            return __Pyx_inner_PyErr_GivenExceptionMatches2(err, NULL, exc_type);
        } else if (likely(PyTuple_Check(exc_type))) {
            return __Pyx_PyErr_GivenExceptionMatchesTuple(err, exc_type);
        } else {
        }
    }
    return PyErr_GivenExceptionMatches(err, exc_type);
}
static CYTHON_INLINE int __Pyx_PyErr_GivenExceptionMatches2(PyObject *err, PyObject *exc_type1, PyObject *exc_type2) {
    assert(PyExceptionClass_Check(exc_type1));
    assert(PyExceptionClass_Check(exc_type2));
    if (likely(err == exc_type1 || err == exc_type2)) return 1;
    if (likely(PyExceptionClass_Check(err))) {
        return __Pyx_inner_PyErr_GivenExceptionMatches2(err, exc_type1, exc_type2);
    }
    return (PyErr_GivenExceptionMatches(err, exc_type1) || PyErr_GivenExceptionMatches(err, exc_type2));
}
#endif

/* CheckBinaryVersion */
static int __Pyx_check_binary_version(void) {
    char ctversion[4], rtversion[4];
    PyOS_snprintf(ctversion, 4, "%d.%d", PY_MAJOR_VERSION, PY_MINOR_VERSION);
    PyOS_snprintf(rtversion, 4, "%s", Py_GetVersion());
    if (ctversion[0] != rtversion[0] || ctversion[2] != rtversion[2]) {
        char message[200];
        PyOS_snprintf(message, sizeof(message),
                      "compiletime version %s of module '%.100s' "
                      "does not match runtime version %s",
                      ctversion, __Pyx_MODULE_NAME, rtversion);
        return PyErr_WarnEx(NULL, message, 1);
    }
    return 0;
}

/* FunctionImport */
#ifndef __PYX_HAVE_RT_ImportFunction
#define __PYX_HAVE_RT_ImportFunction
static int __Pyx_ImportFunction(PyObject *module, const char *funcname, void (**f)(void), const char *sig) {
    PyObject *d = 0;
    PyObject *cobj = 0;
    union {
        void (*fp)(void);
        void *p;
    } tmp;
    d = PyObject_GetAttrString(module, (char *)"__pyx_capi__");
    if (!d)
        goto bad;
    cobj = PyDict_GetItemString(d, funcname);
    if (!cobj) {
        PyErr_Format(PyExc_ImportError,
            "%.200s does not export expected C function %.200s",
                PyModule_GetName(module), funcname);
        goto bad;
    }
#if PY_VERSION_HEX >= 0x02070000
    if (!PyCapsule_IsValid(cobj, sig)) {
        PyErr_Format(PyExc_TypeError,
            "C function %.200s.%.200s has wrong signature (expected %.500s, got %.500s)",
             PyModule_GetName(module), funcname, sig, PyCapsule_GetName(cobj));
        goto bad;
    }
    tmp.p = PyCapsule_GetPointer(cobj, sig);
#else
    {const char *desc, *s1, *s2;
    desc = (const char *)PyCObject_GetDesc(cobj);
    if (!desc)
        goto bad;
    s1 = desc; s2 = sig;
    while (*s1 != '\0' && *s1 == *s2) { s1++; s2++; }
    if (*s1 != *s2) {
        PyErr_Format(PyExc_TypeError,
            "C function %.200s.%.200s has wrong signature (expected %.500s, got %.500s)",
             PyModule_GetName(module), funcname, sig, desc);
        goto bad;
    }
    tmp.p = PyCObject_AsVoidPtr(cobj);}
#endif
    *f = tmp.fp;
    if (!(*f))
        goto bad;
    Py_DECREF(d);
    return 0;
bad:
    Py_XDECREF(d);
    return -1;
}
#endif

/* InitStrings */
static int __Pyx_InitStrings(__Pyx_StringTabEntry *t) {
    while (t->p) {
        #if PY_MAJOR_VERSION < 3
        if (t->is_unicode) {
            *t->p = PyUnicode_DecodeUTF8(t->s, t->n - 1, NULL);
        } else if (t->intern) {
            *t->p = PyString_InternFromString(t->s);
        } else {
            *t->p = PyString_FromStringAndSize(t->s, t->n - 1);
        }
        #else
        if (t->is_unicode | t->is_str) {
            if (t->intern) {
                *t->p = PyUnicode_InternFromString(t->s);
            } else if (t->encoding) {
                *t->p = PyUnicode_Decode(t->s, t->n - 1, t->encoding, NULL);
            } else {
                *t->p = PyUnicode_FromStringAndSize(t->s, t->n - 1);
            }
        } else {
            *t->p = PyBytes_FromStringAndSize(t->s, t->n - 1);
        }
        #endif
        if (!*t->p)
            return -1;
        if (PyObject_Hash(*t->p) == -1)
            return -1;
        ++t;
    }
    return 0;
}

static CYTHON_INLINE PyObject* __Pyx_PyUnicode_FromString(const char* c_str) {
    return __Pyx_PyUnicode_FromStringAndSize(c_str, (Py_ssize_t)strlen(c_str));
}
static CYTHON_INLINE const char* __Pyx_PyObject_AsString(PyObject* o) {
    Py_ssize_t ignore;
    return __Pyx_PyObject_AsStringAndSize(o, &ignore);
}
#if __PYX_DEFAULT_STRING_ENCODING_IS_ASCII || __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT
#if !CYTHON_PEP393_ENABLED
static const char* __Pyx_PyUnicode_AsStringAndSize(PyObject* o, Py_ssize_t *length) {
    char* defenc_c;
    PyObject* defenc = _PyUnicode_AsDefaultEncodedString(o, NULL);
    if (!defenc) return NULL;
    defenc_c = PyBytes_AS_STRING(defenc);
#if __PYX_DEFAULT_STRING_ENCODING_IS_ASCII
    {
        char* end = defenc_c + PyBytes_GET_SIZE(defenc);
        char* c;
        for (c = defenc_c; c < end; c++) {
            if ((unsigned char) (*c) >= 128) {
                PyUnicode_AsASCIIString(o);
                return NULL;
            }
        }
    }
#endif
    *length = PyBytes_GET_SIZE(defenc);
    return defenc_c;
}
#else
static CYTHON_INLINE const char* __Pyx_PyUnicode_AsStringAndSize(PyObject* o, Py_ssize_t *length) {
    if (unlikely(__Pyx_PyUnicode_READY(o) == -1)) return NULL;
#if __PYX_DEFAULT_STRING_ENCODING_IS_ASCII
    if (likely(PyUnicode_IS_ASCII(o))) {
        *length = PyUnicode_GET_LENGTH(o);
        return PyUnicode_AsUTF8(o);
    } else {
        PyUnicode_AsASCIIString(o);
        return NULL;
    }
#else
    return PyUnicode_AsUTF8AndSize(o, length);
#endif
}
#endif
#endif
static CYTHON_INLINE const char* __Pyx_PyObject_AsStringAndSize(PyObject* o, Py_ssize_t *length) {
#if __PYX_DEFAULT_STRING_ENCODING_IS_ASCII || __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT
    if (
#if PY_MAJOR_VERSION < 3 && __PYX_DEFAULT_STRING_ENCODING_IS_ASCII
            __Pyx_sys_getdefaultencoding_not_ascii &&
#endif
            PyUnicode_Check(o)) {
        return __Pyx_PyUnicode_AsStringAndSize(o, length);
    } else
#endif
#if (!CYTHON_COMPILING_IN_PYPY) || (defined(PyByteArray_AS_STRING) && defined(PyByteArray_GET_SIZE))
    if (PyByteArray_Check(o)) {
        *length = PyByteArray_GET_SIZE(o);
        return PyByteArray_AS_STRING(o);
    } else
#endif
    {
        char* result;
        int r = PyBytes_AsStringAndSize(o, &result, length);
        if (unlikely(r < 0)) {
            return NULL;
        } else {
            return result;
        }
    }
}
static CYTHON_INLINE int __Pyx_PyObject_IsTrue(PyObject* x) {
   int is_true = x == Py_True;
   if (is_true | (x == Py_False) | (x == Py_None)) return is_true;
   else return PyObject_IsTrue(x);
}
static CYTHON_INLINE int __Pyx_PyObject_IsTrueAndDecref(PyObject* x) {
    int retval;
    if (unlikely(!x)) return -1;
    retval = __Pyx_PyObject_IsTrue(x);
    Py_DECREF(x);
    return retval;
}
static PyObject* __Pyx_PyNumber_IntOrLongWrongResultType(PyObject* result, const char* type_name) {
#if PY_MAJOR_VERSION >= 3
    if (PyLong_Check(result)) {
        if (PyErr_WarnFormat(PyExc_DeprecationWarning, 1,
                "__int__ returned non-int (type %.200s).  "
                "The ability to return an instance of a strict subclass of int "
                "is deprecated, and may be removed in a future version of Python.",
                Py_TYPE(result)->tp_name)) {
            Py_DECREF(result);
            return NULL;
        }
        return result;
    }
#endif
    PyErr_Format(PyExc_TypeError,
                 "__%.4s__ returned non-%.4s (type %.200s)",
                 type_name, type_name, Py_TYPE(result)->tp_name);
    Py_DECREF(result);
    return NULL;
}
static CYTHON_INLINE PyObject* __Pyx_PyNumber_IntOrLong(PyObject* x) {
#if CYTHON_USE_TYPE_SLOTS
  PyNumberMethods *m;
#endif
  const char *name = NULL;
  PyObject *res = NULL;
#if PY_MAJOR_VERSION < 3
  if (likely(PyInt_Check(x) || PyLong_Check(x)))
#else
  if (likely(PyLong_Check(x)))
#endif
    return __Pyx_NewRef(x);
#if CYTHON_USE_TYPE_SLOTS
  m = Py_TYPE(x)->tp_as_number;
  #if PY_MAJOR_VERSION < 3
  if (m && m->nb_int) {
    name = "int";
    res = m->nb_int(x);
  }
  else if (m && m->nb_long) {
    name = "long";
    res = m->nb_long(x);
  }
  #else
  if (likely(m && m->nb_int)) {
    name = "int";
    res = m->nb_int(x);
  }
  #endif
#else
  if (!PyBytes_CheckExact(x) && !PyUnicode_CheckExact(x)) {
    res = PyNumber_Int(x);
  }
#endif
  if (likely(res)) {
#if PY_MAJOR_VERSION < 3
    if (unlikely(!PyInt_Check(res) && !PyLong_Check(res))) {
#else
    if (unlikely(!PyLong_CheckExact(res))) {
#endif
        return __Pyx_PyNumber_IntOrLongWrongResultType(res, name);
    }
  }
  else if (!PyErr_Occurred()) {
    PyErr_SetString(PyExc_TypeError,
                    "an integer is required");
  }
  return res;
}
static CYTHON_INLINE Py_ssize_t __Pyx_PyIndex_AsSsize_t(PyObject* b) {
  Py_ssize_t ival;
  PyObject *x;
#if PY_MAJOR_VERSION < 3
  if (likely(PyInt_CheckExact(b))) {
    if (sizeof(Py_ssize_t) >= sizeof(long))
        return PyInt_AS_LONG(b);
    else
        return PyInt_AsSsize_t(b);
  }
#endif
  if (likely(PyLong_CheckExact(b))) {
    #if CYTHON_USE_PYLONG_INTERNALS
    const digit* digits = ((PyLongObject*)b)->ob_digit;
    const Py_ssize_t size = Py_SIZE(b);
    if (likely(__Pyx_sst_abs(size) <= 1)) {
        ival = likely(size) ? digits[0] : 0;
        if (size == -1) ival = -ival;
        return ival;
    } else {
      switch (size) {
         case 2:
           if (8 * sizeof(Py_ssize_t) > 2 * PyLong_SHIFT) {
             return (Py_ssize_t) (((((size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
         case -2:
           if (8 * sizeof(Py_ssize_t) > 2 * PyLong_SHIFT) {
             return -(Py_ssize_t) (((((size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
         case 3:
           if (8 * sizeof(Py_ssize_t) > 3 * PyLong_SHIFT) {
             return (Py_ssize_t) (((((((size_t)digits[2]) << PyLong_SHIFT) | (size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
         case -3:
           if (8 * sizeof(Py_ssize_t) > 3 * PyLong_SHIFT) {
             return -(Py_ssize_t) (((((((size_t)digits[2]) << PyLong_SHIFT) | (size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
         case 4:
           if (8 * sizeof(Py_ssize_t) > 4 * PyLong_SHIFT) {
             return (Py_ssize_t) (((((((((size_t)digits[3]) << PyLong_SHIFT) | (size_t)digits[2]) << PyLong_SHIFT) | (size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
         case -4:
           if (8 * sizeof(Py_ssize_t) > 4 * PyLong_SHIFT) {
             return -(Py_ssize_t) (((((((((size_t)digits[3]) << PyLong_SHIFT) | (size_t)digits[2]) << PyLong_SHIFT) | (size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
      }
    }
    #endif
    return PyLong_AsSsize_t(b);
  }
  x = PyNumber_Index(b);
  if (!x) return -1;
  ival = PyInt_AsSsize_t(x);
  Py_DECREF(x);
  return ival;
}
static CYTHON_INLINE PyObject * __Pyx_PyBool_FromLong(long b) {
  return b ? __Pyx_NewRef(Py_True) : __Pyx_NewRef(Py_False);
}
static CYTHON_INLINE PyObject * __Pyx_PyInt_FromSize_t(size_t ival) {
    return PyInt_FromSize_t(ival);
}


#endif /* Py_PYTHON_H */
