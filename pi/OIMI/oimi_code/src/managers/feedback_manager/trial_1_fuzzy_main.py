""" Testing trials for fuzzy variables"""
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
from pprint import pprint
from terms import Term
from variables import FuzzyVariable
from mamdani_fs import MamdaniFuzzySystem
from mf import TriangularMF
import matplotlib.pyplot as plt
import numpy as np
# ==========================================================================================================================================================
# Vars
# ==========================================================================================================================================================
t1 = Term('mf1new', TriangularMF(0, 0, 0.5))
t2 = Term('mf2', TriangularMF(0, 0.5, 1))
t3 = Term('mf3', TriangularMF(0.5, 1, 1))

#print(t1._name)

input1: FuzzyVariable = FuzzyVariable('input1', 0, 1, t1, t2, t3)
input2: FuzzyVariable = FuzzyVariable(
    'input2', 0, 1,
    Term('mf1', TriangularMF(0, 0, 0.5)),
    Term('mf2', TriangularMF(0, 0.5, 1)),
    Term('mf3', TriangularMF(0.5, 1, 1))
)
input3: FuzzyVariable = FuzzyVariable(
    'input3', 0, 1,
    Term('mf11', TriangularMF(0, 0, 0.5)),
    Term('mf12', TriangularMF(0, 0.5, 1)),
    Term('mf13', TriangularMF(0.5, 1, 1)),
    Term('mf14', TriangularMF(0.5, 1, 1))
)
input4: FuzzyVariable = FuzzyVariable(
    'input4', 0, 1,
    Term('mf21', TriangularMF(0, 0, 0.5)),
    Term('mf22', TriangularMF(0, 0.5, 1)),
    Term('mf23', TriangularMF(0.5, 1, 1)),
    Term('mf24', TriangularMF(0.5, 1, 1))
)
output = FuzzyVariable(
    'output', 0, 1,
    Term('oo1', TriangularMF(0, 0, 0.5)),
    Term('oo2', TriangularMF(0, 0.5, 1)),
    Term('oo3', TriangularMF(0.5, 1, 1)),
    Term('oo4', TriangularMF(0.5, 1, 1)),
)

mf: MamdaniFuzzySystem = MamdaniFuzzySystem([input1, input2, input3, input4], [output])
mf.rules.append(mf.parse_rule('if (input1 is mf3) and ((input2 is mf3) or (input2 is mf2)) then (output is oo4)'))
mf.rules.append(mf.parse_rule('if (input1 is mf3) and ((input3 is mf12) or (input2 is mf2)) then (output is oo4)'))

result,fuzzy_result,conclusions = mf.calculate({input1: 0.45, input2: 1, input3: 0.45, input4: 0.45})
print("RES::: ", result)
print(fuzzy_result)
for key in fuzzy_result.keys():
    print(key.values[0]._name)
    print(key.values[1]._name)
    print(key.values[2]._name)
    print(fuzzy_result[key])
    print(fuzzy_result[key].get_value(0.2))
    print(fuzzy_result[key].get_value(0.5))
    print(fuzzy_result[key].get_value(0.8))
print(type(fuzzy_result))
print()
print()
print(result.values())
print(list(result))
a = [result[k] for k in result]
b = a[0]
print("b ===========> ", b)

#if 0 < b < 0.5:
#    term_name_output = 'oo1'
#if b ==0.5:
#    term_name_output = 'oo2'
#if 0.5 < b < 1:
#    term_name_output = 'oo3'
#
print(term_name_output)
print(conclusions)
for key in conclusions.keys():
    print(key)
    print(conclusions[key].get_value(0.3))
#pprint(result)
#print(conditions)
#print(conclusions)
#print(type(result))
#print(fi)