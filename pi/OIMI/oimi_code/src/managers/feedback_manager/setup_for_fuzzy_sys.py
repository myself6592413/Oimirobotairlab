# ==========================================================================================================================================================
#  Imports:
# ==========================================================================================================================================================
import os
import path
import io
import sys
import shutil
from distutils.core import setup
import setuptools
from distutils.extension import Extension
from Cython.Build import build_ext
from Cython.Build import cythonize
import numpy
from multiprocessing import Pool

readme_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), './README_fuzzy.md')
#to consider each pxd file that is in other folders
included_path_0 = os.path.abspath('./fuzzy_controller/')
included_path_1 = os.path.abspath('/home/pi/OIMI/oimi_code/src/managers/database_manager/')
# ==========================================================================================================================================================
#  Vars:
# ==========================================================================================================================================================
if 'SRC_PATH' not in os.environ:
	os.environ['included_path_0'] = included_path_0
	os.environ['included_path_1'] = included_path_1
else:
	included_path_0 = os.environ['included_path_0']
	included_path_1 = os.environ['included_path_1']
# ==========================================================================================================================================================
#  Extension_Modules:
# ==========================================================================================================================================================
ext_modules = [
	Extension("feedback_manager", ["feedback_manager.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1],
		language='c++'),
	Extension("fuzzy_controller_takeout", ["fuzzy_controller/fuzzy_controller_takeout.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1],
		language='c++'),
	Extension("fuzzy_controller_evaluate_question", ["fuzzy_controller/fuzzy_controller_evaluate_question.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1],
		language='c++'),
	Extension("fuzzy_controller_evaluate_game", ["fuzzy_controller/fuzzy_controller_evaluate_game.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1],
		language='c++'),
	Extension("fuzzy_controller_evaluate_match", ["fuzzy_controller/fuzzy_controller_evaluate_match.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1],
		language='c++'),
	Extension("fuzzy_controller_evaluate_session", ["fuzzy_controller/fuzzy_controller_evaluate_session.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1],
		language='c++'),
	Extension("fuzzy_controller_insert", ["fuzzy_controller/fuzzy_controller_insert.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1],
		language='c++'),
	Extension("fuzzy_controller_evaluate_feedbacks_whole_session", ["fuzzy_controller/fuzzy_controller_evaluate_feedbacks_whole_session.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(), included_path_0, included_path_1],
		language='c++'),
	]		
# ==========================================================================================================================================================
#  Class:
# ==========================================================================================================================================================
class BuildExt(build_ext):
	""" Extend Ctyhon build_ext for removing annoying warnings"""
	def build_extensions(self):
		if '-Wstrict-prototypes' in self.compiler.compiler_so:
			self.compiler.compiler_so.remove('-Wstrict-prototypes')
		super().build_extensions()
# ==========================================================================================================================================================
#  Methods:
# ==========================================================================================================================================================
def setup_function():
	if sys.version_info < (3, 5):
		sys.exit("Have you activated the corrent python environment? oimi doesnt support a py version < 3.7")
	#clean()
	for e in ext_modules:
		e.cython_directives = {'embedsignature': True, 'boundscheck': False, 'wraparound': False, 'linetrace': True, 'language_level': "3"}

	setup(
		name='Specific Oimi_Games building package. Compiling and preparing sources file. Binding cython to python...',
		version='2.1.0',
		author='Colombo Giacomo',
		long_description=io.open(readme_file, 'rt', encoding='utf-8').read(),
		ext_modules=ext_modules,
		cmdclass = {'build_ext': BuildExt},
		)
# ==========================================================================================================================================================
#  Main:
# ==========================================================================================================================================================
if __name__=="__main__":
	with Pool(processes=10) as pool:
		res = pool.apply(setup_function)