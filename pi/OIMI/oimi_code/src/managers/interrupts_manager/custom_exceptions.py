""" Oimi Custom Exceptions """
# ===============================================================================================================
#  Imports:
# ===============================================================================================================
import signal
# ===============================================================================================================
#  Classes:
# ===============================================================================================================
class TimeOutException(Exception):
	"""TIME expired!!"""
	pass

class TimeElapsed(Exception):
	"""Nothing to do, no instructions for a lot of time"""
	pass

class StopAllException(Exception):
	"""Stop all exception"""
	pass

class ErrorSerialException(Exception):
	"""Error serial exception"""
	pass

class BlueException(Exception):
	"""Stop all exception"""
	pass

class InternalErrororMethodModality(Exception):
	pass
	
class MovementsError(Exception):
	"""Something happened during movement"""
	def __init__(self, *args):
		if args:
			self.message = args[0]
		else:
			self.message = None

	def __str__(self):
		print('calling str')
		if self.message:
			return 'MovementsError, {0} '.format(self.message)
		else:
			return 'MovementsError has been raised'

class StopAllException(Exception):
	print("simpleStopALLException!")

class StopAllException1(Exception):
	print("simpleStopALLException1!")

class StopAllException2(Exception):
	def __init__(self, *args):
		if args:
			self.message = args[0]
		else:
			self.message = None
	
	def __str__(self):
		print('calling str')
		if self.message:
			return 'StopAllException, {0} '.format(self.message)
		else:
			return 'StopAllException  has been raised'			
# ===============================================================================================================
#  Handlers:
# ===============================================================================================================
def time_handler(signum, frame):
	raise TimeOutException

def sigterm_main_handler(elem, _signo, _stack_frame):
	signal.signal(_signo, signal.SIG_IGN)
	print("Oimi Shutdown")
	raise SystemExit 