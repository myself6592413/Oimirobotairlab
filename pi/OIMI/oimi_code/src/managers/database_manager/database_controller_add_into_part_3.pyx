"""Info:
Oimi robot database_manager, oimi_database.db whole infrastructure creation and maintanance
Notes:
For detailed info, look at ./code_docs_of_dabatase_manager
Author:
Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
import mmap
import random
import pickle
import copy
from importlib import reload
from typing import Optional
import oimi_queries as oq
import db_extern_methods as dbem
cimport modify_default_lists as mdl
cimport database_controller_add_into as dca
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef add_new_Session_table_kid_not_given(db_self): #mandatory impo e need unnecessary
	print("welcome to kid not given sto aggiungendo la session! qui non c'è customized!!!!!")
	all_complex = ['Easy', 'Normal', 'Medium', 'Hard']
	complexity = (random.choice(all_complex))
	mandatory_impositions = 0
	mandatory_needs = 0
	extra_time_tolerance = 0
	movements_allowed = random.randint(0,1)
	stress_flag = 0
	led_brightness = 'high'
	if complexity=='Easy':
		body_enabled = 1
		rules_explication = 1
		frequency_exhortations = 1
		repeat_question, repeat_intro = 1,1
		num_of_matches = random.randint(1,2)
		choose_diff = random.randint(0,6)
		if choose_diff==0:
			order_diff_matches = 'asc'
			order_quan_games = 'same'
		elif choose_diff==1:
			order_diff_matches = 'asc'
			order_quan_games = 'asc'
		elif choose_diff==2 or choose_diff==3:
			order_diff_matches = 'same'
			order_quan_games = 'asc'
		else:
			order_diff_matches = 'same'
			order_quan_games = 'same'
	elif complexity=='Normal':
		body_enabled = 1
		rules_explication = 1
		frequency_exhortations = 1
		repeat_question, repeat_intro = 1,1
		num_of_matches = random.randint(1,3)
		choose_diff = random.randint(0,5)
		choose_quan = random.randint(0,5)
		if choose_diff==0 or choose_diff==1 or choose_diff==2:
			order_diff_matches = 'same'
		elif choose_diff==3 or choose_diff==4:
			order_diff_matches = 'asc'
		elif choose_diff==5:
			order_diff_matches = 'casu'
		if choose_quan==0 or choose_quan==1 or choose_quan==2:
			order_quan_games = 'same'
		elif choose_quan==3 or choose_quan==4:
			order_quan_games = 'asc'
		elif choose_quan==5:
			order_quan_games = 'casu'
	elif complexity=='Medium':
		body_enabled = 1
		rules_explication = random.randint(0,1)
		frequency_exhortations = 0
		repeat_question, repeat_intro = 0,0
		num_of_matches = random.randint(2,4)
		choose_diff = random.randint(0,5)
		choose_quan = random.randint(0,5)
		if choose_diff==0 or choose_diff==1:
			order_diff_matches = 'same'
		elif choose_diff==2 or choose_diff==3:
			order_diff_matches = 'asc'
		elif choose_diff==4:
			order_diff_matches = 'casu'
		elif choose_diff==5:
			order_diff_matches = 'desc'
		if choose_quan==0 or choose_quan==1:
			order_quan_games = 'same'
		elif choose_quan==2 or choose_quan==3:
			order_quan_games = 'asc'
		elif choose_quan==4:
			order_quan_games = 'casu'
		elif choose_quan==5:
			order_quan_games = 'desc'
	elif complexity=='Hard':
		body_enabled = 1
		rules_explication = 0
		frequency_exhortations = 0
		repeat_question, repeat_intro = 1,1
		num_of_matches = random.randint(2,4)
		choose_diff = random.randint(0,5)
		choose_quan = random.randint(0,5)
		if choose_diff==0:
			order_diff_matches = 'same'
		elif choose_diff==1:
			order_diff_matches = 'asc'
		elif choose_diff==2 or choose_diff==3:
			order_diff_matches = 'casu'
		else:
			order_diff_matches = 'desc'
		if choose_quan==0:
			order_quan_games = 'same'
		elif choose_quan==1:
			order_quan_games = 'asc'
		elif choose_quan==2 or choose_quan==3:
			order_quan_games = 'casu'
		else:
			order_quan_games = 'desc'

	status = 'to_do'
	disposable = 0
	desirable_time = 320
	creation_way = 'automatically'
	cdef:
		str new_id = db_self.assign_new_id('Sessions')
		str search_str = "{}, '{}', '{}', {}, {}".format(num_of_matches, complexity, status, mandatory_impositions, mandatory_needs)
		#str search_str = "\t({0}, '{1}', '{2}', {3}),".format(num_of_matches, purpose, difficulty, disposable)
		bint new_del = 0
	#try:
	find_me = search_str.encode()
	print("find_me is {}".format(find_me))
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
		mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
		if s.find(find_me) != -1:
			print('--> The Session you want to insert already exist.')
			#raise Exception
			pass
		reload(oq)
		db_self.curs.execute('''INSERT INTO Sessions (num_of_matches, complexity, status, order_difficulty_matches, order_quantity_games, 
			mandatory_impositions, mandatory_needs, extra_time_tolerance, movements_allowed, body_enabled, 
			rules_explication, stress_flag, frequency_exhortations, repeat_question, repeat_intro, desirable_time, creation_way, disposable, token)
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', (num_of_matches, complexity, status, order_diff_matches, order_quan_games, 
			mandatory_impositions, mandatory_needs, extra_time_tolerance, movements_allowed, body_enabled,
			rules_explication, stress_flag, frequency_exhortations, repeat_question, repeat_intro, desirable_time, creation_way, disposable, new_id,))
		db_self.conn.commit()
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Sessions")
		num_last_line_s = db_self.curs.fetchone()[0]
		mdl.modify_Sessions_list(num_of_matches, complexity, status, order_diff_matches, order_quan_games, mandatory_impositions, mandatory_needs, 
			extra_time_tolerance, movements_allowed, body_enabled, rules_explication, stress_flag, frequency_exhortations, repeat_question, 
			repeat_intro, desirable_time, creation_way, disposable, new_id, num_last_line_s)
		
		db_self.curs.execute("UPDATE Sessions SET date_time = DATETIME('now','localtime') WHERE date_time isNull")
		query_audio_intro = "UPDATE Sessions SET audio_intro = '{}'".format('audio_1')
		query_audio_exit = "UPDATE Sessions SET audio_exit = '{}'".format('audio_2')
		db_self.curs.execute(query_audio_intro)
		db_self.curs.execute(query_audio_exit)
		db_self.conn.commit()
		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
			print("ho trovato questa deletions_list!!!!")
			print("deletions_list = {}".format(db_self.deletions_list))
		if db_self.deletions_list[3]!=[0]:
			new_del = 1
			dbem.bubblesort(db_self.deletions_list[3])
			db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Sessions\'")
			index_to_replace = db_self.curs.fetchone()[1] 
			index_corrected = db_self.deletions_list[3].pop(0)
			db_self.curs.execute('''UPDATE Sessions SET session_id = ?  WHERE session_id = ?''', (index_corrected, index_to_replace))
			if not db_self.deletions_list[3]:
				db_self.deletions_list[3].append(0)
			db_self.conn.commit()
	print("The new Session is now added.")
	#if not kid_id: #da mettere!! siamo sempre in quel caso ...altrimeti tabelle!!
	# here start the creation of matches!!
	db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
	sess_last = db_self.curs.fetchone()[0]
	
	if complexity=='Easy':
		advisable_time = 10
		if num_of_matches==1:
			difficulty_m = 'Easy_1'
			num_of_games = random.randint(1,3)
			if num_of_games==1:
				avail_varieties = ['1F','2L']
			else:
				avail_varieties = ['1F','2L','SUNDRY']
			variety = random.choice(avail_varieties)
			print("variety num_of_matches=1 => ",variety)
			if variety!='SUNDRY':
				criteria_arrangement = 'ignore'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety,))
				scenario = db_self.curs.fetchone()[0]
			else:
				criteria_arrangement = 'regular'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario = db_self.curs.fetchone()[0]
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN complex Easy=> ",scenario)
			ord_casual = random.randint(0,1)
			if ord_casual==0:
				order_diff_games = 'same' 
			elif ord_casual==1:
				order_diff_games = 'asc' 
			ord_casual = random.randint(0,1)
			if ord_casual==0:
				order_quan_ques = 'same' 
			elif ord_casual==1:
				order_quan_ques = 'asc'
			db_self.add_new_Match_table(difficulty_m, num_of_games, variety, order_diff_games, order_quan_ques, criteria_arrangement, advisable_time, scenario, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		elif num_of_matches==2:
			options = ['Easy_1', 'Easy_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m2 = difficulty_m1
			if order_diff_matches=='asc':
				difficulty_m1='Easy_1'
				difficulty_m2='Easy_2'
			if order_diff_matches=='desc':
				difficulty_m1='Easy_2'
				difficulty_m2='Easy_1'
			if order_quan_games=='same':
				casual = random.randint(0,3)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
				elif casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
			elif order_quan_games=='casu':
				casual = random.randint(0,11)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
				elif casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==4 or casual==5:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==6:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
				elif casual==7:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
				elif casual==8 or casual==9:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==10 or casual==11:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
			elif order_quan_games=='asc':
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==3:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
			elif order_quan_games=='desc':
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				elif casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
			
			if difficulty_m1=='Easy_1':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m1 = 'same' 
				elif ord_casual==2:
					order_diff_games_m1 = 'asc' 
				order_quan_ques_m1 = 'same'
			if difficulty_m1=='Easy_2':
				ord_casual = random.randint(0,1)
				if ord_casual==0:
					order_diff_games_m1 = 'same' 
				elif ord_casual==1 :
					order_diff_games_m1 = 'asc' 
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m1 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m1 = 'asc' 
			if difficulty_m2=='Easy_1':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m2 = 'same' 
				elif ord_casual==2:
					order_diff_games_m2 = 'asc' 
				ord_casual = random.randint(0,1)
				if ord_casual==0:
					order_quan_ques_m2 = 'same' 
				elif ord_casual==1:
					order_quan_ques_m2 = 'asc' 
			if difficulty_m2=='Easy_2':
				ord_casual = random.randint(0,1)
				if ord_casual==0: 
					order_diff_games_m2 = 'same' 
				elif ord_casual==1:
					order_diff_games_m2 = 'asc' 
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m2 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m2 = 'asc' 
			
			var_var = random.randint(0,10)
			if num_of_games_m1==1 or num_of_games_m2==1:
				var_var = random.randint(0,7)
			if var_var in (0,1,2):
				variety_m1 = '1F'
				variety_m2 = '2L'
			if var_var in (3,4):
				variety_m1 = '1F'
				variety_m2 = '1F'
			if var_var==5:
				variety_m1 = '2L'
				variety_m2 = '2L'
			if var_var==6:
				variety_m1 = '1F'
				variety_m2 = '3S'
			if var_var==7:
				variety_m1 = '2L'
				variety_m2 = '3S'
			if var_var==8:
				variety_m1 = '2L'
				variety_m2 = 'SUNDRY'
			if var_var==9:
				variety_m1 = '1F'
				variety_m2 = 'SUNDRY'
			else:
				variety_m1 = 'SUNDRY'
				variety_m2 = 'SUNDRY'
			
			if variety_m1=='SUNDRY' and variety_m2=='SUNDRY':
				criteria_arrangement_m1 = 'regular'
				criteria_arrangement_m2 = 'regular'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m1 = db_self.curs.fetchone()[0]
				depends = random.randint(0,2)
				if depends==0:
					scenario_m2 = scenario_m1
				else:
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m2 = db_self.curs.fetchone()[0]
			if variety_m1=='SUNDRY' and variety_m1!='SUNDRY':
				criteria_arrangement_m1 = 'regular'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m1 = db_self.curs.fetchone()[0]				
				criteria_arrangement_m2 = 'ignore'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]

			if variety_m1!='SUNDRY' and variety_m1=='SUNDRY':
				criteria_arrangement_m1 = 'ignore'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
				criteria_arrangement_m1 = 'regular'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m1 = db_self.curs.fetchone()[0]

			if variety_m1!='SUNDRY' and variety_m1!='SUNDRY':
				criteria_arrangement_m1 = 'ignore'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
				criteria_arrangement_m2 = 'ignore'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]
			
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 1 complex Easy=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 2 complex Easy=> ", scenario_m2)

			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
			
			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		elif num_of_matches==3:
			options = ['Easy_1', 'Easy_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m1 = random.choice(options)
				difficulty_m2 = difficulty_m1
				difficulty_m3 = difficulty_m1
			if order_diff_matches=='asc':
				difficulty_m1='Easy_1'
				difficulty_m2='Easy_1'
				difficulty_m3='Easy_2'
			if order_diff_matches=='desc':
				difficulty_m1='Easy_2'
				difficulty_m2='Easy_2'
				difficulty_m3='Easy_1'
			if order_quan_games=='same':
				casual = random.randint(0,3)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 3
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 2
			elif order_quan_games=='casu':
					num_of_games_m1 = random.randint(1,3)
					num_of_games_m2 = random.randint(1,3)
					num_of_games_m3 = random.randint(1,3)
			elif order_quan_games=='asc':
				casual = random.randint(0,5)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2
				elif casual==4:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==5:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 3
			elif order_quan_games=='desc':
				casual = random.randint(0,5)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				elif casual==2 or casual==3:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==4:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==5:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
					num_of_games_m3 = 1
			if difficulty_m1=='Easy_1':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m1 = 'same' 
				elif ord_casual==2:
					order_diff_games_m1 = 'asc' 
				ord_casual = random.randint(0,1)
				if ord_casual==0:
					order_quan_ques_m1 = 'same' 
				elif ord_casual==1:
					order_quan_ques_m1 = 'asc' 
			if difficulty_m1=='Easy_2':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m1 = 'same' 
				elif ord_casual==2:
					order_diff_games_m1 = 'asc'
				ord_casual = random.randint(0,1)
				if ord_casual==0:
					order_quan_ques_m1 = 'same'
				elif ord_casual==1:
					order_quan_ques_m1 = 'asc'
			if difficulty_m2=='Easy_1':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m2 = 'same' 
				elif ord_casual==2:
					order_diff_games_m2 = 'asc' 
				ord_casual = random.randint(0,1)
				if ord_casual==0:
					order_quan_ques_m2 = 'same' 
				elif ord_casual==1:
					order_quan_ques_m2 = 'asc' 
			if difficulty_m2=='Easy_2':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m2 = 'same' 
				elif ord_casual==2:
					order_diff_games_m2 = 'asc'
				ord_casual = random.randint(0,1)
				if ord_casual==0:
					order_quan_ques_m2 = 'same'
				elif ord_casual==1:
					order_quan_ques_m2 = 'asc'
			if difficulty_m3=='Easy_1':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m3 = 'same' 
				elif ord_casual==2:
					order_diff_games_m3 = 'asc' 
				ord_casual = random.randint(0,1)
				if ord_casual==0:
					order_quan_ques_m3 = 'same' 
				elif ord_casual==1:
					order_quan_ques_m3 = 'asc' 
			if difficulty_m3=='Easy_2':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m3 = 'same' 
				elif ord_casual==2:
					order_diff_games_m3 = 'asc'
				ord_casual = random.randint(0,1)
				if ord_casual==0:
					order_quan_ques_m3 = 'same'
				elif ord_casual==1:
					order_quan_ques_m3 = 'asc'							
			
			var_var = random.randint(0,13)
			if var_var in (0,1,2,3):
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '3S'
			if var_var==4:
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '1F'
			if var_var in (5,6):
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '2L'
			if var_var==7:
				variety_m1 = '1F'
				variety_m2 = '3S'
				variety_m3 = 'SUNDRY'
			if var_var in (8,9):
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = 'SUNDRY'
			if var_var in (10,11):
				variety_m1 = '1F'
				variety_m2 = 'SUNDRY'
				variety_m3 = 'SUNDRY'
			if var_var==12:
				variety_m1 = 'SUNDRY'
				variety_m2 = 'SUNDRY'
				variety_m3 = 'SUNDRY'
			else:
				variety_m1 = 'SUNDRY'
				variety_m2 = '2L'
				variety_m3 = 'SUNDRY'

			var_ava = ['1F','2L','3S']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)
			if num_of_games_m3==1 and variety_m3=='SUNDRY':
				variety_m3 = random.choice(var_ava)
			
			if variety_m1=='SUNDRY':
				criteria_arrangement_m1 = 'regular'
			else:
				criteria_arrangement_m1 = 'ignore'
			if variety_m2=='SUNDRY':
				criteria_arrangement_m2 = 'regular'
			else:
				criteria_arrangement_m2 = 'ignore'
			if variety_m3=='SUNDRY':
				criteria_arrangement_m3 = 'regular'
			else:
				criteria_arrangement_m3 = 'ignore'

			depends = random.randint(0,2)
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			if depends==0:
				scenario_m2 = scenario_m3 = scenario_m1
			elif depends==1:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]
				scenario_m3 = scenario_m2
			elif depends==2:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]
				scenario_m3 = scenario_m1

			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]
			if variety_m3!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
				scenario_m3 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 1 complex Easy=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 2 complex Easy=> ", scenario_m2)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 3 complex Easy=> ", scenario_m3)

			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m3, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		elif num_of_matches==4:
			options = ['Easy_1', 'Easy_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
			difficulty_m4 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m1 = random.choice(options)
				difficulty_m2 = difficulty_m1
				difficulty_m3 = difficulty_m1
				difficulty_m4 = difficulty_m1
			if order_diff_matches=='asc':
				difficulty_m1='Easy_1'
				difficulty_m2='Easy_1'
				difficulty_m3='Easy_2'
				difficulty_m4='Easy_2'
			if order_diff_matches=='desc':
				difficulty_m1='Normal_1'
				difficulty_m2='Easy_2'
				difficulty_m3='Easy_1'
				difficulty_m4='Easy_1'
			if order_quan_games=='same':
				casual = random.randint(0,3)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 1
					num_of_games_m4 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 2
			elif order_quan_games=='casu':
					num_of_games_m1 = random.randint(1,3)
					num_of_games_m2 = random.randint(1,3)
					num_of_games_m3 = random.randint(1,3)
					num_of_games_m4 = random.randint(1,3)
			elif order_quan_games=='asc':
				casual = random.randint(0,5)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 3
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2 
					num_of_games_m4 = 3
				elif casual==4:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				elif casual==5:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
			elif order_quan_games=='desc':
				casual = random.randint(0,5)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
					num_of_games_m4 = 1
				elif casual==2 or casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				elif casual==4:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				elif casual==5:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 2

			ord_casual = random.randint(0,6)
			if ord_casual==0 or ord_casual==1:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'same' 
				order_diff_games_m3 = 'asc' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==2 or ord_casual==3:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'same' 
				order_diff_games_m3 = 'same' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==4 or ord_casual==5:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'same' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==6:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'asc' 
			ord_casual = random.randint(0,5)
			if ord_casual==0 or ord_casual==1:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'asc' 
			elif ord_casual==2 or ord_casual==3:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'asc' 
			elif ord_casual==4:
				order_quan_ques_m1 = 'asc' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'casu' 
				order_quan_ques_m4 = 'same' 
			elif ord_casual==5:
				order_quan_ques_m1 = 'asc' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'casu' 

			var_var = random.randint(0,13)
			if var_var in (0,1,2,3):
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '3S'
				variety_m4 = '3S'
			if var_var==4:
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '1F'
				variety_m4 = '2L'
			if var_var in (5,6):
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '2L'
				variety_m4 = 'SUNDRY'
			if var_var==7:
				variety_m1 = '1F'
				variety_m2 = '3S'
				variety_m3 = '7O'
				variety_m4 = '1F'
			if var_var in (8,9):
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = 'SUNDRY'
				variety_m4 = 'SUNDRY'
			if var_var==10:
				variety_m1 = '1F'
				variety_m2 = 'SUNDRY'
				variety_m3 = '3S'
				variety_m4 = 'SUNDRY'
			if var_var==11:
				variety_m1 = '1F'
				variety_m2 = '7O'
				variety_m3 = 'SUNDRY'
				variety_m4 = 'SUNDRY'
			if var_var==12:
				variety_m1 = 'SUNDRY'
				variety_m2 = 'SUNDRY'
				variety_m3 = 'SUNDRY'
				variety_m4 = 'SUNDRY'
			else:
				variety_m1 = 'SUNDRY'
				variety_m2 = '1F'
				variety_m3 = '2L'
				variety_m4 = 'SUNDRY'
			
			var_ava = ['1F','2L','3S','7O']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)
			if num_of_games_m3==1 and variety_m3=='SUNDRY':
				variety_m3 = random.choice(var_ava)
			if num_of_games_m4==1 and variety_m4=='SUNDRY':
				variety_m4 = random.choice(var_ava)

			if variety_m1=='SUNDRY':
				criteria_arrangement_m1 = 'regular'
			else:
				criteria_arrangement_m1 = 'ignore'
			if variety_m2=='SUNDRY':
				criteria_arrangement_m2 = 'regular'
			else:
				criteria_arrangement_m2 = 'ignore'
			if variety_m3=='SUNDRY':
				criteria_arrangement_m3 = 'regular'
			else:
				criteria_arrangement_m3 = 'ignore'
			if variety_m4=='SUNDRY':
				criteria_arrangement_m4 = 'regular'
			else:
				criteria_arrangement_m4 = 'ignore'

			depends = random.randint(0,2)
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m2 = db_self.curs.fetchone()[0]
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m3 = db_self.curs.fetchone()[0]
			if depends==0 or depends==1:
				scenario_m2 = scenario_m1		
				scenario_m4 = scenario_m3
			if depends==2:
				scenario_m3 = scenario_m1		
				scenario_m4 = scenario_m2

			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]
			if variety_m3!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
				scenario_m3 = db_self.curs.fetchone()[0]
			if variety_m4!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m4,))
				scenario_m4 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 1 complex Easy=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 2 complex Easy=> ", scenario_m2)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 3 complex Easy=> ", scenario_m3)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Easy=> ", scenario_m4)

			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m3, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m4, num_of_games_m4, variety_m4, order_diff_games_m4, order_quan_ques_m4, criteria_arrangement_m4, advisable_time, scenario_m4, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
	if complexity=='Normal':
		advisable_time = 8
		if num_of_matches==1:
			rand_di = random.randint(0,3)
			if rand_di==0:
				difficulty_m = 'Normal_1'
			else:
				difficulty_m = 'Normal_2'
			num_of_games = random.randint(1,3)
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario = db_self.curs.fetchone()[0]

			if num_of_games==1:
				avail_varieties = ['1F','2L','3S']
			else:
				avail_varieties = ['1F','2L','3S','SUNDRY']
			variety = random.choice(avail_varieties)
			print("variety num_of_matches=1 => ",variety)
			if variety!='SUNDRY':
				criteria_arrangement = 'ignore'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety,))
				scenario = db_self.curs.fetchone()[0]
			else:
				criteria_arrangement = 'regular'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario = db_self.curs.fetchone()[0]

			ord_casual = random.randint(0,4)
			if ord_casual==0 or ord_casual==1:
				order_diff_games = 'same' 
			elif ord_casual==2 or ord_casual==3:
				order_diff_games = 'asc'
			elif ord_casual==4:
				order_diff_games = 'casu'
			ord_casual = random.randint(0,4)
			if ord_casual==0 or ord_casual==1:
				order_quan_ques = 'same' 
			elif ord_casual==2 or ord_casual==3:
				order_quan_ques = 'asc'
			elif ord_casual==4:
				order_quan_ques = 'casu'

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Normal=> ", scenario)

			db_self.add_new_Match_table(difficulty_m, num_of_games, variety, order_diff_games, order_quan_ques, criteria_arrangement, advisable_time, scenario, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		elif num_of_matches==2:
			options = ['Normal_1', 'Normal_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m1 = random.choice(options)
				difficulty_m2 = difficulty_m1
			if order_diff_matches=='asc':
				difficulty_m1='Normal_1'
				difficulty_m2='Normal_2'
			if order_diff_matches=='desc':
				difficulty_m1='Normal_2'
				difficulty_m2='Normal_1'
			if order_quan_games=='same':
				casual = random.randint(0,3)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
			elif order_quan_games=='casu':
				casual = random.randint(0,11)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
				elif casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==4 or casual==5:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==6:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
				elif casual==7:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
				elif casual==8 or casual==9:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==10 or casual==11:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
			elif order_quan_games=='asc':
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==3:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
			elif order_quan_games=='desc':
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				elif casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
			
			if difficulty_m1=='Normal_1':
				ord_casual = random.randint(0,4)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m1 = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_diff_games_m1 = 'asc' 
				elif ord_casual==4:
					order_diff_games_m1 = 'casu' 
				options = ['same', 'asc']
				order_quan_ques_m1 = random.choice(options)
			if difficulty_m1=='Normal_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m1 = random.choice(options)
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)
			if difficulty_m2=='Normal_1':
				ord_casual = random.randint(0,4)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m2 = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_diff_games_m2 = 'asc' 
				elif ord_casual==4:
					order_diff_games_m2 = 'casu'
				ord_casual = random.randint(0,4)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m2 = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_quan_ques_m2 = 'asc' 
				elif ord_casual==4:
					order_quan_ques_m2 = 'casu'
			if difficulty_m2=='Normal_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m1 = random.choice(options)
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)

			var_var = random.randint(0,12)
			if var_var in (0,1,2):
				variety_m1 = '1F'
				variety_m2 = '2L'
			if var_var==3:
				variety_m1 = '2L'
				variety_m2 = '2L'
			if var_var==4:
				variety_m1 = '1F'
				variety_m2 = '1F'
			if var_var==5:
				variety_m1 = '1F'
				variety_m2 = '7O'
			if var_var==6:
				variety_m1 = '1F'
				variety_m2 = '3S'
			if var_var==7:
				variety_m1 = '3S'
				variety_m2 = '2L'
			if var_var==8:
				variety_m1 = '1F'
				variety_m2 = 'SUNDRY'
			if var_var==9:
				variety_m1 = '7O'
				variety_m2 = 'SUNDRY'
			if var_var==10:
				variety_m1 = 'SUNDRY'
				variety_m2 = 'SUNDRY'
			if var_var==11:
				variety_m1 = '2L'
				variety_m2 = 'SUNDRY'
			else:
				variety_m1 = 'SUNDRY'
				variety_m2 = '1F'
			
			var_ava = ['1F','2L','3S','7O']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)

			if variety_m1=='SUNDRY':
				criteria_arrangement_m1 = 'regular'
			else:
				criteria_arrangement_m1 = 'ignore'
			if variety_m2=='SUNDRY':
				criteria_arrangement_m2 = 'regular'
			else:
				criteria_arrangement_m2 = 'ignore'

			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			depends = random.randint(0,1)
			if depends:
				scenario_m2 = scenario_m1
			else:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]

			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Normal=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Normal=> ", scenario_m2)

			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)					
		elif num_of_matches==3:
			options = ['Easy_2','Normal_1', 'Normal_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m1 = random.choice(options)
				difficulty_m2 = difficulty_m1
				difficulty_m3 = difficulty_m1
			if order_diff_matches=='desc':
				randd = random.randint(0,1)
				if randd:
					difficulty_m1='Easy_2'
					difficulty_m2='Normal_1'
					difficulty_m3='Normal_2'
				else:
					difficulty_m1='Easy_1'
					difficulty_m2='Normal_2'
					difficulty_m3='Normal_2'
			if order_diff_matches=='asc':
				randd = random.randint(0,1)
				if randd:
					difficulty_m1='Normal_2'
					difficulty_m2='Normal_1'
					difficulty_m3='Easy_2'
				else:
					difficulty_m1='Normal_2'
					difficulty_m2='Normal_1'
					difficulty_m3='Normal_1'
			if order_quan_games=='same':
				casual = random.randint(0,4)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 3
				elif casual==2 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2')):
					num_of_games_m1 = 4
					num_of_games_m2 = 4
					num_of_games_m3 = 4
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 2
			elif order_quan_games=='casu':
					num_of_games_m1 = random.randint(2,3)
					num_of_games_m2 = random.randint(2,3)
					num_of_games_m3 = random.randint(2,3)
			elif order_quan_games=='asc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2
				elif casual==4:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==5:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 3
				elif casual==6 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2')):
					num_of_games_m1 = 2
					num_of_games_m2 = 3
					num_of_games_m3 = 4							
			elif order_quan_games=='desc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				elif casual==2 or casual==3:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==4:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==5:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==6 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2')):
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2					
			if difficulty_m1=='Easy_2':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m1 = 'same' 
				elif ord_casual==2:
					order_diff_games_m1 = 'asc'
				ord_casual = random.randint(0,2)							
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m1 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m1 = 'asc'
			if difficulty_m1=='Normal_1':
				ord_casual = random.randint(0,3)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m1 = 'same' 
				elif ord_casual==2:
					order_diff_games_m1 = 'asc'
				elif ord_casual==3:
					order_diff_games_m1 = 'casu'
				ord_casual = random.randint(0,3)							
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m1 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m1 = 'asc'
				elif ord_casual==3:
					order_quan_ques_m1 = 'casu'						
			if difficulty_m1=='Normal_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m1 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
			if difficulty_m2=='Easy_2':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m2 = 'same' 
				elif ord_casual==2:
					order_diff_games_m2 = 'asc'
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m2 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m2 = 'asc'
			if difficulty_m2=='Normal_1':
				ord_casual = random.randint(0,3)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m2 = 'same' 
				elif ord_casual==2:
					order_diff_games_m2 = 'asc'
				elif ord_casual==3:
					order_diff_games_m2 = 'casu'
				ord_casual = random.randint(0,3)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m2 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m2 = 'asc'
				elif ord_casual==3:
					order_quan_ques_m2 = 'casu'
			if difficulty_m2=='Normal_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)
			if difficulty_m3=='Easy_2':
				ord_casual = random.randint(0,2)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m3 = 'same' 
				elif ord_casual==2:
					order_diff_games_m3 = 'asc'
				ord_casual = random.randint(0,2)							
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m3 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m3 = 'asc'					
			if difficulty_m3=='Normal_1':
				ord_casual = random.randint(0,3)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m3 = 'same' 
				elif ord_casual==2:
					order_diff_games_m3 = 'asc' 
				elif ord_casual==3:
					order_diff_games_m3 = 'casu' 
				ord_casual = random.randint(0,2)
				if ord_casual==0:
					order_quan_ques_m3 = 'same' 
				elif ord_casual==1:
					order_quan_ques_m3 = 'asc' 
				elif ord_casual==2:
					order_quan_ques_m3 = 'casu'
			if difficulty_m3=='Normal_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m3 = random.choice(options)
				order_quan_ques_m3 = random.choice(options)						

			var_var = random.randint(0,14)
			if var_var in (0,1,2):
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '3S'
			if var_var==3:
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '3S'
			if var_var==4:
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '7O'
			if var_var in (5,6):
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '2L'
			if var_var==7:
				variety_m1 = '1F'
				variety_m2 = '3S'
				variety_m3 = '7O'
			if var_var==8:
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = 'SUNDRY'
			if var_var==9:
				variety_m1 = '1F'
				variety_m2 = '7O'
				variety_m3 = 'SUNDRY'
			if var_var==10:
				variety_m1 = '1F'
				variety_m2 = 'SUNDRY'
				variety_m3 = '3S'
			if var_var==11:
				variety_m1 = '3S'
				variety_m2 = '2L'
				variety_m3 = 'SUNDRY'
			if var_var==12:
				variety_m1 = 'SUNDRY'
				variety_m2 = 'SUNDRY'
				variety_m3 = 'SUNDRY'
			if var_var==13:
				variety_m1 = '3S'
				variety_m2 = 'SUNDRY'
				variety_m3 = '1F'
			else:
				variety_m1 = 'SUNDRY'
				variety_m2 = '1F'
				variety_m3 = '2L'
			
			var_ava = ['1F','2L','3S','7O']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)
			if num_of_games_m3==1 and variety_m3=='SUNDRY':
				variety_m3 = random.choice(var_ava)

			if variety_m1=='SUNDRY':
				criteria_arrangement_m1 = 'regular'
			else:
				criteria_arrangement_m1 = 'ignore'
			if variety_m2=='SUNDRY':
				criteria_arrangement_m2 = 'regular'
			else:
				criteria_arrangement_m2 = 'ignore'
			if variety_m3=='SUNDRY':
				criteria_arrangement_m3 = 'regular'
			else:
				criteria_arrangement_m3 = 'ignore'

			depends = random.randint(0,2)
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			if depends==0:
				scenario_m2 = scenario_m3 = scenario_m1
			elif depends==1:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]
				scenario_m3 = scenario_m2
			elif depends==2:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]
				scenario_m3 = scenario_m1

			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]
			if variety_m3!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
				scenario_m3 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Normal=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Normal=> ", scenario_m2)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Normal=> ", scenario_m3)

			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m3, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)					
		elif num_of_matches==4:
			options = ['Easy_2', 'Normal_1', 'Normal_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
			difficulty_m4 = random.choice(options)
			if order_quan_games=='same':
				casual = random.randint(0,4)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 1
					num_of_games_m4 = 1
				elif casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				elif casual==2 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2') or (difficulty_m4=='Easy_2')):
					num_of_games_m1 = 4
					num_of_games_m2 = 4
					num_of_games_m3 = 4
					num_of_games_m4 = 4
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 2
			elif order_quan_games=='casu':
					num_of_games_m1 = random.randint(1,3)
					num_of_games_m2 = random.randint(1,3)
					num_of_games_m3 = random.randint(1,3)
					num_of_games_m4 = random.randint(1,3)
			elif order_quan_games=='asc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 3
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2 
					num_of_games_m4 = 3
				elif casual==4:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				elif casual==5:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				elif casual==6 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2') or (difficulty_m4=='Easy_2')):
					num_of_games_m1 = 2
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 4

			elif order_quan_games=='desc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
					num_of_games_m4 = 1
				elif casual==2 or casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				elif casual==4:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				elif casual==5:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 2
				elif casual==6 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2') or (difficulty_m4=='Easy_2')):
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 2
			#mene sbatto le palle della difficoltà qui!!
			ord_casual = random.randint(0,7)
			if ord_casual==0 or ord_casual==1:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'same' 
				order_diff_games_m3 = 'asc' 
				order_diff_games_m4 = 'asc'
			elif ord_casual==2 or ord_casual==3:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'same' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==4 or ord_casual==5:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'same' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==6:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==7:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'casu' 
			ord_casual = random.randint(0,6)
			if ord_casual==0 or ord_casual==1:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'asc' 
			elif ord_casual==2 or ord_casual==3:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'asc' 
			elif ord_casual==4:
				order_quan_ques_m1 = 'asc' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'casu' 
				order_quan_ques_m4 = 'casu' 
			elif ord_casual==5:
				order_quan_ques_m1 = 'asc' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'casu' 
			elif ord_casual==6:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'casu' 
				order_quan_ques_m4 = 'casu' 

			var_var = random.randint(0,14)
			if var_var in (0,1,2):
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '3S'
				variety_m4 = '3S'
			if var_var==3:
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '3S'
				variety_m4 = '7O'
			if var_var==4:
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '1F'
				variety_m4 = '2L'
			if var_var in (5,6):
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '2L'
				variety_m4 = 'SUNDRY'
			if var_var==7:
				variety_m1 = '1F'
				variety_m2 = '3S'
				variety_m3 = '7O'
				variety_m4 = '1F'
			if var_var in (8,9):
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = 'SUNDRY'
				variety_m4 = 'SUNDRY'
			if var_var in (10,11):
				variety_m1 = '1F'
				variety_m2 = 'SUNDRY'
				variety_m3 = '3S'
				variety_m4 = 'SUNDRY'
			if var_var==12:
				variety_m1 = 'SUNDRY'
				variety_m2 = 'SUNDRY'
				variety_m3 = 'SUNDRY'
				variety_m4 = 'SUNDRY'
			if var_var==13:
				variety_m1 = '3S'
				variety_m2 = 'SUNDRY'
				variety_m3 = '1F'
				variety_m4 = '2L'
			else:
				variety_m1 = 'SUNDRY'
				variety_m2 = '1F'
				variety_m3 = '2L'
				variety_m4 = 'SUNDRY'
			

			var_ava = ['1F','2L','3S','7O']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)
			if num_of_games_m3==1 and variety_m3=='SUNDRY':
				variety_m3 = random.choice(var_ava)
			if num_of_games_m4==1 and variety_m4=='SUNDRY':
				variety_m4 = random.choice(var_ava)
											
			if variety_m1=='SUNDRY':
				criteria_arrangement_m1 = 'regular'
			else:
				criteria_arrangement_m1 = 'ignore'
			if variety_m2=='SUNDRY':
				criteria_arrangement_m2 = 'regular'
			else:
				criteria_arrangement_m2 = 'ignore'
			if variety_m3=='SUNDRY':
				criteria_arrangement_m3 = 'regular'
			else:
				criteria_arrangement_m3 = 'ignore'
			if variety_m4=='SUNDRY':
				criteria_arrangement_m4 = 'regular'
			else:
				criteria_arrangement_m4 = 'ignore'

			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m2 = db_self.curs.fetchone()[0]
			depends = random.randint(0,5)
			if depends==0 or depends==1 or depends==2:
				scenario_m3 = scenario_m2
				scenario_m2 = scenario_m1
				scenario_m4 = scenario_m3
			if depends==3 or depends==4:
				scenario_m3 = scenario_m1
				scenario_m4 = scenario_m2
			if depends==5:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m3 = db_self.curs.fetchone()[0]
				decide = random.randint(0,1)
				if decide:
					scenario_m4 = scenario_m2
				else:
					scenario_m4 = scenario_m3

			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]
			if variety_m3!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
				scenario_m3 = db_self.curs.fetchone()[0]
			if variety_m4!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m4,))
				scenario_m4 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Normal=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Normal=> ", scenario_m2)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Normal=> ", scenario_m3)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Normal=> ", scenario_m4)


			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m3, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m4, num_of_games_m4, variety_m4, order_diff_games_m4, order_quan_ques_m4, criteria_arrangement_m4, advisable_time, scenario_m4, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)					
	if complexity=='Medium':
		advisable_time = 10
		if num_of_matches==1:
			sub_diff = random.randint(0,1)
			if sub_diff:
				difficulty_m = 'Medium_1'
				num_of_games = random.randint(1,3)
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario = db_self.curs.fetchone()[0]
				ord_casual = random.randint(0,4)
				if ord_casual==0 or ord_casual==1:
					order_diff_games = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_diff_games = 'asc'
				elif ord_casual==4:
					order_diff_games = 'casu'
				ord_casual = random.randint(0,4)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_quan_ques = 'asc'
				elif ord_casual==4:
					order_quan_ques = 'casu'
			
			else:
				difficulty_m = 'Medium_2'
				num_of_games = random.randint(1,3)
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario = db_self.curs.fetchone()[0]
				ord_casual = random.randint(0,6)
				if ord_casual==0 or ord_casual==1:
					order_diff_games = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_diff_games = 'asc'
				elif ord_casual==4 or ord_casual==5:
					order_diff_games = 'casu'
				elif ord_casual==6:
					order_diff_games = 'desc'
				ord_casual = random.randint(0,6)
				if ord_casual==0 or ord_casual==1:
					order_diff_games = 'same'
				elif ord_casual==2 or ord_casual==3:
					order_diff_games = 'asc'
				elif ord_casual==4:
					order_diff_games = 'casu'
				else:
					order_diff_games = 'desc'
	
				print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 1 complex Medium=> ", scenario)

			if num_of_games==1:
				avail_varieties = ['1F','2L','3S','5K','7O']
			else:
				avail_varieties = ['1F','2L','3S','5K','7O','SUNDRY']
			variety = random.choice(avail_varieties)
			print("variety num_of_matches=1 => ",variety)
			if variety!='SUNDRY':
				criteria_arrangement = 'ignore'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety,))
				scenario = db_self.curs.fetchone()[0]
			else:
				criteria_arrangement = 'regular'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario = db_self.curs.fetchone()[0]

			db_self.add_new_Match_table(difficulty_m, num_of_games, variety, order_diff_games, order_quan_ques, criteria_arrangement, advisable_time, scenario, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)				

			db_self.add_new_Match_table(difficulty_m, num_of_games, variety, order_diff_games, order_quan_ques, criteria_arrangement, advisable_time, scenario, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		
		elif num_of_matches==2:
			options = ['Medium_1', 'Medium_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m1 = random.choice(options)
				difficulty_m2 = difficulty_m1
			if order_diff_matches=='asc':
				difficulty_m1='Medium_1'
				difficulty_m2='Medium_2'
			if order_diff_matches=='desc':
				difficulty_m1='Medium_2'
				difficulty_m2='Medium_1'
			if order_quan_games=='same':
				casual = random.randint(0,5)
				if casual==0 or casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
				elif casual==2 or casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
				elif casual==3 or casual==4:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
				elif casual==5:
					num_of_games_m1 = 4
					num_of_games_m2 = 4
			elif order_quan_games=='casu':
				casual = random.randint(0,15)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
				elif casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==4 or casual==5:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==6:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
				elif casual==7:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
				elif casual==8 or casual==9:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==10 or casual==11:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				elif casual==12:
					num_of_games_m1 = 2
					num_of_games_m2 = 4
				elif casual==13:
					num_of_games_m1 = 4
					num_of_games_m2 = 1
				elif casual==14:
					num_of_games_m1 = 4
					num_of_games_m2 = 2
				elif casual==15:
					num_of_games_m1 = 3
					num_of_games_m2 = 4
			elif order_quan_games=='asc':
				casual = random.randint(0,10)
				if casual==0 or casual==1 or casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==3 or casual==4:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==5 or casual==6:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
				elif casual==7 or casual==8:
					num_of_games_m1 = 1
					num_of_games_m2 = 4
				elif casual==9:
					num_of_games_m1 = 2
					num_of_games_m2 = 4
				elif casual==10:
					num_of_games_m1 = 3
					num_of_games_m2 = 4
			elif order_quan_games=='desc':
				casual = random.randint(0,7)
				if casual==0 or casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				elif casual==3 or casual==4:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
				elif casual==5:
					num_of_games_m1 = 4
					num_of_games_m2 = 1
				elif casual==6:
					num_of_games_m1 = 4
					num_of_games_m2 = 2
				elif casual==7:
					num_of_games_m1 = 4
					num_of_games_m2 = 3																			
			if difficulty_m1=='Medium_1':
				ord_casual = random.randint(0,4)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m1 = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_diff_games_m1 = 'asc' 
				elif ord_casual==4:
					order_diff_games_m1 = 'casu' 
				options = ['same', 'asc', 'casu']
				order_quan_ques_m1 = random.choice(options)
			if difficulty_m1=='Medium_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m1 = random.choice(options)
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)
				ran_opt = random.randint(0,4)
				if ran_opt==0:
					order_diff_games_m2 = 'desc'
					order_quan_ques_m2 = 'desc'
			if difficulty_m2=='Medium_1':
				ord_casual = random.randint(0,7)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m2 = 'same' 
				elif ord_casual==2 or ord_casual==3 or ord_casual==4:
					order_diff_games_m2 = 'asc' 
				elif ord_casual==5 or ord_casual==6:
					order_diff_games_m2 = 'casu'
				elif ord_casual==7:
					order_diff_games_m2 = 'desc'
			if difficulty_m2=='Medium_1':
				ord_casual = random.randint(0,7)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m2 = 'same' 
				elif ord_casual==2 or ord_casual==3 or ord_casual==4:
					order_quan_ques_m2 = 'asc' 
				elif ord_casual==5 or ord_casual==6:
					order_quan_ques_m2 = 'casu'
				elif ord_casual==7:
					order_quan_ques_m2 = 'desc'
			if difficulty_m2=='Medium_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m1 = random.choice(options)
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)
				ran_opt = random.randint(0,4)
				if ran_opt==0:
					order_diff_games_m2 = 'desc'
					order_quan_ques_m2 = 'desc'

			var_var = random.randint(0,16)
			if var_var in (0,1):
				variety_m1 = '1F'
				variety_m2 = '2L'
			if var_var in (2):
				variety_m1 = '2L'
				variety_m2 = '2L'
			if var_var==3:
				variety_m1 = '1F'
				variety_m2 = '1F'
			if var_var==4:
				variety_m1 = '1F'
				variety_m2 = '3S'
			if var_var==5:
				variety_m1 = '3S'
				variety_m2 = '2L'
			if var_var==6:
				variety_m1 = '1F'
				variety_m2 = '7O'
			if var_var==7:
				variety_m1 = '2L'
				variety_m2 = '7O'
			if var_var==8:
				variety_m1 = '1F'
				variety_m2 = '5K'
			if var_var==9:
				variety_m1 = '2L'
				variety_m2 = '5K'
			if var_var==10:
				variety_m1 = '3S'
				variety_m2 = '7O'
			if var_var==11:
				variety_m1 = '2L'
				variety_m2 = 'SUNDRY'
			if var_var==12:
				variety_m1 = '1F'
				variety_m2 = 'SUNDRY'
			if var_var==13:
				variety_m1 = 'SUNDRY'
				variety_m2 = 'SUNDRY'
			if var_var==14:
				variety_m1 = '2L'
				variety_m2 = 'SUNDRY'
			if var_var==15:
				variety_m1 = 'SUNDRY'
				variety_m2 = '7O'
			else:
				variety_m1 = 'SUNDRY'
				variety_m2 = '1F'
			
			var_ava = ['1F','2L','3S','5K','7O']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)

			if variety_m1=='SUNDRY':
				criteria_arrangement_m1 = 'regular'
			else:
				criteria_arrangement_m1 = 'ignore'
			if variety_m2=='SUNDRY':
				criteria_arrangement_m2 = 'regular'
			else:
				criteria_arrangement_m2 = 'ignore'

			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			depends = random.randint(0,1)
			if depends:
				scenario_m2 = scenario_m1
			else:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]					
			
			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 1 complex Medium=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 2 complex Medium=> ", scenario_m2)

			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		elif num_of_matches==3:
			options = ['Normal_2','Medium_1', 'Medium_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m1 = random.choice(options)
				difficulty_m2 = difficulty_m1
				difficulty_m3 = difficulty_m1
			if order_diff_matches=='asc':
				randd = random.randint(0,1)
				if randd:
					difficulty_m1='Normal_2'
					difficulty_m2='Medium_1'
					difficulty_m3='Medium_2'
				else:
					difficulty_m1='Medium_1'
					difficulty_m2='Medium_1'
					difficulty_m3='Medium_2'
			if order_diff_matches=='desc':
				randd = random.randint(0,1)
				if randd:				
					difficulty_m1='Medium_2'
					difficulty_m2='Medium_1'
					difficulty_m3='Normal_2'
				else:
					difficulty_m1='Medium_2'
					difficulty_m2='Medium_1'
					difficulty_m3='Medium_1'
			if order_quan_games=='same':
				casual = random.randint(0,4)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 3
				elif casual==2 and ((difficulty_m1=='Normal_2') or (difficulty_m2=='Normal_2') or (difficulty_m3=='Normal_2')):
					num_of_games_m1 = 4
					num_of_games_m2 = 4
					num_of_games_m3 = 4
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 2
			elif order_quan_games=='casu':
					num_of_games_m1 = random.randint(2,3)
					num_of_games_m2 = random.randint(2,3)
					num_of_games_m3 = random.randint(2,3)
			elif order_quan_games=='asc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2
				elif casual==4:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==5:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 3
				elif casual==6 and ((difficulty_m1=='Normal_2') or (difficulty_m2=='Normal_2') or (difficulty_m3=='Normal_2')):
					num_of_games_m1 = 2
					num_of_games_m2 = 3
					num_of_games_m3 = 4							
			elif order_quan_games=='desc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				elif casual==2 or casual==3:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==4:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==5:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==6 and ((difficulty_m1=='Normal_2') or (difficulty_m2=='Normal_2') or (difficulty_m3=='Normal_2')):
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2							

			if difficulty_m1=='Normal_2':
				options = ['same', 'asc']
				order_diff_games_m1 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
			if difficulty_m1=='Medium_1':
				ord_casual = random.randint(0,3)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m1 = 'same' 
				elif ord_casual==2:
					order_diff_games_m1 = 'asc'
				elif ord_casual==3:
					order_diff_games_m1 = 'casu'
				ord_casual = random.randint(0,6)							
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m1 = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_quan_ques_m1 = 'asc'
				elif ord_casual==4 or ord_casual==5:
					order_quan_ques_m1 = 'casu'
				elif ord_casual==6:
					order_quan_ques_m1 = 'desc'
			if difficulty_m1=='Medium_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m1 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
			if difficulty_m2=='Normal_2':
				options = ['same', 'asc']
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)
			if difficulty_m2=='Medium_1':
				ord_casual = random.randint(0,5)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m2 = 'same' 
				elif ord_casual==2:
					order_diff_games_m2 = 'asc'
				elif ord_casual==3 or ord_casual==4:
					order_diff_games_m2 = 'casu'
				elif ord_casual==5:
					order_diff_games_m2 = 'desc'
				ord_casual = random.randint(0,5)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m2 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m2 = 'asc'
				elif ord_casual==3 or ord_casual==4:
					order_quan_ques_m2 = 'casu'
				elif ord_casual==5:
					order_quan_ques_m2 = 'desc'
			if difficulty_m2=='Medium_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)
			if difficulty_m3=='Normal_2':
				options = ['same', 'asc']
				order_diff_games_m3 = random.choice(options)
				order_quan_ques_m3 = random.choice(options)
			if difficulty_m3=='Medium_1':
				ord_casual = random.randint(0,5)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m3 = 'same' 
				elif ord_casual==2:
					order_diff_games_m3 = 'asc' 
				elif ord_casual==3 or ord_casual==4:
					order_diff_games_m3 = 'casu' 
				elif ord_casual==5:
					order_diff_games_m3 = 'desc' 
				ord_casual = random.randint(0,5)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m3 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m3 = 'asc' 
				elif ord_casual==3 or ord_casual==4:
					order_quan_ques_m3 = 'casu' 
				elif ord_casual==5:
					order_quan_ques_m3 = 'desc' 
			if difficulty_m3=='Medium_2':
				options = ['same', 'asc', 'casu', 'desc']
				order_diff_games_m3 = random.choice(options)
				order_quan_ques_m3 = random.choice(options)						

			var_var = random.randint(0,14)
			if var_var in (0,1,2):
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '3S'
			if var_var==3:
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '3S'
			if var_var==4:
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '7O'
			if var_var in (5,6):
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '2L'
			if var_var==7:
				variety_m1 = '1F'
				variety_m2 = '3S'
				variety_m3 = '7O'
			if var_var==8:
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = 'SUNDRY'
			if var_var==9:
				variety_m1 = '1F'
				variety_m2 = 'SUNDRY'
				variety_m3 = '5K'
			if var_var==10:
				variety_m1 = '1F'
				variety_m2 = 'SUNDRY'
				variety_m3 = '3S'
			if var_var==11:
				variety_m1 = '5K'
				variety_m2 = 'SUNDRY'
				variety_m3 = '3S'
			if var_var==12:
				variety_m1 = '2L'
				variety_m2 = '5K'
				variety_m3 = 'SUNDRY'
			if var_var==13:
				variety_m1 = '3S'
				variety_m2 = 'SUNDRY'
				variety_m3 = '1F'
			else:
				variety_m1 = 'SUNDRY'
				variety_m2 = '1F'
				variety_m3 = '2L'
			
			var_ava = ['1F','2L','3S','5K','7O']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)
			if num_of_games_m3==1 and variety_m3=='SUNDRY':
				variety_m3 = random.choice(var_ava)

			if variety_m1=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m1 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m1 = 'random'
			else:
				criteria_arrangement_m1 = 'ignore'
			if variety_m2=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m2 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m2 = 'random'
			else:
				criteria_arrangement_m2 = 'ignore'
			if variety_m3=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m3 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m3 = 'random'
			else:
				criteria_arrangement_m3 = 'ignore'
			
			depends = random.randint(0,2)
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			if depends==0:
				scenario_m2 = scenario_m3 = scenario_m1
			elif depends==1:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]
				scenario_m3 = scenario_m2
			elif depends==2:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]
				scenario_m3 = scenario_m1

			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]
			if variety_m3!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
				scenario_m3 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 1 complex Medium=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 2 complex Medium=> ", scenario_m2)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 3 complex Medium=> ", scenario_m3)

			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m3, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		elif num_of_matches==4:
			options = ['Normal_2', 'Medium_1', 'Medium_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
			difficulty_m4 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m1 = random.choice(options)
				difficulty_m2 = difficulty_m1
				difficulty_m3 = difficulty_m1
				difficulty_m4 = difficulty_m1
			if order_diff_matches=='asc':
				randd = random.randint(0,1)
				if randd:
					difficulty_m1='Normal_2'
					difficulty_m2='Medium_1'
					difficulty_m3='Medium_1'
					difficulty_m4='Medium_2'
				else:
					difficulty_m1='Normal_1'
					difficulty_m2='Normal_2'
					difficulty_m3='Medium_1'
					difficulty_m4='Medium_2'
			if order_diff_matches=='desc':
				randd = random.randint(0,1)
				if randd:
					difficulty_m1='Medium_2'
					difficulty_m2='Medium_1'
					difficulty_m3='Medium_1'
					difficulty_m4='Normal_2'
				else:
					difficulty_m1='Medium_2'
					difficulty_m2='Medium_2'
					difficulty_m3='Medium_1'
					difficulty_m4='Medium_1'
			if order_quan_games=='same':
				casual = random.randint(0,5)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 1
					num_of_games_m4 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				elif casual==3 and ((difficulty_m1=='Normal_2') or (difficulty_m2=='Normal_2') or (difficulty_m3=='Normal_2') or (difficulty_m4=='Normal_2')):
					num_of_games_m1 = 4
					num_of_games_m2 = 4
					num_of_games_m3 = 4
					num_of_games_m4 = 4
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 2
			elif order_quan_games=='casu':
					num_of_games_m1 = random.randint(1,3)
					num_of_games_m2 = random.randint(1,3)
					num_of_games_m3 = random.randint(1,3)
					num_of_games_m4 = random.randint(1,3)
			elif order_quan_games=='asc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 3
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2 
					num_of_games_m4 = 3
				elif casual==4:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				elif casual==5:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				elif casual==6 and ((difficulty_m1=='Normal_2') or (difficulty_m2=='Normal_2') or (difficulty_m3=='Normal_2') or (difficulty_m4=='Normal_2')):
					num_of_games_m1 = 2
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 4

			elif order_quan_games=='desc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
					num_of_games_m4 = 1
				elif casual==2 or casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				elif casual==4:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				elif casual==5:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 2
				elif casual==6 and ((difficulty_m1=='Normal_2') or (difficulty_m2=='Normal_2') or (difficulty_m3=='Normal_2') or (difficulty_m4=='Normal_2')):
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 2
			#mene sbatto le palle della difficoltà qui!!
			ord_casual = random.randint(0,9)
			if ord_casual==0 or ord_casual==1:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'same' 
				order_diff_games_m3 = 'asc' 
				order_diff_games_m4 = 'asc'
			elif ord_casual==2 or ord_casual==3:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'same' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==4 or ord_casual==5:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'same' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==6:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==7:
				order_diff_games_m1 = 'asc' 
				order_diff_games_m2 = 'same' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'casu' 
			elif ord_casual==8:
				order_diff_games_m1 = 'asc' 
				order_diff_games_m2 = 'desc' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==9:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'desc' 
				order_diff_games_m4 = 'casu' 
			ord_casual = random.randint(0,12)
			if ord_casual==0 or ord_casual==1:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'asc' 
			elif ord_casual==2 or ord_casual==3:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'asc' 
			elif ord_casual==4 or ord_casual==5:
				order_quan_ques_m1 = 'asc' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'casu' 
				order_quan_ques_m4 = 'casu' 
			elif ord_casual==6 or ord_casual==7:
				order_quan_ques_m1 = 'asc' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'casu' 
			elif ord_casual==8 or ord_casual==9:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'casu' 
				order_quan_ques_m4 = 'casu' 
			elif ord_casual==10:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'desc' 
				order_quan_ques_m4 = 'casu'							
			elif ord_casual==11:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'desc' 
				order_quan_ques_m4 = 'asc' 
			elif ord_casual==12:
				order_quan_ques_m1 = 'desc' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'desc' 
				order_quan_ques_m4 = 'casu' 

			var_var = random.randint(0,14)
			if var_var in (0,1,2):
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '3S'
				variety_m4 = '3S'
			if var_var==3:
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '3S'
				variety_m4 = '7O'
			if var_var==4:
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '1F'
				variety_m4 = '2L'
			if var_var==5:
				variety_m1 = '1F'
				variety_m2 = '1F'
				variety_m3 = '2L'
				variety_m4 = 'SUNDRY'
			if var_var==6:
				variety_m1 = '1F'
				variety_m2 = '5K'
				variety_m3 = '2L'
				variety_m4 = 'SUNDRY'
			if var_var==7:
				variety_m1 = '1F'
				variety_m2 = '3S'
				variety_m3 = '7O'
				variety_m4 = '1F'
			if var_var==8:
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = 'SUNDRY'
				variety_m4 = 'SUNDRY'
			if var_var==9:
				variety_m1 = '1F'
				variety_m2 = '2L'
				variety_m3 = '5K'
				variety_m4 = 'SUNDRY'
			if var_var==10:
				variety_m1 = '1F'
				variety_m2 = 'SUNDRY'
				variety_m3 = '3S'
				variety_m4 = 'SUNDRY'
			if var_var==11:
				variety_m1 = '5K'
				variety_m2 = '2L'
				variety_m3 = '7O'
				variety_m4 = 'SUNDRY'
			if var_var==12:
				variety_m1 = 'SUNDRY'
				variety_m2 = 'SUNDRY'
				variety_m3 = 'SUNDRY'
				variety_m4 = 'SUNDRY'
			if var_var==13:
				variety_m1 = '7O'
				variety_m2 = 'SUNDRY'
				variety_m3 = '1F'
				variety_m4 = '2L'
			else:
				variety_m1 = 'SUNDRY'
				variety_m2 = '1F'
				variety_m3 = '2L'
				variety_m4 = 'SUNDRY'
			
			var_ava = ['1F','2L','3S','5K','7O']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)
			if num_of_games_m3==1 and variety_m3=='SUNDRY':
				variety_m3 = random.choice(var_ava)
			if num_of_games_m4==1 and variety_m4=='SUNDRY':
				variety_m4 = random.choice(var_ava)

			if variety_m1=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m1 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m1 = 'random'
			else:
				criteria_arrangement_m1 = 'ignore'
			
			if variety_m2=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m2 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m2 = 'random'
			else:
				criteria_arrangement_m2 = 'ignore'
			
			if variety_m3=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m3 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m3 = 'random'
			else:
				criteria_arrangement_m3 = 'ignore'
			
			if variety_m4=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m4 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m4 = 'random'
			else:
				criteria_arrangement_m4 = 'ignore'

			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m2 = db_self.curs.fetchone()[0]
			depends = random.randint(0,5)
			if depends==0 or depends==1 or depends==2:
				scenario_m3 = scenario_m2
				scenario_m2 = scenario_m1
				scenario_m4 = scenario_m3
			if depends==3 or depends==4:
				scenario_m3 = scenario_m1		
				scenario_m4 = scenario_m2
			if depends==5:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m3 = db_self.curs.fetchone()[0]
				decide = random.randint(0,1)
				if decide:
					scenario_m4 = scenario_m2
				else:
					scenario_m4 = scenario_m3

			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]
			if variety_m3!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
				scenario_m3 = db_self.curs.fetchone()[0]
			if variety_m4!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m4,))
				scenario_m4 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 1 complex Medium=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 2 complex Medium=> ", scenario_m2)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 3 complex Medium=> ", scenario_m3)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Medium=> ", scenario_m4)

			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
			
			db_self.add_new_Match_table(difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m3, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m4, num_of_games_m4, variety_m4, order_diff_games_m4, order_quan_ques_m4, criteria_arrangement_m4, advisable_time, scenario_m4, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
	if complexity=='Hard':
		advisable_time = 10
		if num_of_matches==1:
			sub_diff = random.randint(0,1)
			if sub_diff:
				difficulty_m = 'Hard_1'
				num_of_games = random.randint(1,3)
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario = db_self.curs.fetchone()[0]
				ord_casual = random.randint(0,4)
				if ord_casual==0 or ord_casual==1:
					order_diff_games = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_diff_games = 'asc'
				elif ord_casual==4:
					order_diff_games = 'casu'
				ord_casual = random.randint(0,4)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_quan_ques = 'asc'
				elif ord_casual==4:
					order_quan_ques = 'casu'
			else:
				difficulty_m = 'Hard_2'
				num_of_games = random.randint(1,3)
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario = db_self.curs.fetchone()[0]
				ord_casual = random.randint(0,6)
				if ord_casual==0 or ord_casual==1:
					order_diff_games = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_diff_games = 'asc'
				elif ord_casual==4 or ord_casual==5:
					order_diff_games = 'casu'
				elif ord_casual==6:
					order_diff_games = 'desc'
				ord_casual = random.randint(0,6)
				if ord_casual==0 or ord_casual==1:
					order_diff_games = 'same'
				elif ord_casual==2 or ord_casual==3:
					order_diff_games = 'asc'
				elif ord_casual==4:
					order_diff_games = 'casu'
				else:
					order_diff_games = 'desc'
				
				print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 1 complex Hard=> ", scenario)

			avail_varieties = ['1F','2L','3S','5K','7O','8C','9C','SUNDRY']
			variety = random.choice(avail_varieties)
			print("variety num_of_matches=1 => ",variety)
			if variety!='SUNDRY':
				criteria_arrangement = 'ignore'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety,))
				scenario = db_self.curs.fetchone()[0]
			else:
				criteria_arrangement = 'regular'
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario = db_self.curs.fetchone()[0]

			db_self.add_new_Match_table(difficulty_m, num_of_games, variety, order_diff_games, order_quan_ques, criteria_arrangement, advisable_time, scenario, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m, num_of_games, variety, order_diff_games, order_quan_ques, criteria_arrangement, advisable_time, scenario, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m, num_of_games, variety, order_diff_games, order_quan_ques, criteria_arrangement, advisable_time, scenario, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		
		elif num_of_matches==2:
			options = ['Hard_1', 'Hard_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m1 = random.choice(options)
				difficulty_m2 = difficulty_m1
			if order_diff_matches=='asc':
				difficulty_m1='Hard_1'
				difficulty_m2='Hard_2'
			if order_diff_matches=='desc':
				difficulty_m1='Hard_2'
				difficulty_m2='Hard_1'
			if order_quan_games=='same':
				casual = random.randint(0,7)
				if casual==0 or casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
				elif casual==2 or casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
				elif casual==5 or casual==6:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
				elif casual==7:
					num_of_games_m1 = 4
					num_of_games_m2 = 4
			elif order_quan_games=='casu':
				casual = random.randint(0,15)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
				elif casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==4 or casual==5:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==6:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
				elif casual==7:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
				elif casual==8 or casual==9:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==10 or casual==11:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				elif casual==12:
					num_of_games_m1 = 2
					num_of_games_m2 = 4
				elif casual==13:
					num_of_games_m1 = 4
					num_of_games_m2 = 1
				elif casual==14:
					num_of_games_m1 = 4
					num_of_games_m2 = 2
				elif casual==15:
					num_of_games_m1 = 3
					num_of_games_m2 = 4
			elif order_quan_games=='asc':
				casual = random.randint(0,10)
				if casual==0 or casual==1 or casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==3 or casual==4:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==5 or casual==6:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
				elif casual==7 or casual==8:
					num_of_games_m1 = 1
					num_of_games_m2 = 4
				elif casual==9:
					num_of_games_m1 = 2
					num_of_games_m2 = 4
				elif casual==10:
					num_of_games_m1 = 3
					num_of_games_m2 = 4
			elif order_quan_games=='desc':
				casual = random.randint(0,7)
				if casual==0 or casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				elif casual==3 or casual==4:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
				elif casual==5:
					num_of_games_m1 = 4
					num_of_games_m2 = 1
				elif casual==6:
					num_of_games_m1 = 4
					num_of_games_m2 = 2
				elif casual==7:
					num_of_games_m1 = 4
					num_of_games_m2 = 3																			
			if difficulty_m1=='Hard_1':
				ord_casual = random.randint(0,4)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m1 = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_diff_games_m1 = 'asc' 
				elif ord_casual==4:
					order_diff_games_m1 = 'casu' 
				options = ['same', 'asc', 'casu']
				order_quan_ques_m1 = random.choice(options)
			if difficulty_m1=='Hard_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m1 = random.choice(options)
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)
				ran_opt = random.randint(0,4)
				if ran_opt==0:
					order_diff_games_m2 = 'desc'
					order_quan_ques_m2 = 'desc'
			if difficulty_m2=='Hard_1':
				ord_casual = random.randint(0,7)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m2 = 'same' 
				elif ord_casual==2 or ord_casual==3 or ord_casual==4:
					order_diff_games_m2 = 'asc' 
				elif ord_casual==5 or ord_casual==6:
					order_diff_games_m2 = 'casu'
				elif ord_casual==7:
					order_diff_games_m2 = 'desc'

			if difficulty_m2=='Hard_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m1 = random.choice(options)
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)
				ran_opt = random.randint(0,4)
				if ran_opt==0:
					order_diff_games_m2 = 'desc'
					order_quan_ques_m2 = 'desc'

			avail_varieties = ['1F','2L','3S','4P','5K','7O','8C','9C','SUNDRY']
			variety_m1 = random.choice(avail_varieties)
			variety_m2 = random.choice(avail_varieties)
			print("variety M1 num_of_matches=2 => ",variety_m1)
			print("variety M2 num_of_matches=2 => ",variety_m2)

			var_ava = ['1F','2L','3S','4P,''5K','7O','8C','9C']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)

			if variety_m1=='SUNDRY':
				criteria_arrangement_m1 = 'regular'
			else:
				criteria_arrangement_m1 = 'ignore'
			if variety_m2=='SUNDRY':
				criteria_arrangement_m2 = 'regular'
			else:
				criteria_arrangement_m2 = 'ignore'		
			
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			depends = random.randint(0,1)
			if depends:
				scenario_m2 = scenario_m1
			else:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]

			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 2 complex Hard=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 2 complex Hard=> ", scenario_m2)

			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		elif num_of_matches==3:
			options = ['Medium_2','Hard_1', 'Hard_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m1 = random.choice(options)
				difficulty_m2 = random.choice(options)
				difficulty_m3 = random.choice(options)
			if order_diff_matches=='asc':
				randd = random.randint(0,1)
				if randd:
					difficulty_m1='Medium_2'
					difficulty_m2='Hard_1'
					difficulty_m3='Hard_2'
				else:
					difficulty_m1='Hard_1'
					difficulty_m2='Hard_1'
					difficulty_m3='Hard_2'
			if order_diff_matches=='desc':
				randd = random.randint(0,1)
				if randd:
					difficulty_m1='Hard_2'
					difficulty_m2='Hard_1'
					difficulty_m3='Medium_2'
				else:
					difficulty_m1='Hard_2'
					difficulty_m2='Hard_2'
					difficulty_m3='Hard_1'
			if order_quan_games=='same':
				casual = random.randint(0,4)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 3
				elif casual==2:
					num_of_games_m1 = 4
					num_of_games_m2 = 4
					num_of_games_m3 = 4
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 2
			elif order_quan_games=='casu':
					num_of_games_m1 = random.randint(2,3)
					num_of_games_m2 = random.randint(2,3)
					num_of_games_m3 = random.randint(2,3)
			elif order_quan_games=='asc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2
				elif casual==4:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==5:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 3
				elif casual==6:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
					num_of_games_m3 = 4							
			elif order_quan_games=='desc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				elif casual==2 or casual==3:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==4:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==5:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==6:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2							
			if difficulty_m1=='Medium_2':
				options = ['same', 'asc', 'casu', 'desc']
				order_diff_games_m1 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
			if difficulty_m1=='Hard_1':
				ord_casual = random.randint(0,3)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m1 = 'same' 
				elif ord_casual==2:
					order_diff_games_m1 = 'asc'
				elif ord_casual==3:
					order_diff_games_m1 = 'casu'
				ord_casual = random.randint(0,6)							
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m1 = 'same' 
				elif ord_casual==2 or ord_casual==3:
					order_quan_ques_m1 = 'asc'
				elif ord_casual==4 or ord_casual==5:
					order_quan_ques_m1 = 'casu'
				elif ord_casual==6:
					order_quan_ques_m1 = 'desc'
			if difficulty_m1=='Hard_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m1 = random.choice(options)
				order_quan_ques_m1 = random.choice(options)
			if difficulty_m2=='Medium_2':
				options = ['same', 'asc', 'casu', 'desc']
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)
			if difficulty_m2=='Hard_1':
				ord_casual = random.randint(0,5)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m2 = 'same' 
				elif ord_casual==2:
					order_diff_games_m2 = 'asc'
				elif ord_casual==3 or ord_casual==4:
					order_diff_games_m2 = 'casu'
				elif ord_casual==5:
					order_diff_games_m2 = 'desc'
				ord_casual = random.randint(0,5)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m2 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m2 = 'asc'
				elif ord_casual==3 or ord_casual==4:
					order_quan_ques_m2 = 'casu'
				elif ord_casual==5:
					order_quan_ques_m2 = 'desc'
			if difficulty_m2=='Hard_2':
				options = ['same', 'asc', 'casu']
				order_diff_games_m2 = random.choice(options)
				order_quan_ques_m2 = random.choice(options)
			if difficulty_m3=='Medium_2':
				options = ['same', 'asc', 'casu', 'desc']
				order_diff_games_m3 = random.choice(options)
				order_quan_ques_m3 = random.choice(options)
			if difficulty_m3=='Hard_1':
				ord_casual = random.randint(0,5)
				if ord_casual==0 or ord_casual==1:
					order_diff_games_m3 = 'same' 
				elif ord_casual==2:
					order_diff_games_m3 = 'asc' 
				elif ord_casual==3 or ord_casual==4:
					order_diff_games_m3 = 'casu' 
				elif ord_casual==5:
					order_diff_games_m3 = 'desc' 
				ord_casual = random.randint(0,5)
				if ord_casual==0 or ord_casual==1:
					order_quan_ques_m3 = 'same' 
				elif ord_casual==2:
					order_quan_ques_m3 = 'asc' 
				elif ord_casual==3 or ord_casual==4:
					order_quan_ques_m3 = 'casu' 
				elif ord_casual==5:
					order_quan_ques_m3 = 'desc' 
			if difficulty_m3=='Hard_2':
				options = ['same', 'asc', 'casu', 'desc']
				order_diff_games_m3 = random.choice(options)
				order_quan_ques_m3 = random.choice(options)						
			
			avail_varieties = ['1F','2L','3S','4P','5K','7O','8C','9C','SUNDRY']
			variety_m1 = random.choice(avail_varieties)
			variety_m2 = random.choice(avail_varieties)
			variety_m3 = random.choice(avail_varieties)
			print("variety M1 num_of_matches=3 => ",variety_m1)
			print("variety M2 num_of_matches=3 => ",variety_m2)
			print("variety M3 num_of_matches=3 => ",variety_m3)

			var_ava = ['1F','2L','3S','4P,''5K','7O','8C','9C']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)
			if num_of_games_m3==1 and variety_m3=='SUNDRY':
				variety_m3 = random.choice(var_ava)

			if variety_m1=='SUNDRY':
				criteria_arrangement_m1 = 'regular'
			else:
				criteria_arrangement_m1 = 'ignore'
			if variety_m2=='SUNDRY':
				criteria_arrangement_m2 = 'regular'
			else:
				criteria_arrangement_m2 = 'ignore'
			if variety_m3=='SUNDRY':
				crite_ran = random.randint(0,4)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m3 = 'regular'
				if crite_ran in (3,4):
					criteria_arrangement_m3 = 'random'
			else:
				criteria_arrangement_m2 = 'ignore'

			depends = random.randint(0,2)
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			if depends==0:
				scenario_m2 = scenario_m3 = scenario_m1
			elif depends==1:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]
				scenario_m3 = scenario_m2
			elif depends==2:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m2 = db_self.curs.fetchone()[0]
				scenario_m3 = scenario_m1

			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]
			if variety_m3!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
				scenario_m3 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 3 complex Hard=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 3 complex Hard=> ", scenario_m2)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 3 complex Hard=> ", scenario_m3)

			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m3, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		elif num_of_matches==4:
			options = ['Medium_2', 'Hard_1', 'Hard_2']
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
			difficulty_m4 = random.choice(options)
			if order_diff_matches=='same':
				difficulty_m1 = random.choice(options)
				difficulty_m2 = difficulty_m1
				difficulty_m3 = difficulty_m1
				difficulty_m4 = difficulty_m1
			if order_diff_matches=='asc':
				randd = random.randint(0,1)
				if randd:
					difficulty_m1='Medium_2'
					difficulty_m2='Hard_1'
					difficulty_m3='Hard_1'
					difficulty_m4='Hard_2'
				else:
					difficulty_m1='Hard_1'
					difficulty_m2='Hard_1'
					difficulty_m3='Hard_2'
					difficulty_m4='Hard_2'
			if order_diff_matches=='desc':
				randd = random.randint(0,1)
				if randd:					
					difficulty_m1='Hard_2'
					difficulty_m2='Hard_1'
					difficulty_m3='Hard_1'
					difficulty_m4='Medium_2'
				else:
					difficulty_m1='Hard_2'
					difficulty_m2='Hard_2'
					difficulty_m3='Hard_1'
					difficulty_m4='Hard_1'
			if order_quan_games=='same':
				casual = random.randint(0,5)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 1
					num_of_games_m4 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				elif casual==3 and ((difficulty_m1=='Medium_2') or (difficulty_m2=='Medium_2') or (difficulty_m3=='Medium_2') or (difficulty_m4=='Medium_2')):
					num_of_games_m1 = 4
					num_of_games_m2 = 4
					num_of_games_m3 = 4
					num_of_games_m4 = 4
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 2
			elif order_quan_games=='casu':
					num_of_games_m1 = random.randint(1,3)
					num_of_games_m2 = random.randint(1,3)
					num_of_games_m3 = random.randint(1,3)
					num_of_games_m4 = random.randint(1,3)
			elif order_quan_games=='asc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 3
				elif casual==2 or casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2 
					num_of_games_m4 = 3
				elif casual==4:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				elif casual==5:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				elif casual==6 and ((difficulty_m1=='Medium_2') or (difficulty_m2=='Medium_2') or (difficulty_m3=='Medium_2') or (difficulty_m4=='Medium_2')):
					num_of_games_m1 = 2
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 4
			elif order_quan_games=='desc':
				casual = random.randint(0,6)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
					num_of_games_m4 = 1
				elif casual==2 or casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				elif casual==4:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				elif casual==5:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 2
				elif casual==6 and ((difficulty_m1=='Medium_2') or (difficulty_m2=='Medium_2') or (difficulty_m3=='Medium_2') or (difficulty_m4=='Medium_2')):
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 2
			#mene sbatto le palle della difficoltà qui!!
			ord_casual = random.randint(0,9)
			if ord_casual==0 or ord_casual==1:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'same' 
				order_diff_games_m3 = 'asc' 
				order_diff_games_m4 = 'asc'
			elif ord_casual==2 or ord_casual==3:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'same' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==4 or ord_casual==5:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'same' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==6:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==7:
				order_diff_games_m1 = 'asc' 
				order_diff_games_m2 = 'same' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'casu' 
			elif ord_casual==8:
				order_diff_games_m1 = 'asc' 
				order_diff_games_m2 = 'desc' 
				order_diff_games_m3 = 'casu' 
				order_diff_games_m4 = 'asc' 
			elif ord_casual==9:
				order_diff_games_m1 = 'same' 
				order_diff_games_m2 = 'asc' 
				order_diff_games_m3 = 'desc' 
				order_diff_games_m4 = 'casu' 
			ord_casual = random.randint(0,12)
			if ord_casual==0 or ord_casual==1:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'asc' 
			elif ord_casual==2 or ord_casual==3:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'asc' 
			elif ord_casual==4 or ord_casual==5:
				order_quan_ques_m1 = 'asc' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'casu' 
				order_quan_ques_m4 = 'casu' 
			elif ord_casual==6 or ord_casual==7:
				order_quan_ques_m1 = 'asc' 
				order_quan_ques_m2 = 'same' 
				order_quan_ques_m3 = 'asc' 
				order_quan_ques_m4 = 'casu' 
			elif ord_casual==8 or ord_casual==9:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'casu' 
				order_quan_ques_m4 = 'casu' 
			elif ord_casual==10:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'desc' 
				order_quan_ques_m4 = 'casu'							
			elif ord_casual==11:
				order_quan_ques_m1 = 'same' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'desc' 
				order_quan_ques_m4 = 'asc' 
			elif ord_casual==12:
				order_quan_ques_m1 = 'desc' 
				order_quan_ques_m2 = 'asc' 
				order_quan_ques_m3 = 'desc' 
				order_quan_ques_m4 = 'casu' 

			avail_varieties = ['1F','2L','3S','4P','5K','7O','8C','9C','SUNDRY']
			variety_m1 = random.choice(avail_varieties)
			variety_m2 = random.choice(avail_varieties)
			variety_m3 = random.choice(avail_varieties)
			variety_m4 = random.choice(avail_varieties)
			print("variety M1 num_of_matches=4 => ",variety_m1)
			print("variety M2 num_of_matches=4 => ",variety_m2)
			print("variety M3 num_of_matches=4 => ",variety_m3)
			print("variety M4 num_of_matches=4 => ",variety_m4)

			var_ava = ['1F','2L','3S','4P,''5K','7O','8C','9C']
			if num_of_games_m1==1 and variety_m1=='SUNDRY':
				variety_m1 = random.choice(var_ava)
			if num_of_games_m2==1 and variety_m2=='SUNDRY':
				variety_m2 = random.choice(var_ava)
			if num_of_games_m3==1 and variety_m3=='SUNDRY':
				variety_m3 = random.choice(var_ava)
			if num_of_games_m4==1 and variety_m4=='SUNDRY':
				variety_m4 = random.choice(var_ava)

			if variety_m1=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m1 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m1 = 'random'
			else:
				criteria_arrangement_m1 = 'ignore'
			
			if variety_m2=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m2 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m2 = 'random'
			else:
				criteria_arrangement_m2 = 'ignore'
			
			if variety_m3=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m3 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m3 = 'random'
			else:
				criteria_arrangement_m3 = 'ignore'
			
			if variety_m4=='SUNDRY':
				crite_ran = random.randint(0,3)
				if crite_ran in [0,1,2]:
					criteria_arrangement_m4 = 'regular'
				if crite_ran==3:
					criteria_arrangement_m4 = 'random'
			else:
				criteria_arrangement_m4 = 'ignore'

			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m1 = db_self.curs.fetchone()[0]
			db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
			scenario_m2 = db_self.curs.fetchone()[0]
			depends = random.randint(0,5)
			if depends==0 or depends==1 or depends==2:
				scenario_m3 = scenario_m2
				scenario_m2 = scenario_m1		
				scenario_m4 = scenario_m3
			if depends==3 or depends==4:
				scenario_m3 = scenario_m1		
				scenario_m4 = scenario_m2
			if depends==5:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
				scenario_m3 = db_self.curs.fetchone()[0]
				decide = random.randint(0,1)
				if decide:
					scenario_m4 = scenario_m2
				else:
					scenario_m4 = scenario_m3

			if variety_m1!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
				scenario_m1 = db_self.curs.fetchone()[0]
			if variety_m2!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
				scenario_m2 = db_self.curs.fetchone()[0]
			if variety_m3!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
				scenario_m3 = db_self.curs.fetchone()[0]
			if variety_m4!='SUNDRY':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m4,))
				scenario_m4 = db_self.curs.fetchone()[0]

			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 1 complex Hard=> ", scenario_m1)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 2 complex Hard=> ", scenario_m2)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 3 complex Hard=> ", scenario_m3)
			print("SCENARIO SCELTO ALL'INIZIO IN KID_NOT_GIVEN 4 complex Hard=> ", scenario_m4)


			db_self.add_new_Match_table(difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m3, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			db_self.add_new_Match_table(difficulty_m4, num_of_games_m4, variety_m4, order_diff_games_m4, order_quan_ques_m4, criteria_arrangement_m4, advisable_time, scenario_m4, 0)
			db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
			match_last = db_self.curs.fetchone()[0]
			dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

			print("exiting from submethod add_new_Session_table KID NOT GIVEN")
