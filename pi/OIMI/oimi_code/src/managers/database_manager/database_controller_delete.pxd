# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Cancel
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef cancel_from_needs(db_self, str which_purpose, str which_scope)
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Delete
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef delete_from_id(db_self, int id_to_remove)
cdef delete_row_from_bridge(db_self, str which_table, int first_param, int second_param, int must_eliminate)
cdef delete_row_from_bridge_str(db_self, str which_table, int first_param, str second_param, int must_eliminate)
cdef delete_all_from_bridge_first(db_self, str which_table, int first_id, int must_eliminate)
cdef delete_all_from_bridge_second(db_self, str which_table, int second_id)
cdef delete_from_id_only_db_default(db_self, int id_to_remove)