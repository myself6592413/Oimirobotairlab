"""Info:
Notes:
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ===============================================================================================================================
#  Imports:
# ===============================================================================================================================
import sys
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
import database_manager as db
import audio_blob_conversion as abc
from subprocess import Popen, run
import oimi_queries as oq
import db_default_lists as dbdl
# ===============================================================================================================================
#  Main:
# ===============================================================================================================================
if __name__ == '__main__':
	#abc.blob_creation()
	# now the costructor is changed !!! with argument adjust_dates yes, no
	oimidb = db.DatabaseManager()
	with oimidb:
		############################################################################ NEW OK NEW OK NEW OK
		oimidb.prova_preliminary_activities()
		oimidb.build_Kids()
		oimidb.build_Allidtokens()
		oimidb.build_OimiSettings()

		# oimidb.build_Impositions()
		# oimidb.build_Needs()
		# oimidb.build_Patches()
		# oimidb.start_patches_trigger()
		# oimidb.place_patches_into_Patches()
		# oimidb.build_Audios()
		# oimidb.build_Kind_games()
		# oimidb.build_Type_games()

		# oimidb.build_Substages()
		# oimidb.build_Prompt_Substages()
		# oimidb.build_Reward_Substages()
		# oimidb.build_Mood_Substages()
		# oimidb.build_Reaction_Substages()
		# oimidb.build_Children_Substages()
		# oimidb.build_Stages()
		# oimidb.build_prompt_Stages()
		# oimidb.build_reward_Stages()
		# oimidb.build_mood_Stages()
		# oimidb.build_reaction_Stages()
		# ########################################################### LIMITE 2 

		# oimidb.build_Issues()
		# oimidb.build_Comorbidities()
		# oimidb.build_Strengths()		
		# oimidb.build_Symptoms()
		# oimidb.build_Achievements()

		# oimidb.build_Kids_Parameters()
		# oimidb.build_KidAchievement()
		# oimidb.build_KidSymptom()
		# oimidb.build_KidStrength()
		# oimidb.build_KidIssue()
		# oimidb.build_ClassFreeTarget()
		# oimidb.build_KidComorbidity()

		# oimidb.build_KidNeed()
		# oimidb.build_KidImposition()

		# oimidb.build_OldKidSymptom()
		# oimidb.build_OldKidNeed()
		# oimidb.build_OldKidIssue()
		# oimidb.build_OldKidComorbidity()
		# oimidb.build_OldKidStrength()
	
		# ########################################################### LIMITE 3
		# oimidb.build_Therapies()
		# oimidb.build_Targets()
		# oimidb.build_Treatments()
		# oimidb.build_TreatmentTarget()
		
		# oimidb.build_Treatments_Traits()
		# oimidb.build_Class_free()
		# oimidb.build_Entertainments()
		# oimidb.start_free_games_trigger()
		# oimidb.place_entertainments()
		
		# oimidb.build_Questions()
		# oimidb.insert_in_questions()
		# oimidb.build_Scenarios()
		# oimidb.insert_in_scenarios()
		# oimidb.build_ScenarioQuestion()
		# oimidb.populate_scenarios_questions()
		# oimidb.build_Games()		
		# oimidb.build_Matches()
		# oimidb.build_Sessions()
		# ########################################################### LIMITE 4

		# oimidb.build_SessionMatch()
		# oimidb.build_MatchGame()
		# oimidb.build_GameQuestion()
		# oimidb.build_ActivityRules()
		# oimidb.build_QuestionPatch()
		# oimidb.place_data_into_Questions_Patches()

		# oimidb.build_TherapyTreatment()
		# oimidb.build_TreatmentEntertainment()
		# oimidb.build_TreatmentSession()
		# oimidb.build_TreatmentPastime()
		
		# oimidb.build_KidTherapy()
		# oimidb.build_KidTreatment()
		# oimidb.build_KidEntertainment()
		# oimidb.build_KidSession()
		# # ########################################################### LIMITE 5
		#oimidb.build_ScenarioType()
		#oimidb.build_ScenarioKind()
		#oimidb.build_Scenario4PSUQuestion()
		#oimidb.build_Scenario4PCOQuestion()
		#oimidb.populate_scenarios_kinds()
		#oimidb.populate_scenarios_types()
		
		#oimidb.build_SessionEnforcement()
		#oimidb.build_SessionImposition()
		#oimidb.build_SessionNeed()
		
		# ########################################################### LIMITE 6 TO DO!!
		# ########################################################### LIMITE 6 TO DO!!
		# ########################################################### LIMITE 6 TO DO!!
		oimidb.create_Short_recap_all_kids()
		oimidb.create_Full_sessions_played_recap()
		oimidb.create_Full_played_recap_giga()
		oimidb.fix_contraints_Full_sesions_summary()
		oimidb.fix_contraints_Full_therapies_summary()
		oimidb.create_Full_played_recap_giga()
		oimidb.fix_contraints_Full_therapies_summary()

		oimidb.create_Full_entertainments_played()
		oimidb.fix_contraints_Full_entertainment_played_summary()
		oimidb.create_definitive_one()
		oimidb.create_definitive_two()
		oimidb.compose_definitive()
		# ########################################################### LIMITE 6 ok 
	
		oimidb.construct_ResultsTotalQuestion()
		oimidb.construct_ResultsTotalKindOfGame()
		oimidb.construct_ResultsTotalTypeOfGame()
		oimidb.construct_ResultsTotalSession()
		oimidb.construct_ResultsTotalMatch()
		oimidb.construct_ResultsTotalGame()

		########################################################### LIMITE 7 fine creazione pristine db!!! tempo impegato per far tutto => 
		# oimidb.change_all_advisable_times()
		
		# oimidb.add_new_SessionEnforcement(20000, 'choose two_match')

		########################################################### LIMITE 8
		#oimidb.add_new_Therapy(10000, 2, 'Eibi') #ok

		########################################################### LIMITE 8 prove
		
		#impo_1 = "ask_quest_82"
		#impo_2 = "type_game_1FGCA"
		
		#####oimidb.add_new_Treatment(10005, None, None, None, 2, 1, 1)


		#oimidb.include_therapist_grade_session(80000, 20000, 'Good')
		#oimidb.include_therapist_grade_entertainment(80000, 50000, 'Good')
		"""
		impo00 = "choose two_games"
		impo01 = "choose two_match"

		impo_1 = "type_game_1FSUS"
		impo_2 = "type_game_1FSUD"
		impo_3 = "type_game_1FSUDT"
		impo_4 = "type_game_1FCOS"
		impo_5 = "type_game_1FCOD"
		impo_6 = "type_game_1FCODT"
		impo_7 = "type_game_1FSUCOS"
		impo_8 = "type_game_1FSUCOD"
		impo_9 = "type_game_1FSUCODT"
		impo_10 = "type_game_1FGCA"
		impo_11 = "type_game_1FCACO"
		impo_12 = "type_game_1FCACOA"
		impo_13 = "type_game_2LST"
		impo_14 = "type_game_2LSU"
		impo_15 = "type_game_2LTWO"
		impo_16 = "type_game_2LDSU"
		impo_17 = "type_game_2LDSUOC"
		impo_18 = "type_game_2LDBOC"
		impo_19 = "type_game_2LDSUOB"
		impo_20 = "type_game_2LDBOB"
		impo_21 = "type_game_2LDSU2"
		impo_22 = "type_game_3SSU"
		impo_23 = "type_game_3SCO"
		impo_24 = "type_game_4PSU"
		impo_25 = "type_game_4PCO"
		impo_26 = "type_game_5KS"
		impo_27 = "type_game_5KD"
		impo_28 = "type_game_6I"
		impo_29 = "type_game_7OSU2"
		impo_30 = "type_game_7OSU3"
		impo_31 = "type_game_7OSU4"
		impo_32 = "type_game_7OCO2"
		impo_33 = "type_game_7OCO3"
		impo_34 = "type_game_7OCO4"
		impo_35 = "type_game_7OL2"
		impo_36 = "type_game_7OL3"
		impo_37 = "type_game_8QTSUSC1"
		impo_38 = "type_game_8QTSUSC2"
		impo_39 = "type_game_8QTCOSC1"
		impo_40 = "type_game_8QTCOSC2"
		impo_41 = "type_game_8QTKNSC"
		impo_42 = "type_game_8QTKNDC"
		impo_43 = "type_game_8QTPRSC"
		impo_44 = "type_game_8QTSUDC1"
		impo_45 = "type_game_8QTSUDC2"
		impo_46 = "type_game_8QTCODC1"
		impo_47 = "type_game_8QTCODC2"
		impo_48 = "type_game_8QTSUSB1"
		impo_49 = "type_game_8QTSUSB2"
		impo_50 = "type_game_8QTCOSB1"
		impo_51 = "type_game_8QTCOSB2"
		impo_52 = "type_game_8QTKNSB"
		impo_53 = "type_game_8QTPRSB"
		impo_54 = "type_game_8QTSUDB1"
		impo_55 = "type_game_8QTSUDB2"
		impo_56 = "type_game_8QTCODB1"
		impo_57 = "type_game_8QTCODB2"
		impo_58 = "type_game_8QTSUSCOC1"
		impo_59 = "type_game_9CNSU"
		impo_60 = "type_game_9CNCO"
		impo_61 = "type_game_9CNSSU"
		impo_62 = "type_game_9CNSPA"
		impo_63 = "type_game_9CNSPX"


		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo00)
		impositions_list.append(impo01)
		#impositions_list.append(impo_24)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 0
		manda_need = 0
		kid = 10001
		### default Session no kid 
		#oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		### default Session with kid 
		#oimidb.add_new_Session(kid,manda_impo,manda_need,impos_this_time)
		


		#oimidb.add_new_Session(kid,manda_impo,manda_need)
		

		"""

		"""
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_2)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_3)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_4)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_5)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_6)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_7)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_8)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_9)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_10)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_11)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_12)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_13)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_14)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_15)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_16)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_17)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_18)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_19)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_20)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		
		
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_21)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_22)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_23)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_24)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_25)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_26)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_27)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_28)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_29)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_30)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_31)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_32)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_33)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_34)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_35)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_37)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_38)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_39)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_40)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_41)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_42)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_43)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_44)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_45)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_46)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_47)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_48)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_49)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_50)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_51)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_52)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_53)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_54)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_55)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_56)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_57)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_58)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_59)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_60)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_61)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_62)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		###################################################################### 1 
		level_area = 'session_limit_area'
		impositions_list = []
		impositions_list.append(impo_63)
		impos_this_time = []
		impos_this_time.append(impositions_list)
		impos_this_time.append(level_area)
		print("impos_this_time to pass!!! {}".format(impos_this_time))
		manda_impo = 1 
		manda_need = 0
		oimidb.add_new_Session(0,manda_impo,manda_need,impos_this_time)
		"""











		

		#add_new_Treatment(self, int kid_id, str target_goal_1, str target_goal_2, str target_goal_3, int num_of_pastimes, int num_of_sessions, int num_of_entertainments)
		#add_new_Entertainment(self, int kid_id, str class_of_game, str specific_guidelines)



		############################################################################ NEW OK NEW OK NEW OK
		########################################################### fine!!! fine!!! fine!!!
		""" DOPO!!!!! PROVE ADD / DELETE

		# oimidb.start_pastimes_trigger()
		# oimidb.start_pastimes_trigger_2()
		# oimidb.add_new_TreatmentEntertainment(80000, 50005)
		# oimidb.add_new_TreatmentSession(80000, 20008)
		# oimidb.refresh_Treatments_Pastimes_table()



		#oimidb.add_new_KidIssue(10004, 'Social_relations')
		#oimidb.build_SessionNeed()
		#oimidb.add_new_SessionNeed(20000, 10021)
		#oimidb.add_new_KidStrength(10004, 'Exceptional_memory')
		#oimidb.add_new_KidComorbidity(10004, 'Anxiety')
		
		#oimidb.add_to_OldKidNeed(10004, 10000)
		#oimidb.add_to_OldKidSymptom(10004, 'Calculate')
		#oimidb.add_to_OldKidIssue(10004, 'Family_relationships')
		#oimidb.add_to_OldKidComorbidity(10004, 'Sleep_disturbances')
		#oimidb.add_to_OldKidStrength(10004, 'Great_visual_learner')


		oimidb.delete_by_str('Kids_Symptoms', 10002, 'Respect_rules', 0)
		oimidb.delete_by_str('Kids_Issues', 10001, 'Tolerance', 0)
		oimidb.delete_by_str('Kids_Comorbidities', 10002, 'Sleep_disturbances', 0)
		oimidb.delete_by_str('Kids_Strengths', 10002, 'Follow_instructions_and_rules', 0)















		"""
		###########################################################

		#GIUSTO 2021!!!!
		#oimidb.create_the_entire_database_from_scratch()
		#GIUSTO!!!!
		"""
		oimidb.build_Kids()
		oimidb.build_Allidtokens()
		oimidb.build_OimiSettings()
		oimidb.build_Achievements()
		oimidb.build_Needs()
		oimidb.build_Impositions()
		oimidb.build_Symptoms()
		oimidb.build_Patches()
		oimidb.start_patches_trigger()
		oimidb.place_patches_into_Patches()

		oimidb.build_Kind_games()
		oimidb.build_Type_games()
		oimidb.build_Audios()
		oimidb.build_Questions()
		oimidb.insert_in_questions()
		oimidb.build_Scenarios()
		oimidb.insert_in_scenarios()
		oimidb.build_ScenarioQuestion()
		oimidb.populate_scenarios_questions()
		oimidb.build_Games()		
		oimidb.build_Matches()
		oimidb.build_Sessions()
		oimidb.build_SessionMatch()
		oimidb.build_MatchGame()
		oimidb.build_GameQuestion()
		oimidb.build_QuestionPatch()
		oimidb.place_data_into_Questions_Patches()
		
		oimidb.build_ScenarioType()
		oimidb.build_ScenarioKind()
		oimidb.build_Scenario4PSUQuestion()
		oimidb.build_Scenario4PCOQuestion()
		oimidb.populate_scenarios_kinds()
		oimidb.populate_scenarios_types()

		oimidb.build_KidSession() #inside create kidmatch kidgame kidquestion
		oimidb.build_KidImposition()
		
		oimidb.build_KidAchievement()
		oimidb.build_KidSymptom()
		oimidb.build_KidNeed()
		oimidb.build_NeedQuestion()
		oimidb.build_ImpositionQuestion()
		
		oimidb.build_SessionImposition()
		oimidb.build_SessionNeed()

		#oimidb.create_Full_played_recap()
		oimidb.create_Full_played_recap_giga()
		
		oimidb.construct_ScoresTotalQuestion()
		oimidb.construct_ScoresTotalKindOfGame()
		oimidb.construct_ScoresTotalTypeOfGame()
		oimidb.construct_ScoresTotalSession()
		oimidb.construct_ScoresTotalMatch()
		oimidb.construct_ScoresTotalGame()
		###########new!!!
		oimidb.change_all_advisable_times()
		"""
		#FINE GIUSTO!!!
		#oimidb.execute_a_query("DROP TABLE IF EXISTS Matches_Games")
		#oimidb.build_MatchGame()
		#oimidb.execute_a_query("DROP TABLE IF EXISTS Sessions_Matches")
		#oimidb.build_SessionMatch()
		#oimidb.execute_a_query("DROP TABLE IF EXISTS Questions_Patches")
		#oimidb.build_QuestionPatch()

		#oimidb.generic_commit()
		#oimidb.execute_a_query(oq.sql_bridge_maga)
		#oimidb.curs.executemany('''INSERT INTO Matches_Games (match_id, game_id) VALUES (?,?)''', dbdl.all_matches_games)
		#oimidb.generic_commit()

		#oimidb.execute_a_query("DELETE FROM Matches WHERE match_id = 3053")
		#oimidb.generic_commit()
		"""
		every_question = [[[1011, 1309], [11959, 11957]], [[3033], [11938]]]
		all_values_questions_tmp = []
		val_lis = []
		vall = []
		for qq in range(len(every_question)):
			print("qq ", every_question[qq])
			for qq1 in range(len(every_question[qq])):
				print("qq1 ", every_question[qq][qq1])
				for qqq in range(len(every_question[qq][qq1])):
					print("qqq ===> ", every_question[qq][qq1][qqq])
					val_sql = "SELECT value FROM Questions where question_id=?"
					v_q = oimidb.execute_new_query(val_sql,(every_question[qq][qq1][qqq]))
					print("v_q is ", v_q)
					val_lis.append(v_q[0])
					print("val_lis = ", val_lis)
				vall.append(val_lis)
				print("vall ", vall)
				val_lis = []
			all_values_questions_tmp.append(vall)
			vall = []
		print("allvallist")
		print(all_values_questions_tmp)
		"""


		#a1 = 'Kind 1F done'
		#a2 = 'Kind 2L done'
		#a3 = 'Kind 3S done'
		#id_child = 1001
		##achi_kid = oimidb.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? ORDER BY kid_id ASC LIMIT 18",(id_child))
		#achi_kid = oimidb.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?) ",(id_child,a1,a2,a3))
		##achi_kid = oimidb.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?) ",(id_child,achi1,achi2))
		#print(achi_kid)	
		

		
		############################################################################################################################################################
		############################################################################################################################################################
		############################################################################################################################################################
		############################################################################################################################################################
		############################################################################################################################################################
		############################################################################################################################################################
		############################################################################################################################################################
		#oimidb.change_all_desiderable_times()
		
		##??? non funziona dovrei settare tutti i tipi tabella !!oimidb.execute_a_query("UPDATE Full_played_summary SET time_of_session=cast(time_of_session AS INTEGER)")

		#id_to_remove = 2061
		#oimidb.execute_new_query('''DELETE FROM Games WHERE disposable = 1 AND game_id IN (
		#	SELECT Games.game_id FROM Games JOIN (Sessions_Matches JOIN Matches_Games USING (match_id)) USING (game_id)
		#	WHERE session_id = ?)''',(id_to_remove,))
		#oimidb.generic_commit()


		#oimidb.execute_a_query("""DELETE FROM Full_played_recap_giga WHERE rowid NOT IN (SELECT min(rowid) FROM Full_played_recap_giga GROUP BY
		#	kid_id,session_id,match_id,game_id,question_id)""")
		#oimidb.generic_commit()


		#for k,s,g in res_gk:
		#	nag,ncg,neg,nig,nag,tog,qfg,gfg,vfg= 1,2,3,4,5,6,'quan','grad', 9
		#	
		#	#oimidb.execute_param_query('''UPDATE Kids_Games SET num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, 
		#	#	time_of_game = ?, quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ? WHERE kid_id = ? AND session_id = ? AND game_id = ?''',(nag, ncg, neg, nig, nag, tog, qfg, gfg, vfg,k,s,g))
		#	oimidb.execute_param_query('''UPDATE Kids_Games SET num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, 
		#		time_of_game = ?, quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ? WHERE kid_id IN (
		#		SELECT kid_id FROM Kids_Games WHERE kid_id = ?) AND session_id IN (SELECT session_id FROM Kids_Games WHERE session_id = ?) AND game_id IN (SELECT game_id FROM Kids_Games WHERE game_id = ?)''',(nag, ncg, neg, nig, nag, tog, qfg, gfg, vfg,k,s,g))
		#	oimidb.generic_commit()
		#		#num_answers_game = ?, 
		#		#num_corrects_game = ?, 
		#		#num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?,
		#		#quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
		#		#WHERE kid_id, session_id, game_id IN (
		#		#	SELECT Kids_Games_tmp.kid_id, Kids_Games_tmp.game_id FROM Kids_Games_tmp
		#		#	WHERE Kids_Games_tmp.kid_id = ? AND Kids_Games_tmp.sess_id = ? AND Kids_Games_tmp.game_id = ?''',(nag, ncg, neg, nig, nag, tog, qfg, gfg, vfg, k, s, g))


		#oimidb.execute_param_query("UPDATE Sessions SET disposable = 1 WHERE session_id = ?",(id_sess))
		#oimidb.execute_param_query("""UPDATE Matches SET disposable = 1 WHERE match_id IN (
		#	SELECT match_id FROM Sessions_Matches WHERE session_id = ?)""",(id_sess))
		#oimidb.execute_param_query("""UPDATE Games SET disposable = 1 WHERE game_id IN (
		#	SELECT game_id FROM Sessions_Matches JOIN Matches_Games USING (match_id) WHERE session_id = ?)""",(id_sess))
		#oimidb.generic_commit()
		#oimidb.delete_by_id(id_sess)
		
		"""
		aa = []
		matches = oimidb.execute_a_query("SELECT * FROM Matches")
		for m in matches:
			time_to_change = 0
			a = list(m)
			print(a)
			print()
			print()
			get_num_g = a[1]
			print("get_num_g ======================> ", get_num_g)
			get_num_ques = oimidb.execute_new_query("SELECT num_questions FROM Matches_Games JOIN Games USING (game_id) WHERE match_id = ?",(a[0]))
			print("get_num_q ======================> ", get_num_ques)
			get_kind_game = oimidb.execute_new_query("SELECT kind FROM Matches_Games JOIN Games USING (game_id) WHERE match_id = ?",(a[0]))
			print("get_num_g prima ======================> ", get_kind_game)
			################################################ NEW!! TENGO QUESTO
			for i in range(len(get_kind_game)):
				if get_kind_game[i] in ['1F','2L','5K']:	
					if get_num_ques[i]==1:
						time_to_change+=10
					if get_num_ques[i]==2:
						time_to_change+=20
					if get_num_ques[i]==3:
						time_to_change+=30
					if get_num_ques[i]==4:
						time_to_change+=40
				if get_kind_game[i] in ['3S','4P']:
					if get_num_ques[i]==1:
						time_to_change+=12
					if get_num_ques[i]==2:
						time_to_change+=24
					if get_num_ques[i]==3:
						time_to_change+=36
					if get_num_ques[i]==4:
						time_to_change+=48
				if get_kind_game[i]=='7O':
					if get_num_ques[i]==1:
						time_to_change+=15
					if get_num_ques[i]==2:
						time_to_change+=30
					if get_num_ques[i]==3:
						time_to_change+=55
					if get_num_ques[i]==4:
						time_to_change+=60		
				if get_kind_game[i] in ['8Q','9C']:
					if get_num_ques[i]==1:
						time_to_change+=18
					if get_num_ques[i]==2:
						time_to_change+=36
					if get_num_ques[i]==3:
						time_to_change+=54
					if get_num_ques[i]==4:
						time_to_change+=72
				if get_kind_game[i]=='6I':
					if get_num_ques[i]==1:
						time_to_change+=20
					if get_num_ques[i]==2:
						time_to_change+=40
					if get_num_ques[i]==3:
						time_to_change+=60
					if get_num_ques[i]==4:
						time_to_change+=80
				#print("partial result => ", time_to_change)
			
			print("time_to_change ???? ",time_to_change)
			print("time_to_change ???? ",time_to_change)
			print("leggo questi dati all interno di modify_with_update_single_match_changing_time ")
			print(a)
			print("##########")
			print(a[0])
			print(a[1])
			print(a[2])
			print(a[3])
			print(a[4])
			print(a[5])
			print(a[6])
			print(a[7])
			print(a[8])
			print(a[9])
			print(a[10])
			print(a[11])
			print(a[12])
			print("##########")
			###########nb 7 e 8 inverted in file!!!
			line_to_mod = "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, '{}'),".format(a[1],a[2],a[3],a[4],a[5],a[6],a[8],a[7],a[9],a[12])
			new_line_ok = "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, '{}'),".format(a[1],a[2],a[3],a[4],a[5],a[6],a[8], time_to_change, a[9],a[12])
			print("TESTO DA modify_with_update_in_lines!!! ==> ",line_to_mod)
			cosaquando = "s/$(printf '\t'){}/$(printf '\t'){}/".format(line_to_mod,new_line_ok)
			command_ins = 'sed -i "{}"'.format(cosaquando)
			print("command instruction ===>> ", command_ins)
			cmd_args = '/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py'
			#cmd_args = '/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py'
			def_command = "{} {}".format(command_ins,cmd_args)
			print("def_command =============================================>", def_command)
			run(def_command, shell=True)
			"""
			
