"""Info:
	Oimi robot database assign a default session for a kid is playing the first time
Notes: 
	For detailed info, look at ./code_docs_of_dabatase_manager
Created by Colombo Giacomo, Politecnico di Milano 2020 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
#cimport database_controller_take
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef take_one_default_session_56(db_self, int which_kid):
	db_self.curs.execute("SELECT COUNT(kid_id) FROM Kids_Symptoms WHERE kid_id=?",(which_kid,))
	num_symptoms = db_self.curs.fetchone()[0]
	if num_symptoms < 3:
		db_self.curs.execute("SELECT session_id FROM Sessions WHERE status='default' AND complexity = 'Normal' LIMIT 1")
		which_session = db_self.curs.fetchone()[0]
		db_self.add_new_KidSession_table(which_kid, which_session)
	else:
		db_self.curs.execute("SELECT session_id FROM Sessions WHERE status='default' AND complexity = 'Easy' LIMIT 1")
		which_session = db_self.curs.fetchone()[0]
		db_self.add_new_KidSession_table(which_kid, which_session)

cdef take_one_default_session_78(db_self, int which_kid):
	db_self.curs.execute("SELECT COUNT(kid_id) FROM Kids_Symptoms WHERE kid_id=?",(which_kid,))
	num_symptoms = db_self.curs.fetchone()[0]
	if num_symptoms < 3:
		db_self.curs.execute("SELECT session_id FROM Sessions WHERE status='default' AND complexity = 'Normal' LIMIT 1")
		which_session = db_self.curs.fetchone()[0]
		db_self.add_new_KidSession_table(which_kid, which_session)
	else:
		db_self.curs.execute("SELECT session_id FROM Sessions WHERE status='default' AND complexity = 'Easy' LIMIT 1")
		which_session = db_self.curs.fetchone()[0]
		db_self.add_new_KidSession_table(which_kid, which_session)

cdef take_one_default_session_910(db_self, int which_kid):
	db_self.curs.execute("SELECT COUNT(kid_id) FROM Kids_Symptoms WHERE kid_id=?",(which_kid,))
	num_symptoms = db_self.curs.fetchone()[0]
	if num_symptoms < 3:
		db_self.curs.execute("SELECT session_id FROM Sessions WHERE status='default' AND complexity = 'Medium' LIMIT 1")
		which_session = db_self.curs.fetchone()[0]
		db_self.add_new_KidSession_table(which_kid, which_session)
	else:
		db_self.curs.execute("SELECT session_id FROM Sessions WHERE status='default' AND complexity = 'Normal' LIMIT 1")
		which_session = db_self.curs.fetchone()[0]
		db_self.add_new_KidSession_table(which_kid, which_session)
