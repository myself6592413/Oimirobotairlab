"""Oimi SISTEMO QUI!!!!!
robot database_manager, oimi_database.db whole infrastructure creation and maintanance
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
from typing import Optional
cimport database_controller_add_into as dca
cimport database_controller_add_into_part_6 as dca6
cimport database_controller_delete as dcd
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef sort_games_in_correct_order_if_m_regular(db_self, this_match):
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	print("ENTRO IN sort_games_in_correct_order_if_m_regular")
	db_self.curs.execute("SELECT game_id,kind FROM Matches_Games JOIN Games USING (game_id) WHERE match_id = ?",(this_match,))
	m_games, m_kinds = [],[]
	res_q1 = db_self.curs.fetchall()
	for i in range(len(res_q1)):
		m_games.append(res_q1[i][0])
		m_kinds.append(res_q1[i][1])
	print("m_kinds list to change of this match ", m_kinds)
	print("m_games list to change of this match ", m_games) 
	sorted_m_kinds = sorted(m_kinds)
	if m_kinds==sorted_m_kinds:
		return
	sorted_games = []
	for k in sorted_m_kinds:
		for i in range(len(res_q1)):
			if k in res_q1[i][1]:
				if res_q1[i][0] not in sorted_games:
					sorted_games.append(res_q1[i][0])

	all_g_stats = []
	all_g_quests = []
	for g in sorted_games:
		db_self.curs.execute("SELECT * FROM Games WHERE game_id = ?",(g,))
		res_q3 = db_self.curs.fetchall()
		stats_game = [item for sublist in res_q3 for item in sublist]
		print("for game num {}".format(g))
		print("stats_game are___ {}".format(stats_game))
		all_g_stats.append(stats_game)

	print("sorted_kinds", sorted_m_kinds)
	print("sorted_games", sorted_games)

	questions_ok = []
	for g in sorted_games:
		db_self.curs.execute("SELECT question_id FROM Games_Questions WHERE game_id = ?",(g,))
		res_q4 = db_self.curs.fetchall()
		gques = [item for sublist in res_q4 for item in sublist]
		questions_ok.append(gques)

	print()
	print("all_g_stats = ", all_g_stats)
	print()
	print("questions_ok = ", questions_ok)

	for mg in m_games:
		db_self.execute_param_query("UPDATE Games SET disposable = 1 WHERE game_id = ?",(mg))
		db_self.conn.commit()
		dcd.delete_from_id(db_self, mg)

	for gg in range(len(all_g_stats)):
		dca6.add_again_game_after_order_modifications(db_self, all_g_stats[gg][1], all_g_stats[gg][2], all_g_stats[gg][3], all_g_stats[gg][4], all_g_stats[gg][5], all_g_stats[gg][6], 
			all_g_stats[gg][7], all_g_stats[gg][8], all_g_stats[gg][9], all_g_stats[gg][10], questions_ok[gg])
		g_last = db_self.curs.execute("SELECT game_id FROM Games ORDER BY game_id DESC LIMIT 1")
		game_last = db_self.curs.fetchone()[0]
		dca.add_new_MatchGame_table(db_self, this_match, game_last)

