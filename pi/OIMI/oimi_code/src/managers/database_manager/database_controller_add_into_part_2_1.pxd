# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Define
# ----------------------------------------------------------------------------------------------------------------------------------------------------------			
cdef define_matches_when_complexity_is_medium(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_medium_2(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_medium_3(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_medium_4(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef define_matches_when_complexity_is_hard(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_hard_2(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_hard_3(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_hard_4(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)