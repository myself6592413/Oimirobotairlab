"""Info:
	Oimi robot database_controller for special methods used online during a game session.
	Choose the proper audio according to the specified session, match, game
	For detailed info, look at ./code_docs_of_dabatase_manager --> generate online
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
#sys.path.insert(1, os.path.join(sys.path[0], '.'))
from typing import Optional
cimport database_controller_generate_online
cimport modify_default_lists as mdl
# ================================================================================================================================================================================================================
# Methods
# ================================================================================================================================================================================================================
cdef generate_audio_intro_exit_session_table(db_self):
	db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
	sess_last = db_self.curs.fetchone()[0]
	print("sess last for generating audio_intro e audio_exit session is=={}".format(sess_last))
	db_self.curs.execute("SELECT COUNT(session_id) FROM Kids_Sessions WHERE session_id = ?",(sess_last,))
	num_session_of_current_kid = db_self.curs.fetchone()[0]
	if num_session_of_current_kid<=1: #per adesso uguali
		db_self.curs.execute("UPDATE Sessions SET audio_intro='audio_12', audio_exit='audio_13' WHERE session_id = ?"(sess_last,))
	else:
		db_self.curs.execute("UPDATE Sessions SET audio_intro='audio_12', audio_exit='audio_13' WHERE session_id = ?"(sess_last,))

cdef generate_audio_intro_exit_match_table(db_self):
	db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
	sess_last = db_self.curs.fetchone()[0]
	print("sess last for generating audio_intro e audio_exit session is=={}".format(sess_last))
	db_self.curs.execute("SELECT match_id FROM Sessions_Matches WHERE session_id =?",(sess_last,))
	mama = db_self.curs.fetchall()
	matches_last = [item for sublist in mama for item in sublist]
	print("ECCOOO {}".format(matches_last))
	lista_pa_ani = []
	lista_pa_col = []
	lista_pa_peo = []
	lista_pa_dress = []
	lista_pa_thi = []
	lista_pa_food = []
	for i in range(len(matches_last)):
		print("siamo in {} giro match last for generating audio_intro e audio_exit session is=={}".format(i, matches_last))
		db_self.curs.execute('''SELECT COUNT(p.patch_id) FROM Patches p 
			JOIN (Scenarios s JOIN Matches m ON s.scenario_id = m.scenario)
			ON p.patch_id = s.patch1 OR p.patch_id = s.patch2 OR p.patch_id = s.patch3 OR p.patch_id = s.patch4
			WHERE genre="Animal" AND match_id = ?''',(matches_last[i],))
		pa_gen_ani = db_self.curs.fetchone()[0]
		lista_pa_ani.append(pa_gen_ani)
		db_self.curs.execute('''SELECT COUNT(p.patch_id) FROM Patches p 
			JOIN (Scenarios s JOIN Matches m ON s.scenario_id = m.scenario)
			ON p.patch_id = s.patch1 OR p.patch_id = s.patch2 OR p.patch_id = s.patch3 OR p.patch_id = s.patch4
			WHERE genre="Solid_color" AND match_id = ?''',(matches_last[i],))
		pa_gen_col = db_self.curs.fetchone()[0]
		print("pa_gen_col is {}", format(pa_gen_col))
		lista_pa_col.append(pa_gen_col)
		db_self.curs.execute('''SELECT COUNT(p.patch_id) FROM Patches p 
			JOIN (Scenarios s JOIN Matches m ON s.scenario_id = m.scenario)
			ON p.patch_id = s.patch1 OR p.patch_id = s.patch2 OR p.patch_id = s.patch3 OR p.patch_id = s.patch4
			WHERE genre="People" AND match_id = ?''',(matches_last[i],))
		pa_gen_peo = db_self.curs.fetchone()[0]
		print("pa_gen_peo is {}", format(pa_gen_peo))
		lista_pa_col.append(pa_gen_peo)
		db_self.curs.execute('''SELECT COUNT(p.patch_id) FROM Patches p 
			JOIN (Scenarios s JOIN Matches m ON s.scenario_id = m.scenario)
			ON p.patch_id = s.patch1 OR p.patch_id = s.patch2 OR p.patch_id = s.patch3 OR p.patch_id = s.patch4
			WHERE genre="Dress" AND match_id = ?''',(matches_last[i],))
		pa_gen_dress = db_self.curs.fetchone()[0]
		print("pa_gen_dress is {}", format(pa_gen_dress))
		lista_pa_col.append(pa_gen_dress)
		db_self.curs.execute('''SELECT COUNT(p.patch_id) FROM Patches p 
			JOIN (Scenarios s JOIN Matches m ON s.scenario_id = m.scenario)
			ON p.patch_id = s.patch1 OR p.patch_id = s.patch2 OR p.patch_id = s.patch3 OR p.patch_id = s.patch4
			WHERE genre="Thing" AND match_id = ?''',(matches_last[i],))
		pa_gen_thi = db_self.curs.fetchone()[0]
		print("pa_gen_thi is {}", format(pa_gen_thi))
		lista_pa_col.append(pa_gen_thi)
		db_self.curs.execute('''SELECT COUNT(p.patch_id) FROM Patches p 
			JOIN (Scenarios s JOIN Matches m ON s.scenario_id = m.scenario)
			ON p.patch_id = s.patch1 OR p.patch_id = s.patch2 OR p.patch_id = s.patch3 OR p.patch_id = s.patch4
			WHERE genre="Food" AND match_id = ?''',(matches_last[i],))
		pa_gen_food = db_self.curs.fetchone()[0]
		print("pa_gen_food is {}", format(pa_gen_food))
		lista_pa_col.append(pa_gen_food)
	
	print("lista_pa_ani",lista_pa_ani)
	print("lista_pa_col",lista_pa_col)
	print("lista_pa_peo",lista_pa_peo)
	print("lista_pa_dress",lista_pa_dress)
	print("lista_pa_thi",lista_pa_thi)
	print("lista_pa_food",lista_pa_food)

	for pa_gen in (lista_pa_ani):
		if pa_gen==4: #due audio a caso per adessos tutti diversi ovvio
			db_self.curs.execute("UPDATE Matches SET audio_intro='audio_22', audio_exit='audio_23' WHERE match_id = ?",(matches_last[i],))
	for pa_gen in (lista_pa_col):
		if pa_gen==4: #due audio a caso per adessos tutti diversi ovvio
			db_self.curs.execute("UPDATE Matches SET audio_intro='audio_22', audio_exit='audio_23' WHERE match_id = ?",(matches_last[i],))
	for pa_gen in (lista_pa_peo):
		if pa_gen==4:
			db_self.curs.execute("UPDATE Matches SET audio_intro='audio_24', audio_exit='audio_25' WHERE match_id = ?",(matches_last[i],))
	for pa_gen in (lista_pa_dress):
		if pa_gen==4:
			db_self.curs.execute("UPDATE Matches SET audio_intro='audio_26', audio_exit='audio_27' WHERE match_id = ?",(matches_last[i],))
	for pa_gen in (lista_pa_thi):
		if pa_gen==4:
			db_self.curs.execute("UPDATE Matches SET audio_intro='audio_30', audio_exit='audio_32' WHERE match_id = ?",(matches_last[i],))
	for pa_gen in (lista_pa_food):
		if pa_gen==4:
			db_self.curs.execute("UPDATE Matches SET audio_intro='audio_32', audio_exit='audio_34' WHERE match_id = ?",(matches_last[i],))
	#sistemo lista di lista!!!
	#animali e ...
	#potrei fare quelli misti ma solo considerandone due !! e poi basta!
	else: #audio_generico #troppo diversi i generi!
		db_self.curs.execute("UPDATE Matches SET audio_intro='audio_32', audio_exit='audio_34' WHERE match_id = ?",(matches_last[i],))

cdef generate_audio_intro_exit_game_table(db_self):
	db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
	sess_last = db_self.curs.fetchone()[0]
	print("sess last for generating audio_intro e audio_exit session is=={}".format(sess_last))		
	db_self.curs.execute("SELECT game_id FROM Matches_Games JOIN Sessions_Matches using(match_id) WHERE session_id =?",(sess_last,))
	game_res= db_self.curs.fetchall()
	print(game_res)
	games_last = [item for sublist in game_res for item in sublist]
	print(games_last)
	#for elem in games_last:
	for i in range(len(games_last)):
		db_self.curs.execute('''SELECT kind FROM Games	
			WHERE game_id = ?''',(games_last[i],))
		game_ki = db_self.curs.fetchone()[0]
		if game_ki=='1F':
			db_self.curs.execute("UPDATE Games SET audio_intro='audio_40', audio_exit='audio_41' WHERE game_id = ?",(games_last[i],))
		if game_ki=='2L':
			db_self.curs.execute("UPDATE Games SET audio_intro='audio_40', audio_exit='audio_41' WHERE game_id = ?",(games_last[i],))
		if game_ki=='3S':
			db_self.curs.execute("UPDATE Games SET audio_intro='audio_40', audio_exit='audio_41' WHERE game_id = ?",(games_last[i],))
		if game_ki=='4P':
			db_self.curs.execute("UPDATE Games SET audio_intro='audio_40', audio_exit='audio_41' WHERE game_id = ?",(games_last[i],))
		if game_ki=='5K':
			db_self.curs.execute("UPDATE Games SET audio_intro='audio_40', audio_exit='audio_41' WHERE game_id = ?",(games_last[i],))
		if game_ki=='6I':
			db_self.curs.execute("UPDATE Games SET audio_intro='audio_40', audio_exit='audio_41' WHERE game_id = ?",(games_last[i],))
		if game_ki=='7O':
			db_self.curs.execute("UPDATE Games SET audio_intro='audio_40', audio_exit='audio_41' WHERE game_id = ?",(games_last[i],))
		if game_ki=='8Q':
			db_self.curs.execute("UPDATE Games SET audio_intro='audio_40', audio_exit='audio_41' WHERE game_id = ?",(games_last[i],))
		if game_ki=='9C':
			db_self.curs.execute("UPDATE Games SET audio_intro='audio_40', audio_exit='audio_41' WHERE game_id = ?",(games_last[i],))

cdef change_num_questions_of_a_game(db_self, int numq, int idg):
	print("Enter in generate....change_num_questions_of_a_game")
	db_self.curs.execute("UPDATE Games SET num_questions = ? WHERE game_id = ?",(numq,idg,))
	db_self.conn.commit()

cdef change_all_advisable_times_all_matches(db_self):
	print("entro in change_all_advisable_times_all_matches")
	matches = db_self.execute_a_query("SELECT * FROM Matches")
	for m in matches:
		time_to_change = 0
		single_m = list(m)
		print(single_m)
		print()
		print()
		get_num_g = single_m[1]
		print("get_num_g ======================> ", get_num_g)
		get_kind_game = db_self.execute_new_query("SELECT kind FROM Matches_Games JOIN Games USING (game_id) WHERE match_id = ?",(single_m[0]))
		print("get_num_g ======================> ", get_kind_game)
		get_num_ques = db_self.execute_new_query("SELECT num_questions FROM Matches_Games JOIN Games USING (game_id) WHERE match_id = ?",(single_m[0]))
		print("get_num_q ======================> ", get_num_ques)
		print("SINGOLE MATCH VARIABILE PASSED IS ==>  ")
		print(single_m)
		for i in range(len(get_kind_game)):
			if get_kind_game[i] in ['1F','2L','5K']:
				if get_num_ques[i]==1:
					time_to_change+=10
				if get_num_ques[i]==2:
					time_to_change+=20
				if get_num_ques[i]==3:
					time_to_change+=30
				if get_num_ques[i]==4:
					time_to_change+=40
			if get_kind_game[i] in ['3S','4P']:
				if get_num_ques[i]==1:
					time_to_change+=12
				if get_num_ques[i]==2:
					time_to_change+=24
				if get_num_ques[i]==3:
					time_to_change+=36
				if get_num_ques[i]==4:
					time_to_change+=48
			if get_kind_game[i]=='7O':
				if get_num_ques[i]==1:
					time_to_change+=15
				if get_num_ques[i]==2:
					time_to_change+=30
				if get_num_ques[i]==3:
					time_to_change+=55
				if get_num_ques[i]==4:
					time_to_change+=60		
			if get_kind_game[i] in ['8Q','9C']:
				if get_num_ques[i]==1:
					time_to_change+=18
				if get_num_ques[i]==2:
					time_to_change+=36
				if get_num_ques[i]==3:
					time_to_change+=54
				if get_num_ques[i]==4:
					time_to_change+=72
			if get_kind_game[i]=='6I':
				if get_num_ques[i]==1:
					time_to_change+=20
				if get_num_ques[i]==2:
					time_to_change+=40
				if get_num_ques[i]==3:
					time_to_change+=60
				if get_num_ques[i]==4:
					time_to_change+=80

		print("time_to_change ???? ",time_to_change)
		db_self.execute_new_query("UPDATE Matches SET advisable_time = ? WHERE match_id = ?",(time_to_change,single_m[0]))
		db_self.conn.commit()
		mdl.modify_with_update_single_match_changing_time(single_m, time_to_change)
	
cdef change_all_desirable_times_all_sessions(db_self):
	print("entro in change_all_desirable_times_all_sessions")
	sessions = db_self.execute_a_query("SELECT * FROM Sessions")
	for s in sessions:
		time_sess_to_change = 0
		single_s = list(s)
		print(single_s)
		get_time_match = db_self.execute_new_query("SELECT advisable_time FROM Sessions_Matches JOIN Matches USING (match_id) WHERE session_id = ?",(single_s[0]))
		print("get_time_match ======================> ", get_time_match)		
		for i in range(len(get_time_match)):
			time_sess_to_change += get_time_match[i]
		print("time_sess_to_change is ",time_sess_to_change)
		db_self.execute_new_query("UPDATE Sessions SET desirable_time = ? WHERE session_id = ?",(time_sess_to_change,single_s[0]))
		db_self.conn.commit()
		mdl.modify_with_update_desirable_time_session(single_s, time_sess_to_change)

cdef change_all_durations_or_kick_date_therapies(db_self, old_ther, date_flag):
	print("entro in change_all_durations_or_kick_date_therapies")
	current_therapies = db_self.execute_a_query("SELECT * FROM Therapies")
	print("old_ther", old_ther)
	for i in range(len(old_ther)):
		new_list = list(current_therapies[i])
		old_list = list(old_ther[i])
		print("definitivo new_list", new_list)
		print("definitivo new_list", new_list)
		print("definitivo new_list", new_list)
		print("definitivo new_list", new_list)
		print()
		print("definitivo old_list", old_list)
		print("definitivo old_list", old_list)
		print("definitivo old_list", old_list)
		if date_flag:
			print("IF DATE FLAG!!!")
			print("IF DATE FLAG!!!")
			print("IF DATE FLAG!!!")
			print("IF DATE FLAG!!!")
			print("IF DATE FLAG!!!")
			print("IF DATE FLAG!!!")
			old_list[4] = None
		print("definitivo old_list DOPO!!!", old_list)
		print("definitivo old_list DOPO!!!", old_list)
		print()
		mdl.modify_therapies_duration_or_date(old_list, new_list)

cdef change_day_treatments(db_self, old_tre):
	print("entro in change_day_treatments")
	current_treatments = db_self.execute_a_query("SELECT * FROM Treatments")
	print("old_tre", old_tre)
	for i in range(len(old_tre)):
		new_list = list(current_treatments[i])
		old_list = list(old_tre[i])
		print("definitivo new_list", new_list)
		print("definitivo new_list", new_list)
		print("definitivo new_list", new_list)
		print("definitivo new_list", new_list)
		print()
		print("definitivo old_list", old_list)
		print("definitivo old_list", old_list)
		print("definitivo old_list", old_list)
		old_list[11] = None
		print("definitivo old_list DOPO!!!", old_list)
		print("definitivo old_list DOPO!!!", old_list)
		print()
		mdl.modify_treatment_day(old_list, new_list)
