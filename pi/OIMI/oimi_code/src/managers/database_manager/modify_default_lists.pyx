"""Info:
	Oimi robot modify_default_list of database_manager --> for updating all 4 db_default files.
	The db_default_lists modules (divided in more for simplifying access speed) 
	is basically a complete database backup file, updated together with db.
	>> Writes, checks, retrieves alla data of in oimi_database.db (useful also fo creating from scratch the database with all default values).
	>> Is not an extension of the Database_Manager clmatch_data.
	>> Not for Kid's traits
Notes: For detailed info, look at ./code_docs_of_dabatase_manager --> modify_default_lists

Created by Colombo Giacomo, Politecnico di Milano 2020 """

# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys, os
import db_extern_methods as dbem
from subprocess import Popen, run
import traceback
cimport modify_default_lists
# ================================================================================================================================================================================================================
# Methods
# ================================================================================================================================================================================================================
cdef modify_Audios_list(str audio_id, str audio_path, int index_for_lastest):
	cdef:
		int j
		int N
		str line_to_include_1
		str line_to_include_2
	
	audio_rec = audio_id.split('_')
	take_num = audio_rec[1]
	print("audio_rec = {}".format(audio_rec))
	print("take_num = {}".format(take_num))

	name_file_1 = "audio_file_{0}".format(take_num)
	name_file_2 = "ca.audio_file_{0}".format(take_num)
	print("name_file 1 is {}".format(name_file_1))
	print("name_file 2 is {}".format(name_file_2))
	line_to_include_1 = "\t{} = convert_to_binary('{}')\n".format(name_file_1, audio_path) 
	line_to_include_2 = "\t('{}', '{}'),\n".format(audio_id, name_file_2)
	try:
		print("redo when available")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef modify_Kids_list(str name, str surname, int age, str level, str image_path, str details_path, str token):
	cdef:
		int j
		int N
		str put_str_1 
		str put_str_2 
		str put_str_3
	try:
		name_file_image = "photo_{0}_{1}".format(name, surname)
		name_file_details = "details_{0}_{1}".format(name, surname)
		put_str_1 = "{} = dbem.convert_to_binary('{}')\n".format(name_file_image, image_path) 
		put_str_2 = "{} = dbem.convert_to_binary('{}')\n".format(name_file_details, details_path) 
		put_str_3 = "\t('{}', '{}', {}, '{}', {}, {}, '{}'),\n".format(name, surname, age, level, name_file_image, name_file_details, token) 

		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		
		#locs_1 = [i for i, val in enumerate(lines) if val == "import db_extern_methods as dbem\n"]
		locs_1 = [i for i, val in enumerate(lines) if val == "import casino_kill as ca\n"]
		
		locs = [i for i, val in enumerate(lines) if val =="all_kids = [\n"]

		if len(locs_1) and len(locs):
			N = len(locs_1)
			for j in range(N):
				locs_1[j] = locs_1[j] + 2
				lines.insert(locs_1[-j], put_str_1)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			N = len(locs_1)
			for j in range(N):
				locs_1[j] = locs_1[j] +2
				lines.insert(locs_1[-j], put_str_2)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			N = len(locs)
			for j in range(N):
				locs[j] = locs[j] +4
				lines.insert(locs[-j], put_str_3)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("Kids list succesfully updated")
	except Exception as e:
		print("An error occurred during modfication of patch in default list file")
		print("length locs_1 is {}",format(len(locs_1)))
		print("locs_1 --> {}".format(locs_1))
		print("length locs is {}",format(len(locs)))
		print("locs --> {}".format(locs))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_Needs_list(str focus_constraint, str need_constraint, str scope):
	cdef:
		int j
		int N
		str add_str
	try:
		if scope == 'match':
			add_str = "\t('{0}', '{1}', \'Invalid\', \'Invalid\'),\n".format(focus_constraint, need_constraint) 
		elif scope == 'game':	 
			add_str = "\t('{0}', \'Invalid\', '{1}', \'Invalid\'),\n".format(focus_constraint, need_constraint) 
		elif scope == 'question':	 
			add_str = "\t('{0}', \'Invalid\', \'Invalid\', '{1}'),\n".format(focus_constraint, need_constraint) 
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_2.py',"r") as fp:
			lines = fp.readlines()
		pos = [i for i, val in enumerate(lines) if val == "all_needs = [\n"] 
		if len(pos):
			N = len(pos)
			for j in range(N):
				pos[j] = pos[j] + 1
				lines.insert(pos[-j], add_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_2.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("Needs list succesfully updated")
	except Exception as e:
		print("##############################################################################################################")
		print("An error occurred during Needs list update")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")	

cdef modify_Sessions_list(int num_of_matches, str complexity, str status, str order_difficulty_matches, str order_quantity_games, 
	int mandatory_impositions, int mandatory_needs, int extra_time_tolerance, int movements_allowed, int body_enabled,
	int rules_explication, int stress_flag, int frequency_exhortations, int repeat_question, int repeat_intro, 
	int desirable_time, str creation_way, bint disposable, str token, int num_last_line):
	cdef:
		int j
		int N
		str add_this
	try:
		add_this = "\t({0}, '{1}', '{2}', {3}, {4}, '{5}', '{6}', {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, '{16}', {17}, '{18}'),\n".format(num_of_matches, complexity, 
			status, mandatory_impositions, mandatory_needs, order_difficulty_matches, order_quantity_games, extra_time_tolerance, movements_allowed, body_enabled,  
			rules_explication, stress_flag, frequency_exhortations, repeat_question, repeat_intro, desirable_time, creation_way, int(disposable), token) #int because bint passed....

		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		place = [i for i, val in enumerate(lines) if val == "all_sessions = [\n"]
		if len(place):
			N = len(place)
			for j in range(N):
				place[j] = place[j] + num_last_line  #for last one just added # num_last_line after addition into db that is before the call of this
				lines.insert(place[-j], add_this)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("Sessions list succesfully updated")
	except Exception as e:
		print("--> An error occurred during Session modfication file update")
		print("--> place is {} ".format(place[j]))
		print("--> place of Session length is {}".format(len(place)))
		print("--> Namely the problem is")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_Matches_list(str difficulty, int num_of_games, str variety, str order_difficulty_games, str order_quantity_questions, str criteria_arrangement, int advisable_time, scenario, bint disposable, str token, int num_last_line):
	cdef:
		int j
		int N
		str add_this
	try:
		add_this = "\t({0}, '{1}', '{2}', '{3}', '{4}', '{5}', {6}, {7}, {8}, '{9}'),\n".format(num_of_games, difficulty, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, scenario, advisable_time, int(disposable), token) 

		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		place = [i for i, val in enumerate(lines) if val == "all_matches = [\n"] 
		if len(place):				
			N = len(place)
			for j in range(N):
				place[j] = place[j] + num_last_line
				lines.insert(place[-j], add_this)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("Matches list succesfully updated")
	except Exception as e:
		print("--> An error occurred during Matches list update")
		print("--> place is {} ".format(place[j]))
		print("--> place of Matches length is {}".format(len(place)))			
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_Games_list(int num_questions, str kind_game, str type_game, str difficulty, str order_difficulty_questions, int necessary_time, bint disposable, str token, int num_last_line):
	cdef:
		int j
		int N
		str add_this
	try:
		print("num last".format(num_last_line))
		print("num_ques cosa sto aggiungendo dallinterno in dbdl", num_questions)
		add_this = "\t({0}, '{1}', '{2}', '{3}', '{4}', {5}, {6}, '{7}'),\n".format(num_questions, kind_game, type_game, difficulty, order_difficulty_questions, 
			necessary_time, int(disposable), token)
		print("addis", add_this)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		place = [i for i, val in enumerate(lines) if val == "all_games = [\n"]
		print("place of Games len is {}".format(len(place)))
		if len(place):
			N = len(place)
			for j in range(N):
				place[j] = place[j] + num_last_line
				print("place are {} ".format(place[j]))
				lines.insert(place[-j], add_this)
			
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception	
		print("Games list succesfully updated")
	except Exception as e:
		print("--> An error occurred during Games list update")
		print("--> place is {} ".format(place[j]))
		print("--> place of Games length is {}".format(len(place)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_Kind_Games_list(str kind, str details, int num_last_line):
	cdef:
		int j
		int N
		str add_this
	try:
		add_this = "\t('{0}', '{1}'),\n".format(kind, details)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		place = [i for i, val in enumerate(lines) if val == "all_kind_games = [\n"]
		print("place of Kind_games len is {}".format(len(place)))
		if len(place):
			N = len(place)
			for j in range(N):
				place[j] = place[j] + num_last_line
				print("place are {} ".format(place[j]))
				lines.insert(place[-j], add_this)
			
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception	
		print("Kind Games list succesfully updated")
	except Exception as e:
		print("--> An error occurred during Kind of Games list update")
		print("--> place is {} ".format(place[j]))
		print("--> place of Kind of game length is {}".format(len(place)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_Type_Games_list(str type_g, str description, str example, str kind_g, int num_last_line):
	cdef:
		int j
		int N
		str add_this
	try:
		add_this = "\t('{0}', '{1}', '{2}', '{3}'),\n".format(type_g, description, example, kind_g)

		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		place = [i for i, val in enumerate(lines) if val == "all_type_games = [\n"]
		print("place of Type_games len is {}".format(len(place)))
		if len(place):
			N = len(place)
			for j in range(N):
				place[j] = place[j] + num_last_line
				print("place are {} ".format(place[j]))
				lines.insert(place[-j], add_this)
			
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception	
		print("Type Games list succesfully updated")
	except Exception as e:
		print("--> An error occurred during Type of Games list update")
		print("--> place is {} ".format(place[j]))
		print("--> place of Type of game length is {}".format(len(place)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_Questions_list(str kind_game, str type_game, str audio_id, int value, str description, int position):
	cdef:
		int j
		int N
		str add_this 
	try:
		add_this = "{},{},{},{},{},\n".format(kind_game, type_game, audio_id, value, description)
		if position==0:
			num_last_line = 12012
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/list_all_questions.csv',"r") as fp:
				lines = fp.readlines()
			place = [i for i, val in enumerate(lines)]
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/list_all_questions.csv', 'a',-2) as fp:
				fp.write(add_this)
			print("Questions list succesfully updated")
		else:
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/list_all_questions.csv',"r") as fp:
				lines = fp.readlines()
			lines[position] = add_this
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/list_all_questions.csv', 'w',-2) as fp:
				fp.writelines(lines)
			print("Questions list succesfully updated")

	except Exception as e:
		print("An error occurred during Questions list update")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_Therapies_list(int num_of_treatments, str status, str creation_way, str kick_off_date, str closing_date, str canonical_intervention, int duration_days, int disposable, 
	str token, int num_last_line):
	#(3, 'to_do', 'automatically', None, None, 25, 'Eibi', 0, 'thb543n9ca'),
	cdef:
		int j
		int N
		str add_this 
	try:
		print("kick_off_date DENTRO")
		print(kick_off_date)
		print("closing_date DENTRO")
		print(closing_date)
		if isinstance(kick_off_date, type(None)) and isinstance(closing_date, type(None)):
		#if kick_off_date and closing_date == None:
			print("caso 1 modify therapy")
			add_this = "\t({0}, '{1}', '{2}', {3}, {4}, {5}, '{6}', {7}, '{8}'),\n".format(num_of_treatments, status, creation_way, kick_off_date, closing_date, duration_days, 
			canonical_intervention, disposable, token)
		elif isinstance(kick_off_date, type(None)) and not isinstance(closing_date, type(None)):
		#elif kick_off_date==None and closing_date=None:
			print("caso 2 modify therapy")
			add_this = "\t({0}, '{1}', '{2}', {3}, '{4}', {5}, '{6}', {7}, '{8}'),\n".format(num_of_treatments, status, creation_way, kick_off_date, closing_date, duration_days, 
			canonical_intervention, disposable, token) 
		elif not isinstance(kick_off_date, type(None)) and isinstance(closing_date, type(None)):
		#elif kick_off_date=None and closing_date==None:
			print("caso 3 modify therapy")
			add_this = "\t({0}, '{1}', '{2}', '{3}', {4}, {5}, '{6}', {7}, '{8}'),\n".format(num_of_treatments, status, creation_way, kick_off_date, closing_date, duration_days, 
			canonical_intervention, disposable, token) 
		else:
			print("caso 4 modify therapy")
			add_this = "\t({0}, '{1}', '{2}', '{3}', '{4}', {5}, '{6}', {7}, '{8}'),\n".format(num_of_treatments, status, creation_way, kick_off_date, closing_date, duration_days, 
			canonical_intervention, disposable, token) 
		
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		place = [i for i, val in enumerate(lines) if val == "all_therapies = [\n"] 
		if len(place):				
			N = len(place)
			for j in range(N):
				place[j] = place[j] + num_last_line
				lines.insert(place[-j], add_this)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("Therapies list succesfully updated")

	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new Therapy")
		print("##############################################################################################################")

cdef modify_Treatments_list(str target_goal_1, str target_goal_2, str target_goal_3, int num_of_pastimes, int num_of_sessions, int num_of_entertainments, str status_decided, 
	str creation_way_auto, str day_auto, int disposable_auto, str new_id, int num_last_line):
	cdef:
		int j
		int N
		str add_this 
	#try:
	if isinstance(target_goal_1, type(None)) and isinstance(target_goal_2, type(None)) and isinstance(target_goal_3, type(None)):
		print("caso 1 modify treatment1")
		add_this = "\t({0}, {1}, {2}, {3}, {4}, {5}, '{6}', '{7}', {8}, {9}, '{10}'),\n".format(target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, 
			num_of_sessions, num_of_entertainments, status_decided, creation_way_auto, day_auto, disposable_auto, new_id)

	elif isinstance(target_goal_1, type(None)) and isinstance(target_goal_2, type(None)) and not isinstance(target_goal_3, type(None)):
		print("caso 2 modify treatment2")
		add_this = "\t({0}, {1}, '{2}', {3}, {4}, {5}, '{6}', '{7}', {8}, {9}, '{10}'),\n".format(target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, 
			num_of_sessions, num_of_entertainments, status_decided, creation_way_auto, day_auto, disposable_auto, new_id)

	elif isinstance(target_goal_1, type(None)) and not isinstance(target_goal_2, type(None)) and isinstance(target_goal_3, type(None)):
		print("caso 3 modify treatment3")
		add_this = "\t({0}, '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', {8}, {9}, '{10}'),\n".format(target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, 
			num_of_sessions, num_of_entertainments, status_decided, creation_way_auto, day_auto, disposable_auto, new_id)

	elif not isinstance(target_goal_1, type(None)) and isinstance(target_goal_2, type(None)) and isinstance(target_goal_3, type(None)):
		print("caso 4 modify treatment4")
		add_this = "\t('{0}', {1}, {2}, {3}, {4}, {5}, '{6}', '{7}', {8}, {9}, '{10}'),\n".format(target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, 
			num_of_sessions, num_of_entertainments, status_decided, creation_way_auto, day_auto, disposable_auto, new_id)

	elif not isinstance(target_goal_1, type(None)) and not isinstance(target_goal_2, type(None)) and isinstance(target_goal_3, type(None)):
		print("caso 5 modify treatment5")
		add_this = "\t('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', {8}, {9}, '{10}'),\n".format(target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, 
			num_of_sessions, num_of_entertainments, status_decided, creation_way_auto, day_auto, disposable_auto, new_id)

	elif not isinstance(target_goal_1, type(None)) and isinstance(target_goal_2, type(None)) and not isinstance(target_goal_3, type(None)):
		print("caso 6 modify treatment6")
		add_this = "\t('{0}', {1}, '{2}', {3}, {4}, {5}, '{6}', '{7}', {8}, {9}, '{10}'),\n".format(target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, 
			num_of_sessions, num_of_entertainments, status_decided, creation_way_auto, day_auto, disposable_auto, new_id)

	elif isinstance(target_goal_1, type(None)) and not isinstance(target_goal_2, type(None)) and not isinstance(target_goal_3, type(None)):
		print("caso 7 modify treatment7")
		add_this = "\t({0}, '{1}', '{2}', {3}, {4}, {5}, '{6}', '{7}', {8}, {9}, '{10}'),\n".format(target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, 
			num_of_sessions, num_of_entertainments, status_decided, creation_way_auto, day_auto, disposable_auto, new_id)
	else:
		print("caso 8 modify treatment 8")
		add_this = "\t('{0}', '{1}', '{2}', {3}, {4}, {5}, '{6}', '{7}', {8}, {9}, '{10}'),\n".format(target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, 
			num_of_sessions, num_of_entertainments, status_decided, creation_way_auto, day_auto, disposable_auto, new_id)
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
		lines = fp.readlines()
	place = [i for i, val in enumerate(lines) if val == "all_treatments = [\n"]
	print()
	print("place ", place)
	if len(place):			
		N = len(place)
		for j in range(N):
			place[j] = place[j] + num_last_line
			lines.insert(place[-j], add_this)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
			fp.write(''.join(lines))
	else:
		raise Exception
	print("Treatments list succesfully updated")

cdef modify_Entertainments_list(str creation_way_auto, str status_decided, str class_of_game, str specific_guidelines, str notes_none, int disposable_auto, str new_id, int num_last_line):
	cdef:
		int j
		int N
		str add_this 
	try:
		#('automatically', 'to_do', 'OnlyManual', 'regola_aggiuntiva_1', 'to do at the beginning of a treatment', 0, 'en1kjhgfds'),
		add_this = "\t('{0}', '{1}', '{2}', '{3}', '{4}', {5}, '{6}'),\n".format(creation_way_auto, status_decided, class_of_game, specific_guidelines, 
			notes_none, disposable_auto, new_id)
		
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		place = [i for i, val in enumerate(lines) if val == "all_entertainments = [\n"] 
		if len(place):				
			N = len(place)
			for j in range(N):
				place[j] = place[j] + num_last_line
				lines.insert(place[-j], add_this)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("Entertainments list succesfully updated")

	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new Entertainment")
		print("##############################################################################################################")

cdef modify_Patches_list(str new_subject, str new_color):
	cdef:
		int j
		int N
		str add_str
	try:
		add_str = "\t('{0}','{1}', None),\n".format(new_subject, new_color)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		pos = [i for i, val in enumerate(lines) if val == "all_patches = [\n"]
		if len(pos):
			N = len(pos)
			for j in range(N):
				pos[j] = pos[j] +1 
				lines.insert(pos[-j], add_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("Patches list succesfully updated")
	except Exception as e:
		print("--> An error occurred during modfication of patch in default list file")
		print("--> pos is: {} ".format(pos[j]))
		print("--> Cannot find {} in db_default_lists ".format(add_str))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)			

cdef modify_default_list_kids_sessions(int first_id, int second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_sessions = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("kids_sessions list succesfully updated")
		else:
			raise Exception
	except Exception as e:
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_kids_therapies(int first_id, int second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_therapies = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("kids_therapies list succesfully updated")
		else:
			raise Exception
	except Exception as e:
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_kids_treatments(int first_id, int second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_treatments = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("kids_treatments list succesfully updated")
		else:
			raise Exception
	except Exception as e:
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_kids_entertainments(int first_id, int second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_entertainments = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("Kids_Entertainments list succesfully updated")
		else:
			raise Exception
	except Exception as e:
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_sessions_matches(int first_id, int second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_sessions_matches = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("session_matches list succesfully updated")
		else:
			raise Exception
	except Exception as e:
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)				

cdef modify_default_list_matches_games(int first_id, int second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_matches_games = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("matches_games list succesfully updated")
		else:
			raise Exception
	except Exception as e:
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
cdef modify_default_list_games_questions(int first_id, int second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_games_questions = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("games_questions list succesfully updated")
		else:
			raise Exception
	except Exception as e:
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_questions_patches(int first_id, int second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()			
		found = [i for i, val in enumerate(lines) if val == "all_questions_patches = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("questions_patches list succesfully updated")
		else:
			raise Exception
	except Exception as e:
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]	
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_kids_needs(int first_id, int second_id, int numb_pos):		
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_needs = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("kids_sessions list succesfully updated")
		else:
			raise Exception
	except Exception as e: 
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)				

cdef modify_default_list_kids_impositions(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id) 
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_impositions = [\n"]			 
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("kids_impositions list succesfully updated")
	except Exception as e: 
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_kids_achievements(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id) 
		
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_achievements = [\n"]		 
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("kids_achievements list succesfully updated")
	except Exception as e: 
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_kids_symptoms(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id) 
		
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_symptoms = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("kids_symptoms list succesfully updated")
	except Exception as e: 
		print("An error occurred during dafault_lists update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_kids_issues(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id) 
		
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_issues = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("kids_issues list succesfully updated")
	except Exception as e: 
		print("An error occurred during DB defult_lists update")
		print("the culprit is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_kids_comorbidities(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id) 
		print("insert_str ", insert_str)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_comorbidities = [\n"]
		if len(found):
			print("FOUND =>  " ,found)
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			print("sono finito nell eccezione")
			raise Exception
		print(">>>>>>>>>>>>>>>>>>< Kids_Comorbidities list succesfully updated")
	except Exception as e: 
		print("An error occurred during DB defult_lists update")
		print("the culprit is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)			

cdef modify_default_list_kids_strengths(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id) 
		
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_strengths = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("Kids_strengths list succesfully updated")
	except Exception as e: 
		print("An error occurred during DB defult_lists update")
		print("the culprit is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_list_sessions_needs(int first_id, int second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_sessions_needs = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				print("FOUNDDDDDDDDDDDDDDDDDDDDDD nee")
				print(found[j])				
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("session_needs list succesfully updated")
		else:
			raise Exception
	except Exception as e: 
		print("##############################################################################################################")
		print("An error occurred during session needs")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred during sessions_enforcements")
		print("--> found is {} ".format(found[j]))
		print("--> found length is {}".format(len(found)))
		print("##############################################################################################################")

cdef modify_default_list_sessions_enforcements(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		if second_id='redo':
			insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id) 
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
				lines = fp.readlines()
			found = [i for i, val in enumerate(lines) if val == "all_sessions_enforcements = [\n"]			 
			if len(found):
				N = len(found)
				for j in range(N):
					found[j] = found[j] + numb_pos
					print(found[j])
					lines.insert(found[-j], insert_str)
				with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
					fp.write(''.join(lines))
			else:
				raise Exception
			print("sessions_enforcements list succesfully updated")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred during sessions_enforcements")
		print("--> found is {} ".format(found[j]))
		print("--> found length is {}".format(len(found)))
		print("##############################################################################################################")

cdef modify_default_list_sessions_impositions(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		if second_id='redo':
			insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id) 
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
				lines = fp.readlines()
			found = [i for i, val in enumerate(lines) if val == "all_sessions_impositions = [\n"]			 
			if len(found):
				N = len(found)
				for j in range(N):
					found[j] = found[j] + numb_pos
					print("FOUNDDDDDDDDDDDDDDDDDDDDDD impo")
					print(found[j])
					lines.insert(found[-j], insert_str)
				with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
					fp.write(''.join(lines))
			else:
				raise Exception
			print("session_impositions list succesfully updated")
	except Exception as e: 
		print("An error occurred during session_impo update")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_default_names(str constraint_str, str table, int numb_pos): #occhio che devo controllare il meno 4 se va bene per tutti
	cdef:
		int j
		int N
		str insert_str 
	try: 
		insert_str = "\t'{0}',\n".format(constraint_str)
		print("modify_default_names == stringa che sto inserendo per TOKEN uguale a {}".format(insert_str))
		if table = 'Impositions':
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
				lines = fp.readlines()
		else:
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_2.py',"r") as fp:
				lines = fp.readlines()
		if table == 'Achievements': 
			found = [i for i, val in enumerate(lines) if val == "all_achievements = [\n"]
		elif table == 'Symptoms':
			found = [i for i, val in enumerate(lines) if val == "all_symptoms = [\n"]
		elif table == 'Impositions':
			found = [i for i, val in enumerate(lines) if val == "all_impositions = [\n"]
		elif table == 'All_id_tokens':
			insert_str = "\t('{0}'),\n".format(constraint_str)
			found = [i for i, val in enumerate(lines) if val == "all_id_tokens = [\n"]
		else:
			raise Exception
		if len(found):
			print("sono cmq in len found ok")
			print("found is ==> ",found)
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			if table = 'Impositions':
				with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
					fp.write(''.join(lines))
			else:
				with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_2.py', 'w',-2) as fp:
					fp.write(''.join(lines))
		else:
			raise Exception
		print("{} list succesfully updated".format(table))
	except Exception as e:
		print("An error occurred during dafault_lists names modification")
		print("found is {} ".format(found[j]))
		print("found length is {}".format(len(found)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef modify_with_update_in_lines(str id_token, int change_this, int with_this):
	print("id_token of modify {}".format(id_token))
	dove = '/{}/'.format(id_token)
	cosaquando = 's/{},/{},/g'.format(change_this,with_this)

	comma = "sed -i.bak '{} {}'".format(dove, cosaquando)
	print("comma ", comma)
	#sed -i.bak '/758cab3f/ s/1,/4,/g' db_default_lists.py

	cmd_args = '/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py'
	commann = "{} {}".format(comma,cmd_args)
	print("commann ", commann)
	run(commann, shell=True)
	Popen("trash-put /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py.bak" ,shell=True)

cdef modify_with_update_single_match_changing_time(list match_data, int time_to_change):
	###########nb 7 e 8 inverted in file
	line_to_mod = "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, '{}'),".format(match_data[1],match_data[2],match_data[3],match_data[4],match_data[5],match_data[6],match_data[8],match_data[7],match_data[9],match_data[12])
	new_line_ok = "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, '{}'),".format(match_data[1],match_data[2],match_data[3],match_data[4],match_data[5],match_data[6],match_data[8], time_to_change, match_data[9],match_data[12])
	print("TESTO DA modify_with_update_in_lines ==> ",line_to_mod)
	cosaquando = "s/$(printf '\t'){}/$(printf '\t'){}/".format(line_to_mod,new_line_ok)
	command_ins = 'sed -i "{}"'.format(cosaquando)
	cmd_args = '/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py'
	def_command = "{} {}".format(command_ins,cmd_args)
	run(def_command, shell=True)

cdef modify_with_update_desirable_time_session(list sessions, int time_to_change):
	#INVERTITI 4,5 dopo le 6,7 LE MANDATORy VENGONO PRIMA NEL DB_DEFAULT
	#mandatory_imposition, mandatory_need,
	#order_difficulty_matches, order_quantity_games
	line_to_mod = "({0}, '{1}', '{2}', {3}, {4}, '{5}', '{6}', {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, '{16}', {17}, '{18}'),".format(
	sessions[1], sessions[2], sessions[3], sessions[6], sessions[7], sessions[4], sessions[5],
	sessions[8], sessions[9], sessions[10], sessions[11], sessions[12], sessions[13],
	sessions[14], sessions[15], sessions[16], sessions[17], sessions[18], sessions[22])

	new_line_ok = "({0}, '{1}', '{2}', {3}, {4}, '{5}', '{6}', {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, '{16}', {17}, '{18}'),".format(
	sessions[1], sessions[2], sessions[3], sessions[6], sessions[7], sessions[4], sessions[5],
	sessions[8], sessions[9], sessions[10], sessions[11], sessions[12], sessions[13],
	sessions[14], sessions[15], time_to_change, sessions[17], sessions[18], sessions[22])

	print("TESTO DA modify_with_update_in_lines ==> ",line_to_mod)
	cosaquando = "s/$(printf '\t'){}/$(printf '\t'){}/".format(line_to_mod,new_line_ok)
	command_ins = 'sed -i "{}"'.format(cosaquando)
	print("command instruction ===>> ", command_ins)
	cmd_args = '/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py'
	#cmd_args = '/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py'
	def_command = "{} {}".format(command_ins,cmd_args)
	print("def_command =============================================>", def_command)
	run(def_command, shell=True)

cdef modify_default_list_old_kids_needs(int first_id, int second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "old_kids_needs = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("old_kids_needs list succesfully updated")
		else:
			raise Exception
	except Exception as e: 
		print("##################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred during the update of the db_default_lists_old_tables.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##################################################")

cdef modify_default_list_old_kids_symptoms(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "old_kids_symptoms = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("old_kids_symptoms list succesfully updated")
		else:
			raise Exception
	except Exception as e: 
		print("##################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred during the update of the db_default_lists_old_tables.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##################################################")


cdef modify_default_list_old_kids_issues(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "old_kids_issues = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("old_kids_issues list succesfully updated")
		else:
			raise Exception
	except Exception as e: 
		print("##################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred during the update of the db_default_lists_old_tables.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##################################################")

cdef modify_default_list_old_kids_comorbidities(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "old_kids_comorbidities = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("old_kids_comorbidities list succesfully updated")
		else:
			raise Exception
	except Exception as e: 
		print("##################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred during the update of the db_default_lists_old_tables.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##################################################")

cdef modify_default_list_old_kids_strengths(int first_id, str second_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "old_kids_strengths = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("old_kids_strenghts list succesfully updated")
		else:
			raise Exception
	except Exception as e:
		print("##################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred during the update of the db_default_lists_old_tables.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##################################################")

#########################################################################################################################################################
cdef modify_default_list_old_tables_kids(int first_id, str second_id, int numb_pos, str which_table):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, '{1}'),\n".format(first_id, second_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py',"r") as fp:
			lines = fp.readlines()
	
		if which_table=='Old_Kids_Symptoms':
			found = [i for i, val in enumerate(lines) if val == "old_kids_symptoms = [\n"]
		elif which_table=='Old_Kids_Issues':
			found = [i for i, val in enumerate(lines) if val == "old_kids_issues = [\n"]
		elif which_table=='Old_Kids_Comorbidities':
			found = [i for i, val in enumerate(lines) if val == "old_kids_comorbidities = [\n"]
		elif which_table=='Old_Kids_Strenghts':
			found = [i for i, val in enumerate(lines) if val == "old_kids_strengths = [\n"]
		else:
			raise Exception
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
	except Exception as e:
		print("##################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred during the update of the db_default_lists_old_tables.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##################################################")

cdef modify_KidEntertainment_list(int kid_id, int entertainment_id, int numb_pos):
	cdef:
		int j
		int N
		str insert_str
	try:
		insert_str = "\t({0}, {1}),\n".format(kid_id, entertainment_id)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") as fp:
			lines = fp.readlines()
		found = [i for i, val in enumerate(lines) if val == "all_kids_entertainments = [\n"]
		if len(found):
			N = len(found)
			for j in range(N):
				found[j] = found[j] + numb_pos
				lines.insert(found[-j], insert_str)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
	except Exception as e:
		print("######################################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred during the update of the db_default_lists_old_tables.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("######################################################################################################################")

cdef modify_therapies_duration_or_date(list old_therapy_data, list new_therapy_data):
	print("entro in modify_therapies_duration_or_date")
	print("entro in modify_therapies_duration_or_date")
	print("entro in modify_therapies_duration_or_date")
	print("entro in modify_therapies_duration_or_date")
	print("entro in modify_therapies_duration_or_date")
	try:
		print(len(old_therapy_data))
		print("old_therapy_data", old_therapy_data)
		print(len(new_therapy_data))
		print("new_therapy_data", new_therapy_data)
		list_none_indexes = [i for i in range(len(old_therapy_data)) if old_therapy_data[i] == None]
		print("list_none_indexes", list_none_indexes)
		if 4 in list_none_indexes and 5 in list_none_indexes:
			print("che caso 4-5")
			line_to_mod = "({}, '{}', '{}', {}, {}, {}, '{}', {}, '{}'),".format(old_therapy_data[1], old_therapy_data[2], old_therapy_data[3], old_therapy_data[4], old_therapy_data[5], 
				old_therapy_data[6], old_therapy_data[7], old_therapy_data[8], old_therapy_data[9])
			new_line_ok = "({}, '{}', '{}', {}, {}, {}, '{}', {}, '{}'),".format(new_therapy_data[1], new_therapy_data[2], new_therapy_data[3], new_therapy_data[4], new_therapy_data[5],
				new_therapy_data[6], new_therapy_data[7], new_therapy_data[8], new_therapy_data[9])
		elif 4 in list_none_indexes and 5 not in list_none_indexes:
			print("che caso 4")
			line_to_mod = "({}, '{}', '{}', {}, '{}', {}, '{}', {}, '{}'),".format(old_therapy_data[1], old_therapy_data[2], old_therapy_data[3], old_therapy_data[4], old_therapy_data[5], 
				old_therapy_data[6], old_therapy_data[7], old_therapy_data[8], old_therapy_data[9])
			new_line_ok = "({}, '{}', '{}', {}, '{}', {}, '{}', {}, '{}'),".format(new_therapy_data[1], new_therapy_data[2], new_therapy_data[3], new_therapy_data[4], new_therapy_data[5],
				new_therapy_data[6], new_therapy_data[7], new_therapy_data[8], new_therapy_data[9])
		elif 5 in list_none_indexes and 4 not in list_none_indexes:
			print("che caso 5")
			line_to_mod = "({}, '{}', '{}', '{}', {}, {}, '{}', {}, '{}'),".format(old_therapy_data[1], old_therapy_data[2], old_therapy_data[3], old_therapy_data[4], old_therapy_data[5], 
				old_therapy_data[6], old_therapy_data[7], old_therapy_data[8], old_therapy_data[9])
			new_line_ok = "({}, '{}', '{}', '{}', {}, {}, '{}', {}, '{}'),".format(new_therapy_data[1], new_therapy_data[2], new_therapy_data[3], new_therapy_data[4], new_therapy_data[5],
				new_therapy_data[6], new_therapy_data[7], new_therapy_data[8], new_therapy_data[9])
		else:
			print("che caso ELSE")
			line_to_mod = "({}, '{}', '{}', '{}', '{}', {}, '{}', {}, '{}'),".format(old_therapy_data[1], old_therapy_data[2], old_therapy_data[3], old_therapy_data[4], old_therapy_data[5], 
				old_therapy_data[6], old_therapy_data[7], old_therapy_data[8], old_therapy_data[9])
			new_line_ok = "({}, '{}', '{}', '{}', '{}', {}, '{}', {}, '{}'),".format(new_therapy_data[1], new_therapy_data[2], new_therapy_data[3], new_therapy_data[4], new_therapy_data[5],
				new_therapy_data[6], new_therapy_data[7], new_therapy_data[8], new_therapy_data[9])
		print("TESTO DA modify_with_update_in_lines ",line_to_mod)
		print("TESTO giusto ====> ",new_line_ok)
		cosaquando = "s/$(printf '\t'){}/$(printf '\t'){}/".format(line_to_mod, new_line_ok)
		command_ins = 'sed -i "{}"'.format(cosaquando)
		cmd_args = '/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py'
		def_command = "{} {}".format(command_ins, cmd_args)
		print("def command ---", def_command)
		run(def_command, shell=True)
	except Exception as e:
		print("######################################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred during the update of the db_default_lists_old_tables.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("######################################################################################################################")

cdef modify_treatment_day(list old_treatment_data, list new_treatment_data):
	try:
		print(len(old_treatment_data))
		print("old_treatment_data", old_treatment_data)
		print(len(new_treatment_data))
		print("new_treatment_data", new_treatment_data)
		list_none_indexes = [i for i in range(len(old_treatment_data)) if old_treatment_data[i] == None]
		print("list_none_indexes", list_none_indexes)
		if 1 in list_none_indexes and 2 in list_none_indexes and 3 in list_none_indexes:
			print("che caso 1 2 3")
			line_to_mod = "({}, {}, {}, {}, {}, {}, '{}', '{}', {}, {}, '{}'),".format(old_treatment_data[1], old_treatment_data[2], old_treatment_data[3], old_treatment_data[4], old_treatment_data[5], 
				old_treatment_data[6], old_treatment_data[7], old_treatment_data[8], old_treatment_data[11], old_treatment_data[12], old_treatment_data[13])
			new_line_ok = "({}, {}, {}, {}, {}, {}, '{}', '{}', '{}', {}, '{}'),".format(new_treatment_data[1], new_treatment_data[2], new_treatment_data[3], new_treatment_data[4], new_treatment_data[5], 
				new_treatment_data[6], new_treatment_data[7], new_treatment_data[8], new_treatment_data[11], new_treatment_data[12], new_treatment_data[13])
		elif 1 in list_none_indexes and 2 in list_none_indexes and 3 not in list_none_indexes:
			print("che caso 1 2")
			line_to_mod = "({}, {}, '{}', {}, {}, {}, '{}', '{}', {}, {}, '{}'),".format(old_treatment_data[1], old_treatment_data[2], old_treatment_data[3], old_treatment_data[4], old_treatment_data[5], 
				old_treatment_data[6], old_treatment_data[7], old_treatment_data[8], old_treatment_data[11], old_treatment_data[12], old_treatment_data[13])
			new_line_ok = "({}, {}, '{}', {}, {}, {}, '{}', '{}', '{}', {}, '{}'),".format(new_treatment_data[1], new_treatment_data[2], new_treatment_data[3], new_treatment_data[4], new_treatment_data[5], 
				new_treatment_data[6], new_treatment_data[7], new_treatment_data[8], new_treatment_data[11], new_treatment_data[12], new_treatment_data[13])
		elif 1 in list_none_indexes and 2 not in list_none_indexes and 3 in list_none_indexes:
			print("che caso 1 3")
			line_to_mod = "({}, '{}', {}, {}, {}, {}, '{}', '{}', {}, {}, '{}'),".format(old_treatment_data[1], old_treatment_data[2], old_treatment_data[3], old_treatment_data[4], old_treatment_data[5], 
				old_treatment_data[6], old_treatment_data[7], old_treatment_data[8], old_treatment_data[11], old_treatment_data[12], old_treatment_data[13])
			new_line_ok = "({}, '{}', {}, {}, {}, {}, '{}', '{}', '{}', {}, '{}'),".format(new_treatment_data[1], new_treatment_data[2], new_treatment_data[3], new_treatment_data[4], new_treatment_data[5], 
				new_treatment_data[6], new_treatment_data[7], new_treatment_data[8], new_treatment_data[11], new_treatment_data[12], new_treatment_data[13])
		elif 1 not in list_none_indexes and 2 in list_none_indexes and 3 in list_none_indexes:
			print("che caso 2 3")
			line_to_mod = "('{}', {}, {}, {}, {}, {}, '{}', '{}', {}, {}, '{}'),".format(old_treatment_data[1], old_treatment_data[2], old_treatment_data[3], old_treatment_data[4], old_treatment_data[5], 
				old_treatment_data[6], old_treatment_data[7], old_treatment_data[8], old_treatment_data[11], old_treatment_data[12], old_treatment_data[13])
			new_line_ok = "('{}', {}, {}, {}, {}, {}, '{}', '{}', '{}', {}, '{}'),".format(new_treatment_data[1], new_treatment_data[2], new_treatment_data[3], new_treatment_data[4], new_treatment_data[5], 
				new_treatment_data[6], new_treatment_data[7], new_treatment_data[8], new_treatment_data[11], new_treatment_data[12], new_treatment_data[13])
		elif 1 in list_none_indexes and 2 not in list_none_indexes and 3 not in list_none_indexes:
			print("che caso 1")
			line_to_mod = "({}, '{}', '{}', {}, {}, {}, '{}', '{}', {}, {}, '{}'),".format(old_treatment_data[1], old_treatment_data[2], old_treatment_data[3], old_treatment_data[4], old_treatment_data[5], 
				old_treatment_data[6], old_treatment_data[7], old_treatment_data[8], old_treatment_data[11], old_treatment_data[12], old_treatment_data[13])
			new_line_ok = "({}, '{}', '{}', {}, {}, {}, '{}', '{}', '{}', {}, '{}'),".format(new_treatment_data[1], new_treatment_data[2], new_treatment_data[3], new_treatment_data[4], new_treatment_data[5], 
				new_treatment_data[6], new_treatment_data[7], new_treatment_data[8], new_treatment_data[11], new_treatment_data[12], new_treatment_data[13])
		elif 1 not in list_none_indexes and 2 in list_none_indexes and 3 not in list_none_indexes:
			print("che caso 2")
			line_to_mod = "('{}', {}, '{}', {}, {}, {}, '{}', '{}', {}, {}, '{}'),".format(old_treatment_data[1], old_treatment_data[2], old_treatment_data[3], old_treatment_data[4], old_treatment_data[5], 
				old_treatment_data[6], old_treatment_data[7], old_treatment_data[8], old_treatment_data[11], old_treatment_data[12], old_treatment_data[13])
			new_line_ok = "('{}', {}, '{}', {}, {}, {}, '{}', '{}', '{}', {}, '{}'),".format(new_treatment_data[1], new_treatment_data[2], new_treatment_data[3], new_treatment_data[4], new_treatment_data[5], 
				new_treatment_data[6], new_treatment_data[7], new_treatment_data[8], new_treatment_data[11], new_treatment_data[12], new_treatment_data[13])
		elif 1 not in list_none_indexes and 2 in list_none_indexes and 3 not in list_none_indexes:
			print("che caso 3")
			line_to_mod = "('{}', '{}', {}, {}, {}, {}, '{}', '{}', {}, {}, '{}'),".format(old_treatment_data[1], old_treatment_data[2], old_treatment_data[3], old_treatment_data[4], old_treatment_data[5], 
				old_treatment_data[6], old_treatment_data[7], old_treatment_data[8], old_treatment_data[11], old_treatment_data[12], old_treatment_data[13])
			new_line_ok = "('{}', '{}', {}, {}, {}, {}, '{}', '{}', '{}', {}, '{}'),".format(new_treatment_data[1], new_treatment_data[2], new_treatment_data[3], new_treatment_data[4], new_treatment_data[5], 
				new_treatment_data[6], new_treatment_data[7], new_treatment_data[8], new_treatment_data[11], new_treatment_data[12], new_treatment_data[13])
		else:
			print("che caso ELSE")
			line_to_mod = "('{}', '{}', '{}', {}, {}, {}, '{}', '{}', {}, {}, '{}'),".format(old_treatment_data[1], old_treatment_data[2], old_treatment_data[3], old_treatment_data[4], old_treatment_data[5], 
				old_treatment_data[6], old_treatment_data[7], old_treatment_data[8], old_treatment_data[11], old_treatment_data[12], old_treatment_data[13])
			new_line_ok = "('{}', '{}', '{}', {}, {}, {}, '{}', '{}', '{}', {}, '{}'),".format(new_treatment_data[1], new_treatment_data[2], new_treatment_data[3], new_treatment_data[4], new_treatment_data[5], 
				new_treatment_data[6], new_treatment_data[7], new_treatment_data[8], new_treatment_data[11], new_treatment_data[12], new_treatment_data[13])
		print("TESTO DA modify_with_update_in_lines ",line_to_mod)
		print("TESTO giusto ====> ",new_line_ok)
		cosaquando = "s/$(printf '\t'){}/$(printf '\t'){}/".format(line_to_mod, new_line_ok)
		command_ins = 'sed -i "{}"'.format(cosaquando)
		cmd_args = '/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py'
		def_command = "{} {}".format(command_ins, cmd_args)
		print("def command ---", def_command)
		run(def_command, shell=True)
	except Exception as e:
		print("######################################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error An Exception of type {0} occurred during the update of the db_default_lists_old_tables.\nNamely: \n{1r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("######################################################################################################################")