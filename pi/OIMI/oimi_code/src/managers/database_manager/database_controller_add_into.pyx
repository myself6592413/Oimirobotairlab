"""Info:
	Oimi robot database_controler_add_into 
	database module for adding new elements into the right tables and adding a new line into db_default_lists.
	In the case of adding a new relationship between elements (create a new istance in a Bridge table), no new constraint need to be added (no need to update oimi_queries file).
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager database_controller_add_into
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
#sys.path.insert(1, os.path.join(sys.path[0], '.'))
import mmap
import copy
import random
import pickle
import traceback
from importlib import reload
from typing import Optional
import oimi_queries as oq
import db_default_lists as dbdl
import db_extern_methods as dbem
import db_extern_methods_two as dbem2
import db_default_lists_old_tables as dbdlot

cimport modify_default_lists as mdl
cimport add_prediction as ap
cimport add_customized_prediction as acp
cimport database_controller_save_to_files as dcs
cimport database_controller_build as dcb
cimport database_controller_add_into_part_2 as dba2
cimport database_controller_add_into_part_2_1 as dba21
cimport database_controller_add_into_part_3 as dba3
cimport database_controller_add_into_part_4 as dba4
cimport database_controller_add_into_part_6 as dba6
cimport database_controller_add_into_part_7 as dba7
cimport database_controller_generate_online as dcgo

cimport database_controller_update as dcu
import time
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
# ---
# # Add new row
# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
cdef add_new_Kid_table(db_self, str name, str surname, int age, str level, str image_path, str details_path):
	cdef:
		str new_id = db_self.assign_new_id('Kids')
		str search_str = "\t('{0}', '{1}', {2}, '{3}'".format(name, surname, age, level)
		bint new_del = 0
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The kid you want to insert already exist!!')
				raise Exception
				#pass
			reload(oq)
			photo_to_insert = dbem.convert_to_binary(image_path)
			doc_to_insert = dbem.convert_to_binary(details_path)
			db_self.curs.execute('''INSERT INTO Kids (name, surname, age, current_level, photo, details, token) VALUES (?,?,?,?,?,?,?)''', (name, surname, age, level, photo_to_insert, doc_to_insert, new_id))
			db_self.conn.commit()
			mdl.modify_Kids_list(name, surname, age, level, image_path, details_path, new_id)
			db_self.curs.execute("INSERT INTO All_id_tokens (token) VALUES (?)",(new_id,))
			db_self.conn.commit()
			filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
			with open(filename_pk, 'rb') as fi:
				db_self.deletions_list = pickle.load(fi)
				print("ho trovato questa deletions_list!!!!")
				print("deletions_list = {}".format(db_self.deletions_list))
			if db_self.deletions_list[0]!=[0]:
				new_del = 1
				dbem.bubblesort(db_self.deletions_list[0])
				db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Kids\'")
				index_to_replace = db_self.curs.fetchone()[1]
				index_corrected = db_self.deletions_list[0].pop(0)
				db_self.curs.execute('''UPDATE Kids SET kid_id = ?	WHERE kid_id = ?''', (index_corrected,index_to_replace))
				if not db_self.deletions_list[0]:
					db_self.deletions_list[0].append(0) #kid index = 0
					filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
					with open(filename_pk, 'wb') as fi:
						pickle.dump(db_self.deletions_list, fi)
				db_self.conn.commit()
		print("The new Kid is now added.")
		print("Kids_Achievements up to date with new kid.")
		sql_insertion_achi = "INSERT INTO Kids_Achievements (kid) SELECT kid_id FROM Kids ORDER BY kid_id DESC LIMIT 1" # WHERE kid isNull"
		sql_insertion_achi_2 = '''
			UPDATE Kids_Achievements 
			SET achi = (SELECT achievement_name
						FROM Achievements
						WHERE rowid=?)
			WHERE rowid = ?'''
		db_self.curs.execute("SELECT COUNT(achievement_name) FROM Achievements")
		n_achi = db_self.curs.fetchone()[0]
		print(n_achi)
		for i in range(n_achi):
			db_self.curs.execute(sql_insertion_achi)
		db_self.conn.commit()

		for p in range(n_achi+1):
			db_self.curs.execute(sql_insertion_achi_2,(p,p,))
		db_self.conn.commit()

	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new Kid")
		print("--> Search_str Add_new_Kid{}".format(search_str))
		if new_del:
			print("--> 1))) {} ".format(index_to_replace))
			print("--> 2))) {} ".format(index_corrected))
		print("##############################################################################################################")
		
cdef add_new_Session_table(db_self, int kid_id, int mandatory_impositions, int mandatory_needs):
	if not kid_id:
		dba3.add_new_Session_table_kid_not_given(db_self)
	else:
		useful = ap.DatabaseManager_2(kid_id)
		with useful:
		#with ap.DatabaseManager_2():
			useful.add_new_Session_table_with_kid(kid_id, mandatory_impositions, mandatory_needs)
		
		#dcb.create_Full_played_recap_giga_table(db_self)
		db_self.conn.commit()

	#############change ALL ADVISABLE TIMES!!###################################################
	#dcgo.change_all_advisable_times_all_matches(db_self)
	#dcgo.change_all_desirable_times_all_sessions(db_self)
	#############change!!#######################################################################

cdef add_new_Therapy_table(db_self, int kid_id, int num_of_treatments, str canonical_intervention):
	#quando creo c'è kid + canonical intervention... ci sarà prima un metodo a monte che fa parte del sistema che sceglie il giusto nuymero di treatments in base
	#al canonical intervention e agli achievements...!!! ma qui non ci interessa!!! 
	#i missing values sono invece creati automaticamente 
	#missing:
		#therapy_id
		#status
		#creation_way
		#kick_off_date
		#closing_date
		#duration_days
		#disposable
		#token
	dba7.annex_new_therapy(db_self, kid_id, num_of_treatments, canonical_intervention)

cdef add_new_Treatment_table(db_self, int kid_id, str target_goal_1, str target_goal_2, str target_goal_3, int num_of_pastimes, int num_of_sessions, int num_of_entertainments):
	#i target goal vengono decisi prima da varie prediction ma qui si da scontato che sian0o gia stati decisi
	#missing (auto_generated?):
		#treatment_id	
		#status 
		#creation_way 
		#audio_treatment_intro 
		#audio_treatment_exit 
		#day 
		#disposable 
		#token 
	dba7.annex_new_treatment(db_self, kid_id, target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, num_of_sessions, num_of_entertainments)

cdef add_new_Entertainment_table(db_self, int kid_id, str class_of_game, str specific_guidelines):
	#missing;
		#entertainment_id 
		#creation_way 
		#status 
		#category_of_game (automatic?)
		#specific_guidelines 
		#audio_rules 
		#audio_intro 
		#audio_exit 
		#notes 
		#disposable 
		#token 
	dba7.annex_new_entertainment(db_self, kid_id, class_of_game, specific_guidelines)

###################################################################################################################################################################################################
###################################################################################################################################################################################################

cdef add_new_constrained_Session_table(db_self, int kid_id, int mandatory_impositions, int mandatory_needs, customized):
	print("Im in method.....add_new_constrained_Session_table!!!")
	cdef:
		str new_id = ''
		bint new_del = 0
	if kid_id:
		print("ALEALE kid id ", kid_id)
		pred_kid_custom = acp.DatabaseManager_3(kid_id)
		#with acp.DatabaseManager_3()
		with pred_kid_custom():
			pred_kid_custom.add_new_constrained_Session_table_with_kid(kid_id, mandatory_impositions, mandatory_needs, customized)
		
		#dcb.create_Full_played_recap_giga_table(db_self)
		db_self.conn.commit()
	else:					
		add_new_constrained_Session_table_kid_not_given(db_self, customized) #non gli passo nemmeno il mandatory impo e need

	#############change ALL ADVISABLE TIMES!!###################################################
	dcgo.change_all_advisable_times_all_matches(db_self)
	dcgo.change_all_desirable_times_all_sessions(db_self)
	#############change!!#######################################################################
		
cdef add_new_constrained_Session_table_kid_not_given(db_self, customized): #mandatory impo e need unnecessary
	print("SALIDA EN add_new_constrained_Session_table_kid_not_given!!!")
	new_id = db_self.assign_new_id('Sessions')
	print("SEGRETO SEGRETO SEGRETO SEGRETO SEGRETO {}".format(customized))
	print("SEGRETO SEGRETO SEGRETO SEGRETO SEGRETO {}".format(customized))
	print("SEGRETO SEGRETO SEGRETO SEGRETO SEGRETO {}".format(customized))
	stress_flag,complexity,level_area,must_be_mixed,dont_ask_scenario,num_of_matches,num_of_games_of_one_match,\
	num_of_questions,kind_one_game,type_one_game,ask_never_asked_q, difficulty_mat,difficulty_gam = dbem2.obtain_info_session(customized)
	print("print subito subito subito subito RECEIVED FROM CUTOMIZED: INIZIO")
	print(stress_flag)
	print(complexity)
	print(level_area)
	print(must_be_mixed)
	print(dont_ask_scenario)
	print(num_of_matches)
	print(num_of_games_of_one_match)
	print(num_of_questions)
	print(kind_one_game)
	print(type_one_game)
	print(ask_never_asked_q)
	print(difficulty_mat)
	print(difficulty_gam)

	print("print subito subito subito subito RECEIVED FROM CUTOMIZED: FINE")
	mand_q, plus_one_game = dbem2.impose_id_ques(customized)
	print("mand_q, plus_one_game")
	print(mand_q)
	print(plus_one_game)
	if plus_one_game!=0:
		inds = random.randint(1,len(mand_q))
		db_self.imposed_ques_id = mand_q[inds-1]
		print("ho messo indice ques id !!!! {}".format(db_self.imposed_ques_id))
	else:
		db_self.imposed_ques_id = 99999
		print("ho messo non ce ques id 9999999999999999999 !!!! {}".format(db_self.imposed_ques_id))
	db_self.already_done_imposed_num_questions = 0
	db_self.already_done_imposed_num_matches = 0
	db_self.already_done_imposed_type_game = 0
	db_self.already_done_imposed_kind_game = 0
	db_self.already_done_imposed_num_games = 0
	db_self.already_done_imposed_mixed = 0
	db_self.already_done_imposed_difficulty_match = 0
	db_self.already_done_imposed_difficulty_game = 0
	db_self.already_done_imposed_ask_ques = 0
	db_self.imposed_num_matches = num_of_matches
	db_self.imposed_kind_game = copy.copy(kind_one_game)
	db_self.imposed_type_game = copy.copy(type_one_game)
	db_self.imposed_num_games = copy.copy(num_of_games_of_one_match)
	db_self.imposed_num_questions = copy.copy(num_of_questions)
	db_self.suggest_num_games_more_than = 0
	db_self.suggest_num_matches_more_than = 0
	db_self.suggested_type_game_from_q = ''
	db_self.suggested_kind_game_from_q = ''
	db_self.suggested_kind_game_from_t = ''
	db_self.suggested_scenario_from_t = 0
	db_self.suggested_scenario_from_k = 0
	db_self.suggested_scenario_from_q = 0
	db_self.imposed_mixed = 0
	db_self.game_impo_from_q_added = 0
	db_self.type_impo_from_k_added = 0
	db_self.type_impo_from_t_added = 0
	scena_res = dbem2.take_given_scenario(customized)
	print("scena_res ==D=AS=DAS=das ".format(scena_res))
	if len(scena_res)>0:
		db_self.imposed_scenario = scena_res[0]
	else:
		db_self.imposed_scenario = 0
	print("############################################################ scenario take_given_scenario {}".format(db_self.imposed_scenario))
	if db_self.imposed_ques_id!=99999:
		db_self.curs.execute("SELECT type,kind FROM Questions WHERE question_id=?",(db_self.imposed_ques_id,))
		sugge_res = db_self.curs.fetchall()
		db_self.suggested_type_game_from_q = copy.copy(sugge_res[0][0])
		db_self.suggested_kind_game_from_q = copy.copy(sugge_res[0][1])
		if db_self.suggested_type_game_from_q==db_self.imposed_type_game:
			db_self.imposed_type_game = ''
			print("HO RIMESSO SUGGESTED TPO DA QUESTION A ZERO!!!")
		if db_self.suggested_kind_game_from_q==db_self.imposed_kind_game:
			db_self.imposed_kind_game = ''
		print("HO RIMESSO SUGGESTED kind DA QUESTION A ZERO!!!")
		print("db_self.suggested_kind_game GRAZIE A QUESTION ID ===> {}".format(db_self.suggested_kind_game_from_q))
		print("db_self.suggested type game GRAZIE A QUESTION ID ===> {}".format(db_self.suggested_type_game_from_q))
	if db_self.imposed_type_game!='':
		db_self.curs.execute("SELECT kind FROM Questions WHERE type=?",(db_self.imposed_type_game,))
		db_self.suggested_kind_game_from_t = db_self.curs.fetchone()[0]
		if db_self.suggested_kind_game_from_t==db_self.imposed_kind_game:
			db_self.suggested_kind_game_from_t = ''
		print("db_self.suggested_kind_game GRAZIE a tipo imposto!! ===> {}".format(db_self.suggested_kind_game_from_t))
	
	if db_self.imposed_scenario and db_self.imposed_ques_id!=99999:
		db_self.curs.execute("SELECT scenario_id FROM Questions JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) on (audio=audio_id) WHERE question_id=? AND scenario_id=?",(db_self.imposed_ques_id,db_self.imposed_scenario,))
		resp_sce = db_self.curs.fetchall()
		if len(resp_sce)==1:
			one_match_extra = 1
		else:
			one_match_extra = 0

	# DA METTERE DOPO QUANDO AVRO' CREATO LE TABELLE!!!!!
	if db_self.imposed_scenario and db_self.imposed_type_game!='':
		db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Types USING (scenario_group) WHERE type=? AND scenario_id=?",(db_self.imposed_type_game,db_self.imposed_scenario,))
		resp_sce = db_self.curs.fetchall()
		if one_match_extra==0:
			if len(resp_sce)==1:
				one_match_extra = 1
			else:
				one_match_extra = 0
	if db_self.imposed_scenario and db_self.imposed_kind_game!='':
		db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind=? AND scenario_id=?",(db_self.imposed_kind_game,db_self.imposed_scenario,))
		resp_sce = db_self.curs.fetchall()
		if one_match_extra==0:
			if len(resp_sce)==1:
				one_match_extra = 1
			else:
				one_match_extra = 0

	if db_self.imposed_ques_id==99999 and db_self.imposed_kind_game!='' and db_self.imposed_type_game!='' and one_match_extra==1:
		db_self.suggest_num_matches_more_than = 3
	elif db_self.imposed_ques_id==99999 and db_self.imposed_kind_game!='' and db_self.imposed_type_game!='' and one_match_extra==0:		
		db_self.suggest_num_matches_more_than = 2
	elif db_self.imposed_ques_id==99999 and (db_self.imposed_kind_game=='' and db_self.imposed_type_game!='') or (db_self.imposed_kind_game!='' and db_self.imposed_type_game=='') and one_match_extra==1:
		db_self.suggest_num_games_more_than = random.randint(2,3)
		db_self.suggest_num_matches_more_than = 2
	elif db_self.imposed_ques_id==99999 and (db_self.imposed_kind_game=='' and db_self.imposed_type_game!='') or (db_self.imposed_kind_game!='' and db_self.imposed_type_game=='') and one_match_extra==0:
		db_self.suggest_num_games_more_than = random.randint(2,3)
		db_self.suggest_num_matches_more_than = 1
	elif db_self.imposed_ques_id==99999 and db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and (one_match_extra==1 or one_match_extra==0):
		db_self.suggest_num_matches_more_than = 1

	if db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game!='' and db_self.imposed_type_game!='' and one_match_extra==1:
		db_self.suggest_num_matches_more_than = 4
		db_self.suggest_num_games_more_than = 2
	elif db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game!='' and db_self.imposed_type_game!='' and one_match_extra==0:		
		db_self.suggest_num_matches_more_than = 3
		db_self.suggest_num_games_more_than = 2
	elif db_self.imposed_ques_id!=99999 and (db_self.imposed_kind_game=='' and db_self.imposed_type_game!='') or (db_self.imposed_kind_game!='' and db_self.imposed_type_game=='') and one_match_extra==1:
		db_self.suggest_num_games_more_than = random.randint(2,3)
		db_self.suggest_num_matches_more_than = 3
	elif db_self.imposed_ques_id!=99999 and (db_self.imposed_kind_game=='' and db_self.imposed_type_game!='') or (db_self.imposed_kind_game!='' and db_self.imposed_type_game=='') and one_match_extra==0:
		db_self.suggest_num_games_more_than = random.randint(2,3)
		db_self.suggest_num_matches_more_than = 2
	elif db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and one_match_extra==1:
		db_self.suggest_num_matches_more_than = 2
		db_self.suggest_num_games_more_than = random.randint(2,3)
	elif db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and one_match_extra==0:
		db_self.suggest_num_matches_more_than = 1

	print("SUGGERISCO NUM_:GAME {}".format(db_self.suggest_num_games_more_than))
	print("SUGGERISCO NUM_:MATCH {}".format(db_self.suggest_num_matches_more_than))

	db_self.imposed_difficulty_match = copy.copy(difficulty_mat)
	db_self.imposed_difficulty_game = copy.copy(difficulty_gam)
	db_self.imposed_mixed = copy.copy(must_be_mixed)
	print("ripetiamo come era la complexity!!! {}".format(complexity))
	if complexity=='':
		print("COMPLEXITY SESSION MANUALLY ADDED!!!")
		if difficulty_mat!='':
			if difficulty_mat in ('Easy_1','Easy_2'):
				complexity='Easy'
			if difficulty_mat in ('Normal_1','Normal_2'):
				maybe = random.randint(0,1)
				if maybe==0 or maybe==1:
					complexity='Easy'
				else:
					complexity='Normal'
			if difficulty_mat in ('Medium_1','Medium_2'):
				maybe = random.randint(0,1)
				if maybe==0 or maybe==1:
					complexity='Medium'
				else:
					complexity='Normal'
			if difficulty_mat in ('Hard_1','Hard_2'):
				maybe = random.randint(0,1)
				if maybe==0 or maybe==1:
					complexity='Hard'
				else:
					complexity='Medium'
			print("QUINDI COMPLESSITA INFINE È scelta in base a diff mat! {}".format(complexity))
		else:	
			print("COMPLESSITA NON DATA COMPLESSITA NON DATA COMPLESSITA NON DATA COMPLESSITA NON DATA COMPLESSITA NON DATA COMPLESSITA NON DATA COMPLESSITA NON DATA COMPLESSITA NON DATA COMPLESSITA NON DATA")
			choose_compl = random.randint(0,9)
			if choose_compl==0:
				complexity = 'Hard'
			if choose_compl==1 or choose_compl==2:
				complexity = 'Medium'
			if choose_compl==3 or choose_compl==4 or choose_compl==5 or choose_compl==6:
				complexity = 'Normal'
			if choose_compl==7 or choose_compl==8 or choose_compl==9:
				complexity = 'Easy'
			print("QUINDI COMPLESSITA INFINE È scelta a casissimo!! {}".format(complexity))
	else:
		print("QUINDI COMPLESSITA INFINE È UGUALE A QUELLA IMPOSTA OVVERO! {}".format(complexity))
	db_self.mandatory_impositions = 1
	db_self.mandatory_needs = 0
	extra_time_tolerance = 0
	movements_allowed = random.randint(0,1)
	stress_flag = 0
	led_brightness = 'std'
	if complexity=='Easy':
		print("HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO easy !!!! ==> conferma {}".format(complexity))
		body_enabled = 1
		rules_explication = 1
		frequency_exhortations = 1
		repeat_question, repeat_intro = 1,1
		
		if num_of_matches==0 and db_self.suggest_num_matches_more_than==0:
			num_of_matches = random.randint(1,3)
		if num_of_matches==0 and db_self.suggest_num_matches_more_than==1:
			num_of_matches = 2
		if num_of_matches==0 and db_self.suggest_num_matches_more_than>=2:
			num_of_matches = 3
		
		choose_diff = random.randint(0,6)
		if choose_diff==0:
			order_diff_matches = 'asc'
			order_quan_games = 'same'
		elif choose_diff==1:
			order_diff_matches = 'asc'
			order_quan_games = 'asc'
		elif choose_diff==2 or choose_diff==3:
			order_diff_matches = 'same'
			order_quan_games = 'asc'
		else:
			order_diff_matches = 'same'
			order_quan_games = 'same'
	elif complexity=='Normal':
		print("HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO Normal !!!! ==> conferma {}".format(complexity))
		body_enabled = 1
		rules_explication = 1
		frequency_exhortations = 1
		repeat_question, repeat_intro = 1,1
		if num_of_matches==0 and db_self.suggest_num_matches_more_than==0:
			num_of_matches = random.randint(1,3)
		if num_of_matches==0 and db_self.suggest_num_matches_more_than==1:
			num_of_matches = 2
		if num_of_matches==0 and db_self.suggest_num_matches_more_than>=2:
			num_of_matches = 3
		choose_diff = random.randint(0,5)
		choose_quan = random.randint(0,5)
		if choose_diff==0 or choose_diff==1 or choose_diff==2:
			order_diff_matches = 'same'
		elif choose_diff==3 or choose_diff==4:
			order_diff_matches = 'asc'
		elif choose_diff==5:
			order_diff_matches = 'casu'
		if choose_quan==0 or choose_quan==1 or choose_quan==2:
			order_quan_games = 'same'
		elif choose_quan==3 or choose_quan==4:
			order_quan_games = 'asc'
		elif choose_quan==5:
			order_quan_games = 'casu'
	elif complexity=='Medium':
		print("HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO medium !!!! ==> conferma {}".format(complexity))
		body_enabled = 1
		rules_explication = random.randint(0,1)
		frequency_exhortations = 0
		repeat_question, repeat_intro = 0,0
		if num_of_matches==0 and db_self.suggest_num_matches_more_than==0:
			num_of_matches = random.randint(2,4)
		if num_of_matches==0 and db_self.suggest_num_matches_more_than in (1,2):
			num_of_matches = 3
		if num_of_matches==0 and db_self.suggest_num_matches_more_than>=3:
			num_of_matches = 4
		choose_diff = random.randint(0,5)
		choose_quan = random.randint(0,5)
		if choose_diff==0 or choose_diff==1:
			order_diff_matches = 'same'
		elif choose_diff==2 or choose_diff==3:
			order_diff_matches = 'asc'
		elif choose_diff==4:
			order_diff_matches = 'casu'
		elif choose_diff==5:
			order_diff_matches = 'desc'
		if choose_quan==0 or choose_quan==1:
			order_quan_games = 'same'
		elif choose_quan==2 or choose_quan==3:
			order_quan_games = 'asc'
		elif choose_quan==4:
			order_quan_games = 'casu'
		elif choose_quan==5:
			order_quan_games = 'desc'
	elif complexity=='Hard':
		print("HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO HO SCELTO hard !!!! ==> conferma {}".format(complexity))				
		body_enabled = 1
		rules_explication = 0
		frequency_exhortations = 0
		repeat_question, repeat_intro = 1,1
		if num_of_matches==0 and db_self.suggest_num_matches_more_than==0:
			num_of_matches = random.randint(2,4)
		if num_of_matches==0 and db_self.suggest_num_matches_more_than in (1,2):
			num_of_matches = 3
		if num_of_matches==0 and db_self.suggest_num_matches_more_than>=3:
			num_of_matches = 4
		choose_diff = random.randint(0,5)
		choose_quan = random.randint(0,5)
		if choose_diff==0:
			order_diff_matches = 'same'
		elif choose_diff==1:
			order_diff_matches = 'asc'
		elif choose_diff==2 or choose_diff==3:
			order_diff_matches = 'casu'
		else:
			order_diff_matches = 'desc'
		if choose_quan==0:
			order_quan_games = 'same'
		elif choose_quan==1:
			order_quan_games = 'asc'
		elif choose_quan==2 or choose_quan==3:
			order_quan_games = 'casu'
		else:
			order_quan_games = 'desc'

	status = 'to_do'
	disposable = 0
	desirable_time = 320
	creation_way = 'automatically'
	#search_str = "{}, '{}', '{}', {}, {}".format(num_of_matches, complexity, status, db_self.mandatory_impositions, db_self.mandatory_needs)
	search_str = "{}, '{}', '{}', {}, {}, '{}', '{}', {}, {}, {}, {}, {}, {}, {}, {}, {}, '{}', {}, '{}'".format(num_of_matches, complexity, 
	status, db_self.mandatory_impositions, db_self.mandatory_needs, order_diff_matches, order_quan_games, extra_time_tolerance, movements_allowed, 
	body_enabled, rules_explication, stress_flag, frequency_exhortations, repeat_question, repeat_intro, desirable_time, creation_way, disposable, new_id)
	#try:
	find_me = search_str.encode()
	print("#############################")
	print("find_me is {}".format(find_me))
	print("find_me is {}".format(find_me))
	print("find_me is {}".format(find_me))
	print("find_me is {}".format(find_me))
	print("#############################")
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
		mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
		if s.find(find_me) != -1:
			print('--> The Session you want to insert already exist.')
			#raise Exception
			pass
		reload(oq)
		db_self.curs.execute('''INSERT INTO Sessions (num_of_matches, complexity, status, order_difficulty_matches, order_quantity_games, 
			mandatory_impositions, mandatory_needs, extra_time_tolerance, movements_allowed, body_enabled,
			rules_explication, stress_flag, frequency_exhortations, repeat_question, repeat_intro, desirable_time, creation_way, disposable, token)
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', (num_of_matches, complexity, status, order_diff_matches, order_quan_games, 
			db_self.mandatory_impositions, db_self.mandatory_needs, extra_time_tolerance, movements_allowed, body_enabled, 
			rules_explication, stress_flag, frequency_exhortations, repeat_question, repeat_intro, desirable_time, creation_way, disposable, new_id,))
		db_self.conn.commit()
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Sessions")
		num_last_line_s = db_self.curs.fetchone()[0]
		mdl.modify_Sessions_list(num_of_matches, complexity, status, order_diff_matches, order_quan_games, db_self.mandatory_impositions, db_self.mandatory_needs, 
			extra_time_tolerance, movements_allowed, body_enabled, 
			rules_explication, stress_flag, frequency_exhortations, repeat_question, repeat_intro, desirable_time, creation_way, disposable, new_id, num_last_line_s)
		
		#db_self.curs.execute("UPDATE Sessions SET date_time = DATETIME('now','localtime') WHERE date_time isNull")
		query_audio_intro = "UPDATE Sessions SET audio_intro = '{}'".format('audio_12330')
		query_audio_exit = "UPDATE Sessions SET audio_exit = '{}'".format('audio_12333')
		db_self.curs.execute(query_audio_intro)
		db_self.curs.execute(query_audio_exit)
		db_self.conn.commit()
		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
			print("ho trovato questa deletions_list!!!!")
			print("deletions_list = {}".format(db_self.deletions_list))
		if db_self.deletions_list[3]!=[0]:
			new_del = 1
			dbem.bubblesort(db_self.deletions_list[3])
			db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Sessions\'")
			index_to_replace = db_self.curs.fetchone()[1] 
			index_corrected = db_self.deletions_list[3].pop(0)
			db_self.curs.execute('''UPDATE Sessions SET session_id = ?	WHERE session_id = ?''', (index_corrected, index_to_replace))
			if not db_self.deletions_list[3]:
				db_self.deletions_list[3].append(0)
				filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
				with open(filename_pk, 'wb') as fi:
					pickle.dump(db_self.deletions_list, fi)				
			db_self.conn.commit()
	print("The new Session is now added.")
	#if not kid_id: #da mettere!! siamo sempre in quel caso ...altrimeti tabelle!!
	# here start the creation of matches!!
	print("ADESSO TOCCA AI MATCH!!")
	db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
	sess_last = db_self.curs.fetchone()[0]
	if complexity=='Easy':
		print("ADESSO ENTRO IN COMPLEX EASY!!")
		dba2.define_matches_when_complexity_is_easy(db_self,num_of_matches,sess_last,order_diff_matches,order_quan_games)
	if complexity=='Normal':
		print("ADESSO ENTRO IN COMPLEX NORMAL!!")
		dba2.define_matches_when_complexity_is_normal(db_self,num_of_matches,sess_last,order_diff_matches,order_quan_games)
	if complexity=='Medium':
		print("ADESSO ENTRO IN COMPLEX MEDIUM!!")
		dba21.define_matches_when_complexity_is_medium(db_self,num_of_matches,sess_last,order_diff_matches,order_quan_games)
	if complexity=='Hard':
		print("ADESSO ENTRO IN COMPLEX HARD!!")
		dba21.define_matches_when_complexity_is_hard(db_self,num_of_matches,sess_last,order_diff_matches,order_quan_games)



cdef add_new_Match_table(db_self, str difficulty, int num_of_games, str variety, str order_difficulty_games, str order_quantity_questions, str criteria_arrangement, int advisable_time, scenario, bint disposable):
	dba4.add_new_Match_table(db_self, difficulty, num_of_games, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, advisable_time, scenario, disposable)

cdef add_new_Game_table(db_self, int num_questions, str kind_game, str type_game, str difficulty, str order_difficulty_questions, int necessary_time, bint disposable, scenario, list list_types, list questions_already_choosen_this_match):
	num_ques_to_ret, questions_ok = dba6.add_new_game(db_self, num_questions, kind_game, type_game, difficulty, order_difficulty_questions, necessary_time, disposable, scenario, list_types, questions_already_choosen_this_match)
	return num_ques_to_ret, questions_ok

cdef add_new_Question_table(db_self, str kind_game, str type_game, str audio_id, int value, str description):
	cdef:
		str search_str = "\t('{0}', '{1}', '{2}', {3}, {4}, '{5}'),".format(kind_game, type_game, audio_id, value, description)
		bint new_del = 0
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Question you want to insert already exist.')
				#raise Exception
				pass
			reload(oq)
			reload(dbdl)
			db_self.curs.execute('''INSERT INTO Questions (kind, type, audio_id, value, description) 
				VALUES (?,?,?,?,?,?)''', (kind_game, type_game, audio_id, value, description))
			posi = 0
			db_self.conn.commit()
			mdl.modify_Questions_list(kind_game, type_game, audio_id, value, description, posi) 

			filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
			with open(filename_pk, 'rb') as fi:
				db_self.deletions_list = pickle.load(fi)
				print("ho trovato questa deletions_list!!!!")
				print("deletions_list = {}".format(db_self.deletions_list))
			if db_self.deletions_list[7]!=[0]:
				new_del = 1
				dbem.bubblesort(db_self.deletions_list[7])
				db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Questions\'")
				index_to_replace = db_self.curs.fetchone()[1] 
				index_corrected = db_self.deletions_list[7].pop(0)
				db_self.curs.execute('''UPDATE Questions SET question_id = ?  WHERE question_id = ?''', (index_corrected,index_to_replace))
				if not db_self.deletions_list[7]:
					db_self.deletions_list[7].append(0)
					filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
					with open(filename_pk, 'wb') as fi:
						pickle.dump(db_self.deletions_list, fi)

				db_self.conn.commit()
		print("The new Question is now added.")

	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new Question")
		print("--> Search_str add_new_Question {}".format(search_str))
		if new_del:
			print("--> 1))) {} ".format(index_to_replace))
			print("--> 2))) {} ".format(index_corrected))
		print("##############################################################################################################")


cdef add_the_new_patch(db_self, new_subject, new_color):
	try:
		reload(oq)
		reload(dbdl)	
		db_self.curs.execute("drop trigger if exists complete_patches_info_trigger")
		db_self.enable_patches_trigger()
		db_self.curs.execute("INSERT INTO Patches (element, color) VALUES (?,?)", (new_subject, new_color))
		mdl.modify_Patches_list(new_subject, new_color)
		db_self.conn.commit()
		print("Ok, Patch added inside oimi database.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")
# --------------------------------------------------------------------------------------------
# Add into Bridge
# --------------------------------------------------------------------------------------------
cdef add_new_KidTherapy_table(db_self, int kid_id, int therapy_id):
	print("entro in add_new_kidTherapy_table!!!!!!!!")
	cdef str search_str = "\t({0}, {1}),\n".format(kid_id, therapy_id)
	print("search_str {}".format(search_str))
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The new therapy for kid you want to insert already exist.')
				#raise Exception
				pass
		reload(oq)
		db_self.curs.execute('''INSERT INTO Kids_Therapies (kid_id, therapy_id) VALUES (?,?)''', (kid_id, therapy_id))
		db_self.conn.commit()
		db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Therapies")
		numb_pos = db_self.curs.fetchone()[0]
		mdl.modify_default_list_kids_therapies(kid_id, therapy_id, numb_pos)
		db_self.curs.execute("DROP TABLE IF EXISTS Kids_Therapies_tmp")
		db_self.curs.execute("UPDATE OimiSettings SET val = 0 WHERE keyp = 'fireTrigger'")
		db_self.curs.execute("CREATE TABLE Kids_Therapies_tmp AS SELECT * FROM Kids_Therapies ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE Kids_Therapies")
		db_self.curs.execute("ALTER TABLE Kids_Therapies_tmp RENAME TO Kids_Therapies")
		db_self.curs.execute('''CREATE INDEX IF NOT EXISTS kid_th_index ON Kids_Therapies (kid_id ASC)''')
		db_self.curs.execute("UPDATE OimiSettings SET val = 1 WHERE keyp = 'fireTrigger'")
		db_self.conn.commit()
		
		print("Done add_new_KidTherapy_table.")
		db_self.conn.commit()
		print("The new Kids_Therapies link is now added.")
		dcs.store_kids_therapies_so_far(db_self)
		para_ks = (kid_id, therapy_id)
		
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new therapy for this Kid")
		print("--> Search_str of Kids_Therapies table {}".format(search_str))
		print("##############################################################################################################")

cdef add_new_KidTreatment_table(db_self, int kid_id, int treatment_id):
	print("entro in add_new_kidTherapy_table!!!!!!!!")
	cdef str search_str = "\t({0}, {1}),\n".format(kid_id, treatment_id)
	print("search_str {}".format(search_str))
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The new treatment for kid you want to insert already exist.')
				#raise Exception
				pass
		reload(oq)
		db_self.curs.execute('''INSERT INTO Kids_Treatments (kid_id, treatment_id) VALUES (?,?)''', (kid_id, treatment_id))
		db_self.conn.commit()
		db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Treatments")
		numb_pos = db_self.curs.fetchone()[0]
		mdl.modify_default_list_kids_sessions(kid_id, treatment_id, numb_pos)
		db_self.curs.execute("DROP TABLE IF EXISTS Kids_Treatments_tmp")
		db_self.curs.execute("UPDATE OimiSettings SET val = 0 WHERE keyp = 'fireTrigger'")
		db_self.curs.execute("CREATE TABLE Kids_Treatments_tmp AS SELECT * FROM Kids_Treatments ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE Kids_Treatments")
		db_self.curs.execute("ALTER TABLE Kids_Treatments_tmp RENAME TO Kids_Treatments")
		db_self.curs.execute('''CREATE INDEX IF NOT EXISTS kid_tr_index ON Kids_Treatments (kid_id ASC)''')
		db_self.curs.execute("UPDATE OimiSettings SET val = 1 WHERE keyp = 'fireTrigger'")
		db_self.conn.commit()
		
		print("Done add_new_KidTreatment_table.")
		db_self.conn.commit()
		print("The new Kids_Treatments link is now added.")
		dcs.store_kids_therapies_so_far(db_self)
		para_ks = (kid_id, treatment_id)
		
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new treatment for this Kid")
		print("--> Search_str of Kids_Treatments table {}".format(search_str))
		print("##############################################################################################################")

cdef add_new_KidSession_table(db_self, int kid_id, int session_id):
	print("entro in add_new_kidSession_table!!!!!!!!")
	cdef str search_str = "\t({0}, {1}),\n".format(kid_id, session_id)
	print("search_str {}".format(search_str))
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The new session for kid you want to insert already exist.')
				#raise Exception
				pass
		reload(oq)
		db_self.curs.execute('''INSERT INTO Kids_Sessions (kid_id, session_id) VALUES (?,?)''', (kid_id, session_id))
		db_self.conn.commit()
		db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Sessions")
		numb_pos = db_self.curs.fetchone()[0]
		mdl.modify_default_list_kids_sessions(kid_id, session_id, numb_pos)
		db_self.curs.execute("DROP TABLE IF EXISTS Kids_Sessions_tmp")
		db_self.curs.execute("UPDATE OimiSettings SET val = 0 WHERE keyp = 'fireTrigger'")
		db_self.curs.execute("CREATE TABLE Kids_Sessions_tmp AS SELECT * FROM Kids_Sessions ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE Kids_Sessions")
		db_self.curs.execute("ALTER TABLE Kids_Sessions_tmp RENAME TO Kids_Sessions")
		db_self.curs.execute('''CREATE INDEX IF NOT EXISTS kid_se_index ON Kids_Sessions (kid_id ASC)''')
		db_self.curs.execute("UPDATE OimiSettings SET val = 1 WHERE keyp = 'fireTrigger'")
		db_self.conn.commit()
		#print("Updating also Kids_Matches and Kids_Games... ")
		dcb.build_bridge_KidMatch(db_self)
		dcb.build_bridge_KidGame(db_self)
		dcb.build_bridge_KidQuestion(db_self)
		
		print("Done add_new_KidSession_table.")
		db_self.conn.commit()
		print("The new Kid_Session link is now added.")
		dcs.store_kids_sessions_so_far(db_self)
		para_ks = (kid_id, session_id)
		
		#non qui!!!!
		#db_self.execute_param_query(oq.sql_insertion_table_results,para_ks)
		#db_self.execute_param_query(oq.sql_insertion_summary,para_ks)

		#solo per ordinare
		#db_self.curs.execute("CREATE TABLE giga_tmp AS SELECT * FROM Full_played_recap_giga ORDER BY kid_id ASC")
		#db_self.curs.execute("DROP TABLE IF EXISTS Full_played_recap_giga")
		#db_self.curs.execute("CREATE TABLE Full_played_recap_giga AS SELECT * FROM giga_tmp")
		#db_self.curs.execute('DROP TABLE IF EXISTS giga_tmp')
		#db_self.conn.commit()
		#db_self.curs.execute("CREATE TABLE summary_tmp AS SELECT * FROM Full_played_summary ORDER BY kid_id ASC")
		#db_self.curs.execute("DROP TABLE IF EXISTS Full_played_summary")
		#db_self.curs.execute("CREATE TABLE Full_played_summary AS SELECT * FROM summary_tmp")
		#db_self.curs.execute('DROP TABLE IF EXISTS summary_tmp')
		#db_self.conn.commit()
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new session for this Kid")
		print("--> Search_str of Kids_Sessions table {}".format(search_str))
		print("##############################################################################################################")

cdef add_new_KidEntertainment_table(db_self, int kid_id, int entertainment_id):
	print("entro in add_new_kidEntertainment_table!!!!!!!!")
	cdef str search_str = "\t({0}, {1}),\n".format(kid_id, entertainment_id)
	print("search_str {}".format(search_str))
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The new session for kid you want to insert already exist.')
				#raise Exception
				pass
		reload(oq)
		db_self.curs.execute('''INSERT INTO Kids_Entertainments (kid_id, entertainment_id) VALUES (?,?)''', (kid_id, entertainment_id))
		db_self.conn.commit()
		db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Entertainments")
		numb_pos = db_self.curs.fetchone()[0]
		mdl.modify_default_list_kids_entertainments(kid_id, entertainment_id, numb_pos)
		db_self.curs.execute("DROP TABLE IF EXISTS Kids_Entertainments_tmp")
		db_self.curs.execute("UPDATE OimiSettings SET val = 0 WHERE keyp = 'fireTrigger'")
		db_self.curs.execute("CREATE TABLE Kids_Entertainments_tmp AS SELECT * FROM Kids_Entertainments ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE Kids_Entertainments")
		db_self.curs.execute("ALTER TABLE Kids_Entertainments_tmp RENAME TO Kids_Entertainments")
		db_self.curs.execute('''CREATE INDEX IF NOT EXISTS kid_en_index ON Kids_Entertainments (kid_id ASC)''')
		db_self.curs.execute("UPDATE OimiSettings SET val = 1 WHERE keyp = 'fireTrigger'")
		db_self.conn.commit()
		
		print("Done add_new_Kids_Entertainments_table.")
		db_self.conn.commit()
		print("The new Kids_Entertainments link is now added.")
		dcs.store_kids_sessions_so_far(db_self)
		para_ks = (kid_id, entertainment_id)
		
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new session for this Kid")
		print("--> Search_str of Kids_Sessions table {}".format(search_str))
		print("##############################################################################################################")


cdef add_new_SessionMatch_table(db_self, int session_id, int match_id):
	cdef str search_str = "\t({0}, {1}),\n".format(session_id, match_id)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The new match for this session you want to insert already exist!!')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Sessions_Matches (session_id, match_id) VALUES (?,?)''', (session_id, match_id))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Sessions_Matches")
			numb_pos = db_self.curs.fetchone()[0]
			print("numb numb is {}".format(numb_pos))
			mdl.modify_default_list_sessions_matches(session_id, match_id, numb_pos)
			db_self.curs.execute("DROP TABLE IF EXISTS Sessions_Matches_tmp")
			db_self.curs.execute("CREATE TABLE Sessions_Matches_tmp AS SELECT * FROM Sessions_Matches ORDER BY session_id ASC")
			db_self.curs.execute("DROP TABLE Sessions_Matches")
			db_self.curs.execute("ALTER TABLE Sessions_Matches_tmp RENAME TO Sessions_Matches")
			db_self.curs.execute('''CREATE INDEX session_index ON Sessions_Matches (session_id ASC)''')
			db_self.conn.commit()
		print("The new Session_Match link is now added.")				
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new Session_Match link")
		print("--> Search_str for Session Match{}".format(search_str))
		print("##############################################################################################################")

cdef add_new_MatchGame_table(db_self, int match_id, int game_id):
	print("ENTER INTO add_new_MatchGame_table!!!!!")
	cdef str search_str = "\t({0}, {1}),\n".format(match_id, game_id)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('The new game for this match you want to insert already exist!!')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Matches_Games (match_id, game_id) VALUES (?,?)''', (match_id, game_id))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Matches_Games")
			numb_pos = db_self.curs.fetchone()[0]
			print("numb numb is {}".format(numb_pos))
			mdl.modify_default_list_matches_games(match_id, game_id, numb_pos)
			db_self.curs.execute("DROP TABLE IF EXISTS Matches_Games_tmp")
			db_self.curs.execute("CREATE TABLE Matches_Games_tmp AS SELECT * FROM Matches_Games ORDER BY match_id ASC")
			db_self.curs.execute("DROP TABLE Matches_Games")
			db_self.curs.execute("ALTER TABLE Matches_Games_tmp RENAME TO Matches_Games")
			db_self.curs.execute('''CREATE INDEX match_index ON Matches_Games (match_id ASC)''')
			db_self.conn.commit()
		print("The new Match_Game link is now added. with {}".format(search_str))
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef add_new_GameQuestion_table(db_self, int game_id, int question_id):
	cdef:
		str search_str = "\t({0}, {1}),\n".format(game_id, question_id)
		int add_more = 0
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1: 
				if question_id!=3051:
					print('The new question of game you want to insert already exist.')
					raise Exception
				else:
					print("add new num!!!!! saima!!!!")
					add_more = 1
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Games_Questions (game_id, question_id) VALUES (?,?)''', (game_id, question_id))
			db_self.conn.commit()
			db_self.curs.execute( "SELECT COUNT(rowid) FROM Games_Questions")
			num_gq = db_self.curs.fetchone()[0]
			if add_more:
				print("milano saima ale!")
				num_gq = num_gq+1
				print("aggiungoi un num!!")
			mdl.modify_default_list_games_questions(game_id, question_id, num_gq)
			db_self.curs.execute("DROP TABLE IF EXISTS Games_Questions_tmp")
			db_self.curs.execute("CREATE TABLE Games_Questions_tmp AS SELECT * FROM Games_Questions ORDER BY game_id ASC")
			db_self.curs.execute("DROP TABLE Games_Questions")
			db_self.curs.execute("ALTER TABLE Games_Questions_tmp RENAME TO Games_Questions")
			db_self.curs.execute('''CREATE INDEX game_index ON Games_Questions (game_id ASC)''')
			db_self.conn.commit()
		print("The new Game_Question connection is now added. with {}".format(search_str))
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new Question to a Game")
		print("search_str GameQuestion {}".format(search_str))
		print("##############################################################################################################")



cdef add_new_KidImposition_table(db_self, int kid_id, str imposition_name):
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(kid_id, imposition_name)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The new IMPOSITION for KID you want to insert already exist.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Kids_Impositions (kid_id, imposition_name) VALUES (?,?)''', (kid_id, imposition_name))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Impositions")
			numb_pos = db_self.curs.fetchone()[0]
			print("numb_pos {}".format(numb_pos))				
			mdl.modify_default_list_kids_impositions(kid_id, imposition_name, numb_pos)
			db_self.curs.execute("DROP TABLE IF EXISTS Kids_Impositions_tmp")
			db_self.curs.execute("CREATE TABLE Kids_Impositions_tmp AS SELECT * FROM Kids_Impositions ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Kids_Impositions")
			db_self.curs.execute("ALTER TABLE Kids_Impositions_tmp RENAME TO Kids_Impositions")
			db_self.curs.execute('''CREATE INDEX kid_imp_index ON Kids_Impositions (kid_id ASC)''')
			db_self.conn.commit()
		print("The new Kid_Imposition link is now added.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while linking a kid with a new imposition")
		print("--> Search_str of Kids Impositions {}".format(search_str))
		print("##############################################################################################################")		

cdef add_new_KidNeed_table(db_self, int kid_id, int need_id):
	cdef:
		str	search_str = "\t({0}, {1}),\n".format(kid_id, need_id)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The new need for the kid already exist.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Kids_Needs (kid_id, need_id) VALUES (?,?)''', (kid_id, need_id))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Needs")
			numb_pos = db_self.curs.fetchone()[0]
			print("numb numb is {}".format(numb_pos))
			mdl.modify_default_list_kids_needs(kid_id, need_id, numb_pos)
			db_self.curs.execute("DROP TABLE IF EXISTS Kids_Needs_tmp")
			db_self.curs.execute("CREATE TABLE Kids_Needs_tmp AS SELECT * FROM Kids_Needs ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Kids_Needs")
			db_self.curs.execute("ALTER TABLE Kids_Needs_tmp RENAME TO Kids_Needs")
			db_self.curs.execute('''CREATE INDEX kid_ne_index ON Kids_Needs (kid_id ASC)''')
			db_self.conn.commit()
		print("The new Kid_Need link is now added.")				
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while linking a kid with a new need")
		print("--> Search_str of Kids_Needs {}".format(search_str))
		print("##############################################################################################################")

cdef add_new_KidAchievement_table(db_self, int kid_id, str achievement_name):
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(kid_id, achievement_name)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('The new Achievement for question you want to insert already exist.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Kids_Achievements (kid_id, achievement_name) VALUES (?,?)''', (kid_id, achievement_name))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Achievements")
			numb_pos = db_self.curs.fetchone()[0]
			print("numb_pos {}".format(numb_pos))	
			mdl.modify_default_list_kids_achievements(kid_id, achievement_name, numb_pos)
			db_self.curs.execute("DROP TABLE IF EXISTS Kids_Achievements_tmp")
			db_self.curs.execute("CREATE TABLE Kids_Achievements_tmp AS SELECT * FROM Kids_Achievements ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Kids_Achievements")
			db_self.curs.execute("ALTER TABLE Kids_Achievements_tmp RENAME TO Kids_Achievements")
			db_self.curs.execute('''CREATE INDEX kid_ac_index ON Kids_Achievements (kid_id ASC)''')
			db_self.conn.commit()
		print("The new Kid_Achievement link is now added.")				
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef add_new_KidSymptom_table(db_self, int kid_id, str symptom_name):
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(kid_id, symptom_name)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('The new Symptom for question you want to insert already exist.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Kids_Symptoms (kid_id, symptom_name) VALUES (?,?)''', (kid_id, symptom_name))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Symptoms")
			numb_pos = db_self.curs.fetchone()[0]
			#if numb_pos:
			#	numb_pos = numb_pos[0]
			print("numb_pos {}".format(numb_pos))
			mdl.modify_default_list_kids_symptoms(kid_id, symptom_name, numb_pos)
			db_self.conn.commit()
			db_self.curs.execute("DROP TABLE IF EXISTS Kids_Symptoms_tmp")
			db_self.curs.execute("CREATE TABLE Kids_Symptoms_tmp AS SELECT * FROM Kids_Symptoms ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Kids_Symptoms")
			db_self.curs.execute("ALTER TABLE Kids_Symptoms_tmp RENAME TO Kids_Symptoms")
			db_self.curs.execute('''CREATE INDEX kid_sy_index ON Kids_Symptoms (kid_id ASC)''')
			db_self.conn.commit()
		print("The new Kid_Symptom link is now added.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef add_new_KidIssue_table(db_self, int kid_id, str issue_name):
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(kid_id, issue_name)
	try:
		find_me = search_str.encode()
		print("find_me is = ",find_me)
		print("find_me is = ",find_me)
		print("find_me is = ",find_me)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			print("s ",s)
			if s.find(find_me) != -1:
				print("sono in IF IFIFIFIF ")
				print('The new Kid Issue you want to insert already exist!!!!!!!!!!!!.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Kids_Issues (kid_id, issue_name) VALUES (?,?)''', (kid_id, issue_name))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Issues")
			numb_pos = db_self.curs.fetchone()[0]
			print("FINALLY numb_pos IS ", numb_pos)
			#if numb_pos:
			#	numb_pos = numb_pos[0]
			print("numb_pos {}".format(numb_pos))
			mdl.modify_default_list_kids_issues(kid_id, issue_name, numb_pos)
			db_self.conn.commit()
			db_self.curs.execute("DROP TABLE IF EXISTS Kids_Issues_tmp")
			db_self.curs.execute("CREATE TABLE Kids_Issues_tmp AS SELECT * FROM Kids_Issues ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Kids_Issues")
			db_self.curs.execute("ALTER TABLE Kids_Issues_tmp RENAME TO Kids_Issues")
			db_self.curs.execute('''CREATE INDEX IF NOT EXISTS kid_is_index ON Kids_Issues (kid_id ASC)''')
			db_self.conn.commit()
		print("The new link Kid_Issue is now added.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef add_new_KidComorbidity_table(db_self, int kid_id, str comorbidity_name):
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(kid_id, comorbidity_name)
	try:
		find_me = search_str.encode()
		print("find_me comorb is = ",find_me)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			print("s ",s)
			if s.find(find_me) != -1:
				print("sono in IF IFIFIFIF ")
				print('The new Kid Issue you want to insert already exist!!!!!!!!!!!!.')
				print("ECCEZIONE 111")
				raise Exception
			reload(oq)
			print("ho fatto reload")
			db_self.curs.execute('''INSERT INTO Kids_Comorbidities (kid_id, comorbidity_name) VALUES (?,?)''', (kid_id, comorbidity_name))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Comorbidities")
			numb_posit = db_self.curs.fetchone()
			print("FINALLY numb_pos IS ", numb_posit)
			if numb_posit:
				numb_pos = numb_posit[0]
			print("numb_pos {}".format(numb_pos))
			print("tipo tipo numb_pos {}".format(type(numb_pos)))
			mdl.modify_default_list_kids_comorbidities(kid_id, comorbidity_name, numb_pos)
			db_self.conn.commit()
			db_self.curs.execute("DROP TABLE IF EXISTS Kids_Comorbidities_tmp")
			db_self.curs.execute("CREATE TABLE Kids_Comorbidities_tmp AS SELECT * FROM Kids_Comorbidities ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Kids_Comorbidities")
			db_self.curs.execute("ALTER TABLE Kids_Comorbidities_tmp RENAME TO Kids_Comorbidities")
			db_self.curs.execute('''CREATE INDEX IF NOT EXISTS kid_co_index ON Kids_Comorbidities (kid_id ASC)''')
			db_self.conn.commit()
			print("The new link Kids_Comorbidities is now added.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef add_new_KidStrength_table(db_self, int kid_id, str strength_name):
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(kid_id, strength_name)
	try:
		find_me = search_str.encode()
		print("find_me is = ",find_me)
		print("find_me is = ",find_me)
		print("find_me is = ",find_me)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			print("s ",s)
			if s.find(find_me) != -1:
				print("sono in IF IFIFIFIF ")
				print('The new Kid Issue you want to insert already exist!!!!!!!!!!!!.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Kids_Strengths (kid_id, strength_name) VALUES (?,?)''', (kid_id, strength_name))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Strengths")
			numb_pos = db_self.curs.fetchone()[0]
			print("FINALLY numb_pos IS ", numb_pos)
			#if numb_pos:
			#	numb_pos = numb_pos[0]
			print("numb_pos {}".format(numb_pos))
			mdl.modify_default_list_kids_strengths(kid_id, strength_name, numb_pos)
			db_self.conn.commit()
			db_self.curs.execute("DROP TABLE IF EXISTS Kids_Strengths_tmp")
			db_self.curs.execute("CREATE TABLE Kids_Strengths_tmp AS SELECT * FROM Kids_Strengths ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Kids_Strengths")
			db_self.curs.execute("ALTER TABLE Kids_Strengths_tmp RENAME TO Kids_Strengths")
			db_self.curs.execute('''CREATE INDEX IF NOT EXISTS kid_st_index ON Kids_Strengths (kid_id ASC)''')
			db_self.conn.commit()
		print("The new link Kids_Strengths is now added.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef add_new_SessionEnforcement_table(db_self, int session_id, str enforcement_name):
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(session_id, enforcement_name)
	try:
		print("add_new_SessionEnforcement add_new_SessionEnforcement add_new_SessionEnforcement")
		if enforcement_name!='redo':
			find_me = search_str.encode()
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
				mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
				if s.find(find_me) != -1:
					print('--> The new SessionEnforcement you want to insert already exist.')
					raise Exception
					#pass
				reload(oq)
				db_self.curs.execute('''INSERT INTO Sessions_Enforcements (session_id, enforcement_name) VALUES (?,?)''', (session_id, enforcement_name))
				db_self.conn.commit()
				db_self.curs.execute("SELECT COUNT(rowid) FROM Sessions_Enforcements")
				numb_pos = db_self.curs.fetchone()
				if numb_pos:
					numb_pos = numb_pos[0]
				mdl.modify_default_list_sessions_enforcements(session_id, enforcement_name, numb_pos)
				db_self.curs.execute("DROP TABLE IF EXISTS Sessions_Enforcements_tmp")
				db_self.curs.execute("CREATE TABLE Sessions_Enforcements_tmp AS SELECT * FROM Sessions_Enforcements ORDER BY session_id ASC")
				db_self.curs.execute("DROP TABLE Sessions_Enforcements")
				db_self.curs.execute("ALTER TABLE Sessions_Enforcements_tmp RENAME TO Sessions_Enforcements")
				db_self.curs.execute('''CREATE INDEX sess_imp_index ON Sessions_Enforcements (session_id ASC)''')
				db_self.conn.commit()
			print("The new Session Enforcement link is now added.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while assigning an imposition to a session")
		print("--> Search_str of Sessions_Impositions {}".format(search_str))
		print("##############################################################################################################")

cdef add_new_SessionImposition_table(db_self, int session_id, str imposition_name):
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(session_id, imposition_name)
	try:
		print("add_new_SessionImposition_table add_new_SessionImposition_table add_new_SessionImposition_table")
		if imposition_name!='redo':
			find_me = search_str.encode()
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
				mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
				if s.find(find_me) != -1:
					print('--> The new Session Imposition you want to insert already exist.')
					raise Exception
					#pass
				reload(oq)
				db_self.curs.execute('''INSERT INTO Sessions_Impositions (session_id, imposition_name) VALUES (?,?)''', (session_id, imposition_name))
				db_self.conn.commit()
				db_self.curs.execute("SELECT COUNT(rowid) FROM Sessions_Impositions")
				numb_pos = db_self.curs.fetchone()
				if numb_pos:
					numb_pos = numb_pos[0]
				mdl.modify_default_list_sessions_impositions(session_id, imposition_name, numb_pos)
				db_self.curs.execute("DROP TABLE IF EXISTS Sessions_Impositions_tmp")
				db_self.curs.execute("CREATE TABLE Sessions_Impositions_tmp AS SELECT * FROM Sessions_Impositions ORDER BY session_id ASC")
				db_self.curs.execute("DROP TABLE Sessions_Impositions")
				db_self.curs.execute("ALTER TABLE Sessions_Impositions_tmp RENAME TO Sessions_Impositions")
				db_self.curs.execute('''CREATE INDEX sess_imp_index ON Sessions_Impositions (session_id ASC)''')
				db_self.conn.commit()
			print("The new Session_Imposition link is now added.")		
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while assigning an imposition to a session")
		print("--> Search_str of Sessions_Impositions {}".format(search_str))
		print("##############################################################################################################")

cdef add_new_SessionNeed_table(db_self, int session_id, int need_id):
	print("add_new_SessionNeed_table add_new_SessionNeed_table add_new_SessionNeed_table add_new_SessionNeed_table add_new_SessionNeed_table")
	cdef:
		str	search_str = "\t({0}, {1}),\n".format(session_id, need_id)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The new need for the kid already exist.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Sessions_Needs (session_id, need_id) VALUES (?,?)''', (session_id, need_id))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Sessions_Needs")
			numb_pos = db_self.curs.fetchone()
			print(numb_pos)
			if numb_pos:
				numb_pos = numb_pos[0]
			mdl.modify_default_list_sessions_needs(session_id, need_id, numb_pos)
			db_self.curs.execute("DROP TABLE IF EXISTS Sessions_Needs_tmp")
			db_self.curs.execute("CREATE TABLE Sessions_Needs_tmp AS SELECT * FROM Sessions_Needs ORDER BY session_id ASC")
			db_self.curs.execute("DROP TABLE Sessions_Needs")
			db_self.curs.execute("ALTER TABLE Sessions_Needs_tmp RENAME TO Sessions_Needs")
			db_self.curs.execute('''CREATE INDEX sess_ne_index ON Sessions_Needs (session_id ASC)''')
			db_self.conn.commit()
		print("The new Session_Need link is now added.")				
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while assigning a need to a session")
		print("--> Search_str of Sessions_Needs {}".format(search_str))
		print("##############################################################################################################")

##########################################################################################################################################
cdef add_new_bridge_TreatmentEntertainment(db_self, int treatment_id, int entertainment_id):
	reload(oq)
	db_self.curs.execute('''INSERT INTO Treatments_Entertainments (treatment_id, entertainment_id) VALUES (?,?)''', (treatment_id, entertainment_id))
	db_self.conn.commit()
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Entertainments_tmp')
	db_self.curs.execute("CREATE TABLE Treatments_Entertainments_tmp (treatment_id INTEGER, entertainment_id INTEGER, PRIMARY KEY (treatment_id, entertainment_id))")
	db_self.curs.execute("INSERT INTO Treatments_Entertainments_tmp (treatment_id, entertainment_id) SELECT treatment_id, entertainment_id FROM Treatments_Entertainments \
	ORDER BY treatment_id")
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Entertainments')
	db_self.curs.execute('ALTER TABLE Treatments_Entertainments_tmp RENAME TO Treatments_Entertainments')
	db_self.conn.commit()
	print("Treatments_Entertainments modified")

cdef add_new_bridge_TreatmentSession(db_self, int treatment_id, int session_id):
	db_self.curs.execute('''INSERT INTO Treatments_Sessions (treatment_id, session_id) VALUES (?,?)''', (treatment_id, session_id))
	db_self.conn.commit()
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Sessions_tmp')
	db_self.curs.execute("CREATE TABLE Treatments_Sessions_tmp (treatment_id INTEGER, session_id INTEGER, PRIMARY KEY (treatment_id, session_id))")
	db_self.curs.execute("INSERT INTO Treatments_Sessions_tmp (treatment_id, session_id) SELECT treatment_id, session_id FROM Treatments_Sessions \
	ORDER BY treatment_id")
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Sessions')
	db_self.curs.execute('ALTER TABLE Treatments_Sessions_tmp RENAME TO Treatments_Sessions')
	db_self.conn.commit()
	print("Treatments_Sessions modified")


cdef add_new_bridge_TreatmentPastime(db_self, int treatment_id, int pastime_id):
	db_self.curs.execute('''INSERT INTO Treatments_Pastimes (treatment_id, pastime_id) VALUES (?,?)''', (treatment_id, pastime_id))
	db_self.conn.commit()
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Pastimes_tmp')
	db_self.curs.execute("CREATE TABLE Treatments_Pastimes_tmp (treatment_id INTEGER, pastime_id INTEGER, PRIMARY KEY (treatment_id, pastime_id))")
	db_self.curs.execute("INSERT INTO Treatments_Pastimes_tmp (treatment_id, pastime_id) SELECT treatment_id, pastime_id FROM Treatments_Pastimes \
	ORDER BY treatment_id")
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Pastimes')
	db_self.curs.execute('ALTER TABLE Treatments_Pastimes_tmp RENAME TO Treatments_Pastimes')
	db_self.conn.commit()
	print("Treatments_Pastimes created")


############################### OLD
cdef add_to_OldKidNeed_table(db_self, int kid_id, int old_need_id):
	print("WELCOME TO THE FAMILY!!!!")
	cdef:
		str	search_str = "\t({0}, {1}),\n".format(kid_id, old_need_id)
	try:
		find_me = search_str.encode()
		print("find_me = ",find_me)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			reload(oq)
			db_self.curs.execute('''INSERT INTO Old_Kids_Needs (kid_id, old_need_id) VALUES (?,?)''', (kid_id, old_need_id))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Old_Kids_Needs")
			numb_pos = db_self.curs.fetchone()[0]
			print("numb numb is {}".format(numb_pos))
			mdl.modify_default_list_old_kids_needs(kid_id, old_need_id, numb_pos)
			db_self.curs.execute("DROP TABLE IF EXISTS Old_Kids_Needs_tmp")
			db_self.curs.execute("CREATE TABLE Old_Kids_Needs_tmp AS SELECT * FROM Old_Kids_Needs ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Old_Kids_Needs")
			db_self.curs.execute("ALTER TABLE Old_Kids_Needs_tmp RENAME TO Old_Kids_Needs")
			db_self.curs.execute('''CREATE INDEX if NOT EXISTS old_kid_ne_index ON Kids_Needs (kid_id ASC)''')
			db_self.conn.commit()
		print("BELLA!!! The old Kid_Need is stored.")		
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef add_to_OldKidSymptom_table(db_self, int kid_id, str old_symptom_name):
	print("WELCOME TO add_to_OldKidSymptom_table!!!!")
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(kid_id, old_symptom_name)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('The old Symptom for question you want to insert in Old_Kids_Symptoms already exist.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Old_Kids_Symptoms (kid_id, old_symptom_name) VALUES (?,?)''', (kid_id, old_symptom_name))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Old_Kids_Symptoms")
			numb_pos = db_self.curs.fetchone()[0]
			#if numb_pos:
			#	numb_pos = numb_pos[0]
			print("numb_pos {}".format(numb_pos))
			mdl.modify_default_list_old_tables_kids(kid_id, old_symptom_name, numb_pos, 'Old_Kids_Symptoms')
			#mdl.modify_default_list_old_kids_symptoms(kid_id, old_symptom_name, numb_pos)
			db_self.conn.commit()
			db_self.curs.execute("DROP TABLE IF EXISTS Old_Kids_Symptoms_tmp")
			db_self.curs.execute("CREATE TABLE Old_Kids_Symptoms_tmp AS SELECT * FROM Old_Kids_Symptoms ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Old_Kids_Symptoms")
			db_self.curs.execute("ALTER TABLE Old_Kids_Symptoms_tmp RENAME TO Old_Kids_Symptoms")
			db_self.curs.execute('''CREATE INDEX IF NOT EXISTS old_kid_sy_index ON Old_Kids_Symptoms (kid_id ASC)''')
			db_self.conn.commit()
		print("The old Old_Kid_Symptom link is now added.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")
	
cdef add_to_OldKidIssue_table(db_self, int kid_id, str old_issue_name):
	reload(dbdlot)
	print("WELCOME TO add into Old_Kids_Issues_table!!!!")
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(kid_id, old_issue_name)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('The old Issue for question you want to insert in Old_Kids_Issues already exist.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Old_Kids_Issues (kid_id, old_issue_name) VALUES (?,?)''', (kid_id, old_issue_name))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Old_Kids_Issues")
			numb_pos = db_self.curs.fetchone()[0]
			#if numb_pos:
			#	numb_pos = numb_pos[0]
			print("numb_pos {}".format(numb_pos))
			mdl.modify_default_list_old_tables_kids(kid_id, old_issue_name, numb_pos, 'Old_Kids_Issues')
			#mdl.modify_default_list_old_kids_issues(kid_id, old_issue_name, numb_pos)
			db_self.conn.commit()
			db_self.curs.execute("DROP TABLE IF EXISTS Old_Kids_Issues_tmp")
			db_self.curs.execute("CREATE TABLE Old_Kids_Issues_tmp AS SELECT * FROM Old_Kids_Issues ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Old_Kids_Issues")
			db_self.curs.execute("ALTER TABLE Old_Kids_Issues_tmp RENAME TO Old_Kids_Issues")
			db_self.curs.execute('''CREATE INDEX IF NOT EXISTS old_kid_sy_index ON Old_Kids_Issues (kid_id ASC)''')
			db_self.conn.commit()
		print("The Old_Kids_Issues link is now added.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef add_to_OldKidComorbidity_table(db_self, int kid_id, str old_comorbidity_name):
	print("WELCOME TO add into Old_Kids_Comorbidities_table!!!!")
	cdef:
		str search_str = "\t({0}, '{1}'),\n".format(kid_id, old_comorbidity_name)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('The old Symptom for question you want to insert in Old_Kids_Comorbidities already exist.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Old_Kids_Comorbidities (kid_id, old_comorbidity_name) VALUES (?,?)''', (kid_id, old_comorbidity_name))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Old_Kids_Comorbidities")
			numb_pos = db_self.curs.fetchone()[0]
			#if numb_pos:
			#	numb_pos = numb_pos[0]
			print("numb_pos {}".format(numb_pos))
			#mdl.modify_default_list_old_kids_comorbidities(kid_id, old_comorbidity_name, numb_pos)
			mdl.modify_default_list_old_tables_kids(kid_id, old_comorbidity_name, numb_pos, 'Old_Kids_Comorbidities')
			db_self.conn.commit()
			db_self.curs.execute("DROP TABLE IF EXISTS Old_Kids_Issues_tmp")
			db_self.curs.execute("CREATE TABLE Old_Kids_Issues_tmp AS SELECT * FROM Old_Kids_Comorbidities ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Old_Kids_Comorbidities")
			db_self.curs.execute("ALTER TABLE Old_Kids_Issues_tmp RENAME TO Old_Kids_Comorbidities")
			db_self.curs.execute('''CREATE INDEX IF NOT EXISTS old_kid_sy_index ON Old_Kids_Comorbidities (kid_id ASC)''')
			db_self.conn.commit()
		print("The Old_Kids_Comorbidities link is now added.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef add_to_OldKidStrength_table(db_self, int kid_id, str old_strength_name):
	print("WELCOME TO add into OldKidStrength_table!!!!")
	cdef str search_str = "\t({0}, '{1}'),\n".format(kid_id, old_strength_name)
	print("search_str = search_str = {}".format(search_str))
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_old_tables.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('The old strength for question you want to insert in Old_Kids_Strengths already exist.')
				raise Exception
				#pass
			reload(oq)
			db_self.curs.execute('''INSERT INTO Old_Kids_Strengths (kid_id, old_strength_name) VALUES (?,?)''', (kid_id, old_strength_name))
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(rowid) FROM Old_Kids_Strengths")
			numb_pos = db_self.curs.fetchone()[0]
			#if numb_pos:
			#	numb_pos = numb_pos[0]
			print("numb_pos {}".format(numb_pos))
			mdl.modify_default_list_old_tables_kids(kid_id, old_strength_name, numb_pos, 'Old_Kids_Strengths')
			#mdl.modify_default_list_old_kids_strengths(kid_id, old_strength_name, numb_pos)
			db_self.conn.commit()
			db_self.curs.execute("DROP TABLE IF EXISTS Old_Kids_Strengths_tmp")
			db_self.curs.execute("CREATE TABLE Old_Kids_Strengths_tmp AS SELECT * FROM Old_Kids_Strengths ORDER BY kid_id ASC")
			db_self.curs.execute("DROP TABLE Old_Kids_Strengths")
			db_self.curs.execute("ALTER TABLE Old_Kids_Strengths_tmp RENAME TO Old_Kids_Strengths")
			db_self.curs.execute('''CREATE INDEX IF NOT EXISTS old_kid_sy_index ON Old_Kids_Strengths (kid_id ASC)''')
			db_self.conn.commit()
		print("The Old_Kids_Strengths link is now added.")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")
