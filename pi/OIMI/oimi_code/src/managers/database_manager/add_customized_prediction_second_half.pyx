""" Oimi robot database_manager,
	code used for generating or change the whole DB infrastructure
	--> offline, before powering-on the robot
		e.g. maintenance / updates 
		e.g. kids info before starting execution of games 
	--> online, when robot is on 
		e.g. storing all useful data that the robot need to retrieve or update at runtime 
		e.g. adding kids info directly through mobile app
		e.g. creating games sessions
		e.g. storing games results / update kids focus_on 
		e.g. change game difficulty for next game according to results
	Notes:
		For other detailed info, look at dabatase_doc

	Created by Colombo Giacomo, Politecnico di Milano 2020 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: check=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/learning/game_classification/' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/learning/game_classification/')
import random
import pickle
import sqlite3 as lite
from typing import Optional
import create_session_with_prediction as cswp
cimport add_prediction_3 as adp3
cimport security_prediction as sp
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef add_new_Session_table_with_kid_case34(db_self,dict_table_2,is_mixed,the_game_kind,difficulty_mat,difficulty_gam,type_one_game,num_of_games_of_one_match,num_of_questions,given_sce):
	###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3
	if db_self.num_of_matches==3:
		fores2 = cswp.RanForest_clf_table_2(name="predict_trained_dataset#t2m2", received_data = db_self.csv_lines[3], num_outputs_prev = 3, weights_flag=0)
		fores2.prepare_array(dict_table_2)
		db_self.outputs_table_2 = fores2.predict_instance()

		which_match_is_custom = 0
		which_game_is_custom = 0

		if difficulty_mat=='':
			db_self.difficulty_mat_1 = db_self.outputs_table_2[0][0]
			db_self.difficulty_mat_2 = db_self.outputs_table_2[0][1]
			db_self.difficulty_mat_3 = db_self.outputs_table_2[0][2]
		else:
			pos_ma = random.randint(1,3)
			if db_self.level_area=='ma' or db_self.level_area=='ga':
				which_match_is_custom = pos_ma
			if pos_ma==1:
				db_self.difficulty_mat_1 = difficulty_mat
				db_self.difficulty_mat_2 = db_self.outputs_table_2[0][1]
				db_self.difficulty_mat_3 = db_self.outputs_table_2[0][2]
			if pos_ma==2:
				db_self.difficulty_mat_1 = db_self.outputs_table_2[0][0]
				db_self.difficulty_mat_2 = difficulty_mat
				db_self.difficulty_mat_3 = db_self.outputs_table_2[0][2]
			if pos_ma==3:
				db_self.difficulty_mat_1 = db_self.outputs_table_2[0][0]
				db_self.difficulty_mat_2 = db_self.outputs_table_2[0][1]
				db_self.difficulty_mat_3 = difficulty_mat

		db_self.order_quan_games = db_self.outputs_table_2[0][3]
		dict_table_33 = {
			'col1': [db_self.level_input],
			'col2': [db_self.complexity], #db_self.complexity
			'col3': [db_self.s2_restrictions_of_interest_input], #s1
			'col4': [db_self.s3_executing_long_tasks_input], #s2
			'col5': [db_self.s1_understanding_input], #s3
			'col6': [db_self.difficulty_mat_1], #m_1diff
			'col7': [db_self.difficulty_mat_2], #m_2diff
			'col8': [db_self.difficulty_mat_3], #m_3diff
			'col9': [db_self.order_quan_games], #m_ord_games
			'col10': [db_self.a1_range_self.num_of_matches_with_games_order_of_difficulty],
			'col11': [db_self.n1_focus_on_concentration_input],
			'col12': [db_self.n2_focus_on_resistance_input],
			'col13': [db_self.a2_max_game_match]}

		fores3 = start_forest_pred3_num3(db_self)

		fores3.prepare_array(dict_table_33)
		db_self.outputs_table_3 = fores3.predict_instance()

		db_self.order_quan_quest_m1 = db_self.outputs_table_3[0][0]
		db_self.order_diff_games_m1 = db_self.outputs_table_3[0][1]
		db_self.order_quan_quest_m2 = db_self.outputs_table_3[0][3]
		db_self.order_diff_games_m2 = db_self.outputs_table_3[0][4]
		db_self.order_quan_quest_m3 = db_self.outputs_table_3[0][6]
		db_self.order_diff_games_m3 = db_self.outputs_table_3[0][7]

		dict_table_431 = {
		'col1': [db_self.level_input], #level
		'col2': [db_self.difficulty_mat_1], 
		'col3': [db_self.s3_meltdown_input], 
		'col4': [db_self.s1_understanding_input], 
		'col5': [db_self.s3_distraction_in_listening_input],
		'col6': [db_self.order_quan_quest_m1], 
		'col7': [db_self.order_diff_games_m1], 
		'col8': [db_self.n1_focus_on_errors_input], 
		'col9': [db_self.n2_focus_on_automation_input], 
		'col10': [db_self.n1_focus_on_concentration_input], 
		'col11': [db_self.n4_focus_on_changes_input],
		'col12': [db_self.a1_max_question_game]}

		dict_table_432 = {
		'col1': [db_self.level_input], #level
		'col2': [db_self.difficulty_mat_2], 
		'col3': [db_self.s3_meltdown_input], 
		'col4': [db_self.s1_understanding_input], 
		'col5': [db_self.s3_distraction_in_listening_input],
		'col6': [db_self.order_quan_quest_m2], 
		'col7': [db_self.order_diff_games_m2], 
		'col8': [db_self.n1_focus_on_errors_input], 
		'col9': [db_self.n2_focus_on_automation_input], 
		'col10': [db_self.n1_focus_on_concentration_input], 
		'col11': [db_self.n4_focus_on_changes_input],
		'col12': [db_self.a1_max_question_game]}

		dict_table_433 = {
		'col1': [db_self.level_input], #level
		'col2': [db_self.difficulty_mat_3], 
		'col3': [db_self.s3_meltdown_input], 
		'col4': [db_self.s1_understanding_input], 
		'col5': [db_self.s3_distraction_in_listening_input],
		'col6': [db_self.order_quan_quest_m3], 
		'col7': [db_self.order_diff_games_m3], 
		'col8': [db_self.n1_focus_on_errors_input], 
		'col9': [db_self.n2_focus_on_automation_input], 
		'col10': [db_self.n1_focus_on_concentration_input], 
		'col11': [db_self.n4_focus_on_changes_input],
		'col12': [db_self.a1_max_question_game]}

		if num_of_games_of_one_match==0:
			db_self.games_m1 = int(db_self.outputs_table_3[0][2]) 
			db_self.games_m2 = int(db_self.outputs_table_3[0][5])
			db_self.games_m3 = int(db_self.outputs_table_3[0][8])
		else:
			if which_match_is_custom==0:
				pos_game = random.randint(1,3)
				if db_self.level_area=='ma' or db_self.level_area=='ga':
					which_match_is_custom=pos_game					  
				if pos_game==1:
					db_self.games_m1 = num_of_games_of_one_match
					db_self.games_m2 = int(db_self.outputs_table_3[0][5])
					db_self.games_m3 = int(db_self.outputs_table_3[0][8])
				if pos_game==2:
					db_self.games_m1 = int(db_self.outputs_table_3[0][2])
					db_self.games_m2 = num_of_games_of_one_match
					db_self.games_m3 = int(db_self.outputs_table_3[0][8])
				if pos_game==3:
					db_self.games_m1 = int(db_self.outputs_table_3[0][2])
					db_self.games_m2 = int(db_self.outputs_table_3[0][5])
					db_self.games_m3 = num_of_games_of_one_match
			if which_match_is_custom==1:
				db_self.games_m1 = num_of_games_of_one_match
				db_self.games_m2 = int(db_self.outputs_table_3[0][5])
				db_self.games_m3 = int(db_self.outputs_table_3[0][8])
			if which_match_is_custom==2:
				db_self.games_m1 = int(db_self.outputs_table_3[0][2])
				db_self.games_m2 = num_of_games_of_one_match
				db_self.games_m3 = int(db_self.outputs_table_3[0][8])
			if which_match_is_custom==3:
				db_self.games_m1 = int(db_self.outputs_table_3[0][2])
				db_self.games_m2 = int(db_self.outputs_table_3[0][5])
				db_self.games_m2 = num_of_games_of_one_match

		if db_self.games_m1==1: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g1", received_data = db_self.csv_lines[170], num_outputs_prev = 1, weights_flag=0)
		elif db_self.games_m1==2: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g2", received_data = db_self.csv_lines[171], num_outputs_prev = 2, weights_flag=0)
		elif db_self.games_m1==3: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g3", received_data = db_self.csv_lines[172], num_outputs_prev = 3, weights_flag=0)
		elif db_self.games_m1==4: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g4", received_data = db_self.csv_lines[173], num_outputs_prev = 4, weights_flag=0)
		fores4.prepare_array(dict_table_431)
		db_self.outputs_table_4_m1 = fores4.predict_instance()
		
		if db_self.games_m2==1: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g1", received_data = db_self.csv_lines[170], num_outputs_prev = 1, weights_flag=0)
		elif db_self.games_m2==2: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g2", received_data = db_self.csv_lines[171], num_outputs_prev = 2, weights_flag=0)
		elif db_self.games_m2==3: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g3", received_data = db_self.csv_lines[172], num_outputs_prev = 3, weights_flag=0)
		elif db_self.games_m2==4: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g4", received_data = db_self.csv_lines[173], num_outputs_prev = 4, weights_flag=0)
		fores4.prepare_array(dict_table_432)
		db_self.outputs_table_4_m2 = fores4.predict_instance()

		if db_self.games_m3==1: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m3g1", received_data = db_self.csv_lines[170], num_outputs_prev = 1, weights_flag=0)
		elif db_self.games_m3==2: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m3g2", received_data = db_self.csv_lines[171], num_outputs_prev = 2, weights_flag=0)
		elif db_self.games_m3==3: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m3g3", received_data = db_self.csv_lines[172], num_outputs_prev = 3, weights_flag=0)
		elif db_self.games_m3==4: 
			fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m3g3", received_data = db_self.csv_lines[173], num_outputs_prev = 4, weights_flag=0)
		fores4.prepare_array(dict_table_433)
		db_self.outputs_table_4_m3 = fores4.predict_instance()

		games_and_diffs_num3(db_self)

		if type_one_game!='':
			if which_match_is_custom==0:
				rand_mat = random.randint(1,4)
			else:
				rand_mat = which_match_is_custom
			if rand_mat==1:
				if db_self.games_m1==1:
					db_self.kind_m1_g1 = the_game_kind
				if db_self.games_m1==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.kind_m1_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m1_g2 = the_game_kind
				if db_self.games_m1==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m1_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m1_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m1_g3 = the_game_kind
				if db_self.games_m1==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m1_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m1_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m1_g3 = the_game_kind
					if ran_g==4:
						db_self.kind_m1_g4 = the_game_kind
			if rand_mat==2:
				if db_self.games_m2==1:
					db_self.kind_m2_g1 = the_game_kind
				if db_self.games_m2==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.kind_m2_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m2_g2 = the_game_kind
				if db_self.games_m2==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m2_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m2_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m2_g3 = the_game_kind
				if db_self.games_m2==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m2_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m2_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m2_g3 = the_game_kind
					if ran_g==4:
						db_self.kind_m2_g4 = the_game_kind							
			if rand_mat==3:
				if db_self.games_m3==1:
					db_self.kind_m3_g1 = the_game_kind
				if db_self.games_m3==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.kind_m3_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m3_g2 = the_game_kind
				if db_self.games_m3==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m3_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m3_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m3_g3 = the_game_kind
				if db_self.games_m3==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m3_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m3_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m3_g3 = the_game_kind
					if ran_g==4:
						db_self.kind_m3_g4 = the_game_kind						
			if db_self.level_area=='ga':
				which_game_is_custom=ran_g

		if difficulty_gam!='':
			if which_match_is_custom==0:
				rand_mat = random.randint(1,4)
			else:
				rand_mat = which_match_is_custom
			if rand_mat==1:
				if db_self.games_m1==1:
					db_self.diff_m1_g1 = difficulty_gam
				if db_self.games_m1==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.diff_m1_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m1_g2 = difficulty_gam
				if db_self.games_m1==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m1_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m1_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m1_g3 = difficulty_gam
				if db_self.games_m1==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m1_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m1_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m1_g3 = difficulty_gam
					if ran_g==4:
						db_self.diff_m1_g4 = difficulty_gam
			if rand_mat==2:
				if db_self.games_m2==1:
					db_self.diff_m2_g1 = difficulty_gam
				if db_self.games_m2==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.diff_m2_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m2_g2 = difficulty_gam
				if db_self.games_m2==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m2_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m2_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m2_g3 = difficulty_gam
				if db_self.games_m2==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m2_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m2_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m2_g3 = difficulty_gam
					if ran_g==4:
						db_self.diff_m2_g4 = difficulty_gam							
			if rand_mat==3:
				if db_self.games_m3==1:
					db_self.diff_m3_g1 = difficulty_gam
				if db_self.games_m3==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.diff_m3_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m3_g2 = difficulty_gam
				if db_self.games_m3==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m3_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m3_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m3_g3 = difficulty_gam
				if db_self.games_m3==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m3_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m3_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m3_g3 = difficulty_gam
					if ran_g==4:
						db_self.diff_m3_g4 = difficulty_gam						
			if db_self.level_area=='ga':
				which_game_is_custom=ran_g

		if db_self.games_m1==1: 
			db_self.m1_g1_order_diff_ques = db_self.outputs_table_4_m1[0][3]
			dict_table_511 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g1],
				'col3': [db_self.m1_g1_order_diff_ques], 
				'col4': [db_self.kind_m1_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 

			if num_of_questions==0:
				db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2]) #num_self.m1_questions_g1
			else:
				if which_match_is_custom==1:
					db_self.m1_questions_g1 = num_of_questions
				else:
					db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])				
			if is_mixed:
				db_self.m1_questions_g1 = 1
			if db_self.m1_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
			if db_self.m1_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
			if db_self.m1_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
			if db_self.m1_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
				db_self.type_m1_g1_4r = db_self.outputs_table_5_m1_g1[0][3]
		if db_self.games_m2==1:
			db_self.m2_g1_order_diff_ques = db_self.outputs_table_4_m2[0][3]
			dict_table_511 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g1], 
				'col3': [db_self.m2_g1_order_diff_ques], 
				'col4': [db_self.kind_m1_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			
			if num_of_questions==0:
				db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
			else:
				if which_match_is_custom==2:
					db_self.m2_questions_g1 = num_of_questions
				else:
					db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
			if is_mixed:
				db_self.m2_questions_g1 = 1
			if db_self.m2_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
			if db_self.m2_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
			if db_self.m2_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
			if db_self.m2_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
				db_self.type_m2_g1_4r = db_self.outputs_table_5_m2_g1[0][3]
		if db_self.games_m3==1: 
			db_self.m3_g1_order_diff_ques = db_self.outputs_table_4_m3[0][3]
			dict_table_511 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g1], 
				'col3': [db_self.m3_g1_order_diff_ques], 
				'col4': [db_self.kind_m1_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 

			if num_of_questions==0:
				db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
			else:
				if which_match_is_custom==2:
					db_self.m3_questions_g1 = num_of_questions
				else:
					db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
			if is_mixed:
				db_self.m3_questions_g1 = 1
			if db_self.m3_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
			if db_self.m3_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
			if db_self.m3_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
			if db_self.m3_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
				db_self.type_m3_g1_4r = db_self.outputs_table_5_m3_g1[0][3]
		if db_self.games_m1==2: 
			db_self.m1_g1_order_diff_ques = db_self.outputs_table_4_m1[0][3]
			db_self.m1_g2_order_diff_ques = db_self.outputs_table_4_m1[0][7]
			dict_table_521 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g1], 
				'col3': [db_self.m1_g1_order_diff_ques], 
				'col4': [db_self.kind_m1_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_522 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g2], 
				'col3': [db_self.m1_g2_order_diff_ques], 
				'col4': [db_self.kind_m1_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 

			if num_of_questions==0:
				db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
				db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m1_questions_g1 = num_of_questions
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
					else:
						db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = num_of_questions
				if which_match_is_custom==2:
					db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
					db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
				if which_match_is_custom==3:
					db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
					db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])				
			if is_mixed:
				db_self.m1_questions_g1 = 1
				db_self.m1_questions_g2 = 1
			if db_self.m1_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
			if db_self.m1_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
			if db_self.m1_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
			if db_self.m1_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
				db_self.type_m1_g1_4r = db_self.outputs_table_5_m1_g1[0][3]
			if db_self.m1_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
			if db_self.m1_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
			if db_self.m1_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
			if db_self.m1_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance() 
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
				db_self.type_m1_g2_4r = db_self.outputs_table_5_m1_g2[0][3]
		if db_self.games_m2==2: 
			db_self.m2_g1_order_diff_ques = db_self.outputs_table_4_m2[0][3]
			db_self.m2_g2_order_diff_ques = db_self.outputs_table_4_m2[0][7]
			dict_table_521 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g1], 
				'col3': [db_self.m2_g1_order_diff_ques], #g1_order_diff_ques
				'col4': [db_self.kind_m2_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}
			dict_table_522 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g2], 
				'col3': [db_self.m2_g2_order_diff_ques], #g1_order_diff_ques
				'col4': [db_self.kind_m2_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			
			if num_of_questions==0:
				db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
				db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m2_questions_g1 = num_of_questions
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
					else:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = num_of_questions
				if which_match_is_custom==2:
					db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
					db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
				if which_match_is_custom==3:
					db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
					db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
			if is_mixed:
				db_self.m2_questions_g1 = 1
				db_self.m2_questions_g2 = 1
			if db_self.m2_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
			if db_self.m2_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
			if db_self.m2_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
			if db_self.m2_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
				db_self.type_m2_g1_4r = db_self.outputs_table_5_m2_g1[0][3]
			if db_self.m2_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
			if db_self.m2_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
			if db_self.m2_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
			if db_self.m2_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
				db_self.type_m2_g2_4r = db_self.outputs_table_5_m2_g2[0][3]
		if db_self.games_m3==2:
			db_self.m3_g1_order_diff_ques = db_self.outputs_table_4_m3[0][3]
			db_self.m3_g2_order_diff_ques = db_self.outputs_table_4_m3[0][7]
			dict_table_521 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g1], 
				'col3': [db_self.m3_g1_order_diff_ques], 
				'col4': [db_self.kind_m3_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_522 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g2], 
				'col3': [db_self.m3_g2_order_diff_ques], 
				'col4': [db_self.kind_m3_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}					

			if num_of_questions==0:
				db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
				db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m3_questions_g1 = num_of_questions
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
					else:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = num_of_questions
				if which_match_is_custom==2:
					db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
					db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
				if which_match_is_custom==3:
					db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
					db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
			if is_mixed:
				db_self.m3_questions_g1 = 1
				db_self.m3_questions_g2 = 1
			if db_self.m3_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
			if db_self.m3_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
			if db_self.m3_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
			if db_self.m3_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
				db_self.type_m3_g1_4r = db_self.outputs_table_5_m3_g1[0][3]
			if db_self.m3_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_4r = db_self.outputs_table_5_m3_g2[0][0]
			if db_self.m3_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
			if db_self.m3_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
			if db_self.m3_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
				db_self.type_m3_g2_4r = db_self.outputs_table_5_m3_g2[0][3]
		if db_self.games_m1==3: 
			db_self.m1_g1_order_diff_ques = db_self.outputs_table_4_m1[0][3]
			db_self.m1_g2_order_diff_ques = db_self.outputs_table_4_m1[0][7]
			db_self.m1_g3_order_diff_ques = db_self.outputs_table_4_m1[0][11]
			dict_table_531 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g3], 
				'col3': [db_self.m1_g1_order_diff_ques], 
				'col4': [db_self.kind_m1_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_532 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g2],
				'col3': [db_self.m1_g2_order_diff_ques], 
				'col4': [db_self.kind_m1_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}
			dict_table_533 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g3], 
				'col3': [db_self.m1_g3_order_diff_ques], 
				'col4': [db_self.kind_m1_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			
			if num_of_questions==0:
				db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][2])
				db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
				db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][10])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m1_questions_g2 = num_of_questions
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
					if which_game_is_custom==2:
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = num_of_questions
						db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
					if which_game_is_custom==3:
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m1_questions_g3 = num_of_questions

				if which_match_is_custom==2 or which_match_is_custom==3:
					db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][2])
					db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
					db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
			if is_mixed:
				db_self.m1_questions_g1 = 1
				db_self.m1_questions_g2 = 1
				db_self.m1_questions_g3 = 1
			if db_self.m1_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
			if db_self.m1_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
			if db_self.m1_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
			if db_self.m1_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
				db_self.type_m1_g1_4r = db_self.outputs_table_5_m1_g1[0][3]
			if db_self.m1_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
			if db_self.m1_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
			if db_self.m1_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
			if db_self.m1_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
				db_self.type_m1_g2_4r = db_self.outputs_table_5_m1_g2[0][3]
			if db_self.m1_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
			if db_self.m1_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
			if db_self.m1_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
				type_m1_g3_3r = db_self.outputs_table_5_m1_g3[0][2]
			if db_self.m1_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
				type_m1_g3_3r = db_self.outputs_table_5_m1_g3[0][2]
				type_m1_g3_4r = db_self.outputs_table_5_m1_g3[0][3]
		if db_self.games_m2==3:
			db_self.m2_g1_order_diff_ques = db_self.outputs_table_4_m2[0][3]
			db_self.m2_g2_order_diff_ques = db_self.outputs_table_4_m2[0][7]
			db_self.m2_g3_order_diff_ques = db_self.outputs_table_4_m2[0][11]				  
			dict_table_531 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g1], 
				'col3': [db_self.m2_g1_order_diff_ques], 
				'col4': [db_self.kind_m2_g1],
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_532 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g1],
				'col3': [db_self.m2_g2_order_diff_ques], #g1_order_diff_ques
				'col4': [db_self.kind_m2_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}
			dict_table_533 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g3], 
				'col3': [db_self.m2_g3_order_diff_ques], #g1_order_diff_ques
				'col4': [db_self.kind_m2_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 

			if num_of_questions==0:
				db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
				db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
				db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
			else:
				if which_match_is_custom==2:
					if which_game_is_custom==1:
						db_self.m2_questions_g1 = num_of_questions
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
						db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
					if which_game_is_custom==2:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = num_of_questions
						db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
					if which_game_is_custom==3:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m2_questions_g3 = num_of_questions

				if which_match_is_custom==1 or which_match_is_custom==3:
					db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
					db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
					db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
			if is_mixed:
				db_self.m2_questions_g1 = 1
				db_self.m2_questions_g2 = 1
				db_self.m2_questions_g3 = 1
			if db_self.m2_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
			if db_self.m2_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
			if db_self.m2_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
			if db_self.m2_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
				db_self.type_m2_g1_4r = db_self.outputs_table_5_m2_g1[0][3]
			if db_self.m2_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
			if db_self.m2_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
			if db_self.m2_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
			if db_self.m2_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
				db_self.type_m2_g2_4r = db_self.outputs_table_5_m2_g2[0][3]
			if db_self.m2_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
			if db_self.m2_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
			if db_self.m2_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
				db_self.type_m2_g3_3r = db_self.outputs_table_5_m2_g3[0][2]
			if db_self.m2_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
				db_self.type_m2_g3_3r = db_self.outputs_table_5_m2_g3[0][2]
				db_self.type_m2_g3_4r = db_self.outputs_table_5_m2_g3[0][3]
		if db_self.games_m3==3:
			db_self.m3_g1_order_diff_ques = db_self.outputs_table_4_m3[0][3]
			db_self.m3_g2_order_diff_ques = db_self.outputs_table_4_m3[0][7]
			db_self.m3_g3_order_diff_ques = db_self.outputs_table_4_m3[0][11]
			dict_table_531 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g1], 
				'col3': [db_self.m3_g1_order_diff_ques], 
				'col4': [db_self.kind_m3_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_532 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g2], 
				'col3': [db_self.m3_g2_order_diff_ques], 
				'col4': [db_self.kind_m3_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}
			dict_table_533 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g3], 
				'col3': [db_self.m3_g3_order_diff_ques], 
				'col4': [db_self.kind_m3_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}								

			if num_of_questions==0:
				db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
				db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
				db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
			else:
				if which_match_is_custom==3:
					if which_game_is_custom==1:
						db_self.m3_questions_g1 = num_of_questions
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
						db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
					if which_game_is_custom==2:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = num_of_questions
						db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
					if which_game_is_custom==3:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
						db_self.m3_questions_g3 = num_of_questions

				if which_match_is_custom==1 or which_match_is_custom==2:
					db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
					db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
					db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
			if is_mixed:
				db_self.m3_questions_g1 = 1
				db_self.m3_questions_g2 = 1
				db_self.m3_questions_g3 = 1
			if db_self.m3_questions_g1==1:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
			if db_self.m3_questions_g1==2:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
			if db_self.m3_questions_g1==3:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
			if db_self.m3_questions_g1==4:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
				db_self.type_m3_g1_4r = db_self.outputs_table_5_m3_g1[0][3]
			if db_self.m3_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
			if db_self.m3_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
			if db_self.m3_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
			if db_self.m3_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
				db_self.type_m3_g2_4r = db_self.outputs_table_5_m3_g2[0][3]
			if db_self.m3_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g3[0][0]
			if db_self.m3_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g3[0][1]
			if db_self.m3_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g3[0][1]
				db_self.type_m3_g4_3r = db_self.outputs_table_5_m3_g3[0][2]
			if db_self.m3_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g3[0][1]
				db_self.type_m3_g4_3r = db_self.outputs_table_5_m3_g3[0][2]
				db_self.type_m3_g4_4r = db_self.outputs_table_5_m3_g3[0][3]
		if db_self.games_m1==4:
			db_self.m1_g1_order_diff_ques = db_self.outputs_table_4_m1[0][3]
			db_self.m1_g2_order_diff_ques = db_self.outputs_table_4_m1[0][7]
			db_self.m1_g3_order_diff_ques = db_self.outputs_table_4_m1[0][11]
			db_self.m1_g4_order_diff_ques = db_self.outputs_table_4_m1[0][15]
			dict_table_541 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g1],
				'col3': [db_self.m1_g1_order_diff_ques], 
				'col4': [db_self.kind_m1_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_542 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g2],
				'col3': [db_self.m1_g2_order_diff_ques], 
				'col4': [db_self.kind_m1_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}
			dict_table_543 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g3], 
				'col3': [db_self.m1_g3_order_diff_ques], 
				'col4': [db_self.kind_m1_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_544 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g4],
				'col3': [db_self.m1_g4_order_diff_ques], 
				'col4': [db_self.kind_m1_g4], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 

			if num_of_questions==0:
				db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
				db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
				db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
				db_self.m1_questions_g4 = int(db_self.outputs_table_4_m1[0][14])
			else:
				if which_match_is_custom==3:
					if which_game_is_custom==1:
						db_self.m1_questions_g1 = num_of_questions
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
						db_self.m1_questions_g4 = int(db_self.outputs_table_4_m1[0][14])
					if which_game_is_custom==2:
						db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = num_of_questions
						db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
						db_self.m1_questions_g4 = int(db_self.outputs_table_4_m1[0][14])
					if which_game_is_custom==3:
						db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m1_questions_g3 = num_of_questions
						db_self.m1_questions_g4 = int(db_self.outputs_table_4_m1[0][14])
					if which_game_is_custom==3:
						db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
						db_self.m1_questions_g4 = num_of_questions

				if which_match_is_custom==2 or which_match_is_custom==3:
					db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
					db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
					db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
					db_self.m1_questions_g4 = int(db_self.outputs_table_4_m1[0][14])
			if is_mixed:
				db_self.m1_questions_g1 = 1
				db_self.m1_questions_g2 = 1
				db_self.m1_questions_g3 = 1
				db_self.m1_questions_g4 = 1
			if db_self.m1_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
			if db_self.m1_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
			if db_self.m1_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
			if db_self.m1_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
				db_self.type_m1_g1_4r = db_self.outputs_table_5_m1_g1[0][3]
			if db_self.m1_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
			if db_self.m1_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
			if db_self.m1_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
			if db_self.m1_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
				db_self.type_m1_g2_4r = db_self.outputs_table_5_m1_g2[0][3]
			if db_self.m1_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
			if db_self.m1_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
			if db_self.m1_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
				type_m1_g3_3r = db_self.outputs_table_5_m1_g3[0][2]
			if db_self.m1_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
				type_m1_g3_3r = db_self.outputs_table_5_m1_g3[0][2]
				type_m1_g3_4r = db_self.outputs_table_5_m1_g3[0][3]
			if db_self.m1_questions_g4==1:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g4q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m1_g4 = fores5.predict_instance()
				db_self.type_m1_g4_1r = db_self.outputs_table_5_m1_g4[0][0]
			if db_self.m1_questions_g4==2:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g4q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m1_g4 = fores5.predict_instance()
				db_self.type_m1_g4_1r = db_self.outputs_table_5_m1_g4[0][0]
				db_self.type_m1_g4_2r = db_self.outputs_table_5_m1_g4[0][1]
			if db_self.m1_questions_g4==3:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g4q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m1_g4 = fores5.predict_instance()
				db_self.type_m1_g4_1r = db_self.outputs_table_5_m1_g4[0][0]
				db_self.type_m1_g4_2r = db_self.outputs_table_5_m1_g4[0][1]
				db_self.type_m1_g4_3r = db_self.outputs_table_5_m1_g4[0][2]
			if db_self.m1_questions_g4==4:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g4q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m1_g4 = fores5.predict_instance()
				db_self.type_m1_g4_1r = db_self.outputs_table_5_m1_g4[0][0]
				db_self.type_m1_g4_2r = db_self.outputs_table_5_m1_g4[0][1]
				db_self.type_m1_g4_3r = db_self.outputs_table_5_m1_g4[0][2]
				db_self.type_m1_g4_4r = db_self.outputs_table_5_m1_g4[0][3]
		if db_self.games_m2==4:
			db_self.m2_g1_order_diff_ques = db_self.outputs_table_4_m2[0][3]
			db_self.m2_g2_order_diff_ques = db_self.outputs_table_4_m2[0][7]
			db_self.m2_g3_order_diff_ques = db_self.outputs_table_4_m2[0][11]
			db_self.m2_g4_order_diff_ques = db_self.outputs_table_4_m2[0][15]				  
			dict_table_541 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g1], 
				'col3': [db_self.m2_g1_order_diff_ques], 
				'col4': [db_self.kind_m2_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_542 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g2], 
				'col3': [db_self.m2_g2_order_diff_ques], 
				'col4': [db_self.kind_m2_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}
			dict_table_543 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g3], 
				'col3': [db_self.m2_g3_order_diff_ques], 
				'col4': [db_self.kind_m2_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_544 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g3], 
				'col3': [db_self.m2_g4_order_diff_ques], 
				'col4': [db_self.kind_m2_g4], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
				db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
				db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
				db_self.m2_questions_g4 = int(db_self.outputs_table_4_m2[0][14])
			else:
				if which_match_is_custom==3:
					if which_game_is_custom==1:
						db_self.m2_questions_g1 = num_of_questions
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
						db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
						db_self.m2_questions_g4 = int(db_self.outputs_table_4_m2[0][14])
					if which_game_is_custom==2:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = num_of_questions
						db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
						db_self.m2_questions_g4 = int(db_self.outputs_table_4_m2[0][14])
					if which_game_is_custom==3:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
						db_self.m2_questions_g3 = num_of_questions
						db_self.m2_questions_g4 = int(db_self.outputs_table_4_m2[0][14])
					if which_game_is_custom==4:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
						db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
						db_self.m2_questions_g4 = num_of_questions

				if which_match_is_custom==1 or which_match_is_custom==3:
					db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
					db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
					db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
					db_self.m2_questions_g4 = int(db_self.outputs_table_4_m2[0][14])
			if is_mixed:
				db_self.m2_questions_g1 = 1
				db_self.m2_questions_g2 = 1
				db_self.m2_questions_g3 = 1
				db_self.m2_questions_g4 = 1
			if db_self.m2_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
			if db_self.m2_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
			if db_self.m2_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
			if db_self.m2_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
				db_self.type_m2_g1_4r = db_self.outputs_table_5_m2_g1[0][3]
			if db_self.m2_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
			if db_self.m2_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
			if db_self.m2_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
			if db_self.m2_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
				db_self.type_m2_g2_4r = db_self.outputs_table_5_m2_g2[0][3]
			if db_self.m2_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
			if db_self.m2_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
			if db_self.m2_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
				db_self.type_m2_g3_3r = db_self.outputs_table_5_m2_g3[0][2]
			if db_self.m2_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
				db_self.type_m2_g3_3r = db_self.outputs_table_5_m2_g3[0][2]
				db_self.type_m2_g3_4r = db_self.outputs_table_5_m2_g3[0][3]
			if db_self.m2_questions_g4==1:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m2_g4 = fores5.predict_instance()
				db_self.type_m2_g4_1r = db_self.outputs_table_5_m2_g4[0][0]
			if db_self.m2_questions_g4==2:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m2_g4 = fores5.predict_instance()
				db_self.type_m2_g4_1r = db_self.outputs_table_5_m2_g4[0][0]
				db_self.type_m2_g4_2r = db_self.outputs_table_5_m2_g4[0][1]
			if db_self.m2_questions_g4==3:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m2_g4 = fores5.predict_instance()
				db_self.type_m2_g4_1r = db_self.outputs_table_5_m2_g4[0][0]
				db_self.type_m2_g4_2r = db_self.outputs_table_5_m2_g4[0][1]
				db_self.type_m2_g4_3r = db_self.outputs_table_5_m2_g4[0][2]
			if db_self.m2_questions_g4==4:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m2_g4 = fores5.predict_instance()
				db_self.type_m2_g4_1r = db_self.outputs_table_5_m2_g4[0][0]
				db_self.type_m2_g4_2r = db_self.outputs_table_5_m2_g4[0][1]
				db_self.type_m2_g4_3r = db_self.outputs_table_5_m2_g4[0][2]
				db_self.type_m2_g4_4r = db_self.outputs_table_5_m2_g4[0][3]							
		if db_self.games_m3==4:
			db_self.m3_g1_order_diff_ques = db_self.outputs_table_4_m3[0][3]
			db_self.m3_g2_order_diff_ques = db_self.outputs_table_4_m3[0][7]
			db_self.m3_g3_order_diff_ques = db_self.outputs_table_4_m3[0][11]
			db_self.m3_g4_order_diff_ques = db_self.outputs_table_4_m3[0][15]
			
			dict_table_541 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g1], 
				'col3': [db_self.m3_g1_order_diff_ques], #g1_order_diff_ques
				'col4': [db_self.kind_m3_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_542 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g2], 
				'col3': [db_self.m3_g2_order_diff_ques], #g1_order_diff_ques
				'col4': [db_self.kind_m3_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}
			dict_table_543 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g3], 
				'col3': [db_self.m3_g3_order_diff_ques], #g1_order_diff_ques
				'col4': [db_self.kind_m3_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_544 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g4], 
				'col3': [db_self.m3_g4_order_diff_ques], #g1_order_diff_ques
				'col4': [db_self.kind_m3_g4], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			if num_of_questions==0:
				db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
				db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
				db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
				db_self.m3_questions_g4 = int(db_self.outputs_table_4_m3[0][14])
			else:
				if which_match_is_custom==3:
					if which_game_is_custom==1:
						db_self.m3_questions_g1 = num_of_questions
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
						db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
						db_self.m3_questions_g4 = int(db_self.outputs_table_4_m3[0][14])
					if which_game_is_custom==2:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = num_of_questions
						db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
						db_self.m3_questions_g4 = int(db_self.outputs_table_4_m3[0][14])
					if which_game_is_custom==3:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
						db_self.m3_questions_g3 = num_of_questions
						db_self.m3_questions_g4 = int(db_self.outputs_table_4_m3[0][14])
					if which_game_is_custom==4:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
						db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
						db_self.m3_questions_g4 = num_of_questions

				if which_match_is_custom==1 or which_match_is_custom==2:
					db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
					db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
					db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
					db_self.m3_questions_g4 = int(db_self.outputs_table_4_m3[0][14])
			if is_mixed:
				db_self.m3_questions_g1 = 1
				db_self.m3_questions_g2 = 1
				db_self.m3_questions_g3 = 1
				db_self.m3_questions_g4 = 1
			if db_self.m3_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
			if db_self.m3_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
			if db_self.m3_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
			if db_self.m3_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
				db_self.type_m3_g1_4r = db_self.outputs_table_5_m3_g1[0][3]
			if db_self.m3_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
			if db_self.m3_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
			if db_self.m3_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
			if db_self.m3_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
				db_self.type_m3_g2_4r = db_self.outputs_table_5_m3_g2[0][3]
			if db_self.m3_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g3[0][0]
			if db_self.m3_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g3[0][1]
			if db_self.m3_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g3[0][1]
				db_self.type_m3_g4_3r = db_self.outputs_table_5_m3_g3[0][2]
			if db_self.m3_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g3[0][1]
				db_self.type_m3_g4_3r = db_self.outputs_table_5_m3_g3[0][2]
				db_self.type_m3_g4_4r = db_self.outputs_table_5_m3_g3[0][3]
			if db_self.m3_questions_g4==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m3_g4 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g4[0][0]
			if db_self.m3_questions_g4==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m3_g4 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g4[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g4[0][1]
			if db_self.m3_questions_g4==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m3_g4 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g4[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g4[0][1]
				db_self.type_m3_g4_3r = db_self.outputs_table_5_m3_g4[0][2]
			if db_self.m3_questions_g4==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m3_g4 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g4[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g4[0][1]
				db_self.type_m3_g4_3r = db_self.outputs_table_5_m3_g4[0][2]
				db_self.type_m3_g4_4r = db_self.outputs_table_5_m3_g4[0][3]
	###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4###4
	elif db_self.num_of_matches==4:
		fores2 = cswp.RanForest_clf_table_2(name="predict_trained_dataset#t2m4", received_data = db_self.csv_lines[4], num_outputs_prev = 4, weights_flag=0)
		fores2.prepare_array(dict_table_2)
		db_self.outputs_table_2 = fores2.predict_instance()

		which_match_is_custom = 0
		which_game_is_custom = 0

		if difficulty_mat=='':
			db_self.difficulty_mat_1 = db_self.outputs_table_2[0][0]
			db_self.difficulty_mat_2 = db_self.outputs_table_2[0][1]
			db_self.difficulty_mat_3 = db_self.outputs_table_2[0][2]
			db_self.difficulty_mat_3 = db_self.outputs_table_2[0][3]
		else:
			pos_ma = random.randint(1,4)
			if db_self.level_area=='ma' or db_self.level_area=='ga':
				which_match_is_custom = pos_ma
			if pos_ma==1:
				db_self.difficulty_mat_1 = difficulty_mat
				db_self.difficulty_mat_2 = db_self.outputs_table_2[0][1]
				db_self.difficulty_mat_3 = db_self.outputs_table_2[0][2]
				db_self.difficulty_mat_4 = db_self.outputs_table_2[0][3]
			if pos_ma==2:
				db_self.difficulty_mat_1 = db_self.difficulty_mat_1
				db_self.difficulty_mat_2 = difficulty_mat
				db_self.difficulty_mat_3 = db_self.outputs_table_2[0][2]
				db_self.difficulty_mat_4 = db_self.outputs_table_2[0][3]
			if pos_ma==3:
				db_self.difficulty_mat_1 = db_self.difficulty_mat_1
				db_self.difficulty_mat_2 = db_self.outputs_table_2[0][1]
				db_self.difficulty_mat_3 = difficulty_mat
				db_self.difficulty_mat_4 = db_self.outputs_table_2[0][3]
			if pos_ma==4:
				db_self.difficulty_mat_1 = db_self.difficulty_mat_1
				db_self.difficulty_mat_2 = db_self.outputs_table_2[0][1]
				db_self.difficulty_mat_3 = db_self.outputs_table_2[0][2]
				db_self.difficulty_mat_4 = difficulty_mat

			db_self.order_quan_games = db_self.outputs_table_2[0][4]
		dict_table_34 = {
			'col1': [db_self.level_input],
			'col2': [db_self.complexity], #db_self.complexity
			'col3': [db_self.s2_restrictions_of_interest_input], #s1
			'col4': [db_self.s3_executing_long_tasks_input], #s2
			'col5': [db_self.s1_understanding_input], #s3
			'col6': [db_self.difficulty_mat_1], 
			'col7': [db_self.difficulty_mat_2], 
			'col8': [db_self.difficulty_mat_3], 
			'col9': [db_self.difficulty_mat_4], 
			'col10': [db_self.order_quan_games],
			'col11': [db_self.a1_range_self.num_of_matches_with_games_order_of_difficulty],
			'col12': [db_self.n1_focus_on_concentration_input],
			'col13': [db_self.n2_focus_on_resistance_input],
			'col14': [db_self.a2_max_game_match]}

		fores3 = start_forest_pred3_num4(db_self)
		
		fores3.prepare_array(dict_table_34)
		db_self.outputs_table_3 = fores3.predict_instance()

		db_self.order_quan_quest_m1 = db_self.outputs_table_3[0][0]
		db_self.order_diff_games_m1 = db_self.outputs_table_3[0][1]
		db_self.order_quan_quest_m2 = db_self.outputs_table_3[0][3]
		db_self.order_diff_games_m2 = db_self.outputs_table_3[0][4]
		db_self.order_quan_quest_m3 = db_self.outputs_table_3[0][6]
		db_self.order_diff_games_m3 = db_self.outputs_table_3[0][7]
		db_self.order_quan_quest_m4 = db_self.outputs_table_3[0][9]
		db_self.order_diff_games_m4 = db_self.outputs_table_3[0][10]

		dict_table_441 = {
			'col1': [db_self.level_input], 
			'col2': [db_self.difficulty_mat_1], 
			'col3': [db_self.s3_meltdown_input], 
			'col4': [db_self.s1_understanding_input], 
			'col5': [db_self.s3_distraction_in_listening_input],
			'col6': [db_self.order_quan_quest_m1], 
			'col7': [db_self.order_diff_games_m1], 
			'col8': [db_self.n1_focus_on_errors_input], 
			'col9': [db_self.n2_focus_on_automation_input], 
			'col10': [db_self.n1_focus_on_concentration_input], 
			'col11': [db_self.n4_focus_on_changes_input],
			'col12': [db_self.a1_max_question_game]}

		dict_table_442 = {
			'col1': [db_self.level_input], 
			'col2': [db_self.difficulty_mat_2], 
			'col3': [db_self.s3_meltdown_input], 
			'col4': [db_self.s1_understanding_input], 
			'col5': [db_self.s3_distraction_in_listening_input],
			'col6': [db_self.order_quan_quest_m2], 
			'col7': [db_self.order_diff_games_m2], 
			'col8': [db_self.n1_focus_on_errors_input], 
			'col9': [db_self.n2_focus_on_automation_input], 
			'col10': [db_self.n1_focus_on_concentration_input], 
			'col11': [db_self.n4_focus_on_changes_input],
			'col12': [db_self.a1_max_question_game]}

		dict_table_443 = {
			'col1': [db_self.level_input], 
			'col2': [db_self.difficulty_mat_3], 
			'col3': [db_self.s3_meltdown_input], 
			'col4': [db_self.s1_understanding_input], 
			'col5': [db_self.s3_distraction_in_listening_input],
			'col6': [db_self.order_quan_quest_m3], 
			'col7': [db_self.order_diff_games_m3], 
			'col8': [db_self.n1_focus_on_errors_input], 
			'col9': [db_self.n2_focus_on_automation_input], 
			'col10': [db_self.n1_focus_on_concentration_input], 
			'col11': [db_self.n4_focus_on_changes_input],
			'col12': [db_self.a1_max_question_game]}

		dict_table_444 = {
			'col1': [db_self.level_input], #level
			'col2': [db_self.difficulty_mat_4], 
			'col3': [db_self.s3_meltdown_input], 
			'col4': [db_self.s1_understanding_input], 
			'col5': [db_self.s3_distraction_in_listening_input],
			'col6': [db_self.order_quan_quest_m4], 
			'col7': [db_self.order_diff_games_m4], 
			'col8': [db_self.n1_focus_on_errors_input], 
			'col9': [db_self.n2_focus_on_automation_input], 
			'col10': [db_self.n1_focus_on_concentration_input], 
			'col11': [db_self.n4_focus_on_changes_input],
			'col12': [db_self.a1_max_question_game]}

		if num_of_games_of_one_match==0:
			db_self.games_m1 = int(db_self.outputs_table_3[0][2]) 
			db_self.games_m2 = int(db_self.outputs_table_3[0][5])
			db_self.games_m3 = int(db_self.outputs_table_3[0][8])
			db_self.games_m4 = int(db_self.outputs_table_3[0][11])
		else:
			if which_match_is_custom==0:
				pos_game = random.randint(1,4)
				if db_self.level_area=='ma' or db_self.level_area=='ga':
					which_match_is_custom=pos_game
				if pos_game==1:
					db_self.games_m1 = num_of_games_of_one_match
					db_self.games_m2 = int(db_self.outputs_table_3[0][5])
					db_self.games_m3 = int(db_self.outputs_table_3[0][8])
					db_self.games_m4 = int(db_self.outputs_table_3[0][11])
				if pos_game==2:
					db_self.games_m1 = int(db_self.outputs_table_3[0][2])
					db_self.games_m2 = num_of_games_of_one_match
					db_self.games_m3 = int(db_self.outputs_table_3[0][8])
					db_self.games_m4 = int(db_self.outputs_table_3[0][11])
				if pos_game==3:
					db_self.games_m1 = int(db_self.outputs_table_3[0][2])
					db_self.games_m2 = int(db_self.outputs_table_3[0][5])
					db_self.games_m3 = num_of_games_of_one_match
					db_self.games_m4 = int(db_self.outputs_table_3[0][11])
				if pos_game==4:
					db_self.games_m1 = int(db_self.outputs_table_3[0][2])
					db_self.games_m2 = int(db_self.outputs_table_3[0][5])
					db_self.games_m3 = int(db_self.outputs_table_3[0][8])
					db_self.games_m4 = num_of_games_of_one_match
			if which_match_is_custom==1:
				db_self.games_m1 = num_of_games_of_one_match
				db_self.games_m2 = int(db_self.outputs_table_3[0][5])
				db_self.games_m3 = int(db_self.outputs_table_3[0][8])
				db_self.games_m4 = int(db_self.outputs_table_3[0][8])
			if which_match_is_custom==2:
				db_self.games_m1 = int(db_self.outputs_table_3[0][2])
				db_self.games_m2 = num_of_games_of_one_match
				db_self.games_m3 = int(db_self.outputs_table_3[0][8])
				db_self.games_m4 = int(db_self.outputs_table_3[0][8])
			if which_match_is_custom==3:
				db_self.games_m1 = int(db_self.outputs_table_3[0][2])
				db_self.games_m2 = int(db_self.outputs_table_3[0][5])
				db_self.games_m3 = num_of_games_of_one_match
				db_self.games_m4 = int(db_self.outputs_table_3[0][5])

		if db_self.games_m1==1: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m1g1', received_data = db_self.csv_lines[170], num_outputs_prev = 1, weights_flag=0) 
		elif db_self.games_m1==2: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m1g2', received_data = db_self.csv_lines[171], num_outputs_prev = 2, weights_flag=0) 
		elif db_self.games_m1==3: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m1g3', received_data = db_self.csv_lines[172], num_outputs_prev = 3, weights_flag=0) 
		elif db_self.games_m1==4: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m1g4', received_data = db_self.csv_lines[173], num_outputs_prev = 4, weights_flag=0) 
		fores4.prepare_array(dict_table_441)
		db_self.outputs_table_4_m1 = fores4.predict_instance()
		
		if db_self.games_m2==1:
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m2g1', received_data = db_self.csv_lines[170], num_outputs_prev = 1, weights_flag=0) 
		elif db_self.games_m2==2:
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m2g2', received_data = db_self.csv_lines[171], num_outputs_prev = 2, weights_flag=0) 
		elif db_self.games_m2==3:
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m2g3', received_data = db_self.csv_lines[172], num_outputs_prev = 3, weights_flag=0) 
		elif db_self.games_m2==4:
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m2g4', received_data = db_self.csv_lines[173], num_outputs_prev = 4, weights_flag=0) 
		fores4.prepare_array(dict_table_442)
		db_self.outputs_table_4_m2 = fores4.predict_instance()

		if db_self.games_m3==1: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m3g1', received_data = db_self.csv_lines[170], num_outputs_prev = 1, weights_flag=0) 
		elif db_self.games_m3==2: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m3g2', received_data = db_self.csv_lines[171], num_outputs_prev = 2, weights_flag=0) 
		elif db_self.games_m3==3: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m3g3', received_data = db_self.csv_lines[172], num_outputs_prev = 3, weights_flag=0) 
		elif db_self.games_m3==4: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m3g4', received_data = db_self.csv_lines[173], num_outputs_prev = 4, weights_flag=0) 
		fores4.prepare_array(dict_table_443)
		db_self.outputs_table_4_m3 = fores4.predict_instance()

		if db_self.games_m4==1: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m4g1', received_data = db_self.csv_lines[170], num_outputs_prev = 1, weights_flag=0) 
		elif db_self.games_m4==2: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m4g2', received_data = db_self.csv_lines[171], num_outputs_prev = 2, weights_flag=0) 
		elif db_self.games_m4==3: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m4g3', received_data = db_self.csv_lines[172], num_outputs_prev = 3, weights_flag=0) 
		elif db_self.games_m4==4: 
			fores4 = cswp.RanForest_clf_table_4(name='predict_trained_dataset#t4m4g3', received_data = db_self.csv_lines[173], num_outputs_prev = 4, weights_flag=0) 
		fores4.prepare_array(dict_table_444)
		db_self.outputs_table_4_m4 = fores4.predict_instance()
		
		games_and_diffs_num4(db_self)

		if type_one_game!='':
			if which_match_is_custom==0:
				rand_mat = random.randint(1,4)
			else:
				rand_mat = which_match_is_custom
			if rand_mat==1:
				if db_self.games_m1==1:
					db_self.kind_m1_g1 = the_game_kind
				if db_self.games_m1==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.kind_m1_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m1_g2 = the_game_kind
				if db_self.games_m1==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m1_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m1_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m1_g3 = the_game_kind
				if db_self.games_m1==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m1_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m1_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m1_g3 = the_game_kind
					if ran_g==4:
						db_self.kind_m1_g4 = the_game_kind
			if rand_mat==2:
				if db_self.games_m2==1:
					db_self.kind_m2_g1 = the_game_kind
				if db_self.games_m2==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.kind_m2_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m2_g2 = the_game_kind
				if db_self.games_m2==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m2_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m2_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m2_g3 = the_game_kind
				if db_self.games_m2==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m2_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m2_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m2_g3 = the_game_kind
					if ran_g==4:
						db_self.kind_m2_g4 = the_game_kind							
			if rand_mat==3:
				if db_self.games_m3==1:
					db_self.kind_m3_g1 = the_game_kind
				if db_self.games_m3==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.kind_m3_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m3_g2 = the_game_kind
				if db_self.games_m3==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m3_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m3_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m3_g3 = the_game_kind
				if db_self.games_m3==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m3_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m3_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m3_g3 = the_game_kind
					if ran_g==4:
						db_self.kind_m3_g4 = the_game_kind						
			if rand_mat==4:
				if db_self.games_m4==1:
					db_self.kind_m4_g1 = the_game_kind
				if db_self.games_m4==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.kind_m4_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m4_g2 = the_game_kind
				if db_self.games_m4==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m4_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m4_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m4_g3 = the_game_kind
				if db_self.games_m4==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.kind_m4_g1 = the_game_kind
					if ran_g==2:
						db_self.kind_m4_g2 = the_game_kind
					if ran_g==3:
						db_self.kind_m4_g3 = the_game_kind
					if ran_g==4:
						db_self.kind_m4_g4 = the_game_kind
			if db_self.level_area=='ga':
				which_game_is_custom=ran_g

		if difficulty_gam!='':
			if which_match_is_custom==0:
				rand_mat = random.randint(1,4)
			else:
				rand_mat = which_match_is_custom
			if rand_mat==1:
				if db_self.games_m1==1:
					db_self.diff_m1_g1 = difficulty_gam
				if db_self.games_m1==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.diff_m1_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m1_g2 = difficulty_gam
				if db_self.games_m1==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m1_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m1_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m1_g3 = difficulty_gam
				if db_self.games_m1==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m1_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m1_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m1_g3 = difficulty_gam
					if ran_g==4:
						db_self.diff_m1_g4 = difficulty_gam
			if rand_mat==2:
				if db_self.games_m2==1:
					db_self.diff_m2_g1 = difficulty_gam
				if db_self.games_m2==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.diff_m2_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m2_g2 = difficulty_gam
				if db_self.games_m2==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m2_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m2_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m2_g3 = difficulty_gam
				if db_self.games_m2==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m2_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m2_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m2_g3 = difficulty_gam
					if ran_g==4:
						db_self.diff_m2_g4 = difficulty_gam							
			if rand_mat==3:
				if db_self.games_m3==1:
					db_self.diff_m3_g1 = difficulty_gam
				if db_self.games_m3==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.diff_m3_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m3_g2 = difficulty_gam
				if db_self.games_m3==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m3_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m3_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m3_g3 = difficulty_gam
				if db_self.games_m3==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m3_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m3_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m3_g3 = difficulty_gam
					if ran_g==4:
						db_self.diff_m3_g4 = difficulty_gam						
			if rand_mat==4:
				if db_self.games_m4==1:
					db_self.diff_m4_g1 = difficulty_gam
				if db_self.games_m4==2:
					if which_game_is_custom==0:
						ran_g = random.randint(1,2)
					else:
						ran_g = which_game_is_custom
					if ran_g==1:
						db_self.diff_m4_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m4_g2 = difficulty_gam
				if db_self.games_m4==3:
					if which_game_is_custom==0:
						ran_g = random.randint(1,3)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m4_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m4_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m4_g3 = difficulty_gam
				if db_self.games_m4==4:
					if which_game_is_custom==0:
						ran_g = random.randint(1,4)
					else:
						ran_g = which_game_is_custom								
					if ran_g==1:
						db_self.diff_m4_g1 = difficulty_gam
					if ran_g==2:
						db_self.diff_m4_g2 = difficulty_gam
					if ran_g==3:
						db_self.diff_m4_g3 = difficulty_gam
					if ran_g==4:
						db_self.diff_m4_g4 = difficulty_gam
			if db_self.level_area=='ga':
				which_game_is_custom=ran_g

		if db_self.games_m1==1: 
			db_self.m1_g1_order_diff_ques = db_self.outputs_table_4_m1[0][3]
			dict_table_511 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g1], #g1_diff
				'col3': [db_self.m1_g1_order_diff_ques], #g1_order_diff_ques
				'col4': [db_self.kind_m1_g1], #g1_kind
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				questions_g1 = int(db_self.outputs_table_4_m1[0][2])
			else:
				if which_match_is_custom==1:
					questions_g1 = num_of_questions
				else:
					questions_g1 = int(db_self.outputs_table_4_m1[0][2])
			if is_mixed:
				db_self.m1_questions_g1 = 1
			if questions_g1==1:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
			if questions_g1==2:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
			if questions_g1==3:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
			if questions_g1==4:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
				db_self.type_m1_g1_4r = db_self.outputs_table_5_m1_g1[0][3]   
		elif db_self.games_m1==2: 
			db_self.m1_g1_order_diff_ques = db_self.outputs_table_4_m1[0][3]
			db_self.m1_g2_order_diff_ques = db_self.outputs_table_4_m1[0][7]
			dict_table_521 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g1], 
				'col3': [db_self.m1_g1_order_diff_ques], 
				'col4': [db_self.kind_m1_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_522 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g2], 
				'col3': [db_self.m1_g2_order_diff_ques], 
				'col4': [db_self.kind_m1_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 

			if num_of_questions==0:
				db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
				db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m1_questions_g1 = num_of_questions
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
					else:
						db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = num_of_questions
				else:
					db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
					db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
			if is_mixed:
				db_self.m1_questions_g1 = 1
				db_self.m1_questions_g2 = 1
			if db_self.m1_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
			if db_self.m1_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
			if db_self.m1_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
			if db_self.m1_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
				db_self.type_m1_g1_4r = db_self.outputs_table_5_m1_g1[0][3]  
			if db_self.m1_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
			if db_self.m1_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
			if db_self.m1_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]				
			if db_self.m1_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
				db_self.type_m1_g2_4r = db_self.outputs_table_5_m1_g2[0][3]					
		elif db_self.games_m1==3: 
			db_self.m3_g1_order_diff_ques = db_self.outputs_table_4_m3[0][3]
			db_self.m3_g2_order_diff_ques = db_self.outputs_table_4_m3[0][7]
			db_self.m3_g3_order_diff_ques = db_self.outputs_table_4_m3[0][11]				  
			dict_table_531 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g1], 
				'col3': [db_self.m3_g1_order_diff_ques], 
				'col4': [db_self.kind_m1_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_532 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g2], 
				'col3': [db_self.m3_g2_order_diff_ques], 
				'col4': [db_self.kind_m1_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_533 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g3], 
				'col3': [db_self.m3_g3_order_diff_ques], 
				'col4': [db_self.kind_m1_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
				db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
				db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m1_questions_g1 = num_of_questions
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
					if which_game_is_custom==2:
						db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = num_of_questions
						db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
					if which_game_is_custom==3:
						db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m1_questions_g3 = num_of_questions
				else:
					db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
					db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
					db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
			if is_mixed:
				db_self.m1_questions_g1 = 1
				db_self.m1_questions_g2 = 1
				db_self.m1_questions_g3 = 1
			if db_self.m1_questions_g1==1:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
			if db_self.m1_questions_g1==2:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
			if db_self.m1_questions_g1==3:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
			if db_self.m1_questions_g1==4:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
				db_self.type_m1_g1_4r = db_self.outputs_table_5_m1_g1[0][3]
			if db_self.m1_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
			if db_self.m1_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
			if db_self.m1_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
			if db_self.m1_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
				db_self.type_m1_g2_4r = db_self.outputs_table_5_m1_g2[0][3]
			if db_self.m1_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
			if db_self.m1_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
			if db_self.m1_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
				type_m1_g3_3r = db_self.outputs_table_5_m1_g3[0][2]
			if db_self.m1_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
				type_m1_g3_3r = db_self.outputs_table_5_m1_g3[0][2]
				type_m1_g3_4r = db_self.outputs_table_5_m1_g3[0][3]
		elif db_self.games_m1==4: 
			db_self.m1_g1_order_diff_ques = db_self.outputs_table_4_m1[0][3]
			db_self.m1_g2_order_diff_ques = db_self.outputs_table_4_m1[0][7]
			db_self.m1_g3_order_diff_ques = db_self.outputs_table_4_m1[0][11]
			db_self.m1_g4_order_diff_ques = db_self.outputs_table_4_m1[0][15]				  
			dict_table_541 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g1], 
				'col3': [db_self.m1_g1_order_diff_ques], 
				'col4': [db_self.kind_m1_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_542 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g2], 
				'col3': [db_self.m1_g2_order_diff_ques], 
				'col4': [db_self.kind_m1_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_543 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m1_g3], 
				'col3': [db_self.m1_g3_order_diff_ques], 
				'col4': [db_self.kind_m1_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_544 = {
				'col1': [db_self.level_input], 
				'col3': [db_self.diff_m1_g4], 
				'col2': [db_self.m1_g4_order_diff_ques], 
				'col4': [db_self.kind_m1_g4], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 

			if num_of_questions==0:
				db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
				db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
				db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
				db_self.m1_questions_g4 = int(db_self.outputs_table_4_m1[0][14])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m1_questions_g1 = num_of_questions
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
						db_self.m1_questions_g4 = int(db_self.outputs_table_4_m1[0][14])
					if which_game_is_custom==2:
						db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = num_of_questions
						db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
						db_self.m1_questions_g4 = int(db_self.outputs_table_4_m1[0][14])
					if which_game_is_custom==3:
						db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m1_questions_g3 = num_of_questions
						db_self.m1_questions_g4 = int(db_self.outputs_table_4_m1[0][14])
					if which_game_is_custom==4:
						db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
						db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
						db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
						db_self.m1_questions_g4 = num_of_questions
				else:
					db_self.m1_questions_g1 = int(db_self.outputs_table_4_m1[0][2])
					db_self.m1_questions_g2 = int(db_self.outputs_table_4_m1[0][6])
					db_self.m1_questions_g3 = int(db_self.outputs_table_4_m1[0][10])
					db_self.m1_questions_g4 = int(db_self.outputs_table_4_m1[0][14])
			if is_mixed:
				db_self.m1_questions_g1 = 1
				db_self.m1_questions_g2 = 1
				db_self.m1_questions_g3 = 1
				db_self.m1_questions_g4 = 1
			if db_self.m1_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
			if db_self.m1_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
			if db_self.m1_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
			if db_self.m1_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m1_g1 = fores5.predict_instance()
				db_self.type_m1_g1_1r = db_self.outputs_table_5_m1_g1[0][0]
				db_self.type_m1_g1_2r = db_self.outputs_table_5_m1_g1[0][1]
				db_self.type_m1_g1_3r = db_self.outputs_table_5_m1_g1[0][2]
				db_self.type_m1_g1_4r = db_self.outputs_table_5_m1_g1[0][3]
			if db_self.m1_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
			if db_self.m1_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
			if db_self.m1_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
			if db_self.m1_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m1_g2 = fores5.predict_instance()
				db_self.type_m1_g2_1r = db_self.outputs_table_5_m1_g2[0][0]
				type_m1_g2_2r = db_self.outputs_table_5_m1_g2[0][1]
				db_self.type_m1_g2_3r = db_self.outputs_table_5_m1_g2[0][2]
				db_self.type_m1_g2_4r = db_self.outputs_table_5_m1_g2[0][3]
			if db_self.m1_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
			if db_self.m1_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
			if db_self.m1_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
				type_m1_g3_3r = db_self.outputs_table_5_m1_g3[0][2]
			if db_self.m1_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m1_g3 = fores5.predict_instance()
				db_self.type_m1_g3_1r = db_self.outputs_table_5_m1_g3[0][0]
				type_m1_g3_2r = db_self.outputs_table_5_m1_g3[0][1]
				type_m1_g3_3r = db_self.outputs_table_5_m1_g3[0][2]
				type_m1_g3_4r = db_self.outputs_table_5_m1_g3[0][3]
			if db_self.m1_questions_g4==1:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m1_g4 = fores5.predict_instance()
				db_self.type_m1_g4_1r = db_self.outputs_table_5_m1_g4[0][0]
			if db_self.m1_questions_g4==2:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m1_g4 = fores5.predict_instance()
				db_self.type_m1_g4_1r = db_self.outputs_table_5_m1_g4[0][0]
				db_self.type_m1_g4_2r = db_self.outputs_table_5_m1_g4[0][1]
			if db_self.m1_questions_g4==3:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m1_g4 = fores5.predict_instance()
				db_self.type_m1_g4_1r = db_self.outputs_table_5_m1_g4[0][0]
				db_self.type_m1_g4_2r = db_self.outputs_table_5_m1_g4[0][1]
				db_self.type_m1_g4_3r = db_self.outputs_table_5_m1_g4[0][2]
			if db_self.m1_questions_g4==4:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m1_g4 = fores5.predict_instance()		  
				db_self.type_m1_g4_1r = db_self.outputs_table_5_m1_g4[0][0]
				db_self.type_m1_g4_2r = db_self.outputs_table_5_m1_g4[0][1]
				db_self.type_m1_g4_3r = db_self.outputs_table_5_m1_g4[0][2]
				db_self.type_m1_g4_4r = db_self.outputs_table_5_m1_g4[0][3]

		if db_self.games_m2==1:
			db_self.m2_g1_order_diff_ques = db_self.outputs_table_4_m2[0][3]
			dict_table_511 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g1], 
				'col3': [db_self.m2_g1_order_diff_ques], 
				'col4': [db_self.kind_m2_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
			else:
				if which_match_is_custom==1:
					db_self.m2_questions_g1 = num_of_questions
				else:
					db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
			if is_mixed:
				db_self.m2_questions_g1 = 1
			if db_self.m2_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
			if db_self.m2_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
			if db_self.m2_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
			if db_self.m2_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
				db_self.type_m2_g1_4r = db_self.outputs_table_5_m2_g1[0][3]					
		elif db_self.games_m2==2:
			db_self.m2_g1_order_diff_ques = db_self.outputs_table_4_m2[0][3]
			db_self.m2_g2_order_diff_ques = db_self.outputs_table_4_m2[0][7]
			dict_table_521 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g1], 
				'col3': [db_self.m2_g1_order_diff_ques], 
				'col4': [db_self.kind_m2_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_522 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g2], 
				'col3': [db_self.m2_g2_order_diff_ques], 
				'col4': [db_self.kind_m2_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
				db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m2_questions_g1 = num_of_questions
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
					else:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = num_of_questions
				else:
					db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
					db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
			if is_mixed:
				db_self.m2_questions_g1 = 1
				db_self.m2_questions_g2 = 1
			if db_self.m2_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
			if db_self.m2_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
			if db_self.m2_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()					
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
			if db_self.m2_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
				db_self.type_m2_g1_4r = db_self.outputs_table_5_m2_g1[0][3]
			if db_self.m2_questions_g2==1:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
			if db_self.m2_questions_g2==2:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
			if db_self.m2_questions_g2==3:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
			if db_self.m2_questions_g2==4:
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
				db_self.type_m2_g2_4r = db_self.outputs_table_5_m2_g2[0][3]
		elif db_self.games_m2==3:
			db_self.m2_g1_order_diff_ques = db_self.outputs_table_4_m2[0][3]
			db_self.m2_g2_order_diff_ques = db_self.outputs_table_4_m2[0][7]
			db_self.m2_g3_order_diff_ques = db_self.outputs_table_4_m2[0][11]				  
			dict_table_531 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g1], 
				'col3': [db_self.m2_g1_order_diff_ques], 
				'col4': [db_self.kind_m2_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_532 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g2], 
				'col3': [db_self.m2_g2_order_diff_ques], 
				'col4': [db_self.kind_m2_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_533 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g3], 
				'col3': [db_self.m2_g3_order_diff_ques],
				'col4': [db_self.kind_m2_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
				db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
				db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m2_questions_g1 = num_of_questions
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
						db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
					if which_game_is_custom==2:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = num_of_questions
						db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
					if which_game_is_custom==3:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
						db_self.m2_questions_g3 = num_of_questions
				else:
					db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
					db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
					db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
			if is_mixed:
				db_self.m2_questions_g1 = 1
				db_self.m2_questions_g2 = 1
				db_self.m2_questions_g3 = 1
			if db_self.m2_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
			if db_self.m2_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
			if db_self.m2_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
			if db_self.m2_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
				db_self.type_m2_g1_4r = db_self.outputs_table_5_m2_g1[0][3]
			if db_self.m2_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
			if db_self.m2_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
			if db_self.m2_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
			if db_self.m2_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
				db_self.type_m2_g2_4r = db_self.outputs_table_5_m2_g2[0][3]

			if db_self.m2_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
			if db_self.m2_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
			if db_self.m2_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
				db_self.type_m2_g3_3r = db_self.outputs_table_5_m2_g3[0][2]
			if db_self.m2_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
				db_self.type_m2_g3_3r = db_self.outputs_table_5_m2_g3[0][2]
				db_self.type_m2_g3_4r = db_self.outputs_table_5_m2_g3[0][3]
		elif db_self.games_m2==4:
			db_self.m2_g1_order_diff_ques = db_self.outputs_table_4_m2[0][3]
			db_self.m2_g2_order_diff_ques = db_self.outputs_table_4_m2[0][7]
			db_self.m2_g3_order_diff_ques = db_self.outputs_table_4_m2[0][11]
			db_self.m2_g4_order_diff_ques = db_self.outputs_table_4_m2[0][15]				  
			dict_table_541 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g1], 
				'col3': [db_self.m2_g1_order_diff_ques], 
				'col4': [db_self.kind_m2_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}			
			dict_table_542 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g2], 
				'col3': [db_self.m2_g2_order_diff_ques], 
				'col4': [db_self.kind_m2_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_543 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g3], 
				'col3': [db_self.m2_g3_order_diff_ques],
				'col4': [db_self.kind_m2_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_544 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m2_g4], 
				'col3': [db_self.m2_g4_order_diff_ques], 
				'col4': [db_self.kind_m2_g4], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 

			if num_of_questions==0:
				db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
				db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
				db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
				db_self.m2_questions_g4 = int(db_self.outputs_table_4_m2[0][14])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m2_questions_g1 = num_of_questions
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
						db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
						db_self.m2_questions_g4 = int(db_self.outputs_table_4_m2[0][14])
					if which_game_is_custom==2:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = num_of_questions
						db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
						db_self.m2_questions_g4 = int(db_self.outputs_table_4_m2[0][14])
					if which_game_is_custom==3:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
						db_self.m2_questions_g3 = num_of_questions
						db_self.m2_questions_g4 = int(db_self.outputs_table_4_m2[0][14])
					if which_game_is_custom==4:
						db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
						db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
						db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
						db_self.m2_questions_g4 = num_of_questions
				else:
					db_self.m2_questions_g1 = int(db_self.outputs_table_4_m2[0][2])
					db_self.m2_questions_g2 = int(db_self.outputs_table_4_m2[0][6])
					db_self.m2_questions_g3 = int(db_self.outputs_table_4_m2[0][10])
					db_self.m2_questions_g4 = int(db_self.outputs_table_4_m2[0][14])
			if is_mixed:
				db_self.m2_questions_g1 = 1
				db_self.m2_questions_g2 = 1
				db_self.m2_questions_g3 = 1
				db_self.m2_questions_g4 = 1
			if db_self.m2_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
			if db_self.m2_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
			if db_self.m2_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
			if db_self.m2_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m2_g1 = fores5.predict_instance()
				db_self.type_m2_g1_1r = db_self.outputs_table_5_m2_g1[0][0]
				db_self.type_m2_g1_2r = db_self.outputs_table_5_m2_g1[0][1]
				db_self.type_m2_g1_3r = db_self.outputs_table_5_m2_g1[0][2]
				db_self.type_m2_g1_4r = db_self.outputs_table_5_m2_g1[0][3]
			if db_self.m2_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
			if db_self.m2_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
			if db_self.m2_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
			if db_self.m2_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m2_g2 = fores5.predict_instance()
				db_self.type_m2_g2_1r = db_self.outputs_table_5_m2_g2[0][0]
				db_self.type_m2_g2_2r = db_self.outputs_table_5_m2_g2[0][1]
				db_self.type_m2_g2_3r = db_self.outputs_table_5_m2_g2[0][2]
				db_self.type_m2_g2_4r = db_self.outputs_table_5_m2_g2[0][3]

			if db_self.m2_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
			if db_self.m2_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
			if db_self.m2_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
				db_self.type_m2_g3_3r = db_self.outputs_table_5_m2_g3[0][2]
			if db_self.m2_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m2_g3 = fores5.predict_instance()
				db_self.type_m2_g3_1r = db_self.outputs_table_5_m2_g3[0][0]
				db_self.type_m2_g3_2r = db_self.outputs_table_5_m2_g3[0][1]
				db_self.type_m2_g3_3r = db_self.outputs_table_5_m2_g3[0][2]
				db_self.type_m2_g3_4r = db_self.outputs_table_5_m2_g3[0][3]

			if db_self.m2_questions_g4==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m2_g4 = fores5.predict_instance()
				db_self.type_m2_g4_1r = db_self.outputs_table_5_m2_g4[0][0]
			if db_self.m2_questions_g4==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m2_g4 = fores5.predict_instance()
				db_self.type_m2_g4_1r = db_self.outputs_table_5_m2_g4[0][0]
				db_self.type_m2_g4_2r = db_self.outputs_table_5_m2_g4[0][1]
			if db_self.m2_questions_g4==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m2_g4 = fores5.predict_instance()
				db_self.type_m2_g4_1r = db_self.outputs_table_5_m2_g4[0][0]
				db_self.type_m2_g4_2r = db_self.outputs_table_5_m2_g4[0][1]
				db_self.type_m2_g4_3r = db_self.outputs_table_5_m2_g4[0][2]
			if db_self.m2_questions_g4==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m2_g4 = fores5.predict_instance()
				db_self.type_m2_g4_1r = db_self.outputs_table_5_m2_g4[0][0]
				db_self.type_m2_g4_2r = db_self.outputs_table_5_m2_g4[0][1]
				db_self.type_m2_g4_3r = db_self.outputs_table_5_m2_g4[0][2]
				db_self.type_m2_g4_4r = db_self.outputs_table_5_m2_g4[0][3]

		if db_self.games_m3==1:
			db_self.m3_g1_order_diff_ques = db_self.outputs_table_4_m3[0][3]
			dict_table_511 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g1], 
				'col3': [db_self.m3_g1_order_diff_ques],
				'col4': [db_self.kind_m3_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
			else:
				if which_match_is_custom==1:
					db_self.m3_questions_g1 = num_of_questions
				else:
					db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
			if is_mixed:
				db_self.m3_questions_g1 = 1
			if db_self.m3_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
			if db_self.m3_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
			if db_self.m3_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
			if db_self.m3_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
				db_self.type_m3_g1_4r = db_self.outputs_table_5_m3_g1[0][3]
		elif db_self.games_m3==2:
			db_self.m3_g1_order_diff_ques = db_self.outputs_table_4_m4[0][3]
			db_self.m3_g2_order_diff_ques = db_self.outputs_table_4_m4[0][7]
			dict_table_521 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g1], 
				'col3': [db_self.m3_g1_order_diff_ques], 
				'col4': [db_self.kind_m3_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_522 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g2], 
				'col3': [db_self.m3_g2_order_diff_ques], 
				'col4': [db_self.kind_m3_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
				db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m3_questions_g1 = num_of_questions
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
					else:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = num_of_questions
				else:
					db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
					db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
			if is_mixed:
				db_self.m3_questions_g1 = 1
				db_self.m3_questions_g2 = 1
			if db_self.m3_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
			if db_self.m3_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
			if db_self.m3_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
			if db_self.m3_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
				db_self.type_m3_g1_4r = db_self.outputs_table_5_m3_g1[0][3]
			if db_self.m3_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
			if db_self.m3_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
			if db_self.m3_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
			if db_self.m3_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
				db_self.type_m3_g2_4r = db_self.outputs_table_5_m3_g2[0][3]
		elif db_self.games_m3==3:
			db_self.m3_g1_order_diff_ques = db_self.outputs_table_4_m3[0][3]
			db_self.m3_g2_order_diff_ques = db_self.outputs_table_4_m3[0][7]
			db_self.m3_g3_order_diff_ques = db_self.outputs_table_4_m3[0][11]				  
			dict_table_531 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g1], 
				'col3': [db_self.m3_g1_order_diff_ques], 
				'col4': [db_self.kind_m3_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}
			dict_table_532 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g2], 
				'col3': [db_self.m3_g2_order_diff_ques], 
				'col4': [db_self.kind_m3_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}					
			dict_table_533 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g3], 
				'col3': [db_self.m3_g3_order_diff_ques],
				'col4': [db_self.kind_m3_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
				db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
				db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m3_questions_g1 = num_of_questions
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
						db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
					if which_game_is_custom==2:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = num_of_questions
						db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
					if which_game_is_custom==3:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
						db_self.m3_questions_g3 = num_of_questions
				else:
					db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
					db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
					db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
			if is_mixed:
				db_self.m3_questions_g1 = 1
				db_self.m3_questions_g2 = 1
				db_self.m3_questions_g3 = 1
			if db_self.m3_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
			if db_self.m3_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
			if db_self.m3_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
			if db_self.m3_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
				db_self.type_m3_g1_4r = db_self.outputs_table_5_m3_g1[0][3]
				
			if db_self.m3_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
			if db_self.m3_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
			if db_self.m3_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
			if db_self.m3_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
				db_self.type_m3_g2_4r = db_self.outputs_table_5_m3_g2[0][3]

			if db_self.m3_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g3_1r = db_self.outputs_table_5_m3_g3[0][0]
			if db_self.m3_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g3_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g3_2r = db_self.outputs_table_5_m3_g3[0][1]
			if db_self.m3_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g3_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g3_2r = db_self.outputs_table_5_m3_g3[0][1]
				db_self.type_m3_g3_3r = db_self.outputs_table_5_m3_g3[0][2]
			if db_self.m3_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g3_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g3_2r = db_self.outputs_table_5_m3_g3[0][1]
				db_self.type_m3_g3_3r = db_self.outputs_table_5_m3_g3[0][2]
				db_self.type_m3_g3_4r = db_self.outputs_table_5_m3_g3[0][3]
		elif db_self.games_m3==4:
			db_self.m3_g1_order_diff_ques = db_self.outputs_table_4_m3[0][3]
			db_self.m3_g2_order_diff_ques = db_self.outputs_table_4_m3[0][7]
			db_self.m3_g3_order_diff_ques = db_self.outputs_table_4_m3[0][11]
			db_self.m3_g4_order_diff_ques = db_self.outputs_table_4_m3[0][15]
			dict_table_541 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g1], 
				'col3': [db_self.m3_g1_order_diff_ques], 
				'col4': [db_self.kind_m3_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}
			dict_table_542 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g2], 
				'col3': [db_self.m3_g2_order_diff_ques], 
				'col4': [db_self.kind_m3_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}					
			dict_table_543 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m3_g3], 
				'col3': [db_self.m3_g3_order_diff_ques], 
				'col4': [db_self.kind_m4_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_544 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g4], 
				'col3': [db_self.m3_g4_order_diff_ques], 
				'col4': [db_self.kind_m4_g4], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
				db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
				db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
				db_self.m3_questions_g4 = int(db_self.outputs_table_4_m3[0][14])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m3_questions_g1 = num_of_questions
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
						db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
						db_self.m3_questions_g4 = int(db_self.outputs_table_4_m3[0][14])
					if which_game_is_custom==2:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = num_of_questions
						db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
						db_self.m3_questions_g4 = int(db_self.outputs_table_4_m3[0][14])
					if which_game_is_custom==3:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
						db_self.m3_questions_g3 = num_of_questions
						db_self.m3_questions_g4 = int(db_self.outputs_table_4_m3[0][14])
					if which_game_is_custom==4:
						db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
						db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
						db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
						db_self.m3_questions_g4 = num_of_questions
				else:
					db_self.m3_questions_g1 = int(db_self.outputs_table_4_m3[0][2])
					db_self.m3_questions_g2 = int(db_self.outputs_table_4_m3[0][6])
					db_self.m3_questions_g3 = int(db_self.outputs_table_4_m3[0][10])
					db_self.m3_questions_g4 = int(db_self.outputs_table_4_m3[0][14])
			if is_mixed:
				db_self.m3_questions_g1 = 1
				db_self.m3_questions_g2 = 1
				db_self.m3_questions_g3 = 1
				db_self.m3_questions_g4 = 1
			if db_self.m3_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
			if db_self.m3_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
			if db_self.m3_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
			if db_self.m3_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m3_g1 = fores5.predict_instance()
				db_self.type_m3_g1_1r = db_self.outputs_table_5_m3_g1[0][0]
				db_self.type_m3_g1_2r = db_self.outputs_table_5_m3_g1[0][1]
				db_self.type_m3_g1_3r = db_self.outputs_table_5_m3_g1[0][2]
				db_self.type_m3_g1_4r = db_self.outputs_table_5_m3_g1[0][3]

			if db_self.m3_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
			if db_self.m3_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
			if db_self.m3_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
			if db_self.m3_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m3_g2 = fores5.predict_instance()
				db_self.type_m3_g2_1r = db_self.outputs_table_5_m3_g2[0][0]
				db_self.type_m3_g2_2r = db_self.outputs_table_5_m3_g2[0][1]
				db_self.type_m3_g2_3r = db_self.outputs_table_5_m3_g2[0][2]
				db_self.type_m3_g2_4r = db_self.outputs_table_5_m3_g2[0][3]

			if db_self.m3_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g3_1r = db_self.outputs_table_5_m3_g3[0][0]
			if db_self.m3_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g3_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g3_2r = db_self.outputs_table_5_m3_g3[0][1]
			if db_self.m3_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g3_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g3_2r = db_self.outputs_table_5_m3_g3[0][1]
				db_self.type_m3_g3_3r = db_self.outputs_table_5_m3_g3[0][2]
			if db_self.m3_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m3_g3 = fores5.predict_instance()
				db_self.type_m3_g3_1r = db_self.outputs_table_5_m3_g3[0][0]
				db_self.type_m3_g3_2r = db_self.outputs_table_5_m3_g3[0][1]
				db_self.type_m3_g3_3r = db_self.outputs_table_5_m3_g3[0][2]
				db_self.type_m3_g3_4r = db_self.outputs_table_5_m3_g3[0][3]

			if db_self.m3_questions_g4==1: #num_of_questions g4
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m3_g4 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g4[0][0]
			if db_self.m3_questions_g4==2: #num_of_questions g4
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m3_g4 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g4[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g4[0][1]
			if db_self.m3_questions_g4==3: #num_of_questions g4
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m3_g4 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g4[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g4[0][1]
				db_self.type_m3_g4_3r = db_self.outputs_table_5_m3_g4[0][2]
			if db_self.m3_questions_g4==4: #num_of_questions g4
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m3g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m3_g4 = fores5.predict_instance()
				db_self.type_m3_g4_1r = db_self.outputs_table_5_m3_g4[0][0]
				db_self.type_m3_g4_2r = db_self.outputs_table_5_m3_g4[0][1]
				db_self.type_m3_g4_3r = db_self.outputs_table_5_m3_g4[0][2]
				db_self.type_m3_g4_4r = db_self.outputs_table_5_m3_g4[0][3]

		if db_self.games_m4==1: 
			db_self.m4_g1_order_diff_ques = db_self.outputs_table_4_m4[0][3]
			dict_table_511 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g1], 
				'col3': [db_self.m4_g1_order_diff_ques], 
				'col4': [db_self.kind_m4_g1],
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
			else:
				if which_match_is_custom==1:
					db_self.m4_questions_g1 = num_of_questions
				else:
					db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
			if is_mixed:
				db_self.m4_questions_g1 = 1
			if db_self.m4_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				type_m4_g1_r = db_self.outputs_table_5_m4_g1[0][0]
			if db_self.m4_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
			if db_self.m4_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
				db_self.type_m4_g1_3r = db_self.outputs_table_5_m4_g1[0][2]
			if db_self.m4_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_511)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
				db_self.type_m4_g1_3r = db_self.outputs_table_5_m4_g1[0][2]
				db_self.type_m4_g1_4r = db_self.outputs_table_5_m4_g1[0][3]
		elif db_self.games_m4==2:
			db_self.m4_g1_order_diff_ques = db_self.outputs_table_4_m4[0][3]
			db_self.m4_g2_order_diff_ques = db_self.outputs_table_4_m4[0][7]
			dict_table_521 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g1], 
				'col3': [db_self.m4_g1_order_diff_ques], 
				'col4': [db_self.kind_m4_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_522 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g2], 
				'col3': [db_self.m4_g2_order_diff_ques], 
				'col4': [db_self.kind_m4_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
				db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m4_questions_g1 = num_of_questions
						db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
					else:
						db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
						db_self.m4_questions_g2 = num_of_questions
				else:
					db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
					db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
			if is_mixed:
				db_self.m4_questions_g1 = 1
				db_self.m4_questions_g2 = 1
			if db_self.m4_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
			if db_self.m4_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
			if db_self.m4_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
				db_self.type_m4_g1_3r = db_self.outputs_table_5_m4_g1[0][3]
			if db_self.m4_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_521)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
				db_self.type_m4_g1_3r = db_self.outputs_table_5_m4_g1[0][3]
				db_self.type_m4_g1_4r = db_self.outputs_table_5_m4_g1[0][1]

			if db_self.m4_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
			if db_self.m4_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
				db_self.type_m4_g2_2r = db_self.outputs_table_5_m4_g2[0][1]
			if db_self.m4_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
				db_self.type_m4_g2_2r = db_self.outputs_table_5_m4_g2[0][1]
				db_self.type_m4_g2_3r = db_self.outputs_table_5_m4_g2[0][3]
			if db_self.m4_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_522)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
				db_self.type_m4_g2_2r = db_self.outputs_table_5_m4_g2[0][1]
				db_self.type_m4_g2_3r = db_self.outputs_table_5_m4_g2[0][3]
				db_self.type_m4_g2_4r = db_self.outputs_table_5_m4_g2[0][1]
		elif db_self.games_m4==3:
			db_self.m4_g1_order_diff_ques = db_self.outputs_table_4_m4[0][3]
			db_self.m4_g2_order_diff_ques = db_self.outputs_table_4_m4[0][7]
			db_self.m4_g3_order_diff_ques = db_self.outputs_table_4_m4[0][11]
			dict_table_531 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g1], 
				'col3': [db_self.m4_g1_order_diff_ques],
				'col4': [db_self.kind_m4_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}				
			dict_table_532 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g2], 
				'col3': [db_self.m4_g2_order_diff_ques], 
				'col4': [db_self.kind_m4_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_533 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g3], 
				'col3': [db_self.m4_g3_order_diff_ques], 
				'col4': [db_self.kind_m4_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]}

			if num_of_questions==0:
				db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
				db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
				db_self.m4_questions_g3 = int(db_self.outputs_table_4_m4[0][10])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m4_questions_g1 = num_of_questions
						db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
						db_self.m4_questions_g3 = int(db_self.outputs_table_4_m4[0][10])
					if which_game_is_custom==2:
						db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
						db_self.m4_questions_g2 = num_of_questions
						db_self.m4_questions_g3 = int(db_self.outputs_table_4_m4[0][10])
					if which_game_is_custom==3:
						db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
						db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
						db_self.m4_questions_g3 = num_of_questions
				else:
					db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
					db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
					db_self.m4_questions_g3 = int(db_self.outputs_table_4_m4[0][10])
			if is_mixed:
				db_self.m4_questions_g1 = 1
				db_self.m4_questions_g2 = 1
				db_self.m4_questions_g3 = 1
			if db_self.m4_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
			if db_self.m4_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
			if db_self.m4_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
				db_self.type_m4_g1_3r = db_self.outputs_table_5_m4_g1[0][3]
			if db_self.m4_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_531)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
				db_self.type_m4_g1_3r = db_self.outputs_table_5_m4_g1[0][3]
				db_self.type_m4_g1_4r = db_self.outputs_table_5_m4_g1[0][1]

			if db_self.m4_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
			if db_self.m4_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
				db_self.type_m4_g2_2r = db_self.outputs_table_5_m4_g2[0][1]
			if db_self.m4_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
				db_self.type_m4_g2_2r = db_self.outputs_table_5_m4_g2[0][1]
				db_self.type_m4_g2_3r = db_self.outputs_table_5_m4_g2[0][3]
			if db_self.m4_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_532)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
				db_self.type_m4_g2_2r = db_self.outputs_table_5_m4_g2[0][1]
				db_self.type_m4_g2_3r = db_self.outputs_table_5_m4_g2[0][3]
				db_self.type_m4_g2_4r = db_self.outputs_table_5_m4_g2[0][1]
			if db_self.m4_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m4_g3 = fores5.predict_instance()
				db_self.type_m4_g3_1r = db_self.outputs_table_5_m4_g3[0][0]
			if db_self.m4_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m4_g3 = fores5.predict_instance()
				db_self.type_m4_g3_1r = db_self.outputs_table_5_m4_g3[0][0]
				db_self.type_m4_g3_2r = db_self.outputs_table_5_m4_g3[0][1]
			if db_self.m4_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m4_g3 = fores5.predict_instance()
				db_self.type_m4_g3_1r = db_self.outputs_table_5_m4_g3[0][0]
				db_self.type_m4_g3_2r = db_self.outputs_table_5_m4_g3[0][1]
				db_self.type_m4_g3_3r = db_self.outputs_table_5_m4_g3[0][2]
			if db_self.m4_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_533)
				db_self.outputs_table_5_m4_g3 = fores5.predict_instance()												
				db_self.type_m4_g3_1r = db_self.outputs_table_5_m4_g3[0][0]
				db_self.type_m4_g3_2r = db_self.outputs_table_5_m4_g3[0][1]
				db_self.type_m4_g3_3r = db_self.outputs_table_5_m4_g3[0][3]
				db_self.type_m4_g3_4r = db_self.outputs_table_5_m4_g3[0][1]
		elif db_self.games_m4==4:
			db_self.m4_g1_order_diff_ques = db_self.outputs_table_4_m4[0][3]
			db_self.m4_g2_order_diff_ques = db_self.outputs_table_4_m4[0][7]
			db_self.m4_g3_order_diff_ques = db_self.outputs_table_4_m4[0][11]
			db_self.m4_g4_order_diff_ques = db_self.outputs_table_4_m4[0][15]
			dict_table_541 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g1], 
				'col3': [db_self.m4_g1_order_diff_ques],
				'col4': [db_self.kind_m4_g1], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_542 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g2], 
				'col3': [db_self.m4_g2_order_diff_ques],
				'col4': [db_self.kind_m4_g2], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_543 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g3], 
				'col3': [db_self.m4_g3_order_diff_ques],
				'col4': [db_self.kind_m4_g3], 
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 
			dict_table_544 = {
				'col1': [db_self.level_input], 
				'col2': [db_self.diff_m4_g4], 
				'col3': [db_self.m4_g4_order_diff_ques],
				'col4': [db_self.kind_m4_g4],
				'col5': [db_self.s3_distraction_in_listening_input],
				'col6': [db_self.s1_understanding_input],
				'col7': [db_self.s3_memoria_deficit_input],
				'col8': [db_self.n1_focus_on_comprehension_input],
				'col9': [db_self.n2_focus_on_automation_input]} 

			if num_of_questions==0:
				db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
				db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
				db_self.m4_questions_g3 = int(db_self.outputs_table_4_m4[0][10])
				db_self.m4_questions_g4 = int(db_self.outputs_table_4_m4[0][14])
			else:
				if which_match_is_custom==1:
					if which_game_is_custom==1:
						db_self.m4_questions_g1 = num_of_questions
						db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
						db_self.m4_questions_g3 = int(db_self.outputs_table_4_m4[0][10])
						db_self.m4_questions_g4 = int(db_self.outputs_table_4_m4[0][14])
					if which_game_is_custom==2:
						db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
						db_self.m4_questions_g2 = num_of_questions
						db_self.m4_questions_g3 = int(db_self.outputs_table_4_m4[0][10])
						db_self.m4_questions_g4 = int(db_self.outputs_table_4_m4[0][14])
					if which_game_is_custom==3:
						db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
						db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
						db_self.m4_questions_g3 = num_of_questions
						db_self.m4_questions_g4 = int(db_self.outputs_table_4_m4[0][14])
					if which_game_is_custom==4:
						db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
						db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
						db_self.m4_questions_g3 = int(db_self.outputs_table_4_m4[0][10])
						db_self.m4_questions_g4 = num_of_questions
				else:
					db_self.m4_questions_g1 = int(db_self.outputs_table_4_m4[0][2])
					db_self.m4_questions_g2 = int(db_self.outputs_table_4_m4[0][6])
					db_self.m4_questions_g3 = int(db_self.outputs_table_4_m4[0][10])
					db_self.m4_questions_g4 = int(db_self.outputs_table_4_m4[0][14])
			if is_mixed:
				db_self.m4_questions_g1 = 1
				db_self.m4_questions_g2 = 1
				db_self.m4_questions_g3 = 1
				db_self.m4_questions_g4 = 1
			if db_self.m4_questions_g1==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
			if db_self.m4_questions_g1==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
			if db_self.m4_questions_g1==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
				db_self.type_m4_g1_3r = db_self.outputs_table_5_m4_g1[0][2]
			if db_self.m4_questions_g1==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g1q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_541)
				db_self.outputs_table_5_m4_g1 = fores5.predict_instance()
				db_self.type_m4_g1_1r = db_self.outputs_table_5_m4_g1[0][0]
				db_self.type_m4_g1_2r = db_self.outputs_table_5_m4_g1[0][1]
				db_self.type_m4_g1_3r = db_self.outputs_table_5_m4_g1[0][2]
				db_self.type_m4_g1_4r = db_self.outputs_table_5_m4_g1[0][3]

			if db_self.m4_questions_g2==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
			if db_self.m4_questions_g2==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
				db_self.type_m4_g2_2r = db_self.outputs_table_5_m4_g2[0][1]
			if db_self.m4_questions_g2==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
				db_self.type_m4_g2_2r = db_self.outputs_table_5_m4_g2[0][1]
				db_self.type_m4_g2_3r = db_self.outputs_table_5_m4_g2[0][2]
			if db_self.m4_questions_g2==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g2q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_542)
				db_self.outputs_table_5_m4_g2 = fores5.predict_instance()
				db_self.type_m4_g2_1r = db_self.outputs_table_5_m4_g2[0][0]
				db_self.type_m4_g2_2r = db_self.outputs_table_5_m4_g2[0][1]
				db_self.type_m4_g2_3r = db_self.outputs_table_5_m4_g2[0][2]
				db_self.type_m4_g2_4r = db_self.outputs_table_5_m4_g2[0][3]
			if db_self.m4_questions_g3==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g3q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m4_g3 = fores5.predict_instance()
				db_self.type_m4_g3_1r = db_self.outputs_table_5_m4_g3[0][0]
			if db_self.m4_questions_g3==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g3q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m4_g3 = fores5.predict_instance()
				db_self.type_m4_g3_1r = db_self.outputs_table_5_m4_g3[0][0]
				db_self.type_m4_g3_2r = db_self.outputs_table_5_m4_g3[0][1]
			if db_self.m4_questions_g3==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g3q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m4_g3 = fores5.predict_instance()
				db_self.type_m4_g3_1r = db_self.outputs_table_5_m4_g3[0][0]
				db_self.type_m4_g3_2r = db_self.outputs_table_5_m4_g3[0][1]
				db_self.type_m4_g3_3r = db_self.outputs_table_5_m4_g3[0][2]
			if db_self.m4_questions_g3==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g3q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_543)
				db_self.outputs_table_5_m4_g3 = fores5.predict_instance()
				db_self.type_m4_g3_1r = db_self.outputs_table_5_m4_g3[0][0]
				db_self.type_m4_g3_2r = db_self.outputs_table_5_m4_g3[0][1]
				db_self.type_m4_g3_3r = db_self.outputs_table_5_m4_g3[0][2]
				db_self.type_m4_g3_4r = db_self.outputs_table_5_m4_g3[0][3]
			if db_self.m4_questions_g4==1: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g4q1", received_data = db_self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m4_g4 = fores5.predict_instance()
				db_self.type_m4_g4_1r = db_self.outputs_table_5_m4_g4[0][0]
			if db_self.m4_questions_g4==2: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g4q2", received_data = db_self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m4_g4 = fores5.predict_instance()
				db_self.type_m4_g4_1r = db_self.outputs_table_5_m4_g4[0][0]
				db_self.type_m4_g4_2r = db_self.outputs_table_5_m4_g4[0][1]
			if db_self.m4_questions_g4==3: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g4q3", received_data = db_self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m4_g4 = fores5.predict_instance()
				db_self.type_m4_g4_1r = db_self.outputs_table_5_m4_g4[0][0]
				db_self.type_m4_g4_2r = db_self.outputs_table_5_m4_g4[0][1]
				db_self.type_m4_g4_3r = db_self.outputs_table_5_m4_g4[0][2]					
			if db_self.m4_questions_g4==4: 
				fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m4g4q4", received_data = db_self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
				fores5.prepare_array(dict_table_544)
				db_self.outputs_table_5_m4_g4 = fores5.predict_instance()										
				db_self.type_m4_g4_1r = db_self.outputs_table_5_m4_g4[0][0]
				db_self.type_m4_g4_2r = db_self.outputs_table_5_m4_g4[0][1]
				db_self.type_m4_g4_3r = db_self.outputs_table_5_m4_g4[0][2]
				db_self.type_m4_g4_4r = db_self.outputs_table_5_m4_g4[0][3]

	adp3.conclude_final_session_creation(db_self)

cdef start_forest_pred3_num3(db_self):
	if db_self.complexity=='Easy':
		if db_self.difficulty_mat_3=='Easy_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3ee1", received_data = db_self.csv_lines[10], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Easy_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3ee2", received_data = db_self.csv_lines[11], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Normal_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3en1", received_data = db_self.csv_lines[12], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Normal_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3en2", received_data = db_self.csv_lines[13], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Medium_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3em1", received_data = db_self.csv_lines[14], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Medium_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3em2", received_data = db_self.csv_lines[15], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Hard_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3eh1", received_data = db_self.csv_lines[16], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Hard_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3eh2", received_data = db_self.csv_lines[17], num_outputs_prev = 3, weights_flag=0)
	if db_self.complexity=='Normal':
		if db_self.difficulty_mat_3=='Easy_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3ne1", received_data = db_self.csv_lines[18], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Easy_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3ne2", received_data = db_self.csv_lines[19], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Normal_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3nn1", received_data = db_self.csv_lines[20], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Normal_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3nn2", received_data = db_self.csv_lines[21], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Medium_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3nm1", received_data = db_self.csv_lines[22], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Medium_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3nm2", received_data = db_self.csv_lines[23], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Hard_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3nh1", received_data = db_self.csv_lines[24], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Hard_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3nh2", received_data = db_self.csv_lines[25], num_outputs_prev = 3, weights_flag=0)
	if db_self.complexity=='Medium':
		if db_self.difficulty_mat_3=='Easy_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3me1", received_data = db_self.csv_lines[26], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Easy_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3me2", received_data = db_self.csv_lines[27], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Normal_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3mn1", received_data = db_self.csv_lines[28], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Normal_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3mn2", received_data = db_self.csv_lines[29], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Medium_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3mm1", received_data = db_self.csv_lines[30], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Medium_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3mm2", received_data = db_self.csv_lines[31], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Hard_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3mh1", received_data = db_self.csv_lines[32], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Hard_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3mh2", received_data = db_self.csv_lines[33], num_outputs_prev = 3, weights_flag=0)
	if db_self.complexity=='Hard':
		if db_self.difficulty_mat_3=='Easy_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3he1", received_data = db_self.csv_lines[34], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Easy_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3he2", received_data = db_self.csv_lines[35], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Normal_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3hn1", received_data = db_self.csv_lines[36], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Normal_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3hn2", received_data = db_self.csv_lines[37], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Medium_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3hm1", received_data = db_self.csv_lines[38], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Medium_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3hm2", received_data = db_self.csv_lines[39], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Hard_1':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3hh1", received_data = db_self.csv_lines[40], num_outputs_prev = 3, weights_flag=0)
		if db_self.difficulty_mat_3=='Hard_2':
			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m3hh2", received_data = db_self.csv_lines[41], num_outputs_prev = 3, weights_flag=0)

	return fores3

cdef start_forest_pred3_num4(db_self):
	if db_self.level_input=='beginner':
		if db_self.complexity=='Easy':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bea', received_data = db_self.csv_lines[8], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#beb', received_data = db_self.csv_lines[9], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bec', received_data = db_self.csv_lines[10], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bed', received_data = db_self.csv_lines[11], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bee', received_data = db_self.csv_lines[12], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bef', received_data = db_self.csv_lines[13], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#beg', received_data = db_self.csv_lines[14], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#beh', received_data = db_self.csv_lines[15], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Normal':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bna', received_data = db_self.csv_lines[16], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bnb', received_data = db_self.csv_lines[17], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bnc', received_data = db_self.csv_lines[18], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bnd', received_data = db_self.csv_lines[19], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bne', received_data = db_self.csv_lines[20], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bnf', received_data = db_self.csv_lines[21], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bng', received_data = db_self.csv_lines[22], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bnh', received_data = db_self.csv_lines[23], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Medium':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bma', received_data = db_self.csv_lines[24], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bmb', received_data = db_self.csv_lines[25], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bmc', received_data = db_self.csv_lines[26], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bmd', received_data = db_self.csv_lines[27], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bme', received_data = db_self.csv_lines[28], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bmf', received_data = db_self.csv_lines[29], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bmg', received_data = db_self.csv_lines[30], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bmh', received_data = db_self.csv_lines[31], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Hard':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bha', received_data = db_self.csv_lines[32], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bhb', received_data = db_self.csv_lines[33], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bhc', received_data = db_self.csv_lines[34], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bhd', received_data = db_self.csv_lines[35], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bhe', received_data = db_self.csv_lines[36], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bhf', received_data = db_self.csv_lines[37], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bhg', received_data = db_self.csv_lines[38], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#bhh', received_data = db_self.csv_lines[39], num_outputs_prev = 4, weights_flag=0) 
	if db_self.level_input=='elementary':
		if db_self.complexity=='Easy':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eea', received_data = db_self.csv_lines[40], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eeb', received_data = db_self.csv_lines[41], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eec', received_data = db_self.csv_lines[42], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eed', received_data = db_self.csv_lines[43], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eee', received_data = db_self.csv_lines[44], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eef', received_data = db_self.csv_lines[45], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eeg', received_data = db_self.csv_lines[46], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eeh', received_data = db_self.csv_lines[47], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Normal':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ena', received_data = db_self.csv_lines[48], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#enb', received_data = db_self.csv_lines[49], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#enc', received_data = db_self.csv_lines[50], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#end', received_data = db_self.csv_lines[51], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ene', received_data = db_self.csv_lines[52], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#enf', received_data = db_self.csv_lines[53], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eng', received_data = db_self.csv_lines[54], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#enh', received_data = db_self.csv_lines[55], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Medium':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ema', received_data = db_self.csv_lines[56], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#emb', received_data = db_self.csv_lines[57], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#emc', received_data = db_self.csv_lines[58], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#emd', received_data = db_self.csv_lines[59], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eme', received_data = db_self.csv_lines[60], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#emf', received_data = db_self.csv_lines[61], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#emg', received_data = db_self.csv_lines[62], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#emh', received_data = db_self.csv_lines[63], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Hard':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#eha', received_data = db_self.csv_lines[64], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ehb', received_data = db_self.csv_lines[65], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ehc', received_data = db_self.csv_lines[66], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ehd', received_data = db_self.csv_lines[67], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ehe', received_data = db_self.csv_lines[68], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ehf', received_data = db_self.csv_lines[69], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ehg', received_data = db_self.csv_lines[70], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ehh', received_data = db_self.csv_lines[71], num_outputs_prev = 4, weights_flag=0) 
	if db_self.level_input=='intermediate':
		if db_self.complexity=='Easy':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#iea', received_data = db_self.csv_lines[72], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ieb', received_data = db_self.csv_lines[73], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#iec', received_data = db_self.csv_lines[74], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ied', received_data = db_self.csv_lines[75], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#iee', received_data = db_self.csv_lines[76], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ief', received_data = db_self.csv_lines[77], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ieg', received_data = db_self.csv_lines[78], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ieh', received_data = db_self.csv_lines[79], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Normal':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ina', received_data = db_self.csv_lines[80], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#inb', received_data = db_self.csv_lines[81], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#inc', received_data = db_self.csv_lines[82], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ind', received_data = db_self.csv_lines[83], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ine', received_data = db_self.csv_lines[84], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#inf', received_data = db_self.csv_lines[85], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ing', received_data = db_self.csv_lines[86], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#inh', received_data = db_self.csv_lines[87], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Medium':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ima', received_data = db_self.csv_lines[88], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#imb', received_data = db_self.csv_lines[89], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#imc', received_data = db_self.csv_lines[90], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#imd', received_data = db_self.csv_lines[91], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ime', received_data = db_self.csv_lines[92], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#imf', received_data = db_self.csv_lines[93], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#img', received_data = db_self.csv_lines[94], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#imh', received_data = db_self.csv_lines[95], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Hard':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#iha', received_data = db_self.csv_lines[96], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ihb', received_data = db_self.csv_lines[97], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ihc', received_data = db_self.csv_lines[98], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ihd', received_data = db_self.csv_lines[99], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ihe', received_data = db_self.csv_lines[100], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ihf', received_data = db_self.csv_lines[101], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ihg', received_data = db_self.csv_lines[102], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ihh', received_data = db_self.csv_lines[103], num_outputs_prev = 4, weights_flag=0) 
	if db_self.level_input=='advanced':
		if db_self.complexity=='Easy':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#aea', received_data = db_self.csv_lines[104], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#aeb', received_data = db_self.csv_lines[105], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#aec', received_data = db_self.csv_lines[106], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#aed', received_data = db_self.csv_lines[107], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#aee', received_data = db_self.csv_lines[108], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#aef', received_data = db_self.csv_lines[109], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#aeg', received_data = db_self.csv_lines[110], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#aeh', received_data = db_self.csv_lines[111], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Normal':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ana', received_data = db_self.csv_lines[112], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#anb', received_data = db_self.csv_lines[113], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#anc', received_data = db_self.csv_lines[114], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#and', received_data = db_self.csv_lines[115], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ane', received_data = db_self.csv_lines[116], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#anf', received_data = db_self.csv_lines[117], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ang', received_data = db_self.csv_lines[118], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#anh', received_data = db_self.csv_lines[119], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Medium':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ama', received_data = db_self.csv_lines[120], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#amb', received_data = db_self.csv_lines[121], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#amc', received_data = db_self.csv_lines[122], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#amd', received_data = db_self.csv_lines[123], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ame', received_data = db_self.csv_lines[124], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#amf', received_data = db_self.csv_lines[125], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#amg', received_data = db_self.csv_lines[126], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#amh', received_data = db_self.csv_lines[127], num_outputs_prev = 4, weights_flag=0) 
		if db_self.complexity=='Hard':
			if db_self.difficulty_mat_1=='Easy_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#aha', received_data = db_self.csv_lines[128], num_outputs_prev = 4, weights_flag=0)
			if db_self.difficulty_mat_1=='Easy_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ahb', received_data = db_self.csv_lines[129], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ahc', received_data = db_self.csv_lines[130], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Hard_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ahd', received_data = db_self.csv_lines[131], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ahe', received_data = db_self.csv_lines[132], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Medium_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ahf', received_data = db_self.csv_lines[133], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_1':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ahg', received_data = db_self.csv_lines[134], num_outputs_prev = 4, weights_flag=0) 
			if db_self.difficulty_mat_1=='Normal_2':
				fores3 = cswp.RanForest_clf_table_3(name='predict_trained_dataset#t3m4#ahh', received_data = db_self.csv_lines[135], num_outputs_prev = 4, weights_flag=0)

		return fores3

cdef games_and_diffs_num3(db_self):
	if db_self.games_m1==1:
		db_self.kind_m1_g1 = int(db_self.outputs_table_4_m1[0][1])
		db_self.diff_m1_g1 = int(db_self.outputs_table_4_m1[0][0])
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m1==2:
		db_self.kind_m1_g1 = int(db_self.outputs_table_4_m1[0][1])
		db_self.kind_m1_g2 = int(db_self.outputs_table_4_m1[0][5])
		db_self.diff_m1_g1 = int(db_self.outputs_table_4_m1[0][0])
		db_self.diff_m1_g2 = int(db_self.outputs_table_4_m1[0][4])					   
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m1==3:
		db_self.kind_m1_g1 = int(db_self.outputs_table_4_m1[0][1])
		db_self.kind_m1_g2 = int(db_self.outputs_table_4_m1[0][5])
		db_self.kind_m1_g3 = int(db_self.outputs_table_4_m1[0][9])
		db_self.diff_m1_g1 = int(db_self.outputs_table_4_m1[0][0])
		db_self.diff_m1_g2 = int(db_self.outputs_table_4_m1[0][4])
		db_self.diff_m1_g3 = int(db_self.outputs_table_4_m1[0][8])					   
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m1==4:
		db_self.kind_m1_g1 = int(db_self.outputs_table_4_m1[0][0])
		db_self.kind_m1_g2 = int(db_self.outputs_table_4_m1[0][4])
		db_self.kind_m1_g3 = int(db_self.outputs_table_4_m1[0][8])
		db_self.kind_m1_g4 = int(db_self.outputs_table_4_m1[0][12])
		db_self.diff_m1_g1 = int(db_self.outputs_table_4_m1[0][0])
		db_self.diff_m1_g2 = int(db_self.outputs_table_4_m1[0][4])
		db_self.diff_m1_g3 = int(db_self.outputs_table_4_m1[0][8])
		db_self.diff_m1_g4 = int(db_self.outputs_table_4_m1[0][12])					
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m2==1:
		db_self.kind_m2_g1 = int(db_self.outputs_table_4_m2[0][1])
		db_self.diff_m2_g1 = int(db_self.outputs_table_4_m2[0][0])					   
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m2==2:
		db_self.kind_m2_g1 = int(db_self.outputs_table_4_m2[0][1])
		db_self.kind_m2_g2 = int(db_self.outputs_table_4_m2[0][5])
		db_self.diff_m2_g1 = int(db_self.outputs_table_4_m2[0][0])
		db_self.diff_m2_g2 = int(db_self.outputs_table_4_m2[0][4])					   
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m2==3:
		db_self.kind_m2_g1 = int(db_self.outputs_table_4_m2[0][1])
		db_self.kind_m2_g2 = int(db_self.outputs_table_4_m2[0][5])
		db_self.kind_m2_g3 = int(db_self.outputs_table_4_m2[0][9])
		db_self.diff_m2_g1 = int(db_self.outputs_table_4_m2[0][0])
		db_self.diff_m2_g2 = int(db_self.outputs_table_4_m2[0][4])
		db_self.diff_m2_g3 = int(db_self.outputs_table_4_m2[0][8])					   
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m2==4:
		db_self.kind_m2_g1 = int(db_self.outputs_table_4_m2[0][1])
		db_self.kind_m2_g2 = int(db_self.outputs_table_4_m2[0][5])
		db_self.kind_m2_g3 = int(db_self.outputs_table_4_m2[0][9])
		db_self.kind_m2_g4 = int(db_self.outputs_table_4_m2[0][13])
		db_self.diff_m2_g1 = int(db_self.outputs_table_4_m2[0][0])
		db_self.diff_m2_g2 = int(db_self.outputs_table_4_m2[0][4])
		db_self.diff_m2_g3 = int(db_self.outputs_table_4_m2[0][8])
		db_self.diff_m2_g4 = int(db_self.outputs_table_4_m2[0][12])					
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m3==1:
		db_self.kind_m3_g1 = int(db_self.outputs_table_4_m3[0][1])
		db_self.diff_m3_g1 = int(db_self.outputs_table_4_m3[0][0])
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m3==2:
		db_self.kind_m3_g1 = int(db_self.outputs_table_4_m3[0][1])
		db_self.kind_m3_g2 = int(db_self.outputs_table_4_m3[0][5])
		db_self.diff_m3_g1 = int(db_self.outputs_table_4_m3[0][0])
		db_self.diff_m3_g2 = int(db_self.outputs_table_4_m3[0][4])					   
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m3==3:
		db_self.kind_m3_g1 = int(db_self.outputs_table_4_m3[0][1])
		db_self.kind_m3_g2 = int(db_self.outputs_table_4_m3[0][5])
		db_self.kind_m3_g3 = int(db_self.outputs_table_4_m3[0][9])
		db_self.diff_m3_g1 = int(db_self.outputs_table_4_m3[0][0])
		db_self.diff_m3_g2 = int(db_self.outputs_table_4_m3[0][4])
		db_self.diff_m3_g3 = int(db_self.outputs_table_4_m3[0][8])					   
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m3==4:
		db_self.kind_m3_g1 = int(db_self.outputs_table_4_m3[0][1])
		db_self.kind_m3_g2 = int(db_self.outputs_table_4_m3[0][5])
		db_self.kind_m3_g3 = int(db_self.outputs_table_4_m3[0][9])
		db_self.kind_m3_g4 = int(db_self.outputs_table_4_m3[0][13])
		db_self.diff_m3_g1 = int(db_self.outputs_table_4_m3[0][0])
		db_self.diff_m3_g2 = int(db_self.outputs_table_4_m3[0][4])
		db_self.diff_m3_g3 = int(db_self.outputs_table_4_m3[0][8])
		db_self.diff_m3_g4 = int(db_self.outputs_table_4_m3[0][12])
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
cdef games_and_diffs_num4(db_self):
	if db_self.games_m1==1:
		db_self.kind_m1_g1 = int(db_self.outputs_table_4_m1[0][1])
		db_self.diff_m1_g1 = int(db_self.outputs_table_4_m1[0][0])
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		#####################################################################################################################			
	if db_self.games_m1==2:
		db_self.kind_m1_g1 = int(db_self.outputs_table_4_m1[0][1])
		db_self.kind_m1_g2 = int(db_self.outputs_table_4_m1[0][5])
		db_self.diff_m1_g1 = int(db_self.outputs_table_4_m1[0][0])
		db_self.diff_m1_g2 = int(db_self.outputs_table_4_m1[0][4])				   
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m1==3:
		db_self.kind_m1_g1 = int(db_self.outputs_table_4_m1[0][1])
		db_self.kind_m1_g2 = int(db_self.outputs_table_4_m1[0][5])
		db_self.kind_m1_g3 = int(db_self.outputs_table_4_m1[0][9])
		db_self.diff_m1_g1 = int(db_self.outputs_table_4_m1[0][0])
		db_self.diff_m1_g2 = int(db_self.outputs_table_4_m1[0][4])
		db_self.diff_m1_g3 = int(db_self.outputs_table_4_m1[0][8])				   
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m1==4:
		db_self.kind_m1_g1 = int(db_self.outputs_table_4_m1[0][1])
		db_self.kind_m1_g2 = int(db_self.outputs_table_4_m1[0][5])
		db_self.kind_m1_g3 = int(db_self.outputs_table_4_m1[0][9])
		db_self.kind_m1_g4 = int(db_self.outputs_table_4_m1[0][13])
		db_self.diff_m1_g1 = int(db_self.outputs_table_4_m1[0][0])
		db_self.diff_m1_g2 = int(db_self.outputs_table_4_m1[0][4])
		db_self.diff_m1_g3 = int(db_self.outputs_table_4_m1[0][8])
		db_self.diff_m1_g4 = int(db_self.outputs_table_4_m1[0][12])				
		#####################################################################################################################
		db_self.kind_m1_g1 = sp.last_minute_security_change_kind(db_self.kind_m1_g1, db_self.diff_m1_g1, db_self.level_input) 
		db_self.kind_m1_g2 = sp.last_minute_security_change_kind(db_self.kind_m1_g2, db_self.diff_m1_g2, db_self.level_input) 
		db_self.kind_m1_g3 = sp.last_minute_security_change_kind(db_self.kind_m1_g3, db_self.diff_m1_g3, db_self.level_input) 
		db_self.kind_m1_g4 = sp.last_minute_security_change_kind(db_self.kind_m1_g4, db_self.diff_m1_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m2==1:
		db_self.kind_m2_g1 = int(db_self.outputs_table_4_m2[0][1])
		db_self.diff_m2_g1 = int(db_self.outputs_table_4_m2[0][0])
		#####################################################################################################################
		db_self.kind_m2_g1 = sp.last_minute_security_change_kind(db_self.kind_m2_g1, db_self.diff_m2_g1, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m2==2:
		db_self.kind_m2_g1 = int(db_self.outputs_table_4_m2[0][1])
		db_self.kind_m2_g2 = int(db_self.outputs_table_4_m2[0][5])
		db_self.diff_m2_g1 = int(db_self.outputs_table_4_m2[0][0])
		db_self.diff_m2_g2 = int(db_self.outputs_table_4_m2[0][4])				   
		#####################################################################################################################
		db_self.kind_m2_g1 = sp.last_minute_security_change_kind(db_self.kind_m2_g1, db_self.diff_m2_g1, db_self.level_input) 
		db_self.kind_m2_g2 = sp.last_minute_security_change_kind(db_self.kind_m2_g2, db_self.diff_m2_g2, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m2==3:
		db_self.kind_m2_g1 = int(db_self.outputs_table_4_m2[0][1])
		db_self.kind_m2_g2 = int(db_self.outputs_table_4_m2[0][5])
		db_self.kind_m2_g3 = int(db_self.outputs_table_4_m2[0][9])
		db_self.diff_m2_g1 = int(db_self.outputs_table_4_m2[0][0])
		db_self.diff_m2_g2 = int(db_self.outputs_table_4_m2[0][4])
		db_self.diff_m2_g3 = int(db_self.outputs_table_4_m2[0][8])				   
		#####################################################################################################################
		db_self.kind_m2_g1 = sp.last_minute_security_change_kind(db_self.kind_m2_g1, db_self.diff_m2_g1, db_self.level_input) 
		db_self.kind_m2_g2 = sp.last_minute_security_change_kind(db_self.kind_m2_g2, db_self.diff_m2_g2, db_self.level_input) 
		db_self.kind_m2_g3 = sp.last_minute_security_change_kind(db_self.kind_m2_g3, db_self.diff_m2_g3, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m2==4:
		db_self.kind_m2_g1 = int(db_self.outputs_table_4_m2[0][1])
		db_self.kind_m2_g2 = int(db_self.outputs_table_4_m2[0][5])
		db_self.kind_m2_g3 = int(db_self.outputs_table_4_m2[0][9])
		db_self.kind_m2_g4 = int(db_self.outputs_table_4_m2[0][13])
		db_self.diff_m2_g1 = int(db_self.outputs_table_4_m2[0][0])
		db_self.diff_m2_g2 = int(db_self.outputs_table_4_m2[0][4])
		db_self.diff_m2_g3 = int(db_self.outputs_table_4_m2[0][8])
		db_self.diff_m2_g4 = int(db_self.outputs_table_4_m2[0][12])				
		#####################################################################################################################
		db_self.kind_m2_g1 = sp.last_minute_security_change_kind(db_self.kind_m2_g1, db_self.diff_m2_g1, db_self.level_input) 
		db_self.kind_m2_g2 = sp.last_minute_security_change_kind(db_self.kind_m2_g2, db_self.diff_m2_g2, db_self.level_input) 
		db_self.kind_m2_g3 = sp.last_minute_security_change_kind(db_self.kind_m2_g3, db_self.diff_m2_g3, db_self.level_input) 
		db_self.kind_m2_g4 = sp.last_minute_security_change_kind(db_self.kind_m2_g4, db_self.diff_m2_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m3==1:
		db_self.kind_m3_g1 = int(db_self.outputs_table_4_m3[0][1])
		db_self.diff_m3_g1 = int(db_self.outputs_table_4_m3[0][0])
		#####################################################################################################################
		db_self.kind_m3_g1 = sp.last_minute_security_change_kind(db_self.kind_m3_g1, db_self.diff_m3_g1, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m3==2:
		db_self.kind_m3_g1 = int(db_self.outputs_table_4_m3[0][1])
		db_self.kind_m3_g2 = int(db_self.outputs_table_4_m3[0][5])
		db_self.diff_m3_g1 = int(db_self.outputs_table_4_m3[0][0])
		db_self.diff_m3_g2 = int(db_self.outputs_table_4_m3[0][4])				   
		#####################################################################################################################
		db_self.kind_m3_g1 = sp.last_minute_security_change_kind(db_self.kind_m3_g1, db_self.diff_m3_g1, db_self.level_input) 
		db_self.kind_m3_g2 = sp.last_minute_security_change_kind(db_self.kind_m3_g2, db_self.diff_m3_g2, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m3==3:
		db_self.kind_m3_g1 = int(db_self.outputs_table_4_m3[0][1])
		db_self.kind_m3_g2 = int(db_self.outputs_table_4_m3[0][5])
		db_self.kind_m3_g3 = int(db_self.outputs_table_4_m3[0][9])
		db_self.diff_m3_g1 = int(db_self.outputs_table_4_m3[0][0])
		db_self.diff_m3_g2 = int(db_self.outputs_table_4_m3[0][4])
		db_self.diff_m3_g3 = int(db_self.outputs_table_4_m3[0][8])				   
		#####################################################################################################################
		db_self.kind_m3_g1 = sp.last_minute_security_change_kind(db_self.kind_m3_g1, db_self.diff_m3_g1, db_self.level_input) 
		db_self.kind_m3_g2 = sp.last_minute_security_change_kind(db_self.kind_m3_g2, db_self.diff_m3_g2, db_self.level_input) 
		db_self.kind_m3_g3 = sp.last_minute_security_change_kind(db_self.kind_m3_g3, db_self.diff_m3_g3, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m3==4:
		db_self.kind_m3_g1 = int(db_self.outputs_table_4_m3[0][1])
		db_self.kind_m3_g2 = int(db_self.outputs_table_4_m3[0][5])
		db_self.kind_m3_g3 = int(db_self.outputs_table_4_m3[0][9])
		db_self.kind_m3_g4 = int(db_self.outputs_table_4_m3[0][13])
		db_self.diff_m3_g1 = int(db_self.outputs_table_4_m3[0][0])
		db_self.diff_m3_g2 = int(db_self.outputs_table_4_m3[0][4])
		db_self.diff_m3_g3 = int(db_self.outputs_table_4_m3[0][8])
		db_self.diff_m3_g4 = int(db_self.outputs_table_4_m3[0][12])				
		#####################################################################################################################
		db_self.kind_m3_g1 = sp.last_minute_security_change_kind(db_self.kind_m3_g1, db_self.diff_m3_g1, db_self.level_input) 
		db_self.kind_m3_g2 = sp.last_minute_security_change_kind(db_self.kind_m3_g2, db_self.diff_m3_g2, db_self.level_input) 
		db_self.kind_m3_g3 = sp.last_minute_security_change_kind(db_self.kind_m3_g3, db_self.diff_m3_g3, db_self.level_input) 
		db_self.kind_m3_g4 = sp.last_minute_security_change_kind(db_self.kind_m3_g4, db_self.diff_m3_g4, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m4==1:
		db_self.kind_m4_g1 = int(db_self.outputs_table_4_m4[0][1])
		db_self.diff_m4_g1 = int(db_self.outputs_table_4_m4[0][0])
		#####################################################################################################################
		db_self.kind_m4_g1 = sp.last_minute_security_change_kind(db_self.kind_m4_g1, db_self.diff_m4_g1, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m4==2:
		db_self.kind_m4_g1 = int(db_self.outputs_table_4_m4[0][1])
		db_self.kind_m4_g2 = int(db_self.outputs_table_4_m4[0][5])
		db_self.diff_m4_g1 = int(db_self.outputs_table_4_m4[0][0])
		db_self.diff_m4_g2 = int(db_self.outputs_table_4_m4[0][4])
		#####################################################################################################################
		db_self.kind_m4_g1 = sp.last_minute_security_change_kind(db_self.kind_m4_g1, db_self.diff_m4_g1, db_self.level_input) 
		db_self.kind_m4_g2 = sp.last_minute_security_change_kind(db_self.kind_m4_g2, db_self.diff_m4_g2, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m4==3:
		db_self.kind_m4_g1 = int(db_self.outputs_table_4_m4[0][1])
		db_self.kind_m4_g2 = int(db_self.outputs_table_4_m4[0][5])
		db_self.kind_m4_g3 = int(db_self.outputs_table_4_m4[0][9])
		db_self.diff_m4_g1 = int(db_self.outputs_table_4_m4[0][0])
		db_self.diff_m4_g2 = int(db_self.outputs_table_4_m4[0][4])
		db_self.diff_m4_g3 = int(db_self.outputs_table_4_m4[0][8])
		#####################################################################################################################
		db_self.kind_m4_g1 = sp.last_minute_security_change_kind(db_self.kind_m4_g1, db_self.diff_m4_g1, db_self.level_input) 
		db_self.kind_m4_g2 = sp.last_minute_security_change_kind(db_self.kind_m4_g2, db_self.diff_m4_g2, db_self.level_input) 
		db_self.kind_m4_g3 = sp.last_minute_security_change_kind(db_self.kind_m4_g3, db_self.diff_m4_g3, db_self.level_input) 
		#####################################################################################################################	
	if db_self.games_m4==4:
		db_self.kind_m4_g1 = int(db_self.outputs_table_4_m4[0][1])
		db_self.kind_m4_g2 = int(db_self.outputs_table_4_m4[0][5])
		db_self.kind_m4_g3 = int(db_self.outputs_table_4_m4[0][9])
		db_self.kind_m4_g4 = int(db_self.outputs_table_4_m4[0][13])
		db_self.diff_m4_g1 = int(db_self.outputs_table_4_m4[0][0])
		db_self.diff_m4_g2 = int(db_self.outputs_table_4_m4[0][4])
		db_self.diff_m4_g3 = int(db_self.outputs_table_4_m4[0][8])
		db_self.diff_m4_g4 = int(db_self.outputs_table_4_m4[0][12])
		#####################################################################################################################
		db_self.kind_m4_g1 = sp.last_minute_security_change_kind(db_self.kind_m4_g1, db_self.diff_m4_g1, db_self.level_input) 
		db_self.kind_m4_g2 = sp.last_minute_security_change_kind(db_self.kind_m4_g2, db_self.diff_m4_g2, db_self.level_input) 
		db_self.kind_m4_g3 = sp.last_minute_security_change_kind(db_self.kind_m4_g3, db_self.diff_m4_g3, db_self.level_input) 
		db_self.kind_m4_g4 = sp.last_minute_security_change_kind(db_self.kind_m4_g4, db_self.diff_m4_g4, db_self.level_input) 
		#####################################################################################################################	
