"""Info:
	Oimi robot database module for adding new elements into the right tables --> second part.
	Choose proper characteristics for a match, according to session complexity and other available info
	called by add_new constrained session!!! before add_new_match
	otherwise when session is not constrained ---> add_new_session --> add_new_match direclty!!
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import copy
import random
from importlib import reload
from typing import Optional
import db_extern_methods_two as dbem2
cimport database_controller_add_into as dca
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef define_matches_when_complexity_is_easy(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games):
	advisable_time = 240
	this_match_mixed = 0
	print("ADESSO ENTRO IN COMPLEX EASY inside!!")
	print("FACCIAMO UN RECAPPINO DEI VALORI NELL ALTRO METODO")
	print("TIPOOO db_self.suggested_type_game_from_q = sugge_res[0][0] {}".format(db_self.suggested_type_game_from_q))
	print("kind!!! db_self.suggested_kind_game_from_q = sugge_res[0][1] {}".format(db_self.suggested_kind_game_from_q))
	one_ran_scenario_def = 0
	print("NUM OF MATCHES INSIDE INSIDE INSIDE {}".format(num_of_matches))
	print("MA IMPOSED mixed C'È???????? {}".format(db_self.imposed_mixed))
	print("MA imposed KIND COMEERA CHE NON MI RICORD? {}".format(db_self.imposed_kind_game))
	if num_of_matches==1:
		if db_self.imposed_difficulty_match=='':
			difficulty_m = 'Easy_1'
		else:
			difficulty_m = db_self.imposed_difficulty_match
			db_self.already_done_imposed_difficulty_match = 1
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games = random.randint(2,3)
			########################################################################
			if db_self.suggest_num_games_more_than==3:
				num_of_games = 3
			else:
				num_of_games = random.randint(1,3)
		else:
			num_of_games = db_self.imposed_num_games
			db_self.already_done_imposed_num_games = 1
			########################################################################
			if db_self.imposed_num_games==1:
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					num_of_games = random.randint(2,3)
					db_self.already_done_imposed_num_games = 0
			########################################################################
		print("############################################################ scenario QUANDO SONO ALL INTERNO take_given_scenario {}".format(db_self.imposed_scenario))
		if db_self.imposed_scenario!=0:
			print("scenario is imposed")
			scenario = db_self.imposed_scenario
			random_var = random.randint(0,3)
			if num_of_games==1:
				random_var = 0
			if random_var==0:
				db_self.curs.execute("SELECT kind FROM Scenarios_Kinds JOIN Scenarios USING(scenario_group) WHERE scenario_id = ? ORDER BY RANDOM() LIMIT 1",(scenario,))
				variety = db_self.curs.fetchone()[0]
			else:
				variety = 'SUNDRY'
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				variety=='SUNDRY'
			########################################################################
		else:
			tikiqu_sharing = 0
			tiki_sharing = 0
			tiqu_sharing = 0
			kiqu_sharing = 0
			ok_end = 0
			if db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and db_self.imposed_ques_id==99999:
				print("bugia 00")
				print("TYPE kind and question GAME NOT GIVEN AT ALL, SO CASUAL CHOICE SCENARIO!!")
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				if random_var==0:
					if difficulty_m in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety = random.choice(variety_ava)
					if difficulty_m in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety = random.choice(variety_ava)
					if difficulty_m=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety = random.choice(variety_ava)
					if difficulty_m=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety = random.choice(variety_ava)
					if difficulty_m=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety = random.choice(variety_ava)
					if difficulty_m=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety = random.choice(variety_ava)
					print("variety scelta is, ", variety)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety,))
					scenario = db_self.curs.fetchone()[0]
					print("######## scenario scelto causa variety {}".format(scenario))
				else:
					variety = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario))
			###########################
			###########################
			###########################
			else:
				if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999:
					print("bugia tripla")
					print("TYPE GAME KIND and also question GIVEN FROM IMPOSITIONSLIST!!")
					####
					if db_self.imposed_type_game=='4PSU':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_type_game=='4PCO':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE type=?""",(db_self.imposed_type_game,))
						sce_type = db_self.curs.fetchall()
						resp_type = [item for sublist in sce_type for item in sublist]
					####
					if db_self.imposed_kind_game!='4P':
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
						USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
						scen_kind = db_self.curs.fetchall()
						resp_kind = [item for sublist in scen_kind for item in sublist]
					else:
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
						scen4psu = db_self.curs.fetchall()
						resp4psu = [item for sublist in scen4psu for item in sublist]
						scen4pco = db_self.curs.fetchall()
						resp4pco = [item for sublist in scen4pco for item in sublist]
					####												
					if db_self.imposed_ques_id in [3013,3014,3015,3016]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE question_id=?""",(db_self.imposed_ques_id,))
						sce_ques = db_self.curs.fetchall()
					resp_ques = [item for sublist in sce_ques for item in sublist]
					######
					############## nel caso in cui esistessero non sia possibile trovare lo scenario valido per i 3, cerco 2 ...altrimenti no vince 3 !!! nel caso di un solo match!! nel caso di 2 pure!!
					###########cambia se sono di piu in questo caso gli faccio trovare tutti i tk kq e tq cmq anche se il 3 non c'è, e poi lo spalmo volendo su più match...per esempio quando la complessita e minore li spalmo altrimenti no!!!
					tq_inter = list(set(resp_ques) & set(resp_type))
					if tq_inter:
						tiqu_sharing = 1
						print("share scen tiqu!")
						random.shuffle(tq_inter)
						one_ran_scenario_def = random.choices(tq_inter,k = 1)
						print("one_ran_scenario_def is ==> ", one_ran_scenario_def)
						if db_self.imposed_kind_game!='4P':
							tkq_inter = list(set(tq_inter) & set(resp_kind))						
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tkq_inter = list(set(tq_inter) & set(resp4psu))
									if not tkq_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tkq_inter = list(set(tq_inter) & set(resp4pco))
									if not tkq_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if tkq_inter:
							random.shuffle(tkq_inter)
							one_ran_scenario_def = random.choices(tkq_inter,k = 1)
							print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
							kitiqu_sharing = 1
							ok_end = 1
						else:
							if db_self.imposed_kind_game!='4P':
								tk_inter = list(set(resp_type) & set(resp_kind))
							else:
								fcase = random.randint(0,1)
								dstop = 1
								into_psu = 0
								into_pco = 0
								while dstop>0:
									if fcase==0:
										tk_inter = list(set(resp_type) & set(resp4psu))
										if not tk_inter and not into_pco:
											fcase = 1
											into_psu = 1
										else:
											dstop = 0
									if fcase==1:									
										tk_inter = list(set(resp_type) & set(resp4pco))
										if not tk_inter and not into_psu:
											fcase = 1
											into_pco = 1
										else:
											dstop = 0
							if tk_inter:
								random.shuffle(tk_inter)
								one_ran_scenario_def = random.choices(tk_inter,k = 1)
								print("one_ran_scenario_from_tk_inter ", one_ran_scenario_def)
								kiti_sharing = 1
								ok_end = 1
							else:
								if db_self.imposed_kind_game!='4P':
									kq_inter = list(set(resp_ques) & set(resp_kind))
								else:
									fcase = random.randint(0,1)
									dstop = 1
									into_psu = 0
									into_pco = 0
									while dstop>0:
										if fcase==0:
											tk_inter = list(set(resp_ques) & set(resp4psu))
											if not tk_inter and not into_pco:
												fcase = 1
												into_psu = 1
											else:
												dstop = 0
										if fcase==1:									
											tk_inter = list(set(resp_ques) & set(resp4pco))
											if not tk_inter and not into_psu:
												fcase = 1
												into_pco = 1
											else:
												dstop = 0
								if kq_inter:
									random.shuffle(kq_inter)
									one_ran_scenario_def = random.choices(kq_inter,k = 1)
									print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
									kiqu_sharing = 1
									ok_end = 1
					if not ok_end: #already tkq no sense in search also here....
						################ search tiki
						if db_self.imposed_kind_game!='4P':
							tk_inter = list(set(resp_type) & set(resp_kind))
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tk_inter = list(set(resp_type) & set(resp4psu))
									if not tk_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tk_inter = list(set(resp_type) & set(resp4pco))
									if not tk_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if tk_inter:
							random.shuffle(tk_inter)
							one_ran_scenario_def = random.choices(tk_inter,k = 1)
							print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
							kiti_sharing = 1
						################ search kiqu
						if db_self.imposed_kind_game!='4P':
							kq_inter = list(set(resp_ques) & set(resp_kind))
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tk_inter = list(set(resp_ques) & set(resp4psu))
									if not tk_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tk_inter = list(set(resp_ques) & set(resp4pco))
									if not tk_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if kq_inter:
							random.shuffle(kq_inter)
							one_ran_scenario_def = random.choices(kq_inter,k = 1)
							print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
							kiqu_sharing = 1
					print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
					if one_ran_scenario_def:
						random_var = random.randint(0,3)
						########################################################################
						if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
							random_var = 1
						########################################################################
						scenario = one_ran_scenario_def[0]
						print("scenario trovato {} ".format(scenario))
						if kitiqu_sharing:
							print("######## scenario imposed da tutti e 3 {}".format(scenario))
							db_self.suggested_scenario_from_k = copy.copy(scenario)
							db_self.suggested_scenario_from_t = copy.copy(scenario)
							db_self.suggested_scenario_from_q = copy.copy(scenario)
							if random_var==0:
								variety = db_self.imposed_kind_game
							else:
								variety = 'SUNDRY'
						elif kiti_sharing:
							print("######## scenario imposed tiki sce {}".format(scenario))
							db_self.suggested_scenario_from_k = copy.copy(scenario)
							db_self.suggested_scenario_from_t = copy.copy(scenario)
							if random_var==0:
								variety = db_self.imposed_kind_game
							else:
								variety = 'SUNDRY'
						elif tiqu_sharing:
							print("######## scenario imposed tiqu sce {}".format(scenario))
							db_self.suggested_scenario_from_t = copy.copy(scenario)
							db_self.suggested_scenario_from_q = copy.copy(scenario)
							if random_var==0:
								variety = dbem2.find_kind(db_self.imposed_type_game)
							else:
								variety = 'SUNDRY'
						elif kiqu_sharing:
							print("######## scenario imposed kiqu sce {}".format(scenario))
							db_self.suggested_scenario_from_k = copy.copy(scenario)
							db_self.suggested_scenario_from_q = copy.copy(scenario)
							if random_var==0:
								variety = db_self.imposed_kind_game
							else:
								variety = 'SUNDRY'
				###########################
				###########################
				if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id==99999:
					####
					if db_self.imposed_type_game=='4PSU':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_type_game=='4PCO':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE type=?""",(db_self.imposed_type_game,))
						sce_type = db_self.curs.fetchall()
					resp_type = [item for sublist in sce_type for item in sublist]
					####
					if db_self.imposed_kind_game!='4P':
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
						USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
						scen_kind = db_self.curs.fetchall()
						resp_kind = [item for sublist in scen_kind for item in sublist]
					else:
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
						scen4psu = db_self.curs.fetchall()
						resp4psu = [item for sublist in scen4psu for item in sublist]
						scen4pco = db_self.curs.fetchall()
						resp4pco = [item for sublist in scen4pco for item in sublist]
					################ search tiki
					if db_self.imposed_kind_game!='4P':
						tk_inter = list(set(resp_type) & set(resp_kind))
					else:
						fcase = random.randint(0,1)
						dstop = 1
						into_psu = 0
						into_pco = 0
						while dstop>0:
							if fcase==0:
								tk_inter = list(set(resp_type) & set(resp4psu))
								if not tk_inter and not into_pco:
									fcase = 1
									into_psu = 1
								else:
									dstop = 0
							if fcase==1:									
								tk_inter = list(set(resp_type) & set(resp4pco))
								if not tk_inter and not into_psu:
									fcase = 1
									into_pco = 1
								else:
									dstop = 0
					if tk_inter:
						random.shuffle(tk_inter)
						one_ran_scenario_def = random.choices(tk_inter,k = 1)
						print("one_ran_scenario_from_tk_inter ", one_ran_scenario_def)
						kiti_sharing = 1
					print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
					if one_ran_scenario_def:
						scenario = one_ran_scenario_def[0]
						print("scenario trovato {} ".format(scenario))
						print("######## scenario imposed tiki sce {}".format(scenario))
						db_self.suggested_scenario_from_k = copy.copy(scenario)
						db_self.suggested_scenario_from_t = copy.copy(scenario)
						########################################################################
						random_var = random.randint(0,3)
						########################################################################
						if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
							random_var = 1
						########################################################################
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
						########################################################################
				if db_self.imposed_type_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game=='':
					####
					if db_self.imposed_type_game=='4PSU':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_type_game=='4PCO':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE type=?""",(db_self.imposed_type_game,))
						sce_type = db_self.curs.fetchall()
					resp_type = [item for sublist in sce_type for item in sublist]
					####												
					if db_self.imposed_ques_id in [3013,3014,3015,3016]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE question_id=?""",(db_self.imposed_ques_id,))
						sce_ques = db_self.curs.fetchall()
					resp_ques = [item for sublist in sce_ques for item in sublist]
					########################### search tiqu
					tq_inter = list(set(resp_ques) & set(resp_type))
					if tq_inter:
						tiqu_sharing = 1
						print("share scen tiqu!")
						random.shuffle(tq_inter)
						one_ran_scenario_def = random.choices(tq_inter,k = 1)
					print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
					if one_ran_scenario_def:
						scenario = one_ran_scenario_def[0]
						print("scenario trovato {} ".format(scenario))
						print("######## scenario imposed tiqu sce {}".format(scenario))
						db_self.suggested_scenario_from_t = copy.copy(scenario)
						db_self.suggested_scenario_from_q = copy.copy(scenario)
						########################################################################
						random_var = random.randint(0,3)
						########################################################################
						if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
							random_var = 1
						########################################################################
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'
						########################################################################
				if db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_type_game=='':
					####
					if db_self.imposed_kind_game!='4P':
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
						USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
						scen_kind = db_self.curs.fetchall()
						resp_kind = [item for sublist in scen_kind for item in sublist]
					else:
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
						scen4psu = db_self.curs.fetchall()
						resp4psu = [item for sublist in scen4psu for item in sublist]
						scen4pco = db_self.curs.fetchall()
						resp4pco = [item for sublist in scen4pco for item in sublist]
					####												
					if db_self.imposed_ques_id in [3013,3014,3015,3016]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE question_id=?""",(db_self.imposed_ques_id,))
						sce_ques = db_self.curs.fetchall()
					resp_ques = [item for sublist in sce_ques for item in sublist]
					########################### search kiqu
					################ search kiqu
					if db_self.imposed_kind_game!='4P':
						kq_inter = list(set(resp_ques) & set(resp_kind))
					else:
						fcase = random.randint(0,1)
						dstop = 1
						into_psu = 0
						into_pco = 0
						while dstop>0:
							if fcase==0:
								tk_inter = list(set(resp_ques) & set(resp4psu))
								if not tk_inter and not into_pco:
									fcase = 1
									into_psu = 1
								else:
									dstop = 0
							if fcase==1:									
								tk_inter = list(set(resp_ques) & set(resp4pco))
								if not tk_inter and not into_psu:
									fcase = 1
									into_pco = 1
								else:
									dstop = 0
					if kq_inter:
						random.shuffle(kq_inter)
						one_ran_scenario_def = random.choices(kq_inter,k = 1)
						print("one_ran_scenario_from_tk_inter ", one_ran_scenario_def)
						kiqu_sharing = 1
					print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
					if one_ran_scenario_def:
						scenario = one_ran_scenario_def[0]
						print("scenario trovato {} ".format(scenario))
						print("######## scenario imposed kiqu sce {}".format(scenario))
						db_self.suggested_scenario_from_k = copy.copy(scenario)
						db_self.suggested_scenario_from_q = copy.copy(scenario)
						########################################################################
						random_var = random.randint(0,3)
						########################################################################
						if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
							random_var = 1
						########################################################################
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
						########################################################################
				###########################
				if not one_ran_scenario_def and db_self.imposed_type_game!='':
					print("bugia singola 1")
					####
					if db_self.imposed_type_game=='4PSU':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_type_game=='4PCO':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE type=?""",(db_self.imposed_type_game,))
						sce_type = db_self.curs.fetchall()
					resp_type = [item for sublist in sce_type for item in sublist]
					random.shuffle(resp_type)
					scenario = random.choices(resp_type,k = 1)
					print("come stai a non andare caso s1 {} ".format(scenario))
					scenario = scenario[0]
					print("######## scenario imposed dal tipo!!! sce {}".format(scenario))
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					random_var = random.randint(0,3)
					########################################################################
					if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
						random_var = 1
					########################################################################
					if random_var==0:
						variety = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety = 'SUNDRY'
				###########################
				if not one_ran_scenario_def and db_self.imposed_kind_game!='':
					print("bugia singola 2")
					if db_self.imposed_kind_game!='4P':
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
						USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
						scen_kind = db_self.curs.fetchall()
						resp_kind = [item for sublist in scen_kind for item in sublist]
					else:
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
						scen4psu = db_self.curs.fetchall()
						resp4psu = [item for sublist in scen4psu for item in sublist]
						scen4pco = db_self.curs.fetchall()
						resp4pco = [item for sublist in scen4pco for item in sublist]
						ran4p = random.randint(0,1)
						if ran4p:
							resp_kind = resp4pco
						else:
							resp_kind = resp4psu
					random.shuffle(resp_kind)
					scenario = random.choices(resp_kind,k = 1)
					print("come stai a non andare caso s2 {} ".format(scenario))
					scenario = scenario[0]
					print("######## scenario imposed dal kind is {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					random_var = random.randint(0,3)
					########################################################################
					if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
						random_var = 1
					########################################################################
					if random_var==0:
						variety = db_self.imposed_kind_game
					else:
						variety = 'SUNDRY'
					########################################################################
				###########################
				if not one_ran_scenario_def and db_self.imposed_ques_id!=99999:
					print("bugia singola 3")
					####												
					if db_self.imposed_ques_id in [3013,3014,3015,3016]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE question_id=?""",(db_self.imposed_ques_id,))
						sce_ques = db_self.curs.fetchall()
					resp_ques = [item for sublist in sce_ques for item in sublist]
					######
					random.shuffle(resp_ques)
					scenario = random.choices(resp_ques,k = 1)
					print("come stai a non andare caso s3 {} ".format(scenario))
					scenario = scenario[0]
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					print("ho appena settato db_self.suggested_scenario_from_q = copy.copy(scenario) {}".format(db_self.suggested_scenario_from_q))
					print("######## scenario imposed dalla question!!! sce {}".format(scenario))
					random_var = random.randint(0,3)
					########################################################################
					if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
						random_var = 1
					########################################################################
					if random_var==0:
						db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
						variety = db_self.curs.fetchone()[0]
					else:
						variety = 'SUNDRY'
				###########################
		print("variety is ==================================================================> ",variety)
		if variety=='SUNDRY':
			criteria_arrangement = 'regular'
		else:
			criteria_arrangement = 'ignore'
		########################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
			criteria_arrangement = 'random'
			db_self.already_done_imposed_mixed = 1 
		########################################################################
		ord_casual = random.randint(0,1)
		if ord_casual==0:
			order_diff_games = 'same' 
		elif ord_casual==1:
			order_diff_games = 'asc' 
		ord_casual = random.randint(0,1)
		if ord_casual==0:
			order_quan_ques = 'same' 
		elif ord_casual==1:
			order_quan_ques = 'asc'
		################################ ADD ADVISABLE TIME
		dca.add_new_Match_table(db_self, difficulty_m, num_of_games, variety, order_diff_games, order_quan_ques, criteria_arrangement, advisable_time, scenario, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last = db_self.curs.fetchone()[0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
	elif num_of_matches==2:
		def_matches_when_complexity_is_easy_2(db_self, num_of_matches, sess_last, order_diff_matches, order_quan_games)
	elif num_of_matches==3:
		def_matches_when_complexity_is_easy_3(db_self, num_of_matches, sess_last, order_diff_matches, order_quan_games)
	elif num_of_matches==4:
		def_matches_when_complexity_is_easy_3(db_self, num_of_matches, sess_last, order_diff_matches, order_quan_games)

cdef def_matches_when_complexity_is_easy_2(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games):
	advisable_time = 240
	one_ran_scenario_def = 0
	mix_imposed_in_m1 = 0
	mix_imposed_in_m2 = 0
	options = ['Easy_1', 'Easy_2']
	if db_self.imposed_difficulty_match=='' or (db_self.imposed_difficulty_match!='' and db_self.already_done_imposed_difficulty_match==1):
		if order_diff_matches=='same':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = difficulty_m1
		if order_diff_matches=='casu':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
		if order_diff_matches=='asc':
			difficulty_m1 = 'Easy_1'
			difficulty_m2 = 'Easy_2'
		if order_diff_matches=='desc':
			difficulty_m1 = 'Easy_2'
			difficulty_m2 = 'Easy_1'				
	else:
		db_self.already_done_imposed_difficulty_match = 1
		if order_diff_matches=='same':
			difficulty_m1 = db_self.imposed_difficulty_match
			difficulty_m2 = difficulty_m1
		if order_diff_matches=='casu':
			ran_ran = random.randint(0,1)
			if ran_ran==0:
				difficulty_m1 = db_self.imposed_difficulty_match
				difficulty_m2 = random.choice(options)
			else:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = db_self.imposed_difficulty_match
		if order_diff_matches=='asc':
			if db_self.imposed_difficulty_match=='Easy_1' or db_self.imposed_difficulty_match=='Easy_2':
				difficulty_m1 = 'Easy_1'
				difficulty_m2 = 'Easy_2'
			else:
				ran_2 = random.randint(0,1)
				if ran_2:
					difficulty_m1 = 'Easy_1'
					difficulty_m2 = db_self.imposed_difficulty_match
				else:
					difficulty_m1 = 'Easy_2'
					difficulty_m2 = db_self.imposed_difficulty_match
		if order_diff_matches=='desc':
			if db_self.imposed_difficulty_match=='Easy_1' or db_self.imposed_difficulty_match=='Easy_2':
				difficulty_m1 = 'Easy_2'
				difficulty_m2 = 'Easy_1'
			else:
				ran_2 = random.randint(0,1)
				if ran_2:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Easy_1'
				else:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Easy_2'
	########################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
		if order_diff_matches=='same':
			difficulty_m1 = 'Normal_1'
			mix_imposed_in_m1 = 1
			difficulty_m2 = 'Normal_1'
			mix_imposed_in_m2 = 1			
		if order_diff_matches=='casu':
			rand_change = random.randint(1,2)
			if rand_change==1:
				difficulty_m1 = 'Normal_1'
				mix_imposed_in_m1 = 1
			if rand_change==2:
				difficulty_m2 = 'Normal_1'
				mix_imposed_in_m2 = 1			
		if order_diff_matches=='asc':
			difficulty_m2 = 'Normal_1'
			mix_imposed_in_m2 = 1			
		if order_diff_matches=='desc':
			difficulty_m1 = 'Normal_1'
			mix_imposed_in_m1 = 1
	########################################################################				
	if order_quan_games=='same':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			if db_self.suggest_num_games_more_than==3:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
			else:	
				casual = random.randint(0,3)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
				elif casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
			if db_self.suggest_num_games_more_than in (1,2):
				num_of_games_m1 = 2
				num_of_games_m2 = 2
			if db_self.suggest_num_games_more_than==3:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
			if db_self.suggest_num_games_more_than==4:
				num_of_games_m1 = 4
				num_of_games_m2 = 4
		else:
			num_of_games_m1 = db_self.imposed_num_games
			num_of_games_m2 = num_of_games_m1
			db_self.already_done_imposed_num_games = 1
		########################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
			num_of_games_m1 = 4
			num_of_games_m2 = 2
			db_self.already_done_imposed_num_games = 0
		########################################################################					
	elif order_quan_games=='casu':
		if db_self.suggest_num_games_more_than==3:
			num_of_games_m1 = 3
			num_of_games_m2 = random.randint(1,3)
		else:
			num_of_games_m1 = random.randint(1,3)
			num_of_games_m2 = 3
		########################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
			num_of_games_m1 = random.randint(2,3)
			num_of_games_m2 = random.randint(2,3)
		########################################################################		
		if db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==0:
			num_of_games_m1 = db_self.imposed_num_games
			db_self.already_done_imposed_num_games = 1
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 4
				num_of_games_m2 = 2
				db_self.already_done_imposed_num_games = 0
			########################################################################				
	elif order_quan_games=='asc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			if db_self.suggest_num_games_more_than==3:
				casual = random.randint(0,2)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==2:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
			else:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==2:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
				elif casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 2
				num_of_games_m2 = 3
			########################################################################						
		else:
			db_self.already_done_imposed_num_games = 1
			if db_self.imposed_num_games==1:
				num_of_games_m1 = 1
				num_of_games_m2 = 2
			elif db_self.imposed_num_games==2:
				num_of_games_m1 = 1
				num_of_games_m2 = 2
			elif db_self.imposed_num_games==3:
				num_of_games_m1 = 1
				num_of_games_m2 = 3
			elif db_self.imposed_num_games==4:
				num_of_games_m1 = 2
				num_of_games_m2 = 4
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 2
				num_of_games_m2 = 3
				db_self.already_done_imposed_num_games = 0
			########################################################################					
	elif order_quan_games=='desc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			if db_self.suggest_num_games_more_than==3:
				depends = random.randint(0,2)
				if depends==0:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				if depends==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
			else:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				elif casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2			
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
			########################################################################									
		else:
			db_self.already_done_imposed_num_games = 1
			if db_self.imposed_num_games==1:
				num_of_games_m1 = 2
				num_of_games_m2 = 1
			if db_self.imposed_num_games==2:
				num_of_games_m1 = 2
				num_of_games_m2 = 1
			if db_self.imposed_num_games==3:
				num_of_games_m1 = 3
				num_of_games_m2 = 1
			if db_self.imposed_num_games==4:
				num_of_games_m1 = 4
				num_of_games_m2 = 2
		########################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
			num_of_games_m1 = 3
			num_of_games_m2 = 2
			db_self.already_done_imposed_num_games = 0
		########################################################################
	if difficulty_m1=='Easy_1':
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m1 = 'same' 
		elif ord_casual==2:
			order_diff_games_m1 = 'asc' 
		order_quan_ques_m1 = 'same'
	if difficulty_m1=='Easy_2':
		ord_casual = random.randint(0,1)
		if ord_casual==0:
			order_diff_games_m1 = 'same' 
		elif ord_casual==1 :
			order_diff_games_m1 = 'asc' 
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_quan_ques_m1 = 'same' 
		elif ord_casual==2:
			order_quan_ques_m1 = 'asc' 
	if difficulty_m1 in ['Normal_1','Normal_2','Medium_1','Medium_2','Hard_1','Hard_2']:
		ord_casual = random.randint(0,5)
		if ord_casual in [0,1,2]:
			order_diff_games_m1 = 'same' 
		if ord_casual in [3,4]:
			order_diff_games_m1 = 'asc' 
		else:
			order_diff_games_m1 = 'desc' 
		ord_casual = random.randint(0,5)
		if ord_casual in [0,1,2]:
			order_quan_ques_m1 = 'same' 
		if ord_casual in [3,4]:
			order_quan_ques_m1 = 'asc' 
		else:
			order_quan_ques_m1 = 'desc' 
	if difficulty_m2=='Easy_1':
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m2 = 'same' 
		elif ord_casual==2:
			order_diff_games_m2 = 'asc' 
		ord_casual = random.randint(0,1)
		if ord_casual==0:
			order_quan_ques_m2 = 'same' 
		elif ord_casual==1:
			order_quan_ques_m2 = 'asc' 
	if difficulty_m2=='Easy_2':
		ord_casual = random.randint(0,1)
		if ord_casual==0: 
			order_diff_games_m2 = 'same' 
		elif ord_casual==1:
			order_diff_games_m2 = 'asc' 
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_quan_ques_m2 = 'same' 
		elif ord_casual==2:
			order_quan_ques_m2 = 'asc' 
	if difficulty_m2 in ['Normal_1','Normal_2','Medium_1','Medium_2','Hard_1','Hard_2']:
		ord_casual = random.randint(0,5)
		if ord_casual in [0,1,2]:
			order_diff_games_m2 = 'same' 
		if ord_casual in [3,4]:
			order_diff_games_m2 = 'asc' 
		else:
			order_diff_games_m2 = 'desc' 
		ord_casual = random.randint(0,5)
		if ord_casual in [0,1,2]:
			order_quan_ques_m2 = 'same' 
		if ord_casual in [3,4]:
			order_quan_ques_m2 = 'asc' 
		else:
			order_quan_ques_m2 = 'desc' 				
	#################################################################################################################################################################################
	#scenario imposed can have one positon among 1,2,3 match
	print("############################################################ part_2!!! scenario take_given_scenario {}".format(db_self.imposed_scenario))
	scenario_m1 = 0
	scenario_m2 = 0
	variety_m1 = ''
	variety_m2 = ''
	print("IMPOSED!!!!!! RANMA!!! ".format(db_self.imposed_scenario))
	if db_self.imposed_scenario!=0:
		random_var = random.randint(0,3)
		sundry_or_random_m1 = random.randint(0,2)
		sundry_or_random_m2 = random.randint(0,2)
		db_self.curs.execute("SELECT kind FROM Scenarios_Kinds JOIN Scenarios USING(scenario_group) WHERE scenario_id = ? ORDER BY RANDOM() LIMIT 1",(db_self.imposed_scenario,))	
		if random_var==0:
			variety_m1 = db_self.curs.fetchone()[0]
			scenario_m1 = db_self.imposed_scenario
		if random_var==1:
			variety_m2 = db_self.curs.fetchone()[0]
			scenario_m2 = db_self.imposed_scenario
		##################################################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
			variety_m1 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
			variety_m2 = 'SUNDRY'
		##################################################################################################						
	if db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and db_self.imposed_ques_id==99999:
		print("bugia 00")
		print("TYPE GAME NOT GIVEN AT ALL, SO CASUAL CHOICE SCENARIO!!")
		if scenario_m1==0: #non è stato imposto scenario m1
			print("vado di percorso scenario m1 perche ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1							
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
		###########################
		###########################
		###########################
	else:
		tikiqu_sharing = 0
		tiki_sharing = 0
		tiqu_sharing = 0
		kiqu_sharing = 0
		ok_end = 0
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999:
			print("bugia tripla")
			print("TYPE GAME KIND and also question GIVEN FROM IMPOSITIONSLIST!!")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			############## nel caso in cui esistessero non sia possibile trovare lo scenario valido per i 3, cerco 2 ...altrimenti no vince 3 !!! nel caso di un solo match!! nel caso di 2 pure!!
			###########cambia se sono di piu in questo caso gli faccio trovare tutti i tk kq e tq cmq anche se il 3 non c'è, e poi lo spalmo volendo su più match...per esempio quando la complessita e minore li spalmo altrimenti no!!!
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
				if db_self.imposed_kind_game!='4P':
					tkq_inter = list(set(tq_inter) & set(resp_kind))						
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tkq_inter = list(set(tq_inter) & set(resp4psu))
							if not tkq_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tkq_inter = list(set(tq_inter) & set(resp4pco))
							if not tkq_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tkq_inter:
					random.shuffle(tkq_inter)
					one_ran_scenario_def = random.choices(tkq_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kitiqu_sharing = 1
					ok_end = 1
				else:
					if db_self.imposed_kind_game!='4P':
						tk_inter = list(set(resp_type) & set(resp_kind))
					else:
						fcase = random.randint(0,1)
						dstop = 1
						into_psu = 0
						into_pco = 0
						while dstop>0:
							if fcase==0:
								tk_inter = list(set(resp_type) & set(resp4psu))
								if not tk_inter and not into_pco:
									fcase = 1
									into_psu = 1
								else:
									dstop = 0
							if fcase==1:									
								tk_inter = list(set(resp_type) & set(resp4pco))
								if not tk_inter and not into_psu:
									fcase = 1
									into_pco = 1
								else:
									dstop = 0
					if tk_inter:
						random.shuffle(tk_inter)
						one_ran_scenario_def = random.choices(tk_inter,k = 1)
						print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
						kiti_sharing = 1
						ok_end = 1
					else:
						if db_self.imposed_kind_game!='4P':
							kq_inter = list(set(resp_ques) & set(resp_kind))
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tk_inter = list(set(resp_ques) & set(resp4psu))
									if not tk_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tk_inter = list(set(resp_ques) & set(resp4pco))
									if not tk_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if kq_inter:
							random.shuffle(kq_inter)
							one_ran_scenario_def = random.choices(kq_inter,k = 1)
							print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
							kiqu_sharing = 1
							ok_end = 1
			if not ok_end:
				################ search tiki
				if db_self.imposed_kind_game!='4P':
					tk_inter = list(set(resp_type) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_type) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_type) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tk_inter:
					random.shuffle(tk_inter)
					one_ran_scenario_def = random.choices(tk_inter,k = 1)
					print("one_ran_scenario_from_tk_inter ", one_ran_scenario_def)
					kiti_sharing = 1
				################ search kiqu
				if db_self.imposed_kind_game!='4P':
					kq_inter = list(set(resp_ques) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_ques) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_ques) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if kq_inter:
					random.shuffle(kq_inter)
					one_ran_scenario_def = random.choices(kq_inter,k = 1)
					print("one_ran_scenario_from_KQ_inter ", one_ran_scenario_def)
					kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				if kitiqu_sharing:
					print("######## scenario imposed da tutti e 3 {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
				elif kiti_sharing:
					print("######## scenario imposed tiki sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
				elif tiqu_sharing:
					print("######## scenario imposed tiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'							
				elif kiqu_sharing:
					print("######## scenario imposed kiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
		###########################
		###########################
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id==99999:
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			################ search tiki
			if db_self.imposed_kind_game!='4P':
				tk_inter = list(set(resp_type) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_type) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_type) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if tk_inter:
				random.shuffle(tk_inter)
				one_ran_scenario_def = random.choices(tk_inter,k = 1)
				print("one_ran_scenario_from_tk_inter ", one_ran_scenario_def)
				kiti_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiki sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
		if db_self.imposed_type_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game=='':
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search tiqu
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m2 = 'SUNDRY'
		if db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_type_game=='':
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search kiqu
			################ search kiqu
			if db_self.imposed_kind_game!='4P':
				kq_inter = list(set(resp_ques) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_ques) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_ques) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if kq_inter:
				random.shuffle(kq_inter)
				one_ran_scenario_def = random.choices(kq_inter,k = 1)
				print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
				kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed kiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
		###########################
		if not one_ran_scenario_def and db_self.imposed_type_game!='':
			print("bugia singola 1")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			random.shuffle(resp_type)
			scenario = random.choices(resp_type,k = 1)
			print("come stai a non andare caso s1 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal tipo!!! sce {}".format(scenario))
			db_self.suggested_scenario_from_t = copy.copy(scenario)
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m2 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_kind_game!='':
			print("bugia singola 2")
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
				ran4p = random.randint(0,1)
				if ran4p:
					resp_kind = resp4pco
				else:
					resp_kind = resp4psu
			random.shuffle(resp_kind)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			scenario = random.choices(resp_kind,k = 1)
			print("come stai a non andare caso s2 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal kind is {}".format(scenario))
			db_self.suggested_scenario_from_k = copy.copy(scenario)
			random_var = random.randint(0,3)
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = db_self.imposed_kind_game
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = db_self.imposed_kind_game
				else:
					variety_m2 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_ques_id!=99999:
			print("bugia singola 3")
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			random.shuffle(resp_ques)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			scenario = random.choices(resp_ques,k = 1)
			print("come stai a non andare caso s3 {} ".format(scenario))
			scenario = scenario[0]
			db_self.suggested_scenario_from_q = copy.copy(scenario)
			print("ho appena settato db_self.suggested_scenario_from_q = copy.copy(scenario) {}".format(db_self.suggested_scenario_from_q))
			print("######## scenario imposed dalla question!!! sce {}".format(scenario))
			random_var = random.randint(0,3)
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	if 0 in (scenario_m1,scenario_m2):
		if scenario_m1==0:
			print("vado di percorso scenario m1 perche dopo tutto ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))		
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1	
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
	###########################################################
	may_swap = random.randint(0,2)
	if may_swap==0:
		scenario_m1, scenario_m2 = scenario_m2, scenario_m1
		variety_m1, variety_m2 = variety_m2, variety_m1
		print("ho fatto swap caso 0")
	###########################################################
	print("variety 1 is ==================================================================> ",variety_m1)
	print("variety 2 is ==================================================================> ",variety_m2)
	######################################################################################################################
	if variety_m1=='SUNDRY':
		criteria_arrangement_m1 = 'regular'
	else:
		criteria_arrangement_m1 = 'ignore'
	if variety_m2=='SUNDRY':
		criteria_arrangement_m2 = 'regular'
	else:
		criteria_arrangement_m2 = 'ignore'
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
		criteria_arrangement_m1 = 'random'
		db_self.already_done_imposed_mixed = 1 
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
		criteria_arrangement_m2 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################		
	print("prima di chiamare add_new_matches guardo com'è scenario 1{}".format(scenario_m1))
	print("prima di chiamare add_new_matches guardo com'è scenario 2{}".format(scenario_m2))
	dca.add_new_Match_table(db_self, difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
	
	dca.add_new_Match_table(db_self, difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m1, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
cdef def_matches_when_complexity_is_easy_3(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games):
	advisable_time = 240
	one_ran_scenario_def = 0
	mix_imposed_in_m1 = 0
	mix_imposed_in_m2 = 0
	mix_imposed_in_m3 = 0
	options = ['Easy_1', 'Easy_2']
	if db_self.imposed_difficulty_match=='' or (db_self.imposed_difficulty_match!='' and db_self.already_done_imposed_difficulty_match==1):
		if order_diff_matches=='same':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = difficulty_m1
			difficulty_m3 = difficulty_m1
		if order_diff_matches=='casu':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
		if order_diff_matches=='asc':
			difficulty_m1 = 'Easy_1'
			difficulty_m2 = 'Easy_2'
			difficulty_m3 = 'Easy_2'
		if order_diff_matches=='desc':
			difficulty_m1 = 'Easy_2'
			difficulty_m2 = 'Easy_2'				
			difficulty_m3 = 'Easy_1'				
	else:
		db_self.already_done_imposed_difficulty_match = 1
		if order_diff_matches=='same':
			difficulty_m1 = db_self.imposed_difficulty_match
			difficulty_m2 = difficulty_m1
			difficulty_m3 = difficulty_m1
		if order_diff_matches=='casu':
			ran_ran = random.randint(0,2)
			if ran_ran==0:
				difficulty_m1 = db_self.imposed_difficulty_match
				difficulty_m2 = random.choice(options)
				difficulty_m3 = random.choice(options)
			if ran_ran==1:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = random.choice(options)
				difficulty_m3 = db_self.imposed_difficulty_match
			if ran_ran==2:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = db_self.imposed_difficulty_match
				difficulty_m3 = random.choice(options)
		if order_diff_matches=='asc':
			if db_self.imposed_difficulty_match=='Easy_1' or db_self.imposed_difficulty_match=='Easy_2':
				difficulty_m1 = 'Easy_1'
				difficulty_m2 = 'Easy_1'
				difficulty_m3 = 'Easy_2'
			else:
				ran_2 = random.randint(0,1)
				if ran_2==0:
					difficulty_m1 = 'Easy_2'
					difficulty_m2 = 'Easy_2'
					difficulty_m3 = db_self.imposed_difficulty_match
				else:
					difficulty_m1 = 'Easy_1'
					difficulty_m2 = 'Easy_2'
					difficulty_m3 = db_self.imposed_difficulty_match
		if order_diff_matches=='desc':
			if db_self.imposed_difficulty_match=='Easy_1' or db_self.imposed_difficulty_match=='Easy_2':
				difficulty_m1 = 'Easy_2'
				difficulty_m2 = 'Easy_2'
				difficulty_m3 = 'Easy_1'
			else:
				ran_2 = random.randint(0,1)
				if ran_2:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Easy_1'
					difficulty_m3 = 'Easy_1'
				else:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Easy_2'
					difficulty_m3 = 'Easy_1'
	########################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
		if order_diff_matches=='same':
			difficulty_m1 = 'Normal_2'
			mix_imposed_in_m1 = 1
			difficulty_m2 = 'Normal_2'
			mix_imposed_in_m2 = 1	
			difficulty_m3 = 'Normal_2'
			mix_imposed_in_m3 = 1	
		if order_diff_matches=='casu':
			rand_change = random.randint(1,3)
			if rand_change==1:
				difficulty_m1 = 'Normal_2'
				mix_imposed_in_m1 = 1
			if rand_change==2:
				difficulty_m2 = 'Normal_2'
				mix_imposed_in_m2 = 1			
			if rand_change==3:
				difficulty_m3 = 'Normal_2'
				mix_imposed_in_m3 = 1	
		if order_diff_matches=='asc':
			difficulty_m3 = 'Normal_2'
			mix_imposed_in_m3 = 1			
		if order_diff_matches=='desc':
			difficulty_m1 = 'Normal_2'
			mix_imposed_in_m1 = 1
	########################################################################				
	if order_quan_games=='same':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			casual = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = random.randint(1,2)
			########################################################################	
			if casual==0:
				num_of_games_m1 = 1
				num_of_games_m2 = 1
				num_of_games_m3 = 1
			elif casual==2:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
				num_of_games_m3 = 3
			else:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 2
		else:
			db_self.already_done_imposed_num_games = 1
			num_of_games_m1 = db_self.imposed_num_games
			########################################################################
			if db_self.imposed_num_games==1:
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					num_of_games_m1 = random.randint(2,3)
					db_self.already_done_imposed_num_games = 0
			########################################################################	
			num_of_games_m2 = num_of_games_m1
			num_of_games_m3 = num_of_games_m1
	elif order_quan_games=='casu':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			num_of_games_m1 = random.randint(1,3)
			num_of_games_m2 = random.randint(1,3)
			num_of_games_m3 = random.randint(1,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = random.randint(2,3)
				num_of_games_m2 = random.randint(2,3)
				num_of_games_m3 = random.randint(2,3)
			########################################################################	
		else:
			db_self.already_done_imposed_num_games = 1
			num_of_games_m2 = random.randint(1,3)
			num_of_games_m3 = random.randint(1,3)
			########################################################################
			if db_self.imposed_num_games==1:
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					num_of_games_m1 = random.randint(2,3)
					num_of_games_m2 = random.randint(2,3)
					num_of_games_m3 = random.randint(2,3)
					db_self.already_done_imposed_num_games = 0
			########################################################################					
	elif order_quan_games=='asc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = 4
			########################################################################				
			casual = random.randint(0,5)
			if casual==0 or casual==1:
				num_of_games_m1 = 1
				num_of_games_m2 = 2
				num_of_games_m3 = 3
			elif casual==2 or casual==3:
				num_of_games_m1 = 1
				num_of_games_m2 = 1
				num_of_games_m3 = 2
			elif casual==4:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 3
			elif casual==5:
				num_of_games_m1 = 1
				num_of_games_m2 = 1
				num_of_games_m3 = 3
		else:
			db_self.already_done_imposed_num_games = 1
			if db_self.imposed_num_games==1:
				ramino = random.randint(0,1)
				if ramino:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2
				else:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
			if db_self.imposed_num_games==2:
				ramino = random.randint(0,1)
				if ramino:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2
				else:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
			if db_self.imposed_num_games==3:
				ramino = random.randint(0,1)
				if ramino:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 3
				else:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
			if db_self.imposed_num_games==4:
				ramino = random.randint(0,2)
				if ramino==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 4
				if ramino==2:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
					num_of_games_m3 = 4
				else:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 4
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 3
				db_self.already_done_imposed_num_games = 0
			########################################################################
	elif order_quan_games=='desc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			casual = random.randint(0,5)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = 5
			########################################################################
			if casual==0 or casual==1:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
				num_of_games_m3 = 1
			elif casual==2 or casual==3:
				num_of_games_m1 = 2
				num_of_games_m2 = 1
				num_of_games_m3 = 1
			elif casual==4:
				num_of_games_m1 = 3
				num_of_games_m2 = 1
				num_of_games_m3 = 1
			elif casual==5:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
				num_of_games_m3 = 2
		else:
			db_self.already_done_imposed_num_games = 1
			ramino = random.randint(0,1)		
			if db_self.imposed_num_games in (1,2):
				if ramino==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				else:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
			if db_self.imposed_num_games==3:
				if ramino==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				else:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
			if db_self.imposed_num_games==4:
				if ramino==1:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2
				else:
					num_of_games_m1 = 4
					num_of_games_m2 = 2
					num_of_games_m3 = 2
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 3
				db_self.already_done_imposed_num_games = 0
			########################################################################
	if difficulty_m1 in ['Easy_1','Easy_2']:
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m1 = 'same' 
		elif ord_casual==2:
			order_diff_games_m1 = 'asc' 
		ord_casual = random.randint(0,1)
		if ord_casual==0:
			order_quan_ques_m1 = 'same' 
		elif ord_casual==1:
			order_quan_ques_m1 = 'asc' 
	if difficulty_m1 in ['Normal_1','Normal_2']:
		ord_casual = random.randint(0,3)
		if ord_casual in [0,1]:
			order_diff_games_m1 = 'same' 
		elif ord_casual==2:
			order_diff_games_m1 = 'asc'
		else:
			order_diff_games_m1 = 'desc'
		ord_casual = random.randint(0,2)
		if ord_casual==0:
			order_quan_ques_m1 = 'same'
		elif ord_casual==1:
			order_quan_ques_m1 = 'asc'
		else:
			order_diff_games_m1 = 'desc'
	if difficulty_m1 in ['Medium_1','Medium_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m1 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m1 = 'asc' 
		else:
			order_diff_games_m1 = 'desc' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m1 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m1 = 'asc' 
		else:
			order_quan_ques_m1 = 'desc'
	if difficulty_m1 in ['Hard_1','Hard_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m1 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m1 = 'asc' 
		else:
			order_diff_games_m1 = 'casu' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m1 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m1 = 'asc' 
		else:
			order_quan_ques_m1 = 'casu'
	#####
	if difficulty_m2 in ['Easy_1','Easy_2']:
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m2 = 'same' 
		elif ord_casual==2:
			order_diff_games_m2 = 'asc' 
		ord_casual = random.randint(0,1)
		if ord_casual==0:
			order_quan_ques_m2 = 'same' 
		elif ord_casual==1:
			order_quan_ques_m2 = 'asc' 
	if difficulty_m2 in ['Normal_1','Normal_2']:
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m2 = 'same' 
		elif ord_casual==2:
			order_diff_games_m2 = 'asc'
		else:
			order_diff_games_m2 = 'desc'
		ord_casual = random.randint(0,2)
		if ord_casual==0:
			order_quan_ques_m2 = 'same'
		elif ord_casual==1:
			order_quan_ques_m2 = 'asc'
		else:
			order_quan_ques_m2 = 'desc'
	if difficulty_m2 in ['Medium_1','Medium_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m2 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m2 = 'asc' 
		else:
			order_diff_games_m2 = 'desc' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m2 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m2 = 'asc' 
		else:
			order_quan_ques_m2 = 'desc'
	if difficulty_m2 in ['Hard_1','Hard_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m2 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m2 = 'asc' 
		else:
			order_diff_games_m2 = 'casu' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m2 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m2 = 'asc' 
		else:
			order_quan_ques_m2 = 'casu'
	###
	if difficulty_m3 in ['Easy_1','Easy_2']:
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m3 = 'same' 
		elif ord_casual==2:
			order_diff_games_m3 = 'asc' 
		ord_casual = random.randint(0,1)
		if ord_casual==0:
			order_quan_ques_m3 = 'same' 
		elif ord_casual==1:
			order_quan_ques_m3 = 'asc' 
	if difficulty_m3 in ['Normal_1','Normal_2']:
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m3 = 'same' 
		elif ord_casual==2:
			order_diff_games_m3 = 'asc'
		else:
			order_diff_games_m3 = 'desc'
		ord_casual = random.randint(0,2)
		if ord_casual==0:
			order_quan_ques_m3 = 'same'
		elif ord_casual==1:
			order_quan_ques_m3 = 'asc'
		else:
			order_quan_ques_m3 = 'desc'
	if difficulty_m3 in ['Medium_1','Medium_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m3 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m3 = 'asc' 
		else:
			order_diff_games_m3 = 'desc' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m3 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m3 = 'asc' 
		else:
			order_quan_ques_m3 = 'desc'
	if difficulty_m3 in ['Hard_1','Hard_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m3 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m3 = 'asc' 
		else:
			order_diff_games_m3 = 'casu' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m3 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m3 = 'asc' 
		else:
			order_quan_ques_m3 = 'casu'
	#################################################################################################################################################################################
	#scenario imposed can have one positon among 1,2,3 match
	print("############################################################ part_2!!! scenario take_given_scenario {}".format(db_self.imposed_scenario))
	scenario_m1 = 0
	scenario_m2 = 0
	scenario_m3 = 0
	variety_m1 = ''
	variety_m2 = ''
	variety_m3 = ''
	print("IMPOSED!!!!!! RANMA!!! ".format(db_self.imposed_scenario))
	if db_self.imposed_scenario!=0:
		random_var = random.randint(0,3)
		sundry_or_random_m1 = random.randint(0,2)
		sundry_or_random_m2 = random.randint(0,2)
		sundry_or_random_m3 = random.randint(0,2)
		db_self.curs.execute("SELECT kind FROM Scenarios_Kinds JOIN Scenarios USING(scenario_group) WHERE scenario_id = ? ORDER BY RANDOM() LIMIT 1",(db_self.imposed_scenario,))	
		if random_var==0:
			variety_m1 = db_self.curs.fetchone()[0]
			scenario_m1 = db_self.imposed_scenario
		if random_var==1:
			variety_m2 = db_self.curs.fetchone()[0]
			scenario_m2 = db_self.imposed_scenario
		if random_var==2:
			variety_m3 = db_self.curs.fetchone()[0]
			scenario_m3 = db_self.imposed_scenario
		##################################################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
			variety_m1 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
			variety_m2 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
			variety_m3 = 'SUNDRY'
		##################################################################################################						
	if db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and db_self.imposed_ques_id==99999:
		print("bugia 00")
		print("TYPE GAME NOT GIVEN AT ALL, SO CASUAL CHOICE SCENARIO!!")
		if scenario_m1==0: #non è stato imposto scenario m1
			print("vado di percorso scenario m1 perche ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1		
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
		if scenario_m3==0: #non è stato imposto scenario m3
			print("vado di scenario_m3 {}".format(scenario_m3))
			random_var_m3 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
				random_var_m3 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m3 = scenario_m1
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			else:
				while True:
					if random_var_m3==0:
						print("difficulty_m3 {}".format(difficulty_m3))
						if difficulty_m3 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						print("variety_m3 scelta is, ", variety_m3)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario_m3 variety dependent non a caso {}".format(scenario_m3))
						if variety_m2=='SUNDRY' or variety_m3!=variety_m2:
							break
						else:
							ran3 = random.randint(0,3)
							if ran3==0:
								random_var_m3 = 1
					if random_var_m3!=0:
						variety_m3 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m3))
						break
		###########################
		###########################
		###########################
	else:
		tikiqu_sharing = 0
		tiki_sharing = 0
		tiqu_sharing = 0
		kiqu_sharing = 0
		ok_end = 0
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999:
			print("bugia tripla")
			print("TYPE GAME KIND and also question GIVEN FROM IMPOSITIONSLIST!!")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			############## nel caso in cui esistessero non sia possibile trovare lo scenario valido per i 3, cerco 2 ...altrimenti no vince 3 !!! nel caso di un solo match!! nel caso di 2 pure!!
			###########cambia se sono di piu in questo caso gli faccio trovare tutti i tk kq e tq cmq anche se il 3 non c'è, e poi lo spalmo volendo su più match...per esempio quando la complessita e minore li spalmo altrimenti no!!!
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
				if db_self.imposed_kind_game!='4P':
					tkq_inter = list(set(tq_inter) & set(resp_kind))						
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tkq_inter = list(set(tq_inter) & set(resp4psu))
							if not tkq_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tkq_inter = list(set(tq_inter) & set(resp4pco))
							if not tkq_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tkq_inter:
					random.shuffle(tkq_inter)
					one_ran_scenario_def = random.choices(tkq_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kitiqu_sharing = 1
					ok_end = 1
				else:
					if db_self.imposed_kind_game!='4P':
						tk_inter = list(set(resp_type) & set(resp_kind))
					else:
						fcase = random.randint(0,1)
						dstop = 1
						into_psu = 0
						into_pco = 0
						while dstop>0:
							if fcase==0:
								tk_inter = list(set(resp_type) & set(resp4psu))
								if not tk_inter and not into_pco:
									fcase = 1
									into_psu = 1
								else:
									dstop = 0
							if fcase==1:									
								tk_inter = list(set(resp_type) & set(resp4pco))
								if not tk_inter and not into_psu:
									fcase = 1
									into_pco = 1
								else:
									dstop = 0
					if tk_inter:
						random.shuffle(tk_inter)
						one_ran_scenario_def = random.choices(tk_inter,k = 1)
						print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
						kiti_sharing = 1
						ok_end = 1
					else:
						if db_self.imposed_kind_game!='4P':
							kq_inter = list(set(resp_ques) & set(resp_kind))
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tk_inter = list(set(resp_ques) & set(resp4psu))
									if not tk_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tk_inter = list(set(resp_ques) & set(resp4pco))
									if not tk_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if kq_inter:
							random.shuffle(kq_inter)
							one_ran_scenario_def = random.choices(kq_inter,k = 1)
							print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
							kiqu_sharing = 1
							ok_end = 1
			if not ok_end:
				################ search tiki
				if db_self.imposed_kind_game!='4P':
					tk_inter = list(set(resp_type) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_type) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_type) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tk_inter:
					random.shuffle(tk_inter)
					one_ran_scenario_def = random.choices(tk_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kiti_sharing = 1
				################ search kiqu
				if db_self.imposed_kind_game!='4P':
					kq_inter = list(set(resp_ques) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_ques) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_ques) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if kq_inter:
					random.shuffle(kq_inter)
					one_ran_scenario_def = random.choices(kq_inter,k = 1)
					print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
					kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				if kitiqu_sharing:
					print("######## scenario imposed da tutti e 3 {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety_m3 = db_self.imposed_kind_game
						else:
							variety_m3 = 'SUNDRY'
				elif kiti_sharing:
					print("######## scenario imposed tiki sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
				elif tiqu_sharing:
					print("######## scenario imposed tiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'							
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'	
				elif kiqu_sharing:
					print("######## scenario imposed kiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety_m3 = db_self.imposed_kind_game
						else:
							variety_m3 = 'SUNDRY'
		###########################
		###########################
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id==99999:
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			################ search tiki
			if db_self.imposed_kind_game!='4P':
				tk_inter = list(set(resp_type) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_type) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_type) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if tk_inter:
				random.shuffle(tk_inter)
				one_ran_scenario_def = random.choices(tk_inter,k = 1)
				print("one_ran_scenario_from_tk_inter ", one_ran_scenario_def)
				kiti_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiki sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m3 = scenario
					if random_var==0:
						variety_m3 = db_self.imposed_kind_game
					else:
						variety_m3 = 'SUNDRY'						
		if db_self.imposed_type_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game=='':
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search tiqu
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m3 = scenario
					if random_var==0:
						variety_m3 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m3 = 'SUNDRY'
		if db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_type_game=='':
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search kiqu
			################ search kiqu
			if db_self.imposed_kind_game!='4P':
				kq_inter = list(set(resp_ques) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_ques) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_ques) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if kq_inter:
				random.shuffle(kq_inter)
				one_ran_scenario_def = random.choices(kq_inter,k = 1)
				print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
				kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed kiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m3 = db_self.imposed_kind_game
					else:
						variety_m3 = 'SUNDRY'
		###########################
		if not one_ran_scenario_def and db_self.imposed_type_game!='':
			print("bugia singola 1")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			random.shuffle(resp_type)
			scenario = random.choices(resp_type,k = 1)
			print("come stai a non andare caso s1 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal tipo!!! sce {}".format(scenario))
			db_self.suggested_scenario_from_t = copy.copy(scenario)
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					variety_m3 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m3 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_kind_game!='':
			print("bugia singola 2")
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
				ran4p = random.randint(0,1)
				if ran4p:
					resp_kind = resp4pco
				else:
					resp_kind = resp4psu
			random.shuffle(resp_kind)
			scenario = random.choices(resp_kind,k = 1)
			print("come stai a non andare caso s2 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal kind is {}".format(scenario))
			db_self.suggested_scenario_from_k = copy.copy(scenario)
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = db_self.imposed_kind_game
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = db_self.imposed_kind_game
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					variety_m3 = db_self.imposed_kind_game
				else:
					variety_m3 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_ques_id!=99999:
			print("bugia singola 3")
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			random.shuffle(resp_ques)
			scenario = random.choices(resp_ques,k = 1)
			print("come stai a non andare caso s3 {} ".format(scenario))
			scenario = scenario[0]
			db_self.suggested_scenario_from_q = copy.copy(scenario)
			print("ho appena settato db_self.suggested_scenario_from_q = copy.copy(scenario) {}".format(db_self.suggested_scenario_from_q))
			print("######## scenario imposed dalla question!!! sce {}".format(scenario))
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	if 0 in (scenario_m1,scenario_m2,scenario_m3):
		if scenario_m1==0:
			print("vado di percorso scenario m1 perche ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m1 = scenario_m3
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m3,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m2 = scenario_m3
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m3,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1	
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
		if scenario_m3==0: #non è stato imposto scenario m3
			print("vado di scena m3 è ancora a zero")
			random_var_m3 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
				random_var_m3 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m3 = scenario_m1
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			elif depends==0 and scenario_m2!=0:
				scenario_m3 = scenario_m2
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			else:
				while True:
					if random_var_m3==0:
						print("difficulty_m3 {}".format(difficulty_m3))
						if difficulty_m3 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						print("variety_m3 scelta is, ", variety_m3)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario_m3 variety dependent non a caso {}".format(scenario_m3))
						if variety_m2=='SUNDRY' or variety_m3!=variety_m2:
							break
						else:
							ran3 = random.randint(0,3)
							if ran3==0:
								random_var_m3 = 1
					if random_var_m3!=0:
						variety_m3 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m3))
						break
	###########################################################
	may_swap = random.randint(0,3)
	if may_swap==0:
		scenario_m1, scenario_m2 = scenario_m2, scenario_m1
		variety_m1, variety_m2 = variety_m2, variety_m1
		print("ho fatto swap caso 0")
	if may_swap==1:
		scenario_m1, scenario_m3 = scenario_m3, scenario_m1
		variety_m1, variety_m3 = variety_m3, variety_m1
		print("ho fatto swap caso 1")
	###########################################################
	print("variety 1 is ==================================================================> ",variety_m1)
	print("variety 2 is ==================================================================> ",variety_m2)
	print("variety 3 is ==================================================================> ",variety_m3)
	######################################################################################################################
	if variety_m1=='SUNDRY':
		a_crite = random.randint(0,5)
		if a_crite==0:
			criteria_arrangement_m1 = 'random'
		else:
			criteria_arrangement_m1 = 'regular'
	else:
		criteria_arrangement_m1 = 'ignore'
	if variety_m2=='SUNDRY':
		a_crite = random.randint(0,5)
		if a_crite==0:
			criteria_arrangement_m2 = 'random'
		else:
			criteria_arrangement_m2 = 'regular'
	else:
		criteria_arrangement_m2 = 'ignore'
	if variety_m3=='SUNDRY':
		a_crite = random.randint(0,5)
		if a_crite==0:
			criteria_arrangement_m3 = 'random'
		else:
			criteria_arrangement_m3 = 'regular'
	else:
		criteria_arrangement_m3 = 'ignore'
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
		criteria_arrangement_m1 = 'random'
		db_self.already_done_imposed_mixed = 1 
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
		criteria_arrangement_m2 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################	
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
		criteria_arrangement_m3 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################
	dca.add_new_Match_table(db_self, difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m1, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m1, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
cdef def_matches_when_complexity_is_easy_4(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games):
	advisable_time = 240
	one_ran_scenario_def = 0
	mix_imposed_in_m1 = 0
	mix_imposed_in_m2 = 0
	mix_imposed_in_m3 = 0
	mix_imposed_in_m4 = 0		
	options = ['Easy_1', 'Easy_2']
	if db_self.imposed_difficulty_match=='' or (db_self.imposed_difficulty_match!='' and db_self.already_done_imposed_difficulty_match==1):
		if order_diff_matches=='same':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = difficulty_m1
			difficulty_m3 = difficulty_m1
			difficulty_m4 = difficulty_m1
		if order_diff_matches=='casu':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
			difficulty_m4 = random.choice(options)
		if order_diff_matches=='asc':
			difficulty_m1 = 'Easy_1'
			difficulty_m2 = 'Easy_2'
			difficulty_m3 = 'Easy_2'
			difficulty_m4 = 'Normal_1'
		if order_diff_matches=='desc':
			difficulty_m1 = 'Normal_1'
			difficulty_m2 = 'Easy_2'				
			difficulty_m3 = 'Easy_1'				
			difficulty_m4 = 'Easy_1'				
	else:
		db_self.already_done_imposed_difficulty_match = 1
		if order_diff_matches=='same':
			difficulty_m1 = db_self.imposed_difficulty_match
			difficulty_m2 = difficulty_m1
			difficulty_m3 = difficulty_m1
			difficulty_m3 = difficulty_m1
		if order_diff_matches=='casu':
			ran_ran = random.randint(0,3)
			if ran_ran==0:
				difficulty_m1 = db_self.imposed_difficulty_match
				difficulty_m2 = random.choice(options)
				difficulty_m3 = random.choice(options)
				difficulty_m4 = random.choice(options)
			if ran_ran==1:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = db_self.imposed_difficulty_match
				difficulty_m3 = random.choice(options)
				difficulty_m4 = random.choice(options)
			if ran_ran==2:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = random.choice(options)
				difficulty_m3 = db_self.imposed_difficulty_match
				difficulty_m4 = random.choice(options)
			else:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = random.choice(options)
				difficulty_m3 = random.choice(options)
				difficulty_m4 = db_self.imposed_difficulty_match
		if order_diff_matches=='asc':
			if db_self.imposed_difficulty_match=='Easy_1' or db_self.imposed_difficulty_match=='Easy_2' or db_self.imposed_difficulty_match=='Normal_1':
				difficulty_m1 = 'Easy_1'
				difficulty_m2 = 'Easy_1'
				difficulty_m3 = 'Easy_2'
				difficulty_m4 = 'Normal_1'
			else:
				ran_2 = random.randint(0,1)
				if ran_2==0:
					difficulty_m1 = 'Easy_1'
					difficulty_m2 = 'Easy_2'
					difficulty_m3 = 'Easy_2'
					difficulty_m4 = db_self.imposed_difficulty_match
				else:
					difficulty_m1 = 'Easy_1'
					difficulty_m2 = 'Easy_2'
					difficulty_m3 = 'Normal_1'
					difficulty_m4 = db_self.imposed_difficulty_match
		if order_diff_matches=='desc':
			if db_self.imposed_difficulty_match=='Easy_1' or db_self.imposed_difficulty_match=='Easy_2' or db_self.imposed_difficulty_match=='Normal_1':
				difficulty_m1 = 'Normal_1'
				difficulty_m2 = 'Easy_2'
				difficulty_m3 = 'Easy_2'
				difficulty_m4 = 'Easy_1'
			else:
				ran_2 = random.randint(0,1)
				if ran_2:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Normal_1'
					difficulty_m3 = 'Easy_2'
					difficulty_m4 = 'Easy_1'
				else:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Easy_2'
					difficulty_m3 = 'Easy_1'
					difficulty_m4 = 'Easy_1'
	########################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
		if order_diff_matches=='same':
			difficulty_m1 = 'Medium_2'
			mix_imposed_in_m1 = 1
			difficulty_m2 = 'Medium_2'
			mix_imposed_in_m2 = 1	
			difficulty_m3 = 'Medium_2'
			mix_imposed_in_m3 = 1	
			difficulty_m4 = 'Medium_2'
			mix_imposed_in_m4 = 1	
		if order_diff_matches=='casu':
			rand_change = random.randint(1,3)
			if rand_change==1:
				difficulty_m1 = 'Medium_2'
				mix_imposed_in_m1 = 1
			if rand_change==2:
				difficulty_m2 = 'Medium_2'
				mix_imposed_in_m2 = 1			
			if rand_change==3:
				difficulty_m3 = 'Medium_2'
				mix_imposed_in_m3 = 1	
			if rand_change==3:
				difficulty_m4 = 'Medium_2'
				mix_imposed_in_m4 = 1	
		if order_diff_matches=='asc':
			difficulty_m4 = 'Medium_2'
			mix_imposed_in_m4 = 1			
		if order_diff_matches=='desc':
			difficulty_m1 = 'Medium_2'
			mix_imposed_in_m1 = 1
	########################################################################
	if order_quan_games=='same':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):					
			casual = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = random.randint(1,2)
			########################################################################				
			if casual==0:
				num_of_games_m1 = 1
				num_of_games_m2 = 1
				num_of_games_m3 = 1
				num_of_games_m4 = 1
			elif casual==2:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
				num_of_games_m3 = 3
				num_of_games_m4 = 3
			else:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 2
				num_of_games_m4 = 2
		else:
			db_self.already_done_imposed_num_games = 1
			num_of_games_m1 = db_self.imposed_num_games
			num_of_games_m2 = db_self.imposed_num_games
			num_of_games_m3 = db_self.imposed_num_games
			num_of_games_m4 = db_self.imposed_num_games
			########################################################################
			if db_self.imposed_num_games==1:
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					num_of_games_m1 = random.randint(2,4)
					num_of_games_m2 = num_of_games_m1
					num_of_games_m3 = num_of_games_m1
					num_of_games_m4 = num_of_games_m1
					db_self.already_done_imposed_num_games = 0
			########################################################################					
	elif order_quan_games=='casu':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			num_of_games_m1 = random.randint(1,3)
			num_of_games_m2 = random.randint(1,3)
			num_of_games_m3 = random.randint(1,3)
			num_of_games_m4 = random.randint(1,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = random.randint(2,3)
				num_of_games_m2 = random.randint(2,3)
				num_of_games_m3 = random.randint(2,3)
				num_of_games_m4 = random.randint(2,3)
			########################################################################					
		else:
			db_self.already_done_imposed_num_games = 1
			num_of_games_m1 = random.randint(1,3)
			num_of_games_m2 = random.randint(1,3)
			num_of_games_m3 = random.randint(1,3)
			num_of_games_m4 = random.randint(1,3)
			change_or = random.randint(0,3)
			if change_or==0:
				num_of_games_m1 = db_self.imposed_num_games
			if change_or==1:
				num_of_games_m2 = db_self.imposed_num_games
			if change_or==2:
				num_of_games_m3 = db_self.imposed_num_games
			if change_or==3:
				num_of_games_m4 = db_self.imposed_num_games
			########################################################################
			if db_self.imposed_num_games==1:
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					num_of_games_m1 = random.randint(2,3)
					num_of_games_m2 = random.randint(2,3)
					num_of_games_m3 = random.randint(2,3)
					num_of_games_m4 = random.randint(2,3)
					db_self.already_done_imposed_num_games = 0
			########################################################################						
	elif order_quan_games=='asc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			casual = random.randint(0,5)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = 5
			########################################################################				
			if casual==0 or casual==1:
				num_of_games_m1 = 1
				num_of_games_m2 = 2
				num_of_games_m3 = 2
				num_of_games_m4 = 3
			elif casual==2 or casual==3:
				num_of_games_m1 = 1
				num_of_games_m2 = 1
				num_of_games_m3 = 2 
				num_of_games_m4 = 3
			elif casual==4:
				num_of_games_m1 = 1
				num_of_games_m2 = 2
				num_of_games_m3 = 3
				num_of_games_m4 = 3
			elif casual==5:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 3
				num_of_games_m4 = 3
		else:
			db_self.already_done_imposed_num_games = 1
			if db_self.imposed_num_games==1:
				casual = random.randint(0,1)
				if casual:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 3
				else:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 1
					num_of_games_m4 = 2
			if db_self.imposed_num_games==2:
				casual = random.randint(0,2)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 3
				if casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
				else:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 4
			if db_self.imposed_num_games==3:
				casual = random.randint(0,2)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 3
				if casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 4
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 4
			if db_self.imposed_num_games==4:
				casual = random.randint(0,1)
				if casual:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2
					num_of_games_m4 = 4
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 4
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 3
				num_of_games_m4 = 4					
				db_self.already_done_imposed_num_games = 0
			########################################################################
	elif order_quan_games=='desc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			casual = random.randint(0,5)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = 5
			########################################################################					
			if casual==0 or casual==1:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
				num_of_games_m3 = 1
				num_of_games_m4 = 1
			elif casual==2 or casual==3:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
				num_of_games_m3 = 2
				num_of_games_m4 = 1
			elif casual==4:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
				num_of_games_m3 = 2
				num_of_games_m4 = 1
			elif casual==5:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
				num_of_games_m3 = 2
				num_of_games_m4 = 2
		else:
			db_self.already_done_imposed_num_games = 1
			if db_self.imposed_num_games==1:
				casual = random.randint(0,1)
				if casual:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 1
					num_of_games_m4 = 1
			if db_self.imposed_num_games==2:
				casual = random.randint(0,2)
				if casual==0:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 2
				if casual==1:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				else:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
					num_of_games_m4 = 1
			if db_self.imposed_num_games==3:
				casual = random.randint(0,2)
				if casual==0:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				if casual==1:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 2
				else:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
			if db_self.imposed_num_games==4:
				casual = random.randint(0,1)
				if casual:
					num_of_games_m1 = 4
					num_of_games_m2 = 4
					num_of_games_m3 = 3
					num_of_games_m4 = 2
				else:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 3
				num_of_games_m4 = 3
				db_self.already_done_imposed_num_games = 0
			########################################################################
	ord_casual = random.randint(0,6)
	if ord_casual in [0,1]:
		order_diff_games_m1 = 'same' 
		order_diff_games_m2 = 'same' 
		order_diff_games_m3 = 'asc' 
		order_diff_games_m4 = 'asc' 
	if ord_casual in [2,3]:
		order_diff_games_m1 = 'same' 
		order_diff_games_m2 = 'same' 
		order_diff_games_m3 = 'same' 
		order_diff_games_m4 = 'asc' 
	if ord_casual in [4,5]:
		order_diff_games_m1 = 'same' 
		order_diff_games_m2 = 'asc' 
		order_diff_games_m3 = 'same' 
		order_diff_games_m4 = 'asc' 
	elif ord_casual==6:
		order_diff_games_m1 = 'same' 
		order_diff_games_m2 = 'asc' 
		order_diff_games_m3 = 'casu' 
		order_diff_games_m4 = 'asc' 
	ord_casual = random.randint(0,5)
	if ord_casual in [0,1]:
		order_quan_ques_m1 = 'same' 
		order_quan_ques_m2 = 'same' 
		order_quan_ques_m3 = 'asc' 
		order_quan_ques_m4 = 'asc' 
	if ord_casual in [2,3]:
		order_quan_ques_m1 = 'same' 
		order_quan_ques_m2 = 'asc' 
		order_quan_ques_m3 = 'asc' 
		order_quan_ques_m4 = 'asc' 
	elif ord_casual==4:
		order_quan_ques_m1 = 'asc' 
		order_quan_ques_m2 = 'same' 
		order_quan_ques_m3 = 'casu' 
		order_quan_ques_m4 = 'same' 
	elif ord_casual==5:
		order_quan_ques_m1 = 'asc' 
		order_quan_ques_m2 = 'same' 
		order_quan_ques_m3 = 'desc' 
		order_quan_ques_m4 = 'casu' 
	
	#scenario imposed can have one positon among 1,2,3 match
	print("############################################################ part_2!!! scenario take_given_scenario {}".format(db_self.imposed_scenario))
	scenario_m1 = 0
	scenario_m2 = 0
	scenario_m3 = 0
	scenario_m4 = 0
	variety_m1 = ''
	variety_m2 = ''
	variety_m3 = ''
	variety_m4 = ''
	print("IMPOSED!!!!!! RANMA!!! {}".format(db_self.imposed_scenario))
	if db_self.imposed_scenario!=0:
		random_var = random.randint(0,3)
		sundry_or_random_m1 = random.randint(0,2)
		sundry_or_random_m2 = random.randint(0,2)
		sundry_or_random_m3 = random.randint(0,2)
		sundry_or_random_m4 = random.randint(0,2)
		db_self.curs.execute("SELECT kind FROM Scenarios_Kinds JOIN Scenarios USING(scenario_group) WHERE scenario_id = ? ORDER BY RANDOM() LIMIT 1",(db_self.imposed_scenario,))	
		if random_var==0:
			variety_m1 = db_self.curs.fetchone()[0]
			scenario_m1 = db_self.imposed_scenario
		if random_var==1:
			variety_m2 = db_self.curs.fetchone()[0]
			scenario_m2 = db_self.imposed_scenario
		if random_var==2:
			variety_m3 = db_self.curs.fetchone()[0]
			scenario_m3 = db_self.imposed_scenario
		if random_var==3:
			variety_m4 = db_self.curs.fetchone()[0]
			scenario_m4 = db_self.imposed_scenario
		##################################################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
			variety_m1 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
			variety_m2 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
			variety_m3 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m4:
			variety_m4 = 'SUNDRY'				
		##################################################################################################						
	if db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and db_self.imposed_ques_id==99999:
		print("bugia 00")
		print("TYPE GAME NOT GIVEN AT ALL, SO CASUAL CHOICE SCENARIO!!")
		if scenario_m1==0: #non è stato imposto scenario m1
			print("vado di percorso scenario m1 perche ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))		
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1			
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
		if scenario_m3==0: #non è stato imposto scenario m3
			print("vado di scenario_m3 {}".format(scenario_m3))
			random_var_m3 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
				random_var_m3 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m3 = scenario_m1
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			else:
				while True:
					if random_var_m3==0:
						print("difficulty_m3 {}".format(difficulty_m3))
						if difficulty_m3 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						print("variety_m3 scelta is, ", variety_m3)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario_m3 variety dependent non a caso {}".format(scenario_m3))
						if variety_m2=='SUNDRY' or variety_m3!=variety_m2:
							break
						else:
							ran3 = random.randint(0,3)
							if ran3==0:
								random_var_m3 = 1
					if random_var_m3!=0:
						variety_m3 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m3))
						break
		if scenario_m4==0: #non è stato imposto scenario m4
			print("vado di scenario_m4 {}".format(scenario_m4))
			random_var_m4 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m4:
				random_var_m4 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m4 = scenario_m1
				if random_var_m4==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m4 = db_self.curs.fetchone()[0]
				else:
					variety_m4 = 'SUNDRY'
			else:
				while True:
					if random_var_m4==0:
						print("difficulty_m4 {}".format(difficulty_m4))
						if difficulty_m4 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m4 = random.choice(variety_ava)
						print("variety_m4 scelta is, ", variety_m4)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m4,))
						scenario_m4 = db_self.curs.fetchone()[0]
						print("######## scenario_m4 variety dependent non a caso {}".format(scenario_m4))
						if variety_m3=='SUNDRY' or variety_m4!=variety_m3:
							break
						else:	
							ran4 = random.randint(0,3)
							if ran4==0:
								random_var_m4 = 1
					if random_var_m4!=0:
						variety_m4 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m4 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m4))
						break
		###########################
		###########################
		###########################
	else:
		tikiqu_sharing = 0
		tiki_sharing = 0
		tiqu_sharing = 0
		kiqu_sharing = 0
		ok_end = 0
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999:
			print("bugia tripla")
			print("TYPE GAME KIND and also question GIVEN FROM IMPOSITIONSLIST!!")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			############## nel caso in cui esistessero non sia possibile trovare lo scenario valido per i 3, cerco 2 ...altrimenti no vince 3 !!! nel caso di un solo match!! nel caso di 2 pure!!
			###########cambia se sono di piu in questo caso gli faccio trovare tutti i tk kq e tq cmq anche se il 3 non c'è, e poi lo spalmo volendo su più match...per esempio quando la complessita e minore li spalmo altrimenti no!!!
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
				if db_self.imposed_kind_game!='4P':
					tkq_inter = list(set(tq_inter) & set(resp_kind))						
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tkq_inter = list(set(tq_inter) & set(resp4psu))
							if not tkq_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tkq_inter = list(set(tq_inter) & set(resp4pco))
							if not tkq_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tkq_inter:
					random.shuffle(tkq_inter)
					one_ran_scenario_def = random.choices(tkq_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kitiqu_sharing = 1
					ok_end = 1
				else:
					if db_self.imposed_kind_game!='4P':
						tk_inter = list(set(resp_type) & set(resp_kind))
					else:
						fcase = random.randint(0,1)
						dstop = 1
						into_psu = 0
						into_pco = 0
						while dstop>0:
							if fcase==0:
								tk_inter = list(set(resp_type) & set(resp4psu))
								if not tk_inter and not into_pco:
									fcase = 1
									into_psu = 1
								else:
									dstop = 0
							if fcase==1:									
								tk_inter = list(set(resp_type) & set(resp4pco))
								if not tk_inter and not into_psu:
									fcase = 1
									into_pco = 1
								else:
									dstop = 0
					if tk_inter:
						random.shuffle(tk_inter)
						one_ran_scenario_def = random.choices(tk_inter,k = 1)
						print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
						kiti_sharing = 1
						ok_end = 1
					else:
						if db_self.imposed_kind_game!='4P':
							kq_inter = list(set(resp_ques) & set(resp_kind))
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tk_inter = list(set(resp_ques) & set(resp4psu))
									if not tk_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tk_inter = list(set(resp_ques) & set(resp4pco))
									if not tk_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if kq_inter:
							random.shuffle(kq_inter)
							one_ran_scenario_def = random.choices(kq_inter,k = 1)
							print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
							kiqu_sharing = 1
							ok_end = 1
			if not ok_end:
				################ search tiki
				if db_self.imposed_kind_game!='4P':
					tk_inter = list(set(resp_type) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_type) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_type) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tk_inter:
					random.shuffle(tk_inter)
					one_ran_scenario_def = random.choices(tk_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kiti_sharing = 1
				################ search kiqu
				if db_self.imposed_kind_game!='4P':
					kq_inter = list(set(resp_ques) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_ques) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_ques) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if kq_inter:
					random.shuffle(kq_inter)
					one_ran_scenario_def = random.choices(kq_inter,k = 1)
					print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
					kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				if kitiqu_sharing:
					print("######## scenario imposed da tutti e 3 {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety_m3 = db_self.imposed_kind_game
						else:
							variety_m3 = 'SUNDRY'
					elif scenario_m4==0:
						scenario_m4 = scenario
						if random_var==0:
							variety_m4 = db_self.imposed_kind_game
						else:
							variety_m4 = 'SUNDRY'
				elif kiti_sharing:
					print("######## scenario imposed tiki sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1	= scenario
						########################################################################
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m4==0:
						scenario_m4	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
				elif tiqu_sharing:
					print("######## scenario imposed tiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'							
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'	
					elif scenario_m4==0:
						scenario_m4 = scenario
						########################################################################
						random_var = random.randint(0,3)
						########################################################################
						if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
							random_var = 1
						########################################################################
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'	
				elif kiqu_sharing:
					print("######## scenario imposed kiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety_m3 = db_self.imposed_kind_game
						else:
							variety_m3 = 'SUNDRY'
					elif scenario_m4==0:
						scenario_m4 = scenario
						if random_var==0:
							variety_m4 = db_self.imposed_kind_game
						else:
							variety_m4 = 'SUNDRY'
		###########################
		###########################
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id==99999:
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			################ search tiki
			if db_self.imposed_kind_game!='4P':
				tk_inter = list(set(resp_type) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_type) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_type) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if tk_inter:
				random.shuffle(tk_inter)
				one_ran_scenario_def = random.choices(tk_inter,k = 1)
				print("one_ran_scenario_from_tk_inter ", one_ran_scenario_def)
				kiti_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiki sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m3 = scenario
					########################################################################
					if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
						random_var = 1
					########################################################################
					if random_var==0:
						variety_m3 = db_self.imposed_kind_game
					else:
						variety_m3 = 'SUNDRY'						
				elif scenario_m4==0:
					scenario_m4 = scenario
					if random_var==0:
						variety_m4 = db_self.imposed_kind_game
					else:
						variety_m4 = 'SUNDRY'	
		if db_self.imposed_type_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game=='':
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search tiqu
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
			print("one_ran_scenario_def in comune trovato....", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m3 = scenario
					if random_var==0:
						variety_m3 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m3 = 'SUNDRY'
				elif scenario_m4==0:
					scenario_m4 = scenario
					if random_var==0:
						variety_m4 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m4 = 'SUNDRY'
		if db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_type_game=='':
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search kiqu
			################ search kiqu
			if db_self.imposed_kind_game!='4P':
				kq_inter = list(set(resp_ques) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_ques) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_ques) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if kq_inter:
				random.shuffle(kq_inter)
				one_ran_scenario_def = random.choices(kq_inter,k = 1)
				print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
				kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato....", one_ran_scenario_def)
			if one_ran_scenario_def:				
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed kiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m3 = db_self.imposed_kind_game
					else:
						variety_m3 = 'SUNDRY'
				elif scenario_m4==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m4 = db_self.imposed_kind_game
					else:
						variety_m4 = 'SUNDRY'
		###########################
		if not one_ran_scenario_def and db_self.imposed_type_game!='':
			print("bugia singola 1")
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			random.shuffle(resp_type)
			scenario = random.choices(resp_type,k = 1)
			print("come stai a non andare caso s1 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal tipo!!! sce {}".format(scenario))
			db_self.suggested_scenario_from_t = copy.copy(scenario)
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					variety_m3 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m3 = 'SUNDRY'
			elif scenario_m4==0:
				scenario_m4 = scenario
				if random_var==0:
					variety_m4 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m4 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_kind_game!='':
			print("bugia singola 2")
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
				ran4p = random.randint(0,1)
				if ran4p:
					resp_kind = resp4pco
				else:
					resp_kind = resp4psu
			random.shuffle(resp_kind)
			scenario = random.choices(resp_kind,k = 1)
			print("come stai a non andare caso s2 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal kind is {}".format(scenario))
			db_self.suggested_scenario_from_k = copy.copy(scenario)
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = db_self.imposed_kind_game
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = db_self.imposed_kind_game
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					variety_m3 = db_self.imposed_kind_game
				else:
					variety_m3 = 'SUNDRY'
			elif scenario_m4==0:
				scenario_m4 = scenario
				if random_var==0:
					variety_m4 = db_self.imposed_kind_game
				else:
					variety_m4 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_ques_id!=99999:
			print("bugia singola 3")
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			random.shuffle(resp_ques)
			scenario = random.choices(resp_ques,k = 1)
			print("come stai a non andare caso s3 {} ".format(scenario))
			scenario = scenario[0]
			db_self.suggested_scenario_from_q = copy.copy(scenario)
			print("ho appena settato db_self.suggested_scenario_from_q = copy.copy(scenario) {}".format(db_self.suggested_scenario_from_q))
			print("######## scenario imposed dalla question!!! sce {}".format(scenario))
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			elif scenario_m4==0:
				scenario_m4 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m4 = db_self.curs.fetchone()[0]
				else:
					variety_m4 = 'SUNDRY'	
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	if 0 in (scenario_m1,scenario_m2,scenario_m3,scenario_m4):
		if scenario_m1==0:
			print("vado di percorso scenario m1 perche dopo tiutto1!! ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m1 = scenario_m3
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m3,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m2 = scenario_m3
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m3,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1				
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
		if scenario_m3==0: #non è stato imposto scenario m3
			print("vado di scena m3 è ancora a zero")
			random_var_m3 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
				random_var_m3 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m3 = scenario_m1
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m3 = scenario_m2
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			else:
				while True:
					if random_var_m3==0:
						print("difficulty_m3 {}".format(difficulty_m3))
						if difficulty_m3 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						print("variety_m3 scelta is, ", variety_m3)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario_m3 variety dependent non a caso {}".format(scenario_m3))
						if variety_m2=='SUNDRY' or variety_m3!=variety_m2:
							break
						else:
							ran3 = random.randint(0,3)
							if ran3==0:
								random_var_m3 = 1
					if random_var_m3!=0:
						variety_m3 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m3))
						break
		if scenario_m4==0: #non è stato imposto scenario m4
			print("vado di scanrio m4 perche ancora a zero")
			random_var_m4 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m4:
				random_var_m4 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m4 = scenario_m1
				if random_var_m4==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m4 = db_self.curs.fetchone()[0]
				else:
					variety_m4 = 'SUNDRY'
			elif depends==0 and scenario_m2!=0:
				scenario_m4 = scenario_m2
				if random_var_m4==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m4 = db_self.curs.fetchone()[0]
				else:
					variety_m4 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m4 = scenario_m3
				if random_var_m4==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m3,ki1,ki2,ki3,ki4,))
					variety_m4 = db_self.curs.fetchone()[0]
				else:
					variety_m4 = 'SUNDRY'
			else:
				while True:
					if random_var_m4==0:
						print("difficulty_m4 {}".format(difficulty_m4))
						if difficulty_m4 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m4 = random.choice(variety_ava)
						print("variety_m4 scelta is, ", variety_m4)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m4,))
						scenario_m4 = db_self.curs.fetchone()[0]
						print("######## scenario_m4 variety dependent non a caso {}".format(scenario_m4))
						if variety_m3=='SUNDRY' or variety_m4!=variety_m3:
							break
						else:	
							ran4 = random.randint(0,3)
							if ran4==0:
								random_var_m4 = 1						
					if random_var_m4!=0:
						variety_m4 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m4 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m4))
						break
	###########################################################
	may_swap = random.randint(0,4)
	if may_swap==0:
		scenario_m1, scenario_m2 = scenario_m2, scenario_m1
		variety_m1, variety_m2 = variety_m2, variety_m1
		print("ho fatto swap caso 0")
	if may_swap==1:
		scenario_m1, scenario_m3 = scenario_m3, scenario_m1
		variety_m1, variety_m3 = variety_m3, variety_m1
		print("ho fatto swap caso 1")
	if may_swap==2:
		scenario_m1, scenario_m4 = scenario_m4, scenario_m1
		variety_m1, variety_m4 = variety_m4, variety_m1
		print("ho fatto swap caso 2")
	else:
		print("non c'è stato swap")			
	###########################################################
	print("variety 1 is ==================================================================> ",variety_m1)
	print("variety 2 is ==================================================================> ",variety_m2)
	print("variety 3 is ==================================================================> ",variety_m3)
	print("variety 4 is ==================================================================> ",variety_m4)
	if variety_m1=='SUNDRY':
		a_crite = random.randint(0,5)
		if a_crite==0:
			criteria_arrangement_m1 = 'random'
		else:
			criteria_arrangement_m1 = 'regular'
	else:
		criteria_arrangement_m1 = 'ignore'
	if variety_m2=='SUNDRY':
		a_crite = random.randint(0,5)
		if a_crite==0:
			criteria_arrangement_m2 = 'random'
		else:
			criteria_arrangement_m2 = 'regular'
	else:
		criteria_arrangement_m2 = 'ignore'
	if variety_m3=='SUNDRY':
		a_crite = random.randint(0,5)
		if a_crite==0:
			criteria_arrangement_m3 = 'random'
		else:
			criteria_arrangement_m3 = 'regular'
	else:
		criteria_arrangement_m3 = 'ignore'
	if variety_m4=='SUNDRY':
		a_crite = random.randint(0,5)
		if a_crite==0:
			criteria_arrangement_m4 = 'random'
		else:
			criteria_arrangement_m4 = 'regular'
	else:
		criteria_arrangement_m4 = 'ignore'
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
		criteria_arrangement_m1 = 'random'
		db_self.already_done_imposed_mixed = 1 
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
		criteria_arrangement_m2 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################	
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
		criteria_arrangement_m3 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m4:
		criteria_arrangement_m4 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################
	dca.add_new_Match_table(db_self, difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m1, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m1, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m4, num_of_games_m4, variety_m4, order_diff_games_m4, order_quan_ques_m4, criteria_arrangement_m4, advisable_time, scenario_m4, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

cdef define_matches_when_complexity_is_normal(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games):
	advisable_time = 240
	one_ran_scenario_def = 0
	mix_imposed_in_m1 = 0
	print("TIPOOO db_self.suggested_type_game_from_q = sugge_res[0][0] {}".format(db_self.suggested_type_game_from_q))
	print("kind!!! db_self.suggested_kind_game_from_q = sugge_res[0][1] {}".format(db_self.suggested_kind_game_from_q))		
	print("NUM OF MATCHES INSIDE INSIDE INSIDE {}".format(num_of_matches))
	print("MA IMPOSED mixed C'È???????? ".format(db_self.imposed_mixed))
	if num_of_matches==1:
		if db_self.imposed_difficulty_match=='':
			difficulty_m = 'Normal_1'
		else:
			difficulty_m = db_self.imposed_difficulty_match
			db_self.already_done_imposed_difficulty_match = 1
		num_of_games = random.randint(1,3)
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games = random.randint(2,3)
			########################################################################			
			if db_self.suggest_num_games_more_than==3:
				num_of_games = 3
			else:
				num_of_games = random.randint(1,3)
		else:
			num_of_games = db_self.imposed_num_games	
			db_self.already_done_imposed_num_games = 1
			########################################################################
			if db_self.imposed_num_games==1:
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					num_of_games = random.randint(2,3)
					db_self.already_done_imposed_num_games = 0
			########################################################################			
		print("############################################################ QUANDO SONO ALL INTERNO DI NORMAL part_2!!! scenario take_given_scenario {}".format(db_self.imposed_scenario))
		if db_self.imposed_scenario!=0:
			scenario = db_self.imposed_scenario
			random_var = random.randint(0,3)
			if num_of_games==1:
				random_var = 0
			if random_var==0:
				db_self.curs.execute("SELECT kind FROM Scenarios_Kinds JOIN Scenarios USING(scenario_group) WHERE scenario_id = ? ORDER BY RANDOM() LIMIT 1",(scenario,))
				variety = db_self.curs.fetchone()[0]
			else:
				variety = 'SUNDRY'
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				variety=='SUNDRY'
			########################################################################				
		else:
			if db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and db_self.imposed_ques_id==99999:
				print("bugia 00")
				print("TYPE kind and question GAME NOT GIVEN AT ALL, SO CASUAL CHOICE SCENARIO!!")
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				if random_var==0:
					if difficulty_m in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety = random.choice(variety_ava)
					if difficulty_m in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety = random.choice(variety_ava)
					if difficulty_m=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety = random.choice(variety_ava)
					if difficulty_m=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety = random.choice(variety_ava)
					if difficulty_m=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety = random.choice(variety_ava)
					if difficulty_m=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety = random.choice(variety_ava)
					print("variety scelta is, ", variety)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety,))
					scenario = db_self.curs.fetchone()[0]
					print("######## scenario scelto causa variety vediamo{}".format(scenario))
				else:
					variety = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce vediamo{}".format(scenario))
			tikiqu_sharing = 0
			tiki_sharing = 0
			tiqu_sharing = 0
			kiqu_sharing = 0
			ok_end = 0
			if db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and db_self.imposed_ques_id==99999:
				print("bugia 00")
				print("TYPE kind and question GAME NOT GIVEN AT ALL, SO CASUAL CHOICE SCENARIO!!")
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				if random_var==0:
					if difficulty_m in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety = random.choice(variety_ava)
					if difficulty_m in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety = random.choice(variety_ava)
					if difficulty_m=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety = random.choice(variety_ava)
					if difficulty_m=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety = random.choice(variety_ava)
					if difficulty_m=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety = random.choice(variety_ava)
					if difficulty_m=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety = random.choice(variety_ava)
					print("variety scelta is, ", variety)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety,))
					scenario = db_self.curs.fetchone()[0]
					print("######## scenario scelto causa variety nuovo{}".format(scenario))
					print("######## scenario scelto causa variety nuovo{}".format(scenario))
					print("######## scenario scelto causa variety nuovo{}".format(scenario))
				else:
					variety = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto nuovo sce {}".format(scenario))
					print("######## scenario a caso scelto nuovo sce {}".format(scenario))
					print("######## scenario a caso scelto nuovo sce {}".format(scenario))
			###########################
			###########################
			###########################
			else:
				if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999:
					print("bugia tripla")
					print("TYPE GAME KIND and also question GIVEN FROM IMPOSITIONSLIST!!")
					####
					if db_self.imposed_type_game=='4PSU':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_type_game=='4PCO':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE type=?""",(db_self.imposed_type_game,))
						sce_type = db_self.curs.fetchall()
					resp_type = [item for sublist in sce_type for item in sublist]
					####
					if db_self.imposed_kind_game!='4P':
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
						USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
						scen_kind = db_self.curs.fetchall()
						resp_kind = [item for sublist in scen_kind for item in sublist]
					else:
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
						scen4psu = db_self.curs.fetchall()
						resp4psu = [item for sublist in scen4psu for item in sublist]
						scen4pco = db_self.curs.fetchall()
						resp4pco = [item for sublist in scen4pco for item in sublist]
					####												
					if db_self.imposed_ques_id in [3013,3014,3015,3016]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE question_id=?""",(db_self.imposed_ques_id,))
						sce_ques = db_self.curs.fetchall()
					resp_ques = [item for sublist in sce_ques for item in sublist]
					######
					############## nel caso in cui esistessero non sia possibile trovare lo scenario valido per i 3, cerco 2 ...altrimenti no vince 3 !!! nel caso di un solo match!! nel caso di 2 pure!!
					###########cambia se sono di piu in questo caso gli faccio trovare tutti i tk kq e tq cmq anche se il 3 non c'è, e poi lo spalmo volendo su più match...per esempio quando la complessita e minore li spalmo altrimenti no!!!
					tq_inter = list(set(resp_ques) & set(resp_type))
					if tq_inter:
						tiqu_sharing = 1
						print("share scen tiqu!")
						random.shuffle(tq_inter)
						one_ran_scenario_def = random.choices(tq_inter,k = 1)
						if db_self.imposed_kind_game!='4P':
							tkq_inter = list(set(tq_inter) & set(resp_kind))						
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tkq_inter = list(set(tq_inter) & set(resp4psu))
									if not tkq_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tkq_inter = list(set(tq_inter) & set(resp4pco))
									if not tkq_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if tkq_inter:
							random.shuffle(tkq_inter)
							one_ran_scenario_def = random.choices(tkq_inter,k = 1)
							print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
							kitiqu_sharing = 1
							ok_end = 1
						else:
							if db_self.imposed_kind_game!='4P':
								tk_inter = list(set(resp_type) & set(resp_kind))
							else:
								fcase = random.randint(0,1)
								dstop = 1
								into_psu = 0
								into_pco = 0
								while dstop>0:
									if fcase==0:
										tk_inter = list(set(resp_type) & set(resp4psu))
										if not tk_inter and not into_pco:
											fcase = 1
											into_psu = 1
										else:
											dstop = 0
									if fcase==1:									
										tk_inter = list(set(resp_type) & set(resp4pco))
										if not tk_inter and not into_psu:
											fcase = 1
											into_pco = 1
										else:
											dstop = 0
							if tk_inter:
								random.shuffle(tk_inter)
								one_ran_scenario_def = random.choices(tk_inter,k = 1)
								print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
								kiti_sharing = 1
								ok_end = 1
							else:
								if db_self.imposed_kind_game!='4P':
									kq_inter = list(set(resp_ques) & set(resp_kind))
								else:
									fcase = random.randint(0,1)
									dstop = 1
									into_psu = 0
									into_pco = 0
									while dstop>0:
										if fcase==0:
											tk_inter = list(set(resp_ques) & set(resp4psu))
											if not tk_inter and not into_pco:
												fcase = 1
												into_psu = 1
											else:
												dstop = 0
										if fcase==1:									
											tk_inter = list(set(resp_ques) & set(resp4pco))
											if not tk_inter and not into_psu:
												fcase = 1
												into_pco = 1
											else:
												dstop = 0
								if kq_inter:
									random.shuffle(kq_inter)
									one_ran_scenario_def = random.choices(kq_inter,k = 1)
									print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
									kiqu_sharing = 1
									ok_end = 1
					if not ok_end:
						################ search tiki
						if db_self.imposed_kind_game!='4P':
							tk_inter = list(set(resp_type) & set(resp_kind))
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tk_inter = list(set(resp_type) & set(resp4psu))
									if not tk_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tk_inter = list(set(resp_type) & set(resp4pco))
									if not tk_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if tk_inter:
							random.shuffle(tk_inter)
							one_ran_scenario_def = random.choices(tk_inter,k = 1)
							print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
							kiti_sharing = 1
						################ search kiqu
						if db_self.imposed_kind_game!='4P':
							kq_inter = list(set(resp_ques) & set(resp_kind))
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tk_inter = list(set(resp_ques) & set(resp4psu))
									if not tk_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tk_inter = list(set(resp_ques) & set(resp4pco))
									if not tk_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if kq_inter:
							random.shuffle(kq_inter)
							one_ran_scenario_def = random.choices(kq_inter,k = 1)
							print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
							kiqu_sharing = 1
					print("one_ran_scenario_def in comune trovato....", one_ran_scenario_def)
					if one_ran_scenario_def:
						random_var = random.randint(0,3)
						########################################################################
						if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
							random_var = 1
						########################################################################
						scenario = one_ran_scenario_def[0]
						print("scenario trovato {} ".format(scenario))
						if kitiqu_sharing:
							print("######## scenario imposed da tutti e 3 {}".format(scenario))
							db_self.suggested_scenario_from_k = copy.copy(scenario)
							db_self.suggested_scenario_from_t = copy.copy(scenario)
							db_self.suggested_scenario_from_q = copy.copy(scenario)
							if random_var==0:
								variety = db_self.imposed_kind_game
							else:
								variety = 'SUNDRY'
						elif kiti_sharing:
							print("######## scenario imposed tiki sce {}".format(scenario))
							db_self.suggested_scenario_from_k = copy.copy(scenario)
							db_self.suggested_scenario_from_t = copy.copy(scenario)
							if random_var==0:
								variety = db_self.imposed_kind_game
							else:
								variety = 'SUNDRY'
						elif tiqu_sharing:
							print("######## scenario imposed tiqu sce {}".format(scenario))
							db_self.suggested_scenario_from_t = copy.copy(scenario)
							db_self.suggested_scenario_from_q = copy.copy(scenario)
							if random_var==0:
								variety = dbem2.find_kind(db_self.imposed_type_game)
							else:
								variety = 'SUNDRY'
						elif kiqu_sharing:
							print("######## scenario imposed kiqu sce {}".format(scenario))
							db_self.suggested_scenario_from_k = copy.copy(scenario)
							db_self.suggested_scenario_from_q = copy.copy(scenario)
							if random_var==0:
								variety = db_self.imposed_kind_game
							else:
								variety = 'SUNDRY'
				###########################
				###########################
				if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id==99999:
					####
					if db_self.imposed_type_game=='4PSU':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_type_game=='4PCO':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE type=?""",(db_self.imposed_type_game,))
						sce_type = db_self.curs.fetchall()
					resp_type = [item for sublist in sce_type for item in sublist]
					####
					if db_self.imposed_kind_game!='4P':
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
						USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
						scen_kind = db_self.curs.fetchall()
						resp_kind = [item for sublist in scen_kind for item in sublist]
					else:
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
						scen4psu = db_self.curs.fetchall()
						resp4psu = [item for sublist in scen4psu for item in sublist]
						scen4pco = db_self.curs.fetchall()
						resp4pco = [item for sublist in scen4pco for item in sublist]
					################ search tiki
					if db_self.imposed_kind_game!='4P':
						tk_inter = list(set(resp_type) & set(resp_kind))
					else:
						fcase = random.randint(0,1)
						dstop = 1
						into_psu = 0
						into_pco = 0
						while dstop>0:
							if fcase==0:
								tk_inter = list(set(resp_type) & set(resp4psu))
								if not tk_inter and not into_pco:
									fcase = 1
									into_psu = 1
								else:
									dstop = 0
							if fcase==1:									
								tk_inter = list(set(resp_type) & set(resp4pco))
								if not tk_inter and not into_psu:
									fcase = 1
									into_pco = 1
								else:
									dstop = 0
					if tk_inter:
						random.shuffle(tk_inter)
						one_ran_scenario_def = random.choices(tk_inter,k = 1)
						print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
						kiti_sharing = 1
					print("one_ran_scenario_def in comune trovato....", one_ran_scenario_def)
					if one_ran_scenario_def:
						scenario = one_ran_scenario_def[0]
						print("scenario trovato {} ".format(scenario))
						print("######## scenario imposed tiki sce {}".format(scenario))
						db_self.suggested_scenario_from_k = copy.copy(scenario)
						db_self.suggested_scenario_from_t = copy.copy(scenario)
						random_var = random.randint(0,3)
						########################################################################
						if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
							random_var = 1
						########################################################################
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
						########################################################################
				if db_self.imposed_type_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game=='':
					####
					if db_self.imposed_type_game=='4PSU':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_type_game=='4PCO':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE type=?""",(db_self.imposed_type_game,))
						sce_type = db_self.curs.fetchall()
					resp_type = [item for sublist in sce_type for item in sublist]
					####												
					if db_self.imposed_ques_id in [3013,3014,3015,3016]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE question_id=?""",(db_self.imposed_ques_id,))
						sce_ques = db_self.curs.fetchall()
					resp_ques = [item for sublist in sce_ques for item in sublist]
					########################### search tiqu
					tq_inter = list(set(resp_ques) & set(resp_type))
					if tq_inter:
						tiqu_sharing = 1
						print("share scen tiqu!")
						random.shuffle(tq_inter)
						one_ran_scenario_def = random.choices(tq_inter,k = 1)
					print("one_ran_scenario_def in comune trovato....", one_ran_scenario_def)
					if one_ran_scenario_def:
						scenario = one_ran_scenario_def[0]
						print("scenario trovato {} ".format(scenario))
						print("######## scenario imposed tiqu sce {}".format(scenario))
						db_self.suggested_scenario_from_t = copy.copy(scenario)
						db_self.suggested_scenario_from_q = copy.copy(scenario)
						random_var = random.randint(0,3)
						########################################################################
						if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
							random_var = 1
						########################################################################
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'
						########################################################################
				if db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_type_game=='':
					####
					if db_self.imposed_kind_game!='4P':
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
						USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
						scen_kind = db_self.curs.fetchall()
						resp_kind = [item for sublist in scen_kind for item in sublist]
					else:
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
						scen4psu = db_self.curs.fetchall()
						resp4psu = [item for sublist in scen4psu for item in sublist]
						scen4pco = db_self.curs.fetchall()
						resp4pco = [item for sublist in scen4pco for item in sublist]
					####												
					if db_self.imposed_ques_id in [3013,3014,3015,3016]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE question_id=?""",(db_self.imposed_ques_id,))
						sce_ques = db_self.curs.fetchall()
					resp_ques = [item for sublist in sce_ques for item in sublist]
					########################### search kiqu
					################ search kiqu
					if db_self.imposed_kind_game!='4P':
						kq_inter = list(set(resp_ques) & set(resp_kind))
					else:
						fcase = random.randint(0,1)
						dstop = 1
						into_psu = 0
						into_pco = 0
						while dstop>0:
							if fcase==0:
								tk_inter = list(set(resp_ques) & set(resp4psu))
								if not tk_inter and not into_pco:
									fcase = 1
									into_psu = 1
								else:
									dstop = 0
							if fcase==1:									
								tk_inter = list(set(resp_ques) & set(resp4pco))
								if not tk_inter and not into_psu:
									fcase = 1
									into_pco = 1
								else:
									dstop = 0
					if kq_inter:
						random.shuffle(kq_inter)
						one_ran_scenario_def = random.choices(kq_inter,k = 1)
						print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
						kiqu_sharing = 1
					print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
					if one_ran_scenario_def:
						scenario = one_ran_scenario_def[0]
						print("scenario trovato {} ".format(scenario))
						print("######## scenario imposed kiqu sce {}".format(scenario))
						db_self.suggested_scenario_from_k = copy.copy(scenario)
						db_self.suggested_scenario_from_q = copy.copy(scenario)
						random_var = random.randint(0,3)
						########################################################################
						if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
							random_var = 1
						########################################################################
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
						########################################################################
				###########################
				if not one_ran_scenario_def and db_self.imposed_type_game!='':
					print("bugia singola 1")
					####
					if db_self.imposed_type_game=='4PSU':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_type_game=='4PCO':
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE type=?""",(db_self.imposed_type_game,))
						sce_type = db_self.curs.fetchall()
					resp_type = [item for sublist in sce_type for item in sublist]
					random.shuffle(resp_type)
					print("resp_type", resp_type)
					print("resp_type", resp_type)
					print("resp_type", resp_type)
					print("resp_type", resp_type)
					scenario = random.choices(resp_type,k = 1)
					print("come stai a non andare caso s1 {} ".format(scenario))
					scenario = scenario[0]
					print("######## scenario imposed dal tipo!!! sce {}".format(scenario))
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					random_var = random.randint(0,3)
					########################################################################
					if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
						random_var = 1
					########################################################################
					if random_var==0:
						variety = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety = 'SUNDRY'
				###########################
				if not one_ran_scenario_def and db_self.imposed_kind_game!='':
					print("bugia singola 2")
					if db_self.imposed_kind_game!='4P':
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
						USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
						scen_kind = db_self.curs.fetchall()
						resp_kind = [item for sublist in scen_kind for item in sublist]
					else:
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
						db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
						scen4psu = db_self.curs.fetchall()
						resp4psu = [item for sublist in scen4psu for item in sublist]
						scen4pco = db_self.curs.fetchall()
						resp4pco = [item for sublist in scen4pco for item in sublist]
						ran4p = random.randint(0,1)
						if ran4p:
							resp_kind = resp4pco
						else:
							resp_kind = resp4psu
					random.shuffle(resp_kind)
					print("resp_kind",resp_kind)
					print("resp_kind",resp_kind)
					print("resp_kind",resp_kind)
					print("resp_kind",resp_kind)
					print("resp_kind",resp_kind)
					scenario = random.choices(resp_kind,k = 1)
					print("come stai a non andare caso s2 {} ".format(scenario))
					scenario = scenario[0]
					print("######## scenario imposed dal kind is {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					random_var = random.randint(0,3)
					########################################################################
					if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
						random_var = 1
					########################################################################
					if random_var==0:
						variety = db_self.imposed_kind_game
					else:
						variety = 'SUNDRY'
					########################################################################
				###########################
				if not one_ran_scenario_def and db_self.imposed_ques_id!=99999:
					print("bugia singola 3")
					####												
					if db_self.imposed_ques_id in [3013,3014,3015,3016]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
						db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
						sce_type = db_self.curs.fetchall()
					else:
						db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
						WHERE question_id=?""",(db_self.imposed_ques_id,))
						sce_ques = db_self.curs.fetchall()
					resp_ques = [item for sublist in sce_ques for item in sublist]
					######
					random.shuffle(resp_ques)
					print("resp_ques", resp_ques)
					print("resp_ques", resp_ques)
					print("resp_ques", resp_ques)
					print("resp_ques", resp_ques)
					print("resp_ques", resp_ques)
					scenario = random.choices(resp_ques,k = 1)
					print("come stai a non andare caso s3 {} ".format(scenario))
					scenario = scenario[0]
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					print("ho appena settato db_self.suggested_scenario_from_q = copy.copy(scenario) {}".format(db_self.suggested_scenario_from_q))
					print("######## scenario imposed dalla question!!! sce {}".format(scenario))
					random_var = random.randint(0,3)
					########################################################################
					if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
						random_var = 1
					########################################################################
					if random_var==0:
						db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
						variety = db_self.curs.fetchone()[0]
					else:
						variety = 'SUNDRY'
				###########################

		print("variety is ==================================================================> ",variety)
		if variety=='SUNDRY':
			criteria_arrangement = 'regular'
		else:
			criteria_arrangement = 'ignore'
		########################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
			criteria_arrangement = 'random'
			db_self.already_done_imposed_mixed = 1 
		########################################################################
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games = 'same' 
		if ord_casual in [2,3]:
			order_diff_games = 'asc'
		elif ord_casual==4:
			order_diff_games = 'casu'
		########################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
			num_of_games = 1
			db_self.already_done_imposed_mixed = 1
		########################################################################
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques = 'asc'
		elif ord_casual==4:
			order_quan_ques = 'casu'
		dca.add_new_Match_table(db_self, difficulty_m, num_of_games, variety, order_diff_games, order_quan_ques, criteria_arrangement, advisable_time, scenario, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last = db_self.curs.fetchone()[0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
	
	elif num_of_matches==2:
		def_matches_when_complexity_is_normal_2(db_self, num_of_matches, sess_last, order_diff_matches, order_quan_games)
	elif num_of_matches==3:
		def_matches_when_complexity_is_normal_3(db_self, num_of_matches, sess_last, order_diff_matches, order_quan_games)
	elif num_of_matches==4:
		def_matches_when_complexity_is_normal_4(db_self, num_of_matches, sess_last, order_diff_matches, order_quan_games)
cdef def_matches_when_complexity_is_normal_2(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games):
	advisable_time = 240
	one_ran_scenario_def = 0
	mix_imposed_in_m1 = 0
	mix_imposed_in_m2 = 0
	options = ['Normal_1', 'Normal_2']		
	if db_self.imposed_difficulty_match=='' or (db_self.imposed_difficulty_match!='' and db_self.already_done_imposed_difficulty_match==1):
		if order_diff_matches=='same':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = difficulty_m1
		if order_diff_matches=='casu':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
		if order_diff_matches=='asc':
			difficulty_m1 = 'Normal_1'
			difficulty_m2 = 'Normal_2'
		if order_diff_matches=='desc':
			difficulty_m1 = 'Normal_2'
			difficulty_m2 = 'Normal_1'				
	else:
		db_self.already_done_imposed_difficulty_match = 1
		if order_diff_matches=='same':
			difficulty_m1 = db_self.imposed_difficulty_match
			difficulty_m2 = difficulty_m1
		if order_diff_matches=='casu':
			ran_ran = random.randint(0,1)
			if ran_ran==0:
				difficulty_m1 = db_self.imposed_difficulty_match
				difficulty_m2 = random.choice(options)
			else:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = db_self.imposed_difficulty_match
		if order_diff_matches=='asc':
			if db_self.imposed_difficulty_match=='Normal_1' or db_self.imposed_difficulty_match=='Normal_2':
				difficulty_m1 = 'Normal_1'
				difficulty_m2 = 'Normal_2'
			else:
				ran_2 = random.randint(0,1)
				if ran_2:
					difficulty_m1 = 'Normal_1'
					difficulty_m2 = db_self.imposed_difficulty_match
				else:
					difficulty_m1 = 'Normal_2'
					difficulty_m2 = db_self.imposed_difficulty_match
		if order_diff_matches=='desc':
			if db_self.imposed_difficulty_match=='Normal_1' or db_self.imposed_difficulty_match=='Normal_2':
				difficulty_m1 = 'Normal_2'
				difficulty_m2 = 'Normal_1'
			else:
				ran_2 = random.randint(0,1)
				if ran_2:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Normal_1'
				else:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Normal_2'
	########################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
		if order_diff_matches=='same':
			difficulty_m1 = 'Medium_2'
			mix_imposed_in_m1 = 1
			difficulty_m2 = 'Medium_2'
			mix_imposed_in_m2 = 1			
		if order_diff_matches=='casu':
			rand_change = random.randint(1,2)
			if rand_change==1:
				difficulty_m1 = 'Medium_2'
				mix_imposed_in_m1 = 1
			if rand_change==2:
				difficulty_m2 = 'Medium_2'
				mix_imposed_in_m2 = 1			
		if order_diff_matches=='asc':
			difficulty_m2 = 'Medium_2'
			mix_imposed_in_m2 = 1			
		if order_diff_matches=='desc':
			difficulty_m1 = 'Medium_2'
			mix_imposed_in_m1 = 1
	########################################################################
	if order_quan_games=='same':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			if db_self.suggest_num_games_more_than==3:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
			else:
				casual = random.randint(0,3)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
		else:
			db_self.already_done_imposed_num_games = 1
			num_of_games_m1 = db_self.imposed_num_games
			num_of_games_m2 = db_self.imposed_num_games
		########################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
			num_of_games_m1 = 4
			num_of_games_m2 = 2
			db_self.already_done_imposed_num_games = 0
		########################################################################					
	elif order_quan_games=='casu':
		if db_self.suggest_num_games_more_than==3:
			depends = random.randint(0,4)
			if depends==0:
				num_of_games_m1 = 1
				num_of_games_m2 = 3
			if depends==1:
				num_of_games_m1 = 3 
				num_of_games_m2 = 1
			if depends==2:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
			if depends==3:
				num_of_games_m1 = 2
				num_of_games_m2 = 3
			if depends==4:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
		else:
			num_of_games_m1 = random.randint(1,3)
			num_of_games_m2 = random.randint(1,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = random.randint(2,4)
			########################################################################				
		if db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==0:
			num_of_games_m1 = db_self.imposed_num_games
			db_self.already_done_imposed_num_games = 1
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 4
				num_of_games_m2 = 2
				db_self.already_done_imposed_num_games = 0
			########################################################################			
	elif order_quan_games=='asc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			if db_self.suggest_num_games_more_than==3:
				casual = random.randint(0,2)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==3:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
			else:
				casual = random.randint(0,2)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				elif casual==3:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 2
				num_of_games_m2 = 3
			########################################################################							
		else:
			db_self.already_done_imposed_num_games = 1
			if db_self.imposed_num_games==1:
				casual = random.randint(0,1)
				if casual:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				else:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
			if db_self.imposed_num_games==2:
				casual = random.randint(0,1)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
				elif casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
			if db_self.imposed_num_games==3:
				casual = random.randint(0,1)
				if casual:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
				else:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
			if db_self.imposed_num_games==4:
				casual = random.randint(0,2)
				if casual==0:
					num_of_games_m1 = 1
					num_of_games_m2 = 4
				elif casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 4
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 4
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 2
				num_of_games_m2 = 3
				db_self.already_done_imposed_num_games = 0
			########################################################################							
	elif order_quan_games=='desc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			if db_self.suggest_num_games_more_than==3:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				elif casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
			else:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				elif casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
				########################################################################						
		else:
			db_self.already_done_imposed_num_games = 1
			if db_self.imposed_num_games==1:
				casual = random.randint(0,2)
				if casual==0 or casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
			if db_self.imposed_num_games==2:
				casual = random.randint(0,2)
				if casual==0 or casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
			if db_self.imposed_num_games==3:
				casual = random.randint(0,2)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
			if db_self.imposed_num_games==4:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 4
					num_of_games_m2 = 2
				elif casual==2:
					num_of_games_m1 = 4
					num_of_games_m2 = 1
				elif casual==3:
					num_of_games_m1 = 4
					num_of_games_m2 = 3					
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
				db_self.already_done_imposed_num_games = 0
			########################################################################
	if difficulty_m1 in ['Easy_1','Easy_2']:
		options = ['same', 'asc']
		order_diff_games_m1 = random.choice(options)
		order_quan_ques_m1 = random.choice(options)
	if difficulty_m1=='Normal_1':
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m1 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m1 = 'asc' 
		elif ord_casual==4:
			order_diff_games_m1 = 'casu' 
		options = ['same', 'asc']
		order_quan_ques_m1 = random.choice(options)
	if difficulty_m1=='Normal_2':
		options = ['same', 'asc', 'casu']
		order_diff_games_m1 = random.choice(options)
		order_quan_ques_m1 = random.choice(options)
	if difficulty_m1 in ['Medium_1','Medium_2','Hard_1','Hard_2']:
		ord_casual = random.randint(0,5)
		if ord_casual in [0,1,2]:
			order_diff_games_m1 = 'same' 
		if ord_casual in [3,4]:
			order_diff_games_m1 = 'asc' 
		else:
			order_diff_games_m1 = 'desc' 
		ord_casual = random.randint(0,5)
		if ord_casual in [0,1,2]:
			order_quan_ques_m1 = 'same' 
		if ord_casual in [3,4]:
			order_quan_ques_m1 = 'asc' 
		else:
			order_quan_ques_m1 = 'desc' 
	###
	if difficulty_m2 in ['Easy_1','Easy_2']:
		options = ['same', 'asc']
		order_diff_games_m2 = random.choice(options)
		order_quan_ques_m2 = random.choice(options)		
	if difficulty_m2=='Normal_1':
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m2 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m2 = 'asc' 
		elif ord_casual==4:
			order_diff_games_m2 = 'casu'
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m2 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m2 = 'asc' 
		elif ord_casual==4:
			order_quan_ques_m2 = 'casu'
	if difficulty_m2=='Normal_2':
		options = ['same', 'asc', 'casu']
		order_diff_games_m2 = random.choice(options)
		order_quan_ques_m2 = random.choice(options)
	if difficulty_m2 in ['Medium_1','Medium_2','Hard_1','Hard_2']:
		ord_casual = random.randint(0,5)
		if ord_casual in [0,1,2]:
			order_diff_games_m2 = 'same' 
		if ord_casual in [3,4]:
			order_diff_games_m2 = 'asc' 
		else:
			order_diff_games_m2 = 'desc' 
		ord_casual = random.randint(0,5)
		if ord_casual in [0,1,2]:
			order_quan_ques_m2 = 'same' 
		if ord_casual in [3,4]:
			order_quan_ques_m2 = 'asc' 
		else:
			order_quan_ques_m2 = 'desc' 
	#################################################################################################################################################################################
	#scenario imposed can have one positon among 1,2,3 match
	print("############################################################ part_2!!! scenario take_given_scenario {}".format(db_self.imposed_scenario))
	scenario_m1 = 0
	scenario_m2 = 0
	variety_m1 = ''
	variety_m2 = ''
	print("IMPOSED!!!!!! RANMA!!! ".format(db_self.imposed_scenario))
	if db_self.imposed_scenario!=0:
		random_var = random.randint(0,3)
		sundry_or_random_m1 = random.randint(0,2)
		sundry_or_random_m2 = random.randint(0,2)
		db_self.curs.execute("SELECT kind FROM Scenarios_Kinds JOIN Scenarios USING(scenario_group) WHERE scenario_id = ? ORDER BY RANDOM() LIMIT 1",(db_self.imposed_scenario,))	
		if random_var==0:
			variety_m1 = db_self.curs.fetchone()[0]
			scenario_m1 = db_self.imposed_scenario
		if random_var==1:
			variety_m2 = db_self.curs.fetchone()[0]
			scenario_m2 = db_self.imposed_scenario
		##################################################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
			variety_m1 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
			variety_m2 = 'SUNDRY'
		##################################################################################################						
	if db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and db_self.imposed_ques_id==99999:
		print("bugia 00")
		print("TYPE GAME NOT GIVEN AT ALL, SO CASUAL CHOICE SCENARIO!!")
		if scenario_m1==0: #non è stato imposto scenario m1
			print("vado di percorso scenario m1 perche ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1					
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break					
		###########################
		###########################
		###########################
	else:
		tikiqu_sharing = 0
		tiki_sharing = 0
		tiqu_sharing = 0
		kiqu_sharing = 0
		ok_end = 0
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999:
			print("bugia tripla")
			print("TYPE GAME KIND and also question GIVEN FROM IMPOSITIONSLIST!!")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			############## nel caso in cui esistessero non sia possibile trovare lo scenario valido per i 3, cerco 2 ...altrimenti no vince 3 !!! nel caso di un solo match!! nel caso di 2 pure!!
			###########cambia se sono di piu in questo caso gli faccio trovare tutti i tk kq e tq cmq anche se il 3 non c'è, e poi lo spalmo volendo su più match...per esempio quando la complessita e minore li spalmo altrimenti no!!!
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
				if db_self.imposed_kind_game!='4P':
					tkq_inter = list(set(tq_inter) & set(resp_kind))						
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tkq_inter = list(set(tq_inter) & set(resp4psu))
							if not tkq_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tkq_inter = list(set(tq_inter) & set(resp4pco))
							if not tkq_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tkq_inter:
					random.shuffle(tkq_inter)
					one_ran_scenario_def = random.choices(tkq_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kitiqu_sharing = 1
					ok_end = 1
				else:
					if db_self.imposed_kind_game!='4P':
						tk_inter = list(set(resp_type) & set(resp_kind))
					else:
						fcase = random.randint(0,1)
						dstop = 1
						into_psu = 0
						into_pco = 0
						while dstop>0:
							if fcase==0:
								tk_inter = list(set(resp_type) & set(resp4psu))
								if not tk_inter and not into_pco:
									fcase = 1
									into_psu = 1
								else:
									dstop = 0
							if fcase==1:									
								tk_inter = list(set(resp_type) & set(resp4pco))
								if not tk_inter and not into_psu:
									fcase = 1
									into_pco = 1
								else:
									dstop = 0
					if tk_inter:
						random.shuffle(tk_inter)
						one_ran_scenario_def = random.choices(tk_inter,k = 1)
						print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
						kiti_sharing = 1
						ok_end = 1
					else:
						if db_self.imposed_kind_game!='4P':
							kq_inter = list(set(resp_ques) & set(resp_kind))
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tk_inter = list(set(resp_ques) & set(resp4psu))
									if not tk_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tk_inter = list(set(resp_ques) & set(resp4pco))
									if not tk_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if kq_inter:
							random.shuffle(kq_inter)
							one_ran_scenario_def = random.choices(kq_inter,k = 1)
							print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
							kiqu_sharing = 1
							ok_end = 1
			if not ok_end:
				################ search tiki
				if db_self.imposed_kind_game!='4P':
					tk_inter = list(set(resp_type) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_type) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_type) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tk_inter:
					random.shuffle(tk_inter)
					one_ran_scenario_def = random.choices(tk_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kiti_sharing = 1
				################ search kiqu
				if db_self.imposed_kind_game!='4P':
					kq_inter = list(set(resp_ques) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_ques) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_ques) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if kq_inter:
					random.shuffle(kq_inter)
					one_ran_scenario_def = random.choices(kq_inter,k = 1)
					print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
					kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				if kitiqu_sharing:
					print("######## scenario imposed da tutti e 3 {}".format(scenario))
					random_var = random.randint(0,3)
					########################################################################
					if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
						random_var = 1
					########################################################################
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
				elif kiti_sharing:
					print("######## scenario imposed tiki sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
				elif tiqu_sharing:
					print("######## scenario imposed tiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'							
				elif kiqu_sharing:
					print("######## scenario imposed kiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
		###########################
		###########################
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id==99999:
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			################ search tiki
			if db_self.imposed_kind_game!='4P':
				tk_inter = list(set(resp_type) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_type) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_type) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if tk_inter:
				random.shuffle(tk_inter)
				one_ran_scenario_def = random.choices(tk_inter,k = 1)
				print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
				kiti_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiki sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
		if db_self.imposed_type_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game=='':
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search tiqu
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				scenario = one_ran_scenario_def[0]
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m2 = 'SUNDRY'
		if db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_type_game=='':
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search kiqu
			################ search kiqu
			if db_self.imposed_kind_game!='4P':
				kq_inter = list(set(resp_ques) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_ques) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_ques) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if kq_inter:
				random.shuffle(kq_inter)
				one_ran_scenario_def = random.choices(kq_inter,k = 1)
				print("one_ran_scenario_from_tq_inter ", one_ran_scenario_def)
				kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed kiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
		###########################
		if not one_ran_scenario_def and db_self.imposed_type_game!='':
			print("bugia singola 1")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			print("sce_type",sce_type)
			print("sce_type",sce_type)
			print("sce_type",sce_type)
			resp_type = [item for sublist in sce_type for item in sublist]
			random.shuffle(resp_type)
			print("resp_type",resp_type)
			print("resp_type",resp_type)
			print("resp_type",resp_type)
			scenario = random.choices(resp_type,k = 1)
			print("come stai a non andare caso s1 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal tipo!!! sce {}".format(scenario))
			db_self.suggested_scenario_from_t = copy.copy(scenario)
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m2 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_kind_game!='':
			print("bugia singola 2")
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
				ran4p = random.randint(0,1)
				if ran4p:
					resp_kind = resp4pco
				else:
					resp_kind = resp4psu
			random.shuffle(resp_kind)
			print("resp_kind",resp_kind)
			print("resp_kind",resp_kind)
			scenario = random.choices(resp_kind,k = 1)
			print("come stai a non andare caso s2 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal kind is {}".format(scenario))
			db_self.suggested_scenario_from_k = copy.copy(scenario)
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = db_self.imposed_kind_game
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = db_self.imposed_kind_game
				else:
					variety_m2 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_ques_id!=99999:
			print("bugia singola 3")
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			print("sce_ques", sce_ques)
			print("sce_ques", sce_ques)
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			random.shuffle(resp_ques)
			print("resp_ques", resp_ques)
			print("resp_ques", resp_ques)
			scenario = random.choices(resp_ques,k = 1)
			print("come stai a non andare caso s3 {} ".format(scenario))
			scenario = scenario[0]
			db_self.suggested_scenario_from_q = copy.copy(scenario)
			print("ho appena settato db_self.suggested_scenario_from_q = copy.copy(scenario) {}".format(db_self.suggested_scenario_from_q))
			print("######## scenario imposed dalla question!!! sce {}".format(scenario))
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	if 0 in (scenario_m1,scenario_m2):
		if scenario_m1==0:
			print("vado di percorso scenario m1 perche dopo tutto ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))		
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1	
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
	###########################################################
	may_swap = random.randint(0,2)
	if may_swap==0:
		scenario_m1, scenario_m2 = scenario_m2, scenario_m1
		variety_m1, variety_m2 = variety_m2, variety_m1
		print("ho fatto swap caso 0")
	###########################################################
	print("variety 1 is ==================================================================> ",variety_m1)
	print("variety 2 is ==================================================================> ",variety_m2)
	if variety_m1=='SUNDRY':
		criteria_arrangement_m1 = 'regular'
	else:
		criteria_arrangement_m1 = 'ignore'
	if variety_m2=='SUNDRY':
		criteria_arrangement_m2 = 'regular'
	else:
		criteria_arrangement_m2 = 'ignore'
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
		criteria_arrangement_m1 = 'random'
		db_self.already_done_imposed_mixed = 1 
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
		criteria_arrangement_m2 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################
	print("scenario_m2scenario_m2scenario_m2scenario_m2scenario_m2scenario_m2scenario_m2scenario_m2scenario_m2 {}".format(scenario_m2))
	dca.add_new_Match_table(db_self, difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m1, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)					
cdef def_matches_when_complexity_is_normal_3(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games):
	advisable_time = 240
	one_ran_scenario_def = 0
	mix_imposed_in_m1 = 0
	mix_imposed_in_m2 = 0
	mix_imposed_in_m3 = 0
	options = ['Easy_2','Normal_1','Normal_2']
	if db_self.imposed_difficulty_match=='' or (db_self.imposed_difficulty_match!='' and db_self.already_done_imposed_difficulty_match==1):
		if order_diff_matches=='same':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = difficulty_m1
			difficulty_m3 = difficulty_m1
		if order_diff_matches=='casu':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
		if order_diff_matches=='asc':
			difficulty_m1 = 'Easy_2'
			difficulty_m2 = 'Normal_1'
			difficulty_m3 = 'Normal_2'
		if order_diff_matches=='desc':
			difficulty_m1 = 'Normal_2'
			difficulty_m2 = 'Normal_1'				
			difficulty_m3 = 'Easy_2'				
	else:
		db_self.already_done_imposed_difficulty_match = 1
		if order_diff_matches=='same':
			difficulty_m1 = db_self.imposed_difficulty_match
			difficulty_m2 = difficulty_m1
			difficulty_m3 = difficulty_m1
		if order_diff_matches=='casu':
			ran_ran = random.randint(0,2)
			if ran_ran==0:
				difficulty_m1 = db_self.imposed_difficulty_match
				difficulty_m2 = random.choice(options)
				difficulty_m3 = random.choice(options)
			if ran_ran==1:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = random.choice(options)
				difficulty_m3 = db_self.imposed_difficulty_match
			if ran_ran==2:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = db_self.imposed_difficulty_match
				difficulty_m3 = random.choice(options)
		if order_diff_matches=='asc':
			if db_self.imposed_difficulty_match=='Easy_2' or db_self.imposed_difficulty_match=='Normal_1' or db_self.imposed_difficulty_match=='Normal_2':
				difficulty_m1 = 'Easy_2'
				difficulty_m2 = 'Normal_1'
				difficulty_m3 = 'Normal_2'
			else:
				ran_2 = random.randint(0,1)
				if ran_2==0:
					difficulty_m1 = 'Easy_2'
					difficulty_m2 = 'Easy_2'
					difficulty_m3 = db_self.imposed_difficulty_match
				else:
					difficulty_m1 = 'Easy_1'
					difficulty_m2 = 'Easy_2'
					difficulty_m3 = db_self.imposed_difficulty_match
		if order_diff_matches=='desc':
			if db_self.imposed_difficulty_match=='Easy_2' or db_self.imposed_difficulty_match=='Normal_1' or db_self.imposed_difficulty_match=='Normal_2':
				difficulty_m1 = 'Normal_2'
				difficulty_m2 = 'Normal_1'
				difficulty_m3 = 'Easy_2'
			else:
				ran_2 = random.randint(0,1)
				if ran_2:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Normal_1'
					difficulty_m3 = 'Easy_2'
				else:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Normal_2'
					difficulty_m3 = 'Normal_1'
	########################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
		if order_diff_matches=='same':
			difficulty_m1 = 'Medium_1'
			mix_imposed_in_m1 = 1
			difficulty_m2 = 'Medium_1'
			mix_imposed_in_m2 = 1	
			difficulty_m3 = 'Medium_1'
			mix_imposed_in_m3 = 1	
		if order_diff_matches=='casu':
			rand_change = random.randint(1,3)
			if rand_change==1:
				difficulty_m1 = 'Medium_1'
				mix_imposed_in_m1 = 1
			if rand_change==2:
				difficulty_m2 = 'Medium_1'
				mix_imposed_in_m2 = 1			
			if rand_change==3:
				difficulty_m3 = 'Medium_1'
				mix_imposed_in_m3 = 1	
		if order_diff_matches=='asc':
			difficulty_m3 = 'Medium_1'
			mix_imposed_in_m3 = 1			
		if order_diff_matches=='desc':
			difficulty_m1 = 'Medium_1'
			mix_imposed_in_m1 = 1
	########################################################################	
	if order_quan_games=='same':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			casual = random.randint(0,4)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = random.randint(1,2)
			########################################################################				
			if casual==0:
				num_of_games_m1 = 1
				num_of_games_m2 = 1
				num_of_games_m3 = 1
			elif casual==1:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
				num_of_games_m3 = 3
			elif casual==2 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2')):
				num_of_games_m1 = 4
				num_of_games_m2 = 4
				num_of_games_m3 = 4
			else:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 2
		else:
			db_self.already_done_imposed_num_games = 1
			num_of_games_m1 = db_self.imposed_num_games
			########################################################################
			if db_self.imposed_num_games==1:
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					num_of_games_m1 = random.randint(2,3)
			########################################################################					
			num_of_games_m2 = num_of_games_m1
			num_of_games_m3 = num_of_games_m1
	elif order_quan_games=='casu':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			num_of_games_m1 = random.randint(2,3)
			num_of_games_m2 = random.randint(2,3)
			num_of_games_m3 = random.randint(2,3)
		else:
			db_self.already_done_imposed_num_games = 1
			num_of_games_m1 = random.randint(2,3)
			num_of_games_m2 = random.randint(2,3)
			num_of_games_m3 = random.randint(2,3)
			change_or = random.randint(0,2)
			if change_or==0:
				num_of_games_m1 = db_self.imposed_num_games
			if change_or==1:
				num_of_games_m2 = db_self.imposed_num_games
			if change_or==2:
				num_of_games_m3 = db_self.imposed_num_games
	elif order_quan_games=='asc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			casual = random.randint(0,7)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = random.randint(5,7)
			########################################################################				
			if casual==0 or casual==1:
				num_of_games_m1 = 1
				num_of_games_m2 = 2
				num_of_games_m3 = 3
			elif casual==2 or casual==3:
				num_of_games_m1 = 1
				num_of_games_m2 = 1
				num_of_games_m3 = 2
			elif casual==4:
				num_of_games_m1 = 1
				num_of_games_m2 = 1
				num_of_games_m3 = 3
			elif casual==5:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 3
			elif casual==6 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2')):
				num_of_games_m1 = 2
				num_of_games_m2 = 3
				num_of_games_m3 = 4							
			else:
				num_of_games_m1 = 2
				num_of_games_m2 = 3
				num_of_games_m3 = 4
		else:
			db_self.already_done_imposed_num_games = 1
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				db_self.imposed_num_games = 2
			########################################################################						
			if db_self.imposed_num_games==1:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2
				elif casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
			if db_self.imposed_num_games==2:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2
				elif casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2								
			if db_self.imposed_num_games==3:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 3
				elif casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
					num_of_games_m3 = 3								
			if db_self.imposed_num_games==4:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 4
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 3
					num_of_games_m3 = 4
				elif casual==3:
					num_of_games_m1 = 2
					num_of_games_m2 = 3
					num_of_games_m3 = 4						
	elif order_quan_games=='desc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			casual = random.randint(0,7)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = random.randint(5,7)
			########################################################################
			if casual==0 or casual==1:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
				num_of_games_m3 = 1
			elif casual==2 or casual==3:
				num_of_games_m1 = 2
				num_of_games_m2 = 1
				num_of_games_m3 = 1
			elif casual==4:
				num_of_games_m1 = 3
				num_of_games_m2 = 1
				num_of_games_m3 = 1
			elif casual==5:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
				num_of_games_m3 = 2
			elif casual==6 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2')):
				num_of_games_m1 = 4
				num_of_games_m2 = 3
				num_of_games_m3 = 2	
			else:
				num_of_games_m1 = 2
				num_of_games_m2 = 3
				num_of_games_m3 = 4
		else:
			db_self.already_done_imposed_num_games = 1
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				db_self.imposed_num_games =3
			########################################################################	
			if db_self.imposed_num_games==1:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 2
					num_of_games_m2 = 1
					num_of_games_m3 = 1
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				elif casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 1
			if db_self.imposed_num_games==2:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				elif casual==2:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				elif casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 2
			if db_self.imposed_num_games==3:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 2
				elif casual==2:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				elif casual==3:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2	
			if db_self.imposed_num_games==4:
				casual = random.randint(0,3)
				if casual==0 or casual==1:
					num_of_games_m1 = 4
					num_of_games_m2 = 2
					num_of_games_m3 = 2
				elif casual==2:
					num_of_games_m1 = 4
					num_of_games_m2 = 2
					num_of_games_m3 = 1
				elif casual==3:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2											
	if difficulty_m1 in ['Easy_1','Easy_2']:
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m1 = 'same' 
		elif ord_casual==2:
			order_diff_games_m1 = 'asc' 
		ord_casual = random.randint(0,1)
		if ord_casual==0:
			order_quan_ques_m1 = 'same' 
		elif ord_casual==1:
			order_quan_ques_m1 = 'asc' 
	if difficulty_m1 in ['Normal_1','Normal_2']:
		ord_casual = random.randint(0,3)
		if ord_casual in [0,1]:
			order_diff_games_m1 = 'same' 
		elif ord_casual==2:
			order_diff_games_m1 = 'asc'
		else:
			order_diff_games_m1 = 'desc'
		ord_casual = random.randint(0,3)
		if ord_casual==0:
			order_quan_ques_m1 = 'same'
		elif ord_casual==1:
			order_quan_ques_m1 = 'asc'
		else:
			order_quan_ques_m1 = 'desc'
	if difficulty_m1 in ['Medium_1','Medium_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m1 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m1 = 'asc' 
		else:
			order_diff_games_m1 = 'desc' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m1 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m1 = 'asc' 
		else:
			order_quan_ques_m1 = 'desc'
	if difficulty_m1 in ['Hard_1','Hard_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m1 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m1 = 'asc' 
		else:
			order_diff_games_m1 = 'casu' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m1 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m1 = 'asc' 
		else:
			order_quan_ques_m1 = 'casu'
	###
	if difficulty_m2 in ['Easy_1','Easy_2']:
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m2 = 'same' 
		elif ord_casual==2:
			order_diff_games_m2 = 'asc' 
		ord_casual = random.randint(0,1)
		if ord_casual==0:
			order_quan_ques_m2 = 'same' 
		elif ord_casual==1:
			order_quan_ques_m2 = 'asc' 
	if difficulty_m2 in ['Normal_1','Normal_2']:
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m2 = 'same' 
		elif ord_casual==2:
			order_diff_games_m2 = 'asc'
		else:
			order_diff_games_m2 = 'desc'
		ord_casual = random.randint(0,2)
		if ord_casual==0:
			order_quan_ques_m2 = 'same'
		elif ord_casual==1:
			order_quan_ques_m2 = 'asc'
		else:
			order_quan_ques_m2 = 'desc'
	if difficulty_m2 in ['Medium_1','Medium_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m2 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m2 = 'asc' 
		else:
			order_diff_games_m2 = 'desc' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m2 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m2 = 'asc' 
		else:
			order_quan_ques_m2 = 'desc'
	if difficulty_m2 in ['Hard_1','Hard_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m2 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m2 = 'asc' 
		else:
			order_diff_games_m2 = 'casu' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m2 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m2 = 'asc' 
		else:
			order_quan_ques_m2 = 'casu'
	####
	if difficulty_m3 in ['Easy_1','Easy_2']:
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m3 = 'same' 
		elif ord_casual==2:
			order_diff_games_m3 = 'asc' 
		ord_casual = random.randint(0,1)
		if ord_casual==0:
			order_quan_ques_m3 = 'same' 
		elif ord_casual==1:
			order_quan_ques_m3 = 'asc' 
	if difficulty_m3 in ['Normal_1','Normal_2']:
		ord_casual = random.randint(0,2)
		if ord_casual in [0,1]:
			order_diff_games_m3 = 'same' 
		elif ord_casual==2:
			order_diff_games_m3 = 'asc'
		else:
			order_diff_games_m3 = 'desc'
		ord_casual = random.randint(0,2)
		if ord_casual==0:
			order_quan_ques_m3 = 'same'
		elif ord_casual==1:
			order_quan_ques_m3 = 'asc'
		else:
			order_quan_ques_m3 = 'desc'
	if difficulty_m3 in ['Medium_1','Medium_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m3 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m3 = 'asc' 
		else:
			order_diff_games_m3 = 'desc' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m3 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m3 = 'asc' 
		else:
			order_quan_ques_m3 = 'desc'
	if difficulty_m3 in ['Hard_1','Hard_2']:
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_diff_games_m3 = 'same' 
		if ord_casual in [2,3]:
			order_diff_games_m3 = 'asc' 
		else:
			order_diff_games_m3 = 'casu' 
		ord_casual = random.randint(0,4)
		if ord_casual in [0,1]:
			order_quan_ques_m3 = 'same' 
		if ord_casual in [2,3]:
			order_quan_ques_m3 = 'asc' 
		else:
			order_quan_ques_m3 = 'casu'		
	#################################################################################################################################################################################
	#scenario imposed can have one positon among 1,2,3 match
	print("############################################################ part_2!!! scenario take_given_scenario {}".format(db_self.imposed_scenario))
	scenario_m1 = 0
	scenario_m2 = 0
	scenario_m3 = 0
	variety_m1 = ''
	variety_m2 = ''
	variety_m3 = ''
	print("IMPOSED!!!!!! RANMA!!! ".format(db_self.imposed_scenario))
	if db_self.imposed_scenario!=0:
		random_var = random.randint(0,3)
		sundry_or_random_m1 = random.randint(0,2)
		sundry_or_random_m2 = random.randint(0,2)
		sundry_or_random_m3 = random.randint(0,2)
		db_self.curs.execute("SELECT kind FROM Scenarios_Kinds JOIN Scenarios USING(scenario_group) WHERE scenario_id = ? ORDER BY RANDOM() LIMIT 1",(db_self.imposed_scenario,))	
		if random_var==0:
			variety_m1 = db_self.curs.fetchone()[0]
			scenario_m1 = db_self.imposed_scenario
		if random_var==1:
			variety_m2 = db_self.curs.fetchone()[0]
			scenario_m2 = db_self.imposed_scenario
		if random_var==2:
			variety_m3 = db_self.curs.fetchone()[0]
			scenario_m3 = db_self.imposed_scenario
		##################################################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
			variety_m1 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
			variety_m2 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
			variety_m3 = 'SUNDRY'
		##################################################################################################						
	if db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and db_self.imposed_ques_id==99999:
		print("bugia 00")
		print("TYPE GAME NOT GIVEN AT ALL, SO CASUAL CHOICE SCENARIO!!")
		if scenario_m1==0: #non è stato imposto scenario m1
			print("vado di percorso scenario m1 perche ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1					
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
		if scenario_m3==0: #non è stato imposto scenario m3
			print("vado di scenario_m3 {}".format(scenario_m3))
			random_var_m3 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
				random_var_m3 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m3 = scenario_m1
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			else:
				while True:
					if random_var_m3==0:
						print("difficulty_m3 {}".format(difficulty_m3))
						if difficulty_m3 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						print("variety_m3 scelta is, ", variety_m3)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario_m3 variety dependent non a caso {}".format(scenario_m3))		
						if variety_m2=='SUNDRY' or variety_m3!=variety_m2:
							break
						else:
							ran3 = random.randint(0,3)
							if ran3==0:
								random_var_m3 = 1
					if random_var_m3!=0:
						variety_m3 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m3))
						break
		###########################
		###########################
		###########################
	else:
		tikiqu_sharing = 0
		tiki_sharing = 0
		tiqu_sharing = 0
		kiqu_sharing = 0
		ok_end = 0
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999:
			print("bugia tripla")
			print("TYPE GAME KIND and also question GIVEN FROM IMPOSITIONSLIST!!")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			############## nel caso in cui esistessero non sia possibile trovare lo scenario valido per i 3, cerco 2 ...altrimenti no vince 3 !!! nel caso di un solo match!! nel caso di 2 pure!!
			###########cambia se sono di piu in questo caso gli faccio trovare tutti i tk kq e tq cmq anche se il 3 non c'è, e poi lo spalmo volendo su più match...per esempio quando la complessita e minore li spalmo altrimenti no!!!
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
				if db_self.imposed_kind_game!='4P':
					tkq_inter = list(set(tq_inter) & set(resp_kind))						
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tkq_inter = list(set(tq_inter) & set(resp4psu))
							if not tkq_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tkq_inter = list(set(tq_inter) & set(resp4pco))
							if not tkq_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tkq_inter:
					random.shuffle(tkq_inter)
					one_ran_scenario_def = random.choices(tkq_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kitiqu_sharing = 1
					ok_end = 1
				else:
					if db_self.imposed_kind_game!='4P':
						tk_inter = list(set(resp_type) & set(resp_kind))
					else:
						fcase = random.randint(0,1)
						dstop = 1
						into_psu = 0
						into_pco = 0
						while dstop>0:
							if fcase==0:
								tk_inter = list(set(resp_type) & set(resp4psu))
								if not tk_inter and not into_pco:
									fcase = 1
									into_psu = 1
								else:
									dstop = 0
							if fcase==1:									
								tk_inter = list(set(resp_type) & set(resp4pco))
								if not tk_inter and not into_psu:
									fcase = 1
									into_pco = 1
								else:
									dstop = 0
					if tk_inter:
						random.shuffle(tk_inter)
						one_ran_scenario_def = random.choices(tk_inter,k = 1)
						print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
						kiti_sharing = 1
						ok_end = 1
					else:
						if db_self.imposed_kind_game!='4P':
							kq_inter = list(set(resp_ques) & set(resp_kind))
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tk_inter = list(set(resp_ques) & set(resp4psu))
									if not tk_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tk_inter = list(set(resp_ques) & set(resp4pco))
									if not tk_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if kq_inter:
							random.shuffle(kq_inter)
							one_ran_scenario_def = random.choices(kq_inter,k = 1)
							print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
							kiqu_sharing = 1
							ok_end = 1
			if not ok_end:
				################ search tiki
				if db_self.imposed_kind_game!='4P':
					tk_inter = list(set(resp_type) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_type) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_type) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tk_inter:
					random.shuffle(tk_inter)
					one_ran_scenario_def = random.choices(tk_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kiti_sharing = 1
				################ search kiqu
				if db_self.imposed_kind_game!='4P':
					kq_inter = list(set(resp_ques) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_ques) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_ques) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if kq_inter:
					random.shuffle(kq_inter)
					one_ran_scenario_def = random.choices(kq_inter,k = 1)
					print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
					kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				if kitiqu_sharing:
					print("######## scenario imposed da tutti e 3 {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety_m3 = db_self.imposed_kind_game
						else:
							variety_m3 = 'SUNDRY'
				elif kiti_sharing:
					print("######## scenario imposed tiki sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
				elif tiqu_sharing:
					print("######## scenario imposed tiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'							
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'	
				elif kiqu_sharing:
					print("######## scenario imposed kiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety_m3 = db_self.imposed_kind_game
						else:
							variety_m3 = 'SUNDRY'
		###########################
		###########################
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id==99999:
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			################ search tiki
			if db_self.imposed_kind_game!='4P':
				tk_inter = list(set(resp_type) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_type) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_type) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if tk_inter:
				random.shuffle(tk_inter)
				one_ran_scenario_def = random.choices(tk_inter,k = 1)
				print("one_ran_scenario_from_tk_inter ", one_ran_scenario_def)
				kiti_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiki sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m3 = scenario
					if random_var==0:
						variety_m3 = db_self.imposed_kind_game
					else:
						variety_m3 = 'SUNDRY'						
		if db_self.imposed_type_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game=='':
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search tiqu
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m3 = scenario
					if random_var==0:
						variety_m3 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m3 = 'SUNDRY'
		if db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_type_game=='':
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search kiqu
			################ search kiqu
			if db_self.imposed_kind_game!='4P':
				kq_inter = list(set(resp_ques) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_ques) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_ques) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if kq_inter:
				random.shuffle(kq_inter)
				one_ran_scenario_def = random.choices(kq_inter,k = 1)
				print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
				kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed kiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m3 = db_self.imposed_kind_game
					else:
						variety_m3 = 'SUNDRY'
		###########################
		if not one_ran_scenario_def and db_self.imposed_type_game!='':
			print("bugia singola 1")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			print("sce_type", sce_type)
			print("sce_type", sce_type)
			print("sce_type", sce_type)
			print("sce_type", sce_type)
			resp_type = [item for sublist in sce_type for item in sublist]
			random.shuffle(resp_type)
			print("resp_type", resp_type)
			print("resp_type", resp_type)
			print("resp_type", resp_type)
			print("resp_type", resp_type)
			scenario = random.choices(resp_type, k = 1)
			print("come stai a non andare caso s1 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal tipo!!! sce {}".format(scenario))
			db_self.suggested_scenario_from_t = copy.copy(scenario)
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1	
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					variety_m3 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m3 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_kind_game!='':
			print("bugia singola 2")
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
				ran4p = random.randint(0,1)
				if ran4p:
					resp_kind = resp4pco
				else:
					resp_kind = resp4psu
			random.shuffle(resp_kind)
			scenario = random.choices(resp_kind,k = 1)
			print("come stai a non andare caso s2 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal kind is {}".format(scenario))
			db_self.suggested_scenario_from_k = copy.copy(scenario)
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = db_self.imposed_kind_game
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = db_self.imposed_kind_game
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					variety_m3 = db_self.imposed_kind_game
				else:
					variety_m3 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_ques_id!=99999:
			print("bugia singola 3")
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			random.shuffle(resp_ques)
			scenario = random.choices(resp_ques,k = 1)
			print("come stai a non andare caso s3 {} ".format(scenario))
			scenario = scenario[0]
			db_self.suggested_scenario_from_q = copy.copy(scenario)
			print("ho appena settato db_self.suggested_scenario_from_q = copy.copy(scenario) {}".format(db_self.suggested_scenario_from_q))
			print("######## scenario imposed dalla question!!! sce {}".format(scenario))
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	if 0 in (scenario_m1,scenario_m2,scenario_m3):
		if scenario_m1==0:
			print("vado di percorso scenario m1 dopo tuittto !! perche ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m1 = scenario_m3
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m3,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m2 = scenario_m3
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m3,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1	
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
		if scenario_m3==0: #non è stato imposto scenario m3
			print("vado di scena m3 è ancora a zero")
			random_var_m3 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
				random_var_m3 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m3 = scenario_m1
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			elif depends==0 and scenario_m2!=0:
				scenario_m3 = scenario_m2
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			else:
				while True:
					if random_var_m3==0:
						print("difficulty_m3 {}".format(difficulty_m3))
						if difficulty_m3 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','5K']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						print("variety_m3 scelta is, ", variety_m3)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario_m3 variety dependent non a caso {}".format(scenario_m3))
						if variety_m2=='SUNDRY' or variety_m3!=variety_m2:
							break
						else:
							ran3 = random.randint(0,3)
							if ran3==0:
								random_var_m3 = 1
					if random_var_m3!=0:
						variety_m3 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m3))
						break
	###########################################################
	may_swap = random.randint(0,3)
	if may_swap==0:
		scenario_m1, scenario_m2 = scenario_m2, scenario_m1
		variety_m1, variety_m2 = variety_m2, variety_m1
		print("ho fatto swap caso 0")
	if may_swap==1:
		scenario_m1, scenario_m3 = scenario_m3, scenario_m1
		variety_m1, variety_m3 = variety_m3, variety_m1
		print("ho fatto swap caso 1")
	###########################################################
	print("variety 1 is ==================================================================> ",variety_m1)
	print("variety 2 is ==================================================================> ",variety_m2)
	print("variety 3 is ==================================================================> ",variety_m3)
	######################################################################################################################
	if variety_m1=='SUNDRY':
		a_crite = random.randint(0,5)
		if a_crite==0:
			criteria_arrangement_m1 = 'random'
		else:
			criteria_arrangement_m1 = 'regular'
	else:
		criteria_arrangement_m1 = 'ignore'
	if variety_m2=='SUNDRY':
		a_crite = random.randint(0,5)
		if a_crite==0:
			criteria_arrangement_m2 = 'random'
		else:
			criteria_arrangement_m2 = 'regular'
	else:
		criteria_arrangement_m2 = 'ignore'
	if variety_m3=='SUNDRY':
		a_crite = random.randint(0,5)
		if a_crite==0:
			criteria_arrangement_m3 = 'random'
		else:
			criteria_arrangement_m3 = 'regular'
	else:
		criteria_arrangement_m3 = 'ignore'
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
		criteria_arrangement_m1 = 'random'
		db_self.already_done_imposed_mixed = 1 
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
		criteria_arrangement_m2 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################	
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
		criteria_arrangement_m3 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################
	dca.add_new_Match_table(db_self, difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m3, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)					
cdef def_matches_when_complexity_is_normal_4(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games):
	advisable_time = 240
	one_ran_scenario_def = 0
	mix_imposed_in_m1 = 0
	mix_imposed_in_m2 = 0
	mix_imposed_in_m3 = 0
	mix_imposed_in_m4 = 0
	options = ['Easy_2', 'Normal_1', 'Normal_2']
	if db_self.imposed_difficulty_match=='' or (db_self.imposed_difficulty_match!='' and db_self.already_done_imposed_difficulty_match==1):
		if order_diff_matches=='same':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = difficulty_m1
			difficulty_m3 = difficulty_m1
			difficulty_m4 = difficulty_m1
		if order_diff_matches=='casu':
			difficulty_m1 = random.choice(options)
			difficulty_m2 = random.choice(options)
			difficulty_m3 = random.choice(options)
			difficulty_m4 = random.choice(options)
		if order_diff_matches=='asc':
			difficulty_m1 = 'Easy_2'
			difficulty_m2 = 'Normal_1'
			difficulty_m3 = 'Normal_1'
			difficulty_m4 = 'Normal_2'
		if order_diff_matches=='desc':
			difficulty_m1 = 'Normal_2'
			difficulty_m2 = 'Normal_1'				
			difficulty_m3 = 'Easy_2'				
			difficulty_m4 = 'Easy_2'				
	else:
		db_self.already_done_imposed_difficulty_match = 1
		if order_diff_matches=='same':
			difficulty_m1 = db_self.imposed_difficulty_match
			difficulty_m2 = difficulty_m1
			difficulty_m3 = difficulty_m1
			difficulty_m3 = difficulty_m1
		if order_diff_matches=='casu':
			ran_ran = random.randint(0,3)
			if ran_ran==0:
				difficulty_m1 = db_self.imposed_difficulty_match
				difficulty_m2 = random.choice(options)
				difficulty_m3 = random.choice(options)
				difficulty_m4 = random.choice(options)
			if ran_ran==1:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = db_self.imposed_difficulty_match
				difficulty_m3 = random.choice(options)
				difficulty_m4 = random.choice(options)
			if ran_ran==2:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = random.choice(options)
				difficulty_m3 = db_self.imposed_difficulty_match
				difficulty_m4 = random.choice(options)
			else:
				difficulty_m1 = random.choice(options)
				difficulty_m2 = random.choice(options)
				difficulty_m3 = random.choice(options)
				difficulty_m4 = db_self.imposed_difficulty_match
		if order_diff_matches=='asc':
			if db_self.imposed_difficulty_match=='Easy_2' or db_self.imposed_difficulty_match=='Normal_1' or db_self.imposed_difficulty_match=='Normal_2':
				difficulty_m1 = 'Easy_2'
				difficulty_m2 = 'Easy_2'
				difficulty_m3 = 'Normal_1'
				difficulty_m4 = 'Normal_2'
			else:
				ran_2 = random.randint(0,1)
				if ran_2==0:
					difficulty_m1 = 'Easy_2'
					difficulty_m2 = 'Normal_1'
					difficulty_m3 = 'Normal_1'
					difficulty_m4 = db_self.imposed_difficulty_match
				else:
					difficulty_m1 = 'Easy_2'
					difficulty_m2 = 'Normal_1'
					difficulty_m3 = 'Normal_2'
					difficulty_m4 = db_self.imposed_difficulty_match
		if order_diff_matches=='desc':
			if db_self.imposed_difficulty_match=='Easy_2' or db_self.imposed_difficulty_match=='Normal_1' or db_self.imposed_difficulty_match=='Normal_2':
				difficulty_m1 = 'Normal_2'
				difficulty_m2 = 'Normal_1'
				difficulty_m3 = 'Normal_1'
				difficulty_m4 = 'Easy_2'
			else:
				ran_2 = random.randint(0,1)
				if ran_2:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Normal_2'
					difficulty_m3 = 'Normal_1'
					difficulty_m4 = 'Easy_2'
				else:
					difficulty_m1 = db_self.imposed_difficulty_match
					difficulty_m2 = 'Normal_1'
					difficulty_m3 = 'Easy_2'
					difficulty_m4 = 'Easy_2'
	########################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
		if order_diff_matches=='same':
			difficulty_m1 = 'Medium_2'
			mix_imposed_in_m1 = 1
			difficulty_m2 = 'Medium_2'
			mix_imposed_in_m2 = 1	
			difficulty_m3 = 'Medium_2'
			mix_imposed_in_m3 = 1	
			difficulty_m4 = 'Medium_2'
			mix_imposed_in_m4 = 1	
		if order_diff_matches=='casu':
			rand_change = random.randint(1,3)
			if rand_change==1:
				difficulty_m1 = 'Medium_2'
				mix_imposed_in_m1 = 1
			if rand_change==2:
				difficulty_m2 = 'Medium_2'
				mix_imposed_in_m2 = 1			
			if rand_change==3:
				difficulty_m3 = 'Medium_2'
				mix_imposed_in_m3 = 1	
			if rand_change==3:
				difficulty_m4 = 'Medium_2'
				mix_imposed_in_m4 = 1	
		if order_diff_matches=='asc':
			difficulty_m4 = 'Medium_2'
			mix_imposed_in_m4 = 1			
		if order_diff_matches=='desc':
			difficulty_m1 = 'Medium_2'
			mix_imposed_in_m1 = 1
	########################################################################
	if order_quan_games=='same':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			casual = random.randint(0,4)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = random.randint(2,3)
			########################################################################				
			if casual==0:
				num_of_games_m1 = 1
				num_of_games_m2 = 1
				num_of_games_m3 = 1
				num_of_games_m4 = 1
			elif casual==1:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
				num_of_games_m3 = 3
				num_of_games_m4 = 3
			elif casual==2 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2') or (difficulty_m4=='Easy_2')):
				num_of_games_m1 = 4
				num_of_games_m2 = 4
				num_of_games_m3 = 4
				num_of_games_m4 = 4
			else:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 2
				num_of_games_m4 = 2
		else:
			db_self.already_done_imposed_num_games = 1
			num_of_games_m1 = db_self.imposed_num_games
			num_of_games_m2 = db_self.imposed_num_games
			num_of_games_m3 = db_self.imposed_num_games
			num_of_games_m4 = db_self.imposed_num_games
			########################################################################
			if db_self.imposed_num_games==1:
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					num_of_games_m1 = random.randint(2,4)
					num_of_games_m2 = num_of_games_m1
					num_of_games_m3 = num_of_games_m1
					num_of_games_m4 = num_of_games_m1
					db_self.already_done_imposed_num_games = 0
			########################################################################						
	elif order_quan_games=='casu':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			num_of_games_m1 = random.randint(1,3)
			num_of_games_m2 = random.randint(1,3)
			num_of_games_m3 = random.randint(1,3)
			num_of_games_m4 = random.randint(1,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				num_of_games_m1 = random.randint(2,3)
				num_of_games_m2 = random.randint(2,3)
				num_of_games_m3 = random.randint(2,3)
				num_of_games_m4 = random.randint(2,3)
			########################################################################					
		else:
			db_self.already_done_imposed_num_games = 1
			num_of_games_m1 = random.randint(1,3)
			num_of_games_m2 = random.randint(1,3)
			num_of_games_m3 = random.randint(1,3)
			num_of_games_m4 = random.randint(1,3)
			change_or = random.randint(0,3)
			if change_or==0:
				num_of_games_m1 = db_self.imposed_num_games
			if change_or==1:
				num_of_games_m2 = db_self.imposed_num_games
			if change_or==2:
				num_of_games_m3 = db_self.imposed_num_games
			if change_or==3:
				num_of_games_m4 = db_self.imposed_num_games
			########################################################################
			if db_self.imposed_num_games==1:
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					num_of_games_m1 = random.randint(2,3)
					num_of_games_m2 = random.randint(2,3)
					num_of_games_m3 = random.randint(2,3)
					num_of_games_m4 = random.randint(2,3)
					db_self.already_done_imposed_num_games = 0
			########################################################################					
	elif order_quan_games=='asc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			casual = random.randint(0,6)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = random.randint(5,6)
			########################################################################							
			if casual==0 or casual==1:
				num_of_games_m1 = 1
				num_of_games_m2 = 2
				num_of_games_m3 = 2
				num_of_games_m4 = 3
			elif casual==2 or casual==3:
				num_of_games_m1 = 1
				num_of_games_m2 = 1
				num_of_games_m3 = 2 
				num_of_games_m4 = 3
			elif casual==4:
				num_of_games_m1 = 1
				num_of_games_m2 = 2
				num_of_games_m3 = 3
				num_of_games_m4 = 3
			elif casual==6 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2') or (difficulty_m4=='Easy_2')):
				num_of_games_m1 = 2
				num_of_games_m2 = 3
				num_of_games_m3 = 3
				num_of_games_m4 = 4
			else:
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 3
				num_of_games_m4 = 3
		else:
			db_self.already_done_imposed_num_games = 1
			casual = random.randint(0,3)
			if db_self.imposed_num_games==1:
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 3
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2 
					num_of_games_m4 = 3
				elif casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
			if db_self.imposed_num_games==2:
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 3
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2 
					num_of_games_m4 = 3
				elif casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
			if db_self.imposed_num_games==3:
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 3
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2 
					num_of_games_m4 = 3
				elif casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 3
			if db_self.imposed_num_games==4:
				if casual==0 or casual==1:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 2
					num_of_games_m4 = 4
				elif casual==2:
					num_of_games_m1 = 1
					num_of_games_m2 = 1
					num_of_games_m3 = 2 
					num_of_games_m4 = 4
				elif casual==3:
					num_of_games_m1 = 1
					num_of_games_m2 = 2
					num_of_games_m3 = 3
					num_of_games_m4 = 4
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				db_self.already_done_imposed_num_games = 0
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 3
				num_of_games_m4 = 4					
			########################################################################						
	elif order_quan_games=='desc':
		if db_self.imposed_num_games==0 or (db_self.imposed_num_games!=0 and db_self.already_done_imposed_num_games==1):
			casual = random.randint(0,6)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				casual = random.randint(5,6)
			########################################################################					
			if casual==0 or casual==1:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
				num_of_games_m3 = 1
				num_of_games_m4 = 1
			elif casual==2 or casual==3:
				num_of_games_m1 = 3
				num_of_games_m2 = 2
				num_of_games_m3 = 2
				num_of_games_m4 = 1
			elif casual==4:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
				num_of_games_m3 = 2
				num_of_games_m4 = 1
			elif casual==6 and ((difficulty_m1=='Easy_2') or (difficulty_m2=='Easy_2') or (difficulty_m3=='Easy_2') or (difficulty_m4=='Easy_2')):
				num_of_games_m1 = 4
				num_of_games_m2 = 3
				num_of_games_m3 = 3
				num_of_games_m4 = 2
			else:
				num_of_games_m1 = 3
				num_of_games_m2 = 3
				num_of_games_m3 = 2
				num_of_games_m4 = 2							
		else:
			db_self.already_done_imposed_num_games = 1
			casual = random.randint(0,3)
			if db_self.imposed_num_games==1:
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 1
					num_of_games_m4 = 1
				elif casual==2:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2 
					num_of_games_m4 = 1
				elif casual==3:
					num_of_games_m1 = 3
					num_of_games_m2 = 2
					num_of_games_m3 = 1
					num_of_games_m4 = 1
			if db_self.imposed_num_games==2:
				if casual==0 or casual==1:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 3
					num_of_games_m4 = 2
				elif casual==2:
					num_of_games_m1 = 2
					num_of_games_m2 = 2
					num_of_games_m3 = 1 
					num_of_games_m4 = 1
				elif casual==3:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
			if db_self.imposed_num_games==3:
				if casual==0 or casual==1:
					num_of_games_m1 = 3
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
				elif casual==2:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2 
					num_of_games_m4 = 1
				elif casual==3:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 2
			if db_self.imposed_num_games==4:
				if casual==0 or casual==1:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 2
				elif casual==2:
					num_of_games_m1 = 4
					num_of_games_m2 = 2
					num_of_games_m3 = 1
					num_of_games_m4 = 1
				elif casual==3:
					num_of_games_m1 = 4
					num_of_games_m2 = 3
					num_of_games_m3 = 2
					num_of_games_m4 = 1
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				db_self.already_done_imposed_num_games = 0
				num_of_games_m1 = 2
				num_of_games_m2 = 2
				num_of_games_m3 = 3
				num_of_games_m4 = 4
			########################################################################
	ord_casual = random.randint(0,7)
	if ord_casual in [0,1]:
		order_diff_games_m1 = 'same' 
		order_diff_games_m2 = 'same' 
		order_diff_games_m3 = 'asc' 
		order_diff_games_m4 = 'asc'
	if ord_casual in [2,3]:
		order_diff_games_m1 = 'same' 
		order_diff_games_m2 = 'same' 
		order_diff_games_m3 = 'casu' 
		order_diff_games_m4 = 'asc' 
	if ord_casual in [4,5]:
		order_diff_games_m1 = 'same' 
		order_diff_games_m2 = 'asc' 
		order_diff_games_m3 = 'same' 
		order_diff_games_m4 = 'asc' 
	elif ord_casual==6:
		order_diff_games_m1 = 'same' 
		order_diff_games_m2 = 'asc' 
		order_diff_games_m3 = 'casu' 
		order_diff_games_m4 = 'asc' 
	elif ord_casual==7:
		order_diff_games_m1 = 'same' 
		order_diff_games_m2 = 'asc' 
		order_diff_games_m3 = 'casu' 
		order_diff_games_m4 = 'casu' 
	ord_casual = random.randint(0,6)
	if ord_casual in [0,1]:
		order_quan_ques_m1 = 'same' 
		order_quan_ques_m2 = 'same' 
		order_quan_ques_m3 = 'asc' 
		order_quan_ques_m4 = 'asc' 
	if ord_casual in [2,3]:
		order_quan_ques_m1 = 'same' 
		order_quan_ques_m2 = 'asc' 
		order_quan_ques_m3 = 'asc' 
		order_quan_ques_m4 = 'asc' 
	elif ord_casual==4:
		order_quan_ques_m1 = 'asc' 
		order_quan_ques_m2 = 'same' 
		order_quan_ques_m3 = 'casu' 
		order_quan_ques_m4 = 'casu' 
	elif ord_casual==5:
		order_quan_ques_m1 = 'asc' 
		order_quan_ques_m2 = 'same' 
		order_quan_ques_m3 = 'asc' 
		order_quan_ques_m4 = 'casu' 
	elif ord_casual==6:
		order_quan_ques_m1 = 'same' 
		order_quan_ques_m2 = 'asc' 
		order_quan_ques_m3 = 'casu' 
		order_quan_ques_m4 = 'casu' 
	#scenario imposed can have one positon among 1,2,3 match
	print("############################################################ part_2!!! scenario take_given_scenario {}".format(db_self.imposed_scenario))
	scenario_m1 = 0
	scenario_m2 = 0
	scenario_m3 = 0
	scenario_m4 = 0
	variety_m1 = ''
	variety_m2 = ''
	variety_m3 = ''
	variety_m4 = ''
	print("IMPOSED!!!!!! RANMA!!! ".format(db_self.imposed_scenario))
	if db_self.imposed_scenario!=0:
		random_var = random.randint(0,3)
		sundry_or_random_m1 = random.randint(0,2)
		sundry_or_random_m2 = random.randint(0,2)
		sundry_or_random_m3 = random.randint(0,2)
		sundry_or_random_m4 = random.randint(0,2)
		db_self.curs.execute("SELECT kind FROM Scenarios_Kinds JOIN Scenarios USING(scenario_group) WHERE scenario_id = ? ORDER BY RANDOM() LIMIT 1",(db_self.imposed_scenario,))	
		if random_var==0:
			variety_m1 = db_self.curs.fetchone()[0]
			scenario_m1 = db_self.imposed_scenario
		if random_var==1:
			variety_m2 = db_self.curs.fetchone()[0]
			scenario_m2 = db_self.imposed_scenario
		if random_var==2:
			variety_m3 = db_self.curs.fetchone()[0]
			scenario_m3 = db_self.imposed_scenario
		if random_var==3:
			variety_m4 = db_self.curs.fetchone()[0]
			scenario_m4 = db_self.imposed_scenario
		##################################################################################################
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
			variety_m1 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
			variety_m2 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
			variety_m3 = 'SUNDRY'
		if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m4:
			variety_m4 = 'SUNDRY'				
		##################################################################################################						
	if db_self.imposed_kind_game=='' and db_self.imposed_type_game=='' and db_self.imposed_ques_id==99999:
		print("bugia 00")
		print("TYPE GAME NOT GIVEN AT ALL, SO CASUAL CHOICE SCENARIO!!")
		if scenario_m1==0: #non è stato imposto scenario m1
			print("vado di percorso scenario m1 perche ancora zero")
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1						
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
		if scenario_m3==0: #non è stato imposto scenario m3
			print("vado di scenario_m3 {}".format(scenario_m3))
			random_var_m3 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
				random_var_m3 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m3 = scenario_m1
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			else:
				while True:
					if random_var_m3==0:
						print("difficulty_m3 {}".format(difficulty_m3))
						if difficulty_m3 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						print("variety_m3 scelta is, ", variety_m3)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario_m3 variety dependent non a caso {}".format(scenario_m3))
						if variety_m2=='SUNDRY' or variety_m3!=variety_m2:
							break
						else:
							ran3 = random.randint(0,3)
							if ran3==0:
								random_var_m3 = 1
					if random_var_m3!=0:
						variety_m3 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m3))
						break
		if scenario_m4==0: #non è stato imposto scenario m4
			print("vado di scenario_m4 {}".format(scenario_m4))
			random_var_m4 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m4:
				random_var_m4 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m4 = scenario_m1
				if random_var_m4==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m4 = db_self.curs.fetchone()[0]
				else:
					variety_m4 = 'SUNDRY'
			else:
				while True:
					if random_var_m4==0:
						print("difficulty_m4 {}".format(difficulty_m4))
						if difficulty_m4 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m4 = random.choice(variety_ava)
						print("variety_m4 scelta is, ", variety_m4)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m4,))
						scenario_m4 = db_self.curs.fetchone()[0]
						print("######## scenario_m4 variety dependent non a caso {}".format(scenario_m4))
						if variety_m3=='SUNDRY' or variety_m4!=variety_m3:
							break
						else:	
							ran4 = random.randint(0,3)
							if ran4==0:
								random_var_m4 = 1						
					if random_var_m4!=0:
						variety_m4 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m4 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m4))
						break
		###########################
		###########################
		###########################
	else:
		tikiqu_sharing = 0
		tiki_sharing = 0
		tiqu_sharing = 0
		kiqu_sharing = 0
		ok_end = 0
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999:
			print("bugia tripla")
			print("TYPE GAME KIND and also question GIVEN FROM IMPOSITIONSLIST!!")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			############## nel caso in cui esistessero non sia possibile trovare lo scenario valido per i 3, cerco 2 ...altrimenti no vince 3 !!! nel caso di un solo match!! nel caso di 2 pure!!
			###########cambia se sono di piu in questo caso gli faccio trovare tutti i tk kq e tq cmq anche se il 3 non c'è, e poi lo spalmo volendo su più match...per esempio quando la complessita e minore li spalmo altrimenti no!!!
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
				if db_self.imposed_kind_game!='4P':
					tkq_inter = list(set(tq_inter) & set(resp_kind))						
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tkq_inter = list(set(tq_inter) & set(resp4psu))
							if not tkq_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tkq_inter = list(set(tq_inter) & set(resp4pco))
							if not tkq_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tkq_inter:
					random.shuffle(tkq_inter)
					one_ran_scenario_def = random.choices(tkq_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kitiqu_sharing = 1
					ok_end = 1
				else:
					if db_self.imposed_kind_game!='4P':
						tk_inter = list(set(resp_type) & set(resp_kind))
					else:
						fcase = random.randint(0,1)
						dstop = 1
						into_psu = 0
						into_pco = 0
						while dstop>0:
							if fcase==0:
								tk_inter = list(set(resp_type) & set(resp4psu))
								if not tk_inter and not into_pco:
									fcase = 1
									into_psu = 1
								else:
									dstop = 0
							if fcase==1:									
								tk_inter = list(set(resp_type) & set(resp4pco))
								if not tk_inter and not into_psu:
									fcase = 1
									into_pco = 1
								else:
									dstop = 0
					if tk_inter:
						random.shuffle(tk_inter)
						one_ran_scenario_def = random.choices(tk_inter,k = 1)
						print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
						kiti_sharing = 1
						ok_end = 1
					else:
						if db_self.imposed_kind_game!='4P':
							kq_inter = list(set(resp_ques) & set(resp_kind))
						else:
							fcase = random.randint(0,1)
							dstop = 1
							into_psu = 0
							into_pco = 0
							while dstop>0:
								if fcase==0:
									tk_inter = list(set(resp_ques) & set(resp4psu))
									if not tk_inter and not into_pco:
										fcase = 1
										into_psu = 1
									else:
										dstop = 0
								if fcase==1:									
									tk_inter = list(set(resp_ques) & set(resp4pco))
									if not tk_inter and not into_psu:
										fcase = 1
										into_pco = 1
									else:
										dstop = 0
						if kq_inter:
							random.shuffle(kq_inter)
							one_ran_scenario_def = random.choices(kq_inter,k = 1)
							print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
							kiqu_sharing = 1
							ok_end = 1
			if not ok_end:
				################ search tiki
				if db_self.imposed_kind_game!='4P':
					tk_inter = list(set(resp_type) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_type) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_type) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if tk_inter:
					random.shuffle(tk_inter)
					one_ran_scenario_def = random.choices(tk_inter,k = 1)
					print("one_ran_scenario_from_tkq_inter ", one_ran_scenario_def)
					kiti_sharing = 1
				################ search kiqu
				if db_self.imposed_kind_game!='4P':
					kq_inter = list(set(resp_ques) & set(resp_kind))
				else:
					fcase = random.randint(0,1)
					dstop = 1
					into_psu = 0
					into_pco = 0
					while dstop>0:
						if fcase==0:
							tk_inter = list(set(resp_ques) & set(resp4psu))
							if not tk_inter and not into_pco:
								fcase = 1
								into_psu = 1
							else:
								dstop = 0
						if fcase==1:									
							tk_inter = list(set(resp_ques) & set(resp4pco))
							if not tk_inter and not into_psu:
								fcase = 1
								into_pco = 1
							else:
								dstop = 0
				if kq_inter:
					random.shuffle(kq_inter)
					one_ran_scenario_def = random.choices(kq_inter,k = 1)
					print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
					kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				if kitiqu_sharing:
					print("######## scenario imposed da tutti e 3 {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety_m3 = db_self.imposed_kind_game
						else:
							variety_m3 = 'SUNDRY'
					elif scenario_m4==0:
						scenario_m4 = scenario
						if random_var==0:
							variety_m4 = db_self.imposed_kind_game
						else:
							variety_m4 = 'SUNDRY'
				elif kiti_sharing:
					print("######## scenario imposed tiki sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					if scenario_m2==0:
						scenario_m2	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
					elif scenario_m4==0:
						scenario_m4	= scenario
						if random_var==0:
							variety = db_self.imposed_kind_game
						else:
							variety = 'SUNDRY'
				elif tiqu_sharing:
					print("######## scenario imposed tiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_t = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'							
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'	
					elif scenario_m4==0:
						scenario_m4 = scenario
						if random_var==0:
							variety = dbem2.find_kind(db_self.imposed_type_game)
						else:
							variety = 'SUNDRY'	
				elif kiqu_sharing:
					print("######## scenario imposed kiqu sce {}".format(scenario))
					db_self.suggested_scenario_from_k = copy.copy(scenario)
					db_self.suggested_scenario_from_q = copy.copy(scenario)
					if scenario_m1==0:
						scenario_m1 = scenario
						if random_var==0:
							variety_m1 = db_self.imposed_kind_game
						else:
							variety_m1 = 'SUNDRY'
					elif scenario_m2==0:
						scenario_m2 = scenario
						if random_var==0:
							variety_m2 = db_self.imposed_kind_game
						else:
							variety_m2 = 'SUNDRY'
					elif scenario_m3==0:
						scenario_m3 = scenario
						if random_var==0:
							variety_m3 = db_self.imposed_kind_game
						else:
							variety_m3 = 'SUNDRY'
					elif scenario_m4==0:
						scenario_m4 = scenario
						if random_var==0:
							variety_m4 = db_self.imposed_kind_game
						else:
							variety_m4 = 'SUNDRY'
		###########################
		###########################
		if db_self.imposed_type_game!='' and db_self.imposed_kind_game!='' and db_self.imposed_ques_id==99999:
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			################ search tiki
			if db_self.imposed_kind_game!='4P':
				tk_inter = list(set(resp_type) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_type) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_type) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if tk_inter:
				random.shuffle(tk_inter)
				one_ran_scenario_def = random.choices(tk_inter,k = 1)
				print("one_ran_scenario_from_tk_inter ", one_ran_scenario_def)
				kiti_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiki sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m3 = scenario
					if random_var==0:
						variety_m3 = db_self.imposed_kind_game
					else:
						variety_m3 = 'SUNDRY'						
				elif scenario_m4==0:
					scenario_m4 = scenario
					if random_var==0:
						variety_m4 = db_self.imposed_kind_game
					else:
						variety_m4 = 'SUNDRY'	
		if db_self.imposed_type_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_kind_game=='':
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search tiqu
			tq_inter = list(set(resp_ques) & set(resp_type))
			if tq_inter:
				tiqu_sharing = 1
				print("share scen tiqu!")
				random.shuffle(tq_inter)
				one_ran_scenario_def = random.choices(tq_inter,k = 1)
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed tiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_t = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m1 = 'SUNDRY'
				elif scenario_m2==0:
					scenario_m2 = scenario
					if random_var==0:
						variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m3 = scenario
					if random_var==0:
						variety_m3 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m3 = 'SUNDRY'
				elif scenario_m4==0:
					scenario_m4 = scenario
					if random_var==0:
						variety_m4 = dbem2.find_kind(db_self.imposed_type_game)
					else:
						variety_m4 = 'SUNDRY'
		if db_self.imposed_kind_game!='' and db_self.imposed_ques_id!=99999 and db_self.imposed_type_game=='':
			####
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			########################### search kiqu
			################ search kiqu
			if db_self.imposed_kind_game!='4P':
				kq_inter = list(set(resp_ques) & set(resp_kind))
			else:
				fcase = random.randint(0,1)
				dstop = 1
				into_psu = 0
				into_pco = 0
				while dstop>0:
					if fcase==0:
						tk_inter = list(set(resp_ques) & set(resp4psu))
						if not tk_inter and not into_pco:
							fcase = 1
							into_psu = 1
						else:
							dstop = 0
					if fcase==1:									
						tk_inter = list(set(resp_ques) & set(resp4pco))
						if not tk_inter and not into_psu:
							fcase = 1
							into_pco = 1
						else:
							dstop = 0
			if kq_inter:
				random.shuffle(kq_inter)
				one_ran_scenario_def = random.choices(kq_inter,k = 1)
				print("one_ran_scenario_from_kq_inter ", one_ran_scenario_def)
				kiqu_sharing = 1
			print("one_ran_scenario_def in comune trovato ...", one_ran_scenario_def)
			if one_ran_scenario_def:
				random_var = random.randint(0,3)
				########################################################################
				if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
					random_var = 1
				########################################################################
				scenario = one_ran_scenario_def[0]
				print("scenario trovato {} ".format(scenario))
				print("######## scenario imposed kiqu sce {}".format(scenario))
				db_self.suggested_scenario_from_k = copy.copy(scenario)
				db_self.suggested_scenario_from_q = copy.copy(scenario)
				if scenario_m1==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m1 = db_self.imposed_kind_game
					else:
						variety_m1 = 'SUNDRY'
				if scenario_m2==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m2 = db_self.imposed_kind_game
					else:
						variety_m2 = 'SUNDRY'
				elif scenario_m3==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m3 = db_self.imposed_kind_game
					else:
						variety_m3 = 'SUNDRY'
				elif scenario_m4==0:
					scenario_m1 = scenario
					if random_var==0:
						variety_m4 = db_self.imposed_kind_game
					else:
						variety_m4 = 'SUNDRY'
		###########################
		if not one_ran_scenario_def and db_self.imposed_type_game!='':
			print("bugia singola 1")
			####
			if db_self.imposed_type_game=='4PSU':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_type_game=='4PCO':
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE type=?""",(db_self.imposed_type_game,))
				sce_type = db_self.curs.fetchall()
			resp_type = [item for sublist in sce_type for item in sublist]
			random.shuffle(resp_type)
			scenario = random.choices(resp_type,k = 1)
			print("come stai a non andare caso s1 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal tipo!!! sce {}".format(scenario))
			db_self.suggested_scenario_from_t = copy.copy(scenario)
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					variety_m3 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m3 = 'SUNDRY'
			elif scenario_m4==0:
				scenario_m4 = scenario
				if random_var==0:
					variety_m4 = dbem2.find_kind(db_self.imposed_type_game)
				else:
					variety_m4 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_kind_game!='':
			print("bugia singola 2")
			if db_self.imposed_kind_game!='4P':
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) 
				USING (scenario_group) WHERE kind=?""",(db_self.imposed_kind_game,))
				scen_kind = db_self.curs.fetchall()
				resp_kind = [item for sublist in scen_kind for item in sublist]
			else:
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4psu_Questions""")
				db_self.curs.execute("""SELECT scenario_id FROM Scenarios_4pco_Questions""")
				scen4psu = db_self.curs.fetchall()
				resp4psu = [item for sublist in scen4psu for item in sublist]
				scen4pco = db_self.curs.fetchall()
				resp4pco = [item for sublist in scen4pco for item in sublist]
				ran4p = random.randint(0,1)
				if ran4p:
					resp_kind = resp4pco
				else:
					resp_kind = resp4psu
			random.shuffle(resp_kind)
			scenario = random.choices(resp_kind,k = 1)
			print("come stai a non andare caso s2 {} ".format(scenario))
			scenario = scenario[0]
			print("######## scenario imposed dal kind is {}".format(scenario))
			db_self.suggested_scenario_from_k = copy.copy(scenario)
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					variety_m1 = db_self.imposed_kind_game
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					variety_m2 = db_self.imposed_kind_game
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					variety_m3 = db_self.imposed_kind_game
				else:
					variety_m3 = 'SUNDRY'
			elif scenario_m4==0:
				scenario_m4 = scenario
				if random_var==0:
					variety_m4 = db_self.imposed_kind_game
				else:
					variety_m4 = 'SUNDRY'
		if not one_ran_scenario_def and db_self.imposed_ques_id!=99999:
			print("bugia singola 3")
			####												
			if db_self.imposed_ques_id in [3013,3014,3015,3016]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			elif db_self.imposed_ques_id in [3017,3018,3019,3020]:
				db_self.curs.execute("SELECT scenario_id FROM Scenarios_4psu_Questions")
				sce_type = db_self.curs.fetchall()
			else:
				db_self.curs.execute("""SELECT Scenarios.scenario_id FROM Scenarios JOIN (Scenarios_Questions JOIN Questions ON Scenarios_Questions.audio_id = Questions.audio) USING (scenario_group) 
				WHERE question_id=?""",(db_self.imposed_ques_id,))
				sce_ques = db_self.curs.fetchall()
			resp_ques = [item for sublist in sce_ques for item in sublist]
			######
			random.shuffle(resp_ques)
			scenario = random.choices(resp_ques,k = 1)
			print("come stai a non andare caso s3 {} ".format(scenario))
			scenario = scenario[0]
			db_self.suggested_scenario_from_q = copy.copy(scenario)
			print("ho appena settato db_self.suggested_scenario_from_q = copy.copy(scenario) {}".format(db_self.suggested_scenario_from_q))
			print("######## scenario imposed dalla question!!! sce {}".format(scenario))
			random_var = random.randint(0,3)
			########################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0:
				random_var = 1
			########################################################################
			if scenario_m1==0:
				scenario_m1 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			elif scenario_m2==0:
				scenario_m2 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			elif scenario_m3==0:
				scenario_m3 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			elif scenario_m4==0:
				scenario_m4 = scenario
				if random_var==0:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					variety_m4 = db_self.curs.fetchone()[0]
				else:
					variety_m4 = 'SUNDRY'	
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	#if there are scenarios still == to zero,,,,
	if 0 in (scenario_m1,scenario_m2,scenario_m3,scenario_m4):
		if scenario_m1==0:
			random_var_m1 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
				random_var_m1 = 1
			##################################################################################################
			if depends==0 and scenario_m2!=0:
				scenario_m1 = scenario_m2
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m1 = scenario_m3
				if random_var_m1==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m3,ki1,ki2,ki3,ki4,))
					variety_m1 = db_self.curs.fetchone()[0]
				else:
					variety_m1 = 'SUNDRY'
			else: #not if depends in (1,2): -->general case also if all scenario still == 0!!!
				if random_var_m1==0:
					print("difficulty_m1 {}".format(difficulty_m1))
					if difficulty_m1 in ('Easy_1','Easy_2'):
						variety_ava = ['1F','2L','3S']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1 in ('Normal_1','Normal_2'):
						variety_ava = ['1F','2L','3S','4P','5K']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Medium_2':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_1':
						variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					if difficulty_m1=='Hard_2':
						variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
						variety_m1 = random.choice(variety_ava)
					print("variety_m1 scelta is, ", variety_m1)
					db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m1,))
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario_m1 variety dependent non a caso {}".format(scenario_m1))
				else:
					variety_m1 = 'SUNDRY'
					db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
					scenario_m1 = db_self.curs.fetchone()[0]
					print("######## scenario a caso scelto sce {}".format(scenario_m1))
		if scenario_m2==0: #non è stato imposto scenario m2
			print("vado di scenario_m2 {}".format(scenario_m2))
			random_var_m2 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
				random_var_m2 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m2 = scenario_m1
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m2 = scenario_m3
				if random_var_m2==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m3,ki1,ki2,ki3,ki4,))
					variety_m2 = db_self.curs.fetchone()[0]
				else:
					variety_m2 = 'SUNDRY'
			else:
				while True:
					if random_var_m2==0:
						print("difficulty_m2 {}".format(difficulty_m2))
						if difficulty_m2 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						if difficulty_m2=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m2 = random.choice(variety_ava)
						print("variety_m2 scelta is, ", variety_m2)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m2,))
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario_m2 variety dependent non a caso {}".format(scenario_m2))
						if variety_m1=='SUNDRY' or variety_m1!=variety_m2:
							break
						else:	
							ran2 = random.randint(0,3)
							if ran2==0:
								random_var_m2 = 1					
					if random_var_m2!=0:
						variety_m2 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m2 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m2))
						break
		if scenario_m3==0: #non è stato imposto scenario m3
			print("vado di scena m3 è ancora a zero")
			random_var_m3 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
				random_var_m3 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m3 = scenario_m1
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m3 = scenario_m2
				if random_var_m3==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m3 = db_self.curs.fetchone()[0]
				else:
					variety_m3 = 'SUNDRY'
			else:
				while True:
					if random_var_m3==0:
						print("difficulty_m3 {}".format(difficulty_m3))
						if difficulty_m3 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						if difficulty_m3=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m3 = random.choice(variety_ava)
						print("variety_m3 scelta is, ", variety_m3)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m3,))
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario_m3 variety dependent non a caso {}".format(scenario_m3))
						if variety_m2=='SUNDRY' or variety_m3!=variety_m2:
							break
						else:
							ran3 = random.randint(0,3)
							if ran3==0:
								random_var_m3 = 1
					if random_var_m3!=0:
						variety_m3 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m3 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m3))
						break
		if scenario_m4==0: #non è stato imposto scenario m4
			print("vado di scanrio m4 perche ancora a zero")
			random_var_m4 = random.randint(0,2)
			depends = random.randint(0,2)
			##################################################################################################
			if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m4:
				random_var_m4 = 1
			##################################################################################################
			if depends==0 and scenario_m1!=0:
				scenario_m4 = scenario_m1
				if random_var_m4==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m1,ki1,ki2,ki3,ki4,))
					variety_m4 = db_self.curs.fetchone()[0]
				else:
					variety_m4 = 'SUNDRY'
			elif depends==0 and scenario_m2!=0:
				scenario_m4 = scenario_m2
				if random_var_m4==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m2,ki1,ki2,ki3,ki4,))
					variety_m4 = db_self.curs.fetchone()[0]
				else:
					variety_m4 = 'SUNDRY'
			elif depends==0 and scenario_m3!=0:
				scenario_m4 = scenario_m3
				if random_var_m4==0:
					#scelgo un kind che vada bene con lo scenario imposto
					ki1,ki2,ki3,ki4 = '4P','6I','8Q','9C'
					db_self.curs.execute("SELECT kind FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE scenario_id = ? and kind not in (?,?,?,?) ORDER BY RANDOM() LIMIT 1",(scenario_m3,ki1,ki2,ki3,ki4,))
					variety_m4 = db_self.curs.fetchone()[0]
				else:
					variety_m4 = 'SUNDRY'
			else:
				while True:
					if random_var_m4==0:
						print("difficulty_m4 {}".format(difficulty_m4))
						if difficulty_m4 in ('Easy_1','Easy_2'):
							variety_ava = ['1F','2L','3S']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4 in ('Normal_1','Normal_2'):
							variety_ava = ['1F','2L','3S','4P','5K']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Medium_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Medium_2':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Hard_1':
							variety_ava = ['1F','2L','3S','4P','5K','7O','8Q','9C']
							variety_m4 = random.choice(variety_ava)
						if difficulty_m4=='Hard_2':
							variety_ava = ['1F','2L','3S','4P','5K','6I','7O','8Q','9C']
							variety_m4 = random.choice(variety_ava)
						print("variety_m4 scelta is, ", variety_m4)
						db_self.curs.execute("SELECT scenario_id FROM Scenarios JOIN Scenarios_Kinds USING (scenario_group) WHERE kind = ? ORDER BY RANDOM() LIMIT 1",(variety_m4,))
						scenario_m4 = db_self.curs.fetchone()[0]
						print("######## scenario_m4 variety dependent non a caso {}".format(scenario_m4))
						if variety_m3=='SUNDRY' or variety_m4!=variety_m3:
							break
						else:	
							ran4 = random.randint(0,3)
							if ran4==0:
								random_var_m4 = 1
					if random_var_m4!=0:
						variety_m4 = 'SUNDRY'
						db_self.curs.execute("SELECT scenario_id FROM Scenarios ORDER BY RANDOM() LIMIT 1")
						scenario_m4 = db_self.curs.fetchone()[0]
						print("######## scenario a caso scelto sce {}".format(scenario_m4))
						break
	###########################################################
	may_swap = random.randint(0,4)
	if may_swap==0:
		scenario_m1, scenario_m2 = scenario_m2, scenario_m1
		variety_m1, variety_m2 = variety_m2, variety_m1
		print("ho fatto swap caso 0")
	if may_swap==1:
		scenario_m1, scenario_m3 = scenario_m3, scenario_m1
		variety_m1, variety_m3 = variety_m3, variety_m1
		print("ho fatto swap caso 1")
	if may_swap==2:
		scenario_m1, scenario_m4 = scenario_m4, scenario_m1
		variety_m1, variety_m4 = variety_m4, variety_m1
		print("ho fatto swap caso 2")
	else:
		print("non c'è stato swap")
	###########################################################
	print("variety 1 is ==================================================================> ",variety_m1)
	print("variety 2 is ==================================================================> ",variety_m2)
	print("variety 3 is ==================================================================> ",variety_m3)
	print("variety 4 is ==================================================================> ",variety_m4)
	if variety_m1=='SUNDRY':
		a_crite = random.randint(0,2)
		if a_crite==0:
			criteria_arrangement_m1 = 'random'
		else:
			criteria_arrangement_m1 = 'regular'
	else:
		criteria_arrangement_m1 = 'ignore'
	if variety_m2=='SUNDRY':
		a_crite = random.randint(0,2)
		if a_crite==0:
			criteria_arrangement_m2 = 'random'
		else:
			criteria_arrangement_m2 = 'regular'
	else:
		criteria_arrangement_m2 = 'ignore'
	if variety_m3=='SUNDRY':
		a_crite = random.randint(0,2)
		if a_crite==0:
			criteria_arrangement_m3 = 'random'
		else:
			criteria_arrangement_m3 = 'regular'
	else:
		criteria_arrangement_m3 = 'ignore'
	if variety_m4=='SUNDRY':
		a_crite = random.randint(0,2)
		if a_crite==0:
			criteria_arrangement_m4 = 'random'
		else:
			criteria_arrangement_m4 = 'regular'
	else:
		criteria_arrangement_m4 = 'ignore'
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m1:
		criteria_arrangement_m1 = 'random'
		db_self.already_done_imposed_mixed = 1 
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m2:
		criteria_arrangement_m2 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################	
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m3:
		criteria_arrangement_m3 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################
	if db_self.imposed_mixed!=0 and db_self.already_done_imposed_mixed==0 and mix_imposed_in_m4:
		criteria_arrangement_m4 = 'random'
		db_self.already_done_imposed_mixed = 1
	##################################################################################################

	dca.add_new_Match_table(db_self, difficulty_m1, num_of_games_m1, variety_m1, order_diff_games_m1, order_quan_ques_m1, criteria_arrangement_m1, advisable_time, scenario_m1, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m2, num_of_games_m2, variety_m2, order_diff_games_m2, order_quan_ques_m2, criteria_arrangement_m2, advisable_time, scenario_m2, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m3, num_of_games_m3, variety_m3, order_diff_games_m3, order_quan_ques_m3, criteria_arrangement_m3, advisable_time, scenario_m3, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)

	dca.add_new_Match_table(db_self, difficulty_m4, num_of_games_m4, variety_m4, order_diff_games_m4, order_quan_ques_m4, criteria_arrangement_m4, advisable_time, scenario_m4, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchone()[0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
	
