"""
Database Manager setup file
to speed up setup, better run few extensions at time, commenting the others

"""
# ==========================================================================================================================================================
#  Imports:
# ==========================================================================================================================================================
import os
import path
import io
import sys
import shutil
from distutils.core import setup
import setuptools
from distutils.extension import Extension
from Cython.Build import build_ext
from Cython.Build import cythonize
import numpy
#a ==========================================================================================================================================================
#  Variables:
# ==========================================================================================================================================================
readme_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), './README_database_manager.md')

""" Paths to include the pxd files that are without pyx + the imported modules that are in different folders than the setup"""
included_path_0 = os.path.abspath('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/audio_blob_conversion')
included_path_1 = os.path.abspath('/home/pi/OIMI/oimi_code/src/')
if 'SRC_PATH' not in os.environ:
	os.environ['included_path_0'] = included_path_0
	os.environ['included_path_1'] = included_path_1
else:
	included_path_0 = os.environ['included_path_0']
	included_path_1 = os.environ['included_path_1']
# ==========================================================================================================================================================
#  Extension Modules:
# ==========================================================================================================================================================
#dont comment with """, it will not work! use only # 
# non mettere in cartelle separate nulla!
ext_modules = [
 	Extension("modify_default_lists", ["modify_default_lists.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(),included_path_0,included_path_1],
		language='c++'),
	Extension("database_triggers", ["database_triggers.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(),included_path_0,included_path_1],
		language='c++'),	
	Extension("database_controller_build", ["database_controller_build.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(),included_path_0, included_path_1],
		language='c++'),
	Extension("database_controller_populate", ["database_controller_populate.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(),included_path_0,included_path_1],
		language='c++'),
	Extension("database_controller_generate_online", ["database_controller_generate_online.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(),included_path_0,included_path_1],
		language='c++'),
	Extension("database_controller_delete", ["database_controller_delete.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(),included_path_0,included_path_1],
		language='c++'),
	Extension("database_controller_add_into", ["database_controller_add_into.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("database_controller_add_into_part_2", ["database_controller_add_into_part_2.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(),included_path_0,included_path_1],
		language='c++'),
	Extension("database_controller_add_into_part_2_1", ["database_controller_add_into_part_2_1.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(),included_path_0,included_path_1],
		language='c++'),
	Extension("database_controller_add_into_part_3", ["database_controller_add_into_part_3.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(),included_path_0,included_path_1],
		language='c++'),
	Extension("database_controller_add_into_part_4", ["database_controller_add_into_part_4.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include(),included_path_0,included_path_1],
		language='c++'),
	Extension("database_controller_add_into_part_5", ["database_controller_add_into_part_5.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("database_controller_add_into_part_6", ["database_controller_add_into_part_6.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("database_controller_add_into_part_7", ["database_controller_add_into_part_7.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	##################################################################################
	Extension("database_controller_take", ["database_controller_take.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("database_controller_update", ["database_controller_update.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("database_controller_save_to_files", ["database_controller_save_to_files.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	###################################################################################
	Extension("database_controller_renew", ["database_controller_renew.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("database_controller_update_second", ["database_controller_update_second.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("database_controller_check_mixed", ["database_controller_check_mixed.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("database_controller_fix_regular_match", ["database_controller_fix_regular_match.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("database_manager", ["database_manager.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	##################################################################################
	Extension("add_prediction_6", ["add_prediction_6.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("add_prediction_5", ["add_prediction_5.pyx"],
	    extra_compile_args=['-O3','-w','-fopenmp'],
	    extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	    include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	    language='c++'),
	Extension("add_prediction_4", ["add_prediction_4.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("add_prediction_3", ["add_prediction_3.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("add_prediction_2", ["add_prediction_2.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("add_prediction_second_half", ["add_prediction_second_half.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	##################################################################################
	Extension("add_prediction", ["add_prediction.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("add_customized_prediction_second_half", ["add_customized_prediction_second_half.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("add_customized_prediction", ["add_customized_prediction.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	Extension("security_prediction", ["security_prediction.pyx"],
	 	extra_compile_args=['-O3','-w','-fopenmp'],
	 	extra_link_args=['-fopenmp','-ffast-math','-march=native'],
	 	include_dirs=[numpy.get_include(),included_path_0,included_path_1],
	 	language='c++'),
	]
# ========================================================================================================================================================
#  Methods:
# ========================================================================================================================================================
def clean():
	""" Cleaning ...
	Remove all .cpp and .so files with a certain name
	For all files in the whole folder tree, walking bottom-up
	Remove also build directory
	"""
	print("Cleaning ...")
	for root, dirs, files in os.walk(".", topdown=False): 
		for name in files:
			if (name.startswith("modify_default_lists") and not(name.endswith(".pyx")) and not(name.endswith(".pxd"))) or \
				(name.startswith("database_manager") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))   or \
				(name.startswith("database_triggers") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_build") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_populate") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_add_into") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_add_into_part_2") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_add_into_part_2_1") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_add_into_part_3") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_add_into_part_4") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_add_into_part_5") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_add_into_part_6") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_take") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_save_to_files") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_delete") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_generate_online") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_renew") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_update") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("database_controller_update_second") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))	 or \
				(name.startswith("add_prediction") and not(name.endswith(".pyx")) and not(name.endswith(".pxd"))) or \
				(name.startswith("add_prediction_second_half") and not(name.endswith(".pyx")) and not(name.endswith(".pxd"))):
				(name.startswith("add_prediction_2") and not(name.endswith(".pyx")) and not(name.endswith(".pxd"))) or \
				(name.startswith("add_prediction_3") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))   or \
				(name.startswith("add_prediction_4") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))   or \
				(name.startswith("add_prediction_5") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))   or \
				(name.startswith("add_prediction_6") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))   or \
				(name.startswith("security_prediction") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))   or \
				(name.startswith("add_customized_prediction") and not(name.endswith(".pyx")) and not(name.endswith(".pxd"))) or \
				(name.startswith("add_customized_prediction_second_half") and not(name.endswith(".pyx")) and not(name.endswith(".pxd")))   or \
				os.remove(os.path.join(root, name))									  
		for name in dirs:
			if name in ["build","__pycache__"]:
				shutil.rmtree(name)
# ==========================================================================================================================================================
#  Class:
# ==========================================================================================================================================================
class BuildExt(build_ext):
	""" Extend Cyhon extension builder, necessary for removing annoying warnings"""
	def build_extensions(self):
		print("Building database_manager package")
		if '-Wstrict-prototypes' in self.compiler.compiler_so:
			self.compiler.compiler_so.remove('-Wstrict-prototypes')
		super().build_extensions()
# ==========================================================================================================================================================
#  Main:
# ==========================================================================================================================================================
if sys.version_info < (3, 5):
	sys.exit("Have you activated the corrent python environment? oimi doesnt support a py version < 3.7")
	
#clean()

#for e in ext_modules:
#	e.cython_directives = {'embedsignature': True, 'boundscheck': False, 'wraparound': False, 'linetrace': True, 'language_level': "3"}

for e in ext_modules:
	e.cython_directives = {'embedsignature': True, 'boundscheck': False, 'wraparound': False, 'linetrace': True, 'language_level': "3"}


setup(
	name='oimi-robot source compilation',
	version='2.1.0',
	author='Colombo Giacomo',
	long_description=io.open(readme_file, 'rt', encoding='utf-8').read(),
	ext_modules=ext_modules,
	cmdclass = {'build_ext': BuildExt},
	)
