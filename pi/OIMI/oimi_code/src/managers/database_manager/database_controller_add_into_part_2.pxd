# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Define
# ----------------------------------------------------------------------------------------------------------------------------------------------------------			
cdef define_matches_when_complexity_is_easy(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_easy_2(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_easy_3(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_easy_4(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef define_matches_when_complexity_is_normal(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_normal_2(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_normal_3(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
cdef def_matches_when_complexity_is_normal_4(db_self, int num_of_matches, int sess_last, str order_diff_matches, str order_quan_games)
	
