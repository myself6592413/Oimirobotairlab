"""Info:
	Oimi robot database_controller_populate, for update tables regarding Patches, Scenarios, and summary tables.
Notes: For detailed info, look at ./code_docs_of_dabatase_manager --> modify_default_lists
Created by Colombo Giacomo, Politecnico di Milano 2020 """
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Place
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef place_patches_into_Patches_table(db_self)
cdef place_entertainments_into_table(db_self)
cdef place_data_into_Questions_Patches_table(db_self)
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Insert
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef insert_in_scenarios_table(db_self)
cdef insert_in_questions_table(db_self)
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Populate
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef populate_scenarios_types_table(db_self)
cdef populate_scenarios_kinds_table(db_self)
cdef populate_scenarios_questions_table(db_self)
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Fill
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef fill_recap_table(db_self, kid)
cdef fill_summary_table(db_self, kid)
