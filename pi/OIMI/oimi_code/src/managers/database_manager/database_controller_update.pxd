# ----------------------------------------------------------------------------------------------------------------------------------------
# Update 
# ----------------------------------------------------------------------------------------------------------------------------------------
cdef update_Needs_constraints(db_self, str focus_contr, str need_contr, str position)

cdef update_Kind_constraints(db_self, str new_kind, str new_details)
	
cdef update_Patches_genre_1(db_self, str new_genre)

cdef update_Patches_genre_2(db_self, str new_subject, str new_genre)

cdef update_Patches_constraints(db_self, str new_subject, str new_color)

cdef update_constraint_of(db_self, str constraint_str, str table)
# ----------------------------------------------------------------------------------------------------------------------------------------
# Insert
# ----------------------------------------------------------------------------------------------------------------------------------------
cdef insert_new_Audios_table(db_self, str constraint_audio, str audio_path, int is_a_substitution)

cdef insert_new_Achievements_table(db_self, str constraint_str)

cdef insert_new_Needs_table(db_self, str constraint_str_1, str constraint_str_2, str scope)
cdef insert_new_Symptoms_table(db_self, str constraint_str)
cdef insert_new_Impositions_table(db_self, str constraint_str)

cdef insert_new_Kind_of_games_table(db_self, str constraint_kind, str details_kind)

cdef insert_new_Type_of_games_table(db_self, str constraint_type, str details, str example, str kind)

cdef insert_new_Patches_table(db_self, str new_subject, str new_color, str new_genre)
# ----------------------------------------------------------------------------------------------------------------------------------------
# Refresh
# ----------------------------------------------------------------------------------------------------------------------------------------
cdef refresh_Patches_table(db_self, str new_subject, str new_color, str new_genre)
cdef refresh_Treatments_Pastimes_table(db_self)
# ----------------------------------------------------------------------------------------------------------------------------------------
# Put
# ----------------------------------------------------------------------------------------------------------------------------------------
cpdef put_kid_questions_scores(db_self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
	str quantity_feedback_param, str grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest)
cpdef put_kid_games_scores(db_self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
	str quantity_feedback_param, str grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest)
cpdef put_kid_matches_scores(db_self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
	int quantity_feedback_param, int grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest)
cpdef put_kid_session_scores(db_self, float time_param, int quantity_feedback_param, int grade_feedback_param, float value_feedback_param,
	int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest)
# ----------------------------------------------------------------------------------------------------------------------------------------
# Include
# ----------------------------------------------------------------------------------------------------------------------------------------
cdef include_all_feeds_in_once(db_self, int kid_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds)
cdef include_all_results_in_once(db_self, int kid_id, float time_sess, list questions_results, list matches_results, list games_results)
cdef include_all_feeds_in_once_given_session(db_self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds)
cdef include_all_results_in_once_given_session(db_self, int kid_id, int session_id, float time_sess, list questions_results, list matches_results, list games_results)

cdef include_new_therapist_grade_session(db_self, int treat_id, int sess_id, str grade)
cdef include_new_therapist_grade_entertainment(db_self, int treat_id, int enter_id, str grade)