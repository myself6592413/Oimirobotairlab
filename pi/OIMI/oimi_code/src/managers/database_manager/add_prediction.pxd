from database_manager cimport DatabaseManager

cdef class DatabaseManager_2(DatabaseManager):
	cdef:
		list kids_needs
		list kids_syms
		list csv_lines
		outputs_table_1
		outputs_table_2
		outputs_table_3
		outputs_table_4_m1
		outputs_table_4_m2
		outputs_table_4_m3
		outputs_table_4_m4
		outputs_table_5_m1_g1
		outputs_table_5_m2_g1
		outputs_table_5_m3_g1
		outputs_table_5_m4_g1
		outputs_table_5_m1_g2
		outputs_table_5_m2_g2
		outputs_table_5_m3_g2
		outputs_table_5_m4_g2
		outputs_table_5_m1_g3
		outputs_table_5_m2_g3
		outputs_table_5_m3_g3
		outputs_table_5_m4_g3
		outputs_table_5_m1_g4
		outputs_table_5_m2_g4
		outputs_table_5_m3_g4
		outputs_table_5_m4_g4

		bint movements_allowed
		bint extra_time_tolerance
		bint body_enabled
		bint exhortations
		bint audio_slow
		bint repeat_intro
		bint rules_explication
		bint repeat_question
		bint disposable
		bint ask_never_asked_q
		bint stress_flag
		bint plus_one_game
		int desirable_time

		str search_str
		int new_deleletion_index
		str new_id_token
		str led_brightness

		int num_of_matches
		str complexity
		str matches_order_diff
		str order_quan_games
		str order_diff_matches
		str difficulty_mat_1
		str difficulty_mat_2
		str difficulty_mat_3
		str difficulty_mat_4
		int games_m1
		int games_m2
		int games_m3
		int games_m4
		str diff_m1_g1
		str diff_m1_g2
		str diff_m1_g3
		str diff_m1_g4
		str diff_m2_g1
		str diff_m2_g2
		str diff_m2_g3
		str diff_m2_g4
		str diff_m3_g1
		str diff_m3_g2
		str diff_m3_g3
		str diff_m3_g4		
		str diff_m4_g1
		str diff_m4_g2
		str diff_m4_g3
		str diff_m4_g4
		str kind_m1_g1
		str kind_m1_g2
		str kind_m1_g3
		str kind_m1_g4
		str kind_m2_g1
		str kind_m2_g2
		str kind_m2_g3
		str kind_m2_g4
		str kind_m3_g1
		str kind_m3_g2
		str kind_m3_g3
		str kind_m3_g4
		str kind_m4_g1
		str kind_m4_g2
		str kind_m4_g3
		str kind_m4_g4
		int m1_questions_g1
		int m1_questions_g2
		int m1_questions_g3
		int m1_questions_g4
		int m2_questions_g1
		int m2_questions_g2
		int m2_questions_g3
		int m2_questions_g4
		int m3_questions_g1
		int m3_questions_g2
		int m3_questions_g3
		int m3_questions_g4
		int m4_questions_g1
		int m4_questions_g2
		int m4_questions_g3
		int m4_questions_g4
		str order_quan_quest_m1
		str order_quan_quest_m2
		str order_quan_quest_m3
		str order_quan_quest_m4
		str order_diff_games_m1
		str order_diff_games_m2
		str order_diff_games_m3
		str order_diff_games_m4		
		str m1_g1_order_diff_ques
		str m1_g2_order_diff_ques
		str m1_g3_order_diff_ques
		str m1_g4_order_diff_ques
		str m2_g1_order_diff_ques
		str m2_g2_order_diff_ques
		str m2_g3_order_diff_ques
		str m2_g4_order_diff_ques
		str m3_g1_order_diff_ques
		str m3_g2_order_diff_ques
		str m3_g3_order_diff_ques
		str m3_g4_order_diff_ques
		str m4_g1_order_diff_ques
		str m4_g2_order_diff_ques
		str m4_g3_order_diff_ques
		str m4_g4_order_diff_ques
		str type_m1_g1_1r
		str type_m1_g1_2r
		str type_m1_g1_3r
		str type_m1_g1_4r
		str type_m1_g2_1r
		str type_m1_g2_2r
		str type_m1_g2_3r
		str type_m1_g2_4r
		str type_m1_g3_1r
		str type_m1_g3_2r
		str type_m1_g3_3r
		str type_m1_g3_4r
		str type_m1_g4_1r
		str type_m1_g4_2r
		str type_m1_g4_3r
		str type_m1_g4_4r
		str type_m2_g1_1r
		str type_m2_g1_2r
		str type_m2_g1_3r
		str type_m2_g1_4r
		str type_m2_g2_1r
		str type_m2_g2_2r
		str type_m2_g2_3r
		str type_m2_g2_4r
		str type_m2_g3_1r
		str type_m2_g3_2r
		str type_m2_g3_3r
		str type_m2_g3_4r
		str type_m2_g4_1r
		str type_m2_g4_2r
		str type_m2_g4_3r
		str type_m2_g4_4r
		str type_m3_g1_1r
		str type_m3_g1_2r
		str type_m3_g1_3r
		str type_m3_g1_4r
		str type_m3_g2_1r
		str type_m3_g2_2r
		str type_m3_g2_3r
		str type_m3_g2_4r
		str type_m3_g3_1r
		str type_m3_g3_2r
		str type_m3_g3_3r
		str type_m3_g3_4r
		str type_m3_g4_1r
		str type_m3_g4_2r
		str type_m3_g4_3r
		str type_m3_g4_4r
		str type_m4_g1_1r
		str type_m4_g1_2r
		str type_m4_g1_3r
		str type_m4_g1_4r
		str type_m4_g2_1r
		str type_m4_g2_2r
		str type_m4_g2_3r
		str type_m4_g2_4r
		str type_m4_g3_1r
		str type_m4_g3_2r
		str type_m4_g3_3r
		str type_m4_g3_4r
		str type_m4_g4_1r
		str type_m4_g4_2r
		str type_m4_g4_3r
		str type_m4_g4_4r

		int advisable_time_m1
		int advisable_time_m2
		int advisable_time_m3
		int advisable_time_m4
		int necessary_time_g1_m1
		int necessary_time_g2_m1
		int necessary_time_g3_m1
		int necessary_time_g4_m1
		int necessary_time_g1_m2
		int necessary_time_g2_m2
		int necessary_time_g3_m2
		int necessary_time_g4_m2
		int necessary_time_g1_m3
		int necessary_time_g2_m3
		int necessary_time_g3_m3
		int necessary_time_g4_m3
		int necessary_time_g1_m4
		int necessary_time_g2_m4
		int necessary_time_g3_m4
		int necessary_time_g4_m4

		int a1_max_question_game
		int a2_max_game_match
		int a2_match_session_input 
		str a1_range_num_of_matches_with_games_order_of_difficulty
		str level_input
		str n1_focus_on_comprehension_input
		str n1_focus_on_concentration_input
		str n1_focus_on_errors_input
		str n2_focus_on_automation_input
		str n2_focus_on_resistance_input
		str n4_focus_on_changes_input
		str s1_understanding_input
		str s2_restrictions_of_interest_input
		str s3_distraction_in_listening_input
		str s3_executing_long_tasks_input
		str s3_meltdown_input
		str s3_memoria_deficit_input
		str s1_diversion_input
		str s2_endurance_input 
		str a1_range_num_input 
		str a1_range_diff_matches_done_input 
		str a2_num_of_matches_with_games_quan_ques_input 
		str a3_order_diff_match_input 
		str a3_num_of_session_with_matches_quan_games_input 

		str variety_m1
		str variety_m2
		str variety_m3
		str variety_m4
		str criteria_m1
		str criteria_m2
		str criteria_m3
		str criteria_m4

		int kid_id
		str status
	# --------------------------------------------------------------------------------------------------------------------------------------
	# Methods
	# --------------------------------------------------------------------------------------------------------------------------------------
	cdef take_csvs(self)
	cpdef add_new_Session_table_with_kid(self, int kid_id, int mandatory_imposition, int mandatory_needs)

