""" Oimi robot database_manager add aggiuntivo scrivo qiualcopsa di sensatodsa as,
code used for generating or change the whole DB infrastructure
--> offline, before powering-on the robot
	e.g. maintenance / updates 
	e.g. kids info before starting execution of games 
Notes: Called by add_predic
Created by Colombo Giacomo, Politecnico di Milano 2020 """

# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import random
from database_manager cimport DatabaseManager
cimport add_prediction_2
# ==========================================================================================================================================================
# Classes
# ==========================================================================================================================================================
cdef extract_necessary_features_for_prediction(db_self, kid_id):
	print("sono in extract_necessary_features_for_prediction")
	print("sono in extract_necessary_features_for_prediction")
	cdef:
		kids_needs
		kids_syms
	s1_diversion_input = 'no'
	s1_understanding_input = 'no'
	s2_endurance_input = 'no'
	s2_restrictions_of_interest_input = 'no'
	s3_meltdown_input = 'no'
	s3_executing_long_tasks_input = 'no'
	s3_distraction_in_listening_input = 'no'
	s3_memoria_deficit_input = 'no'

	db_self.curs.execute("SELECT current_level FROM Kids WHERE kid_id = ?",(kid_id,))
	level_input = db_self.curs.fetchall()[0][0]
	db_self.curs.execute("SELECT symptom_name FROM Kids_Symptoms WHERE kid_id = ?",(kid_id,))
	res_sym = db_self.curs.fetchall()
	kids_syms = [item for sublist in res_sym for item in sublist]
	#all symptomps here, despite on whch table
	if 'distraction_in_activity' in kids_syms:
		s1_diversion_input = 'yes'
	if 'endurance' in kids_syms:
		s2_endurance_input = 'yes'
	if 'meltdown' in kids_syms:
		s3_meltdown_input = 'yes'
	if 'understanding' in kids_syms:
		s1_understanding_input = 'yes'
	if 'restrictions_of_interest' in kids_syms:
		s2_restrictions_of_interest_input = 'yes'
	if 'executing_long_tasks' in kids_syms:
		s3_executing_long_tasks_input = 'yes'
	if 'distraction_in_listening' in kids_syms:
		s3_distraction_in_listening_input = 'yes'
	if 'memoria_deficit' in kids_syms:
		s3_memoria_deficit_input = 'yes'
	
	db_self.curs.execute("SELECT * FROM Full_Sessions_played_recap fs JOIN Sessions USING (session_id) WHERE kid_id = ? AND fs.complexity = 'Easy' GROUP BY session_id", (kid_id,))
	sess_easy = db_self.curs.fetchall()
	num_sess_easy = len(sess_easy)
	db_self.curs.execute("SELECT * FROM Full_Sessions_played_recap fs JOIN Sessions USING (session_id) WHERE kid_id = ? AND fs.complexity = 'Normal' GROUP BY session_id", (kid_id,))
	sess_normal = db_self.curs.fetchall()
	num_sess_normal = len(sess_normal)
	db_self.curs.execute("SELECT * FROM Full_Sessions_played_recap fs JOIN Sessions USING (session_id) WHERE kid_id = ? AND fs.complexity = 'Medium' GROUP BY session_id", (kid_id,))
	sess_medium = db_self.curs.fetchall()
	num_sess_medium = len(sess_medium)
	db_self.curs.execute("SELECT * FROM Full_Sessions_played_recap fs JOIN Sessions USING (session_id) WHERE kid_id = ? AND fs.complexity = 'Hard' GROUP BY session_id", (kid_id,))
	sess_hard = db_self.curs.fetchall()
	num_sess_hard = len(sess_hard)

	if num_sess_easy==0: 
		first_part_a1 = 'A'
	if 0 < num_sess_easy <=3: 
		first_part_a1 = 'B'
	if 3 < num_sess_easy <=6:
		first_part_a1 = 'C'
	if num_sess_easy > 6:
		first_part_a1 = 'D'
	if num_sess_normal==0: 
		second_part_a1 = 'A'
	if 0 < num_sess_normal <=3: 
		second_part_a1 = 'B'
	if 3 < num_sess_normal <=6:
		second_part_a1 = 'C'
	if num_sess_normal > 6:
		second_part_a1 = 'D'
	if num_sess_medium==0: 
		third_part_a1 = 'A'
	if 0 < num_sess_medium <=3: 
		third_part_a1 = 'B'
	if 3 < num_sess_medium <=6:
		third_part_a1 = 'C'
	if num_sess_medium > 6:
		third_part_a1 = 'D'
	if num_sess_hard==0: 
		fourth_part_a1 = 'A'
	if 0 < num_sess_hard <=3: 
		fourth_part_a1 = 'B'
	if 3 < num_sess_hard <=6:
		fourth_part_a1 = 'C'
	if num_sess_hard > 6:
		fourth_part_a1 = 'D'

	a1_range_num_input = '{}{}{}{}'.format(first_part_a1,second_part_a1,third_part_a1,fourth_part_a1)
	print("a1_range_num_input ====> {}".format(a1_range_num_input))
	verify_me = ['AAAA','BAAA','ABAA','CAAA','ACAA','BBAA','BBBA','BBAB','BBBB','CBAA','CBAB','CBBA','CBBB','BCAA','BCAB','BCBA','BCBB','CCBB','CBCB','CBBC','CCCB','CCCC']
	while a1_range_num_input not in verify_me:
		where = random.randint(0,3)
		depend = random.randint(0,1)
		if a1_range_num_input.find('D')!=-1:
			a1_range_num_input = a1_range_num_input.replace('D','C')
			continue
		elif a1_range_num_input.find('C')!=-1 and depend==1: 
			a1_range_num_input = a1_range_num_input.replace('C','B',where)
			continue
		elif a1_range_num_input.find('B')!=-1 and depend==0: 
			a1_range_num_input = a1_range_num_input.replace('B','A',where)
			continue			
		else:	 
			pos = random.randint(0,3)
			place = a1_range_num_input.find('D')
			if place==-1:
				place = a1_range_num_input.find('C')
			if place==-1:
				place = a1_range_num_input.find('B')
			strli = list(a1_range_num_input)
			strli[pos], strli[place] = strli[place], strli[pos]
			a1_range_num_input = "".join(strli)

	db_self.curs.execute("SELECT num_of_matches FROM Full_Sessions_played_recap WHERE kid_id = ? GROUP BY match_id",(kid_id,))
	a2_tmp = db_self.curs.fetchall()
	a2_tmp_list = [item for sublist in a2_tmp for item in sublist]
	a2_match_session_input = max(a2_tmp_list)
	
	db_self.curs.execute("SELECT * FROM Sessions s JOIN Full_Sessions_played_recap USING(session_id) WHERE kid_id = ? AND s.order_difficulty_matches='same' GROUP BY match_id",(kid_id,))
	a3_same = db_self.curs.fetchall()
	num_a3_same = len(a3_same)
	db_self.curs.execute("SELECT * FROM Sessions s JOIN Full_Sessions_played_recap USING(session_id) WHERE kid_id = ? AND s.order_difficulty_matches='casu' GROUP BY match_id",(kid_id,))
	a3_casu = db_self.curs.fetchall()
	num_a3_casu = len(a3_casu)
	db_self.curs.execute("SELECT * FROM Sessions s JOIN Full_Sessions_played_recap USING(session_id) WHERE kid_id = ? AND s.order_difficulty_matches='asc' GROUP BY match_id",(kid_id,))
	a3_asc = db_self.curs.fetchall()
	num_a3_asc = len(a3_asc)
	db_self.curs.execute("SELECT * FROM Sessions s JOIN Full_Sessions_played_recap USING(session_id) WHERE kid_id = ? AND s.order_difficulty_matches='desc' GROUP BY match_id",(kid_id,))
	a3_desc = db_self.curs.fetchall()
	num_a3_desc = len(a3_desc)

	if num_a3_same==0:
		first_part_a3 = 'A'
	if 0<num_a3_same<=3:
		first_part_a3 = 'B'
	if 3<num_a3_same<=6:
		first_part_a3 = 'C'
	if num_a3_same>6:
		first_part_a3 = 'D'
	if num_a3_casu==0:
		second_part_a3 = 'A'
	if 0<num_a3_casu<=3:
		second_part_a3 = 'B'
	if 3<num_a3_casu<=6:
		second_part_a3 = 'C'
	if num_a3_casu>6:
		second_part_a3 = 'D'
	if num_a3_asc==0:
		third_part_a3 = 'A'
	if 0<num_a3_asc<=3:
		third_part_a3 = 'B'
	if 3<num_a3_asc<=6:
		third_part_a3 = 'C'
	if num_a3_asc>6:
		third_part_a3 = 'D'
	if num_a3_desc==0:
		fourth_part_a3 = 'A'
	if 0<num_a3_desc<=3:
		fourth_part_a3 = 'B'
	if 3<num_a3_desc<=6:
		fourth_part_a3 = 'C'
	if num_a3_desc>6:
		fourth_part_a3 = 'D'
	a3_order_diff_match_input = '{}{}{}{}'.format(first_part_a3, second_part_a3, third_part_a3, fourth_part_a3)
	print("a3_order_diff_match_input ====> {}".format(a3_order_diff_match_input))

	verify_me = ['AAAA','BAAA','BBAA','BBBA','BBBB','CBBB','CCBB','CBCB','CBBC','CCCB','CCCC']
	while a3_order_diff_match_input not in verify_me:
		where = random.randint(0,3)
		depend = random.randint(0,1)
		if a3_order_diff_match_input.find('D')!=-1:
			a3_order_diff_match_input = a3_order_diff_match_input.replace('D','C')
			continue
		elif a3_order_diff_match_input.find('C')!=-1 and depend==1: 
			a3_order_diff_match_input = a3_order_diff_match_input.replace('C','B',where)
			continue
		elif a3_order_diff_match_input.find('B')!=-1 and depend==0: 
			a3_order_diff_match_input = a3_order_diff_match_input.replace('B','A',where)
			continue			
		else:	 
			pos = random.randint(0,3)
			place = a3_order_diff_match_input.find('D')
			if place==-1:
				place = a3_order_diff_match_input.find('C')
			if place==-1:
				place = a3_order_diff_match_input.find('B')
			strli = list(a3_order_diff_match_input)
			strli[pos], strli[place] = strli[place], strli[pos]
			a3_order_diff_match_input = "".join(strli)
	################################################################################################### 
	###########	for table_2
	db_self.curs.execute("SELECT difficulty FROM Matches JOIN Full_Sessions_played_recap USING(match_id) WHERE kid_id = ? GROUP BY match_id",(kid_id,))
	res_a1 = db_self.curs.fetchall()
	diff_matches_done = [item for sublist in res_a1 for item in sublist]
	print(diff_matches_done)
	num_diff = [0,0,0,0,0,0,0,0]
	if 'Easy_1' in diff_matches_done:
		num_diff[0]=num_diff[0]+1
	if 'Easy_2' in diff_matches_done:
		num_diff[1]+=1
	if 'Normal_1' in diff_matches_done:
		num_diff[2]+=1
	if 'Normal_2' in diff_matches_done:
		num_diff[3]+=1
	if 'Medium_1' in diff_matches_done:
		num_diff[4]+=1
	if 'Medium_2' in diff_matches_done:
		num_diff[5]+=1
	if 'Hard_1' in diff_matches_done:
		num_diff[6]+=1
	if 'Hard_2' in diff_matches_done:
		num_diff[7]+=1

	if num_diff[0]==0:
		part1_a1 = 'A'
	elif 0<num_diff[0]<=3:
		part1_a1 = 'B'
	elif 3<num_diff[0]<=6:
		part1_a1 = 'C'
	else:
		part1_a1 = 'D'
	if num_diff[1]==0:
		part2_a1 = 'A'
	elif 0<num_diff[1]<=3:
		part2_a1 = 'B'
	elif 3<num_diff[1]<=6:
		part2_a1 = 'C'
	else:
		part3_a1 = 'D'
	if num_diff[2]==0:
		part3_a1 = 'A'
	elif 0<num_diff[2]<=3:
		part3_a1 = 'B'
	elif 3<num_diff[2]<=6:
		part3_a1 = 'C'
	else:
		part3_a1 = 'D'
	if num_diff[3]==0:
		part4_a1 = 'A'
	elif 0<num_diff[3]<=3:
		part4_a1 = 'B'
	elif 3<num_diff[3]<=6:
		part4_a1 = 'C'
	else:
		part4_a1 = 'D'
	if num_diff[4]==0:
		part5_a1 = 'A'
	elif 0<num_diff[4]<=3:
		part5_a1 = 'B'
	elif 3<num_diff[4]<=6:
		part5_a1 = 'C'
	else:
		part5_a1 = 'D'
	if num_diff[5]==0:
		part6_a1 = 'A'
	elif 0<num_diff[5]<=3:
		part6_a1 = 'B'
	elif 3<num_diff[5]<=6:
		part6_a1 = 'C'
	else:
		part6_a1 = 'D'
	if num_diff[6]==0:
		part7_a1 = 'A'
	elif 0<num_diff[6]<=3:
		part7_a1 = 'B'
	elif 3<num_diff[6]<=6:
		part7_a1 = 'C'
	else:
		part7_a1 = 'D'
	if num_diff[7]==0:
		part8_a1 = 'A'
	elif 0<num_diff[7]<=3:
		part8_a1 = 'B'
	elif 3<num_diff[7]<=6:
		part8_a1 = 'C'
	else:
		part8_a1 = 'D'

	a1_range_diff_matches_done_input = '{}{}{}{}{}{}{}{}'.format(part1_a1,part2_a1,part3_a1,part4_a1,part5_a1,part6_a1,part7_a1,part8_a1)

	verify_me = ['AAAAAAAA','BAAAAAAA','ABAAAAAA','BBAAAAAA','BBBBAAAA','BBBBBAAA','BBBBBBAA','BBBBBBBA','BBBBBBBB','CCBBBBBB','CCCBBBBB','CCCBBBBB','CCCCBBBB','CCCCCBBB','CCCCCBBB','CCCCCBBB','CCCCCCBB','CCCCCCCB','CCCCCCCC']
	while a1_range_diff_matches_done_input not in verify_me:
		where = random.randint(0,3)
		depend = random.randint(0,1)
		if a1_range_diff_matches_done_input.find('D')!=-1:
			a1_range_diff_matches_done_input = a1_range_diff_matches_done_input.replace('D','C')
			continue
		elif a1_range_diff_matches_done_input.find('C')!=-1 and depend==1: 
			a1_range_diff_matches_done_input = a1_range_diff_matches_done_input.replace('C','B',where)
			continue
		elif a1_range_diff_matches_done_input.find('B')!=-1 and depend==0: 
			a1_range_diff_matches_done_input = a1_range_diff_matches_done_input.replace('B','A',where)
			continue			
		else:	 
			pos = random.randint(0,3)
			place = a1_range_diff_matches_done_input.find('D')
			if place==-1:
				place = a1_range_diff_matches_done_input.find('C')
			if place==-1:
				place = a1_range_diff_matches_done_input.find('B')
			strli = list(a1_range_diff_matches_done_input)
			strli[pos], strli[place] = strli[place], strli[pos]
			a1_range_diff_matches_done_input = "".join(strli)

	print("a1_range_diff_matches_done_input ==> {}".format(a1_range_diff_matches_done_input))

	#db_self.curs.execute("SELECT m.order_quantity_questions FROM Matches m JOIN Full_Sessions_played_recap USING(match_id) WHERE kid_id = ? GROUP BY match_id",(kid_id,))
	db_self.curs.execute("SELECT order_quantity_questions FROM Full_Sessions_played_recap WHERE kid_id = ? GROUP BY match_id",(kid_id,))
	res_a2 = db_self.curs.fetchall()
	quan_ques_done = [item for sublist in res_a2 for item in sublist]
	print(quan_ques_done)
	num_quan_ques = [0,0,0,0]

	if 'same' in quan_ques_done:
		num_quan_ques[0]+=1
	if 'casu' in quan_ques_done:
		num_quan_ques[1]+=1
	if 'asc' in quan_ques_done:
		num_quan_ques[2]+=1
	if 'desc' in quan_ques_done:
		num_quan_ques[3]+=1

	if num_quan_ques[0]==0:
		first_part_a2 = 'A'
	elif 0<num_quan_ques[0]<=3:
		first_part_a2 = 'B'
	elif 3<num_quan_ques[0]<=6:
		first_part_a2 = 'C'
	elif num_quan_ques[0]>6:
		first_part_a2 = 'D'
	if num_quan_ques[1]==0:
		second_part_a2 = 'A'
	elif 0<num_quan_ques[1]<=3:
		second_part_a2 = 'B'
	elif 3<num_quan_ques[1]<=6:
		second_part_a2 = 'C'
	elif num_quan_ques[1]>6:
		second_part_a2 = 'D'
	if num_quan_ques[2]==0:
		third_part_a2 = 'A'
	elif 0<num_quan_ques[2]<=3:
		third_part_a2 = 'B'
	elif 3<num_quan_ques[2]<=6:
		third_part_a2 = 'C'
	elif num_quan_ques[2]>6:
		third_part_a2 = 'D'
	if num_quan_ques[3]==0:
		fourth_part_a2 = 'A'
	elif 0<num_quan_ques[3]<=3:
		fourth_part_a2 = 'B'
	elif 3<num_quan_ques[3]<=6:
		fourth_part_a2 = 'C'
	elif num_quan_ques[3]>6:
		fourth_part_a2 = 'D'

	a2_num_of_matches_with_games_quan_ques_input = '{}{}{}{}'.format(first_part_a2,second_part_a2,third_part_a2,fourth_part_a2)
	print("a2_num_of_matches_with_games_quan_ques_input ==> {}".format(a2_num_of_matches_with_games_quan_ques_input))
	verify_me2 = ['AAAA','BAAA','CAAA','ACAA','BBAA','BBBA','BBAB','BBBB','CBAA','CBAB','CBBA','CBBB','BCAA','BCAB','BCBA','BCBB','CCBB','CBCB','CBBC','CCBC','CCCB','CCCC']

	while a2_num_of_matches_with_games_quan_ques_input not in verify_me2:
		where = random.randint(0,3)
		depend = random.randint(0,1)
		if a2_num_of_matches_with_games_quan_ques_input.find('D')!=-1:
			a2_num_of_matches_with_games_quan_ques_input = a2_num_of_matches_with_games_quan_ques_input.replace('D','C')
			continue
		elif a2_num_of_matches_with_games_quan_ques_input.find('C')!=-1 and depend==1: 
			a2_num_of_matches_with_games_quan_ques_input = a2_num_of_matches_with_games_quan_ques_input.replace('C','B',where)
			continue
		elif a2_num_of_matches_with_games_quan_ques_input.find('B')!=-1 and depend==0: 
			a2_num_of_matches_with_games_quan_ques_input = a2_num_of_matches_with_games_quan_ques_input.replace('B','A',where)
			continue			
		else:	 
			pos = random.randint(0,3)
			place = a2_num_of_matches_with_games_quan_ques_input.find('D')
			if place==-1:
				place = a2_num_of_matches_with_games_quan_ques_input.find('C')
			if place==-1:
				place = a2_num_of_matches_with_games_quan_ques_input.find('B')
			strli = list(a2_num_of_matches_with_games_quan_ques_input)
			strli[pos], strli[place] = strli[place], strli[pos]
			a2_num_of_matches_with_games_quan_ques_input = "".join(strli)
	#db_self.curs.execute("SELECT order_quantity_games FROM Sessions JOIN Full_Sessions_played_recap USING(session_id) WHERE kid_id = ? GROUP BY session_id",(kid_id,))
	db_self.curs.execute("SELECT order_quantity_games FROM Full_Sessions_played_recap WHERE kid_id = ? GROUP BY session_id",(kid_id,))
	res_a3 = db_self.curs.fetchall()
	quan_games_done = [item for sublist in res_a3 for item in sublist]
	print(quan_games_done)
	num_quan_games = [0,0,0,0]

	if 'same' in quan_games_done:
		num_quan_games[0]+=1
	if 'casu' in quan_games_done:
		num_quan_games[1]+=1
	if 'asc' in quan_games_done:
		num_quan_games[2]+=1
	if 'desc' in quan_games_done:
		num_quan_games[3]+=1

	if num_quan_games[0]==0:
		first_part_a3 = 'A'
	elif 0<num_quan_games[0]<=3:
		first_part_a3 = 'B'
	elif 3<num_quan_games[0]<=6:
		first_part_a3 = 'C'
	elif num_quan_games[0]>6:
		first_part_a3 = 'D'
	if num_quan_games[1]==0:
		second_part_a3 = 'A'
	elif 0<num_quan_games[1]<=3:
		second_part_a3 = 'B'
	elif 3<num_quan_games[1]<=6:
		second_part_a3 = 'C'
	elif num_quan_games[1]>6:
		second_part_a3 = 'D'
	if num_quan_games[2]==0:
		third_part_a3 = 'A'
	elif 0<num_quan_games[2]<=3:
		third_part_a3 = 'B'
	elif 3<num_quan_games[2]<=6:
		third_part_a3 = 'C'
	elif num_quan_games[2]>6:
		third_part_a3 = 'D'
	if num_quan_games[3]==0:
		fourth_part_a3 = 'A'
	elif 0<num_quan_games[3]<=3:
		fourth_part_a3 = 'B'
	elif 3<num_quan_games[3]<=6:
		fourth_part_a3 = 'C'
	elif num_quan_games[3]>6:
		fourth_part_a3 = 'D'

	a3_num_of_session_with_matches_quan_games_input = '{}{}{}{}'.format(first_part_a3,second_part_a3,third_part_a3,fourth_part_a3)

	while a3_num_of_session_with_matches_quan_games_input not in verify_me2:
		where = random.randint(0,3)
		depend = random.randint(0,1)
		if a3_num_of_session_with_matches_quan_games_input.find('D')!=-1:
			a3_num_of_session_with_matches_quan_games_input = a3_num_of_session_with_matches_quan_games_input.replace('D','C')
			continue
		elif a3_num_of_session_with_matches_quan_games_input.find('C')!=-1 and depend==1: 
			a3_num_of_session_with_matches_quan_games_input = a3_num_of_session_with_matches_quan_games_input.replace('C','B',where)
			continue
		elif a3_num_of_session_with_matches_quan_games_input.find('B')!=-1 and depend==0: 
			a3_num_of_session_with_matches_quan_games_input = a3_num_of_session_with_matches_quan_games_input.replace('B','A',where)
			continue			
		else:	 
			pos = random.randint(0,3)
			place = a3_num_of_session_with_matches_quan_games_input.find('D')
			if place==-1:
				place = a3_num_of_session_with_matches_quan_games_input.find('C')
			if place==-1:
				place = a3_num_of_session_with_matches_quan_games_input.find('B')
			strli = list(a3_num_of_session_with_matches_quan_games_input)
			strli[pos], strli[place] = strli[place], strli[pos]
			a3_num_of_session_with_matches_quan_games_input = "".join(strli)
	print("a3_num_of_session_with_matches_quan_games_input ==> {}".format(a3_num_of_session_with_matches_quan_games_input))

	db_self.curs.execute("SELECT focus FROM Needs JOIN Kids_Needs USING(need_id) WHERE kid_id = ? ",(kid_id,))
	res_n1 = db_self.curs.fetchall()
	quan_games_done = [item for sublist in res_n1 for item in sublist]
	print(quan_games_done)

	n1_focus_on_concentration_input = 'no'
	n2_focus_on_resistance_input = 'no'
	n1_focus_on_errors_input = 'no'
	n2_focus_on_automation_input = 'no'
	n4_focus_on_changes_input = 'no'
	n1_focus_on_comprehension_input = 'no'
	
	res_needs = db_self.curs.fetchall()
	kids_needs = [item for sublist in res_needs for item in sublist]
	#all symptomps here, despite on whch table
	if 'focus_on_concentration' in kids_needs:
		n1_focus_on_concentration_input = 'yes'
	if 'focus_on_resistance' in kids_needs:
		n2_focus_on_resistance_input = 'yes'
	if 'focus_on_errors' in kids_needs:
		n1_focus_on_errors_input = 'yes'
	if 'focus_on_automation' in kids_needs:
		focus_on_automation_input = 'yes'
	if 'focus_on_changes' in kids_needs:
		n4_focus_on_changes_input = 'yes'
	if 'focus_on_comprehension' in kids_needs:
		n1_focus_on_comprehension_input = 'yes'

	#db_self.curs.execute("SELECT order_difficulty_games FROM Matches JOIN Full_Sessions_played_recap USING(match_id) WHERE kid_id = ? GROUP BY match_id",(kid_id,))
	db_self.curs.execute("SELECT order_difficulty_games FROM Full_Sessions_played_recap WHERE kid_id = ? GROUP BY match_id",(kid_id,))
	res_a1 = db_self.curs.fetchall()
	diff_games_done = [item for sublist in res_a1 for item in sublist]
	print(diff_games_done)
	num_diff_games = [0,0,0,0]

	if 'same' in diff_games_done:
		num_diff_games[0]+=1
	if 'casu' in diff_games_done:
		num_diff_games[1]+=1
	if 'asc' in diff_games_done:
		num_diff_games[2]+=1
	if 'desc' in diff_games_done:
		num_diff_games[3]+=1

	if num_diff_games[0]==0:
		first_part_a3 = 'A'
	elif 0<num_diff_games[0]<=3:
		first_part_a3 = 'B'
	elif 3<num_diff_games[0]<=6:
		first_part_a3 = 'C'
	elif num_diff_games[0]>6:
		first_part_a3 = 'D'
	if num_diff_games[1]==0:
		second_part_a3 = 'A'
	elif 0<num_diff_games[1]<=3:
		second_part_a3 = 'B'
	elif 3<num_diff_games[1]<=6:
		second_part_a3 = 'C'
	elif num_diff_games[1]>6:
		second_part_a3 = 'D'
	if num_diff_games[2]==0:
		third_part_a3 = 'A'
	elif 0<num_diff_games[2]<=3:
		third_part_a3 = 'B'
	elif 3<num_diff_games[2]<=6:
		third_part_a3 = 'C'
	elif num_diff_games[2]>6:
		third_part_a3 = 'D'
	if num_diff_games[3]==0:
		fourth_part_a3 = 'A'
	elif 0<num_diff_games[3]<=3:
		fourth_part_a3 = 'B'
	elif 3<num_diff_games[3]<=6:
		fourth_part_a3 = 'C'
	elif num_diff_games[3]>6:
		fourth_part_a3 = 'D'

	a1_range_num_of_matches_with_games_order_of_difficulty	= '{}{}{}{}'.format(first_part_a3,second_part_a3,third_part_a3,fourth_part_a3)
	print("a1_range_num_of_matches_with_games_order_of_difficulty ==> {}".format(a1_range_num_of_matches_with_games_order_of_difficulty))

	while a1_range_num_of_matches_with_games_order_of_difficulty not in verify_me2:
		where = random.randint(0,3)
		depend = random.randint(0,1)
		if a1_range_num_of_matches_with_games_order_of_difficulty.find('D')!=-1:
			a1_range_num_of_matches_with_games_order_of_difficulty = a1_range_num_of_matches_with_games_order_of_difficulty.replace('D','C')
			continue
		elif a1_range_num_of_matches_with_games_order_of_difficulty.find('C')!=-1 and depend==1: 
			a1_range_num_of_matches_with_games_order_of_difficulty = a1_range_num_of_matches_with_games_order_of_difficulty.replace('C','B',where)
			continue
		elif a1_range_num_of_matches_with_games_order_of_difficulty.find('B')!=-1 and depend==0: 
			a1_range_num_of_matches_with_games_order_of_difficulty = a1_range_num_of_matches_with_games_order_of_difficulty.replace('B','A',where)
			continue			
		else:	 
			pos = random.randint(0,3)
			place = a1_range_num_of_matches_with_games_order_of_difficulty.find('D')
			if place==-1:
				place = a1_range_num_of_matches_with_games_order_of_difficulty.find('C')
			if place==-1:
				place = a1_range_num_of_matches_with_games_order_of_difficulty.find('B')
			strli = list(a1_range_num_of_matches_with_games_order_of_difficulty)
			strli[pos], strli[place] = strli[place], strli[pos]
			a1_range_num_of_matches_with_games_order_of_difficulty = "".join(strli)
	print(diff_games_done)
	print(num_diff_games)
	######################
	db_self.curs.execute("SELECT MAX(m.num_of_games) FROM Matches m JOIN Full_Sessions_played_recap USING(match_id) WHERE kid_id = ?",(kid_id,))
	a2_max_game_match = db_self.curs.fetchone()[0]
	print(a2_max_game_match)

	db_self.curs.execute("SELECT MAX(g.num_questions) FROM Games g JOIN Full_Sessions_played_recap USING(game_id) WHERE kid_id = ?",(kid_id,))
	a1_max_question_game = db_self.curs.fetchone()[0]
	print(a1_max_question_game)
	tot_nec_features = [
		s1_understanding_input, #['no',
		s1_diversion_input, #'no',
		s2_endurance_input, #'no',
		s2_restrictions_of_interest_input, #'no',
		s3_meltdown_input, #'no',
		s3_memoria_deficit_input, #'no',
		s3_distraction_in_listening_input, #'no',
		s3_executing_long_tasks_input, #'no', 
		a1_max_question_game, #3, 
		a1_range_num_input, #'AAAA', 
		a1_range_diff_matches_done_input, #'AAAAAAAA',
		a1_range_num_of_matches_with_games_order_of_difficulty, #'AAAA',
		a2_max_game_match, #4,
		a2_match_session_input, #7,
		a2_num_of_matches_with_games_quan_ques_input, #'BAAA',
		a3_order_diff_match_input, #'BAAA',
		a3_num_of_session_with_matches_quan_games_input, #'BAAA',
		n1_focus_on_errors_input, #'no',
		n1_focus_on_concentration_input, #'no',
		n1_focus_on_comprehension_input, #'no',
		n2_focus_on_automation_input, #'no',
		n2_focus_on_resistance_input, #'no',
		n4_focus_on_changes_input, #'no',
		level_input] #'beginner']
	
	print("add_prediction_2 =>tot_nec_features  ", tot_nec_features)
	return tot_nec_features,kids_syms,kids_needs
