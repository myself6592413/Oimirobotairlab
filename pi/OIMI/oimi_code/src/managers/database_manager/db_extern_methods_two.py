"""info:
	Oimi robot db_external_methods_two, second file with extra methods used by different modules
	in the case of data extraction for prediction during kids playing session.
Notes:
	For other detailed info, look at dabatase_doc

Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import re
import copy
import itertools
from collections import defaultdict
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
def iterFlatten(a_list):
    if isinstance(a_list, (list, tuple)):
        for element in a_list:
            for e in iterFlatten(element):
                yield e
    else:
        yield a_list

def do_difference(lis1, lis2):
    return list(set(lis1) - set(lis2)) + list(set(lis2) - set(lis1))

def list_duplicates(seq):
    tally = defaultdict(list)
    for i,item in enumerate(seq):
        tally[item].append(i)
    return ((key,locs) for key,locs in tally.items())

def take_given_scenario(custom_par):
	print("enter in take_given_scenariotake_given_scenariotake_given_scenariotake_given_scenariotake_given_scenario")
	impo_or_need = []
	tip = custom_par.__class__.__name__
	if tip=='tuple':
		customized = [item for sublist in custom_par for item in sublist]
	else:
		customized = copy.copy(custom_par)
	print("custom scenario!!{}".format(customized))
	print("custom scenario!!{}".format(customized))
	print("custom scenario!!{}".format(customized))
	print("custom scenario!!{}".format(customized))
	exact_scen_id = 0
	for x in customized:
		if 'scenario' in x:
			exact_scen_id = int(re.search(r'\d+', x).group())
			impo_or_need.append(exact_scen_id)
	return impo_or_need

def impose_id_ques(custom_par):
	mand_q = []
	one_quest_id = 99999
	plus_one_game = 0
	customized = list(iterFlatten(custom_par))
	for x in customized:
		if 'ask_quest' in x:
			print("sono entreo in impose_id_ques")
			plus_one_game = 1
			one_quest_id = int(re.search(r'\d+', x).group())
			print("one_quest_id")
			print(one_quest_id)
			mand_q.append(one_quest_id)
			print("mand_q")
			print(mand_q)
	return mand_q, plus_one_game

def obtain_info_session(custom_par):
	print("SEGRETO SEGRETO SEGRETO SEGRETO SEGRETO {}".format(custom_par))
	level_area = ''
	is_mixed = 0
	dont_ask_scenario = 0
	num_of_matches = 0
	num_of_games_of_one_match = 0
	num_of_questions = 0
	kind_one_game = ''
	type_one_game = ''
	ask_never_asked_q = 0
	difficulty_mat = ''
	difficulty_gam = ''
	complexity = ''
	stress_flag = 0
	plus_one_game = 0
	#customized = []
	#if len(custom_par)>1:
	#	customized = [item for sublist in custom_par for item in sublist]
	#else: 
	customized = list(iterFlatten(custom_par))
	print("SEGRETO2 SEGRETO2 SEGRETO2 {}".format(customized))
	for cus in customized:
		if 'session_limit_area' in cus:
			level_area = 'se'			 
		elif 'match_limit_area' in cus:
			level_area = 'ma'			 
		elif 'game_limit_area' in cus:
			level_area = 'ga'			 
		elif 'mixed_match' in cus:
			is_mixed = 1
		elif 'mixed' in cus:
			is_mixed = 1
		
		if 'avoid asking scenario' in cus:
			dont_ask_scenario = 1
		if 'choose one_match' in cus:
			num_of_matches = 1
		elif 'choose two_matches' in cus:
			num_of_matches = 2
		elif 'choose three_matches' in cus:
			num_of_matches = 3
		elif 'choose four_matches' in cus:
			num_of_matches = 4
		elif 'choose one_game' in cus:
			num_of_games_of_one_match = 1
		elif 'choose two_games' in cus:
			num_of_games_of_one_match = 2
		elif 'choose three_games' in cus:
			num_of_games_of_one_match = 3
		elif 'choose four_games' in cus:
			num_of_games_of_one_match = 4
		elif 'choose one_question' in cus:
			num_of_questions = 1
		elif 'choose two_questions' in cus:
			num_of_questions = 2
		elif 'choose three_questions' in cus:
			num_of_questions = 3
		elif 'choose four_questions' in cus:
			num_of_questions = 4

		#case together before the next (share same prefix) 
		if 'type_game_1FSUS' in cus:
			type_one_game = '1FSUS'
		elif 'type_game_1FSUDT'in cus:
			type_one_game = '1FSUDT'
		elif 'type_game_1FSUD'in cus:
			type_one_game = '1FSUD'
		elif 'type_game_1FCOS' in cus:
			type_one_game = '1FCOS'
		elif 'type_game_1FCODT' in cus: 
			type_one_game = '1FCODT'
		elif 'type_game_1FCOD' in cus:
			type_one_game = '1FCOD'
		elif 'type_game_1FSUCOS' in cus:
			type_one_game = '1FSUCOS'
		elif 'type_game_1FSUCODT' in cus:
			type_one_game = '1FSUCODT'
		elif 'type_game_1FSUCOD' in cus:
			type_one_game = '1FSUCOD'
		elif 'type_game_1FGCA' in cus:
			type_one_game = '1FGCA'
		elif 'type_game_1FCACOA' in cus:
			type_one_game = '1FCACOA'
		elif 'type_game_1FCACO' in cus:
			type_one_game = '1FCACO'
		elif 'type_game_2LST' in cus:
			type_one_game = '2LST'
		elif 'type_game_2LSU' in cus:
			type_one_game = '2LSU'
		elif 'type_game_2LTWO' in cus:
			type_one_game = '2LTWO'
		elif 'type_game_2LDSUOC' in cus:
			type_one_game = '2LDSUOC'
		elif 'type_game_2LDBOC' in cus:
			type_one_game = '2LDBOC'
		elif 'type_game_2LDSUOB' in cus:
			type_one_game = '2LDSUOB'
		elif 'type_game_2LDBOB' in cus:
			type_one_game = '2LDBOB'
		elif 'type_game_2LDSU2' in cus:
			type_one_game = '2LDSU2'
		elif 'type_game_2LDSU' in cus:
			type_one_game = '2LDSU'
		elif 'type_game_3SSU' in cus:
			type_one_game = '3SSU'
		elif 'type_game_3SCO' in cus:
			type_one_game = '3SCO'
		elif 'type_game_4PSU' in cus:
			type_one_game = '4PSU'
		elif 'type_game_4PCO' in cus:
			type_one_game = '4PCO'
		elif 'type_game_5KS' in cus:
			type_one_game = '5KS'
		elif 'type_game_5KD' in cus:
			type_one_game = '5KD'
		elif 'type_game_6I' in cus:
			type_one_game = '6I'
		elif 'type_game_7OSU2' in cus:
			type_one_game = '7OSU2'
		elif 'type_game_7OSU3' in cus:
			type_one_game = '7OSU3'
		elif 'type_game_7OSU4' in cus:
			type_one_game = '7OSU4'
		elif 'type_game_7OCO2' in cus:
			type_one_game = '7OCO2'
		elif 'type_game_7OCO3' in cus:
			type_one_game = '7OCO3'
		elif 'type_game_7OCO4' in cus:
			type_one_game = '7OCO4'
		elif 'type_game_7OL2' in cus:
			type_one_game = '7OL2'
		elif 'type_game_7OL3' in cus:
			type_one_game = '7OL3'
		elif 'type_game_8QTSUSC1' in cus:
			type_one_game = '8QTSUSC1'
		elif 'type_game_8QTSUSC2' in cus:
			type_one_game = '8QTSUSC2'
		elif 'type_game_8QTCOSC1' in cus:
			type_one_game = '8QTCOSC1'
		elif 'type_game_8QTCOSC2' in cus:
			type_one_game = '8QTCOSC2'
		elif 'type_game_8QTKNSC' in cus:
			type_one_game = '8QTKNSC'
		elif 'type_game_8QTKNDC' in cus:
			type_one_game = '8QTKNDC'
		elif 'type_game_8QTPRSC' in cus:
			type_one_game = '8QTPRSC'
		elif 'type_game_8QTSUDC1' in cus:
			type_one_game = '8QTSUDC1'
		elif 'type_game_8QTSUDC2' in cus:
			type_one_game = '8QTSUDC2'
		elif 'type_game_8QTCODC1' in cus:
			type_one_game = '8QTCODC1'
		elif 'type_game_8QTCODC2' in cus:
			type_one_game = '8QTCODC2'
		elif 'type_game_8QTSUSB1' in cus:
			type_one_game = '8QTSUSB1'
		elif 'type_game_8QTSUSB2' in cus:
			type_one_game = '8QTSUSB2'
		elif 'type_game_8QTCOSB1' in cus:
			type_one_game = '8QTCOSB1'
		elif 'type_game_8QTCOSB2' in cus:
			type_one_game = '8QTCOSB2'
		elif 'type_game_8QTKNSB' in cus:
			type_one_game = '8QTKNSB'
		elif 'type_game_8QTPRSB' in cus:
			type_one_game = '8QTPRSB'
		elif 'type_game_8QTSUDB1' in cus:
			type_one_game = '8QTSUDB1'
		elif 'type_game_8QTSUDB2' in cus:
			type_one_game = '8QTSUDB2'
		elif 'type_game_8QTCODB1' in cus:
			type_one_game = '8QTCODB1'
		elif 'type_game_8QTCODB2' in cus:
			type_one_game = '8QTCODB2'
		elif 'type_game_8QTSUSCOC1' in cus:
			type_one_game = '8QTSUSCOC1'
		elif 'type_game_9CNSU' in cus:
			type_one_game = '9CNSU'
		elif 'type_game_9CNCO' in cus:
			type_one_game = '9CNCO'
		elif 'type_game_9CNSSU' in cus:
			type_one_game = '9CNSSU'
		elif 'type_game_9CNSPA' in cus:
			type_one_game = '9CNSPA'
		elif 'type_game_9CNSPX' in cus:
			type_one_game = '9CNSPX'
		
		if 'question_never_asked' in cus:
			ask_never_asked_q = 1

		if 'create Easy_1 match' in cus:
			difficulty_mat = 'Easy_1'
		elif 'create Easy_2 match' in cus:
			difficulty_mat = 'Easy_2'
		elif 'create Normal_1 match' in cus:
			difficulty_mat = 'Normal_1'
		elif 'create Normal_2 match' in cus:
			difficulty_mat = 'Normal_2'
		elif 'create Medium_1 match' in cus:
			difficulty_mat = 'Medium_1'
		elif 'create Medium_2 match' in cus:
			difficulty_mat = 'Medium_2'
		elif 'create Hard_1 match' in cus:
			difficulty_mat = 'Hard_1'
		elif 'create Hard_2 match' in cus:
			difficulty_mat = 'Hard_2'
	   
		if 'create Easy_1 game' in cus:
			difficulty_gam = 'Easy_1'
		elif 'create Easy_2 game' in cus:
			difficulty_gam = 'Easy_2'
		elif 'create Normal_1 game' in cus:
			difficulty_gam = 'Normal_1'
		elif 'create Normal_2 game' in cus:
			difficulty_gam = 'Normal_2'
		elif 'create Medium_1 game' in cus:
			difficulty_gam = 'Medium_1'
		elif 'create Medium_2 game' in cus:
			difficulty_gam = 'Medium_2'
		elif 'create Hard_1 game' in cus:
			difficulty_gam = 'Hard_1'
		elif 'create Hard_2 game' in cus:
			difficulty_gam = 'Hard_2'

		if 'complex_easy' in cus:			
			complexity = 'Easy'
		if 'complex_normal' in cus:
			complexity = 'Normal'
		if 'complex_medium' in cus:
			complexity = 'Medium'
		if 'complex_hard' in cus:
			complexity = 'Hard'
		if 'stress me' in cus:
			stress_flag = 1
	
		if 'kind_game_1F' in cus:
			kind_one_game = '1F'
		if 'kind_game_2L' in cus:
			kind_one_game = '2L'
		if 'kind_game_3S' in cus:
			kind_one_game = '3S'
		if 'kind_game_4P' in cus:
			kind_one_game = '4P'
		if 'kind_game_5K' in cus:
			kind_one_game = '5K'
		if 'kind_game_6I' in cus:
			kind_one_game = '6I'
		if 'kind_game_7O' in cus:
			kind_one_game = '7O'
		if 'kind_game_8Q' in cus:
			kind_one_game = '8Q'
		if 'kind_game_9C' in cus:
			kind_one_game = '9C'

	return stress_flag,complexity,level_area,is_mixed,dont_ask_scenario,num_of_matches,num_of_games_of_one_match,\
	num_of_questions,kind_one_game,type_one_game,ask_never_asked_q,difficulty_mat,difficulty_gam

def create_new_anyway(customized):
	if 'redo' in customized:
		print("REDO")
		print("REDO")
		return 1
	else:
		return 0

def find_kind(type_one_game):
	if '1F' in type_one_game:
		the_game_kind = '1F'
	elif '2L' in type_one_game:
		the_game_kind = '2L'
	elif '3S' in type_one_game:
		the_game_kind = '3S'
	elif '4P' in type_one_game:
		the_game_kind = '4P'
	elif '5K' in type_one_game:
		the_game_kind = '5K'
	elif '6I' in type_one_game:
		the_game_kind = '6I'
	elif '7O' in type_one_game:
		the_game_kind = '7O'
	elif '8Q' in type_one_game:
		the_game_kind = '8Q'
	elif '9C' in type_one_game:
		the_game_kind = '9C'
	print("THE GAME KIND INSIDE ACP2 IS!!! {}".format(the_game_kind))
	return the_game_kind

def find_criteria(list_kinds):
	ord_list = list_kinds.sort()
	if list_kinds==ord_list:
		criteria = 'regular'
	else:
		criteria = 'random' 