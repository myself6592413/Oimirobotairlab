"""Info:
Notes:
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
if '/home/pi/OIMI/oimi_code/src/colorprint_stdout' not in sys.path:
	#sys.path.append('/home/pi/OIMI/oimi_code/src/colorprint_stdout.py')
	sys.path.insert(1, os.path.join(sys.path[0], '../../'))
import re
import csv
import traceback
import linecache as lica
from subprocess import Popen, PIPE, STDOUT
from importlib import reload
from typing import Optional
import oimi_queries as oq
cimport modify_default_lists as mdl
import oimi_queries_playing as oqp
import db_default_lists as dbdl
import db_default_lists_old_tables as dbdlot
import db_default_audio_lists as dbdal
import db_default_lists_2 as dbdl2
import db_default_lists_3 as dbdl3
import db_default_lists_4 as dbdl4
import colorprint_stdout as col
cimport database_controller_build
cimport database_controller_save_to_files as dcs
cimport database_triggers as dbtr
# ================================================================================================================================================================================================================
# Methods
# ================================================================================================================================================================================================================
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Useful
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef build_OimiSettings_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS OimiSettings')
	db_self.curs.execute(oq.sql_create_OimiSettings)
	db_self.curs.execute("INSERT INTO OimiSettings (keyp,val) VALUES ('fireTrigger',1)")
	db_self.conn.commit()
cdef build_Allidtokens_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS All_id_tokens')
	db_self.curs.execute(oq.sql_create_allidtokens)
	for item in dbdl.all_id_tokens:
		db_self.curs.execute("INSERT INTO All_id_tokens VALUES (?)", (item, ))
	db_self.conn.commit()
	print("All_id_tokens table built")
	print("All_id_tokens added")
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Kids
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef build_Kids_table(db_self):
	cdef:
		int begin
		int end
		str creation_query
		int param = 10000
	reload(oq)
	reload(dbdl)
	try:
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
			lines = fp.readlines()
		for index, line in enumerate(lines):
			if line.startswith('sql_create_kids'):
				begin = index
			if line.startswith('sql_create_achievements'):
				end = index-1
		if not end:
			raise Exception 
		chunk = lines[begin:end]
		chunk.pop(0)
		chunk = map(lambda s: s.strip(), chunk) 
		joined_chunk = ' '.join(chunk)
		creation_query = re.sub('\'\'\'', '', joined_chunk) 
		db_self.curs.execute('DROP TABLE IF EXISTS Kids')
		db_self.curs.execute(creation_query)
		all_kids = dbdl.all_kids
		one_kid = all_kids.pop(0)
		first_name = one_kid[0]
		first_surname = one_kid[1]
		first_age = one_kid[2]
		first_level = one_kid[3]
		first_photo = one_kid[4]
		first_details = one_kid[5]
		first_token = one_kid[6]
		col.prWhite_back("Kids table built")
		db_self.curs.execute('''
			INSERT INTO Kids (name, surname, age, current_level, photo, details, token) VALUES (?,?,?,?,?,?,?)''', 
			(first_name, first_surname, first_age, first_level, first_photo, first_details,first_token))
		db_self.curs.execute('''
			UPDATE SQLITE_SEQUENCE SET seq = {} WHERE name =\'Kids\' '''.format(param))
		db_self.curs.execute('''
			UPDATE Kids SET kid_id = 10000 WHERE kid_id = 1''')		   
		db_self.curs.executemany('''
			INSERT INTO Kids (name, surname, age, current_level, photo, details, token) VALUES (?,?,?,?,?,?,?)''', all_kids)
		db_self.conn.commit()
		print("Kids added, starting from id 10000")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef build_Kids_Parameters_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Parameters')
	db_self.curs.execute(oq.sql_create_parameters_kids)
	print("Kids_Parameters table built")
	db_self.curs.executemany('''
		INSERT INTO Kids_Parameters (kid_id, audio_intro, audio_middle, audio_end, audio_stage_num, audio_rate, repeat_audio, read_instructions, 
			optional_cues_flag, additive_reward_flag, movements_allowed, touch_detection_enabled, fast_pattern_lights_authorized, 
			brightness_LEDs_body, brightness_LEDs_base, rorward_speed_value, rotational_speed_value, pid_controller_kd_constant) 
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', dbdl.all_kids_parameters)
	db_self.conn.commit()
	print("Kids_Parameters added")

cdef build_Achievements_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Achievements')
	db_self.curs.execute(oq.sql_create_achievements)
	print("Achievements table built")
	for item in dbdl.all_achievements:
		db_self.curs.execute("INSERT INTO Achievements VALUES (?)", (item, ))
	db_self.conn.commit()
	print("Achievements added")
cdef build_Symptoms_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Symptoms')
	db_self.curs.execute(oq.sql_create_symptoms)
	print("Symptoms table built")
	for item in dbdl.all_symptoms:
		db_self.curs.execute("INSERT INTO Symptoms VALUES (?)", (item, ))
	db_self.conn.commit()
	print("Symptoms added")
cdef build_Issues_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Issues')
	db_self.curs.execute(oq.sql_create_issues)
	print("Issues table built")
	for item in dbdl.all_issues:
		db_self.curs.execute("INSERT INTO Issues VALUES (?)", (item, ))
	db_self.conn.commit()
	print("Issues added")
cdef build_Comorbidities_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Comorbidities')
	db_self.curs.execute(oq.sql_create_comorbidities)
	print("Comorbidities table built")
	for item in dbdl.all_comorbidities:
		db_self.curs.execute("INSERT INTO Comorbidities VALUES (?)", (item, ))
	db_self.conn.commit()
	print("Comorbidities added")
cdef build_Strengths_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Strengths')
	db_self.curs.execute(oq.sql_create_strengths)
	print("Strengths table built")
	for item in dbdl.all_strengths:
		db_self.curs.execute("INSERT INTO Strengths VALUES (?)", (item, ))
	db_self.conn.commit()
	print("Strengths added")
cdef build_Impositions_table(db_self):
	reload(oqp)
	db_self.curs.execute('DROP TABLE IF EXISTS Impositions')
	db_self.curs.execute(oqp.sql_create_impositions)
	print("Impositions table built")
	for item in dbdl2.all_impositions:
		db_self.curs.execute("INSERT INTO Impositions VALUES (?)", (item, ))
	db_self.conn.commit()
	print("Impositions added")
cdef build_Needs_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Needs')
	db_self.curs.execute(oq.sql_create_needs)
	cdef int param = 10000
	all_needs = dbdl2.all_needs
	need_one = all_needs.pop(0)
	first_focus = need_one[0]
	first_match_scope = need_one[1]
	first_game_scope = need_one[2]
	first_question_scope = need_one[3]
	print("Needs table built")
	db_self.curs.execute('''
		INSERT INTO Needs(focus, match_scope, game_scope, question_scope) VALUES (?,?,?,?)''',(first_focus, first_match_scope, first_game_scope, first_question_scope))
	db_self.curs.execute('''
		UPDATE SQLITE_SEQUENCE SET seq = {} WHERE name =\'Needs\' '''.format(param))
	db_self.curs.execute('''
		UPDATE Needs SET need_id = 10000 WHERE need_id = 1''')	
	db_self.curs.executemany('''
		INSERT INTO Needs(focus, match_scope, game_scope, question_scope) VALUES (?,?,?,?)''', dbdl2.all_needs)
	db_self.conn.commit()
	print("Needs added")

cdef build_Targets_behaviours_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Target_behaviours')
	db_self.curs.execute(oq.sql_targets)
	print("Target_behaviours table built")
	for item in dbdl.all_targets_behaviours:
		db_self.curs.execute("INSERT INTO Target_behaviours VALUES (?)", (item, ))
	db_self.conn.commit()
	print("Target_behaviours added")

cdef build_survey_treatment_targets_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Targets')
	db_self.curs.execute(oq.sql_create_survey_targets)
	print("Treatments_Targets table built")
	db_self.curs.executemany('''INSERT INTO Treatments_Targets VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', dbdl.all_treatments_targets)
	db_self.conn.commit()
	print("Treatments_Targets added")
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Play
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef build_Patches_table(db_self): 
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Patches')
	db_self.curs.execute(oq.sql_create_patches)
	print("Patches table built")
cdef build_Scenarios_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Scenarios')
	db_self.curs.execute(oq.sql_create_scenarios)
	print("Scenarios table built")

cdef build_Kind_games_table(db_self):
	reload(oq)
	#db_self.curs.execute('DROP TABLE IF EXISTS Kind_games')
	db_self.curs.execute(oq.sql_create_kind_games)
	#db_self.start_kind_games_trigger()
	db_self.curs.executemany('''INSERT INTO Kind_games (kind_of_game, details_kind) VALUES (?,?)''', dbdl.all_kind_games)
	db_self.conn.commit()
	print("Kind_games table built")
	print("Kind_games added")
cdef build_Type_games_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Type_games')
	db_self.curs.execute(oq.sql_create_type_games)
	print("Type_games table built")
	db_self.curs.executemany('''INSERT INTO Type_games (type_of_game, details_type, possible_resp, kind_game, example, rules_instructions) VALUES (?,?,?,?,?,?)''', dbdl.all_type_games)
	db_self.conn.commit()
	print("Type_games added")

cdef build_Audios_table(db_self): 
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Audios')
	db_self.curs.execute(oq.sql_create_audios)
	print("Audios table built")
	db_self.curs.executemany('''INSERT INTO Audios (audio_id, audio_file) VALUES (?,?)''', dbdal.all_audios)
	db_self.conn.commit()
	print("Audios added")
cdef build_Questions_table(db_self):
	reload(oqp)
	db_self.curs.execute('DROP TABLE IF EXISTS Questions')
	db_self.curs.execute(oqp.sql_create_questions)
	print("Questions table built")

cdef build_Therapies_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Therapies')
	db_self.curs.execute(oqp.sql_create_therapies)
	print("Therapies table built")
	cdef int param = 70000
	all_therapies = dbdl.all_therapies
	thera_one = all_therapies.pop(0)
	first_num_of_treatments = thera_one[0]
	first_status = thera_one[1]
	first_creation_way = thera_one[2]
	first_kick_off_date  = thera_one[3]
	first_closing_date	= thera_one[4]
	first_duration_days = thera_one[5]
	first_canonical_intervention = thera_one[6]
	first_disposable = thera_one[7]
	first_token = thera_one[8]
	db_self.curs.execute('''
		INSERT INTO Therapies (num_of_treatments, status, creation_way, kick_off_date, closing_date, duration_days, canonical_intervention, disposable, token) VALUES 
		(?,?,?,?,?,?,?,?,?)''', (first_num_of_treatments, first_status, first_creation_way, first_kick_off_date, first_closing_date, first_duration_days, first_canonical_intervention, first_disposable, first_token))
	db_self.curs.execute('''
		UPDATE Therapies SET therapy_id = 70000 WHERE therapy_id = 1''')
	db_self.curs.executemany('''
		INSERT INTO Therapies (num_of_treatments, status, creation_way, kick_off_date, closing_date, duration_days, canonical_intervention, disposable, token) 
		VALUES (?,?,?,?,?,?,?,?,?)''', all_therapies)
	db_self.conn.commit()
	print("All Therapies added")

cdef build_Treatments_table(db_self):
	#reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments')
	db_self.curs.execute(oqp.sql_create_treatments)
	print("Treatments table built")
	cdef int param = 80000
	all_treatments = dbdl.all_treatments
	treatment_one = all_treatments.pop(0)
	first_target_goal_1 = treatment_one[0]
	first_target_goal_2 = treatment_one[1]
	first_target_goal_3 = treatment_one[2]
	first_num_of_pastimes = treatment_one[3]
	first_num_of_sessions = treatment_one[4]
	first_num_of_entertainments = treatment_one[5]
	first_status = treatment_one[6]
	first_creation_way = treatment_one[7]
	first_day = treatment_one[8]
	first_disposable = treatment_one[9]
	first_token = treatment_one[10]
	db_self.curs.execute('''
		INSERT INTO Treatments (target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, num_of_sessions, num_of_entertainments, status, creation_way, day, disposable, token) VALUES (?,?,?,?,?,?,?,?,?,?,?)''', 
			(first_target_goal_1, first_target_goal_2, first_target_goal_3, first_num_of_pastimes, first_num_of_sessions, first_num_of_entertainments, first_status, first_creation_way, first_day, first_disposable, first_token))
	db_self.curs.execute('''
		UPDATE Treatments SET treatment_id = 80000 WHERE treatment_id = 1''')
	db_self.curs.executemany('''
		INSERT INTO Treatments (target_goal_1,target_goal_2,target_goal_3, num_of_pastimes, num_of_sessions, num_of_entertainments, status, creation_way, day, disposable, token) VALUES (?,?,?,?,?,?,?,?,?,?,?)''', all_treatments)
	db_self.conn.commit()
		
	query_audio_intro = "UPDATE Treatments SET audio_treatment_intro = '{}'".format('audio_1')
	query_audio_exit = "UPDATE Treatments SET audio_treatment_exit = '{}'".format('audio_2')
	db_self.curs.execute(query_audio_intro)
	db_self.curs.execute(query_audio_exit)
	print("Treatments added")

cdef build_Class_free_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Class_free')
	db_self.curs.execute(oqp.sql_class_free_games)
	print("Class_free table built")
	all_class_free = dbdl.all_class_free
	db_self.curs.executemany('''INSERT INTO Class_free (activity, category, free_rules) VALUES (?,?,?)''', all_class_free)
	db_self.conn.commit()
	print("Class_free added")

cdef build_bridge_EntertainmentSubstage(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Entertainments_Substages')
	db_self.curs.execute(oq.sql_create_bridge_entertainment_substages)
	db_self.curs.executemany('''INSERT INTO Entertainments_Substages (entertainment_id, substage_id) VALUES (?,?)''', dbdl.all_entertainments_substages)
	db_self.conn.commit()
	print("Entertainments_Substages created")


cdef build_Entertainments_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Entertainments')
	db_self.curs.execute(oqp.sql_create_entertainments)
	print("Entertainments table built")
	

cdef build_Games_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Games')
	db_self.curs.execute(oqp.sql_create_games)
	cdef int param = 40000
	all_games = dbdl.all_games
	game_one = all_games.pop(0)
	first_num_questions = game_one[0]
	first_kind = game_one[1]
	first_type = game_one[2]
	first_difficulty = game_one[3]
	first_ord_diff_quest = game_one[4]
	first_time = game_one[5]
	first_disposable = game_one[6]
	first_token = game_one[7]
	db_self.curs.execute('''
		INSERT INTO Games (num_questions, kind, type, difficulty, order_difficulty_questions, necessary_time, disposable, token) VALUES (?,?,?,?,?,?,?,?)''', (first_num_questions, 
			first_kind, first_type, first_difficulty, first_ord_diff_quest, first_time, first_disposable, first_token))
	db_self.conn.commit()
	db_self.curs.execute('''
		UPDATE SQLITE_SEQUENCE SET seq = {} WHERE name =\'Games\' '''.format(param))
	db_self.conn.commit()
	db_self.curs.execute('''
		UPDATE Games SET game_id = 40000 WHERE game_id = 1''')
	db_self.curs.executemany('''
		INSERT INTO Games (num_questions, kind, type, difficulty, order_difficulty_questions, necessary_time, disposable, token) VALUES (?,?,?,?,?,?,?,?)''', all_games)
	db_self.conn.commit()

	query_audio_intro = "UPDATE Games SET audio_intro = '{}'".format('audio_1')
	query_audio_exit = "UPDATE Games SET audio_exit = '{}'".format('audio_2')
	query_audio_rule = "UPDATE Games SET audio_rules = '{}'".format('audio_rules')
	db_self.curs.execute(query_audio_intro)
	db_self.curs.execute(query_audio_exit)
	db_self.conn.commit()
	print("Games table built")
	print("Games added")

cdef build_ActivityRules_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Games_Rules')
	db_self.curs.execute('DROP TABLE IF EXISTS Activity_Rules')
	db_self.curs.execute(oq.sql_create_bridge_activity_rules)
	print("Activity_Rules created")
	db_self.curs.executemany('''INSERT INTO Activity_Rules (activity_id, specific_rule) VALUES (?,?)''', dbdl.all_activities_specific_rules)
	db_self.conn.commit()
	print("Activity_Rules populated")

cdef build_Matches_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Matches')
	db_self.curs.execute(oqp.sql_create_matches)
	cdef int param = 30000
	all_matches = dbdl.all_matches
	match_one = all_matches.pop(0)
	first_num_of_games = match_one[0]
	first_difficulty = match_one[1]
	first_variety = match_one[2]
	first_ord_diff_games = match_one[3]
	first_ord_quantity_questions = match_one[4]
	first_criteria_arrangement = match_one[5]
	first_scenario = match_one[6]
	first_time = match_one[7]
	first_disposable = match_one[8]
	first_token = match_one[9]
	db_self.curs.execute('''
		INSERT INTO Matches (num_of_games, difficulty, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, scenario, advisable_time, disposable, token) 
		VALUES (?,?,?,?,?,?,?,?,?,?)''', (first_num_of_games, first_difficulty, first_variety, first_ord_diff_games, first_ord_quantity_questions, first_criteria_arrangement,	
			first_scenario, first_time, first_disposable, first_token))
	db_self.conn.commit()
	db_self.curs.execute("UPDATE SQLITE_SEQUENCE SET seq = {} WHERE name =\'Matches\' ".format(param))
	db_self.conn.commit()
	query_audio_intro = "UPDATE Matches SET audio_intro = '{}'".format('audio_1')
	query_audio_exit = "UPDATE Matches SET audio_exit = '{}'".format('audio_2')
	db_self.curs.execute(query_audio_intro)
	db_self.curs.execute(query_audio_exit)
	db_self.conn.commit()
	db_self.curs.execute('''
		UPDATE Matches SET match_id = 30000 WHERE match_id = 1''')		  
	db_self.curs.executemany('''
		INSERT INTO Matches (num_of_games, difficulty, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, scenario, advisable_time, disposable, token) VALUES (?,?,?,?,?,?,?,?,?,?)''', all_matches)
	db_self.conn.commit()
	print("Matches table built")
	print("Matches added") 

cdef build_Sessions_table(db_self):
	reload(oqp)
	db_self.curs.execute('DROP TABLE IF EXISTS Sessions')
	db_self.curs.execute(oqp.sql_create_sessions)
	cdef int param = 20000
	all_sessions = dbdl.all_sessions
	session_one = all_sessions.pop(0)
	f_num_of_matches = session_one[0]
	f_complexity = session_one[1]
	f_status = session_one[2]
	f_mandatory_impositions = session_one[3]
	f_mandatory_needs = session_one[4]
	f_ord_diff_matches = session_one[5]
	f_ord_quant_games = session_one[6]
	f_extra_time_tolerance = session_one[7]
	f_movements_allowed = session_one[8]
	f_body_enabled = session_one[9]
	f_rules_explication = session_one[10]
	f_stress_flag = session_one[11]
	f_exhortations = session_one[12]
	f_repeat_question = session_one[13]
	f_repeat_intro = session_one[14]
	f_desirable_time = session_one[15]
	f_creation_way = session_one[16]
	f_disposable = session_one[17]
	f_token = session_one[18]

	db_self.curs.execute('''
		INSERT INTO Sessions (num_of_matches, complexity, status, mandatory_impositions, mandatory_needs, order_difficulty_matches, order_quantity_games, extra_time_tolerance, movements_allowed, body_enabled, 
			rules_explication, stress_flag, frequency_exhortations, repeat_question, repeat_intro, desirable_time, creation_way, disposable, token)
		 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ''', (f_num_of_matches, f_complexity, f_status, f_mandatory_impositions, f_mandatory_needs, f_ord_diff_matches, 
			f_ord_quant_games, f_extra_time_tolerance, f_movements_allowed, f_body_enabled, f_rules_explication, f_stress_flag, f_exhortations, 
			f_repeat_question, f_repeat_intro, f_desirable_time, f_creation_way, f_disposable, f_token))
	db_self.conn.commit()
	db_self.curs.execute('''
		UPDATE SQLITE_SEQUENCE SET seq = {} WHERE name =\'Sessions\' '''.format(param))
	db_self.conn.commit()
	db_self.curs.execute('''
		UPDATE Sessions SET session_id = 20000 WHERE session_id = 1''')
	db_self.curs.executemany('''
		INSERT INTO  Sessions (num_of_matches, complexity, status, mandatory_impositions, mandatory_needs, order_difficulty_matches, order_quantity_games, extra_time_tolerance, movements_allowed, body_enabled, 
			rules_explication, stress_flag, frequency_exhortations, repeat_question, repeat_intro, desirable_time, creation_way, disposable, token)
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', all_sessions) #not directly dbdl !! but the local version with the 1 popped out
	

	query_audio_intro = "UPDATE Sessions SET audio_intro = '{}'".format('audio_12330')
	query_audio_exit = "UPDATE Sessions SET audio_exit = '{}'".format('audio_12333')
	db_self.curs.execute(query_audio_intro)
	db_self.curs.execute(query_audio_exit)
	db_self.conn.commit()
	print("Sessions table built")
	print("Sessions added")
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Bridges
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef build_bridge_TherapyTreatment(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Therapies_Treatments')
	db_self.curs.execute(oq.sql_bridge_thtr)
	db_self.curs.executemany('''INSERT INTO Therapies_Treatments (therapy_id, treatment_id) VALUES (?,?)''', dbdl.all_therapies_treatments)
	db_self.conn.commit()
	print("Therapies_Treatments created")

cdef build_bridge_TreatmentSession(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Sessions')
	db_self.curs.execute(oq.sql_bridge_trse)
	db_self.curs.executemany('''INSERT INTO Treatments_Sessions (treatment_id, session_id) VALUES (?,?)''', dbdl.all_treatments_sessions)
	db_self.conn.commit()
	print("Treatments_Sessions created")

cdef build_bridge_TreatmentEntertainment(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Entertainments')
	db_self.curs.execute(oq.sql_bridge_tren)
	db_self.curs.executemany('''INSERT INTO Treatments_Entertainments (treatment_id, entertainment_id) VALUES (?,?)''', dbdl.all_treatments_entertainments)
	db_self.conn.commit()
	print("Treatments_Entertainments created")

cdef build_bridge_TreatmentPastime(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Pastimes')
	db_self.curs.execute("CREATE TABLE Treatments_Pastimes AS \
		SELECT treatment_id, entertainment_id as pastime_id FROM Treatments_Entertainments")
	db_self.conn.commit()		
	db_self.curs.execute('''INSERT INTO Treatments_Pastimes (treatment_id, pastime_id)
			SELECT treatment_id, session_id as pastime_id
			FROM Treatments_Sessions 
			ORDER BY treatment_id
		''')
	db_self.conn.commit()
	db_self.curs.execute('DROP TABLE IF EXISTS ordered_pastimes')
	db_self.curs.execute("CREATE TABLE ordered_pastimes (treatment_id INTEGER, pastime_id INTEGER, PRIMARY KEY (treatment_id, pastime_id))")
	db_self.curs.execute("INSERT INTO ordered_pastimes (treatment_id, pastime_id) SELECT treatment_id, pastime_id FROM Treatments_Pastimes \
	ORDER BY treatment_id")
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Pastimes')
	db_self.curs.execute('ALTER TABLE ordered_pastimes RENAME TO Treatments_Pastimes')
	db_self.conn.commit()
	print("Treatments_Pastimes created")

cdef build_bridge_SessionMatch(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Sessions_Matches')
	db_self.curs.execute(oq.sql_bridge_sema)
	print("Sessions_Matches table created")
	db_self.curs.executemany('''INSERT INTO Sessions_Matches (session_id, match_id) VALUES (?,?)''', dbdl.all_sessions_matches)
	db_self.conn.commit()
	print("Sessions_Matches table populated")
cdef build_bridge_MatchGame(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Matches_Games')
	db_self.curs.execute(oq.sql_bridge_maga)
	print("Matches_Games table created")
	db_self.curs.executemany('''INSERT INTO Matches_Games (match_id, game_id) VALUES (?,?)''', dbdl.all_matches_games)
	db_self.conn.commit()
	print("Matches_Games table populated")
cdef build_bridge_GameQuestion(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Games_Questions')
	db_self.curs.execute(oq.sql_bridge_gaqu)
	print("Games_Questions table created")
	db_self.curs.executemany('''INSERT INTO Games_Questions (game_id, question_id) VALUES (?,?)''', dbdl.all_games_questions)
	db_self.conn.commit()
	print("Games_Questions table populated")
cdef build_bridge_QuestionPatch(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Questions_Patches')
	db_self.curs.execute(oq.sql_bridge_qupa)
	print("Questions_Patches created")
cdef build_bridge_ScenarioQuestion(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Scenarios_Questions')
	db_self.curs.execute(oq.sql_bridge_scqu)
	print("Scenarios_Questions table created")

cdef build_bridge_ScenarioType(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Scenarios_Types')
	db_self.curs.execute(oq.sql_bridge_scty)
	print("Scenarios_Types table created")
cdef build_bridge_ScenarioKind(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Scenarios_Kinds')
	db_self.curs.execute(oq.sql_bridge_scki)
	print("Scenarios_Kinds table created")

cdef build_bridge_Scenario4PSUQuestion(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Scenarios_4psu_Questions')
	db_self.curs.execute(oq.sql_bridge_scqu4psu)
	db_self.curs.executemany('''INSERT INTO Scenarios_4psu_Questions (scenario_id, audio_id) VALUES (?,?)''', dbdl3.all_scenarios_questions_4psu)
	db_self.conn.commit()
	print("Scenarios_4psu_Questions created")
cdef build_bridge_Scenario4PCOQuestion(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Scenarios_4pco_Questions')
	db_self.curs.execute(oq.sql_bridge_scqu4pco)
	db_self.curs.executemany('''INSERT INTO Scenarios_4pco_Questions (scenario_id, audio_id) VALUES (?,?)''', dbdl4.all_scenarios_questions_4pco)
	db_self.conn.commit()
	print("Scenarios_4pco_Questions created")

cdef build_bridge_KidTherapy(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Therapies')
	db_self.curs.execute(oq.sql_bridge_kith)
	db_self.curs.executemany('''INSERT INTO Kids_Therapies (kid_id, therapy_id) VALUES (?,?)''', dbdl.all_kids_therapies)
	db_self.conn.commit()
	#dcs.store_kids_therapies_so_far(db_self)
	print("Kids_Therapies created")

cdef build_bridge_KidTreatment(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Treatments')
	db_self.curs.execute(oq.sql_bridge_kitr)
	#non serve vero?
	#db_self.curs.executemany('''INSERT INTO Kids_Treatments (kid_id, treatment_id) VALUES (?,?)''', dbdl.all_kids_treatments)
	db_self.conn.commit()
	#dcs.store_kids_treatments_so_far(db_self)
	print("Kids_Treatments created")

cdef build_bridge_KidEntertainment(db_self):
	''' dont insert columns with values that obtain in realtime clearly like the scores'''
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Entertainments')
	db_self.curs.execute(oq.sql_bridge_kien)
	print("Kids_Entertainments table created")
	
	db_self.curs.execute('''
	SELECT kid_id, entertainment_id 
	FROM Kids_Treatments JOIN Treatments_Entertainments USING (treatment_id)''')
	findings = db_self.curs.fetchall()
	db_self.curs.execute("SELECT COUNT(rowid) FROM Kids_Entertainments")
	numb_pos = db_self.curs.fetchone()[0]
	pp = 1
	if findings:
		for row in findings:
			kid_is = row[0]
			enter_is = row[1]
			mdl.modify_default_list_kids_entertainments(kid_is, enter_is, numb_pos+pp)
			pp += 1
	db_self.curs.execute('''INSERT INTO Kids_Entertainments (kid_id, entertainment_id)
		SELECT kid_id, entertainment_id 
		FROM Kids_Treatments JOIN Treatments_Entertainments USING (treatment_id)''')
	db_self.conn.commit()
	#dcs.store_kids_entertainments_so_far(db_self)
	print("Kids_Entertainments table populated")

	
cdef build_bridge_KidSession(db_self):
	''' dont insert columns with values that obtain in realtime clearly'''
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Sessions')
	db_self.curs.execute(oq.sql_bridge_kise)
	print("Kids_Sessions table created")
	db_self.curs.executemany('''INSERT INTO Kids_Sessions (kid_id, session_id, LED_body_brightness, LED_base_brightness, 
								volume_speakers, forward_speed, rotational_speed, num_interruptions_session) 
								VALUES (?,?,?,?,?,?,?,?)''', dbdl.all_kids_sessions)
	db_self.conn.commit()
	dcs.store_kids_sessions_so_far(db_self)
	print("Kids_Sessions table populated")

	build_bridge_KidMatch(db_self)
	build_bridge_KidGame(db_self)
	build_bridge_KidQuestion(db_self)

cdef build_bridge_KidMatch(db_self):
	try:
		reload(oq)
		db_self.curs.execute("SELECT count(name) FROM sqlite_master WHERE type='table' AND name='Kids_Matches'")
		is_present = db_self.curs.fetchone()[0]
		if is_present:
			dcs.store_kids_matches_so_far(db_self) #overwrite kid_matches.csv
			print("PRESENT!!!!")
			db_self.curs.execute("DROP TABLE IF EXISTS Kids_Matches_tmp")
			db_self.curs.execute("CREATE TABLE Kids_Matches_tmp AS SELECT * FROM Kids_Matches ORDER BY kid_id ASC")
			db_self.curs.execute('''SELECT kid_id, session_id, match_id FROM Kids_Matches''')
			res_mk = db_self.curs.fetchall()
			db_self.conn.commit()
		
		db_self.curs.execute('DROP TABLE IF EXISTS Kids_Matches')
		db_self.curs.execute(oq.sql_create_bridge_kima)
		db_self.conn.commit()
		db_self.curs.execute(oq.sql_add_num_answers_to_match)
		db_self.curs.execute(oq.sql_add_corrects_to_match)
		db_self.curs.execute(oq.sql_add_errors_to_match)
		db_self.curs.execute(oq.sql_add_indecisions_to_match)
		db_self.curs.execute(oq.sql_add_already_to_match)
		db_self.curs.execute(oq.sql_add_time_to_match)
		db_self.curs.execute(oq.sql_add_result_quantity_feedback_to_match)
		db_self.curs.execute(oq.sql_add_result_grade_feedback_to_match)
		db_self.curs.execute(oq.sql_add_result_value_feedback_to_match)
		db_self.conn.commit()
		if is_present:
			for k,s,m in res_mk:
				db_self.curs.execute("""SELECT num_answers_match, num_corrects_match, num_errors_match, num_indecisions_match, num_already_match, time_of_match, quantity_feedback_match, grade_feedback_match, value_feedback_match
					FROM Kids_Matches_tmp WHERE kid_id = ? AND session_id = ? AND match_id = ?""",(k,s,m,))
				resma = db_self.curs.fetchall()
				tmp_matches =[item for sublist in resma for item in sublist]
				print("tmp_matchestmp_matchestmp_matchestmp_matchestmp_matches ", tmp_matches)
				nam = tmp_matches[0]
				ncm = tmp_matches[1]
				nem = tmp_matches[2]
				nim = tmp_matches[3]
				nam = tmp_matches[4]
				tom = tmp_matches[5]
				qfm = tmp_matches[6]
				gfm = tmp_matches[7]
				vfm = tmp_matches[8]
				db_self.execute_param_query('''UPDATE Kids_Matches SET num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, 
					time_of_match = ?, quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ? WHERE kid_id IN (
					SELECT kid_id FROM Kids_Matches_tmp WHERE kid_id = ?) AND session_id IN (SELECT session_id FROM Kids_Matches_tmp WHERE session_id = ?) AND match_id IN (SELECT match_id FROM Kids_Matches_tmp WHERE match_id = ?)''',(nam, ncm, nem, nim, nam, tom, qfm, gfm, vfm,k,s,m))


		db_self.curs.execute(oq.sql_adjust_types_kima)
		db_self.curs.execute("INSERT INTO Kids_Matches_tmp_1 SELECT * FROM Kids_Matches")
		db_self.curs.execute("DROP TABLE Kids_Matches")
		db_self.curs.execute("ALTER TABLE Kids_Matches_tmp_1 RENAME TO Kids_Matches")
		db_self.conn.commit()	
		db_self.curs.execute('DROP TABLE IF EXISTS Kids_Matches_tmp_1')
		db_self.curs.execute('DROP TABLE IF EXISTS Kids_Matches_tmp')
		db_self.curs.execute("DELETE FROM Kids_Matches WHERE kid_id isNull")
		db_self.conn.commit()
		print("Kids_Matches created or updated")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef build_bridge_KidGame(db_self):
	try:
		reload(oq)
		db_self.curs.execute("SELECT count(name) FROM sqlite_master WHERE type='table' AND name='Kids_Games'")
		is_present = db_self.curs.fetchone()[0]
		print("is_present kid game => ", is_present)
		if is_present:
			dcs.store_kids_games_so_far(db_self) #overwrite kid_games.csv
			db_self.curs.execute("DROP TABLE IF EXISTS Kids_Games_tmp")
			db_self.curs.execute("CREATE TABLE Kids_Games_tmp AS SELECT * FROM Kids_Games ORDER BY kid_id ASC")
			db_self.curs.execute('''SELECT kid_id, session_id, game_id FROM Kids_Games''')
			res_gk = db_self.curs.fetchall()
			print("res_gk ",res_gk)
			db_self.conn.commit()
		db_self.curs.execute('DROP TABLE IF EXISTS Kids_Games')
		db_self.curs.execute(oq.sql_create_bridge_kiga)
		db_self.conn.commit()
		db_self.curs.execute(oq.sql_add_num_answers_to_game)
		db_self.curs.execute(oq.sql_add_corrects_to_game)
		db_self.curs.execute(oq.sql_add_errors_to_game)
		db_self.curs.execute(oq.sql_add_indecisions_to_game)
		db_self.curs.execute(oq.sql_add_already_to_game)
		db_self.curs.execute(oq.sql_add_time_to_game)
		db_self.curs.execute(oq.sql_add_interruptions_to_game)
		db_self.curs.execute(oq.sql_add_result_quantity_feedback_to_game)
		db_self.curs.execute(oq.sql_add_result_grade_feedback_to_game)
		db_self.curs.execute(oq.sql_add_result_value_feedback_to_game)
		db_self.conn.commit()
		if is_present:
			print("sono nell IFFF di kid_game!!!")
			for k,s,g in res_gk:
				db_self.curs.execute("""SELECT num_answers_game, num_corrects_game, num_errors_game, num_indecisions_game, num_already_game, time_of_game, quantity_feedback_game, grade_feedback_game, value_feedback_game
					FROM Kids_Games_tmp WHERE kid_id = ? AND session_id = ? AND game_id = ?""",(k,s,g,))
				resga = db_self.curs.fetchall()
				tmp_games =[item for sublist in resga for item in sublist]
				print("tmp_gamestmp_gamestmp_gamestmp_gamestmp_gamestmp_games ", tmp_games)
				nag = tmp_games[0]
				ncg = tmp_games[1]
				neg = tmp_games[2]
				nig = tmp_games[3]
				nag = tmp_games[4]
				tog = tmp_games[5]
				qfg = tmp_games[6]
				gfg = tmp_games[7]
				vfg = tmp_games[8]
				db_self.execute_param_query('''UPDATE Kids_Games SET num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, 
					time_of_game = ?, quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ? WHERE kid_id IN (
					SELECT kid_id FROM Kids_Games_tmp WHERE kid_id = ?) AND session_id IN (SELECT session_id FROM Kids_Games_tmp WHERE session_id = ?) AND game_id IN (SELECT game_id FROM Kids_Games_tmp WHERE game_id = ?)''',(nag, ncg, neg, nig, nag, tog, qfg, gfg, vfg,k,s,g))

		
		db_self.curs.execute('DROP TABLE IF EXISTS Kids_Games_tmp_1')
		db_self.curs.execute(oq.sql_adjust_types_kiga)
		db_self.curs.execute("INSERT INTO Kids_Games_tmp_1 SELECT * FROM Kids_Games")
		db_self.curs.execute("DROP TABLE Kids_Games")
		db_self.curs.execute("ALTER TABLE Kids_Games_tmp_1 RENAME TO Kids_Games")
		db_self.conn.commit()		

		db_self.curs.execute('DROP TABLE IF EXISTS Kids_Games_tmp_1')
		db_self.curs.execute('DROP TABLE IF EXISTS Kids_Games_tmp')
		db_self.curs.execute("DELETE FROM Kids_Games WHERE kid_id isNull")
		db_self.conn.commit()
		print("Kids_Games created or updated")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef build_bridge_KidQuestion(db_self):
	try:
		reload(oq)
		db_self.curs.execute("SELECT count(name) FROM sqlite_master WHERE type='table' AND name='Kids_Questions'")
		is_present = db_self.curs.fetchone()[0]
		print("is_present kid questions!!! => ", is_present)
		if is_present:
			dcs.store_kids_questions_so_far(db_self) #overwrite kid_question.csv
			db_self.curs.execute("DROP TABLE IF EXISTS Kids_Questions_tmp")
			db_self.curs.execute("CREATE TABLE Kids_Questions_tmp AS SELECT * FROM Kids_Questions ORDER BY kid_id ASC")
			db_self.curs.execute("SELECT kid_id, session_id, question_id FROM Kids_Questions")
			res_qk = db_self.curs.fetchall()
			db_self.conn.commit()
		
		db_self.curs.execute('DROP TABLE IF EXISTS Kids_Questions')
		db_self.curs.execute(oq.sql_create_bridge_kiqu)
		db_self.conn.commit()
		db_self.curs.execute(oq.sql_add_num_answers_to_ques)
		db_self.curs.execute(oq.sql_add_corrects_to_ques)
		db_self.curs.execute(oq.sql_add_errors_to_ques)
		db_self.curs.execute(oq.sql_add_indecisions_to_ques)
		db_self.curs.execute(oq.sql_add_already_to_ques)
		db_self.curs.execute(oq.sql_add_time_to_ques)
		db_self.curs.execute(oq.sql_add_result_quantity_feedback_to_ques)
		db_self.curs.execute(oq.sql_add_result_grade_feedback_to_ques)
		db_self.curs.execute(oq.sql_add_result_value_feedback_to_ques)
		db_self.conn.commit()
		
		if is_present:
			for k,s,q in res_qk:
				db_self.curs.execute("""SELECT num_answers_question, num_corrects_question, num_errors_question, num_indecisions_question, num_already_question, time_of_question, quantity_feedback_question, grade_feedback_question, value_feedback_question
					FROM Kids_Questions_tmp WHERE kid_id = ? AND session_id = ? AND question_id = ?""",(k,s,q,))
				resqu = db_self.curs.fetchall()
				tmp_questions =[item for sublist in resqu for item in sublist]
				naq = tmp_questions[0]
				ncq = tmp_questions[1]
				neq = tmp_questions[2]
				niq = tmp_questions[3]
				naq = tmp_questions[4]
				toq = tmp_questions[5]
				qfq = tmp_questions[6]
				gfq = tmp_questions[7]
				vfq = tmp_questions[8]
				db_self.execute_param_query('''UPDATE Kids_Questions SET num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, 
					time_of_question = ?, quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ? WHERE kid_id IN (
					SELECT kid_id FROM Kids_Questions_tmp WHERE kid_id = ?) AND session_id IN (SELECT session_id FROM Kids_Questions_tmp WHERE session_id = ?) AND question_id IN (SELECT question_id FROM Kids_Questions_tmp WHERE question_id = ?)''',(naq, ncq, neq, niq, naq, toq, qfq, gfq, vfq,k,s,q))

		#db_self.conn.commit()
		db_self.curs.execute('DROP TABLE IF EXISTS Kids_Questions_tmp')
		db_self.conn.commit()	
		print("Kids_Questions created and/or updated")
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef build_bridge_KidAchievement(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Achievements')
	db_self.curs.execute(oq.sql_bridge_kiac)
	db_self.curs.execute("SELECT seq FROM sqlite_sequence WHERE name = 'Kids'")
	n_kids = db_self.curs.fetchone()[0] + 1
	db_self.curs.execute("SELECT COUNT(achievement_name) FROM Achievements")
	n_achi = db_self.curs.fetchone()[0]
	sql_insertion_achi = "INSERT INTO Kids_Achievements (kid_id) SELECT kid_id FROM Kids where kid_id=?"
	sql_insertion_achi_2 = ''' UPDATE Kids_Achievements 
		SET achievement_name = (SELECT a.achievement_name
			FROM Achievements a
			WHERE rowid=?)
		WHERE rowid = ?'''
	
	for n in range(10000,n_kids):
		db_self.curs.execute("SELECT kid_id FROM Kids WHERE rowid=?",(n,))
		onek = db_self.curs.fetchone()[0]
		for i in range(n_achi):
			db_self.curs.execute(sql_insertion_achi,(onek,))
		db_self.conn.commit()

	n_new_limit = 0
	#metto stop!
	for n in range(10000,n_kids):
		for p in range(n_achi+1):
			if n==10000:
				k = p
			else:
				k = n_new_limit + p
			db_self.curs.execute(sql_insertion_achi_2,(p,k,))
		db_self.conn.commit()
		n_new_limit += 76
	db_self.curs.execute("DELETE FROM Kids_Achievements WHERE achievement_name isNull")
	db_self.conn.commit()
	print("Kids_Achievements created")
cdef build_bridge_KidSymptom(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Symptoms')
	db_self.curs.execute(oq.sql_bridge_kisy)
	db_self.curs.executemany('''INSERT INTO Kids_Symptoms (kid_id, symptom_name) VALUES (?,?)''', dbdl.all_kids_symptoms)
	db_self.conn.commit()
	print("Kids_Symptoms created")
cdef build_bridge_KidIssue(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Issues')
	db_self.curs.execute(oq.sql_bridge_kiis)
	db_self.curs.executemany('''INSERT INTO Kids_Issues (kid_id, issue_name) VALUES (?,?)''', dbdl.all_kids_issues)
	db_self.conn.commit()
	print("Kids_Issues created")
cdef build_bridge_KidComorbidity(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Comorbidities')
	db_self.curs.execute(oq.sql_bridge_kico)
	db_self.curs.executemany('''INSERT INTO Kids_Comorbidities (kid_id, comorbidity_name) VALUES (?,?)''', dbdl.all_kids_comorbidities)
	db_self.conn.commit()
	print("Kids_Comorbidities created")
cdef build_bridge_KidStrength(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Strengths')
	db_self.curs.execute(oq.sql_bridge_kist)
	db_self.curs.executemany('''INSERT INTO Kids_Strengths (kid_id, strength_name) VALUES (?,?)''', dbdl.all_kids_strengths)
	db_self.conn.commit()
	print("Kids_Strengths created")

cdef build_bridge_OldKidSymptom(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Old_Kids_Symptoms')
	db_self.curs.execute(oq.sql_bridge_oldkisy)
	print("Old_Kids_Symptoms table created")
	if dbdlot.old_kids_symptoms:
		db_self.curs.executemany('''INSERT INTO Old_Kids_Symptoms (kid_id, old_symptom_name) VALUES (?,?)''', dbdlot.old_kids_symptoms)
		db_self.conn.commit()
		print("Old_Kids_Symptoms table populated")

cdef build_bridge_OldKidIssue(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Old_Kids_Issues')
	db_self.curs.execute(oq.sql_bridge_oldkiis)
	if dbdlot.old_kids_issues:
		db_self.curs.executemany('''INSERT INTO Old_Kids_Issues (kid_id, old_issue_name) VALUES (?,?)''', dbdlot.old_kids_issues)
		db_self.conn.commit()
	print("Old_Kids_Issues table created")
	print("Old_Kids_Issues table populated")

cdef build_bridge_OldKidComorbidity(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Old_Kids_Comorbidities')
	db_self.curs.execute(oq.sql_bridge_oldkico)
	print("Old_Kids_Comorbidities table created")
	if dbdlot.old_kids_comorbidities:
		db_self.curs.executemany('''INSERT INTO Old_Kids_Comorbidities (kid_id, old_comorbidity_name) VALUES (?,?)''', dbdlot.old_kids_comorbidities)
		db_self.conn.commit()
		print("Old_Kids_Comorbidities table populated")

cdef build_bridge_OldKidStrength(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Old_Kids_Strengths')
	db_self.curs.execute(oq.sql_bridge_oldkist)
	print("Old_Kids_Strengths table created")	
	if dbdlot.old_kids_strengths:
		db_self.curs.executemany('''INSERT INTO Old_Kids_Strengths (kid_id, old_strength_name) VALUES (?,?)''', dbdlot.old_kids_strengths)
		db_self.conn.commit()
		print("Old_Kids_Strengths table populated")	

cdef build_bridge_OldKidNeed(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Old_Kids_Needs')
	db_self.curs.execute(oq.sql_bridge_oldkine)
	print("Old_Kids_Needs table created")
	if dbdlot.old_kids_needs:
		db_self.curs.executemany('''INSERT INTO Old_Kids_Needs (kid_id, old_need_id) VALUES (?,?)''', dbdlot.old_kids_needs)
		db_self.conn.commit()
		print("Old_Kids_Needs table populated")

cdef build_bridge_KidNeed(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Needs')
	db_self.curs.execute(oq.sql_bridge_kine)
	db_self.curs.executemany('''INSERT INTO Kids_Needs (kid_id, need_id) VALUES (?,?)''', dbdl.all_kids_needs)
	db_self.conn.commit()
	print("Kids_Needs created")
cdef build_bridge_KidImposition(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Kids_Impositions')
	db_self.curs.execute(oq.sql_bridge_kiim)
	db_self.curs.executemany('''INSERT INTO Kids_Impositions (kid_id, imposition_name) VALUES (?,?)''', dbdl.all_kids_impositions)
	db_self.conn.commit()
	print("Kids_Impositions created")

cdef build_bridge_NeedQuestion(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Needs_Questions')
	db_self.curs.execute(oq.sql_bridge_nequ)
	db_self.curs.executemany('''INSERT INTO Needs_Questions (need_id, question_id) VALUES (?,?)''', dbdl2.all_needs_questions)
	db_self.conn.commit()
	print("Needs_Questions created")
cdef build_bridge_ImpositionQuestion(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Impositions_Questions')
	db_self.curs.execute(oq.sql_bridge_imqu)
	db_self.curs.executemany('''INSERT INTO Impositions_Questions (imposition_name, question_id) VALUES (?,?)''', dbdl2.all_impositions_questions)
	db_self.conn.commit()
	print("Impositions_Questions created")

cdef build_bridge_SessionEnforcement(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Sessions_Enforcements')
	db_self.curs.execute(oq.sql_bridge_seen)
	db_self.curs.executemany('''INSERT INTO Sessions_Enforcements (session_id, enforcement_name) VALUES (?,?)''', dbdl.all_sessions_enforcements)
	db_self.conn.commit()
	print("Sessions_Enforcements created")

cdef build_bridge_SessionImposition(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Sessions_Impositions')
	db_self.curs.execute(oq.sql_bridge_seim)
	db_self.curs.executemany('''INSERT INTO Sessions_Impositions (session_id, imposition_name) VALUES (?,?)''', dbdl.all_sessions_impositions)
	db_self.conn.commit()
	print("Sessions_Impositions created")

cdef build_bridge_SessionNeed(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Sessions_Needs')
	db_self.curs.execute(oq.sql_bridge_sene)
	print("Sessions_Needs created")
	db_self.curs.executemany('''INSERT INTO Sessions_Needs (session_id, need_id) VALUES (?,?)''', dbdl.all_sessions_needs)
	db_self.conn.commit()
	print("Sessions_Needs table populated")

cdef build_bridge_SessionNote(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Sessions_Notes')
	db_self.curs.execute(oq.sql_bridge_seno)
	print("Sessions_Notes created")
	db_self.curs.executemany('''INSERT INTO Sessions_Notes (session_id, notes) VALUES (?,?)''', dbdl.all_sessions_notes)
	db_self.conn.commit()
	print("Sessions_Notes table populated")	

cdef build_bridge_ClassFreeTarget(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Class_free_Targets')
	db_self.curs.execute(oq.sql_class_bridge_class_free_targets)
	print("Class_free_Targets created")
	db_self.curs.executemany('''INSERT INTO Class_free_Targets (class, target_name) VALUES (?,?)''', dbdl.all_class_free_targets)
	db_self.conn.commit()
	print("Class_free_Targets table populated")

######################################################################################################################################################
cdef build_template_Stages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Stages')
	db_self.curs.execute(oq.sql_create_stage_template)
	print("Stages created")
	db_self.curs.executemany('''INSERT INTO Stages (stage_code, sort, style, type_of_stage, substage_id_1, substage_id_2, substage_id_3) VALUES (?,?,?,?,?,?,?)''', dbdl.all_stages_template)
	db_self.conn.commit()
	print("Stages populated")

cdef build_prompt_Stages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Prompt_Stages')
	db_self.curs.execute(oq.sql_create_prompt_stage)
	print("Prompt Stages created")
	db_self.curs.executemany('''INSERT INTO Prompt_Stages (stage_code, sort, style, type_of_stage, substage_id_1, substage_id_2, substage_id_3) VALUES (?,?,?,?,?,?,?)''', dbdl.all_prompt_stages)
	db_self.conn.commit()
	print("Prompt Stages populated")

cdef build_reward_Stages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Reward_Stages')
	db_self.curs.execute(oq.sql_create_reward_stage)
	print("Reward_Stages created")
	db_self.curs.executemany('''INSERT INTO Reward_Stages (stage_code, sort, style, type_of_stage, substage_id_1, substage_id_2, substage_id_3) VALUES (?,?,?,?,?,?,?)''', dbdl.all_reward_stages)
	db_self.conn.commit()
	print("Reward_Stages populated")

cdef build_mood_Stages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Mood_Stages')
	db_self.curs.execute(oq.sql_create_mood_stage)
	print("Mood_Stages table created")
	db_self.curs.executemany('''INSERT INTO Mood_Stages (stage_code, sort, style, type_of_stage, substage_id_1, substage_id_2, substage_id_3) VALUES (?,?,?,?,?,?,?)''', dbdl.all_mood_stages)
	db_self.conn.commit()
	print("Mood_Stages table populated")

cdef build_reaction_Stages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Reaction_Stages')
	db_self.curs.execute(oq.sql_create_reaction_stage)
	print("Reaction_Stages tahble created")
	db_self.curs.executemany('''INSERT INTO Reaction_Stages (stage_code, sort, style, type_of_stage, substage_id_1) VALUES (?,?,?,?,?)''', dbdl.all_reaction_stages)
	db_self.conn.commit()
	print("Reaction_Stages tahble populated")
######################################################################################################################################################
cdef build_Substages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS SubStages')
	db_self.curs.execute(oq.sql_create_substage_template)
	db_self.curs.executemany('''INSERT INTO SubStages 
		(substage_id, sort, sub_name, stage_reference, stage_flow,
		stage_output_lights_body_1, stage_output_lights_body_2, stage_output_lights_base_1, stage_output_lights_base_2,
		stage_output_speeche_1, stage_output_speeche_2, stage_output_speeche_3, stage_output_speeche_4,
		stage_output_movement_1, stage_output_movement_2, stage_output_movement_3) 
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''', dbdl.all_substages_template)
	db_self.conn.commit()
	print("SubStages created and added")

cdef build_Prompt_Substages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Prompt_Substages')
	db_self.curs.execute(oq.sql_create_prompt_substage)
	db_self.curs.executemany('''INSERT INTO Prompt_Substages 
		(substage_id, sort, sub_name, stage_reference, stage_flow,
		stage_output_lights_body_1, stage_output_lights_body_2, stage_output_lights_base_1, stage_output_lights_base_2,
		stage_output_speeche_1, stage_output_speeche_2, stage_output_speeche_3, stage_output_speeche_4,
		stage_output_movement_1, stage_output_movement_2, stage_output_movement_3) 
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', dbdl.all_prompt_substages)
	db_self.conn.commit()
	print("Prompt_Substages created and added")

cdef build_Reward_Substages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Reward_Substages')
	db_self.curs.execute(oq.sql_create_reward_substage)
	db_self.curs.executemany('''INSERT INTO Reward_Substages 
		(substage_id, sort, sub_name, stage_reference, stage_flow,
		stage_output_lights_body_1, stage_output_lights_body_2, stage_output_lights_base_1, stage_output_lights_base_2,
		stage_output_speeche_1, stage_output_speeche_2, stage_output_speeche_3, stage_output_speeche_4,
		stage_output_movement_1, stage_output_movement_2, stage_output_movement_3) 
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', dbdl.all_reward_substages)
	db_self.conn.commit()
	print("Reward_Substages created and added")

cdef build_Mood_Substages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Mood_Substages')
	db_self.curs.execute(oq.sql_create_mood_substage)
	db_self.curs.executemany('''INSERT INTO Mood_Substages 
		(substage_id, sort, sub_name, stage_reference, stage_flow,
		stage_output_lights_body_1, stage_output_lights_body_2, stage_output_lights_base_1, stage_output_lights_base_2,
		stage_output_speeche_1, stage_output_speeche_2, stage_output_speeche_3, stage_output_speeche_4,
		stage_output_movement_1, stage_output_movement_2, stage_output_movement_3) 
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', dbdl.all_mood_substages)
	db_self.conn.commit()
	print("Mood_Substages created and added")

cdef build_Reaction_Substages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Reaction_Substages')
	db_self.curs.execute(oq.sql_create_reaction_substage)
	db_self.curs.executemany('''INSERT INTO Reaction_Substages 
		(substage_id, sort, sub_name, stage_reference, stage_flow,
		stage_output_lights_body_1, stage_output_lights_body_2, stage_output_lights_base_1, stage_output_lights_base_2,
		stage_output_speeche_1, stage_output_speeche_2, stage_output_speeche_3, stage_output_speeche_4,
		stage_output_movement_1, stage_output_movement_2, stage_output_movement_3) 
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', dbdl.all_reaction_substages)
	db_self.conn.commit()
	print("Reaction_Substages created and added")

cdef build_ChildrenSubstages_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Children_SubStages')
	db_self.curs.execute(oq.sql_create_child_substage) 
	db_self.curs.executemany('''INSERT INTO Children_SubStages (child_id, permission_flag_Pr_st_1, permission_flag_Pr_st_2, permission_flag_Pr_st_3, 
		permission_flag_Pr_st_4, permission_flag_Rw_st_1, permission_flag_Rw_st_2, permission_flag_Rw_st_3, permission_flag_Rw_st_4, permission_flag_Rw_st_5, 
		permission_flag_Mo_st_1, permission_flag_Mo_st_2, permission_flag_Mo_st_3, permission_flag_Mo_st_4, permission_flag_Mo_st_5, permission_flag_Mo_st_6,
		do_Rep_st_1, do_Rep_st_2, do_Rep_st_3, do_Rep_st_4, do_Rep_st_5, do_Rep_st_6, do_Ren_st_1, do_Ren_st_2, do_Ren_st_3, do_Ren_st_4, do_Ren_st_5, do_Ren_st_6) 
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', dbdl.all_children_stages)
	db_self.conn.commit()
	print("Children_SubStages created")

# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Create
# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
cdef create_Short_recap_all_kids_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Short_recap_all_kids')
	db_self.curs.execute(oq.sql_short_table_recap)
	print("Short_recap_all_kids created and filled")

cdef create_Full_sessions_played_recap_table(db_self):
	reload(oq)
	db_self.curs.execute("SELECT count(name) FROM sqlite_master WHERE type='table' AND name='Full_sessions_played_recap'")
	is_present = db_self.curs.fetchone()[0]
	if is_present:
		n_query = "SELECT Sessions.session_id FROM Sessions ORDER BY session_id DESC LIMIT 1" 
		sess_id = db_self.execute_a_query(n_query)
		last_sess_id = sess_id[0]
		k_query = "SELECT kid_id FROM Kids_Sessions ORDER BY session_id DESC LIMIT 1" 
		kid_id = db_self.execute_a_query(k_query)
		last_kid = kid_id[0]
		par = (last_kid, last_sess_id)
		db_self.curs.execute("DROP TABLE IF EXISTS recap_tmp")
		db_self.curs.execute("CREATE TABLE recap_tmp AS SELECT * FROM Full_sessions_played_recap ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE IF EXISTS Full_sessions_played_recap")
		db_self.curs.execute("CREATE TABLE Full_sessions_played_recap AS SELECT * FROM recap_tmp")
		db_self.curs.execute("DROP TABLE IF EXISTS recap_tmp")
		db_self.execute_param_query(oq.sql_insertion_in_total_sessions, par)
		print("sto per creare summary")
		db_self.curs.execute("DROP TABLE IF EXISTS sess_summary_tmp")
		db_self.curs.execute("CREATE TABLE sess_summary_tmp AS SELECT * FROM Full_sessions_played_summary ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE IF EXISTS Full_sessions_played_summary")
		db_self.curs.execute("CREATE TABLE Full_sessions_played_summary AS SELECT * FROM sess_summary_tmp")
		db_self.curs.execute("DROP TABLE IF EXISTS sess_summary_tmp")
		db_self.execute_param_query(oq.sql_insertion_sessions_summary, par)
		db_self.conn.commit()
		print("Full_sessions_played_recap updated")
		print("Full_sessions_played_summary updated")

	elif not is_present:
		db_self.curs.execute('DROP TABLE IF EXISTS Full_sessions_played_recap')
		db_self.curs.execute('DROP TABLE IF EXISTS Full_sessions_played_summary')
		db_self.curs.execute('DROP TABLE IF EXISTS Short_recap_all_kids')
		db_self.conn.commit()
		db_self.curs.execute(oq.sql_total_sessions_recap)
		#db_self.conn.commit()
		print("Full_played_sessions_recap created and filled")
		db_self.curs.execute(oq.sql_create_total_sessions_summary)
		db_self.conn.commit()
		print("Full_played_sessions_summary created and filled")

cdef create_Full_played_recap_giga_table(db_self):
	reload(oq)
	db_self.curs.execute("SELECT count(name) FROM sqlite_master WHERE type='table' AND name='Full_played_recap_with_sessions'")
	is_present = db_self.curs.fetchone()[0]
	if is_present:
		n_query = "SELECT Therapies.therapy_id FROM Therapies ORDER BY therapy_id DESC LIMIT 1" 
		th_id = db_self.execute_a_query(n_query)
		last_therapy_id = th_id[0]
		k_query = "SELECT kid_id FROM Kids_Therapies ORDER BY therapy_id DESC LIMIT 1" 
		kid_id = db_self.execute_a_query(k_query)
		last_kid = kid_id[0]
		par = (last_kid, last_therapy_id)
		db_self.curs.execute("DROP TABLE IF EXISTS recap_tmp")
		db_self.curs.execute("CREATE TABLE recap_tmp AS SELECT * FROM Full_played_recap_with_sessions ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE IF EXISTS Full_played_recap_with_sessions")
		db_self.curs.execute("CREATE TABLE Full_played_recap_with_sessions AS SELECT * FROM recap_tmp")
		db_self.curs.execute("DROP TABLE IF EXISTS recap_tmp")
		db_self.execute_param_query(oq.sql_insertion_in_whole_table_results, par)
		print("sto per creare summary")
		db_self.curs.execute("DROP TABLE IF EXISTS ther_summary_tmp")
		db_self.curs.execute("CREATE TABLE ther_summary_tmp AS SELECT * FROM Full_therapies_played_summary ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE IF EXISTS Full_therapies_played_summary")
		db_self.curs.execute("CREATE TABLE Full_therapies_played_summary AS SELECT * FROM ther_summary_tmp")
		db_self.curs.execute("DROP TABLE IF EXISTS ther_summary_tmp")
		db_self.execute_param_query(oq.sql_insertion_total_summary, par)
		db_self.conn.commit()
		print("Full_played_recap_with_sessions updated")
		print("Full_therapies_played_summary updated")

	elif not is_present:
		db_self.curs.execute('DROP TABLE IF EXISTS Full_played_recap_with_sessions')
		db_self.curs.execute('DROP TABLE IF EXISTS Full_therapies_played_summary')
		db_self.curs.execute('DROP TABLE IF EXISTS Short_recap_all_kids')
		db_self.conn.commit()
		db_self.curs.execute(oq.sql_whole_table_results)
		#db_self.conn.commit()
		print("Full_played_recap_with_sessions created and filled")
		db_self.curs.execute(oq.sql_create_full_summary)
		db_self.conn.commit()
		print("Full_played_summary created and filled")

cdef fix_contraints_Full_sesions_summary_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Full_sessions_played_summary_new')
	db_self.curs.execute(oq.sql_create_sessions_summary_again)
	db_self.curs.execute("INSERT INTO Full_sessions_played_summary_new SELECT * FROM Full_sessions_played_summary")
	db_self.curs.execute("DROP TABLE Full_sessions_played_summary")
	db_self.curs.execute("ALTER TABLE Full_sessions_played_summary_new RENAME TO Full_sessions_played_summary")
	db_self.conn.commit()
	print("Full_sessions_played_summary table built / updated")

cdef fix_contraints_Full_therapies_summary_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Full_therapies_played_summary_new')
	db_self.curs.execute(oq.sql_create_therapies_summary_again)		
	db_self.curs.execute("INSERT INTO Full_therapies_played_summary_new SELECT * FROM Full_therapies_played_summary")
	db_self.curs.execute("DROP TABLE Full_therapies_played_summary")
	db_self.curs.execute("ALTER TABLE Full_therapies_played_summary_new RENAME TO Full_therapies_played_summary")
	db_self.conn.commit()
	print("Full_therapies_played_summary table built / updated")

cdef create_Full_played_recap_with_entertainment(db_self):
	reload(oq)
	db_self.curs.execute("SELECT count(name) FROM sqlite_master WHERE type='table' AND name='Full_played_recap_with_entertainment'")
	is_present = db_self.curs.fetchone()[0]
	if is_present:
		n_query = "SELECT Therapies.therapy_id FROM Therapies ORDER BY therapy_id DESC LIMIT 1" 
		th_id = db_self.execute_a_query(n_query)
		last_therapy_id = th_id[0]
		k_query = "SELECT kid_id FROM Kids_Therapies ORDER BY therapy_id DESC LIMIT 1" 
		kid_id = db_self.execute_a_query(k_query)
		last_kid = kid_id[0]
		par = (last_kid, last_therapy_id)
		db_self.curs.execute("DROP TABLE IF EXISTS en_recap_tmp")
		db_self.curs.execute("CREATE TABLE en_recap_tmp AS SELECT * FROM Full_played_recap_with_entertainment ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE IF EXISTS Full_played_recap_with_entertainment")
		db_self.curs.execute("CREATE TABLE Full_played_recap_with_entertainment AS SELECT * FROM en_recap_tmp")
		db_self.curs.execute("DROP TABLE IF EXISTS en_recap_tmp")
		db_self.execute_param_query(oq.sql_insertion_in_total_entertainments, par)
		db_self.curs.execute("DROP TABLE IF EXISTS en_summary_tmp")
		db_self.curs.execute("CREATE TABLE en_summary_tmp AS SELECT * FROM Full_entertainment_played_summary ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE IF EXISTS Full_entertainment_played_summary")
		db_self.curs.execute("CREATE TABLE Full_entertainment_played_summary AS SELECT * FROM en_summary_tmp")
		db_self.curs.execute("DROP TABLE IF EXISTS en_summary_tmp")
		db_self.execute_param_query(oq.sql_insertion_entertainments_recap, par)
		db_self.conn.commit()
		print("Full_played_recap_with_entertainment updated")
		print("Full_entertainment_played_summary updated")

	elif not is_present:
		db_self.curs.execute('DROP TABLE IF EXISTS Full_played_recap_with_entertainment')
		db_self.curs.execute('DROP TABLE IF EXISTS Full_entertainment_played_summary')
		db_self.conn.commit()
		db_self.curs.execute(oq.sql_whole_table_results_part_2)
		#db_self.conn.commit()
		print("Full_played_recap_with_entertainment created and filled")
		db_self.curs.execute(oq.sql_create_full_entertainment_summary)
		db_self.conn.commit()
		print("Full_entertainment_played_summary created and filled")

cdef fix_contraints_Full_entertainment_played_summary_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Full_entertainment_played_summary_new')
	db_self.curs.execute(oq.sql_create_entertainments_summary_again)		
	db_self.curs.execute("INSERT INTO Full_entertainment_played_summary_new SELECT * FROM Full_entertainment_played_summary")
	db_self.curs.execute("DROP TABLE Full_entertainment_played_summary")
	db_self.curs.execute("ALTER TABLE Full_entertainment_played_summary_new RENAME TO Full_entertainment_played_summary")
	db_self.conn.commit()
	print("Full_entertainment_played_summary table built / updated")

cdef create_definitive_recap_table_one(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Definitive_recap_tmp_1')
	db_self.curs.execute(oq.sql_create_definitive_one)	
	print("Definitive_recap_tmp_1 table created")
	db_self.curs.executescript(""" 
		ALTER TABLE Definitive_recap_tmp_1 ADD session_id TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD s_status TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_of_matches INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD complexity TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD order_difficulty_matches TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD order_quantity_games TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD mandatory_impositions INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD mandatory_needs INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD extra_time_tolerance INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD movements_allowed INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD body_enabled INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD rules_explication INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD stress_flag INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD frequency_exhortations INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD repeat_question INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD repeat_intro INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD desirable_time INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD s_creation_way TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD s_disposable INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD s_audio_intro TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD s_audio_exit TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD s_notes TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD s_token TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD s_therapist_grade TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD quantity_feedback_session TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD grade_feedback_session TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD value_feedback_session REAL;
		ALTER TABLE Definitive_recap_tmp_1 ADD time_of_session REAL;
		ALTER TABLE Definitive_recap_tmp_1 ADD LED_base_brightness TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD LED_body_brightness TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD volume_speakers TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD forward_speed TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD rotational_speed TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_interruptions_session TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD goal_fulfilled TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD occurred_event_kid TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD real_time_changes TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD match_id INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_of_games INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD m_difficulty TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD variety TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD order_difficulty_games TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD order_quantity_questions TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD criteria_arrangement TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD advisable_time INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD scenario INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD m_disposable INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD m_audio_intro TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD m_audio_exit TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD m_token TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_answers_match INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_corrects_match INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_errors_match INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_indecisions_match INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_already_match INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD time_of_match REAL;
		ALTER TABLE Definitive_recap_tmp_1 ADD quantity_feedback_match TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD grade_feedback_match TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD value_feedback_match REAL;
		ALTER TABLE Definitive_recap_tmp_1 ADD game_id INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_questions INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD kind TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD type TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD g_difficulty TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD order_difficulty_questions TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD necessary_time INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD g_disposable INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD g_audio_intro TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD g_audio_exit TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD g_token TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_answers_game INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_corrects_game INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_errors_game INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_indecisions_game INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_already_game INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD time_of_game REAL;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_interruptions_game INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD quantity_feedback_game INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD grade_feedback_game INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD value_feedback_game REAL;
		ALTER TABLE Definitive_recap_tmp_1 ADD question_id INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_answers_question INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_corrects_question INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_errors_question INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_indecisions_question INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD num_already_question INTEGER;
		ALTER TABLE Definitive_recap_tmp_1 ADD time_of_question REAL;
		ALTER TABLE Definitive_recap_tmp_1 ADD quantity_feedback_question TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD grade_feedback_question TEXT;
		ALTER TABLE Definitive_recap_tmp_1 ADD value_feedback_question REAL; 
	""")

	print("Definitive_recap_tmp_1 table added")
	db_self.conn.commit()

cdef create_definitive_recap_table_two(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Definitive_recap_tmp_2')
	db_self.curs.execute(oq.sql_create_definitive_two)	
	print("Definitive_recap_tmp_1 table created")
	db_self.curs.executescript(""" 
	ALTER TABLE Definitive_recap_tmp_2 ADD entertainment_id INTEGER;
	ALTER TABLE Definitive_recap_tmp_2 ADD en_creation_way TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD e_status TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD class_of_game TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD category_of_game TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD specific_guidelines TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD audio_rules TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD e_audio_intro TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD e_audio_exit TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD e_notes TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD e_disposable INTEGER;
	ALTER TABLE Definitive_recap_tmp_2 ADD e_token TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD e_therapist_grade TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD interacting_mode TEXT;
	ALTER TABLE Definitive_recap_tmp_2 ADD duration_minutes REAL;
	""")
	print("Definitive_recap_tmp_2 table added")
	db_self.conn.commit()
# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Construct
# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
cdef construct_ResultsTotalQuestion_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS ResultsTotalQuestion')
	db_self.curs.execute(oq.sql_create_results_question)
	print("ResultsTotalQuestion table built / updated")

cdef construct_ResultsTotalKindOfGame_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS ResultsTotalKindOfGame')
	db_self.curs.execute(oq.sql_create_results_kind_of_game)

cdef construct_ResultsKindOfGame_new_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS ResultsKindOfGame_new')
	db_self.curs.execute(oq.sql_create_results_new_kind_of_game)		
	db_self.curs.execute("INSERT INTO ResultsKindOfGame_new SELECT * FROM ResultsTotalKindOfGame")
	db_self.curs.execute("DROP TABLE ResultsTotalKindOfGame")
	db_self.curs.execute("ALTER TABLE ResultsKindOfGame_new RENAME TO ResultsTotalKindOfGame")
	db_self.conn.commit()
	print("ResultsTotalKindOfGame table built / updated")
	
cdef construct_ResultsTotalTypeOfGame_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS ResultsTotalTypeOfGame')
	db_self.curs.execute(oq.sql_create_results_type_of_game)

cdef construct_ResultsTypeOfGame_new_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS ResultsTypeOfGame_new')
	db_self.curs.execute(oq.sql_create_results_new_type_of_game)		
	db_self.curs.execute("INSERT INTO ResultsTypeOfGame_new SELECT * FROM ResultsTotalTypeOfGame")
	db_self.curs.execute("DROP TABLE ResultsTotalTypeOfGame")
	db_self.curs.execute("ALTER TABLE ResultsTypeOfGame_new RENAME TO ResultsTotalTypeOfGame")
	db_self.conn.commit()
	print("ResultsTotalTypeOfGame table built / updated")

cdef fix_ResultsKindOfGame_new_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS ResultsKindOfGame_new')
	db_self.curs.execute('DROP TABLE IF EXISTS ResultsTotalKindOfGame')
	db_self.curs.execute(oq.sql_create_results_kind_of_game)
	db_self.curs.execute(oq.sql_create_results_new_kind_of_game)
	db_self.curs.execute("INSERT INTO ResultsKindOfGame_new SELECT * FROM ResultsTotalKindOfGame")
	db_self.conn.commit()
	db_self.curs.execute("DROP TABLE IF EXISTS ResultsTotalKindOfGame")
	db_self.curs.execute("ALTER TABLE ResultsKindOfGame_new RENAME TO ResultsTotalKindOfGame")
	db_self.conn.commit()
	print("ResultsTotalKindOfGame table built")
	print("ResultsTotalKindOfGame added")

cdef construct_ResultsTotalGame_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS ResultsTotalGame')
	db_self.curs.execute(oq.sql_create_results_game)	
	print("ResultsTotalGame table built / updated")

cdef construct_ResultsTotalMatch_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS ResultsTotalMatch')
	db_self.curs.execute(oq.sql_create_results_match)	 
	print("ErrorMatch table built / updated")

cdef construct_ResultsTotalSession_table(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS ResultsTotalSession')
	db_self.curs.execute(oq.sql_create_results_session)
	print("ResultsTotalSession table built / updated")

# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Link
# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
cdef build_link_Treatments_Traits(db_self):
	reload(oq)
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Traits')
	db_self.curs.execute(oq.sql_treatment_traits)
	print("Treatments_Traits table created")
	db_self.curs.executemany('''INSERT INTO Treatments_Traits (treatment_id, trait_id) VALUES (?,?)''', dbdl.all_treatments_traits_targeted)
	db_self.conn.commit()
	print("Treatments_Traits table populated")

cdef compose_definitive_recap(db_self):
	reload(oq)
	#db_self.curs.execute('CREATE table Full_definitive_recap as SELECT * from Definitive_recap_tmp_1 UNION SELECT * from Definitive_recap_tmp_2')
	print("Definitive_recap table ultimated")
	db_self.curs.execute('DROP TABLE IF EXISTS Full_definitive_recap_giga')
	db_self.curs.execute(oq.sql_create_giga)
	db_self.curs.execute(oq.sql_insert_into_giga_1)
	db_self.curs.execute(oq.sql_insert_into_giga_2)
	db_self.curs.execute('DROP TABLE IF EXISTS temp_Full_definitive_recap')
	db_self.curs.execute('DROP TABLE IF EXISTS Definitive_recap_tmp_1')
	db_self.curs.execute('DROP TABLE IF EXISTS Definitive_recap_tmp_2')
	db_self.conn.commit()
