"""Info:
	Oimi robot database controller for deletions (in cascade) of rows or entire tables
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager -->database_controller_delete
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
import io
import traceback
import mmap
import random
import pickle
import linecache as lica
from importlib import reload
from subprocess import run, PIPE, Popen
from typing import Optional
import oimi_queries as oq
import db_extern_methods as dbem

#import db_default_lists_old_tables as dbdlot
cimport database_controller_build as dcb
cimport database_triggers as dbt
cimport database_controller_delete
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Cancel
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef cancel_from_needs(db_self, str which_purpose, str which_scope): 
	cdef:
		int scope
		bint locs_issue = 0
		str search_str_1 = "('{0}', '{1}', 'Invalid', 'Invalid'),".format(which_purpose, which_scope)
		str search_str_2 = "('{0}', 'Invalid', '{1}', 'Invalid'),".format(which_purpose, which_scope)
		str search_str_3 = "('{0}', 'Invalid', 'Invalid', '{1}'),".format(which_purpose, which_scope)
	print("Cancelling {} {} from Needs table".format(which_purpose, which_scope))
	try:
		find_me_1 = search_str_1.encode()
		find_me_2 = search_str_2.encode()
		find_me_3 = search_str_3.encode()
		
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if search_str_1==search_str_2==search_str_3:
				print("Cancelling from Needs...search_str are all equals, scope is equal to zero")
				scope = 0
			elif s.find(find_me_1) != -1:
				print("need scope is equal to one")
				scope = 1
			elif s.find(find_me_2) != -1:
				print("need scope is equal to two")
				scope = 2
			elif s.find(find_me_3) != -1:
				print("need scope is equal to three")
				scope = 3
			else:
				raise Exception
		if scope==0:
			db_self.curs.execute("SELECT need_id FROM Needs WHERE focus = 'no_focus'")
		elif scope==1:
			db_self.curs.execute("SELECT need_id FROM Needs WHERE focus = ? AND match_scope = ?", (which_purpose,which_scope,))
		elif scope==2:
			db_self.curs.execute("SELECT need_id FROM Needs WHERE focus = ? AND game_scope = ?", (which_purpose,which_scope,))
		elif scope==3:
			db_self.curs.execute("SELECT need_id FROM Needs WHERE focus = ? AND question_scope = ?", (which_purpose,which_scope,))
		couple_result = db_self.curs.fetchone()
		print("scope is {}".format(scope))
		if not couple_result:
			('--> The Need you want to delete is not present!!!')
			raise Exception
		else:	 
			id_found = couple_result[0] 
			code = 'need_id' 
			which_table = 'Needs'
			which_table = which_table.replace("\'", "")
			which_id = code.replace("\'", "")
			print("which_table need {}".format(which_id))
			print("which_id need {}".format(which_table))
			delete_list = [id_found]
			place_holders = ",".join("?"*len(delete_list))

			gather_line = dbem.get_line_first_deletion()
			print("gather_line need {}".format(gather_line))
			data = gather_line.replace('?', which_table)
			data = data.replace('!', which_id)
			print("data need {}".format(data))
			query = data.replace("query_string = ", "")
			print("query need {}".format(query))
			qq = query.format(place_holders)
			print("qq need {}".format(qq))
			query_1 = qq.replace(".format(place_holders)", "")
			query_1 = query_1.replace("\'\'\'", "")
			print("query_1 need {}".format(query_1))
			db_self.curs.execute(query_1, delete_list)
			db_self.conn.commit()
			if scope==0:
				print("which_purpose is now {}".format(which_purpose))
				txt_to_remove_0 = "grep -v \"\'{}\'\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_5 && mv tmpfile_5 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(which_purpose)
				run(txt_to_remove_0, shell = True)
				print("text_to_remove_3 ))) {}".format(txt_to_remove_0))	
			else:
				txt_to_remove = "grep -v \"\'{}\'\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_5 && mv tmpfile_5 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(which_scope)
				print("text_to_remove need {}".format(txt_to_remove))
				run(txt_to_remove, shell = True)
				if which_scope=='mixed_match' or which_scope=='9CNSPX' \
				or which_scope=='quest_12012':
					with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
						lines = fp.readlines()
					if scope==1:
						locs = [i for i, val in enumerate(lines) if val=="		'mixed_match'\n"]
					if scope==2:
						locs = [i for i, val in enumerate(lines) if val=="		'9CNSPX'\n"]
					if scope==3:
						locs = [i for i, val in enumerate(lines) if val=="		'quest_12012'\n"]
					
					if len(locs)>0:
						read_line = lica.getline('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', locs[0])

						search_str_2 = "{0}".format(read_line)
						search_str_3 = search_str_2.replace(",", "")

						N = len(locs)
						for j in range(N):
							print("locs {} ".format(locs[j]))
							lines.insert(locs[-j], search_str_3)
						
						with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
							fp.write(''.join(lines))
					else:
						locs_issue = 1
						raise Exception

					txt_to_remove_1 = "grep -v \"\'{}\'\" oimi_queries.py > tmpfile_6 && mv tmpfile_6 oimi_queries.py".format(which_scope) 
					print("text_to_remove_1 ))) {}".format(txt_to_remove_1))
					run(txt_to_remove_1, shell = True)

					string_4 = search_str_2.replace("\n", "")
					txt_to_remove_2 = "grep -v \"{}\" oimi_queries.py > tmpfile_7 && mv tmpfile_7 oimi_queries.py".format(string_4) 
					print("text_to_remove_2 ))) {}".format(txt_to_remove_2))
					run(txt_to_remove_2, shell = True)
				else:
					txt_to_remove_1 = "grep -v \"\'{}\'\" oimi_queries.py > tmpfile_6 && mv tmpfile_6 oimi_queries.py".format(which_scope) 
					run(txt_to_remove_1, shell = True)
					print("text_to_remove_3 ))) {}".format(txt_to_remove_1))					

			db_self.curs.execute("DELETE FROM Kids_Needs WHERE need_id isNull")
			db_self.conn.commit()
			print("{} {} cancelled correctly from Needs table".format(which_purpose, which_scope))					
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The need you want to cancel doesn't exists")
		print("--> locs len is {}".format(len(locs)))
		print("--> locs is -->{}".format(locs[0]))
		print("--> Search_str_1 {}".format(search_str_1))
		print("--> Search_str_2 {}".format(search_str_2))
		print("--> Search_str_3 {}".format(search_str_3))
		print("##############################################################################################################")

# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Delete
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef delete_from_id(db_self, int id_to_remove): 
	print("ENTER IN DELETE FROM ID , FOR DELETING = ", id_to_remove)
	cdef:
		list to_eliminate = []
		list all_id_matches = []
		list all_id_games = []
		int is_disposable_or_not = 0
		int total_digits = dbem.count_number(id_to_remove)
		int digit_1 = dbem.digit_selector(id_to_remove, 1,total_digits)
		int digit_2 = int(str(id_to_remove)[:2])
		int leng = len(str(id_to_remove))
		str filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
	print("digit_1 given id is = {}".format(digit_1))
	print("digit_2 given id is = {}".format(digit_2))
	try:
		if digit_1==1 and digit_2==10 and (not id_to_remove==10) and leng==5:
			ind_del = 8
			which_table = 'Needs'
			which_id = 'need_id'
			first_col = 'focus'
			second_col = 'match_scope'
			third_col = 'game_scope'
			fourth_col = 'question_scope'
			what_select = "{}, {}, {}, {}".format(first_col, second_col, third_col, fourth_col) 
		
		elif digit_1==1:
			ind_del = 0
			which_table = 'Kids'
			which_id = 'kid_id'
			col1 = 'name'
			col2 = 'surname'
			col3 = 'age'
			col4 = 'current_level'
			col7 = 'token'
			what_select = "{}, {}, {}, {}, {}".format(col1, col2, col3, col4, col7)

		elif digit_1==2 and leng==5:
			ind_del = 3
			which_table = 'Sessions' #order not count ...it is a select
			which_id = 'session_id'
			col1 = 'num_of_matches'
			col2 = 'complexity'
			col3 = 'status'
			col4 = 'order_difficulty_matches'
			col5 = 'order_quantity_games'
			col6 = 'mandatory_impositions'
			col7 = 'mandatory_needs'
			col8 = 'extra_time_tolerance'
			col9 = 'movements_allowed'
			col10 = 'body_enabled'
			col11 = 'rules_explication'
			col12 = 'stress_flag'
			col13 = 'frequency_exhortations'
			col14 = 'repeat_question'
			col15 = 'repeat_intro'
			col16 = 'desirable_time'
			col17 = 'creation_way'
			col18 = 'disposable'
			col19 = 'token'
			what_select = "{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}".format(col1, 
				col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, 
				col12, col13, col14, col15, col16, col17, col18, col19)
		
		elif digit_1==3 and leng==5:
			ind_del = 4
			which_table = 'Matches'
			which_id = 'match_id'
			col1 = 'num_of_games'
			col2 = 'difficulty'
			col3 = 'variety'
			col4 = 'order_difficulty_games'
			col5 = 'order_quantity_questions'
			col6 = 'criteria_arrangement'
			col7 = 'advisable_time'
			col8 = 'scenario'
			col9 = 'disposable'
			col10 = 'token'
			what_select = "{}, {}, {}, {}, {}, {}, {}, {}, {}, {}".format(col1, 
				col2, col3, col4, col5, col6, col7, col8, col9, col10)
		
		elif digit_1==4 and leng==5:
			ind_del = 5
			which_table = 'Games'
			which_id = 'game_id'
			col1 = 'num_questions'
			col2 = 'kind'
			col3 = 'type'
			col4 = 'difficulty'
			col5 = 'order_difficulty_questions'
			col6 = 'necessary_time'
			col7 = 'disposable'
			col8 = 'token'
			what_select = "{}, {}, {}, {}, {}, {}, {}, {}".format(col1, 
				col2, col3, col4, col5, col6, col7, col8)

		elif digit_1==5 and leng==5:
			ind_del = 6
			which_table = 'Entertainments'
			which_id = 'entertainment_id'
			col1 = 'creation_way'
			col2 = 'status'
			col3 = 'class_of_game'
			col4 = 'specific_guidelines'
			col5 = 'notes'
			col6 = 'disposable'
			col7 = 'token'
			what_select = "{}, {}, {}, {}, {}, {}, {}".format(col1, 
				col2, col3, col4, col5, col6, col7)

		elif digit_1==7 and leng==5:
			ind_del = 1
			which_table = 'Therapies'
			which_id = 'therapy_id'
			col1 = 'num_of_treatments'
			col2 = 'status'
			col3 = 'creation_way'
			col4 = 'duration_days'
			col5 = 'canonical_intervention'
			col6 = 'disposable'
			col7 = 'token'
			what_select = "{}, {}, {}, {}, {}, {}, {}".format(col1, 
				col2, col3, col4, col5, col6, col7)

		elif digit_1==8 and leng==5:
			ind_del = 2
			which_table = 'Treatments'
			which_id = 'treatment_id'
			col1 = 'target_goal_1'
			col2 = 'target_goal_2'
			col3 = 'target_goal_3'
			col4 = 'num_of_pastimes'
			col5 = 'num_of_sessions'
			col6 = 'num_of_entertainments'
			col7 = 'status'
			col8 = 'creation_way'
			col9 = 'disposable'
			col10 = 'token'
			what_select = "{}, {}, {}, {}, {}, {}, {}, {}, {}, {}".format(col1, 
				col2, col3, col4, col5, col6, col7, col8, col9, col10)

		else:
			print("sono in else DELETE")
			print("sono in else DELETE")
			print("sono in else DELETE")
			print("sono in else DELETE")
			print("sono in else DELETE")
			print("sono in else DELETE")
			print("sono in else DELETE")
			print("sono in else DELETE")
			print("sono in else DELETE")
			print("sono in else DELETE")
			ind_del = 7
			which_table = 'Questions'
			which_id = 'question_id'
			col1 = 'kind'
			col2 = 'type'
			col3 = 'audio'
			col4 = 'value'
			col5 = 'description'
			what_select = "{}, {}, {}, {}, {}".format(col1, col2, col3, col4, col5)
			
		which_table = which_table.replace("\'", "")
		which_id = which_id.replace("\'", "")
		
		selections = [id_to_remove]
		place_holders = ",".join("?"*len(selections))
		gather_line = dbem.get_line_selection()
		##################
		print("what_select IS {}".format(what_select))
		print("which_table IS {}".format(which_table))
		print("which_id IS {}".format(which_id))
		print("place_holders IS {}".format(place_holders))
		print("gather_line IS {}".format(gather_line))
		##################
		almost_ready = gather_line.replace("&", what_select)
		almost_ready = almost_ready.replace('?', which_table)
		almost_ready = almost_ready.replace('!', which_id)
		print("almost_ready : {}".format(almost_ready))
		query = almost_ready.replace("query_string_3 = ", "")
		print("query : {}".format(query))
		qq = query.format(place_holders)
		print("qq : {}".format(qq))
		query_1 = qq.replace(".format(place_holders)", "")
		query_1 = query_1.replace("\'\'\'", "")
		print("query_1 : {}".format(query_1))
		db_self.curs.execute(query_1, selections)
		
		#com'era la query???? scrivila nei commenti!!!
		return_que = db_self.curs.fetchall() 
		print("return_que {}".format(return_que))
		db_self.conn.commit()
		
		if not return_que:
			print('--> The element you want to delete FROM ID doesn\'t exist!!')
			raise Exception
		
		if which_table=='Sessions':
			#return que is the fetchall --> a selection!! not transformed in list.... lo scrivo nei commenti!!!!!
			this_num_of_matches = return_que[0][0]
			this_complexity = return_que[0][1]
			this_status = return_que[0][2]
			this_order_difficulty_matches = return_que[0][3]
			this_order_quantity_games = return_que[0][4]
			this_mandatory_impositions = return_que[0][5]
			this_mandatory_needs = return_que[0][6]
			this_extra_time_tolerance = return_que[0][7]
			this_movements_allowed = return_que[0][8]
			this_body_enabled = return_que[0][9]
			this_rules_explication = return_que[0][10]
			this_stress_flag = return_que[0][11]
			this_frequency_exhortations = return_que[0][12]
			this_repeat_question = return_que[0][13]
			this_repeat_intro = return_que[0][14]
			this_desirable_time = return_que[0][15]
			this_creation_way = return_que[0][16]
			this_disposable = return_que[0][17]
			this_token = return_que[0][18]


			if is_disposable_or_not:
				print("is disposable or not!!!! ",is_disposable_or_not)
				db_self.curs.execute("DELETE FROM Full_played_recap_giga WHERE session_id = ?",(id_to_remove,))
				db_self.curs.execute("DELETE FROM Full_played_summary WHERE session_id = ?",(id_to_remove,))		
				db_self.curs.execute('''SELECT m.match_id
				FROM Matches m JOIN Sessions_Matches sm ON m.match_id = sm.match_id
				WHERE sm.session_id = ?''', (id_to_remove,))
				res_m = db_self.curs.fetchall()
				all_id_matches = [item for sublist in res_m for item in sublist]
				print("all_id_matchesall_id_matchesall_id_matchesall_id_matchesall_id_matchesall_id_matchesall_id_matchesall_id_matches ", all_id_matches)
				db_self.curs.execute('''SELECT m.token, m.disposable
				FROM Matches m JOIN Sessions_Matches sm ON m.match_id = sm.match_id
				WHERE sm.session_id = ?''', (id_to_remove,))
				cascade = db_self.curs.fetchall()
				linked_match = [item for sublist in cascade for item in sublist] #flatten list of list in list
				#linked_match = []
				#for i in range(len(cascade)):
				#	for j in range(len(cascade[i])):
				#		linked_match.append(cascade[i][j])
				print("linked_match === ", linked_match)
				match_only = [x for x in linked_match[::2]]
				print("match_only ==> ", match_only)
				disposable_only_m = [linked_match[x] for x in range(len(linked_match)) if x%2 == 1]
				print("disposable_only_m ==> ", disposable_only_m)
				#if matches associated with this session are marekd as disposablke = 1 so I can remove them too...the disposable of the matches is unknow at this time, va recuperato!
				for mat in match_only:
					print("next mat is ", mat)
					print("match_only.index(mat) ==> ", match_only.index(mat))
					print("disposable_only_m[match_only.index(mat)] ??? ",disposable_only_m[match_only.index(mat)])
					if disposable_only_m[match_only.index(mat)]:
						db_self.curs.execute('''DELETE FROM All_id_tokens WHERE token = ?''', [mat])
						db_self.conn.commit()
						rm_token = "grep -v \"('{}')\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_55 && mv tmpfile_55 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(mat)
						run(rm_token, shell = True)

						txt_to_remove_also_mat = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_53 && mv tmpfile_53 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(mat) 
						run(txt_to_remove_also_mat, shell = True)
						print("text_to_remove_match ))) {}".format(txt_to_remove_also_mat))

						db_self.curs.execute('''SELECT match_id 
							FROM Matches 
							WHERE token = ?''', (mat,))
						mat_id = db_self.curs.fetchone()[0]

						db_self.curs.execute('''SELECT g.game_id
							FROM Games g JOIN Matches_Games mg ON g.game_id = mg.game_id
							WHERE mg.match_id = ?''', (mat_id,))
						res_g = db_self.curs.fetchall()
						all_id_games = [item for sublist in res_g for item in sublist]
						print("all_id_gamesall_id_gamesall_id_gamesall_id_gamesall_id_gamesall_id_gamesall_id_gamesall_id_games, ", all_id_games)
						db_self.curs.execute('''SELECT g.token, g.disposable 
							FROM Games g JOIN Matches_Games mg ON g.game_id = mg.game_id
							WHERE mg.match_id = ?''', (mat_id,))
						cascade = db_self.curs.fetchall()
						linked_game = [item for sublist in cascade for item in sublist] #flatten list of list in list
						print("linked_game === ", linked_game)
						#linked_game = []
						#for i in range(len(cascade)):
						#	for j in range(len(cascade[i])):
						#		linked_game.append(cascade[i][j])
						game_only = [x for x in linked_game[::2]]
						print("game_only ==> ", game_only)
						disposable_only_g = [linked_game[x] for x in range(len(linked_game)) if x%2 == 1]
						print("disposable_only_g games ==> ", disposable_only_g)
						for gam in game_only:
							print("disposable_only_g[game_only.index(gam)] ==> ", disposable_only_g[game_only.index(gam)])
							if disposable_only_g[game_only.index(gam)]:
								db_self.curs.execute('''DELETE FROM All_id_tokens WHERE token = ?''', [gam])
								db_self.conn.commit()
								txt_to_remove_also_gam = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_53 && mv tmpfile_53 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(gam) 
								run(txt_to_remove_also_gam, shell = True)
								print("text_to_remove_game ))) {}".format(txt_to_remove_also_gam))
								rm_token = "grep -v \"('{}')\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_55 && mv tmpfile_55 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(gam)				
								run(rm_token, shell = True)
				
				#####add to deletions list
				for aim in all_id_matches:
					with open(filename_pk, 'rb') as fi:
						db_self.deletions_list = pickle.load(fi)
					print("SIAMO IN DELETE ED HO TROVATO QUESTA DELETION LIST!!! ", db_self.deletions_list)
					if db_self.deletions_list[ind_del]==[0]:
						db_self.deletions_list[ind_del].pop(0)
					db_self.deletions_list[ind_del].append(aim)
					#if db_self.deletions_list[2]==[0]:
					#	db_self.deletions_list[2].pop(0)
					#db_self.deletions_list[2].append(aim)
					db_self.deletions_list = [list(set(x)) for x in db_self.deletions_list]
					with open(filename_pk, 'wb') as fi:
						pickle.dump(db_self.deletions_list, fi)
					file_poi_cancello = open("/home/pi/OIMI/oimi_code/src/managers/database_manager/save_deletion_to_remove_later.txt", "w")
					file_poi_cancello.write(str(db_self.deletions_list))

				for aig in all_id_games:
					with open(filename_pk, 'rb') as fi:
						db_self.deletions_list = pickle.load(fi)
					print("SIAMO IN DELETE ED HO TROVATO QUESTA DELETION LIST!!! ", db_self.deletions_list)
					if db_self.deletions_list[ind_del]==[0]:
						db_self.deletions_list[ind_del].pop(0)
					db_self.deletions_list[ind_del].append(aig)
					#if db_self.deletions_list[3]==[0]:
					#	db_self.deletions_list[3].pop(0)
					#db_self.deletions_list[3].append(aig)
					db_self.deletions_list = [list(set(x)) for x in db_self.deletions_list]
					with open(filename_pk, 'wb') as fi:
						pickle.dump(db_self.deletions_list, fi)
					file_poi_cancello = open("/home/pi/OIMI/oimi_code/src/managers/database_manager/save_deletion_to_remove_later.txt", "w")
					file_poi_cancello.write(str(db_self.deletions_list))
			else: 
				raise Exception
			
		elif which_table=='Matches':
			this_num_of_games = return_que[0][0]
			this_difficulty = return_que[0][1]
			this_variety = return_que[0][2]
			this_order_difficulty_games = return_que[0][3]
			this_order_quantity_questions = return_que[0][4]
			this_criteria_arrangement = return_que[0][5]
			this_advisable_time = return_que[0][6]
			this_scenario = return_que[0][7]
			is_disposable_or_not = return_que[0][8]
			this_token = return_que[0][9]			

			if is_disposable_or_not:
				db_self.curs.execute("DELETE FROM Full_played_recap_giga WHERE match_id = ?",(id_to_remove,))
				db_self.curs.execute("DELETE FROM Full_played_summary WHERE match_id = ?",(id_to_remove,))	

				db_self.curs.execute('''SELECT g.token, g.disposable 
				FROM Games g JOIN Matches_Games mg ON g.game_id = mg.game_id
				WHERE mg.match_id = ?''', (id_to_remove,))
				cascade = db_self.curs.fetchall()
				#al posto di fare linked_game, qui metto esempio con doppio for anziche linked_game = [item for sublist in cascade for item in sublist] #flatten list of list in list
				linked_game = []
				for i in range(len(cascade)):
					for j in range(len(cascade[i])):
						linked_game.append(cascade[i][j])
				game_only = [x for x in linked_game[::2]]
				disposable_only = [linked_game[x] for x in range(len(linked_game)) if x%2 == 1]
				#if matches associated with this session are marekd as disposablke = 1 so I can remove them too...the disposable of the matches is unknow at this time, va recuperato!
				for gam in game_only:
					if disposable_only[game_only.index(gam)]:
						db_self.curs.execute('''DELETE FROM All_id_tokens WHERE token = ?''', [gam])
						db_self.conn.commit()					
						txt_to_remove_also_gam = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_53 && mv tmpfile_53 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(gam)
						run(txt_to_remove_also_gam, shell = True)
						print("text_to_remove_game ))) {}".format(txt_to_remove_also_gam))
					with open(filename_pk, 'rb') as fi:
						db_self.deletions_list = pickle.load(fi)
					if db_self.deletions_list[ind_del]==[0]:
						db_self.deletions_list[ind_del].pop(0)
					db_self.deletions_list[ind_del].append(id_to_remove)
					#if db_self.deletions_list[3]==[0]:
					#	db_self.deletions_list[3].pop(0)
					#db_self.deletions_list[3].append(id_to_remove)
					db_self.deletions_list = [list(set(x)) for x in db_self.deletions_list]
					with open(filename_pk, 'wb') as fi:
						pickle.dump(db_self.deletions_list, fi)
			else: 
				raise Exception		

		elif which_table=='Needs':
			first_column = return_que[0][0]
			second_column = return_que[0][1]
			third_column = return_que[0][2]
			fourth_column = return_que[0][3]
			if second_column=='Invalid' and third_column=='Invalid':
				second_val_to_send = fourth_column
			if second_column=='Invalid' and fourth_column=='Invalid':
				second_val_to_send = third_column
			if third_column=='Invalid' and third_column=='Invalid':
				second_val_to_send = second_column
			db_self.cancel_from_needs(first_column, second_val_to_send)

			with open(filename_pk, 'rb') as fi:
				db_self.deletions_list = pickle.load(fi)
			if db_self.deletions_list[ind_del]==[0]:
				db_self.deletions_list[ind_del].pop(0)
			db_self.deletions_list[ind_del].append(id_to_remove)
			db_self.deletions_list = [list(set(x)) for x in db_self.deletions_list]
			with open(filename_pk, 'wb') as fi:
				pickle.dump(db_self.deletions_list, fi)

			txt_to_remove_1 = "grep -v \" {}),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_66 && mv tmpfile_66 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(id_to_remove)
			run(txt_to_remove_1, shell = True)
			return()

		elif which_table=='Questions':
			db_self.curs.execute('''
				DELETE FROM ScoresQuestion 
				WHERE question_id = ?''', [id_to_remove]) 
			db_self.conn.commit()
			db_self.build_ScoresKindOfGame_table()
			db_self.build_ScoresKindOfGame_new_table()
			db_self.conn.commit()

			this_kind = return_que[0][0]
			this_type = return_que[0][1]
			this_audio = return_que[0][2]
			this_value = return_que[0][3]
			this_desc = return_que[0][4]

		elif which_table=='Games':
			this_num_questions = return_que[0][0]
			this_kind = return_que[0][1]
			this_type = return_que[0][2]
			this_difficulty = return_que[0][3]
			this_order_difficulty_questions = return_que[0][4]
			this_necessary_time = return_que[0][5]
			is_disposable_or_not = return_que[0][6]
			this_token = return_que[0][7]
		
			if is_disposable_or_not:
				db_self.curs.execute("DELETE FROM Full_played_recap_giga WHERE game_id = ?",(id_to_remove,))
				db_self.curs.execute("DELETE FROM Full_played_summary WHERE game_id = ?",(id_to_remove,))

		elif which_table=='Kids':
			this_name = return_que[0][0]
			this_surname = return_que[0][1]
			this_age = return_que[0][2]
			this_level = return_que[0][3]
			this_token = return_que[0][4]

		elif which_table=='Entertainments':
			this_creation_way = return_que[0][0]
			this_status = return_que[0][1]
			this_class_of_game = return_que[0][2]
			this_specific_guidelines = return_que[0][3]
			this_notes = return_que[0][4]
			is_disposable_or_not = return_que[0][8]
			this_token = return_que[0][6]

			if is_disposable_or_not:
				db_self.curs.execute("DELETE FROM Full_played_recap_giga WHERE match_id = ?",(id_to_remove,))
				db_self.curs.execute("DELETE FROM Full_played_summary WHERE match_id = ?",(id_to_remove,))	
			
			else: 
				raise Exception

		if which_table not in ['Needs', 'Questions']:
			db_self.curs.execute('''
				DELETE FROM All_id_tokens 
				WHERE token = ?''', [this_token])

			rm_token = "grep -v \"('{}')\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_55 && mv tmpfile_55 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_token)				
			run(rm_token, shell = True)

		###########for all again
		if is_disposable_or_not or which_table in ['Kids','Needs','Questions']: 
			which_table = which_table.replace("\'", "")
			print("table removal is {}".format(which_table))
			which_id = which_id.replace("\'", "")
			print("id removal is {}".format(which_id))
			
			to_eliminate = [id_to_remove]
			place_holders = ",".join("?"*len(to_eliminate))
			gather_line = dbem.get_line_first_deletion()
			print("gather_line removal	,, {}".format(gather_line))
		
			dat = gather_line.replace('?', which_table)
			dat = dat.replace('!', which_id)
			print("dat removal ,, {}".format(dat))

			quer = dat.replace("query_string = ", "")
			que_tmp = quer.format(place_holders)
			print("que_tmp removal ,, {}".format(que_tmp))
			quer_1 = que_tmp.replace(".format(place_holders)", "")
			quer_1 = quer_1.replace("\'\'\'", "")
			print("quer_1 removal ,, {}".format(quer_1))
			
			db_self.curs.execute(quer_1, to_eliminate)
			db_self.conn.commit()

			if which_table in ['Sessions']:
				txt_to_remove_32 = "grep -v \"({}, '{}', '{}', {}, {}, '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, {}, {}, {}, '{}', {}, '{}'),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_53 && mv tmpfile_53 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_num_of_matches, 
					this_complexity, this_status, this_mandatory_impositions, this_mandatory_needs, this_order_difficulty_matches, this_order_quantity_games, this_extra_time_tolerance, 
					this_movements_allowed, this_body_enabled, this_rules_explication, this_stress_flag, this_frequency_exhortations, this_repeat_question, this_repeat_intro, 
					this_desirable_time, this_creation_way, this_disposable, this_token)
				run(txt_to_remove_32, shell = True)
				print("text_to_remove_sesion ))) {}".format(txt_to_remove_32))
				print("Ok, Session/s deleted without issues.")
			
				db_self.curs.execute('''DELETE FROM Games_Questions WHERE game_id IN (
					SELECT Games.game_id FROM Games JOIN (Sessions_Matches JOIN Matches_Games USING (match_id)) USING (game_id)
					WHERE session_id = ?)''',(id_to_remove,))
				db_self.conn.commit()
				db_self.curs.execute('''DELETE FROM Games WHERE disposable = 1 AND game_id IN (
					SELECT Games.game_id FROM Games JOIN Matches_Games USING (game_id)
					WHERE match_id isNull)''')
				db_self.conn.commit()
				db_self.curs.execute('''DELETE FROM Games WHERE disposable = 1 AND game_id IN (
					SELECT Games.game_id FROM Games JOIN (Sessions_Matches JOIN Matches_Games USING (match_id)) USING (game_id)
					WHERE session_id = ?)''',(id_to_remove,))
				db_self.conn.commit()

				db_self.curs.execute('''DELETE FROM Matches_Games WHERE match_id IN (
					SELECT Matches.match_id FROM Matches JOIN Sessions_Matches USING (match_id)
					WHERE session_id = ?)''',(id_to_remove,))
				db_self.conn.commit()

				db_self.curs.execute('''DELETE FROM Matches WHERE disposable = 1 AND match_id IN (
					SELECT Matches.match_id FROM Matches JOIN Sessions_Matches USING (match_id)
					WHERE session_id isNull)''')
				db_self.conn.commit()
				db_self.curs.execute('''DELETE FROM Matches WHERE disposable = 1 AND match_id IN (
					SELECT Matches.match_id FROM Matches JOIN Sessions_Matches USING (match_id)
					WHERE session_id = ?)''',(id_to_remove,))
				db_self.conn.commit()

				db_self.curs.execute("DELETE FROM Sessions_Matches WHERE session_id = ?",(id_to_remove,))
				db_self.conn.commit()
				db_self.curs.execute("DELETE FROM Sessions_Impositions WHERE session_id = ?",(id_to_remove,))
				db_self.conn.commit()
				db_self.curs.execute("DELETE FROM Sessions_Needs WHERE session_id = ?",(id_to_remove,))
				db_self.conn.commit()

			elif which_table in ['Matches']:
				txt_to_remove_32 = "grep -v \"({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, '{}'),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_53 && mv tmpfile_53 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_num_of_games, 
					this_difficulty, this_variety, this_order_difficulty_games, this_order_quantity_questions, this_criteria_arrangement, this_scenario, this_advisable_time, 0, this_token)
				run(txt_to_remove_32, shell = True)
				print("text_to_remove_match ))) {}".format(txt_to_remove_32))
				print("Ok, Match/es deleted without issues.")
			elif which_table in ['Games']:
				txt_to_remove_32 = "grep -v \"({}, '{}', '{}', '{}', '{}', {}, {}, '{}'),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_53 && mv tmpfile_53 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_num_questions, 
					this_kind, this_type, this_difficulty, this_order_difficulty_questions, this_necessary_time, 0, this_token)
				run(txt_to_remove_32, shell = True)
				print("text_to_remove_game ))) {}".format(txt_to_remove_32))
				print("Ok, Game/es deleted without issues.")
				
				db_self.curs.execute("DELETE FROM Games_Questions WHERE game_id = ?",(id_to_remove,))
				db_self.curs.execute("DELETE FROM Matches_Games WHERE game_id = ?",(id_to_remove,))
				db_self.conn.commit()
			elif which_table in ['Questions']:	
				remove_in_list_all_questions = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/list_all_questions.csv > tmpfile_53 && mv tmpfile_53 '/home/pi/OIMI/oimi_code/src/managers/database_manager/list_all_questions.csv'".format(this_desc) 
				run(remove_in_list_all_questions, shell = True)
				print("remove_in_list_all_questions ))) {}".format(remove_in_list_all_questions))
				print("Ok, Question/s deleted without issues.")
			elif which_table in ['Kids']:
				db_self.curs.execute("drop trigger if exists enable_deletion_trigger_kid_1")
				dbt.enable_deletion_trigger_kid_1(db_self)
				db_self.curs.execute("drop trigger if exists enable_deletion_trigger_kid_2")
				dbt.enable_deletion_trigger_kid_2(db_self)
				db_self.curs.execute("drop trigger if exists enable_deletion_trigger_kid_3")
				dbt.enable_deletion_trigger_kid_3(db_self)
				db_self.curs.execute("drop trigger if exists enable_deletion_trigger_kid_4")
				dbt.enable_deletion_trigger_kid_4(db_self)
				db_self.curs.execute("drop trigger if exists enable_deletion_trigger_kid_5")
				dbt.enable_deletion_trigger_kid_5(db_self)
				db_self.curs.execute("drop trigger if exists enable_deletion_trigger_kid_6")
				dbt.enable_deletion_trigger_kid_6(db_self)
				db_self.curs.execute("drop trigger if exists enable_deletion_trigger_kid_7")
				dbt.enable_deletion_trigger_kid_7(db_self)
				db_self.conn.commit()

				txt_to_rmv = "grep -v \"('{}', '{}', {}, '{}', photo_{}_{}, details_{}_{}, '{}'),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_59 && mv tmpfile_59 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_name, 
						this_surname, this_age, this_level, this_name, this_surname, this_name, this_surname, this_token)
				print("text_to_remove kids!{}".format(txt_to_rmv))
				run(txt_to_rmv, shell = True)
				txt_to_rmv_2 = "grep -v \"photo_{}_{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_1 && mv tmpfile_1 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_name, this_surname)
				print("text_to_remove kids! foto{}".format(txt_to_rmv_2))
				run(txt_to_rmv_2, shell = True)
				txt_to_rmv_3 = "grep -v \"details_{}_{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_1 && mv tmpfile_1 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_name, this_surname)
				print("text_to_remove kids! details{}".format(txt_to_rmv_3))
				run(txt_to_rmv_3, shell = True)
				
				print("Ok, Kid/s deleted without issues.")

			########with ind_del -->works for all tables!!!!!!
			with open(filename_pk, 'rb') as fi:
				db_self.deletions_list = pickle.load(fi)
			print("SIAMO IN DELETE ED HO TROVATO QUESTA DELETION LIST!!! ", db_self.deletions_list)
			if db_self.deletions_list[ind_del]==[0]:
				db_self.deletions_list[ind_del].pop(0)
			for d in to_eliminate:
				db_self.deletions_list[ind_del].append(d)
			db_self.deletions_list = [list(set(x)) for x in db_self.deletions_list]
			with open(filename_pk, 'wb') as fi:
				pickle.dump(db_self.deletions_list, fi)
			file_poi_cancello = open("/home/pi/OIMI/oimi_code/src/managers/database_manager/save_deletion_to_remove_later.txt", "w")
			file_poi_cancello.write(str(db_self.deletions_list))

			#for all tables remove bridges
			txt_to_remove = "grep -v \"({},\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_55 && mv tmpfile_55 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(id_to_remove)
			run(txt_to_remove, shell = True)
			print("txt_to_remove_finale_bridges >> ",txt_to_remove) 

			txt_to_remove_33 = "grep -v \" {}),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_54 && mv tmpfile_54 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(id_to_remove)
			run(txt_to_remove_33, shell = True)
			print(txt_to_remove_33)
			db_self.conn.commit()
			print("txt_to_remove_finale_bridges_2  >> ",txt_to_remove_33)			

			if len(all_id_matches)>0:
				for am in all_id_matches:
					txt_to_remove = "grep -v \"({},\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_55 && mv tmpfile_55 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(am)
					run(txt_to_remove, shell = True)
					print("txt_to_remove_finale_bridges >> ",txt_to_remove) 

					txt_to_remove_33 = "grep -v \" {}),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_54 && mv tmpfile_54 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(am)
					run(txt_to_remove_33, shell = True)
					print(txt_to_remove_33)
					db_self.conn.commit()
					print("txt_to_remove_finale_bridges_2  >> ",txt_to_remove_33)			

			if len(all_id_games)>0:
				for ag in all_id_games:
					txt_to_remove = "grep -v \"({},\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_55 && mv tmpfile_55 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(ag)
					run(txt_to_remove, shell = True)
					print("txt_to_remove_finale_bridges >> ",txt_to_remove) 

					txt_to_remove_33 = "grep -v \" {}),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_54 && mv tmpfile_54 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(ag)
					run(txt_to_remove_33, shell = True)
					print(txt_to_remove_33)
					db_self.conn.commit()
					print("txt_to_remove_finale_bridges_2  >> ",txt_to_remove_33)			

			db_self.curs.execute("DELETE FROM Kids_Therapies WHERE therapy_id isNull")
			db_self.curs.execute("DELETE FROM Kids_Treatments WHERE treatment_id isNull")
			db_self.curs.execute("DELETE FROM Kids_Entertainments WHERE entertainment_id isNull")
			db_self.curs.execute("DELETE FROM Kids_Sessions WHERE kid_id isNull")
			db_self.curs.execute("DELETE FROM Kids_Matches WHERE kid_id isNull")
			db_self.curs.execute("DELETE FROM Kids_Games WHERE kid_id isNull")
			db_self.curs.execute("DELETE FROM Kids_Matches WHERE match_id isNull")
			db_self.curs.execute("DELETE FROM Kids_Games WHERE game_id isNull")
			db_self.curs.execute("DELETE FROM Therapies_Treatments WHERE therapy_id isNull")
			db_self.curs.execute("DELETE FROM Therapies_Treatments WHERE treatment_id isNull")
			db_self.curs.execute("DELETE FROM Treatments_Sessions WHERE treatment_id isNull")
			db_self.curs.execute("DELETE FROM Treatments_Sessions WHERE session_id isNull")
			db_self.curs.execute("DELETE FROM Treatments_Entertainments WHERE treatment_id isNull")
			db_self.curs.execute("DELETE FROM Treatments_Entertainments WHERE entertainment_id isNull")
			db_self.curs.execute("DELETE FROM Sessions_Matches WHERE session_id isNull")
			db_self.curs.execute("DELETE FROM Sessions_Matches WHERE match_id isNull")	
			db_self.curs.execute("DELETE FROM Matches_Games WHERE match_id isNull")
			db_self.curs.execute("DELETE FROM Matches_Games WHERE game_id isNull")
			db_self.curs.execute("DELETE FROM Games_Questions WHERE game_id isNull")				
			db_self.curs.execute("DELETE FROM Games_Questions WHERE question_id isNull")
			db_self.curs.execute("DELETE FROM Questions_Patches WHERE question_id isNull")
			db_self.curs.execute("DELETE FROM Sessions_Impositions WHERE session_id isNull")
			db_self.curs.execute("DELETE FROM Sessions_Needs WHERE session_id isNull")
			db_self.curs.execute("DELETE FROM Kids_Needs WHERE kid_id isNull")
			db_self.conn.commit()
						
		else: 
			raise Exception

	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef delete_row_from_bridge(db_self, str which_table, int first_param, int second_param, int must_eliminate):
	cdef:
		bint dontContinue = 0 
		str search_str = "({}, {})".format(first_param, second_param)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print("Ok, instance of {} id found. Proceding with removal..".format(which_table.replace("'",'')))
			else:
				print("ECCEZIONE 1111!!")
				print('--> The element you want to delete from {} doesn\'t exist!'.format(which_table.replace("'",'')))
				raise Exception
		#ind_del relative to second_column
		if	which_table=='Kids_Therapies':
			code_1 = 'kid_id'
			code_2 = 'therapy_id'
			ind_del = 1
		elif which_table=='Kids_Treatments':
			code_1 = 'kid_id'
			code_2 = 'treatment_id'
			ind_del = 2
		elif which_table=='Kids_Entertainments':
			code_1 = 'kid_id'
			code_2 = 'entertainment_id'
			ind_del = 6
		elif which_table=='Therapies_Treatments':
			code_1 = 'therapy_id'
			code_2 = 'treatment_id'
			ind_del = 2
		elif which_table=='Treatments_Sessions':
			code_1 = 'treatment_id'
			code_2 = 'session_id'
			ind_del = 3
		elif which_table=='Treatments_Entertainments':
			code_1 = 'treatment_id'
			code_2 = 'entertainment_id'
			ind_del = 6
		elif which_table=='Kids_Sessions':
			code_1 = 'kid_id'
			code_2 = 'session_id'
			ind_del = 3
		elif which_table=='Sessions_Matches':
			code_1 = 'session_id'
			code_2 = 'match_id'
			ind_del = 4 
		elif which_table=='Matches_Games':
			code_1 = 'match_id'
			code_2 = 'game_id'
			ind_del = 5 
		elif which_table=='Games_Questions':
			code_1 = 'game_id'
			code_2 = 'question_id'
		elif which_table=='Questions_Patches':
			code_1 = 'question_id'
			code_2 = 'patch_id'
		elif which_table=='Kids_Needs':
			code_1 = 'kid_id'
			code_2 = 'need_id'
			ind_del = 8
			db_self.add_to_OldKidNeed(first_param, second_param)
		
		elif which_table=='Sessions_Needs':
			code_1 = 'session_id'
			code_2 = 'need_id'
			ind_del = 11
		else:
			print("--> This table cannot be deleted")
			raise Exception

		which_table = which_table.replace("\'", "")
		which_id_1 = code_1.replace("\'", "")
		which_id_2 = code_2.replace("\'", "")
		
		print("which_table ",which_table)
		print("which_id_1 ",which_id_1)
		print("which_id_2 ",which_id_2)

		gather_line = dbem.get_line_second_deletion()
		
		print("gather_line ",gather_line)
		almost_ready = gather_line.replace('?', which_table)
		almost_ready = almost_ready.replace('!', which_id_1)
		almost_ready = almost_ready.replace('$', which_id_2)
		almost_ready = almost_ready.replace('&', str(first_param))
		ready_query = almost_ready.replace('£', str(second_param))

		print("almost_ready = ", almost_ready)
		print("ready_query = ", ready_query)

		query = ready_query.replace("query_string_2 = ", "")
		query_1 = query.replace("\'\'\'", "")

		print("query = ", query)
		print("query_1 = ", query_1)
		
		db_self.curs.execute(query_1) 
		db_self.conn.commit()

		txt_to_rmv = "grep -v \"{}, {}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(first_param, second_param)
		run(txt_to_rmv, shell = True)

		print("txt_to_rmv ==== ",txt_to_rmv)

		if must_eliminate and which_table!='Games_Questions' and which_table!='Questions_Patches':

			rows_to_cancel = [second_param]
			place_holders = ",".join("?"*len(rows_to_cancel))

			filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
			with open(filename_pk, 'rb') as fi:
				db_self.deletions_list = pickle.load(fi)
			if db_self.deletions_list[ind_del]==[0]:
				db_self.deletions_list[ind_del].pop(0)
			for d in rows_to_cancel:
				db_self.deletions_list[ind_del].append(d)
			db_self.deletions_list = [list(set(x)) for x in db_self.deletions_list]
			with open(filename_pk, 'wb') as fi:
				pickle.dump(db_self.deletions_list, fi)

			remove_also_from_table = which_table.split("_", 1)[1].split("/n", 1)[0]
			gather_line_2 = dbem.get_line_first_deletion()
			remove_this_line = which_id_2
			ready_query = gather_line_2.replace('?', remove_also_from_table)
			ready_query = ready_query.replace('!', remove_this_line)
			query_ok = ready_query.replace("query_string = ", "")
			qq = query_ok.format(place_holders)
			query_2 = qq.replace(".format(place_holders)", "")
			query_2 = query_2.replace("\'\'\'", "")
			db_self.curs.execute(query_2, rows_to_cancel) 
			db_self.conn.commit()

			txt_to_rmv_2 = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_3 && mv tmpfile_3 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(second_param)
			run(txt_to_rmv_2, shell = True)
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef delete_row_from_bridge_str(db_self, str which_table, int first_param, str second_param, int must_eliminate):
	cdef:
		bint dontContinue = 0 
		str search_str = "({}, '{}')".format(first_param, second_param)
	print("search_str search_str = ",search_str)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me)==-1:
				print('--> The element you want to delete doesn\'t exist!!')
				raise Exception
		if	which_table=='Kids_Impositions':
			code_1 = 'kid_id'
			code_2 = 'imposition_name'
		elif which_table=='Kids_Achievements':
			code_1 = 'kid_id'
			code_2 = 'achievement_name'
		elif which_table=='Kids_Symptoms':
			code_1 = 'kid_id'
			code_2 = 'symptom_name'
			db_self.add_to_OldKidSymptom(first_param, second_param)
		elif which_table=='Kids_Issues':
			code_1 = 'kid_id'
			code_2 = 'issue_name'
			db_self.add_to_OldKidIssue(first_param, second_param)
		elif which_table=='Kids_Comorbidities':
			code_1 = 'kid_id'
			code_2 = 'comorbidity_name'
			db_self.add_to_OldKidComorbidity(first_param, second_param)
		elif which_table=='Kids_Strengths':
			code_1 = 'kid_id'
			code_2 = 'strength_name'
			db_self.add_to_OldKidStrength(first_param, second_param)
		elif which_table=='Sessions_Impositions':
			code_1 = 'session_id'
			code_2 = 'imposition_name'
		elif which_table=='Sessions_Notes':
			code_1 = 'session_id'
			code_2 = 'notes'
		else:
			raise Exception

		which_table = which_table.replace("\'", "")
		which_id_1 = code_1.replace("\'", "")
		which_id_2 = code_2.replace("\'", "")
		
		gather_line = dbem.get_line_second_deletion()
		second = "'{}'".format(second_param)
		
		almost_ready = gather_line.replace('?', which_table)
		almost_ready = almost_ready.replace('!', which_id_1)
		almost_ready = almost_ready.replace('$', which_id_2)
		almost_ready = almost_ready.replace('&', str(first_param))
		
		ready_query = almost_ready.replace('£', second)

		query = ready_query.replace("query_string_2 = ", "")
		query_1 = query.replace("\'\'\'", "")
		db_self.curs.execute(query_1) 
		db_self.conn.commit()

		txt_to_rmv = "grep -v \"({}, '{}'),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(first_param, second_param)
		run(txt_to_rmv, shell = True)

	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef delete_all_from_bridge_first(db_self, str which_table, int first_id, int must_eliminate):
	cdef:
		list to_eliminate = []
		str search_str = "({},".format(first_id)
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print("The element you want to delete has been detected")
			else:
				print('--> The element you want to delete doesnt exist')
				raise Exception
		if which_table=='Kids_Sessions':
			code = 'kid_id'
			code_2 = 'session_id'
			if must_eliminate:
				db_self.curs.execute("SELECT session_id FROM Kids_Sessions WHERE kid_id = ?", [str(first_id)])
				result = db_self.curs.fetchall()
				for i in range(len(result)):
					to_eliminate.append(result[i][0])
				ind_del = 3
				db_self.curs.execute("SELECT token from Sessions WHERE session_id = ?", [str(result[0][0])])
				tok = db_self.curs.fetchone()[0]
				txt_to_del = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(tok)
				run(txt_to_del, shell = True)

		elif which_table=='Kids_Therapies':
			code = 'kid_id'
			code_2 = 'therapy_id'
			if must_eliminate:
				db_self.curs.execute("SELECT therapy_id FROM Kids_Therapies WHERE kid_id = ?", [str(first_id)])
				result = db_self.curs.fetchall()
				for i in range(len(result)):
					to_eliminate.append(result[i][0])
				ind_del = 1
				db_self.curs.execute("SELECT token from Therapies WHERE entertainment_id = ?", [str(result[0][0])])
				tok = db_self.curs.fetchone()[0]
				txt_to_del = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(tok)
				run(txt_to_del, shell = True)

		elif which_table=='Kids_Treatments':
			code = 'kid_id'
			code_2 = 'treatment_id'
			if must_eliminate:
				db_self.curs.execute("SELECT treatment_id FROM Kids_Treatments WHERE kid_id = ?", [str(first_id)])
				result = db_self.curs.fetchall()
				for i in range(len(result)):
					to_eliminate.append(result[i][0])
				ind_del = 2
				db_self.curs.execute("SELECT token from Treatments WHERE treatment_id = ?", [str(result[0][0])])
				tok = db_self.curs.fetchone()[0]
				txt_to_del = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(tok)
				run(txt_to_del, shell = True)

		elif which_table=='Kids_Entertainments':
			code = 'kid_id'
			code_2 = 'entertainment_id'
			if must_eliminate:
				db_self.curs.execute("SELECT entertainment_id FROM Kids_Entertainments WHERE kid_id = ?", [str(first_id)])
				result = db_self.curs.fetchall()
				for i in range(len(result)):
					to_eliminate.append(result[i][0])
				ind_del = 6
				db_self.curs.execute("SELECT token from Entertainments WHERE entertainment_id = ?", [str(result[0][0])])
				tok = db_self.curs.fetchone()[0]
				txt_to_del = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(tok)
				run(txt_to_del, shell = True)

		elif which_table=='Therapies_Treatments':
			code = 'therapy_id'
			code_2 = 'treatment_id'
			if must_eliminate:
				db_self.curs.execute("SELECT match_id FROM Therapies_Treatments WHERE therapy_id = ? ", [str(first_id)])
				result = db_self.curs.fetchone()[0]
				ind_del = 2
				db_self.curs.execute("SELECT token from Treatments WHERE treatment_id = ?", [str(result[0][0])])
				tok = db_self.curs.fetchone()[0]
				txt_to_del = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(tok)
				run(txt_to_del, shell = True)

		elif which_table=='Treatments_Sessions':
			code = 'treatment_id'
			code_2 = 'session_id'
			if must_eliminate:
				db_self.curs.execute("SELECT match_id FROM Treatments_Sessions WHERE treatment_id = ? ", [str(first_id)])
				result = db_self.curs.fetchone()[0]
				ind_del = 3
				db_self.curs.execute("SELECT token from Sessions WHERE session_id = ?", [str(result[0][0])])
				tok = db_self.curs.fetchone()[0]
				txt_to_del = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(tok)
				run(txt_to_del, shell = True)

		elif which_table=='Sessions_Matches':
			code = 'session_id'
			code_2 = 'match_id'
			if must_eliminate:
				db_self.curs.execute("SELECT match_id FROM Sessions_Matches WHERE session_id = ? ", [str(first_id)])
				result = db_self.curs.fetchone()[0]
				ind_del = 4
				db_self.curs.execute("SELECT token from Matches WHERE match_id = ?", [str(result[0][0])])
				tok = db_self.curs.fetchone()[0]
				txt_to_del = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(tok)
				run(txt_to_del, shell = True)

		elif which_table=='Matches_Games':
			code = 'match_id'
			code_2 = 'game_id'
			if must_eliminate:
				db_self.curs.execute("SELECT game_id FROM Matches_Games WHERE match_id = ? ", [str(first_id)])
				result = db_self.curs.fetchone()[0]
				ind_del = 5
				db_self.curs.execute("SELECT token from Games WHERE game_id = ?", [str(result[0][0])])
				tok = db_self.curs.fetchone()[0]
				txt_to_del = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(tok)
				run(txt_to_del, shell = True)			
		elif which_table=='Kids_Needs':
			code = 'kid_id'
			code_2 = 'need_id'
		
		elif which_table=='Games_Questions':
			code = 'game_id'
			code_2 = 'question_id'
		
		elif which_table=='Questions_Patches':
			code = 'question_id'								   
		else:
			print("--> The table cannot be deleted here")
			raise Exception

		which_table = which_table.replace("\'", "")
		which_id = code.replace("\'", "")
		which_id_2 = code_2.replace("\'", "")
		
		delete_this = [first_id]
		place_holders = ",".join("?"*len(delete_this))

		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
		if db_self.deletions_list[ind_del]==[0]:
			db_self.deletions_list[ind_del].pop(0)
		for d in delete_this:
			db_self.deletions_list[ind_del].append(d)
			db_self.deletions_list = [list(set(x)) for x in db_self.deletions_list]
			with open(filename_pk, 'wb') as fi:
				pickle.dump(db_self.deletions_list, fi)

		read_the_line = dbem.get_line_first_deletion() 
		
		almost_ready = read_the_line.replace('?', which_table)
		almost_ready = almost_ready.replace('!', which_id)
		query = almost_ready.replace("query_string = ", "")
		qq = query.format(place_holders)
		query_1 = qq.replace(".format(place_holders)", "")
		query_1 = query_1.replace("\'\'\'", "")
		db_self.curs.execute(query_1, delete_this) 
		db_self.conn.commit()

		if must_eliminate and which_table!='Games_Questions' and which_table!='Questions_Patches':
			place_holders = ",".join("?"*len(to_eliminate))

			filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
			with open(filename_pk, 'rb') as fi:
				db_self.deletions_list = pickle.load(fi)
			if db_self.deletions_list[ind_del]==[0]:
				db_self.deletions_list[ind_del].pop(0)
			for d in to_eliminate:
				db_self.deletions_list[ind_del].append(d)
			db_self.deletions_list = [list(set(x)) for x in db_self.deletions_list]
			with open(filename_pk, 'wb') as fi:
				pickle.dump(db_self.deletions_list, fi)

			remove_also_from_table = which_table.split("_", 1)[1].split("/n", 1)[0]  
			read_the_line_2 = dbem.get_line_first_deletion()
			almost_ready = read_the_line_2.replace('?', remove_also_from_table)
			almost_ready = almost_ready.replace('!', which_id_2)

			query_ok = almost_ready.replace("query_string = ", "")				
			qq = query_ok.format(place_holders)
			query_2 = qq.replace(".format(place_holders)", "")
			query_2 = query_2.replace("\'\'\'", "")
			db_self.curs.execute(query_2, to_eliminate) 
			db_self.conn.commit()

		txt_to_rmv_db = "grep -v \"({},\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_2 && mv tmpfile_2 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(first_id)
		run(txt_to_rmv_db, shell = True)
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> Alert. The error happened in deletion of rows of table {}".format(which_table))
		print("--> Search_str is {}".format(search_str))
		print("##############################################################################################################")	

############################ riprendo da qui???
cdef delete_all_from_bridge_second(db_self, str which_table, int second_id):
	cdef:
		list to_eliminate = []
		str search_str = "({},".format(second_id)
	try:		
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print("Element to delete is detected")
			else:
				print('--> The element you want to delete doesnt exist!!')
				raise Exception
		if	which_table=='Kids_Sessions':
			code = 'session_id'
			code_2 = 'kid_id'
			db_self.curs.execute("SELECT kid_id FROM Kids_Sessions WHERE session_id = ?", [str(second_id)])
			result = db_self.curs.fetchall()
			for i in range(len(result)):
				to_eliminate.append(result[i][0])
			ind_del = 3
		elif  which_table=='Kids_Therapies':
			code = 'therapy_id'
			code_2 = 'kid_id'
		elif  which_table=='Kids_Treatments':
			code = 'treatment_id'
			code_2 = 'kid_id'
		elif  which_table=='Kids_Entertainments':
			code = 'entertainment_id'
			code_2 = 'kid_id'
		elif  which_table=='Therapies_Treatments':
			code = 'treatment_id'
			code_2 = 'therapy_id'
		elif  which_table=='Treatments_Sessions':
			code = 'session_id'
			code_2 = 'treatment_id'
		elif  which_table=='Treatments_Entertainments':
			code = 'entertainment_id'
			code_2 = 'treatment_id'
		elif  which_table=='Sessions_Matches':
			code = 'match_id'
			code_2 = 'session_id'
		elif  which_table=='Matches_Games':
			code = 'game_id'
			code_2 = 'match_id'
		elif  which_table=='Kids_Needs':
			code = 'need_id'
			code_2 = 'kid_id'
		elif  which_table=='Sessions_Needs':
			code = 'need_id'
			code_2 = 'session_id'		
		elif  which_table=='Games_Questions':
			code = 'question_id'
			code_2 = 'game_id'
		else:
			print("--> Error. This table cannot be deleted here")
			raise Exception

		which_table = which_table.replace("\'", "")
		which_id_1 = code.replace("\'", "")
		which_id_2 = code_2.replace("\'", "")
		
		delete_this = [second_id]
		place_holders = ",".join("?"*len(delete_this))

		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
		if db_self.deletions_list[ind_del]==[0]:
			db_self.deletions_list[ind_del].pop(0)
		for d in delete_this:
			db_self.deletions_list[ind_del].append(d)
			db_self.deletions_list = [list(set(x)) for x in db_self.deletions_list]
			with open(filename_pk, 'wb') as fi:
				pickle.dump(db_self.deletions_list, fi)

		gather_line = dbem.get_line_first_deletion() 
		
		almost_ready = gather_line.replace('?', which_table)
		almost_ready = almost_ready.replace('!', which_id_1)
		query = almost_ready.replace("query_string = ", "")
		qq = query.format(place_holders)
		query_1 = qq.replace(".format(place_holders)", "")
		query_1 = query_1.replace("\'\'\'", "")
		db_self.curs.execute(query_1, delete_this) 
		db_self.conn.commit()

		txt_to_rmv = "sed -i '/ {}),/d' /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(second_id)
		run(txt_to_rmv, shell = True) 
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> Alert. The error happened in deletion of rows of table {}".format(which_table))
		print("--> Search_str is {}".format(search_str))
		print("##############################################################################################################")	

cdef delete_from_id_only_db_default(db_self, int id_to_remove): 
	print("ENTER IN DELETE FROM ID , FOR DELETING = ", id_to_remove)
	cdef:
		list to_eliminate = [id_to_remove]
		int is_disposable_or_not = 0
		int total_digits = dbem.count_number(id_to_remove)
		int digit_1 = dbem.digit_selector(id_to_remove, 1,total_digits)
		int digit_2 = int(str(id_to_remove)[:2])
		int leng = len(str(id_to_remove))
	print("digit_1 given id is = {}".format(digit_1))
	print("digit_2 given id is = {}".format(digit_2))
	filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_game_after_change.pk'
	with open(filename_pk, 'wb') as fi:
		pickle.dump(11, fi)
	
	try:
		if digit_1==1 and digit_2==10 and (not id_to_remove==10) and leng==5:
			ind_del = 8
			which_table = 'Needs'
			which_id = 'need_id'
			first_col = 'focus'
			second_col = 'match_scope'
			third_col = 'game_scope'
			fourth_col = 'question_scope'
			what_select = "{}, {}, {}, {}".format(first_col, second_col, third_col, fourth_col)
		
		elif digit_1==1:
			ind_del = 0
			which_table = 'Kids'
			which_id = 'kid_id'
			col1 = 'name'
			col2 = 'surname'
			col3 = 'age'
			col4 = 'current_level'
			col7 = 'token'
			what_select = "{}, {}, {}, {}, {}".format(col1, col2, col3, col4, col7)

		elif digit_1==2 and leng==5:
			ind_del = 3
			which_table = 'Sessions'
			which_id = 'session_id'
			col1 = 'num_of_matches'
			col2 = 'complexity'
			col3 = 'status'
			col4 = 'order_difficulty_matches'
			col5 = 'order_quantity_games'
			col6 = 'mandatory_impositions'
			col7 = 'mandatory_needs'
			col8 = 'extra_time_tolerance'
			col9 = 'movements_allowed'
			col10 = 'body_enabled'
			col11 = 'rules_explication'
			col12 = 'stress_flag'
			col13 = 'frequency_exhortations'
			col14 = 'repeat_question'
			col15 = 'repeat_intro'
			col16 = 'desirable_time'
			col17 = 'creation_way'
			col18 = 'disposable'
			col19 = 'token'
			what_select = "{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}".format(col1, col2, col3, col4, col5, col6, col7, col8, 
				col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19)
		
		elif digit_1==3 and leng==5:
			ind_del = 4
			which_table = 'Matches'
			which_id = 'match_id'
			col1 = 'num_of_games'
			col2 = 'difficulty'
			col3 = 'variety'
			col4 = 'order_difficulty_games'
			col5 = 'order_quantity_questions'
			col6 = 'criteria_arrangement'
			col7 = 'advisable_time'
			col8 = 'scenario'
			col9 = 'disposable'
			col10 = 'token'
			what_select = "{}, {}, {}, {}, {}, {}, {}, {}, {}, {}".format(col1, col2, col3, col4, col5, col6, col7, col8, col9, col10)
		elif digit_1==4 and leng==5:
			ind_del = 5
			which_table = 'Games'
			which_id = 'game_id'
			col1 = 'num_questions'
			col2 = 'kind'
			col3 = 'type'
			col4 = 'difficulty'
			col5 = 'order_difficulty_questions'
			col6 = 'necessary_time'
			col7 = 'disposable'
			col8 = 'token'
			what_select = "{}, {}, {}, {}, {}, {}, {}, {}".format(col1, col2, col3, col4, col5, col6, col7, col8)
		else:
			ind_del = 7
			which_table = 'Questions'
			which_id = 'question_id'
			col1 = 'kind'
			col2 = 'type'
			col3 = 'audio'
			col4 = 'value'
			col5 = 'description'
			what_select = "{}, {}, {}, {}, {}".format(col1, col2, col3, col4, col5)
			
		which_table = which_table.replace("\'", "")
		which_id = which_id.replace("\'", "")
		
		selections = [id_to_remove]
		place_holders = ",".join("?"*len(selections))
		gather_line = dbem.get_line_selection()
		##################
		print("what_select IS {}".format(what_select))
		print("which_table IS {}".format(which_table))
		print("which_id IS {}".format(which_id))
		print("place_holders IS {}".format(place_holders))
		print("gather_line IS {}".format(gather_line))
		##################
		almost_ready = gather_line.replace("&", what_select)
		almost_ready = almost_ready.replace('?', which_table)
		almost_ready = almost_ready.replace('!', which_id)
		print("almost_ready : {}".format(almost_ready))
		query = almost_ready.replace("query_string_3 = ", "")
		print("query : {}".format(query))
		qq = query.format(place_holders)
		print("qq : {}".format(qq))
		query_1 = qq.replace(".format(place_holders)", "")
		query_1 = query_1.replace("\'\'\'", "")
		print("query_1 : {}".format(query_1))
		db_self.curs.execute(query_1, selections)
		
		return_que = db_self.curs.fetchall() 
		print("return_que {}".format(return_que))
		db_self.conn.commit()
		
		if not return_que:
			print('--> The element you want to delete FROM ID doesn\'t exist!!')
			raise Exception
		
		if which_table=='Sessions':
			#return que is the fetchall not transformed in list....
			this_num_of_matches = return_que[0][0]
			this_complexity = return_que[0][1]
			this_status = return_que[0][2]
			this_order_difficulty_matches = return_que[0][3]
			this_order_quantity_games = return_que[0][4]
			this_mandatory_impositions = return_que[0][5]
			this_mandatory_needs = return_que[0][6]
			this_extra_time_tolerance = return_que[0][7]
			this_movements_allowed = return_que[0][8]
			this_body_enabled = return_que[0][9]
			this_rules_explication = return_que[0][10]
			this_stress_flag = return_que[0][11]
			this_frequency_exhortations = return_que[0][12]
			this_repeat_question = return_que[0][13]
			this_repeat_intro = return_que[0][14]
			this_desirable_time = return_que[0][15]
			this_creation_way = return_que[0][16]
			this_disposable = return_que[0][17]
			this_token = return_que[0][18]


		elif which_table=='Matches':
			this_num_of_games = return_que[0][0]
			this_difficulty = return_que[0][1]
			this_variety = return_que[0][2]
			this_order_difficulty_games = return_que[0][3]
			this_order_quantity_questions = return_que[0][4]
			this_criteria_arrangement = return_que[0][5]
			this_advisable_time = return_que[0][6]
			this_scenario = return_que[0][7]
			is_disposable_or_not = return_que[0][8]
			this_token = return_que[0][9]			

		elif which_table=='Needs':
			first_column = return_que[0][0]
			second_column = return_que[0][1]
			third_column = return_que[0][2]
			fourth_column = return_que[0][3]
			if second_column=='Invalid' and third_column=='Invalid':
				second_val_to_send = fourth_column
			if second_column=='Invalid' and fourth_column=='Invalid':
				second_val_to_send = third_column
			if third_column=='Invalid' and third_column=='Invalid':
				second_val_to_send = second_column

		elif which_table=='Questions':
			this_kind = return_que[0][0]
			this_type = return_que[0][1]
			this_audio = return_que[0][2]
			this_value = return_que[0][3]
			this_desc = return_que[0][4]

		elif which_table=='Games':
			this_num_questions = return_que[0][0]
			this_kind = return_que[0][1]
			this_type = return_que[0][2]
			this_difficulty = return_que[0][3]
			this_order_difficulty_questions = return_que[0][4]
			this_necessary_time = return_que[0][5]
			is_disposable_or_not = return_que[0][6]
			this_token = return_que[0][7]

		elif which_table=='Kids':
			this_name = return_que[0][0]
			this_surname = return_que[0][1]
			this_age = return_que[0][2]
			this_level = return_que[0][3]
			this_token = return_que[0][4]

		if not is_disposable_or_not or which_table in ['Kids','Needs','Questions']: 
			if which_table in ['Sessions']:
				txt_to_remove_32 = "grep -v \"({}, '{}', '{}', {}, {}, '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, {}, {}, {}, {}', {}, '{}'),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_53 && mv tmpfile_53 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_num_of_matches, 
					this_complexity, this_status, this_mandatory_impositions, this_mandatory_needs, this_order_difficulty_matches, this_order_quantity_games, this_extra_time_tolerance, 
					this_movements_allowed, this_body_enabled, this_rules_explication, this_stress_flag, this_frequency_exhortations, this_repeat_question, 
					this_repeat_intro, this_desirable_time, this_creation_way, this_disposable, this_token)
				run(txt_to_remove_32, shell = True)
				print("text_to_remove_sesion ))) {}".format(txt_to_remove_32))
				print("Ok, Session/s deleted without issues.")
			elif which_table in ['Matches']:
				txt_to_remove_32 = "grep -v \"({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, '{}'),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_53 && mv tmpfile_53 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_num_of_games, 
					this_difficulty, this_variety, this_order_difficulty_games, this_order_quantity_questions, this_criteria_arrangement, this_scenario, this_advisable_time, is_disposable_or_not, this_token)
				run(txt_to_remove_32, shell = True)
				print("text_to_remove_match ))) {}".format(txt_to_remove_32))
				print("Ok, Match/es deleted without issues.")
			elif which_table in ['Games']:
				txt_to_remove_32 = "grep -v \"({}, '{}', '{}', '{}', '{}', {}, {}, '{}'),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_53 && mv tmpfile_53 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_num_questions, 
					this_kind, this_type, this_difficulty, this_order_difficulty_questions, this_necessary_time, is_disposable_or_not, this_token)
				run(txt_to_remove_32, shell = True)
				print("text_to_remove_game ))) {}".format(txt_to_remove_32))
				print("Ok, Game/es deleted without issues.")

			elif which_table in ['Questions']:	
				remove_in_list_all_questions = "grep -v \"{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/list_all_questions.csv > tmpfile_53 && mv tmpfile_53 '/home/pi/OIMI/oimi_code/src/managers/database_manager/list_all_questions.csv'".format(this_desc) 
				run(remove_in_list_all_questions, shell = True)
				print("remove_in_list_all_questions ))) {}".format(remove_in_list_all_questions))
				print("Ok, Question/s deleted without issues.")
			elif which_table in ['Kids']:
				txt_to_rmv = "grep -v \"('{}', '{}', {}, '{}', photo_{}_{}, details_{}_{}, '{}'),\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_59 && mv tmpfile_59 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_name, 
						this_surname, this_age, this_level, this_name, this_surname, this_name, this_surname, this_token)
				print("text_to_remove kids!{}".format(txt_to_rmv))
				run(txt_to_rmv, shell = True)
				txt_to_rmv_2 = "grep -v \"photo_{}_{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_1 && mv tmpfile_1 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_name, this_surname)
				print("text_to_remove kids! foto{}".format(txt_to_rmv_2))
				run(txt_to_rmv_2, shell = True)
				txt_to_rmv_3 = "grep -v \"details_{}_{}\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_1 && mv tmpfile_1 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(this_name, this_surname)
				print("text_to_remove kids! details{}".format(txt_to_rmv_3))
				run(txt_to_rmv_3, shell = True)
				
				print("Ok, Kid/s deleted without issues.")

			""" non serve qui!!!????!!! giusto no?????
			filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
			with open(filename_pk, 'rb') as fi:
				db_self.deletions_list = pickle.load(fi)
			if db_self.deletions_list[ind_del]==[0]:
				db_self.deletions_list[ind_del].pop(0)
			for d in to_eliminate:
				db_self.deletions_list[ind_del].append(d)
			db_self.deletions_list = [list(set(x)) for x in db_self.deletions_list]
			with open(filename_pk, 'wb') as fi:
				pickle.dump(db_self.deletions_list, fi)
			"""
		else: 
			print("--> Alert. The element is not disposable, or not exists, thus cannot be removed. ")
			raise Exception

	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		#print("--> Alert. The error happened in deletion of rows of table {}".format(which_table))
		print("--> The problem occurred in {} deletion".format(which_table.replace("'",'')))
		print("##############################################################################################################")