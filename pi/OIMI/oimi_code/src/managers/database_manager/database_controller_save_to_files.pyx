"""Info:
	Oimi robot database_controller_save_to_files, for storing tables pivotal data into files for backup and for creating detailed reports.
	For instance, feedbacks and results of each played session. 
	db_self object not necessary here, oimi_robot_database.db called from terminal
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager --> database_controller_save_to_files
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
import re
import csv
import sqlite3 as lite # da togliere no???
import oimi_queries as oq #da togliere no???
from subprocess import run, Popen, PIPE, STDOUT
from typing import Optional
cimport database_controller_save_to_files
# ---------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Store
# ---------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef store_kids_therapies_so_far(db_self):
	already_erase_times = check_number_of_kids_files_after_erase()
	db_self.curs.execute("SELECT * FROM Kids_Therapies")
	if already_erase_times==0:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_therapies_feedbacks.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==1:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_therapies_feedbacks_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)	
	if already_erase_times==2:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_therapies_feedbacks_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==3:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_therapies_feedbacks_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

cdef store_kids_treatments_so_far(db_self):
	already_erase_times = check_number_of_kids_files_after_erase()
	db_self.curs.execute("SELECT * FROM Kids_Treatments")
	if already_erase_times==0:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_treatments_feedbacks.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==1:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_treatments_feedbacks_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)	
	if already_erase_times==2:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_treatments_feedbacks_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==3:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_treatments_feedbacks_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

cdef store_kids_sessions_so_far(db_self):
	already_erase_times = check_number_of_kids_files_after_erase()
	db_self.curs.execute("SELECT * FROM Kids_Sessions")
	if already_erase_times==0:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==1:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)	
	if already_erase_times==2:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==3:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

cdef store_kids_entertainments_so_far(db_self):
	already_erase_times = check_number_of_kids_files_after_erase()
	db_self.curs.execute("SELECT * FROM Kids_Entertainments")
	if already_erase_times==0:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_entertainments_feedbacks.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==1:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_entertainments_feedbacks_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)	
	if already_erase_times==2:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_entertainments_feedbacks_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==3:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_entertainments_feedbacks_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

cdef store_kids_matches_so_far(db_self):
	already_erase_times = check_number_of_kids_files_after_erase()
	db_self.curs.execute("SELECT * FROM Kids_Matches")
	if already_erase_times==0:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbackscsv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==1:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==2:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==3:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

cdef store_kids_games_so_far(db_self):
	already_erase_times = check_number_of_kids_files_after_erase()
	db_self.curs.execute("SELECT * FROM Kids_Games")
	if already_erase_times==0:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==1:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==2:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==3:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

cdef store_kids_questions_so_far(db_self):
	already_erase_times = check_number_of_kids_files_after_erase()
	db_self.curs.execute("SELECT * FROM Kids_Questions")
	if already_erase_times==0:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==1:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==2:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
	if already_erase_times==3:
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

cdef store_all_scores_and_results_so_far(db_self):
	already_erase_times = check_number_of_kids_files_after_erase()
	if already_erase_times==0:
		db_self.curs.execute("SELECT * FROM Full_played_recap_giga")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/reports_res_full_recap.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM Full_played_summary")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_res_full_summary.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalQuestion")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_questions.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalKindOfGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_kind.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
		db_self.curs.execute("SELECT * FROM ScoresTotalTypeOfGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_type.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalSession")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_sessions.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalMatch")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_matches.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
		db_self.curs.execute("SELECT * FROM ScoresTotalGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_games.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

	if already_erase_times==1:
		db_self.curs.execute("SELECT * FROM Full_played_recap_giga")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/reports_res_full_recap_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM Full_played_summary")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_res_full_summary_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalQuestion")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_questions_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalKindOfGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_kind_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
		db_self.curs.execute("SELECT * FROM ScoresTotalTypeOfGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_type_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalSession")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_sessions_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalMatch")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_matches_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
		db_self.curs.execute("SELECT * FROM ScoresTotalGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_games_1.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

	if already_erase_times==2:
		db_self.curs.execute("SELECT * FROM Full_played_recap_giga")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/reports_res_full_recap_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM Full_played_summary")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_res_full_summary_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalQuestion")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_questions_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalKindOfGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_kind_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
		db_self.curs.execute("SELECT * FROM ScoresTotalTypeOfGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_type_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalSession")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_sessions_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalMatch")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_matches_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
		db_self.curs.execute("SELECT * FROM ScoresTotalGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_games_2.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

	if already_erase_times==3:
		db_self.curs.execute("SELECT * FROM Full_played_recap_giga")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/reports_res_full_recap_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM Full_played_summary")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_res_full_summary_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalQuestion")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_questions_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalKindOfGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_kind_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
		db_self.curs.execute("SELECT * FROM ScoresTotalTypeOfGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_type_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalSession")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_sessions_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

		db_self.curs.execute("SELECT * FROM ScoresTotalMatch")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_matches_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)
		db_self.curs.execute("SELECT * FROM ScoresTotalGame")
		with open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_games_3.csv", "w") as csv_file:
			#csv_writer = csv.writer(csv_file, delimiter="\t")
			csv_writer = csv.writer(csv_file, delimiter=",")
			csv_writer.writerow([i[0] for i in db_self.curs.description])
			csv_writer.writerows(db_self.curs)

cdef store_all_scores_and_results_offline():
	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/tempo_oimi_recap.sql","w+")
	f.write("SELECT * \nFROM Full_played_recap_giga")
	f.close()
	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_full_summary.sql","w+")
	f.write("SELECT * \nFROM Full_played_summary")
	f.close()

	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_sco_ques.sql","w+")
	f.write("SELECT * \nFROM ScoresTotalQuestion")
	f.close()

	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_sco_kind.sql","w+")
	f.write("SELECT * \nFROM ScoresTotalKindOfGame")
	f.close()
	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_sco_type.sql","w+")
	f.write("SELECT * \nFROM ScoresTotalTypeOfGame")
	f.close()

	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_errsess.sql","w+")
	f.write("SELECT * \nFROM ScoresTotalSession")
	f.close()
	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_errmatch.sql","w+")
	f.write("SELECT * \nFROM ScoresTotalMatch")
	f.close()
	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_errgame.sql","w+")
	f.write("SELECT * \nFROM ScoresTotalGame")
	f.close()

	Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/tempo_oimi_recap.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/reports_res_full_recap.csv' ,shell = True)
	Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_full_summary.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_res_full_summary.csv' ,shell = True)
	Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_sco_ques.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_questions.csv' ,shell = True)
	Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_sco_kind.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_kind.csv' ,shell = True)
	Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_sco_type.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_type.csv' ,shell = True)
	Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_errsess.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_sessions.csv' ,shell = True)
	Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_errmatch.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_matches.csv' ,shell = True)
	Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_errgame.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/report_recap_scores_results_all_games.csv' ,shell = True)

	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/tempo_oimi_recap.sql' ,shell = True)
	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_full_summary.sql' ,shell = True)
	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_sco_ques.sql' ,shell = True)
	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_sco_kind.sql' ,shell = True)
	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_sco_type.sql' ,shell = True)
	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_errsess.sql' ,shell = True)
	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_errmatch.sql' ,shell = True)
	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_errgame.sql' ,shell = True)

cdef store_all_scores_last_kid_offline(int which_kid):
	name_fil = 'temp_{}_scores_questions.sql'.format(which_kid)
	fil_name_scoqu = '/home/pi/OIMI/oimi_code/src/playing/methodical_games/score/{}'.format(name_fil)
	f= open(fil_name_scoqu,"w+")
	str_sql_que = "SELECT * FROM ScoresTotalQuestion WHERE kid_id={}".format(which_kid)
	f.write(str_sql_que)
	f.close()
	csv_name = 'scores_questions_for_kid_{}'.format(which_kid)
	comma_save = 'sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{} < {} > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{}.csv'.format(fil_name_scoqu,csv_name)
	comm_rm = 'rm {}'.format(fil_name_scoqu)
	Popen(comma_save ,shell = True)
	Popen(comm_rm ,shell = True)

	name_fil = 'temp_{}_scores_game.sql'.format(which_kid)
	fil_name_scoki = '/home/pi/OIMI/oimi_code/src/playing/methodical_games/score/{}'.format(name_fil)
	f= open(fil_name_scoki,"w+")
	str_sql_que = "SELECT * FROM ScoresTotalKindOfGame WHERE kid_id={}".format(which_kid)
	f.write(str_sql_que)
	f.close()
	csv_name = 'scores_game_for_kid_{}'.format(which_kid)
	comma_save = 'sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{} < {} > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{}.csv'.format(fil_name_scoki,csv_name)
	comm_rm = 'rm {}'.format(fil_name_scoki)
	Popen(comma_save ,shell = True)
	Popen(comm_rm ,shell = True)

	name_fil = 'temp_{}_scores_type_game.sql'.format(which_kid)
	fil_name_scoty = '/home/pi/OIMI/oimi_code/src/playing/methodical_games/score/{}'.format(name_fil)
	f= open(fil_name_scoty,"w+")
	str_sql_que = "SELECT * FROM ScoresTotalTypeOfGame WHERE kid_id={}".format(which_kid)
	f.write(str_sql_que)
	f.close()
	csv_name = 'scores_game_for_type_{}'.format(which_kid)
	comma_save = 'sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{} < {} > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/}.csv'.format(fil_name_scoty,csv_name)
	comm_rm = 'rm {}'.format(fil_name_scoty)
	Popen(comma_save ,shell = True)
	Popen(comm_rm ,shell = True)

	name_fil = 'temp_{}_errsess.sql'.format(which_kid)
	fil_name_errse = '/home/pi/OIMI/oimi_code/src/playing/methodical_games/score/{}'.format(name_fil)
	f= open(fil_name_errse, "w+")
	str_sql_que = "SELECT * FROM ScoresTotalSession WHERE kid_id={}".format(which_kid)
	f.write(str_sql_que)
	f.close()
	csv_name = 'scores_correrrsess_for_kid_{}'.format(which_kid)
	comma_save = 'sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{} < {} > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{}.csv'.format(fil_name_errse,csv_name)
	comm_rm = 'rm {}'.format(fil_name_errse)
	Popen(comma_save ,shell = True)
	Popen(comm_rm ,shell = True)
	
	name_fil = 'temp_{}_errmatch.sql'.format(which_kid)
	fil_name_errma = '/home/pi/OIMI/oimi_code/src/playing/methodical_games/score/{}'.format(name_fil)
	f= open(fil_name_errma,"w+")
	str_sql_que = "SELECT * FROM ScoresTotalMatch WHERE kid_id={}".format(which_kid)
	f.write(str_sql_que)
	f.close()
	csv_name = 'scores_correrrmat_for_kid_{}'.format(which_kid)
	comma_save = 'sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{} < {} > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{}.csv'.format(fil_name_errma,csv_name)
	comm_rm = 'rm {}'.format(fil_name_errma)
	Popen(comma_save ,shell = True)
	Popen(comm_rm ,shell = True)

	name_fil = 'temp_{}_errgame.sql'.format(which_kid)
	fil_name_erroga = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{}'.format(name_fil)
	f= open(fil_name_erroga,"w+")
	str_sql_que = "SELECT * FROM ScoresTotalGame WHERE kid_id={}".format(which_kid)
	f.write(str_sql_que)
	f.close()
	csv_name = 'scores_correrrgam_for_kid_{}'.format(which_kid)
	comma_save = 'sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{} < {} > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/{}.csv'.format(fil_name_erroga,csv_name)
	comm_rm = 'rm {}'.format(fil_name_erroga)
	Popen(comma_save ,shell = True)
	Popen(comm_rm ,shell = True)

cdef store_kids_sessions_so_far_offline():
	already_erase_times = check_number_of_kids_files_after_erase()
	print("ok, arrived...kids_sess")
	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_sessions.sql","w+")
	f.write("SELECT * \nFROM Kids_Sessions")
	f.close()
	if already_erase_times==0:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_sessions.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks.csv' ,shell = True)
	if already_erase_times==1:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_sessions.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_1.csv' ,shell = True)
	if already_erase_times==2:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_sessions.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_2.csv' ,shell = True)
	if already_erase_times==3:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_sessions.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_3.csv' ,shell = True)

	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_sessions.sql' ,shell = True)

cdef store_kids_matches_so_far_offline():
	already_erase_times = check_number_of_kids_files_after_erase()
	print("ok, arrived...kids_matc")
	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_matches.sql","w+")
	f.write("SELECT * \nFROM Kids_Matches")
	f.close()
	if already_erase_times==0:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_matches.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks.csv' ,shell = True)
	if already_erase_times==1:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_matches.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_1.csv' ,shell = True)
	if already_erase_times==2:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_matches.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_2.csv' ,shell = True)
	if already_erase_times==3:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_matches.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_3.csv' ,shell = True)
	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_matches.sql' ,shell = True)

cdef store_kids_games_so_far_offline():
	already_erase_times = check_number_of_kids_files_after_erase()
	print("ok, arrived...kids_game")
	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_games.sql","w+")
	f.write("SELECT * \nFROM Kids_Games")
	f.close()
	if already_erase_times==0:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_games.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks.csv' ,shell = True)
	if already_erase_times==1:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_games.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_1.csv' ,shell = True)
	if already_erase_times==2:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_games.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_2.csv' ,shell = True)
	if already_erase_times==3:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_games.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_3.csv' ,shell = True)
	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_games.sql' ,shell = True)

cdef store_kids_questions_so_far_offline():
	already_erase_times = check_number_of_kids_files_after_erase()
	print("ok, arrived...kids_game")
	f= open("/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_questions.sql","w+")
	f.write("SELECT * \nFROM Kids_Questions")
	f.close()
	if already_erase_times==0:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_questions.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks.csv' ,shell = True)
	if already_erase_times==1:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_questions.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_1.csv' ,shell = True)
	if already_erase_times==2:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_questions.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_2.csv' ,shell = True)
	if already_erase_times==3:
		Popen('sqlite3 -header -csv /home/pi/OIMI/oimi_code/src/oimi_robot_database.db < /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_questions.sql > /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_3.csv' ,shell = True)
	Popen('rm /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/temp_oimi_kids_questions.sql' ,shell = True)

cdef check_number_of_kids_files_after_erase():
	cdef bint already_erased_times = 0
	cmd_find = "find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_1.csv"
	pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	output = pf.stdout.read()
	if 'No such file' in str(output):
		already_erased_times = 0
	else:
		cmd_find = "find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_2.csv"
		pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		output = pf.stdout.read()
		if 'No such file' in str(output):
			already_erased_times = 1
		else:
			cmd_find = "find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_3.csv"
			pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
			output = pf.stdout.read()
			if 'No such file' in str(output):
				already_erased_times = 2
			else:
				already_erased_times = 3
	print("already_erased_times!!!!", already_erased_times)
	return already_erased_times

cdef copy_kids_sessions_in_new_file():
	cmd_find = "find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_1.csv"
	pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	output = pf.stdout.read()
	if 'No such file' in str(output):
		cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks.csv
			/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_1.csv"""
		command = re.sub('\s+', ' ', cmd_copy)
		p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	else:
		cmd_find = "find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_2.csv"
		pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		output = pf.stdout.read()
		if 'No such file' in str(output):
			cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_1.csv
				/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_2.csv"""
			command = re.sub('\s+', ' ', cmd_copy)
			p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		else:
			cmd_find = "find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_3.csv"
			pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
			output = pf.stdout.read()
			if 'No such file' in str(output):
				cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_2.csv
					/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_sessions_feedbacks_3.csv"""
				command = re.sub('\s+', ' ', cmd_copy)
				p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
				
cdef copy_kids_matches_in_new_file():
	cmd_find = "find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_1.csv"
	pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	output = pf.stdout.read()
	if 'No such file' in str(output):
		cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks.csv
			/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_1.csv"""
		command = re.sub('\s+', ' ', cmd_copy)
		p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	else:
		cmd_find = "find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_2.csv"
		pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		output = pf.stdout.read()
		
		if 'No such file' in str(output):
			cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_1.csv
				/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_2.csv"""
			command = re.sub('\s+', ' ', cmd_copy)
			p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		else:
			cmd_find = "find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_3.csv"
			pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
			output = pf.stdout.read()
			
			if 'No such file' in str(output):
				cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_2.csv
					/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_matches_feedbacks_3.csv"""
				command = re.sub('\s+', ' ', cmd_copy)
				p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)

cdef copy_kids_games_in_new_file():
	cmd_find = 'find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_1.csv'
	pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	output = pf.stdout.read()
	if 'No such file' in str(output):
		cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks.csv
			/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_1.csv"""
		command = re.sub('\s+', ' ', cmd_copy)
		p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	else:
		cmd_find = 'find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_2.csv'
		pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		output = pf.stdout.read()
		
		if 'No such file' in str(output):
			cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_1.csv
				/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_2.csv"""
			command = re.sub('\s+', ' ', cmd_copy)
			p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		else:
			cmd_find = 'find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_3.csv'
			pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
			output = pf.stdout.read()
			
			if 'No such file' in str(output):
				cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_2.csv
					/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_games_feedbacks_3.csv"""
				command = re.sub('\s+', ' ', cmd_copy)
				p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)

cdef copy_kids_questions_in_new_file():
	cmd_find = 'find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_1.csv'
	pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	output = pf.stdout.read()
	if 'No such file' in str(output):
		cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks.csv
			/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_1.csv"""
		command = re.sub('\s+', ' ', cmd_copy)
		p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	else:
		cmd_find = 'find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_2.csv'
		pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		output = pf.stdout.read()
		
		if 'No such file' in str(output):
			cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_1.csv
				/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_2.csv"""
			command = re.sub('\s+', ' ', cmd_copy)
			p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
		else:
			cmd_find = 'find /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_3.csv'
			pf = Popen(cmd_find, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
			output = pf.stdout.read()
			
			if 'No such file' in str(output):
				cmd_copy = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_2.csv
					/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/Saved/kids_questions_feedbacks_3.csv"""
				command = re.sub('\s+', ' ', cmd_copy)
				p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
