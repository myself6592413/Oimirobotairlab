#The .pxd file is a Cython file that defines the external Cython API for a Python extension module. 
#It contains the declarations of Cython functions, variables and Cython classes that are meant to be used by other Cython modules.

cdef class DatabaseManager():
	cdef:
		conn
		curs
		list deletions_list
		int imposed_num_matches
		bint imposed_num_questions_done
		str imposed_kind_game
		str imposed_type_game
		str imposed_type_game_by_k
		int imposed_num_games
		int imposed_num_questions
		str imposed_difficulty_match
		str imposed_difficulty_game
		bint imposed_mixed
		imposed_scenario
		int imposed_ques_id
		bint already_done_imposed_num_questions
		bint already_done_imposed_num_matches
		bint already_done_imposed_kind_game
		bint already_done_imposed_type_game
		bint already_done_imposed_num_games
		bint already_done_imposed_ask_ques
		bint already_done_imposed_mixed
		bint already_done_imposed_difficulty_match
		bint already_done_imposed_difficulty_game
		int suggest_num_games_more_than
		int suggest_num_matches_more_than
		str suggested_type_game_from_q
		str suggested_kind_game_from_q
		str suggested_kind_game_from_t
		suggested_scenario_from_t
		suggested_scenario_from_k
		suggested_scenario_from_q
		int mandatory_impositions
		int mandatory_needs
		
		
		bint game_impo_from_q_added
		bint type_impo_from_k_added
		bint type_impo_from_t_added
		#int kid_id
	# --------------------------------------------------------------------------------------------------------------------------------------
	# Pivotal
	# --------------------------------------------------------------------------------------------------------------------------------------	
	cdef setup_database(self)
	cdef adjust_dates(self)
	cdef preliminary_activities_at_creation(self)

	cpdef prova_preliminary_activities(self)
	cpdef drop_all_tables(self)
	cpdef build_OimiSettings(self)
	cpdef execute_a_query(self, query)
	cpdef execute_param_query(self, query, param)
	cpdef execute_new_query(self, query, param)
	cpdef execute_last_query(self, query, param)
	cpdef generic_commit(self)
	cpdef assign_new_id(self, which_table)
	cpdef assign_new_id_after_deletion(self, which_table)
	cpdef create_the_entire_database_from_scratch(self)
	cdef erase_part_of_database_if_full(self)
	# --------------------------------------------------------------------------------------------------------------------------------------
	# Build
	# --------------------------------------------------------------------------------------------------------------------------------------
	cpdef build_Allidtokens(self)
	cpdef build_Kids(self)
	cpdef build_Kids_Parameters(self)
	
	cpdef build_Achievements(self)
	cpdef build_Symptoms(self)
	cpdef build_Issues(self)
	cpdef build_Comorbidities(self)
	cpdef build_Strengths(self)
	cpdef build_Impositions(self)
	cpdef build_Needs(self)

	cpdef build_Patches(self)
	cpdef build_Scenarios(self)
	cpdef build_Kind_games(self)
	cpdef build_Type_games(self)

	cpdef build_Audios(self)

	cpdef build_Therapies(self)
	cpdef build_Treatments(self)
	cpdef build_Class_free(self)
	cpdef build_Entertainments(self)
	cpdef build_Questions(self)
	cpdef build_Games(self)
	cpdef build_ActivityRules(self)
	cpdef build_Matches(self)
	cpdef build_Sessions(self)

	cpdef build_Targets(self)
	cpdef build_TreatmentTarget(self)
	cpdef build_Stages(self)
	cpdef build_prompt_Stages(self)
	cpdef build_reward_Stages(self)
	cpdef build_mood_Stages(self)
	cpdef build_reaction_Stages(self)
	
	cpdef build_Substages(self)
	cpdef build_Prompt_Substages(self)
	cpdef build_Reward_Substages(self)
	cpdef build_Mood_Substages(self)
	cpdef build_Reaction_Substages(self)
	cpdef build_Children_Substages(self)

	cpdef build_TherapyTreatment(self)
	cpdef build_TreatmentSession(self)
	cpdef build_TreatmentEntertainment(self)
	cpdef build_TreatmentPastime(self)
	cpdef build_SessionMatch(self)
	cpdef build_MatchGame(self)
	cpdef build_GameQuestion(self)
	cpdef build_QuestionPatch(self)
	cpdef build_SessionNote(self)
	
	cpdef build_KidNeed(self)
	cpdef build_KidSymptom(self)
	cpdef build_KidIssue(self)
	cpdef build_KidComorbidity(self)
	cpdef build_KidStrength(self)
	cpdef build_KidAchievement(self)
	cpdef build_KidImposition(self)

	cpdef build_NeedQuestion(self)
	cpdef build_ImpositionQuestion(self)

	cpdef build_KidSession(self)
	cpdef build_KidTreatment(self)
	cpdef build_KidTherapy(self)
	cpdef build_KidEntertainment(self)
	cpdef build_KidMatch(self)
	cpdef build_KidGame(self)
	cpdef build_KidQuestion(self)
	
	cpdef build_ScenarioQuestion(self)
	cpdef build_ScenarioType(self)
	cpdef build_ScenarioKind(self)
	cpdef build_Scenario4PSUQuestion(self)
	cpdef build_Scenario4PCOQuestion(self)

	
	cpdef build_SessionEnforcement(self)
	cpdef build_SessionImposition(self)
	cpdef build_SessionNeed(self)

	cpdef build_EntertainmentSubstage(self)
	cpdef build_ClassFreeTarget(self)
	# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	# Create
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef create_Short_recap_all_kids(self)
	cpdef create_Full_sessions_played_recap(self)
	cpdef create_Full_played_recap_giga(self)
	cpdef create_Full_entertainments_played(self)
	cpdef fix_contraints_Full_sesions_summary(self)
	cpdef fix_contraints_Full_therapies_summary(self)
	cpdef fix_contraints_Full_entertainment_played_summary(self)
	cpdef create_definitive_one(self)
	cpdef create_definitive_two(self)
	cpdef compose_definitive(self)
	# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	# Construct
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef construct_ResultsTotalQuestion(self)
	cpdef construct_ResultsTotalKindOfGame_raw(self)
	cpdef construct_ResultsKindOfGame_new(self)
	cpdef construct_ResultsTotalKindOfGame(self)
	cpdef construct_ResultsTotalTypeOfGame_raw(self)
	cpdef construct_ResultsTypeOfGame_new(self)
	cpdef construct_ResultsTotalTypeOfGame(self)
	cpdef fix_TotalResultsKindOfGame_new(self)
	cpdef construct_ResultsTotalGame(self)
	cpdef construct_ResultsTotalMatch(self)
	cpdef construct_ResultsTotalSession(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Place
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef place_patches_into_Patches(self)
	cpdef place_entertainments(self)
	cpdef place_data_into_Questions_Patches(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Populate
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef populate_scenarios_types(self)
	cpdef populate_scenarios_kinds(self)
	cpdef populate_scenarios_questions(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Insert
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef insert_in_scenarios(self)
	cpdef insert_in_questions(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Fill
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef fill_recap(self, kid)
	cpdef fill_summary(self, kid)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Enable
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef start_duration_trigger(self)
	cpdef start_pastimes_trigger(self)
	cpdef start_pastimes_trigger_2(self)
	cpdef start_patches_trigger(self)
	cpdef start_deletion_triggers(self)
	cpdef start_scores_triggers(self)
	cpdef start_disposable_triggers(self)
	cpdef start_needs_triggers(self)
	cpdef start_kind_games_trigger(self)
	cpdef start_free_games_trigger(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Generate
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef generate_audio_intro_exit_session(self)
	cpdef generate_audio_intro_exit_match(self)
	cpdef generate_audio_intro_exit_game(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Change
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef change_num_questions(self, int numq, int idg)
	cpdef change_all_advisable_times(self)
	cpdef change_all_desirable_times(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Delete
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef delete_by_id(self, int id_to_remove)
	cpdef delete_by_id_only_db_default(self, int id_to_remove)
	cpdef delete_by_bridge(self, str which_table, int first_param, int second_param, int must_eliminate)
	cpdef delete_by_str(self, str which_table, int first_param, str second_param, int must_eliminate)
	cpdef delete_all_by_first(self, str which_table, int first_id, int must_eliminate)
	cpdef delete_all_by_second(self, str which_table, int second_id)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Add
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef add_new_Therapy(self, int kid_id, int num_of_treatments, str canonical_intervention)
	cpdef add_new_Treatment(self, int kid_id, str target_goal_1, str target_goal_2, str target_goal_3, int num_of_pastimes, int num_of_sessions, int num_of_entertainments)
	cpdef add_new_Entertainment(self, int kid_id, str class_of_game, str specific_guidelines)

	cpdef add_new_Match(self, str difficulty, int num_of_games, str variety, str order_difficulty_games, str order_quantity_questions, str criteria_arrangement, int advisable_time, scenario, bint disposable)
	cpdef add_new_Game(self, int num_questions, str kind_game, str type_game, str difficulty, str order_difficulty_questions, int necessary_time, bint disposable, scenario, list list_types, list questions_already_choosen_prev_match)
	cpdef add_new_Question(self, str kind_game, str type_game, str audio_id, int value, str description)
	cpdef add_new_SessionMatch(self, int session_id, int match_id)
	cpdef add_new_MatchGame(self, int match_id, int game_id)
	cpdef add_new_GameQuestion(self, int game_id, int question_id)
	cpdef add_new_KidSession(self, int kid_id, int session_id)
	cpdef add_new_KidImposition(self, int kid_id, str imposition_name)
	cpdef add_new_KidNeed(self, int kid_id, int need_id)
	cpdef add_new_KidAchievement(self, int kid_id, str achievement_name)
	cpdef add_new_KidSymptom(self, int kid_id, str symptom_name)
	cpdef add_new_KidIssue(self, int kid_id, str issue_name)
	cpdef add_new_KidComorbidity(self, int kid_id, str comorbidity_name)
	cpdef add_new_KidStrength(self, int kid_id, str strength_name)
	cpdef add_new_SessionEnforcement(self, int session_id, str enforcement_name)	
	cpdef add_new_SessionImposition(self, int session_id, str imposition_name)
	cpdef add_new_SessionNeed(self, int session_id, int need_id)

	cpdef add_new_TreatmentEntertainment(self, int treatment_id, int entertainment_id)
	cpdef add_new_TreatmentSession(self, int treatment_id, int session_id)
	cpdef add_new_TreatmentPastime(self, int treatment_id, int pastime_id)

	cpdef add_to_OldKidNeed(self, int kid_id, int need_id)
	cpdef add_to_OldKidSymptom(db_self, int kid_id, str old_symptom_name)
	cpdef add_to_OldKidIssue(db_self, kid_id, old_issue_name)
	cpdef add_to_OldKidComorbidity(db_self, int kid_id, str old_comorbidity_name)
	cpdef add_to_OldKidStrength(db_self, int kid_id, str old_strenght_name)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Take
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------	
	cdef take_one_session_56(self, int which_kid)
	cdef take_one_session_78(self, int which_kid)
	cdef take_one_session_910(self, int which_kid)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Update 
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef update_Needs_constraints(self, str focus_contr, str need_contr, str position)
	cpdef update_Kind_constraints(self, str new_kind, str new_details)
	cpdef update_Patches_genre_1(self, str new_genre)
	cpdef update_Patches_genre_2(self, str new_subject, str new_genre)
	cpdef update_Patches_constraints(self, str new_subject, str new_color)
	cpdef update_constraint_of(self, str constraint_str, str table)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Insert
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef insert_new_Audios_table(self, str constraint_audio, str audio_path, int is_a_substitution)
	cpdef insert_new_Achievements_table(self, str constraint_str)
	cpdef insert_new_Needs_table(self, str constraint_str_1, str constraint_str_2, str scope)
	cpdef insert_new_Symptoms_table(self, str constraint_str)
	cpdef insert_new_Impositions_table(self, str constraint_str)
	cpdef insert_new_Kind_of_games_table(self, str constraint_kind, str details_kind)
	cpdef insert_new_Type_of_games_table(self, str constraint_type, str details, str example, str kind)
	cpdef insert_new_Patches_table(self, str new_subject, str new_color, str new_genre)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Refresh
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef refresh_Patches_table(self, str new_subject, str new_color, str new_genre)
	cpdef refresh_Treatments_Pastimes_table(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Renew
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef renew_Kids_Achievements(self, int which_kid)
	cpdef renew_Kids_Achievements_1(self, int which_kid)
	cpdef renew_Kids_Achievements_2(self, int which_kid)
	cpdef renew_Kids_Achievements_3(self, int which_kid)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Put
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef put_kid_questions_scores(self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
		str quantity_feedback_param, str grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest)
	cpdef put_kid_games_scores(self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
		str quantity_feedback_param, str grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest)
	cpdef put_kid_matches_scores(self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
		int quantity_feedback_param, int grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest)
	cpdef put_kid_session_scores(self, float time_param, int quantity_feedback_param, int grade_feedback_param, float value_feedback_param, 
		int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Include
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef include_all_feeds_in_once(self, int kid_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds)
	cpdef include_all_results_in_once(self, int kid_id, float time_sess, list questions_results, list matches_results, list games_results)
	cpdef include_all_feeds_in_once_given_session(self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds)
	cpdef include_all_results_in_once_given_session(self, int kid_id, int session_id, float time_sess, list questions_results, list matches_results, list games_results)
	cpdef include_therapist_grade_session(self, int treat_id, int sess_id, str grade)
	cpdef include_therapist_grade_entertainment(self, int treat_id, int enter_id, str grade)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Store
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef store_scores_results_offline(self)
	cpdef store_scores_last_kid_offline(self,int which_kid)
	cpdef store_kids_sessions_offline(self)
	cpdef store_kids_matches_offline(self)
	cpdef store_kids_games_offline(self)
	cpdef store_kids_questions_offline(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Check
	# ----------------------------------------------------------------------------------------------------------------------------------------	
	cpdef int check_session_mixed(self, session_id)
	cpdef int check_match_mixed(self, match_id)

	cpdef build_OldKidSymptom(self)
	cpdef build_OldKidNeed(self)
	cpdef build_OldKidIssue(self)
	cpdef build_OldKidComorbidity(self)
	cpdef build_OldKidStrength(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Link
	# ----------------------------------------------------------------------------------------------------------------------------------------	
	cpdef build_Treatments_Traits(self)