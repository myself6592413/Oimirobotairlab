"""Info:
	Oimi robot database_controler_add_into 
	database module for adding new elements into the right tables and adding a new line into db_default_lists.
	In the case of adding a new relationship between elements (create a new istance in a Bridge table), no new constraint need to be added (no need to update oimi_queries file).
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager database_controller_add_into
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
import mmap
import copy
import random
import pickle
import traceback
from importlib import reload
from typing import Optional
import oimi_queries as oq
import db_default_lists as dbdl
import db_extern_methods as dbem
import db_extern_methods_two as dbem2

cimport modify_default_lists as mdl
cimport add_prediction as ap
cimport add_customized_prediction as acp
cimport database_controller_save_to_files as dcs
cimport database_controller_build as dcb
cimport database_controller_add_into as dba
cimport database_controller_generate_online as dbg
cimport database_controller_add_into_part_2 as dba2
cimport database_controller_add_into_part_2_1 as dba21
cimport database_controller_add_into_part_3 as dba3
cimport database_controller_add_into_part_4 as dba4
# =============================================================================================================================================================================================================================================
# Methods
# =============================================================================================================================================================================================================================================
cdef annex_new_therapy(db_self, int kid_id, int num_of_treatments, str canonical_intervention):	
	''' qui non cerco se esiste già non serve a nulla!! nuova terapia specifica!
	#missing:
		#therapy_id
		#status
		#creation_way
		#kick_off_date
		#closing_date
		#duration_days
		#disposable
		#token
		
	#(3, 'to_do', 'automatically', None, None, 25, 'Eibi', 0, 'thb543n9ca'),
	'''
	print("annex_new_therapy")
	print("annex_new_therapy")
	print("annex_new_therapy")
	print("annex_new_therapy")
	cdef:
		str new_id = db_self.assign_new_id('Therapies')
		bint new_del = 0
		str status_decided = 'to_do'
		str creation_way_auto = 'automatically'
		int disposable_auto = 0
		int duration_days_auto = 1
	try:
		reload(oq)
		reload(dbdl)
		kick_off = None
		closing = None

		db_self.curs.execute('''INSERT INTO Therapies (status, num_of_treatments, canonical_intervention, creation_way, duration_days, disposable, token)
			VALUES (?,?,?,?,?,?,?)''', (status_decided, num_of_treatments, canonical_intervention, creation_way_auto, duration_days_auto, disposable_auto, new_id))

		#db_self.curs.execute('''INSERT INTO Therapies (status, num_of_treatments, canonical_intervention, creation_way, 
		#	kick_off_date, closing_date, duration_days, disposable, token)
		#	VALUES (?,?,?,?,?,?,?,?,?)''', (status_decided, num_of_treatments, canonical_intervention, creation_way_auto, 
		#	kick_off, closing, duration_days_auto, disposable_auto, new_id))
		posi = 0
		db_self.conn.commit()

		db_self.curs.execute( "SELECT COUNT(rowid) FROM Therapies")
		num_last_line_th = db_self.curs.fetchone()
		print("num_last_line_th {}".format(num_last_line_th))
		if num_last_line_th:
			num_last_line_th = num_last_line_th[0]
			#num_last_line_th = num_last_line_th 
		else:
			num_last_line_th = 0
			print("ERRORE_GRAVISSIMOOOOOOOOOOOOOO")

		mdl.modify_Therapies_list(num_of_treatments, status_decided, creation_way_auto, None, None, canonical_intervention, duration_days_auto, disposable_auto, new_id, num_last_line_th)

		db_self.curs.execute('''UPDATE Therapies SET kick_off_date = DATETIME('now','localtime') WHERE therapy_id IN (
			SELECT therapy_id FROM Therapies ORDER BY therapy_id DESC LIMIT 1)''')
		db_self.conn.commit()
		db_self.curs.execute("SELECT * FROM Therapies")
		old_ther = db_self.curs.fetchall()
		dbg.change_all_durations_or_kick_date_therapies(db_self, old_ther, 1)
		db_self.conn.commit()
		#print("new_id to add che da problemi...", new_id)
		#db_self.curs.execute("INSERT INTO All_id_tokens (token) VALUES (?)",(new_id,))
		#db_self.conn.commit()
		
		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
			print("ho trovato questa deletions_list!!!!")
			print("deletions_list = {}".format(db_self.deletions_list))
		if db_self.deletions_list[1]!=[0]:
			new_del = 1
			dbem.bubblesort(db_self.deletions_list[1])
			db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Therapies\'")
			index_to_replace = db_self.curs.fetchone()[1] 
			index_corrected = db_self.deletions_list[1].pop(0)
			db_self.curs.execute('''UPDATE Therapies SET therapy_id = ?  WHERE therapy_id = ?''', (index_corrected,index_to_replace))
			db_self.conn.commit()
			if not db_self.deletions_list[1]:
				db_self.deletions_list[1].append(0)
				filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
				with open(filename_pk, 'wb') as fi:
					pickle.dump(db_self.deletions_list, fi)

		print("The new Therapy is now added.")
		id_ther = db_self.execute_a_query("SELECT therapy_id FROM Therapies ORDER BY therapy_id DESC LIMIT 1")
		id_to_send = id_ther[0]
		print("id_ther id_ther id_ther ", id_ther)
		print("id_ther id_ther id_ther ", id_to_send)
		dba.add_new_KidTherapy_table(db_self, kid_id, id_to_send)

	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new Therapy")
		print("##############################################################################################################")	

cdef annex_new_treatment(db_self, int kid_id, str target_goal_1, str target_goal_2, str target_goal_3, int num_of_pastimes, int num_of_sessions, int num_of_entertainments):
	''' qui non cerco se esiste già non serve a nulla!! nuova terapia specifica!
	#missing:
		#treatment_id	
		#status 
		#creation_way 
		#audio_treatment_intro 
		#audio_treatment_exit 
		#day 
		#disposable 
		#token 
	('cognitive_skills', 'emotion_expression', None, 3, 1, 2, 'to_do', 'manually', '2022-10-07 13:08:55', 0, 'trl2hkjlj2'),
	int kid_id, str target_goal_1, str target_goal_2, str target_goal_3, int num_of_pastimes, int num_of_sessions, int num_of_entertainments):
	'''
	print("annex_new_treatment")
	print("annex_new_treatment")
	print("annex_new_treatment")
	print("annex_new_treatment")
	cdef:
		str new_id = db_self.assign_new_id('Treatments')
		bint new_del = 0
		day_auto = None
		str status_decided = 'to_do'
		str creation_way_auto = 'automatically'
		int disposable_auto = 0

	try:
		reload(oq)
		reload(dbdl)
		db_self.curs.execute('''INSERT INTO Treatments (target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, num_of_sessions, num_of_entertainments,
			status, creation_way, day, disposable, token)
			VALUES (?,?,?,?,?,?,?,?,?,?,?)''', (target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, num_of_sessions, num_of_entertainments,
			status_decided, creation_way_auto, day_auto, disposable_auto, new_id))
		posi = 0
		db_self.conn.commit()

		query_audio_intro = "UPDATE Treatments SET audio_treatment_intro = '{}'".format('audio_1')
		query_audio_exit = "UPDATE Treatments SET audio_treatment_exit = '{}'".format('audio_2')
		db_self.curs.execute(query_audio_intro)
		db_self.curs.execute(query_audio_exit)

		db_self.curs.execute( "SELECT COUNT(rowid) FROM Treatments")
		num_last_line_tr = db_self.curs.fetchone()
		print("num_last_line_tr COMINCIOCOMINCIO {}".format(num_last_line_tr))
		if num_last_line_tr:
			num_last_line_tr = num_last_line_tr[0]
			#num_last_line_tr = num_last_line_tr 
			print("num_last_line_tr DOPODOPO {}".format(num_last_line_tr))
		else:
			num_last_line_tr = 0
			print("ERRORE_GRAVISSIMOOOOOOOOOOOOOO")

		mdl.modify_Treatments_list(target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, num_of_sessions, num_of_entertainments,
			status_decided, creation_way_auto, day_auto, disposable_auto, new_id, num_last_line_tr)

		db_self.curs.execute('''UPDATE Treatments SET day = DATETIME('now','localtime') WHERE treatment_id IN (
			SELECT treatment_id FROM Treatments ORDER BY treatment_id DESC LIMIT 1)''')
		db_self.conn.commit()
		db_self.curs.execute("SELECT * FROM Treatments")
		old_tre = db_self.curs.fetchall()
		dbg.change_day_treatments(db_self, old_tre)
		db_self.conn.commit()
		
		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
			print("ho trovato questa deletions_list!!!!")
			print("deletions_list = {}".format(db_self.deletions_list))
		if db_self.deletions_list[2]!=[0]:
			new_del = 1
			dbem.bubblesort(db_self.deletions_list[2])
			db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Treatments\'")
			index_to_replace = db_self.curs.fetchone()[1] 
			index_corrected = db_self.deletions_list[2].pop(0)
			db_self.curs.execute('''UPDATE Treatments SET treatment_id = ?	WHERE treatment_id = ?''', (index_corrected, index_to_replace))
			db_self.conn.commit()
			if not db_self.deletions_list[2]:
				db_self.deletions_list[2].append(0)
				filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
				with open(filename_pk, 'wb') as fi:
					pickle.dump(db_self.deletions_list, fi)

		print("The new Treatments is now added.")
		id_tr = db_self.execute_a_query("SELECT treatment_id FROM Treatments ORDER BY treatment_id DESC LIMIT 1")
		id_to_send = id_tr[0]
		print("id_ther id_ther id_ther ", id_tr)
		print("id_ther id_ther id_ther ", id_to_send)
		dba.add_new_KidTreatment_table(db_self, kid_id, id_to_send)

	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new Treatment")
		print("##############################################################################################################")	

cdef annex_new_entertainment(db_self, int kid_id, str class_of_game, str specific_guidelines):
	''' 
	('automatically', 'to_do', 'OnlyManual', 'regola_aggiuntiva_1', 'to do at the beginning of a treatment', 0, 'en1kjhgfds'),
	#missing;
		#entertainment_id 
		#creation_way 
		#status 
		#category_of_game (automatic?)
		#specific_guidelines 
		#audio_rules 
		#audio_intro 
		#audio_exit 
		#notes 
		#disposable 
		#token 
	'''
	print("annex_new_entertainment")
	print("annex_new_entertainment")
	print("annex_new_entertainment")
	print("annex_new_entertainment")
	cdef:
		str new_id = db_self.assign_new_id('Entertainments')
		bint new_del = 0
		str status_decided = 'to_do'
		str creation_way_auto = 'automatically'
		int disposable_auto = 0
		str notes_none = None
	#@category_of_game^?????????? com'era??? automatic?"" TRIGGER NO????
	try:
		reload(oq)
		reload(dbdl)
		db_self.curs.execute('''INSERT INTO Entertainments (creation_way, status, class_of_game, specific_guidelines, notes, disposable, token)
			VALUES (?,?,?,?,?,?,?)''', (creation_way_auto, status_decided, class_of_game, specific_guidelines, notes_none, disposable_auto, new_id))
		posi = 0
		db_self.conn.commit()

		db_self.curs.execute( "SELECT COUNT(rowid) FROM Entertainments")
		num_last_line_en = db_self.curs.fetchone()
		print("num_last_line_en {}".format(num_last_line_en))
		if num_last_line_en:
			num_last_line_en = num_last_line_en[0]
			#num_last_line_en = num_last_line_en 
		else:
			num_last_line_en = 0
			print("ERRORE_GRAVISSIMOOOOOOOOOOOOOO")
		mdl.modify_Entertainments_list(creation_way_auto, status_decided, class_of_game, specific_guidelines, notes_none, disposable_auto, new_id, num_last_line_en) 

		query_audio_rules = "UPDATE Treatments SET audio_rules = '{}'".format('audio_rules')
		query_audio_intro = "UPDATE Treatments SET audio_intro = '{}'".format('audio_11')
		query_audio_exit = "UPDATE Treatments SET audio_exit = '{}'".format('audio_22')
		db_self.curs.execute(query_audio_rules)
		db_self.curs.execute(query_audio_intro)
		db_self.curs.execute(query_audio_exit)

		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
			print("ho trovato questa deletions_list!!!!")
			print("deletions_list = {}".format(db_self.deletions_list))
		if db_self.deletions_list[6]!=[0]:
			new_del = 1
			dbem.bubblesort(db_self.deletions_list[6])
			db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Entertainments\'")
			index_to_replace = db_self.curs.fetchone()[1] 
			index_corrected = db_self.deletions_list[6].pop(0)
			db_self.curs.execute('''UPDATE Entertainments SET entertainment_id = ?	WHERE entertainment_id = ?''', (index_corrected, index_to_replace))
			db_self.conn.commit()
			if not db_self.deletions_list[6]:
				db_self.deletions_list[6].append(0)
				filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
				with open(filename_pk, 'wb') as fi:
					pickle.dump(db_self.deletions_list, fi)

		print("The new Entertainments is now added.")

		id_en = db_self.execute_a_query("SELECT entertainment_id FROM Entertainments ORDER BY entertainment_id DESC LIMIT 1")
		id_to_send = id_en[0]
		print("id_ther id_ther id_ther ", id_en)
		print("id_ther id_ther id_ther ", id_to_send)
		dba.add_new_KidEntertainment_table(db_self, kid_id, id_to_send)

	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> The error occurred while adding a new entertainments")
		print("##############################################################################################################")	
