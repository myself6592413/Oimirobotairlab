"""Oimi robot database_manager, oimi_database.db whole infrastructure creation and maintanance
Notes:
For detailed info, look at ./code_docs_of_dabatase_manager
Author:
Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
import mmap
import random
import pickle
import copy
from importlib import reload
from typing import Optional
import oimi_queries as oq
import db_default_lists as dbdl
import db_extern_methods as dbem
import db_extern_methods_two as dbem2
cimport modify_default_lists as mdl
cimport database_controller_add_into as dca
cimport database_controller_add_into_part_5 as dca5
cimport database_controller_fix_regular_match as dcfr
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef add_new_Match_table(db_self, str difficulty, int num_of_games, str variety, str order_difficulty_games, str order_quantity_questions, str criteria_arrangement, int advisable_time, scenario, bint disposable):
	cdef:
		str new_id = db_self.assign_new_id('Matches')
		str search_str = "{0}, '{1}', '{2}', '{3}', '{4}', '{5}', {6}".format(num_of_games, difficulty, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, scenario)
		bint new_del = 0
	try:
		kind_previous_choosen = ''
		type_is_already_in_this_match = []
		print("entro nel try di add_new_Match")
		print("scenario ottenuto è {}".format(scenario))
		print("scenario ottenuto è {}".format(scenario))
		print("scenario ottenuto è {}".format(scenario))
		print("scenario ottenuto è {}".format(scenario))
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Match you want to insert already exist.')
				#raise Exception
				pass
		reload(oq)
		db_self.curs.execute('''INSERT INTO Matches (difficulty, num_of_games, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, advisable_time, scenario, disposable, token) 
			VALUES (?,?,?,?,?,?,?,?,?,?)''', (difficulty, num_of_games, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, advisable_time, scenario, disposable, new_id))
		db_self.conn.commit()
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Matches")
		num_last_line_m = db_self.curs.fetchone()
		print("num_last_line_m {}".format(num_last_line_m))
		if num_last_line_m:
			num_last_line_m = num_last_line_m[0]
			#num_last_line_m = num_last_line_m 
		else:
			num_last_line_m = 0
			print("ERRORE_GRAVISSIMOOOOOOOOOOOOOO")

		query_audio_intro = "UPDATE Matches SET audio_intro = '{}'".format('audio_1')
		query_audio_exit = "UPDATE Matches SET audio_exit = '{}'".format('audio_2')
		db_self.curs.execute(query_audio_intro)
		db_self.curs.execute(query_audio_exit)
		db_self.conn.commit()
		mdl.modify_Matches_list(difficulty, num_of_games, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, advisable_time, scenario, disposable, new_id, num_last_line_m)
		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
			print("ho trovato questa deletions_list!!!!")
			print("deletions_list = {}".format(db_self.deletions_list))
		if db_self.deletions_list[4]!=[0]:
			print("importa eccome!!!!!")
			new_del = 1
			dbem.bubblesort(db_self.deletions_list[4])
			db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Matches\'")
			index_to_replace = db_self.curs.fetchone()[1]
			index_corrected = db_self.deletions_list[4].pop(0)
			db_self.curs.execute('''UPDATE Matches SET match_id = ?  WHERE match_id = ?''', (index_corrected,index_to_replace))
			if not db_self.deletions_list[4]:
				db_self.deletions_list[4].append(0)
				filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
				with open(filename_pk, 'wb') as fi:
					pickle.dump(db_self.deletions_list, fi)

			db_self.conn.commit()
		else:
			print("non importa!!!!!")
		print("The new Match is now added.")
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last = db_self.curs.fetchone()[0]
		print("SONO ARRIVATO  A match_last {}".format(match_last))
		print("sta per partire con la caccia!!!! sta per partire con la caccia!!!! sta per partire con la caccia!!!! sta per partire con la caccia!!!! sta per partire con la caccia!!!!")
		list_kinds = []
		list_types = []
		listone_tipi = []

		if difficulty in ['Easy_1','Easy_2','Normal_1','Normal_2','Medium_1']:
			if variety=='SUNDRY':
				print("eccolo qua il sundry!!!!")
				db_self.curs.execute('''SELECT kind, type 
					FROM Questions 
						JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
						ON Scenarios_Questions.audio_id = Questions.audio
					WHERE scenario_id = ? AND value <= 3''',(scenario,))
				avail_ques = db_self.curs.fetchall()
				for i in range(len(avail_ques)):
					list_kinds.append(avail_ques[i][0])
					list_types.append(avail_ques[i][1])
				tot_ques_types = list(set(list_types))
				tot_ques_kinds = list(set(list_kinds))
				print("tot ques turno 1 T {}".format(tot_ques_types))
				print("tot ques turno 1 K {}".format(tot_ques_kinds))
				############choose avail questions according to value ############choose avail questions according to value ############choose avail questions according to value ############choose avail questions according to value
				if (scenario==db_self.suggested_scenario_from_t and db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==0 and db_self.imposed_type_game not in (tot_ques_types)) \
					or (scenario==db_self.suggested_scenario_from_k and db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==0 and db_self.imposed_kind_game not in (tot_ques_kinds)) \
					or (scenario==db_self.suggested_scenario_from_q and db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==0 and db_self.suggested_type_game_from_q not in (tot_ques_types)) \
					or (len(list_kinds)==0 or len(list_types)==0):
					print("PRIMA DI SCEGLIERE list_kinds AND list_types!!!! TURNO 2 SECONDA CHANCE")
					list_kinds = []
					list_types = []
					db_self.curs.execute('''SELECT kind, type FROM Questions JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) ON Scenarios_Questions.audio_id = Questions.audio WHERE scenario_id = ? AND value <= 4''',(scenario,))
					avail_ques = db_self.curs.fetchall()
					for i in range(len(avail_ques)):
						list_kinds.append(avail_ques[i][0])
						list_types.append(avail_ques[i][1])
					tot_ques_types = list(set(list_types))
					tot_ques_kinds = list(set(list_kinds))
					print("tot ques turno 2 K {}".format(tot_ques_kinds))
					print("tot ques turno 2 T {}".format(tot_ques_types))
					if (scenario==db_self.suggested_scenario_from_t and db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==0 and db_self.imposed_type_game not in (tot_ques_types)) \
						or (scenario==db_self.suggested_scenario_from_k and db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==0 and db_self.imposed_kind_game not in (tot_ques_kinds)) \
						or (scenario==db_self.suggested_scenario_from_q and db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==0 and db_self.suggested_type_game_from_q not in (tot_ques_types)) \
						or (len(list_kinds)==0 or len(list_types)==0):
						print("PRIMA DI SCEGLIERE list_kinds AND list_types!!!! TURNO 3 TERZA CHANCE")
						list_kinds = []
						list_types = []
						db_self.curs.execute('''SELECT kind, type FROM Questions JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) ON Scenarios_Questions.audio_id = Questions.audio WHERE scenario_id = ? AND value <= 5''',(scenario,))
						avail_ques = db_self.curs.fetchall()
						for i in range(len(avail_ques)):
							list_kinds.append(avail_ques[i][0])
							list_types.append(avail_ques[i][1])
						tot_ques_types = list(set(list_types))
						tot_ques_kinds = list(set(list_kinds))
						print("tot ques turno 3 K{}".format(tot_ques_kinds))
						print("tot ques turno 3 T {}".format(tot_ques_types))
						if (scenario==db_self.suggested_scenario_from_t and db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==0 and db_self.imposed_type_game not in (tot_ques_types)) \
							or (scenario==db_self.suggested_scenario_from_k and db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==0 and db_self.imposed_kind_game not in (tot_ques_kinds)) \
							or (scenario==db_self.suggested_scenario_from_q and db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==0 and db_self.suggested_type_game_from_q not in (tot_ques_types)) \
							or (len(list_kinds)==0 or len(list_types)==0):
							print("PRIMA DI SCEGLIERE list_kinds AND list_types!!!! TURNO 4 ULTIMA CHANCE")
							list_kinds = []
							list_types = []
							db_self.curs.execute('''SELECT kind, type FROM Questions 
									JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
									ON Scenarios_Questions.audio_id = Questions.audio
								WHERE scenario_id = ?''',(scenario,))
							avail_ques = db_self.curs.fetchall()
							for i in range(len(avail_ques)):
								list_kinds.append(avail_ques[i][0])
								list_types.append(avail_ques[i][1])
							print("tot ques turno 3 K {}".format(list_kinds))
							print("tot ques turno 3 T {}".format(list_types))
			else: #!=sundry --> single kind!
				print("non è sundry!!!!")
				print("CASO 1 SCELTA STASERA")
				list_kinds.append(variety)
				db_self.curs.execute('''SELECT DISTINCT type FROM Questions 
						JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
						ON Scenarios_Questions.audio_id = Questions.audio
					WHERE scenario_id = ? AND kind = ? AND value <= 4 LIMIT ?''',(scenario,variety,num_of_games,))
				avail_ques = db_self.curs.fetchall()
				list_types = [item for sublist in avail_ques for item in sublist]
				if (scenario==db_self.suggested_scenario_from_t and db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==0 and db_self.imposed_type_game not in (list_types)) \
					or (scenario==db_self.suggested_scenario_from_q and db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==0 and db_self.suggested_type_game_from_q not in (list_types)) \
					or (len(list_types)<num_of_games):
					print("CASO 2 SCELTA STASERA")
					db_self.curs.execute('''SELECT DISTINCT type FROM Questions 
						JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
						ON Scenarios_Questions.audio_id = Questions.audio
						WHERE scenario_id = ? AND kind = ? AND value <= 6 LIMIT ?''',(scenario,variety,num_of_games,))
					avail_ques = db_self.curs.fetchall()
					list_types = [item for sublist in avail_ques for item in sublist]
					if (scenario==db_self.suggested_scenario_from_t and db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==0 and db_self.imposed_type_game not in (list_types)) \
						or (scenario==db_self.suggested_scenario_from_q and db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==0 and db_self.suggested_type_game_from_q not in (list_types)) \
						or (len(list_types)<num_of_games):	
						print("CASO 3 SCELTA STASERA")
						db_self.curs.execute('''SELECT DISTINCT type FROM Questions 
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND kind = ? LIMIT ?''',(scenario,variety,num_of_games,))
						avail_ques = db_self.curs.fetchall()
						list_types = [item for sublist in avail_ques for item in sublist]
			print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			print(list_kinds)
			print(list_types)
			print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			#creo listone
			for dup in sorted(dbem2.list_duplicates(list_kinds)):
				listone_tipi.append(dup)
			print("listone_tipi!!! {}".format(listone_tipi))
			turn = 0
			original_right_kinds = list(set(list_kinds))
			right_kinds = original_right_kinds
			right_kinds.sort()
			print("]]]]]]]]]]]begin]]]]]]]")
			print("]]]]]]]]]]]]]]]]]]")
			print("suggested_scenario from kind!! {}".format(db_self.suggested_scenario_from_k))
			print("suggested_scenario from type!! {}".format(db_self.suggested_scenario_from_t))
			print("suggested_scenario from question!! {}".format(db_self.suggested_scenario_from_q))
			print("db_self.imposed_kind_game {}".format(db_self.imposed_kind_game))
			print("db_self.imposed_type_game {}".format(db_self.imposed_type_game))
			print("db_self.imposed_ques_id {}".format(db_self.imposed_ques_id))
			print("already_done_imposed_kind_game {}".format(db_self.already_done_imposed_kind_game))
			print("already_done_imposed_type_game {}".format(db_self.already_done_imposed_type_game))
			print("already_done_imposed_type_game {}".format(db_self.already_done_imposed_ask_ques))
			print("]]]]]]]]]]]]]]]]]]")
			print("]]]]]]]]]]]begin]]]]]]]")
			kind_previous_choosen = ''	
			questions_already_choosen_prev_match = []
			for nm in range(num_of_games):
				if (scenario==db_self.suggested_scenario_from_k and db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==0) and \
					(scenario==db_self.suggested_scenario_from_t and db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==0) and \
					(scenario==db_self.suggested_scenario_from_q and db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==0):
					specific_case = 33
				elif (scenario==db_self.suggested_scenario_from_k and db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==0) and \
					(scenario==db_self.suggested_scenario_from_t and db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==0) and \
					(scenario!=db_self.suggested_scenario_from_q or (db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==1)):
					specific_case = 12
				elif (scenario==db_self.suggested_scenario_from_k and db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==0) and \
					(scenario!=db_self.suggested_scenario_from_t or (db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==1)) and \
					(scenario==db_self.suggested_scenario_from_q and db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==0):
					specific_case = 13
				elif (scenario!=db_self.suggested_scenario_from_k or (db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==1)) and \
					(scenario==db_self.suggested_scenario_from_t and db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==0) and \
					(scenario==db_self.suggested_scenario_from_q and db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==0):
					specific_case = 23
				elif (scenario==db_self.suggested_scenario_from_k and db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==0) and \
					(scenario!=db_self.suggested_scenario_from_t or (db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==1)) and \
					(scenario!=db_self.suggested_scenario_from_q or (db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==1)):
					specific_case = 1
				elif (scenario!=db_self.suggested_scenario_from_k or (db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==1)) and \
					(scenario==db_self.suggested_scenario_from_t and db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==0) and \
					(scenario!=db_self.suggested_scenario_from_q or (db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==1)):
					specific_case = 2
				elif (scenario!=db_self.suggested_scenario_from_k or (db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==1)) and \
					(scenario!=db_self.suggested_scenario_from_t or (db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==1)) and \
					(scenario==db_self.suggested_scenario_from_q and db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==0):
					specific_case = 3				
				elif (scenario!=db_self.suggested_scenario_from_k or (db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==1)) and \
					(scenario!=db_self.suggested_scenario_from_t or (db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==1)) and \
					(scenario!=db_self.suggested_scenario_from_q or (db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==1)):
					specific_case = 0
				if variety=='SUNDRY':
					print("scenario sudicio = {}".format(scenario))
					print("NM GIRO NM {}".format(nm))
					print("dentor for che costruisco")
					########################################################################################## decide for num of questions
					if nm==0:
						num_of_questions = random.randint(1,3)
						if db_self.imposed_num_questions!=0 and db_self.already_done_imposed_num_questions==0:
							maybe_num = random.randint(0,1)
							if order_quantity_questions=='same' or maybe_num==1:
								num_of_questions = db_self.imposed_num_questions
								if kind_previous_choosen in ['3S','4P']:
									db_self.already_done_imposed_num_questions = 0
								else:
									stop_impo_num = random.randint(0,2)
									if stop_impo_num==0:
										db_self.already_done_imposed_num_questions = 1
									else:
										db_self.already_done_imposed_num_questions = 0
					else:
						print("prev_num_q is --> ",prev_num_q)
						if order_quantity_questions=='same':
							num_of_questions = prev_num_q
						if order_quantity_questions=='casu':
							num_of_questions = random.randint(1,3)
							if db_self.imposed_num_questions!=0 and db_self.already_done_imposed_num_questions==0:
								num_of_questions = db_self.imposed_num_questions
								print("kind_previous_choosen {}".format(kind_previous_choosen))
								if kind_previous_choosen in ['3S','4P']:
									db_self.already_done_imposed_num_questions = 0
								else:
									stop_impo_num = random.randint(0,1)
									if stop_impo_num==0:
										db_self.already_done_imposed_num_questions = 1
									else:
										db_self.already_done_imposed_num_questions = 0
						if order_quantity_questions=='asc':
							if db_self.imposed_num_questions!=0 and db_self.already_done_imposed_num_questions==0 and db_self.imposed_num_questions>=prev_num_q:
								num_of_questions = db_self.imposed_num_questions
								print("kind_previous_choosen {}".format(kind_previous_choosen))
								if kind_previous_choosen in ['3S','4P']:
									db_self.already_done_imposed_num_questions = 0
								else:
									stop_impo_num = random.randint(0,1)
									if stop_impo_num==0:
										db_self.already_done_imposed_num_questions = 1
									else:
										db_self.already_done_imposed_num_questions = 0
							else:
								maybe_plus = random.randint(0,3)
								if maybe_plus==0:
									num_of_questions = prev_num_q
								else:
									num_of_questions = prev_num_q + 1
								if num_of_questions>3:
									num_of_questions = 3
						if order_quantity_questions=='desc':
							if db_self.imposed_num_questions!=0 and db_self.already_done_imposed_num_questions==0 and db_self.imposed_num_questions<=prev_num_q:
								num_of_questions = db_self.imposed_num_questions
								print("kind_previous_choosen {}".format(kind_previous_choosen))
								if kind_previous_choosen in ['3S','4P']:
									db_self.already_done_imposed_num_questions = 0
								else:
									stop_impo_num = random.randint(0,1)
									if stop_impo_num==0:
										db_self.already_done_imposed_num_questions = 1
									else:
										db_self.already_done_imposed_num_questions = 0							
							maybe_min = random.randint(0,3)
							if maybe_min==0:
								num_of_questions = prev_num_q
							else:
								num_of_questions = prev_num_q - 1
							if num_of_questions<=0:
								num_of_questions = 1

					prev_num_q = num_of_questions
					print("num_of_questions for this game", num_of_questions)
					##########################################################################################						
					choose_miscellan = random.randint(0,2)
					if num_of_questions==1:
						choose_miscellan = 1
					##########################################################################################						
					print("####r#####r#r#####r##r#########################################################################")
					print("RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA ")
					print("scenario ", scenario)
					print("db_self.suggested_scenario_from_k ", db_self.suggested_scenario_from_k)
					print("db_self.imposed_kind_game ", db_self.imposed_kind_game)
					print("db_self.already_done_imposed_kind_game ", db_self.already_done_imposed_kind_game)
					print("db_self.suggested_scenario_from_t ", db_self.suggested_scenario_from_t)
					print("db_self.imposed_kind_game ", db_self.imposed_type_game)
					print("db_self.already_done_imposed_type_game ", db_self.already_done_imposed_type_game)
					print("db_self.suggested_scenario_from_q ", db_self.suggested_scenario_from_q)
					print("db_self.imposed_ques_id ", db_self.imposed_ques_id)
					print("db_self.already_done_imposed_ask_ques ", db_self.already_done_imposed_ask_ques)
					print("RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA RECA ")
					print("#######################r##r##r######r###r######################################################")
					jumped_already = 0
					if specific_case==33:
						print("specific_case deciso part_4 is {}".format(specific_case))
						if nm==0:
							if num_of_games==4:
								choose_one_to_do_33 = ['','start_from_kind','start_from_type','start_from_ques']
							if num_of_games==3:
								choose_one_to_do_33 = ['','start_from_kind','start_from_type']
							if num_of_games==2:
								choose_one_to_do_33 = ['start_from_type','start_from_ques']
							if num_of_games==1:
								choose_one_to_do_33 = ['start_from_ques']
						if nm==1: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_kind','start_from_type','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['','start_from_kind','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['','start_from_type','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [0,1,3]
							
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['','start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['','start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['']
							#####	
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_type','start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_kind','start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_type','start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [1,3]

							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['','start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['','start_from_kind']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['','start_from_ques']

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['']
							#####
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_type']
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']

							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_type']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_kind']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']

							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['']								
						if nm==2: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_type','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_kind','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_type','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [1,3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['','start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['','start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['']
							############
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']
							
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_kind']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['']								
						if nm==3: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = ['start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = ['']
						starting_from = random.choice(choose_one_to_do_33)
						if starting_from=='start_from_kind':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from kind!!!!")
								kind_previous_choosen = db_self.imposed_kind_game
								kind_game =  db_self.imposed_kind_game
								k_index = right_kinds.index(db_self.imposed_kind_game)
								print("k_index {}".format(k_index))
								bb = listone_tipi[k_index][1]
								print("kBB {}".format(bb))
								azz = list_types[bb[0]:bb[len(bb)-1]+1]
								print("kAZZZ!!! {}".format(azz))
								list_types_for_this_game = list(set(azz))
								print("klist_types AZZZ GIUSTO!!! {}".format(list_types_for_this_game))
								if len(list_types_for_this_game)>1:
									imposed_t = random.choices(list_types_for_this_game,k = 1)
									db_self.imposed_type_game_by_k = copy.copy(imposed_t[0])
								else:
									db_self.imposed_type_game_by_k = copy.copy(list_types_for_this_game[0])
									print("ktipo gioco FITTIZIO IMPOSTO!!!!")  
								type_game = db_self.imposed_type_game_by_k
								list_types_param=[]
								list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)
							   
								db_self.already_done_imposed_kind_game = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_type_game = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_q:
									db_self.already_done_imposed_ask_ques = 1			
								#####################################################################################################				    
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.imposed_type_game_by_k
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0
						elif starting_from=='start_from_type':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from TYPE!!!!")
								kind_game = dbem2.find_kind(db_self.imposed_type_game)
								choose_kind_type = list_types.index(db_self.imposed_type_game)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.imposed_type_game
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)	  
								db_self.already_done_imposed_type_game = 1
								if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_ask_ques = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_kind_game = 1
								#####################################################################################################					
								print("ho messo alredy KIND NORMAL2 done a 1!!!")
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0
						elif starting_from=='start_from_ques':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from question!!!!")
								kind_game = dbem2.find_kind(db_self.suggested_type_game_from_q)
								choose_kind_type = list_types.index(db_self.suggested_type_game_from_q)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.suggested_type_game_from_q
									list_types_param=[]
									list_types_param.append(type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
								db_self.already_done_imposed_ask_ques = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_q:
									db_self.already_done_imposed_kind_game = 1
								if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_type_game = 1
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0								
							#####################################################################################################						    
						elif starting_from=='':
							do_again_type = 1
							num_of_redo = 0
							correct_right_kinds_bef_dels = copy.copy(right_kinds)
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from NULL!!!!")
								print("uso scenario not not not not not imposed!!!")
								if criteria_arrangement=='regular':
									if len(original_right_kinds)>=num_of_games:
										differita = len(original_right_kinds) - num_of_games
										print("differita caso piu",differita)
										for i in range(differita):
											vediamo = random.randint(0,2)
											print("vediamo un po caso no type imposed!!! ", vediamo)
											if vediamo==0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
									print("regular case --- right_kinds AFTER!!!", right_kinds)
								if turn==0:
									if criteria_arrangement=='regular':
										choose_kind_type = 0
										print("choose_kind_type regular! == {}".format(choose_kind_type))
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type random! == {}".format(choose_kind_type))
								else:
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type RANDOM == {}".format(choose_kind_type))
									if criteria_arrangement=='regular':				
										if len(original_right_kinds)>=num_of_games:
											ranran = random.randint(0,2)
											print("ranran ranran ranran ranran ranran ranran ",ranran)
											if ranran!=0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
											choose_kind_type = 0
											print("choose_kind_type == ",choose_kind_type)
										else:
											range_possib = num_of_games-len(original_right_kinds)
											print("range_possib", range_possib)
											choose_kind_type = turn -1 
											print("INSIDIAINSIDIAINSIDIAINSIDIAINSIDIA ",choose_kind_type)
											if turn==range_possib:
												choose_kind_type = 0
												for q in range(turn-1):
													if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
														del(right_kinds[0])
														turn = 0
								kind_game = right_kinds[choose_kind_type]
								print("name_k ",kind_game)
								list_tmp = [p for p in list_types if kind_game in p]
								print("list_tmp is == ", list_tmp)
								choose_kind_type_from_tmp = random.randint(0,len(list_tmp)-1)
								print("choose_kind_type_from_tmp", choose_kind_type_from_tmp)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
									print("type_gametype_gametype_gametype_game ",type_game)
								else:
									print("caso non MISCE!!!! NON MISCENON MISCENON MISCENON MISCE")
									type_game = list_tmp[choose_kind_type_from_tmp]
									list_types_param = copy.copy(list_types)
									print("type_game", type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
									right_kinds = copy.copy(correct_right_kinds_bef_dels)
								else:
									do_again_type = 0
									num_of_redo = 0
					elif specific_case==12:
						print("specific_case deciso part_4 is {}".format(specific_case))
						if nm in [1,2] and num_of_games in [3,4] and jumped_already==0 and nm!=num_of_games:
							maybe_start = random.randint(0,2)
							if maybe_start==0:
								starting_from = ''
								jumped_already = 1
						if nm==0:
							if num_of_games==4:
								choose_one_to_do_12 = ['','start_from_kind','start_from_type']
							if num_of_games==3:
								choose_one_to_do_12 = ['','start_from_kind','start_from_type']
							if num_of_games==2:
								choose_one_to_do_12 = ['start_from_kind','start_from_type']
							if num_of_games==1:
								choose_one_to_do_12 = ['start_from_type']
						if nm==1: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['','start_from_kind','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['','start_from_kind','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['','start_from_kind']

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['','start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['']
							#####	
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['start_from_kind','start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_kind','start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['','start_from_kind']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['','start_from_kind']

							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['','start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['','start_from_kind']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['']

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['']

							#####
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:								
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['start_from_kind']

							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_kind']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:								
								choose_one_to_do_12 = ['']
							
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['']
						if nm==2: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['start_from_kind','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_kind','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['','start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['','start_from_kind']

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['','start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['','start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['']
							############
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['start_from_kind']
							
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_kind']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['']

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['']
						if nm==3: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['start_from_kind']

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = ['']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = ['']
						
						starting_from = random.choice(choose_one_to_do_12)
						if starting_from=='start_from_kind':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from kind!!!!")
								kind_previous_choosen = db_self.imposed_kind_game
								kind_game = db_self.imposed_kind_game
								print("kind_game in start_from_kind {}".format(kind_game))		 
								k_index = right_kinds.index(db_self.imposed_kind_game)
								print("k_index {}".format(k_index))

								bb = listone_tipi[k_index][1]
								print("kBB {}".format(bb))
								azz = list_types[bb[0]:bb[len(bb)-1]+1]
								print("kAZZZ!!! {}".format(azz))
								list_types_for_this_game = list(set(azz))
								print("klist_types AZZZ GIUSTO!!! {}".format(list_types_for_this_game))
								if len(list_types_for_this_game)>1:
									imposed_t = random.choices(list_types_for_this_game,k = 1)
									db_self.imposed_type_game_by_k = copy.copy(imposed_t[0])
								else:
									db_self.imposed_type_game_by_k = copy.copy(list_types_for_this_game[0])
									print("ktipo gioco FITTIZIO IMPOSTO!!!!")  
									type_game = db_self.imposed_type_game_by_k
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)
							   
								db_self.already_done_imposed_kind_game = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_type_game = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_q:
									db_self.already_done_imposed_ask_ques = 1								
								#####################################################################################################						    
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.imposed_type_game_by_k
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0
						elif starting_from=='start_from_type':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from TYPE!!!!")
								kind_game = dbem2.find_kind(db_self.imposed_type_game)
								choose_kind_type = list_types.index(db_self.imposed_type_game)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.imposed_type_game
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)	  
								db_self.already_done_imposed_type_game = 1
								if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_ask_ques = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_kind_game = 1
								print("ho messo alredy KIND NORMAL2 done a 1 MISCE !!!")
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0
								#####################################################################################################
						elif starting_from=='':
							do_again_type = 1
							num_of_redo = 0
							correct_right_kinds_bef_dels = copy.copy(right_kinds)
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from NULL!!!!")
								print("uso scenario not not not not not imposed!!!")
								if criteria_arrangement=='regular':
									if len(original_right_kinds)>=num_of_games:
										differita = len(original_right_kinds) - num_of_games
										print("differita caso piu",differita)
										for i in range(differita):
											vediamo = random.randint(0,2)
											print("vediamo un po caso no type imposed!!! ", vediamo)
											if vediamo==0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
									print("regular case --- right_kinds AFTER!!!", right_kinds)
								if turn==0:
									if criteria_arrangement=='regular':
										choose_kind_type = 0
										print("choose_kind_type regular! == {}".format(choose_kind_type))
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type random! == {}".format(choose_kind_type))
								else:
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type RANDOM == {}".format(choose_kind_type))
									if criteria_arrangement=='regular':				
										if len(original_right_kinds)>=num_of_games:
											ranran = random.randint(0,2)
											print("ranran ranran ranran ranran ranran ranran ",ranran)
											if ranran!=0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
											choose_kind_type = 0
											print("choose_kind_type == ",choose_kind_type)
										else:
											range_possib = num_of_games-len(original_right_kinds)
											print("range_possib", range_possib)
											choose_kind_type = turn -1 
											print("INSIDIAINSIDIAINSIDIAINSIDIAINSIDIA ",choose_kind_type)
											if turn==range_possib:
												choose_kind_type = 0
												for q in range(turn-1):
													if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
														del(right_kinds[0])
														turn = 0
								kind_game = right_kinds[choose_kind_type]
								print("name_k ",kind_game)
								list_tmp = [p for p in list_types if kind_game in p]
								print("list_tmp is == ", list_tmp)
								choose_kind_type_from_tmp = random.randint(0,len(list_tmp)-1)
								print("choose_kind_type_from_tmp", choose_kind_type_from_tmp)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
									print("type_gametype_gametype_gametype_game ",type_game)
								else:
									print("caso non MISCE!!!! NON MISCENON MISCENON MISCENON MISCE")
									type_game = list_tmp[choose_kind_type_from_tmp]
									list_types_param = copy.copy(list_types)
									print("type_game", type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
									right_kinds = copy.copy(correct_right_kinds_bef_dels)
								else:
									do_again_type = 0
									num_of_redo = 0
					elif specific_case==13:
						print("specific_case deciso part_4 is {}".format(specific_case))
						if nm in [1,2] and num_of_games in [3,4] and jumped_already==0 and nm!=num_of_games:
							maybe_start = random.randint(0,2)
							if maybe_start==0:
								starting_from = ''
								jumped_already = 1

						if nm==0:
							if num_of_games==4:
								choose_one_to_do_13 = ['','start_from_kind','start_from_ques']
							if num_of_games==3:
								choose_one_to_do_13 = ['','start_from_kind','start_from_ques']
							if num_of_games==2:
								choose_one_to_do_13 = ['start_from_kind','start_from_ques']
							if num_of_games==1:
								choose_one_to_do_13 = ['start_from_ques']
						if nm==1: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['','start_from_kind','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['','start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['','start_from_kind','start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['','start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['','start_from_ques']
							
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']
							#####	
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_kind','start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['','start_from_kind']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['','start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_kind','start_from_ques']

							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['start_from_kind','start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['','start_from_kind']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['','start_from_ques']

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']
							#####
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['start_from_kind']
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:								
								choose_one_to_do_13 = ['start_from_ques']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']

							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['start_from_kind']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']
							
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']
						if nm==2: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_kind','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['','start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_kind','start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['','start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['','start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']
							############
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['start_from_kind']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']
							
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['start_from_kind']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']
							
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']
						if nm==3: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['start_from_kind']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = ['start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = ['']

						starting_from = random.choice(choose_one_to_do_13)
						if starting_from=='start_from_kind':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from kind!!!!")
								kind_previous_choosen = db_self.imposed_kind_game
								kind_game = db_self.imposed_kind_game
								print("kind_game in start_from_kind {}".format(kind_game))		 
								k_index = right_kinds.index(db_self.imposed_kind_game)
								print("k_index {}".format(k_index))
								bb = listone_tipi[k_index][1]
								print("kBB {}".format(bb))
								azz = list_types[bb[0]:bb[len(bb)-1]+1]
								print("kAZZZ!!! {}".format(azz))
								list_types_for_this_game = list(set(azz))
								print("klist_types AZZZ GIUSTO!!! {}".format(list_types_for_this_game))
								if len(list_types_for_this_game)>1:
									imposed_t = random.choices(list_types_for_this_game,k = 1)
									db_self.imposed_type_game_by_k = copy.copy(imposed_t[0])
								else:
									db_self.imposed_type_game_by_k = copy.copy(list_types_for_this_game[0])
									print("ktipo gioco FITTIZIO IMPOSTO!!!!")  
									type_game = db_self.imposed_type_game_by_k
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)
							   
								db_self.already_done_imposed_kind_game = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_type_game = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_q:
									db_self.already_done_imposed_ask_ques = 1								
								#####################################################################################################				    
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.imposed_type_game_by_k
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0
						elif starting_from=='start_from_ques':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from question!!!!")
								kind_game = dbem2.find_kind(db_self.suggested_type_game_from_q)
								choose_kind_type = list_types.index(db_self.suggested_type_game_from_q)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.suggested_type_game_from_q
									list_types_param=[]
									list_types_param.append(type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
								db_self.already_done_imposed_ask_ques = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_q:
									db_self.already_done_imposed_kind_game = 1
								if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_type_game = 1
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0
							#####################################################################################################
						elif starting_from=='':
							do_again_type = 1
							num_of_redo = 0
							correct_right_kinds_bef_dels = copy.copy(right_kinds)
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from NULL!!!!")
								print("uso scenario not not not not not imposed!!!")
								if criteria_arrangement=='regular':
									if len(original_right_kinds)>=num_of_games:
										differita = len(original_right_kinds) - num_of_games
										print("differita caso piu",differita)
										for i in range(differita):
											vediamo = random.randint(0,2)
											print("vediamo un po caso no type imposed!!! ", vediamo)
											if vediamo==0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
									print("regular case --- right_kinds AFTER!!!", right_kinds)
								if turn==0:
									if criteria_arrangement=='regular':
										choose_kind_type = 0
										print("choose_kind_type regular! == {}".format(choose_kind_type))
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type random! == {}".format(choose_kind_type))
								else:
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type RANDOM == {}".format(choose_kind_type))
									if criteria_arrangement=='regular':				
										if len(original_right_kinds)>=num_of_games:
											ranran = random.randint(0,2)
											print("ranran ranran ranran ranran ranran ranran ",ranran)
											if ranran!=0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
											choose_kind_type = 0
											print("choose_kind_type == ",choose_kind_type)
										else:
											range_possib = num_of_games-len(original_right_kinds)
											print("range_possib", range_possib)
											choose_kind_type = turn -1 
											print("INSIDIAINSIDIAINSIDIAINSIDIAINSIDIA ",choose_kind_type)
											if turn==range_possib:
												choose_kind_type = 0
												for q in range(turn-1):
													if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
														del(right_kinds[0])
														turn = 0
								kind_game = right_kinds[choose_kind_type]
								print("name_k ",kind_game)
								list_tmp = [p for p in list_types if kind_game in p]
								print("list_tmp is == ", list_tmp)
								choose_kind_type_from_tmp = random.randint(0,len(list_tmp)-1)
								print("choose_kind_type_from_tmp", choose_kind_type_from_tmp)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
									print("type_gametype_gametype_gametype_game ",type_game)
								else:
									print("caso non MISCE!!!! NON MISCENON MISCENON MISCENON MISCE")
									type_game = list_tmp[choose_kind_type_from_tmp]
									list_types_param = copy.copy(list_types)
									print("type_game", type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
									##########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
									right_kinds = copy.copy(correct_right_kinds_bef_dels)
								else:
									do_again_type = 0
									num_of_redo = 0
					elif specific_case==23:
						print("specific_case deciso part_4 is {}".format(specific_case))
						if nm in [1,2] and num_of_games in [3,4] and jumped_already==0 and nm!=num_of_games:
							maybe_start = random.randint(0,2)
							if maybe_start==0:
								starting_from = ''
								jumped_already = 1
						if nm==0:
							if num_of_games==4:
								choose_one_to_do_23 = ['','start_from_type','start_from_ques']
							if num_of_games==3:
								choose_one_to_do_23 = ['','start_from_type','start_from_ques']
							if num_of_games==2:
								choose_one_to_do_23 = ['start_from_type','start_from_ques']
							if num_of_games==1:
								choose_one_to_do_23 = ['start_from_ques']
						if nm==1: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['','start_from_type','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['','start_from_type','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['','start_from_ques']
							
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['','start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']
							#####	
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_type','start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['','start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_type','start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['','start_from_ques']

							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['','start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['','start_from_ques']

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']
							#####
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['start_from_type']
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:								
								choose_one_to_do_23 = ['start_from_ques']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']

							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['start_from_ques']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['start_from_type']
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']

							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']
						if nm==2: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_type','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_type','start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['','start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['','start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['','start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']

							############
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']
							
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['start_from_type']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']
						if nm==3: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['start_from_type']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = ['start_from_ques']

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = ['']

						starting_from = random.choice(choose_one_to_do_23)
						if starting_from=='start_from_type':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from TYPE!!!!")
								kind_game = dbem2.find_kind(db_self.imposed_type_game)
								choose_kind_type = list_types.index(db_self.imposed_type_game)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.imposed_type_game
									db_self.already_done_imposed_type_game = copy.copy(1)							
									print("ho messo alredy KIND NORMAL2 done a 1!!!")
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)	  

								db_self.already_done_imposed_type_game = 1
								if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_ask_ques = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_kind_game = 1
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0
							#####################################################################################################							
						elif starting_from=='start_from_ques':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from question!!!!")
								kind_game = dbem2.find_kind(db_self.suggested_type_game_from_q)
								choose_kind_type = list_types.index(db_self.suggested_type_game_from_q)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.suggested_type_game_from_q
									list_types_param=[]
									list_types_param.append(type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
								db_self.already_done_imposed_ask_ques = 1
								if db_self.imposed_kind_game==db_self.suggested_kind_game_from_q:
									db_self.already_done_imposed_kind_game = 1
								if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
									db_self.already_done_imposed_type_game = 1
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0								
							#####################################################################################################

						elif starting_from=='':
							do_again_type = 1
							num_of_redo = 0
							correct_right_kinds_bef_dels = copy.copy(right_kinds)
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from NULL!!!!")
								print("uso scenario not not not not not imposed!!!")
								if criteria_arrangement=='regular':
									if len(original_right_kinds)>=num_of_games:
										differita = len(original_right_kinds) - num_of_games
										print("differita caso piu",differita)
										for i in range(differita):
											vediamo = random.randint(0,2)
											print("vediamo un po caso no type imposed!!! ", vediamo)
											if vediamo==0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
									print("regular case --- right_kinds AFTER!!!", right_kinds)
								if turn==0:
									if criteria_arrangement=='regular':
										choose_kind_type = 0
										print("choose_kind_type regular! == {}".format(choose_kind_type))
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type random! == {}".format(choose_kind_type))
								else:
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type RANDOM == {}".format(choose_kind_type))
									if criteria_arrangement=='regular':				
										if len(original_right_kinds)>=num_of_games:
											ranran = random.randint(0,2)
											print("ranran ranran ranran ranran ranran ranran ",ranran)
											if ranran!=0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
											choose_kind_type = 0
											print("choose_kind_type == ",choose_kind_type)
										else:
											range_possib = num_of_games-len(original_right_kinds)
											print("range_possib", range_possib)
											choose_kind_type = turn -1 
											print("INSIDIAINSIDIAINSIDIAINSIDIAINSIDIA ",choose_kind_type)
											if turn==range_possib:
												choose_kind_type = 0
												for q in range(turn-1):
													if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
														del(right_kinds[0])
														turn = 0
								kind_game = right_kinds[choose_kind_type]
								print("name_k ",kind_game)
								list_tmp = [p for p in list_types if kind_game in p]
								print("list_tmp is == ", list_tmp)
								choose_kind_type_from_tmp = random.randint(0,len(list_tmp)-1)
								print("choose_kind_type_from_tmp", choose_kind_type_from_tmp)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
									print("type_gametype_gametype_gametype_game ",type_game)
								else:
									print("caso non MISCE!!!! NON MISCENON MISCENON MISCENON MISCE")
									type_game = list_tmp[choose_kind_type_from_tmp]
									list_types_param = copy.copy(list_types)
									print("type_game", type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
									###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
									right_kinds = copy.copy(correct_right_kinds_bef_dels)
								else:
									do_again_type = 0
									num_of_redo = 0
					elif specific_case==1:
						print("specific_case deciso part_4 is {}".format(specific_case))
						if nm in [1,2] and num_of_games in [3,4] and jumped_already==0 and nm!=num_of_games:
							maybe_start = random.randint(0,2)
							if maybe_start==0:
								starting_from = 'start_from_kind'
							else:
								starting_from = ''
								jumped_already = 1
						else:
							starting_from = 'start_from_kind'

						if starting_from=='start_from_kind':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from kind!!!!")
								kind_previous_choosen = db_self.imposed_kind_game
								kind_game = db_self.imposed_kind_game
								print("kind_game in start_from_kind {}".format(kind_game))
								k_index = right_kinds.index(db_self.imposed_kind_game)
								print("k_index {}".format(k_index))
								bb = listone_tipi[k_index][1]
								print("kBB {}".format(bb))
								azz = list_types[bb[0]:bb[len(bb)-1]+1]
								print("kAZZZ!!! {}".format(azz))
								list_types_for_this_game = list(set(azz))
								print("klist_types AZZZ GIUSTO!!! {}".format(list_types_for_this_game))
								if len(list_types_for_this_game)>1:
									imposed_t = random.choices(list_types_for_this_game,k = 1)
									db_self.imposed_type_game_by_k = copy.copy(imposed_t[0])
								else:
									db_self.imposed_type_game_by_k = copy.copy(list_types_for_this_game[0])
									print("ktipo gioco FITTIZIO IMPOSTO!!!!")
									type_game = db_self.imposed_type_game_by_k
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)
							   
								db_self.already_done_imposed_kind_game = copy.copy(1)					    

								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.imposed_type_game_by_k
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0
						elif starting_from=='':
							do_again_type = 1
							num_of_redo = 0
							correct_right_kinds_bef_dels = copy.copy(right_kinds)
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from NULL!!!!")
								print("uso scenario not not not not not imposed!!!")
								if criteria_arrangement=='regular':
									if len(original_right_kinds)>=num_of_games:
										differita = len(original_right_kinds) - num_of_games
										print("differita caso piu",differita)
										for i in range(differita):
											vediamo = random.randint(0,2)
											print("vediamo un po caso no type imposed!!! ", vediamo)
											if vediamo==0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
									print("regular case --- right_kinds AFTER!!!", right_kinds)
								if turn==0:
									if criteria_arrangement=='regular':
										choose_kind_type = 0
										print("choose_kind_type regular! == {}".format(choose_kind_type))
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type random! == {}".format(choose_kind_type))
								else:
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type RANDOM == {}".format(choose_kind_type))
									if criteria_arrangement=='regular':				
										if len(original_right_kinds)>=num_of_games:
											ranran = random.randint(0,2)
											print("ranran ranran ranran ranran ranran ranran ",ranran)
											if ranran!=0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
											choose_kind_type = 0
											print("choose_kind_type == ",choose_kind_type)
										else:
											range_possib = num_of_games-len(original_right_kinds)
											print("range_possib", range_possib)
											choose_kind_type = turn -1 
											print("INSIDIAINSIDIAINSIDIAINSIDIAINSIDIA ",choose_kind_type)
											if turn==range_possib:
												choose_kind_type = 0
												for q in range(turn-1):
													if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
														del(right_kinds[0])
														turn = 0
								kind_game = right_kinds[choose_kind_type]
								print("name_k ",kind_game)
								list_tmp = [p for p in list_types if kind_game in p]
								print("list_tmp is == ", list_tmp)
								choose_kind_type_from_tmp = random.randint(0,len(list_tmp)-1)
								print("choose_kind_type_from_tmp", choose_kind_type_from_tmp)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
									print("type_gametype_gametype_gametype_game ",type_game)
								else:
									print("caso non MISCE!!!! NON MISCENON MISCENON MISCENON MISCE")
									type_game = list_tmp[choose_kind_type_from_tmp]
									list_types_param = copy.copy(list_types)
									print("type_game", type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
									right_kinds = copy.copy(correct_right_kinds_bef_dels)
								else:
									do_again_type = 0
									num_of_redo = 0
					elif specific_case==2:
						print("specific_case deciso part_4 is {}".format(specific_case))
						if nm in [1,2] and num_of_games in [3,4] and jumped_already==0 and nm!=num_of_games:
							maybe_start = random.randint(0,2)
							if maybe_start==0:
								starting_from = 'start_from_type'
							else:
								starting_from = ''
								jumped_already = 1
						else:
							starting_from = 'start_from_type'								

						if starting_from=='start_from_type':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from TYPE!!!!")
								kind_game = dbem2.find_kind(db_self.imposed_type_game)
								choose_kind_type = list_types.index(db_self.imposed_type_game)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
									db_self.already_done_imposed_type_game = copy.copy(1)							
									print("ho messo alredy KIND NORMAL2 done a 1 MISCE !!!")			    
								else:
									type_game = db_self.imposed_type_game
									db_self.already_done_imposed_type_game = copy.copy(1)
									print("ho messo alredy KIND NORMAL2 done a 1!!!")
									list_types_param=[]
									list_types_param.append(type_game)
								if type_game=='':
									print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
									type_game = list_types[choose_kind_type]
									list_types_param = copy.copy(list_types)	  
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0
						elif starting_from=='':
							do_again_type = 1
							num_of_redo = 0
							correct_right_kinds_bef_dels = copy.copy(right_kinds)
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from NULL!!!!")
								print("uso scenario not not not not not imposed!!!")
								if criteria_arrangement=='regular':
									if len(original_right_kinds)>=num_of_games:
										differita = len(original_right_kinds) - num_of_games
										print("differita caso piu",differita)
										for i in range(differita):
											vediamo = random.randint(0,2)
											print("vediamo un po caso no type imposed!!! ", vediamo)
											if vediamo==0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
									print("regular case --- right_kinds AFTER!!!", right_kinds)
								if turn==0:
									if criteria_arrangement=='regular':
										choose_kind_type = 0
										print("choose_kind_type regular! == {}".format(choose_kind_type))
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type random! == {}".format(choose_kind_type))
								else:
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type RANDOM == {}".format(choose_kind_type))
									if criteria_arrangement=='regular':				
										if len(original_right_kinds)>=num_of_games:
											ranran = random.randint(0,2)
											print("ranran ranran ranran ranran ranran ranran ",ranran)
											if ranran!=0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
											choose_kind_type = 0
											print("choose_kind_type == ",choose_kind_type)
										else:
											range_possib = num_of_games-len(original_right_kinds)
											print("range_possib", range_possib)
											choose_kind_type = turn -1 
											print("INSIDIAINSIDIAINSIDIAINSIDIAINSIDIA ",choose_kind_type)
											if turn==range_possib:
												choose_kind_type = 0
												for q in range(turn-1):
													if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
														del(right_kinds[0])
														turn = 0
								kind_game = right_kinds[choose_kind_type]
								print("name_k ",kind_game)
								list_tmp = [p for p in list_types if kind_game in p]
								print("list_tmp is == ", list_tmp)
								choose_kind_type_from_tmp = random.randint(0,len(list_tmp)-1)
								print("choose_kind_type_from_tmp", choose_kind_type_from_tmp)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
									print("type_gametype_gametype_gametype_game ",type_game)
								else:
									print("caso non MISCE!!!! NON MISCENON MISCENON MISCENON MISCE")
									type_game = list_tmp[choose_kind_type_from_tmp]
									list_types_param = copy.copy(list_types)
									print("type_game", type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
									right_kinds = copy.copy(correct_right_kinds_bef_dels)
								else:
									do_again_type = 0
									num_of_redo = 0
					elif specific_case==3:
						print("specific_case deciso part_4 is {}".format(specific_case))
						if nm in [1,2] and num_of_games in [3,4] and jumped_already==0 and nm!=num_of_games:
							maybe_start = random.randint(0,2)
							if maybe_start==0:
								starting_from = 'start_from_ques'
							else:
								starting_from = ''
								jumped_already = 1
						else:
							starting_from = 'start_from_ques'
						
						if starting_from=='start_from_ques':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from question!!!!")
								kind_game = dbem2.find_kind(db_self.suggested_type_game_from_q)
								choose_kind_type = list_types.index(db_self.suggested_type_game_from_q)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
								else:
									type_game = db_self.suggested_type_game_from_q
									list_types_param=[]
									list_types_param.append(type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
								db_self.already_done_imposed_ask_ques = copy.copy(1)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0
								###########################################################################
						elif starting_from=='':
							do_again_type = 1
							num_of_redo = 0
							correct_right_kinds_bef_dels = copy.copy(right_kinds)
							while do_again_type>0:
								print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from NULL!!!!")
								print("uso scenario not not not not not imposed!!!")
								if criteria_arrangement=='regular':
									if len(original_right_kinds)>=num_of_games:
										differita = len(original_right_kinds) - num_of_games
										print("differita caso piu",differita)
										for i in range(differita):
											vediamo = random.randint(0,2)
											print("vediamo un po caso no type imposed!!! ", vediamo)
											if vediamo==0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
									print("regular case --- right_kinds AFTER!!!", right_kinds)
								if turn==0:
									if criteria_arrangement=='regular':
										choose_kind_type = 0
										print("choose_kind_type regular! == {}".format(choose_kind_type))
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type random! == {}".format(choose_kind_type))
								else:
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type RANDOM == {}".format(choose_kind_type))
									if criteria_arrangement=='regular':				
										if len(original_right_kinds)>=num_of_games:
											ranran = random.randint(0,2)
											print("ranran ranran ranran ranran ranran ranran ",ranran)
											if ranran!=0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
											choose_kind_type = 0
											print("choose_kind_type == ",choose_kind_type)
										else:
											range_possib = num_of_games-len(original_right_kinds)
											print("range_possib", range_possib)
											choose_kind_type = turn -1 
											print("INSIDIAINSIDIAINSIDIAINSIDIAINSIDIA ",choose_kind_type)
											if turn==range_possib:
												choose_kind_type = 0
												for q in range(turn-1):
													if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
														del(right_kinds[0])
														turn = 0
								kind_game = right_kinds[choose_kind_type]
								print("name_k ",kind_game)
								list_tmp = [p for p in list_types if kind_game in p]
								print("list_tmp is == ", list_tmp)
								choose_kind_type_from_tmp = random.randint(0,len(list_tmp)-1)
								print("choose_kind_type_from_tmp", choose_kind_type_from_tmp)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
									print("type_gametype_gametype_gametype_game ",type_game)
								else:
									print("caso non MISCE!!!! NON MISCENON MISCENON MISCENON MISCE")
									type_game = list_tmp[choose_kind_type_from_tmp]
									list_types_param = copy.copy(list_types)
									print("type_game", type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
									right_kinds = copy.copy(correct_right_kinds_bef_dels)
								else:
									do_again_type = 0
									num_of_redo = 0
					elif specific_case==0:
						print("specific_case deciso part_4 is {}".format(specific_case))
						print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!starting from NULL!!!!")
						starting_from = ''
						if starting_from=='':
							do_again_type = 1
							num_of_redo = 0
							while do_again_type>0:
								print("uso scenario not not not not not imposed!!!")
								if criteria_arrangement=='regular':
									if len(original_right_kinds)>=num_of_games:
										differita = len(original_right_kinds) - num_of_games
										print("differita caso piu",differita)
										for i in range(differita):
											vediamo = random.randint(0,2)
											print("vediamo un po caso no type imposed!!! ", vediamo)
											if vediamo==0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
									print("regular case --- right_kinds AFTER!!!", right_kinds)
								if turn==0:
									if criteria_arrangement=='regular':
										choose_kind_type = 0
										print("choose_kind_type regular! == {}".format(choose_kind_type))
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type random! == {}".format(choose_kind_type))
								else:
									if criteria_arrangement=='random':
										choose_kind_type = random.randint(0,len(right_kinds)-1)
										print("choose_kind_type RANDOM == {}".format(choose_kind_type))
									if criteria_arrangement=='regular':				
										if len(original_right_kinds)>=num_of_games:
											ranran = random.randint(0,2)
											print("ranran ranran ranran ranran ranran ranran ",ranran)
											if ranran!=0:
												if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
													del(right_kinds[0])
											choose_kind_type = 0
											print("choose_kind_type == ",choose_kind_type)
										else:
											range_possib = num_of_games-len(original_right_kinds)
											print("range_possib", range_possib)
											choose_kind_type = turn -1 
											print("INSIDIAINSIDIAINSIDIAINSIDIAINSIDIA ",choose_kind_type)
											if turn==range_possib:
												choose_kind_type = 0
												for q in range(turn-1):
													if right_kinds[0]!=db_self.imposed_kind_game or db_self.already_done_imposed_kind_game==1:
														del(right_kinds[0])
														turn = 0
								kind_game = right_kinds[choose_kind_type]
								print("name_k ",kind_game)
								list_tmp = [p for p in list_types if kind_game in p]
								print("list_tmp is == ", list_tmp)
								choose_kind_type_from_tmp = random.randint(0,len(list_tmp)-1)
								print("choose_kind_type_from_tmp", choose_kind_type_from_tmp)
								if choose_miscellan==0:
									lsk = list(set(list_kinds))
									lsk = sorted(lsk)
									print("lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk lsk {}".format(lsk))
									lsk_index = lsk.index(kind_game)
									print("lsk_index {}".format(lsk_index))
									type_game = 'MISCELLANEOUS'
									bb = listone_tipi[lsk_index][1]
									print("BB {}".format(bb))
									azz = list_types[bb[0]:bb[len(bb)-1]+1]
									print("AZZZ!!! {}".format(azz))
									list_types_param = list(set(azz))
									print("list_types AZZZ GIUSTO!!! {}".format(list_types_param))
									if len(list_types_param)==1:
										type_game = list_types_param[0]
									print("type_gametype_gametype_gametype_game ",type_game)
									#PER ADESSO C'È SEMPRE MA METTO ANCHE UN CASO IN CUI SI PUO RIPETERE!!!
									if scenario==db_self.suggested_scenario_from_k and db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==0:
										db_self.already_done_imposed_kind_game = copy.copy(1)	
										print("ho messo KIND alredy done a 1!!!")
								else:
									print("caso non MISCE!!!! NON MISCENON MISCENON MISCENON MISCE")
									type_game = list_tmp[choose_kind_type_from_tmp]
									list_types_param = copy.copy(list_types)
									print("type_game", type_game)
									if type_game=='':
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										print("CASO STRANO!! SCENARIO SCELTO A CASO È PROPRIO QUELLO CHE NON VOLEVO!! QUINDI TYPE_GAME DEVE ESISTERE CMQ!!!")
										type_game = list_types[choose_kind_type]
										list_types_param = copy.copy(list_types)
								###########################################################################
								if type_game in type_is_already_in_this_match and num_of_redo<=2:
									print("REDO EXACT TYPE CHOOSEN BEFORE IN THIS MATCH")
									num_of_redo+=1
								else:
									do_again_type = 0
									num_of_redo = 0									
				#########################################################################################################################																
				else: #not sundry
					case_inside = 99
					print("SCELGO NOT SUNDRY!!!! CHE SUCCEDE ADESSOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")
					print("specific_case ==> specific_case {} ".format(specific_case))
					if specific_case==33:
						if nm==0:
							if num_of_games==4:
								choose_one_to_do_33 = [0,1,2,3]
							if num_of_games==3:
								choose_one_to_do_33 = [1,2,3]
							if num_of_games==2:
								choose_one_to_do_33 = [2,3]
							if num_of_games==1:
								choose_one_to_do_33 = [3]
						if nm==1: 
							#############################################riparto da qui mettendo gli already pronti anchge qui e tutti i casi in cui ho metsso piu di um already contemporanemante perche cmq ne aggiorno 
							#piu di uno se sono stesso kind e tipo dello stesso kind 
							################# e devi fare lo stesso per caso sundry!! aggiungi tutto!!! ---> copio per tutti gli specific case e anche in part_5!!!!
							###########################################################################################################################################################################
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [1,2,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0,1,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [0,2,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [0,1,3]
							
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [0,3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0]
							#####	
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [2,3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [1,2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [2,3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [1,3]

							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0,2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0,1]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [0,3]

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0]
							#####
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [2]
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]

							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [2]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [1]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]

							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0]								
						if nm==2: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [2,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [1,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [2,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [1,3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [0,3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0]
							############
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]
							
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [1]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0]								
						if nm==3: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_33 = [3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_33 = [0]
						###########################################################################################################################################################################
						case_inside = random.choice(choose_one_to_do_33)
						misce_fix = random.randint(0,1)
						kind_game = list_kinds[0]
						if case_inside==0:
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								type_game = list_types[turned]
							else:		    
								type_game = 'MISCELLANEOUS'
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								spare_type_game = list_types[turned]
						if case_inside==1:
							db_self.already_done_imposed_kind_game = 1
							if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_type_game = 1
							if db_self.imposed_kind_game==db_self.suggested_kind_game_from_q:
								db_self.already_done_imposed_ask_ques = 1								
							#####################################################################################################
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								type_game = list_types[turned]
							else:		    
								type_game = 'MISCELLANEOUS'				
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								spare_type_game = list_types[turned]
						if case_inside==2:
							db_self.already_done_imposed_type_game = 1
							if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_ask_ques = 1
							if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_kind_game = 1
							#####################################################################################################
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if variety==dbem2.find_kind(db_self.imposed_type_game):
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
										type_game = db_self.imposed_type_game
								else:		    
									type_game = 'MISCELLANEOUS'				
									spare_type_game = db_self.imposed_type_game
							else:
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									type_game = list_types[turned]							
								else:
									type_game = 'MISCELLANEOUS'				
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									spare_type_game = list_types[turned]
						if case_inside==3:
							db_self.already_done_imposed_ask_ques = 1
							if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_type_game = 1
							if db_self.suggested_kind_game_from_q==db_self.imposed_kind_game:
								db_self.already_done_imposed_kind_game = 1
							#####################################################################################################							
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if variety==db_self.suggested_kind_game_from_q:
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
									type_game = db_self.suggested_type_game_from_q
								else:
									type_game = 'MISCELLANEOUS'
									spare_type_game = db_self.suggested_type_game_from_q
							else:	    
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									type_game = list_types[turned]	
								else:
									type_game = 'MISCELLANEOUS'
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									spare_type_game = list_types[turned]
					#####	
					elif specific_case==12:
						if nm==0:
							if num_of_games==4:
								choose_one_to_do_12 = [0,1,2]
							if num_of_games==3:
								choose_one_to_do_12 = [0,1,2]
							if num_of_games==2:
								choose_one_to_do_12 = [1,2]
							if num_of_games==1:
								choose_one_to_do_12 = [2]
						if nm==1: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0,1,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0,1,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0,1]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0]
							#####	
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [1,2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [1,2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0,1]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0,1]

							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0,2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0,1]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0]

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0]

							#####
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [2]
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [2]
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:								
								choose_one_to_do_12 = [2]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [1]

							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [2]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [1]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:								
								choose_one_to_do_12 = [0]
							
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0]
						if nm==2: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [1,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [1,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0,1]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0]
							############
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [1]
							
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [1]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0]

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0]
						if nm==3: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [1]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_12 = [0]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_12 = [0]
						
						kind_game = list_kinds[0]
						misce_fix = random.randint(0,1)						
						case_inside = random.choice(choose_one_to_do_12)
						if case_inside==0:
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								type_game = list_types[turned]
							else:		    
								type_game = 'MISCELLANEOUS'
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								spare_type_game = list_types[turned]	
						if case_inside==1:
							db_self.already_done_imposed_kind_game = 1
							if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_type_game = 1
							if db_self.imposed_kind_game==db_self.suggested_kind_game_from_q:
								db_self.already_done_imposed_ask_ques = 1								
							#####################################################################################################
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)							
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								type_game = list_types[turned]
							else:		    
								type_game = 'MISCELLANEOUS'				
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								spare_type_game = list_types[turned]
						if case_inside==2:
							db_self.already_done_imposed_type_game = 1
							if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_ask_ques = 1
							if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_kind_game = 1
							#####################################################################################################
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if variety==dbem2.find_kind(db_self.imposed_type_game):
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
										type_game = db_self.imposed_type_game
								else:		    
									type_game = 'MISCELLANEOUS'				
									spare_type_game = db_self.imposed_type_game
							else:
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									type_game = list_types[turned]							
								else:
									type_game = 'MISCELLANEOUS'				
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									spare_type_game = list_types[turned]
					#####
					elif specific_case==13:
						if nm==0:
							if num_of_games==4:
								choose_one_to_do_13 = [0,1,3]
							if num_of_games==3:
								choose_one_to_do_13 = [0,1,3]
							if num_of_games==2:
								choose_one_to_do_13 = [1,3]
							if num_of_games==1:
								choose_one_to_do_13 = [3]
						if nm==1: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [0,1,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [0,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [0,1,3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [0,3]
							
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
							#####	
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [1,3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0,1]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [0,3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [1,3]

							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [1,3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0,1]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [0,3]

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
							#####
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [1]
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:								
								choose_one_to_do_13 = [3]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]

							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [1]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]
							
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
						if nm==2: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [1,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [0,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [1,3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [0,3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
							############
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [1]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]
							
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [1]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]
							
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
						if nm==3: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_13 = [3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_13 = [0]
						kind_game = list_kinds[0]
						misce_fix = random.randint(0,1)
						case_inside = random.choice(choose_one_to_do_13)
						if case_inside==0:
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								type_game = list_types[turned]
							else:		    
								type_game = 'MISCELLANEOUS'
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								spare_type_game = list_types[turned]	
						if case_inside==1:
							db_self.already_done_imposed_kind_game = 1
							if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_type_game = 1
							if db_self.imposed_kind_game==db_self.suggested_kind_game_from_q:
								db_self.already_done_imposed_ask_ques = 1								
							#####################################################################################################
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								type_game = list_types[turned]
							else:		    
								type_game = 'MISCELLANEOUS'				
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								spare_type_game = list_types[turned]
						if case_inside==3:
							db_self.already_done_imposed_ask_ques = 1
							if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_type_game = 1
							if db_self.suggested_kind_game_from_q==db_self.imposed_kind_game:
								db_self.already_done_imposed_kind_game = 1
							#####################################################################################################
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if variety==db_self.suggested_kind_game_from_q:
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
									type_game = db_self.suggested_type_game_from_q
								else:
									type_game = 'MISCELLANEOUS'
									spare_type_game = db_self.suggested_type_game_from_q
							else:	    
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									type_game = list_types[turned]	
								else:
									type_game = 'MISCELLANEOUS'
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									spare_type_game = list_types[turned]							
					######
					elif specific_case==23:
						if nm==0:
							if num_of_games==4:
								choose_one_to_do_23 = [0,2,3]
							if num_of_games==3:
								choose_one_to_do_23 = [0,2,3]
							if num_of_games==2:
								choose_one_to_do_23 = [2,3]
							if num_of_games==1:
								choose_one_to_do_23 = [3]
						if nm==1: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [0,2,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [0,2,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [0,3]
							
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0,1]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [0,3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]
							#####	
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [2,3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0,2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [2,3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [0,3]

							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0,2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [0,3]

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]
							#####
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [2]
							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:								
								choose_one_to_do_23 = [3]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]

							if num_of_games==2 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [3]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [2]
							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]

							if num_of_games==2 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]
						if nm==2: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [2,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [2,3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [0,3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0,2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [0,3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]

							############
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]
							
							if num_of_games==3 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [2]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]
							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]

							if num_of_games==3 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]
						if nm==3: 
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==0 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [2]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==0 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]
							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==0:
								choose_one_to_do_23 = [3]

							if num_of_games==4 and db_self.already_done_imposed_type_game==1 and db_self.already_done_imposed_kind_game==1 and db_self.already_done_imposed_num_questions==1:
								choose_one_to_do_23 = [0]
						kind_game = list_kinds[0]
						misce_fix = random.randint(0,1)
						case_inside = random.choice(choose_one_to_do_23)
						if case_inside==0:
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								type_game = list_types[turned]
							else:		    
								type_game = 'MISCELLANEOUS'
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								spare_type_game = list_types[turned]	
						if case_inside==2:
							db_self.already_done_imposed_type_game = 1
							if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_ask_ques = 1
							if db_self.imposed_kind_game==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_kind_game = 1
							#####################################################################################################
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if variety==dbem2.find_kind(db_self.imposed_type_game):
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
										type_game = db_self.imposed_type_game
								else:		    
									type_game = 'MISCELLANEOUS'				
									spare_type_game = db_self.imposed_type_game
							else:
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									type_game = list_types[turned]							
								else:
									type_game = 'MISCELLANEOUS'				
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									spare_type_game = list_types[turned]			
						if case_inside==3:
							db_self.already_done_imposed_ask_ques = 1
							if db_self.suggested_kind_game_from_q==db_self.suggested_kind_game_from_t:
								db_self.already_done_imposed_type_game = 1
							if db_self.suggested_kind_game_from_q==db_self.imposed_kind_game:
								db_self.already_done_imposed_kind_game = 1
							#####################################################################################################
							list_types_param = copy.copy(list_types)
							num_of_questions = random.randint(1,4)
							if variety==db_self.suggested_kind_game_from_q:
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
									type_game = db_self.suggested_type_game_from_q
								else:
									type_game = 'MISCELLANEOUS'
									spare_type_game = db_self.suggested_type_game_from_q
							else:	    
								if num_of_questions==1:
									misce_fix=1
								if misce_fix==1:
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									type_game = list_types[turned]	
								else:
									type_game = 'MISCELLANEOUS'
									turned = random.randint(0,len(list_types)-1)
									print("tunn caso nuovo!!".format(turned))
									spare_type_game = list_types[turned]
					######
					if specific_case==0:
						kind_game = list_kinds[0]
						misce_fix = random.randint(0,1)
						list_types_param = copy.copy(list_types)
						num_of_questions = random.randint(1,4)
						if num_of_questions==1:
							misce_fix=1
						if misce_fix==1:
							turned = random.randint(0,len(list_types)-1)
							print("tunn caso nuovo!!".format(turned))
							type_game = list_types[turned]
						else:		    
							type_game = 'MISCELLANEOUS'
							turned = random.randint(0,len(list_types)-1)
							print("tunn caso nuovo!!".format(turned))
							spare_type_game = list_types[turned]							
					######
					if specific_case==1:
						db_self.already_done_imposed_kind_game = copy.copy(1)
						kind_game = list_kinds[0]
						misce_fix = random.randint(0,1)
						list_types_param = copy.copy(list_types)
						num_of_questions = random.randint(1,4)
						if num_of_questions==1:
							misce_fix=1
						if misce_fix==1:
							turned = random.randint(0,len(list_types)-1)
							print("tunn caso nuovo!!".format(turned))
							type_game = list_types[turned]
						else:		    
							type_game = 'MISCELLANEOUS'
					######
					if specific_case==2:
						db_self.already_done_imposed_type_game = copy.copy(1)
						kind_game = list_kinds[0]
						misce_fix = random.randint(0,1)
						list_types_param = copy.copy(list_types)
						num_of_questions = random.randint(1,4)
						if variety==dbem2.find_kind(db_self.imposed_type_game):
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
									type_game = db_self.imposed_type_game
							else:		    
								type_game = 'MISCELLANEOUS'				
								spare_type_game = db_self.imposed_type_game
						else:
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								type_game = list_types[turned]							
							else:
								type_game = 'MISCELLANEOUS'				
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								spare_type_game = list_types[turned]							
					######
					if specific_case==3:
						db_self.already_done_imposed_ask_ques = copy.copy(1)
						kind_game = list_kinds[0]
						misce_fix = random.randint(0,1)
						list_types_param = copy.copy(list_types)
						num_of_questions = random.randint(1,4)
						if variety==db_self.suggested_kind_game_from_q:
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
								type_game = db_self.suggested_type_game_from_q
							else:
								type_game = 'MISCELLANEOUS'
								spare_type_game = db_self.suggested_type_game_from_q
						else:	    
							if num_of_questions==1:
								misce_fix=1
							if misce_fix==1:
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								type_game = list_types[turned]	
							else:
								type_game = 'MISCELLANEOUS'
								turned = random.randint(0,len(list_types)-1)
								print("tunn caso nuovo!!".format(turned))
								spare_type_game = list_types[turned]							
					#############################################################################################################################################
					########################################################################################## redo decide for num of questions also for not sundry
					if nm==0:
						num_of_questions = random.randint(1,3)
						if db_self.imposed_num_questions!=0 and db_self.already_done_imposed_num_questions==0:
							maybe_num = random.randint(0,1)
							if order_quantity_questions=='same' or maybe_num==1:
								num_of_questions = db_self.imposed_num_questions
								if kind_previous_choosen in ['3S','4P']:
									db_self.already_done_imposed_num_questions = 0
								else:
									stop_impo_num = random.randint(0,2)
									if stop_impo_num==0:
										db_self.already_done_imposed_num_questions = 1
									else:
										db_self.already_done_imposed_num_questions = 0
					else:
						print("prev_num_q is --> ",prev_num_q)
						if order_quantity_questions=='same':
							num_of_questions = prev_num_q
						if order_quantity_questions=='casu':
							num_of_questions = random.randint(1,3)
							if db_self.imposed_num_questions!=0 and db_self.already_done_imposed_num_questions==0:
								num_of_questions = db_self.imposed_num_questions
								print("kind_previous_choosen {}".format(kind_previous_choosen))
								if kind_previous_choosen in ['3S','4P']:
									db_self.already_done_imposed_num_questions = 0
								else:
									stop_impo_num = random.randint(0,1)
									if stop_impo_num==0:
										db_self.already_done_imposed_num_questions = 1
									else:
										db_self.already_done_imposed_num_questions = 0
						if order_quantity_questions=='asc':
							if db_self.imposed_num_questions!=0 and db_self.already_done_imposed_num_questions==0 and db_self.imposed_num_questions>=prev_num_q:
								num_of_questions = db_self.imposed_num_questions
								print("kind_previous_choosen {}".format(kind_previous_choosen))
								if kind_previous_choosen in ['3S','4P']:
									db_self.already_done_imposed_num_questions = 0
								else:
									stop_impo_num = random.randint(0,1)
									if stop_impo_num==0:
										db_self.already_done_imposed_num_questions = 1
									else:
										db_self.already_done_imposed_num_questions = 0
							else:
								maybe_plus = random.randint(0,3)
								if maybe_plus==0:
									num_of_questions = prev_num_q
								else:
									num_of_questions = prev_num_q + 1
								if num_of_questions>3:
									num_of_questions = 3
						if order_quantity_questions=='desc':
							if db_self.imposed_num_questions!=0 and db_self.already_done_imposed_num_questions==0 and db_self.imposed_num_questions<=prev_num_q:
								num_of_questions = db_self.imposed_num_questions
								print("kind_previous_choosen {}".format(kind_previous_choosen))
								if kind_previous_choosen in ['3S','4P']:
									db_self.already_done_imposed_num_questions = 0
								else:
									stop_impo_num = random.randint(0,1)
									if stop_impo_num==0:
										db_self.already_done_imposed_num_questions = 1
									else:
										db_self.already_done_imposed_num_questions = 0							
							maybe_min = random.randint(0,3)
							if maybe_min==0:
								num_of_questions = prev_num_q
							else:
								num_of_questions = prev_num_q - 1
							if num_of_questions<=0:
								num_of_questions = 1

					prev_num_q = num_of_questions
					print("num_of_questions for this game", num_of_questions)
					if num_of_questions==1 and type_game=='MISCELLANEOUS':
						type_game = spare_type_game
						print("ho camnbiayto con spare perchè ho scelto 1 num games non va bene piu bene che sia MISCELLANEOUS")
				#############################################################################################################################################								
				#################################################################
				turn+=1
				#################################################################
				if difficulty=='Easy_1':
					options_diff_g = ['Easy_1','Easy_2']
					options_or_dif_g = ['same', 'casu', 'asc']
				if difficulty=='Easy_2':
					options_diff_g = ['Easy_1','Easy_2','Normal_1']
					options_or_dif_g = ['same', 'casu', 'asc']
				if difficulty=='Normal_1':
					options_diff_g = ['Easy_1','Easy_2','Normal_1','Normal_2']
					options_or_dif_g = ['same', 'casu', 'asc']
				if difficulty=='Normal_2':
					options_diff_g = ['Easy_2','Normal_1','Normal_2','Medium_1']
					options_or_dif_g = ['same', 'casu', 'asc']
				if difficulty=='Medium_1':
					options_diff_g = ['Normal_1','Normal_2','Medium_1']
					options_or_dif_g = ['casu', 'asc', 'desc']
				########################################################################################### check order diff games
				if nm==0:
					difficulty_g = random.choice(options_diff_g)
					if db_self.imposed_difficulty_game!='' and not db_self.already_done_imposed_difficulty_game:
						if order_difficulty_games!='same':
							if nm in [1,2] and num_of_games in [3,4] and nm!=num_of_games:
								maybe_start = random.randint(0,1)
								print("maybe_start!!! is = to ", maybe_start)
								if maybe_start==0:			
									difficulty_g = db_self.imposed_difficulty_game
									db_self.already_done_imposed_difficulty_game = 1
						if order_difficulty_games=='same' or nm==num_of_games:
							difficulty_g = db_self.imposed_difficulty_game
							db_self.already_done_imposed_difficulty_game = 1
				else:
					print("prev_diff_g all'inizio di ogni quabndo non è il primo giro = ", prev_diff_g)
					if order_difficulty_games=='same':
						difficulty_g = prev_diff_g
					if order_difficulty_games=='casu':
						difficulty_g = random.choice(options_diff_g)
						if db_self.imposed_difficulty_game!='' and not db_self.already_done_imposed_difficulty_game:
							if nm in [1,2] and num_of_games in [3,4] and nm!=num_of_games:
								maybe_start = random.randint(0,1)
								print("maybe_start!!! is = to ", maybe_start)
								if maybe_start==0:
									
									difficulty_g = db_self.imposed_difficulty_game
									db_self.already_done_imposed_difficulty_game = 1
									
							if nm==num_of_games:
								difficulty_g = db_self.imposed_difficulty_game
								db_self.already_done_imposed_difficulty_game = 1						
					if order_difficulty_games=='asc':
						prev_ind_d = options_diff_g.index(prev_diff_g)
						ran_dif = random.randint(0,3)
						if ran_dif==0:
							difficulty_g = options_diff_g[prev_ind_d]
						else:
							try:
								difficulty_g = options_diff_g[prev_ind_d+1]
							except:
								difficulty_g = options_diff_g[prev_ind_d]
						try:
							if db_self.imposed_difficulty_game!='' and not db_self.already_done_imposed_difficulty_game \
								and options_diff_g.index(db_self.imposed_difficulty_game)>=options_diff_g.index(prev_diff_g):
								if nm in [1,2] and num_of_games in [3,4] and nm!=num_of_games:
									maybe_start = random.randint(0,1)
									print("maybe_start!!! is = to ", maybe_start)
									if maybe_start==0:
										difficulty_g = db_self.imposed_difficulty_game
										db_self.already_done_imposed_difficulty_game = 1
								if nm==num_of_games:
									difficulty_g = db_self.imposed_difficulty_game
									db_self.already_done_imposed_difficulty_game = 1
						except:
							if db_self.imposed_difficulty_game!='' and not db_self.already_done_imposed_difficulty_game \
								and db_self.imposed_difficulty_game not in options_diff_g:
								if nm in [1,2] and num_of_games in [3,4] and nm!=num_of_games:
									maybe_start = random.randint(0,1)
									print("maybe_start!!! is = to ", maybe_start)
									if maybe_start==0:
										difficulty_g = db_self.imposed_difficulty_game
										db_self.already_done_imposed_difficulty_game = 1
								if nm==num_of_games:
									difficulty_g = db_self.imposed_difficulty_game
									db_self.already_done_imposed_difficulty_game = 1				
					if order_difficulty_games=='desc':
						prev_ind_d = options_diff_g.index(prev_diff_g)
						ran_dif = random.randint(0,3)
						if ran_dif==0:
							difficulty_g = options_diff_g[prev_ind_d]
						else:
							try:
								if prev_ind_d - 1 >= 0:
									difficulty_g = options_diff_g[prev_ind_d-1]
								else:
									raise Exception
							except Exception:
								difficulty_g = options_diff_g[prev_ind_d]
						try:
							if db_self.imposed_difficulty_game!='' and not db_self.already_done_imposed_difficulty_game \
								and options_diff_g.index(db_self.imposed_difficulty_game)<=options_diff_g.index(prev_diff_g):
								if nm in [1,2] and num_of_games in [3,4] and nm!=num_of_games:
									maybe_start = random.randint(0,1)
									print("maybe_start!!! is = to ", maybe_start)
									if maybe_start==0:
										difficulty_g = db_self.imposed_difficulty_game
										db_self.already_done_imposed_difficulty_game = 1
								if nm==num_of_games:
									difficulty_g = db_self.imposed_difficulty_game
									db_self.already_done_imposed_difficulty_game = 1
						except:
							if db_self.imposed_difficulty_game!='' and not db_self.already_done_imposed_difficulty_game \
								and db_self.imposed_difficulty_game not in options_diff_g:
								if nm in [1,2] and num_of_games in [3,4] and nm!=num_of_games:
									maybe_start = random.randint(0,1)
									print("maybe_start!!! is = to ", maybe_start)
									if maybe_start==0:
										difficulty_g = db_self.imposed_difficulty_game
										db_self.already_done_imposed_difficulty_game = 1
								if nm==num_of_games:
									difficulty_g = db_self.imposed_difficulty_game
									db_self.already_done_imposed_difficulty_game = 1							
				prev_diff_g = difficulty_g
				print("difficulty_g scelta = prev_diff_g", difficulty_g)
				print("prev_diff_g alla fine di ogni giro = ", prev_diff_g)
				order_diff_questions = random.choice(options_or_dif_g)
				if type_game!='MISCELLANEOUS':
					type_is_already_in_this_match.append(type_game)
				print("type_is_already_in_this_match type_is_already_in_this_match type_is_already_in_this_match type_is_already_in_this_match")
				print(type_is_already_in_this_match)				
				############################################################################################
				if kind_game=='6I':
					num_of_questions = 1
					type_game = '6I'
				if kind_game=='3S':
					depends_num = random.randint(0,3)
					if depends_num==0:
						num_of_questions = 2
						if db_self.imposed_type_game=='3SSU' or db_self.imposed_ques_id==3011:
							type_game = '3SSU'
						elif db_self.imposed_type_game=='3SCO' or db_self.imposed_ques_id==3012:
							type_game = '3SCO'
					else:
						num_of_questions = 1
						if db_self.imposed_type_game=='3SSU' or db_self.imposed_ques_id==3011:
							type_game = '3SSU'
						elif db_self.imposed_type_game=='3SCO' or db_self.imposed_ques_id==3012:
							type_game = '3SCO'
				####################################################################################				
				if difficulty_g in ['Easy_1','Easy_2']:
					if num_of_questions>=3:
						necessary_time = 60
					else:
						necessary_time = 50
				if difficulty_g=='Normal_1':
					if num_of_questions>=3:
						necessary_time = 50
					else:
						necessary_time = 45
				if difficulty_g=='Normal_2':
					if num_of_questions>=3:
						necessary_time = 40
					else:
						necessary_time = 35
				if difficulty_g=='Medium_1':
					if num_of_questions>=3:
						necessary_time = 30
					else:
						necessary_time = 25
				#######################################################################################
				print("sono in part4 ed è arrivato il momento di creare gioco...mi servono")
				print("PASSO PARAMETRI TO ADD_NEW_GAME")
				print("num_of_questions {}".format(num_of_questions))
				print("kind_game {}".format(kind_game))
				print("type_game {}".format(type_game))
				print("difficulty_g {}".format(difficulty_g,))
				print("order_diff_questions {}".format(order_diff_questions,))
				print("necessary_time {}".format(necessary_time))
				print("scenario {}".format(scenario))
				print("list_types_para {}".format(list_types_param))
				print("sono in part4 ed è arrivato il momento di creare gioco...mi servono")
				print("PASSO PARAMETRI TO ADD_NEW_GAME")
				#######################################################################################
				kind_previous_choosen = copy.copy(kind_game)
				####################################################################################
				num_ques_returned, ques_ok_ret = dca.add_new_Game_table(db_self, num_of_questions, kind_game, type_game, difficulty_g, order_diff_questions, necessary_time, 0, scenario, list_types_param, questions_already_choosen_prev_match)
				print("questions_ok received part_4 ==> {}".format(ques_ok_ret))
				print("questions_ok in part_4 prima di iterFlatten ==> {}".format(questions_already_choosen_prev_match))
				questions_already_choosen_prev_match.append(ques_ok_ret)
				questions_already_choosen_prev_match = list(dbem2.iterFlatten(questions_already_choosen_prev_match))
				print("questions_ok in part_4 dopo iterFlatten ==> {}".format(questions_already_choosen_prev_match))
				db_self.curs.execute("SELECT game_id FROM Games ORDER BY game_id DESC LIMIT 1")
				game_last = db_self.curs.fetchone()[0]
				print("game_last game_last game_last game_last {}".format(game_last))
				dca.add_new_MatchGame_table(db_self, match_last, game_last)
				if num_ques_returned==db_self.imposed_num_questions and db_self.already_done_imposed_num_questions==1:
					db_self.already_done_imposed_num_questions = 0
			
			if variety=='SUNDRY' and criteria_arrangement=='regular':
				print("REGULAR CASE")
				print("REGULAR CASE")
				print("REGULAR CASE")
				print("REGULAR CASE")
				print("REGULAR CASE")
				print("REGULAR CASE")
				print("REGULAR CASE")
				print("REGULAR CASE")
				print("REGULAR CASE")
				print("REGULAR CASE")
				db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
				match_l = db_self.curs.fetchone()[0]
				dcfr.sort_games_in_correct_order_if_m_regular(db_self, match_l)
		#################################################################################################################################################################
		elif difficulty in ['Medium_2','Hard_1','Hard_2']:
			dca5.add_new_Match_table_5(db_self, difficulty, variety, criteria_arrangement, list_kinds, list_types, listone_tipi, match_last, num_of_games, scenario, order_difficulty_games, order_quantity_questions)

	except Exception as e:
		print("--> An error occurred while adding a new Match")
		print("--> Search_str add_new_Match {}".format(search_str))
		print("Namely:")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
