"""Info:
	Oimi robot database controller of additions of new data into tables, where all existing constraints must be revised or new constraints must be added.
	Insert session results and feedbacks into database.
	Neither db_defaults_list or oimi_queries is modified in this case.
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os
from typing import Optional
#cimport database_controller_update_second
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef include_all_feeds_in_once(db_self, int kid_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
	cdef:
		str quant_fed_ques_1 = None
		str grade_fed_ques_1 = None
		float val_fed_ques_1 = 0.0
		str quant_fed_ques_2 = None
		str grade_fed_ques_2 = None
		float val_fed_ques_2 = 0.0
		str quant_fed_ques_3 = None
		str grade_fed_ques_3 = None
		float val_fed_ques_3 = 0.0
		str quant_fed_ques_4 = None
		str grade_fed_ques_4 = None
		float val_fed_ques_4 = 0.0
		str quantity_feedback_session = the_session_feeds[0]
		str grade_feedback_session = the_session_feeds[1]
		float val_fed_session = the_session_feeds[2]
		str quant_fed_match_1 = None
		str grade_fed_match_1 = None
		float val_fed_match_1 = 0.0
		str quant_fed_match_2 = None
		str grade_fed_match_2 = None
		float val_fed_match_2 = 0.0
		str quant_fed_match_3 = None
		str grade_fed_match_3 = None
		float val_fed_match_3 = 0.0
		str quant_fed_match_4 = None
		str grade_fed_match_4 = None
		float val_fed_match_4 = 0.0
		str quant_fed_game_1 = None
		str grade_fed_game_1 = None
		float val_fed_game_1 = 0.0
		str quant_fed_game_2 = None
		str grade_fed_game_2 = None
		float val_fed_game_2 = 0.0
		str quant_fed_game_3 = None
		str grade_fed_game_3 = None
		float val_fed_game_3 = 0.0
		str quant_fed_game_4 = None
		str grade_fed_game_4 = None
		float val_fed_game_4 = 0.0
	try:
		#####################àserve mettere il WHERE quant_fed_match_1 isNull da aggiungere a !!! ...>>>>> kid_id = ? AND match_id = ?''',
		db_self.curs.execute("SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1", (kid_id,))
		this_session_id = db_self.curs.fetchone()[0]
		print("this_session_id update second!!!", this_session_id)
		
		db_self.curs.execute('''UPDATE Kids_Sessions SET
			quantity_feedback_session = ?, grade_feedback_session = ?, value_feedback_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(quantity_feedback_session,grade_feedback_session,val_fed_session,kid_id,this_session_id,))
		db_self.conn.commit()

		db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
			quantity_feedback_session = ?, grade_feedback_session = ?, value_feedback_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(quantity_feedback_session,grade_feedback_session,val_fed_session,kid_id,this_session_id,))
		db_self.conn.commit()
		db_self.curs.execute('''UPDATE Full_played_summary SET
			quantity_feedback_session = ?, grade_feedback_session = ?, value_feedback_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(quantity_feedback_session,grade_feedback_session,val_fed_session,kid_id,this_session_id,))
		db_self.conn.commit()

		db_self.curs.execute("SELECT match_id FROM Sessions_Matches WHERE session_id = ?", (this_session_id,))
		resp_m = db_self.curs.fetchall()
		print("resp_m resp_m resp_m => ",resp_m)
		if resp_m:
			s_matches = [item for sublist in resp_m for item in sublist]
			print("this_session_matches update second!!!", s_matches)
			print("num_of_matches update second!!!", len(s_matches))
		else:
			print(" NESSUN MATCH update second!!! update second!!! FINDUS FINDUS FINDUS")	
		print("s_matches s_matches s_matches s_matches => ",s_matches)
		if len(s_matches)==1:
			this_num_of_matches = 1
			this_match_id = s_matches[0]
			quant_fed_match_1 = matches_feeds[0][0]
			grade_fed_match_1 = matches_feeds[0][1]
			val_fed_match_1 = matches_feeds[0][2]
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id,))
			db_self.conn.commit()
		
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id,))
			db_self.conn.commit()

		if len(s_matches)==2:
			this_num_of_matches = 2
			quant_fed_match_1 = matches_feeds[0][0]
			grade_fed_match_1 = matches_feeds[0][1]
			val_fed_match_1 = matches_feeds[0][2]
			quant_fed_match_2 = matches_feeds[1][0]
			grade_fed_match_2 = matches_feeds[1][1]
			val_fed_match_2 = matches_feeds[1][2]
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()		
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
		
		if len(s_matches)==3:
			this_num_of_matches = 1
			quant_fed_match_1 = matches_feeds[0][0]
			grade_fed_match_1 = matches_feeds[0][1]
			val_fed_match_1 = matches_feeds[0][2]
			quant_fed_match_2 = matches_feeds[1][0]
			grade_fed_match_2 = matches_feeds[1][1]
			val_fed_match_2 = matches_feeds[1][2]
			quant_fed_match_3 = matches_feeds[2][0]
			grade_fed_match_3 = matches_feeds[2][1]
			val_fed_match_3 = matches_feeds[2][2]
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			this_match_id_3 = s_matches[2]

			print("quant_fed_match_1 --> ", quant_fed_match_1)
			print("grade_fed_match_1 --> ", grade_fed_match_1)
			print("val_fed_match_1 --> ", val_fed_match_1)
			print("quant_fed_match_2 --> ", quant_fed_match_2)
			print("grade_fed_match_2 --> ", grade_fed_match_2)
			print("val_fed_match_2 --> ", val_fed_match_2)
			print("quant_fed_match_3 --> ", quant_fed_match_3)
			print("grade_fed_match_3 --> ", grade_fed_match_3)
			print("val_fed_match_3 --> ", val_fed_match_3)
			print("this_match_id_1 --> ", this_match_id_1)
			print("this_match_id_2 --> ", this_match_id_2)
			print("this_match_id_3 --> ", this_match_id_3)

			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()		
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
		
		if len(s_matches)==4:
			this_num_of_matches = 4
			quant_fed_match_1 = matches_feeds[0][0]
			grade_fed_match_1 = matches_feeds[0][1]
			val_fed_match_1 = matches_feeds[0][2]
			quant_fed_match_2 = matches_feeds[1][0]
			grade_fed_match_2 = matches_feeds[1][1]
			val_fed_match_2 = matches_feeds[1][2]
			quant_fed_match_3 = matches_feeds[2][0]
			grade_fed_match_3 = matches_feeds[2][1]
			val_fed_match_3 = matches_feeds[2][2]
			quant_fed_match_4 = matches_feeds[3][0]
			grade_fed_match_4 = matches_feeds[3][1]
			val_fed_match_4 = matches_feeds[3][2]
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			this_match_id_3 = s_matches[2]
			this_match_id_4 = s_matches[3]

			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()		
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_4,grade_fed_match_4,val_fed_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_4,grade_fed_match_4,val_fed_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_4,grade_fed_match_4,val_fed_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()
		
		where_I_was_m = 0
		where_I_was_g = 0
		for mm in s_matches:
			db_self.curs.execute("SELECT game_id FROM Matches_Games WHERE match_id = ?", (mm,))
			resp_g = db_self.curs.fetchall()
			if resp_g:
				m_games = [item for sublist in resp_g for item in sublist]
			############## game = 1
			if len(m_games)==1:
				quant_fed_game_1 = games_feeds[where_I_was_m][0]
				grade_fed_game_1 = games_feeds[where_I_was_m][1]
				val_fed_game_1 = games_feeds[where_I_was_m][2]
				this_game_id = m_games[0]
				where_I_was_m += 1
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id,))
				db_self.conn.commit()

				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id,))
				db_self.conn.commit()
				
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id,))
				db_self.conn.commit()

			if len(m_games)==2:
				quant_fed_game_1 = games_feeds[where_I_was_m][0]
				grade_fed_game_1 = games_feeds[where_I_was_m][1]
				val_fed_game_1 = games_feeds[where_I_was_m][2]
				quant_fed_game_2 = games_feeds[where_I_was_m+1][0]
				grade_fed_game_2 = games_feeds[where_I_was_m+1][1]
				val_fed_game_2 = games_feeds[where_I_was_m+1][2]
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				where_I_was_m += 2
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()

				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()

			if len(m_games)==3:
				quant_fed_game_1 = games_feeds[where_I_was_m][0]
				grade_fed_game_1 = games_feeds[where_I_was_m][1]
				val_fed_game_1 = games_feeds[where_I_was_m][2]
				quant_fed_game_2 = games_feeds[where_I_was_m+1][0]
				grade_fed_game_2 = games_feeds[where_I_was_m+1][1]
				val_fed_game_2 = games_feeds[where_I_was_m+1][2]
				quant_fed_game_3 = games_feeds[where_I_was_m+2][0]
				grade_fed_game_3 = games_feeds[where_I_was_m+2][1]
				val_fed_game_3 = games_feeds[where_I_was_m+2][2]
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				this_game_id_3 = m_games[2]
				where_I_was_m += 3
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()

				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()

			if len(m_games)==4:
				quant_fed_game_1 = games_feeds[where_I_was_m][0]
				grade_fed_game_1 = games_feeds[where_I_was_m][1]
				val_fed_game_1 = games_feeds[where_I_was_m][2]
				quant_fed_game_2 = games_feeds[where_I_was_m+1][0]
				grade_fed_game_2 = games_feeds[where_I_was_m+1][1]
				val_fed_game_2 = games_feeds[where_I_was_m+1][2]
				quant_fed_game_3 = games_feeds[where_I_was_m+2][0]
				grade_fed_game_3 = games_feeds[where_I_was_m+2][1]
				val_fed_game_3 = games_feeds[where_I_was_m+2][2]
				quant_fed_game_4 = games_feeds[where_I_was_m+3][0]
				grade_fed_game_4 = games_feeds[where_I_was_m+3][1]
				val_fed_game_4 = games_feeds[where_I_was_m+3][2]
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				this_game_id_3 = m_games[2]
				this_game_id_4 = m_games[3]
				where_I_was_m += 4
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_4,grade_fed_game_4,val_fed_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_4,grade_fed_game_4,val_fed_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()

				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_4,grade_fed_game_4,val_fed_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()
			
			for gg in m_games:
				db_self.curs.execute("SELECT question_id FROM Games_Questions WHERE game_id = ?", (gg,))
				resp_qs = db_self.curs.fetchall()
				if resp_qs:
					g_questions = [item for sublist in resp_qs for item in sublist]

				if len(g_questions)==1:
					quant_fed_ques_1 = questions_feeds[where_I_was_g][0]
					grade_fed_ques_1 = questions_feeds[where_I_was_g][1]
					val_fed_ques_1 = questions_feeds[where_I_was_g][2]
					this_ques_id = g_questions[0]
					where_I_was_g += 1
					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?,value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
				if len(g_questions)==2:
					quant_fed_ques_1 = questions_feeds[where_I_was_g][0]
					grade_fed_ques_1 = questions_feeds[where_I_was_g][1]
					val_fed_ques_1 = questions_feeds[where_I_was_g][2]
					quant_fed_ques_2 = questions_feeds[where_I_was_g+1][0]
					grade_fed_ques_2 = questions_feeds[where_I_was_g+1][1]
					val_fed_ques_2 = questions_feeds[where_I_was_g+1][2]					
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					where_I_was_g += 2
					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?,value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
				if len(g_questions)==3:
					quant_fed_ques_1 = questions_feeds[where_I_was_g][0]
					grade_fed_ques_1 = questions_feeds[where_I_was_g][1]
					val_fed_ques_1 = questions_feeds[where_I_was_g][2]
					quant_fed_ques_2 = questions_feeds[where_I_was_g+1][0]
					grade_fed_ques_2 = questions_feeds[where_I_was_g+1][1]
					val_fed_ques_2 = questions_feeds[where_I_was_g+1][2]					
					quant_fed_ques_3 = questions_feeds[where_I_was_g+2][0]
					grade_fed_ques_3 = questions_feeds[where_I_was_g+2][1]
					val_fed_ques_3 = questions_feeds[where_I_was_g+2][2]	
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					this_ques_id_3 = g_questions[2]
					where_I_was_g +=3
					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?,value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
				if len(g_questions)==4:
					quant_fed_ques_1 = questions_feeds[where_I_was_g][0]
					grade_fed_ques_1 = questions_feeds[where_I_was_g][1]
					val_fed_ques_1 = questions_feeds[where_I_was_g][2]
					quant_fed_ques_2 = questions_feeds[where_I_was_g+1][0]
					grade_fed_ques_2 = questions_feeds[where_I_was_g+1][1]
					val_fed_ques_2 = questions_feeds[where_I_was_g+1][2]					
					quant_fed_ques_3 = questions_feeds[where_I_was_g+2][0]
					grade_fed_ques_3 = questions_feeds[where_I_was_g+2][1]
					val_fed_ques_3 = questions_feeds[where_I_was_g+2][2]	
					quant_fed_ques_4 = questions_feeds[where_I_was_g+3][0]
					grade_fed_ques_4 = questions_feeds[where_I_was_g+3][1]
					val_fed_ques_4 = questions_feeds[where_I_was_g+3][2]	
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					this_ques_id_3 = g_questions[2]
					this_ques_id_4 = g_questions[3]
					where_I_was_g += 4
					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?,value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_4,grade_fed_ques_4,val_fed_ques_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))				
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_4,grade_fed_ques_4,val_fed_ques_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_4,grade_fed_ques_4,val_fed_ques_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))
					db_self.conn.commit()

	except Exception as e:
		print("--> An error occurred while adding a new Question")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef include_all_results_in_once(db_self, int kid_id, float time_sess, list questions_results, list matches_results, list games_results):
	print("SIAMO IN include_all_results_in_once!!!")
	print("per prima cosa cosa ricevo??")
	print("kid_id is {} | float time_sess is {} | list questions_results is {} | list matches_results is {} | list games_results is {}".format(kid_id, time_sess, questions_results, matches_results, games_results))
	cdef:
		int time_of_session = 0
		int num_answers_game_1 = 0
		int num_corrects_game_1 = 0
		int num_errors_game_1 = 0
		int num_indecisions_game_1 = 0
		int num_already_game_1 = 0
		int time_of_game_1 = 0
		int num_answers_game_2 = 0
		int num_corrects_game_2 = 0
		int num_errors_game_2 = 0
		int num_indecisions_game_2 = 0
		int num_already_game_2 = 0
		int time_of_game_2 = 0
		int num_answers_game_3 = 0
		int num_corrects_game_3 = 0
		int num_errors_game_3 = 0
		int num_indecisions_game_3 = 0
		int num_already_game_3 = 0
		int time_of_game_3 = 0
		int num_answers_game_4 = 0
		int num_corrects_game_4 = 0
		int num_errors_game_4 = 0
		int num_indecisions_game_4 = 0
		int num_already_game_4 = 0
		int time_of_game_4 = 0	
		int num_answers_match_1 = 0
		int num_corrects_match_1 = 0
		int num_errors_match_1 = 0
		int num_indecisions_match_1 = 0
		int num_already_match_1 = 0
		int time_of_match_1 = 0
		int num_answers_match_2 = 0
		int num_corrects_match_2 = 0
		int num_errors_match_2 = 0
		int num_indecisions_match_2 = 0
		int num_already_match_2 = 0
		int time_of_match_2 = 0
		int num_answers_match_3 = 0
		int num_corrects_match_3 = 0
		int num_errors_match_3 = 0
		int num_already_match_3 = 0
		int time_of_match_3 = 0
		int num_answers_match_4 = 0
		int num_corrects_match_4 = 0
		int num_errors_match_4 = 0
		int num_indecisions_match_4 = 0
		int num_already_match_4 = 0
		int time_of_match_4 = 0
		int num_answers_question_1 = 0
		int num_corrects_question_1 = 0
		int num_errors_question_1 = 0
		int num_indecisions_question_1 = 0
		int num_already_question_1 = 0
		int time_of_question_1 = 0
		int num_answers_question_2 = 0
		int num_corrects_question_2 = 0
		int num_errors_question_2 = 0
		int num_indecisions_question_2 = 0
		int num_already_question_2 = 0
		int time_of_question_2 = 0
		int num_answers_question_3 = 0
		int num_corrects_question_3 = 0
		int num_errors_question_3 = 0
		int num_indecisions_question_3 = 0
		int num_already_question_3 = 0
		int time_of_question_3 = 0
		int num_answers_question_4 = 0
		int num_corrects_question_4 = 0
		int num_errors_question_4 = 0
		int num_indecisions_question_4 = 0
		int num_already_question_4 = 0
		int time_of_question_4 = 0	

	try:
		time_of_session = int(time_sess)
		#####################àserve mettere il WHERE quant_fed_match_1 isNull da aggiungere a !!! ...>>>>> kid_id = ? AND match_id = ?''',
		db_self.curs.execute("SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1", (kid_id,))
		this_session_id = db_self.curs.fetchone()[0]
		print("this_session_id", this_session_id)
		
		db_self.curs.execute('''UPDATE Kids_Sessions SET
			time_of_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(time_of_session,kid_id,this_session_id,))
		db_self.conn.commit()
		db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
			time_of_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(time_of_session,kid_id,this_session_id,))
		db_self.conn.commit()
		db_self.curs.execute('''UPDATE Full_played_summary SET
			time_of_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(time_of_session,kid_id,this_session_id,))
		db_self.conn.commit()

		db_self.curs.execute("SELECT match_id FROM Sessions_Matches WHERE session_id = ?", (this_session_id,))
		resp_m = db_self.curs.fetchall()
		print("resp_m resp_m resp_m => ",resp_m)
		if resp_m:
			s_matches = [item for sublist in resp_m for item in sublist]
		else:
			print("")
		print("this_session_matches", s_matches)
		print("num_of_matches", len(s_matches))
		if len(s_matches)==1:
			this_num_of_matches = 1
			this_match_id_1 = s_matches[0]
			num_answers_match_1 = matches_results[0][0]
			num_corrects_match_1 = matches_results[0][1]
			num_errors_match_1 = matches_results[0][2]
			num_indecisions_match_1 = matches_results[0][3]
			num_already_match_1 = matches_results[0][4]
			time_of_match_1 = matches_results[0][5]
			print("eccoci time_of_match_1", time_of_match_1)
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()

		if len(s_matches)==2:
			this_num_of_matches = 2
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			num_answers_match_1 = matches_results[0][0]
			num_corrects_match_1 = matches_results[0][1]
			num_errors_match_1 = matches_results[0][2]
			num_indecisions_match_1 = matches_results[0][3]
			num_already_match_1 = matches_results[0][4]
			time_of_match_1 = matches_results[0][5]
			print("eccoci time_of_match_1", time_of_match_1)
			num_answers_match_2 = matches_results[1][0]
			num_corrects_match_2 = matches_results[1][1]
			num_errors_match_2 = matches_results[1][2]
			num_indecisions_match_2 = matches_results[1][3]
			num_already_match_2 = matches_results[1][4]
			time_of_match_2 = matches_results[1][5]
			print("eccoci time_of_match_2", time_of_match_2)

			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()

		if len(s_matches)==3:
			this_num_of_matches = 3
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			this_match_id_3 = s_matches[2]
			num_answers_match_1 = matches_results[0][0]
			num_corrects_match_1 = matches_results[0][1]
			num_errors_match_1 = matches_results[0][2]
			num_indecisions_match_1 = matches_results[0][3]
			num_already_match_1 = matches_results[0][4]
			time_of_match_1 = matches_results[0][5]
			print("eccoci time_of_match_1", time_of_match_1)
			num_answers_match_2 = matches_results[1][0]
			num_corrects_match_2 = matches_results[1][1]
			num_errors_match_2 = matches_results[1][2]
			num_indecisions_match_2 = matches_results[1][3]
			num_already_match_2 = matches_results[1][4]
			time_of_match_2 = matches_results[1][5]
			print("eccoci time_of_match_2", time_of_match_2)
			num_answers_match_3 = matches_results[2][0]
			num_corrects_match_3 = matches_results[2][1]
			num_errors_match_3 = matches_results[2][2]
			num_indecisions_match_3 = matches_results[2][3]
			num_already_match_3 = matches_results[2][4]
			time_of_match_3 = matches_results[2][5]
			print("eccoci time_of_match_3", time_of_match_3)
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()

		if len(s_matches)==4:
			this_num_of_matches = 4
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			this_match_id_3 = s_matches[2]
			this_match_id_4 = s_matches[3]
			num_answers_match_1 = matches_results[0][0]
			num_corrects_match_1 = matches_results[0][1]
			num_errors_match_1 = matches_results[0][2]
			num_indecisions_match_1 = matches_results[0][3]
			num_already_match_1 = matches_results[0][4]
			time_of_match_1 = matches_results[0][5]
			print("eccoci time_of_match_1", time_of_match_1)
			num_answers_match_2 = matches_results[1][0]
			num_corrects_match_2 = matches_results[1][1]
			num_errors_match_2 = matches_results[1][2]
			num_indecisions_match_2 = matches_results[1][3]
			num_already_match_2 = matches_results[1][4]
			time_of_match_2 = matches_results[1][5]
			print("eccoci time_of_match_2", time_of_match_2)
			num_answers_match_3 = matches_results[2][0]
			num_corrects_match_3 = matches_results[2][1]
			num_errors_match_3 = matches_results[2][2]
			num_indecisions_match_3 = matches_results[2][3]
			num_already_match_3 = matches_results[2][4]
			time_of_match_3 = matches_results[2][5]
			print("eccoci time_of_match_3", time_of_match_3)
			num_answers_match_4 = matches_results[3][0]
			num_corrects_match_4 = matches_results[3][1]
			num_errors_match_4 = matches_results[3][2]
			num_indecisions_match_4 = matches_results[3][3]
			num_already_match_4 = matches_results[3][4]
			time_of_match_4 = matches_results[3][5]
			print("eccoci time_of_match_4", time_of_match_4)
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_4,num_corrects_match_4,num_errors_match_4,num_indecisions_match_4,num_already_match_4,time_of_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_4,num_corrects_match_4,num_errors_match_4,num_indecisions_match_4,num_already_match_4,time_of_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()
			
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_4,num_corrects_match_4,num_errors_match_4,num_indecisions_match_4,num_already_match_4,time_of_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()
		
		where_I_was_m = 0
		where_I_was_g = 0
		for mm in s_matches:
			db_self.curs.execute("SELECT game_id FROM Matches_Games WHERE match_id = ?", (mm,))
			resp_g = db_self.curs.fetchall()
			if resp_g:
				m_games = [item for sublist in resp_g for item in sublist]
			############## game = 1
			if len(m_games)==1:
				this_num_of_games = 1
				this_game_id_1 = m_games[where_I_was_m]
				num_answers_game_1 = games_results[where_I_was_m][0]
				num_corrects_game_1 = games_results[where_I_was_m][1]
				num_errors_game_1 = games_results[where_I_was_m][2]
				num_indecisions_game_1 = games_results[where_I_was_m][3]
				num_already_game_1 = games_results[where_I_was_m][4]
				time_of_game_1 = int(games_results[where_I_was_m][5])
				where_I_was_m += 1
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()			
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
			if len(m_games)==2:
				this_num_of_games = 2
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				num_answers_game_1 = games_results[where_I_was_m][0]
				num_corrects_game_1 = games_results[where_I_was_m][1]
				num_errors_game_1 = games_results[where_I_was_m][2]
				num_indecisions_game_1 = games_results[where_I_was_m][3]
				num_already_game_1 = games_results[where_I_was_m][4]
				time_of_game_1 = int(games_results[where_I_was_m][5])
				num_answers_game_2 = games_results[where_I_was_m+1][0]
				num_corrects_game_2 = games_results[where_I_was_m+1][1]
				num_errors_game_2 = games_results[where_I_was_m+1][2]
				num_indecisions_game_2 = games_results[where_I_was_m+1][3]
				num_already_game_2 = games_results[where_I_was_m+1][4]
				time_of_game_2 = int(games_results[where_I_was_m+1][5])
				where_I_was_m += 2
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()			
				
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()

			if len(m_games)==3:
				this_num_of_games = 3
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				this_game_id_3 = m_games[2]
				num_answers_game_1 = games_results[where_I_was_m][0]
				num_corrects_game_1 = games_results[where_I_was_m][1]
				num_errors_game_1 = games_results[where_I_was_m][2]
				num_indecisions_game_1 = games_results[where_I_was_m][3]
				num_already_game_1 = games_results[where_I_was_m][4]
				time_of_game_1 = int(games_results[where_I_was_m][5])
				num_answers_game_2 = games_results[where_I_was_m+1][0]
				num_corrects_game_2 = games_results[where_I_was_m+1][1]
				num_errors_game_2 = games_results[where_I_was_m+1][2]
				num_indecisions_game_2 = games_results[where_I_was_m+1][3]
				num_already_game_2 = games_results[where_I_was_m+1][4]
				time_of_game_2 = int(games_results[where_I_was_m+1][5])
				num_answers_game_3 = games_results[where_I_was_m+2][0]
				num_corrects_game_3 = games_results[where_I_was_m+2][1]
				num_errors_game_3 = games_results[where_I_was_m+2][2]
				num_indecisions_game_3 = games_results[where_I_was_m+2][3]
				num_already_game_3 = games_results[where_I_was_m+2][4]
				time_of_game_3 = int(games_results[where_I_was_m+2][5])
				where_I_was_m += 3
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()						
				
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()

			if len(m_games)==4:
				this_num_of_games = 4
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				this_game_id_3 = m_games[2]
				this_game_id_4 = m_games[3]
				num_answers_game_1 = games_results[where_I_was_m][0]
				num_corrects_game_1 = games_results[where_I_was_m][1]
				num_errors_game_1 = games_results[where_I_was_m][2]
				num_indecisions_game_1 = games_results[where_I_was_m][3]
				num_already_game_1 = games_results[where_I_was_m][4]
				time_of_game_1 = int(games_results[where_I_was_m][5])
				num_answers_game_2 = games_results[where_I_was_m+1][0]
				num_corrects_game_2 = games_results[where_I_was_m+1][1]
				num_errors_game_2 = games_results[where_I_was_m+1][2]
				num_indecisions_game_2 = games_results[where_I_was_m+1][3]
				num_already_game_2 = games_results[where_I_was_m+1][4]
				time_of_game_2 = int(games_results[where_I_was_m+1][5])
				num_answers_game_3 = games_results[where_I_was_m+2][0]
				num_corrects_game_3 = games_results[where_I_was_m+2][1]
				num_errors_game_3 = games_results[where_I_was_m+2][2]
				num_indecisions_game_3 = games_results[where_I_was_m+2][3]
				num_already_game_3 = games_results[where_I_was_m+2][4]
				time_of_game_3 = int(games_results[where_I_was_m+2][5])
				num_answers_game_4 = games_results[where_I_was_m+3][0]
				num_corrects_game_4 = games_results[where_I_was_m+3][1]
				num_errors_game_4 = games_results[where_I_was_m+3][2]
				num_indecisions_game_4 = games_results[where_I_was_m+3][3]
				num_already_game_4 = games_results[where_I_was_m+3][4]
				time_of_game_4 = int(games_results[where_I_was_m+3][5])
				where_I_was_m += 4
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_4,num_corrects_game_4,num_errors_game_4,num_indecisions_game_4,num_already_game_4,time_of_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()									

				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_4,num_corrects_game_4,num_errors_game_4,num_indecisions_game_4,num_already_game_4,time_of_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()
				
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_4,num_corrects_game_4,num_errors_game_4,num_indecisions_game_4,num_already_game_4,time_of_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()

			
			for gg in m_games:
				db_self.curs.execute("SELECT question_id FROM Games_Questions WHERE game_id = ?", (gg,))
				resp_qs = db_self.curs.fetchall()
				if resp_qs:
					g_questions = [item for sublist in resp_qs for item in sublist]

				if len(g_questions)==1:
					this_num_of_questions = 1
					this_ques_id_1 = g_questions[0]
					num_answers_question_1 = questions_results[where_I_was_g][0]
					num_corrects_question_1 = questions_results[where_I_was_g][1]
					num_errors_question_1 = questions_results[where_I_was_g][2]
					num_indecisions_question_1 = questions_results[where_I_was_g][3]
					num_already_question_1 = questions_results[where_I_was_g][4]
					time_of_question_1 = int(questions_results[where_I_was_g][5])
					where_I_was_g += 1
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
				
				if len(g_questions)==2:
					this_num_of_questions = 2
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					num_answers_question_1 = questions_results[where_I_was_g][0]
					num_corrects_question_1 = questions_results[where_I_was_g][1]
					num_errors_question_1 = questions_results[where_I_was_g][2]
					num_indecisions_question_1 = questions_results[where_I_was_g][3]
					num_already_question_1 = questions_results[where_I_was_g][4]
					time_of_question_1 = int(questions_results[where_I_was_g][5])
					num_answers_question_2 = questions_results[where_I_was_g+1][0]
					num_corrects_question_2 = questions_results[where_I_was_g+1][1]
					num_errors_question_2 = questions_results[where_I_was_g+1][2]
					num_indecisions_question_2 = questions_results[where_I_was_g+1][3]
					num_already_question_2 = questions_results[where_I_was_g+1][4]
					time_of_question_2 = int(questions_results[where_I_was_g+1][5])
					where_I_was_g += 2
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()				

					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
				
				if len(g_questions)==3:
					this_num_of_questions = 3
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					this_ques_id_3 = g_questions[2]
					num_answers_question_1 = questions_results[where_I_was_g][0]
					num_corrects_question_1 = questions_results[where_I_was_g][1]
					num_errors_question_1 = questions_results[where_I_was_g][2]
					num_indecisions_question_1 = questions_results[where_I_was_g][3]
					num_already_question_1 = questions_results[where_I_was_g][4]
					time_of_question_1 = int(questions_results[where_I_was_g][5])
					num_answers_question_2 = questions_results[where_I_was_g+1][0]
					num_corrects_question_2 = questions_results[where_I_was_g+1][1]
					num_errors_question_2 = questions_results[where_I_was_g+1][2]
					num_indecisions_question_2 = questions_results[where_I_was_g+1][3]
					num_already_question_2 = questions_results[where_I_was_g+1][4]
					time_of_question_2 = int(questions_results[where_I_was_g+1][5])
					num_answers_question_3 = questions_results[where_I_was_g+2][0]
					num_corrects_question_3 = questions_results[where_I_was_g+2][1]
					num_errors_question_3 = questions_results[where_I_was_g+2][2]
					num_indecisions_question_3 = questions_results[where_I_was_g+2][3]
					num_already_question_3 = questions_results[where_I_was_g+2][4]
					time_of_question_3 = int(questions_results[where_I_was_g+2][5])
					where_I_was_g += 3
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()								
					
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()

				if len(g_questions)==4:
					this_num_of_questions = 4
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					this_ques_id_3 = g_questions[2]
					this_ques_id_4 = g_questions[3]
					num_answers_question_1 = questions_results[where_I_was_g][0]
					num_corrects_question_1 = questions_results[where_I_was_g][1]
					num_errors_question_1 = questions_results[where_I_was_g][2]
					num_indecisions_question_1 = questions_results[where_I_was_g][3]
					num_already_question_1 = questions_results[where_I_was_g][4]
					time_of_question_1 = int(questions_results[where_I_was_g][5])
					num_answers_question_2 = questions_results[where_I_was_g+1][0]
					num_corrects_question_2 = questions_results[where_I_was_g+1][1]
					num_errors_question_2 = questions_results[where_I_was_g+1][2]
					num_indecisions_question_2 = questions_results[where_I_was_g+1][3]
					num_already_question_2 = questions_results[where_I_was_g+1][4]
					time_of_question_2 = int(questions_results[where_I_was_g+1][5])
					num_answers_question_3 = questions_results[where_I_was_g+2][0]
					num_corrects_question_3 = questions_results[where_I_was_g+2][1]
					num_errors_question_3 = questions_results[where_I_was_g+2][2]
					num_indecisions_question_3 = questions_results[where_I_was_g+2][3]
					num_already_question_3 = questions_results[where_I_was_g+2][4]
					time_of_question_3 = int(questions_results[where_I_was_g+2][5])
					num_answers_question_4 = questions_results[where_I_was_g+3][0]
					num_corrects_question_4 = questions_results[where_I_was_g+3][1]
					num_errors_question_4 = questions_results[where_I_was_g+3][2]
					num_indecisions_question_4 = questions_results[where_I_was_g+3][3]
					num_already_question_4 = questions_results[where_I_was_g+3][4]
					time_of_question_4 = int(questions_results[where_I_was_g+3][5])
					where_I_was_g += 4
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_4,num_corrects_question_4,num_errors_question_4,num_indecisions_question_4,num_already_question_4,time_of_question_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))
					db_self.conn.commit()												
					
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_4,num_corrects_question_4,num_errors_question_4,num_indecisions_question_4,num_already_question_4,time_of_question_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))
					db_self.conn.commit()
					
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_4,num_corrects_question_4,num_errors_question_4,num_indecisions_question_4,num_already_question_4,time_of_question_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))
					db_self.conn.commit()


	except Exception as e:
		print("--> An error occurred while adding a new Question")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef include_all_feeds_in_once_given_session(db_self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
	cdef:
		str quant_fed_ques_1 = None
		str grade_fed_ques_1 = None
		float val_fed_ques_1 = 0.0
		str quant_fed_ques_2 = None
		str grade_fed_ques_2 = None
		float val_fed_ques_2 = 0.0
		str quant_fed_ques_3 = None
		str grade_fed_ques_3 = None
		float val_fed_ques_3 = 0.0
		str quant_fed_ques_4 = None
		str grade_fed_ques_4 = None
		float val_fed_ques_4 = 0.0
		str quantity_feedback_session = the_session_feeds[0]
		str grade_feedback_session = the_session_feeds[1]
		float val_fed_session = the_session_feeds[2]
		str quant_fed_match_1 = None
		str grade_fed_match_1 = None
		float val_fed_match_1 = 0.0
		str quant_fed_match_2 = None
		str grade_fed_match_2 = None
		float val_fed_match_2 = 0.0
		str quant_fed_match_3 = None
		str grade_fed_match_3 = None
		float val_fed_match_3 = 0.0
		str quant_fed_match_4 = None
		str grade_fed_match_4 = None
		float val_fed_match_4 = 0.0
		str quant_fed_game_1 = None
		str grade_fed_game_1 = None
		float val_fed_game_1 = 0.0
		str quant_fed_game_2 = None
		str grade_fed_game_2 = None
		float val_fed_game_2 = 0.0
		str quant_fed_game_3 = None
		str grade_fed_game_3 = None
		float val_fed_game_3 = 0.0
		str quant_fed_game_4 = None
		str grade_fed_game_4 = None
		float val_fed_game_4 = 0.0
	
	try:
		#####################àserve mettere il WHERE quant_fed_match_1 isNull da aggiungere a !!! ...>>>>> kid_id = ? AND match_id = ?''',
		this_session_id = session_id
		print("this_session_id", this_session_id)
		
		db_self.curs.execute('''UPDATE Kids_Sessions SET
			quantity_feedback_session = ?, grade_feedback_session = ?, value_feedback_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(quantity_feedback_session,grade_feedback_session,val_fed_session,kid_id,this_session_id,))
		db_self.conn.commit()

		db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
			quantity_feedback_session = ?, grade_feedback_session = ?, value_feedback_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(quantity_feedback_session,grade_feedback_session,val_fed_session,kid_id,this_session_id,))
		db_self.conn.commit()
		db_self.curs.execute('''UPDATE Full_played_summary SET
			quantity_feedback_session = ?, grade_feedback_session = ?, value_feedback_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(quantity_feedback_session,grade_feedback_session,val_fed_session,kid_id,this_session_id,))
		db_self.conn.commit()

		db_self.curs.execute("SELECT match_id FROM Sessions_Matches WHERE session_id = ?", (this_session_id,))
		resp_m = db_self.curs.fetchall()
		if resp_m:
			s_matches = [item for sublist in resp_m for item in sublist]
		print("this_session_matches", s_matches)
		print("num_of_matches", len(s_matches))
		if len(s_matches)==1:
			this_num_of_matches = 1
			this_match_id = s_matches[0]
			quant_fed_match_1 = matches_feeds[0][0]
			grade_fed_match_1 = matches_feeds[0][1]
			val_fed_match_1 = matches_feeds[0][2]
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id,))
			db_self.conn.commit()
		
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id,))
			db_self.conn.commit()

		if len(s_matches)==2:
			this_num_of_matches = 2
			quant_fed_match_1 = matches_feeds[0][0]
			grade_fed_match_1 = matches_feeds[0][1]
			val_fed_match_1 = matches_feeds[0][2]
			quant_fed_match_2 = matches_feeds[1][0]
			grade_fed_match_2 = matches_feeds[1][1]
			val_fed_match_2 = matches_feeds[1][2]
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()		
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
		
		if len(s_matches)==3:
			this_num_of_matches = 1
			quant_fed_match_1 = matches_feeds[0][0]
			grade_fed_match_1 = matches_feeds[0][1]
			val_fed_match_1 = matches_feeds[0][2]
			quant_fed_match_2 = matches_feeds[1][0]
			grade_fed_match_2 = matches_feeds[1][1]
			val_fed_match_2 = matches_feeds[1][2]
			quant_fed_match_3 = matches_feeds[2][0]
			grade_fed_match_3 = matches_feeds[2][1]
			val_fed_match_3 = matches_feeds[2][2]
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			this_match_id_3 = s_matches[2]

			print("quant_fed_match_1 --> ", quant_fed_match_1)
			print("grade_fed_match_1 --> ", grade_fed_match_1)
			print("val_fed_match_1 --> ", val_fed_match_1)
			print("quant_fed_match_2 --> ", quant_fed_match_2)
			print("grade_fed_match_2 --> ", grade_fed_match_2)
			print("val_fed_match_2 --> ", val_fed_match_2)
			print("quant_fed_match_3 --> ", quant_fed_match_3)
			print("grade_fed_match_3 --> ", grade_fed_match_3)
			print("val_fed_match_3 --> ", val_fed_match_3)
			print("this_match_id_1 --> ", this_match_id_1)
			print("this_match_id_2 --> ", this_match_id_2)
			print("this_match_id_3 --> ", this_match_id_3)

			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()		
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
		
		if len(s_matches)==4:
			this_num_of_matches = 4
			quant_fed_match_1 = matches_feeds[0][0]
			grade_fed_match_1 = matches_feeds[0][1]
			val_fed_match_1 = matches_feeds[0][2]
			quant_fed_match_2 = matches_feeds[1][0]
			grade_fed_match_2 = matches_feeds[1][1]
			val_fed_match_2 = matches_feeds[1][2]
			quant_fed_match_3 = matches_feeds[2][0]
			grade_fed_match_3 = matches_feeds[2][1]
			val_fed_match_3 = matches_feeds[2][2]
			quant_fed_match_4 = matches_feeds[3][0]
			grade_fed_match_4 = matches_feeds[3][1]
			val_fed_match_4 = matches_feeds[3][2]
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			this_match_id_3 = s_matches[2]
			this_match_id_4 = s_matches[3]

			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()		
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(quant_fed_match_4,grade_fed_match_4,val_fed_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_4,grade_fed_match_4,val_fed_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_1,grade_fed_match_1,val_fed_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_2,grade_fed_match_2,val_fed_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_3,grade_fed_match_3,val_fed_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(quant_fed_match_4,grade_fed_match_4,val_fed_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()
		
		where_I_was_m = 0
		where_I_was_g = 0
		for mm in s_matches:
			db_self.curs.execute("SELECT game_id FROM Matches_Games WHERE match_id = ?", (mm,))
			resp_g = db_self.curs.fetchall()
			if resp_g:
				m_games = [item for sublist in resp_g for item in sublist]
			############## game = 1
			if len(m_games)==1:
				quant_fed_game_1 = games_feeds[where_I_was_m][0]
				grade_fed_game_1 = games_feeds[where_I_was_m][1]
				val_fed_game_1 = games_feeds[where_I_was_m][2]
				this_game_id = m_games[0]
				where_I_was_m += 1
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id,))
				db_self.conn.commit()

				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id,))
				db_self.conn.commit()
				
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id,))
				db_self.conn.commit()

			if len(m_games)==2:
				quant_fed_game_1 = games_feeds[where_I_was_m][0]
				grade_fed_game_1 = games_feeds[where_I_was_m][1]
				val_fed_game_1 = games_feeds[where_I_was_m][2]
				quant_fed_game_2 = games_feeds[where_I_was_m+1][0]
				grade_fed_game_2 = games_feeds[where_I_was_m+1][1]
				val_fed_game_2 = games_feeds[where_I_was_m+1][2]
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				where_I_was_m += 2
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()

				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()

			if len(m_games)==3:
				quant_fed_game_1 = games_feeds[where_I_was_m][0]
				grade_fed_game_1 = games_feeds[where_I_was_m][1]
				val_fed_game_1 = games_feeds[where_I_was_m][2]
				quant_fed_game_2 = games_feeds[where_I_was_m+1][0]
				grade_fed_game_2 = games_feeds[where_I_was_m+1][1]
				val_fed_game_2 = games_feeds[where_I_was_m+1][2]
				quant_fed_game_3 = games_feeds[where_I_was_m+2][0]
				grade_fed_game_3 = games_feeds[where_I_was_m+2][1]
				val_fed_game_3 = games_feeds[where_I_was_m+2][2]
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				this_game_id_3 = m_games[2]
				where_I_was_m += 3
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()

				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()

			if len(m_games)==4:
				quant_fed_game_1 = games_feeds[where_I_was_m][0]
				grade_fed_game_1 = games_feeds[where_I_was_m][1]
				val_fed_game_1 = games_feeds[where_I_was_m][2]
				quant_fed_game_2 = games_feeds[where_I_was_m+1][0]
				grade_fed_game_2 = games_feeds[where_I_was_m+1][1]
				val_fed_game_2 = games_feeds[where_I_was_m+1][2]
				quant_fed_game_3 = games_feeds[where_I_was_m+2][0]
				grade_fed_game_3 = games_feeds[where_I_was_m+2][1]
				val_fed_game_3 = games_feeds[where_I_was_m+2][2]
				quant_fed_game_4 = games_feeds[where_I_was_m+3][0]
				grade_fed_game_4 = games_feeds[where_I_was_m+3][1]
				val_fed_game_4 = games_feeds[where_I_was_m+3][2]
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				this_game_id_3 = m_games[2]
				this_game_id_4 = m_games[3]
				where_I_was_m += 4
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					quantity_feedback_game = ?,grade_feedback_game = ?,value_feedback_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(quant_fed_game_4,grade_fed_game_4,val_fed_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_4,grade_fed_game_4,val_fed_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()

				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_1,grade_fed_game_1,val_fed_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_2,grade_fed_game_2,val_fed_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_3,grade_fed_game_3,val_fed_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(quant_fed_game_4,grade_fed_game_4,val_fed_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()
			
			for gg in m_games:
				db_self.curs.execute("SELECT question_id FROM Games_Questions WHERE game_id = ?", (gg,))
				resp_qs = db_self.curs.fetchall()
				if resp_qs:
					g_questions = [item for sublist in resp_qs for item in sublist]

				if len(g_questions)==1:
					quant_fed_ques_1 = questions_feeds[where_I_was_g][0]
					grade_fed_ques_1 = questions_feeds[where_I_was_g][1]
					val_fed_ques_1 = questions_feeds[where_I_was_g][2]
					this_ques_id_1 = g_questions[0]
					where_I_was_g += 1
					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?,value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
				if len(g_questions)==2:
					quant_fed_ques_1 = questions_feeds[where_I_was_g][0]
					grade_fed_ques_1 = questions_feeds[where_I_was_g][1]
					val_fed_ques_1 = questions_feeds[where_I_was_g][2]
					quant_fed_ques_2 = questions_feeds[where_I_was_g+1][0]
					grade_fed_ques_2 = questions_feeds[where_I_was_g+1][1]
					val_fed_ques_2 = questions_feeds[where_I_was_g+1][2]					
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					where_I_was_g += 2
					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?,value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
				if len(g_questions)==3:
					quant_fed_ques_1 = questions_feeds[where_I_was_g][0]
					grade_fed_ques_1 = questions_feeds[where_I_was_g][1]
					val_fed_ques_1 = questions_feeds[where_I_was_g][2]
					quant_fed_ques_2 = questions_feeds[where_I_was_g+1][0]
					grade_fed_ques_2 = questions_feeds[where_I_was_g+1][1]
					val_fed_ques_2 = questions_feeds[where_I_was_g+1][2]					
					quant_fed_ques_3 = questions_feeds[where_I_was_g+2][0]
					grade_fed_ques_3 = questions_feeds[where_I_was_g+2][1]
					val_fed_ques_3 = questions_feeds[where_I_was_g+2][2]	
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					this_ques_id_3 = g_questions[2]
					where_I_was_g +=3
					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?,value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
				if len(g_questions)==4:
					quant_fed_ques_1 = questions_feeds[where_I_was_g][0]
					grade_fed_ques_1 = questions_feeds[where_I_was_g][1]
					val_fed_ques_1 = questions_feeds[where_I_was_g][2]
					quant_fed_ques_2 = questions_feeds[where_I_was_g+1][0]
					grade_fed_ques_2 = questions_feeds[where_I_was_g+1][1]
					val_fed_ques_2 = questions_feeds[where_I_was_g+1][2]					
					quant_fed_ques_3 = questions_feeds[where_I_was_g+2][0]
					grade_fed_ques_3 = questions_feeds[where_I_was_g+2][1]
					val_fed_ques_3 = questions_feeds[where_I_was_g+2][2]	
					quant_fed_ques_4 = questions_feeds[where_I_was_g+3][0]
					grade_fed_ques_4 = questions_feeds[where_I_was_g+3][1]
					val_fed_ques_4 = questions_feeds[where_I_was_g+3][2]	
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					this_ques_id_3 = g_questions[2]
					this_ques_id_4 = g_questions[3]
					where_I_was_g += 4
					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?,value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_1,grade_fed_ques_1,val_fed_ques_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_2,grade_fed_ques_2,val_fed_ques_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_3,grade_fed_ques_3,val_fed_ques_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()

					db_self.curs.execute('''UPDATE Kids_Questions SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kg.kid_id, kg.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kg USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(quant_fed_ques_4,grade_fed_ques_4,val_fed_ques_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))				
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						quantity_feedback_question = ?,grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_4,grade_fed_ques_4,val_fed_ques_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						quantity_feedback_question = ?, grade_feedback_question = ?, value_feedback_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(quant_fed_ques_4,grade_fed_ques_4,val_fed_ques_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))
					db_self.conn.commit()

	except Exception as e:
		print("--> An error occurred while adding a new Question")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef include_all_results_in_once_given_session(db_self, int kid_id, int session_id, float time_sess, list questions_results, list matches_results, list games_results):
	print("SIAMO IN include_all_results_in_once given session!!!")
	print("per prima cosa cosa ricevo??")
	print("kid_id is {} | float time_sess is {} | list questions_results is {} | list matches_results is {} | list games_results is {}".format(kid_id, time_sess, questions_results, matches_results, games_results))

	print("ricevuto")
	print(kid_id)
	print(session_id)
	print(time_sess)
	print(questions_results)
	print(matches_results)
	print(games_results)
	print("ricevuto")
	cdef:
		int time_of_session = 0
		int num_answers_game_1 = 0
		int num_corrects_game_1 = 0
		int num_errors_game_1 = 0
		int num_indecisions_game_1 = 0
		int num_already_game_1 = 0
		int num_interrupts_game_1 = 0
		int time_of_game_1 = 0
		int num_answers_game_2 = 0
		int num_corrects_game_2 = 0
		int num_errors_game_2 = 0
		int num_indecisions_game_2 = 0
		int num_interrupts_game_2 = 0
		int num_already_game_2 = 0
		int time_of_game_2 = 0
		int num_answers_game_3 = 0
		int num_corrects_game_3 = 0
		int num_errors_game_3 = 0
		int num_indecisions_game_3 = 0
		int num_interrupts_game_3 = 0
		int num_already_game_3 = 0
		int time_of_game_3 = 0
		int num_answers_game_4 = 0
		int num_corrects_game_4 = 0
		int num_errors_game_4 = 0
		int num_indecisions_game_4 = 0
		int num_interrupts_game_4 = 0
		int num_already_game_4 = 0
		int time_of_game_4 = 0	
		int num_answers_match_1 = 0
		int num_corrects_match_1 = 0
		int num_errors_match_1 = 0
		int num_indecisions_match_1 = 0
		int num_already_match_1 = 0
		int time_of_match_1 = 0
		int num_answers_match_2 = 0
		int num_corrects_match_2 = 0
		int num_errors_match_2 = 0
		int num_indecisions_match_2 = 0
		int num_already_match_2 = 0
		int time_of_match_2 = 0
		int num_answers_match_3 = 0
		int num_corrects_match_3 = 0
		int num_errors_match_3 = 0
		int num_already_match_3 = 0
		int time_of_match_3 = 0
		int num_answers_match_4 = 0
		int num_corrects_match_4 = 0
		int num_errors_match_4 = 0
		int num_indecisions_match_4 = 0
		int num_already_match_4 = 0
		int time_of_match_4 = 0
		int num_answers_question_1 = 0
		int num_corrects_question_1 = 0
		int num_errors_question_1 = 0
		int num_indecisions_question_1 = 0
		int num_already_question_1 = 0
		int time_of_question_1 = 0
		int num_answers_question_2 = 0
		int num_corrects_question_2 = 0
		int num_errors_question_2 = 0
		int num_indecisions_question_2 = 0
		int num_already_question_2 = 0
		int time_of_question_2 = 0
		int num_answers_question_3 = 0
		int num_corrects_question_3 = 0
		int num_errors_question_3 = 0
		int num_indecisions_question_3 = 0
		int num_already_question_3 = 0
		int time_of_question_3 = 0
		int num_answers_question_4 = 0
		int num_corrects_question_4 = 0
		int num_errors_question_4 = 0
		int num_indecisions_question_4 = 0
		int num_already_question_4 = 0
		int time_of_question_4 = 0	

	try:
		time_of_session = int(time_sess)
		print("time_of_sessiontime_of_sessiontime_of_sessiontime_of_sessiontime_of_sessiontime_of_session time_of_session = > ", time_of_session)
		#####################àserve mettere il WHERE quant_fed_match_1 isNull da aggiungere a !!! ...>>>>> kid_id = ? AND match_id = ?''',
		this_session_id = session_id
		print("this_session_id", this_session_id)
		
		db_self.curs.execute('''UPDATE Kids_Sessions SET
			time_of_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(time_of_session,kid_id,this_session_id,))
		db_self.conn.commit()
		db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
			time_of_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(time_of_session,kid_id,this_session_id,))
		db_self.conn.commit()
		db_self.curs.execute('''UPDATE Full_played_summary SET
			time_of_session = ?
			WHERE kid_id = ? AND session_id = ?''',
			(time_of_session,kid_id,this_session_id,))
		db_self.conn.commit()
		#db_self.curs.execute("UPDATE Full_played_summary SET time_of_session=cast(time_of_session) AS INTEGER")
		#db_self.conn.commit()
		
		db_self.curs.execute("SELECT match_id FROM Sessions_Matches WHERE session_id = ?", (this_session_id,))
		resp_m = db_self.curs.fetchall()
		if resp_m:
			s_matches = [item for sublist in resp_m for item in sublist]
		print("this_session_matches", s_matches)
		print("num_of_matches", len(s_matches))
		if len(s_matches)==1:
			this_num_of_matches = 1
			this_match_id_1 = s_matches[0]
			num_answers_match_1 = matches_results[0][0]
			num_corrects_match_1 = matches_results[0][1]
			num_errors_match_1 = matches_results[0][2]
			num_indecisions_match_1 = matches_results[0][3]
			num_already_match_1 = matches_results[0][4]
			time_of_match_1 = matches_results[0][5]
			print("eccoci time_of_match_1 = ", time_of_match_1)

			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()

		if len(s_matches)==2:
			this_num_of_matches = 2
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			num_answers_match_1 = matches_results[0][0]
			num_corrects_match_1 = matches_results[0][1]
			num_errors_match_1 = matches_results[0][2]
			num_indecisions_match_1 = matches_results[0][3]
			num_already_match_1 = matches_results[0][4]
			time_of_match_1 = matches_results[0][5]
			print("eccoci time_of_match_1", time_of_match_1)
			num_answers_match_2 = matches_results[1][0]
			num_corrects_match_2 = matches_results[1][1]
			num_errors_match_2 = matches_results[1][2]
			num_indecisions_match_2 = matches_results[1][3]
			num_already_match_2 = matches_results[1][4]
			time_of_match_2 = matches_results[1][5]
			print("eccoci time_of_match_2", time_of_match_2)

			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()

		if len(s_matches)==3:
			this_num_of_matches = 3
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			this_match_id_3 = s_matches[2]
			num_answers_match_1 = matches_results[0][0]
			num_corrects_match_1 = matches_results[0][1]
			num_errors_match_1 = matches_results[0][2]
			num_indecisions_match_1 = matches_results[0][3]
			num_already_match_1 = matches_results[0][4]
			time_of_match_1 = matches_results[0][5]
			print("eccoci time_of_match_1", time_of_match_1)
			num_answers_match_2 = matches_results[1][0]
			num_corrects_match_2 = matches_results[1][1]
			num_errors_match_2 = matches_results[1][2]
			num_indecisions_match_2 = matches_results[1][3]
			num_already_match_2 = matches_results[1][4]
			time_of_match_2 = matches_results[1][5]
			print("eccoci time_of_match_2", time_of_match_2)
			num_answers_match_3 = matches_results[2][0]
			num_corrects_match_3 = matches_results[2][1]
			num_errors_match_3 = matches_results[2][2]
			num_indecisions_match_3 = matches_results[2][3]
			num_already_match_3 = matches_results[2][4]
			time_of_match_3 = matches_results[2][5]
			print("eccoci time_of_match_3", time_of_match_3)
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()

		if len(s_matches)==4:
			this_num_of_matches = 4
			this_match_id_1 = s_matches[0]
			this_match_id_2 = s_matches[1]
			this_match_id_3 = s_matches[2]
			this_match_id_4 = s_matches[3]
			num_answers_match_1 = matches_results[0][0]
			num_corrects_match_1 = matches_results[0][1]
			num_errors_match_1 = matches_results[0][2]
			num_indecisions_match_1 = matches_results[0][3]
			num_already_match_1 = matches_results[0][4]
			time_of_match_1 = matches_results[0][5]
			print("eccoci time_of_match_1", time_of_match_1)
			num_answers_match_2 = matches_results[1][0]
			num_corrects_match_2 = matches_results[1][1]
			num_errors_match_2 = matches_results[1][2]
			num_indecisions_match_2 = matches_results[1][3]
			num_already_match_2 = matches_results[1][4]
			time_of_match_2 = matches_results[1][5]
			print("eccoci time_of_match_2", time_of_match_2)
			num_answers_match_3 = matches_results[2][0]
			num_corrects_match_3 = matches_results[2][1]
			num_errors_match_3 = matches_results[2][2]
			num_indecisions_match_3 = matches_results[2][3]
			num_already_match_3 = matches_results[2][4]
			time_of_match_3 = matches_results[2][5]
			print("eccoci time_of_match_3", time_of_match_3)
			num_answers_match_4 = matches_results[3][0]
			num_corrects_match_4 = matches_results[3][1]
			num_errors_match_4 = matches_results[3][2]
			num_indecisions_match_4 = matches_results[3][3]
			num_already_match_4 = matches_results[3][4]
			time_of_match_4 = matches_results[3][5]
			print("eccoci time_of_match_4", time_of_match_4)

			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Kids_Matches SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE (kid_id,match_id) IN (
					SELECT km.kid_id, km.match_id
					FROM  Full_played_summary fps JOIN Kids_Matches km USING(match_id)
					WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ?
					)''',
					(num_answers_match_4,num_corrects_match_4,num_errors_match_4,num_indecisions_match_4,num_already_match_4,time_of_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()

			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_4,num_corrects_match_4,num_errors_match_4,num_indecisions_match_4,num_already_match_4,time_of_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()
			
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_1,num_corrects_match_1,num_errors_match_1,num_indecisions_match_1,num_already_match_1,time_of_match_1,kid_id,this_session_id,this_match_id_1,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_2,num_corrects_match_2,num_errors_match_2,num_indecisions_match_2,num_already_match_2,time_of_match_2,kid_id,this_session_id,this_match_id_2,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_3,num_corrects_match_3,num_errors_match_3,num_indecisions_match_3,num_already_match_3,time_of_match_3,kid_id,this_session_id,this_match_id_3,))
			db_self.conn.commit()
			db_self.curs.execute('''UPDATE Full_played_summary SET
				num_answers_match = ?, num_corrects_match = ?, num_errors_match = ?, num_indecisions_match = ?, num_already_match = ?, time_of_match = ?
				WHERE kid_id = ? AND session_id = ? AND match_id = ?''',
				(num_answers_match_4,num_corrects_match_4,num_errors_match_4,num_indecisions_match_4,num_already_match_4,time_of_match_4,kid_id,this_session_id,this_match_id_4,))
			db_self.conn.commit()
		
		where_I_was_m = 0
		where_I_was_g = 0
		for mm in s_matches:
			db_self.curs.execute("SELECT game_id FROM Matches_Games WHERE match_id = ?", (mm,))
			resp_g = db_self.curs.fetchall()
			if resp_g:
				m_games = [item for sublist in resp_g for item in sublist]
			############## game = 1
			if len(m_games)==1:
				this_num_of_games = 1
				this_game_id_1 = m_games[where_I_was_m]
				num_answers_game_1 = games_results[where_I_was_m][0]
				num_corrects_game_1 = games_results[where_I_was_m][1]
				num_errors_game_1 = games_results[where_I_was_m][2]
				num_indecisions_game_1 = games_results[where_I_was_m][3]
				num_already_game_1 = games_results[where_I_was_m][4]
				time_of_game_1 = int(games_results[where_I_was_m][5])
				num_interruptions_game_1 = games_results[where_I_was_m][6]
				where_I_was_m += 1
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()			
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
			if len(m_games)==2:
				this_num_of_games = 2
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				num_answers_game_1 = games_results[where_I_was_m][0]
				num_corrects_game_1 = games_results[where_I_was_m][1]
				num_errors_game_1 = games_results[where_I_was_m][2]
				num_indecisions_game_1 = games_results[where_I_was_m][3]
				num_already_game_1 = games_results[where_I_was_m][4]
				time_of_game_1 = int(games_results[where_I_was_m][5])
				num_interruptions_game_1 = games_results[where_I_was_m][6]
				num_answers_game_2 = games_results[where_I_was_m+1][0]
				num_corrects_game_2 = games_results[where_I_was_m+1][1]
				num_errors_game_2 = games_results[where_I_was_m+1][2]
				num_indecisions_game_2 = games_results[where_I_was_m+1][3]
				num_already_game_2 = games_results[where_I_was_m+1][4]
				time_of_game_2 = int(games_results[where_I_was_m+1][5])
				num_interruptions_game_2 = games_results[where_I_was_m+1][6]
				where_I_was_m += 2
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,num_interruptions_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()			
				
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,num_interruptions_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,num_interruptions_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()

			if len(m_games)==3:
				this_num_of_games = 3
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				this_game_id_3 = m_games[2]
				num_answers_game_1 = games_results[where_I_was_m][0]
				num_corrects_game_1 = games_results[where_I_was_m][1]
				num_errors_game_1 = games_results[where_I_was_m][2]
				num_indecisions_game_1 = games_results[where_I_was_m][3]
				num_already_game_1 = games_results[where_I_was_m][4]
				time_of_game_1 = int(games_results[where_I_was_m][5])
				num_interruptions_game_1 = games_results[where_I_was_m][6]
				num_answers_game_2 = games_results[where_I_was_m+1][0]
				num_corrects_game_2 = games_results[where_I_was_m+1][1]
				num_errors_game_2 = games_results[where_I_was_m+1][2]
				num_indecisions_game_2 = games_results[where_I_was_m+1][3]
				num_already_game_2 = games_results[where_I_was_m+1][4]
				time_of_game_2 = int(games_results[where_I_was_m+1][5])
				num_interruptions_game_2 = games_results[where_I_was_m][6]
				num_answers_game_3 = games_results[where_I_was_m+2][0]
				num_corrects_game_3 = games_results[where_I_was_m+2][1]
				num_errors_game_3 = games_results[where_I_was_m+2][2]
				num_indecisions_game_3 = games_results[where_I_was_m+2][3]
				num_already_game_3 = games_results[where_I_was_m+2][4]
				time_of_game_3 = int(games_results[where_I_was_m+2][5])
				num_interruptions_game_3 = games_results[where_I_was_m+2][6]
				where_I_was_m += 3
				
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,num_interruptions_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,num_interruptions_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()						
				
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,num_interruptions_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,num_interruptions_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,num_interruptions_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?num_interruptions_game_,
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,num_interruptions_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()

			if len(m_games)==4:
				this_num_of_games = 4
				this_game_id_1 = m_games[0]
				this_game_id_2 = m_games[1]
				this_game_id_3 = m_games[2]
				this_game_id_4 = m_games[3]
				num_answers_game_1 = games_results[where_I_was_m][0]
				num_corrects_game_1 = games_results[where_I_was_m][1]
				num_errors_game_1 = games_results[where_I_was_m][2]
				num_indecisions_game_1 = games_results[where_I_was_m][3]
				num_already_game_1 = games_results[where_I_was_m][4]
				time_of_game_1 = int(games_results[where_I_was_m][5])
				num_interruptions_game_1 = games_results[where_I_was_m][6]
				num_answers_game_2 = games_results[where_I_was_m+1][0]
				num_corrects_game_2 = games_results[where_I_was_m+1][1]
				num_errors_game_2 = games_results[where_I_was_m+1][2]
				num_indecisions_game_2 = games_results[where_I_was_m+1][3]
				num_already_game_2 = games_results[where_I_was_m+1][4]
				time_of_game_2 = int(games_results[where_I_was_m+1][5])
				num_interruptions_game_2 = games_results[where_I_was_m+1][6]
				num_answers_game_3 = games_results[where_I_was_m+2][0]
				num_corrects_game_3 = games_results[where_I_was_m+2][1]
				num_errors_game_3 = games_results[where_I_was_m+2][2]
				num_indecisions_game_3 = games_results[where_I_was_m+2][3]
				num_already_game_3 = games_results[where_I_was_m+2][4]
				time_of_game_3 = int(games_results[where_I_was_m+2][5])
				num_interruptions_game_3 = games_results[where_I_was_m+2][6]
				num_answers_game_4 = games_results[where_I_was_m+3][0]
				num_corrects_game_4 = games_results[where_I_was_m+3][1]
				num_errors_game_4 = games_results[where_I_was_m+3][2]
				num_indecisions_game_4 = games_results[where_I_was_m+3][3]
				num_already_game_4 = games_results[where_I_was_m+3][4]
				time_of_game_4 = int(games_results[where_I_was_m+3][5])
				num_interruptions_game_4 = games_results[where_I_was_m+3][6]
				where_I_was_m += 4
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,num_interruptions_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,num_interruptions_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Kids_Games SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE (kid_id,game_id) IN (
						SELECT kg.kid_id, kg.game_id
						FROM  Full_played_summary fps JOIN Kids_Games kg USING(game_id)
						WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ?
						)''',
						(num_answers_game_4,num_corrects_game_4,num_errors_game_4,num_indecisions_game_4,num_already_game_4,time_of_game_4,num_interruptions_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()									

				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,num_interruptions_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,num_interruptions_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_4,num_corrects_game_4,num_errors_game_4,num_indecisions_game_4,num_already_game_4,time_of_game_4,num_interruptions_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()
				
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_1,num_corrects_game_1,num_errors_game_1,num_indecisions_game_1,num_already_game_1,time_of_game_1,num_interruptions_game_1,kid_id,this_session_id,mm,this_game_id_1,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_2,num_corrects_game_2,num_errors_game_2,num_indecisions_game_2,num_already_game_2,time_of_game_2,num_interruptions_game_2,kid_id,this_session_id,mm,this_game_id_2,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_3,num_corrects_game_3,num_errors_game_3,num_indecisions_game_3,num_already_game_3,time_of_game_3,num_interruptions_game_3,kid_id,this_session_id,mm,this_game_id_3,))
				db_self.conn.commit()
				db_self.curs.execute('''UPDATE Full_played_summary SET
					num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?, num_interruptions_game = ?
					WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ?''',
					(num_answers_game_4,num_corrects_game_4,num_errors_game_4,num_indecisions_game_4,num_already_game_4,time_of_game_4,num_interruptions_game_4,kid_id,this_session_id,mm,this_game_id_4,))
				db_self.conn.commit()

			for gg in m_games:
				db_self.curs.execute("SELECT question_id FROM Games_Questions WHERE game_id = ?", (gg,))
				resp_qs = db_self.curs.fetchall()
				if resp_qs:
					g_questions = [item for sublist in resp_qs for item in sublist]

				if len(g_questions)==1:
					this_num_of_questions = 1
					this_ques_id_1 = g_questions[0]
					num_answers_question_1 = questions_results[where_I_was_g][0]
					num_corrects_question_1 = questions_results[where_I_was_g][1]
					num_errors_question_1 = questions_results[where_I_was_g][2]
					num_indecisions_question_1 = questions_results[where_I_was_g][3]
					num_already_question_1 = questions_results[where_I_was_g][4]
					time_of_question_1 = int(questions_results[where_I_was_g][5])
					where_I_was_g += 1
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
				
				elif len(g_questions)==2:
					this_num_of_questions = 2
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					num_answers_question_1 = questions_results[where_I_was_g][0]
					num_corrects_question_1 = questions_results[where_I_was_g][1]
					num_errors_question_1 = questions_results[where_I_was_g][2]
					num_indecisions_question_1 = questions_results[where_I_was_g][3]
					num_already_question_1 = questions_results[where_I_was_g][4]
					time_of_question_1 = int(questions_results[where_I_was_g][5])
					num_answers_question_2 = questions_results[where_I_was_g+1][0]
					num_corrects_question_2 = questions_results[where_I_was_g+1][1]
					num_errors_question_2 = questions_results[where_I_was_g+1][2]
					num_indecisions_question_2 = questions_results[where_I_was_g+1][3]
					num_already_question_2 = questions_results[where_I_was_g+1][4]
					time_of_question_2 = int(questions_results[where_I_was_g+1][5])
					where_I_was_g += 2
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()				

					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
				
				elif len(g_questions)==3:
					this_num_of_questions = 3
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					this_ques_id_3 = g_questions[2]
					num_answers_question_1 = questions_results[where_I_was_g][0]
					num_corrects_question_1 = questions_results[where_I_was_g][1]
					num_errors_question_1 = questions_results[where_I_was_g][2]
					num_indecisions_question_1 = questions_results[where_I_was_g][3]
					num_already_question_1 = questions_results[where_I_was_g][4]
					time_of_question_1 = int(questions_results[where_I_was_g][5])
					num_answers_question_2 = questions_results[where_I_was_g+1][0]
					num_corrects_question_2 = questions_results[where_I_was_g+1][1]
					num_errors_question_2 = questions_results[where_I_was_g+1][2]
					num_indecisions_question_2 = questions_results[where_I_was_g+1][3]
					num_already_question_2 = questions_results[where_I_was_g+1][4]
					time_of_question_2 = int(questions_results[where_I_was_g+1][5])
					num_answers_question_3 = questions_results[where_I_was_g+2][0]
					num_corrects_question_3 = questions_results[where_I_was_g+2][1]
					num_errors_question_3 = questions_results[where_I_was_g+2][2]
					num_indecisions_question_3 = questions_results[where_I_was_g+2][3]
					num_already_question_3 = questions_results[where_I_was_g+2][4]
					time_of_question_3 = int(questions_results[where_I_was_g+2][5])
					where_I_was_g += 3
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()								
					
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()

				elif len(g_questions)==4:
					this_num_of_questions = 4
					this_ques_id_1 = g_questions[0]
					this_ques_id_2 = g_questions[1]
					this_ques_id_3 = g_questions[2]
					this_ques_id_4 = g_questions[3]
					num_answers_question_1 = questions_results[where_I_was_g][0]
					num_corrects_question_1 = questions_results[where_I_was_g][1]
					num_errors_question_1 = questions_results[where_I_was_g][2]
					num_indecisions_question_1 = questions_results[where_I_was_g][3]
					num_already_question_1 = questions_results[where_I_was_g][4]
					time_of_question_1 = int(questions_results[where_I_was_g][5])
					num_answers_question_2 = questions_results[where_I_was_g+1][0]
					num_corrects_question_2 = questions_results[where_I_was_g+1][1]
					num_errors_question_2 = questions_results[where_I_was_g+1][2]
					num_indecisions_question_2 = questions_results[where_I_was_g+1][3]
					num_already_question_2 = questions_results[where_I_was_g+1][4]
					time_of_question_2 = int(questions_results[where_I_was_g+1][5])
					num_answers_question_3 = questions_results[where_I_was_g+2][0]
					num_corrects_question_3 = questions_results[where_I_was_g+2][1]
					num_errors_question_3 = questions_results[where_I_was_g+2][2]
					num_indecisions_question_3 = questions_results[where_I_was_g+2][3]
					num_already_question_3 = questions_results[where_I_was_g+2][4]
					time_of_question_3 = int(questions_results[where_I_was_g+2][5])
					num_answers_question_4 = questions_results[where_I_was_g+3][0]
					num_corrects_question_4 = questions_results[where_I_was_g+3][1]
					num_errors_question_4 = questions_results[where_I_was_g+3][2]
					num_indecisions_question_4 = questions_results[where_I_was_g+3][3]
					num_already_question_4 = questions_results[where_I_was_g+3][4]
					time_of_question_4 = int(questions_results[where_I_was_g+3][5])
					where_I_was_g += 4
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Kids_Questions SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE (kid_id,question_id) IN (
							SELECT kq.kid_id, kq.question_id
							FROM  Full_played_summary fps JOIN Kids_Questions kq USING(question_id)
							WHERE fps.kid_id = ? AND fps.session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?
							)''',
							(num_answers_question_4,num_corrects_question_4,num_errors_question_4,num_indecisions_question_4,num_already_question_4,time_of_question_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))
					db_self.conn.commit()												
					
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_Recap_giga SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_4,num_corrects_question_4,num_errors_question_4,num_indecisions_question_4,num_already_question_4,time_of_question_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))
					db_self.conn.commit()
					
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_1,num_corrects_question_1,num_errors_question_1,num_indecisions_question_1,num_already_question_1,time_of_question_1,kid_id,this_session_id,mm,gg,this_ques_id_1,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_2,num_corrects_question_2,num_errors_question_2,num_indecisions_question_2,num_already_question_2,time_of_question_2,kid_id,this_session_id,mm,gg,this_ques_id_2,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_3,num_corrects_question_3,num_errors_question_3,num_indecisions_question_3,num_already_question_3,time_of_question_3,kid_id,this_session_id,mm,gg,this_ques_id_3,))
					db_self.conn.commit()
					db_self.curs.execute('''UPDATE Full_played_summary SET
						num_answers_question = ?, num_corrects_question = ?, num_errors_question = ?, num_indecisions_question = ?, num_already_question = ?, time_of_question = ?
						WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? ''',
						(num_answers_question_4,num_corrects_question_4,num_errors_question_4,num_indecisions_question_4,num_already_question_4,time_of_question_4,kid_id,this_session_id,mm,gg,this_ques_id_4,))
					db_self.conn.commit()

	except Exception as e:
		print("--> An error occurred while adding all results of a session")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)