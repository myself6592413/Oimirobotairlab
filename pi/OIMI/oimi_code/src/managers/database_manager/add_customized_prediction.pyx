""" Oimi robot database_manager,
	add_customized_prediction 
	
	Notes:
		For other detailed info, look at dabatase_doc

	Created by Colombo Giacomo, Politecnico di Milano 2020 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import os 
import sys
if '/home/pi/OIMI/oimi_code/src/learning/game_classification/' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/learning/game_classification/')
import random
from typing import Optional
import create_session_with_prediction as cswp
import db_extern_methods_two as dbem2
from add_prediction cimport DatabaseManager_2
cimport add_prediction_2 as adp2
cimport add_prediction_3 as adp3
cimport security_prediction as sp
cimport database_controller_add_into as dca
cimport database_controller_add_into_part_4 as dca4
cimport add_customized_prediction_second_half as acpsh 
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class DatabaseManager_3(DatabaseManager_2):
	def __cinit__(self, kid_id):
		self.kid_id = kid_id
		#self.take_csvs()

	cpdef add_new_constrained_Session_table_with_kid(self, int kid_id, int mandatory_imposition, int mandatory_need, customized):
		print("sono in add_new_constrained_Session_table_with_kid")
		print("kid_id" , kid_id)
		self.num_of_matches = 0
		self.mandatory_impositions, self.mandatory_needs = mandatory_imposition, mandatory_need #ma tanto li ignoro ho gia i constraint col limite senno è un macello
		self.movements_allowed, self.extra_time_tolerance, self.body_enabled, self.exhortations, self.audio_slow = 0,0,0,0,0
		self.new_deleletion_index, self.repeat_intro, self.rules_explication, self.repeat_question = 0,0,0,0
		print("self.new_deleletion_index   ", self.new_deleletion_index)
		self.disposable = 0
		self.desirable_time = 320
		self.stress_flag = 0
		self.led_brightness = 'std'
		self.num_of_matches = 0
		self.complexity = ''
		self.status = ''
		self.matches_order_diff = ''
		self.order_quan_games = ''
		self.difficulty_mat_1 = ''
		self.difficulty_mat_2 = ''
		self.difficulty_mat_3 = ''
		self.difficulty_mat_4 = ''
		self.diff_m1_g1 = ''
		self.diff_m1_g2 = ''
		self.diff_m1_g3 = ''
		self.diff_m1_g4 = ''
		self.diff_m2_g1 = ''
		self.diff_m2_g2 = ''
		self.diff_m2_g3 = ''
		self.diff_m2_g4 = ''
		self.diff_m3_g1 = ''
		self.diff_m3_g2 = ''
		self.diff_m3_g3 = ''
		self.diff_m3_g4 = ''		
		self.diff_m4_g1 = ''
		self.diff_m4_g2 = ''
		self.diff_m4_g3 = ''
		self.diff_m4_g4 = ''
		self.kind_m1_g1 = ''
		self.kind_m1_g2 = ''
		self.kind_m1_g3 = ''
		self.kind_m1_g4 = ''
		self.kind_m2_g1 = ''
		self.kind_m2_g2 = ''
		self.kind_m2_g3 = ''
		self.kind_m2_g4 = ''
		self.kind_m3_g1 = ''
		self.kind_m3_g2 = ''
		self.kind_m3_g3 = ''
		self.kind_m3_g4 = ''
		self.kind_m4_g1 = ''
		self.kind_m4_g2 = ''
		self.kind_m4_g3 = ''
		self.kind_m4_g4 = ''
		self.m1_questions_g1 = 0
		self.m1_questions_g2 = 0
		self.m1_questions_g3 = 0
		self.m1_questions_g4 = 0
		self.m2_questions_g1 = 0
		self.m2_questions_g2 = 0
		self.m2_questions_g3 = 0
		self.m2_questions_g4 = 0
		self.m3_questions_g1 = 0
		self.m3_questions_g2 = 0
		self.m3_questions_g3 = 0
		self.m3_questions_g4 = 0
		self.m4_questions_g1 = 0
		self.m4_questions_g2 = 0
		self.m4_questions_g3 = 0
		self.m4_questions_g4 = 0
		self.order_quan_quest_m1 = ''
		self.order_quan_quest_m2 = ''
		self.order_quan_quest_m3 = ''
		self.order_quan_quest_m4 = ''
		self.order_diff_games_m1 = ''
		self.order_diff_games_m2 = ''
		self.order_diff_games_m3 = ''
		self.order_diff_games_m4 = ''		
		self.m1_g1_order_diff_ques = ''
		self.m1_g2_order_diff_ques = ''
		self.m1_g3_order_diff_ques = ''
		self.m1_g4_order_diff_ques = ''
		self.m2_g1_order_diff_ques = ''
		self.m2_g2_order_diff_ques = ''
		self.m2_g3_order_diff_ques = ''
		self.m2_g4_order_diff_ques = ''
		self.m3_g1_order_diff_ques = ''
		self.m3_g2_order_diff_ques = ''
		self.m3_g3_order_diff_ques = ''
		self.m3_g4_order_diff_ques = ''
		self.m4_g1_order_diff_ques = ''
		self.m4_g2_order_diff_ques = ''
		self.m4_g3_order_diff_ques = ''
		self.m4_g4_order_diff_ques = ''
		self.type_m1_g1_1r = ''
		self.type_m1_g1_2r = ''
		self.type_m1_g1_3r = ''
		self.type_m1_g1_4r = ''
		self.type_m1_g2_1r = ''
		self.type_m1_g2_2r = ''
		self.type_m1_g2_3r = ''
		self.type_m1_g2_4r = ''
		self.type_m1_g3_1r = ''
		self.type_m1_g3_2r = ''
		self.type_m1_g3_3r = ''
		self.type_m1_g3_4r = ''
		self.type_m1_g4_1r = ''
		self.type_m1_g4_2r = ''
		self.type_m1_g4_3r = ''
		self.type_m1_g4_4r = ''
		self.type_m2_g1_1r = ''
		self.type_m2_g1_2r = ''
		self.type_m2_g1_3r = ''
		self.type_m2_g1_4r = ''
		self.type_m2_g2_1r = ''
		self.type_m2_g2_2r = ''
		self.type_m2_g2_3r = ''
		self.type_m2_g2_4r = ''
		self.type_m2_g3_1r = ''
		self.type_m2_g3_2r = ''
		self.type_m2_g3_3r = ''
		self.type_m2_g3_4r = ''
		self.type_m2_g4_1r = ''
		self.type_m2_g4_2r = ''
		self.type_m2_g4_3r = ''
		self.type_m2_g4_4r = ''
		self.type_m3_g1_1r = ''
		self.type_m3_g1_2r = ''
		self.type_m3_g1_3r = ''
		self.type_m3_g1_4r = ''
		self.type_m3_g2_1r = ''
		self.type_m3_g2_2r = ''
		self.type_m3_g2_3r = ''
		self.type_m3_g2_4r = ''
		self.type_m3_g3_1r = ''
		self.type_m3_g3_2r = ''
		self.type_m3_g3_3r = ''
		self.type_m3_g3_4r = ''
		self.type_m3_g4_1r = ''
		self.type_m3_g4_2r = ''
		self.type_m3_g4_3r = ''
		self.type_m3_g4_4r = ''
		self.type_m4_g1_1r = ''
		self.type_m4_g1_2r = ''
		self.type_m4_g1_3r = ''
		self.type_m4_g1_4r = ''
		self.type_m4_g2_1r = ''
		self.type_m4_g2_2r = ''
		self.type_m4_g2_3r = ''
		self.type_m4_g2_4r = ''
		self.type_m4_g3_1r = ''
		self.type_m4_g3_2r = ''
		self.type_m4_g3_3r = ''
		self.type_m4_g3_4r = ''
		self.type_m4_g4_1r = ''
		self.type_m4_g4_2r = ''
		self.type_m4_g4_3r = ''
		self.type_m4_g4_4r = ''
		self.imposed_ques_id = 99999
		self.ask_never_asked_q = 0		

		self.curs.execute("SELECT session_id FROM Kids_Sessions WHERE kid_id = ?",(self.kid_id,))
		exist_already = self.curs.fetchone()
		print("exist_already", exist_already)

		if exist_already:
			exist_already = exist_already[0]
			print("exist_already {}".format(exist_already))
		else:
			print("exist_already non trovatooooooooo")
		if exist_already is None:
			self.curs.execute("SELECT age FROM Kids WHERE kid_id = ?",(self.kid_id,))
			kid_age = self.curs.fetchone()
			if kid_age:
				kid_age = kid_age[0]
				if kid_age<=6:
					self.take_one_session_56(self.kid_id)
				elif 6<kid_age<=8:
					self.take_one_session_78(self.kid_id)
				elif kid_age>8:
					self.take_one_session_910(self.kid_id)
			else:
				print("Error. kid_age parameter not found. Are you sure the kid already exists?")
		else:
			tot_nec_features = []
			tot_nec_features, self.kids_syms, self.kids_needs= adp2.extract_necessary_features_for_prediction(self, self.kid_id)

			self.s1_understanding_input = tot_nec_features[0]
			self.s1_diversion_input = tot_nec_features[1]
			self.s2_endurance_input = tot_nec_features[2]
			self.s2_restrictions_of_interest_input = tot_nec_features[3]
			self.s3_meltdown_input = tot_nec_features[4]
			self.s3_memoria_deficit_input = tot_nec_features[5]
			self.s3_distraction_in_listening_input = tot_nec_features[6]
			self.s3_executing_long_tasks_input = tot_nec_features[7]
			self.a1_max_question_game = tot_nec_features[8]
			self.a1_range_num_input = tot_nec_features[9]
			self.a1_range_diff_matches_done_input = tot_nec_features[10]
			self.a1_range_self.num_of_matches_with_games_order_of_difficulty = tot_nec_features[11]
			self.a2_max_game_match = tot_nec_features[12]
			self.a2_match_session_input = tot_nec_features[13]
			self.a2_self.num_of_matches_with_games_quan_ques_input = tot_nec_features[14]
			self.a3_order_diff_match_input = tot_nec_features[15]
			self.a3_num_of_session_with_matches_quan_games_input = tot_nec_features[16]
			self.n1_focus_on_errors_input = tot_nec_features[17]
			self.n1_focus_on_concentration_input = tot_nec_features[18]
			self.n1_focus_on_comprehension_input = tot_nec_features[19]
			self.n2_focus_on_automation_input = tot_nec_features[20]
			self.n2_focus_on_resistance_input = tot_nec_features[21]
			self.n4_focus_on_changes_input = tot_nec_features[22]
			self.level_input = tot_nec_features[23]

		scen = []
		print("customized {}".format(customized))
		mand_q, self.plus_one_game = dbem2.impose_id_ques(customized)
		print("mand_q, self.plus_one_game")
		print(mand_q)
		print(self.plus_one_game)
		if self.plus_one_game!=0:
			inds = random.randint(1,len(mand_q))
			self.imposed_ques_id = mand_q[inds-1]
		else:
			self.imposed_ques_id = 99999
		print("IMPOSED QUESTION_ID!!!! {}".format(self.imposed_ques_id))
		self.stress_flag,self.complexity,self.level_area,is_mixed,dont_ask_scenario,self.num_of_matches,num_of_games_of_one_match,\
		num_of_questions,kind_one_game,type_one_game,self.ask_never_asked_q, difficulty_mat,difficulty_gam = dbem2.obtain_info_session(customized)
		print("print subito subito subito subito RECEIVED FROM CUTOMIZED: INIZIO")
		print(self.stress_flag)
		print(self.complexity)
		print(self.level_area)
		print(is_mixed)
		print(dont_ask_scenario)
		print(self.num_of_matches)
		print(num_of_games_of_one_match)
		print(num_of_questions)
		print(kind_one_game)
		print(type_one_game)
		print(self.ask_never_asked_q)
		print(difficulty_mat)
		print(difficulty_gam)
		print("print subito subito subito subito RECEIVED FROM CUTOMIZED: FINE")
		self.extra_time_tolerance = 0
		self.body_enabled = 1
		self.exhortations = 0
		self.audio_slow = 0
		self.repeat_intro = 0
		self.rules_explication = 0
		self.repeat_question  = 0
		self.led_brightness = 'std'
		
		if self.stress_flag: #add next age tables to consider with stress!!!
			self.curs.execute("SELECT age FROM Kids WHERE kid_id = ?",(self.kid_id,))
			kid_age = self.curs.fetchone()
			if kid_age:
				kid_age = kid_age[0]
				if kid_age<=6:
					self.take_one_session_56(self.kid_id)
				elif 6<kid_age<=8:
					self.take_one_session_78(self.kid_id)
				elif kid_age>8:
					self.take_one_session_910(self.kid_id)
				if kid_age==6:
					with open('/home/pi/OIMI/oimi_code/src/learning/game_classification/All_paths/complete_78_csv_paths.txt') as f:
						self.csv_lines = f.read().splitlines()
				elif kid_age==8:
					with open('/home/pi/OIMI/oimi_code/src/learning/game_classification/All_paths/All_paths/complete_910_csv_paths.txt') as f:
						self.csv_lines = f.read().splitlines()

		tot_nec_features = []
		tot_nec_features, self.kids_syms, self.kids_needs= adp2.extract_necessary_features_for_prediction(self, self.kid_id)

		self.s1_understanding_input = tot_nec_features[0]
		self.s1_diversion_input = tot_nec_features[1]
		self.s2_endurance_input = tot_nec_features[2]
		self.s2_restrictions_of_interest_input = tot_nec_features[3]
		self.s3_meltdown_input = tot_nec_features[4]
		self.s3_memoria_deficit_input = tot_nec_features[5]
		self.s3_distraction_in_listening_input = tot_nec_features[6]
		self.s3_executing_long_tasks_input = tot_nec_features[7]
		self.a1_max_question_game = tot_nec_features[8]
		self.a1_range_num_input = tot_nec_features[9]
		self.a1_range_diff_matches_done_input = tot_nec_features[10]
		self.a1_range_self.num_of_matches_with_games_order_of_difficulty = tot_nec_features[11]
		self.a2_max_game_match = tot_nec_features[12]
		self.a2_match_session_input = tot_nec_features[13]
		self.a2_self.num_of_matches_with_games_quan_ques_input = tot_nec_features[14]
		self.a3_order_diff_match_input = tot_nec_features[15]
		self.a3_num_of_session_with_matches_quan_games_input = tot_nec_features[16]
		self.n1_focus_on_errors_input = tot_nec_features[17]
		self.n1_focus_on_concentration_input = tot_nec_features[18]
		self.n1_focus_on_comprehension_input = tot_nec_features[19]
		self.n2_focus_on_automation_input = tot_nec_features[20]
		self.n2_focus_on_resistance_input = tot_nec_features[21]
		self.n4_focus_on_changes_input = tot_nec_features[22]
		self.level_input = tot_nec_features[23]		


		search_given_sce = dbem2.take_given_scenario(customized)
		print("DAJE!!!! search_given_sce {}".format(search_given_sce))
		#must be one in this case scenario just one considered if passed more than one for error
		if len(search_given_sce)>0:
			inds = random.randint(1,len(search_given_sce))
			given_sce = search_given_sce[inds-1]
		else:
			given_sce = 0
		print("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDAI  SCENARIOOOOOOOOOOOOOOOOOOOOOOOOOO ==== {}".format(given_sce))
		if 'focus_on_errors' in customized:
			self.n1_focus_on_errors_input = 'yes'
		if 'focus_on_concentration' in customized:
			self.n1_focus_on_concentration_input = 'yes'
		if 'focus_on_comprehension' in customized:
			self.n1_focus_on_comprehension_input = 'yes'
		if 'focus_on_errors' in customized:
			self.n1_focus_on_errors_input = 'yes'
		if 'focus_on_resitance' in customized:
			self.n2_focus_on_resistance_input = 'yes'
		if 'focus_on_changes' in customized:
			self.n4_focus_on_changes_input = 'yes'
		if 'dont_move' in customized:
			self.movements_allowed = 0
		#this time are not caused by Kids_Symptoms but from needs impo given
		if self.n1_focus_on_comprehension_input=='yes':
			self.repeat_question = 1
		if self.n1_focus_on_concentration_input=='yes':
			self.led_brightness = 'low'
			self.audio_slow = 1
		if self.n4_focus_on_changes_input=='yes':
			self.exhortations = 1
		if 'focus_on_concentration scope:game' in customized:
			self.repeat_intro = 1
		################################################if self.level_input=='beginner':!!!!!! RICORDA
		#predic 1 table
		###########	for table_1
		############################################## fill 
		dict_table_1 = {'col1': [self.level_input],
			 'col2': [self.s1_diversion_input],
			 'col3': [self.s2_endurance_input],
			 'col4': [self.s3_meltdown_input],
			 'col5': [self.a1_range_num_input],
			 'col6': [self.a3_order_diff_match_input],
			 'col7': [self.a2_match_session_input]}

		fores1 = cswp.RanForest_clf_table_1(name="predict_trained_dataset#1", received_data = self.csv_lines[0], num_outputs_prev = 4, weights_flag=0) 
		fores1.prepare_array(dict_table_1)
		self.outputs_table_1 = fores1.predict_instance()
		print(self.outputs_table_1)
		print("!!!self.complexity prima {}".format(self.complexity))
		if self.complexity=='':
			self.complexity = self.outputs_table_1[0][0]
		print("!!!self.complexity dopo {}".format(self.complexity))
		print("ADD_CUSTOM kind_one_game {}".format(type_one_game))
		print("ADD_CUSTOM type_one_game {}".format(type_one_game))
		the_game_kind = dbem2.find_kind(type_one_game)
		self.matches_order_diff = self.outputs_table_1[0][2]
		dict_table_2 = {
			'col1': [self.level_input],
			'col2': [self.complexity],
			'col3': [self.matches_order_diff],
			'col4': [self.s1_understanding_input],
			'col5': [self.a1_range_diff_matches_done_input],
			'col6': [self.a2_self.num_of_matches_with_games_quan_ques_input],
			'col7': [self.a3_num_of_session_with_matches_quan_games_input],
			'col8': [self.n1_focus_on_concentration_input]}
	
		if self.num_of_matches==0:
			self.num_of_matches = int(self.outputs_table_1[0][1])
		if self.level_area=='ma' or self.level_area=='ga':
			depends = random.randint(0,4)
			if depends==1:
				self.num_of_matches = 1
			if depends==2:
				self.num_of_matches = 2

		if self.plus_one_game and self.num_of_matches>1:
			if self.num_of_matches==2:
				depends = random.randint(0,4)
				if depends==0 or depends==1:
					self.num_of_matches = 1
			if self.num_of_matches==3 or self.num_of_matches==4:
				self.num_of_matches = self.num_of_matches-1
		
		if given_sce!=0:
			self.num_of_matches = 1
		###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1###1
		if self.num_of_matches==1:
			print("ADD_CUSTOM PREDICTION!!!---> self.num_of_matches = 1")
			fores2 = cswp.RanForest_clf_table_2(name='predict_trained_dataset#t2m1', received_data = self.csv_lines[1], num_outputs_prev = 1, weights_flag=0) 
			fores2.prepare_array(dict_table_2)
			self.outputs_table_2 = fores2.predict_instance()

			if difficulty_mat=='':
				self.difficulty_mat_1 = self.outputs_table_2[0][0]
			self.order_quan_games = self.outputs_table_2[0][1]
			dict_table_31 = {
				'col1': [self.level_input],
				'col2': [self.complexity],
				'col3': [self.s2_restrictions_of_interest_input], #s1
				'col4': [self.s3_executing_long_tasks_input], #s2
				'col5': [self.s1_understanding_input], #s3
				'col6': [self.difficulty_mat_1],
				'col7': [self.a1_range_self.num_of_matches_with_games_order_of_difficulty],
				'col8': [self.n1_focus_on_concentration_input],
				'col9': [self.n2_focus_on_resistance_input],
				'col10': [self.a2_max_game_match]
				}

			fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m1", received_data = self.csv_lines[5], num_outputs_prev = 1, weights_flag=0)
			fores3.prepare_array(dict_table_31)
			self.outputs_table_3 = fores3.predict_instance()
			
			self.order_quan_quest_m1 = self.outputs_table_3[0][0]
			self.order_diff_games_m1 = self.outputs_table_3[0][1]
			###############################################################################################################################################
			if given_sce!=0: #just a match when scenario uis given!!!
				print("ENTRO IN GIVEN SCENARIO!!!! ####################")
				disposable = 0
				if self.complexity=='Easy':
					self.games_m1 = random.randint(1,2)
					advisable_time = 800
				if self.complexity=='Normal':
					self.games_m1 = random.randint(1,3)
					advisable_time = 600
				if self.complexity=='Medium':
					self.games_m1 = random.randint(1,4)
					advisable_time = 500
				if self.complexity=='Hard':
					self.games_m1 = random.randint(2,4)
					advisable_time = 400
				if difficulty_mat!='':
					self.games_m1 = num_of_games_of_one_match
				
				adp3.add_new_Session_with_kid(self)
				if self.games_m1>1:
					self.variety_m1 = 'SUNDRY'
					self.criteria_m1 = 'regular'
				else:
					self.curs.execute("SELECT kind FROM Scenarios s JOIN (Scenarios_Questions sq JOIN Questions q ON (sq.audio_id=q.audio)) USING (scenario_group) WHERE scenario_id = ? GROUP BY random() LIMIT 1"(given_sce,))
					self.variety_m1 = self.curs.fetchall()[0]
					self.criteria_m1 = 'ignore'
				print("ritorno trionfale in GIVEN SCENARIO!!!! trionfale trionfale trionfale trionfale trionfale trionfale trionfale")
				dca4.add_new_Match_table(self, self.difficulty_mat_1, self.games_m1, self.variety_m1, self.order_diff_games_m1, self.order_quan_quest_m1, self.criteria_m1, advisable_time, given_sce, disposable)
				self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
				sess_last = self.curs.fetchall()[0][0]
				print("sess_last {}".format(sess_last))
				self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
				match_last = self.curs.fetchall()[0][0]
				print("match_last {}".format(match_last))
				dca.add_new_SessionMatch_table(self, sess_last, match_last)
				end_here = 1			
			###############################################################################################################################################
			else:
				dict_table_41 = {
				'col1': [self.level_input], 
				'col2': [self.difficulty_mat_1], 
				'col3': [self.s3_meltdown_input],
				'col4': [self.s1_understanding_input], 
				'col5': [self.s3_distraction_in_listening_input],
				'col6': [self.order_quan_quest_m1], 
				'col7': [self.order_diff_games_m1], 
				'col8': [self.n1_focus_on_errors_input], 
				'col9': [self.n2_focus_on_automation_input], 
				'col10': [self.n1_focus_on_concentration_input], 
				'col11': [self.n4_focus_on_changes_input],
				'col12': [self.a1_max_question_game]}

				if num_of_games_of_one_match==0:
					self.games_m1 = int(self.outputs_table_3[0][2])
				else:
					self.games_m1 = num_of_games_of_one_match

				if self.games_m1==1:
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1", received_data = self.csv_lines[170], num_outputs_prev = 1, weights_flag=0)
					fores4.prepare_array(dict_table_41)
					self.outputs_table_4_m1 = fores4.predict_instance()
					
					if type_one_game=='':
						self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
						##########################################################################################################
						self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
						##########################################################################################################							
					else:
						self.kind_m1_g1 = the_game_kind	  
					if difficulty_gam=='':
						self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
					
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					dict_table_511 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques], 
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					
					if num_of_questions==0:
						self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					if is_mixed:
						self.m1_questions_g1 = 1
					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					elif self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]					
					elif self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					elif self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]					
				if self.games_m1==2:
					pos_both, os_game, pos_kind = 0,0,0
					if self.level_area=='se' or self.level_area=='ma':
						if the_game_kind=='':
							self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
							self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
							##########################################################################################################
							self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
							self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
							##########################################################################################################	
						else:
							pos_kind = random.randint(1,2)
							if pos_kind==1:
								self.kind_m1_g1 = the_game_kind
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								##########################################################################################################	
							if pos_kind==2:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = the_game_kind
								##########################################################################################################
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								##########################################################################################################
						if difficulty_gam=='':
							self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
							self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
						else:
							pos_game = random.randint(1,2)
							if pos_game==1:
								self.diff_m1_g1 = difficulty_gam
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
							if pos_kind==2:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = difficulty_gam
					if self.level_area=='ga':
						if the_game_kind!='' and difficulty_gam=='':
							pos_kind = random.randint(1,2)
							if pos_kind==1:
								self.kind_m1_g1 = the_game_kind
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								##########################################################################################################
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								##########################################################################################################
							if pos_kind==2:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = the_game_kind
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								##########################################################################################################
							self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
							self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
						
						if the_game_kind=='' and difficulty_gam!='':
							self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
							self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
							##########################################################################################################
							self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
							self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
							##########################################################################################################							
							pos_game = random.randint(1,2)
							if pos_game==1:
								self.diff_m1_g1 = difficulty_gam
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
							if pos_kind==2:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = difficulty_gam

						if the_game_kind!='' and difficulty_gam!='':
							pos_both = random.randint(1,2)
							if pos_both==1:
								self.kind_m1_g1 = the_game_kind
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								##########################################################################################################
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								##########################################################################################################	
								self.diff_m1_g1 = difficulty_gam
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
							if pos_both==2:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = the_game_kind
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								##########################################################################################################									
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = difficulty_gam

						if the_game_kind=='' and difficulty_gam=='':
							self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
							self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
							self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
							self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
					
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]

					dict_table_521 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques], 
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}		
					dict_table_522 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g2], 
						'col3': [self.m1_g2_order_diff_ques],
						'col4': [self.kind_m1_g2],
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}
						
					if num_of_questions==0:
						self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
						self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
					else:
						pos_ques = random.randint(0,1)
						if pos_ques:
							self.m1_questions_g1 = num_of_questions
							self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
						else:
							self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
							self.m1_questions_g2 = num_of_questions
					if is_mixed:
						self.m1_questions_g1 = 1
						self.m1_questions_g2 = 1
					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					if self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					if self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					if self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]

					if self.m1_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					if self.m1_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					if self.m1_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					if self.m1_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
						self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]
				if self.games_m1==3:
					que_already_assign=0
					if self.level_area=='se' or self.level_area=='ma':
						if the_game_kind=='':
							self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
							self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
							self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
							##########################################################################################################
							self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
							self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
							self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
							##########################################################################################################	
						else:
							pos_kind = random.randint(0,2)
							if pos_kind==0:
								self.kind_m1_g1 = the_game_kind
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								##########################################################################################################
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								##########################################################################################################	
							if pos_kind==1:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = the_game_kind
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								##########################################################################################################	
							else:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = the_game_kind
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								##########################################################################################################	
						if difficulty_gam=='':
							self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
							self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
							self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
						else:
							pos_game = random.randint(0,2)
							if pos_game==0:
								self.diff_m1_g1 = difficulty_gam
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
							if pos_kind==1:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = difficulty_gam
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
							else:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = difficulty_gam
					if self.level_area=='ga':
						if the_game_kind!='' and difficulty_gam=='':
							pos_kind = random.randint(0,2)
							if pos_kind==0:
								self.kind_m1_g1 = the_game_kind
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								##########################################################################################################
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								##########################################################################################################	
							if pos_kind==1:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = the_game_kind
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								##########################################################################################################	
							if pos_kind==2:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = the_game_kind
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								##########################################################################################################	
							self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
							self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
							self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
						
						if the_game_kind=='' and difficulty_gam!='':
							self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
							self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
							self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
							##########################################################################################################
							self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
							self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
							self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
							##########################################################################################################	
							pos_game = random.randint(0,2)
							if pos_game==0:
								self.diff_m1_g1 = difficulty_gam
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
							if pos_game==1:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = difficulty_gam
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
							if pos_kind==2:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = difficulty_gam

						if the_game_kind!='' and difficulty_gam!='':
							pos_both = random.randint(0,2)
							if pos_both==0:
								self.kind_m1_g1 = the_game_kind
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								self.diff_m1_g1 = difficulty_gam
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								##########################################################################################################
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								##########################################################################################################
							if pos_both==1:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = the_game_kind
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = difficulty_gam
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								##########################################################################################################
							if pos_both==2:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = the_game_kind
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = difficulty_gam
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								##########################################################################################################
							if num_of_questions!=0:
								que_already_assign=1
								if pos_both==0:
									questions_g1 = num_of_questions
									questions_g2 = int(self.outputs_table_4_m1[0][6])
									questions_g3 = int(self.outputs_table_4_m1[0][10])
								if pos_both==1:
									questions_g1 = int(self.outputs_table_4_m1[0][2])
									questions_g2 = num_of_questions
									questions_g3 = int(self.outputs_table_4_m1[0][10])
								if pos_both==2:
									questions_g1 = int(self.outputs_table_4_m1[0][2])
									questions_g1 = int(self.outputs_table_4_m1[0][6])
									questions_g3 = num_of_questions

						if the_game_kind=='' and difficulty_gam=='':
							self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
							self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
							self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
							self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
							self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
							self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
							##########################################################################################################
							self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
							self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
							self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
							##########################################################################################################	
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
					self.m1_g3_order_diff_ques = self.outputs_table_4_m1[0][11]
					dict_table_531 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques],
						'col4': [self.kind_m1_g1],
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}		
					dict_table_532 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g2], 
						'col3': [self.m1_g2_order_diff_ques], 
						'col4': [self.kind_m1_g2], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_533 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g3], 
						'col3': [self.m1_g3_order_diff_ques],
						'col4': [self.kind_m1_g3], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					
					if not que_already_assign:
						if num_of_questions==0:
							self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
							self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
							self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
						else:
							pos_ques = random.randint(0,2)
							if pos_ques==0:
								self.m1_questions_g1 = num_of_questions
								self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
								self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
							if pos_ques==1:
								self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
								self.m1_questions_g2 = num_of_questions
								self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
							else:
								self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
								self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
								self.m1_questions_g3 = num_of_questions
					if is_mixed:
						self.m1_questions_g1 = 1
						self.m1_questions_g2 = 1
						self.m1_questions_g3 = 1
					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					if self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					if self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					if self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]

					if self.m1_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					if self.m1_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					if self.m1_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					if self.m1_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
						self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]

					if self.m1_questions_g3==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					if self.m1_questions_g3==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
					if self.m1_questions_g3==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
					if self.m1_questions_g3==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()			
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
						self.type_m1_g3_4r = self.outputs_table_5_m1_g3[0][3]
				if self.games_m1==4:
					que_already_assign=0
					if self.level_area=='se' or self.level_area=='ma':
						if the_game_kind=='':
							self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
							self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
							self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
							self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
							##########################################################################################################
							self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
							self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
							self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
							self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
							##########################################################################################################	
						else:
							pos_kind = random.randint(0,3)
							if pos_kind==0:
								self.kind_m1_g1 = the_game_kind
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
								##########################################################################################################	
							if pos_kind==1:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = the_game_kind
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
								##########################################################################################################	
							if pos_kind==2:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = the_game_kind
								self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
								##########################################################################################################	
							else:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								self.kind_m1_g4 = the_game_kind
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								##########################################################################################################	
						if difficulty_gam=='':
							self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
							self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
							self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
							self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
						else:
							pos_game = random.randint(0,3)
							if pos_game==0:
								self.diff_m1_g1 = difficulty_gam
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
							if pos_kind==1:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = difficulty_gam
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
							if pos_kind==1:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = difficulty_gam
								self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
							else:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								self.diff_m1_g4 = difficulty_gam
					if self.level_area=='ga':
						if the_game_kind!='' and difficulty_gam=='':
							pos_kind = random.randint(0,3)
							if pos_kind==0:
								self.kind_m1_g1 = the_game_kind
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
								##########################################################################################################
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
								##########################################################################################################									
							if pos_kind==1:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = the_game_kind
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
								##########################################################################################################									
							if pos_kind==2:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = the_game_kind
								self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
								##########################################################################################################									
							else:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								self.kind_m1_g4 = the_game_kind
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								##########################################################################################################									
							self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
							self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
							self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
							self.diff_m1_g3 = self.outputs_table_4_m1[0][12]
						
						if the_game_kind=='' and difficulty_gam!='':
							self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
							self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
							self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
							self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
							##########################################################################################################
							self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
							self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
							self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
							self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
							##########################################################################################################	
							pos_game = random.randint(0,3)
							if pos_game==0:
								self.diff_m1_g1 = difficulty_gam
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
							if pos_game==1:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = difficulty_gam
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
							if pos_kind==2:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = difficulty_gam
								self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
							else:
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								self.diff_m1_g4 = difficulty_gam
						
						if the_game_kind!='' and difficulty_gam!='':
							pos_both = random.randint(0,2)
							if pos_both==0:
								self.kind_m1_g1 = the_game_kind
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
								##########################################################################################################	
								self.diff_m1_g1 = difficulty_gam
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
							if pos_both==1:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = the_game_kind
								self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
								self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
								##########################################################################################################	
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = difficulty_gam
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
							if pos_both==2:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3 = the_game_kind
								self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
								##########################################################################################################									
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = difficulty_gam
								self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
							if pos_both==2:
								self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
								self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
								self.kind_m1_g3= self.outputs_table_4_m1[0][9]
								self.kind_m1_g4 = the_game_kind
								##########################################################################################################
								self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
								self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
								self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
								##########################################################################################################								
								self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
								self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
								self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
								self.diff_m1_g4 = difficulty_gam

							if num_of_questions!=0:
								que_already_assign=1
								if pos_both==0:
									questions_g1 = num_of_questions
									questions_g2 = int(self.outputs_table_4_m1[0][6])
									questions_g3 = int(self.outputs_table_4_m1[0][10])
									questions_g4 = int(self.outputs_table_4_m1[0][14])
								elif pos_both==1:
									questions_g1 = int(self.outputs_table_4_m1[0][2])
									questions_g2 = num_of_questions
									questions_g3 = int(self.outputs_table_4_m1[0][10])
									questions_g4 = int(self.outputs_table_4_m1[0][14])
								elif pos_both==2:
									questions_g1 = int(self.outputs_table_4_m1[0][2])
									questions_g1 = int(self.outputs_table_4_m1[0][6])
									questions_g3 = num_of_questions
									questions_g4 = int(self.outputs_table_4_m1[0][14])
								else:
									questions_g1 = int(self.outputs_table_4_m1[0][2])
									questions_g1 = int(self.outputs_table_4_m1[0][6])
									questions_g3 = int(self.outputs_table_4_m1[0][10])
									questions_g4 = num_of_questions

						if the_game_kind=='' and difficulty_gam=='':
							self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
							self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
							self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
							self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
							self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
							self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
							self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
							self.diff_m1_g4 = self.outputs_table_4_m1[0][12]		   
							##########################################################################################################
							self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
							self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
							self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
							self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
							##########################################################################################################	
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
					self.m1_g3_order_diff_ques = self.outputs_table_4_m1[0][11]
					self.m1_g4_order_diff_ques = self.outputs_table_4_m1[0][15]

					dict_table_541 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques],
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}		
					dict_table_542 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g2], #g1_diff
						'col3': [self.m1_g2_order_diff_ques],
						'col4': [self.kind_m1_g2], #g1_kind
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_543 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g3], #g1_diff
						'col3': [self.m1_g3_order_diff_ques], 
						'col4': [self.kind_m1_g3], #g1_kind
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_544 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g4], #g1_diff
						'col3': [self.m1_g4_order_diff_ques], 
						'col4': [self.kind_m1_g4], #g1_kind
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
				   
					if not que_already_assign:
						if num_of_questions==0:
							self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
							self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
							self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
							self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])
						else:
							pos_ques = random.randint(0,3)
							if pos_ques==0:
								self.m1_questions_g1 = num_of_questions
								self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
								self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
								self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])
							if pos_ques==1:
								self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
								self.m1_questions_g2 = num_of_questions
								self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
								self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])
							if pos_ques==2:
								self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
								self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
								self.m1_questions_g3 = num_of_questions
								self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])
							else:
								self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
								self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
								self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
								self.m1_questions_g4 = num_of_questions
					if is_mixed:
						self.m1_questions_g1 = 1
						self.m1_questions_g2 = 1
						self.m1_questions_g3 = 1
						self.m1_questions_g4 = 1
					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					elif self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					elif self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					elif self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]

					if self.m1_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					elif self.m1_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					elif self.m1_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					elif self.m1_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
						self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]

					if self.m1_questions_g3==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					elif self.m1_questions_g3==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]					
					elif self.m1_questions_g3==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
					elif self.m1_questions_g3==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
						self.type_m1_g3_4r = self.outputs_table_5_m1_g3[0][3]

					if self.m1_questions_g4==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
					elif self.m1_questions_g4==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
						self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
					elif self.m1_questions_g4==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
						self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
						self.type_m1_g4_3r = self.outputs_table_5_m1_g4[0][2]
					elif self.m1_questions_g4==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outpts_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
						self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
						self.type_m1_g4_3r = self.outputs_table_5_m1_g4[0][2]
						self.type_m1_g4_4r = self.outputs_table_5_m1_g4[0][3]
		
			if not end_here:
				adp3.conclude_final_session_creation(self)

		###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2###2
		elif self.num_of_matches==2:
			print("ADD_CUSTOM PREDICTION!!!---> self.num_of_matches = 2")
			fores2 = cswp.RanForest_clf_table_2(name="predict_trained_dataset#t2m2", received_data = self.csv_lines[2], num_outputs_prev = 2, weights_flag=0) 
			fores2.prepare_array(dict_table_2)
			self.outputs_table_2 = fores2.predict_instance()

			which_match_is_custom = 0
			which_game_is_custom = 0

			if difficulty_mat=='':
				print("LA SIGNORA IN GIALLO")
				self.difficulty_mat_1 = self.outputs_table_2[0][0]
				self.difficulty_mat_2 = self.outputs_table_2[0][1]
				print("COSA HO OTTENUTO:")
				print(self.difficulty_mat_1)
				print(self.difficulty_mat_2)
			else:
				print("LA SIGNORA IN rosso")
				pos_ma = random.randint(1,2)
				if self.level_area=='ma' or self.level_area=='ga':
					which_match_is_custom = pos_ma
				if pos_ma==1:
					self.difficulty_mat_1 = difficulty_mat
					self.difficulty_mat_2 = self.outputs_table_2[0][1]
				if pos_ma==2:
					self.difficulty_mat_1 = self.outputs_table_2[0][0]
					self.difficulty_mat_2 = difficulty_mat

			self.order_quan_games = self.outputs_table_2[0][2]
			dict_table_32 = {
				'col1': [self.level_input],
				'col2': [self.complexity], 
				'col3': [self.s2_restrictions_of_interest_input], #s1
				'col4': [self.s3_executing_long_tasks_input], #s2
				'col5': [self.s1_understanding_input], #s3
				'col6': [self.difficulty_mat_1], 
				'col7': [self.difficulty_mat_2], 
				'col8': [self.order_quan_games], 
				'col9': [self.a1_range_self.num_of_matches_with_games_order_of_difficulty],
				'col10': [self.n1_focus_on_concentration_input],
				'col11': [self.n2_focus_on_resistance_input],
				'col12': [self.a2_max_game_match]}
			print("THE PARCH!!!!!!!!!!!")
			print("self.level_input {}".format(self.level_input))
			print("self.complexity {}".format(self.complexity))
			print("self.s2_restrictions_of_interest_input {}".format(self.s2_restrictions_of_interest_input))
			print("self.s3_executing_long_tasks_input {}".format(self.s3_executing_long_tasks_input))
			print("self.s1_understanding_input {}".format(self.s1_understanding_input))
			print("self.difficulty_mat_1 {}".format(self.difficulty_mat_1))
			print("self.difficulty_mat_2 {}".format(self.difficulty_mat_2))
			print("self.order_quan_games {}".format(self.order_quan_games))
			print("self.a1_range_self.num_of_matches_with_games_order_of_difficulty {}".format(self.a1_range_self.num_of_matches_with_games_order_of_difficulty))
			print("self.n1_focus_on_concentration_input {}".format(self.n1_focus_on_concentration_input))
			print("self.n2_focus_on_resistance_input {}".format(self.n2_focus_on_resistance_input))
			print("self.a2_max_game_match {}".format(self.a2_max_game_match))

			if self.complexity=='Easy':
				print("THE DARKEST EASy")
				fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m2e", received_data = self.csv_lines[6], num_outputs_prev = 2, weights_flag=0)
			if self.complexity=='Normal':
				print("THE DARKEST nromal")
				fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m2n", received_data = self.csv_lines[7], num_outputs_prev = 2, weights_flag=0)
			if self.complexity=='Medium':
				print("THE DARKEST medi")
				fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m2m", received_data = self.csv_lines[8], num_outputs_prev = 2, weights_flag=0)
			if self.complexity=='Hard':
				print("THE DARKEST Hard")
				fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m2h", received_data = self.csv_lines[9], num_outputs_prev = 2, weights_flag=0)
			fores3.prepare_array(dict_table_32)
			self.outputs_table_3 = fores3.predict_instance()

			self.order_quan_quest_m1 = self.outputs_table_3[0][0]
			self.order_diff_games_m1 = self.outputs_table_3[0][1]
			self.order_quan_quest_m2 = self.outputs_table_3[0][3]
			self.order_diff_games_m2 = self.outputs_table_3[0][4]

			dict_table_421 = {
			'col1': [self.level_input], #level
			'col2': [self.difficulty_mat_1], 
			'col3': [self.s3_meltdown_input], 
			'col4': [self.s1_understanding_input], 
			'col5': [self.s3_distraction_in_listening_input],
			'col6': [self.order_quan_quest_m1],
			'col7': [self.order_diff_games_m1],
			'col8': [self.n1_focus_on_errors_input], 
			'col9': [self.n2_focus_on_automation_input], 
			'col10': [self.n1_focus_on_concentration_input], 
			'col11': [self.n4_focus_on_changes_input],
			'col12': [self.a1_max_question_game]}

			dict_table_422 = {
			'col1': [self.level_input], #level
			'col2': [self.difficulty_mat_2], 
			'col3': [self.s3_meltdown_input], 
			'col4': [self.s1_understanding_input], 
			'col5': [self.s3_distraction_in_listening_input],
			'col6': [self.order_quan_quest_m2], 
			'col7': [self.order_diff_games_m2], 
			'col8': [self.n1_focus_on_errors_input], 
			'col9': [self.n2_focus_on_automation_input], 
			'col10': [self.n1_focus_on_concentration_input], 
			'col11': [self.n4_focus_on_changes_input],
			'col12': [self.a1_max_question_game]}

			if num_of_games_of_one_match==0:
				self.games_m1 = int(self.outputs_table_3[0][2])
				self.games_m2 = int(self.outputs_table_3[0][5])
			else:
				if which_match_is_custom==1:
					self.games_m1 = num_of_games_of_one_match
					self.games_m2 = int(self.outputs_table_3[0][5])
				if which_match_is_custom==2:
					self.games_m1 = int(self.outputs_table_3[0][2])
					self.games_m2 = num_of_games_of_one_match
				if which_match_is_custom==0:
					pos_game = random.randint(0,1)
					if pos_game:
						self.games_m1 = num_of_games_of_one_match
						self.games_m2 = int(self.outputs_table_3[0][5])
					if self.level_area=='ma' or self.level_area=='ga':
						which_match_is_custom=1
					else:
						self.games_m1 = int(self.outputs_table_3[0][2])
						self.games_m2 = num_of_games_of_one_match
					if self.level_area=='ma' or self.level_area=='ga':
						which_match_is_custom=2

			if self.games_m1==1: 
				fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g1", received_data = self.csv_lines[170], num_outputs_prev = 1, weights_flag=0)
			elif self.games_m1==2:
				fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g2", received_data = self.csv_lines[171], num_outputs_prev = 2, weights_flag=0)
			elif self.games_m1==3:
				fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g3", received_data = self.csv_lines[172], num_outputs_prev = 3, weights_flag=0)
			elif self.games_m1==4: 
				fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g4", received_data = self.csv_lines[173], num_outputs_prev = 4, weights_flag=0)
			fores4.prepare_array(dict_table_421)
			self.outputs_table_4_m1 = fores4.predict_instance()
			if self.games_m2==1: 
				fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g1", received_data = self.csv_lines[170], num_outputs_prev = 1, weights_flag=0)
			elif self.games_m2==2:
				fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g2", received_data = self.csv_lines[171], num_outputs_prev = 2, weights_flag=0)
			elif self.games_m2==3: 
				fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g3", received_data = self.csv_lines[172], num_outputs_prev = 3, weights_flag=0)
			elif self.games_m2==4: 
				fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g4", received_data = self.csv_lines[173], num_outputs_prev = 4, weights_flag=0)
			fores4.prepare_array(dict_table_422)
			self.outputs_table_4_m2 = fores4.predict_instance()
			 
			if self.games_m1==1:
				self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
				self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
				self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
				##########################################################################################################
				self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
				##########################################################################################################	
			if self.games_m1==2:
				self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
				self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
				self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
				self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
				self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
				self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
				##########################################################################################################
				self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
				self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
				##########################################################################################################	
			if self.games_m1==3:
				self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
				self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
				self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
				self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
				self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
				self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
				self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
				self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
				self.m1_g3_order_diff_ques = self.outputs_table_4_m1[0][11]
				##########################################################################################################
				self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
				self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
				self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
				##########################################################################################################	
			if self.games_m1==4:
				self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
				self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
				self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
				self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
				self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
				self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
				self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
				self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
				self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
				self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
				self.m1_g3_order_diff_ques = self.outputs_table_4_m1[0][11]
				self.m1_g4_order_diff_ques = self.outputs_table_4_m1[0][15]
				##########################################################################################################
				self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
				self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
				self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
				self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
				##########################################################################################################	
			if self.games_m2==1:
				self.kind_m2_g1 = self.outputs_table_4_m2[0][1]
				self.diff_m2_g1 = self.outputs_table_4_m2[0][0]
				self.m2_g1_order_diff_ques = self.outputs_table_4_m2[0][3]				
				##########################################################################################################
				self.kind_m2_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
				##########################################################################################################	
			if self.games_m2==2:
				self.kind_m2_g1 = self.outputs_table_4_m2[0][1]
				self.kind_m2_g2 = self.outputs_table_4_m2[0][5]
				self.diff_m2_g1 = self.outputs_table_4_m2[0][0]
				self.diff_m2_g2 = self.outputs_table_4_m2[0][4]					   
				self.m2_g1_order_diff_ques = self.outputs_table_4_m2[0][3]
				self.m2_g2_order_diff_ques = self.outputs_table_4_m2[0][7]
				##########################################################################################################
				self.kind_m2_g1 = sp.last_minute_security_change_kind(self.kind_m2_g1, self.diff_m2_g1, self.level_input) 
				self.kind_m2_g2 = sp.last_minute_security_change_kind(self.kind_m2_g2, self.diff_m2_g2, self.level_input) 
				##########################################################################################################	
			if self.games_m2==3:
				self.kind_m2_g1 = self.outputs_table_4_m2[0][1]
				self.kind_m2_g2 = self.outputs_table_4_m2[0][5]
				self.kind_m2_g3 = self.outputs_table_4_m2[0][9]
				self.diff_m2_g1 = self.outputs_table_4_m2[0][0]
				self.diff_m2_g2 = self.outputs_table_4_m2[0][4]
				self.diff_m2_g3 = self.outputs_table_4_m2[0][8]
				self.m2_g1_order_diff_ques = self.outputs_table_4_m2[0][3]
				self.m2_g2_order_diff_ques = self.outputs_table_4_m2[0][7]
				self.m2_g3_order_diff_ques = self.outputs_table_4_m2[0][11]								  
				##########################################################################################################
				self.kind_m2_g1 = sp.last_minute_security_change_kind(self.kind_m2_g1, self.diff_m2_g1, self.level_input) 
				self.kind_m2_g2 = sp.last_minute_security_change_kind(self.kind_m2_g2, self.diff_m2_g2, self.level_input) 
				self.kind_m2_g3 = sp.last_minute_security_change_kind(self.kind_m2_g3, self.diff_m2_g3, self.level_input) 
				##########################################################################################################	
			if self.games_m2==4:
				self.kind_m2_g1 = self.outputs_table_4_m2[0][1]
				self.kind_m2_g2 = self.outputs_table_4_m2[0][5]
				self.kind_m2_g3 = self.outputs_table_4_m2[0][9]
				self.kind_m2_g4 = self.outputs_table_4_m2[0][13]
				self.diff_m2_g1 = self.outputs_table_4_m2[0][0]
				self.diff_m2_g2 = self.outputs_table_4_m2[0][4]
				self.diff_m2_g3 = self.outputs_table_4_m2[0][8]
				self.diff_m2_g4 = self.outputs_table_4_m2[0][12]	
				self.m2_g1_order_diff_ques = self.outputs_table_4_m2[0][3]
				self.m2_g2_order_diff_ques = self.outputs_table_4_m2[0][7]
				self.m2_g3_order_diff_ques = self.outputs_table_4_m2[0][11]
				self.m2_g4_order_diff_ques = self.outputs_table_4_m2[0][15]
				##########################################################################################################
				self.kind_m2_g1 = sp.last_minute_security_change_kind(self.kind_m2_g1, self.diff_m2_g1, self.level_input) 
				self.kind_m2_g2 = sp.last_minute_security_change_kind(self.kind_m2_g2, self.diff_m2_g2, self.level_input) 
				self.kind_m2_g3 = sp.last_minute_security_change_kind(self.kind_m2_g3, self.diff_m2_g3, self.level_input) 
				self.kind_m2_g4 = sp.last_minute_security_change_kind(self.kind_m2_g4, self.diff_m2_g4, self.level_input) 
				##########################################################################################################	
			if type_one_game!='':
				if which_match_is_custom==0:
					rand_mat = random.randint(1,2)
				else:
					rand_mat = which_match_is_custom
				if rand_mat==1:
					if self.games_m1==1:
						self.kind_m1_g1 = the_game_kind
					if self.games_m1==2:
						if which_game_is_custom==0:
							ran_g = random.randint(1,2)
						else:
							ran_g = which_game_is_custom
						if ran_g==1:
							self.kind_m1_g1 = the_game_kind
						if ran_g==2:
							self.kind_m1_g2 = the_game_kind
					if self.games_m1==3:
						if which_game_is_custom==0:
							ran_g = random.randint(1,3)
						else:
							ran_g = which_game_is_custom								
						if ran_g==1:
							self.kind_m1_g1 = the_game_kind
						if ran_g==2:
							self.kind_m1_g2 = the_game_kind
						if ran_g==3:
							self.kind_m1_g3 = the_game_kind
					if self.games_m1==4:
						if which_game_is_custom==0:
							ran_g = random.randint(1,4)
						else:
							ran_g = which_game_is_custom								
						if ran_g==1:
							self.kind_m1_g1 = the_game_kind
						if ran_g==2:
							self.kind_m1_g2 = the_game_kind
						if ran_g==3:
							self.kind_m1_g3 = the_game_kind
						if ran_g==4:
							self.kind_m1_g4 = the_game_kind
				if rand_mat==2:
					if self.games_m2==1:
						self.kind_m2_g1 = the_game_kind
					if self.games_m2==2:
						if which_game_is_custom==0:
							ran_g = random.randint(1,2)
						else:
							ran_g = which_game_is_custom
						if ran_g==1:
							self.kind_m2_g1 = the_game_kind
						if ran_g==2:
							self.kind_m2_g2 = the_game_kind
					if self.games_m2==3:
						if which_game_is_custom==0:
							ran_g = random.randint(1,3)
						else:
							ran_g = which_game_is_custom								
						if ran_g==1:
							self.kind_m2_g1 = the_game_kind
						if ran_g==2:
							self.kind_m2_g2 = the_game_kind
						if ran_g==3:
							self.kind_m2_g3 = the_game_kind
					if self.games_m2==4:
						if which_game_is_custom==0:
							ran_g = random.randint(1,4)
						else:
							ran_g = which_game_is_custom								
						if ran_g==1:
							self.kind_m2_g1 = the_game_kind
						if ran_g==2:
							self.kind_m2_g2 = the_game_kind
						if ran_g==3:
							self.kind_m2_g3 = the_game_kind
						if ran_g==4:
							self.kind_m2_g4 = the_game_kind							
				if self.level_area=='ga':
					which_game_is_custom=ran_g
			if difficulty_gam!='':
				if which_match_is_custom==0:
					rand_mat = random.randint(1,4)
				else:
					rand_mat = which_match_is_custom
				if rand_mat==1:
					if self.games_m1==1:
						self.diff_m1_g1 = difficulty_gam
					if self.games_m1==2:
						if which_game_is_custom==0:
							ran_g = random.randint(1,2)
						else:
							ran_g = which_game_is_custom
						if ran_g==1:
							self.diff_m1_g1 = difficulty_gam
						if ran_g==2:
							self.diff_m1_g2 = difficulty_gam
					if self.games_m1==3:
						if which_game_is_custom==0:
							ran_g = random.randint(1,3)
						else:
							ran_g = which_game_is_custom								
						if ran_g==1:
							self.diff_m1_g1 = difficulty_gam
						if ran_g==2:
							self.diff_m1_g2 = difficulty_gam
						if ran_g==3:
							self.diff_m1_g3 = difficulty_gam
					if self.games_m1==4:
						if which_game_is_custom==0:
							ran_g = random.randint(1,4)
						else:
							ran_g = which_game_is_custom								
						if ran_g==1:
							self.diff_m1_g1 = difficulty_gam
						if ran_g==2:
							self.diff_m1_g2 = difficulty_gam
						if ran_g==3:
							self.diff_m1_g3 = difficulty_gam
						if ran_g==4:
							self.diff_m1_g4 = difficulty_gam
				if rand_mat==2:
					if self.games_m2==1:
						self.diff_m2_g1 = difficulty_gam
					if self.games_m2==2:
						if which_game_is_custom==0:
							ran_g = random.randint(1,2)
						else:
							ran_g = which_game_is_custom
						if ran_g==1:
							self.diff_m2_g1 = difficulty_gam
						if ran_g==2:
							self.diff_m2_g2 = difficulty_gam
					if self.games_m2==3:
						if which_game_is_custom==0:
							ran_g = random.randint(1,3)
						else:
							ran_g = which_game_is_custom								
						if ran_g==1:
							self.diff_m2_g1 = difficulty_gam
						if ran_g==2:
							self.diff_m2_g2 = difficulty_gam
						if ran_g==3:
							self.diff_m2_g3 = difficulty_gam
					if self.games_m2==4:
						if which_game_is_custom==0:
							ran_g = random.randint(1,4)
						else:
							ran_g = which_game_is_custom								
						if ran_g==1:
							self.diff_m2_g1 = difficulty_gam
						if ran_g==2:
							self.diff_m2_g2 = difficulty_gam
						if ran_g==3:
							self.diff_m2_g3 = difficulty_gam
						if ran_g==4:
							self.diff_m2_g4 = difficulty_gam							
				if self.level_area=='ga':
					which_game_is_custom=ran_g

			if self.games_m1==1: 
				dict_table_511 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m1_g1], 
					'col3': [self.m1_g1_order_diff_ques], 
					'col4': [self.kind_m1_g1], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 
				if num_of_questions==0:
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
				else:
					if which_match_is_custom==1:
						self.m1_questions_g1 = num_of_questions
					else:
						self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
				if is_mixed:
					self.m1_questions_g1 = 1
				if self.m1_questions_g1==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_511)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
				if self.m1_questions_g1==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_511)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
				if self.m1_questions_g1==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_511)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
				if self.m1_questions_g1==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_511)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]
			if self.games_m2==1:
				dict_table_511 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m2_g1], 
					'col3': [self.m2_g1_order_diff_ques], #g1_order_diff_ques
					'col4': [self.kind_m2_g1], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 

				if num_of_questions==0:
					self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
				else:
					if which_match_is_custom==2:
						self.m2_questions_g1 = num_of_questions
					else:
						self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
				if is_mixed:
					self.m2_questions_g1 = 1
				if self.m2_questions_g1==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_511)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
				if self.m2_questions_g1==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_511)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
				if self.m2_questions_g1==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_511)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
				if self.m2_questions_g1==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_511)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
					self.type_m2_g1_4r = self.outputs_table_5_m2_g1[0][3]
			
			if self.games_m1==2:
				dict_table_521 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m1_g1], 
					'col3': [self.m1_g1_order_diff_ques], 
					'col4': [self.kind_m1_g1], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 
				dict_table_522 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m1_g2], 
					'col3': [self.m1_g2_order_diff_ques], 
					'col4': [self.kind_m1_g2], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 
				
				if num_of_questions==0:
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
				else:
					if which_match_is_custom==1:
						if which_game_is_custom==1:
							self.m1_questions_g1 = num_of_questions
							self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
						else:
							self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
							self.m1_questions_g2 = num_of_questions
					else:
						self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
						self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
				if is_mixed:
					self.m1_questions_g1 = 1
					self.m1_questions_g2 = 1
				if self.m1_questions_g1==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_521)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
				if self.m1_questions_g1==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_521)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
				if self.m1_questions_g1==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_521)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
				if self.m1_questions_g1==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_521)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]

				if self.m1_questions_g2==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_522)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
				if self.m1_questions_g2==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_522)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
				if self.m1_questions_g2==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_522)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
				if self.m1_questions_g2==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_522)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]
			if self.games_m2==2: 
				dict_table_521 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m2_g1], 
					'col3': [self.m2_g1_order_diff_ques], #g1_order_diff_ques
					'col4': [self.kind_m2_g1], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 
				dict_table_522 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m2_g2], 
					'col3': [self.m2_g2_order_diff_ques], #g2_order_diff_ques
					'col4': [self.kind_m2_g2], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 

				if num_of_questions==0:
					self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
					self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
				else:
					if which_match_is_custom==2:
						if which_game_is_custom==1:
							self.m2_questions_g1 = num_of_questions
							self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
						else:
							self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
							self.m2_questions_g2 = num_of_questions
					else:
						self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
						self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
				if is_mixed:
					self.m2_questions_g1 = 1
					self.m2_questions_g2 = 1				
				if self.m2_questions_g1==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_521)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
				elif self.m2_questions_g1==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_521)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
				elif self.m2_questions_g1==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_521)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
				elif self.m2_questions_g1==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_521)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
					self.type_m2_g1_4r = self.outputs_table_5_m2_g1[0][3]

				if self.m2_questions_g2==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_522)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
				elif self.m2_questions_g2==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_522)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
				elif self.m2_questions_g2==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_522)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
					self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
				elif self.m2_questions_g2==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_522)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()							
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
					self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
					self.type_m2_g2_4r = self.outputs_table_5_m2_g2[0][3]
			
			if self.games_m1==3: 
				self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
				self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
				self.m1_g3_order_diff_ques = self.outputs_table_4_m1[0][11]
				dict_table_531 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m1_g1], 
					'col3': [self.m1_g1_order_diff_ques], 
					'col4': [self.kind_m1_g1], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 
				dict_table_532 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m1_g2], 
					'col3': [self.m1_g2_order_diff_ques], 
					'col4': [self.kind_m1_g2], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]}
				dict_table_533 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m1_g3], 
					'col3': [self.m1_g3_order_diff_ques], 
					'col4': [self.kind_m1_g3], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 
				

				if num_of_questions==0:
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
					self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
				else:
					if which_match_is_custom==1:
						if which_game_is_custom==1:
							self.m1_questions_g1 = num_of_questions
							self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
							self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
						if which_game_is_custom==2:
							self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
							self.m1_questions_g2 = num_of_questions
							self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
						if which_game_is_custom==3:
							self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
							self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
							self.m1_questions_g3 = num_of_questions
					else:
						self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
						self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
						self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
				if is_mixed:
					self.m1_questions_g1 = 1
					self.m1_questions_g2 = 1
					self.m1_questions_g3 = 1
				if self.m1_questions_g1==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_531)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
				if self.m1_questions_g1==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_531)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
				if self.m1_questions_g1==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_531)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
				if self.m1_questions_g1==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_531)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]

				if self.m1_questions_g2==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_532)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
				if self.m1_questions_g2==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_532)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
				if self.m1_questions_g2==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_532)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
				if self.m1_questions_g2==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_532)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]

				if self.m1_questions_g3==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_533)
					self.outputs_table_5_m1_g3 = fores5.predict_instance()
					self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
				if self.m1_questions_g3==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_533)
					self.outputs_table_5_m1_g3 = fores5.predict_instance()
					self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
				if self.m1_questions_g3==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_533)
					self.outputs_table_5_m1_g3 = fores5.predict_instance()
					self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
					self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
				if self.m1_questions_g3==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_533)
					self.outputs_table_5_m1_g3 = fores5.predict_instance()			
					self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
					self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
					self.type_m1_g3_4r = self.outputs_table_5_m1_g3[0][3]
			if self.games_m2==3: 
				dict_table_531 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m2_g1], 
					'col3': [self.m2_g1_order_diff_ques], 
					'col4': [self.kind_m2_g1], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 
				dict_table_532 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m2_g2], 
					'col3': [self.m2_g2_order_diff_ques], 
					'col4': [self.kind_m2_g2], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]}
				dict_table_533 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m2_g3],	
					'col3': [self.m2_g3_order_diff_ques], 
					'col4': [self.kind_m2_g3],	
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]}

				if num_of_questions==0:
					self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
					self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
					self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10])
				else:
					if which_match_is_custom==2:
						if which_game_is_custom==1:
							self.m2_questions_g1 = num_of_questions
							self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
							self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10])
						if which_game_is_custom==2:
							self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
							self.m2_questions_g2 = num_of_questions
							self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10])
						if which_game_is_custom==3:
							self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
							self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
							self.m2_questions_g3 = num_of_questions
					else:
						self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
						self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
						self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10])
				if is_mixed:
					self.m2_questions_g1 = 1
					self.m2_questions_g2 = 1
					self.m2_questions_g3 = 1
				if self.m2_questions_g1==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_531)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
				elif self.m2_questions_g1==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_531)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
				elif self.m2_questions_g1==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_531)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
				elif self.m2_questions_g1==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_531)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
					self.type_m2_g1_4r = self.outputs_table_5_m2_g1[0][3]
				if self.m2_questions_g2==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_532)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
				elif self.m2_questions_g2==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_532)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
				elif self.m2_questions_g2==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_532)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
					self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
				elif self.m2_questions_g2==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_532)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
					self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
					self.type_m2_g2_4r = self.outputs_table_5_m2_g2[0][3]
				if self.m2_questions_g3==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_533)
					self.outputs_table_5_m2_g3 = fores5.predict_instance()
					self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
				elif self.m2_questions_g3==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_533)
					self.outputs_table_5_m2_g3 = fores5.predict_instance()
					self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
					self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
				elif self.m2_questions_g3==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_533)
					self.outputs_table_5_m2_g3 = fores5.predict_instance()
					self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
					self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
					self.type_m2_g3_3r = self.outputs_table_5_m2_g3[0][2]
				elif self.m2_questions_g3==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_533)
					self.outputs_table_5_m2_g3 = fores5.predict_instance()
					self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
					self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
					self.type_m2_g3_3r = self.outputs_table_5_m2_g3[0][2]
					self.type_m2_g3_4r = self.outputs_table_5_m2_g3[0][3]

			if self.games_m1==4: 
				dict_table_541 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m1_g1], 
					'col3': [self.m1_g1_order_diff_ques], 
					'col4': [self.kind_m1_g1], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 
				dict_table_542 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m1_g2], 
					'col3': [self.m1_g2_order_diff_ques], 
					'col4': [self.kind_m1_g2], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]}
				dict_table_543 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m1_g3],	
					'col3': [self.m1_g3_order_diff_ques], 
					'col4': [self.kind_m1_g3],	
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]}
				dict_table_544 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m1_g4], 
					'col3': [self.m1_g4_order_diff_ques], 
					'col4': [self.kind_m1_g4], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 

				if num_of_questions==0:
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
					self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
					self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])
				else:
					if which_match_is_custom==1:
						if which_game_is_custom==1:
							self.m1_questions_g1 = num_of_questions
							self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
							self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
							self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])
						if which_game_is_custom==2:
							self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
							self.m1_questions_g2 = num_of_questions
							self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
							self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])
						if which_game_is_custom==3:
							self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
							self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
							self.m1_questions_g3 = num_of_questions
							self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])
						if which_game_is_custom==4:
							self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
							self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
							self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
							self.m1_questions_g4 = num_of_questions
					else:
						self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
						self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
						self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
						self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])
				if is_mixed:
					self.m1_questions_g1 = 1
					self.m1_questions_g2 = 1
					self.m1_questions_g3 = 1
					self.m1_questions_g4 = 1
				if self.m1_questions_g1==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_541)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
				if self.m1_questions_g1==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_541)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
				if self.m1_questions_g1==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_541)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
				if self.m1_questions_g1==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_541)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()
					self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]

				if self.m1_questions_g2==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_542)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
				if self.m1_questions_g2==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_542)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
				if self.m1_questions_g2==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_542)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
				if self.m1_questions_g2==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_542)
					self.outputs_table_5_m1_g2 = fores5.predict_instance()
					self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]

				if self.m1_questions_g3==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_543)
					self.outputs_table_5_m1_g3 = fores5.predict_instance()
					self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
				if self.m1_questions_g3==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_543)
					self.outputs_table_5_m1_g3 = fores5.predict_instance()
					self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]					
				if self.m1_questions_g3==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_543)
					self.outputs_table_5_m1_g3 = fores5.predict_instance()
					self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
					self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
				if self.m1_questions_g3==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_543)
					self.outputs_table_5_m1_g3 = fores5.predict_instance()
					self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
					self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
					self.type_m1_g3_4r = self.outputs_table_5_m1_g3[0][3]

				if self.m1_questions_g4==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_544)
					self.outputs_table_5_m1_g4 = fores5.predict_instance()
					self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
				if self.m1_questions_g4==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_544)
					self.outputs_table_5_m1_g4 = fores5.predict_instance()
					self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
					self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
				if self.m1_questions_g4==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_544)
					self.outputs_table_5_m1_g4 = fores5.predict_instance()
					self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
					self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
					self.type_m1_g4_3r = self.outputs_table_5_m1_g4[0][2]
				if self.m1_questions_g4==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_544)
					self.outpts_table_5_m1_g4 = fores5.predict_instance()
					self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
					self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
					self.type_m1_g4_3r = self.outputs_table_5_m1_g4[0][2]
					self.type_m1_g4_4r = self.outputs_table_5_m1_g4[0][3]
			if self.games_m2==4: 
				dict_table_541 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m2_g1], 
					'col3': [self.m2_g1_order_diff_ques], 
					'col4': [self.kind_m2_g1], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]} 
				dict_table_542 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m2_g2], 
					'col3': [self.m2_g2_order_diff_ques], 
					'col4': [self.kind_m2_g2], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]}
				dict_table_543 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m2_g3],	
					'col3': [self.m2_g3_order_diff_ques], 
					'col4': [self.kind_m2_g3],	
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]}
				dict_table_544 = {
					'col1': [self.level_input], 
					'col2': [self.diff_m2_g4], 
					'col3': [self.m2_g4_order_diff_ques], 
					'col4': [self.kind_m2_g4], 
					'col5': [self.s3_distraction_in_listening_input],
					'col6': [self.s1_understanding_input],
					'col7': [self.s3_memoria_deficit_input],
					'col8': [self.n1_focus_on_comprehension_input],
					'col9': [self.n2_focus_on_automation_input]}
				
				if num_of_questions==0:
					self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
					self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
					self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10])
					self.m2_questions_g4 = int(self.outputs_table_4_m2[0][14])
				else:
					if which_match_is_custom==2:
						if which_game_is_custom==1:
							self.m2_questions_g1 = num_of_questions
							self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
							self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10])
							self.m2_questions_g4 = int(self.outputs_table_4_m2[0][14])
						if which_game_is_custom==2:
							self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
							self.m2_questions_g2 = num_of_questions
							self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10])
							self.m2_questions_g4 = int(self.outputs_table_4_m2[0][14])
						if which_game_is_custom==3:
							self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
							self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
							self.m2_questions_g3 = num_of_questions
							self.m2_questions_g4 = int(self.outputs_table_4_m2[0][14])
						if which_game_is_custom==4:
							self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
							self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
							self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10])
							self.m2_questions_g4 = num_of_questions
					else:
						self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
						self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])
						self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10])
						self.m2_questions_g4 = int(self.outputs_table_4_m2[0][14])
				if is_mixed:
					self.m2_questions_g1 = 1
					self.m2_questions_g2 = 1
					self.m2_questions_g3 = 1
					self.m2_questions_g4 = 1
				if self.m2_questions_g1==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_541)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]					
				if self.m2_questions_g1==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_541)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
				if self.m2_questions_g1==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_541)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
				if self.m2_questions_g1==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_541)
					self.outputs_table_5_m2_g1 = fores5.predict_instance()
					self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
					self.type_m2_g1_4r = self.outputs_table_5_m2_g1[0][3]
				if self.m2_questions_g2==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_542)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
				if self.m2_questions_g2==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_542)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
				if self.m2_questions_g2==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_542)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
					self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
				if self.m2_questions_g2==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_542)
					self.outputs_table_5_m2_g2 = fores5.predict_instance()
					self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
					self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
					self.type_m2_g2_4r = self.outputs_table_5_m2_g2[0][3]
				if self.m2_questions_g3==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_543)
					self.outputs_table_5_m2_g3 = fores5.predict_instance()
					self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
				if self.m2_questions_g3==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_543)
					self.outputs_table_5_m2_g3 = fores5.predict_instance()
					self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
					self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
				if self.m2_questions_g3==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_543)
					self.outputs_table_5_m2_g3 = fores5.predict_instance()
					self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
					self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
					self.type_m2_g3_3r = self.outputs_table_5_m2_g3[0][2]
				if self.m2_questions_g3==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_543)
					self.outputs_table_5_m2_g3 = fores5.predict_instance()
					self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
					self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
					self.type_m2_g3_3r = self.outputs_table_5_m2_g3[0][2]
					self.type_m2_g3_4r = self.outputs_table_5_m2_g3[0][3]
				if self.m2_questions_g4==1: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q1", received_data = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
					fores5.prepare_array(dict_table_544)
					self.outputs_table_5_m2_g4 = fores5.predict_instance()
					self.type_m2_g4_1r = self.outputs_table_5_m2_g4[0][0]
				if self.m2_questions_g4==2: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q2", received_data = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
					fores5.prepare_array(dict_table_544)
					self.outputs_table_5_m2_g4 = fores5.predict_instance()
					self.type_m2_g4_1r = self.outputs_table_5_m2_g4[0][0]
					self.type_m2_g4_2r = self.outputs_table_5_m2_g4[0][1]
				if self.m2_questions_g4==3: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q3", received_data = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
					fores5.prepare_array(dict_table_544)
					self.outputs_table_5_m2_g4 = fores5.predict_instance()
					self.type_m2_g4_1r = self.outputs_table_5_m2_g4[0][0]
					self.type_m2_g4_2r = self.outputs_table_5_m2_g4[0][1]
					self.type_m2_g4_3r = self.outputs_table_5_m2_g4[0][2]
				if self.m2_questions_g4==4: 
					fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q4", received_data = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
					fores5.prepare_array(dict_table_544)
					self.outputs_table_5_m2_g4 = fores5.predict_instance()
					self.type_m2_g4_1r = self.outputs_table_5_m2_g4[0][0]
					self.type_m2_g4_2r = self.outputs_table_5_m2_g4[0][1]
					self.type_m2_g4_3r = self.outputs_table_5_m2_g4[0][2]
					self.type_m2_g4_4r = self.outputs_table_5_m2_g4[0][3]

			adp3.conclude_final_session_creation(self)

		###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3###3
		elif self.num_of_matches==3 or self.num_of_matches==4:
			acpsh.add_new_Session_table_with_kid_case34(self,dict_table_2,is_mixed,the_game_kind,difficulty_mat,difficulty_gam,type_one_game,num_of_games_of_one_match,num_of_questions,given_sce)

