# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Modify
# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
cdef modify_Audios_list(str audio_id, str audio_path, int index_for_lastest)
cdef modify_Kids_list(str name, str surname, int age, str level, str image_path, str details_path, str token)
cdef modify_Needs_list(str focus_constraint, str need_constraint, str scope)
cdef modify_Therapies_list(int num_of_treatments, str status, str creation_way, str kick_off_date, str closing_date, str canonical_intervention, int duration_days, int disposable, 
	str token, int num_last_line)
cdef modify_Treatments_list(str target_goal_1, str target_goal_2, str target_goal_3, int num_of_pastimes, int num_of_sessions, int num_of_entertainments, str status_decided, 
	str creation_way_auto, str day_auto, int disposable_auto, str new_id, int num_last_line)
cdef modify_Entertainments_list(str creation_way_auto, str status_decided, str class_of_game, str specific_guidelines, str notes_none, int disposable_auto, str new_id, int num_last_line)
cdef modify_Sessions_list(int num_of_matches, str complexity, str status, str order_difficulty_matches, str order_quantity_games, 
	int mandatory_impositions, int mandatory_needs, int extra_time_tolerance, int movements_allowed, int body_enabled,
	int rules_explication, int stress_flag, int frequency_exhortations, int repeat_question, int repeat_intro, int desirable_time, str creation_way, bint disposable, str token, int num_last_line)

cdef modify_Matches_list(str difficulty, int num_of_games, str variety, str order_difficulty_games, str order_quantity_questions, str criteria_arrangement, int advisable_time, scenario, bint disposable, str token, int num_last_line)
cdef modify_Games_list(int num_questions, str kind_game, str type_game, str difficulty, str order_difficulty_questions, int necessary_time, bint disposable, str token, int num_last_line)
cdef modify_Kind_Games_list(str kind, str details, int num_last_line)
cdef modify_Type_Games_list(str type_g, str description, str example, str kind_g, int num_last_line)
cdef modify_Questions_list(str kind_game, str type_game, str audio_id, int value, str description, int position)
cdef modify_Patches_list(str new_subject, str new_color)

cdef modify_default_list_kids_therapies(int kid_id, int therapy_id, int numb_pos)
cdef modify_default_list_kids_treatments(int kid_id, int therapy_id, int numb_pos)
cdef modify_default_list_kids_sessions(int first_id, int second_id, int numb_pos)
cdef modify_default_list_kids_entertainments(int first_id, int second_id, int numb_pos)
cdef modify_default_list_sessions_matches(int first_id, int second_id, int numb_pos)
cdef modify_default_list_matches_games(int first_id, int second_id, int numb_pos)
cdef modify_default_list_games_questions(int first_id, int second_id, int numb_pos)
cdef modify_default_list_questions_patches(int first_id, int second_id, int numb_pos)
cdef modify_default_list_kids_needs(int first_id, int second_id, int numb_pos)
cdef modify_default_list_kids_impositions(int first_id, str second_id, int numb_pos)
cdef modify_default_list_kids_achievements(int first_id, str second_id, int numb_pos)
cdef modify_default_list_kids_symptoms(int first_id, str second_id, int numb_pos)
cdef modify_default_list_kids_issues(int first_id, str second_id, int numb_pos)
cdef modify_default_list_kids_strengths(int first_id, str second_id, int numb_pos)
cdef modify_default_list_kids_comorbidities(int first_id, str second_id, int numb_pos)
cdef modify_default_list_sessions_enforcements(int first_id, str second_id, int numb_pos)
cdef modify_default_list_sessions_impositions(int first_id, str second_id, int numb_pos)
cdef modify_default_list_sessions_needs(int first_id, int second_id, int numb_pos)

cdef modify_default_names(str constraint_str, str table, int numb_pos)
cdef modify_with_update_in_lines(str id_token, int change_this, int with_this)
cdef modify_with_update_single_match_changing_time(list match_data, int time_to_change)
cdef modify_with_update_desirable_time_session(list sessions, int time_to_change)

cdef modify_KidEntertainment_list(int kid_id, int entertainment_id, int numb_pos)
############ old
cdef modify_default_list_old_kids_needs(int first_id, int second_id, int numb_pos)
cdef modify_default_list_old_kids_symptoms(int first_id, str second_id, int numb_pos)
cdef modify_default_list_old_kids_strengths(int first_id, str second_id, int numb_pos)
cdef modify_default_list_old_kids_issues(int first_id, str second_id, int numb_pos)
cdef modify_default_list_old_kids_comorbidities(int first_id, str second_id, int numb_pos)
cdef modify_default_list_old_tables_kids(int first_id, str second_id, int numb_pos, str which_table)

cdef modify_therapies_duration_or_date(list old_therapy_data, list new_therapy_data)
cdef modify_treatment_day(list old_treatment_data, list new_treatment_data)