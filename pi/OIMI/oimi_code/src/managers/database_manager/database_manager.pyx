"""Info:
	Oimi robot database_manager principal and leading file. 
	Handles the creation and maintanance oimi_robot_database.db whole infrastructure, governing its access, writing, updates, constraints' control.
	Contains the main class DatabaseManager extended in submodules.
	Calls all other methods involving the database placed in different modules.

	In Cython It is impossible two __cinit__ methods for the same class, as Cython uses a single __cinit__ method for constructors. 
	Instead of defining two __cinit__ methods, you can use default values for the argument in the single constructor method. For example:
	def __cinit__(self, adjust=False)
	In this way, by calling obj = DatabaseManager() without arguments it takes the default value for the specified argument.
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
import re
import mmap
import csv
import copy
import random
import pickle
import sqlite3 as lite
import linecache as lica
from datetime import datetime
from importlib import reload
from subprocess import run, PIPE, Popen, STDOUT
from typing import Optional
import oimi_queries as oq
import oimi_queries_triggers as oqt
import db_default_lists as dbdl
import db_default_lists_2 as dbdl2
import db_default_lists_3 as dbdl3
import db_default_lists_4 as dbdl4
import db_extern_methods as dbem
import db_extern_methods_two as dbem2
import audio_blob_conversion as abc
cimport database_manager

cimport modify_default_lists as mdl
cimport database_controller_add_into as dca
cimport database_controller_build as dcb
cimport database_controller_check_mixed as dcc
cimport database_controller_delete as dcd
cimport database_controller_generate_online as dbg
cimport database_controller_populate as dcp
cimport database_controller_renew as dcr
cimport database_controller_save_to_files as dcs
cimport database_controller_take as dct
cimport database_controller_update as dcu
cimport database_triggers as dbtr

# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class DatabaseManager():
	def __cinit__(self, adjust=False):
		self.setup_database()
		if adjust:
			self.adjust_dates()

	def __enter__(self):
		which_subclass = self.getClass()
		if which_subclass=="DatabaseManager":
			print("Starting DatabaseManager...")

	def __exit__(self, exc_type, exc_val, exc_tb):
		which_subclass = self.getClass()
		if which_subclass=="DatabaseManager":
			self.conn.close()
			print("Closing DatabaseManager...")
			print("Closing connection with oimi_database...")
			print("Conn Closed.")
			print("Exiting from DatabaseManager.")
			print("Done.")
		
	def getClass(self):
		return self.__class__.__name__

	def which_class(self):
		cla = self.getClass()
		#print("which class is --> ".format(cla))
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# First 
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------	
	cdef preliminary_activities_at_creation(self):
		print("enter in preliminary_activities_at_creation")
		which_subclass = self.getClass()
		if which_subclass=="DatabaseManager":
			print("Resetting deletions_list inside pickle_deletions_list.pk")
			self.deletions_list = [[0]*1 for n in range(9)]
			filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
			with open(filename_pk, 'wb') as fi:
				pickle.dump(self.deletions_list, fi)
			#self.last_words = dbdl.last_words
			abc.blob_creation()
	

	cpdef prova_preliminary_activities(self):
		self.preliminary_activities_at_creation()
		
	cpdef create_the_entire_database_from_scratch(self):
		#self.preliminary_activities_at_creation()
		self.prova_preliminary_activities()
		self.build_Kids()
		self.build_Allidtokens()
		self.build_OimiSettings()
		self.build_Impositions() 
		self.build_Needs() 
		self.build_Patches()
		self.start_patches_trigger()
		self.place_patches_into_Patches()
		self.build_Audios()
		self.build_Kind_games()
		self.build_Type_games()
		
		self.build_Substages()
		self.build_Prompt_Substages()
		self.build_Reward_Substages()
		self.build_Mood_Substages()
		self.build_Reaction_Substages()
		self.build_Children_Substages()
		self.build_Stages()
		self.build_prompt_Stages()
		self.build_reward_Stages()
		self.build_mood_Stages()
		self.build_reaction_Stages()		
		########################################################## 
		self.build_Issues()
		self.build_Comorbidities()
		self.build_Strengths()		
		self.build_Symptoms()
		self.build_Achievements()

		self.build_Kids_Parameters()
		self.build_KidAchievement()
		self.build_KidSymptom()
		self.build_KidStrength()
		self.build_KidIssue()
		self.build_ClassFreeTarget()
		self.build_KidComorbidity()
		self.build_KidNeed()
		self.build_KidImposition()

		self.build_OldKidSymptom()
		self.build_OldKidNeed()
		self.build_OldKidIssue()
		self.build_OldKidComorbidity()
		self.build_OldKidStrength()
		########################################################### 
		self.build_Therapies()
		self.build_Targets()
		self.build_Treatments()
		self.build_TreatmentTarget()
		
		self.build_Treatments_Traits()
		self.build_Class_free()
		self.build_Entertainments()
		self.start_free_games_trigger()
		self.place_entertainments()
		
		self.build_Questions()
		self.insert_in_questions()
		self.build_Scenarios()
		self.insert_in_scenarios()
		self.build_ScenarioQuestion()
		self.populate_scenarios_questions()
		self.build_Games()		
		self.build_Matches()
		self.build_Sessions()
		###########################################################
		self.build_SessionMatch()
		self.build_MatchGame()
		self.build_GameQuestion()
		self.build_ActivityRules()
		self.build_QuestionPatch()
		self.place_data_into_Questions_Patches()
		self.build_TherapyTreatment()
		self.build_TreatmentEntertainment()
		self.build_TreatmentSession()
		self.build_TreatmentPastime()
		
		self.build_KidTherapy()
		self.build_KidTreatment()
		self.build_KidEntertainment()
		self.build_KidSession()
		###########################################################
		self.build_ScenarioType()
		self.build_ScenarioKind()
		self.build_Scenario4PSUQuestion()
		self.build_Scenario4PCOQuestion()
		self.populate_scenarios_kinds()
		self.populate_scenarios_types()
		self.build_SessionEnforcement()
		self.build_SessionImposition()
		self.build_SessionNeed()
		########################################################### 
		self.create_Short_recap_all_kids()
		self.create_Full_sessions_played_recap()
		self.create_Full_played_recap_giga()
		self.fix_contraints_Full_sesions_summary()
		self.fix_contraints_Full_therapies_summary()
		self.create_Full_played_recap_giga()
		self.fix_contraints_Full_therapies_summary()
		self.create_Full_entertainments_played()
		self.fix_contraints_Full_entertainment_played_summary()
		self.create_definitive_one()
		self.create_definitive_two()
		self.compose_definitive()
		###########################################################
		self.construct_ResultsTotalQuestion()
		self.construct_ResultsTotalKindOfGame()
		self.construct_ResultsTotalTypeOfGame()
		self.construct_ResultsTotalSession()
		self.construct_ResultsTotalMatch()
		self.construct_ResultsTotalGame()
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Class Attributes getters,setters 
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	@property
	def curs(self):
		return self.curs
	@curs.setter
	def curs(self, value):
		self.curs = value
	@property
	def conn(self):
		return self.conn
	@conn.setter
	def conn(self, value):
		self.conn = value
	@property
	def deletions_list(self):
		return self.deletions_list
	@deletions_list.setter
	def deletions_list(self, value):
		self.deletions_list = value
	@property
	def game_impo_from_q_added(self):
		return self.game_impo_from_q_added
	@game_impo_from_q_added.setter
	def game_impo_from_q_added(self, value):
		self.game_impo_from_q_added = value
	@property
	def suggest_num_games_more_than(self):
		return self.suggest_num_games_more_than
	@suggest_num_games_more_than.setter
	def suggest_num_games_more_than(self, value):
		self.suggest_num_games_more_than = value
	@property
	def suggest_num_matches_more_than(self):
		return self.suggest_num_matches_more_than
	@suggest_num_matches_more_than.setter
	def suggest_num_matches_more_than(self, value):
		self.suggest_num_matches_more_than = value
	@property
	def suggested_type_game_from_q(self):
		return self.suggested_type_game_from_q
	@suggested_type_game_from_q.setter
	def suggested_type_game_from_q(self, value):
		self.suggested_type_game_from_q = value
	@property
	def suggested_kind_game_from_q(self):
		return self.suggested_kind_game_from_q
	@suggested_kind_game_from_q.setter
	def suggested_kind_game_from_q(self, value):
		self.suggested_kind_game_from_q = value
	@property
	def suggested_kind_game_from_t(self):
		return self.suggested_kind_game_from_t
	@suggested_kind_game_from_t.setter
	def suggested_kind_game_from_t(self, value):
		self.suggested_kind_game_from_t = value
	@property
	def suggested_scenario_from_t(self):
		return self.suggested_scenario_from_t
	@suggested_scenario_from_t.setter
	def suggested_scenario_from_t(self, value):
		self.suggested_scenario_from_t = value
	@property
	def suggested_scenario_from_k(self):
		return self.suggested_scenario_from_k
	@suggested_scenario_from_k.setter
	def suggested_scenario_from_k(self, value):
		self.suggested_scenario_from_k = value
	@property
	def suggested_scenario_from_q(self):
		return self.suggested_scenario_from_q
	@suggested_scenario_from_q.setter
	def suggested_scenario_from_q(self, value):
		self.suggested_scenario_from_q = value
	@property
	def imposed_scenario(self):
		return self.imposed_scenario
	@imposed_scenario.setter
	def imposed_scenario(self, value):
		self.imposed_scenario = value
	@property
	def imposed_num_matches(self):
		return self.imposed_num_matches
	@imposed_num_matches.setter
	def imposed_num_matches(self, value):
		self.imposed_num_matches = value
	@property
	def already_done_imposed_num_matches(self):
		return self.already_done_imposed_num_matches
	@already_done_imposed_num_matches.setter
	def already_done_imposed_num_matches(self, value):
		self.already_done_imposed_num_matches = value
	@property
	def already_done_imposed_type_game(self):
		return self.already_done_imposed_type_game
	@already_done_imposed_type_game.setter
	def already_done_imposed_type_game(self, value):
		self.already_done_imposed_type_game = value
	@property
	def already_done_imposed_kind_game(self):
		return self.already_done_imposed_kind_game
	@already_done_imposed_kind_game.setter
	def already_done_imposed_kind_game(self, value):
		self.already_done_imposed_kind_game = value
	@property
	def already_done_imposed_num_games(self):
		return self.already_done_imposed_num_games
	@already_done_imposed_num_games.setter
	def already_done_imposed_num_games(self, value):
		self.already_done_imposed_num_games = value
	@property
	def already_done_imposed_ask_ques(self):
		return self.already_done_imposed_ask_ques
	@already_done_imposed_ask_ques.setter
	def already_done_imposed_ask_ques(self, value):
		self.already_done_imposed_ask_ques = value
	@property
	def mandatory_impositions(self):
		return self.mandatory_impositions
	@mandatory_impositions.setter
	def mandatory_impositions(self, value):
		self.mandatory_impositions = value
	@property
	def mandatory_needs(self):
		return self.mandatory_needs
	@mandatory_needs.setter
	def mandatory_needs(self, value):
		self.mandatory_needs = value
	@property
	def imposed_mixed(self):
		return self.imposed_mixed
	@imposed_mixed.setter
	def imposed_mixed(self, value):
		self.imposed_mixed = value
	@property
	def already_done_imposed_mixed(self):
		return self.already_done_imposed_mixed
	@already_done_imposed_mixed.setter
	def already_done_imposed_mixed(self, value):
		self.already_done_imposed_mixed = value
	@property
	def already_done_imposed_difficulty_match(self):
		return self.already_done_imposed_difficulty_match
	@already_done_imposed_difficulty_match.setter
	def already_done_imposed_difficulty_match(self, value):
		self.already_done_imposed_difficulty_match = value
	@property
	def already_done_imposed_difficulty_game(self):
		return self.already_done_imposed_difficulty_game
	@already_done_imposed_difficulty_game.setter
	def already_done_imposed_difficulty_game(self, value):
		self.already_done_imposed_difficulty_game = value
	@property
	def suggest_num_games_more_than(self):
		return self.suggest_num_games_more_than
	@suggest_num_games_more_than.setter
	def suggest_num_games_more_than(self, value):
		self.suggest_num_games_more_than = value
	@property
	def already_done_imposed_num_questions(self):
		return self.already_done_imposed_num_questions
	@already_done_imposed_num_questions.setter
	def already_done_imposed_num_questions(self, value):
		self.already_done_imposed_num_questions = value
	@property
	def imposed_ques_id(self):
		return self.imposed_ques_id
	@imposed_ques_id.setter
	def imposed_ques_id(self, value):
		self.imposed_ques_id = value
	@property
	def imposed_difficulty_game(self):
		return self.imposed_difficulty_game
	@imposed_difficulty_game.setter
	def imposed_difficulty_game(self, value):
		self.imposed_difficulty_game = value
	@property
	def imposed_difficulty_match(self):
		return self.imposed_difficulty_match
	@imposed_difficulty_match.setter
	def imposed_difficulty_match(self, value):
		self.imposed_difficulty_match = value
	@property
	def imposed_num_questions(self):
		return self.imposed_num_questions
	@imposed_num_questions.setter
	def imposed_num_questions(self, value):
		self.imposed_num_questions = value
	@property
	def imposed_num_games(self):
		return self.imposed_num_games
	@imposed_num_games.setter
	def imposed_num_games(self, value):
		self.imposed_num_games = value
	@property
	def imposed_type_game_by_k(self):
		return self.imposed_type_game_by_k
	@imposed_type_game_by_k.setter
	def imposed_type_game_by_k(self, value):
		self.imposed_type_game_by_k = value
	@property
	def imposed_type_game(self):
		return self.imposed_type_game
	@imposed_type_game.setter
	def imposed_type_game(self, value):
		self.imposed_type_game = value
	@property
	def imposed_kind_game(self):
		return self.imposed_kind_game
	@imposed_kind_game.setter
	def imposed_kind_game(self, value):
		self.imposed_kind_game = value
	@property
	def imposed_num_questions_done(self):
		return self.imposed_num_questions_done
	@imposed_num_questions_done.setter
	def imposed_num_questions_done(self, value):
		self.imposed_num_questions_done = value
	@property
	def type_impo_from_k_added(self):
		return self.type_impo_from_k_added
	@type_impo_from_k_added.setter
	def type_impo_from_k_added(self, value):
		self.type_impo_from_k_added = value
	@property
	def type_impo_from_t_added(self):
		return self.type_impo_from_t_added
	@type_impo_from_t_added.setter
	def type_impo_from_t_added(self, value):
		self.type_impo_from_t_added = value
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Pivotal
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cdef setup_database(self): 
		which_subclass = self.getClass()
		if which_subclass=="DatabaseManager":
			print("Setting up connection with oimi_database...")
		okConn = False 
		while True:
			try:
				self.conn = lite.connect('file:/home/pi/OIMI/oimi_code/src/oimi_robot_database.db?mode = rw', uri = True) # SQLITE_OPEN_READWRITE only
				self.curs = self.conn.cursor() 
				if self.curs.connection==self.conn: 
					print("Ok, connection with oimi db succesfully completed.")
					vers = run('sqlite3 --version',shell = True, stdout = PIPE, stderr = PIPE) 
					print("using SQLite version: {}".format(vers.stdout[:7].decode()))
					okConn = True
				if okConn==True:
					break
			except lite.Error as e:
				print("The requested database doesn't exists, waiting for creation ...")
				run('sqlite3 /home/pi/OIMI/oimi_code/src/oimi_robot_database.db "VACUUM;"', shell = True)
				gir += 1
				continue
		try: 
			self.curs.execute("PRAGMA foreign_keys = ON")
			self.curs.execute("PRAGMA foreign_keys")
			data = self.curs.fetchone()[0]
			if which_subclass=="DatabaseManager":
				print("PRAGMA foreing keys availability setted to: {}".format(data))
			self.erase_part_of_database_if_full()

		except lite.Error as err:
			print("There\'s something wrong with database_manager connection to oimi_robot_database.db")
			print(err)
	
	cdef adjust_dates(self):
		question_exists = self.execute_a_query("""SELECT count(name) FROM sqlite_master WHERE type='table' AND name='Questions'""")
		if question_exists[0]:
			print('Questions table exists.')
			dbtr.enable_duration_adjustment(self)
			qq00 = "SELECT * FROM Therapies"
			old_ther = self.execute_a_query(qq00)
			ther_old = old_ther[0]
			print("old_ther", old_ther)
			print("ther_old", ther_old)
			qq01 = '''SELECT question_id FROM Questions where description = "Tocca il bottone rosso"'''
			sequence = self.execute_a_query(qq01)
			print("sequence id= is ")
			print(sequence)
			id_sec = sequence[0]
			print("id_sec is ")
			print(id_sec)
			qq02 = '''UPDATE Questions set question_id = ? WHERE description = "Tocca il bottone rosso"'''
			self.execute_param_query(qq02, id_sec)
			self.generic_commit()
			dbg.change_all_durations_or_kick_date_therapies(self, old_ther, 0)
		else:
			print('NOTHING TO ADJUST HERE')

	cpdef execute_a_query(self, query):
		self.curs.execute(query)
		res = self.curs.fetchall()
		if len(res)==1:
			res_ok = [item for sublist in res for item in sublist]
		else:
			res_ok = res
		print("The internal result of this current query is --> {}".format(res_ok))
		return res_ok

	cpdef execute_param_query(self, query, param):
		try:
			if len(param)>1:
				self.curs.execute(query, (param))
		except Exception:
			self.curs.execute(query, (param,))
		res = self.curs.fetchall()
		print("the internal res of param query is = {}".format(res))
		if len(res)==1:
			res_ok = [item for sublist in res for item in sublist]
		else:
			res_ok = res
		return res_ok
		##################################################### quello giusto!!!!!! cambio tutto !!!!
		#try:
		#	if len(param)>1:
		#		self.curs.execute(query, (param))
		#except Exception:
		#	self.curs.execute(query, (param,))
		#res = self.curs.fetchall()
		#print("res interna is ", res)
		#if res:
		#	res_ok = [item for sublist in res for item in sublist]
		#else:
		#	res_ok = []
		#return res_ok

	cpdef execute_new_query(self, query, param):
		try:
			if len(param)>1:
				self.curs.execute(query, (param))
		except Exception:
			self.curs.execute(query, (param,))
		res = self.curs.fetchall()
		print("res interna new is ", res)
		if res:
			res_ok = [item for sublist in res for item in sublist]
		else:
			res_ok = []
		return res_ok
	
	cpdef execute_last_query(self, query, param):
		try:
			if len(param)>1:
				self.curs.execute(query, (param))
		except Exception:
			self.curs.execute(query, (param,))
		res = self.curs.fetchall()
		print("res interna new is ", res)
		if res:
			if len(res)==1:
				res_ok = [item for sublist in res for item in sublist]
			if len(res)>1:
				res_ok = [list(item) for item in res]
		else:
			res_ok = []
		return res_ok

	cpdef drop_all_tables(self):
		print("Deleting all tables from oimi_robot_database.db ...")
		self.curs.execute("PRAGMA writable_schema = 1")
		self.curs.execute("delete from sqlite_master where type in ('table', 'index', 'trigger', 'view')")
		self.curs.execute("PRAGMA writable_schema = 0")
		self.conn.isolation_level = None
		self.conn.execute('VACUUM')
		self.conn.isolation_level = ''
		self.curs.execute("PRAGMA INTEGRITY_CHECK")
		self.curs.execute("PRAGMA foreign_key_check")
		print("Done. All tables are erased")

	cpdef generic_commit(self):
		self.conn.commit()

	cpdef assign_new_id_after_deletion(self, which_table):
		print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>entro in assign_new_id_after_deletion")
		print("which_table = ", which_table)
		while True:
			if which_table=='Kids':
				id_new = dbem.create_kid_id()
			elif which_table=='Therapies':
				id_new = dbem.create_therapy_id()
			elif which_table=='Treatments':
				id_new = dbem.create_treatment_id()
			elif which_table=='Sessions':
				id_new = dbem.create_session_id()
			elif which_table=='Matches':
				id_new = dbem.create_match_id()
			elif which_table=='Games':
				id_new = dbem.create_game_id()
			elif which_table=='Entertainments':
				id_new = dbem.create_entertainment_id()
			else:
				id_new = dbem.create_unique_id()
			self.curs.execute("SELECT token FROM All_id_tokens WHERE token=?",(id_new,))
			found_token = self.curs.fetchone()
			if not found_token:
				self.curs.execute("INSERT INTO All_id_tokens (token) VALUES (?)",(id_new,))
				self.conn.commit()
				print("The new created token is... {}".format(id_new))
				if which_table=='Games':
					self.curs.execute( "SELECT COUNT(rowid) FROM All_id_tokens")
					num_tokens = self.curs.fetchone()
					if num_tokens:
						num_tokens = num_tokens[0] - 1
					mdl.modify_default_names(id_new, 'All_id_tokens', num_tokens)
				break
			else:
				print("Error during token creation. The token is already present in the database.")
		return id_new

	#NUOVA CON ID SEPARATI!!
	cpdef assign_new_id(self, which_table):
		print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>entro in assign_new_id NORMALE")
		print("which_table = ", which_table)
		while True:
			if which_table=='Kids':
				id_new = dbem.create_kid_id()
			elif which_table=='Therapies':
				id_new = dbem.create_therapy_id()
			elif which_table=='Treatments':
				id_new = dbem.create_treatment_id()
			elif which_table=='Sessions':
				id_new = dbem.create_session_id()
			elif which_table=='Matches':
				id_new = dbem.create_match_id()
			elif which_table=='Games':
				id_new = dbem.create_game_id()
			elif which_table=='Entertainments':
				id_new = dbem.create_entertainment_id()
			else:
				id_new = dbem.create_unique_id()
			self.curs.execute("SELECT token FROM All_id_tokens WHERE token=?",(id_new,))
			found_token = self.curs.fetchone()
			if not found_token:
				self.curs.execute("INSERT INTO All_id_tokens (token) VALUES (?)",(id_new,))
				self.conn.commit()
				print("The new created token is... {}".format(id_new))
				if which_table=='Kids':
					self.curs.execute( "SELECT count(rowid) FROM Kids")
					num_tokens_kids = self.curs.fetchone()
					if num_tokens_kids:
						print("num_tokens_kids " ,num_tokens_kids)
						num_tokens_kids = num_tokens_kids[0] + 1

				elif which_table=='Therapies':
					self.curs.execute( "SELECT count(rowid) FROM Kids")
					num_tokens_kids = self.curs.fetchone()
					if num_tokens_kids:
						num_tokens_kids = num_tokens_kids[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Therapies")
					num_tokens_therapies = self.curs.fetchone()
					if num_tokens_therapies:
						print("---> num_tokens_therapies ", num_tokens_therapies)
						num_tokens_therapies = num_tokens_therapies[0] + 1
						position_token = num_tokens_kids + num_tokens_therapies  
						mdl.modify_default_names(id_new, 'All_id_tokens', position_token)

				elif which_table=='Treatments':
					self.curs.execute( "SELECT count(rowid) FROM Kids")
					num_tokens_kids = self.curs.fetchone()
					if num_tokens_kids:
						num_tokens_kids = num_tokens_kids[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Therapies")
					num_tokens_therapies = self.curs.fetchone()
					if num_tokens_therapies:
						num_tokens_therapies = num_tokens_therapies[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Treatments")
					num_tokens_treatments = self.curs.fetchone()
					if num_tokens_treatments:
						print("---> num_tokens_treatments ", num_tokens_treatments)
						num_tokens_treatments = num_tokens_treatments[0] + 1
						position_token = num_tokens_kids + num_tokens_therapies + num_tokens_treatments 
						mdl.modify_default_names(id_new, 'All_id_tokens', position_token)

				elif which_table=='Entertainments':
					self.curs.execute( "SELECT count(rowid) FROM Kids")
					num_tokens_kids = self.curs.fetchone()
					if num_tokens_kids:
						num_tokens_kids = num_tokens_kids[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Therapies")
					num_tokens_therapies = self.curs.fetchone()
					if num_tokens_therapies:
						num_tokens_therapies = num_tokens_therapies[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Treatments")
					num_tokens_treatments = self.curs.fetchone()
					if num_tokens_treatments:
						num_tokens_treatments = num_tokens_treatments[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Entertainments")
					num_tokens_entertainments = self.curs.fetchone()
					if num_tokens_entertainments:
						print("num_tokens_entertainments   ",num_tokens_entertainments)
						num_tokens_entertainments = num_tokens_entertainments[0] + 1
						position_token = num_tokens_kids + num_tokens_therapies + num_tokens_treatments + num_tokens_entertainments  
						mdl.modify_default_names(id_new, 'All_id_tokens', position_token)

				elif which_table=='Sessions':
					self.curs.execute( "SELECT count(rowid) FROM Kids")
					num_tokens_kids = self.curs.fetchone()
					if num_tokens_kids:
						num_tokens_kids = num_tokens_kids[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Therapies")
					num_tokens_therapies = self.curs.fetchone()
					if num_tokens_therapies:
						num_tokens_therapies = num_tokens_therapies[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Treatments")
					num_tokens_treatments = self.curs.fetchone()
					if num_tokens_treatments:
						num_tokens_treatments = num_tokens_treatments[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Entertainments")
					num_tokens_entertainments = self.curs.fetchone()
					if num_tokens_entertainments:
						num_tokens_entertainments = num_tokens_entertainments[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Sessions")
					num_tokens_sessions = self.curs.fetchone()
					if num_tokens_sessions:
						print("---> num_tokens_sessions ", num_tokens_sessions)
						num_tokens_sessions = num_tokens_sessions[0] + 1
						position_token = num_tokens_kids + num_tokens_therapies + num_tokens_treatments + num_tokens_entertainments + num_tokens_sessions
						mdl.modify_default_names(id_new, 'All_id_tokens', position_token)
				
				elif which_table=='Matches':
					self.curs.execute( "SELECT count(rowid) FROM Kids")
					num_tokens_kids = self.curs.fetchone()
					if num_tokens_kids:
						num_tokens_kids = num_tokens_kids[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Therapies")
					num_tokens_therapies = self.curs.fetchone()
					if num_tokens_therapies:
						num_tokens_therapies = num_tokens_therapies[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Treatments")
					num_tokens_treatments = self.curs.fetchone()
					if num_tokens_treatments:
						num_tokens_treatments = num_tokens_treatments[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Entertainments")
					num_tokens_entertainments = self.curs.fetchone()
					if num_tokens_entertainments:
						num_tokens_entertainments = num_tokens_entertainments[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Sessions")
					num_tokens_sessions = self.curs.fetchone()
					if num_tokens_sessions:
						num_tokens_sessions = num_tokens_sessions[0] + 1
					self.curs.execute( "SELECT count(rowid) FROM Matches")
					num_tokens_matches = self.curs.fetchone()
					if num_tokens_matches:
						print("---> num_tokens_matches ", num_tokens_matches)
						num_tokens_matches = num_tokens_matches[0] + 1
						position_token = num_tokens_kids + num_tokens_therapies + num_tokens_treatments + num_tokens_entertainments + num_tokens_sessions + num_tokens_matches
						mdl.modify_default_names(id_new, 'All_id_tokens', position_token)
				
				elif which_table=='Games':
					self.curs.execute( "SELECT count(rowid) FROM Kids")
					num_tokens_kids = self.curs.fetchone()
					if num_tokens_kids:
						num_tokens_kids = num_tokens_kids[0]
					self.curs.execute( "SELECT count(rowid) FROM Therapies")
					num_tokens_therapies = self.curs.fetchone()
					if num_tokens_therapies:
						num_tokens_therapies = num_tokens_therapies[0]
						position_token = num_tokens_kids + num_tokens_therapies  
					self.curs.execute( "SELECT count(rowid) FROM Treatments")
					num_tokens_treatments = self.curs.fetchone()
					if num_tokens_treatments:
						num_tokens_treatments = num_tokens_treatments[0]
					self.curs.execute( "SELECT count(rowid) FROM Entertainments")
					num_tokens_entertainments = self.curs.fetchone()
					if num_tokens_entertainments:
						num_tokens_entertainments = num_tokens_entertainments[0]
					self.curs.execute( "SELECT count(rowid) FROM Sessions")
					num_tokens_sessions = self.curs.fetchone()
					if num_tokens_sessions:
						num_tokens_sessions = num_tokens_sessions[0] 
					self.curs.execute( "SELECT count(rowid) FROM Matches")
					num_tokens_matches = self.curs.fetchone()
					if num_tokens_matches:
						num_tokens_matches = num_tokens_matches[0] 
					self.curs.execute( "SELECT count(rowid) FROM Games")
					num_tokens_games = self.curs.fetchone()
					print()
					print()					
					print("stampo tutti stampo tutti stampo tutti stampo tutti stampo tutti stampo tutti stampo tutti stampo tutti ")
					print("num_tokens_therapies ====", num_tokens_therapies)
					print("num_tokens_treatments ====", num_tokens_treatments)
					print("num_tokens_sessions ====", num_tokens_sessions)
					print("num_tokens_entertainments ====", num_tokens_entertainments)
					print("num_tokens_matches ====", num_tokens_matches)
					print("num_tokens_games ====", num_tokens_games)
					print("stampo tutti stampo tutti stampo tutti stampo tutti stampo tutti stampo tutti stampo tutti stampo tutti ")
					print()
					print()
					if num_tokens_games:
						print("---> num_tokens_games ", num_tokens_games)
						num_tokens_games = num_tokens_games[0] +7 #plus the spaces ... 
						position_token = num_tokens_kids + num_tokens_therapies + num_tokens_treatments + num_tokens_entertainments + num_tokens_sessions + num_tokens_matches + num_tokens_games
						mdl.modify_default_names(id_new, 'All_id_tokens', position_token)
					else:
						self.curs.execute( "SELECT COUNT(rowid) FROM All_id_tokens")
						num_tokens = self.curs.fetchone()
						if num_tokens:
							num_tokens = num_tokens[0]
						mdl.modify_default_names(id_new, 'All_id_tokens', num_tokens)
				break
			else:
				print("Error during token creation. The token is already present in the database.")
		return id_new

	cdef erase_part_of_database_if_full(self):
		cdef:
			bint is_present = 0
			bint is_present_t1 = 0
			bint is_present_t2 = 0
			bint is_present_e = 0
			bint is_present_k = 0
			bint is_present_s = 0
			bint is_present_m = 0
			bint is_present_g = 0
			bint are_all_present = 0
		self.curs.execute("SELECT count(name) FROM sqlite_master WHERE type='table' AND name='sqlite_sequence'")
		is_present = self.curs.fetchone()[0]
		if is_present:
			self.curs.execute("SELECT count(name) FROM sqlite_sequence WHERE name='Kids'")
			is_present_k = self.curs.fetchone()[0]
			self.curs.execute("SELECT count(name) FROM sqlite_sequence WHERE name='Therapies'")
			is_present_t1 = self.curs.fetchone()[0]
			self.curs.execute("SELECT count(name) FROM sqlite_sequence WHERE name='Treatments'")
			is_present_t2 = self.curs.fetchone()[0]
			self.curs.execute("SELECT count(name) FROM sqlite_sequence WHERE name='Entertainments'")
			is_present_e = self.curs.fetchone()[0]
			self.curs.execute("SELECT count(name) FROM sqlite_sequence WHERE name='Sessions'")
			is_present_s = self.curs.fetchone()[0]
			self.curs.execute("SELECT count(name) FROM sqlite_sequence WHERE name='Matches'")
			is_present_m = self.curs.fetchone()[0]
			self.curs.execute("SELECT count(name) FROM sqlite_sequence WHERE name='Games'")
			is_present_g = self.curs.fetchone()[0]
		are_all_present = is_present_t1 and is_present_t2 and is_present_e and is_present_k and is_present_s and is_present_g and is_present_m
		if are_all_present:
			self.curs.execute("SELECT seq FROM sqlite_sequence WHERE name='Kids'")
			kids_until_now = self.curs.fetchone()[0]
			print("kids_until_now ",kids_until_now)
			print("TIPO kids_until_now ",kids_until_now)
			print("TIPO kids_until_now ",kids_until_now)
			self.curs.execute("SELECT seq FROM sqlite_sequence WHERE name='Sessions'")
			sessions_until_now = self.curs.fetchone()[0]
			print("sessions_until_now ", sessions_until_now)
			print("sessions_until_now ", sessions_until_now)
			print("TIPO sessions_until_now ", sessions_until_now)
			print("TIPO sessions_until_now ", sessions_until_now)
			self.curs.execute("SELECT seq FROM sqlite_sequence WHERE name='Matches'")
			matches_until_now = self.curs.fetchone()[0]
			print("matches_until_now = ",matches_until_now)
			print("matches_until_now = ",matches_until_now)
			print("TIPO matches_until_now = ",matches_until_now)
			print("TIPO matches_until_now = ",matches_until_now)
			self.curs.execute("SELECT seq FROM sqlite_sequence WHERE name='Games'")
			games_until_now = self.curs.fetchone()[0]
			print("games_until_now = ",games_until_now)
			print("games_until_now = ",games_until_now)
			print("TIPO games_until_now = ",games_until_now)
			print("TIPO games_until_now = ",games_until_now)
			self.curs.execute("SELECT seq FROM sqlite_sequence WHERE name='Entertainments'")
			entertainments_until_now = self.curs.fetchone()[0]
			print("entertainments_until_now ",entertainments_until_now)
			print("TIPO entertainments_until_now ",entertainments_until_now)
			print("TIPO entertainments_until_now ",entertainments_until_now)
			self.curs.execute("SELECT seq FROM sqlite_sequence WHERE name='Therapies'")
			therapies_until_now = self.curs.fetchone()[0]
			print("therapies_until_now",therapies_until_now)
			print("TIPO therapies_until_now",therapies_until_now)
			print("TIPO therapies_until_now",therapies_until_now)
			self.curs.execute("SELECT seq FROM sqlite_sequence WHERE name='Treatments'")
			treatments_until_now = self.curs.fetchone()[0]
			print("treatments_until_now ",treatments_until_now)
			print("TIPO treatments_until_now ",treatments_until_now)
			print("TIPO treatments_until_now ",treatments_until_now)		
			############################change values in future!!!!???
			############################change values in future!!!!???
			############################change values in future!!!!???
			if kids_until_now>15000 or sessions_until_now>25000 or matches_until_now>35000 or games_until_now>45000:
				print(" >>>> OPZIONE, sono entrato	1 !!!")
				#dcs.copy_kids_sessions_in_new_file()
				#dcs.copy_kids_matches_in_new_file()
				#dcs.copy_kids_games_in_new_file()
				#dcs.copy_kids_questions_in_new_file()
			if kids_until_now>15000:
				print(" >>>> OPZIONE, sono entrato	2 = kids_until_now")
				#now = datetime.now()
				#data_ok = (now.strftime("%Y-%m-%d__%H:%M:%S"))
				#string_comm = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py 
				#/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_backup_before_deletions_in_date_{}.py""".format(data_ok)
				#command = re.sub('\s+', ' ', string_comm)
				#proc = Popen(command, shell=True, preexec_fn=os.setsid)
				#for i in range(10000,15000):
				#	dcd.delete_from_id(self, i)		
			if sessions_until_now>25000:
				print(" >>>> OPZIONE, sono entrato	3 = sessions_until_now")
				#now = datetime.now()
				#data_ok = (now.strftime("%Y-%m-%d__%H:%M:%S"))
				#string_comm = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py 
				#/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_backup_before_deletions_in_date_{}.py""".format(data_ok)
				#command = re.sub('\s+', ' ', string_comm)
				#proc = Popen(command, shell=True, preexec_fn=os.setsid)
				#for i in range(20000,25000):
				#	dcd.delete_from_id(self, i)
			if matches_until_now>35000:
				print(" >>>> OPZIONE, sono entrato	4 = MATCHES_until_now")
				#now = datetime.now()
				#data_ok = (now.strftime("%Y-%m-%d__%H:%M:%S"))
				#string_comm = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py 
				#/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_backup_before_deletions_in_date_{}.py""".format(data_ok)
				#command = re.sub('\s+', ' ', string_comm)
				#proc = Popen(command, shell=True, preexec_fn=os.setsid)
				#for i in range(30000,35000):
				#	dcd.delete_from_id(self, i)
			if games_until_now>45000:
				print(" >>>> OPZIONE, sono entrato	5 = games_until_now")
				#now = datetime.now()
				#data_ok = (now.strftime("%Y-%m-%d__%H:%M:%S"))
				#string_comm = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py 
				#/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_backup_before_deletions_in_date_{}.py""".format(data_ok)
				#command = re.sub('\s+', ' ', string_comm)
				#proc = Popen(command, shell=True, preexec_fn=os.setsid)
				#for i in range(40000,45000):
				#	dcd.delete_from_id(self, i)
			if entertainments_until_now>55000:
				print(" >>>> OPZIONE, sono entrato	6 = entertainments_until_now")
				#now = datetime.now()
				#data_ok = (now.strftime("%Y-%m-%d__%H:%M:%S"))
				#string_comm = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py 
				#/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_backup_before_deletions_in_date_{}.py""".format(data_ok)
				#command = re.sub('\s+', ' ', string_comm)
				#proc = Popen(command, shell=True, preexec_fn=os.setsid)
				#for i in range(50000,55000):
				#	dcd.delete_from_id(self, i)
			if therapies_until_now>75000:
				print(" >>>> OPZIONE, sono entrato	7 = therapies_until_now")
				#now = datetime.now()
				#data_ok = (now.strftime("%Y-%m-%d__%H:%M:%S"))
				#string_comm = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py 
				#/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_backup_before_deletions_in_date_{}.py""".format(data_ok)
				#command = re.sub('\s+', ' ', string_comm)
				#proc = Popen(command, shell=True, preexec_fn=os.setsid)
				#for i in range(70000,75000):
				#	dcd.delete_from_id(self, i)
			if treatments_until_now>85000:
				print(" >>>> OPZIONE, sono entrato	8 = treatments_until_now")
				#now = datetime.now()
				#data_ok = (now.strftime("%Y-%m-%d__%H:%M:%S"))
				#string_comm = """cp /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py 
				#/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_backup_before_deletions_in_date_{}.py""".format(data_ok)
				#command = re.sub('\s+', ' ', string_comm)
				#proc = Popen(command, shell=True, preexec_fn=os.setsid)
				#for i in range(80000,85000):
				#	dcd.delete_from_id(self, i)				
	# --------------------------------------------------------------------------------------------------------------------------------------
	# Build
	# --------------------------------------------------------------------------------------------------------------------------------------
	cpdef build_OimiSettings(self):
		dcb.build_OimiSettings_table(self)
	cpdef build_Allidtokens(self):
		dcb.build_Allidtokens_table(self)
	cpdef build_Kids(self):
		dcb.build_Kids_table(self)
	cpdef build_Kids_Parameters(self):
		dcb.build_Kids_Parameters_table(self)
	cpdef build_Patches(self):
		dcb.build_Patches_table(self)
	cpdef build_Scenarios(self):
		dcb.build_Scenarios_table(self)
	cpdef build_Kind_games(self):
		dcb.build_Kind_games_table(self)
	cpdef build_Type_games(self):
		dcb.build_Type_games_table(self)

	cpdef build_Achievements(self):
		dcb.build_Achievements_table(self)
	cpdef build_Symptoms(self):
		dcb.build_Symptoms_table(self)
	cpdef build_Issues(self):
		dcb.build_Issues_table(self)
	cpdef build_Comorbidities(self):
		dcb.build_Comorbidities_table(self)
	cpdef build_Strengths(self):
		dcb.build_Strengths_table(self)

	cpdef build_Impositions(self):
		dcb.build_Impositions_table(self)
	cpdef build_Needs(self):
		dcb.build_Needs_table(self)

	cpdef build_Audios(self):
		dcb.build_Audios_table(self)
	
	cpdef build_Therapies(self):
		dcb.build_Therapies_table(self)
	cpdef build_Treatments(self):
		dcb.build_Treatments_table(self)
	cpdef build_Class_free(self):
		dcb.build_Class_free_table(self)
	cpdef build_Entertainments(self):
		dcb.build_Entertainments_table(self)
	cpdef build_Sessions(self):
		dcb.build_Sessions_table(self)
	cpdef build_Matches(self):
		dcb.build_Matches_table(self)
	cpdef build_Games(self):
		dcb.build_Games_table(self)
	cpdef build_Questions(self):
		dcb.build_Questions_table(self)
	cpdef build_ActivityRules(self):
		dcb.build_ActivityRules_table(self)
	cpdef build_Targets(self):
		dcb.build_Targets_behaviours_table(self)
	cpdef build_TreatmentTarget(self):
		dcb.build_survey_treatment_targets_table(self)
	cpdef build_Stages(self):
		dcb.build_template_Stages_table(self)
	cpdef build_prompt_Stages(self):
		dcb.build_prompt_Stages_table(self)
	cpdef build_reward_Stages(self):
		dcb.build_reward_Stages_table(self)
	cpdef build_mood_Stages(self):
		dcb.build_mood_Stages_table(self)
	cpdef build_reaction_Stages(self):
		dcb.build_reaction_Stages_table(self)

	cpdef build_Substages(self):
		dcb.build_Substages_table(self)
	cpdef build_Prompt_Substages(self):
		dcb.build_Prompt_Substages_table(self)
	cpdef build_Reward_Substages(self):
		dcb.build_Reward_Substages_table(self)
	cpdef build_Mood_Substages(self):
		dcb.build_Mood_Substages_table(self)
	cpdef build_Reaction_Substages(self):
		dcb.build_Reaction_Substages_table(self)
	cpdef build_Children_Substages(self):
		dcb.build_ChildrenSubstages_table(self)

	cpdef build_EntertainmentSubstage(self):
		dcb.build_bridge_EntertainmentSubstage(self)

	cpdef build_TherapyTreatment(self):
		dcb.build_bridge_TherapyTreatment(self)
	cpdef build_TreatmentSession(self):
		dcb.build_bridge_TreatmentSession(self)
	cpdef build_TreatmentEntertainment(self):
		dcb.build_bridge_TreatmentEntertainment(self)
	cpdef build_TreatmentPastime(self):
		dcb.build_bridge_TreatmentPastime(self)
	cpdef build_SessionMatch(self):
		dcb.build_bridge_SessionMatch(self)
	cpdef build_MatchGame(self):
		dcb.build_bridge_MatchGame(self)
	cpdef build_GameQuestion(self):
		dcb.build_bridge_GameQuestion(self)
	cpdef build_QuestionPatch(self):
		dcb.build_bridge_QuestionPatch(self)
	
	cpdef build_KidTreatment(self):
		dcb.build_bridge_KidTreatment(self)
	cpdef build_KidTherapy(self):
		dcb.build_bridge_KidTherapy(self)
	cpdef build_KidSession(self):
		dcb.build_bridge_KidSession(self)
	cpdef build_KidEntertainment(self):
		dcb.build_bridge_KidEntertainment(self)
	cpdef build_KidMatch(self):
		dcb.build_bridge_KidMatch(self)
	cpdef build_KidGame(self):
		dcb.build_bridge_KidGame(self)
	cpdef build_KidQuestion(self):
		dcb.build_bridge_KidQuestion(self)
	
	cpdef build_KidAchievement(self):
		dcb.build_bridge_KidAchievement(self)
	cpdef build_KidSymptom(self):
		dcb.build_bridge_KidSymptom(self)
	cpdef build_KidIssue(self):
		dcb.build_bridge_KidIssue(self)
	cpdef build_KidComorbidity(self):
		dcb.build_bridge_KidComorbidity(self)
	cpdef build_KidStrength(self):
		dcb.build_bridge_KidStrength(self)

	cpdef build_KidNeed(self):
		dcb.build_bridge_KidNeed(self)
	cpdef build_KidImposition(self):
		dcb.build_bridge_KidImposition(self)

	cpdef build_NeedQuestion(self):
		dcb.build_bridge_NeedQuestion(self)
	cpdef build_ImpositionQuestion(self):
		dcb.build_bridge_ImpositionQuestion(self)
	cpdef build_SessionNote(self):
		dcb.build_bridge_SessionNote(self)

	cpdef build_SessionEnforcement(self):
		dcb.build_bridge_SessionEnforcement(self)
	cpdef build_SessionImposition(self):
		dcb.build_bridge_SessionImposition(self)
	cpdef build_SessionNeed(self):
		dcb.build_bridge_SessionNeed(self)

	cpdef build_ScenarioQuestion(self):
		dcb.build_bridge_ScenarioQuestion(self)
	cpdef build_ScenarioType(self):
		dcb.build_bridge_ScenarioType(self)
	cpdef build_ScenarioKind(self):
		dcb.build_bridge_ScenarioKind(self)
	cpdef build_Scenario4PCOQuestion(self):
		dcb.build_bridge_Scenario4PCOQuestion(self)
	cpdef build_Scenario4PSUQuestion(self):
		dcb.build_bridge_Scenario4PSUQuestion(self)

	cpdef build_ClassFreeTarget(self):
		dcb.build_bridge_ClassFreeTarget(self)
	# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	# Create
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef create_Short_recap_all_kids(self):
		dcb.create_Short_recap_all_kids_table(self)
	cpdef create_Full_sessions_played_recap(self):
		dcb.create_Full_sessions_played_recap_table(self)
	cpdef create_Full_played_recap_giga(self):
		dcb.create_Full_played_recap_giga_table(self)
	cpdef create_Full_entertainments_played(self):
		dcb.create_Full_played_recap_with_entertainment(self)
	cpdef fix_contraints_Full_sesions_summary(self):
		dcb.fix_contraints_Full_sesions_summary_table(self)
	cpdef fix_contraints_Full_therapies_summary(self):
		dcb.fix_contraints_Full_therapies_summary_table(self)
	cpdef fix_contraints_Full_entertainment_played_summary(self):
		dcb.fix_contraints_Full_entertainment_played_summary_table(self)

	cpdef create_definitive_one(self):
		dcb.create_definitive_recap_table_one(self)
	cpdef create_definitive_two(self):
		dcb.create_definitive_recap_table_two(self)
	cpdef compose_definitive(self):
		dcb.compose_definitive_recap(self)
	# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	# Construct
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef construct_ResultsTotalQuestion(self):
		dcb.construct_ResultsTotalQuestion_table(self)
	cpdef construct_ResultsTotalKindOfGame_raw(self):
		dcb.construct_ResultsTotalKindOfGame_table(self)
	cpdef construct_ResultsKindOfGame_new(self):
		dcb.construct_ResultsKindOfGame_new_table(self)
	cpdef construct_ResultsTotalKindOfGame(self):
		dcb.construct_ResultsTotalKindOfGame_table(self)
		dcb.construct_ResultsKindOfGame_new_table(self)
	cpdef construct_ResultsTotalTypeOfGame_raw(self):
		dcb.construct_ResultsTotalTypeOfGame_table(self)
	cpdef construct_ResultsTypeOfGame_new(self):
		dcb.construct_ResultsTypeOfGame_new_table(self)
	cpdef construct_ResultsTotalTypeOfGame(self):
		dcb.construct_ResultsTotalTypeOfGame_table(self)
		dcb.construct_ResultsTypeOfGame_new_table(self)
	cpdef fix_TotalResultsKindOfGame_new(self):
		dcb.fix_ResultsKindOfGame_new_table(self)
	cpdef construct_ResultsTotalGame(self):
		dcb.construct_ResultsTotalGame_table(self)
	cpdef construct_ResultsTotalMatch(self):
		dcb.construct_ResultsTotalMatch_table(self)
	cpdef construct_ResultsTotalSession(self):
		dcb.construct_ResultsTotalSession_table(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Place
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef place_patches_into_Patches(self):
		dcp.place_patches_into_Patches_table(self)
	cpdef place_entertainments(self):
		dcp.place_entertainments_into_table(self)
	cpdef place_data_into_Questions_Patches(self):
		dcp.place_data_into_Questions_Patches_table(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Populate
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef populate_scenarios_types(self):
		dcp.populate_scenarios_types_table(self)
	cpdef populate_scenarios_kinds(self):
		dcp.populate_scenarios_kinds_table(self)
	cpdef populate_scenarios_questions(self):
		dcp.populate_scenarios_questions_table(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Insert
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef insert_in_scenarios(self):
		dcp.insert_in_scenarios_table(self)
	cpdef insert_in_questions(self):
		dcp.insert_in_questions_table(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Fill
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef fill_recap(self, kid):
		dcp.fill_recap_table(self, kid)
	cpdef fill_summary(self, kid):
		dcp.fill_summary_table(self, kid)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Enable
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------	
	cpdef start_duration_trigger(self):
		dbtr.enable_duration_adjustment(self)
	cpdef start_pastimes_trigger(self):
		dbtr.enable_pastimes_trigger(self)
	cpdef start_pastimes_trigger_2(self):
		dbtr.enable_pastimes_trigger_2(self)
		
	cpdef start_patches_trigger(self):
		dbtr.enable_patches_trigger(self)
	cpdef start_kind_games_trigger(self):
		dbtr.enable_kind_trigger(self)
	cpdef start_free_games_trigger(self):
		dbtr.enable_free_trigger(self)
	cpdef start_deletion_triggers(self):
		dbtr.enable_deletion_trigger_kid_1(self)
		dbtr.enable_deletion_trigger_kid_2(self)
		dbtr.enable_deletion_trigger_question(self)
		#dbtr.enable_deletion_trigger_audios(self)
		#dbtr.enable_deletion_trigger_kid_3(self)
		#dbtr.enable_deletion_trigger_kid_4(self)
		#dbtr.enable_deletion_trigger_kid_5(self)
		#dbtr.enable_deletion_trigger_kid_6(self)
		#dbtr.enable_deletion_trigger_kid_7(self)
		#dbtr.enable_deletion_trigger_kid_achievement(self)
		#dbtr.enable_deletion_trigger_kid_imposition(self)
		#dbtr.enable_deletion_trigger_kid_symptom(self)		
	cpdef start_scores_triggers(self):
		dbtr.enable_update_tot_game_corrects(self)
		dbtr.enable_update_tot_game_errors(self)
		dbtr.enable_update_tot_game_indecisions(self)
		dbtr.enable_update_tot_game_already(self)
		dbtr.enable_insert_tot_game_answers(self)
		dbtr.enable_insert_tot_game_corrects(self)
		dbtr.enable_insert_tot_game_errors(self)
		dbtr.enable_insert_tot_game_indecisions(self)
		dbtr.enable_insert_tot_game_already(self)

		dbtr.enable_update_tot_match_answers(self)
		dbtr.enable_update_tot_match_corrects(self)
		dbtr.enable_update_tot_match_errors(self)
		dbtr.enable_update_tot_match_indecisions(self)
		dbtr.enable_update_tot_match_already(self)
		dbtr.enable_insert_tot_match_answers(self)
		dbtr.enable_insert_tot_match_corrects(self)
		dbtr.enable_insert_tot_match_errors(self)
		dbtr.enable_insert_tot_match_incecisions(self)
		dbtr.enable_insert_tot_match_already(self)

		dbtr.enable_update_tot_sess_answers(self)
		dbtr.enable_update_tot_sess_corrects(self)
		dbtr.enable_update_tot_sess_errors(self)
		dbtr.enable_update_tot_sess_indecisions(self)
		dbtr.enable_update_tot_sess_already(self)
		dbtr.enable_insert_tot_sess_answers(self)
		dbtr.enable_insert_tot_sess_corrects(self)
		dbtr.enable_insert_tot_sess_errors(self)
		dbtr.enable_insert_tot_sess_indecisions(self)
		dbtr.enable_insert_tot_sess_already(self)

		dbtr.enable_update_tot_kind_answers(self)
		dbtr.enable_update_tot_kind_corrects(self)
		dbtr.enable_update_tot_kind_errors(self)
		dbtr.enable_update_tot_kind_indecisions(self)
		dbtr.enable_update_tot_kind_already(self)
		dbtr.enable_insert_tot_kind_answers(self)
		dbtr.enable_insert_tot_kind_corrects(self)
		dbtr.enable_insert_tot_kind_errors(self)
		dbtr.enable_insert_tot_kind_indecisions(self)
		dbtr.enable_insert_tot_kind_already(self)

		dbtr.enable_update_tot_type_answers(self)
		dbtr.enable_update_tot_type_corrects(self)
		dbtr.enable_update_tot_type_errors(self)
		dbtr.enable_update_tot_type_indecisions(self)
		dbtr.enable_update_tot_type_already(self)
		dbtr.enable_insert_tot_type_answers(self)
		dbtr.enable_insert_tot_type_corrects(self)
		dbtr.enable_insert_tot_type_errors(self)
		dbtr.enable_insert_tot_type_indecisions(self)
		dbtr.enable_insert_tot_type_already(self)	
	cpdef start_disposable_triggers(self):
		dbtr.enable_disposable_trigger_11(self)
		dbtr.enable_disposable_trigger_12(self)
		dbtr.enable_disposable_trigger_21(self)
		dbtr.enable_disposable_trigger_22(self)
	cpdef start_needs_triggers(self):
		dbtr.enable_needs_trigger_1(self)
		dbtr.enable_needs_trigger_2(self)
		dbtr.enable_needs_trigger_3(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Generate
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef generate_audio_intro_exit_session(self):
		dbg.generate_audio_intro_exit_session_table(self)
	cpdef generate_audio_intro_exit_match(self):
		dbg.generate_audio_intro_exit_match_table(self)
	cpdef generate_audio_intro_exit_game(self):
		dbg.generate_audio_intro_exit_game_table(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Change
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef change_num_questions(self, int numq, int idg):
		dbg.change_num_questions_of_a_game(self, numq, idg)
	cpdef change_all_advisable_times(self):
		dbg.change_all_advisable_times_all_matches(self)
	cpdef change_all_desirable_times(self):
		dbg.change_all_desirable_times_all_sessions(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Delete
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef delete_by_id(self, int id_to_remove):
		dcd.delete_from_id(self, id_to_remove)
	cpdef delete_by_id_only_db_default(self, int id_to_remove):
		dcd.delete_from_id_only_db_default(self, id_to_remove)
	cpdef delete_by_bridge(self, str which_table, int first_param, int second_param, int must_eliminate):
		dcd.delete_row_from_bridge(self, which_table, first_param, second_param, must_eliminate)
	cpdef delete_by_str(self, str which_table, int first_param, str second_param, int must_eliminate):
		dcd.delete_row_from_bridge_str(self, which_table, first_param, second_param, must_eliminate)
	cpdef delete_all_by_first(self, str which_table, int first_id, int must_eliminate):
		dcd.delete_all_from_bridge_first(self, which_table, first_id, must_eliminate)
	cpdef delete_all_by_second(self, str which_table, int second_id):
		dcd.delete_all_from_bridge_second(self, which_table, second_id)
	# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	# Add
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	cpdef add_new_Therapy(self, int kid_id, int num_of_treatments, str canonical_intervention):
		dca.add_new_Therapy_table(self, kid_id, num_of_treatments, canonical_intervention)
	cpdef add_new_Treatment(self, int kid_id, str target_goal_1, str target_goal_2, str target_goal_3, int num_of_pastimes, int num_of_sessions, int num_of_entertainments):
		dca.add_new_Treatment_table(self, kid_id, target_goal_1, target_goal_2, target_goal_3, num_of_pastimes, num_of_sessions, num_of_entertainments)
	cpdef add_new_Entertainment(self, int kid_id, str class_of_game, str specific_guidelines):
		dca.add_new_Entertainment_table(self, kid_id, class_of_game, specific_guidelines)
	
	def add_new_Session(self, kid_id: int, mandatory_imposition: int, mandatory_need: int, *args):
		if not args:
			dca.add_new_Session_table(self, kid_id, mandatory_imposition, mandatory_need)
		else:
			dca.add_new_constrained_Session_table(self, kid_id, mandatory_imposition, mandatory_need, args)

	cpdef add_new_Match(self, str difficulty, int num_of_games, str variety, str order_difficulty_games, str order_quantity_questions, str criteria_arrangement, int advisable_time, scenario, bint disposable):
		dca.add_new_Match_table(self, difficulty, num_of_games, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, advisable_time, scenario, disposable)
	cpdef add_new_Game(self, int num_questions, str kind_game, str type_game, str difficulty, str order_difficulty_questions, int necessary_time, bint disposable, scenario, list list_types, list questions_already_choosen_prev_match):
		dca.add_new_Game_table(self, num_questions, kind_game, type_game, difficulty, order_difficulty_questions, necessary_time, disposable, scenario, list_types, questions_already_choosen_prev_match)
	cpdef add_new_Question(self, str kind_game, str type_game, str audio_id, int value, str description):
		dca.add_new_Question_table(self, kind_game, type_game, audio_id, value, description)
	cpdef add_new_SessionMatch(self, int session_id, int match_id):
		dca.add_new_SessionMatch_table(self, session_id, match_id)
	cpdef add_new_MatchGame(self, int match_id, int game_id):
		dca.add_new_MatchGame_table(self, match_id, game_id)
	cpdef add_new_GameQuestion(self, int game_id, int question_id):
		dca.add_new_GameQuestion_table(self, game_id, question_id)
	cpdef add_new_KidSession(self, int kid_id, int session_id):
		dca.add_new_KidSession_table(self, kid_id, session_id)
	cpdef add_new_KidImposition(self, int kid_id, str imposition_name):
		dca.add_new_KidImposition_table(self, kid_id, imposition_name)
	cpdef add_new_KidNeed(self, int kid_id, int need_id):
		dca.add_new_KidNeed_table(self, kid_id, need_id)
	
	cpdef add_new_KidAchievement(self, int kid_id, str achievement_name):
		dca.add_new_KidAchievement_table(self, kid_id, achievement_name)
	cpdef add_new_KidSymptom(self, int kid_id, str symptom_name):
		dca.add_new_KidSymptom_table(self, kid_id, symptom_name)
	cpdef add_new_KidIssue(self, int kid_id, str issue_name):
		dca.add_new_KidIssue_table(self, kid_id, issue_name)
	cpdef add_new_KidComorbidity(self, int kid_id, str comorbidity_name):
		dca.add_new_KidComorbidity_table(self, kid_id, comorbidity_name)
	cpdef add_new_KidStrength(self, int kid_id, str strength_name):
		dca.add_new_KidStrength_table(self, kid_id, strength_name)

	cpdef add_new_SessionEnforcement(self, int session_id, str enforcement_name):
		dca.add_new_SessionEnforcement_table(self, session_id, enforcement_name)
	cpdef add_new_SessionImposition(self, int session_id, str imposition_name):
		dca.add_new_SessionImposition_table(self, session_id, imposition_name)
	cpdef add_new_SessionNeed(self, int session_id, int need_id):
		dca.add_new_SessionNeed_table(self, session_id, need_id)

	cpdef add_new_TreatmentEntertainment(self, int treatment_id, int entertainment_id):
		dca.add_new_bridge_TreatmentEntertainment(self, treatment_id, entertainment_id)

	cpdef add_new_TreatmentSession(self, int treatment_id, int session_id):
		dca.add_new_bridge_TreatmentSession(self, treatment_id, session_id)

	cpdef add_new_TreatmentPastime(self, int treatment_id, int pastime_id):
		dca.add_new_bridge_TreatmentPastime(self, treatment_id, pastime_id)

	cpdef add_to_OldKidNeed(self, int kid_id, int old_need_id):
		dca.add_to_OldKidNeed_table(self, kid_id, old_need_id)
	cpdef add_to_OldKidSymptom(self, int kid_id, str old_symptom_name):
		dca.add_to_OldKidSymptom_table(self, kid_id, old_symptom_name)
	cpdef add_to_OldKidIssue(self, kid_id, old_issue_name):
		dca.add_to_OldKidIssue_table(self, kid_id, old_issue_name)
	cpdef add_to_OldKidComorbidity(self, int kid_id, str old_comorbidity_name):
		dca.add_to_OldKidComorbidity_table(self, kid_id, old_comorbidity_name)
	cpdef add_to_OldKidStrength(self, int kid_id, str old_strenght_name):
		dca.add_to_OldKidStrength_table(self, kid_id, old_strenght_name)		
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Take
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------	
	cdef take_one_session_56(self, int which_kid):
		dct.take_one_default_session_56(self, which_kid)
	cdef take_one_session_78(self, int which_kid):
		dct.take_one_default_session_78(self, which_kid)
	cdef take_one_session_910(self, int which_kid):
		dct.take_one_default_session_910(self, which_kid)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Update 
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef update_Needs_constraints(self, str focus_contr, str need_contr, str position):
		dcu.update_Needs_constraints(self, focus_contr, need_contr, position)

	cpdef update_Kind_constraints(self, str new_kind, str new_details):
		dcu.update_Kind_constraints(self, new_kind, new_details)
		
	cpdef update_Patches_genre_1(self, str new_genre):
		dcu.update_Patches_genre_1(self, new_genre)

	cpdef update_Patches_genre_2(self, str new_subject, str new_genre):
		dcu.update_Patches_genre_2(self, new_subject, new_genre)

	cpdef update_Patches_constraints(self, str new_subject, str new_color):
		dcu.update_Patches_constraints(self, new_subject, new_color)

	cpdef update_constraint_of(self, str constraint_str, str table):
		dcu.update_constraint_of(self, constraint_str, table)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Insert
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef insert_new_Audios_table(self, str constraint_audio, str audio_path, int is_a_substitution):
		dcu.insert_new_Audios_table(self, constraint_audio, audio_path, is_a_substitution)

	cpdef insert_new_Achievements_table(self, str constraint_str):
		dcu.insert_new_Achievements_table(self, constraint_str)

	cpdef insert_new_Needs_table(self, str constraint_str_1, str constraint_str_2, str scope):
		dcu.insert_new_Needs_table(self, constraint_str_1, constraint_str_2, scope)
	cpdef insert_new_Symptoms_table(self, str constraint_str):
		dcu.insert_new_Symptoms_table(self, constraint_str)
	cpdef insert_new_Impositions_table(self, str constraint_str):
		dcu.insert_new_Impositions_table(self, constraint_str)

	cpdef insert_new_Kind_of_games_table(self, str constraint_kind, str details_kind):
		dcu.insert_new_Kind_of_games_table(self, constraint_kind, details_kind)
	cpdef insert_new_Type_of_games_table(self, str constraint_type, str details, str example, str kind):
		dcu.insert_new_Type_of_games_table(self, constraint_type, details, example, kind)

	cpdef insert_new_Patches_table(self, str new_subject, str new_color, str new_genre):
		dcu.insert_new_Patches_table(self, new_subject, new_color, new_genre)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Refresh
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef refresh_Patches_table(self, str new_subject, str new_color, str new_genre):
		dcu.refresh_Patches_table(self, new_subject, new_color, new_genre)

	cpdef refresh_Treatments_Pastimes_table(self):
		dcu.refresh_Treatments_Pastimes_table(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Renew
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef renew_Kids_Achievements(self, int which_kid):
		dcr.renew_Kids_Achievements_table(self, which_kid)
	cpdef renew_Kids_Achievements_1(self, int which_kid):
		dcr.renew_Kids_Achievements_table_1(self, which_kid)
	cpdef renew_Kids_Achievements_2(self, int which_kid):
		dcr.renew_Kids_Achievements_table_2(self, which_kid)
	cpdef renew_Kids_Achievements_3(self, int which_kid):
		dcr.renew_Kids_Achievements_table_3(self, which_kid)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Put
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef put_kid_questions_scores(self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
		str quantity_feedback_param, str grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest):
		dcu.put_kid_questions_scores(self, num_answers_param, corrects_param, errors_param, indecisions_param, already_param, time_param, 
			quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest)
	cpdef put_kid_games_scores(self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
		str quantity_feedback_param, str grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest):
		dcu.put_kid_games_scores(self, num_answers_param, corrects_param, errors_param, indecisions_param, already_param, time_param, 
			quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest)
	cpdef put_kid_matches_scores(self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
		int quantity_feedback_param, int grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest):
		dcu.put_kid_matches_scores(self, num_answers_param, corrects_param, errors_param, indecisions_param, already_param, time_param,  
			quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest)
	cpdef put_kid_session_scores(self, float time_param, int quantity_feedback_param, int grade_feedback_param, float value_feedback_param,
		int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest):
		dcu.put_kid_session_scores(self, time_param, quantity_feedback_param, grade_feedback_param, value_feedback_param,
			curr_kid_id, curr_session, curr_match, curr_game, curr_quest)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Include
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef include_all_feeds_in_once(self, int kid_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
		dcu.include_all_feeds_in_once(self, kid_id, questions_feeds, the_session_feeds, matches_feeds, games_feeds)
	cpdef include_all_results_in_once(self, int kid_id, float time_sess, list questions_results, list matches_results, list games_results):
		dcu.include_all_results_in_once(self, kid_id, time_sess, questions_results, matches_results, games_results)
	cpdef include_all_feeds_in_once_given_session(self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
		dcu.include_all_feeds_in_once_given_session(self, kid_id, session_id, questions_feeds, the_session_feeds, matches_feeds, games_feeds)
	cpdef include_all_results_in_once_given_session(self, int kid_id, int session_id, float time_sess, list questions_results, list matches_results, list games_results):
		dcu.include_all_results_in_once_given_session(self, kid_id, session_id, time_sess, questions_results, matches_results, games_results)		


	cpdef include_therapist_grade_session(self, int trat_id, int sess_id, str grade):
		dcu.include_new_therapist_grade_session(self, trat_id, sess_id, grade)

	cpdef include_therapist_grade_entertainment(self, int trat_id, int enter_id, str grade):
		dcu.include_new_therapist_grade_entertainment(self, trat_id, enter_id, grade)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Store
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef store_scores_results_offline(self):
		dcs.store_all_scores_and_results_offline()
	cpdef store_scores_last_kid_offline(self, int which_kid):
		dcs.store_all_scores_last_kid_offline(which_kid)
	cpdef store_kids_sessions_offline(self):
		dcs.store_kids_sessions_so_far_offline()
	cpdef store_kids_matches_offline(self):
		dcs.store_kids_matches_so_far_offline()
	cpdef store_kids_games_offline(self):
		dcs.store_kids_games_so_far_offline()
	cpdef store_kids_questions_offline(self):
		dcs.store_kids_questions_so_far_offline()
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Check
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef int check_session_mixed(self, session_id):
		resp = dcc.check_session_is_mixed(self, session_id)
		return resp 
	cpdef int check_match_mixed(self, match_id):
		resp = dcc.check_match_is_mixed(self, match_id)
		return resp

	cpdef build_OldKidSymptom(self):
		dcb.build_bridge_OldKidSymptom(self)
	cpdef build_OldKidNeed(self):
		dcb.build_bridge_OldKidNeed(self)
	cpdef build_OldKidIssue(self):
		dcb.build_bridge_OldKidIssue(self)
	cpdef build_OldKidComorbidity(self):
		dcb.build_bridge_OldKidComorbidity(self)
	cpdef build_OldKidStrength(self):
		dcb.build_bridge_OldKidStrength(self)
	# ----------------------------------------------------------------------------------------------------------------------------------------
	# Link
	# ----------------------------------------------------------------------------------------------------------------------------------------
	cpdef build_Treatments_Traits(self):
		dcb.build_link_Treatments_Traits(self)
	
