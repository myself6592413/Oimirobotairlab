""" Oimi robot databse add_prediction, 
	predict a complete new session with its matches and games for a kid, according to their achievements, symptoms, needs

	Notes:
		For other detailed info, look at dabatase_doc --> add_prediction

	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/learning/game_classification/' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/learning/game_classification/')
import re
import csv
import random
from typing import Optional
import database_manager as db
import db_extern_methods as dbem
import create_session_with_prediction as cswp
cimport add_prediction
cimport add_prediction_2 as adp2
cimport add_prediction_3 as adp3
cimport add_prediction_second_half as apsh
cimport security_prediction as sp
from database_manager cimport DatabaseManager
import faulthandler; faulthandler.enable()
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class DatabaseManager_2(DatabaseManager):
	def __cinit__(self, kid_id):
		self.kid_id = kid_id
		print("self kid DatabaseManager_2 = ", self.kid_id)
		#DatabaseManager.nop(self)
		#print("self kid DatabaseManager_2 = ", self.kid_id)
		self.take_csvs()
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Class Attributes getters,setters 
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	@property
	def status(self):
		return self.status
	@status.setter
	def status(self, value):
		self.status = value
	@property
	def kid_id(self):
		return self.kid_id
	@kid_id.setter
	def kid_id(self, value):
		self.kid_id = value
	@property
	def kids_needs(self):
		return self.kids_needs
	@kids_needs.setter
	def kids_needs(self, value):
		self.kids_needs = value	

	@property
	def audio_slow(self):
		return self.audio_slow
	@audio_slow.setter
	def audio_slow(self, value):
		self.audio_slow = value


	@property
	def kids_syms(self):
		return self.kids_syms
	@kids_syms.setter
	def kids_syms(self, value):
		self.kids_syms = value
	@property
	def csv_lines(self):
		return self.csv_lines
	@csv_lines.setter
	def csv_lines(self, value):
		self.csv_lines = value
	@property
	def outputs_table_1(self):
		return self.outputs_table_1
	@outputs_table_1.setter
	def outputs_table_1(self, value):
		self.outputs_table_1 = value
	@property
	def outputs_table_2(self):
		return self.outputs_table_2
	@outputs_table_2.setter
	def outputs_table_2(self, value):
		self.outputs_table_2 = value
	@property
	def outputs_table_3(self):
		return self.outputs_table_3
	@outputs_table_3.setter
	def outputs_table_3(self, value):
		self.outputs_table_3 = value
	@property
	def outputs_table_4_m1(self):
		return self.outputs_table_4_m1
	@outputs_table_4_m1.setter
	def outputs_table_4_m1(self, value):
		self.outputs_table_4_m1 = value
	@property
	def outputs_table_4_m2(self):
		return self.outputs_table_4_m2
	@outputs_table_4_m2.setter
	def outputs_table_4_m2(self, value):
		self.outputs_table_4_m2 = value
	@property
	def outputs_table_4_m3(self):
		return self.outputs_table_4_m3
	@outputs_table_4_m3.setter
	def outputs_table_4_m3(self, value):
		self.outputs_table_4_m3 = value
	@property
	def outputs_table_4_m4(self):
		return self.outputs_table_4_m4
	@outputs_table_4_m4.setter
	def outputs_table_4_m4(self, value):
		self.outputs_table_4_m4 = value
	@property
	def outputs_table_5_m1_g1(self):
		return self.outputs_table_5_m1_g1
	@outputs_table_5_m1_g1.setter
	def outputs_table_5_m1_g1(self, value):
		self.outputs_table_5_m1_g1 = value
	@property
	def outputs_table_5_m1_g2(self):
		return self.outputs_table_5_m1_g2
	@outputs_table_5_m1_g2.setter
	def outputs_table_5_m1_g2(self, value):
		self.outputs_table_5_m1_g2 = value
	@property
	def outputs_table_5_m1_g3(self):
		return self.outputs_table_5_m1_g3
	@outputs_table_5_m1_g3.setter
	def outputs_table_5_m1_g3(self, value):
		self.outputs_table_5_m1_g3 = value
	@property
	def outputs_table_5_m1_g4(self):
		return self.outputs_table_5_m1_g4
	@outputs_table_5_m1_g4.setter
	def outputs_table_5_m1_g4(self, value):
		self.outputs_table_5_m1_g4 = value
	@property
	def outputs_table_5_m2_g1(self):
		return self.outputs_table_5_m2_g1
	@outputs_table_5_m2_g1.setter
	def outputs_table_5_m2_g1(self, value):
		self.outputs_table_5_m2_g1 = value
	@property
	def outputs_table_5_m2_g2(self):
		return self.outputs_table_5_m2_g2
	@outputs_table_5_m2_g2.setter
	def outputs_table_5_m2_g2(self, value):
		self.outputs_table_5_m2_g2 = value
	@property
	def outputs_table_5_m2_g3(self):
		return self.outputs_table_5_m2_g3
	@outputs_table_5_m2_g3.setter
	def outputs_table_5_m2_g3(self, value):
		self.outputs_table_5_m2_g3 = value
	@property
	def outputs_table_5_m2_g4(self):
		return self.outputs_table_5_m2_g4
	@outputs_table_5_m2_g4.setter
	def outputs_table_5_m2_g4(self, value):
		self.outputs_table_5_m2_g4 = value
	@property
	def outputs_table_5_m3_g1(self):
		return self.outputs_table_5_m3_g1
	@outputs_table_5_m3_g1.setter
	def outputs_table_5_m3_g1(self, value):
		self.outputs_table_5_m3_g1 = value
	@property
	def outputs_table_5_m3_g2(self):
		return self.outputs_table_5_m3_g2
	@outputs_table_5_m3_g2.setter
	def outputs_table_5_m3_g2(self, value):
		self.outputs_table_5_m3_g2 = value
	@property
	def outputs_table_5_m3_g3(self):
		return self.outputs_table_5_m3_g3
	@outputs_table_5_m3_g3.setter
	def outputs_table_5_m3_g3(self, value):
		self.outputs_table_5_m3_g3 = value
	@property
	def outputs_table_5_m3_g4(self):
		return self.outputs_table_5_m3_g4
	@outputs_table_5_m3_g4.setter
	def outputs_table_5_m3_g4(self, value):
		self.outputs_table_5_m3_g4 = value
	@property
	def outputs_table_5_m4_g1(self):
		return self.outputs_table_5_m4_g1
	@outputs_table_5_m4_g1.setter
	def outputs_table_5_m4_g1(self, value):
		self.outputs_table_5_m4_g1 = value
	@property
	def outputs_table_5_m4_g2(self):
		return self.outputs_table_5_m4_g2
	@outputs_table_5_m4_g2.setter
	def outputs_table_5_m4_g2(self, value):
		self.outputs_table_5_m4_g2 = value
	@property
	def outputs_table_5_m4_g3(self):
		return self.outputs_table_5_m4_g3
	@outputs_table_5_m4_g3.setter
	def outputs_table_5_m4_g3(self, value):
		self.outputs_table_5_m4_g3 = value
	@property
	def outputs_table_5_m4_g4(self):
		return self.outputs_table_5_m4_g4
	@outputs_table_5_m4_g4.setter
	def outputs_table_5_m4_g4(self, value):
		self.outputs_table_5_m4_g4 = value
	@property
	def movements_allowed(self):
		return self.movements_allowed
	@movements_allowed.setter
	def movements_allowed(self, value):
		self.movements_allowed = value
	@property
	def extra_time_tolerance(self):
		return self.extra_time_tolerance
	@extra_time_tolerance.setter
	def extra_time_tolerance(self, value):
		self.extra_time_tolerance = value
	@property
	def body_enabled(self):
		return self.body_enabled
	@body_enabled.setter
	def body_enabled(self, value):
		self.body_enabled = value
	@property
	def exhortations(self):
		return self.exhortations
	@exhortations.setter
	def exhortations(self, value):
		self.exhortations = value
	@property
	def creation_way(self):
		return self.creation_way
	@creation_way.setter
	def creation_way(self, value):
		self.creation_way = value
	@property
	def repeat_intro(self):
		return self.repeat_intro
	@repeat_intro.setter
	def repeat_intro(self, value):
		self.repeat_intro = value
	@property
	def rules_explication(self):
		return self.rules_explication
	@rules_explication.setter
	def rules_explication(self, value):
		self.rules_explication = value
	@property
	def repeat_question(self):
		return self.repeat_question
	@repeat_question.setter
	def repeat_question(self, value):
		self.repeat_question = value
	@property
	def disposable(self):
		return self.disposable
	@disposable.setter
	def disposable(self, value):
		self.disposable = value
	@property
	def desirable_time(self):
		return self.desirable_time
	@desirable_time.setter
	def desirable_time(self, value):
		self.desirable_time = value
	@property
	def ask_never_asked_q(self):
		return self.ask_never_asked_q
	@ask_never_asked_q.setter
	def ask_never_asked_q(self, value):
		self.ask_never_asked_q = value
	@property
	def stress_flag(self):
		return self.stress_flag
	@stress_flag.setter
	def stress_flag(self, value):
		self.stress_flag = value
	@property
	def plus_one_game(self):
		return self.plus_one_game
	@plus_one_game.setter
	def plus_one_game(self, value):
		self.plus_one_game = value
	@property
	def search_str(self):
		return self.search_str
	@search_str.setter
	def search_str(self, value):
		self.search_str = value
	@property
	def new_deleletion_index(self):
		return self.new_deleletion_index
	@new_deleletion_index.setter
	def new_deleletion_index(self, value):
		self.new_deleletion_index = value
	@property
	def new_id_token(self):
		return self.new_id_token
	@new_id_token.setter
	def new_id_token(self, value):
		self.new_id_token = value
	@property
	def imposed_ques_id(self):
		return self.imposed_ques_id
	@imposed_ques_id.setter
	def imposed_ques_id(self, value):
		self.imposed_ques_id = value
	@property
	def led_brightness(self):
		return self.led_brightness
	@led_brightness.setter
	def led_brightness(self, value):
		self.led_brightness = value
	@property
	def num_of_matches(self):
		return self.num_of_matches
	@num_of_matches.setter
	def num_of_matches(self, value):
		self.num_of_matches = value
	@property
	def complexity(self):
		return self.complexity
	@complexity.setter
	def complexity(self, value):
		self.complexity = value
	@property
	def matches_order_diff(self):
		return self.matches_order_diff
	@matches_order_diff.setter
	def matches_order_diff(self, value):
		self.matches_order_diff = value
	@property
	def order_quan_games(self):
		return self.order_quan_games
	@order_quan_games.setter
	def order_quan_games(self, value):
		self.order_quan_games = value
	@property
	def order_diff_matches(self):
		return self.order_diff_matches
	@order_diff_matches.setter
	def order_diff_matches(self, value):
		self.order_diff_matches = value
	@property
	def difficulty_mat_1(self):
		return self.difficulty_mat_1
	@difficulty_mat_1.setter
	def difficulty_mat_1(self, value):
		self.difficulty_mat_1 = value
	@property
	def difficulty_mat_2(self):
		return self.difficulty_mat_2
	@difficulty_mat_2.setter
	def difficulty_mat_2(self, value):
		self.difficulty_mat_2 = value
	@property
	def difficulty_mat_3(self):
		return self.difficulty_mat_3
	@difficulty_mat_3.setter
	def difficulty_mat_3(self, value):
		self.difficulty_mat_3 = value
	@property
	def difficulty_mat_4(self):
		return self.difficulty_mat_4
	@difficulty_mat_4.setter
	def difficulty_mat_4(self, value):
		self.difficulty_mat_4 = value
	@property
	def games_m1(self):
		return self.games_m1
	@games_m1.setter
	def games_m1(self, value):
		self.games_m1 = value
	@property
	def games_m2(self):
		return self.games_m2
	@games_m2.setter
	def games_m2(self, value):
		self.games_m2 = value
	@property
	def games_m3(self):
		return self.games_m3
	@games_m3.setter
	def games_m3(self, value):
		self.games_m3 = value
	@property
	def games_m4(self):
		return self.games_m4
	@games_m4.setter
	def games_m4(self, value):
		self.games_m4 = value
	@property
	def diff_m1_g1(self):
		return self.diff_m1_g1
	@diff_m1_g1.setter
	def diff_m1_g1(self, value):
		self.diff_m1_g1 = value
	@property
	def diff_m1_g2(self):
		return self.diff_m1_g2
	@diff_m1_g2.setter
	def diff_m1_g2(self, value):
		self.diff_m1_g2 = value
	@property
	def diff_m1_g3(self):
		return self.diff_m1_g3
	@diff_m1_g3.setter
	def diff_m1_g3(self, value):
		self.diff_m1_g3 = value
	@property
	def diff_m1_g4(self):
		return self.diff_m1_g4
	@diff_m1_g4.setter
	def diff_m1_g4(self, value):
		self.diff_m1_g4 = value
	@property
	def diff_m2_g1(self):
		return self.diff_m2_g1
	@diff_m2_g1.setter
	def diff_m2_g1(self, value):
		self.diff_m2_g1 = value
	@property
	def diff_m2_g2(self):
		return self.diff_m2_g2
	@diff_m2_g2.setter
	def diff_m2_g2(self, value):
		self.diff_m2_g2 = value
	@property
	def diff_m2_g3(self):
		return self.diff_m2_g3
	@diff_m2_g3.setter
	def diff_m2_g3(self, value):
		self.diff_m2_g3 = value
	@property
	def diff_m2_g4(self):
		return self.diff_m2_g4
	@diff_m2_g4.setter
	def diff_m2_g4(self, value):
		self.diff_m2_g4 = value				
	@property
	def diff_m3_g1(self):
		return self.diff_m3_g1
	@diff_m3_g1.setter
	def diff_m3_g1(self, value):
		self.diff_m3_g1 = value
	@property
	def diff_m3_g2(self):
		return self.diff_m3_g2
	@diff_m3_g2.setter
	def diff_m3_g2(self, value):
		self.diff_m3_g2 = value
	@property
	def diff_m3_g3(self):
		return self.diff_m3_g3
	@diff_m3_g3.setter
	def diff_m3_g3(self, value):
		self.diff_m3_g3 = value
	@property
	def diff_m3_g4(self):
		return self.diff_m3_g4
	@diff_m3_g4.setter
	def diff_m3_g4(self, value):
		self.diff_m3_g4 = value
	@property
	def diff_m4_g1(self):
		return self.diff_m4_g1
	@diff_m4_g1.setter
	def diff_m4_g1(self, value):
		self.diff_m4_g1 = value
	@property
	def diff_m4_g2(self):
		return self.diff_m4_g2
	@diff_m4_g2.setter
	def diff_m4_g2(self, value):
		self.diff_m4_g2 = value
	@property
	def diff_m4_g3(self):
		return self.diff_m4_g3
	@diff_m4_g3.setter
	def diff_m4_g3(self, value):
		self.diff_m4_g3 = value
	@property
	def diff_m4_g4(self):
		return self.diff_m4_g4
	@diff_m4_g4.setter
	def diff_m4_g4(self, value):
		self.diff_m4_g4 = value
	@property
	def kind_m1_g1(self):
		return self.kind_m1_g1
	@kind_m1_g1.setter
	def kind_m1_g1(self, value):
		self.kind_m1_g1 = value
	@property
	def kind_m1_g2(self):
		return self.kind_m1_g2
	@kind_m1_g2.setter
	def kind_m1_g2(self, value):
		self.kind_m1_g2 = value
	@property
	def kind_m1_g3(self):
		return self.kind_m1_g3
	@kind_m1_g3.setter
	def kind_m1_g3(self, value):
		self.kind_m1_g3 = value
	@property
	def kind_m1_g4(self):
		return self.kind_m1_g4
	@kind_m1_g4.setter
	def kind_m1_g4(self, value):
		self.kind_m1_g4 = value
	@property
	def kind_m2_g1(self):
		return self.kind_m2_g1
	@kind_m2_g1.setter
	def kind_m2_g1(self, value):
		self.kind_m2_g1 = value
	@property
	def kind_m2_g2(self):
		return self.kind_m2_g2
	@kind_m2_g2.setter
	def kind_m2_g2(self, value):
		self.kind_m2_g2 = value
	@property
	def kind_m2_g3(self):
		return self.kind_m2_g3
	@kind_m2_g3.setter
	def kind_m2_g3(self, value):
		self.kind_m2_g3 = value
	@property
	def kind_m2_g4(self):
		return self.kind_m2_g4
	@kind_m2_g4.setter
	def kind_m2_g4(self, value):
		self.kind_m2_g4 = value
	@property
	def kind_m3_g1(self):
		return self.kind_m3_g1
	@kind_m3_g1.setter
	def kind_m3_g1(self, value):
		self.kind_m3_g1 = value
	@property
	def kind_m3_g2(self):
		return self.kind_m3_g2
	@kind_m3_g2.setter
	def kind_m3_g2(self, value):
		self.kind_m3_g2 = value
	@property
	def kind_m3_g3(self):
		return self.kind_m3_g3
	@kind_m3_g3.setter
	def kind_m3_g3(self, value):
		self.kind_m3_g3 = value
	@property
	def kind_m3_g4(self):
		return self.kind_m3_g4
	@kind_m3_g4.setter
	def kind_m3_g4(self, value):
		self.kind_m3_g4 = value
	@property
	def kind_m4_g1(self):
		return self.kind_m4_g1
	@kind_m4_g1.setter
	def kind_m4_g1(self, value):
		self.kind_m4_g1 = value
	@property
	def kind_m4_g2(self):
		return self.kind_m4_g2
	@kind_m4_g2.setter
	def kind_m4_g2(self, value):
		self.kind_m4_g2 = value
	@property
	def kind_m4_g3(self):
		return self.kind_m4_g3
	@kind_m4_g3.setter
	def kind_m4_g3(self, value):
		self.kind_m4_g3 = value
	@property
	def kind_m4_g4(self):
		return self.kind_m4_g4
	@kind_m4_g4.setter
	def kind_m4_g4(self, value):
		self.kind_m4_g4 = value
	@property
	def m1_questions_g1(self):
		return self.m1_questions_g1
	@m1_questions_g1.setter
	def m1_questions_g1(self, value):
		self.m1_questions_g1 = value
	@property
	def m1_questions_g2(self):
		return self.m1_questions_g2
	@m1_questions_g2.setter
	def m1_questions_g2(self, value):
		self.m1_questions_g2 = value
	@property
	def m1_questions_g3(self):
		return self.m1_questions_g3
	@m1_questions_g3.setter
	def m1_questions_g3(self, value):
		self.m1_questions_g3 = value
	@property
	def m1_questions_g4(self):
		return self.m1_questions_g4
	@m1_questions_g4.setter
	def m1_questions_g4(self, value):
		self.m1_questions_g4 = value
	@property
	def m2_questions_g1(self):
		return self.m2_questions_g1
	@m2_questions_g1.setter
	def m2_questions_g1(self, value):
		self.m2_questions_g1 = value
	@property
	def m2_questions_g2(self):
		return self.m2_questions_g2
	@m2_questions_g2.setter
	def m2_questions_g2(self, value):
		self.m2_questions_g2 = value
	@property
	def m2_questions_g3(self):
		return self.m2_questions_g3
	@m2_questions_g3.setter
	def m2_questions_g3(self, value):
		self.m2_questions_g3 = value
	@property
	def m2_questions_g4(self):
		return self.m2_questions_g4
	@m2_questions_g4.setter
	def m2_questions_g4(self, value):
		self.m2_questions_g4 = value
	@property
	def m3_questions_g1(self):
		return self.m3_questions_g1
	@m3_questions_g1.setter
	def m3_questions_g1(self, value):
		self.m3_questions_g1 = value
	@property
	def m3_questions_g2(self):
		return self.m3_questions_g2
	@m3_questions_g2.setter
	def m3_questions_g2(self, value):
		self.m3_questions_g2 = value
	@property
	def m3_questions_g3(self):
		return self.m3_questions_g3
	@m3_questions_g3.setter
	def m3_questions_g3(self, value):
		self.m3_questions_g3 = value
	@property
	def m3_questions_g4(self):
		return self.m3_questions_g4
	@m3_questions_g4.setter
	def m3_questions_g4(self, value):
		self.m3_questions_g4 = value
	@property
	def m4_questions_g1(self):
		return self.m4_questions_g1
	@m4_questions_g1.setter
	def m4_questions_g1(self, value):
		self.m4_questions_g1 = value
	@property
	def m4_questions_g2(self):
		return self.m4_questions_g2
	@m4_questions_g2.setter
	def m4_questions_g2(self, value):
		self.m4_questions_g2 = value
	@property
	def m4_questions_g3(self):
		return self.m4_questions_g3
	@m4_questions_g3.setter
	def m4_questions_g3(self, value):
		self.m4_questions_g3 = value
	@property
	def m4_questions_g4(self):
		return self.m4_questions_g4
	@m4_questions_g4.setter
	def m4_questions_g4(self, value):
		self.m4_questions_g4 = value
	@property
	def order_quan_quest_m1(self):
		return self.order_quan_quest_m1
	@order_quan_quest_m1.setter
	def order_quan_quest_m1(self, value):
		self.order_quan_quest_m1 = value
	@property
	def order_quan_quest_m2(self):
		return self.order_quan_quest_m2
	@order_quan_quest_m2.setter
	def order_quan_quest_m2(self, value):
		self.order_quan_quest_m2 = value
	@property
	def order_quan_quest_m3(self):
		return self.order_quan_quest_m3
	@order_quan_quest_m3.setter
	def order_quan_quest_m3(self, value):
		self.order_quan_quest_m3 = value
	@property
	def order_quan_quest_m4(self):
		return self.order_quan_quest_m4
	@order_quan_quest_m4.setter
	def order_quan_quest_m4(self, value):
		self.order_quan_quest_m4 = value
	@property
	def order_diff_games_m1(self):
		return self.order_diff_games_m1
	@order_diff_games_m1.setter
	def order_diff_games_m1(self, value):
		self.order_diff_games_m1 = value
	@property
	def order_diff_games_m2(self):
		return self.order_diff_games_m2
	@order_diff_games_m2.setter
	def order_diff_games_m2(self, value):
		self.order_diff_games_m2 = value
	@property
	def order_diff_games_m3(self):
		return self.order_diff_games_m3
	@order_diff_games_m3.setter
	def order_diff_games_m3(self, value):
		self.order_diff_games_m3 = value
	@property
	def order_diff_games_m4(self):
		return self.order_diff_games_m4
	@order_diff_games_m4.setter
	def order_diff_games_m4(self, value):
		self.order_diff_games_m4 = value
	@property
	def m1_g1_order_diff_ques(self):
		return self.m1_g1_order_diff_ques
	@m1_g1_order_diff_ques.setter
	def m1_g1_order_diff_ques(self, value):
		self.m1_g1_order_diff_ques = value
	@property
	def m1_g2_order_diff_ques(self):
		return self.m1_g2_order_diff_ques
	@m1_g2_order_diff_ques.setter
	def m1_g2_order_diff_ques(self, value):
		self.m1_g2_order_diff_ques = value
	@property
	def m1_g3_order_diff_ques(self):
		return self.m1_g3_order_diff_ques
	@m1_g3_order_diff_ques.setter
	def m1_g3_order_diff_ques(self, value):
		self.m1_g3_order_diff_ques = value
	@property
	def m1_g4_order_diff_ques(self):
		return self.m1_g4_order_diff_ques
	@m1_g4_order_diff_ques.setter
	def m1_g4_order_diff_ques(self, value):
		self.m1_g4_order_diff_ques = value
	@property
	def m2_g1_order_diff_ques(self):
		return self.m2_g1_order_diff_ques
	@m2_g1_order_diff_ques.setter
	def m2_g1_order_diff_ques(self, value):
		self.m2_g1_order_diff_ques = value
	@property
	def m2_g2_order_diff_ques(self):
		return self.m2_g2_order_diff_ques
	@m2_g2_order_diff_ques.setter
	def m2_g2_order_diff_ques(self, value):
		self.m2_g2_order_diff_ques = value
	@property
	def m2_g3_order_diff_ques(self):
		return self.m2_g3_order_diff_ques
	@m2_g3_order_diff_ques.setter
	def m2_g3_order_diff_ques(self, value):
		self.m2_g3_order_diff_ques = value
	@property
	def m2_g4_order_diff_ques(self):
		return self.m2_g4_order_diff_ques
	@m2_g4_order_diff_ques.setter
	def m2_g4_order_diff_ques(self, value):
		self.m2_g4_order_diff_ques = value
	@property
	def m3_g1_order_diff_ques(self):
		return self.m3_g1_order_diff_ques
	@m3_g1_order_diff_ques.setter
	def m3_g1_order_diff_ques(self, value):
		self.m3_g1_order_diff_ques = value
	@property
	def m3_g2_order_diff_ques(self):
		return self.m3_g2_order_diff_ques
	@m3_g2_order_diff_ques.setter
	def m3_g2_order_diff_ques(self, value):
		self.m3_g2_order_diff_ques = value
	@property
	def m3_g3_order_diff_ques(self):
		return self.m3_g3_order_diff_ques
	@m3_g3_order_diff_ques.setter
	def m3_g3_order_diff_ques(self, value):
		self.m3_g3_order_diff_ques = value
	@property
	def m3_g4_order_diff_ques(self):
		return self.m3_g4_order_diff_ques
	@m3_g4_order_diff_ques.setter
	def m3_g4_order_diff_ques(self, value):
		self.m3_g4_order_diff_ques = value
	@property
	def m4_g1_order_diff_ques(self):
		return self.m4_g1_order_diff_ques
	@m4_g1_order_diff_ques.setter
	def m4_g1_order_diff_ques(self, value):
		self.m4_g1_order_diff_ques = value
	@property
	def m4_g2_order_diff_ques(self):
		return self.m4_g2_order_diff_ques
	@m4_g2_order_diff_ques.setter
	def m4_g2_order_diff_ques(self, value):
		self.m4_g2_order_diff_ques = value
	@property
	def m4_g3_order_diff_ques(self):
		return self.m4_g3_order_diff_ques
	@m4_g3_order_diff_ques.setter
	def m4_g3_order_diff_ques(self, value):
		self.m4_g3_order_diff_ques = value
	@property
	def m4_g4_order_diff_ques(self):
		return self.m4_g4_order_diff_ques
	@m4_g4_order_diff_ques.setter
	def m4_g4_order_diff_ques(self, value):
		self.m4_g4_order_diff_ques = value
	@property
	def type_m1_g1_1r(self):
		return self.type_m1_g1_1r
	@type_m1_g1_1r.setter
	def type_m1_g1_1r(self, value):
		self.type_m1_g1_1r = value
	@property
	def type_m1_g2_1r(self):
		return self.type_m1_g2_1r
	@type_m1_g2_1r.setter
	def type_m1_g2_1r(self, value):
		self.type_m1_g2_1r = value
	@property
	def type_m1_g3_1r(self):
		return self.type_m1_g3_1r
	@type_m1_g3_1r.setter
	def type_m1_g3_1r(self, value):
		self.type_m1_g3_1r = value
	@property
	def type_m1_g4_1r(self):
		return self.type_m1_g4_1r
	@type_m1_g4_1r.setter
	def type_m1_g4_1r(self, value):
		self.type_m1_g4_1r = value
	@property
	def type_m2_g1_1r(self):
		return self.type_m2_g1_1r
	@type_m2_g1_1r.setter
	def type_m2_g1_1r(self, value):
		self.type_m2_g1_1r = value
	@property
	def type_m2_g2_1r(self):
		return self.type_m2_g2_1r
	@type_m2_g2_1r.setter
	def type_m2_g2_1r(self, value):
		self.type_m2_g2_1r = value
	@property
	def type_m2_g3_1r(self):
		return self.type_m2_g3_1r
	@type_m2_g3_1r.setter
	def type_m2_g3_1r(self, value):
		self.type_m2_g3_1r = value
	@property
	def type_m2_g4_1r(self):
		return self.type_m2_g4_1r
	@type_m2_g4_1r.setter
	def type_m2_g4_1r(self, value):
		self.type_m2_g4_1r = value
	@property
	def type_m3_g1_1r(self):
		return self.type_m3_g1_1r
	@type_m3_g1_1r.setter
	def type_m3_g1_1r(self, value):
		self.type_m3_g1_1r = value
	@property
	def type_m3_g2_1r(self):
		return self.type_m3_g2_1r
	@type_m3_g2_1r.setter
	def type_m3_g2_1r(self, value):
		self.type_m3_g2_1r = value
	@property
	def type_m3_g3_1r(self):
		return self.type_m3_g3_1r
	@type_m3_g3_1r.setter
	def type_m3_g3_1r(self, value):
		self.type_m3_g3_1r = value
	@property
	def type_m3_g4_1r(self):
		return self.type_m3_g4_1r
	@type_m3_g4_1r.setter
	def type_m3_g4_1r(self, value):
		self.type_m3_g4_1r = value
	@property
	def type_m4_g1_1r(self):
		return self.type_m4_g1_1r
	@type_m4_g1_1r.setter
	def type_m4_g1_1r(self, value):
		self.type_m4_g1_1r = value
	@property
	def type_m4_g2_1r(self):
		return self.type_m4_g2_1r
	@type_m4_g2_1r.setter
	def type_m4_g2_1r(self, value):
		self.type_m4_g2_1r = value
	@property
	def type_m4_g3_1r(self):
		return self.type_m4_g3_1r
	@type_m4_g3_1r.setter
	def type_m4_g3_1r(self, value):
		self.type_m4_g3_1r = value
	@property
	def type_m4_g4_1r(self):
		return self.type_m4_g4_1r
	@type_m4_g4_1r.setter
	def type_m4_g4_1r(self, value):
		self.type_m4_g4_1r = value
	@property
	def advisable_time_m1(self):
		return self.advisable_time_m1
	@advisable_time_m1.setter
	def advisable_time_m1(self, value):
		self.advisable_time_m1 = value
	@property
	def advisable_time_m2(self):
		return self.advisable_time_m2
	@advisable_time_m2.setter
	def advisable_time_m2(self, value):
		self.advisable_time_m2 = value
	@property
	def advisable_time_m3(self):
		return self.advisable_time_m3
	@advisable_time_m3.setter
	def advisable_time_m3(self, value):
		self.advisable_time_m3 = value
	@property
	def advisable_time_m4(self):
		return self.advisable_time_m4
	@advisable_time_m4.setter
	def advisable_time_m4(self, value):
		self.advisable_time_m4 = value
	@property
	def necessary_time_g1_m1(self):
		return self.necessary_time_g1_m1
	@necessary_time_g1_m1.setter
	def necessary_time_g1_m1(self, value):
		self.necessary_time_g1_m1 = value
	@property
	def necessary_time_g1_m2(self):
		return self.necessary_time_g1_m2
	@necessary_time_g1_m2.setter
	def necessary_time_g1_m2(self, value):
		self.necessary_time_g1_m2 = value
	@property
	def necessary_time_g1_m3(self):
		return self.necessary_time_g1_m3
	@necessary_time_g1_m3.setter
	def necessary_time_g1_m3(self, value):
		self.necessary_time_g1_m3 = value
	@property
	def necessary_time_g1_m4(self):
		return self.necessary_time_g1_m4
	@necessary_time_g1_m4.setter
	def necessary_time_g1_m4(self, value):
		self.necessary_time_g1_m4 = value
	@property
	def necessary_time_g2_m1(self):
		return self.necessary_time_g2_m1
	@necessary_time_g2_m1.setter
	def necessary_time_g2_m1(self, value):
		self.necessary_time_g2_m1 = value
	@property
	def necessary_time_g2_m2(self):
		return self.necessary_time_g2_m2
	@necessary_time_g2_m2.setter
	def necessary_time_g2_m2(self, value):
		self.necessary_time_g2_m2 = value
	@property
	def necessary_time_g2_m3(self):
		return self.necessary_time_g2_m3
	@necessary_time_g2_m3.setter
	def necessary_time_g2_m3(self, value):
		self.necessary_time_g2_m3 = value
	@property
	def necessary_time_g2_m4(self):
		return self.necessary_time_g2_m4
	@necessary_time_g2_m4.setter
	def necessary_time_g2_m4(self, value):
		self.necessary_time_g2_m4 = value
	@property
	def necessary_time_g3_m1(self):
		return self.necessary_time_g3_m1
	@necessary_time_g3_m1.setter
	def necessary_time_g3_m1(self, value):
		self.necessary_time_g3_m1 = value
	@property
	def necessary_time_g3_m2(self):
		return self.necessary_time_g3_m2
	@necessary_time_g3_m2.setter
	def necessary_time_g3_m2(self, value):
		self.necessary_time_g3_m2 = value
	@property
	def necessary_time_g3_m3(self):
		return self.necessary_time_g3_m3
	@necessary_time_g3_m3.setter
	def necessary_time_g3_m3(self, value):
		self.necessary_time_g3_m3 = value
	@property
	def necessary_time_g3_m4(self):
		return self.necessary_time_g3_m4
	@necessary_time_g3_m4.setter
	def necessary_time_g3_m4(self, value):
		self.necessary_time_g3_m4 = value
	@property
	def necessary_time_g4_m1(self):
		return self.necessary_time_g4_m1
	@necessary_time_g4_m1.setter
	def necessary_time_g4_m1(self, value):
		self.necessary_time_g4_m1 = value
	@property
	def necessary_time_g4_m2(self):
		return self.necessary_time_g4_m2
	@necessary_time_g4_m2.setter
	def necessary_time_g4_m2(self, value):
		self.necessary_time_g4_m2 = value
	@property
	def necessary_time_g4_m3(self):
		return self.necessary_time_g4_m3
	@necessary_time_g4_m3.setter
	def necessary_time_g4_m3(self, value):
		self.necessary_time_g4_m3 = value
	@property
	def necessary_time_g4_m4(self):
		return self.necessary_time_g4_m4
	@necessary_time_g4_m4.setter
	def necessary_time_g4_m4(self, value):
		self.necessary_time_g4_m4 = value
	@property
	def a1_max_question_game(self):
		return self.a1_max_question_game
	@a1_max_question_game.setter
	def a1_max_question_game(self, value):
		self.a1_max_question_game = value
	@property
	def a2_max_game_match(self):
		return self.a2_max_game_match
	@a2_max_game_match.setter
	def a2_max_game_match(self, value):
		self.a2_max_game_match = value
	@property
	def a1_range_num_of_matches_with_games_order_of_difficulty(self):
		return self.a1_range_num_of_matches_with_games_order_of_difficulty
	@a1_range_num_of_matches_with_games_order_of_difficulty.setter
	def a1_range_num_of_matches_with_games_order_of_difficulty(self, value):
		self.a1_range_num_of_matches_with_games_order_of_difficulty = value
	@property
	def level_input(self):
		return self.level_input
	@level_input.setter
	def level_input(self, value):
		self.level_input = value
	@property
	def n1_focus_on_comprehension_input(self):
		return self.n1_focus_on_comprehension_input
	@n1_focus_on_comprehension_input.setter
	def n1_focus_on_comprehension_input(self, value):
		self.n1_focus_on_comprehension_input = value
	@property
	def n1_focus_on_concentration_input(self):
		return self.n1_focus_on_concentration_input
	@n1_focus_on_concentration_input.setter
	def n1_focus_on_concentration_input(self, value):
		self.n1_focus_on_concentration_input = value								
	@property
	def n1_focus_on_errors_input(self):
		return self.n1_focus_on_errors_input
	@n1_focus_on_errors_input.setter
	def n1_focus_on_errors_input(self, value):
		self.n1_focus_on_errors_input = value
	@property
	def n2_focus_on_automation_input(self):
		return self.n2_focus_on_automation_input
	@n2_focus_on_automation_input.setter
	def n2_focus_on_automation_input(self, value):
		self.n2_focus_on_automation_input = value
	@property
	def n2_focus_on_resistance_input(self):
		return self.n2_focus_on_resistance_input
	@n2_focus_on_resistance_input.setter
	def n2_focus_on_resistance_input(self, value):
		self.n2_focus_on_resistance_input = value
	@property
	def n4_focus_on_changes_input(self):
		return self.n4_focus_on_changes_input
	@n4_focus_on_changes_input.setter
	def n4_focus_on_changes_input(self, value):
		self.n4_focus_on_changes_input = value
	@property
	def s1_understanding_input(self):
		return self.s1_understanding_input
	@s1_understanding_input.setter
	def s1_understanding_input(self, value):
		self.s1_understanding_input = value								
	@property
	def s2_restrictions_of_interest_input(self):
		return self.s2_restrictions_of_interest_input
	@s2_restrictions_of_interest_input.setter
	def s2_restrictions_of_interest_input(self, value):
		self.s2_restrictions_of_interest_input = value
	@property
	def s3_distraction_in_listening_input(self):
		return self.s3_distraction_in_listening_input
	@s3_distraction_in_listening_input.setter
	def s3_distraction_in_listening_input(self, value):
		self.s3_distraction_in_listening_input = value
	@property
	def s3_executing_long_tasks_input(self):
		return self.s3_executing_long_tasks_input
	@s3_executing_long_tasks_input.setter
	def s3_executing_long_tasks_input(self, value):
		self.s3_executing_long_tasks_input = value
	@property
	def s3_meltdown_input(self):
		return self.s3_meltdown_input
	@s3_meltdown_input.setter
	def s3_meltdown_input(self, value):
		self.s3_meltdown_input = value
	@property
	def s3_memoria_deficit_input(self):
		return self.s3_memoria_deficit_input
	@s3_memoria_deficit_input.setter
	def s3_memoria_deficit_input(self, value):
		self.s3_memoria_deficit_input = value
	@property
	def s1_diversion_input(self):
		return self.s1_diversion_input
	@s1_diversion_input.setter
	def s1_diversion_input(self, value):
		self.s1_diversion_input = value
	@property
	def s2_endurance_input(self):
		return self.s2_endurance_input
	@s2_endurance_input.setter
	def s2_endurance_input(self, value):
		self.s2_endurance_input = value
	@property
	def a1_range_num_input(self):
		return self.a1_range_num_input
	@a1_range_num_input.setter
	def a1_range_num_input(self, value):
		self.a1_range_num_input = value														
	@property
	def a1_range_diff_matches_done_input(self):
		return self.a1_range_diff_matches_done_input
	@a1_range_diff_matches_done_input.setter
	def a1_range_diff_matches_done_input(self, value):
		self.a1_range_diff_matches_done_input = value
	@property
	def a2_match_session_input(self):
		return self.a2_match_session_input
	@a2_match_session_input.setter
	def a2_match_session_input(self, value):
		self.a2_match_session_input = value
	@property
	def a2_num_of_matches_with_games_quan_ques_input(self):
		return self.a2_num_of_matches_with_games_quan_ques_input
	@a2_num_of_matches_with_games_quan_ques_input.setter
	def a2_num_of_matches_with_games_quan_ques_input(self, value):
		self.a2_num_of_matches_with_games_quan_ques_input = value
	@property
	def a3_order_diff_match_input(self):
		return self.a3_order_diff_match_input
	@a3_order_diff_match_input.setter
	def a3_order_diff_match_input(self, value):
		self.a3_order_diff_match_input = value		
	@property
	def a3_num_of_session_with_matches_quan_games_input(self):
		return self.a3_num_of_session_with_matches_quan_games_input
	@a3_num_of_session_with_matches_quan_games_input.setter
	def a3_num_of_session_with_matches_quan_games_input(self, value):
		self.a3_num_of_session_with_matches_quan_games_input = value
	@property
	def variety_m1(self):
		return self.variety_m1
	@variety_m1.setter
	def variety_m1(self, value):
		self.variety_m1 = value
	@property
	def variety_m2(self):
		return self.variety_m2
	@variety_m2.setter
	def variety_m2(self, value):
		self.variety_m2 = value
	@property
	def variety_m3(self):
		return self.variety_m3
	@variety_m3.setter
	def variety_m3(self, value):
		self.variety_m3 = value
	@property
	def variety_m4(self):
		return self.variety_m4
	@variety_m4.setter
	def variety_m4(self, value):
		self.variety_m4 = value
	@property
	def criteria_m1(self):
		return self.criteria_m1
	@criteria_m1.setter
	def criteria_m1(self, value):
		self.criteria_m1 = value
	@property
	def criteria_m2(self):
		return self.criteria_m2
	@criteria_m2.setter
	def criteria_m2(self, value):
		self.criteria_m2 = value
	@property
	def criteria_m3(self):
		return self.criteria_m3
	@criteria_m3.setter
	def criteria_m3(self, value):
		self.criteria_m3 = value
	@property
	def criteria_m4(self):
		return self.criteria_m4
	@criteria_m4.setter
	def criteria_m4(self, value):
		self.criteria_m4 = value

	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	# Methods 
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------
	cdef take_csvs(self):
		print("entro in take_csvs!!!!")
		print("self kid id dovrebbe essere = ", self.kid_id)
		self.curs.execute("SELECT age FROM Kids WHERE kid_id = ?",(self.kid_id,))
		age_is = self.curs.fetchone()[0]
		print("RIFACCIO !!!")
		child = 10001
		q1 = "SELECT age FROM Kids WHERE kid_id = ?"
		q2 = "SELECT age FROM Kids WHERE kid_id = 10001"
		#kidda = (self.kid_id)
		patio = self.execute_param_query(q1,self.kid_id)
		patio1 = self.execute_a_query(q2)
		#patio22 = self.execute_param_query(q1,kidda)
		print("age_is age_is ", age_is)
		print("patio  ", patio)
		print("patio1  ", patio1)
		if age_is==(5,6):
			with open('/home/pi/OIMI/oimi_code/src/learning/game_classification/All_paths/complete_56_csv_paths.txt') as f:
				self.csv_lines = f.read().splitlines()
		elif age_is in (7,8):
			with open('/home/pi/OIMI/oimi_code/src/learning/game_classification/All_paths/complete_78_csv_paths.txt') as f:
				self.csv_lines = f.read().splitlines()
		elif age_is in (9,10):
			with open('/home/pi/OIMI/oimi_code/src/learning/game_classification/All_paths/complete_910_csv_paths.txt') as f:
				self.csv_lines = f.read().splitlines()
		print("esco da take_csvs")
	
	cpdef add_new_Session_table_with_kid(self, int kid_id, int mandatory_imposition, int mandatory_need):
		print("sono in add_new_Session_table_with_kid")
		self.num_of_matches = 0
		#self.mandatory_impositions, self.mandatory_needs = mandatory_imposition, mandatory_need
		self.movements_allowed, self.extra_time_tolerance, self.body_enabled, self.exhortations, self.audio_slow = 0,0,0,0,0
		print("self.movements_allowed ", self.movements_allowed)
		print("extra_time_tolerance ", self.extra_time_tolerance)
		print("self.body_enabled ", self.body_enabled)
		print("self.exhortations ", self.exhortations)
		print("self.audio_slow ", self.audio_slow)
		self.new_deleletion_index, self.repeat_intro, self.rules_explication, self.repeat_question = 0,0,0,0
		print("self.new_deleletion_index " , self.new_deleletion_index)
		print("self.repeat_intro ", self.repeat_question)
		self.disposable = 0
		self.desirable_time = 320
		self.stress_flag = 0
		self.led_brightness = 'std'
		print("bellazio0")
		self.complexity = ''
		print("complesso")
		print("status")
		self.matches_order_diff = ''
		self.order_quan_games = ''
		self.difficulty_mat_1 = ''
		print("bellazio1")
		self.difficulty_mat_2 = ''
		self.difficulty_mat_3 = ''
		self.difficulty_mat_4 = ''
		self.diff_m1_g1 = ''
		self.diff_m1_g2 = ''
		self.diff_m1_g3 = ''
		self.diff_m1_g4 = ''
		self.diff_m2_g1 = ''
		self.diff_m2_g2 = ''
		self.diff_m2_g3 = ''
		self.diff_m2_g4 = ''
		self.diff_m3_g1 = ''
		self.diff_m3_g2 = ''
		self.diff_m3_g3 = ''
		self.diff_m3_g4 = ''		
		self.diff_m4_g1 = ''
		self.diff_m4_g2 = ''
		self.diff_m4_g3 = ''
		self.diff_m4_g4 = ''
		self.kind_m1_g1 = ''
		self.kind_m1_g2 = ''
		self.kind_m1_g3 = ''
		self.kind_m1_g4 = ''
		self.kind_m2_g1 = ''
		self.kind_m2_g2 = ''
		self.kind_m2_g3 = ''
		self.kind_m2_g4 = ''
		self.kind_m3_g1 = ''
		self.kind_m3_g2 = ''
		self.kind_m3_g3 = ''
		self.kind_m3_g4 = ''
		self.kind_m4_g1 = ''
		self.kind_m4_g2 = ''
		self.kind_m4_g3 = ''
		self.kind_m4_g4 = ''
		print("bellazio2")
		self.m1_questions_g1 = 0
		self.m1_questions_g2 = 0
		self.m1_questions_g3 = 0
		self.m1_questions_g4 = 0
		self.m2_questions_g1 = 0
		self.m2_questions_g2 = 0
		self.m2_questions_g3 = 0
		self.m2_questions_g4 = 0
		self.m3_questions_g1 = 0
		self.m3_questions_g2 = 0
		self.m3_questions_g3 = 0
		self.m3_questions_g4 = 0
		self.m4_questions_g1 = 0
		self.m4_questions_g2 = 0
		self.m4_questions_g3 = 0
		self.m4_questions_g4 = 0
		self.order_quan_quest_m1 = ''
		self.order_quan_quest_m2 = ''
		self.order_quan_quest_m3 = ''
		self.order_quan_quest_m4 = ''
		self.order_diff_games_m1 = ''
		self.order_diff_games_m2 = ''
		self.order_diff_games_m3 = ''
		self.order_diff_games_m4 = ''
		self.status = ''
		print("come faccio?")
		self.m1_g1_order_diff_ques = ''
		self.m1_g2_order_diff_ques = ''
		self.m1_g3_order_diff_ques = ''
		self.m1_g4_order_diff_ques = ''
		self.m2_g1_order_diff_ques = ''
		self.m2_g2_order_diff_ques = ''
		self.m2_g3_order_diff_ques = ''
		self.m2_g4_order_diff_ques = ''
		self.m3_g1_order_diff_ques = ''
		self.m3_g2_order_diff_ques = ''
		self.m3_g3_order_diff_ques = ''
		self.m3_g4_order_diff_ques = ''
		self.m4_g1_order_diff_ques = ''
		self.m4_g2_order_diff_ques = ''
		self.m4_g3_order_diff_ques = ''
		self.m4_g4_order_diff_ques = ''
		self.type_m1_g1_1r = ''
		self.type_m1_g1_2r = ''
		self.type_m1_g1_3r = ''
		self.type_m1_g1_4r = ''
		self.type_m1_g2_1r = ''
		self.type_m1_g2_2r = ''
		self.type_m1_g2_3r = ''
		self.type_m1_g2_4r = ''
		self.type_m1_g3_1r = ''
		self.type_m1_g3_2r = ''
		self.type_m1_g3_3r = ''
		self.type_m1_g3_4r = ''
		self.type_m1_g4_1r = ''
		self.type_m1_g4_2r = ''
		self.type_m1_g4_3r = ''
		self.type_m1_g4_4r = ''
		self.type_m2_g1_1r = ''
		self.type_m2_g1_2r = ''
		self.type_m2_g1_3r = ''
		self.type_m2_g1_4r = ''
		self.type_m2_g2_1r = ''
		self.type_m2_g2_2r = ''
		self.type_m2_g2_3r = ''
		self.type_m2_g2_4r = ''
		self.type_m2_g3_1r = ''
		self.type_m2_g3_2r = ''
		self.type_m2_g3_3r = ''
		self.type_m2_g3_4r = ''
		self.type_m2_g4_1r = ''
		self.type_m2_g4_2r = ''
		self.type_m2_g4_3r = ''
		self.type_m2_g4_4r = ''
		self.type_m3_g1_1r = ''
		self.type_m3_g1_2r = ''
		self.type_m3_g1_3r = ''
		self.type_m3_g1_4r = ''
		self.type_m3_g2_1r = ''
		self.type_m3_g2_2r = ''
		self.type_m3_g2_3r = ''
		self.type_m3_g2_4r = ''
		self.type_m3_g3_1r = ''
		self.type_m3_g3_2r = ''
		self.type_m3_g3_3r = ''
		self.type_m3_g3_4r = ''
		self.type_m3_g4_1r = ''
		self.type_m3_g4_2r = ''
		self.type_m3_g4_3r = ''
		self.type_m3_g4_4r = ''
		self.type_m4_g1_1r = ''
		self.type_m4_g1_2r = ''
		self.type_m4_g1_3r = ''
		self.type_m4_g1_4r = ''
		self.type_m4_g2_1r = ''
		self.type_m4_g2_2r = ''
		self.type_m4_g2_3r = ''
		self.type_m4_g2_4r = ''
		self.type_m4_g3_1r = ''
		self.type_m4_g3_2r = ''
		self.type_m4_g3_3r = ''
		self.type_m4_g3_4r = ''
		self.type_m4_g4_1r = ''
		self.type_m4_g4_2r = ''
		self.type_m4_g4_3r = ''
		self.type_m4_g4_4r = ''
		self.imposed_ques_id = 99999
		self.ask_never_asked_q = 0
		print("bellazio3")
		print("PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO self.kid_id ", self.kid_id)
		print("PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO self.kid_id ", self.kid_id)
		print("PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO self.kid_id ", self.kid_id)
		print("PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO self.kid_id ", self.kid_id)
		print("PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO PATIO self.kid_id ", self.kid_id)
		#exist_already = 0
		self.curs.execute("SELECT session_id FROM Kids_Sessions WHERE kid_id = ?",(self.kid_id,))
		exist_already = self.curs.fetchone()
		print("exist_already  is ", exist_already)
		exist_already = 0
		if exist_already:
			exist_already = exist_already[0]
			print("exist_already {}".format(exist_already))
		else:
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
			print("exist_already non trovatooooooooo")
		if exist_already is None:
			print("exist_already = none")
			self.curs.execute("SELECT age FROM Kids WHERE kid_id = ?",(self.kid_id,))
			kid_age = self.curs.fetchone()
			if kid_age:
				kid_age = kid_age[0]
				if kid_age<=6:
					self.take_one_session_56(self.kid_id)
				elif 6<kid_age<=8:
					self.take_one_session_78(self.kid_id)
				elif kid_age>8:
					self.take_one_session_910(self.kid_id)
			else:
				print("Error. kid_age parameter not found. Are you sure the kid already exists?")
		else:
			print("THE END!!!!!")
			
			tot_nec_features = []
			tot_nec_features, self.kids_syms, self.kids_needs= adp2.extract_necessary_features_for_prediction(self, self.kid_id)

			self.s1_understanding_input = tot_nec_features[0]
			self.s1_diversion_input = tot_nec_features[1]
			self.s2_endurance_input = tot_nec_features[2]
			self.s2_restrictions_of_interest_input = tot_nec_features[3]
			self.s3_meltdown_input = tot_nec_features[4]
			self.s3_memoria_deficit_input = tot_nec_features[5]
			self.s3_distraction_in_listening_input = tot_nec_features[6]
			self.s3_executing_long_tasks_input = tot_nec_features[7]
			self.a1_max_question_game = tot_nec_features[8]
			self.a1_range_num_input = tot_nec_features[9]
			self.a1_range_diff_matches_done_input = tot_nec_features[10]
			self.a1_range_num_of_matches_with_games_order_of_difficulty = tot_nec_features[11]
			self.a2_max_game_match = tot_nec_features[12]
			self.a2_match_session_input = tot_nec_features[13]
			self.a2_num_of_matches_with_games_quan_ques_input = tot_nec_features[14]
			self.a3_order_diff_match_input = tot_nec_features[15]
			self.a3_num_of_session_with_matches_quan_games_input = tot_nec_features[16]
			self.n1_focus_on_errors_input = tot_nec_features[17]
			self.n1_focus_on_concentration_input = tot_nec_features[18]
			self.n1_focus_on_comprehension_input = tot_nec_features[19]
			self.n2_focus_on_automation_input = tot_nec_features[20]
			self.n2_focus_on_resistance_input = tot_nec_features[21]
			self.n4_focus_on_changes_input = tot_nec_features[22]
			self.level_input = tot_nec_features[23]

			#predic 1 table
			###########	for table_1
			############################################## fill 
			dict_table_1 = {'col1': [self.level_input],
				 'col2': [self.s1_diversion_input],
				 'col3': [self.s2_endurance_input],
				 'col4': [self.s3_meltdown_input],
				 'col5': [self.a1_range_num_input],
				 'col6': [self.a3_order_diff_match_input],
				 'col7': [self.a2_match_session_input]}

			fores1 = cswp.RanForest_clf_table_1(name="predict_trained_dataset#1", received_dataset = self.csv_lines[0], num_outputs_prev = 4, weights_flag=0) 
			fores1.prepare_array(dict_table_1)
			self.outputs_table_1 = fores1.predict_instance()
			print(self.outputs_table_1)

			self.complexity = self.outputs_table_1[0][0]
			self.matches_order_diff = self.outputs_table_1[0][2]
			dict_table_2 = {
				'col1': [self.level_input],
				'col2': [self.complexity],
				'col3': [self.matches_order_diff],
				'col4': [self.s1_understanding_input],
				'col5': [self.a1_range_diff_matches_done_input],
				'col6': [self.a2_num_of_matches_with_games_quan_ques_input],
				'col7': [self.a3_num_of_session_with_matches_quan_games_input],
				'col8': [self.n1_focus_on_concentration_input]}

			self.num_of_matches = int(self.outputs_table_1[0][1])
			if self.num_of_matches==1:
				fores2 = cswp.RanForest_clf_table_2(name='predict_trained_dataset#t2m1', received_dataset = self.csv_lines[1], num_outputs_prev = 1, weights_flag=0) 
				fores2.prepare_array(dict_table_2)
				self.outputs_table_2 = fores2.predict_instance()
				
				self.difficulty_mat_1 = self.outputs_table_2[0][0]
				self.order_quan_games = self.outputs_table_2[0][1]
				dict_table_31 = {
					'col1': [self.level_input],
					'col2': [self.complexity],
					'col3': [self.s2_restrictions_of_interest_input], #s1
					'col4': [self.s3_executing_long_tasks_input], #s2
					'col5': [self.s1_understanding_input], #s3
					'col6': [self.difficulty_mat_1],
					'col7': [self.a1_range_num_of_matches_with_games_order_of_difficulty],
					'col8': [self.n1_focus_on_concentration_input],
					'col9': [self.n2_focus_on_resistance_input],
					'col10': [self.a2_max_game_match]
					}

				fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m1", received_dataset = self.csv_lines[5], num_outputs_prev = 1, weights_flag=0)
				fores3.prepare_array(dict_table_31)
				self.outputs_table_3 = fores3.predict_instance()
				
				self.order_quan_quest_m1 = self.outputs_table_3[0][0]
				self.order_diff_games_m1 = self.outputs_table_3[0][1]
				dict_table_41 = {
				'col1': [self.level_input], 
				'col2': [self.difficulty_mat_1],
				'col3': [self.s3_meltdown_input],
				'col4': [self.s1_understanding_input], 
				'col5': [self.s3_distraction_in_listening_input],
				'col6': [self.order_quan_quest_m1], 
				'col7': [self.order_diff_games_m1], 
				'col8': [self.n1_focus_on_errors_input], 
				'col9': [self.n2_focus_on_automation_input], 
				'col10': [self.n1_focus_on_concentration_input], 
				'col11': [self.n4_focus_on_changes_input],
				'col12': [self.a1_max_question_game]}
				games_m1 = int(self.outputs_table_3[0][2])
				if games_m1==1:
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g1", received_dataset = self.csv_lines[170], num_outputs_prev = 1, weights_flag=0)
					fores4.prepare_array(dict_table_41)
					self.outputs_table_4_m1 = fores4.predict_instance()

					self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
					##########################################################################################################
					self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
					##########################################################################################################
					dict_table_511 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques], 
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					if self.m1_questions_g1:
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					elif self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					elif self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]					
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]					
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]					
					elif self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]
				if games_m1==2:
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g2", received_dataset = self.csv_lines[171], num_outputs_prev = 2, weights_flag=0)
					fores4.prepare_array(dict_table_41)
					self.outputs_table_4_m1 = fores4.predict_instance()

					self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
					self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
					self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
					self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
					##########################################################################################################
					self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
					self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
					##########################################################################################################
					dict_table_521 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques], 
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}		
					dict_table_522 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g2], 
						'col3': [self.m1_g2_order_diff_ques],
						'col4': [self.kind_m1_g2], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					elif self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					elif self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					elif self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]						
					fores5.prepare_array(dict_table_521)
					self.outputs_table_5_m1_g1 = fores5.predict_instance()

					if self.m1_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					elif self.m1_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					elif self.m1_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					elif self.m1_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
						self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]
				if games_m1==3:
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g3", received_dataset = self.csv_lines[172], num_outputs_prev = 3, weights_flag=0)
					fores4.prepare_array(dict_table_41)
					self.outputs_table_4_m1 = fores4.predict_instance()

					self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
					self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
					self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
					self.m1_g3_order_diff_ques = self.outputs_table_4_m1[0][11]
					self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
					self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
					self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
					self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])
					##########################################################################################################
					self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
					self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
					self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
					##########################################################################################################
					dict_table_531 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques], 
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}		
					dict_table_532 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g2], 
						'col3': [self.m1_g2_order_diff_ques], 
						'col4': [self.kind_m1_g2], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_533 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g3], 
						'col3': [self.m1_g3_order_diff_ques], 
						'col4': [self.kind_m1_g3], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					elif self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					elif self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					elif self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]							
					if self.m1_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					elif self.m1_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					elif self.m1_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					elif self.m1_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
						self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]							
					if self.m1_questions_g3==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					elif self.m1_questions_g3==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
					elif self.m1_questions_g3==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
					elif self.m1_questions_g3==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
						self.type_m1_g3_4r = self.outputs_table_5_m1_g3[0][3]					
				if games_m1==4:
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g4", received_dataset = self.csv_lines[173], num_outputs_prev = 4, weights_flag=0)
					fores4.prepare_array(dict_table_41)
					self.outputs_table_4_m1 = fores4.predict_instance()

					self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
					self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
					self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
					self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
					self.m1_g3_order_diff_ques = self.outputs_table_4_m1[0][11]
					self.m1_g4_order_diff_ques = self.outputs_table_4_m1[0][15]
					self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
					self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
					self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
					self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
					self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])					
					self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])					
					##########################################################################################################
					self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
					self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
					self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
					self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
					##########################################################################################################
					dict_table_541 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques], 
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}		
					dict_table_542 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g2], 
						'col3': [self.m1_g2_order_diff_ques], 
						'col4': [self.kind_m1_g2], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_543 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g3], 
						'col3': [self.m1_g3_order_diff_ques], 
						'col4': [self.kind_m1_g3], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_544 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g4], 
						'col3': [self.m1_g4_order_diff_ques], 
						'col4': [self.kind_m1_g4], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					elif self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					elif self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					elif self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]						
					if self.m1_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					elif self.m1_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					elif self.m1_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					elif self.m1_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
						self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]						
					if self.m1_questions_g3==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					elif self.m1_questions_g3==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
					elif self.m1_questions_g3==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
					elif self.m1_questions_g3==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
						self.type_m1_g3_4r = self.outputs_table_5_m1_g3[0][3]						
					if int(self.outputs_table_4_m1[0][14])==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g4q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
					elif int(self.outputs_table_4_m1[0][14])==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g4q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
						self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
					elif int(self.outputs_table_4_m1[0][14])==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g4q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
						self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
						self.type_m1_g4_3r = self.outputs_table_5_m1_g4[0][2]
					elif int(self.outputs_table_4_m1[0][14])==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g4q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
						self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
						self.type_m1_g4_3r = self.outputs_table_5_m1_g4[0][2]
						self.type_m1_g4_4r = self.outputs_table_5_m1_g4[0][3]						
					
					apsh.check_security_before_creation(self)
					adp3.conclude_final_session_creation(self)
			
			###########################################################################################################################################################################2
			elif self.num_of_matches==2:
				fores2 = cswp.RanForest_clf_table_2(name="predict_trained_dataset#t2m2", received_dataset = self.csv_lines[2], num_outputs_prev = 2, weights_flag=0) 
				fores2.prepare_array(dict_table_2)
				self.outputs_table_2 = fores2.predict_instance()

				self.difficulty_mat_1 = self.outputs_table_2[0][0]
				self.difficulty_mat_2 = self.outputs_table_2[0][1]
				self.order_quan_games = self.outputs_table_2[0][2]
				dict_table_32 = {
					'col1': [self.level_input],
					'col2': [self.complexity], 
					'col3': [self.s2_restrictions_of_interest_input], #s1
					'col4': [self.s3_executing_long_tasks_input], #s2
					'col5': [self.s1_understanding_input], #s3
					'col6': [self.difficulty_mat_1], 
					'col7': [self.difficulty_mat_2], 
					'col8': [self.order_quan_games],
					'col9': [self.a1_range_num_of_matches_with_games_order_of_difficulty],
					'col10': [self.n1_focus_on_concentration_input],
					'col11': [self.n2_focus_on_resistance_input],
					'col12': [self.a2_max_game_match]}
				
				if self.complexity=='Easy':
					fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m2e", received_dataset = self.csv_lines[6], num_outputs_prev = 2, weights_flag=0)
				if self.complexity=='Normal':
					fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m2n", received_dataset = self.csv_lines[7], num_outputs_prev = 2, weights_flag=0)
				if self.complexity=='Medium':
					fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m2m", received_dataset = self.csv_lines[8], num_outputs_prev = 2, weights_flag=0)
				if self.complexity=='Hard':
					fores3 = cswp.RanForest_clf_table_3(name="predict_trained_dataset#t3m2h", received_dataset = self.csv_lines[9], num_outputs_prev = 2, weights_flag=0)

				fores3.prepare_array(dict_table_32)
				self.outputs_table_3 = fores3.predict_instance()

				self.order_quan_quest_m1 = self.outputs_table_3[0][0]
				self.order_diff_games_m1 = self.outputs_table_3[0][1]
				self.order_quan_quest_m2 = self.outputs_table_3[0][3]
				self.order_diff_games_m2 = self.outputs_table_3[0][4]

				dict_table_421 = {
				'col1': [self.level_input], 
				'col2': [self.difficulty_mat_1], 
				'col3': [self.s3_meltdown_input], 
				'col4': [self.s1_understanding_input], 
				'col5': [self.s3_distraction_in_listening_input],
				'col6': [self.order_quan_quest_m1], 
				'col7': [self.order_diff_games_m1], 
				'col8': [self.n1_focus_on_errors_input], 
				'col9': [self.n2_focus_on_automation_input], 
				'col10': [self.n1_focus_on_concentration_input], 
				'col11': [self.n4_focus_on_changes_input],
				'col12': [self.a1_max_question_game]}

				dict_table_422 = {
				'col1': [self.level_input], #level
				'col2': [self.difficulty_mat_2], 
				'col3': [self.s3_meltdown_input], 
				'col4': [self.s1_understanding_input], 
				'col5': [self.s3_distraction_in_listening_input],
				'col6': [self.order_quan_quest_m2], 
				'col7': [self.order_diff_games_m2], 
				'col8': [self.n1_focus_on_errors_input], 
				'col9': [self.n2_focus_on_automation_input], 
				'col10': [self.n1_focus_on_concentration_input], 
				'col11': [self.n4_focus_on_changes_input],
				'col12': [self.a1_max_question_game]}

				games_m1 = int(self.outputs_table_3[0][2])
				games_m2 = int(self.outputs_table_3[0][5])
				if games_m1==1: 
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g1", received_dataset = self.csv_lines[170], num_outputs_prev = 1, weights_flag=0)
					fores4.prepare_array(dict_table_421)
					self.outputs_table_4_m1 = fores4.predict_instance()
				elif games_m1==2: 
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g2", received_dataset = self.csv_lines[171], num_outputs_prev = 2, weights_flag=0)
					fores4.prepare_array(dict_table_421)
					self.outputs_table_4_m1 = fores4.predict_instance()
				elif games_m1==3: 
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g3", received_dataset = self.csv_lines[172], num_outputs_prev = 3, weights_flag=0)
					fores4.prepare_array(dict_table_421)
					self.outputs_table_4_m1 = fores4.predict_instance()
				elif games_m1==4: 
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m1g4", received_dataset = self.csv_lines[173], num_outputs_prev = 4, weights_flag=0)
					fores4.prepare_array(dict_table_421)
					self.outputs_table_4_m1 = fores4.predict_instance()
				if games_m2==1: 
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g1", received_dataset = self.csv_lines[170], num_outputs_prev = 1, weights_flag=0)
					fores4.prepare_array(dict_table_422)
					self.outputs_table_4_m2 = fores4.predict_instance()
				elif games_m2==2: 
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g2", received_dataset = self.csv_lines[171], num_outputs_prev = 2, weights_flag=0)
					fores4.prepare_array(dict_table_422)
					self.outputs_table_4_m2 = fores4.predict_instance()
				elif games_m2==3: 
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g3", received_dataset = self.csv_lines[172], num_outputs_prev = 3, weights_flag=0)
					fores4.prepare_array(dict_table_422)
					self.outputs_table_4_m2 = fores4.predict_instance()
				elif games_m2==4: 
					fores4 = cswp.RanForest_clf_table_4(name="predict_trained_dataset#t4m2g4", received_dataset = self.csv_lines[173], num_outputs_prev = 4, weights_flag=0)
					fores4.prepare_array(dict_table_422)
					self.outputs_table_4_m2 = fores4.predict_instance()
				################################
				if games_m1==1:
					self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
					self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					##########################################################################################################
					self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
					##########################################################################################################
				if games_m1==2:
					self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
					self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
					self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
					self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
					##########################################################################################################
					self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
					self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
					##########################################################################################################
				if games_m1==3:
					self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
					self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
					self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
					self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
					self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
					self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
					self.m1_g3_order_diff_ques = self.outputs_table_4_m1[0][11]
					##########################################################################################################
					self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
					self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
					self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
					##########################################################################################################
				if games_m1==4:
					self.kind_m1_g1 = self.outputs_table_4_m1[0][1]
					self.kind_m1_g2 = self.outputs_table_4_m1[0][5]
					self.kind_m1_g3 = self.outputs_table_4_m1[0][9]
					self.kind_m1_g4 = self.outputs_table_4_m1[0][13]
					self.diff_m1_g1 = self.outputs_table_4_m1[0][0]
					self.diff_m1_g2 = self.outputs_table_4_m1[0][4]
					self.diff_m1_g3 = self.outputs_table_4_m1[0][8]
					self.diff_m1_g4 = self.outputs_table_4_m1[0][12]
					self.m1_g1_order_diff_ques = self.outputs_table_4_m1[0][3]
					self.m1_g2_order_diff_ques = self.outputs_table_4_m1[0][7]
					self.m1_g3_order_diff_ques = self.outputs_table_4_m1[0][11]
					self.m1_g4_order_diff_ques = self.outputs_table_4_m1[0][15]
					##########################################################################################################
					self.kind_m1_g1 = sp.last_minute_security_change_kind(self.kind_m1_g1, self.diff_m1_g1, self.level_input) 
					self.kind_m1_g2 = sp.last_minute_security_change_kind(self.kind_m1_g2, self.diff_m1_g2, self.level_input) 
					self.kind_m1_g3 = sp.last_minute_security_change_kind(self.kind_m1_g3, self.diff_m1_g3, self.level_input) 
					self.kind_m1_g4 = sp.last_minute_security_change_kind(self.kind_m1_g4, self.diff_m1_g4, self.level_input) 
					##########################################################################################################
				if games_m2==1:
					self.kind_m2_g1 = self.outputs_table_4_m2[0][1]
					self.diff_m2_g1 = self.outputs_table_4_m2[0][0]
					self.m2_g1_order_diff_ques = self.outputs_table_4_m2[0][3]				
					##########################################################################################################
					self.kind_m2_g1 = sp.last_minute_security_change_kind(self.kind_m2_g1, self.diff_m2_g1, self.level_input) 
					##########################################################################################################
				if games_m2==2:
					self.kind_m2_g1 = self.outputs_table_4_m2[0][1]
					self.kind_m2_g2 = self.outputs_table_4_m2[0][5]
					self.diff_m2_g1 = self.outputs_table_4_m2[0][0]
					self.diff_m2_g2 = self.outputs_table_4_m2[0][4]					   
					self.m2_g1_order_diff_ques = self.outputs_table_4_m2[0][3]
					self.m2_g2_order_diff_ques = self.outputs_table_4_m2[0][7]
					##########################################################################################################
					self.kind_m2_g1 = sp.last_minute_security_change_kind(self.kind_m2_g1, self.diff_m2_g1, self.level_input) 
					self.kind_m2_g2 = sp.last_minute_security_change_kind(self.kind_m2_g2, self.diff_m2_g2, self.level_input) 
					##########################################################################################################
				if games_m2==3:
					self.kind_m2_g1 = self.outputs_table_4_m2[0][1]
					self.kind_m2_g2 = self.outputs_table_4_m2[0][5]
					self.kind_m2_g3 = self.outputs_table_4_m2[0][9]
					self.diff_m2_g1 = self.outputs_table_4_m2[0][0]
					self.diff_m2_g2 = self.outputs_table_4_m2[0][4]
					self.diff_m2_g3 = self.outputs_table_4_m2[0][8]
					self.m2_g1_order_diff_ques = self.outputs_table_4_m2[0][3]
					self.m2_g2_order_diff_ques = self.outputs_table_4_m2[0][7]
					self.m2_g3_order_diff_ques = self.outputs_table_4_m2[0][11]								  
					##########################################################################################################
					self.kind_m2_g1 = sp.last_minute_security_change_kind(self.kind_m2_g1, self.diff_m1_g1, self.level_input) 
					self.kind_m2_g2 = sp.last_minute_security_change_kind(self.kind_m2_g2, self.diff_m1_g2, self.level_input) 
					self.kind_m2_g3 = sp.last_minute_security_change_kind(self.kind_m2_g3, self.diff_m1_g3, self.level_input) 
					##########################################################################################################
				if games_m2==4:
					self.kind_m2_g1 = self.outputs_table_4_m2[0][1]
					self.kind_m2_g2 = self.outputs_table_4_m2[0][5]
					self.kind_m2_g3 = self.outputs_table_4_m2[0][9]
					self.kind_m2_g4 = self.outputs_table_4_m2[0][13]
					self.diff_m2_g1 = self.outputs_table_4_m2[0][0]
					self.diff_m2_g2 = self.outputs_table_4_m2[0][4]
					self.diff_m2_g3 = self.outputs_table_4_m2[0][8]
					self.diff_m2_g4 = self.outputs_table_4_m2[0][12]	
					self.m2_g1_order_diff_ques = self.outputs_table_4_m2[0][3]
					self.m2_g2_order_diff_ques = self.outputs_table_4_m2[0][7]
					self.m2_g3_order_diff_ques = self.outputs_table_4_m2[0][11]
					self.m2_g4_order_diff_ques = self.outputs_table_4_m2[0][15]
					##########################################################################################################
					self.kind_m2_g1 = sp.last_minute_security_change_kind(self.kind_m2_g1, self.diff_m2_g1, self.level_input) 
					self.kind_m2_g2 = sp.last_minute_security_change_kind(self.kind_m2_g2, self.diff_m2_g2, self.level_input) 
					self.kind_m2_g3 = sp.last_minute_security_change_kind(self.kind_m2_g3, self.diff_m2_g3, self.level_input) 
					self.kind_m2_g4 = sp.last_minute_security_change_kind(self.kind_m2_g4, self.diff_m2_g4, self.level_input) 
					##########################################################################################################
				################################
				if games_m1==1:
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					dict_table_511 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques], 
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					elif self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					elif self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					elif self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]	
				if games_m2==1: 
					self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
					dict_table_511 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m2_g1], 
						'col3': [self.order_diff_games_m1], 
						'col4': [self.kind_m2_g1],
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					if self.m2_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					elif self.m2_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					elif self.m2_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
						self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
					elif self.m2_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_511)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
						self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
						self.type_m2_g1_4r = self.outputs_table_5_m2_g1[0][3]
				if games_m1==2:
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6])
					dict_table_521 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques], 
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_522 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g2], 
						'col3': [self.m1_g2_order_diff_ques], 
						'col4': [self.kind_m1_g2], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					elif self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					elif self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					elif self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]					

					if self.m1_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					elif self.m1_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					elif self.m1_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					elif self.m1_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
						self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]						
				if games_m2==2:
					self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
					self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6])					
					dict_table_521 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m2_g1], 
						'col3': [self.m2_g1_order_diff_ques], 
						'col4': [self.kind_m2_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_522 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m2_g1], 
						'col3': [self.m2_g2_order_diff_ques], 
						'col4': [self.kind_m2_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}	

					if self.m2_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					elif self.m2_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					elif self.m2_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
						self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
					elif self.m2_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_521)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
						self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
						self.type_m2_g1_4r = self.outputs_table_5_m2_g1[0][3]						
					
					if self.m2_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					elif self.m2_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
						self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
					elif self.m2_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
						self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
						self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
					elif self.m2_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_522)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
						self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
						self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
						self.type_m2_g2_4r = self.outputs_table_5_m2_g2[0][3]						
				
				if games_m1==3:
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6]) 
					self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])					
					dict_table_531 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques], 
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_532 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g2], 
						'col3': [self.m1_g2_order_diff_ques], 
						'col4': [self.kind_m1_g2], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}
					dict_table_533 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g3], 
						'col3': [self.m1_g3_order_diff_ques], 
						'col4': [self.kind_m1_g3], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					elif self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					elif self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					elif self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]						
					
					if self.m1_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					elif self.m1_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					elif self.m1_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					elif self.m1_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
						self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]						
					
					if self.m1_questions_g3==1:
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					elif self.m1_questions_g3==2:
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
					elif self.m1_questions_g3==3:
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
					elif self.m1_questions_g3==4:
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
						self.type_m1_g3_4r = self.outputs_table_5_m1_g3[0][3]						
								
				if games_m2==3:
					self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
					self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6]) 
					self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10]) 
					dict_table_531 = {
						'col1': [self.level_input], 
						'col2': [self.outputs_table_4_m2[0][0]], 
						'col3': [self.outputs_table_4_m2[0][3]], 
						'col4': [self.outputs_table_4_m2[0][1]], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_532 = {
						'col1': [self.level_input], 
						'col2': [self.outputs_table_4_m2[0][4]], 
						'col3': [self.outputs_table_4_m2[0][7]], 
						'col4': [self.outputs_table_4_m2[0][5]], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}
					dict_table_533 = {
						'col1': [self.level_input], 
						'col2': [self.outputs_table_4_m2[0][8]],  
						'col3': [self.outputs_table_4_m2[0][11]], 
						'col4': [self.outputs_table_4_m2[0][9]],  
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}
					if self.m2_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					elif self.m2_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					elif self.m2_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
						self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
					elif self.m2_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_531)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
						self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
						self.type_m2_g1_4r = self.outputs_table_5_m2_g1[0][3]					

					if self.m2_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					elif self.m2_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m2_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m2_g2[0][1]
					elif self.m2_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
						self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
						self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
					elif self.m2_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_532)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
						self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
						self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
						self.type_m2_g2_4r = self.outputs_table_5_m2_g2[0][3]						

					if self.m2_questions_g3==1:
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m2_g3 = fores5.predict_instance()
						self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
					elif self.m2_questions_g3==2:
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m2_g3 = fores5.predict_instance()
						self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
						self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
					elif self.m2_questions_g3==3:
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m2_g3 = fores5.predict_instance()
						self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
						self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
						self.type_m2_g3_3r = self.outputs_table_5_m2_g3[0][2]
					elif self.m2_questions_g3==4:
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g3q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_533)
						self.outputs_table_5_m2_g3 = fores5.predict_instance()
						self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
						self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
						self.type_m2_g3_3r = self.outputs_table_5_m2_g3[0][2]
						self.type_m2_g3_4r = self.outputs_table_5_m2_g3[0][3]						
					
				if games_m1==4:
					self.m1_questions_g1 = int(self.outputs_table_4_m1[0][2])
					self.m1_questions_g2 = int(self.outputs_table_4_m1[0][6]) 
					self.m1_questions_g3 = int(self.outputs_table_4_m1[0][10])	
					self.m1_questions_g4 = int(self.outputs_table_4_m1[0][14])	
					dict_table_541 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g1], 
						'col3': [self.m1_g1_order_diff_ques], 
						'col4': [self.kind_m1_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_542 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g2], 
						'col3': [self.m1_g2_order_diff_ques], 
						'col4': [self.kind_m1_g2], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}
					dict_table_543 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g3],	
						'col3': [self.m1_g3_order_diff_ques], 
						'col4': [self.kind_m1_g3],	
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}
					dict_table_544 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m1_g4], 
						'col3': [self.m1_g4_order_diff_ques], 
						'col4': [self.kind_m1_g4], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 

					if self.m1_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
					elif self.m1_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
					elif self.m1_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
					elif self.m1_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m1_g1 = fores5.predict_instance()
						self.type_m1_g1_1r = self.outputs_table_5_m1_g1[0][0]
						self.type_m1_g1_2r = self.outputs_table_5_m1_g1[0][1]
						self.type_m1_g1_3r = self.outputs_table_5_m1_g1[0][2]
						self.type_m1_g1_4r = self.outputs_table_5_m1_g1[0][3]						
					
					if self.m1_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
					elif self.m1_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
					elif self.m1_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
					elif self.m1_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m1_g2 = fores5.predict_instance()
						self.type_m1_g2_1r = self.outputs_table_5_m1_g2[0][0]
						self.type_m1_g2_2r = self.outputs_table_5_m1_g2[0][1]
						self.type_m1_g2_3r = self.outputs_table_5_m1_g2[0][2]
						self.type_m1_g2_4r = self.outputs_table_5_m1_g2[0][3]						

					if self.m1_questions_g3==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
					elif self.m1_questions_g3==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
					elif self.m1_questions_g3==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
					elif self.m1_questions_g3==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m1_g3 = fores5.predict_instance()
						self.type_m1_g3_1r = self.outputs_table_5_m1_g3[0][0]
						self.type_m1_g3_2r = self.outputs_table_5_m1_g3[0][1]
						self.type_m1_g3_3r = self.outputs_table_5_m1_g3[0][2]
						self.type_m1_g3_4r = self.outputs_table_5_m1_g3[0][3]						

					if self.m1_questions_g4==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
					elif self.m1_questions_g4==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
						self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
					elif self.m1_questions_g4==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
						self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
						self.type_m1_g4_3r = self.outputs_table_5_m1_g4[0][2]
					elif self.m1_questions_g4==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m1g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m1_g4 = fores5.predict_instance()
						self.type_m1_g4_1r = self.outputs_table_5_m1_g4[0][0]
						self.type_m1_g4_2r = self.outputs_table_5_m1_g4[0][1]
						self.type_m1_g4_3r = self.outputs_table_5_m1_g4[0][2]
						self.type_m1_g4_4r = self.outputs_table_5_m1_g4[0][3]						

				if games_m2==4:
					self.m2_questions_g1 = int(self.outputs_table_4_m2[0][2])
					self.m2_questions_g2 = int(self.outputs_table_4_m2[0][6]) 
					self.m2_questions_g3 = int(self.outputs_table_4_m2[0][10])	
					self.m2_questions_g4 = int(self.outputs_table_4_m2[0][14])
					dict_table_541 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m2_g1], 
						'col3': [self.m2_g1_order_diff_ques], 
						'col4': [self.kind_m2_g1], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]} 
					dict_table_542 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m2_g2], 
						'col3': [self.m2_g2_order_diff_ques], 
						'col4': [self.kind_m2_g2], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}
					dict_table_543 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m2_g3],	
						'col3': [self.m2_g3_order_diff_ques], 
						'col4': [self.kind_m2_g3],	
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}
					dict_table_544 = {
						'col1': [self.level_input], 
						'col2': [self.diff_m2_g4], 
						'col3': [self.m2_g4_order_diff_ques], 
						'col4': [self.kind_m2_g4], 
						'col5': [self.s3_distraction_in_listening_input],
						'col6': [self.s1_understanding_input],
						'col7': [self.s3_memoria_deficit_input],
						'col8': [self.n1_focus_on_comprehension_input],
						'col9': [self.n2_focus_on_automation_input]}
					
					if self.m2_questions_g1==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
					elif self.m2_questions_g1==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
					elif self.m2_questions_g1==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
						self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
					elif self.m2_questions_g1==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g1q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_541)
						self.outputs_table_5_m2_g1 = fores5.predict_instance()
						self.type_m2_g1_1r = self.outputs_table_5_m2_g1[0][0]
						self.type_m2_g1_2r = self.outputs_table_5_m2_g1[0][1]
						self.type_m2_g1_3r = self.outputs_table_5_m2_g1[0][2]
						self.type_m2_g1_4r = self.outputs_table_5_m2_g1[0][3]						

					if self.m2_questions_g2==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
					elif self.m2_questions_g2==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
						self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
					elif self.m2_questions_g2==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
						self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
						self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
					elif self.m2_questions_g2==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g2q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_542)
						self.outputs_table_5_m2_g2 = fores5.predict_instance()
						self.type_m2_g2_1r = self.outputs_table_5_m2_g2[0][0]
						self.type_m2_g2_2r = self.outputs_table_5_m2_g2[0][1]
						self.type_m2_g2_3r = self.outputs_table_5_m2_g2[0][2]
						self.type_m2_g2_4r = self.outputs_table_5_m2_g2[0][3]						

					if self.m2_questions_g3==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m2_g3 = fores5.predict_instance()
						self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
					elif self.m2_questions_g3==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m2_g3 = fores5.predict_instance()
						self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
						self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
					elif self.m2_questions_g3==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m2_g3 = fores5.predict_instance()
						self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
						self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
						self.type_m2_g3_3r = self.outputs_table_5_m2_g3[0][2]
					elif self.m2_questions_g3==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g3q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_543)
						self.outputs_table_5_m2_g3 = fores5.predict_instance()
						self.type_m2_g3_1r = self.outputs_table_5_m2_g3[0][0]
						self.type_m2_g3_2r = self.outputs_table_5_m2_g3[0][1]
						self.type_m2_g3_3r = self.outputs_table_5_m2_g3[0][2]
						self.type_m2_g3_4r = self.outputs_table_5_m2_g3[0][3]						

					if self.m2_questions_g4==1: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q1", received_dataset = self.csv_lines[174], num_outputs_prev = 1, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m2_g4 = fores5.predict_instance()
						self.type_m2_g4_1r = self.outputs_table_5_m2_g4[0][0]
					elif self.m2_questions_g4==2: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q2", received_dataset = self.csv_lines[175], num_outputs_prev = 2, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m2_g4 = fores5.predict_instance()
						self.type_m2_g4_1r = self.outputs_table_5_m2_g4[0][0]
						self.type_m2_g4_2r = self.outputs_table_5_m2_g4[0][1]
					elif self.m2_questions_g4==3: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q3", received_dataset = self.csv_lines[176], num_outputs_prev = 3, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m2_g4 = fores5.predict_instance()
						self.type_m2_g4_1r = self.outputs_table_5_m2_g4[0][0]
						self.type_m2_g4_2r = self.outputs_table_5_m2_g4[0][1]
						self.type_m2_g4_3r = self.outputs_table_5_m2_g4[0][2]
					elif self.m2_questions_g4==4: 
						fores5 = cswp.RanForest_clf_table_5(name="predict_trained_dataset#t5m2g4q4", received_dataset = self.csv_lines[177], num_outputs_prev = 4, weights_flag=0)
						fores5.prepare_array(dict_table_544)
						self.outputs_table_5_m2_g4 = fores5.predict_instance()
						self.type_m2_g4_1r = self.outputs_table_5_m2_g4[0][0]
						self.type_m2_g4_2r = self.outputs_table_5_m2_g4[0][1]
						self.type_m2_g4_3r = self.outputs_table_5_m2_g4[0][2]
						self.type_m2_g4_4r = self.outputs_table_5_m2_g4[0][3]

					apsh.check_security_before_creation(self)
					adp3.conclude_final_session_creation(self)

			#########3
			elif self.num_of_matches==3 or self.num_of_matches==4:
				apsh.add_new_Session_table_with_kid_case34(self, dict_table_2)
			
