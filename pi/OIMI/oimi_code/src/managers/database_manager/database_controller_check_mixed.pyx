"""Info:
	Oimi robot database_manager check mixed matches
	matches with one game per kind, random criteria + 1 questions for each game, 
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
#sys.path.insert(1, os.path.join(sys.path[0], '.'))
import re
import mmap
from importlib import reload
from subprocess import run 
from typing import Optional
import oimi_queries as oq
cimport modify_default_lists as mdl
cimport database_controller_build as acb
cimport database_controller_add_into as dca
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef int check_match_is_mixed(db_self, int match_id):
	cdef int is_mixed = 0
	db_self.curs.execute("SELECT num_questions FROM Full_played_recap_giga WHERE match_id=?",(match_id,))
	re1 = db_self.curs.fetchall()
	res_match = [item for sublist in re1 for item in sublist]
	print(res_match)
	db_self.curs.execute("SELECT num_of_games FROM Full_played_recap_giga WHERE match_id=?",(match_id,))
	nu_ga = db_self.curs.fetchone()[0]
	print("num of games of this match", nu_ga)
	if nu_ga>1:
		is_mixed = all(numq==1 for numq in res_match)
	
	print("is_mixed or not mixed? ".format(is_mixed))
	return is_mixed

cdef int check_session_is_mixed(db_self, int session_id):
	cdef int is_mix = 0
	db_self.curs.execute("SELECT DISTINCT match_id FROM Full_played_recap_giga WHERE session_id=?",(session_id,))
	mama = db_self.curs.fetchall()
	rema = [item for sublist in mama for item in sublist]
	list_mat = []
	for r in rema:
		db_self.curs.execute("SELECT num_of_games FROM Full_played_recap_giga WHERE match_id=?",(r,))
		nu_ga = db_self.curs.fetchone()[0]
		print("num of games for match r ", nu_ga)
		if nu_ga>1:
			db_self.curs.execute("SELECT num_questions FROM Full_played_recap_giga WHERE match_id=?",(r,))
			re1 = db_self.curs.fetchall()
			res_match = [item for sublist in re1 for item in sublist]
			is_mix = all(numq==1 for numq in res_match)
			if not is_mix:
				break
		else:
			break
	print("There is a mixed ?".format(is_mix))
	return is_mix



	
