"""Info:
	Oimi robot database_controler_add_into 
	database module for adding new elements into the right tables and adding a new line into db_default_lists.
	In the case of adding a new relationship between elements (create a new istance in a Bridge table), no new constraint need to be added (no need to update oimi_queries file).
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager database_controller_add_into
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
#sys.path.insert(1, os.path.join(sys.path[0], '.'))
import mmap
import copy
import random
import pickle
import traceback
from subprocess import run
from importlib import reload
from typing import Optional
import oimi_queries as oq
import db_default_lists as dbdl
import db_extern_methods as dbem
import db_extern_methods_two as dbem2

cimport modify_default_lists as mdl
cimport add_prediction as ap
cimport add_customized_prediction as acp
cimport database_controller_save_to_files as dcs
cimport database_controller_build as dcb
cimport database_controller_add_into as dba
cimport database_controller_add_into_part_2 as dba2
cimport database_controller_add_into_part_2_1 as dba21
cimport database_controller_add_into_part_3 as dba3
cimport database_controller_add_into_part_4 as dba4
# =============================================================================================================================================================================================================================================
# Methods
# =============================================================================================================================================================================================================================================
cdef add_new_game(db_self, int num_questions, str kind_game, str type_game, str difficulty, str order_difficulty_questions, int necessary_time, bint disposable, scenario, list list_types, list questions_already_choosen_this_match):
	print("ENTRO IN add_new_Game_tableadd_new_Game_tableadd_new_Game_table!! add_new_Game_table")
	print("questions_already_choosen_this_match received as param is ==> {}".format(questions_already_choosen_this_match))
	cdef:
		str new_id = db_self.assign_new_id('Games')
		str	search_str = "\t({0}, '{1}', '{2}', '{3}', '{4}'".format(num_questions, kind_game, type_game, difficulty, order_difficulty_questions)
		bint new_del = 0
	print("search str in add_new_game per trovare errore che lo cambia a caso cmq, quando non dovrebbe!!! {}".format(search_str))
	try:
		print("entro nel try di add_new_game_db")
		print("list types list types list types list types !!!!add_new_Game_table!! {}".format(list_types))
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Game you want to insert already exist.')
				#raise Exception
				pass
			reload(oq)
			################################### set time
			if num_questions==1 and kind_game in ['1F','2L','5K']:
				necessary_time = 10
			if num_questions==1 and kind_game in ['3S','4P']:
				necessary_time = 12
			if num_questions==1 and kind_game=='7O':
				necessary_time = 15
			if num_questions==1 and kind_game in['8Q','9C']:
				necessary_time = 18
			if num_questions==1 and kind_game=='6I':
				necessary_time = 20

			if num_questions==2 and kind_game in ['1F','2L','5K']:
				necessary_time = 20
			if num_questions==2 and kind_game in ['3S','4P']:
				necessary_time = 24
			if num_questions==2 and kind_game=='7O':
				necessary_time = 30
			if num_questions==2 and kind_game in['8Q','9C']:
				necessary_time = 36
			if num_questions==2 and kind_game=='6I':
				necessary_time = 40

			if num_questions==3 and kind_game in ['1F','2L','5K']:
				necessary_time = 30
			if num_questions==3 and kind_game in ['3S','4P']:
				necessary_time = 36
			if num_questions==3 and kind_game=='7O':
				necessary_time = 45
			if num_questions==3 and kind_game in['8Q','9C']:
				necessary_time = 54
			if num_questions==3 and kind_game=='6I':
				necessary_time = 60

			if num_questions==4 and kind_game in ['1F','2L','5K']:
				necessary_time = 40
			if num_questions==4 and kind_game in ['3S','4P']:
				necessary_time = 48
			if num_questions==4 and kind_game=='7O':
				necessary_time = 60
			if num_questions==4 and kind_game in['8Q','9C']:
				necessary_time = 72
			if num_questions==4 and kind_game=='6I':
				necessary_time = 80
			################################### set time
			db_self.curs.execute('''INSERT INTO Games (num_questions, kind, type, difficulty, order_difficulty_questions, necessary_time, disposable, token) 
								VALUES (?,?,?,?,?,?,?,?)''', (num_questions, kind_game, type_game, difficulty, order_difficulty_questions, necessary_time, disposable, new_id,))
			db_self.conn.commit()
			query_audio_intro = "UPDATE Games SET audio_intro = '{}'".format('audio_1')
			query_audio_exit = "UPDATE Games SET audio_exit = '{}'".format('audio_2')
			db_self.curs.execute(query_audio_intro)
			db_self.curs.execute(query_audio_exit)
			db_self.conn.commit()
			db_self.curs.execute( "SELECT COUNT(rowid) FROM Games")
			num_last_line_g = db_self.curs.fetchone()				
			print("num_last_line_gnum_last_line_gnum_last_line_gnum_last_line_g {}".format(num_last_line_g))
			if num_last_line_g:
				num_last_line_g= num_last_line_g[0]
			else:
				num_last_line_g = 0
				print("ERRORE_GRAVISSIMOOOOOOOOOOOOOO")					
			mdl.modify_Games_list(num_questions, kind_game, type_game, difficulty, order_difficulty_questions, necessary_time, disposable, new_id, num_last_line_g)
		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
			print("ho trovato questa deletions_list!!!!")
			print("deletions_list = {}".format(db_self.deletions_list))
			if db_self.deletions_list[5]!=[0]:
				new_del = 1
				dbem.bubblesort(db_self.deletions_list[5])
				db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Games\'")
				index_to_replace = db_self.curs.fetchone()[1]
				index_corrected = db_self.deletions_list[5].pop(0)
				db_self.curs.execute('''UPDATE Games SET game_id = ?  WHERE game_id = ?''', (index_corrected, index_to_replace))
				#se vuota rimetto [0]
				if not db_self.deletions_list[5]:
					db_self.deletions_list[5].append(0)
					with open(filename_pk, 'wb') as fi:
						pickle.dump(db_self.deletions_list, fi)
				db_self.conn.commit()
		print("The new Game is now added.")
		db_self.curs.execute('''SELECT COUNT(question_id)
		FROM Questions
			JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
			ON Scenarios_Questions.audio_id = Questions.audio
		WHERE scenario_id = ? ''',(scenario,))
		num_possible_questions = db_self.curs.fetchone()[0]
		#if num_possible_questions < num_of_questions:
		#	num_of_questions = num_possible_questions
		avoid_this_question = 1 #for avoid 5ks il più vecchio e il più giovane che non posso fare al momento
		#QUESTIONS #QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS
		#QUESTIONS #QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS
		#QUESTIONS #QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS
		num_times_always_same_is_choosen = 0
		while avoid_this_question>0:
			if type_game!='MISCELLANEOUS': #type already = to imposed if is the case
				questions_ok = []
				if kind_game!='4P':
					db_self.curs.execute('''SELECT DISTINCT question_id FROM Questions
						JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
						ON Scenarios_Questions.audio_id = Questions.audio
						WHERE scenario_id = ? AND type = ?
						ORDER BY RANDOM()
						LIMIT ?''',(scenario,type_game,num_questions,))
				elif type_game=='4PSU':
					print("non siamo in misce!! 4PSU4PSU4PSU4PSU4PSU4PSU 4PSU4PSU4PSU4PSU niente miscellaneous!! parametri da inserire query nulla scenario = {} type_question(type_game) = {} num_questions = {}".format(scenario,type_game,num_questions))
					db_self.curs.execute('''SELECT DISTINCT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
						WHERE scenario_id = ?  
						ORDER BY RANDOM()
						LIMIT ?''',(scenario,num_questions,))
				elif type_game=='4PCO':
					print("non siamo in misce!! 4PCO4PCO4PCO4PCO4PCO niente miscellaneous!! parametri da inserire query nulla scenario = {} type_question(type_game) = {} num_questions = {}".format(scenario,type_game,num_questions))
					db_self.curs.execute('''SELECT DISTINCT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
						WHERE scenario_id = ?  
						ORDER BY RANDOM()
						LIMIT ?''',(scenario,num_questions,))
				choosen_quests = db_self.curs.fetchall()
				print("!!!!!choosen_quests!!!! QUELLO DELLA LOLA--> {}".format(choosen_quests))
				for i in range(len(choosen_quests)):
					questions_ok.append(choosen_quests[i][0])
				print("!!!!!questions_ok!!!! BEFORE CHANGE IT--> {}".format(questions_ok))
				if db_self.imposed_ques_id!=99999 and scenario==db_self.suggested_scenario_from_q and db_self.already_done_imposed_ask_ques==1 and db_self.game_impo_from_q_added==0:
					ind_to_change = random.randint(1,len(questions_ok)) - 1
					questions_ok[ind_to_change] = db_self.imposed_ques_id
					db_self.game_impo_from_q_added =  1
					print("!!!!!questions_ok!!!! dopo che gli ho messo gioco uguale!!! --> {}".format(questions_ok))
			else: #case miscella!!!
				types_to_pass = []
				#case miscellaneous!! there as an imposition??? check!
				print("parametri SECONDO CASO siamo in miscellaneous!! da inserire query nulla scenario = {} type_question(type_game) = {} num_questions = {}".format(scenario,type_game,num_questions))
				if len(list_types)>1:
					types_to_pass = random.choices(list_types,k = num_questions)
				else:
					types_to_pass.append(list_types[0])
				print("NUOVA STAMPADI types_to_pass -> ", types_to_pass)
				#if db_self.imposed_kind_game!='' and db_self.already_done_imposed_kind_game==1 and scenario==db_self.suggested_scenario_from_k and db_self.type_impo_from_k_added==0:
				#	print("CE UN IMPOSED TYPE imposto dal kind!!! e siamo in add_new_Game_table!!!!!!!")
				#	if db_self.imposed_type_game_by_k not in types_to_pass and kind_game==db_self.imposed_kind_game:
				#		print("CE UN IMPOSED TYPE imposto dal kind ma non ho scelto quello imposed casualmente e siamo in add_new_Game_table !!!!!!!!!!")
				#		ind_to_change = random.randint(1,len(types_to_pass)) - 1
				#		types_to_pass[ind_to_change]=db_self.imposed_type_game_by_k
				#		db_self.type_impo_from_k_added = 1
				if db_self.imposed_type_game!='' and db_self.already_done_imposed_type_game==1 and scenario==db_self.suggested_scenario_from_t and db_self.type_impo_from_t_added==0:
					print("CE UN IMPOSED TYPE e siamo in add_new_Game_table!!!!!!!")
					kind_this_type = dbem2.find_kind(db_self.imposed_type_game)
					if db_self.imposed_type_game not in types_to_pass and kind_game==kind_this_type:
						print("CE UN IMPOSED TYPE ed è proprio quello imposto!!!!!!!!!!")
						ind_to_change = random.randint(1,len(types_to_pass)) - 1
						types_to_pass[ind_to_change]=db_self.imposed_type_game
						db_self.type_impo_from_t_added = 1
				if db_self.imposed_ques_id!=99999 and db_self.already_done_imposed_ask_ques==1 and db_self.game_impo_from_q_added==0 and scenario==db_self.suggested_scenario_from_q:
					db_self.curs.execute("SELECT kind FROM Questions WHERE question_id = ?",(db_self.imposed_ques_id,))
					kind_this_ques = db_self.curs.fetchone()[0]
					print("CE UN IMPOSED TYPE imposto dalla question!! ma dovuto alla domanda richiesta specifica!! add_new_Game_table!!!!!!!")
					if db_self.suggested_type_game_from_q not in types_to_pass and kind_game==kind_this_ques:
						print("CE UN IMPOSED ques from imposed question ma non ho scelto quello imposed casualmente e siamo in add_new_Game_table !!!!!!!!!!")
						ind_to_change = random.randint(1,len(types_to_pass)) - 1
						types_to_pass[ind_to_change]=db_self.suggested_type_game_from_q
						db_self.game_impo_from_q_added = 1
				print("types_to_pass types_to_pass types_to_pass types_to_passtypes_to_pass types_to_pass types_to_pass BEFORE SHUFFLE!!!{}".format(types_to_pass))
				random.shuffle(types_to_pass)
				print("types_to_pass types_to_pass types_to_pass types_to_passtypes_to_pass types_to_pass types_to_pass AFTER SHUFFLE!!! {}".format(types_to_pass))
				ucanexit = 0
				while ucanexit==0:
					questions_ok = []
					print("LA LUNGHEZZA DI types_to_pass {}".format(len(types_to_pass)))
					for i in range(len(types_to_pass)):
						print("CASO MISCELLANEOUS!!!parametri da inserire query GIRO NUM ---> {}  nulla scenario = {} type_question = {}".format(i,scenario,types_to_pass[i]))
						if len(questions_ok)==0:
							print("when in add_new_game seleziono domanda precisa....caso 0")
							if kind_game!='4P':
								db_self.curs.execute('''SELECT question_id FROM Questions
									JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
									ON Scenarios_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND type = ? 
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,types_to_pass[i],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)													
							elif types_to_pass[i]=='4PSU':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
									WHERE scenario_id = ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
							elif types_to_pass[i]=='4PCO':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
									WHERE scenario_id = ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
							else:
								print("DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!! 4P ANDATO STORTO!!!")							
						if len(questions_ok)==1:
							print("when in add_new_game seleziono domanda precisa....caso 1")
							if kind_game!='4P':
								db_self.curs.execute('''SELECT question_id FROM Questions
									JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
									ON Scenarios_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND type = ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions
										JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
										ON Scenarios_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND type = ? 
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,types_to_pass[i],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità::")
									print(choosen_quests)
							elif types_to_pass[i]=='4PSU':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ?
									ORDER BY RANDOM() 
									LIMIT 1''',(scenario,questions_ok[0],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? 
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
							elif types_to_pass[i]=='4PCO':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,questions_ok[0],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? 
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
							else:
								print("DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!! 4P ANDATO STORTO!!!")
						if len(questions_ok)==2:
							print("when in add_new_game seleziono domanda precisa....caso 2")
							if kind_game!='4P':
								db_self.curs.execute('''SELECT question_id FROM Questions
									JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
									ON Scenarios_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND type = ? AND question_id != ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],questions_ok[1],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions
										JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
										ON Scenarios_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND type = ? AND (question_id != ? OR question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],questions_ok[1],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions
											JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
											ON Scenarios_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? AND type = ? 
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,types_to_pass[i],))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità::")
										print(choosen_quests)
							elif types_to_pass[i]=='4PSU':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND (question_id != ? OR question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? 
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità is::")
										print(choosen_quests)
							elif types_to_pass[i]=='4PCO':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND (question_id != ? OR question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? 
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità is::")
										print(choosen_quests)
							else:
								print("DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!! 4P ANDATO STORTO!!!")
						if len(questions_ok)==3:
							print("when in add_new_game seleziono domanda precisa....caso 3")
							if kind_game!='4P':
								db_self.curs.execute('''SELECT question_id FROM Questions
									JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
									ON Scenarios_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND type = ? AND question_id != ? AND question_id != ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],questions_ok[1],questions_ok[2],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions
										JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
										ON Scenarios_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND type = ? AND question_id != ? AND (question_id != ? AND question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],questions_ok[1],questions_ok[2],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions
											JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
											ON Scenarios_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? AND type = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,types_to_pass[i],questions_ok[1],questions_ok[0],questions_ok[2],))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità::")
										print(choosen_quests)
										if not choosen_quests:
											db_self.curs.execute('''SELECT question_id FROM Questions
												JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
												ON Scenarios_Questions.audio_id = Questions.audio
												WHERE scenario_id = ? AND type = ? AND question_id != ? AND (question_id != ? AND question_id != ?)
												ORDER BY RANDOM()
												LIMIT 1''',(scenario,types_to_pass[i],questions_ok[2],questions_ok[0],questions_ok[1],))
											choosen_quests = db_self.curs.fetchone()
											print("choosen_quests quarta possibilità::")
											print(choosen_quests)
											if not choosen_quests:
												db_self.curs.execute('''SELECT question_id FROM Questions
													JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
													ON Scenarios_Questions.audio_id = Questions.audio
													WHERE scenario_id = ? AND type = ? AND (question_id != ? OR question_id != ? OR question_id != ?)
													ORDER BY RANDOM()
													LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],questions_ok[1],questions_ok[2],))
												choosen_quests = db_self.curs.fetchone()
												print("choosen_quests quinta possibilità::")
												print(choosen_quests)
												if not choosen_quests:
													db_self.curs.execute('''SELECT question_id FROM Questions
														JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
														ON Scenarios_Questions.audio_id = Questions.audio
														WHERE scenario_id = ?
														ORDER BY RANDOM()
														LIMIT 1''',(scenario,types_to_pass[i],))
													choosen_quests = db_self.curs.fetchone()
													print("choosen_quests sesta possibilità::")
													print(choosen_quests)												
							elif types_to_pass[i]=='4PSU':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ? OR question_id != ? AND question_id != ?)
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,questions_ok[1],questions_ok[0],questions_ok[2],))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità is::")
										print(choosen_quests)
										if not choosen_quests:
											db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
												WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
												ORDER BY RANDOM()
												LIMIT 1''',(scenario,questions_ok[2],questions_ok[0],questions_ok[1],))
											choosen_quests = db_self.curs.fetchone()
											print("choosen_quests quarta possibilità is::")
											print(choosen_quests)
											if not choosen_quests:
												db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
													WHERE scenario_id = ? AND (question_id != ? OR question_id != ? OR question_id != ?)
													ORDER BY RANDOM()
													LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
												choosen_quests = db_self.curs.fetchone()
												print("choosen_quests quinta possibilità is::")
												print(choosen_quests)
												if not choosen_quests:
													db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
														WHERE scenario_id = ?
														ORDER BY RANDOM()
														LIMIT 1''',(scenario,))
													choosen_quests = db_self.curs.fetchone()
													print("choosen_quests sesta possibilità is::")
													print(choosen_quests)
							elif types_to_pass[i]=='4PCO':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ? AND question_id != ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,questions_ok[1],questions_ok[0],questions_ok[2],))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità is::")
										print(choosen_quests)
										if not choosen_quests:
											db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
												WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
												ORDER BY RANDOM()
												LIMIT 1''',(scenario,questions_ok[2],questions_ok[0],questions_ok[1],))
											choosen_quests = db_self.curs.fetchone()
											print("choosen_quests quarta possibilità is::")
											print(choosen_quests)
											if not choosen_quests:
												db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
													WHERE scenario_id = ? AND (question_id != ? OR question_id != ? OR question_id != ?)
													ORDER BY RANDOM()
													LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
												choosen_quests = db_self.curs.fetchone()
												print("choosen_quests quinta possibilità is::")
												print(choosen_quests)
												if not choosen_quests:
													db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
														WHERE scenario_id = ?
														ORDER BY RANDOM()
														LIMIT 1''',(scenario,))
													choosen_quests = db_self.curs.fetchone()
													print("choosen_quests sesta possibilità is::")
													print(choosen_quests)
							else:
								print("DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!! 4P ANDATO STORTO!!!")
						print("!!!!!questions_ok!!!! BEFORE CHANGE siamo in misce!! IT--> {}".format(questions_ok))
						questions_ok.append(choosen_quests[0])
						################################################## controllo!!! verify
						if kind_game!='4P':
							db_self.curs.execute('''SELECT question_id FROM Questions
								JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
								ON Scenarios_Questions.audio_id = Questions.audio
								WHERE scenario_id = ? AND type = ? 
								ORDER BY RANDOM()''',(scenario,types_to_pass[i],))
						elif types_to_pass[i]=='4PSU':
							db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
								WHERE scenario_id = ?
								ORDER BY RANDOM()''',(scenario,))
						elif types_to_pass[i]=='4PCO':
							db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
								WHERE scenario_id = ?
								ORDER BY RANDOM()''',(scenario,))
						choosen_quests_verify = db_self.curs.fetchall()
						quest_verify = [item for sublist in choosen_quests_verify for item in sublist]
						print("quest_verify", quest_verify)
						################################################## controllo ma deve combaciare col kind
						if db_self.imposed_ques_id!=99999 and kind_game==db_self.suggested_kind_game_from_q and scenario==db_self.suggested_scenario_from_q and db_self.already_done_imposed_ask_ques==1 and db_self.game_impo_from_q_added==0:
							ind_to_change = random.randint(1,len(questions_ok)) - 1
							questions_ok[ind_to_change] = db_self.imposed_ques_id
							db_self.game_impo_from_q_added =  1
							print("!!!!!questions_ok!!!! dopo che gli ho messo gioco uguale!!! --> {}".format(questions_ok))
						################################################## controllo ma deve combaciare col kind
					print("!!!!!questions_ok SECONDO CASO NOT MISCE--> {}".format(questions_ok))
					questions_ok = list(set(questions_ok))
					print("!!!!!questions_ok SECONDO dopo riduzione del set--> {}".format(questions_ok))
					if len(questions_ok)>1 and len(quest_verify)>1 and any(questions_ok.count(x)==len(questions_ok) for x in questions_ok): #if all same question = not miscellaneous anymore!
							ucanexit = 0
							print("RICOMINCIO IL GIRO!!!")
							print("RICOMINCIO IL GIRO!!!")			
							print("RICOMINCIO IL GIRO!!!")			
							print("RICOMINCIO IL GIRO!!!")
					else:
						choose = random.randint(0,2)
						if choose==0:
							if len(questions_ok)>1 and len(quest_verify)>1 and any(questions_ok.count(x) > 2 for x in questions_ok): #if duplicates 
								ucanexit = 0
								print("RICOMINCIO IL GIRO!!!")
								print("RICOMINCIO IL GIRO!!!")
								print("RICOMINCIO IL GIRO!!!")
								print("RICOMINCIO IL GIRO!!!")
							else:
								ucanexit = 1
						else:
							ucanexit = 1
			###############################################################################################################################################################################
			counter_already_done = 0
			round_n = 0
			for q in questions_ok:
				round_n+=1 
				if q in ['audio_3046','audio_3047']: #5k tocca il piu giovace o il piu vecchio per adesso c'è solo la bambina ....
					avoid_this_question = 1
					db_self.game_impo_from_q_added =  0
					break
				elif q in questions_already_choosen_this_match:
					if round_n==1:
						num_times_always_same_is_choosen+=1
					print("trovato stesso in questions_already_choosen_this_match")
					counter_already_done+=1
					idx_qma = questions_ok.index(q)
			print("counter_already_done prima dei controlli", counter_already_done)
			print("num_times_always_same_is_choosen prima dei controlli", num_times_always_same_is_choosen)
			if counter_already_done==1 and len(questions_ok)>3:
				questions_ok.pop(idx_qma)
				print("non rifaccio il giro perché mi basta levare la domanda da questions_ok!!!")
				num_times_always_same_is_choosen = 0
				avoid_this_question = 0
			elif counter_already_done>0 and num_times_always_same_is_choosen>=3:
				print("basta rifare il giro che evidentemente è impossibile da evitare la stessa domanda")
				num_times_always_same_is_choosen = 0
				avoid_this_question = 0
			elif counter_already_done>0 and num_times_always_same_is_choosen<3:
				avoid_this_question = 1
				print("rifaccio il giro perché già usata la domanda nel match!!!")
			else:
				print("ok stop!!!!")
				avoid_this_question = 0
			###############################################################################################################################################################################
			#redo in case of ripetutute subject for question 9cnssu 9cnsu 9cnco
			if difficulty in ['Normal_2','Medium_1','Medium_2','Hard_1','Hard_2'] and kind_game=='9C':
				print("OREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREO")
				print("!!!!!questions_ok questions_ok questions_ok	{}".format(questions_ok))
				go_on = 1
				while go_on>0:
					if len(questions_ok)==1:
						avoid_this_question = 0
						go_on = 0
					if len(questions_ok)==2:
					#9cnsu
						if questions_ok[0] in [11889,11890,11891,11892] and questions_ok[1] in [11889,11890,11891,11892]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11893,11894,11895,11896] and questions_ok[1] in [11893,11894,11895,11896]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11897,11898,11899,11900] and questions_ok[1] in [11897,11898,11899,11900]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11901,11902,11903,11904] and questions_ok[1] in [11901,11902,11903,11904]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11905,11906,11907,11908] and questions_ok[1] in [11905,11906,11907,11908]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11909,11910,11911,11912] and questions_ok[1] in [11909,11910,11911,11912]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11913,11914,11915,11916] and questions_ok[1] in [11913,11914,11915,11916]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11917,11918,11919,11920] and questions_ok[1] in [11917,11918,11919,11920]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11921,11922,11923,11924] and questions_ok[1] in [11921,11922,11923,11924]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11925,11926,11927,11928] and questions_ok[1] in [11925,11926,11927,11928]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[0] in [11929,11930,11931] and questions_ok[1] in [11929,11930,11931]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11929,11930,11931] and questions_ok[1] in [11929,11930,11931]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11935,11936,11937] and questions_ok[1] in [11935,11936,11937]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11938,11939,11940] and questions_ok[1] in [11938,11939,11940]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[0] in [11941,11942,11943] and questions_ok[1] in [11941,11942,11943]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11944,11945,11946] and questions_ok[1] in [11944,11945,11946]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11947,11948,11949] and questions_ok[1] in [11947,11948,11949]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11950,11951,11952] and questions_ok[1] in [11950,11951,11952]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11953,11954,11955] and questions_ok[1] in [11953,11954,11955]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11956,11957,11958] and questions_ok[1] in [11956,11957,11958]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11959,11960,11961] and questions_ok[1] in [11959,11960,11961]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11962,11963,11964] and questions_ok[1] in [11962,11963,11964]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11965,11966,11967] and questions_ok[1] in [11965,11966,11967]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11968,11969,11970] and questions_ok[1] in [11968,11969,11970]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11971,11972,11973] and questions_ok[1] in [11971,11972,11973]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11974,11975,11976] and questions_ok[1] in [11974,11975,11976]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11977,11978,11979] and questions_ok[1] in [11977,11978,11979]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11980,11981,11982] and questions_ok[1] in [11980,11981,11982]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						else:
							avoid_this_question = 0
							go_on = 0

					if len(questions_ok)==3:
					#9cnsu
						if questions_ok[0] in [11889,11890,11891,11892] and (questions_ok[1] in [11889,11890,11891,11892] or questions_ok[2] in [11889,11890,11891,11892]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11893,11894,11895,11896] and (questions_ok[1] in [11893,11894,11895,11896] or questions_ok[2] in [11893,11894,11895,11896]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11897,11898,11899,11900] and (questions_ok[1] in [11897,11898,11899,11900] or questions_ok[2] in [11897,11898,11899,11900]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11901,11902,11903,11904] and (questions_ok[1] in [11901,11902,11903,11904] or questions_ok[2] in [11901,11902,11903,11904]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11905,11906,11907,11908] and (questions_ok[1] in [11905,11906,11907,11908] or questions_ok[2] in [11905,11906,11907,11908]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11909,11910,11911,11912] and (questions_ok[1] in [11909,11910,11911,11912] or questions_ok[2] in [11909,11910,11911,11912]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11913,11914,11915,11916] and (questions_ok[1] in [11913,11914,11915,11916] or questions_ok[2] in [11913,11914,11915,11916]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11917,11918,11919,11920] and (questions_ok[1] in [11917,11918,11919,11920] or questions_ok[2] in [11917,11918,11919,11920]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11921,11922,11923,11924] and (questions_ok[1] in [11921,11922,11923,11924] or questions_ok[2] in [11921,11922,11923,11924]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11925,11926,11927,11928] and (questions_ok[1] in [11925,11926,11927,11928] or questions_ok[2] in [11925,11926,11927,11928]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[0] in [11929,11930,11931] and (questions_ok[1] in [11929,11930,11931] or questions_ok[2] in [11929,11930,11931]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11929,11930,11931] and (questions_ok[1] in [11929,11930,11931] or questions_ok[2] in [11929,11930,11931]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11935,11936,11937] and (questions_ok[1] in [11935,11936,11937] or questions_ok[2] in [11935,11936,11937]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11938,11939,11940] and (questions_ok[1] in [11938,11939,11940] or questions_ok[2] in [11938,11939,11940]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[0] in [11941,11942,11943] and (questions_ok[1] in [11941,11942,11943] or questions_ok[2] in [11941,11942,11943]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11944,11945,11946] and (questions_ok[1] in [11944,11945,11946] or questions_ok[2] in [11944,11945,11946]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11947,11948,11949] and (questions_ok[1] in [11947,11948,11949] or questions_ok[2] in [11947,11948,11949]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11950,11951,11952] and (questions_ok[1] in [11950,11951,11952] or questions_ok[2] in [11950,11951,11952]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11953,11954,11955] and (questions_ok[1] in [11953,11954,11955] or questions_ok[2] in [11953,11954,11955]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11956,11957,11958] and (questions_ok[1] in [11956,11957,11958] or questions_ok[2] in [11956,11957,11958]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11959,11960,11961] and (questions_ok[1] in [11959,11960,11961] or questions_ok[2] in [11959,11960,11961]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11962,11963,11964] and (questions_ok[1] in [11962,11963,11964] or questions_ok[2] in [11962,11963,11964]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11965,11966,11967] and (questions_ok[1] in [11965,11966,11967] or questions_ok[2] in [11965,11966,11967]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11968,11969,11970] and (questions_ok[1] in [11968,11969,11970] or questions_ok[2] in [11968,11969,11970]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11971,11972,11973] and (questions_ok[1] in [11971,11972,11973] or questions_ok[2] in [11971,11972,11973]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11974,11975,11976] and (questions_ok[1] in [11974,11975,11976] or questions_ok[2] in [11974,11975,11976]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11977,11978,11979] and (questions_ok[1] in [11977,11978,11979] or questions_ok[2] in [11977,11978,11979]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11980,11981,11982] and (questions_ok[1] in [11980,11981,11982] or questions_ok[2] in [11980,11981,11982]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnsu
						elif questions_ok[1] in [11889,11890,11891,11892] and questions_ok[2] in [11889,11890,11891,11892]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11893,11894,11895,11896] and questions_ok[2] in [11893,11894,11895,11896]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11897,11898,11899,11900] and questions_ok[2] in [11897,11898,11899,11900]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11901,11902,11903,11904] and questions_ok[2] in [11901,11902,11903,11904]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11905,11906,11907,11908] and questions_ok[2] in [11905,11906,11907,11908]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11909,11910,11911,11912] and questions_ok[2] in [11909,11910,11911,11912]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11913,11914,11915,11916] and questions_ok[2] in [11913,11914,11915,11916]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11917,11918,11919,11920] and questions_ok[2] in [11917,11918,11919,11920]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11921,11922,11923,11924] and questions_ok[2] in [11921,11922,11923,11924]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11925,11926,11927,11928] and questions_ok[2] in [11925,11926,11927,11928]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[1] in [11929,11930,11931] and questions_ok[2] in [11929,11930,11931]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11929,11930,11931] and questions_ok[2] in [11929,11930,11931]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11935,11936,11937] and questions_ok[2] in [11935,11936,11937]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11938,11939,11940] and questions_ok[2] in [11938,11939,11940]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[1] in [11941,11942,11943] and questions_ok[2] in [11941,11942,11943]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11944,11945,11946] and questions_ok[2] in [11944,11945,11946]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11947,11948,11949] and questions_ok[2] in [11947,11948,11949]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11950,11951,11952] and questions_ok[2] in [11950,11951,11952]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11953,11954,11955] and questions_ok[2] in [11953,11954,11955]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11956,11957,11958] and questions_ok[2] in [11956,11957,11958]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11959,11960,11961] and questions_ok[2] in [11959,11960,11961]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11962,11963,11964] and questions_ok[2] in [11962,11963,11964]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11965,11966,11967] and questions_ok[2] in [11965,11966,11967]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11968,11969,11970] and questions_ok[2] in [11968,11969,11970]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11971,11972,11973] and questions_ok[2] in [11971,11972,11973]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11974,11975,11976] and questions_ok[2] in [11974,11975,11976]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11977,11978,11979] and questions_ok[2] in [11977,11978,11979]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11980,11981,11982] and questions_ok[2] in [11980,11981,11982]:
							avoid_this_question = 1
							print("devo rifare!!")
							break
						else:
							avoid_this_question = 0						
							go_on = 0

					if len(questions_ok)==4:
					#9cnsu
						if questions_ok[0] in [11889,11890,11891,11892] and (questions_ok[1] in [11889,11890,11891,11892] or questions_ok[2] in [11889,11890,11891,11892] or questions_ok[3] in [11889,11890,11891,11892]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11893,11894,11895,11896] and (questions_ok[1] in [11893,11894,11895,11896] or questions_ok[2] in [11893,11894,11895,11896] or questions_ok[3] in [11893,11894,11895,11896]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11897,11898,11899,11900] and (questions_ok[1] in [11897,11898,11899,11900] or questions_ok[2] in [11897,11898,11899,11900] or questions_ok[3] in [11897,11898,11899,11900]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11901,11902,11903,11904] and (questions_ok[1] in [11901,11902,11903,11904] or questions_ok[2] in [11901,11902,11903,11904] or questions_ok[3] in [11901,11902,11903,11904]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11905,11906,11907,11908] and (questions_ok[1] in [11905,11906,11907,11908] or questions_ok[2] in [11905,11906,11907,11908] or questions_ok[3] in [11905,11906,11907,11908]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11909,11910,11911,11912] and (questions_ok[1] in [11909,11910,11911,11912] or questions_ok[2] in [11909,11910,11911,11912] or questions_ok[3] in [11909,11910,11911,11912]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11913,11914,11915,11916] and (questions_ok[1] in [11913,11914,11915,11916] or questions_ok[2] in [11913,11914,11915,11916] or questions_ok[3] in [11913,11914,11915,11916]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11917,11918,11919,11920] and (questions_ok[1] in [11917,11918,11919,11920] or questions_ok[2] in [11917,11918,11919,11920] or questions_ok[3] in [11917,11918,11919,11920]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11921,11922,11923,11924] and (questions_ok[1] in [11921,11922,11923,11924] or questions_ok[2] in [11921,11922,11923,11924] or questions_ok[3] in [11921,11922,11923,11924]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11925,11926,11927,11928] and (questions_ok[1] in [11925,11926,11927,11928] or questions_ok[2] in [11925,11926,11927,11928] or questions_ok[3] in [11925,11926,11927,11928]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[0] in [11929,11930,11931] and (questions_ok[1] in [11929,11930,11931] or questions_ok[2] in [11929,11930,11931] or questions_ok[3] in [11929,11930,11931]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11929,11930,11931] and (questions_ok[1] in [11929,11930,11931] or questions_ok[2] in [11929,11930,11931] or questions_ok[3] in [11929,11930,11931]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11935,11936,11937] and (questions_ok[1] in [11935,11936,11937] or questions_ok[2] in [11935,11936,11937] or questions_ok[3] in [11935,11936,11937]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11938,11939,11940] and (questions_ok[1] in [11938,11939,11940] or questions_ok[2] in [11938,11939,11940] or questions_ok[3] in [11938,11939,11940]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[0] in [11941,11942,11943] and (questions_ok[1] in [11941,11942,11943] or questions_ok[2] in [11941,11942,11943] or questions_ok[3] in [11941,11942,11943]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11944,11945,11946] and (questions_ok[1] in [11944,11945,11946] or questions_ok[2] in [11944,11945,11946] or questions_ok[3] in [11944,11945,11946]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11947,11948,11949] and (questions_ok[1] in [11947,11948,11949] or questions_ok[2] in [11947,11948,11949] or questions_ok[3] in [11947,11948,11949]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11950,11951,11952] and (questions_ok[1] in [11950,11951,11952] or questions_ok[2] in [11950,11951,11952] or questions_ok[3] in [11950,11951,11952]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11953,11954,11955] and (questions_ok[1] in [11953,11954,11955] or questions_ok[2] in [11953,11954,11955] or questions_ok[3] in [11953,11954,11955]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11956,11957,11958] and (questions_ok[1] in [11956,11957,11958] or questions_ok[2] in [11956,11957,11958] or questions_ok[3] in [11956,11957,11958]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11959,11960,11961] and (questions_ok[1] in [11959,11960,11961] or questions_ok[2] in [11959,11960,11961] or questions_ok[3] in [11959,11960,11961]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11962,11963,11964] and (questions_ok[1] in [11962,11963,11964] or questions_ok[2] in [11962,11963,11964] or questions_ok[3] in [11962,11963,11964]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11965,11966,11967] and (questions_ok[1] in [11965,11966,11967] or questions_ok[2] in [11965,11966,11967] or questions_ok[3] in [11965,11966,11967]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11968,11969,11970] and (questions_ok[1] in [11968,11969,11970] or questions_ok[2] in [11968,11969,11970] or questions_ok[3] in [11968,11969,11970]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11971,11972,11973] and (questions_ok[1] in [11971,11972,11973] or questions_ok[2] in [11971,11972,11973] or questions_ok[3] in [11971,11972,11973]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11974,11975,11976] and (questions_ok[1] in [11974,11975,11976] or questions_ok[2] in [11974,11975,11976] or questions_ok[3] in [11974,11975,11976]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11977,11978,11979] and (questions_ok[1] in [11977,11978,11979] or questions_ok[2] in [11977,11978,11979] or questions_ok[3] in [11977,11978,11979]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11980,11981,11982] and (questions_ok[1] in [11980,11981,11982] or questions_ok[2] in [11980,11981,11982] or questions_ok[3] in [11980,11981,11982]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnsu
						elif questions_ok[1] in [11889,11890,11891,11892] and (questions_ok[2] in [11889,11890,11891,11892] or questions_ok[3] in [11889,11890,11891,11892]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11893,11894,11895,11896] and (questions_ok[2] in [11893,11894,11895,11896] or questions_ok[3] in [11893,11894,11895,11896]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11897,11898,11899,11900] and (questions_ok[2] in [11897,11898,11899,11900] or questions_ok[3] in [11897,11898,11899,11900]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11901,11902,11903,11904] and (questions_ok[2] in [11901,11902,11903,11904] or questions_ok[3] in [11901,11902,11903,11904]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11905,11906,11907,11908] and (questions_ok[2] in [11905,11906,11907,11908] or questions_ok[3] in [11905,11906,11907,11908]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11909,11910,11911,11912] and (questions_ok[2] in [11909,11910,11911,11912] or questions_ok[3] in [11909,11910,11911,11912]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11913,11914,11915,11916] and (questions_ok[2] in [11913,11914,11915,11916] or questions_ok[3] in [11913,11914,11915,11916]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11917,11918,11919,11920] and (questions_ok[2] in [11917,11918,11919,11920] or questions_ok[3] in [11917,11918,11919,11920]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11921,11922,11923,11924] and (questions_ok[2] in [11921,11922,11923,11924] or questions_ok[3] in [11921,11922,11923,11924]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11925,11926,11927,11928] and (questions_ok[2] in [11925,11926,11927,11928] or questions_ok[3] in [11925,11926,11927,11928]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[1] in [11929,11930,11931] and (questions_ok[2] in [11929,11930,11931] or questions_ok[3] in [11929,11930,11931]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11929,11930,11931] and (questions_ok[2] in [11929,11930,11931] or questions_ok[3] in [11929,11930,11931]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11935,11936,11937] and (questions_ok[2] in [11935,11936,11937] or questions_ok[3] in [11935,11936,11937]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11938,11939,11940] and (questions_ok[2] in [11938,11939,11940] or questions_ok[3] in [11938,11939,11940]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[1] in [11941,11942,11943] and (questions_ok[2] in [11941,11942,11943] or questions_ok[3] in [11941,11942,11943]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11944,11945,11946] and (questions_ok[2] in [11944,11945,11946] or questions_ok[3] in [11944,11945,11946]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11947,11948,11949] and (questions_ok[2] in [11947,11948,11949] or questions_ok[3] in [11947,11948,11949]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11950,11951,11952] and (questions_ok[2] in [11950,11951,11952] or questions_ok[3] in [11950,11951,11952]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11953,11954,11955] and (questions_ok[2] in [11953,11954,11955] or questions_ok[3] in [11953,11954,11955]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11956,11957,11958] and (questions_ok[2] in [11956,11957,11958] or questions_ok[3] in [11956,11957,11958]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11959,11960,11961] and (questions_ok[2] in [11959,11960,11961] or questions_ok[3] in [11959,11960,11961]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11962,11963,11964] and (questions_ok[2] in [11962,11963,11964] or questions_ok[3] in [11962,11963,11964]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11965,11966,11967] and (questions_ok[2] in [11965,11966,11967] or questions_ok[3] in [11965,11966,11967]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11968,11969,11970] and (questions_ok[2] in [11968,11969,11970] or questions_ok[3] in [11968,11969,11970]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11971,11972,11973] and (questions_ok[2] in [11971,11972,11973] or questions_ok[3] in [11971,11972,11973]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11974,11975,11976] and (questions_ok[2] in [11974,11975,11976] or questions_ok[3] in [11974,11975,11976]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11977,11978,11979] and (questions_ok[2] in [11977,11978,11979] or questions_ok[3] in [11977,11978,11979]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11980,11981,11982] and (questions_ok[2] in [11980,11981,11982] or questions_ok[3] in [11980,11981,11982]):
							avoid_this_question = 1
							print("devo rifare!!")
							break
					#9cnsu
						elif questions_ok[2] in [11889,11890,11891,11892] and questions_ok[3] in [11889,11890,11891,11892]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11893,11894,11895,11896] and questions_ok[3] in [11893,11894,11895,11896]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11897,11898,11899,11900] and questions_ok[3] in [11897,11898,11899,11900]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11901,11902,11903,11904] and questions_ok[3] in [11901,11902,11903,11904]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11905,11906,11907,11908] and questions_ok[3] in [11905,11906,11907,11908]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11909,11910,11911,11912] and questions_ok[3] in [11909,11910,11911,11912]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11913,11914,11915,11916] and questions_ok[3] in [11913,11914,11915,11916]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11917,11918,11919,11920] and questions_ok[3] in [11917,11918,11919,11920]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11921,11922,11923,11924] and questions_ok[3] in [11921,11922,11923,11924]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11925,11926,11927,11928] and questions_ok[3] in [11925,11926,11927,11928]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
					#9cnco
						elif questions_ok[2] in [11929,11930,11931] and questions_ok[3] in [11929,11930,11931]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11929,11930,11931] and questions_ok[3] in [11929,11930,11931]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11935,11936,11937] and questions_ok[3] in [11935,11936,11937]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11938,11939,11940] and questions_ok[3] in [11938,11939,11940]:
							questions_ok.pop(3)
							avoid_this_question = 0
							break
					#9cnssu
						elif questions_ok[2] in [11941,11942,11943] and questions_ok[3] in [11941,11942,11943]:
							questions_ok.pop(3)		
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11944,11945,11946] and questions_ok[3] in [11944,11945,11946]:
							questions_ok.pop(3)		
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11947,11948,11949] and questions_ok[3] in [11947,11948,11949]:
							questions_ok.pop(3)		
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11950,11951,11952] and questions_ok[3] in [11950,11951,11952]:
							questions_ok.pop(3)		
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11953,11954,11955] and questions_ok[3] in [11953,11954,11955]:
							questions_ok.pop(3)		
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11956,11957,11958] and questions_ok[3] in [11956,11957,11958]:
							questions_ok.pop(3)		
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11959,11960,11961] and questions_ok[3] in [11959,11960,11961]:
							questions_ok.pop(3)		
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11962,11963,11964] and questions_ok[3] in [11962,11963,11964]:
							questions_ok.pop(3)		
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11965,11966,11967] and questions_ok[3] in [11965,11966,11967]:
							questions_ok.pop(3)		
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11968,11969,11970] and questions_ok[3] in [11968,11969,11970]:
							questions_ok.pop(3)							
							avoid_this_question = 0
							break
						elif questions_ok[2] in [11971,11972,11973] and questions_ok[3] in [11971,11972,11973]:
							questions_ok.pop(3)
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11974,11975,11976] and questions_ok[3] in [11974,11975,11976]:
							questions_ok.pop(3)
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11977,11978,11979] and questions_ok[3] in [11977,11978,11979]:
							questions_ok.pop(3)
							avoid_this_question = 0					
							break
						elif questions_ok[2] in [11980,11981,11982] and questions_ok[3] in [11980,11981,11982]:
							questions_ok.pop(3)							
							avoid_this_question = 0
							break
						else:
							avoid_this_question = 0
							go_on = 0

		db_self.curs.execute("SELECT game_id FROM Games ORDER BY game_id DESC LIMIT 1")
		game_last = db_self.curs.fetchone()[0]
		print("GAME GAME GAME LAST TROVATO È {}".format(game_last))
		#############change!!################################################### START
		print("contyrollo qui!!! ")
		print("lunghezza questions_ok!!! {}".format(len(questions_ok)))
		print("numnum_questionsnum_questions {}".format(num_questions))
		if len(questions_ok)!=num_questions:
			num_ques_to_ret = num_questions
			db_self.delete_by_id_only_db_default(game_last)
			print("ho cancellato per poi rimettere!!!!")
			db_self.change_num_questions(len(questions_ok), game_last)
			num_questions_new = len(questions_ok)
			print("ho cambiato qui!!!! CALLING CHANGE QUESTIONS!!!!S numero di questrion era maggiore di quello realmente possibile!!!")
			############change necessary time
			if num_questions_new==1 and kind_game in ['1F','2L','5K']:
				new_necessary_time = 10
			if num_questions_new==1 and kind_game in ['3S','4P']:
				new_necessary_time = 12
			if num_questions_new==1 and kind_game=='7O':
				new_necessary_time = 15
			if num_questions_new==1 and kind_game in['8Q','9C']:
				new_necessary_time = 18
			if num_questions_new==1 and kind_game=='6I':
				new_necessary_time = 20

			if num_questions_new==2 and kind_game in ['1F','2L','5K']:
				new_necessary_time = 20
			if num_questions_new==2 and kind_game in ['3S','4P']:
				new_necessary_time = 24
			if num_questions_new==2 and kind_game=='7O':
				new_necessary_time = 30
			if num_questions_new==2 and kind_game in['8Q','9C']:
				new_necessary_time = 36
			if num_questions_new==2 and kind_game=='6I':
				new_necessary_time = 40

			if num_questions_new==3 and kind_game in ['1F','2L','5K']:
				new_necessary_time = 30
			if num_questions_new==3 and kind_game in ['3S','4P']:
				new_necessary_time = 36
			if num_questions_new==3 and kind_game=='7O':
				new_necessary_time = 45
			if num_questions_new==3 and kind_game in['8Q','9C']:
				new_necessary_time = 44
			if num_questions_new==3 and kind_game=='6I':
				new_necessary_time = 60

			if num_questions_new==4 and kind_game in ['1F','2L','5K']:
				new_necessary_time = 40
			if num_questions_new==4 and kind_game in ['3S','4P']:
				new_necessary_time = 48
			if num_questions_new==4 and kind_game=='7O':
				new_necessary_time = 60
			if num_questions_new==4 and kind_game in['8Q','9C']:
				new_necessary_time = 72
			if num_questions_new==4 and kind_game=='6I':
				new_necessary_time = 80			

			db_self.execute_new_query("UPDATE Games SET necessary_time = ? WHERE token = ?",(new_necessary_time,new_id))
			mdl.modify_Games_list(num_questions_new, kind_game, type_game, difficulty, order_difficulty_questions, new_necessary_time, disposable, new_id, num_last_line_g)
		#############change!!################################################### END
		else: 
			num_ques_to_ret = 0
		values_q = []
		for q in questions_ok:
			db_self.curs.execute("SELECT value FROM Questions WHERE question_id = ?",(q,))
			va = db_self.curs.fetchone()[0]
			values_q.append(va)
		print("values_q is {} and order_difficulty_questions is {}".format(values_q,order_difficulty_questions))
		#same and casu not change anything.....per semplicità e tempo che per same dovrei controllare tutto e mettere mega while ma chissene!!!
		if order_difficulty_questions=='asc':
			print("ASC...DEVO CONTROLLARE E FARE SWAP NEL CASO")
			#bubblesort classic
			for n in range(len(values_q)-1, 0, -1):
				for i in range(n):
					if values_q[i] > values_q[i + 1]:
						# swapping data if the element is less than next element in the array
						values_q[i], values_q[i + 1] = values_q[i + 1], values_q[i]
						questions_ok[i], questions_ok[i + 1] = questions_ok[i + 1], questions_ok[i]
			print("dopo asc questions_ok is {}".format(questions_ok))
		elif order_difficulty_questions=='desc':
			print("DESC...DEVO CONTROLLARE E FARE SWAP NEL CASO")
			for n in range(len(values_q)-1, 0, -1):
				for i in range(n):
					if values_q[i] < values_q[i + 1]:
						values_q[i], values_q[i + 1] = values_q[i + 1], values_q[i]
						questions_ok[i], questions_ok[i + 1] = questions_ok[i + 1], questions_ok[i]
			print("dopo desc questions_ok is {}".format(questions_ok))
		#########################################################################
		for k in range(len(questions_ok)):
			dba.add_new_GameQuestion_table(db_self, game_last, questions_ok[k])

		return num_ques_to_ret, questions_ok
	
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> An error occurred while adding a new Game")
		print("--> Search_str add_new_Game {}".format(search_str))
		if new_del:
			print("--> 1 match))) {} ".format(index_to_replace))
			print("--> 2 match))) {} ".format(index_corrected))
		print("##############################################################################################################")

cdef add_again_game_after_order_modifications(db_self, int num_questions, str kind_game, str type_game, str difficulty, str order_difficulty_questions, int necessary_time, bint disposable, str audio_intro, str audio_exit, str old_token_id, list questions_ok):
	print("ENTRO IN add_new_Game_simple after MODIFICATIONS!!!")
	print("ENTRO IN add_new_Game_simple after MODIFICATIONS!!!")
	print("ENTRO IN add_new_Game_simple after MODIFICATIONS!!!")
	print("ENTRO IN add_new_Game_simple after MODIFICATIONS!!!")
	print("ENTRO IN add_new_Game_simple after MODIFICATIONS!!!")
	print("ENTRO IN add_new_Game_simple after MODIFICATIONS!!!")
	cdef:
		str new_id = db_self.assign_new_id_after_deletion('Games')
		str	search_str = "\t({0}, '{1}', '{2}', '{3}', '{4}'".format(num_questions, kind_game, type_game, difficulty, order_difficulty_questions)
		bint new_del = 0
	print("search str {}".format(search_str))
	try:
		print("entro nel try di add_new_game_db after delete")
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Game you want to insert already exist.')
				#raise Exception
				pass
		reload(oq)
		db_self.curs.execute('''INSERT INTO Games (num_questions, kind, type, difficulty, order_difficulty_questions, necessary_time, disposable, audio_intro, audio_exit, token) 
							VALUES (?,?,?,?,?,?,?,?,?,?)''', (num_questions, kind_game, type_game, difficulty, order_difficulty_questions, necessary_time, disposable, audio_intro, audio_exit, new_id,))
		db_self.conn.commit()
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Games")
		num_last_line_g = db_self.curs.fetchone()
		print("num_last_line_gnum_last_line_gnum_last_line_gnum_last_line_g {}".format(num_last_line_g))
		if num_last_line_g:
			num_last_line_g= num_last_line_g[0]
		else:
			num_last_line_g = 0
			print("ERRORE_GRAVISSIMOOOOOOOOOOOOOO")
		##manca la deletion da db default list!!!!
		print("old_token_id che voglio eliminare da all_id_token", old_token_id)
		print("old_token_id che voglio eliminare da all_id_token", old_token_id)
		print("old_token_id che voglio eliminare da all_id_token", old_token_id)
		print("old_token_id che voglio eliminare da all_id_token", old_token_id)
		print("old_token_id che voglio eliminare da all_id_token", old_token_id)
		print("old_token_id che voglio eliminare da all_id_token", old_token_id)
		##manca la deletion da db default list!!!!
		rm_token = "grep -v \"('{}')\" /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py > tmpfile_110 && mv tmpfile_110 /home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py".format(old_token_id)
		run(rm_token, shell = True)
		##manca la deletion da db default list!!!!
		##manca la deletion da db default list!!!!
		mdl.modify_Games_list(num_questions, kind_game, type_game, difficulty, order_difficulty_questions, necessary_time, disposable, new_id, num_last_line_g)
		
		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
			print("ho trovato questa deletions_list!!!!")
			print("deletions_list = {}".format(db_self.deletions_list))
		if db_self.deletions_list[5]!=[0]:
			#dbem.bubblesort(db_self.deletions_list[5])
			if len(db_self.deletions_list[5])==4:
				index_corrected = db_self.deletions_list[5].pop(0)
				print("index_corrected ", index_corrected)
				index_to_replace = index_corrected+4
				print("index_to_replace after " , index_to_replace)
			elif len(db_self.deletions_list[5])==3:
				index_corrected = db_self.deletions_list[5].pop(0)
				print("index_corrected ", index_corrected)
				index_to_replace = index_corrected+4
				print("index_to_replace after " , index_to_replace)
			elif len(db_self.deletions_list[5])==2:
				index_corrected = db_self.deletions_list[5].pop(0)
				print("index_corrected ", index_corrected)
				index_to_replace = index_corrected+4
				print("index_to_replace after " , index_to_replace)
			elif len(db_self.deletions_list[5])==1:
				index_corrected = db_self.deletions_list[5].pop(0)
				print("index_corrected ", index_corrected)
				index_to_replace = index_corrected+4
				print("index_to_replace after " , index_to_replace)
			
		with open(filename_pk, 'wb') as fi:
			pickle.dump(db_self.deletions_list, fi)
			
		db_self.curs.execute('''UPDATE Games SET game_id = ?  WHERE game_id = ?''', (index_corrected, index_to_replace))
		db_self.conn.commit()
		
		for k in range(len(questions_ok)):
			dba.add_new_GameQuestion_table(db_self, index_corrected, questions_ok[k])

		if not db_self.deletions_list[5]:
			print("last round!!! only 1 time!")
			db_self.deletions_list[5].append(0)
			with open(filename_pk, 'wb') as fi:
				pickle.dump(db_self.deletions_list, fi)

			db_self.curs.execute("SELECT game_id FROM Games ORDER BY game_id DESC LIMIT 1")
			game_last = db_self.curs.fetchone()[0]
			db_self.curs.execute('''UPDATE SQLITE_SEQUENCE SET seq = ?	WHERE seq = ?''', (game_last, index_to_replace))
			print("sqlite squence is updated.")
			db_self.conn.commit()
			
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("--> An error occurred while adding a new Game")
		print("--> Search_str add_new_Game {}".format(search_str))
		if new_del:
			print("--> 1 match))) {} ".format(index_to_replace))
			print("--> 2 match))) {} ".format(index_corrected))
		print("##############################################################################################################")
