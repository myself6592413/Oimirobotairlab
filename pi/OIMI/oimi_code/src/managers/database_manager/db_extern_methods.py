"""info:
    Oimi robot db_external_methods, simple function share between different modules 
    for mantainance of oim_database tables, no prediction involved here.
Notes:
    For other detailed info, look at dabatase_doc

Created by Colombo Giacomo, Politecnico di Milano 2020 """
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import linecache, math, uuid, sys
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
def get_line_first_deletion():
	read_line = linecache.getline('db_extern_methods.py', 28)
	return read_line
def get_line_second_deletion():
	read_line = linecache.getline('db_extern_methods.py', 29)
	return read_line
def get_line_selection():
    read_line = linecache.getline('db_extern_methods.py', 30)
    return read_line
def get_line_selection_2():
    read_line = linecache.getline('db_extern_methods.py', 31)
    return read_line

query_string = '''delete from ? where ! in ({})'''
query_string_2 = '''delete from ? where ! = & and $ = £'''
query_string_3 = '''select & from ? where ! in ({})'''
query_string_4 = '''select & from ? where ! = $ and @ = £'''

def convert_to_binary(filename):
    with open(filename, 'rb') as file:
      blobData = file.read()
    return blobData

def merge(l1, l2): 
    combined = []
    c1 = 0 
    c2 = 0
    n1 = l1[c1]
    n2 = l2[c2]
    len1 = len(l1)
    len2 = len(l2)
    while (c1<len1 and c2<len2):
        if n1 < n2: 
            combined.append(n1)
            c1+=1
            if (c1 < len1): 
                n1 = l1[c1]
        else: 
            combined.append(n2)
            c2+=1
            if (c2 < len2):
                n2 = l2[c2]
    combined.extend(l1[c1:])
    combined.extend(l2[c2:])
    return combined

def mergesort(mylist):
    n = len(mylist)
    if n==0:
        return mylist
    if n ==1:
        return mylist
    if n > 1:
        return merge(mergesort(mylist[:int(n/2)]), mergesort(mylist[int(n/2):]))

def isSorted(a):
    for i in range(1, len(a)):
        if a[i-1] > a[i]:
            return False
    return True

def bubblesort(a):
    while(not isSorted(a)):
        for i in range (1, len(a)):
            if a[i-1] > a[i]:
                temp = a[i-1]
                a[i-1] = a[i]
                a[i] = temp
    return a

def count_number(number):
    counter = 0
    counter_number = number
    while counter_number > 0:
        counter_number //= 10
        counter += 1
    return counter

def digit_selector(number, selected_digit, total):
    total_counter = total ##??? useless!!!
    calculated_select = total_counter - selected_digit
    number_selected = int(number / math.pow(10, calculated_select))
    while number_selected > 10:
        number_selected -= 10
    return number_selected
    
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

#####################################################################################################################
def create_unique_id():
    casual_id = uuid.uuid4().hex[:8]
    return casual_id

def create_kid_id():
    casual_id = uuid.uuid4().hex[:8]
    id_kid = ''.join(('ki', casual_id))
    return id_kid

def create_therapy_id():
    casual_id = uuid.uuid4().hex[:8]
    id_therapy = ''.join(('th', casual_id))
    return id_therapy

def create_treatment_id():
    casual_id = uuid.uuid4().hex[:8]
    id_treatment = ''.join(('tr', casual_id))
    return id_treatment

def create_session_id():
    casual_id = uuid.uuid4().hex[:8]
    id_session = ''.join(('se', casual_id))
    return id_session

def create_match_id():
    casual_id = uuid.uuid4().hex[:8]
    id_match = ''.join(('ma', casual_id))
    return id_match

def create_game_id():
    casual_id = uuid.uuid4().hex[:8]
    id_game = ''.join(('ga', casual_id))
    return id_game

def create_entertainment_id():
    casual_id = uuid.uuid4().hex[:8]
    id_entertainment = ''.join(('en', casual_id))
    return id_entertainment
#####################################################################################################################
def get_line_first_deletion_doc():
    ''' 
adsaInfo
get_line_first_deletion
    Reads the first sigle deletion query string from here at line 14
Details
    Linecache allows to get lines from a Python source optimizing with the use of the cache, 
        This is used by the traceback module to retrieve source lines for inclusion in the formatted traceback.
    getline() takes the lineno() from file.
        This function will never raise an exception — it will return '' on errors
'''    
def get_line_second_deletion_doc():
    ''' Info
            Reads the second double deletion query string from here at line 15
        Details
            Linecache allows to get lines from a Python source optimizing with the use of the cache, 
                This is used by the traceback module to retrieve source lines for inclusion in the formatted traceback.
            getline() takes the lineno() from file.
                This function will never raise an exception — it will return '' on errors
    '''
def get_line_selection_doc():
    ''' Info
            Reads the selection query string from here at line 13
        Details
            Linecache allows to get lines from a Python source optimizing with the use of the cache, 
                This is used by the traceback module to retrieve source lines for inclusion in the formatted traceback.
            getline() takes the lineno() from file.
                This function will never raise an exception — it will return '' on errors
    '''
def convert_to_binary_doc():
    ''' Info
            Converts digital data to binary format
        Details
            Creates blob data for oimi_database, reads the file for inserting real file (photo and details of a Kid)
            => read() in binary mode, returns a bytes object 
        Parameters 
            filename 
        Notes 
            Using the 'with' keyword, file will be closed automatically
            Called by 
                db_manager.pyx (automatically importing db_default_lists)
                modify_Audios_list()
                modify_Kids_list()
                add_new_Kid_table()
    '''    
def merge_doc(): 
    ''' Info
            Merge two list in input 
        Details
            Using extend methods is faster than other concations method (eg. sum, chain, product ...)
            extend(): Iterates over its argument, adding each element to the list and extending the list. 
                The length of the list increases by number of elements in it’s argument.
        Parameters 
            list_1
            list_2
        Notes     
            Called by mergesort()           
    '''
def mergesort_doc():
    ''' Info
            Sort a list using mergesort algorithm [nlog(n)]
        Details
            Splits list in two parts and resursively calls mergesort in each of them
        Parameters 
            list to sort
        Notes 
            Called by ??? what in db_manager???
    '''
def isSorted_doc():
    ''' Info
            Check is a list is sorted 
        Parameters 
            list
        Notes
            Called by bubblesort()
    '''    
def bubblesort_doc():
    ''' Info
            Sort a list using mergesort algorithm [n^2]
        Details
            Sort a list until is_sorted is true 
        Parameters 
            list to sort
        Notes 
            Called by ??? what in db_manager???
    '''    
def count_number_doc():
    ''' Info
            Counts digits of a given number 
        Details
            Uses floor division
        Parameters 
            number 
        Notes 
            Useful for calling digit_selector
    '''
def digit_selector_doc(): #ok giusto!!! per stampare su terminale
    ''' 
    --> digit_selector_method of db_extern_methods.py
    Info
        Returns the first digit of a given integer number 
    Details
        Divides the number for  10 ^ (the difference of total num of digits and digit I'm looking for)
        Subtracts 10 until the founded number is lower than 10 
    Parameters 
        number, digit I'm looking for, total number of digits 
    Notes 
        Called by delete_from_id()
    '''
    print(digit_selector_doc.__doc__)
def replace_all_doc():
    ''' Info
            Swaps multiple character of a given string, according to given dictionary of replacements.
        Details
            Calls py replace
        Parameters 
            text, dic
        Notes 
            Called bu Alter_Kids_table()
    '''

def db_extern_methods_doc():
    print(get_line_first_deletion_doc.__doc__)
    print(get_line_second_deletion_doc.__doc__)
    print(get_line_selection_doc.__doc__)
    print(convert_to_binary_doc.__doc__)
    print(merge_doc.__doc__)
    print(mergesort_doc.__doc__)
    print(isSorted_doc.__doc__)
    print(bubblesort_doc.__doc__)
    print(count_number_doc.__doc__)
    print(digit_selector_doc.__doc__)
    print(replace_all_doc.__doc__)


#if __name__ == '__main__':
#    globals()[sys.argv[1]]()
