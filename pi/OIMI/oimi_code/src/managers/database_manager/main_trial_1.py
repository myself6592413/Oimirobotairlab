import sys
# ===============================================================================================================================
#  Imports:
# ===============================================================================================================================
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
import database_manager as db
import audio_blob_conversion as abc
from subprocess import Popen, run
import oimi_queries as oq
import db_default_lists as dbdl
# ===============================================================================================================================
#  Main:
# ===============================================================================================================================
if __name__ == '__main__':
	#abc.blob_creation()
	# now the costructor is changed !!! with argument adjust_dates yes, no
	oimidb = db.DatabaseManager()
	with db.DatabaseManager(): #no!!!!! metto with oimidb: !!!!!! non chiamo due volte!!!
		oimidb.build_Audios()
		oimidb.build_Therapies()
		oimidb.build_Targets()
		oimidb.build_Treatments()
		oimidb.build_TreatmentTarget()
		oimidb.build_Class_free()
		oimidb.build_Entertainments()
		oimidb.start_free_games_trigger()
		oimidb.place_entertainments()

		oimidb.build_TherapyTreatment()
		oimidb.build_TreatmentEntertainment()
		oimidb.build_Impositions()
		oimidb.build_Needs()
		oimidb.build_Sessions()
		
		oimidb.build_TreatmentSession()
		oimidb.build_TreatmentPastime()
		
	
		oimidb.build_Kids()
		oimidb.build_Allidtokens()
		oimidb.build_OimiSettings()
		oimidb.build_Kind_games()
		oimidb.build_Type_games()
		
		oimidb.build_Games()
		oimidb.build_GameRules()
		oimidb.start_pastimes_trigger()
		oimidb.start_pastimes_trigger_2()
		oimidb.add_new_TreatmentEntertainment(80000, 50005)
		oimidb.add_new_TreatmentSession(80000, 20008)
		oimidb.refresh_Treatments_Pastimes_table()

		oimidb.build_Substages()
		oimidb.build_Prompt_Substages()
		oimidb.build_Reward_Substages()
		oimidb.build_Mood_Substages()
		oimidb.build_Reaction_Substages()
		oimidb.build_Children_Substages()
		oimidb.build_Stages()
		oimidb.build_prompt_Stages()
		oimidb.build_reward_Stages()
		oimidb.build_mood_Stages()
		oimidb.build_reaction_Stages()

		#oimidb.build_KidSession()
		
		#GIUSTO!!!!
		#oimidb.create_the_entire_database_from_scratch()
		#GIUSTO!!!!
		"""
		oimidb.build_Kids()
		oimidb.build_Allidtokens()
		oimidb.build_OimiSettings()
		oimidb.build_Achievements()
		oimidb.build_Needs()
		oimidb.build_Impositions()
		oimidb.build_Symptoms()
		oimidb.build_Patches()
		oimidb.start_patches_trigger()
		oimidb.place_patches_into_Patches()

		oimidb.build_Kind_games()
		oimidb.build_Type_games()
		oimidb.build_Audios()
		oimidb.build_Questions()
		oimidb.insert_in_questions()
		oimidb.build_Scenarios()
		oimidb.insert_in_scenarios()
		oimidb.build_ScenarioQuestion()
		oimidb.populate_scenarios_questions()
		oimidb.build_Games()		
		oimidb.build_Matches()
		oimidb.build_Sessions()
		oimidb.build_SessionMatch()
		oimidb.build_MatchGame()
		oimidb.build_GameQuestion()
		oimidb.build_QuestionPatch()
		oimidb.place_data_into_Questions_Patches()
		
		oimidb.build_ScenarioType()
		oimidb.build_ScenarioKind()
		oimidb.build_Scenario4PSUQuestion()
		oimidb.build_Scenario4PCOQuestion()
		oimidb.populate_scenarios_kinds()
		oimidb.populate_scenarios_types()

		oimidb.build_KidSession() #inside create kidmatch kidgame kidquestion
		oimidb.build_KidImposition()
		
		oimidb.build_KidAchievement()
		oimidb.build_KidSymptom()
		oimidb.build_KidNeed()
		oimidb.build_NeedQuestion()
		oimidb.build_ImpositionQuestion()
		
		oimidb.build_SessionImposition()
		oimidb.build_SessionNeed()

		#oimidb.create_Full_played_recap()
		oimidb.create_Full_played_recap_giga()
		
		oimidb.construct_ScoresTotalQuestion()
		oimidb.construct_ScoresTotalKindOfGame()
		oimidb.construct_ScoresTotalTypeOfGame()
		oimidb.construct_ScoresTotalSession()
		oimidb.construct_ScoresTotalMatch()
		oimidb.construct_ScoresTotalGame()
		###########new!!!
		oimidb.change_all_advisable_times()
		"""
		#FINE GIUSTO!!!
		#oimidb.execute_a_query("DROP TABLE IF EXISTS Matches_Games")
		#oimidb.build_MatchGame()
		#oimidb.execute_a_query("DROP TABLE IF EXISTS Sessions_Matches")
		#oimidb.build_SessionMatch()
		#oimidb.execute_a_query("DROP TABLE IF EXISTS Questions_Patches")
		#oimidb.build_QuestionPatch()

		#oimidb.generic_commit()
		#oimidb.execute_a_query(oq.sql_bridge_maga)
		#oimidb.curs.executemany('''INSERT INTO Matches_Games (match_id, game_id) VALUES (?,?)''', dbdl.all_matches_games)
		#oimidb.generic_commit()

		#oimidb.execute_a_query("DELETE FROM Matches WHERE match_id = 3053")
		#oimidb.generic_commit()
		"""
		every_question = [[[1011, 1309], [11959, 11957]], [[3033], [11938]]]
		all_values_questions_tmp = []
		val_lis = []
		vall = []
		for qq in range(len(every_question)):
			print("qq ", every_question[qq])
			for qq1 in range(len(every_question[qq])):
				print("qq1 ", every_question[qq][qq1])
				for qqq in range(len(every_question[qq][qq1])):
					print("qqq ===> ", every_question[qq][qq1][qqq])
					val_sql = "SELECT value FROM Questions where question_id=?"
					v_q = oimidb.execute_new_query(val_sql,(every_question[qq][qq1][qqq]))
					print("v_q is ", v_q)
					val_lis.append(v_q[0])
					print("val_lis = ", val_lis)
				vall.append(val_lis)
				print("vall ", vall)
				val_lis = []
			all_values_questions_tmp.append(vall)
			vall = []
		print("allvallist")
		print(all_values_questions_tmp)
		"""


		#a1 = 'Kind 1F done'
		#a2 = 'Kind 2L done'
		#a3 = 'Kind 3S done'
		#id_child = 1001
		##achi_kid = oimidb.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? ORDER BY kid_id ASC LIMIT 18",(id_child))
		#achi_kid = oimidb.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?) ",(id_child,a1,a2,a3))
		##achi_kid = oimidb.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?) ",(id_child,achi1,achi2))
		#print(achi_kid)	
		

		
		############################################################################################################################################################
		############################################################################################################################################################
		############################################################################################################################################################
		############################################################################################################################################################
		############################################################################################################################################################
		############################################################################################################################################################
		############################################################################################################################################################
		#oimidb.change_all_desiderable_times()
		
		##??? non funziona dovrei settare tutti i tipi tabella !!oimidb.execute_a_query("UPDATE Full_played_summary SET time_of_session=cast(time_of_session AS INTEGER)")

		#id_to_remove = 2061
		#oimidb.execute_new_query('''DELETE FROM Games WHERE disposable = 1 AND game_id IN (
		#	SELECT Games.game_id FROM Games JOIN (Sessions_Matches JOIN Matches_Games USING (match_id)) USING (game_id)
		#	WHERE session_id = ?)''',(id_to_remove,))
		#oimidb.generic_commit()


		#oimidb.execute_a_query("""DELETE FROM Full_played_recap_giga WHERE rowid NOT IN (SELECT min(rowid) FROM Full_played_recap_giga GROUP BY
		#	kid_id,session_id,match_id,game_id,question_id)""")
		#oimidb.generic_commit()


		#for k,s,g in res_gk:
		#	nag,ncg,neg,nig,nag,tog,qfg,gfg,vfg= 1,2,3,4,5,6,'quan','grad', 9
		#	
		#	#oimidb.execute_param_query('''UPDATE Kids_Games SET num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, 
		#	#	time_of_game = ?, quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ? WHERE kid_id = ? AND session_id = ? AND game_id = ?''',(nag, ncg, neg, nig, nag, tog, qfg, gfg, vfg,k,s,g))
		#	oimidb.execute_param_query('''UPDATE Kids_Games SET num_answers_game = ?, num_corrects_game = ?, num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, 
		#		time_of_game = ?, quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ? WHERE kid_id IN (
		#		SELECT kid_id FROM Kids_Games WHERE kid_id = ?) AND session_id IN (SELECT session_id FROM Kids_Games WHERE session_id = ?) AND game_id IN (SELECT game_id FROM Kids_Games WHERE game_id = ?)''',(nag, ncg, neg, nig, nag, tog, qfg, gfg, vfg,k,s,g))
		#	oimidb.generic_commit()
		#		#num_answers_game = ?, 
		#		#num_corrects_game = ?, 
		#		#num_errors_game = ?, num_indecisions_game = ?, num_already_game = ?, time_of_game = ?,
		#		#quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
		#		#WHERE kid_id, session_id, game_id IN (
		#		#	SELECT Kids_Games_tmp.kid_id, Kids_Games_tmp.game_id FROM Kids_Games_tmp
		#		#	WHERE Kids_Games_tmp.kid_id = ? AND Kids_Games_tmp.sess_id = ? AND Kids_Games_tmp.game_id = ?''',(nag, ncg, neg, nig, nag, tog, qfg, gfg, vfg, k, s, g))


		#oimidb.execute_param_query("UPDATE Sessions SET disposable = 1 WHERE session_id = ?",(id_sess))
		#oimidb.execute_param_query("""UPDATE Matches SET disposable = 1 WHERE match_id IN (
		#	SELECT match_id FROM Sessions_Matches WHERE session_id = ?)""",(id_sess))
		#oimidb.execute_param_query("""UPDATE Games SET disposable = 1 WHERE game_id IN (
		#	SELECT game_id FROM Sessions_Matches JOIN Matches_Games USING (match_id) WHERE session_id = ?)""",(id_sess))
		#oimidb.generic_commit()
		#oimidb.delete_by_id(id_sess)
		
		"""
		aa = []
		matches = oimidb.execute_a_query("SELECT * FROM Matches")
		for m in matches:
			time_to_change = 0
			a = list(m)
			print(a)
			print()
			print()
			get_num_g = a[1]
			print("get_num_g ======================> ", get_num_g)
			get_num_ques = oimidb.execute_new_query("SELECT num_questions FROM Matches_Games JOIN Games USING (game_id) WHERE match_id = ?",(a[0]))
			print("get_num_q ======================> ", get_num_ques)
			get_kind_game = oimidb.execute_new_query("SELECT kind FROM Matches_Games JOIN Games USING (game_id) WHERE match_id = ?",(a[0]))
			print("get_num_g prima ======================> ", get_kind_game)
			################################################ NEW!! TENGO QUESTO
			for i in range(len(get_kind_game)):
				if get_kind_game[i] in ['1F','2L','5K']:	
					if get_num_ques[i]==1:
						time_to_change+=10
					if get_num_ques[i]==2:
						time_to_change+=20
					if get_num_ques[i]==3:
						time_to_change+=30
					if get_num_ques[i]==4:
						time_to_change+=40
				if get_kind_game[i] in ['3S','4P']:
					if get_num_ques[i]==1:
						time_to_change+=12
					if get_num_ques[i]==2:
						time_to_change+=24
					if get_num_ques[i]==3:
						time_to_change+=36
					if get_num_ques[i]==4:
						time_to_change+=48
				if get_kind_game[i]=='7O':
					if get_num_ques[i]==1:
						time_to_change+=15
					if get_num_ques[i]==2:
						time_to_change+=30
					if get_num_ques[i]==3:
						time_to_change+=55
					if get_num_ques[i]==4:
						time_to_change+=60		
				if get_kind_game[i] in ['8Q','9C']:
					if get_num_ques[i]==1:
						time_to_change+=18
					if get_num_ques[i]==2:
						time_to_change+=36
					if get_num_ques[i]==3:
						time_to_change+=54
					if get_num_ques[i]==4:
						time_to_change+=72
				if get_kind_game[i]=='6I':
					if get_num_ques[i]==1:
						time_to_change+=20
					if get_num_ques[i]==2:
						time_to_change+=40
					if get_num_ques[i]==3:
						time_to_change+=60
					if get_num_ques[i]==4:
						time_to_change+=80
				#print("partial result => ", time_to_change)
			
			print("time_to_change ???? ",time_to_change)
			print("time_to_change ???? ",time_to_change)
			print("leggo questi dati all interno di modify_with_update_single_match_changing_time ")
			print(a)
			print("##########")
			print(a[0])
			print(a[1])
			print(a[2])
			print(a[3])
			print(a[4])
			print(a[5])
			print(a[6])
			print(a[7])
			print(a[8])
			print(a[9])
			print(a[10])
			print(a[11])
			print(a[12])
			print("##########")
			###########nb 7 e 8 inverted in file!!!
			line_to_mod = "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, '{}'),".format(a[1],a[2],a[3],a[4],a[5],a[6],a[8],a[7],a[9],a[12])
			new_line_ok = "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, '{}'),".format(a[1],a[2],a[3],a[4],a[5],a[6],a[8], time_to_change, a[9],a[12])
			print("TESTO DA modify_with_update_in_lines!!! ==> ",line_to_mod)
			cosaquando = "s/$(printf '\t'){}/$(printf '\t'){}/".format(line_to_mod,new_line_ok)
			command_ins = 'sed -i "{}"'.format(cosaquando)
			print("command instruction ===>> ", command_ins)
			cmd_args = '/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py'
			#cmd_args = '/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py'
			def_command = "{} {}".format(command_ins,cmd_args)
			print("def_command =============================================>", def_command)
			run(def_command, shell=True)
			"""
