""" ### database_controller_populate 
per quando serve populare una tabella dopo la sua creazione e non in contemporanea, quando ci sono dei trigger involti, 
prima la tabella (e.g., patches) va creata in controller_build e poi questo viene chiamato)
### """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import csv
import os 
import sys
from importlib import reload
from typing import Optional
import oimi_queries as oq
import db_default_lists as dbdl
import db_default_lists_2 as dbdl2
import db_default_lists_3 as dbdl3
import db_default_lists_4 as dbdl4
cimport database_controller_populate
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Place
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef place_patches_into_Patches_table(db_self):
	db_self.curs.execute("drop trigger if exists complete_patches_info_trigger")
	db_self.start_patches_trigger()
	#db_self.enable_update_tot_game_errors()
	#db_self.conn.commit()

	cdef int param = 6000
	all_patches = dbdl.all_patches
	patch_one = all_patches.pop(0)
	first_element = patch_one[0]
	first_color = patch_one[1]
	first_genre = patch_one[2]
	db_self.curs.execute('''
		INSERT INTO Patches (element, color, genre) VALUES (?,?,?)''', (first_element, first_color, first_genre))
	db_self.curs.execute('''
		UPDATE SQLITE_SEQUENCE SET seq = {} WHERE name =\'Patches\' '''.format(param))
	db_self.curs.execute('''
		UPDATE Patches SET patch_id = 6000 WHERE patch_id = 1''')
	db_self.curs.executemany('''
		INSERT INTO Patches (element, color, genre) VALUES (?,?,?)''', all_patches)
	db_self.conn.commit()
	print("Patches added")

cdef place_entertainments_into_table(db_self):
	db_self.curs.execute("drop trigger if exists complete_free_category_trigger")
	db_self.start_free_games_trigger()
	all_entertainments = dbdl.all_entertainments
	enter_one = all_entertainments.pop(0)
	cdef:
		int param = 50000
		#char * first_creation_way
		str first_creation_way = enter_one[0]
		str first_status = enter_one[1]
		str first_activity = enter_one[2]
		str first_guidelines = enter_one[3]
		str first_notes = enter_one[4]
		int first_disposable = enter_one[5]
		str first_token = enter_one[6]
	#first_creation_way =  <char*> enter_one[0].encode()
	db_self.curs.execute('''
		INSERT INTO Entertainments (creation_way, status, class_of_game, specific_guidelines, notes, disposable, token) VALUES (?,?,?,?,?,?,?)''', 
			(first_creation_way, first_status, first_activity, first_guidelines, first_notes, first_disposable, first_token))
	db_self.conn.commit()
	db_self.curs.execute('''
		UPDATE Entertainments SET entertainment_id = 50000 WHERE entertainment_id = 1''')
	db_self.curs.executemany('''
		INSERT INTO Entertainments (creation_way, status, class_of_game, specific_guidelines, notes, disposable, token) VALUES (?,?,?,?,?,?,?)''', all_entertainments)
	db_self.conn.commit()

	cdef str query_audio_rules = "UPDATE Entertainments SET audio_rules = '{}'".format('audio_rules')
	cdef str query_audio_intro = "UPDATE Entertainments SET audio_intro = '{}'".format('audio_1')
	cdef str query_audio_exit = "UPDATE Entertainments SET audio_exit = '{}'".format('audio_2')
	db_self.curs.execute(query_audio_rules)
	db_self.curs.execute(query_audio_intro)
	db_self.curs.execute(query_audio_exit)
	db_self.conn.commit()
	print("Entertainments added")

cdef place_data_into_Questions_Patches_table(db_self):
	db_self.curs.execute(oq.sql_fill_questions_patches_1)
	db_self.curs.execute(oq.sql_fill_questions_patches_2)
	db_self.curs.execute(oq.sql_fill_questions_patches_3)
	db_self.curs.execute(oq.sql_fill_questions_patches_4)
	db_self.conn.commit()
	print("Questions_Patches added")
	reload(oq)
	db_self.curs.execute(oq.sql_temp_qupa)
	db_self.curs.execute('''DROP TABLE Questions_Patches''')
	db_self.curs.execute('''ALTER TABLE temp_qupa RENAME TO Questions_Patches''')
	print("Questions_Patches sorted")
	db_self.conn.commit()
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Populate
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef populate_scenarios_types_table(db_self):
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/scen_type_populate.csv','r') as fil:
		dr = csv.DictReader(fil)
		to_db = [(i['scenario_group'], i['type']) for i in dr]
		db_self.curs.executemany("INSERT INTO Scenarios_Types (scenario_group, type) VALUES (?,?);", to_db)
		db_self.conn.commit()
	print("Scenarios_Types table is ready")			

cdef populate_scenarios_kinds_table(db_self):
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/scen_kind_populate.csv','r') as fil:
		dr = csv.DictReader(fil)
		to_db = [(i['scenario_group'], i['kind']) for i in dr]
		db_self.curs.executemany("INSERT INTO Scenarios_Kinds (scenario_group, kind) VALUES (?,?);", to_db)
		db_self.conn.commit()
	print("Scenarios_Kinds table is ready")	

cdef populate_scenarios_questions_table(db_self):
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/list_scen_ques_1.csv','r') as fin_1:
		dr_1 = csv.DictReader(fin_1)
		to_db_1 = [(i['scenario_group'], i['audio_id']) for i in dr_1]
		db_self.curs.executemany("INSERT INTO Scenarios_Questions (scenario_group, audio_id) VALUES (?,?);", to_db_1)
		db_self.conn.commit()
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/list_scen_ques_2.csv','r') as fin_2:
		dr_2 = csv.DictReader(fin_2)
		to_db_2 = [(i['scenario_group'], i['audio_id']) for i in dr_2]
		db_self.curs.executemany("INSERT INTO Scenarios_Questions (scenario_group, audio_id) VALUES (?,?);", to_db_2)
		db_self.conn.commit()
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/list_scen_ques_3.csv','r') as fin_3:
		dr_3 = csv.DictReader(fin_3)
		to_db_3 = [(i['scenario_group'], i['audio_id']) for i in dr_3]
		db_self.curs.executemany("INSERT INTO Scenarios_Questions (scenario_group, audio_id) VALUES (?,?);", to_db_3)
		db_self.conn.commit()
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/list_scen_ques_4.csv','r') as fin_4:
		dr_4 = csv.DictReader(fin_4)
		to_db_4 = [(i['scenario_group'], i['audio_id']) for i in dr_4]
		db_self.curs.executemany("INSERT INTO Scenarios_Questions (scenario_group, audio_id) VALUES (?,?);", to_db_4)
		db_self.conn.commit()
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/list_scen_ques_5.csv','r') as fin_5:
		dr_5 = csv.DictReader(fin_5)
		to_db_5 = [(i['scenario_group'], i['audio_id']) for i in dr_5]
		db_self.curs.executemany("INSERT INTO Scenarios_Questions (scenario_group, audio_id) VALUES (?,?);", to_db_5)
		db_self.conn.commit()
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/list_scen_ques_6.csv','r') as fin_6:
		dr_6 = csv.DictReader(fin_6)
		to_db_6 = [(i['scenario_group'], i['audio_id']) for i in dr_6]
		db_self.curs.executemany("INSERT INTO Scenarios_Questions (scenario_group, audio_id) VALUES (?,?);", to_db_6)
		db_self.conn.commit()
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/list_scen_ques_7.csv','r') as fin_7:
		dr_7 = csv.DictReader(fin_7)
		to_db_7 = [(i['scenario_group'], i['audio_id']) for i in dr_7]
		db_self.curs.executemany("INSERT INTO Scenarios_Questions (scenario_group, audio_id) VALUES (?,?);", to_db_7)
		db_self.conn.commit()
	print("Scenario_Questions table is ready")
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Insert
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef insert_in_scenarios_table(db_self):
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/def_list_all_scenarios.csv','r') as fin:
		dr = csv.DictReader(fin)
		to_db = [(i['scenario_id'], i['scenario_group'], i['patch1'], i['patch2'], i['patch3'], i['patch4']) for i in dr]
		db_self.curs.executemany("INSERT INTO Scenarios (scenario_id, scenario_group, patch1, patch2, patch3, patch4) VALUES (?,?,?,?,?,?);", to_db)
		db_self.conn.commit()
		print("Scenarios table is ready")

cdef insert_in_questions_table(db_self):
	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/def_list_all_questions.csv','r') as fin:
		dr = csv.DictReader(fin)
		to_db = [(i['kind'], i['type'], i['audio'], i['value'], i['description']) for i in dr]
		db_self.curs.executemany("INSERT INTO Questions (kind, type, audio, value, description) VALUES (?,?,?,?,?);", to_db)
		db_self.conn.commit()
		print("Questions table is ready")
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Fill
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef fill_recap_table(db_self, kid):
	try:
		print("Inserting new line in Full_played_Recap_giga")
		n_query = "SELECT session_id FROM Kids_Sessions where kid_id = ? ORDER BY session_id DESC LIMIT 1" 
		sess_id = db_self.execute_param_query(n_query,(kid))
		print("sess_id before ", sess_id)
		last_sess_id = sess_id[0]
		print("last_sess_id!!!! ", last_sess_id)
		par = (kid, last_sess_id)
		while True:
			print("ricomincio firo in fill_recap_table")
			db_self.execute_param_query(oq.sql_insertion_table_results,par)
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(session_id) FROM Full_played_recap_giga WHERE session_id = ?",(last_sess_id,))
			res = db_self.curs.fetchone()
			print("il conto recap di quelle trovate é ::: ", res)
			if res:
				res = res[0]
				if res>=1:
					break
		#mantain order
		db_self.curs.execute("CREATE TABLE giga_tmp AS SELECT * FROM Full_played_recap_giga ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE IF EXISTS Full_played_recap_giga")
		db_self.curs.execute("CREATE TABLE Full_played_recap_giga AS SELECT * FROM giga_tmp")
		db_self.curs.execute('DROP TABLE IF EXISTS giga_tmp')
		db_self.conn.commit()

		#remove duplicates to be sure
		db_self.execute_a_query("""DELETE FROM Full_played_recap_giga WHERE rowid NOT IN (SELECT min(rowid) FROM Full_played_recap_giga GROUP BY kid_id,session_id,match_id,game_id,question_id)""")
		db_self.generic_commit()

	except Exception as e:
		print("Error! Something wrong happened during filling recap table")
		print("Namely:")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
	
cdef fill_summary_table(db_self, kid):
	try:
		print("Inserting new row into Full_played_summary")
		n_query = "SELECT Sessions.session_id FROM Sessions ORDER BY session_id DESC LIMIT 1" 
		sess_id = db_self.execute_a_query(n_query)
		print("sess_id before ", sess_id)
		last_sess_id = sess_id[0]
		print("last_sess_id!!!! ", last_sess_id)
		par = (kid, last_sess_id)
		while True:
			print("ricomincio firo in fill_summary_table")
			db_self.execute_param_query(oq.sql_insertion_summary,par)
			db_self.conn.commit()
			db_self.curs.execute("SELECT COUNT(session_id) FROM Full_played_recap_giga WHERE session_id = ?",(last_sess_id,))
			res = db_self.curs.fetchone()
			print("il conto summary di quelle trovate é ::: ", res)
			if res:
				res = res[0]
				if res>=1:
					break
		#mantain order
		db_self.curs.execute("CREATE TABLE summary_tmp AS SELECT * FROM Full_played_summary ORDER BY kid_id ASC")
		db_self.curs.execute("DROP TABLE IF EXISTS Full_played_summary")
		db_self.curs.execute("CREATE TABLE Full_played_summary AS SELECT * FROM summary_tmp")
		db_self.curs.execute('DROP TABLE IF EXISTS summary_tmp')
		db_self.conn.commit()

		#remove duplicates to be sure
		db_self.execute_a_query("""DELETE FROM Full_played_summary WHERE rowid NOT IN (SELECT min(rowid) FROM Full_played_summary GROUP BY kid_id,session_id,match_id,game_id,question_id)""")
		db_self.generic_commit()

	except Exception as e:
		print("Error! Something wrong happened during filling summary table")
		print("Namely:")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
