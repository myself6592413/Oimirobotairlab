# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Add new row, modifies db_default_lists not oimi_queries
# ----------------------------------------------------------------------------------------------------------------------------------------------------------	
cdef add_new_Kid_table(db_self, str name, str surname, int age, str level, str image_path, str details_path)
		
cdef add_new_constrained_Session_table(db_self, int kid_id, int mandatory_impositions, int mandatory_needs, customized)
cdef add_new_constrained_Session_table_kid_not_given(db_self, customized)
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Adding
# ----------------------------------------------------------------------------------------------------------------------------------------------------------			
cdef add_new_Therapy_table(db_self, int kid_id, int num_of_treatments, str canonical_intervention)
cdef add_new_Treatment_table(db_self, int kid_id, str target_goal_1, str target_goal_2, str target_goal_3, int num_of_pastimes, int num_of_sessions, int num_of_entertainments)
cdef add_new_Entertainment_table(db_self, int kid_id, str class_of_game, str specific_guidelines)
cdef add_new_Session_table(db_self, int kid_id, int mandatory_impositions, int mandatory_needs)
cdef add_new_Match_table(db_self, str difficulty, int num_of_games, str variety, str order_difficulty_games, str order_quantity_questions, str criteria_arrangement, int advisable_time, scenario, bint disposable)
cdef add_new_Game_table(db_self, int num_questions, str kind_game, str type_game, str difficulty, str order_difficulty_questions, 
	int necessary_time, bint disposable, scenario, list list_types, list questions_already_choosen_this_match)
cdef add_new_Question_table(db_self, str kind_game, str type_game, str audio_id, int value, str description)
cdef add_new_SessionMatch_table(db_self, int session_id, int match_id)
cdef add_new_MatchGame_table(db_self, int match_id, int game_id)
cdef add_new_GameQuestion_table(db_self, int game_id, int question_id)

cdef add_new_KidTherapy_table(db_self, int kid_id, int therapy_id)
cdef add_new_KidTreatment_table(db_self, int kid_id, int treatment_id)
cdef add_new_KidSession_table(db_self, int kid_id, int session_id)
cdef add_new_KidEntertainment_table(db_self, int kid_id, int entertainment_id)

cdef add_new_KidSession_table(db_self, int kid_id, int session_id)
cdef add_new_KidImposition_table(db_self, int kid_id, str imposition_name)
cdef add_new_KidNeed_table(db_self, int kid_id, int need_id)
cdef add_new_KidAchievement_table(db_self, int kid_id, str achievement_name)
cdef add_new_KidSymptom_table(db_self, int kid_id, str symptom_name)
cdef add_new_KidIssue_table(db_self, int kid_id, str issue_name)
cdef add_new_KidComorbidity_table(db_self, int kid_id, str comorbidity_name)
cdef add_new_KidStrength_table(db_self, int kid_id, str strength_name)

cdef add_new_SessionEnforcement_table(db_self, int session_id, str enforcement_name)
cdef add_new_SessionImposition_table(db_self, int session_id, str imposition_name)
cdef add_new_SessionNeed_table(db_self, int session_id, int need_id)
cdef add_the_new_patch(db_self, new_subject, new_color)


##################################################################################################
cdef add_new_bridge_TreatmentEntertainment(db_self, int treatment_id, int entertainment_id)
cdef add_new_bridge_TreatmentSession(db_self, int treatment_id, int session_id)
cdef add_new_bridge_TreatmentPastime(db_self, int treatment_id, int pastime_id)



cdef add_to_OldKidNeed_table(db_self, int kid_id, int need_id)
cdef add_to_OldKidSymptom_table(db_self, int kid_id, str old_symptom_name)
cdef add_to_OldKidIssue_table(db_self, int kid_id, str old_issue_name)
cdef add_to_OldKidComorbidity_table(db_self, int kid_id, str old_comorbidity_name)
cdef add_to_OldKidStrength_table(db_self, int kid_id, str old_strenght_name)