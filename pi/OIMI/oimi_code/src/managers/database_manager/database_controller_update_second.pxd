# ----------------------------------------------------------------------------------------------------------------------------------------
# Include #all in once
# ----------------------------------------------------------------------------------------------------------------------------------------
cdef include_all_feeds_in_once(db_self, int kid_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds)
cdef include_all_results_in_once(db_self, int kid_id, float time_sess, list questions_results, list matches_results, list games_results)

cdef include_all_feeds_in_once_given_session(db_self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds)
cdef include_all_results_in_once_given_session(db_self, int kid_id, int session_id, float time_sess, list questions_results, list matches_results, list games_results)
