""" Oimi robot database_manager, fix prediction important errors 
	called by add_prediction and add_customized_prediction 
	
	Notes:
		For other detailed info, look at dabatase_doc

	Created by Colombo Giacomo, Politecnico di Milano 2020 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import random
from typing import Optional
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef str last_minute_security_change_kind(str this_kind, str this_g_diff, str this_level):
	print("sono entrato in last minute change kind")
	cdef bint kchan = 0 
	if this_kind=='6I' and this_level in ['beginner','elementary']:
		ki_possibilities = ['1F','2L','3S','4P','5K','7O','8Q','9C']
		this_kind = random.choice(ki_possibilities)
		kchan = 1
	elif this_kind=='6I' and this_g_diff not in ['Hard_1','Hard_2']:
		ki_possibilities = ['1F','2L','3S','4P','5K','7O','8Q','9C']
		this_kind = random.choice(ki_possibilities)
		kchan = 1
	if this_kind in ['4P','6I','8Q','9C'] and this_level=='beginner' and this_g_diff in ['Easy_1','Easy_2','Normal_1','Normal_2','Medium_1','Medium_2']:
		ki_possibilities = ['1F','2L','3S','5K','7O']
		this_kind = random.choice(ki_possibilities)
		kchan = 1
	if this_kind in ['4P','6I'] and this_level=='beginner' and this_g_diff in ['Hard_1','Hard_2']:
		ki_possibilities = ['1F','2L','3S','5K','7O','8Q','9C']
		this_kind = random.choice(ki_possibilities)
		kchan = 1
	if this_kind in ['4P','6I','8Q','9C'] and this_level=='elementary' and this_g_diff in ['Easy_1','Easy_2','Normal_1','Normal_2','Medium_1','Medium_2']:
		ki_possibilities = ['1F','2L','3S','5K','7O']
		this_kind = random.choice(ki_possibilities)
		kchan = 1
	if this_kind in ['6I','8Q'] and this_level=='intermediate' and this_g_diff in ['Easy_1','Easy_2','Normal_1','Normal_2']:
		ki_possibilities = ['1F','2L','3S','4P','5K','7O','9C']
		this_kind = random.choice(ki_possibilities)
		kchan = 1
	if kchan==1:
		print("HO CAMBIATO IL kind CON SECURITY KIND PREDICTION!!!!!!!!!!!!!!")
	return this_kind

cdef str last_minute_security_change_type(str this_type, str this_kind, str this_g_diff, str this_level):
	print("sono entrato in last minute change type")
	cdef:
		bint ktchan = 0
		bint tchan = 0
	######first check diff type relation is correct
	if this_kind=='1F' and '1F' not in this_type:
		ty_possibilities = ['1FCACO','1FCACOA','1FCOD','1FCODT','1FCOS','1FGCA','1FSUCOD','1FSUCODT','1FSUCOS','1FSUD','1FSUDT','1FSUS']
		this_type = random.choice(ty_possibilities)
		ktchan = 1	
	if this_kind=='2L' and '2L' not in this_type:
		ty_possibilities = ['2LDBOB','2LDBOC','2LDSU','2LDSU2','2LDSUOB','2LDSUOC','2LST','2LSU','2LTWO']
		this_type = random.choice(ty_possibilities)
		ktchan = 1
	if this_kind=='3S' and '3S' not in this_type:
		ty_possibilities = ['3SCO','3SSU']
		this_type = random.choice(ty_possibilities)
		ktchan = 1
	if this_kind=='4P' and '4P' not in this_type:
		ty_possibilities = ['4PCO','4PSU']
		this_type = random.choice(ty_possibilities)
		ktchan = 1
	if this_kind=='5K' and '5K' not in this_type:
		ty_possibilities = ['5KD','5KS']
		this_type = random.choice(ty_possibilities)
		ktchan = 1
	if this_kind=='6I' and this_type!='6I':
		this_type = '6I'
		ktchan = 1
	if this_kind=='7O' and '7O' not in this_type:
		ty_possibilities = ['7OCO2','7OCO3','7OCO4','7OL2','7OL3','7OSU2','7OSU3','7OSU4']
		this_type = random.choice(ty_possibilities)
		ktchan = 1
	if this_kind=='8Q' and '8Q' not in this_type:
		ty_possibilities = ['8QTCODB1','8QTCODB2','8QTCODC1','8QTCODC2','8QTCOSB1','8QTCOSB2','8QTCOSC1','8QTCOSC2','8QTKNDC','8QTKNSB','8QTKNSC','8QTPRSB','8QTPRSC',
			'8QTSUDB1','8QTSUDB2','8QTSUDC1','8QTSUDC2','8QTSUSB1','8QTSUSB2','8QTSUSC1','8QTSUSC2','8QTSUSCOC1']
		this_type = random.choice(ty_possibilities)
		ktchan = 1
	if this_kind=='9C' and '9C' not in this_type:
		ty_possibilities = ['9CNCO','9CNSPA','9CNSPX','9CNSSU','9CNSU']
		this_type = random.choice(ty_possibilities)
		ktchan = 1
	######second check on diff type question when game = kind binding is ok		
	if this_kind=='1F' and '1F' in this_type:
		if this_type in ['1FCODT','1FSUCODT','1FSUDT'] and this_level=='beginner' and this_g_diff in ['Easy_1']:
			ty_possibilities = ['1FCACO','1FCACOA','1FCOD','1FCOS','1FGCA','1FSUCOD','1FSUCOS','1FSUD','1FSUS']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in ['1FCODT','1FSUCODT','1FSUDT'] and this_level=='elementary' and this_g_diff in ['Easy_1','Easy_2','Normal_1','Normal_2','Medium_1']:
			ty_possibilities = ['1FCACO','1FCACOA','1FCOD','1FCOS','1FGCA','1FSUCOD','1FSUCOS','1FSUD','1FSUS']
			this_type = random.choice(ty_possibilities)
			tchan = 1
	if this_kind=='2L' and '2L' in this_type:
		if this_type in ['2LDBOB','2LDBOC','2LDSU2','2LDSUOB','2LDSUOC'] and this_level=='beginner' and this_g_diff in ['Easy_1','Easy_2','Normal_1','Normal_2','Medium_1']:
			ty_possibilities = ['2LDSU','2LST','2LSU']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in ['2LDBOB','2LDBOC','2LDSU2','2LDSUOB','2LDSUOC'] and this_level=='beginner' and this_g_diff in ['Medium_2','Hard_1','Hard_2']:
			ty_possibilities = ['2LDSU','2LST','2LSU','2LTWO']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in ['2LDBOB','2LDBOC','2LDSU2','2LDSUOB','2LDSUOC'] and this_level=='elementary' and this_g_diff in ['Easy_1','Easy_2','Normal_1','Normal_2','Medium_1']:
			ty_possibilities = ['2LDSU','2LST','2LSU','2LTWO']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in ['2LDBOB','2LDBOC','2LDSU2','2LDSUOB','2LDSUOC'] and this_level=='intermediate' and this_g_diff in ['Easy_1','Easy_2','Normal_1']:
			ty_possibilities = ['2LDSU','2LST','2LSU','2LTWO']
			this_type = random.choice(ty_possibilities)
			tchan = 1
	if this_kind=='5K' and '5K' in this_type:
		if this_type=='5KD' and this_level in ['beginner','elementary']:
			ty_possibilities = ['5KS']
			this_type = random.choice(ty_possibilities)
			tchan = 1
	if this_kind=='7O' and '7O' in this_type:
		if this_type in['7OCO3','7OCO4','7OL3','7OSU3','7OSU4'] and this_level=='beginner':
			ty_possibilities = ['7OCO2','7OL2','7OSU2']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in['7OCO4','7OL3','7OSU3','7OSU4'] and this_level=='elementary' and this_g_diff in ['Easy_1','Easy_2','Normal_1','Normal_2']:
			ty_possibilities = ['7OCO2','7OCO3','7OL2','7OSU2']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in['7OCO4','7OSU4'] and this_level=='elementary' and this_g_diff in ['Medium_1','Medium_2','Hard_1','Hard_2']:
			ty_possibilities = ['7OCO4','7OSU4']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in['7OCO4','7OSU3','7OSU4'] and this_level=='intermediate' and this_g_diff in ['Easy_1','Easy_2']:
			ty_possibilities = ['7OCO2','7OCO3','7OL2','7OL3','7OSU2']
			this_type = random.choice(ty_possibilities)
			tchan = 1
	if this_kind=='8Q' and '8Q' in this_type:
		if this_type in ['8QTSUDB1','8QTSUDB2','8QTKNDC','8QTKNSB','8QTKNSC','8QTPRSB','8QTPRSC','8QTSUSC1','8QTSUSC2','8QTSUSCOC1'] and this_level=='beginner':
			ty_possibilities = ['8QTCODB1','8QTCODB2','8QTCODC1','8QTCODC2','8QTCOSB1','8QTCOSB2','8QTCOSC1','8QTCOSC2','8QTSUDC1','8QTSUDC2','8QTSUSB1','8QTSUSB2']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in ['8QTSUDB2','8QTKNDC','8QTKNSB','8QTKNSC','8QTPRSB','8QTPRSC','8QTSUSCOC1'] and this_level=='elementary':
			ty_possibilities = ['8QTCODB1','8QTCODB2','8QTCODC1','8QTCODC2','8QTCOSB1','8QTCOSB2','8QTCOSC1','8QTCOSC2','8QTSUDB1','8QTSUDC1','8QTSUDC2','8QTSUSB1','8QTSUSB2']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in ['8QTPRSB','8QTPRSC','8QTSUSCOC1'] and this_level=='intermediate' and this_g_diff in ['Easy_1','Easy_2','Normal_1','Normal_2']:
			ty_possibilities = ['8QTCODB1','8QTCODB2','8QTCODC1','8QTCODC2','8QTCOSB1','8QTCOSB2','8QTCOSC1','8QTCOSC2','8QTKNDC','8QTKNSB','8QTKNSC',
				'8QTSUDB1','8QTSUDB2','8QTSUDC1','8QTSUDC2','8QTSUSB1','8QTSUSB2','8QTSUSC1','8QTSUSC2']
			this_type = random.choice(ty_possibilities)
			tchan = 1
	if this_kind=='9C' and '9C' in this_type:
		if this_type in ['9CNCO','9CNSPX','9CNSU'] and this_level=='beginner':
			ty_possibilities = ['9CNSPA','9CNSSU']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in ['9CNSPX'] and this_level=='elementary':
			ty_possibilities = ['9CNCO','9CNSPA','9CNSSU','9CNSU']
			this_type = random.choice(ty_possibilities)
			tchan = 1
		elif this_type in ['9CNSPX'] and this_level=='intermediate' and this_g_diff in ['Easy_1','Easy_2','Normal_1']:
			ty_possibilities = ['9CNCO','9CNSPA','9CNSSU','9CNSU']
			this_type = random.choice(ty_possibilities)
			tchan = 1
	if ktchan==1:
		print("HO CAMBIATO IL TIPO/kind legame legame CON SECURITY PREDICTION!!!!!!!!!!!!!!")
	if tchan==1:
		print("HO CAMBIATO solo solo IL TIPO IN SEGRETO CON SECURITY PREDICTION!!!!!!!!!!!!!!")
	return this_type


