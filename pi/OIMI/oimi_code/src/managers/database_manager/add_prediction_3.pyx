""" Oimi robot database_manager,
	Notes:
		For other detailed info, look at dabatase_doc

	Created by Colombo Giacomo, Politecnico di Milano 2020 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
import mmap
import random
import pickle
import pandas as pd
import db_extern_methods as dbem
import add_prediction_4 as adp4
cimport modify_default_lists as mdl
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef int check_need_mand(db_self):
	current_need = db_self.kid_id
	db_self.curs.execute("SELECT need_id FROM Kids_Needs WHERE db_self.kid_id = ?", (db_self.kid_id,))
	get_need = db_self.curs.fetchall()
	some_needs = [item for sublist in get_need for item in sublist] #flatten list of list in list
	while current_need==db_self.kid_id:
		current_need = random.choice(some_needs)
	print("THE CURRENT NEED IS {}".format(current_need))
	return current_need
cdef str check_impo_mand(db_self):
	current_impo = db_self.kid_id
	db_self.curs.execute("SELECT imposition_name FROM Kids_Impositions WHERE db_self.kid_id = ?", (db_self.kid_id,))
	get_impo = db_self.curs.fetchall()
	some_impositions = [item for sublist in get_impo for item in sublist] #flatten list of list in list
	while current_impo==db_self.kid_id:
		current_impo = random.choice(some_impositions)
	print("THE CURRENT impo IS {}".format(current_impo))
	return current_impo
cdef check_symps(db_self):
	db_self.curs.execute("SELECT symptom_name FROM Kids_Symptoms WHERE db_self.kid_id = ?", (db_self.kid_id,))
	get_syms = db_self.curs.fetchall()
	if len(get_syms)==0:
		kids_syms = []
	if len(get_syms)==1:
		kids_syms = get_syms[0]
	if len(get_syms)>1:
		kids_syms = [item for sublist in get_syms for item in sublist]
	return kids_syms

cpdef add_new_Session_with_kid(db_self):
	cdef:
		int new_del = 0
		str search_str = ''
		str current_impo = ''
		int current_need = db_self.kid_id
	print("SIAMO IN 3333 PRED =?= ADD_NEW SESSION!!!T")
	db_self.disposable = 0
	db_self.status = 'to_do'
	try:
		if db_self.mandatory_need==1:
			current_need = check_need_mand(db_self)
		if db_self.mandatory_imposition==1:
			current_impo = check_impo_mand(db_self)
		kids_syms = check_symps(db_self)
		
		search_str = adp4.extract_pred_info(db_self)

		print("{} ==> is the search_str search_strsearch_str ".format(search_str))
		find_me = search_str.encode()
		print("find_me is {}".format(find_me))
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Session you want to insert already exist???')
				print("OCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIOOCIO")
				#raise Exception
				#controllo inutile a sto punto!!!!
				pass
			db_self.new_id_token = db_self.assign_new_id('Sessions')
			db_self.curs.execute('''INSERT INTO Sessions (num_of_matches, complexity, status, order_difficulty_matches, order_quantity_games, 
				mandatory_impositions, mandatory_needs, extra_time_tolerance, movements_allowed, body_enabled, 
				rules_explication, stress_flag, frequency_exhortations, repeat_question, repeat_intro, creation_way, desirable_time, disposable, token)
				VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', (db_self.num_of_matches, db_self.complexity, db_self.status, db_self.order_diff_matches, db_self.order_quan_games, 
				db_self.mandatory_impositions, db_self.mandatory_needs, db_self.extra_time_tolerance, db_self.movements_allowed, db_self.body_enabled, db_self.rules_explication, 
				db_self.stress_flag, db_self.frequency_exhortations, db_self.repeat_question, db_self.repeat_intro, db_self.desirable_time, db_self.creation_way, db_self.disposable, db_self.new_id_token,))
			db_self.conn.commit()
			db_self.curs.execute( "SELECT COUNT(rowid) FROM Sessions")
			num_last_line_s = db_self.curs.fetchone()
			if num_last_line_s:
				print("num_last_line_s {}".format(num_last_line_s))
				num_last_line_s = num_last_line_s[0]
			else:
				print("num_last_line_s!!!! errrore ma tanto non ci entro mai!!!")
			mdl.modify_Sessions_list(db_self.num_of_matches, db_self.complexity, db_self.status, db_self.order_diff_matches, db_self.order_quan_games, db_self.mandatory_impositions, db_self.mandatory_needs, 
				db_self.extra_time_tolerance, db_self.movements_allowed, db_self.body_enabled, db_self.rules_explication, db_self.stress_flag, db_self.frequency_exhortations, db_self.repeat_question, db_self.repeat_intro, 
				db_self.desirable_time, db_self.creation_way, db_self.disposable, db_self.new_id_token, num_last_line_s)

			db_self.curs.execute("UPDATE Sessions SET date_time = DATETIME('now','localtime') WHERE date_time isNull")
			query_audio_intro = "UPDATE Sessions SET audio_intro = '{}'".format('audio_1')
			query_audio_exit = "UPDATE Sessions SET audio_exit = '{}'".format('audio_2')
			db_self.curs.execute(query_audio_intro)
			db_self.curs.execute(query_audio_exit)
			db_self.conn.commit()
			db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
			last_sess = db_self.curs.fetchall()[0][0]
			print("LAST SESS LAST SESS = = > {}".format(last_sess))
			print("db_self.kid_id {}".format(db_self.kid_id))
			print("current_impo {}".format(current_impo))
			print("current_need {}".format(current_need))
			##add also in db and into default_list
			db_self.add_new_KidSession_table(db_self.kid_id, last_sess) #fa dentro tutto lui 
			if current_impo!='':
				print("entro dove non dovrei impo")
				db_self.add_new_SessionImposition_table(last_sess, current_impo)
			if current_need!=db_self.kid_id:
				print("entro dove non dovrei need")
				db_self.add_new_SessionNeed_table(last_sess, current_need)
				
			filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/pickle_deletions_list.pk'
			with open(filename_pk, 'rb') as fi:
				deletions_list = pickle.load(fi)
				print("ho trovato questa deletions_list!!!!")
				print("deletions_list = {}".format(deletions_list))
			print(deletions_list)
			if deletions_list[0]!=[0]:
				print("EMTRP ONMD DEÒETAS ÒOST")
				("VOLGIO andareeeeeeeeeeeeeeeeeeeeeeeeeeee")
				new_del = 1
				dbem.bubblesort(deletions_list[1])
				db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Sessions\'")
				index_to_replace = db_self.curs.fetchone()[1] #1 is correct!!
				index_corrected = deletions_list[1].pop(0)
				db_self.curs.execute('''UPDATE Sessions SET session_id = ?	WHERE session_id = ?''', (index_corrected, index_to_replace,))
				if not deletions_list[1]:
					deletions_list[1].append(0)
					with open(filename_pk, 'wb') as fi:
						pickle.dump(db_self.deletions_list, fi)
				db_self.conn.commit()
			else:
				("NO NON VOLGIO ENTRAREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE")
				index_corrected = 99
				index_to_replace = 99
		print(" FINE!!!!! The new Session is now added.")
		#db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
		#sess_last = db_self.curs.fetchall()_tab_00
	except Exception as e:
		print("--> An error occurred while adding a new Session with kid")
		print("--> Search_str add_new_Session_with_kid {}".format(search_str))
		print("Namely:")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
		if new_del:
			print("--> 1 match))) {} ".format(index_to_replace))
			print("--> 2 match))) {} ".format(index_corrected))

cdef conclude_final_session_creation(db_self):
	print("ADD_PRE_3 --> conclude_final_session_creation START")
	add_new_Session_with_kid(db_self)
	adp4.complete_prediction_info(db_self)
