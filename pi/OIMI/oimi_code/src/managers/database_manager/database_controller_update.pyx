"""Info:
	Oimi robot database controller of additions of new data into tables, where all existing constraints must be revised or new constraints must be added.
	when both db_default_lists and oimi_queries files must be update.
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
import re
import mmap
from importlib import reload
from subprocess import run 
from typing import Optional
import traceback
import oimi_queries as oq
import db_default_lists as dbdl
import db_default_audio_lists as dbdal
import db_extern_methods as dbem
import audio_blob_conversion as abc
cimport modify_default_lists as mdl
cimport database_controller_build as dcb
cimport database_controller_add_into as dca
cimport database_controller_update_second as dcu2
#cimport database_controller_update
# ----------------------------------------------------------------------------------------------------------------------------------------
# Update 
# ----------------------------------------------------------------------------------------------------------------------------------------
cdef update_Needs_constraints(db_self, str focus_contr, str need_contr, str position): 
	cdef:
		int j
		int N
		str update_with
		str update_with_1
	try:
		update_with = "\t\t'{0}',\n".format(focus_contr)
		update_with_1 = "\t\t'{}',\n".format(need_contr)
		
		db_self.curs.execute("SELECT focus FROM Needs WHERE focus = ?", (focus_contr,))
		focus_result = db_self.curs.fetchone()
		if not focus_result:
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
				lines = fp.readlines()
			old_lines = lines[:]					
			reached_focus = [i for i, val in enumerate(lines) if val=="	CHECK(	focus IN (\n"]	  
			N = len(reached_focus)
			if N:
				for j in range(N):
					reached_focus[j] = reached_focus[j] + 1
					lines.insert(reached_focus[-j], update_with)
			else:
				raise Exception
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
				fp.write(''.join(lines))		
		
		if position=='match':		 
			db_self.curs.execute("SELECT match_scope FROM Needs WHERE match_scope = ?", (need_contr,))
			q_result = db_self.curs.fetchone()
		elif position=='game':
			db_self.curs.execute("SELECT game_scope FROM Needs WHERE game_scope = ?", (need_contr,))
			q_result = db_self.curs.fetchone()
		elif position=='question':
			db_self.curs.execute("SELECT question_scope FROM Needs WHERE question_scope = ?", (need_contr,))
			q_result = db_self.curs.fetchone()
		else:
			raise Exception()
		if not q_result:
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
				lines = fp.readlines()
			if position=='match':
				reached = [i for i, val in enumerate(lines) if val=="	CHECK(	match_scope IN (\n"]
			elif position=='game':
				reached = [i for i, val in enumerate(lines) if val=="	CHECK(	game_scope IN (\n"]
			elif position=='question':
				reached = [i for i, val in enumerate(lines) if val=="	CHECK(	question_scope IN (\n"]
			if len(reached):
				N = len(reached)
				for j in range(N):
					reached[j] = reached[j] + 1
					lines.insert(reached[-j], update_with_1)
				with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
					fp.write(''.join(lines))
			else:
				raise Exception	 
		print("Needs constraints succesfully updated")
		return old_lines
	except Exception as e:
		print("--> An error occurred during Needs' contraints update")
		print("--> reached is {} ".format(reached[j]))
		print("--> reached length is {}".format(len(reached)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)			

cdef update_Kind_constraints(db_self, str new_kind, str new_details): 
	cdef:
		int j
		int N
		str update_with
		str update_with_1
	try:
		update_with = "\t\t'{0}',\n".format(new_kind)
		update_with_1 = "\t\t'{}',\n".format(new_details)
		
		db_self.curs.execute("SELECT kind_of_game FROM Kind_games WHERE kind_of_game = ?", (new_kind,))
		kind_found = db_self.curs.fetchone()
		if not kind_found:
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
				lines = fp.readlines()
			old_lines = lines[:]				
			reached_kind = [i for i, val in enumerate(lines) if val=="	CHECK( kind_of_game IN (\n"]
			N = len(reached_kind)
			if N:
				for j in range(N):
					reached_kind[j] = reached_kind[j] + 1
					lines.insert(reached_kind[-j], update_with)
			else:
				raise Exception
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("Kind of games name succesfully updated")
		db_self.curs.execute("SELECT details_kind FROM Kind_games WHERE details_kind = ?", (new_details,))
		details_found = db_self.curs.fetchone()
		if not details_found:
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
				lines = fp.readlines()
			old_lines = lines[:]
			reached_det = [i for i, val in enumerate(lines) if val=="	CHECK( details_kind IN (\n"]
			N = len(reached_det)
			if N:
				for j in range(N):
					reached_det[j] = reached_det[j] + 1
					lines.insert(reached_det[-j], update_with_1)
			else:
				raise Exception
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			print("Kind of games details constraints succesfully updated")
		return old_lines
	except Exception as e:
		print("--> An error occurred during Kind_games' contraints update")
		print("--> reached details is {} ".format(reached_det[j]))
		print("--> reached details length is {}".format(len(reached_det)))
		print("--> reached kind is {} ".format(reached_kind[j]))
		print("--> reached kind length is {}".format(len(reached_kind)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
	
cdef update_Patches_genre_1(db_self, str new_genre): 
	cdef:
		int j
		int N
		str update_with
	try:
		update_with_1 = "\t\t'{}',\n".format(new_genre)

		db_self.curs.execute("SELECT genre FROM Patches WHERE genre = ?", (new_genre,))
		query_result = db_self.curs.fetchone()
		if not query_result:
			reload(oq)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
				lines = fp.readlines()
			old_lines = lines[:]
			reached_elem = [i for i, val in enumerate(lines) if val=="	CHECK( genre IN (\n"]
			#print("locs len is {}".format(len(reached_elem)))
			if len(reached_elem):	
				N = len(reached_elem)
				for j in range(N):
					reached_elem[j] = reached_elem[j] +1
					lines.insert(reached_elem[-j], update_with_1)
				with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
					last_pos = fp.tell()		
					fp.write(''.join(lines))
			else:
				raise Exception
	except Exception as e:
		print("Something went wrong in update patches genre 1")
		print("reached_1 len is {}".format(len(reached_elem)))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef update_Patches_genre_2(db_self, str new_subject, str new_genre): 
	cdef:
		int j
		int N
		str update_with_1
		str update_with_2
	to_canc = "		WHERE element IN ("
	txt_to_remove_1 = "sed -i '/{}*/d' oimi_queries.py".format(to_canc)
	run(txt_to_remove_1, shell = True)
	reload(oq)
	try:	
		db_self.curs.execute("SELECT DISTINCT element FROM Patches")
		elements_now = db_self.curs.fetchall()
		elems = '\', \''.join([str(x) for t in elements_now for x in t])
		elems_def = "'{}'".format(elems)
		update_with_1 = "\t\t\t\tWHEN element = '{}' THEN '{}'\n".format(new_subject,new_genre)
		update_with_2 = "\t\tWHERE element IN ({}, '{}');\n".format(elems_def, new_subject)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
			lines = fp.readlines()
		old_lines = lines[:]
		reached_1 = [i for i, val in enumerate(lines) if val=="			SET genre = CASE\n"]
		if len(reached_1):
			N = len(reached_1)
			for j in range(N):
				reached_1[j] = reached_1[j] +1
				lines.insert(reached_1[-j], update_with_1)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
				fp.write(''.join(lines))
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
				lines = fp.readlines()
		else:
			raise Exception
		reached_2 = [i for i, val in enumerate(lines) if val=="END\n"]
		if len(reached_2):
			N = len(reached_2)
			for j in range(N):
				reached_2[j] = reached_2[j] +1 
				lines.insert(reached_2[-j], update_with_2)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception
		print("Patches genres trigger succesfully updated")
		return old_lines			
	except Exception as e:
		print("Something went wrong in genre update")
		print("reached_1 len is {}".format(len(reached_1)))
		print("reached_2 len is {}".format(len(reached_2)))
		print("reached_1 {} ".format(reached_1[j]))
		print("reached_2 {} ".format(reached_2[j]))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef update_Patches_constraints(db_self, str new_subject, str new_color):
	cdef:
		int j
		int N
		str update_with_1
		str update_with_2
		bint add_genre = 0
	try:
		update_with_1 = "\t\t'{}',\n".format(new_subject)
		update_with_2 = "\t\t'{}',\n".format(new_color)

		db_self.curs.execute("SELECT element FROM Patches WHERE element = ?", (new_subject,))
		query_result = db_self.curs.fetchone()
		print("aa {}".format(query_result))
		if not query_result:
			reload(oq)
			add_genre = 1
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
				lines = fp.readlines()
			old_lines = lines[:]
			reached_elem = [i for i, val in enumerate(lines) if val=="	CHECK( element IN (\n"]
			print("locs len is {}".format(len(reached_elem)))
			if len(reached_elem):	
				N = len(reached_elem)
				for j in range(N):
					reached_elem[j] = reached_elem[j] +1 
					lines.insert(reached_elem[-j], update_with_1)
				with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
					last_pos = fp.tell()						
					fp.write(''.join(lines))
			else:
				raise Exception

		db_self.curs.execute("SELECT color FROM Patches WHERE color = ?", (new_color,))
		query_result = db_self.curs.fetchone()
		if not query_result:
			reload(oq)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
				lines = fp.readlines()
			reached_col = [i for i, val in enumerate(lines) if val=="	CHECK( color IN (\n"]		 
			if len(reached_col):
				N = len(reached_col)
				for j in range(N):
					reached_col[j] = reached_col[j] +1 
					lines.insert(reached_col[-j], update_with_2)
				with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
					last_pos = fp.tell()						
					fp.write(''.join(lines))
			else:
				raise Exception
		print("Patches constraints succesfully updated")
		return add_genre, last_pos, old_lines
	except Exception as e: 
		print("--> Error in updating Patches constraints")
		print("--> reached_elem {} ".format(reached_elem[j]))
		print("--> reached_col {} ".format(reached_col[j]))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef update_constraint_of(db_self, str constraint_str, str table):
	cdef:
		int j
		int N
		str update_with = "\t\t'{0}',\n".format(constraint_str)
	try:
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
			lines = fp.readlines()
		old_lines = lines[:]
		if table=='Achievements':
			reached = [i for i, val in enumerate(lines) if val=="	CHECK( achievement_name IN (\n"]
		elif table=='Symptoms':
			reached = [i for i, val in enumerate(lines) if val=="	CHECK( symptom_name IN (\n"]
		elif table=='Impositions':
			reached = [i for i, val in enumerate(lines) if val=="	CHECK( imposition_name IN (\n"]
		elif table=='Type_games':
			reached = [i for i, val in enumerate(lines) if val=="	CHECK( type_of_game IN (\n"]
		if len(reached):
			N = len(reached)
			for j in range(N):
				reached[j] = reached[j] + 1
				lines.insert(reached[-j], update_with)
			with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
				fp.write(''.join(lines))
		else:
			raise Exception	
		print("{} constraints succesfully updated".format(table))
		return old_lines
	except Exception as e:
		print("Some errors occurs in updating constraint of {}".format(table))
		print("reached len is {}".format(len(reached)))
		print("reached {} ".format(reached[j]))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
# ----------------------------------------------------------------------------------------------------------------------------------------
# Insert
# ----------------------------------------------------------------------------------------------------------------------------------------
cdef insert_new_Audios_table(db_self, str constraint_audio, str audio_path, int is_a_substitution): 
	cdef:
		int begin
		int end
		str find_me_str = "\t\t{},\n".format(constraint_audio)
	try:
		print("Inserting new Audio ...")
		print("find_me_str=={}".format(find_me_str))
		find_me = find_me_str.encode()
		print("find_me=={}".format(find_me))

		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The value you want to insert already exist!!')
				raise Exception
		db_self.conn.commit()
		db_self.curs.execute("SELECT COUNT(*) FROM Audios")
		index_for_lastest = db_self.curs.fetchone()[0]
		mdl.modify_Audios_list(constraint_audio, audio_path, index_for_lastest)

		if(is_a_substitution):
			db_self.curs.execute("SELECT rowid, kind, type, value, description FROM Questions WHERE audio_id isNull")
			select_res = db_self.curs.fetchall()
			row_n = select_res[0][0]
			kind_q = select_res[0][1]
			type_q = select_res[0][2]
			value_q = select_res[0][3]
			desc_q = select_res[0][4]

		db_self.curs.execute('DROP TABLE IF EXISTS Audios')
		reload(oq)
		reload(dbdl)
		reload(abc)
		abc.blob_creation()
		db_self.curs.execute(oq.sql_create_audios)
		db_self.curs.executemany('''INSERT INTO Audios (audio_id, audio_file) VALUES (?,?)''', dbdal.all_audios)
		db_self.conn.commit()
		if(is_a_substitution):
			#the question already exists ! but audio change ..position matters here ! put in right place in dbdl
			try:
				mdl.modify_Questions_list(kind_q, type_q, constraint_audio, value_q, desc_q, row_n) 
				db_self.build_Questions()
				db_self.build_ResultsTotalQuestion()
				db_self.build_ResultsTotalKindOfGame()		   
				db_self.build_ResultsTotalKindOfGame_new()
				db_self.build_QuestionPatch()

			except Exception as e:
				print("--> Error. IS a substitution, but in this case cannot insert new Audio")
				print("--> Namely")
				print(e)
				exc_type, exc_obj, exc_tb = sys.exc_info()
				fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
				print(exc_type, fname, exc_tb.tb_lineno)			

		print("Ok, audio added to oimi database.")
	except Exception as e:
		print("--> Error. Cannot insert new Audio")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)			

cdef insert_new_Achievements_table(db_self, str constraint_str):
	cdef:
		int begin
		int end
		str search_str = "\t'{0}',".format(constraint_str)
	try:
		print("Inserting new Achievement ...")
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Achievement you want to insert already exist!!')
				raise Exception
		old_lines_queries = db_self.update_constraint_of(constraint_str, 'Achievements')
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
			lines = fp.readlines()

		for index, line in enumerate(lines):
			if line.startswith('sql_create_achievements'):
				begin = index
			if line.startswith('sql_create_needs'):
				end = index-1
		chunk = lines[begin:end]
		chunk.pop(0)
		chunk = map(lambda s: s.strip(), chunk) 
		joined_chunk = ' '.join(chunk)
		str_for_query = re.sub('\'\'\'', '', joined_chunk) 
	
		db_self.curs.execute("SELECT sql FROM sqlite_master WHERE type='table' AND name='Achievements'")
		schema_0 = db_self.curs.fetchone()
		schema = schema_0[0]
		schema_copy = schema.replace("Achievements", "Achi_tmp")
		sql_00 = 'DROP TABLE IF EXISTS Achi_tmp'
		sql_1 = 'INSERT INTO Achi_tmp SELECT * FROM Achievements'
		sql_2 = 'DROP TABLE Achievements'
		sql_4 = 'INSERT INTO Achievements SELECT * FROM Achi_tmp'
		sql_5 = 'DROP TABLE Achi_tmp'
		
		db_self.curs.execute(sql_00)
		db_self.curs.execute(schema_copy)
		db_self.curs.execute(sql_1)
		db_self.curs.execute(sql_2)
		db_self.conn.commit()
		db_self.curs.execute(str_for_query)
		db_self.curs.execute(sql_4)
		db_self.curs.execute(sql_5)
		db_self.conn.commit()
		db_self.curs.execute("INSERT INTO Achievements (achievement_name) VALUES (?)", (constraint_str,))
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Achievements")
		num_lines = db_self.curs.fetchone()
		if num_lines:
			num_lines = num_lines[0]
		mdl.modify_default_names(constraint_str, 'Achievements', num_lines)
		db_self.conn.commit()
	except Exception as e:
		print("--> An error occurs while adding a new achievement")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)			
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
			fp.write(''.join(old_lines_queries))

cdef insert_new_Needs_table(db_self, str constraint_str_1, str constraint_str_2, str scope): 
	cdef:
		int begin
		int end
		str search_str_1 = "('{0}', '{1}', 'Invalid', 'Invalid'),".format(constraint_str_1, constraint_str_2)
		str search_str_2 = "('{0}', 'Invalid', '{1}', 'Invalid'),".format(constraint_str_1, constraint_str_2)
		str search_str_3 = "('{0}', 'Invalid', 'Invalid', '{1}'),".format(constraint_str_1, constraint_str_2)
	try:
		find_me_1 = search_str_1.encode()
		find_me_2 = search_str_2.encode()
		find_me_3 = search_str_3.encode()
		print("Inserting new Need...")
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_2.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me_1) != -1 or s.find(find_me_2) != -1 or s.find(find_me_3) != -1:
				print('--> The need you want to insert already exist!!')
				raise Exception
		old_lines_queries = db_self.update_Needs_constraints(constraint_str_1, constraint_str_2, scope)
		
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
			lines = fp.readlines()
		for index, line in enumerate(lines):
			if line.startswith('sql_create_needs'):
				begin = index
			if line.startswith('sql_create_symptoms'):
				end = index-1 
		chunk = lines[begin:end]
		chunk.pop(0) 
		chunk = map(lambda s: s.strip(), chunk)
		joined_chunk = ' '.join(chunk)
		str_for_query = re.sub('\'\'\'', '', joined_chunk)

		db_self.curs.execute("SELECT sql FROM sqlite_master WHERE type='table' AND name='Needs'")
		schema_0 = db_self.curs.fetchone()
		schema = schema_0[0]
		schema_copy = schema.replace("Needs", "Needs_tmp")
		sql_00 = 'DROP TABLE IF EXISTS Needs_tmp'
		sql_1 = 'INSERT INTO Needs_tmp SELECT * FROM Needs'
		sql_2 = 'DROP TABLE Needs'
		sql_4 = 'INSERT INTO Needs SELECT * FROM Needs_tmp'
		sql_5 = 'DROP TABLE Needs_tmp'
		reload(oq)
		reload(dbdl)
		db_self.curs.execute(sql_00)
		db_self.curs.execute(schema_copy)
		db_self.curs.execute(sql_1)
		db_self.curs.execute(sql_2)
		db_self.conn.commit()
		db_self.curs.execute(str_for_query)
		db_self.curs.execute(sql_4)
		db_self.curs.execute(sql_5)
		db_self.conn.commit()

		db_self.enable_needs_trigger_1()
		db_self.enable_needs_trigger_2()
		db_self.enable_needs_trigger_3()
		
		reload(oq)
		reload(dbdl)
		if scope=='match':
			db_self.curs.execute("INSERT INTO Needs (focus, match_scope, game_scope, question_scope) VALUES (?,?,?,?)", (constraint_str_1, constraint_str_2, None, None,))
		elif scope=='game':
			db_self.curs.execute("INSERT INTO Needs (focus, match_scope, game_scope, question_scope) VALUES (?,?,?,?)", (constraint_str_1, None, constraint_str_2, None,))
		elif scope=='question':
			db_self.curs.execute("INSERT INTO Needs (focus, match_scope, game_scope, question_scope) VALUES (?,?,?,?)", (constraint_str_1, None, None, constraint_str_2,))
		else:
			raise Exception
		mdl.modify_Needs_list(constraint_str_1, constraint_str_2, scope)
		db_self.conn.commit()

		if db_self.deletions_list[5]!=[0]:
			dbem.bubblesort(db_self.deletions_list[5])
			db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Needs\'")
			index_to_replace = db_self.curs.fetchone()[1] 
			index_corrected = db_self.deletions_list[5].pop(0)
			db_self.curs.execute('''UPDATE Needs SET need_id = ?  WHERE need_id = ?''', (index_corrected,index_to_replace))
			if not db_self.deletions_list[5]:
				db_self.deletions_list[5].append(0)
			db_self.conn.commit()
	except Exception as e:
		print("--> An error occurs in Needs insertion")
		print("--> search_str_1 {}".format(search_str_1))
		print("--> search_str_2 {}".format(search_str_2))
		print("--> search_str_3 {}".format(search_str_3))
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
			fp.write(''.join(old_lines_queries))	
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef insert_new_Symptoms_table(db_self, str constraint_str):
	cdef:
		int begin
		int end
		str search_str = "\t'{0}',".format(constraint_str)
	try:
		print("Inserting new Symptom...")
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Symptom you want to insert already exist!!')
				raise Exception
		old_lines_queries = db_self.update_constraint_of(constraint_str, 'Symptoms')
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
			lines = fp.readlines()
		for index, line in enumerate(lines):
			if line.startswith('sql_create_symptoms'):
				begin = index
			if line.startswith('sql_create_impositions'):
				end = index-1 
		chunk = lines[begin:end]
		chunk.pop(0) 
		chunk = map(lambda s: s.strip(), chunk) 
		joined_chunk = ' '.join(chunk)
		str_for_query = re.sub('\'\'\'', '', joined_chunk) 
			
		db_self.curs.execute('DROP TABLE IF EXISTS Symptoms')
		db_self.curs.execute(str_for_query)

		for item in dbdl.all_symptoms:
			db_self.curs.execute("INSERT INTO Symptoms VALUES (?)", (item, ))
		db_self.curs.execute("INSERT INTO Symptoms (symptom_name) VALUES (?)", (constraint_str,))
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Symptoms")
		num_lines = db_self.curs.fetchone()
		if num_lines:
			num_lines = num_lines[0]
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Symptoms")
		num_lines = db_self.curs.fetchone()
		if num_lines:
			num_lines = num_lines[0]
		mdl.modify_default_names(constraint_str, 'Symptoms', num_lines)
		db_self.conn.commit()
	except Exception as e:
		print("--> Error. Cannot insert new Symptom")
		print("--> search_str is {}".format(search_str))
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
			fp.write(''.join(old_lines_queries))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef insert_new_Impositions_table(db_self, str constraint_str): 
	cdef:
		int begin
		int end
		str search_str = "\t'{0}',".format(constraint_str)
	try:
		print("Inserting new Imposition ...")
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists_2.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Imposition you want to insert already exist!!')
				raise Exception
		old_lines_queries = db_self.update_constraint_of(constraint_str, 'Impositions')
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
			lines = fp.readlines()
		for index, line in enumerate(lines):
			if line.startswith('sql_create_impositions'):
				begin = index
			if line.startswith('sql_create_patches'):
				end = index-1 
		chunk = lines[begin:end]
		chunk.pop(0) 
		chunk = map(lambda s: s.strip(), chunk) 
		joined_chunk = ' '.join(chunk)
		str_for_query = re.sub('\'\'\'', '', joined_chunk)
			
		db_self.curs.execute('DROP TABLE Impositions')
		db_self.curs.execute(str_for_query)
		for item in dbdl.all_impositions:
			db_self.curs.execute("INSERT INTO Impositions VALUES (?)", (item, ))
		db_self.curs.execute("INSERT INTO Impositions (imposition_name) VALUES (?)", (constraint_str,)) 
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Impositions")
		num_lines = db_self.curs.fetchone()
		if num_lines:
			num_lines = num_lines[0]
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Impositions")
		num_lines = db_self.curs.fetchone()
		if num_lines:
			num_lines = num_lines[0]
		mdl.modify_default_names(constraint_str, 'Impositions', num_lines)
		db_self.conn.commit()
		print("Ok, Imposition added to oimi database.")						
	except Exception as e:
		print("--> Error. Cannot insert new Imposition")
		print("--> search_str is {}".format(search_str))
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
			fp.write(''.join(old_lines_queries))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef insert_new_Kind_of_games_table(db_self, str constraint_kind, str details_kind): 
	cdef:
		int begin
		int end
		str search_str = "\t'{0}',".format(constraint_kind)
	try:
		print("Inserting new Kind of game ...")
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Kind_games you want to insert already exist!!')
				raise Exception
		old_lines_queries = db_self.update_Kind_constraints(constraint_kind, details_kind)
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
			lines = fp.readlines()
		for index, line in enumerate(lines):
			if line.startswith('sql_create_kind_games'):
				begin = index
			if line.startswith('sql_create_type_games'):
				end = index-1 
		chunk = lines[begin:end]

		chunk.pop(0) 
		chunk = map(lambda s: s.strip(), chunk) 
		
		joined_chunk = ' '.join(chunk)
		str_for_query = re.sub('\'\'\'', '', joined_chunk)
			
		db_self.curs.execute("SELECT sql FROM sqlite_master WHERE type ='table' AND name='Kind_games'")
		schema_0 = db_self.curs.fetchone()
		schema = schema_0[0]
		schema_copy = schema.replace("Kind_games", "Kinds_tmp")
		sql_00 = 'DROP TABLE IF EXISTS Kind_tmp'
		sql_1 = 'INSERT INTO Kinds_tmp SELECT * FROM Kind_games'
		sql_2 = 'DROP TABLE Kind_games'
		sql_4 = 'INSERT INTO Kind_games SELECT * FROM Kinds_tmp'
		sql_5 = 'DROP TABLE Kinds_tmp'
		reload(oq)
		reload(dbdl)
		db_self.curs.execute(sql_00)
		db_self.curs.execute(schema_copy)
		db_self.curs.execute(sql_1)
		db_self.curs.execute(sql_2)
		db_self.conn.commit()
		db_self.curs.execute(str_for_query)
		db_self.curs.execute(sql_4)
		db_self.curs.execute(sql_5)
		db_self.conn.commit()

		db_self.curs.execute("INSERT INTO Kind_games (kind_of_game, details_kind) VALUES (?,?)", (constraint_kind, details_kind)) 
		db_self.conn.commit()
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Kind_games")
		num_last_line_kg = db_self.curs.fetchone()[0]
		mdl.modify_Kind_Games_list(constraint_kind, details_kind,num_last_line_kg)
		print("Ok, Kind_games added to oimi database.")
	except Exception as e:
		print("--> Error. Cannot insert new Kind of game")
		print("--> search_str is {}".format(search_str))
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
			fp.write(''.join(old_lines_queries))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef insert_new_Type_of_games_table(db_self, str constraint_type, str details, str example, str kind): 
	cdef:
		int begin
		int end
		str search_str = "\t'{0}',".format(constraint_type)
	try:
		print("Inserting new Type of game ...")
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Kind_games you want to insert already exist!!')
				#raise Exception
				pass
		old_lines_queries = db_self.update_constraint_of(constraint_type, 'Type_games')
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
			lines = fp.readlines()
		for index, line in enumerate(lines):
			if line.startswith('sql_create_type_games'):
				begin = index
			if line.startswith('sql_create_audios'):
				end = index-1
		chunk = lines[begin:end]

		chunk.pop(0) 
		chunk = map(lambda s: s.strip(), chunk) 
		joined_chunk = ' '.join(chunk)
		str_for_query = re.sub('\'\'\'', '', joined_chunk)

		db_self.curs.execute("SELECT sql FROM sqlite_master WHERE type='table' AND name='Type_games'")
		schema_0 = db_self.curs.fetchone()
		schema = schema_0[0]
		schema_copy = schema.replace("Type_games", "Types_tmp")
		sql_00 = 'DROP TABLE IF EXISTS Type_tmp'
		sql_1 = 'INSERT INTO Types_tmp SELECT * FROM Type_games'
		sql_2 = 'DROP TABLE Type_games'
		sql_4 = 'INSERT INTO Type_games SELECT * FROM Types_tmp'
		sql_5 = 'DROP TABLE Types_tmp'
		reload(oq)
		reload(dbdl)
		db_self.curs.execute(sql_00)
		db_self.curs.execute(schema_copy)
		db_self.curs.execute(sql_1)
		db_self.curs.execute(sql_2)
		db_self.conn.commit()
		db_self.curs.execute(str_for_query)
		db_self.curs.execute(sql_4)
		db_self.curs.execute(sql_5)
		db_self.conn.commit()

		db_self.curs.execute("INSERT INTO Type_games (type_of_game, details_type, example, kind_game) VALUES (?,?,?,?)", (constraint_type, details, example, kind))
		db_self.conn.commit()
		db_self.curs.execute( "SELECT COUNT(rowid) FROM Type_games")
		num_last_line = db_self.curs.fetchone()[0]
		mdl.modify_Type_Games_list(constraint_type, details, example, kind, num_last_line)
		print("Ok, Type of game added to oimi database.")
	except Exception as e:
		print("--> Error. Cannot insert new Type of game")
		print("--> search_str is {}".format(search_str))
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
			fp.write(''.join(old_lines_queries))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef insert_new_Patches_table(db_self, str new_subject, str new_color, str new_genre):
	refresh_Patches_table(db_self, new_subject, new_color, new_genre)
	dca.add_the_new_patch(db_self, new_subject, new_color)
# ----------------------------------------------------------------------------------------------------------------------------------------
# Refresh
# ----------------------------------------------------------------------------------------------------------------------------------------
cdef refresh_Patches_table(db_self, str new_subject, str new_color, str new_genre):
	cdef: 
		int begin
		int end
		str search_str = "('{0}', '{1}'),".format(new_subject, new_color)
	try:
		print("Inserting new Patch ...")
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access = mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The patch you want to insert already exist!!')
				raise Exception
		add_genre, last_pos, old_lines_queries = db_self.update_Patches_constraints(new_subject, new_color)
		reload(oq)
		if add_genre:
			db_self.update_Patches_genre_1(new_genre)
			db_self.update_Patches_genre_2(new_subject, new_genre)

		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") as fp:
			lines = fp.readlines()
		for index, line in enumerate(lines):
			if line.startswith('sql_create_patches'):
				begin = index
			if line.startswith('sql_create_scenarios'):
				end = index-1 
		chunk = lines[begin:end]
		chunk.pop(0) 
		chunk = map(lambda s: s.strip(), chunk) 
		joined_chunk = ' '.join(chunk)
		str_for_query = re.sub('\'\'\'', '', joined_chunk) 
		db_self.curs.execute("SELECT sql FROM sqlite_master WHERE type='table' AND name='Patches'")
		schema_0 = db_self.curs.fetchone()
		schema = schema_0[0]
		schema_copy = schema.replace("Patches", "Patches_tmp")
		sql_00 = 'DROP TABLE IF EXISTS Patches_tmp'
		sql_1 = 'INSERT INTO Patches_tmp SELECT * FROM Patches'
		sql_2 = 'DROP TABLE Patches'
		sql_4 = 'INSERT INTO Patches SELECT * FROM Patches_tmp'
		sql_5 = 'DROP TABLE Patches_tmp'
		reload(oq)
		reload(dbdl)
		db_self.curs.execute(sql_00)
		db_self.curs.execute(schema_copy)
		db_self.curs.execute(sql_1)
		db_self.curs.execute(sql_2)
		db_self.conn.commit()
		db_self.curs.execute(str_for_query)
		db_self.curs.execute(sql_4)
		db_self.curs.execute(sql_5)
		db_self.conn.commit()
		
	except Exception as e:
		print("--> Error. Cannot insert new Patch")
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) as fp:
			fp.write(''.join(old_lines_queries))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef refresh_Treatments_Pastimes_table(db_self):
	db_self.curs.execute('DROP TABLE IF EXISTS ordered_pastimes')
	db_self.curs.execute("CREATE TABLE ordered_pastimes (treatment_id INTEGER, pastime_id INTEGER, PRIMARY KEY (treatment_id, pastime_id))")
	db_self.curs.execute("INSERT INTO ordered_pastimes (treatment_id, pastime_id) SELECT treatment_id, pastime_id FROM Treatments_Pastimes \
	ORDER BY treatment_id")
	db_self.conn.commit()
	db_self.curs.execute('DROP TABLE IF EXISTS Treatments_Pastimes')
	db_self.curs.execute('ALTER TABLE ordered_pastimes RENAME TO Treatments_Pastimes')
	db_self.conn.commit()
	print("Treatments_Pastimes created")
# ----------------------------------------------------------------------------------------------------------------------------------------
# Put #one single question updated in a row...need multiple calls
# ----------------------------------------------------------------------------------------------------------------------------------------
cpdef put_kid_questions_scores(db_self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
	str quantity_feedback_param, str grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest):
	print("NEGRITA put_kid_questions_scores")
	db_self.curs.execute('''UPDATE Kids_Questions SET num_answers_question = ?, num_corrects_question = ? , num_errors_question = ? , num_indecisions_question = ?, num_already_question = ?, time_of_question = ?, 
		quantity_feedback_question = ?, grade_feeedback_question = ?, value_feedback_question = ?
		WHERE question_id IN (
		SELECT question_id
		FROM Full_played_summary 
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?)'''
		,(num_answers_param, errors_param, corrects_param, indecisions_param, already_param, time_param, 
		quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))
	db_self.conn.commit()

	db_self.curs.execute('''UPDATE Full_played_summary SET num_answers_question = ?, num_corrects_question = ? , num_errors_question = ? , num_indecisions_question = ?, num_already_question = ?, time_of_question = ?, 
		quantity_feedback_question = ?, grade_feeedback_question = ?, value_feedback_question = ?
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? '''
		,(num_answers_param, errors_param, corrects_param, indecisions_param, already_param, time_param, 
		quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))
	db_self.conn.commit()

	db_self.curs.execute('''UPDATE Full_played_recap_giga SET num_answers_question = ?, num_corrects_question = ? , num_errors_question = ? , num_indecisions_question = ?, num_already_question = ?, time_of_question = ?, 
		quantity_feedback_question = ?, grade_feeedback_question = ?, value_feedback_question = ?
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? '''
		,(num_answers_param, errors_param, corrects_param, indecisions_param, already_param, time_param, 
		quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))
	db_self.conn.commit()
	print("Kids_Questions new value put")
	print("Full_played_recap_giga updated by database_controller_update")
	print("Full_played_summary updated by database_controller_update")

	print("Kids_Session new value put")
	dcb.construct_ResultsTotalQuestion_table(db_self)
	dcb.construct_ResultsTotalTypeOfGame_table(db_self)
	dcb.construct_ResultsTypeOfGame_new_table(db_self)
	dcb.construct_ResultsTotalKindOfGame_table(db_self)
	dcb.construct_ResultsKindOfGame_new_table(db_self)
	dcb.construct_ResultsTotalSession_table(db_self)
	dcb.construct_ResultsTotalMatch_table(db_self)
	dcb.construct_ResultsTotalGame_table(db_self)
	print("Ok, single tables scores updated succesfully")

cpdef put_kid_games_scores(db_self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
	str quantity_feedback_param, str grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest):
	print("NEGRITA put_kid_games_scores")
	db_self.curs.execute('''UPDATE Kids_Games SET num_answers_question = ?, num_corrects_question = ? , num_errors_question = ? , num_indecisions_question = ?, num_already_question = ?, time_of_question = ?,
		quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
		WHERE question_id IN (
		SELECT question_id
		FROM Full_played_summary 
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?)'''
		,(num_answers_param, errors_param, corrects_param, indecisions_param, already_param, time_param, 
		quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))
	db_self.conn.commit()

	db_self.curs.execute('''UPDATE Full_played_summary SET num_answers_game = ?, num_corrects_game = ?, num_errors_game = ? , num_indecisions_game = ?, num_already_game = ?, time_of_game = ?,
		quantity_feedback_game = ?, grade_feedback_game = ?, value_feedback_game = ?
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? '''
		,(num_answers_param, errors_param, corrects_param, indecisions_param, already_param, time_param, 
		quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))		
	db_self.conn.commit()

	db_self.curs.execute('''UPDATE Full_played_recap_giga SET num_answers_game = ?, num_corrects_game = ?, num_errors_game = ? , num_indecisions_game = ?, num_already_game = ?, time_of_game = ?,
		quantity_feedback_game = ?,  grade_feedback_game = ?, value_feedback_game = ?
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? '''
		,(num_answers_param, errors_param, corrects_param, indecisions_param, already_param, time_param, 
		quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))	
	db_self.conn.commit()
	print("Kids_Games new value put")
	print("Full_played_recap_giga updated by database_controller_update")
	print("Full_played_summary updated by database_controller_update")

	print("Kids_Session new value put")
	dcb.construct_ResultsTotalQuestion_table(db_self)
	dcb.construct_ResultsTotalTypeOfGame_table(db_self)
	dcb.construct_ResultsTypeOfGame_new_table(db_self)
	dcb.construct_ResultsTotalKindOfGame_table(db_self)
	dcb.construct_ResultsKindOfGame_new_table(db_self)
	dcb.construct_ResultsTotalSession_table(db_self)
	dcb.construct_ResultsTotalMatch_table(db_self)
	dcb.construct_ResultsTotalGame_table(db_self)
	print("Ok, single tables scores updated succesfully")

cpdef put_kid_matches_scores(db_self, int num_answers_param, int corrects_param, int errors_param, int indecisions_param, int already_param, float time_param, 
	int quantity_feedback_param, int grade_feedback_param, float value_feedback_param, int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest):
	print("NEGRITA put_kid_matches_scores")
	db_self.curs.execute('''UPDATE Kids_Matches SET num_answers_match = ?, num_corrects_match = ? , num_errors_match = ? , num_indecisions_match = ?, num_already_match = ?, time_of_match = ?,
		quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
		WHERE question_id IN (
		SELECT question_id
		FROM Full_played_summary 
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?)'''
		,(num_answers_param, errors_param, corrects_param, indecisions_param, already_param, time_param, 
		quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))	
	db_self.conn.commit()

	db_self.curs.execute('''UPDATE Full_played_summary SET num_answers_game = ?, num_corrects_game = ?, num_errors_game = ? , num_indecisions_game = ?, num_already_game = ?, time_of_game = ?,
		quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? '''
		,(num_answers_param, errors_param, corrects_param, indecisions_param, already_param, time_param, 
		quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))	
	db_self.conn.commit()

	db_self.curs.execute('''UPDATE Full_played_recap_giga SET num_answers_game = ?, num_corrects_game = ?, num_errors_game = ? , num_indecisions_game = ?, num_already_game = ?, time_of_game = ?,
		quantity_feedback_match = ?, grade_feedback_match = ?, value_feedback_match = ?
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? '''
		,(num_answers_param, errors_param, corrects_param, indecisions_param, already_param, time_param, 
		quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))
	db_self.conn.commit()
	print("Kids_Matches new value put")
	print("Full_played_recap_giga updated by database_controller_update")
	print("Full_played_summary updated by database_controller_update")

	print("Kids_Session new value put")
	dcb.construct_ResultsTotalQuestion_table(db_self)
	dcb.construct_ResultsTotalTypeOfGame_table(db_self)
	dcb.construct_ResultsTypeOfGame_new_table(db_self)
	dcb.construct_ResultsTotalKindOfGame_table(db_self)
	dcb.construct_ResultsKindOfGame_new_table(db_self)
	dcb.construct_ResultsTotalSession_table(db_self)
	dcb.construct_ResultsTotalMatch_table(db_self)
	dcb.construct_ResultsTotalGame_table(db_self)
	print("Ok, single tables scores updated succesfully")

cpdef put_kid_session_scores(db_self, float time_param, int quantity_feedback_param, int grade_feedback_param, float value_feedback_param,
	int curr_kid_id, int curr_session, int curr_match, int curr_game, int curr_quest):
	print("NEGRITA put_kid_session_scores")
	db_self.curs.execute('''UPDATE Kids_Sessions SET time_of_session = ?, quantity_feedback_session = ?, value_feedback_session = ?
		WHERE question_id IN (
		SELECT question_id
		FROM Full_played_summary 
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ?)'''
		,(time_param, quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))
	db_self.conn.commit()

	db_self.curs.execute('''UPDATE Full_played_summary SET time_of_session = ?, quantity_feedback_session = ?, grade_feedback_session = ?, value_feedback_session = ?
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? '''
		,(time_param, quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))
	db_self.conn.commit()

	db_self.curs.execute('''UPDATE Full_played_recap_giga SET time_of_session = ?, quantity_feedback_session = ?, grade_feedback_session = ?, value_feedback_session = ?
		WHERE kid_id = ? AND session_id = ? AND match_id = ? AND game_id = ? AND question_id = ? '''
		,(time_param, quantity_feedback_param, grade_feedback_param, value_feedback_param, curr_kid_id, curr_session, curr_match, curr_game, curr_quest,))
	db_self.conn.commit()

	print("Kids_Session new value put")
	print("Full_played_recap_giga updated by database_controller_update")
	print("Full_played_summary updated by database_controller_update")

	print("Kids_Session new value put")
	dcb.construct_ResultsTotalQuestion_table(db_self)
	dcb.construct_ResultsTotalTypeOfGame_table(db_self)
	dcb.construct_ResultsTypeOfGame_new_table(db_self)
	dcb.construct_ResultsTotalKindOfGame_table(db_self)
	dcb.construct_ResultsKindOfGame_new_table(db_self)
	dcb.construct_ResultsTotalSession_table(db_self)
	dcb.construct_ResultsTotalMatch_table(db_self)
	dcb.construct_ResultsTotalGame_table(db_self)
	print("Ok, single tables scores updated succesfully")
# ----------------------------------------------------------------------------------------------------------------------------------------
# Include #all in once
# ----------------------------------------------------------------------------------------------------------------------------------------
cdef include_all_feeds_in_once(db_self, int kid_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
	dcu2.include_all_feeds_in_once(db_self, kid_id, questions_feeds, the_session_feeds, matches_feeds, games_feeds)
	print("Kids_Session new value put")
	print("Kids_Matches new value put")
	print("Kids_Games new value put")
	print("Kids_Questions new value put")
	print("Full_played_recap_giga updated by database_controller_update")
	print("Full_played_summary updated by database_controller_update")

	dcb.construct_ResultsTotalQuestion_table(db_self)
	dcb.construct_ResultsTotalTypeOfGame_table(db_self)
	dcb.construct_ResultsTypeOfGame_new_table(db_self)
	dcb.construct_ResultsTotalKindOfGame_table(db_self)
	dcb.construct_ResultsKindOfGame_new_table(db_self)
	dcb.construct_ResultsTotalSession_table(db_self)
	dcb.construct_ResultsTotalMatch_table(db_self)
	dcb.construct_ResultsTotalGame_table(db_self)
	print("Ok, single tables tot scores updated succesfully")

cdef include_all_results_in_once(db_self, int kid_id, float time_sess, list questions_results, list matches_results, list games_results):
	dcu2.include_all_results_in_once(db_self, kid_id, time_sess, questions_results, matches_results, games_results)
	print("Kids_Session new value put")
	print("Kids_Matches new value put")
	print("Kids_Games new value put")
	print("Kids_Questions new value put")
	print("Full_played_recap_giga updated by database_controller_update")
	print("Full_played_summary updated by database_controller_update")

	dcb.construct_ResultsTotalQuestion_table(db_self)
	dcb.construct_ResultsTotalTypeOfGame_table(db_self)
	dcb.construct_ResultsTypeOfGame_new_table(db_self)
	dcb.construct_ResultsTotalKindOfGame_table(db_self)
	dcb.construct_ResultsKindOfGame_new_table(db_self)
	dcb.construct_ResultsTotalSession_table(db_self)
	dcb.construct_ResultsTotalMatch_table(db_self)
	dcb.construct_ResultsTotalGame_table(db_self)
	print("Ok, single tables tot scores updated succesfully")

	db_self.execute("DELETE FROM Kids_Questions WHERE session_id isNull")
	db_self.conn.commit()

cdef include_all_feeds_in_once_given_session(db_self, int kid_id, int session_id, list questions_feeds, list the_session_feeds, list matches_feeds, list games_feeds):
	print("siamo entrati in include_all_FEEEDBACKSS_in_once_given_session!!!!")
	dcu2.include_all_feeds_in_once_given_session(db_self, kid_id, session_id, questions_feeds, the_session_feeds, matches_feeds, games_feeds)
	print("Kids_Session new value put")
	print("Kids_Matches new value put")
	print("Kids_Games new value put")
	print("Kids_Questions new value put")
	print("Full_played_recap_giga updated by database_controller_update")
	print("Full_played_summary updated by database_controller_update")

	db_self.curs.execute("DELETE FROM Kids_Questions WHERE session_id isNull")
	db_self.conn.commit()

	dcb.construct_ResultsTotalQuestion_table(db_self)
	dcb.construct_ResultsTotalTypeOfGame_table(db_self)
	dcb.construct_ResultsTypeOfGame_new_table(db_self)
	dcb.construct_ResultsTotalKindOfGame_table(db_self)
	dcb.construct_ResultsKindOfGame_new_table(db_self)
	dcb.construct_ResultsTotalSession_table(db_self)
	dcb.construct_ResultsTotalMatch_table(db_self)
	dcb.construct_ResultsTotalGame_table(db_self)
	print("Ok, single tables tot scores updated succesfully")

cdef include_all_results_in_once_given_session(db_self, int kid_id, int session_id, float time_sess, list questions_results, list matches_results, list games_results):
	print("siamo entrati in include_all_results_in_once_given_session!!!!")
	dcu2.include_all_results_in_once_given_session(db_self, kid_id, session_id, time_sess, questions_results, matches_results, games_results)
	print("Kids_Session new value put")
	print("Kids_Matches new value put")
	print("Kids_Games new value put")
	print("Kids_Questions new value put")
	print("Full_played_recap_giga updated by database_controller_update")
	print("Full_played_summary updated by database_controller_update")

	db_self.curs.execute("DELETE FROM Kids_Questions WHERE session_id isNull")
	db_self.conn.commit()

	dcb.construct_ResultsTotalQuestion_table(db_self)
	dcb.construct_ResultsTotalTypeOfGame_table(db_self)
	dcb.construct_ResultsTypeOfGame_new_table(db_self)
	dcb.construct_ResultsTotalKindOfGame_table(db_self)
	dcb.construct_ResultsKindOfGame_new_table(db_self)
	dcb.construct_ResultsTotalSession_table(db_self)
	dcb.construct_ResultsTotalMatch_table(db_self)
	dcb.construct_ResultsTotalGame_table(db_self)
	print("Ok, single tables tot scores updated succesfully")

cdef include_new_therapist_grade_session(db_self, int treat_id, int sess_id, str grade):
	try:
		right_child = db_self.execute_param_query("SELECT kid_id from Full_definitive_recap_giga WHERE treatment_id = ? AND session_id = ?",(treat_id, sess_id))
		print("right_child , right_child , right_child => ",right_child)
		if len(right_child)==1:
			right_child = right_child[0]
		elif len(right_child)>1:
			right_child = right_child[0][0]
		db_self.curs.execute("UPDATE Kids_Sessions SET therapist_grade = ? WHERE kid_id = ? AND session_id = ?", (grade,right_child,sess_id,))
		db_self.curs.execute("UPDATE Full_played_recap_with_sessions SET therapist_grade = ? WHERE treatment_id = ? AND kid_id = ? AND session_id = ?", (grade,treat_id,right_child,sess_id,))
		db_self.curs.execute("UPDATE Full_sessions_played_summary SET therapist_grade = ? WHERE kid_id = ? AND session_id = ?", (grade,right_child,sess_id,))
		db_self.curs.execute("UPDATE Full_therapies_played_summary SET therapist_grade = ? WHERE treatment_id = ? AND kid_id = ? AND session_id = ?", (grade,treat_id,right_child,sess_id,))
		db_self.curs.execute("UPDATE Full_definitive_recap_giga SET s_therapist_grade = ? WHERE treatment_id = ? AND kid_id = ? AND session_id = ?", (grade,treat_id,right_child,sess_id,))
		db_self.conn.commit()
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")

cdef include_new_therapist_grade_entertainment(db_self, int treat_id, int enter_id, str grade):
	try:
		right_child = db_self.execute_param_query("SELECT kid_id from Full_definitive_recap_giga WHERE treatment_id = ? AND entertainment_id = ?",(treat_id, enter_id))
		print("right_child , right_child , right_child => ",right_child)
		if len(right_child)==1:
			right_child = right_child[0]
		elif len(right_child)>1:
			right_child = right_child[0][0]
		db_self.curs.execute("UPDATE Kids_Entertainments SET therapist_grade = ? WHERE kid_id = ? AND entertainment_id = ?", (grade,right_child,enter_id,))
		db_self.curs.execute("UPDATE Full_played_recap_with_entertainment SET e_therapist_grade = ? WHERE treatment_id = ? AND kid_id = ? AND entertainment_id = ?", (grade,treat_id,right_child,enter_id,))
		db_self.curs.execute("UPDATE Full_entertainment_played_summary SET e_therapist_grade = ? WHERE kid_id = ? AND entertainment_id = ?", (grade,right_child,enter_id,))
		db_self.curs.execute("UPDATE Full_definitive_recap_giga SET e_therapist_grade = ? WHERE treatment_id = ? AND kid_id = ? AND entertainment_id = ?", (grade,treat_id,right_child,enter_id,))
		db_self.conn.commit()
	except Exception as e:
		print("##############################################################################################################")
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
		message1 = msg.format(type(e).__name__, e.args)
		print(message1)
		print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
		error_is = traceback.format_exc().splitlines()
		bb = ''.join(error_is[2].split())
		print("The culprit is: {}".format(bb))
		print("##############################################################################################################")
