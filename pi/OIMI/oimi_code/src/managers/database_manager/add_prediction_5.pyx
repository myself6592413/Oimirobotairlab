""" Oimi robot database_manager
	Notes:
		For detailed info, look at dabatase_doc

	cpdef pick_plus_choosen_scenario_creation(db_self, str type_game):
	cpdef pick_plus_choosen_scenario(db_self, str type_game):
		# - the `subset=None` means that every column is used to determine if two rows are different; to change that specify the columns as an array
		# - the `inplace=True` means that the data structure is changed and the duplicate rows are gone  
		#creating_directly_scen_type
		# Write the results to a different filne	
		# Read again 	
	
	Created by Colombo Giacomo, Politecnico di Milano 2020 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import random
import pandas as pd
import db_extern_methods_two as dbem2
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cdef extract_one_scenario(db_self, int a_kid, str difficulty_mat, table, list type_games_lst):
	cdef:
		list get_a_scenario = []
		list all_groups = []
		list group_curr = []
		bint ok_found = 0
	print("entro in extract one scenario for this match !!!!")
	print("type_games_lst for this match{}".format(type_games_lst))
	print("vado col primo match")
	type1 = type_games_lst[0]
	res1 = table[table["type"] == type1] 
	fin1 = res1["scenario_group"].unique()
	all_groups = list(set(fin1))
	#print("TUTTO PICK round num 1")
	#print("RES RES RES") 
	#print(res1)
	#print("FIN FIN FIN")
	#print(fin1)
	#print("GROUP1")
	#print(all_groups)
	print("all_groups prima del inter ==== {}".format(len(all_groups)))
	for i in range(1,len(type_games_lst)):
		type_curr = type_games_lst[i]
		res_curr = table[table["type"] == type_curr] 
		fin_curr = res_curr["scenario_group"].unique()
		group_curr = list(set(fin_curr))
		#print("TUTTO PICK round num {}".format(i))
		#print("RES RES RES")
		#print(res_curr)
		#print("FIN FIN FIN")
		#print(fin_curr)
		#print("GROUP")
		#print(group_curr)
		print("LEN LEN LEN LEN all_groups prima del inter ==== {}".format(len(all_groups)))
		all_groups = list(set(all_groups) & set(group_curr))
		print("LEN LEN LEN LEN all_groups DOPO inter === {}".format(len(all_groups)))
		all_groups_ok = [item for sublist in all_groups for item in sublist]
	
	if difficulty_mat in ['Normal_2','Medium_1']:
		final_group = random.choices(all_groups, k=1)
		final_group = final_group[0]
		print("final_groupfinal_groupfinal_groupfinal_groupfinal_group {}".format(final_group))
		db_self.curs.execute('''SELECT scenario_id
			FROM Scenarios
			WHERE scenario_group = ?
			ORDER BY RANDOM()
			LIMIT 1''',(final_group,))
		tmp_scen = db_self.curs.fetchone()
		print("tmp_scen {}".format(tmp_scen))
		if len(tmp_scen)>0:
			tmp_scen = tmp_scen[0]
			get_a_scenario.append(tmp_scen)
		print("get_a_scenario ISSSSSSSSSSSSSSS = {}".format(get_a_scenario))
		print()
		print("all_groups {}".format(all_groups))
	if difficulty_mat in ['Medium_2','Hard_1','Hard_2']:
		print("all_gall_gall_gall_gall_g {}".format(all_groups))
		db_self.curs.execute('''SELECT scenario
			FROM Matches JOIN Kids_played_recap USING(match_id)
			WHERE kid_id = ?
			GROUP BY scenario''',(db_self.a_kid,))
		kids_scen_done = db_self.curs.fetchall()
		get_kids_scen = [item for sublist in kids_scen_done for item in sublist]
		print("kids_scen_done kids_scen_done {}".format(get_kids_scen))
		print("get_kids_scen is {}".format(get_kids_scen))
		all_all_s = []
		for j in range(len(all_groups)):
			db_self.curs.execute('''SELECT scenario_id
				FROM Scenarios
				WHERE scenario_group = ?
				ORDER BY RANDOM()''',(all_groups[j],))
			all_s = db_self.curs.fetchall()[0][0]
			all_all_s.append(all_s)
		print("all_all_s all_all_s all_all_s PRIMA {}".format(all_all_s))
		#if all_scenario not in get_kids_scen:
		#redo=1	
		in_common = list(set(get_kids_scen) & set(all_all_s))
		print("all_groups LEN = {}".format(len(in_common)))
		print("all_groups TOTALE = {}".format(in_common))

		differ = dbem2.do_difference(all_all_s,get_kids_scen)
		print(differ)
		if len(differ)==0:
			final_secondo_caso = all_all_s
			
		final_secondo_caso = random.choices(differ, k=1)
		print("final_secondo_caso")
		print(final_secondo_caso)
		get_a_scenario = final_secondo_caso[0]
		print("final_sc")
		print(get_a_scenario)
		redo=1
	if difficulty_mat in ['Easy_1','Easy_2','Normal_1']:
		db_self.curs.execute('''SELECT scenario_group FROM Scenarios JOIN 
			(Matches JOIN Kids_played_recap USING(match_id)) ON (scenario=scenario_id)
		WHERE kid_id = ?
		GROUP BY scenario''',(db_self.a_kid,))
		kids_scen_to_use = db_self.curs.fetchall()
		if len(kids_scen_to_use)>0:
			print("kids_scen_to_use kids_scen_to_use {}".format(kids_scen_to_use))
			get_kids_scen_group = [item for sublist in kids_scen_to_use for item in sublist]
			#print("kids_scen_group kids_scen_done {}".format(get_kids_scen_group))
			print("get_kids_scen_group is {}".format(get_kids_scen_group))
			common_elems = list(set(all_groups).intersection(set(get_kids_scen_group)))		
			if len(common_elems)==0:
				final_group = random.choices(all_groups, k=1)
				final_group = final_group[0]
				print("final_groupfinal_groupfinal_groupfinal_groupfinal_group {}".format(final_group))
			else:
				final_group = random.choices(common_elems, k=1)
				final_group = final_group[0]
			db_self.curs.execute('''SELECT scenario_id
				FROM Scenarios
				WHERE scenario_group = ?
				ORDER BY RANDOM()
				LIMIT 1''',(final_group,))
			tmp_scen = db_self.curs.fetchone()
			print("tmp_scen {}".format(tmp_scen))
			if len(tmp_scen)>0:
				tmp_scen = tmp_scen[0]
				get_a_scenario.append(tmp_scen)
			print("get_a_scenario ISSSSSSSSSSSSSSS = {}".format(get_a_scenario))
			print()
			print("all_groups {}".format(all_groups))
		else:
			final_group = random.choices(all_groups, k=1)
			final_group = final_group[0]
			db_self.curs.execute('''SELECT scenario_id
				FROM Scenarios
				WHERE scenario_group = ?
				ORDER BY RANDOM()
				LIMIT 1''',(final_group,))
			tmp_scen = db_self.curs.fetchone()
			print("tmp_scen {}".format(tmp_scen))
			if len(tmp_scen)>0:
				tmp_scen = tmp_scen[0]
				get_a_scenario.append(tmp_scen)
			print("get_a_scenario ISSSSSSSSSSSSSSS = {}".format(get_a_scenario))
			print()
			print("all_groups {}".format(all_groups))				
	print("RISULTATO DI extract_one_scenario PRIMA DI PASSARLO ==> get_a_scenario is ==> {}".format(get_a_scenario))
	return get_a_scenario

cdef pick_scenario(db_self):
	scen = []

	table = pd.read_csv('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/scen_type_populate.csv',skiprows=None, engine='python', encoding="utf-8")
	
	print("db_self.kid_id {}".format(db_self.kid_id))
	print("num_of_matches {}".format(db_self.num_of_matches))
	print("db_self.difficulty_mat_1 {}".format(db_self.difficulty_mat_1))
	print("db_self.difficulty_mat_2 {}".format(db_self.difficulty_mat_2))
	print("db_self.difficulty_mat_3 {}".format(db_self.difficulty_mat_3))
	print("db_self.difficulty_mat_4 {}".format(db_self.difficulty_mat_4))
	print("db_self.games_m1 {}".format(db_self.games_m1))
	print("db_self.games_m2 {}".format(db_self.games_m2))
	print("db_self.games_m3 {}".format(db_self.games_m3))
	print("db_self.games_m4 {}".format(db_self.games_m4))
	print("db_self.m1_questions_g1 {}".format(db_self.m1_questions_g1))
	print("db_self.m1_questions_g2 {}".format(db_self.m1_questions_g2))
	print("db_self.m1_questions_g3 {}".format(db_self.m1_questions_g3))
	print("db_self.m1_questions_g4 {}".format(db_self.m1_questions_g4))
	print("db_self.m2_questions_g1 {}".format(db_self.m2_questions_g1))
	print("db_self.m2_questions_g2 {}".format(db_self.m2_questions_g2))
	print("db_self.m2_questions_g3 {}".format(db_self.m2_questions_g3))
	print("db_self.m2_questions_g4 {}".format(db_self.m2_questions_g4))
	print("db_self.m3_questions_g1 {}".format(db_self.m3_questions_g1))
	print("db_self.m3_questions_g2 {}".format(db_self.m3_questions_g2))
	print("db_self.m3_questions_g3 {}".format(db_self.m3_questions_g3))
	print("db_self.m3_questions_g4 {}".format(db_self.m3_questions_g4))
	print("db_self.m4_questions_g1 {}".format(db_self.m4_questions_g1))
	print("db_self.m4_questions_g2 {}".format(db_self.m4_questions_g2))
	print("db_self.m4_questions_g3 {}".format(db_self.m4_questions_g3))
	print("db_self.m4_questions_g4 {}".format(db_self.m4_questions_g4))
	print("kind_m1_g1 {}".format(db_self.kind_m1_g1))
	print("kind_m1_g2 {}".format(db_self.kind_m1_g2))
	print("kind_m1_g3 {}".format(db_self.kind_m1_g3))
	print("kind_m1_g4 {}".format(db_self.kind_m1_g4))
	print("kind_m2_g1 {}".format(db_self.kind_m2_g1))
	print("kind_m2_g2 {}".format(db_self.kind_m2_g2))
	print("kind_m2_g3 {}".format(db_self.kind_m2_g3))
	print("kind_m2_g4 {}".format(db_self.kind_m2_g4))
	print("kind_m3_g1 {}".format(db_self.kind_m3_g1))
	print("kind_m3_g2 {}".format(db_self.kind_m3_g2))
	print("kind_m3_g3 {}".format(db_self.kind_m3_g3))
	print("kind_m3_g4 {}".format(db_self.kind_m3_g4))
	print("kind_m4_g1 {}".format(db_self.kind_m4_g1))
	print("kind_m4_g2 {}".format(db_self.kind_m4_g2))
	print("kind_m4_g3 {}".format(db_self.kind_m4_g3))
	print("kind_m4_g4 {}".format(db_self.kind_m4_g4))
	print("db_self.type_m1_g1_1r {}".format(db_self.type_m1_g1_1r))
	print("db_self.type_m1_g1_2r {}".format(db_self.type_m1_g1_2r))
	print("db_self.type_m1_g1_3r {}".format(db_self.type_m1_g1_3r))
	print("db_self.type_m1_g1_4r {}".format(db_self.type_m1_g1_4r))
	print("db_self.type_m1_g2_1r {}".format(db_self.type_m1_g2_1r))
	print("db_self.type_m1_g2_2r {}".format(db_self.type_m1_g2_2r))
	print("db_self.type_m1_g2_3r {}".format(db_self.type_m1_g2_3r))
	print("db_self.type_m1_g2_4r {}".format(db_self.type_m1_g2_4r))
	print("db_self.type_m1_g3_1r {}".format(db_self.type_m1_g3_1r))
	print("db_self.type_m1_g3_2r {}".format(db_self.type_m1_g3_2r))
	print("db_self.type_m1_g3_3r {}".format(db_self.type_m1_g3_3r))
	print("db_self.type_m1_g3_4r {}".format(db_self.type_m1_g3_4r))
	print("db_self.type_m1_g4_1r {}".format(db_self.type_m1_g4_1r))
	print("db_self.type_m1_g4_2r {}".format(db_self.type_m1_g4_2r))
	print("db_self.type_m1_g4_3r {}".format(db_self.type_m1_g4_3r))
	print("db_self.type_m1_g4_4r {}".format(db_self.type_m1_g4_4r))
	print("db_self.type_m2_g1_1r {}".format(db_self.type_m2_g1_1r))
	print("db_self.type_m2_g1_2r {}".format(db_self.type_m2_g1_2r))
	print("db_self.type_m2_g1_3r {}".format(db_self.type_m2_g1_3r))
	print("db_self.type_m2_g1_4r {}".format(db_self.type_m2_g1_4r))
	print("db_self.type_m2_g2_1r {}".format(db_self.type_m2_g2_1r))
	print("db_self.type_m2_g2_2r {}".format(db_self.type_m2_g2_2r))
	print("db_self.type_m2_g2_3r {}".format(db_self.type_m2_g2_3r))
	print("db_self.type_m2_g2_4r {}".format(db_self.type_m2_g2_4r))
	print("db_self.type_m2_g3_1r {}".format(db_self.type_m2_g3_1r))
	print("db_self.type_m2_g3_2r {}".format(db_self.type_m2_g3_2r))
	print("db_self.type_m2_g3_3r {}".format(db_self.type_m2_g3_3r))
	print("db_self.type_m2_g3_4r {}".format(db_self.type_m2_g3_4r))
	print("db_self.type_m2_g4_1r {}".format(db_self.type_m2_g4_1r))
	print("db_self.type_m2_g4_2r {}".format(db_self.type_m2_g4_2r))
	print("db_self.type_m2_g4_3r {}".format(db_self.type_m2_g4_3r))
	print("db_self.type_m2_g4_4r {}".format(db_self.type_m2_g4_4r))
	print("db_self.type_m3_g1_1r {}".format(db_self.type_m3_g1_1r))
	print("db_self.type_m3_g1_2r {}".format(db_self.type_m3_g1_2r))
	print("db_self.type_m3_g1_3r {}".format(db_self.type_m3_g1_3r))
	print("db_self.type_m3_g1_4r {}".format(db_self.type_m3_g1_4r))
	print("db_self.type_m3_g2_1r {}".format(db_self.type_m3_g2_1r))
	print("db_self.type_m3_g2_2r {}".format(db_self.type_m3_g2_2r))
	print("db_self.type_m3_g2_3r {}".format(db_self.type_m3_g2_3r))
	print("db_self.type_m3_g2_4r {}".format(db_self.type_m3_g2_4r))
	print("db_self.type_m3_g3_1r {}".format(db_self.type_m3_g3_1r))
	print("db_self.type_m3_g3_2r {}".format(db_self.type_m3_g3_2r))
	print("db_self.type_m3_g3_3r {}".format(db_self.type_m3_g3_3r))
	print("db_self.type_m3_g3_4r {}".format(db_self.type_m3_g3_4r))
	print("db_self.type_m3_g4_1r {}".format(db_self.type_m3_g4_1r))
	print("db_self.type_m3_g4_2r {}".format(db_self.type_m3_g4_2r))
	print("db_self.type_m3_g4_3r {}".format(db_self.type_m3_g4_3r))
	print("db_self.type_m3_g4_4r {}".format(db_self.type_m3_g4_4r))
	print("db_self.type_m4_g1_1r {}".format(db_self.type_m4_g1_1r))
	print("db_self.type_m4_g1_2r {}".format(db_self.type_m4_g1_2r))
	print("db_self.type_m4_g1_3r {}".format(db_self.type_m4_g1_3r))
	print("db_self.type_m4_g1_4r {}".format(db_self.type_m4_g1_4r))
	print("db_self.type_m4_g2_1r {}".format(db_self.type_m4_g2_1r))
	print("db_self.type_m4_g2_2r {}".format(db_self.type_m4_g2_2r))
	print("db_self.type_m4_g2_3r {}".format(db_self.type_m4_g2_3r))
	print("db_self.type_m4_g2_4r {}".format(db_self.type_m4_g2_4r))
	print("db_self.type_m4_g3_1r {}".format(db_self.type_m4_g3_1r))
	print("db_self.type_m4_g3_2r {}".format(db_self.type_m4_g3_2r))
	print("db_self.type_m4_g3_3r {}".format(db_self.type_m4_g3_3r))
	print("db_self.type_m4_g3_4r {}".format(db_self.type_m4_g3_4r))
	print("db_self.type_m4_g4_1r {}".format(db_self.type_m4_g4_1r))
	print("db_self.type_m4_g4_2r {}".format(db_self.type_m4_g4_2r))
	print("db_self.type_m4_g4_3r {}".format(db_self.type_m4_g4_3r))
	print("db_self.type_m4_g4_4r {}".format(db_self.type_m4_g4_4r))
	print("scen {}".format(scen))
	types_tmp = []
	types_to_pass1, types_to_pass2 = [],[]
	if db_self.num_of_matches==1:
		if db_self.games_m1==1:
			print("!!db_self.games_m1==1!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==2:
			print("!!db_self.games_m1==2!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
				types_tmp = list(set(types_tmp))
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==3:
			print("!!db_self.games_m1==3!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g3==1:
				print("^^db_self.games_db_self.m1_questions_g3==1^^")
				types_tmp = [db_self.type_m1_g3_1r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==2^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==3^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r]
			elif db_self.m1_questions_g3==4:
				print("^^db_self.games_db_self.m1_questions_g3==4^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r,db_self.type_m1_g3_4r]
			types_tmp = list(set(types_tmp))		
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==4:
			print("!!db_self.games_m1==4!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g3==1:
				print("^^db_self.games_db_self.m1_questions_g3==1^^")
				types_tmp = [db_self.type_m1_g3_1r]
			elif db_self.m1_questions_g3==2:
				print("^^db_self.games_db_self.m1_questions_g3==2^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r]
			elif db_self.m1_questions_g3==4:
				print("^^db_self.games_db_self.m1_questions_g3==4^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r,db_self.type_m1_g3_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g4==1:
				print("^^db_self.games_db_self.m4_questions_g4==1^^")
				types_tmp = [db_self.type_m1_g4_1r]
			elif db_self.m1_questions_g4==2:
				print("^^db_self.games_db_self.m4_questions_g4==2^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r]
			elif db_self.m1_questions_g4==3:
				print("^^db_self.games_db_self.m4_questions_g4==3^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r,db_self.type_m1_g4_3r]
			elif db_self.m1_questions_g4==4:
				print("^^db_self.games_db_self.m4_questions_g4==4^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r,db_self.type_m1_g4_3r,db_self.type_m1_g4_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			print("DEFINITIVO PER TUTTI types_to_pass1 {}".format(types_to_pass1))
		print("TYPES BEFORE {}".format(types_to_pass1))
		types_to_pass1 = list(dbem2.iterFlatten(types_to_pass1))
		scen_1_is = extract_one_scenario(db_self,db_self.kid_id,db_self.difficulty_mat_1,table,types_to_pass1)
		print("TYPES AFTER FLAT {}".format(types_to_pass1))
		print("scen_1_is")
		print(scen_1_is)
		scen.append(scen_1_is)
	
		scen = list(dbem2.iterFlatten(scen))
		print("SCENSCENASCENASCENSCENASCENASCENSCENASCENASCENSCENASCENA AFTER FLAT {}".format(scen))
		print("scen FINALE PRIMA DEL FATTACCIO!!! ====> {}".format(scen))
		return scen

	elif db_self.num_of_matches==2:
		if db_self.games_m1==1:
			print("!!db_self.games_m1==1!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==2:
			print("!!db_self.games_m1==2!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
				types_tmp = list(set(types_tmp))
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==3:
			print("!!db_self.games_m1==3!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g3==1:
				print("^^db_self.games_db_self.m1_questions_g3==1^^")
				types_tmp = [db_self.type_m1_g3_1r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==2^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==3^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r]
			elif db_self.m1_questions_g3==4:
				print("^^db_self.games_db_self.m1_questions_g3==4^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r,db_self.type_m1_g3_4r]
			types_tmp = list(set(types_tmp))		
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==4:
			print("!!db_self.games_m1==4!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g3==1:
				print("^^db_self.games_db_self.m1_questions_g3==1^^")
				types_tmp = [db_self.type_m1_g3_1r]
			elif db_self.m1_questions_g3==2:
				print("^^db_self.games_db_self.m1_questions_g3==2^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r]
			elif db_self.m1_questions_g3==4:
				print("^^db_self.games_db_self.m1_questions_g3==4^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r,db_self.type_m1_g3_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g4==1:
				print("^^db_self.games_db_self.m4_questions_g4==1^^")
				types_tmp = [db_self.type_m1_g4_1r]
			elif db_self.m1_questions_g4==2:
				print("^^db_self.games_db_self.m4_questions_g4==2^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r]
			elif db_self.m1_questions_g4==3:
				print("^^db_self.games_db_self.m4_questions_g4==3^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r,db_self.type_m1_g4_3r]
			elif db_self.m1_questions_g4==4:
				print("^^db_self.games_db_self.m4_questions_g4==4^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r,db_self.type_m1_g4_3r,db_self.type_m1_g4_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			print("DEFINITIVO PER TUTTI types_to_pass1 {}".format(types_to_pass1))
		print("TYPES BEFORE {}".format(types_to_pass1))
		types_to_pass1 = list(dbem2.iterFlatten(types_to_pass1))
		scen_1_is = extract_one_scenario(db_self,db_self.kid_id,db_self.difficulty_mat_1,table,types_to_pass1)
		print("TYPES AFTER FLAT {}".format(types_to_pass1))
		print("scen_1_is")
		print(scen_1_is)
		scen.append(scen_1_is)			
		if db_self.games_m2==1:
			print("!!db_self.games_m2==1!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
		elif db_self.games_m2==2:
			print("!!db_self.games_m2==2!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g2==1:
				print("^^db_self.games_db_self.m2_questions_g2==1^^")
				types_tmp = [db_self.type_m2_g2_1r]
			elif db_self.m2_questions_g2==2:
				print("^^db_self.games_db_self.m2_questions_g2==2^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r]
			elif db_self.m2_questions_g2==3:
				print("^^db_self.games_db_self.m2_questions_g2==3^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r]
				types_tmp = list(set(types_tmp))
			elif db_self.m2_questions_g2==4:
				print("^^db_self.games_db_self.m2_questions_g2==4^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r,db_self.type_m2_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
		elif db_self.games_m2==3:
			print("!!db_self.games_m2==3!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g2==1:
				print("^^db_self.games_db_self.m2_questions_g2==1^^")
				types_tmp = [db_self.type_m2_g2_1r]
			elif db_self.m2_questions_g2==2:
				print("^^db_self.games_db_self.m2_questions_g2==2^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r]
			elif db_self.m2_questions_g2==3:
				print("^^db_self.games_db_self.m2_questions_g2==3^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r]
			elif db_self.m2_questions_g2==4:
				print("^^db_self.games_db_self.m2_questions_g2==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g2_3r,db_self.type_m2_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g3==1:
				print("^^db_self.games_db_self.m2_questions_g3==1^^")
				types_tmp = [db_self.type_m2_g3_1r]
			elif db_self.m2_questions_g3==3:
				print("^^db_self.games_db_self.m2_questions_g3==2^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r]
			elif db_self.m2_questions_g3==3:
				print("^^db_self.games_db_self.m2_questions_g3==3^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r]
			elif db_self.m2_questions_g3==4:
				print("^^db_self.games_db_self.m2_questions_g3==4^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r,db_self.type_m2_g3_4r]
			types_tmp = list(set(types_tmp))		
			types_to_pass2.append(types_tmp)
		elif db_self.games_m2==4:
			print("!!db_self.games_m2==4!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g2==1:
				print("^^db_self.games_db_self.m2_questions_g2==1^^")
				types_tmp = [db_self.type_m2_g2_1r]
			elif db_self.m2_questions_g2==2:
				print("^^db_self.games_db_self.m2_questions_g2==2^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r]
			elif db_self.m2_questions_g2==3:
				print("^^db_self.games_db_self.m2_questions_g2==3^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r]
			elif db_self.m2_questions_g2==4:
				print("^^db_self.games_db_self.m2_questions_g2==4^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r,db_self.type_m2_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g3==1:
				print("^^db_self.games_db_self.m2_questions_g3==1^^")
				types_tmp = [db_self.type_m2_g3_1r]
			elif db_self.m2_questions_g3==2:
				print("^^db_self.games_db_self.m2_questions_g3==2^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r]
			elif db_self.m2_questions_g3==3:
				print("^^db_self.games_db_self.m2_questions_g3==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r]
			elif db_self.m2_questions_g3==4:
				print("^^db_self.games_db_self.m2_questions_g3==4^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r,db_self.type_m2_g3_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g4==1:
				print("^^db_self.games_db_self.m4_questions_g4==1^^")
				types_tmp = [db_self.type_m2_g4_1r]
			elif db_self.m2_questions_g4==2:
				print("^^db_self.games_db_self.m4_questions_g4==2^^")
				types_tmp = [db_self.type_m2_g4_1r,db_self.type_m2_g4_2r]
			elif db_self.m2_questions_g4==3:
				print("^^db_self.games_db_self.m4_questions_g4==3^^")
				types_tmp = [db_self.type_m2_g4_1r,db_self.type_m2_g4_2r,db_self.type_m2_g4_3r]
			elif db_self.m2_questions_g4==4:
				print("^^db_self.games_db_self.m4_questions_g4==4^^")
				types_tmp = [db_self.type_m2_g4_1r,db_self.type_m2_g4_2r,db_self.type_m2_g4_3r,db_self.type_m2_g4_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			print("DEFINITIVO PER TUTTI types_to_pass2 {}".format(types_to_pass2))
		print("TYPES BEFORE {}".format(types_to_pass2))
		types_to_pass2 = list(dbem2.iterFlatten(types_to_pass2))
		print("TYPES AFTER FLAT {}".format(types_to_pass2))
		scen_2_is = extract_one_scenario(db_self,db_self.kid_id,db_self.difficulty_mat_2,table,types_to_pass2)
		print("scen_2_is")
		print(scen_2_is)
		scen.append(scen_2_is)
	
		scen = list(dbem2.iterFlatten(scen))
		print("SCENSCENASCENASCENSCENASCENASCENSCENASCENASCENSCENASCENA AFTER FLAT {}".format(scen))
		print("scen FINALE PRIMA DEL FATTACCIO!!! ====> {}".format(scen))
		return scen

	elif db_self.num_of_matches==3:
		pick_scenario_nm3(db_self,table)
	elif db_self.num_of_matches==4:
		pick_scenario_nm4(db_self,table)
	
cdef pick_scenario_nm3(db_self,table):
	types_tmp = []
	scen = []
	types_to_pass1, types_to_pass2, types_to_pass3 = [],[],[]
	if db_self.num_of_matches==3:
		
		if db_self.games_m1==1:
			print("!!db_self.games_m1==1!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==2:
			print("!!db_self.games_m1==2!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
				types_tmp = list(set(types_tmp))
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==3:
			print("!!db_self.games_m1==3!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g3==1:
				print("^^db_self.games_db_self.m1_questions_g3==1^^")
				types_tmp = [db_self.type_m1_g3_1r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==2^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==3^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r]
			elif db_self.m1_questions_g3==4:
				print("^^db_self.games_db_self.m1_questions_g3==4^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r,db_self.type_m1_g3_4r]
			types_tmp = list(set(types_tmp))		
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==4:
			print("!!db_self.games_m1==4!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g3==1:
				print("^^db_self.games_db_self.m1_questions_g3==1^^")
				types_tmp = [db_self.type_m1_g3_1r]
			elif db_self.m1_questions_g3==2:
				print("^^db_self.games_db_self.m1_questions_g3==2^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r]
			elif db_self.m1_questions_g3==4:
				print("^^db_self.games_db_self.m1_questions_g3==4^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r,db_self.type_m1_g3_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g4==1:
				print("^^db_self.games_db_self.m4_questions_g4==1^^")
				types_tmp = [db_self.type_m1_g4_1r]
			elif db_self.m1_questions_g4==2:
				print("^^db_self.games_db_self.m4_questions_g4==2^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r]
			elif db_self.m1_questions_g4==3:
				print("^^db_self.games_db_self.m4_questions_g4==3^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r,db_self.type_m1_g4_3r]
			elif db_self.m1_questions_g4==4:
				print("^^db_self.games_db_self.m4_questions_g4==4^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r,db_self.type_m1_g4_3r,db_self.type_m1_g4_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			print("DEFINITIVO PER TUTTI types_to_pass1 {}".format(types_to_pass1))
		print("TYPES BEFORE {}".format(types_to_pass1))
		types_to_pass1 = list(dbem2.iterFlatten(types_to_pass1))
		scen_1_is = extract_one_scenario(db_self,db_self.kid_id,db_self.difficulty_mat_1,table,types_to_pass1)
		print("TYPES AFTER FLAT {}".format(types_to_pass1))
		print("scen_1_is")
		print(scen_1_is)
		scen.append(scen_1_is)			
		if db_self.games_m2==1:
			print("!!db_self.games_m2==1!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
		elif db_self.games_m2==2:
			print("!!db_self.games_m2==2!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g2==1:
				print("^^db_self.games_db_self.m2_questions_g2==1^^")
				types_tmp = [db_self.type_m2_g2_1r]
			elif db_self.m2_questions_g2==2:
				print("^^db_self.games_db_self.m2_questions_g2==2^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r]
			elif db_self.m2_questions_g2==3:
				print("^^db_self.games_db_self.m2_questions_g2==3^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r]
				types_tmp = list(set(types_tmp))
			elif db_self.m2_questions_g2==4:
				print("^^db_self.games_db_self.m2_questions_g2==4^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r,db_self.type_m2_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
		elif db_self.games_m2==3:
			print("!!db_self.games_m2==3!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g2==1:
				print("^^db_self.games_db_self.m2_questions_g2==1^^")
				types_tmp = [db_self.type_m2_g2_1r]
			elif db_self.m2_questions_g2==2:
				print("^^db_self.games_db_self.m2_questions_g2==2^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r]
			elif db_self.m2_questions_g2==3:
				print("^^db_self.games_db_self.m2_questions_g2==3^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r]
			elif db_self.m2_questions_g2==4:
				print("^^db_self.games_db_self.m2_questions_g2==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g2_3r,db_self.type_m2_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g3==1:
				print("^^db_self.games_db_self.m2_questions_g3==1^^")
				types_tmp = [db_self.type_m2_g3_1r]
			elif db_self.m2_questions_g3==3:
				print("^^db_self.games_db_self.m2_questions_g3==2^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r]
			elif db_self.m2_questions_g3==3:
				print("^^db_self.games_db_self.m2_questions_g3==3^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r]
			elif db_self.m2_questions_g3==4:
				print("^^db_self.games_db_self.m2_questions_g3==4^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r,db_self.type_m2_g3_4r]
			types_tmp = list(set(types_tmp))		
			types_to_pass2.append(types_tmp)
		elif db_self.games_m2==4:
			print("!!db_self.games_m2==4!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g2==1:
				print("^^db_self.games_db_self.m2_questions_g2==1^^")
				types_tmp = [db_self.type_m2_g2_1r]
			elif db_self.m2_questions_g2==2:
				print("^^db_self.games_db_self.m2_questions_g2==2^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r]
			elif db_self.m2_questions_g2==3:
				print("^^db_self.games_db_self.m2_questions_g2==3^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r]
			elif db_self.m2_questions_g2==4:
				print("^^db_self.games_db_self.m2_questions_g2==4^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r,db_self.type_m2_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g3==1:
				print("^^db_self.games_db_self.m2_questions_g3==1^^")
				types_tmp = [db_self.type_m2_g3_1r]
			elif db_self.m2_questions_g3==2:
				print("^^db_self.games_db_self.m2_questions_g3==2^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r]
			elif db_self.m2_questions_g3==3:
				print("^^db_self.games_db_self.m2_questions_g3==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r]
			elif db_self.m2_questions_g3==4:
				print("^^db_self.games_db_self.m2_questions_g3==4^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r,db_self.type_m2_g3_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g4==1:
				print("^^db_self.games_db_self.m4_questions_g4==1^^")
				types_tmp = [db_self.type_m2_g4_1r]
			elif db_self.m2_questions_g4==2:
				print("^^db_self.games_db_self.m4_questions_g4==2^^")
				types_tmp = [db_self.type_m2_g4_1r,db_self.type_m2_g4_2r]
			elif db_self.m2_questions_g4==3:
				print("^^db_self.games_db_self.m4_questions_g4==3^^")
				types_tmp = [db_self.type_m2_g4_1r,db_self.type_m2_g4_2r,db_self.type_m2_g4_3r]
			elif db_self.m2_questions_g4==4:
				print("^^db_self.games_db_self.m4_questions_g4==4^^")
				types_tmp = [db_self.type_m2_g4_1r,db_self.type_m2_g4_2r,db_self.type_m2_g4_3r,db_self.type_m2_g4_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			print("DEFINITIVO PER TUTTI types_to_pass2 {}".format(types_to_pass2))
		print("TYPES BEFORE {}".format(types_to_pass2))
		types_to_pass2 = list(dbem2.iterFlatten(types_to_pass2))
		print("TYPES AFTER FLAT {}".format(types_to_pass2))
		scen_2_is = extract_one_scenario(db_self,db_self.kid_id,db_self.difficulty_mat_2,table,types_to_pass2)
		print("scen_2_is")
		print(scen_2_is)
		scen.append(scen_2_is)
		if db_self.games_m3==1:
			print("!!db_self.games_m3==1!!")
			if db_self.m3_questions_g1==1:
				print("^^db_self.games_db_self.m3_questions_g1==1^^")
				types_tmp = [db_self.type_m3_g1_1r]
			elif db_self.m3_questions_g1==2:
				print("^^db_self.games_db_self.m3_questions_g1==2^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r]
			elif db_self.m3_questions_g1==3:
				print("^^db_self.games_db_self.m3_questions_g1==3^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r]
			elif db_self.m3_questions_g1==4:
				print("^^db_self.games_db_self.m3_questions_g1==4^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r,db_self.type_m3_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
		elif db_self.games_m3==2:
			print("!!db_self.games_m3==2!!")
			if db_self.m3_questions_g1==1:
				print("^^db_self.games_db_self.m3_questions_g1==1^^")
				types_tmp = [db_self.type_m3_g1_1r]
			elif db_self.m3_questions_g1==2:
				print("^^db_self.games_db_self.m3_questions_g1==2^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r]
			elif db_self.m3_questions_g1==3:
				print("^^db_self.games_db_self.m3_questions_g1==3^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r]
			elif db_self.m3_questions_g1==4:
				print("^^db_self.games_db_self.m3_questions_g1==4^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r,db_self.type_m3_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g2==1:
				print("^^db_self.games_db_self.m3_questions_g2==1^^")
				types_tmp = [db_self.type_m3_g2_1r]
			elif db_self.m3_questions_g2==2:
				print("^^db_self.games_db_self.m3_questions_g2==2^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r]
			elif db_self.m3_questions_g2==3:
				print("^^db_self.games_db_self.m3_questions_g2==3^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r,db_self.type_m3_g2_3r]
				types_tmp = list(set(types_tmp))
			elif db_self.m3_questions_g2==4:
				print("^^db_self.games_db_self.m3_questions_g2==4^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r,db_self.type_m3_g2_3r,db_self.type_m3_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
		elif db_self.games_m3==3:
			print("!!db_self.games_m3==3!!")
			if db_self.m3_questions_g1==1:
				print("^^db_self.games_db_self.m3_questions_g1==1^^")
				types_tmp = [db_self.type_m3_g1_1r]
			elif db_self.m3_questions_g1==2:
				print("^^db_self.games_db_self.m3_questions_g1==2^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r]
			elif db_self.m3_questions_g1==3:
				print("^^db_self.games_db_self.m3_questions_g1==3^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r]
			elif db_self.m3_questions_g1==4:
				print("^^db_self.games_db_self.m3_questions_g1==4^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r,db_self.type_m3_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g2==1:
				print("^^db_self.games_db_self.m3_questions_g2==1^^")
				types_tmp = [db_self.type_m3_g2_1r]
			elif db_self.m3_questions_g2==2:
				print("^^db_self.games_db_self.m3_questions_g2==2^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r]
			elif db_self.m3_questions_g2==3:
				print("^^db_self.games_db_self.m3_questions_g2==3^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r,db_self.type_m3_g2_3r]
			elif db_self.m3_questions_g2==4:
				print("^^db_self.games_db_self.m3_questions_g2==4^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g2_3r,db_self.type_m3_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g3==1:
				print("^^db_self.games_db_self.m3_questions_g3==1^^")
				types_tmp = [db_self.type_m3_g3_1r]
			elif db_self.m3_questions_g3==3:
				print("^^db_self.games_db_self.m3_questions_g3==2^^")
				types_tmp = [db_self.type_m3_g3_1r,db_self.type_m3_g3_2r]
			elif db_self.m3_questions_g3==3:
				print("^^db_self.games_db_self.m3_questions_g3==3^^")
				types_tmp = [db_self.type_m3_g3_1r,db_self.type_m3_g3_2r,db_self.type_m3_g3_3r]
			elif db_self.m3_questions_g3==4:
				print("^^db_self.games_db_self.m3_questions_g3==4^^")
				types_tmp = [db_self.type_m3_g3_1r,db_self.type_m3_g3_2r,db_self.type_m3_g3_3r,db_self.type_m3_g3_4r]
			types_tmp = list(set(types_tmp))		
			types_to_pass3.append(types_tmp)
		elif db_self.games_m3==4:
			print("!!db_self.games_m3==4!!")
			if db_self.m3_questions_g1==1:
				print("^^db_self.games_db_self.m3_questions_g1==1^^")
				types_tmp = [db_self.type_m3_g1_1r]
			elif db_self.m3_questions_g1==2:
				print("^^db_self.games_db_self.m3_questions_g1==2^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r]
			elif db_self.m3_questions_g1==3:
				print("^^db_self.games_db_self.m3_questions_g1==3^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r]
			elif db_self.m3_questions_g1==4:
				print("^^db_self.games_db_self.m3_questions_g1==4^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r,db_self.type_m3_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g2==1:
				print("^^db_self.games_db_self.m3_questions_g2==1^^")
				types_tmp = [db_self.type_m3_g2_1r]
			elif db_self.m3_questions_g2==2:
				print("^^db_self.games_db_self.m3_questions_g2==2^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r]
			elif db_self.m3_questions_g2==3:
				print("^^db_self.games_db_self.m3_questions_g2==3^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r,db_self.type_m3_g2_3r]
			elif db_self.m3_questions_g2==4:
				print("^^db_self.games_db_self.m3_questions_g2==4^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r,db_self.type_m3_g2_3r,db_self.type_m3_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g3==1:
				print("^^db_self.games_db_self.m3_questions_g3==1^^")
				types_tmp = [db_self.type_m3_g3_1r]
			elif db_self.m3_questions_g3==2:
				print("^^db_self.games_db_self.m3_questions_g3==2^^")
				types_tmp = [db_self.type_m3_g3_1r,db_self.type_m3_g3_2r]
			elif db_self.m3_questions_g3==3:
				print("^^db_self.games_db_self.m3_questions_g3==3^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g3_2r,db_self.type_m3_g3_3r]
			elif db_self.m3_questions_g3==4:
				print("^^db_self.games_db_self.m3_questions_g3==4^^")
				types_tmp = [db_self.type_m3_g3_1r,db_self.type_m3_g3_2r,db_self.type_m3_g3_3r,db_self.type_m3_g3_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g4==1:
				print("^^db_self.games_db_self.m4_questions_g4==1^^")
				types_tmp = [db_self.type_m3_g4_1r]
			elif db_self.m3_questions_g4==2:
				print("^^db_self.games_db_self.m4_questions_g4==2^^")
				types_tmp = [db_self.type_m3_g4_1r,db_self.type_m3_g4_2r]
			elif db_self.m3_questions_g4==3:
				print("^^db_self.games_db_self.m4_questions_g4==3^^")
				types_tmp = [db_self.type_m3_g4_1r,db_self.type_m3_g4_2r,db_self.type_m3_g4_3r]
			elif db_self.m3_questions_g4==4:
				print("^^db_self.games_db_self.m4_questions_g4==4^^")
				types_tmp = [db_self.type_m3_g4_1r,db_self.type_m3_g4_2r,db_self.type_m3_g4_3r,db_self.type_m3_g4_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			print("DEFINITIVO PER TUTTI types_to_pass3 {}".format(types_to_pass3))
		print("TYPES BEFORE {}".format(types_to_pass3))
		types_to_pass3 = list(dbem2.iterFlatten(types_to_pass3))
		print("TYPES AFTER FLAT {}".format(types_to_pass3))
		scen_3_is = extract_one_scenario(db_self,db_self.kid_id,db_self.difficulty_mat_3,table,types_to_pass3)
		print("scen_3_is")
		print(scen_3_is)
		scen.append(scen_3_is)

	scen = list(dbem2.iterFlatten(scen))
	print("SCENSCENASCENASCENSCENASCENASCENSCENASCENASCENSCENASCENA AFTER FLAT {}".format(scen))
	print("scen FINALE PRIMA DEL FATTACCIO!!! ====> {}".format(scen))
	return scen

cdef pick_scenario_nm4(db_self,table):
	types_tmp = []
	scen = []
	types_to_pass1, types_to_pass2, types_to_pass3, types_to_pass4 = [],[],[],[]	
	if db_self.num_of_matches==4:
		if db_self.games_m1==1:
			print("!!db_self.games_m1==1!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==2:
			print("!!db_self.games_m1==2!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
				types_tmp = list(set(types_tmp))
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==3:
			print("!!db_self.games_m1==3!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g3==1:
				print("^^db_self.games_db_self.m1_questions_g3==1^^")
				types_tmp = [db_self.type_m1_g3_1r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==2^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==3^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r]
			elif db_self.m1_questions_g3==4:
				print("^^db_self.games_db_self.m1_questions_g3==4^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r,db_self.type_m1_g3_4r]
			types_tmp = list(set(types_tmp))		
			types_to_pass1.append(types_tmp)
		elif db_self.games_m1==4:
			print("!!db_self.games_m1==4!!")
			if db_self.m1_questions_g1==1:
				print("^^db_self.games_db_self.m1_questions_g1==1^^")
				types_tmp = [db_self.type_m1_g1_1r]
			elif db_self.m1_questions_g1==2:
				print("^^db_self.games_db_self.m1_questions_g1==2^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r]
			elif db_self.m1_questions_g1==3:
				print("^^db_self.games_db_self.m1_questions_g1==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r]
			elif db_self.m1_questions_g1==4:
				print("^^db_self.games_db_self.m1_questions_g1==4^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g1_2r,db_self.type_m1_g1_3r,db_self.type_m1_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g2==1:
				print("^^db_self.games_db_self.m1_questions_g2==1^^")
				types_tmp = [db_self.type_m1_g2_1r]
			elif db_self.m1_questions_g2==2:
				print("^^db_self.games_db_self.m1_questions_g2==2^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r]
			elif db_self.m1_questions_g2==3:
				print("^^db_self.games_db_self.m1_questions_g2==3^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r]
			elif db_self.m1_questions_g2==4:
				print("^^db_self.games_db_self.m1_questions_g2==4^^")
				types_tmp = [db_self.type_m1_g2_1r,db_self.type_m1_g2_2r,db_self.type_m1_g2_3r,db_self.type_m1_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g3==1:
				print("^^db_self.games_db_self.m1_questions_g3==1^^")
				types_tmp = [db_self.type_m1_g3_1r]
			elif db_self.m1_questions_g3==2:
				print("^^db_self.games_db_self.m1_questions_g3==2^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r]
			elif db_self.m1_questions_g3==3:
				print("^^db_self.games_db_self.m1_questions_g3==3^^")
				types_tmp = [db_self.type_m1_g1_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r]
			elif db_self.m1_questions_g3==4:
				print("^^db_self.games_db_self.m1_questions_g3==4^^")
				types_tmp = [db_self.type_m1_g3_1r,db_self.type_m1_g3_2r,db_self.type_m1_g3_3r,db_self.type_m1_g3_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			if db_self.m1_questions_g4==1:
				print("^^db_self.games_db_self.m4_questions_g4==1^^")
				types_tmp = [db_self.type_m1_g4_1r]
			elif db_self.m1_questions_g4==2:
				print("^^db_self.games_db_self.m4_questions_g4==2^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r]
			elif db_self.m1_questions_g4==3:
				print("^^db_self.games_db_self.m4_questions_g4==3^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r,db_self.type_m1_g4_3r]
			elif db_self.m1_questions_g4==4:
				print("^^db_self.games_db_self.m4_questions_g4==4^^")
				types_tmp = [db_self.type_m1_g4_1r,db_self.type_m1_g4_2r,db_self.type_m1_g4_3r,db_self.type_m1_g4_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass1.append(types_tmp)
			print("DEFINITIVO PER TUTTI types_to_pass1 {}".format(types_to_pass1))
		print("TYPES BEFORE {}".format(types_to_pass1))
		types_to_pass1 = list(dbem2.iterFlatten(types_to_pass1))
		scen_1_is = extract_one_scenario(db_self,db_self.kid_id,db_self.difficulty_mat_1,table,types_to_pass1)
		print("TYPES AFTER FLAT {}".format(types_to_pass1))
		print("scen_1_is")
		print(scen_1_is)
		scen.append(scen_1_is)			
		if db_self.games_m2==1:
			print("!!db_self.games_m2==1!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
		elif db_self.games_m2==2:
			print("!!db_self.games_m2==2!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g2==1:
				print("^^db_self.games_db_self.m2_questions_g2==1^^")
				types_tmp = [db_self.type_m2_g2_1r]
			elif db_self.m2_questions_g2==2:
				print("^^db_self.games_db_self.m2_questions_g2==2^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r]
			elif db_self.m2_questions_g2==3:
				print("^^db_self.games_db_self.m2_questions_g2==3^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r]
				types_tmp = list(set(types_tmp))
			elif db_self.m2_questions_g2==4:
				print("^^db_self.games_db_self.m2_questions_g2==4^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r,db_self.type_m2_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
		elif db_self.games_m2==3:
			print("!!db_self.games_m2==3!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g2==1:
				print("^^db_self.games_db_self.m2_questions_g2==1^^")
				types_tmp = [db_self.type_m2_g2_1r]
			elif db_self.m2_questions_g2==2:
				print("^^db_self.games_db_self.m2_questions_g2==2^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r]
			elif db_self.m2_questions_g2==3:
				print("^^db_self.games_db_self.m2_questions_g2==3^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r]
			elif db_self.m2_questions_g2==4:
				print("^^db_self.games_db_self.m2_questions_g2==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g2_3r,db_self.type_m2_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g3==1:
				print("^^db_self.games_db_self.m2_questions_g3==1^^")
				types_tmp = [db_self.type_m2_g3_1r]
			elif db_self.m2_questions_g3==3:
				print("^^db_self.games_db_self.m2_questions_g3==2^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r]
			elif db_self.m2_questions_g3==3:
				print("^^db_self.games_db_self.m2_questions_g3==3^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r]
			elif db_self.m2_questions_g3==4:
				print("^^db_self.games_db_self.m2_questions_g3==4^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r,db_self.type_m2_g3_4r]
			types_tmp = list(set(types_tmp))		
			types_to_pass2.append(types_tmp)
		elif db_self.games_m2==4:
			print("!!db_self.games_m2==4!!")
			if db_self.m2_questions_g1==1:
				print("^^db_self.games_db_self.m2_questions_g1==1^^")
				types_tmp = [db_self.type_m2_g1_1r]
			elif db_self.m2_questions_g1==2:
				print("^^db_self.games_db_self.m2_questions_g1==2^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r]
			elif db_self.m2_questions_g1==3:
				print("^^db_self.games_db_self.m2_questions_g1==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r]
			elif db_self.m2_questions_g1==4:
				print("^^db_self.games_db_self.m2_questions_g1==4^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g1_2r,db_self.type_m2_g1_3r,db_self.type_m2_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g2==1:
				print("^^db_self.games_db_self.m2_questions_g2==1^^")
				types_tmp = [db_self.type_m2_g2_1r]
			elif db_self.m2_questions_g2==2:
				print("^^db_self.games_db_self.m2_questions_g2==2^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r]
			elif db_self.m2_questions_g2==3:
				print("^^db_self.games_db_self.m2_questions_g2==3^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r]
			elif db_self.m2_questions_g2==4:
				print("^^db_self.games_db_self.m2_questions_g2==4^^")
				types_tmp = [db_self.type_m2_g2_1r,db_self.type_m2_g2_2r,db_self.type_m2_g2_3r,db_self.type_m2_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g3==1:
				print("^^db_self.games_db_self.m2_questions_g3==1^^")
				types_tmp = [db_self.type_m2_g3_1r]
			elif db_self.m2_questions_g3==2:
				print("^^db_self.games_db_self.m2_questions_g3==2^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r]
			elif db_self.m2_questions_g3==3:
				print("^^db_self.games_db_self.m2_questions_g3==3^^")
				types_tmp = [db_self.type_m2_g1_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r]
			elif db_self.m2_questions_g3==4:
				print("^^db_self.games_db_self.m2_questions_g3==4^^")
				types_tmp = [db_self.type_m2_g3_1r,db_self.type_m2_g3_2r,db_self.type_m2_g3_3r,db_self.type_m2_g3_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			if db_self.m2_questions_g4==1:
				print("^^db_self.games_db_self.m4_questions_g4==1^^")
				types_tmp = [db_self.type_m2_g4_1r]
			elif db_self.m2_questions_g4==2:
				print("^^db_self.games_db_self.m4_questions_g4==2^^")
				types_tmp = [db_self.type_m2_g4_1r,db_self.type_m2_g4_2r]
			elif db_self.m2_questions_g4==3:
				print("^^db_self.games_db_self.m4_questions_g4==3^^")
				types_tmp = [db_self.type_m2_g4_1r,db_self.type_m2_g4_2r,db_self.type_m2_g4_3r]
			elif db_self.m2_questions_g4==4:
				print("^^db_self.games_db_self.m4_questions_g4==4^^")
				types_tmp = [db_self.type_m2_g4_1r,db_self.type_m2_g4_2r,db_self.type_m2_g4_3r,db_self.type_m2_g4_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass2.append(types_tmp)
			print("DEFINITIVO PER TUTTI types_to_pass2 {}".format(types_to_pass2))
		print("TYPES BEFORE {}".format(types_to_pass2))
		types_to_pass2 = list(dbem2.iterFlatten(types_to_pass2))
		print("TYPES AFTER FLAT {}".format(types_to_pass2))
		scen_2_is = extract_one_scenario(db_self,db_self.kid_id,db_self.difficulty_mat_2,table,types_to_pass2)
		print("scen_2_is")
		print(scen_2_is)
		scen.append(scen_2_is)
		if db_self.games_m3==1:
			print("!!db_self.games_m3==1!!")
			if db_self.m3_questions_g1==1:
				print("^^db_self.games_db_self.m3_questions_g1==1^^")
				types_tmp = [db_self.type_m3_g1_1r]
			elif db_self.m3_questions_g1==2:
				print("^^db_self.games_db_self.m3_questions_g1==2^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r]
			elif db_self.m3_questions_g1==3:
				print("^^db_self.games_db_self.m3_questions_g1==3^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r]
			elif db_self.m3_questions_g1==4:
				print("^^db_self.games_db_self.m3_questions_g1==4^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r,db_self.type_m3_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
		elif db_self.games_m3==2:
			print("!!db_self.games_m3==2!!")
			if db_self.m3_questions_g1==1:
				print("^^db_self.games_db_self.m3_questions_g1==1^^")
				types_tmp = [db_self.type_m3_g1_1r]
			elif db_self.m3_questions_g1==2:
				print("^^db_self.games_db_self.m3_questions_g1==2^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r]
			elif db_self.m3_questions_g1==3:
				print("^^db_self.games_db_self.m3_questions_g1==3^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r]
			elif db_self.m3_questions_g1==4:
				print("^^db_self.games_db_self.m3_questions_g1==4^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r,db_self.type_m3_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g2==1:
				print("^^db_self.games_db_self.m3_questions_g2==1^^")
				types_tmp = [db_self.type_m3_g2_1r]
			elif db_self.m3_questions_g2==2:
				print("^^db_self.games_db_self.m3_questions_g2==2^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r]
			elif db_self.m3_questions_g2==3:
				print("^^db_self.games_db_self.m3_questions_g2==3^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r,db_self.type_m3_g2_3r]
				types_tmp = list(set(types_tmp))
			elif db_self.m3_questions_g2==4:
				print("^^db_self.games_db_self.m3_questions_g2==4^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r,db_self.type_m3_g2_3r,db_self.type_m3_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
		elif db_self.games_m3==3:
			print("!!db_self.games_m3==3!!")
			if db_self.m3_questions_g1==1:
				print("^^db_self.games_db_self.m3_questions_g1==1^^")
				types_tmp = [db_self.type_m3_g1_1r]
			elif db_self.m3_questions_g1==2:
				print("^^db_self.games_db_self.m3_questions_g1==2^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r]
			elif db_self.m3_questions_g1==3:
				print("^^db_self.games_db_self.m3_questions_g1==3^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r]
			elif db_self.m3_questions_g1==4:
				print("^^db_self.games_db_self.m3_questions_g1==4^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r,db_self.type_m3_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g2==1:
				print("^^db_self.games_db_self.m3_questions_g2==1^^")
				types_tmp = [db_self.type_m3_g2_1r]
			elif db_self.m3_questions_g2==2:
				print("^^db_self.games_db_self.m3_questions_g2==2^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r]
			elif db_self.m3_questions_g2==3:
				print("^^db_self.games_db_self.m3_questions_g2==3^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r,db_self.type_m3_g2_3r]
			elif db_self.m3_questions_g2==4:
				print("^^db_self.games_db_self.m3_questions_g2==4^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g2_3r,db_self.type_m3_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g3==1:
				print("^^db_self.games_db_self.m3_questions_g3==1^^")
				types_tmp = [db_self.type_m3_g3_1r]
			elif db_self.m3_questions_g3==3:
				print("^^db_self.games_db_self.m3_questions_g3==2^^")
				types_tmp = [db_self.type_m3_g3_1r,db_self.type_m3_g3_2r]
			elif db_self.m3_questions_g3==3:
				print("^^db_self.games_db_self.m3_questions_g3==3^^")
				types_tmp = [db_self.type_m3_g3_1r,db_self.type_m3_g3_2r,db_self.type_m3_g3_3r]
			elif db_self.m3_questions_g3==4:
				print("^^db_self.games_db_self.m3_questions_g3==4^^")
				types_tmp = [db_self.type_m3_g3_1r,db_self.type_m3_g3_2r,db_self.type_m3_g3_3r,db_self.type_m3_g3_4r]
			types_tmp = list(set(types_tmp))		
			types_to_pass3.append(types_tmp)
		elif db_self.games_m3==4:
			print("!!db_self.games_m3==4!!")
			if db_self.m3_questions_g1==1:
				print("^^db_self.games_db_self.m3_questions_g1==1^^")
				types_tmp = [db_self.type_m3_g1_1r]
			elif db_self.m3_questions_g1==2:
				print("^^db_self.games_db_self.m3_questions_g1==2^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r]
			elif db_self.m3_questions_g1==3:
				print("^^db_self.games_db_self.m3_questions_g1==3^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r]
			elif db_self.m3_questions_g1==4:
				print("^^db_self.games_db_self.m3_questions_g1==4^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g1_2r,db_self.type_m3_g1_3r,db_self.type_m3_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g2==1:
				print("^^db_self.games_db_self.m3_questions_g2==1^^")
				types_tmp = [db_self.type_m3_g2_1r]
			elif db_self.m3_questions_g2==2:
				print("^^db_self.games_db_self.m3_questions_g2==2^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r]
			elif db_self.m3_questions_g2==3:
				print("^^db_self.games_db_self.m3_questions_g2==3^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r,db_self.type_m3_g2_3r]
			elif db_self.m3_questions_g2==4:
				print("^^db_self.games_db_self.m3_questions_g2==4^^")
				types_tmp = [db_self.type_m3_g2_1r,db_self.type_m3_g2_2r,db_self.type_m3_g2_3r,db_self.type_m3_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g3==1:
				print("^^db_self.games_db_self.m3_questions_g3==1^^")
				types_tmp = [db_self.type_m3_g3_1r]
			elif db_self.m3_questions_g3==2:
				print("^^db_self.games_db_self.m3_questions_g3==2^^")
				types_tmp = [db_self.type_m3_g3_1r,db_self.type_m3_g3_2r]
			elif db_self.m3_questions_g3==3:
				print("^^db_self.games_db_self.m3_questions_g3==3^^")
				types_tmp = [db_self.type_m3_g1_1r,db_self.type_m3_g3_2r,db_self.type_m3_g3_3r]
			elif db_self.m3_questions_g3==4:
				print("^^db_self.games_db_self.m3_questions_g3==4^^")
				types_tmp = [db_self.type_m3_g3_1r,db_self.type_m3_g3_2r,db_self.type_m3_g3_3r,db_self.type_m3_g3_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			if db_self.m3_questions_g4==1:
				print("^^db_self.games_db_self.m4_questions_g4==1^^")
				types_tmp = [db_self.type_m3_g4_1r]
			elif db_self.m3_questions_g4==2:
				print("^^db_self.games_db_self.m4_questions_g4==2^^")
				types_tmp = [db_self.type_m3_g4_1r,db_self.type_m3_g4_2r]
			elif db_self.m3_questions_g4==3:
				print("^^db_self.games_db_self.m4_questions_g4==3^^")
				types_tmp = [db_self.type_m3_g4_1r,db_self.type_m3_g4_2r,db_self.type_m3_g4_3r]
			elif db_self.m3_questions_g4==4:
				print("^^db_self.games_db_self.m4_questions_g4==4^^")
				types_tmp = [db_self.type_m3_g4_1r,db_self.type_m3_g4_2r,db_self.type_m3_g4_3r,db_self.type_m3_g4_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass3.append(types_tmp)
			print("DEFINITIVO PER TUTTI types_to_pass3 {}".format(types_to_pass3))
		print("TYPES BEFORE {}".format(types_to_pass3))
		types_to_pass3 = list(dbem2.iterFlatten(types_to_pass3))
		print("TYPES AFTER FLAT {}".format(types_to_pass3))
		scen_3_is = extract_one_scenario(db_self,db_self.kid_id,db_self.difficulty_mat_3,table,types_to_pass3)
		print("scen_3_is")
		print(scen_3_is)
		scen.append(scen_3_is)
		if db_self.games_m4==1:
			print("!!db_self.games_m4==1!!")
			if db_self.m4_questions_g1==1:
				print("^^db_self.games_db_self.m4_questions_g1==1^^")
				types_tmp = [db_self.type_m4_g1_1r]
			elif db_self.m4_questions_g1==2:
				print("^^db_self.games_db_self.m4_questions_g1==2^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r]
			elif db_self.m4_questions_g1==3:
				print("^^db_self.games_db_self.m4_questions_g1==3^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r,db_self.type_m4_g1_3r]
			elif db_self.m4_questions_g1==4:
				print("^^db_self.games_db_self.m4_questions_g1==4^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r,db_self.type_m4_g1_3r,db_self.type_m4_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass4.append(types_tmp)
		elif db_self.games_m4==2:
			print("!!db_self.games_m4==2!!")
			if db_self.m4_questions_g1==1:
				print("^^db_self.games_db_self.m4_questions_g1==1^^")
				types_tmp = [db_self.type_m4_g1_1r]
			elif db_self.m4_questions_g1==2:
				print("^^db_self.games_db_self.m4_questions_g1==2^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r]
			elif db_self.m4_questions_g1==3:
				print("^^db_self.games_db_self.m4_questions_g1==3^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r,db_self.type_m4_g1_3r]
			elif db_self.m4_questions_g1==4:
				print("^^db_self.games_db_self.m4_questions_g1==4^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r,db_self.type_m4_g1_3r,db_self.type_m4_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass4.append(types_tmp)
			if db_self.m4_questions_g2==1:
				print("^^db_self.games_db_self.m4_questions_g2==1^^")
				types_tmp = [db_self.type_m4_g2_1r]
			elif db_self.m4_questions_g2==2:
				print("^^db_self.games_db_self.m4_questions_g2==2^^")
				types_tmp = [db_self.type_m4_g2_1r,db_self.type_m4_g2_2r]
			elif db_self.m4_questions_g2==3:
				print("^^db_self.games_db_self.m4_questions_g2==3^^")
				types_tmp = [db_self.type_m4_g2_1r,db_self.type_m4_g2_2r,db_self.type_m4_g2_3r]
				types_tmp = list(set(types_tmp))
			elif db_self.m4_questions_g2==4:
				print("^^db_self.games_db_self.m4_questions_g2==4^^")
				types_tmp = [db_self.type_m4_g2_1r,db_self.type_m4_g2_2r,db_self.type_m4_g2_3r,db_self.type_m4_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass4.append(types_tmp)
		elif db_self.games_m4==3:
			print("!!db_self.games_m4==3!!")
			if db_self.m4_questions_g1==1:
				print("^^db_self.games_db_self.m4_questions_g1==1^^")
				types_tmp = [db_self.type_m4_g1_1r]
			elif db_self.m4_questions_g1==2:
				print("^^db_self.games_db_self.m4_questions_g1==2^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r]
			elif db_self.m4_questions_g1==3:
				print("^^db_self.games_db_self.m4_questions_g1==3^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r,db_self.type_m4_g1_3r]
			elif db_self.m4_questions_g1==4:
				print("^^db_self.games_db_self.m4_questions_g1==4^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r,db_self.type_m4_g1_3r,db_self.type_m4_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass4.append(types_tmp)
			if db_self.m4_questions_g2==1:
				print("^^db_self.games_db_self.m4_questions_g2==1^^")
				types_tmp = [db_self.type_m4_g2_1r]
			elif db_self.m4_questions_g2==2:
				print("^^db_self.games_db_self.m4_questions_g2==2^^")
				types_tmp = [db_self.type_m4_g2_1r,db_self.type_m4_g2_2r]
			elif db_self.m4_questions_g2==3:
				print("^^db_self.games_db_self.m4_questions_g2==3^^")
				types_tmp = [db_self.type_m4_g2_1r,db_self.type_m4_g2_2r,db_self.type_m4_g2_3r]
			elif db_self.m4_questions_g2==4:
				print("^^db_self.games_db_self.m4_questions_g2==4^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r,db_self.type_m4_g2_3r,db_self.type_m4_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass4.append(types_tmp)
			if db_self.m4_questions_g3==1:
				print("^^db_self.games_db_self.m4_questions_g3==1^^")
				types_tmp = [db_self.type_m4_g3_1r]
			elif db_self.m4_questions_g3==3:
				print("^^db_self.games_db_self.m4_questions_g3==2^^")
				types_tmp = [db_self.type_m4_g3_1r,db_self.type_m4_g3_2r]
			elif db_self.m4_questions_g3==3:
				print("^^db_self.games_db_self.m4_questions_g3==3^^")
				types_tmp = [db_self.type_m4_g3_1r,db_self.type_m4_g3_2r,db_self.type_m4_g3_3r]
			elif db_self.m4_questions_g3==4:
				print("^^db_self.games_db_self.m4_questions_g3==4^^")
				types_tmp = [db_self.type_m4_g3_1r,db_self.type_m4_g3_2r,db_self.type_m4_g3_3r,db_self.type_m4_g3_4r]
			types_tmp = list(set(types_tmp))		
			types_to_pass4.append(types_tmp)
		elif db_self.games_m4==4:
			print("!!db_self.games_m4==4!!")
			if db_self.m4_questions_g1==1:
				print("^^db_self.games_db_self.m4_questions_g1==1^^")
				types_tmp = [db_self.type_m4_g1_1r]
			elif db_self.m4_questions_g1==2:
				print("^^db_self.games_db_self.m4_questions_g1==2^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r]
			elif db_self.m4_questions_g1==3:
				print("^^db_self.games_db_self.m4_questions_g1==3^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r,db_self.type_m4_g1_3r]
			elif db_self.m4_questions_g1==4:
				print("^^db_self.games_db_self.m4_questions_g1==4^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g1_2r,db_self.type_m4_g1_3r,db_self.type_m4_g1_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass4.append(types_tmp)
			if db_self.m4_questions_g2==1:
				print("^^db_self.games_db_self.m4_questions_g2==1^^")
				types_tmp = [db_self.type_m4_g2_1r]
			elif db_self.m4_questions_g2==2:
				print("^^db_self.games_db_self.m4_questions_g2==2^^")
				types_tmp = [db_self.type_m4_g2_1r,db_self.type_m4_g2_2r]
			elif db_self.m4_questions_g2==3:
				print("^^db_self.games_db_self.m4_questions_g2==3^^")
				types_tmp = [db_self.type_m4_g2_1r,db_self.type_m4_g2_2r,db_self.type_m4_g2_3r]
			elif db_self.m4_questions_g2==4:
				print("^^db_self.games_db_self.m4_questions_g2==4^^")
				types_tmp = [db_self.type_m4_g2_1r,db_self.type_m4_g2_2r,db_self.type_m4_g2_3r,db_self.type_m4_g2_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass4.append(types_tmp)
			if db_self.m4_questions_g3==1:
				print("^^db_self.games_db_self.m4_questions_g3==1^^")
				types_tmp = [db_self.type_m4_g3_1r]
			elif db_self.m4_questions_g3==2:
				print("^^db_self.games_db_self.m4_questions_g3==2^^")
				types_tmp = [db_self.type_m4_g3_1r,db_self.type_m4_g3_2r]
			elif db_self.m4_questions_g3==3:
				print("^^db_self.games_db_self.m4_questions_g3==3^^")
				types_tmp = [db_self.type_m4_g1_1r,db_self.type_m4_g3_2r,db_self.type_m4_g3_3r]
			elif db_self.m4_questions_g3==4:
				print("^^db_self.games_db_self.m4_questions_g3==4^^")
				types_tmp = [db_self.type_m4_g3_1r,db_self.type_m4_g3_2r,db_self.type_m4_g3_3r,db_self.type_m4_g3_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass4.append(types_tmp)
			if db_self.m4_questions_g4==1:
				print("^^db_self.games_db_self.m4_questions_g4==1^^")
				types_tmp = [db_self.type_m4_g4_1r]
			elif db_self.m4_questions_g4==2:
				print("^^db_self.games_db_self.m4_questions_g4==2^^")
				types_tmp = [db_self.type_m4_g4_1r,db_self.type_m4_g4_2r]
			elif db_self.m4_questions_g4==3:
				print("^^db_self.games_db_self.m4_questions_g4==3^^")
				types_tmp = [db_self.type_m4_g4_1r,db_self.type_m4_g4_2r,db_self.type_m4_g4_3r]
			elif db_self.m4_questions_g4==4:
				print("^^db_self.games_db_self.m4_questions_g4==4^^")
				types_tmp = [db_self.type_m4_g4_1r,db_self.type_m4_g4_2r,db_self.type_m4_g4_3r,db_self.type_m4_g4_4r]
			types_tmp = list(set(types_tmp))
			types_to_pass4.append(types_tmp)
			print("DEFINITIVO PER TUTTI types_to_pass4 {}".format(types_to_pass4))
		print("TYPES BEFORE {}".format(types_to_pass4))
		types_to_pass4 = list(dbem2.iterFlatten(types_to_pass4))
		print("TYPES AFTER FLAT {}".format(types_to_pass4))
		scen_4_is = extract_one_scenario(db_self,db_self.kid_id,db_self.difficulty_mat_4,table,types_to_pass4)
		print("scen_4_is")
		print(scen_4_is)
		scen.append(scen_4_is)

	scen = list(dbem2.iterFlatten(scen))
	print("SCENSCENASCENASCENSCENASCENASCENSCENASCENASCENSCENASCENA AFTER FLAT {}".format(scen))
	print("scen FINALE PRIMA DEL FATTACCIO!!! ====> {}".format(scen))
	return scen

cdef pick_plus_choosen_scenario_creation(db_self, str type_game):
	fake_kid_id = 0
	scen_is = []
	fake_difficulty = 'Nothing'
	query_create_csv_scen_type = '''SELECT scenario_group, type 
		FROM Scenarios_Questions sq JOIN Questions q ON sq.audio_id=q.audio ORDER BY scenario_group'''
	db_df = pd.read_sql_query(query_create_csv_scen_type, db_self.conn)
	db_df.to_csv('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/scen_type_populate.csv', index=False)
	file_to_fix = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/scen_type_populate.csv'
	file_fixed_output = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/scen_type_correct.csv'

	df = pd.read_csv(file_to_fix, sep=",")
	df.drop_duplicates(subset=None, inplace=True)
	
	df.to_csv(file_fixed_output, index=False)
	
	table = pd.read_csv('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/scen_type_correct.csv',skiprows=None, engine='python', encoding="utf-8")
	scen_is = extract_one_scenario(db_self,fake_kid_id,fake_difficulty,table,type_game)
	return scen_is

cdef pick_plus_choosen_scenario(db_self, str type_game):
	fake_kid_id = 0
	scen_is = []
	fake_difficulty = 'Nothing'
	table = pd.read_csv('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/scen_type_populate.csv',skiprows=None, engine='python', encoding="utf-8")
	scen_is = extract_one_scenario(db_self,fake_kid_id,fake_difficulty,table,type_game)
	return scen_is