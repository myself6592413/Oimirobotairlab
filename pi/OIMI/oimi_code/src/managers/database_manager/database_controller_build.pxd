# ----------------------------------------------------------
# Build
# ----------------------------------------------------------
cdef build_OimiSettings_table(db_db_self)
cdef build_Allidtokens_table(db_db_self)
cdef build_Kids_table(db_db_self)
cdef build_Kids_Parameters_table(db_self)
cdef build_Patches_table(db_db_self)
cdef build_Scenarios_table(db_db_self)
cdef build_Kind_games_table(db_db_self)
cdef build_Type_games_table(db_db_self)
cdef build_Audios_table(db_db_self)

cdef build_Targets_behaviours_table(db_self)
cdef build_survey_treatment_targets_table(db_self)
cdef build_template_Stages_table(db_self)
cdef build_prompt_Stages_table(db_self)
cdef build_reward_Stages_table(db_self)
cdef build_mood_Stages_table(db_self)
cdef build_reaction_Stages_table(db_self)
cdef build_Substages_table(db_self)
cdef build_Prompt_Substages_table(db_self)
cdef build_Reward_Substages_table(db_self)
cdef build_Mood_Substages_table(db_self)
cdef build_Reaction_Substages_table(db_self)
cdef build_ChildrenSubstages_table(db_self)

cdef build_Therapies_table(db_self)
cdef build_Treatments_table(db_self)
cdef build_Class_free_table(db_self)
cdef build_Entertainments_table(db_self)

cdef build_Questions_table(db_db_self)
cdef build_Games_table(db_db_self)
cdef build_ActivityRules_table(db_self)
cdef build_Matches_table(db_db_self)
cdef build_Sessions_table(db_db_self)

cdef build_Achievements_table(db_self)
cdef build_Symptoms_table(db_self)
cdef build_Issues_table(db_self)
cdef build_Comorbidities_table(db_self)
cdef build_Strengths_table(db_self)
cdef build_Impositions_table(db_self)
cdef build_Needs_table(db_self)


cdef build_bridge_EntertainmentSubstage(db_self)
cdef build_bridge_TherapyTreatment(db_self)
cdef build_bridge_TreatmentSession(db_self)
cdef build_bridge_TreatmentEntertainment(db_self)
cdef build_bridge_TreatmentPastime(db_self)

cdef build_bridge_SessionMatch(db_self)
cdef build_bridge_MatchGame(db_self)
cdef build_bridge_GameQuestion(db_self)
cdef build_bridge_QuestionPatch(db_self)

cdef build_bridge_KidSession(db_self)
cdef build_bridge_KidEntertainment(db_self)
cdef build_bridge_KidTreatment(db_self)
cdef build_bridge_KidTherapy(db_self)
cdef build_bridge_KidMatch(db_self)
cdef build_bridge_KidGame(db_self)
cdef build_bridge_KidQuestion(db_self)

cdef build_bridge_ScenarioQuestion(db_db_self)
cdef build_bridge_ScenarioType(db_db_self)
cdef build_bridge_ScenarioKind(db_db_self)
cdef build_bridge_Scenario4PSUQuestion(db_db_self)
cdef build_bridge_Scenario4PCOQuestion(db_db_self)

cdef build_bridge_KidAchievement(db_self)
cdef build_bridge_KidSymptom(db_self)
cdef build_bridge_KidIssue(db_self)
cdef build_bridge_KidComorbidity(db_self)
cdef build_bridge_KidStrength(db_self)
cdef build_bridge_OldKidSymptom(db_self)
cdef build_bridge_OldKidIssue(db_self)
cdef build_bridge_OldKidNeed(db_self)
cdef build_bridge_OldKidComorbidity(db_self)
cdef build_bridge_OldKidStrength(db_self)

cdef build_bridge_KidNeed(db_self)
cdef build_bridge_KidImposition(db_self)
cdef build_bridge_NeedQuestion(db_self)
cdef build_bridge_ImpositionQuestion(db_self)
cdef build_bridge_SessionEnforcement(db_self)
cdef build_bridge_SessionImposition(db_self)
cdef build_bridge_SessionNeed(db_self)

cdef build_bridge_SessionNote(db_self)

cdef build_bridge_ClassFreeTarget(db_self)
# --------------------------------------------------------------
# Create
# --------------------------------------------------------------
cdef create_Short_recap_all_kids_table(db_self)
cdef create_Full_sessions_played_recap_table(db_self)
cdef create_Full_played_recap_giga_table(db_self)
cdef create_Full_played_recap_with_entertainment(db_self)
cdef fix_contraints_Full_sesions_summary_table(db_self)
cdef fix_contraints_Full_therapies_summary_table(db_self)
cdef fix_contraints_Full_entertainment_played_summary_table(db_self)
cdef create_definitive_recap_table_one(db_self)
cdef create_definitive_recap_table_two(db_self)
cdef compose_definitive_recap(db_self)
# --------------------------------------------------------------
# Construct
# --------------------------------------------------------------
cdef construct_ResultsTotalQuestion_table(db_self)
cdef construct_ResultsTotalKindOfGame_table(db_self)
cdef construct_ResultsKindOfGame_new_table(db_self)
cdef construct_ResultsTotalTypeOfGame_table(db_self)
cdef construct_ResultsTypeOfGame_new_table(db_self)
cdef construct_ResultsTotalGame_table(db_self)
cdef construct_ResultsTotalMatch_table(db_self)
cdef construct_ResultsTotalSession_table(db_self)
# --------------------------------------------------------------
# Fix
# --------------------------------------------------------------
cdef fix_ResultsKindOfGame_new_table(db_self)
# --------------------------------------------------------------
# Link
# --------------------------------------------------------------
cdef build_link_Treatments_Traits(db_self)
