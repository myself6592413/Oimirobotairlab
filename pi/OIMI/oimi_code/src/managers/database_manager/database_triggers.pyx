"""Info:
	Oimi robot database_manager file with triggers that must start in the DatabaseManager setup method. 
	
	_Patches: for setting genre.
	_Deletions: for removing the value/ update also from all tables involved, after a deletion.
	_Disposable: for changing the disposable value, that specify if a session/match/game can be removed without creating problems elsewhere.
	_Errors: to update results of games
	_Needs: to set the proper Need column to invalid 
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
from importlib import reload
from typing import Optional
import oimi_queries_triggers as oqt
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# Triggers
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
cdef enable_duration_adjustment(db_self):
	db_self.curs.execute(oqt.sql_adjsut_date_trigger)
	db_self.conn.commit()

cdef enable_pastimes_trigger(db_self):
	db_self.curs.execute(oqt.sql_pastimes_trigger)
	db_self.conn.commit()

cdef enable_pastimes_trigger_2(db_self):
	db_self.curs.execute(oqt.sql_pastimes_trigger_2)
	db_self.conn.commit()

cdef enable_patches_trigger(db_self):
	db_self.curs.execute(oqt.sql_patches_trigger)
	db_self.conn.commit()

cdef enable_free_trigger(db_self):
	db_self.curs.execute(oqt.sql_free_trigger)
	db_self.conn.commit()

cdef enable_kind_trigger(db_self):
	db_self.curs.execute(oqt.sql_kind_games_trigger)
	db_self.conn.commit()
######################################################################### del
cdef enable_deletion_trigger_question(db_self):
	db_self.curs.execute(oqt.sql_deletion_ques_trigger)
	db_self.conn.commit()
cdef enable_deletion_trigger_audios(db_self):
	db_self.curs.execute(oqt.sql_deletion_audios_trigger)
	db_self.conn.commit()
cdef enable_deletion_trigger_kid_1(db_self):
	db_self.curs.execute(oqt.sql_deletion_kid_trigger_1)
	db_self.conn.commit()
cdef enable_deletion_trigger_kid_2(db_self):
	db_self.curs.execute(oqt.sql_deletion_kid_trigger_2)
	db_self.conn.commit()
cdef enable_deletion_trigger_kid_3(db_self):
	db_self.curs.execute(oqt.sql_deletion_kid_trigger_3)
	db_self.conn.commit()
cdef enable_deletion_trigger_kid_4(db_self):
	db_self.curs.execute(oqt.sql_deletion_kid_trigger_4)
	db_self.conn.commit()
cdef enable_deletion_trigger_kid_5(db_self):
	db_self.curs.execute(oqt.sql_deletion_kid_trigger_5)
	db_self.conn.commit()
cdef enable_deletion_trigger_kid_6(db_self):
	db_self.curs.execute(oqt.sql_deletion_kid_trigger_6)
	db_self.conn.commit()
cdef enable_deletion_trigger_kid_7(db_self):
	db_self.curs.execute(oqt.sql_deletion_kid_trigger_7)
	db_self.conn.commit()

cdef enable_deletion_trigger_kid_achievement(db_self):
	db_self.curs.execute(oqt.sql_deletion_kid_achievement_trigger)
	db_self.conn.commit()
cdef enable_deletion_trigger_kid_imposition(db_self):
	db_self.curs.execute(oqt.sql_deletion_kid_imposition_trigger)
	db_self.conn.commit()
cdef enable_deletion_trigger_kid_symptom(db_self):
	db_self.curs.execute(oqt.sql_deletion_kid_symptom_trigger)
	db_self.conn.commit()
######################################################################### insert / update
### game
cdef enable_update_tot_game_answers(db_self):
	db_self.curs.execute(oqt.sql_err_game_ans_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_game_corrects(db_self):
	db_self.curs.execute(oqt.sql_err_game_corr_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_game_errors(db_self):
	db_self.curs.execute(oqt.sql_err_game_errs_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_game_indecisions(db_self):
	db_self.curs.execute(oqt.sql_err_game_ind_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_game_already(db_self):
	db_self.curs.execute(oqt.sql_err_game_already_trigger_after_update)
	db_self.conn.commit()
cdef enable_insert_tot_game_answers(db_self):
	db_self.curs.execute(oqt.sql_err_game_ans_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_game_corrects(db_self):
	db_self.curs.execute(oqt.sql_err_game_corr_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_game_errors(db_self):
	db_self.curs.execute(oqt.sql_err_game_errs_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_game_indecisions(db_self):
	db_self.curs.execute(oqt.sql_err_game_ind_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_game_already(db_self):
	db_self.curs.execute(oqt.sql_err_game_already_trigger_1_after_insert)
	db_self.conn.commit()
### match
cdef enable_update_tot_match_answers(db_self):
	db_self.curs.execute(oqt.sql_err_match_ans_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_match_corrects(db_self):
	db_self.curs.execute(oqt.sql_err_match_corr_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_match_errors(db_self):
	db_self.curs.execute(oqt.sql_err_match_errs_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_match_indecisions(db_self):
	db_self.curs.execute(oqt.sql_err_match_ind_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_match_already(db_self):
	db_self.curs.execute(oqt.sql_err_match_already_trigger_after_update)
	db_self.conn.commit()
cdef enable_insert_tot_match_answers(db_self):
	db_self.curs.execute(oqt.sql_err_match_ans_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_match_corrects(db_self):
	db_self.curs.execute(oqt.sql_err_match_corr_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_match_errors(db_self):
	db_self.curs.execute(oqt.sql_err_match_errs_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_match_incecisions(db_self):
	db_self.curs.execute(oqt.sql_err_match_ind_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_match_already(db_self):
	db_self.curs.execute(oqt.sql_err_match_already_trigger_1_after_insert)
	db_self.conn.commit()
### sess
cdef enable_update_tot_sess_answers(db_self):
	db_self.curs.execute(oqt.sql_err_sess_ans_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_sess_corrects(db_self):
	db_self.curs.execute(oqt.sql_err_sess_corr_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_sess_errors(db_self):
	db_self.curs.execute(oqt.sql_err_sess_errs_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_sess_indecisions(db_self):
	db_self.curs.execute(oqt.sql_err_sess_ind_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_sess_already(db_self):
	db_self.curs.execute(oqt.sql_err_sess_already_trigger_after_update)
	db_self.conn.commit()
cdef enable_insert_tot_sess_answers(db_self):
	db_self.curs.execute(oqt.sql_err_sess_ans_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_sess_corrects(db_self):
	db_self.curs.execute(oqt.sql_err_sess_corr_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_sess_errors(db_self):
	db_self.curs.execute(oqt.sql_err_sess_errs_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_sess_indecisions(db_self):
	db_self.curs.execute(oqt.sql_err_sess_ind_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_sess_already(db_self):
	db_self.curs.execute(oqt.sql_err_sess_already_trigger_1_after_insert)
	db_self.conn.commit()
### tot kind
cdef enable_update_tot_kind_answers(db_self):
	db_self.curs.execute(oqt.sql_err_kind_ans_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_kind_corrects(db_self):
	db_self.curs.execute(oqt.sql_err_kind_corr_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_kind_errors(db_self):
	db_self.curs.execute(oqt.sql_err_kind_errs_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_kind_indecisions(db_self):
	db_self.curs.execute(oqt.sql_err_kind_ind_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_kind_already(db_self):
	db_self.curs.execute(oqt.sql_err_kind_already_trigger_after_update)
	db_self.conn.commit()
cdef enable_insert_tot_kind_answers(db_self):
	db_self.curs.execute(oqt.sql_err_sess_ans_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_kind_corrects(db_self):
	db_self.curs.execute(oqt.sql_err_sess_corr_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_kind_errors(db_self):
	db_self.curs.execute(oqt.sql_err_sess_errs_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_kind_indecisions(db_self):
	db_self.curs.execute(oqt.sql_err_sess_ind_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_kind_already(db_self):
	db_self.curs.execute(oqt.sql_err_sess_already_trigger_1_after_insert)
	db_self.conn.commit()
### tot type
cdef enable_update_tot_type_answers(db_self):
	db_self.curs.execute(oqt.sql_type_ans_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_type_corrects(db_self):
	db_self.curs.execute(oqt.sql_type_corr_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_type_errors(db_self):
	db_self.curs.execute(oqt.sql_type_errs_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_type_indecisions(db_self):
	db_self.curs.execute(oqt.sql_type_ind_trigger_after_update)
	db_self.conn.commit()
cdef enable_update_tot_type_already(db_self):
	db_self.curs.execute(oqt.sql_type_already_trigger_after_update)
	db_self.conn.commit()
cdef enable_insert_tot_type_answers(db_self):
	db_self.curs.execute(oqt.sql_type_ans_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_type_corrects(db_self):
	db_self.curs.execute(oqt.sql_type_corr_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_type_errors(db_self):
	db_self.curs.execute(oqt.sql_type_errs_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_type_indecisions(db_self):
	db_self.curs.execute(oqt.sql_type_ind_trigger_1_after_insert)
	db_self.conn.commit()
cdef enable_insert_tot_type_already(db_self):
	db_self.curs.execute(oqt.sql_type_already_trigger_1_after_insert)
	db_self.conn.commit()
######################################################################### disposable
cdef enable_disposable_trigger_11(db_self):
	db_self.curs.execute(oqt.sql_disposable_trigger_11)
	db_self.conn.commit()
cdef enable_disposable_trigger_12(db_self):
	db_self.curs.execute(oqt.sql_disposable_trigger_12)
	db_self.conn.commit()
cdef enable_disposable_trigger_21(db_self):
	db_self.curs.execute(oqt.sql_disposable_trigger_21)
	db_self.conn.commit()
cdef enable_disposable_trigger_22(db_self):
	db_self.curs.execute(oqt.sql_disposable_trigger_22)
	db_self.conn.commit()
	
cdef enable_needs_trigger_1(db_self):
	db_self.curs.execute(oqt.sql_needs_trigger_1)
	db_self.conn.commit()
cdef enable_needs_trigger_2(db_self):
	db_self.curs.execute(oqt.sql_needs_trigger_2)
	db_self.conn.commit()
cdef enable_needs_trigger_3(db_self):
	db_self.curs.execute(oqt.sql_needs_trigger_3)
	db_self.conn.commit()