# ----------------------------------------------------------------------------------------------------------------------------------------
# Online creations
# ----------------------------------------------------------------------------------------------------------------------------------------
cdef generate_audio_intro_exit_session_table(db_self)
cdef generate_audio_intro_exit_match_table(db_self)
cdef generate_audio_intro_exit_game_table(db_self)
cdef change_num_questions_of_a_game(db_self, int numq, int idg)
cdef change_all_advisable_times_all_matches(db_self)
cdef change_all_desirable_times_all_sessions(db_self)
cdef change_all_durations_or_kick_date_therapies(db_self, old_ther, date_flag)
cdef change_day_treatments(db_self, old_tre)
