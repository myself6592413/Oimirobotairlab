""" Oimi robot database_manager,
	Notes:
		For other detailed info, look at dabatase_doc
	Created by Colombo Giacomo, Politecnico di Milano 2020 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os 
import mmap
import random
import pickle
from typing import Optional
import db_extern_methods as dbem
import db_extern_methods_two as dbem2
cimport modify_default_lists as mdl
cimport database_controller_add_into as dca
cimport add_prediction_5 as adp5
# ==========================================================================================================================================================
# Classes
# ==========================================================================================================================================================
cdef link_to_last_session_plus_choosen(db_self):
	print("STO ENTRANDO IN link_to_last_session_plus_choosen")
	db_self.curs("SELECT kind, type FROM Questions WHERE question_id = ?", (db_self.imposed_ques_id))
	res_imp_ques = db_self.curs.fetchall()
	imp_ques = [item for sublist in res_imp_ques for item in sublist]
	kind_imp_ques = imp_ques[0]
	type_imp_ques = imp_ques[1] 
	print("type_imp_questype_imp_questype_imp_questype_imp_ques {}".format(type_imp_ques))
	scen = adp5.pick_plus_choosen_scenario(db_self, type_imp_ques)
	num_of_games = 1 
	disposable = 0
	db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
	sess_last = db_self.curs.fetchall()[0][0]
	#prendo stesse caratteristiche match appena creato
	db_self.curs.execute("SELECT difficulty, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement FROM Matches ORDER BY match_id DESC LIMIT 1")
	resp_last = db_self.curs.fetchall()
	match_last = [item for sublist in resp_last for item in sublist]
	dif = match_last[0]
	var = match_last[1]
	ord_d_g = match_last[2]
	ord_q_q = match_last[3]
	crite = match_last[4]
	add_new_Match_table_with_kid(db_self, dif, var, num_of_games, ord_d_g, ord_q_q, crite, 5, scen, 0)
	db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
	match_last = db_self.curs.fetchall()[0][0]
	dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
	cas_questions_game = random.randint(1,4)
	miscell = random.randint(0,1)
	if cas_questions_game>1:
		if miscell:
			type_game = 'MISCELLANEOUS'
			db_self.curs.execute("SELECT type FROM Questions WHERE kind = ? ORDER BY RANDOM() LIMIT ?",(kind_imp_ques,cas_questions_game,))
			resp_types = db_self.curs.fetchall()
			all_types_game = [item for sublist in resp_types for item in sublist] 
	else:
		type_game = type_imp_ques
	kind_game = kind_imp_ques
	diff_game = dif
	game_order_diff_ques = 'same'
	necessary_time = 120
	db_self.ask_never_asked_q = 0
	game_idx = add_new_Game_table_with_kid(db_self, cas_questions_game, kind_game, type_game, all_types_game, diff_game, game_order_diff_ques, necessary_time, scen, 0)
	dca.add_new_MatchGame_table(db_self, match_last, game_idx)

cdef addition_to_add_predic(db_self, scen):
	print("SONO ENTRATO IN addition_to_add_predic ")
	db_self.disposable = 0
	db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
	sess_last = db_self.curs.fetchall()[0][0]
	print("----->SCENARIO COME MI ARRIVA DA PRED3 {} !!!!".format(scen))
	if db_self.num_of_matches==1:
		print("add_pre_5  addition_to_add_predic nuuuuu 1 ")
		scenario_m1 = scen[0]
		add_new_Match_table_with_kid(db_self, db_self.difficulty_m1, db_self.games_m1, db_self.variety_m1, db_self.order_diff_games_m1, db_self.order_quan_quest_m1, db_self.criteria_m1, db_self.advisable_time_m1, scenario_m1, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last = db_self.curs.fetchall()[0][0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last)
		all_types_game1_m1 = []
		all_types_game2_m1 = []
		all_types_game3_m1 = []
		all_types_game4_m1 = []
		if db_self.games_m1==1:
			db_self.variety_m1 = db_self.kind_m1_g1
		elif db_self.games_m1==2:
			if db_self.kind_m1_g1==db_self.kind_m1_g2:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2]
		elif db_self.games_m1==3:
			if db_self.kind_m1_g1==db_self.kind_m1_g2==db_self.kind_m1_g3:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2,db_self.kind_m1_g3]
		elif db_self.games_m1==4:
			if db_self.kind_m1_g1==db_self.kind_m1_g2==db_self.kind_m1_g3==db_self.kind_m1_g4:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2,db_self.kind_m1_g3,db_self.kind_m1_g4]
		if db_self.variety_m1=='SUNDRY':
			db_self.criteria_m1 = dbem2.find_criteria(list_kinds)
		else:
			db_self.criteria_m1 = 'ignore'

		if db_self.m1_questions_g1==1:
			db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
			print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
		elif db_self.m1_questions_g1==2:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))
		elif db_self.m1_questions_g1==3:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r==db_self.type_m1_g1_3r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				all_types_game1_m1.append(db_self.type_m1_g1_3r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))					
		elif db_self.m1_questions_g1==4:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r==db_self.type_m1_g1_3r==db_self.type_m1_g1_4r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				all_types_game1_m1.append(db_self.type_m1_g1_3r)
				all_types_game1_m1.append(db_self.type_m1_g1_4r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))
		if db_self.m1_questions_g2==1:
			db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
			print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
		elif db_self.m1_questions_g2==2:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))					
		elif db_self.m1_questions_g2==3:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r==db_self.type_m1_g2_3r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				all_types_game2_m1.append(db_self.type_m1_g2_3r)
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))
		elif db_self.m1_questions_g2==4:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r==db_self.type_m1_g2_3r==db_self.type_m1_g2_4r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				all_types_game2_m1.append(db_self.type_m1_g2_3r)
				all_types_game2_m1.append(db_self.type_m1_g2_4r)					
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))					
		if db_self.m1_questions_g3==1:
			db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
			print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
		elif db_self.m1_questions_g3==2:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))
		elif db_self.m1_questions_g3==3:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r==db_self.type_m1_g3_3r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				all_types_game3_m1.append(db_self.type_m1_g3_3r)
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))					
		elif db_self.m1_questions_g3==4:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r==db_self.type_m1_g3_3r==db_self.type_m1_g3_4r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				all_types_game3_m1.append(db_self.type_m1_g3_3r)					
				all_types_game3_m1.append(db_self.type_m1_g3_4r)					
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))
		if db_self.m1_questions_g4==1:
			db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
			print("TIPO GIOCO G4 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
		elif db_self.m1_questions_g4==2:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))					
		elif db_self.m1_questions_g4==3:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r==db_self.type_m1_g4_3r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				all_types_game4_m1.append(db_self.type_m1_g4_3r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))
		elif db_self.m1_questions_g4==4:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r==db_self.type_m1_g4_3r==db_self.type_m1_g4_4r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				all_types_game4_m1.append(db_self.type_m1_g4_3r)
				all_types_game4_m1.append(db_self.type_m1_g4_4r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))
		if db_self.games_m1==1:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last, game_idx)
		if db_self.games_m1==2:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last, game_idx)
		if db_self.games_m1==3:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g3, db_self.kind_m1_g3, db_self.type_game_g3_m1, all_types_game3_m1, db_self.diff_m1_g3, db_self.m1_g3_order_diff_ques, db_self.necessary_time_g3_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last, game_idx)	
		if db_self.games_m1==4:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g3, db_self.kind_m1_g3, db_self.type_game_g3_m1, all_types_game3_m1, db_self.diff_m1_g3, db_self.m1_g3_order_diff_ques, db_self.necessary_time_g3_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g4, db_self.kind_m1_g4, db_self.type_game_g4_m1, all_types_game4_m1, db_self.diff_m1_g4, db_self.m1_g4_order_diff_ques, db_self.necessary_time_g4_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last, game_idx)	
	
		if db_self.plus_one_game:
			#add_game se c'è una domanda iomposta 
			link_to_last_session_plus_choosen(db_self)
		print("GAME OVER OVER OVER OVER GAME OVER GAME OVERGAME OVERGAME OVERGAME OVERGAME OVER GAME OVERGAME OVERGAME OVERGAME OVERGAME OVER OVER OVER")

	elif db_self.num_of_matches==2:
		print("add_pre_5  addition_to_add_predic nuuuuu 2 ")
		scenario_m1 = scen[0]
		scenario_m2 = scen[1]
		add_new_Match_table_with_kid(db_self, db_self.difficulty_m1, db_self.games_m1, db_self.variety_m1, db_self.order_diff_games_m1, db_self.order_quan_quest_m1, db_self.criteria_m1, db_self.advisable_time_m1, scenario_m1, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last_1 = db_self.curs.fetchall()[0][0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last_1)
		add_new_Match_table_with_kid(db_self, db_self.difficulty_m2, db_self.games_m2, db_self.variety_m2, db_self.order_diff_games_m2, db_self.order_quan_quest_m2, db_self.criteria_m2, db_self.advisable_time_m2, scenario_m2, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last_2 = db_self.curs.fetchall()[0][0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last_2)
		all_types_game1_m1 = []
		all_types_game2_m1 = []
		all_types_game3_m1 = []
		all_types_game4_m1 = []
		all_types_game1_m2 = []
		all_types_game2_m2 = []
		all_types_game3_m2 = []
		all_types_game4_m2 = []
		if db_self.games_m1==1:
			db_self.variety_m1 = db_self.kind_m1_g1
		elif db_self.games_m1==2:
			if db_self.kind_m1_g1==db_self.kind_m1_g2:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2]
		elif db_self.games_m1==3:
			if db_self.kind_m1_g1==db_self.kind_m1_g2==db_self.kind_m1_g3:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2,db_self.kind_m1_g3]
		elif db_self.games_m1==4:
			if db_self.kind_m1_g1==db_self.kind_m1_g2==db_self.kind_m1_g3==db_self.kind_m1_g4:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2,db_self.kind_m1_g3,db_self.kind_m1_g4]
		if db_self.games_m2==1:
			db_self.variety_m2 = db_self.kind_m2_g1
			db_self.criteria_m1 = 'ignore'
		elif db_self.games_m2==2:
			if db_self.kind_m2_g1==db_self.kind_m2_g2:
				db_self.variety_m2 = db_self.kind_m2_g1
			else:
				db_self.variety_m2 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2]
		elif db_self.games_m2==3:
			if db_self.kind_m2_g1==db_self.kind_m2_g2==db_self.kind_m2_g3:
				db_self.variety_m2 = db_self.kind_m2_g1
			else:
				db_self.variety_m2 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2,db_self.kind_m1_g3]
		elif db_self.games_m2==4:
			if db_self.kind_m2_g1==db_self.kind_m2_g2==db_self.kind_m2_g3==db_self.kind_m2_g4:
				db_self.variety_m2 = db_self.kind_m2_g1
			else:
				db_self.variety_m2 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2,db_self.kind_m1_g3,db_self.kind_m1_g4]
		if db_self.variety_m1=='SUNDRY':
			db_self.criteria_m1 = dbem2.find_criteria(list_kinds)
		else:
			db_self.criteria_m1 = 'ignore'
		if db_self.variety_m2=='SUNDRY':
			db_self.criteria_m2 = dbem2.find_criteria(list_kinds)
		else:
			db_self.criteria_m2 = 'ignore'

		if db_self.m1_questions_g1==1:
			db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
			print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
		elif db_self.m1_questions_g1==2:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))
		elif db_self.m1_questions_g1==3:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r==db_self.type_m1_g1_3r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				all_types_game1_m1.append(db_self.type_m1_g1_3r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))					
		elif db_self.m1_questions_g1==4:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r==db_self.type_m1_g1_3r==db_self.type_m1_g1_4r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				all_types_game1_m1.append(db_self.type_m1_g1_3r)
				all_types_game1_m1.append(db_self.type_m1_g1_4r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))
		if db_self.m1_questions_g2==1:
			db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
			print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
		elif db_self.m1_questions_g2==2:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))					
		elif db_self.m1_questions_g2==3:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r==db_self.type_m1_g2_3r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				all_types_game2_m1.append(db_self.type_m1_g2_3r)
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))
		elif db_self.m1_questions_g2==4:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r==db_self.type_m1_g2_3r==db_self.type_m1_g2_4r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				all_types_game2_m1.append(db_self.type_m1_g2_3r)
				all_types_game2_m1.append(db_self.type_m1_g2_4r)					
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))					
		if db_self.m1_questions_g3==1:
			db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
			print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
		elif db_self.m1_questions_g3==2:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))
		elif db_self.m1_questions_g3==3:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r==db_self.type_m1_g3_3r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				all_types_game3_m1.append(db_self.type_m1_g3_3r)
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))					
		elif db_self.m1_questions_g3==4:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r==db_self.type_m1_g3_3r==db_self.type_m1_g3_4r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				all_types_game3_m1.append(db_self.type_m1_g3_3r)					
				all_types_game3_m1.append(db_self.type_m1_g3_4r)					
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))					
		if db_self.m1_questions_g4==1:
			db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
			print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
		elif db_self.m1_questions_g4==2:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))					
		elif db_self.m1_questions_g4==3:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r==db_self.type_m1_g4_3r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				all_types_game4_m1.append(db_self.type_m1_g4_3r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))					
		elif db_self.m1_questions_g4==4:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r==db_self.type_m1_g4_3r==db_self.type_m1_g4_4r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				all_types_game4_m1.append(db_self.type_m1_g4_3r)
				all_types_game4_m1.append(db_self.type_m1_g4_4r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))				
		if db_self.m2_questions_g1==1:
			db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
			print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m2))
		elif db_self.m2_questions_g1==2:
			if db_self.type_m2_g1_1r==db_self.type_m2_g1_2r:
				db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m2))
			else: 
				db_self.type_game_g1_m2 = 'MISCELLANEOUS'
				all_types_game1_m2.append(db_self.type_m2_g1_1r)
				all_types_game1_m2.append(db_self.type_m2_g1_2r)
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m2, all_types_game1_m2))
		elif db_self.m2_questions_g1==3:
			if db_self.type_m2_g1_1r==db_self.type_m2_g1_2r==db_self.type_m2_g1_3r:
				db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m2))
			else: 
				db_self.type_game_g1_m2 = 'MISCELLANEOUS'
				all_types_game1_m2.append(db_self.type_m2_g1_1r)
				all_types_game1_m2.append(db_self.type_m2_g1_2r)
				all_types_game1_m2.append(db_self.type_m2_g1_3r)
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m2, all_types_game1_m2))					
		elif db_self.m2_questions_g1==4:
			if db_self.type_m2_g1_1r==db_self.type_m2_g1_2r==db_self.type_m2_g1_3r==db_self.type_m2_g1_4r:
				db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m2))
			else: 
				db_self.type_game_g1_m2 = 'MISCELLANEOUS'
				all_types_game1_m2.append(db_self.type_m2_g1_1r)
				all_types_game1_m2.append(db_self.type_m2_g1_2r)
				all_types_game1_m2.append(db_self.type_m2_g1_3r)
				all_types_game1_m2.append(db_self.type_m2_g1_4r)
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m2, all_types_game1_m2))
		if db_self.m2_questions_g2==1:
			db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
			print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g2_m2))
		elif db_self.m2_questions_g2==2:
			if db_self.type_m2_g2_1r==db_self.type_m2_g2_2r:
				db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g2_m2))
			else: 
				db_self.type_game_g2_m2 = 'MISCELLANEOUS'
				all_types_game2_m2.append(db_self.type_m2_g2_1r)
				all_types_game2_m2.append(db_self.type_m2_g2_2r)
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m2, all_types_game2_m2))					
		elif db_self.m2_questions_g2==3:
			if db_self.type_m2_g2_1r==db_self.type_m2_g2_2r==db_self.type_m2_g2_3r:
				db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g2_m2))
			else: 
				db_self.type_game_g2_m2 = 'MISCELLANEOUS'
				all_types_game2_m2.append(db_self.type_m2_g2_1r)
				all_types_game2_m2.append(db_self.type_m2_g2_2r)
				all_types_game2_m2.append(db_self.type_m2_g2_3r)
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m2, all_types_game2_m2))
		elif db_self.m2_questions_g2==4:
			if db_self.type_m2_g2_1r==db_self.type_m2_g2_2r==db_self.type_m2_g2_3r==db_self.type_m2_g2_4r:
				db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g2_m2))
			else: 
				db_self.type_game_g2_m2 = 'MISCELLANEOUS'
				all_types_game2_m2.append(db_self.type_m2_g2_1r)
				all_types_game2_m2.append(db_self.type_m2_g2_2r)
				all_types_game2_m2.append(db_self.type_m2_g2_3r)
				all_types_game2_m2.append(db_self.type_m2_g2_4r)					
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m2, all_types_game2_m2))
		if db_self.m2_questions_g3==1:
			db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
			print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
		elif db_self.m2_questions_g3==2:
			if db_self.type_m2_g3_1r==db_self.type_m2_g3_2r:
				db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
			else: 
				db_self.type_game_g3_m2 = 'MISCELLANEOUS'
				all_types_game3_m2.append(db_self.type_m2_g3_1r)
				all_types_game3_m2.append(db_self.type_m2_g3_2r)
				print("TIPO GIOCO G3 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m2, all_types_game3_m2))
		elif db_self.m2_questions_g3==3:
			if db_self.type_m2_g3_1r==db_self.type_m2_g3_2r==db_self.type_m2_g3_3r:
				db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
			else: 
				db_self.type_game_g3_m2 = 'MISCELLANEOUS'
				all_types_game3_m2.append(db_self.type_m2_g3_1r)
				all_types_game3_m2.append(db_self.type_m2_g3_2r)
				all_types_game3_m2.append(db_self.type_m2_g3_3r)
				print("TIPO GIOCO G3 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m2, all_types_game3_m2))					
		elif db_self.m2_questions_g3==4:
			if db_self.type_m2_g3_1r==db_self.type_m2_g3_2r==db_self.type_m2_g3_3r==db_self.type_m2_g3_4r:
				db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
			else: 
				db_self.type_game_g3_m2 = 'MISCELLANEOUS'
				all_types_game3_m2.append(db_self.type_m2_g3_1r)
				all_types_game3_m2.append(db_self.type_m2_g3_2r)
				all_types_game3_m2.append(db_self.type_m2_g3_3r)					
				all_types_game3_m2.append(db_self.type_m2_g3_4r)					
				print("TIPO GIOCO G3 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m2, all_types_game3_m2))					
		if db_self.m2_questions_g4==1:
			db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
			print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g4_m2))
		elif db_self.m2_questions_g4==2:
			if db_self.type_m2_g4_1r==db_self.type_m2_g4_2r:
				db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g4_m2))
			else: 
				db_self.type_game_g4_m2 = 'MISCELLANEOUS'
				all_types_game4_m2.append(db_self.type_m2_g4_1r)
				all_types_game4_m2.append(db_self.type_m2_g4_2r)
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m2, all_types_game4_m2))					
		elif db_self.m2_questions_g4==3:
			if db_self.type_m2_g4_1r==db_self.type_m2_g4_2r==db_self.type_m2_g4_3r:
				db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g4_m2))
			else: 
				db_self.type_game_g4_m2 = 'MISCELLANEOUS'
				all_types_game4_m2.append(db_self.type_m2_g4_1r)
				all_types_game4_m2.append(db_self.type_m2_g4_2r)
				all_types_game4_m2.append(db_self.type_m2_g4_3r)
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m2, all_types_game4_m2))					
		elif db_self.m2_questions_g4==4:
			if db_self.type_m2_g4_1r==db_self.type_m2_g4_2r==db_self.type_m2_g4_3r==db_self.type_m2_g4_4r:
				db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g4_m2))
			else: 
				db_self.type_game_g4_m2 = 'MISCELLANEOUS'
				all_types_game4_m2.append(db_self.type_m2_g4_1r)
				all_types_game4_m2.append(db_self.type_m2_g4_2r)
				all_types_game4_m2.append(db_self.type_m2_g4_3r)
				all_types_game4_m2.append(db_self.type_m2_g4_4r)
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m2, all_types_game4_m2))				
		if db_self.games_m1==1:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
		if db_self.games_m1==2:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
		if db_self.games_m1==3:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g3, db_self.kind_m1_g3, db_self.type_game_g3_m1, all_types_game3_m1, db_self.diff_m1_g3, db_self.m1_g3_order_diff_ques, db_self.necessary_time_g3_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
		if db_self.games_m1==4:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g3, db_self.kind_m1_g3, db_self.type_game_g3_m1, all_types_game3_m1, db_self.diff_m1_g3, db_self.m1_g3_order_diff_ques, db_self.necessary_time_g3_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g4, db_self.kind_m1_g4, db_self.type_game_g4_m1, all_types_game4_m1, db_self.diff_m1_g4, db_self.m1_g4_order_diff_ques, db_self.necessary_time_g4_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
		if db_self.games_m2==1:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
		if db_self.games_m2==2:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g2, db_self.kind_m2_g2, db_self.type_game_g2_m2, all_types_game2_m2, db_self.diff_m2_g2, db_self.m2_g2_order_diff_ques, db_self.necessary_time_g2_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
		if db_self.games_m2==3:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g2, db_self.kind_m2_g2, db_self.type_game_g2_m2, all_types_game2_m2, db_self.diff_m2_g2, db_self.m2_g2_order_diff_ques, db_self.necessary_time_g2_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g3, db_self.kind_m2_g3, db_self.type_game_g3_m2, all_types_game3_m2, db_self.diff_m2_g3, db_self.m2_g3_order_diff_ques, db_self.necessary_time_g3_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
		if db_self.games_m2==4:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g2, db_self.kind_m2_g2, db_self.type_game_g2_m2, all_types_game2_m2, db_self.diff_m2_g2, db_self.m2_g2_order_diff_ques, db_self.necessary_time_g2_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g3, db_self.kind_m2_g3, db_self.type_game_g3_m2, all_types_game3_m2, db_self.diff_m2_g3, db_self.m2_g3_order_diff_ques, db_self.necessary_time_g3_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g4, db_self.kind_m2_g4, db_self.type_game_g4_m2, all_types_game4_m2, db_self.diff_m2_g4, db_self.m2_g4_order_diff_ques, db_self.necessary_time_g4_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	

		if db_self.plus_one_game:
			#add_game se c'è una domanda iomposta 
			link_to_last_session_plus_choosen(db_self)
		print("GAME OVER OVER OVER OVER GAME OVER GAME OVERGAME OVERGAME OVERGAME OVERGAME OVER GAME OVERGAME OVERGAME OVERGAME OVERGAME OVER OVER OVER")

	elif db_self.num_of_matches==3:
		addition_to_add_predic_nma3(db_self, scen)
	elif db_self.num_of_matches==4:
		addition_to_add_predic_nma4(db_self, scen)

cdef addition_to_add_predic_nma3(db_self, scen):
	print("SONO ENTRATO IN addition_to_add_predic numero 3 match")
	db_self.disposable = 0
	db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
	sess_last = db_self.curs.fetchall()[0][0]
	if db_self.num_of_matches==3:
		print("add_pre_5 addition_to_add_predic nuuuuu 3 ")
		scenario_m1 = scen[0]
		scenario_m2 = scen[1]
		scenario_m3 = scen[2]
		add_new_Match_table_with_kid(db_self, db_self.difficulty_m1, db_self.games_m1, db_self.variety_m1, db_self.order_diff_games_m1, db_self.order_quan_quest_m1, db_self.criteria_m1, db_self.advisable_time_m1, scenario_m1, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last_1 = db_self.curs.fetchall()[0][0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last_1)
		add_new_Match_table_with_kid(db_self, db_self.difficulty_m2, db_self.games_m2, db_self.variety_m2, db_self.order_diff_games_m2, db_self.order_quan_quest_m2, db_self.criteria_m2, db_self.advisable_time_m2, scenario_m2, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last_2 = db_self.curs.fetchall()[0][0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last_2)
		add_new_Match_table_with_kid(db_self, db_self.difficulty_m3, db_self.games_m3, db_self.variety_m3, db_self.order_diff_games_m3, db_self.order_quan_quest_m3, db_self.criteria_m3, db_self.advisable_time_m3, scenario_m3, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last_3 = db_self.curs.fetchall()[0][0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last_3)	
		all_types_game1_m1 = []
		all_types_game2_m1 = []
		all_types_game3_m1 = []
		all_types_game4_m1 = []
		all_types_game1_m2 = []
		all_types_game2_m2 = []
		all_types_game3_m2 = []
		all_types_game4_m2 = []
		all_types_game1_m3 = []
		all_types_game2_m3 = []
		all_types_game3_m3 = []
		all_types_game4_m3 = []
		if db_self.games_m1==1:
			db_self.variety_m1 = db_self.kind_m1_g1
		elif db_self.games_m1==2:
			if db_self.kind_m1_g1==db_self.kind_m1_g2:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2]
		elif db_self.games_m1==3:
			if db_self.kind_m1_g1==db_self.kind_m1_g2==db_self.kind_m1_g3:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2,db_self.kind_m1_g3]
		elif db_self.games_m1==4:
			if db_self.kind_m1_g1==db_self.kind_m1_g2==db_self.kind_m1_g3==db_self.kind_m1_g4:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2,db_self.kind_m1_g3,db_self.kind_m1_g4]
		if db_self.games_m2==1:
			db_self.variety_m2 = db_self.kind_m2_g1
		elif db_self.games_m2==2:
			if db_self.kind_m2_g1==db_self.kind_m2_g2:
				db_self.variety_m2 = db_self.kind_m2_g1
			else:
				db_self.variety_m2 = 'SUNDRY'
				list_kinds = [db_self.kind_m2_g1,db_self.kind_m2_g2]
		elif db_self.games_m2==3:
			if db_self.kind_m2_g1==db_self.kind_m2_g2==db_self.kind_m2_g3:
				db_self.variety_m2 = db_self.kind_m2_g1
			else:
				db_self.variety_m2 = 'SUNDRY'
				list_kinds = [db_self.kind_m2_g1,db_self.kind_m2_g2,db_self.kind_m2_g3]
		elif db_self.games_m2==4:
			if db_self.kind_m2_g1==db_self.kind_m2_g2==db_self.kind_m2_g3==db_self.kind_m2_g4:
				db_self.variety_m2 = db_self.kind_m2_g1
			else:
				db_self.variety_m2 = 'SUNDRY'
				list_kinds = [db_self.kind_m2_g1,db_self.kind_m2_g2,db_self.kind_m2_g3,db_self.kind_m2_g4]
		if db_self.m1_questions_g1==1:
			db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
		if db_self.games_m3==1:
			db_self.variety_m3 = db_self.kind_m3_g1
		elif db_self.games_m3==2:
			if db_self.kind_m3_g1==db_self.kind_m3_g2:
				db_self.variety_m3 = db_self.kind_m3_g1
			else:
				db_self.variety_m3 = 'SUNDRY'
				list_kinds=[db_self.kind_m3_g1,db_self.kind_m3_g2]
		elif db_self.games_m3==3:
			if db_self.kind_m3_g1==db_self.kind_m3_g2==db_self.kind_m3_g3:
				db_self.variety_m3 = db_self.kind_m3_g1
			else:
				db_self.variety_m3 = 'SUNDRY'
				list_kinds=[db_self.kind_m3_g1,db_self.kind_m3_g2,db_self.kind_m3_g3]
		elif db_self.games_m3==4:
			if db_self.kind_m3_g1==db_self.kind_m3_g2==db_self.kind_m3_g3==db_self.kind_m3_g4:
				db_self.variety_m3 = db_self.kind_m3_g1
			else:
				db_self.variety_m3 = 'SUNDRY'
				list_kinds=[db_self.kind_m3_g1,db_self.kind_m3_g2,db_self.kind_m3_g3,db_self.kind_m3_g4]

		if db_self.variety_m1=='SUNDRY':
			db_self.criteria_m1 = dbem2.find_criteria(list_kinds)
		else:
			db_self.criteria_m1 = 'ignore'
		if db_self.variety_m2=='SUNDRY':
			db_self.criteria_m2 = dbem2.find_criteria(list_kinds)
		else:
			db_self.criteria_m2 = 'ignore'
		if db_self.variety_m3=='SUNDRY':
			db_self.criteria_m3 = dbem2.find_criteria(list_kinds)
		else:
			db_self.criteria_m3 = 'ignore'

		if db_self.m1_questions_g1==1:
			db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
			print("TIPO GIOCO G1 m1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
		elif db_self.m1_questions_g1==2:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))
		elif db_self.m1_questions_g1==3:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r==db_self.type_m1_g1_3r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				all_types_game1_m1.append(db_self.type_m1_g1_3r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))					
		elif db_self.m1_questions_g1==4:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r==db_self.type_m1_g1_3r==db_self.type_m1_g1_4r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				all_types_game1_m1.append(db_self.type_m1_g1_3r)
				all_types_game1_m1.append(db_self.type_m1_g1_4r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))
		if db_self.m1_questions_g2==1:
			db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
			print("TIPO GIOCO G2 m1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
		elif db_self.m1_questions_g2==2:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))					
		elif db_self.m1_questions_g2==3:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r==db_self.type_m1_g2_3r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				all_types_game2_m1.append(db_self.type_m1_g2_3r)
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))
		elif db_self.m1_questions_g2==4:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r==db_self.type_m1_g2_3r==db_self.type_m1_g2_4r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				all_types_game2_m1.append(db_self.type_m1_g2_3r)
				all_types_game2_m1.append(db_self.type_m1_g2_4r)					
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))					
		if db_self.m1_questions_g3==1:
			db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
			print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
		elif db_self.m1_questions_g3==2:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))
		elif db_self.m1_questions_g3==3:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r==db_self.type_m1_g3_3r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				all_types_game3_m1.append(db_self.type_m1_g3_3r)
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))					
		elif db_self.m1_questions_g3==4:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r==db_self.type_m1_g3_3r==db_self.type_m1_g3_4r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				all_types_game3_m1.append(db_self.type_m1_g3_3r)					
				all_types_game3_m1.append(db_self.type_m1_g3_4r)					
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))					
		if db_self.m1_questions_g4==1:
			db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
			print("TIPO GIOCO G4 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
		elif db_self.m1_questions_g4==2:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))					
		elif db_self.m1_questions_g4==3:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r==db_self.type_m1_g4_3r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				all_types_game4_m1.append(db_self.type_m1_g4_3r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))					
		elif db_self.m1_questions_g4==4:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r==db_self.type_m1_g4_3r==db_self.type_m1_g4_4r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				all_types_game4_m1.append(db_self.type_m1_g4_3r)
				all_types_game4_m1.append(db_self.type_m1_g4_4r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))				
		if db_self.m2_questions_g1==1:
			db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
			print("TIPO GIOCO G1 M2!!!! NEW!!!! {}".format(db_self.type_game_g1_m2))
		elif db_self.m2_questions_g1==2:
			if db_self.type_m2_g1_1r==db_self.type_m2_g1_2r:
				db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!! {}".format(db_self.type_game_g1_m2))
			else: 
				db_self.type_game_g1_m2 = 'MISCELLANEOUS'
				all_types_game1_m2.append(db_self.type_m2_g1_1r)
				all_types_game1_m2.append(db_self.type_m2_g1_2r)
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m2, all_types_game1_m2))
		elif db_self.m2_questions_g1==3:
			if db_self.type_m2_g1_1r==db_self.type_m2_g1_2r==db_self.type_m2_g1_3r:
				db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!! {}".format(db_self.type_game_g1_m2))
			else: 
				db_self.type_game_g1_m2 = 'MISCELLANEOUS'
				all_types_game1_m2.append(db_self.type_m2_g1_1r)
				all_types_game1_m2.append(db_self.type_m2_g1_2r)
				all_types_game1_m2.append(db_self.type_m2_g1_3r)
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m2, all_types_game1_m2))					
		elif db_self.m2_questions_g1==4:
			if db_self.type_m2_g1_1r==db_self.type_m2_g1_2r==db_self.type_m2_g1_3r==db_self.type_m2_g1_4r:
				db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!! {}".format(db_self.type_game_g1_m2))
			else: 
				db_self.type_game_g1_m2 = 'MISCELLANEOUS'
				all_types_game1_m2.append(db_self.type_m2_g1_1r)
				all_types_game1_m2.append(db_self.type_m2_g1_2r)
				all_types_game1_m2.append(db_self.type_m2_g1_3r)
				all_types_game1_m2.append(db_self.type_m2_g1_4r)
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m2, all_types_game1_m2))
		if db_self.m2_questions_g2==1:
			db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
			print("TIPO GIOCO G2 M2!!!! NEW!!!! {}".format(db_self.type_game_g2_m2))
		elif db_self.m2_questions_g2==2:
			if db_self.type_m2_g2_1r==db_self.type_m2_g2_2r:
				db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
				print("TIPO GIOCO G2 M2!!!! NEW!!!! {}".format(db_self.type_game_g2_m2))
			else: 
				db_self.type_game_g2_m2 = 'MISCELLANEOUS'
				all_types_game2_m2.append(db_self.type_m2_g2_1r)
				all_types_game2_m2.append(db_self.type_m2_g2_2r)
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m2, all_types_game2_m2))					
		elif db_self.m2_questions_g2==3:
			if db_self.type_m2_g2_1r==db_self.type_m2_g2_2r==db_self.type_m2_g2_3r:
				db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
				print("TIPO GIOCO G2 M2!!!! NEW!!!! {}".format(db_self.type_game_g2_m2))
			else: 
				db_self.type_game_g2_m2 = 'MISCELLANEOUS'
				all_types_game2_m2.append(db_self.type_m2_g2_1r)
				all_types_game2_m2.append(db_self.type_m2_g2_2r)
				all_types_game2_m2.append(db_self.type_m2_g2_3r)
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m2, all_types_game2_m2))
		elif db_self.m2_questions_g2==4:
			if db_self.type_m2_g2_1r==db_self.type_m2_g2_2r==db_self.type_m2_g2_3r==db_self.type_m2_g2_4r:
				db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
				print("TIPO GIOCO G2 M2!!!! NEW!!!! {}".format(db_self.type_game_g2_m2))
			else: 
				db_self.type_game_g2_m2 = 'MISCELLANEOUS'
				all_types_game2_m2.append(db_self.type_m2_g2_1r)
				all_types_game2_m2.append(db_self.type_m2_g2_2r)
				all_types_game2_m2.append(db_self.type_m2_g2_3r)
				all_types_game2_m2.append(db_self.type_m2_g2_4r)					
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m2, all_types_game2_m2))					
		if db_self.m2_questions_g3==1:
			db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
			print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
		elif db_self.m2_questions_g3==2:
			if db_self.type_m2_g3_1r==db_self.type_m2_g3_2r:
				db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
			else: 
				db_self.type_game_g3_m2 = 'MISCELLANEOUS'
				all_types_game3_m2.append(db_self.type_m2_g3_1r)
				all_types_game3_m2.append(db_self.type_m2_g3_2r)
				print("TIPO GIOCO G3 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m2, all_types_game3_m2))
		elif db_self.m2_questions_g3==3:
			if db_self.type_m2_g3_1r==db_self.type_m2_g3_2r==db_self.type_m2_g3_3r:
				db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
			else: 
				db_self.type_game_g3_m2 = 'MISCELLANEOUS'
				all_types_game3_m2.append(db_self.type_m2_g3_1r)
				all_types_game3_m2.append(db_self.type_m2_g3_2r)
				all_types_game3_m2.append(db_self.type_m2_g3_3r)
				print("TIPO GIOCO G3 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m2, all_types_game3_m2))					
		elif db_self.m2_questions_g3==4:
			if db_self.type_m2_g3_1r==db_self.type_m2_g3_2r==db_self.type_m2_g3_3r==db_self.type_m2_g3_4r:
				db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
			else: 
				db_self.type_game_g3_m2 = 'MISCELLANEOUS'
				all_types_game3_m2.append(db_self.type_m2_g3_1r)
				all_types_game3_m2.append(db_self.type_m2_g3_2r)
				all_types_game3_m2.append(db_self.type_m2_g3_3r)					
				all_types_game3_m2.append(db_self.type_m2_g3_4r)					
				print("TIPO GIOCO G3 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m2, all_types_game3_m2))					
		if db_self.m2_questions_g4==1:
			db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
			print("TIPO GIOCO G4 M2!!!! NEW!!!! {}".format(db_self.type_game_g4_m2))
		elif db_self.m2_questions_g4==2:
			if db_self.type_m2_g4_1r==db_self.type_m2_g4_2r:
				db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
				print("TIPO GIOCO G4 M2!!!! NEW!!!! {}".format(db_self.type_game_g4_m2))
			else: 
				db_self.type_game_g4_m2 = 'MISCELLANEOUS'
				all_types_game4_m2.append(db_self.type_m2_g4_1r)
				all_types_game4_m2.append(db_self.type_m2_g4_2r)
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m2, all_types_game4_m2))					
		elif db_self.m2_questions_g4==3:
			if db_self.type_m2_g4_1r==db_self.type_m2_g4_2r==db_self.type_m2_g4_3r:
				db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
				print("TIPO GIOCO G4 M2!!!! NEW!!!! {}".format(db_self.type_game_g4_m2))
			else: 
				db_self.type_game_g4_m2 = 'MISCELLANEOUS'
				all_types_game4_m2.append(db_self.type_m2_g4_1r)
				all_types_game4_m2.append(db_self.type_m2_g4_2r)
				all_types_game4_m2.append(db_self.type_m2_g4_3r)
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m2, all_types_game4_m2))					
		elif db_self.m2_questions_g4==4:
			if db_self.type_m2_g4_1r==db_self.type_m2_g4_2r==db_self.type_m2_g4_3r==db_self.type_m2_g4_4r:
				db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
				print("TIPO GIOCO G4 M2!!!! NEW!!!! {}".format(db_self.type_game_g4_m2))
			else: 
				db_self.type_game_g4_m2 = 'MISCELLANEOUS'
				all_types_game4_m2.append(db_self.type_m2_g4_1r)
				all_types_game4_m2.append(db_self.type_m2_g4_2r)
				all_types_game4_m2.append(db_self.type_m2_g4_3r)
				all_types_game4_m2.append(db_self.type_m2_g4_4r)
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m2, all_types_game4_m2))				
		
		if db_self.m3_questions_g1==1:
			db_self.type_game_g1_m3 = db_self.type_m3_g1_1r
			print("TIPO GIOCO G1 M3!!!! NEW!!!!!{}".format(db_self.type_game_g1_m3))
		elif db_self.m3_questions_g1==2:
			if db_self.type_m3_g1_1r==db_self.type_m3_g1_2r:
				db_self.type_game_g1_m3 = db_self.type_m3_g1_1r
				print("TIPO GIOCO G1 M3!!!! NEW!!!!!{}".format(db_self.type_game_g1_m3))
			else: 
				db_self.type_game_g1_m3 = 'MISCELLANEOUS'
				all_types_game1_m3.append(db_self.type_m3_g1_1r)
				all_types_game1_m3.append(db_self.type_m3_g1_2r)
				print("TIPO GIOCO G1 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m3, all_types_game1_m3))
		elif db_self.m3_questions_g1==3:
			if db_self.type_m3_g1_1r==db_self.type_m3_g1_2r==db_self.type_m3_g1_3r:
				db_self.type_game_g1_m3 = db_self.type_m3_g1_1r
				print("TIPO GIOCO G1 M3!!!! NEW!!!!!{}".format(db_self.type_game_g1_m3))
			else: 
				db_self.type_game_g1_m3 = 'MISCELLANEOUS'
				all_types_game1_m3.append(db_self.type_m3_g1_1r)
				all_types_game1_m3.append(db_self.type_m3_g1_2r)
				all_types_game1_m3.append(db_self.type_m3_g1_3r)
				print("TIPO GIOCO G1 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m3, all_types_game1_m3))					
		elif db_self.m3_questions_g1==4:
			if db_self.type_m3_g1_1r==db_self.type_m3_g1_2r==db_self.type_m3_g1_3r==db_self.type_m3_g1_4r:
				db_self.type_game_g1_m3 = db_self.type_m3_g1_1r
				print("TIPO GIOCO G1 M3!!!! NEW!!!!!{}".format(db_self.type_game_g1_m3))
			else: 
				db_self.type_game_g1_m3 = 'MISCELLANEOUS'
				all_types_game1_m3.append(db_self.type_m3_g1_1r)
				all_types_game1_m3.append(db_self.type_m3_g1_2r)
				all_types_game1_m3.append(db_self.type_m3_g1_3r)
				all_types_game1_m3.append(db_self.type_m3_g1_4r)
				print("TIPO GIOCO G1 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m3, all_types_game1_m3))
		if db_self.m3_questions_g2==1:
			db_self.type_game_g2_m3 = db_self.type_m3_g2_1r
			print("TIPO GIOCO G2 M3!!!! NEW!!!!!{}".format(db_self.type_game_g2_m3))
		elif db_self.m3_questions_g2==2:
			if db_self.type_m3_g2_1r==db_self.type_m3_g2_2r:
				db_self.type_game_g2_m3 = db_self.type_m3_g2_1r
				print("TIPO GIOCO G2 M3!!!! NEW!!!!!{}".format(db_self.type_game_g2_m3))
			else: 
				db_self.type_game_g2_m3 = 'MISCELLANEOUS'
				all_types_game2_m3.append(db_self.type_m3_g2_1r)
				all_types_game2_m3.append(db_self.type_m3_g2_2r)
				print("TIPO GIOCO G2 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m3, all_types_game2_m3))					
		elif db_self.m3_questions_g2==3:
			if db_self.type_m3_g2_1r==db_self.type_m3_g2_2r==db_self.type_m3_g2_3r:
				db_self.type_game_g2_m3 = db_self.type_m3_g2_1r
				print("TIPO GIOCO G2 M3!!!! NEW!!!!!{}".format(db_self.type_game_g2_m3))
			else: 
				db_self.type_game_g2_m3 = 'MISCELLANEOUS'
				all_types_game2_m3.append(db_self.type_m3_g2_1r)
				all_types_game2_m3.append(db_self.type_m3_g2_2r)
				all_types_game2_m3.append(db_self.type_m3_g2_3r)
				print("TIPO GIOCO G2 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m3, all_types_game2_m3))
		elif db_self.m3_questions_g2==4:
			if db_self.type_m3_g2_1r==db_self.type_m3_g2_2r==db_self.type_m3_g2_3r==db_self.type_m3_g2_4r:
				db_self.type_game_g2_m3 = db_self.type_m3_g2_1r
				print("TIPO GIOCO G2 M3!!!! NEW!!!!!{}".format(db_self.type_game_g2_m3))
			else: 
				db_self.type_game_g2_m3 = 'MISCELLANEOUS'
				all_types_game2_m3.append(db_self.type_m3_g2_1r)
				all_types_game2_m3.append(db_self.type_m3_g2_2r)
				all_types_game2_m3.append(db_self.type_m3_g2_3r)
				all_types_game2_m3.append(db_self.type_m3_g2_4r)					
				print("TIPO GIOCO G2 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m3, all_types_game2_m3))					
		if db_self.m3_questions_g3==1:
			db_self.type_game_g3_m3 = db_self.type_m3_g3_1r
			print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g3_m3))
		elif db_self.m3_questions_g3==2:
			if db_self.type_m3_g3_1r==db_self.type_m3_g3_2r:
				db_self.type_game_g3_m3 = db_self.type_m3_g3_1r
				print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g3_m3))
			else: 
				db_self.type_game_g3_m3 = 'MISCELLANEOUS'
				all_types_game3_m3.append(db_self.type_m3_g3_1r)
				all_types_game3_m3.append(db_self.type_m3_g3_2r)
				print("TIPO GIOCO G3 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m3, all_types_game3_m3))
		elif db_self.m3_questions_g3==3:
			if db_self.type_m3_g3_1r==db_self.type_m3_g3_2r==db_self.type_m3_g3_3r:
				db_self.type_game_g3_m3 = db_self.type_m3_g3_1r
				print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g3_m3))
			else: 
				db_self.type_game_g3_m3 = 'MISCELLANEOUS'
				all_types_game3_m3.append(db_self.type_m3_g3_1r)
				all_types_game3_m3.append(db_self.type_m3_g3_2r)
				all_types_game3_m3.append(db_self.type_m3_g3_3r)
				print("TIPO GIOCO G3 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m3, all_types_game3_m3))					
		elif db_self.m3_questions_g3==4:
			if db_self.type_m3_g3_1r==db_self.type_m3_g3_2r==db_self.type_m3_g3_3r==db_self.type_m3_g3_4r:
				db_self.type_game_g3_m3 = db_self.type_m3_g3_1r
				print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g3_m3))
			else: 
				db_self.type_game_g3_m3 = 'MISCELLANEOUS'
				all_types_game3_m3.append(db_self.type_m3_g3_1r)
				all_types_game3_m3.append(db_self.type_m3_g3_2r)
				all_types_game3_m3.append(db_self.type_m3_g3_3r)					
				all_types_game3_m3.append(db_self.type_m3_g3_4r)					
				print("TIPO GIOCO G3 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m3, all_types_game3_m3))					
		if db_self.m3_questions_g4==1:
			db_self.type_game_g4_m3 = db_self.type_m3_g4_1r
			print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g4_m3))
		elif db_self.m3_questions_g4==2:
			if db_self.type_m3_g4_1r==db_self.type_m3_g4_2r:
				db_self.type_game_g4_m3 = db_self.type_m3_g4_1r
				print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g4_m3))
			else: 
				db_self.type_game_g4_m3 = 'MISCELLANEOUS'
				all_types_game4_m3.append(db_self.type_m3_g4_1r)
				all_types_game4_m3.append(db_self.type_m3_g4_2r)
				print("TIPO GIOCO G4 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m3, all_types_game4_m3))					
		elif db_self.m3_questions_g4==3:
			if db_self.type_m3_g4_1r==db_self.type_m3_g4_2r==db_self.type_m3_g4_3r:
				db_self.type_game_g4_m3 = db_self.type_m3_g4_1r
				print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g4_m3))
			else: 
				db_self.type_game_g4_m3 = 'MISCELLANEOUS'
				all_types_game4_m3.append(db_self.type_m3_g4_1r)
				all_types_game4_m3.append(db_self.type_m3_g4_2r)
				all_types_game4_m3.append(db_self.type_m3_g4_3r)
				print("TIPO GIOCO G4 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m3, all_types_game4_m3))					
		elif db_self.m3_questions_g4==4:
			if db_self.type_m3_g4_1r==db_self.type_m3_g4_2r==db_self.type_m3_g4_3r==db_self.type_m3_g4_4r:
				db_self.type_game_g4_m3 = db_self.type_m3_g4_1r
				print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g4_m3))
			else: 
				db_self.type_game_g4_m3 = 'MISCELLANEOUS'
				all_types_game4_m3.append(db_self.type_m3_g4_1r)
				all_types_game4_m3.append(db_self.type_m3_g4_2r)
				all_types_game4_m3.append(db_self.type_m3_g4_3r)
				all_types_game4_m3.append(db_self.type_m3_g4_4r)
				print("TIPO GIOCO G4 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m3, all_types_game4_m3))				
		if db_self.games_m1==1:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
		if db_self.games_m1==2:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
		if db_self.games_m1==3:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g3, db_self.kind_m1_g3, db_self.type_game_g3_m1, all_types_game3_m1, db_self.diff_m1_g3, db_self.m1_g3_order_diff_ques, db_self.necessary_time_g3_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
		if db_self.games_m1==4:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g3, db_self.kind_m1_g3, db_self.type_game_g3_m1, all_types_game3_m1, db_self.diff_m1_g3, db_self.m1_g3_order_diff_ques, db_self.necessary_time_g3_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g4, db_self.kind_m1_g4, db_self.type_game_g4_m1, all_types_game4_m1, db_self.diff_m1_g4, db_self.m1_g4_order_diff_ques, db_self.necessary_time_g4_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
		if db_self.games_m2==1:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
		if db_self.games_m2==2:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g2, db_self.kind_m2_g2, db_self.type_game_g2_m2, all_types_game2_m2, db_self.diff_m2_g2, db_self.m2_g2_order_diff_ques, db_self.necessary_time_g2_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
		if db_self.games_m2==3:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g2, db_self.kind_m2_g2, db_self.type_game_g2_m2, all_types_game2_m2, db_self.diff_m2_g2, db_self.m2_g2_order_diff_ques, db_self.necessary_time_g2_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g3, db_self.kind_m2_g3, db_self.type_game_g3_m2, all_types_game3_m2, db_self.diff_m2_g3, db_self.m2_g3_order_diff_ques, db_self.necessary_time_g3_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
		if db_self.games_m2==4:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g2, db_self.kind_m2_g2, db_self.type_game_g2_m2, all_types_game2_m2, db_self.diff_m2_g2, db_self.m2_g2_order_diff_ques, db_self.necessary_time_g2_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g3, db_self.kind_m2_g3, db_self.type_game_g3_m2, all_types_game3_m2, db_self.diff_m2_g3, db_self.m2_g3_order_diff_ques, db_self.necessary_time_g3_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g4, db_self.kind_m2_g4, db_self.type_game_g4_m2, all_types_game4_m2, db_self.diff_m2_g4, db_self.m2_g4_order_diff_ques, db_self.necessary_time_g4_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
		if db_self.games_m3==1:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g1, db_self.kind_m3_g1, db_self.type_game_g1_m3, all_types_game1_m3, db_self.diff_m3_g1, db_self.m3_g1_order_diff_ques, db_self.necessary_time_g1_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)
		if db_self.games_m3==2:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g1, db_self.kind_m3_g1, db_self.type_game_g1_m3, all_types_game1_m3, db_self.diff_m3_g1, db_self.m3_g1_order_diff_ques, db_self.necessary_time_g1_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g2, db_self.kind_m3_g2, db_self.type_game_g2_m3, all_types_game2_m3, db_self.diff_m3_g2, db_self.m3_g2_order_diff_ques, db_self.necessary_time_g2_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)
		if db_self.games_m3==3:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g1, db_self.kind_m3_g1, db_self.type_game_g1_m3, all_types_game1_m3, db_self.diff_m3_g1, db_self.m3_g1_order_diff_ques, db_self.necessary_time_g1_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g2, db_self.kind_m3_g2, db_self.type_game_g2_m3, all_types_game2_m3, db_self.diff_m3_g2, db_self.m3_g2_order_diff_ques, db_self.necessary_time_g2_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g3, db_self.kind_m3_g3, db_self.type_game_g3_m3, all_types_game3_m3, db_self.diff_m3_g3, db_self.m3_g3_order_diff_ques, db_self.necessary_time_g3_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)	
		if db_self.games_m3==4:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g1, db_self.kind_m3_g1, db_self.type_game_g1_m3, all_types_game1_m3, db_self.diff_m3_g1, db_self.m3_g1_order_diff_ques, db_self.necessary_time_g1_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g2, db_self.kind_m3_g2, db_self.type_game_g2_m3, all_types_game2_m3, db_self.diff_m3_g2, db_self.m3_g2_order_diff_ques, db_self.necessary_time_g2_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g3, db_self.kind_m3_g3, db_self.type_game_g3_m3, all_types_game3_m3, db_self.diff_m3_g3, db_self.m3_g3_order_diff_ques, db_self.necessary_time_g3_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g4, db_self.kind_m3_g4, db_self.type_game_g4_m3, all_types_game4_m3, db_self.diff_m3_g4, db_self.m3_g4_order_diff_ques, db_self.necessary_time_g4_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)	
	
	if db_self.plus_one_game:
		#add_game se c'è una domanda iomposta 
		link_to_last_session_plus_choosen(db_self)
	print("GAME OVER OVER OVER OVER GAME OVER GAME OVERGAME OVERGAME OVERGAME OVERGAME OVER GAME OVERGAME OVERGAME OVERGAME OVERGAME OVER OVER OVER")	
cdef addition_to_add_predic_nma4(db_self, scen):
	print("SONO ENTRATO IN addition_to_add_predic numero 4 match")
	db_self.disposable = 0
	db_self.curs.execute("SELECT session_id FROM Sessions ORDER BY session_id DESC LIMIT 1")
	sess_last = db_self.curs.fetchall()[0][0]	
	if db_self.num_of_matches==4:
		print("add_pre_5  addition_to_add_predic nuuuuu 4 ")
		scenario_m1 = scen[0]
		scenario_m2 = scen[1]
		scenario_m3 = scen[2]				
		scenario_m4 = scen[3]			
		add_new_Match_table_with_kid(db_self, db_self.difficulty_m1, db_self.games_m1, db_self.variety_m1, db_self.order_diff_games_m1, db_self.order_quan_quest_m1, db_self.criteria_m1, db_self.advisable_time_m1, scenario_m1, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last_1 = db_self.curs.fetchall()[0][0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last_1)
		add_new_Match_table_with_kid(db_self, db_self.difficulty_m2, db_self.games_m2, db_self.variety_m2, db_self.order_diff_games_m2, db_self.order_quan_quest_m2, db_self.criteria_m2, db_self.advisable_time_m2, scenario_m2, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last_2 = db_self.curs.fetchall()[0][0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last_2)
		add_new_Match_table_with_kid(db_self, db_self.difficulty_m3, db_self.games_m3, db_self.variety_m3, db_self.order_diff_games_m3, db_self.order_quan_quest_m3, db_self.criteria_m3, db_self.advisable_time_m3, scenario_m3, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last_3 = db_self.curs.fetchall()[0][0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last_3)
		add_new_Match_table_with_kid(db_self, db_self.difficulty_m4, db_self.games_m4, db_self.variety_m4, db_self.order_diff_games_m4, db_self.order_quan_quest_m4, db_self.criteria_m4, db_self.advisable_time_m4, scenario_m4, 0)
		db_self.curs.execute("SELECT match_id FROM Matches ORDER BY match_id DESC LIMIT 1")
		match_last_4 = db_self.curs.fetchall()[0][0]
		dca.add_new_SessionMatch_table(db_self, sess_last, match_last_4)
		all_types_game1_m1 = []
		all_types_game2_m1 = []
		all_types_game3_m1 = []
		all_types_game4_m1 = []
		all_types_game1_m2 = []
		all_types_game2_m2 = []
		all_types_game3_m2 = []
		all_types_game4_m2 = []
		all_types_game1_m3 = []
		all_types_game2_m3 = []
		all_types_game3_m3 = []
		all_types_game4_m3 = []
		all_types_game1_m4 = []
		all_types_game2_m4 = []
		all_types_game3_m4 = []
		all_types_game4_m4 = []
		if db_self.games_m1==1:
			db_self.variety_m1 = db_self.kind_m1_g1
		elif db_self.games_m1==2:
			if db_self.kind_m1_g1==db_self.kind_m1_g2:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2]
		elif db_self.games_m1==3:
			if db_self.kind_m1_g1==db_self.kind_m1_g2==db_self.kind_m1_g3:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2,db_self.kind_m1_g3]
		elif db_self.games_m1==4:
			if db_self.kind_m1_g1==db_self.kind_m1_g2==db_self.kind_m1_g3==db_self.kind_m1_g4:
				db_self.variety_m1 = db_self.kind_m1_g1
			else:
				db_self.variety_m1 = 'SUNDRY'
				list_kinds = [db_self.kind_m1_g1,db_self.kind_m1_g2,db_self.kind_m1_g3,db_self.kind_m1_g4]
		if db_self.games_m2==1:
			db_self.variety_m2 = db_self.kind_m2_g1
		elif db_self.games_m2==2:
			if db_self.kind_m2_g1==db_self.kind_m2_g2:
				db_self.variety_m2 = db_self.kind_m2_g1
			else:
				db_self.variety_m2 = 'SUNDRY'
				list_kinds = [db_self.kind_m2_g1,db_self.kind_m2_g2]
		elif db_self.games_m2==3:
			if db_self.kind_m2_g1==db_self.kind_m2_g2==db_self.kind_m2_g3:
				db_self.variety_m2 = db_self.kind_m2_g1
			else:
				db_self.variety_m2 = 'SUNDRY'
				list_kinds = [db_self.kind_m2_g1,db_self.kind_m2_g2,db_self.kind_m2_g3]
		elif db_self.games_m2==4:
			if db_self.kind_m2_g1==db_self.kind_m2_g2==db_self.kind_m2_g3==db_self.kind_m2_g4:
				db_self.variety_m2 = db_self.kind_m2_g1
			else:
				db_self.variety_m2 = 'SUNDRY'
				list_kinds = [db_self.kind_m2_g1,db_self.kind_m2_g2,db_self.kind_m2_g3,db_self.kind_m2_g4]
		if db_self.m1_questions_g1==1:
			db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
		if db_self.games_m3==1:
			db_self.variety_m3 = db_self.kind_m3_g1
		elif db_self.games_m3==2:
			if db_self.kind_m3_g1==db_self.kind_m3_g2:
				db_self.variety_m3 = db_self.kind_m3_g1
			else:
				db_self.variety_m3 = 'SUNDRY'
				list_kinds=[db_self.kind_m3_g1,db_self.kind_m3_g2]
		elif db_self.games_m3==3:
			if db_self.kind_m3_g1==db_self.kind_m3_g2==db_self.kind_m3_g3:
				db_self.variety_m3 = db_self.kind_m3_g1
			else:
				db_self.variety_m3 = 'SUNDRY'
				list_kinds=[db_self.kind_m3_g1,db_self.kind_m3_g2,db_self.kind_m3_g3]
		elif db_self.games_m3==4:
			if db_self.kind_m3_g1==db_self.kind_m3_g2==db_self.kind_m3_g3==db_self.kind_m3_g4:
				db_self.variety_m3 = db_self.kind_m3_g1
			else:
				db_self.variety_m3 = 'SUNDRY'
				list_kinds=[db_self.kind_m3_g1,db_self.kind_m3_g2,db_self.kind_m3_g3,db_self.kind_m3_g4]

		if db_self.games_m4==1:
			db_self.variety_m4 = db_self.kind_m4_g1
		elif db_self.games_m4==2:
			if db_self.kind_m4_g1==db_self.kind_m4_g2:
				db_self.variety_m4 = db_self.kind_m4_g1
			else:
				db_self.variety_m4 = 'SUNDRY'
				list_kinds=[db_self.kind_m4_g1,db_self.kind_m4_g2]
		elif db_self.games_m4==3:
			if db_self.kind_m4_g1==db_self.kind_m4_g2==db_self.kind_m4_g3:
				db_self.variety_m4 = db_self.kind_m4_g1
			else:
				db_self.variety_m4 = 'SUNDRY'
				list_kinds=[db_self.kind_m4_g1,db_self.kind_m4_g2,db_self.kind_m4_g3]
		elif db_self.games_m4==4:
			if db_self.kind_m4_g1==db_self.kind_m4_g2==db_self.kind_m4_g3==db_self.kind_m4_g4:
				db_self.variety_m4 = db_self.kind_m4_g1
			else:
				db_self.variety_m4 = 'SUNDRY'
				list_kinds=[db_self.kind_m4_g1,db_self.kind_m4_g2,db_self.kind_m4_g3,db_self.kind_m4_g4]

		if db_self.variety_m1=='SUNDRY':
			db_self.criteria_m1 = dbem2.find_criteria(list_kinds)
		else:
			db_self.criteria_m1 = 'ignore'
		if db_self.variety_m2=='SUNDRY':
			db_self.criteria_m2 = dbem2.find_criteria(list_kinds)
		else:
			db_self.criteria_m2 = 'ignore'
		if db_self.variety_m3=='SUNDRY':
			db_self.criteria_m3 = dbem2.find_criteria(list_kinds)
		else:
			db_self.criteria_m3 = 'ignore'
		if db_self.variety_m4=='SUNDRY':
			db_self.criteria_m4 = dbem2.find_criteria(list_kinds)
		else:
			db_self.criteria_m4 = 'ignore'

		if db_self.m1_questions_g1==1:
			db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
			print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
		elif db_self.m1_questions_g1==2:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))
		elif db_self.m1_questions_g1==3:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r==db_self.type_m1_g1_3r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				all_types_game1_m1.append(db_self.type_m1_g1_3r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))					
		elif db_self.m1_questions_g1==4:
			if db_self.type_m1_g1_1r==db_self.type_m1_g1_2r==db_self.type_m1_g1_3r==db_self.type_m1_g1_4r:
				db_self.type_game_g1_m1 = db_self.type_m1_g1_1r
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! {}".format(db_self.type_game_g1_m1))
			else: 
				db_self.type_game_g1_m1 = 'MISCELLANEOUS'
				all_types_game1_m1.append(db_self.type_m1_g1_1r)
				all_types_game1_m1.append(db_self.type_m1_g1_2r)
				all_types_game1_m1.append(db_self.type_m1_g1_3r)
				all_types_game1_m1.append(db_self.type_m1_g1_4r)
				print("TIPO GIOCO G1 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m1, all_types_game1_m1))
		if db_self.m1_questions_g2==1:
			db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
			print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
		elif db_self.m1_questions_g2==2:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))					
		elif db_self.m1_questions_g2==3:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r==db_self.type_m1_g2_3r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				all_types_game2_m1.append(db_self.type_m1_g2_3r)
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))
		elif db_self.m1_questions_g2==4:
			if db_self.type_m1_g2_1r==db_self.type_m1_g2_2r==db_self.type_m1_g2_3r==db_self.type_m1_g2_4r:
				db_self.type_game_g2_m1 = db_self.type_m1_g2_1r
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! {}".format(db_self.type_game_g2_m1))
			else: 
				db_self.type_game_g2_m1 = 'MISCELLANEOUS'
				all_types_game2_m1.append(db_self.type_m1_g2_1r)
				all_types_game2_m1.append(db_self.type_m1_g2_2r)
				all_types_game2_m1.append(db_self.type_m1_g2_3r)
				all_types_game2_m1.append(db_self.type_m1_g2_4r)					
				print("TIPO GIOCO G2 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m1, all_types_game2_m1))					
		if db_self.m1_questions_g3==1:
			db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
			print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
		elif db_self.m1_questions_g3==2:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))
		elif db_self.m1_questions_g3==3:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r==db_self.type_m1_g3_3r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				all_types_game3_m1.append(db_self.type_m1_g3_3r)
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m1, all_types_game3_m1))					
		elif db_self.m1_questions_g3==4:
			if db_self.type_m1_g3_1r==db_self.type_m1_g3_2r==db_self.type_m1_g3_3r==db_self.type_m1_g3_4r:
				db_self.type_game_g3_m1 = db_self.type_m1_g3_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
			else: 
				db_self.type_game_g3_m1 = 'MISCELLANEOUS'
				all_types_game3_m1.append(db_self.type_m1_g3_1r)
				all_types_game3_m1.append(db_self.type_m1_g3_2r)
				all_types_game3_m1.append(db_self.type_m1_g3_3r)					
				all_types_game3_m1.append(db_self.type_m1_g3_4r)					
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g3_m1))
		if db_self.m1_questions_g4==1:
			db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
			print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
		elif db_self.m1_questions_g4==2:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))					
		elif db_self.m1_questions_g4==3:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r==db_self.type_m1_g4_3r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				all_types_game4_m1.append(db_self.type_m1_g4_3r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))					
		elif db_self.m1_questions_g4==4:
			if db_self.type_m1_g4_1r==db_self.type_m1_g4_2r==db_self.type_m1_g4_3r==db_self.type_m1_g4_4r:
				db_self.type_game_g4_m1 = db_self.type_m1_g4_1r
				print("TIPO GIOCO G3 M1!!!! NEW!!!!! {}".format(db_self.type_game_g4_m1))
			else: 
				db_self.type_game_g4_m1 = 'MISCELLANEOUS'
				all_types_game4_m1.append(db_self.type_m1_g4_1r)
				all_types_game4_m1.append(db_self.type_m1_g4_2r)
				all_types_game4_m1.append(db_self.type_m1_g4_3r)
				all_types_game4_m1.append(db_self.type_m1_g4_4r)
				print("TIPO GIOCO G4 M1!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m1, all_types_game4_m1))
		if db_self.m2_questions_g1==1:
			db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
			print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m2))
		elif db_self.m2_questions_g1==2:
			if db_self.type_m2_g1_1r==db_self.type_m2_g1_2r:
				db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m2))
			else: 
				db_self.type_game_g1_m2 = 'MISCELLANEOUS'
				all_types_game1_m2.append(db_self.type_m2_g1_1r)
				all_types_game1_m2.append(db_self.type_m2_g1_2r)
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m2, all_types_game1_m2))
		elif db_self.m2_questions_g1==3:
			if db_self.type_m2_g1_1r==db_self.type_m2_g1_2r==db_self.type_m2_g1_3r:
				db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m2))
			else: 
				db_self.type_game_g1_m2 = 'MISCELLANEOUS'
				all_types_game1_m2.append(db_self.type_m2_g1_1r)
				all_types_game1_m2.append(db_self.type_m2_g1_2r)
				all_types_game1_m2.append(db_self.type_m2_g1_3r)
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m2, all_types_game1_m2))					
		elif db_self.m2_questions_g1==4:
			if db_self.type_m2_g1_1r==db_self.type_m2_g1_2r==db_self.type_m2_g1_3r==db_self.type_m2_g1_4r:
				db_self.type_game_g1_m2 = db_self.type_m2_g1_1r
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! {}".format(db_self.type_game_g1_m2))
			else: 
				db_self.type_game_g1_m2 = 'MISCELLANEOUS'
				all_types_game1_m2.append(db_self.type_m2_g1_1r)
				all_types_game1_m2.append(db_self.type_m2_g1_2r)
				all_types_game1_m2.append(db_self.type_m2_g1_3r)
				all_types_game1_m2.append(db_self.type_m2_g1_4r)
				print("TIPO GIOCO G1 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m2, all_types_game1_m2))
		if db_self.m2_questions_g2==1:
			db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
			print("TIPO GIOCO G2 M2!!!! NEW!!!!! {}".format(db_self.type_game_g2_m2))
		elif db_self.m2_questions_g2==2:
			if db_self.type_m2_g2_1r==db_self.type_m2_g2_2r:
				db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! {}".format(db_self.type_game_g2_m2))
			else: 
				db_self.type_game_g2_m2 = 'MISCELLANEOUS'
				all_types_game2_m2.append(db_self.type_m2_g2_1r)
				all_types_game2_m2.append(db_self.type_m2_g2_2r)
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m2, all_types_game2_m2))					
		elif db_self.m2_questions_g2==3:
			if db_self.type_m2_g2_1r==db_self.type_m2_g2_2r==db_self.type_m2_g2_3r:
				db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! {}".format(db_self.type_game_g2_m2))
			else: 
				db_self.type_game_g2_m2 = 'MISCELLANEOUS'
				all_types_game2_m2.append(db_self.type_m2_g2_1r)
				all_types_game2_m2.append(db_self.type_m2_g2_2r)
				all_types_game2_m2.append(db_self.type_m2_g2_3r)
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m2, all_types_game2_m2))
		elif db_self.m2_questions_g2==4:
			if db_self.type_m2_g2_1r==db_self.type_m2_g2_2r==db_self.type_m2_g2_3r==db_self.type_m2_g2_4r:
				db_self.type_game_g2_m2 = db_self.type_m2_g2_1r
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! {}".format(db_self.type_game_g2_m2))
			else: 
				db_self.type_game_g2_m2 = 'MISCELLANEOUS'
				all_types_game2_m2.append(db_self.type_m2_g2_1r)
				all_types_game2_m2.append(db_self.type_m2_g2_2r)
				all_types_game2_m2.append(db_self.type_m2_g2_3r)
				all_types_game2_m2.append(db_self.type_m2_g2_4r)					
				print("TIPO GIOCO G2 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m2, all_types_game2_m2))					
		if db_self.m2_questions_g3==1:
			db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
			print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
		elif db_self.m2_questions_g3==2:
			if db_self.type_m2_g3_1r==db_self.type_m2_g3_2r:
				db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
				print("TIPO GIOCO G3 M2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
			else: 
				db_self.type_game_g3_m2 = 'MISCELLANEOUS'
				all_types_game3_m2.append(db_self.type_m2_g3_1r)
				all_types_game3_m2.append(db_self.type_m2_g3_2r)
				print("TIPO GIOCO G3 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m2, all_types_game3_m2))
		elif db_self.m2_questions_g3==3:
			if db_self.type_m2_g3_1r==db_self.type_m2_g3_2r==db_self.type_m2_g3_3r:
				db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
			else: 
				db_self.type_game_g3_m2 = 'MISCELLANEOUS'
				all_types_game3_m2.append(db_self.type_m2_g3_1r)
				all_types_game3_m2.append(db_self.type_m2_g3_2r)
				all_types_game3_m2.append(db_self.type_m2_g3_3r)
				print("TIPO GIOCO G3 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m2, all_types_game3_m2))					
		elif db_self.m2_questions_g3==4:
			if db_self.type_m2_g3_1r==db_self.type_m2_g3_2r==db_self.type_m2_g3_3r==db_self.type_m2_g3_4r:
				db_self.type_game_g3_m2 = db_self.type_m2_g3_1r
				print("TIPO GIOCO G3 m2!!!! NEW!!!!! {}".format(db_self.type_game_g3_m2))
			else: 
				db_self.type_game_g3_m2 = 'MISCELLANEOUS'
				all_types_game3_m2.append(db_self.type_m2_g3_1r)
				all_types_game3_m2.append(db_self.type_m2_g3_2r)
				all_types_game3_m2.append(db_self.type_m2_g3_3r)					
				all_types_game3_m2.append(db_self.type_m2_g3_4r)					
				print("TIPO GIOCO G3 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m2, all_types_game3_m2))					
		if db_self.m2_questions_g4==1:
			db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
			print("TIPO GIOCO G4 M2!!!! NEW!!!!! {}".format(db_self.type_game_g4_m2))
		elif db_self.m2_questions_g4==2:
			if db_self.type_m2_g4_1r==db_self.type_m2_g4_2r:
				db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! {}".format(db_self.type_game_g4_m2))
			else: 
				db_self.type_game_g4_m2 = 'MISCELLANEOUS'
				all_types_game4_m2.append(db_self.type_m2_g4_1r)
				all_types_game4_m2.append(db_self.type_m2_g4_2r)
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m2, all_types_game4_m2))					
		elif db_self.m2_questions_g4==3:
			if db_self.type_m2_g4_1r==db_self.type_m2_g4_2r==db_self.type_m2_g4_3r:
				db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! {}".format(db_self.type_game_g4_m2))
			else: 
				db_self.type_game_g4_m2 = 'MISCELLANEOUS'
				all_types_game4_m2.append(db_self.type_m2_g4_1r)
				all_types_game4_m2.append(db_self.type_m2_g4_2r)
				all_types_game4_m2.append(db_self.type_m2_g4_3r)
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m2, all_types_game4_m2))					
		elif db_self.m2_questions_g4==4:
			if db_self.type_m2_g4_1r==db_self.type_m2_g4_2r==db_self.type_m2_g4_3r==db_self.type_m2_g4_4r:
				db_self.type_game_g4_m2 = db_self.type_m2_g4_1r
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! {}".format(db_self.type_game_g4_m2))
			else: 
				db_self.type_game_g4_m2 = 'MISCELLANEOUS'
				all_types_game4_m2.append(db_self.type_m2_g4_1r)
				all_types_game4_m2.append(db_self.type_m2_g4_2r)
				all_types_game4_m2.append(db_self.type_m2_g4_3r)
				all_types_game4_m2.append(db_self.type_m2_g4_4r)
				print("TIPO GIOCO G4 M2!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m2, all_types_game4_m2))				
		
		if db_self.m3_questions_g1==1:
			db_self.type_game_g1_m3 = db_self.type_m3_g1_1r
			print("TIPO GIOCO G1 M3!!!! NEW!!!!! {}".format(db_self.type_game_g1_m3))
		elif db_self.m3_questions_g1==2:
			if db_self.type_m3_g1_1r==db_self.type_m3_g1_2r:
				db_self.type_game_g1_m3 = db_self.type_m3_g1_1r
				print("TIPO GIOCO G1 M3!!!! NEW!!!!! {}".format(db_self.type_game_g1_m3))
			else: 
				db_self.type_game_g1_m3 = 'MISCELLANEOUS'
				all_types_game1_m3.append(db_self.type_m3_g1_1r)
				all_types_game1_m3.append(db_self.type_m3_g1_2r)
				print("TIPO GIOCO G1 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m3, all_types_game1_m3))
		elif db_self.m3_questions_g1==3:
			if db_self.type_m3_g1_1r==db_self.type_m3_g1_2r==db_self.type_m3_g1_3r:
				db_self.type_game_g1_m3 = db_self.type_m3_g1_1r
				print("TIPO GIOCO G1 M3!!!! NEW!!!!! {}".format(db_self.type_game_g1_m3))
			else: 
				db_self.type_game_g1_m3 = 'MISCELLANEOUS'
				all_types_game1_m3.append(db_self.type_m3_g1_1r)
				all_types_game1_m3.append(db_self.type_m3_g1_2r)
				all_types_game1_m3.append(db_self.type_m3_g1_3r)
				print("TIPO GIOCO G1 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m3, all_types_game1_m3))					
		elif db_self.m3_questions_g1==4:
			if db_self.type_m3_g1_1r==db_self.type_m3_g1_2r==db_self.type_m3_g1_3r==db_self.type_m3_g1_4r:
				db_self.type_game_g1_m3 = db_self.type_m3_g1_1r
				print("TIPO GIOCO G1 M3!!!! NEW!!!!! {}".format(db_self.type_game_g1_m3))
			else: 
				db_self.type_game_g1_m3 = 'MISCELLANEOUS'
				all_types_game1_m3.append(db_self.type_m3_g1_1r)
				all_types_game1_m3.append(db_self.type_m3_g1_2r)
				all_types_game1_m3.append(db_self.type_m3_g1_3r)
				all_types_game1_m3.append(db_self.type_m3_g1_4r)
				print("TIPO GIOCO G1 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m3, all_types_game1_m3))
		if db_self.m3_questions_g2==1:
			db_self.type_game_g2_m3 = db_self.type_m3_g2_1r
			print("TIPO GIOCO G2 M3!!!! NEW!!!!! {}".format(db_self.type_game_g2_m3))
		elif db_self.m3_questions_g2==2:
			if db_self.type_m3_g2_1r==db_self.type_m3_g2_2r:
				db_self.type_game_g2_m3 = db_self.type_m3_g2_1r
				print("TIPO GIOCO G2 M3!!!! NEW!!!!! {}".format(db_self.type_game_g2_m3))
			else: 
				db_self.type_game_g2_m3 = 'MISCELLANEOUS'
				all_types_game2_m3.append(db_self.type_m3_g2_1r)
				all_types_game2_m3.append(db_self.type_m3_g2_2r)
				print("TIPO GIOCO G2 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m3, all_types_game2_m3))					
		elif db_self.m3_questions_g2==3:
			if db_self.type_m3_g2_1r==db_self.type_m3_g2_2r==db_self.type_m3_g2_3r:
				db_self.type_game_g2_m3 = db_self.type_m3_g2_1r
				print("TIPO GIOCO G2 M3!!!! NEW!!!!! {}".format(db_self.type_game_g2_m3))
			else: 
				db_self.type_game_g2_m3 = 'MISCELLANEOUS'
				all_types_game2_m3.append(db_self.type_m3_g2_1r)
				all_types_game2_m3.append(db_self.type_m3_g2_2r)
				all_types_game2_m3.append(db_self.type_m3_g2_3r)
				print("TIPO GIOCO G2 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m3, all_types_game2_m3))
		elif db_self.m3_questions_g2==4:
			if db_self.type_m3_g2_1r==db_self.type_m3_g2_2r==db_self.type_m3_g2_3r==db_self.type_m3_g2_4r:
				db_self.type_game_g2_m3 = db_self.type_m3_g2_1r
				print("TIPO GIOCO G2 M3!!!! NEW!!!!! {}".format(db_self.type_game_g2_m3))
			else: 
				db_self.type_game_g2_m3 = 'MISCELLANEOUS'
				all_types_game2_m3.append(db_self.type_m3_g2_1r)
				all_types_game2_m3.append(db_self.type_m3_g2_2r)
				all_types_game2_m3.append(db_self.type_m3_g2_3r)
				all_types_game2_m3.append(db_self.type_m3_g2_4r)					
				print("TIPO GIOCO G2 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m3, all_types_game2_m3))					
		if db_self.m3_questions_g3==1:
			db_self.type_game_g3_m3 = db_self.type_m3_g3_1r
			print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g3_m3))
		elif db_self.m3_questions_g3==2:
			if db_self.type_m3_g3_1r==db_self.type_m3_g3_2r:
				db_self.type_game_g3_m3 = db_self.type_m3_g3_1r
				print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g3_m3))
			else: 
				db_self.type_game_g3_m3 = 'MISCELLANEOUS'
				all_types_game3_m3.append(db_self.type_m3_g3_1r)
				all_types_game3_m3.append(db_self.type_m3_g3_2r)
				print("TIPO GIOCO G3 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m3, all_types_game3_m3))
		elif db_self.m3_questions_g3==3:
			if db_self.type_m3_g3_1r==db_self.type_m3_g3_2r==db_self.type_m3_g3_3r:
				db_self.type_game_g3_m3 = db_self.type_m3_g3_1r
				print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g3_m3))
			else: 
				db_self.type_game_g3_m3 = 'MISCELLANEOUS'
				all_types_game3_m3.append(db_self.type_m3_g3_1r)
				all_types_game3_m3.append(db_self.type_m3_g3_2r)
				all_types_game3_m3.append(db_self.type_m3_g3_3r)
				print("TIPO GIOCO G3 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m3, all_types_game3_m3))					
		elif db_self.m3_questions_g3==4:
			if db_self.type_m3_g3_1r==db_self.type_m3_g3_2r==db_self.type_m3_g3_3r==db_self.type_m3_g3_4r:
				db_self.type_game_g3_m3 = db_self.type_m3_g3_1r
				print("TIPO GIOCO G3 m3!!!! NEW!!!!! {}".format(db_self.type_game_g3_m3))
			else: 
				db_self.type_game_g3_m3 = 'MISCELLANEOUS'
				all_types_game3_m3.append(db_self.type_m3_g3_1r)
				all_types_game3_m3.append(db_self.type_m3_g3_2r)
				all_types_game3_m3.append(db_self.type_m3_g3_3r)					
				all_types_game3_m3.append(db_self.type_m3_g3_4r)					
				print("TIPO GIOCO G3 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m3, all_types_game3_m3))					
		if db_self.m3_questions_g4==1:
			db_self.type_game_g4_m3 = db_self.type_m3_g4_1r
			print("TIPO GIOCO G4 M3!!!! NEW!!!!! {}".format(db_self.type_game_g4_m3))
		elif db_self.m3_questions_g4==2:
			if db_self.type_m3_g4_1r==db_self.type_m3_g4_2r:
				db_self.type_game_g4_m3 = db_self.type_m3_g4_1r
				print("TIPO GIOCO G4 M3!!!! NEW!!!!! {}".format(db_self.type_game_g4_m3))
			else: 
				db_self.type_game_g4_m3 = 'MISCELLANEOUS'
				all_types_game4_m3.append(db_self.type_m3_g4_1r)
				all_types_game4_m3.append(db_self.type_m3_g4_2r)
				print("TIPO GIOCO G4 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m3, all_types_game4_m3))					
		elif db_self.m3_questions_g4==3:
			if db_self.type_m3_g4_1r==db_self.type_m3_g4_2r==db_self.type_m3_g4_3r:
				db_self.type_game_g4_m3 = db_self.type_m3_g4_1r
				print("TIPO GIOCO G4 M3!!!! NEW!!!!! {}".format(db_self.type_game_g4_m3))
			else: 
				db_self.type_game_g4_m3 = 'MISCELLANEOUS'
				all_types_game4_m3.append(db_self.type_m3_g4_1r)
				all_types_game4_m3.append(db_self.type_m3_g4_2r)
				all_types_game4_m3.append(db_self.type_m3_g4_3r)
				print("TIPO GIOCO G4 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m3, all_types_game4_m3))					
		elif db_self.m3_questions_g4==4:
			if db_self.type_m3_g4_1r==db_self.type_m3_g4_2r==db_self.type_m3_g4_3r==db_self.type_m3_g4_4r:
				db_self.type_game_g4_m3 = db_self.type_m3_g4_1r
				print("TIPO GIOCO G4 M3!!!! NEW!!!!! {}".format(db_self.type_game_g4_m3))
			else: 
				db_self.type_game_g4_m3 = 'MISCELLANEOUS'
				all_types_game4_m3.append(db_self.type_m3_g4_1r)
				all_types_game4_m3.append(db_self.type_m3_g4_2r)
				all_types_game4_m3.append(db_self.type_m3_g4_3r)
				all_types_game4_m3.append(db_self.type_m3_g4_4r)
				print("TIPO GIOCO G4 M3!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m3, all_types_game4_m3))				
		all_types_game_m4 = []
		if db_self.m4_questions_g1==1:
			db_self.type_game_g1_m4 = db_self.db_self.type_m4_g4_1r
			print("TIPO GIOCO G1 M4!!!! NEW!!!!! {}".format(db_self.type_game_g1_m4))
		elif db_self.m4_questions_g1==2:
			if db_self.db_self.type_m4_g4_1r==db_self.db_self.type_m4_g4_2r:
				db_self.type_game_g1_m4 = db_self.db_self.type_m4_g4_1r
				print("TIPO GIOCO G1 M4!!!! NEW!!!!! {}".format(db_self.type_game_g1_m4))
			else: 
				db_self.type_game_g1_m4 = 'MISCELLANEOUS'
				all_types_game1_m4.append(db_self.db_self.type_m4_g4_1r)
				all_types_game1_m4.append(db_self.db_self.type_m4_g4_2r)
				print("TIPO GIOCO G1 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m4, all_types_game1_m4))
		elif db_self.m4_questions_g1==3:
			if db_self.db_self.type_m4_g4_1r==db_self.db_self.type_m4_g4_2r==db_self.db_self.type_m4_g4_3r:
				db_self.type_game_g1_m4 = db_self.db_self.type_m4_g4_1r
				print("TIPO GIOCO G1 M4!!!! NEW!!!!! {}".format(db_self.type_game_g1_m4))
			else: 
				db_self.type_game_g1_m4 = 'MISCELLANEOUS'
				all_types_game1_m4.append(db_self.db_self.type_m4_g4_1r)
				all_types_game1_m4.append(db_self.db_self.type_m4_g4_2r)
				all_types_game1_m4.append(db_self.db_self.type_m4_g4_3r)
				print("TIPO GIOCO G1 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m4, all_types_game1_m4))					
		elif db_self.m4_questions_g1==4:
			if db_self.db_self.type_m4_g4_1r==db_self.db_self.type_m4_g4_2r==db_self.db_self.type_m4_g4_3r==db_self.db_self.type_m4_g4_4r:
				db_self.type_game_g1_m4 = db_self.db_self.type_m4_g4_1r
				print("TIPO GIOCO G1 M4!!!! NEW!!!!! {}".format(db_self.type_game_g1_m4))
			else: 
				db_self.type_game_g1_m4 = 'MISCELLANEOUS'
				all_types_game1_m4.append(db_self.db_self.type_m4_g4_1r)
				all_types_game1_m4.append(db_self.db_self.type_m4_g4_2r)
				all_types_game1_m4.append(db_self.db_self.type_m4_g4_3r)
				all_types_game1_m4.append(db_self.db_self.type_m4_g4_4r)
				print("TIPO GIOCO G1 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g1_m4, all_types_game1_m4))
		if db_self.m4_questions_g2==1:
			db_self.type_game_g2_m4 = db_self.type_m4_g2_1r
			print("TIPO GIOCO G2 M4!!!! NEW!!!!! {}".format(db_self.type_game_g2_m4))
		elif db_self.m4_questions_g2==2:
			if db_self.type_m4_g2_1r==db_self.type_m4_g2_2r:
				db_self.type_game_g2_m4 = db_self.type_m4_g2_1r
				print("TIPO GIOCO G2 M4!!!! NEW!!!!! {}".format(db_self.type_game_g2_m4))
			else: 
				db_self.type_game_g2_m4 = 'MISCELLANEOUS'
				all_types_game2_m4.append(db_self.type_m4_g2_1r)
				all_types_game2_m4.append(db_self.type_m4_g2_2r)
				print("TIPO GIOCO G2 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m4, all_types_game2_m4))					
		elif db_self.m4_questions_g2==3:
			if db_self.type_m4_g2_1r==db_self.type_m4_g2_2r==db_self.type_m4_g2_3rh:
				db_self.type_game_g2_m4 = db_self.type_m4_g2_1r
				print("TIPO GIOCO G2 M4!!!! NEW!!!!! {}".format(db_self.type_game_g2_m4))
			else: 
				db_self.type_game_g2_m4 = 'MISCELLANEOUS'
				all_types_game2_m4.append(db_self.type_m4_g2_1r)
				all_types_game2_m4.append(db_self.type_m4_g2_2r)
				all_types_game2_m4.append(db_self.type_m4_g2_3r)
				print("TIPO GIOCO G2 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m4, all_types_game2_m4))
		elif db_self.m4_questions_g2==4:
			if db_self.type_m4_g2_1r==db_self.type_m4_g2_2r==db_self.type_m4_g2_3r==db_self.type_m4_g2_4r:
				db_self.type_game_g2_m4 = db_self.type_m4_g2_1r
				print("TIPO GIOCO G2 M4!!!! NEW!!!!! {}".format(db_self.type_game_g2_m4))
			else: 
				db_self.type_game_g2_m4 = 'MISCELLANEOUS'
				all_types_game2_m4.append(db_self.type_m4_g2_1r)
				all_types_game2_m4.append(db_self.type_m4_g2_2r)
				all_types_game2_m4.append(db_self.type_m4_g2_3r)
				all_types_game2_m4.append(db_self.type_m4_g2_4r)					
				print("TIPO GIOCO G2 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g2_m4, all_types_game2_m4))					
		if db_self.m4_questions_g3==1:
			db_self.type_game_g3_m4 = db_self.type_m4_g3_1r
			print("TIPO GIOCO G2 m4!!!! NEW!!!!! {}".format(db_self.type_game_g3_m4))
		elif db_self.m4_questions_g3==2:
			if db_self.type_m4_g3_1r==db_self.type_m4_g3_2r:
				db_self.type_game_g3_m4 = db_self.type_m4_g3_1r
				print("TIPO GIOCO G3 M4!!!! NEW!!!!!{}".format(db_self.type_game_g3_m4))
			else: 
				db_self.type_game_g3_m4 = 'MISCELLANEOUS'
				all_types_game3_m4.append(db_self.type_m4_g3_1r)
				all_types_game3_m4.append(db_self.type_m4_g3_2r)
				print("TIPO GIOCO G3 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m4, all_types_game3_m4))
		elif db_self.m4_questions_g3==3:
			if db_self.type_m4_g3_1r==db_self.type_m4_g3_2r==db_self.type_m4_g3_3r:
				db_self.type_game_g3_m4 = db_self.type_m4_g3_1r
				print("TIPO GIOCO G3 M4!!!! NEW!!!!!{}".format(db_self.type_game_g3_m4))
			else: 
				db_self.type_game_g3_m4 = 'MISCELLANEOUS'
				all_types_game3_m4.append(db_self.type_m4_g3_1r)
				all_types_game3_m4.append(db_self.type_m4_g3_2r)
				all_types_game3_m4.append(db_self.type_m4_g3_3r)
				print("TIPO GIOCO G3 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m4, all_types_game3_m4))					
		elif db_self.m4_questions_g3==4:
			if db_self.type_m4_g3_1r==db_self.type_m4_g3_2r==db_self.type_m4_g3_3r==db_self.type_m4_g3_4r:
				db_self.type_game_g3_m4 = db_self.type_m4_g3_1r
				print("TIPO GIOCO G3 M4!!!! NEW!!!!!{}".format(db_self.type_game_g3_m4))
			else: 
				db_self.type_game_g3_m4 = 'MISCELLANEOUS'
				all_types_game3_m4.append(db_self.type_m4_g3_1r)
				all_types_game3_m4.append(db_self.type_m4_g3_2r)
				all_types_game3_m4.append(db_self.type_m4_g3_3r)					
				all_types_game3_m4.append(db_self.type_m4_g3_4r)					
				print("TIPO GIOCO G3 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g3_m4, all_types_game3_m4))					
		if db_self.m4_questions_g4==1:
			db_self.type_game_g4_m4 = db_self.type_m4_g4_1r
			print("TIPO GIOCO G4 M4!!!! NEW!!!!!{}".format(db_self.type_game_g4_m4))
		elif db_self.m4_questions_g4==2:
			if db_self.type_m4_g4_1r==db_self.type_m4_g4_2r:
				db_self.type_game_g4_m4 = db_self.type_m4_g4_1r
				print("TIPO GIOCO G4 M4!!!! NEW!!!!!{}".format(db_self.type_game_g4_m4))
			else: 
				db_self.type_game_g4_m4 = 'MISCELLANEOUS'
				all_types_game4_m4.append(db_self.type_m4_g4_1r)
				all_types_game4_m4.append(db_self.type_m4_g4_2r)
				print("TIPO GIOCO G4 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m4, all_types_game4_m4))					
		elif db_self.m4_questions_g4==3:
			if db_self.type_m4_g4_1r==db_self.type_m4_g4_2r==db_self.type_m4_g4_3r:
				db_self.type_game_g4_m4 = db_self.type_m4_g4_1r
				print("TIPO GIOCO G4 M4!!!! NEW!!!!!{}".format(db_self.type_game_g4_m4))
			else: 
				db_self.type_game_g4_m4 = 'MISCELLANEOUS'
				all_types_game4_m4.append(db_self.type_m4_g4_1r)
				all_types_game4_m4.append(db_self.type_m4_g4_2r)
				all_types_game4_m4.append(db_self.type_m4_g4_3r)
				print("TIPO GIOCO G4 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m4, all_types_game4_m4))					
		elif db_self.m4_questions_g4==4:
			if db_self.type_m4_g4_1r==db_self.type_m4_g4_2r==db_self.type_m4_g4_3r==db_self.type_m4_g4_4r:
				db_self.type_game_g4_m4 = db_self.type_m4_g4_1r
				print("TIPO GIOCO G4 M4!!!! NEW!!!!!{}".format(db_self.type_game_g4_m4))
			else: 
				db_self.type_game_g4_m4 = 'MISCELLANEOUS'
				all_types_game4_m4.append(db_self.type_m4_g4_1r)
				all_types_game4_m4.append(db_self.type_m4_g4_2r)
				all_types_game4_m4.append(db_self.type_m4_g4_3r)
				all_types_game4_m4.append(db_self.type_m4_g4_4r)
				print("TIPO GIOCO G4 M4!!!! NEW!!!!! and all_Types_is {} {}".format(db_self.type_game_g4_m4, all_types_game4_m4))				
		if db_self.games_m1==1:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
		if db_self.games_m1==2:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
		if db_self.games_m1==3:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g3, db_self.kind_m1_g3, db_self.type_game_g3_m1, all_types_game3_m1, db_self.diff_m1_g3, db_self.m1_g3_order_diff_ques, db_self.necessary_time_g3_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
		if db_self.games_m1==4:
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g1, db_self.kind_m1_g1, db_self.type_game_g1_m1, all_types_game1_m1, db_self.diff_m1_g1, db_self.m1_g1_order_diff_ques, db_self.necessary_time_g1_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g2, db_self.kind_m1_g2, db_self.type_game_g2_m1, all_types_game2_m1, db_self.diff_m1_g2, db_self.m1_g2_order_diff_ques, db_self.necessary_time_g2_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g3, db_self.kind_m1_g3, db_self.type_game_g3_m1, all_types_game3_m1, db_self.diff_m1_g3, db_self.m1_g3_order_diff_ques, db_self.necessary_time_g3_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self.m1_questions_g4, db_self.kind_m1_g4, db_self.type_game_g4_m1, all_types_game4_m1, db_self.diff_m1_g4, db_self.m1_g4_order_diff_ques, db_self.necessary_time_g4_m1, scenario_m1, 0)
			dca.add_new_MatchGame_table(db_self, match_last_1, game_idx)	
		if db_self.games_m2==1:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
		if db_self.games_m2==2:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g2, db_self.kind_m2_g2, db_self.type_game_g2_m2, all_types_game2_m2, db_self.diff_m2_g2, db_self.m2_g2_order_diff_ques, db_self.necessary_time_g2_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
		if db_self.games_m2==3:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g2, db_self.kind_m2_g2, db_self.type_game_g2_m2, all_types_game2_m2, db_self.diff_m2_g2, db_self.m2_g2_order_diff_ques, db_self.necessary_time_g2_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g3, db_self.kind_m2_g3, db_self.type_game_g3_m2, all_types_game3_m2, db_self.diff_m2_g3, db_self.m2_g3_order_diff_ques, db_self.necessary_time_g3_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
		if db_self.games_m2==4:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g1, db_self.kind_m2_g1, db_self.type_game_g1_m2, all_types_game1_m2, db_self.diff_m2_g1, db_self.m2_g1_order_diff_ques, db_self.necessary_time_g1_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g2, db_self.kind_m2_g2, db_self.type_game_g2_m2, all_types_game2_m2, db_self.diff_m2_g2, db_self.m2_g2_order_diff_ques, db_self.necessary_time_g2_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g3, db_self.kind_m2_g3, db_self.type_game_g3_m2, all_types_game3_m2, db_self.diff_m2_g3, db_self.m2_g3_order_diff_ques, db_self.necessary_time_g3_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m2_questions_g4, db_self.kind_m2_g4, db_self.type_game_g4_m2, all_types_game4_m2, db_self.diff_m2_g4, db_self.m2_g4_order_diff_ques, db_self.necessary_time_g4_m2, scenario_m2, 0)
			dca.add_new_MatchGame_table(db_self, match_last_2, game_idx)	
		if db_self.games_m3==1:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g1, db_self.kind_m3_g1, db_self.type_game_g1_m3, all_types_game1_m3, db_self.diff_m3_g1, db_self.m3_g1_order_diff_ques, db_self.necessary_time_g1_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)
		if db_self.games_m3==2:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g1, db_self.kind_m3_g1, db_self.type_game_g1_m3, all_types_game1_m3, db_self.diff_m3_g1, db_self.m3_g1_order_diff_ques, db_self.necessary_time_g1_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g2, db_self.kind_m3_g2, db_self.type_game_g2_m3, all_types_game2_m3, db_self.diff_m3_g2, db_self.m3_g2_order_diff_ques, db_self.necessary_time_g2_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)
		if db_self.games_m3==3:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g1, db_self.kind_m3_g1, db_self.type_game_g1_m3, all_types_game1_m3, db_self.diff_m3_g1, db_self.m3_g1_order_diff_ques, db_self.necessary_time_g1_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g2, db_self.kind_m3_g2, db_self.type_game_g2_m3, all_types_game2_m3, db_self.diff_m3_g2, db_self.m3_g2_order_diff_ques, db_self.necessary_time_g2_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g3, db_self.kind_m3_g3, db_self.type_game_g3_m3, all_types_game3_m3, db_self.diff_m3_g3, db_self.m3_g3_order_diff_ques, db_self.necessary_time_g3_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)	
		if db_self.games_m3==4:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g1, db_self.kind_m3_g1, db_self.type_game_g1_m3, all_types_game1_m3, db_self.diff_m3_g1, db_self.m3_g1_order_diff_ques, db_self.necessary_time_g1_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g2, db_self.kind_m3_g2, db_self.type_game_g2_m3, all_types_game2_m3, db_self.diff_m3_g2, db_self.m3_g2_order_diff_ques, db_self.necessary_time_g2_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g3, db_self.kind_m3_g3, db_self.type_game_g3_m3, all_types_game3_m3, db_self.diff_m3_g3, db_self.m3_g3_order_diff_ques, db_self.necessary_time_g3_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m3_questions_g4, db_self.kind_m3_g4, db_self.type_game_g4_m3, all_types_game4_m3, db_self.diff_m3_g4, db_self.m3_g4_order_diff_ques, db_self.necessary_time_g4_m3, scenario_m3, 0)
			dca.add_new_MatchGame_table(db_self, match_last_3, game_idx)	
		if db_self.games_m4==1:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m4_questions_g1, db_self.kind_m4_g1, db_self.type_game_g1_m4, all_types_game1_m4, db_self.diff_m4_g1, db_self.m4_g1_order_diff_ques, db_self.necessary_time_g1_m4, scenario_m4, 0)
			dca.add_new_MatchGame_table(db_self, match_last_4, game_idx)
		if db_self.games_m4==2:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m4_questions_g1, db_self.kind_m4_g1, db_self.type_game_g1_m4, all_types_game1_m4, db_self.diff_m4_g1, db_self.m4_g1_order_diff_ques, db_self.necessary_time_g1_m4, scenario_m4, 0)
			dca.add_new_MatchGame_table(db_self, match_last_4, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m4_questions_g2, db_self.kind_m4_g2, db_self.type_game_g2_m4, all_types_game2_m4, db_self.diff_m4_g2, db_self.m4_g2_order_diff_ques, db_self.necessary_time_g2_m4, scenario_m4, 0)
			dca.add_new_MatchGame_table(db_self, match_last_4, game_idx)
		if db_self.games_m4==3:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m4_questions_g1, db_self.kind_m4_g1, db_self.type_game_g1_m4, all_types_game1_m4, db_self.diff_m4_g1, db_self.m4_g1_order_diff_ques, db_self.necessary_time_g1_m4, scenario_m4, 0)
			dca.add_new_MatchGame_table(db_self, match_last_4, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m4_questions_g2, db_self.kind_m4_g2, db_self.type_game_g2_m4, all_types_game2_m4, db_self.diff_m4_g2, db_self.m4_g2_order_diff_ques, db_self.necessary_time_g2_m4, scenario_m4, 0)
			dca.add_new_MatchGame_table(db_self, match_last_4, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m4_questions_g3, db_self.kind_m4_g3, db_self.type_game_g3_m4, all_types_game3_m4, db_self.diff_m4_g3, db_self.m4_g3_order_diff_ques, db_self.necessary_time_g3_m4, scenario_m4, 0)
			dca.add_new_MatchGame_table(db_self, match_last_4, game_idx)	
		if db_self.games_m4==4:
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m4_questions_g1, db_self.kind_m4_g1, db_self.type_game_g1_m4, all_types_game1_m4, db_self.diff_m4_g1, db_self.m4_g1_order_diff_ques, db_self.necessary_time_g1_m4, scenario_m4, 0)
			dca.add_new_MatchGame_table(db_self, match_last_4, game_idx)
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m4_questions_g2, db_self.kind_m4_g2, db_self.type_game_g2_m4, all_types_game2_m4, db_self.diff_m4_g2, db_self.m4_g2_order_diff_ques, db_self.necessary_time_g2_m4, scenario_m4, 0)
			dca.add_new_MatchGame_table(db_self, match_last_4, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m4_questions_g3, db_self.kind_m4_g3, db_self.type_game_g3_m4, all_types_game3_m4, db_self.diff_m4_g3, db_self.m4_g3_order_diff_ques, db_self.necessary_time_g3_m4, scenario_m4, 0)
			dca.add_new_MatchGame_table(db_self, match_last_4, game_idx)	
			game_idx = add_new_Game_table_with_kid(db_self, db_self .m4_questions_g4, db_self.kind_m4_g4, db_self.type_game_g4_m4, all_types_game3_m4, db_self.diff_m4_g4, db_self.m4_g4_order_diff_ques, db_self.necessary_time_g4_m4, scenario_m4, 0)
			dca.add_new_MatchGame_table(db_self, match_last_4, game_idx)
	if db_self.plus_one_game:
		#add_game se c'è una domanda iomposta 
		link_to_last_session_plus_choosen(db_self)
	print("GAME OVER OVER OVER OVER GAME OVER GAME OVERGAME OVERGAME OVERGAME OVERGAME OVER GAME OVERGAME OVERGAME OVERGAME OVERGAME OVER OVER OVER")	

cdef add_new_Match_table_with_kid(db_self, str difficulty, int num_of_games, str variety, str order_difficulty_games, str order_quantity_questions, str criteria_arrangement, int advisable_time, scenario, bint disposable):
	print("ENTRO SOLENNEMANETE IN add_new_Match_table_with_kid CON PARAMMMASDA")
	print("str difficulty, int num_of_games, str order_difficulty_games, str order_quantity_questions, int advisable_time, scenario, bint disposable")
	print("{} {} {} {} {} {} {} {} {}".format(difficulty,num_of_games,variety,order_difficulty_games,order_quantity_questions,criteria_arrangement,advisable_time,scenario,disposable))
	cdef:
		str new_id = db_self.assign_new_id('Matches')
		str search_str = "{0}, '{1}', '{2}', '{3}', '{4}', '{5}', {6}".format(num_of_games, difficulty, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, scenario)
		bint new_del = 0
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Match you want to insert already exist.')
				#raise Exception
				pass
			db_self.curs.execute('''INSERT INTO Matches (difficulty, num_of_games, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, advisable_time, scenario, disposable, token) 
				VALUES (?,?,?,?,?,?,?,?,?,?)''', (difficulty, num_of_games, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, advisable_time, scenario, disposable, new_id,))
			db_self.conn.commit()
			db_self.curs.execute( "SELECT COUNT(rowid) FROM Matches")
			num_last_line_m = db_self.curs.fetchone()
			if num_last_line_m:
				num_last_line_m = num_last_line_m[0]
			else:
				num_last_line_m = 0
				print("ERRORE_GRAVISSIMOOOOOOOOOOOOOO")
			mdl.modify_Matches_list(difficulty, num_of_games, variety, order_difficulty_games, order_quantity_questions, criteria_arrangement, advisable_time, scenario, disposable, new_id, num_last_line_m)
			filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_db_self.deletions_list.pk'
			with open(filename_pk, 'rb') as fi:
				db_self.deletions_list = pickle.load(fi)
				print("ho trovato questa db_self.deletions_list!!!!")
				print("db_self.deletions_list = {}".format(db_self.deletions_list))
			if db_self.deletions_list[2]!=[0]:
				new_del = 1
				dbem.bubblesort(db_self.deletions_list[2])
				db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Matches\'")
				index_to_replace = db_self.curs.fetchone()[1] #1 is correct!!
				index_corrected = db_self.deletions_list[2].pop(0)
				db_self.curs.execute('''UPDATE Matches SET match_id = ?  WHERE match_id = ?''', (index_corrected,index_to_replace,))
				if not db_self.deletions_list[2]:
					db_self.deletions_list[2].append(0)
					with open(filename_pk, 'wb') as fi:
						pickle.dump(db_self.deletions_list, fi)
				db_self.conn.commit()
			else:
				#just for the exception
				index_corrected = 999
				index_to_replace = 999			
		print("The new Match is now added.")
	except Exception as e:
		print("--> An error occurred while adding a new Match")
		print("--> Search_str add_new_Match {}".format(search_str))
		print("Namely:")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
		if new_del:
			print("--> 1 match))) {} ".format(index_to_replace))
			print("--> 2 match))) {} ".format(index_corrected))

cdef int add_new_Game_table_with_kid(db_self, int num_questions, str kind_game, str type_game, list types_to_pass, str difficulty, str order_difficulty_questions, int necessary_time, scenario, bint disposable):
	print("ENTRO IN ADD NEW GAME CON PARAMETRI NOTI = int db_self.kid_id, int num_questions, str kind_game, str type_game, str difficulty, str order_difficulty_questions, int necessary_time, db_self.never_asked_q, db_self.imposed_ques_id scenario, bint disposable):")
	print("{} {} {} {} {} {} {} {} {} {} {}".format(db_self.kid_id,num_questions,kind_game,type_game,difficulty,order_difficulty_questions,necessary_time,db_self.never_asked_q,db_self.imposed_ques_id,scenario,disposable))
	cdef:
		str new_id = ''
		str	search_str = "\t({0}, '{1}', '{2}', '{3}', '{4}'".format(num_questions, kind_game, type_game, difficulty, order_difficulty_questions)
		bint new_del = 0
	try:
		find_me = search_str.encode()
		with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py', 'rb', 0) as file, \
			mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
			if s.find(find_me) != -1:
				print('--> The Game you want to insert already exist.')
				if db_self.never_asked_q or db_self.imposed_ques_id!=99999:
					#raise Exception
					pass
				else:
					depends = random.randint(0,2)
					if depends==0:
						raise Exception
					else:
						pass
			################################### set time
			if num_questions==1 and kind_game in ['1F','2L','5K']:
				necessary_time = 10
			if num_questions==1 and kind_game in ['3S','4P']:
				necessary_time = 12
			if num_questions==1 and kind_game=='7O':
				necessary_time = 15
			if num_questions==1 and kind_game in['8Q','9C']:
				necessary_time = 18
			if num_questions==1 and kind_game=='6I':
				necessary_time = 20

			if num_questions==2 and kind_game in ['1F','2L','5K']:
				necessary_time = 20
			if num_questions==2 and kind_game in ['3S','4P']:
				necessary_time = 24
			if num_questions==2 and kind_game=='7O':
				necessary_time = 30
			if num_questions==2 and kind_game in['8Q','9C']:
				necessary_time = 36
			if num_questions==2 and kind_game=='6I':
				necessary_time = 40

			if num_questions==3 and kind_game in ['1F','2L','5K']:
				necessary_time = 30
			if num_questions==3 and kind_game in ['3S','4P']:
				necessary_time = 36
			if num_questions==3 and kind_game=='7O':
				necessary_time = 45
			if num_questions==3 and kind_game in['8Q','9C']:
				necessary_time = 54
			if num_questions==3 and kind_game=='6I':
				necessary_time = 60

			if num_questions==4 and kind_game in ['1F','2L','5K']:
				necessary_time = 40
			if num_questions==4 and kind_game in ['3S','4P']:
				necessary_time = 48
			if num_questions==4 and kind_game=='7O':
				necessary_time = 60
			if num_questions==4 and kind_game in['8Q','9C']:
				necessary_time = 72
			if num_questions==4 and kind_game=='6I':
				necessary_time = 80
			################################### set time
			new_id = db_self.assign_new_id('Games')
			db_self.curs.execute('''INSERT INTO Games (num_questions, kind, type, difficulty, order_difficulty_questions, necessary_time, disposable, token) 
								VALUES (?,?,?,?,?,?,?,?)''', (num_questions, kind_game, type_game, difficulty, order_difficulty_questions, necessary_time, disposable, new_id,))
			db_self.conn.commit()
			db_self.curs.execute( "SELECT COUNT(rowid) FROM Games")
			num_last_line_g = db_self.curs.fetchone()
			if num_last_line_g:
				num_last_line_g = num_last_line_g[0]
			else:
				num_last_line_g = 0
			mdl.modify_Games_list(num_questions, kind_game, type_game, difficulty, order_difficulty_questions, necessary_time, disposable, new_id, num_last_line_g)
		filename_pk = '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager/pickle_db_self.deletions_list.pk'
		with open(filename_pk, 'rb') as fi:
			db_self.deletions_list = pickle.load(fi)
			print("ho trovato questa db_self.deletions_list!!!!")
			print("db_self.deletions_list = {}".format(db_self.deletions_list))
		if db_self.deletions_list[3]!=[0]:
			new_del = 1
			dbem.bubblesort(db_self.deletions_list[3])
			db_self.curs.execute("SELECT * FROM SQLITE_SEQUENCE WHERE name=\'Games\'")
			index_to_replace = db_self.curs.fetchone()[1]
			index_corrected = db_self.deletions_list[3].pop(0)
			db_self.curs.execute('''UPDATE Games SET game_id = ?  WHERE game_id = ?''', (index_corrected, index_to_replace,))
			if not db_self.deletions_list[3]:
				db_self.deletions_list[3].append(0)
				with open(filename_pk, 'wb') as fi:
					pickle.dump(db_self.deletions_list, fi)						
			db_self.conn.commit()
		else:
			#just for the exception
			index_corrected = 999
			index_to_replace = 999
		print("The new Game is now added.")		
		#QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS QUESTIONS
		print("Adding questions")
		db_self.curs.execute('''SELECT COUNT(question_id)
		FROM Questions
			JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
			ON Scenarios_Questions.audio_id = Questions.audio
		WHERE scenario_id = ? ''',(scenario,))
		num_possible_questions = db_self.curs.fetchone()
		if num_possible_questions:
			num_possible_questions = num_possible_questions[0]
		else:
			print("ERRORE GRAVISSIM DUE")
			num_possible_questions = 0
		print("num_possible_questions {}".format(num_possible_questions))
		#if num_possible_questions < num_of_questions: non serve c'è limit!!
		#	num_of_questions = num_possible_questions
		avoid_this_question=1
		while avoid_this_question>0:
			if type_game!='MISCELLANEOUS':
				print("parametri da inserire query nulla scenario = {} type_game = {} num_questions = {}".format(scenario,type_game,num_questions))
				if kind_game!='4P':
					db_self.curs.execute('''SELECT DISTINCT question_id FROM Questions
						JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
						ON Scenarios_Questions.audio_id = Questions.audio
						WHERE scenario_id = ? AND type = ? 
						ORDER BY RANDOM()
						LIMIT ?''',(scenario,type_game,num_questions,))
				elif type_game=='4PSU':
					print("non siamo in misce!! 4PSU4PSU4PSU4PSU4PSU4PSU 4PSU4PSU4PSU4PSU niente miscellaneous!! parametri da inserire query nulla scenario = {} type_game(type_game) = {} num_questions = {}".format(scenario,type_game,num_questions))
					db_self.curs.execute('''SELECT DISTINCT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
						WHERE scenario_id = ?  
						ORDER BY RANDOM()
						LIMIT ?''',(scenario,num_questions,))
				elif type_game=='4PCO':
					print("non siamo in misce!! 4PCO4PCO4PCO4PCO4PCO niente miscellaneous!! parametri da inserire query nulla scenario = {} type_game(type_game) = {} num_questions = {}".format(scenario,type_game,num_questions))
					db_self.curs.execute('''SELECT DISTINCT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
						WHERE scenario_id = ?  
						ORDER BY RANDOM()
						LIMIT ?''',(scenario,num_questions,))			
				questions_ok = []
				qu_to_change = []
				qu_index_to_change = []
				choosen_quests = db_self.curs.fetchall()
				print("!!!!!choosen_quests!!!! QUELLO DELLA LOLA--> {}".format(choosen_quests))
				for i in range(len(choosen_quests)):
					questions_ok.append(choosen_quests[i][0])
				print("!!!!!questions_ok!!!! BEFORE CHANGE IT--> {}".format(questions_ok))
			else:
				ucanexit=0
				while ucanexit==0:
					questions_ok = []
					print("LA LUNGHEZZA DI types_to_pass {}".format(len(types_to_pass)))
					for i in range(len(types_to_pass)):
						print("CASO MISCELLANEOUS!!!parametri da inserire query GIRO NUM ---> {}  nulla scenario = {} type_game = {}".format(i,scenario,types_to_pass[i]))
						if len(questions_ok)==0:
							print("when in add_new_game seleziono domanda precisa....caso 0")
							if kind_game!='4P':
								db_self.curs.execute('''SELECT question_id FROM Questions
									JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
									ON Scenarios_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND type = ? 
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,types_to_pass[i],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)													
							elif types_to_pass[i]=='4PSU':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
									WHERE scenario_id = ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
							elif types_to_pass[i]=='4PCO':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
									WHERE scenario_id = ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
							else:
								print("DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!! 4P ANDATO STORTO!!!")							
						if len(questions_ok)==1:
							print("when in add_new_game seleziono domanda precisa....caso 1")
							if kind_game!='4P':
								db_self.curs.execute('''SELECT question_id FROM Questions
									JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
									ON Scenarios_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND type = ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions
										JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
										ON Scenarios_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND type = ? 
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,types_to_pass[i],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità::")
									print(choosen_quests)
							elif types_to_pass[i]=='4PSU':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ?
									ORDER BY RANDOM() 
									LIMIT 1''',(scenario,questions_ok[0],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? 
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
							elif types_to_pass[i]=='4PCO':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,questions_ok[0],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? 
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
							else:
								print("DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!! 4P ANDATO STORTO!!!")
						if len(questions_ok)==2:
							print("when in add_new_game seleziono domanda precisa....caso 2")
							if kind_game!='4P':
								db_self.curs.execute('''SELECT question_id FROM Questions
									JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
									ON Scenarios_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND type = ? AND question_id != ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],questions_ok[1],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions
										JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
										ON Scenarios_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND type = ? AND (question_id != ? OR question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],questions_ok[1],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions
											JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
											ON Scenarios_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? AND type = ? 
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,types_to_pass[i],))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità::")
										print(choosen_quests)
							elif types_to_pass[i]=='4PSU':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND (question_id != ? OR question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? 
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità is::")
										print(choosen_quests)
							elif types_to_pass[i]=='4PCO':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND (question_id != ? OR question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? 
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità is::")
										print(choosen_quests)
							else:
								print("DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!! 4P ANDATO STORTO!!!")
						if len(questions_ok)==3:
							print("when in add_new_game seleziono domanda precisa....caso 3")
							if kind_game!='4P':
								db_self.curs.execute('''SELECT question_id FROM Questions
									JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
									ON Scenarios_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND type = ? AND question_id != ? AND question_id != ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],questions_ok[1],questions_ok[2],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions
										JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
										ON Scenarios_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND type = ? AND question_id != ? AND (question_id != ? AND question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],questions_ok[1],questions_ok[2],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions
											JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
											ON Scenarios_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? AND type = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,types_to_pass[i],questions_ok[1],questions_ok[0],questions_ok[2],))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità::")
										print(choosen_quests)
										if not choosen_quests:
											db_self.curs.execute('''SELECT question_id FROM Questions
												JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
												ON Scenarios_Questions.audio_id = Questions.audio
												WHERE scenario_id = ? AND type = ? AND question_id != ? AND (question_id != ? AND question_id != ?)
												ORDER BY RANDOM()
												LIMIT 1''',(scenario,types_to_pass[i],questions_ok[2],questions_ok[0],questions_ok[1],))
											choosen_quests = db_self.curs.fetchone()
											print("choosen_quests quarta possibilità::")
											print(choosen_quests)
											if not choosen_quests:
												db_self.curs.execute('''SELECT question_id FROM Questions
													JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
													ON Scenarios_Questions.audio_id = Questions.audio
													WHERE scenario_id = ? AND type = ? AND (question_id != ? OR question_id != ? OR question_id != ?)
													ORDER BY RANDOM()
													LIMIT 1''',(scenario,types_to_pass[i],questions_ok[0],questions_ok[1],questions_ok[2],))
												choosen_quests = db_self.curs.fetchone()
												print("choosen_quests quinta possibilità::")
												print(choosen_quests)
												if not choosen_quests:
													db_self.curs.execute('''SELECT question_id FROM Questions
														JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
														ON Scenarios_Questions.audio_id = Questions.audio
														WHERE scenario_id = ?
														ORDER BY RANDOM()
														LIMIT 1''',(scenario,types_to_pass[i],))
													choosen_quests = db_self.curs.fetchone()
													print("choosen_quests sesta possibilità::")
													print(choosen_quests)												
							elif types_to_pass[i]=='4PSU':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ? OR question_id != ? AND question_id != ?)
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,questions_ok[1],questions_ok[0],questions_ok[2],))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità is::")
										print(choosen_quests)
										if not choosen_quests:
											db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
												WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
												ORDER BY RANDOM()
												LIMIT 1''',(scenario,questions_ok[2],questions_ok[0],questions_ok[1],))
											choosen_quests = db_self.curs.fetchone()
											print("choosen_quests quarta possibilità is::")
											print(choosen_quests)
											if not choosen_quests:
												db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
													WHERE scenario_id = ? AND (question_id != ? OR question_id != ? OR question_id != ?)
													ORDER BY RANDOM()
													LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
												choosen_quests = db_self.curs.fetchone()
												print("choosen_quests quinta possibilità is::")
												print(choosen_quests)
												if not choosen_quests:
													db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
														WHERE scenario_id = ?
														ORDER BY RANDOM()
														LIMIT 1''',(scenario,))
													choosen_quests = db_self.curs.fetchone()
													print("choosen_quests sesta possibilità is::")
													print(choosen_quests)
							elif types_to_pass[i]=='4PCO':
								db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
									WHERE scenario_id = ? AND question_id != ? AND question_id != ? AND question_id != ?
									ORDER BY RANDOM()
									LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
								choosen_quests = db_self.curs.fetchone()
								print("choosen_quests PRIMAPOSS::")
								print(choosen_quests)
								if not choosen_quests:
									db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
										WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
										ORDER BY RANDOM()
										LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
									choosen_quests = db_self.curs.fetchone()
									print("choosen_quests seconda possibilità is::")
									print(choosen_quests)
									if not choosen_quests:
										db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
											WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
											ORDER BY RANDOM()
											LIMIT 1''',(scenario,questions_ok[1],questions_ok[0],questions_ok[2],))
										choosen_quests = db_self.curs.fetchone()
										print("choosen_quests terza possibilità is::")
										print(choosen_quests)
										if not choosen_quests:
											db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
												WHERE scenario_id = ? AND question_id != ? AND (question_id != ? OR question_id != ?)
												ORDER BY RANDOM()
												LIMIT 1''',(scenario,questions_ok[2],questions_ok[0],questions_ok[1],))
											choosen_quests = db_self.curs.fetchone()
											print("choosen_quests quarta possibilità is::")
											print(choosen_quests)
											if not choosen_quests:
												db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
													WHERE scenario_id = ? AND (question_id != ? OR question_id != ? OR question_id != ?)
													ORDER BY RANDOM()
													LIMIT 1''',(scenario,questions_ok[0],questions_ok[1],questions_ok[2],))
												choosen_quests = db_self.curs.fetchone()
												print("choosen_quests quinta possibilità is::")
												print(choosen_quests)
												if not choosen_quests:
													db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
														WHERE scenario_id = ?
														ORDER BY RANDOM()
														LIMIT 1''',(scenario,))
													choosen_quests = db_self.curs.fetchone()
													print("choosen_quests sesta possibilità is::")
													print(choosen_quests)
							else:
								print("DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!!DAMNED!!! 4P ANDATO STORTO!!!")						
						"""
						db_self.curs.execute('''SELECT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,types_to_pass[i],))
						choosen_quests = db_self.curs.fetchall()
						print("choosen_quests::::")
						print(choosen_quests)
						"""
						questions_ok.append(choosen_quests[0][0])
						################################################## controllo!!! verify
						if kind_game!='4P':
							db_self.curs.execute('''SELECT question_id FROM Questions
								JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
								ON Scenarios_Questions.audio_id = Questions.audio
								WHERE scenario_id = ? AND type = ? 
								ORDER BY RANDOM()''',(scenario,types_to_pass[i],))
						elif types_to_pass[i]=='4PSU':
							db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
								WHERE scenario_id = ?
								ORDER BY RANDOM()''',(scenario,))
						elif types_to_pass[i]=='4PCO':
							db_self.curs.execute('''SELECT question_id FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
								WHERE scenario_id = ?
								ORDER BY RANDOM()''',(scenario,))
						choosen_quests_verify = db_self.curs.fetchall()
						quest_verify = [item for sublist in choosen_quests_verify for item in sublist]
						print("quest_verify", quest_verify)
						################################################## controllo ma deve combaciare col kind						
					questions_ok = list(set(questions_ok))
					print("!!!!!questions_ok SECONDO dopo riduzione del set--> {}".format(questions_ok))
					if len(questions_ok)>1 and len(quest_verify)>1 and any(questions_ok.count(x)==len(questions_ok) for x in questions_ok): #if all same question = not miscellaneous anymore!
							ucanexit = 0
							print("RICOMINCIO IL GIRO!!!")
							print("RICOMINCIO IL GIRO!!!")			
							print("RICOMINCIO IL GIRO!!!")			
							print("RICOMINCIO IL GIRO!!!")
					else:
						choose = random.randint(0,2)
						if choose==0:
							if len(questions_ok)>1 and len(quest_verify)>1 and any(questions_ok.count(x) > 2 for x in questions_ok): #if duplicates 
								ucanexit = 0
								print("RICOMINCIO IL GIRO!!!")
								print("RICOMINCIO IL GIRO!!!")
								print("RICOMINCIO IL GIRO!!!")
								print("RICOMINCIO IL GIRO!!!")
							else:
								ucanexit = 1
						else:
							ucanexit = 1
			for q in questions_ok:
				if q=='audio_3046' or q=='audio_3047':
					avoid_this_question=1
				else:
					avoid_this_question=0
			#redo in case of ripetutute subject for question 9cnssu 9cnsu 9cnco
			if difficulty>'Normal_1' and kind_game=='9C':
				print("OREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREOOREO")
				go_on = 1
				while go_on>0:
					if len(questions_ok)==1:
						avoid_this_question = 0
						go_on = 0					
					if len(questions_ok)==2:
					#9cnsu
						if questions_ok[0] in [11889,11890,11891,11892] and questions_ok[1] in [11889,11890,11891,11892]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11893,11894,11895,11896] and questions_ok[1] in [11893,11894,11895,11896]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11897,11898,11899,11900] and questions_ok[1] in [11897,11898,11899,11900]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11901,11902,11903,11904] and questions_ok[1] in [11901,11902,11903,11904]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11905,11906,11907,11908] and questions_ok[1] in [11905,11906,11907,11908]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11909,11910,11911,11912] and questions_ok[1] in [11909,11910,11911,11912]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11913,11914,11915,11916] and questions_ok[1] in [11913,11914,11915,11916]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11917,11918,11919,11920] and questions_ok[1] in [11917,11918,11919,11920]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11921,11922,11923,11924] and questions_ok[1] in [11921,11922,11923,11924]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11925,11926,11927,11928] and questions_ok[1] in [11925,11926,11927,11928]:
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[0] in [11929,11930,11931] and questions_ok[1] in [11929,11930,11931]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11929,11930,11931] and questions_ok[1] in [11929,11930,11931]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11935,11936,11937] and questions_ok[1] in [11935,11936,11937]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11938,11939,11940] and questions_ok[1] in [11938,11939,11940]:
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[0] in [11941,11942,11943] and questions_ok[1] in [11941,11942,11943]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11944,11945,11946] and questions_ok[1] in [11944,11945,11946]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11947,11948,11949] and questions_ok[1] in [11947,11948,11949]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11950,11951,11952] and questions_ok[1] in [11950,11951,11952]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11953,11954,11955] and questions_ok[1] in [11953,11954,11955]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11956,11957,11958] and questions_ok[1] in [11956,11957,11958]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11959,11960,11961] and questions_ok[1] in [11959,11960,11961]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11962,11963,11964] and questions_ok[1] in [11962,11963,11964]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11965,11966,11967] and questions_ok[1] in [11965,11966,11967]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11968,11969,11970] and questions_ok[1] in [11968,11969,11970]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11971,11972,11973] and questions_ok[1] in [11971,11972,11973]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11974,11975,11976] and questions_ok[1] in [11974,11975,11976]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11977,11978,11979] and questions_ok[1] in [11977,11978,11979]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11980,11981,11982] and questions_ok[1] in [11980,11981,11982]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						else:
							avoid_this_question=0
							go_on = 0

					if len(questions_ok)==3:
					#9cnsu
						if questions_ok[0] in [11889,11890,11891,11892] and (questions_ok[1] in [11889,11890,11891,11892] or questions_ok[2] in [11889,11890,11891,11892]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11893,11894,11895,11896] and (questions_ok[1] in [11893,11894,11895,11896] or questions_ok[2] in [11893,11894,11895,11896]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11897,11898,11899,11900] and (questions_ok[1] in [11897,11898,11899,11900] or questions_ok[2] in [11897,11898,11899,11900]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11901,11902,11903,11904] and (questions_ok[1] in [11901,11902,11903,11904] or questions_ok[2] in [11901,11902,11903,11904]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11905,11906,11907,11908] and (questions_ok[1] in [11905,11906,11907,11908] or questions_ok[2] in [11905,11906,11907,11908]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11909,11910,11911,11912] and (questions_ok[1] in [11909,11910,11911,11912] or questions_ok[2] in [11909,11910,11911,11912]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11913,11914,11915,11916] and (questions_ok[1] in [11913,11914,11915,11916] or questions_ok[2] in [11913,11914,11915,11916]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11917,11918,11919,11920] and (questions_ok[1] in [11917,11918,11919,11920] or questions_ok[2] in [11917,11918,11919,11920]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11921,11922,11923,11924] and (questions_ok[1] in [11921,11922,11923,11924] or questions_ok[2] in [11921,11922,11923,11924]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11925,11926,11927,11928] and (questions_ok[1] in [11925,11926,11927,11928] or questions_ok[2] in [11925,11926,11927,11928]):
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[0] in [11929,11930,11931] and (questions_ok[1] in [11929,11930,11931] or questions_ok[2] in [11929,11930,11931]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11929,11930,11931] and (questions_ok[1] in [11929,11930,11931] or questions_ok[2] in [11929,11930,11931]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11935,11936,11937] and (questions_ok[1] in [11935,11936,11937] or questions_ok[2] in [11935,11936,11937]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11938,11939,11940] and (questions_ok[1] in [11938,11939,11940] or questions_ok[2] in [11938,11939,11940]):
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[0] in [11941,11942,11943] and (questions_ok[1] in [11941,11942,11943] or questions_ok[2] in [11941,11942,11943]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11944,11945,11946] and (questions_ok[1] in [11944,11945,11946] or questions_ok[2] in [11944,11945,11946]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11947,11948,11949] and (questions_ok[1] in [11947,11948,11949] or questions_ok[2] in [11947,11948,11949]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11950,11951,11952] and (questions_ok[1] in [11950,11951,11952] or questions_ok[2] in [11950,11951,11952]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11953,11954,11955] and (questions_ok[1] in [11953,11954,11955] or questions_ok[2] in [11953,11954,11955]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11956,11957,11958] and (questions_ok[1] in [11956,11957,11958] or questions_ok[2] in [11956,11957,11958]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11959,11960,11961] and (questions_ok[1] in [11959,11960,11961] or questions_ok[2] in [11959,11960,11961]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11962,11963,11964] and (questions_ok[1] in [11962,11963,11964] or questions_ok[2] in [11962,11963,11964]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11965,11966,11967] and (questions_ok[1] in [11965,11966,11967] or questions_ok[2] in [11965,11966,11967]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11968,11969,11970] and (questions_ok[1] in [11968,11969,11970] or questions_ok[2] in [11968,11969,11970]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11971,11972,11973] and (questions_ok[1] in [11971,11972,11973] or questions_ok[2] in [11971,11972,11973]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11974,11975,11976] and (questions_ok[1] in [11974,11975,11976] or questions_ok[2] in [11974,11975,11976]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11977,11978,11979] and (questions_ok[1] in [11977,11978,11979] or questions_ok[2] in [11977,11978,11979]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11980,11981,11982] and (questions_ok[1] in [11980,11981,11982] or questions_ok[2] in [11980,11981,11982]):
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnsu
						elif questions_ok[1] in [11889,11890,11891,11892] and questions_ok[2] in [11889,11890,11891,11892]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11893,11894,11895,11896] and questions_ok[2] in [11893,11894,11895,11896]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11897,11898,11899,11900] and questions_ok[2] in [11897,11898,11899,11900]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11901,11902,11903,11904] and questions_ok[2] in [11901,11902,11903,11904]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11905,11906,11907,11908] and questions_ok[2] in [11905,11906,11907,11908]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11909,11910,11911,11912] and questions_ok[2] in [11909,11910,11911,11912]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11913,11914,11915,11916] and questions_ok[2] in [11913,11914,11915,11916]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11917,11918,11919,11920] and questions_ok[2] in [11917,11918,11919,11920]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11921,11922,11923,11924] and questions_ok[2] in [11921,11922,11923,11924]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11925,11926,11927,11928] and questions_ok[2] in [11925,11926,11927,11928]:
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[1] in [11929,11930,11931] and questions_ok[2] in [11929,11930,11931]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11929,11930,11931] and questions_ok[2] in [11929,11930,11931]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11935,11936,11937] and questions_ok[2] in [11935,11936,11937]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11938,11939,11940] and questions_ok[2] in [11938,11939,11940]:
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[1] in [11941,11942,11943] and questions_ok[2] in [11941,11942,11943]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11944,11945,11946] and questions_ok[2] in [11944,11945,11946]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11947,11948,11949] and questions_ok[2] in [11947,11948,11949]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11950,11951,11952] and questions_ok[2] in [11950,11951,11952]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11953,11954,11955] and questions_ok[2] in [11953,11954,11955]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11956,11957,11958] and questions_ok[2] in [11956,11957,11958]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11959,11960,11961] and questions_ok[2] in [11959,11960,11961]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11962,11963,11964] and questions_ok[2] in [11962,11963,11964]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11965,11966,11967] and questions_ok[2] in [11965,11966,11967]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11968,11969,11970] and questions_ok[2] in [11968,11969,11970]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11971,11972,11973] and questions_ok[2] in [11971,11972,11973]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11974,11975,11976] and questions_ok[2] in [11974,11975,11976]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11977,11978,11979] and questions_ok[2] in [11977,11978,11979]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11980,11981,11982] and questions_ok[2] in [11980,11981,11982]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						else:
							avoid_this_question=0						
							go_on=0

					if len(questions_ok)==4:
					#9cnsu
						if questions_ok[0] in [11889,11890,11891,11892] and (questions_ok[1] in [11889,11890,11891,11892] or questions_ok[2] in [11889,11890,11891,11892] or questions_ok[3] in [11889,11890,11891,11892]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11893,11894,11895,11896] and (questions_ok[1] in [11893,11894,11895,11896] or questions_ok[2] in [11893,11894,11895,11896] or questions_ok[3] in [11893,11894,11895,11896]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11897,11898,11899,11900] and (questions_ok[1] in [11897,11898,11899,11900] or questions_ok[2] in [11897,11898,11899,11900] or questions_ok[3] in [11897,11898,11899,11900]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11901,11902,11903,11904] and (questions_ok[1] in [11901,11902,11903,11904] or questions_ok[2] in [11901,11902,11903,11904] or questions_ok[3] in [11901,11902,11903,11904]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11905,11906,11907,11908] and (questions_ok[1] in [11905,11906,11907,11908] or questions_ok[2] in [11905,11906,11907,11908] or questions_ok[3] in [11905,11906,11907,11908]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11909,11910,11911,11912] and (questions_ok[1] in [11909,11910,11911,11912] or questions_ok[2] in [11909,11910,11911,11912] or questions_ok[3] in [11909,11910,11911,11912]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11913,11914,11915,11916] and (questions_ok[1] in [11913,11914,11915,11916] or questions_ok[2] in [11913,11914,11915,11916] or questions_ok[3] in [11913,11914,11915,11916]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11917,11918,11919,11920] and (questions_ok[1] in [11917,11918,11919,11920] or questions_ok[2] in [11917,11918,11919,11920] or questions_ok[3] in [11917,11918,11919,11920]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11921,11922,11923,11924] and (questions_ok[1] in [11921,11922,11923,11924] or questions_ok[2] in [11921,11922,11923,11924] or questions_ok[3] in [11921,11922,11923,11924]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11925,11926,11927,11928] and (questions_ok[1] in [11925,11926,11927,11928] or questions_ok[2] in [11925,11926,11927,11928] or questions_ok[3] in [11925,11926,11927,11928]):
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[0] in [11929,11930,11931] and (questions_ok[1] in [11929,11930,11931] or questions_ok[2] in [11929,11930,11931] or questions_ok[3] in [11929,11930,11931]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11929,11930,11931] and (questions_ok[1] in [11929,11930,11931] or questions_ok[2] in [11929,11930,11931] or questions_ok[3] in [11929,11930,11931]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11935,11936,11937] and (questions_ok[1] in [11935,11936,11937] or questions_ok[2] in [11935,11936,11937] or questions_ok[3] in [11935,11936,11937]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11938,11939,11940] and (questions_ok[1] in [11938,11939,11940] or questions_ok[2] in [11938,11939,11940] or questions_ok[3] in [11938,11939,11940]):
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[0] in [11941,11942,11943] and (questions_ok[1] in [11941,11942,11943] or questions_ok[2] in [11941,11942,11943] or questions_ok[3] in [11941,11942,11943]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11944,11945,11946] and (questions_ok[1] in [11944,11945,11946] or questions_ok[2] in [11944,11945,11946] or questions_ok[3] in [11944,11945,11946]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11947,11948,11949] and (questions_ok[1] in [11947,11948,11949] or questions_ok[2] in [11947,11948,11949] or questions_ok[3] in [11947,11948,11949]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11950,11951,11952] and (questions_ok[1] in [11950,11951,11952] or questions_ok[2] in [11950,11951,11952] or questions_ok[3] in [11950,11951,11952]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11953,11954,11955] and (questions_ok[1] in [11953,11954,11955] or questions_ok[2] in [11953,11954,11955] or questions_ok[3] in [11953,11954,11955]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11956,11957,11958] and (questions_ok[1] in [11956,11957,11958] or questions_ok[2] in [11956,11957,11958] or questions_ok[3] in [11956,11957,11958]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11959,11960,11961] and (questions_ok[1] in [11959,11960,11961] or questions_ok[2] in [11959,11960,11961] or questions_ok[3] in [11959,11960,11961]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11962,11963,11964] and (questions_ok[1] in [11962,11963,11964] or questions_ok[2] in [11962,11963,11964] or questions_ok[3] in [11962,11963,11964]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11965,11966,11967] and (questions_ok[1] in [11965,11966,11967] or questions_ok[2] in [11965,11966,11967] or questions_ok[3] in [11965,11966,11967]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11968,11969,11970] and (questions_ok[1] in [11968,11969,11970] or questions_ok[2] in [11968,11969,11970] or questions_ok[3] in [11968,11969,11970]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11971,11972,11973] and (questions_ok[1] in [11971,11972,11973] or questions_ok[2] in [11971,11972,11973] or questions_ok[3] in [11971,11972,11973]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11974,11975,11976] and (questions_ok[1] in [11974,11975,11976] or questions_ok[2] in [11974,11975,11976] or questions_ok[3] in [11974,11975,11976]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11977,11978,11979] and (questions_ok[1] in [11977,11978,11979] or questions_ok[2] in [11977,11978,11979] or questions_ok[3] in [11977,11978,11979]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[0] in [11980,11981,11982] and (questions_ok[1] in [11980,11981,11982] or questions_ok[2] in [11980,11981,11982] or questions_ok[3] in [11980,11981,11982]):
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnsu
						elif questions_ok[1] in [11889,11890,11891,11892] and (questions_ok[2] in [11889,11890,11891,11892] or questions_ok[3] in [11889,11890,11891,11892]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11893,11894,11895,11896] and (questions_ok[2] in [11893,11894,11895,11896] or questions_ok[3] in [11893,11894,11895,11896]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11897,11898,11899,11900] and (questions_ok[2] in [11897,11898,11899,11900] or questions_ok[3] in [11897,11898,11899,11900]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11901,11902,11903,11904] and (questions_ok[2] in [11901,11902,11903,11904] or questions_ok[3] in [11901,11902,11903,11904]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11905,11906,11907,11908] and (questions_ok[2] in [11905,11906,11907,11908] or questions_ok[3] in [11905,11906,11907,11908]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11909,11910,11911,11912] and (questions_ok[2] in [11909,11910,11911,11912] or questions_ok[3] in [11909,11910,11911,11912]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11913,11914,11915,11916] and (questions_ok[2] in [11913,11914,11915,11916] or questions_ok[3] in [11913,11914,11915,11916]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11917,11918,11919,11920] and (questions_ok[2] in [11917,11918,11919,11920] or questions_ok[3] in [11917,11918,11919,11920]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11921,11922,11923,11924] and (questions_ok[2] in [11921,11922,11923,11924] or questions_ok[3] in [11921,11922,11923,11924]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11925,11926,11927,11928] and (questions_ok[2] in [11925,11926,11927,11928] or questions_ok[3] in [11925,11926,11927,11928]):
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[1] in [11929,11930,11931] and (questions_ok[2] in [11929,11930,11931] or questions_ok[3] in [11929,11930,11931]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11929,11930,11931] and (questions_ok[2] in [11929,11930,11931] or questions_ok[3] in [11929,11930,11931]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11935,11936,11937] and (questions_ok[2] in [11935,11936,11937] or questions_ok[3] in [11935,11936,11937]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11938,11939,11940] and (questions_ok[2] in [11938,11939,11940] or questions_ok[3] in [11938,11939,11940]):
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[1] in [11941,11942,11943] and (questions_ok[2] in [11941,11942,11943] or questions_ok[3] in [11941,11942,11943]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11944,11945,11946] and (questions_ok[2] in [11944,11945,11946] or questions_ok[3] in [11944,11945,11946]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11947,11948,11949] and (questions_ok[2] in [11947,11948,11949] or questions_ok[3] in [11947,11948,11949]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11950,11951,11952] and (questions_ok[2] in [11950,11951,11952] or questions_ok[3] in [11950,11951,11952]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11953,11954,11955] and (questions_ok[2] in [11953,11954,11955] or questions_ok[3] in [11953,11954,11955]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11956,11957,11958] and (questions_ok[2] in [11956,11957,11958] or questions_ok[3] in [11956,11957,11958]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11959,11960,11961] and (questions_ok[2] in [11959,11960,11961] or questions_ok[3] in [11959,11960,11961]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11962,11963,11964] and (questions_ok[2] in [11962,11963,11964] or questions_ok[3] in [11962,11963,11964]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11965,11966,11967] and (questions_ok[2] in [11965,11966,11967] or questions_ok[3] in [11965,11966,11967]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11968,11969,11970] and (questions_ok[2] in [11968,11969,11970] or questions_ok[3] in [11968,11969,11970]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11971,11972,11973] and (questions_ok[2] in [11971,11972,11973] or questions_ok[3] in [11971,11972,11973]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11974,11975,11976] and (questions_ok[2] in [11974,11975,11976] or questions_ok[3] in [11974,11975,11976]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11977,11978,11979] and (questions_ok[2] in [11977,11978,11979] or questions_ok[3] in [11977,11978,11979]):
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[1] in [11980,11981,11982] and (questions_ok[2] in [11980,11981,11982] or questions_ok[3] in [11980,11981,11982]):
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnsu
						elif questions_ok[2] in [11889,11890,11891,11892] and questions_ok[3] in [11889,11890,11891,11892]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11893,11894,11895,11896] and questions_ok[3] in [11893,11894,11895,11896]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11897,11898,11899,11900] and questions_ok[3] in [11897,11898,11899,11900]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11901,11902,11903,11904] and questions_ok[3] in [11901,11902,11903,11904]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11905,11906,11907,11908] and questions_ok[3] in [11905,11906,11907,11908]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11909,11910,11911,11912] and questions_ok[3] in [11909,11910,11911,11912]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11913,11914,11915,11916] and questions_ok[3] in [11913,11914,11915,11916]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11917,11918,11919,11920] and questions_ok[3] in [11917,11918,11919,11920]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11921,11922,11923,11924] and questions_ok[3] in [11921,11922,11923,11924]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11925,11926,11927,11928] and questions_ok[3] in [11925,11926,11927,11928]:
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnco
						elif questions_ok[2] in [11929,11930,11931] and questions_ok[3] in [11929,11930,11931]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11929,11930,11931] and questions_ok[3] in [11929,11930,11931]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11935,11936,11937] and questions_ok[3] in [11935,11936,11937]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11938,11939,11940] and questions_ok[3] in [11938,11939,11940]:
							avoid_this_question=1
							print("devo rifare!!")
							break
					#9cnssu
						elif questions_ok[2] in [11941,11942,11943] and questions_ok[3] in [11941,11942,11943]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11944,11945,11946] and questions_ok[3] in [11944,11945,11946]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11947,11948,11949] and questions_ok[3] in [11947,11948,11949]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11950,11951,11952] and questions_ok[3] in [11950,11951,11952]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11953,11954,11955] and questions_ok[3] in [11953,11954,11955]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11956,11957,11958] and questions_ok[3] in [11956,11957,11958]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11959,11960,11961] and questions_ok[3] in [11959,11960,11961]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11962,11963,11964] and questions_ok[3] in [11962,11963,11964]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11965,11966,11967] and questions_ok[3] in [11965,11966,11967]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11968,11969,11970] and questions_ok[3] in [11968,11969,11970]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11971,11972,11973] and questions_ok[3] in [11971,11972,11973]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11974,11975,11976] and questions_ok[3] in [11974,11975,11976]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11977,11978,11979] and questions_ok[3] in [11977,11978,11979]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						elif questions_ok[2] in [11980,11981,11982] and questions_ok[3] in [11980,11981,11982]:
							avoid_this_question=1
							print("devo rifare!!")
							break
						else:
							avoid_this_question=0
							go_on=0
		#redo again, changing one with a never asked ques
		if db_self.never_asked_q==1:
			print("etnro in NEVER ENDING STORYYY")
			for qu in range(len(questions_ok)):
				db_self.curs.execute('''SELECT question_id FROM Kids_full_played_summary
				WHERE kid_id = ? AND question_id = ?''',(db_self.kid_id,questions_ok[qu],))
				q_found = db_self.curs.fetchone()
				print(q_found)
				if q_found:
					qu_index_to_change.append(qu)
					qu_to_change.append(questions_ok[qu])
			print("qu_to_changequ_to_changequ_to_changequ_to_change {}".format(qu_to_change))
			print("qu_to_changequ_tINDEX {}".format(qu_index_to_change))
			
			db_self.curs.execute("DROP TABLE IF EXISTS tmp_fake_tab")
			if len(qu_to_change)==1:
				if type_game!='MISCELLANEOUS':
					if kind_game!='4P':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
						SELECT DISTINCT question_id FROM Questions
						JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
						ON Scenarios_Questions.audio_id = Questions.audio
						WHERE scenario_id = ? AND type = ? 
						AND question_id != ? 
						ORDER BY RANDOM()
						LIMIT ?''',(scenario,type_game,qu_to_change[0],num_questions,))
					elif type_game=='4PSU':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
						SELECT DISTINCT question_id
						FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
						WHERE scenario_id = ? AND type = ? 
						AND question_id != ? 
						ORDER BY RANDOM()
						LIMIT ?''',(scenario,type_game,qu_to_change[0],num_questions,))					
					elif type_game=='4PCO':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
						SELECT DISTINCT question_id
						FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
						WHERE scenario_id = ? AND type = ? 
						AND question_id != ? 
						ORDER BY RANDOM()
						LIMIT ?''',(scenario,type_game,qu_to_change[0],num_questions,))
				else:
					db_self.curs.execute("SELECT type FROM QUESTIONS WHERE question_id = ?",(qu_to_change[0],))
					t_q1 = db_self.curs.fetchone()[0]
					if kind_game!='4P':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
					elif type_game=='4PSU':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))						
					elif type_game=='4PCO':	
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
			elif len(qu_to_change)==2:
				if type_game!='MISCELLANEOUS':
					if kind_game!='4P':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT ?''',(scenario,type_game,qu_to_change[0],qu_to_change[1],num_questions,))
					elif type_game=='4PSU':	
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT ?''',(scenario,type_game,qu_to_change[0],qu_to_change[1],num_questions,))						
					elif type_game=='4PCO':	
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT ?''',(scenario,type_game,qu_to_change[0],qu_to_change[1],num_questions,))						
				else:
					db_self.curs.execute("SELECT type FROM QUESTIONS WHERE question_id = ?",(qu_to_change[0],))
					t_q1 = db_self.curs.fetchone()[0]
					db_self.curs.execute("SELECT type FROM QUESTIONS WHERE question_id = ?",(qu_to_change[1],))
					t_q2 = db_self.curs.fetchone()[0]
					if kind_game!='4P':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q2,qu_to_change[1],))
					elif type_game=='4PSU':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q2,qu_to_change[1],))						
					elif type_game=='4PCO':	
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q2,qu_to_change[1],))						
			elif len(qu_to_change)==3:
				if type_game!='MISCELLANEOUS':
					if kind_game!='4P':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? AND question_id != ? AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT ?''',(scenario,type_game,qu_to_change[0],qu_to_change[1],qu_to_change[2],num_questions,))
					elif type_game=='4PSU':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? AND question_id != ? AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT ?''',(scenario,type_game,qu_to_change[0],qu_to_change[1],qu_to_change[2],num_questions,))						
					elif type_game=='4PCO':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? AND question_id != ? AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT ?''',(scenario,type_game,qu_to_change[0],qu_to_change[1],qu_to_change[2],num_questions,))
				else:
					db_self.curs.execute("SELECT type FROM QUESTIONS WHERE question_id = ?",(qu_to_change[0],))
					t_q1 = db_self.curs.fetchone()[0]
					db_self.curs.execute("SELECT type FROM QUESTIONS WHERE question_id = ?",(qu_to_change[1],))
					t_q2 = db_self.curs.fetchone()[0]
					db_self.curs.execute("SELECT type FROM QUESTIONS WHERE question_id = ?",(qu_to_change[2],))
					t_q3 = db_self.curs.fetchone()[0]
					print("t_q1 {}".format(t_q1))
					print("t_q2 {}".format(t_q2))
					print("t_q3 {}".format(t_q3))
					if kind_game!='4P':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q2,qu_to_change[1],))					
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q3,qu_to_change[2],))
					elif type_game=='4PSU':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q2,qu_to_change[1],))					
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q3,qu_to_change[2],))						
					elif type_game=='4PCO':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q2,qu_to_change[1],))					
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q3,qu_to_change[2],))									
			elif len(qu_to_change)==4:
				if type_game!='MISCELLANEOUS':
					if kind_game!='4P':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? AND question_id != ? AND question_id != ? AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT ?''',(scenario,type_game,qu_to_change[0],qu_to_change[1],qu_to_change[2],qu_to_change[3],num_questions,))
					elif type_game=='4PSU':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? AND question_id != ? AND question_id != ? AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT ?''',(scenario,type_game,qu_to_change[0],qu_to_change[1],qu_to_change[2],qu_to_change[3],num_questions,))
					elif type_game=='4PCO':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? AND question_id != ? AND question_id != ? AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT ?''',(scenario,type_game,qu_to_change[0],qu_to_change[1],qu_to_change[2],qu_to_change[3],num_questions,))
				else:
					db_self.curs.execute("SELECT type FROM QUESTIONS WHERE question_id = ?",(qu_to_change[0],))
					t_q1 = db_self.curs.fetchone()[0]
					db_self.curs.execute("SELECT type FROM QUESTIONS WHERE question_id = ?",(qu_to_change[1],))
					t_q2 = db_self.curs.fetchone()[0]
					db_self.curs.execute("SELECT type FROM QUESTIONS WHERE question_id = ?",(qu_to_change[2],))
					t_q3 = db_self.curs.fetchone()[0]
					db_self.curs.execute("SELECT type FROM QUESTIONS WHERE question_id = ?",(qu_to_change[3],))
					t_q4 = db_self.curs.fetchone()[0]
					print("t_q1 {}".format(t_q1))
					print("t_q2 {}".format(t_q2))
					print("t_q3 {}".format(t_q3))
					print("t_q4 {}".format(t_q4))
					if kind_game!='4P':
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q2,qu_to_change[1],))					
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q3,qu_to_change[2],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							JOIN (Scenarios_Questions JOIN Scenarios USING (scenario_group)) 
							ON Scenarios_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q4,qu_to_change[3],))
					elif type_game=='4PSU':		
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q2,qu_to_change[1],))					
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q3,qu_to_change[2],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4psu_Questions ON Scenarios_4psu_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q4,qu_to_change[3],))									
					elif type_game=='4PCO':		
						db_self.curs.execute('''CREATE TABLE tmp_fake_tab AS
							SELECT DISTINCT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q1,qu_to_change[0],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q2,qu_to_change[1],))					
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q3,qu_to_change[2],))
						db_self.curs.execute('''INSERT INTO tmp_fake_tab (question_id)
							SELECT question_id FROM Questions
							FROM Questions JOIN Scenarios_4pco_Questions ON Scenarios_4pco_Questions.audio_id = Questions.audio
							WHERE scenario_id = ? AND type = ? 
							AND question_id != ? 
							ORDER BY RANDOM()
							LIMIT 1''',(scenario,t_q4,qu_to_change[3],))
			if len(qu_to_change)>0:
				db_self.curs.execute("SELECT * FROM tmp_fake_tab")
				res = db_self.curs.fetchall()
				db_self.curs.execute("DROP TABLE IF EXISTS tmp_fake_tab")
				print("res {}".format(res))
				resp = [item for sublist in res for item in sublist] 
				questions_ok = resp
		print("QUELLO CHE MI INTERESSA ADESSO È!!!! ")
		print("resp finale questions_ok {}".format(questions_ok))
		#######MODIFICO DB_DEFAULT!!!!!		
		#check imposed
		if db_self.imposed_ques_id!=99999:
			print("ENTER IN IMPOSED QUESS")
			print("db_self.imposed_ques_id! {}".format(db_self.imposed_ques_id))
			sel_pos = random.randint(0,len(questions_ok))
			print("selection sel_pos is is {} ".format(sel_pos))
			questions_ok[sel_pos] = db_self.imposed_ques_id
			print("!!!!!questions_ok!!!! BEFORE AFTER CHANGEMENT PERCHE CÈ IMPOSED QUES--> {}".format(questions_ok))

		db_self.curs.execute("SELECT game_id FROM Games ORDER BY game_id DESC LIMIT 1")
		this_game = db_self.curs.fetchall()[0][0]
		print("GAME GAME GAME LAST TROVATO È {}".format(this_game))
		#############change!!################################################### START
		print("contyrollo qui!!! ")
		print("lunghezza questions_ok!!! {}".format(len(questions_ok)))
		print("numnum_questionsnum_questions {}".format(num_questions))
		if len(questions_ok)!=num_questions:
			num_ques_to_ret = num_questions
			db_self.delete_by_id_only_db_default(this_game)
			print("ho cancellato per poi rimettere!!!!")
			db_self.change_num_questions(len(questions_ok), this_game)
			num_questions_new = len(questions_ok)
			print("ho cambiato qui!!!! CALLING CHANGE QUESTIONS!!!!S numero di questrion era maggiore di quello realmente possibile!!!")
			############change necessary time
			if num_questions_new==1 and kind_game in ['1F','2L','5K']:
				new_necessary_time = 10
			if num_questions_new==1 and kind_game in ['3S','4P']:
				new_necessary_time = 12
			if num_questions_new==1 and kind_game=='7O':
				new_necessary_time = 15
			if num_questions_new==1 and kind_game in['8Q','9C']:
				new_necessary_time = 18
			if num_questions_new==1 and kind_game=='6I':
				new_necessary_time = 20

			if num_questions_new==2 and kind_game in ['1F','2L','5K']:
				new_necessary_time = 20
			if num_questions_new==2 and kind_game in ['3S','4P']:
				new_necessary_time = 24
			if num_questions_new==2 and kind_game=='7O':
				new_necessary_time = 30
			if num_questions_new==2 and kind_game in['8Q','9C']:
				new_necessary_time = 36
			if num_questions_new==2 and kind_game=='6I':
				new_necessary_time = 40

			if num_questions_new==3 and kind_game in ['1F','2L','5K']:
				new_necessary_time = 30
			if num_questions_new==3 and kind_game in ['3S','4P']:
				new_necessary_time = 36
			if num_questions_new==3 and kind_game=='7O':
				new_necessary_time = 45
			if num_questions_new==3 and kind_game in['8Q','9C']:
				new_necessary_time = 44
			if num_questions_new==3 and kind_game=='6I':
				new_necessary_time = 60

			if num_questions_new==4 and kind_game in ['1F','2L','5K']:
				new_necessary_time = 40
			if num_questions_new==4 and kind_game in ['3S','4P']:
				new_necessary_time = 48
			if num_questions_new==4 and kind_game=='7O':
				new_necessary_time = 60
			if num_questions_new==4 and kind_game in['8Q','9C']:
				new_necessary_time = 72
			if num_questions_new==4 and kind_game=='6I':
				new_necessary_time = 80			

			db_self.execute_new_query("UPDATE Games SET necessary_time = ? WHERE token = ?",(new_necessary_time,new_id))
			mdl.modify_Games_list(num_questions_new, kind_game, type_game, difficulty, order_difficulty_questions, new_necessary_time, disposable, new_id, num_last_line_g)
		#############change!!################################################### END
		else: 
			num_ques_to_ret = 0
		values_q = []
		for q in questions_ok:
			db_self.curs.execute("SELECT value FROM Questions WHERE question_id = ?",(q,))
			va = db_self.curs.fetchone()[0]
			values_q.append(va)
		print("values_q is {} and order_difficulty_questions is {}".format(values_q,order_difficulty_questions))
		#same and casu not change anything.....per semplicità e tempo che per same dovrei controllare tutto e mettere mega while ma chissene!!!
		if order_difficulty_questions=='asc':
			print("ASC...DEVO CONTROLLARE E FARE SWAP NEL CASO")
			#bubblesort classic
			for n in range(len(values_q)-1, 0, -1):
				for i in range(n):
					if values_q[i] > values_q[i + 1]:
						# swapping data if the element is less than next element in the array
						values_q[i], values_q[i + 1] = values_q[i + 1], values_q[i]
						questions_ok[i], questions_ok[i + 1] = questions_ok[i + 1], questions_ok[i]
			print("dopo asc questions_ok is {}".format(questions_ok))
		elif order_difficulty_questions=='desc':
			print("DESC...DEVO CONTROLLARE E FARE SWAP NEL CASO")
			for n in range(len(values_q)-1, 0, -1):
				for i in range(n):
					if values_q[i] < values_q[i + 1]:
						values_q[i], values_q[i + 1] = values_q[i + 1], values_q[i]
						questions_ok[i], questions_ok[i + 1] = questions_ok[i + 1], questions_ok[i]
			print("dopo desc questions_ok is {}".format(questions_ok))
		#########################################################################
		for k in range(len(questions_ok)):
			dca.add_new_GameQuestion_table(db_self, this_game, questions_ok[k])
			print("exiting from add_predict extern methof add_new_Session_table_with_kid!!!")
		print("----------------------------------------OK GIOCO AGGIUNTO------------------------------------------------------")
		return this_game
	except Exception as e:
		print("--> Don't create a new Game, but take an existing one")
		print("--> Search_str add_new_Game {}".format(search_str))
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
		db_self.curs.execute("SELECT game_id FROM Games WHERE	\
			num_questions = ? AND kind= ? \
			AND type = ? AND difficulty = ? \
			AND order_difficulty_questions = ?",(num_questions, kind_game, type_game, difficulty, order_difficulty_questions,))
		game_found = db_self.curs.fetchone()[0]
		return game_found
