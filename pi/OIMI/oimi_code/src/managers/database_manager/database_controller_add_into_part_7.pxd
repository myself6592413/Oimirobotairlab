cdef annex_new_therapy(db_self, int kid_id, int num_of_treatments, str canonical_intervention)
cdef annex_new_treatment(db_self, int kid_id, str target_goal_1, str target_goal_2, str target_goal_3, int num_of_pastimes, int num_of_sessions, int num_of_entertainments)
cdef annex_new_entertainment(db_self, int kid_id, str class_of_game, str specific_guidelines)
