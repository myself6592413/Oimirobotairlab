"""Info:
Notes:
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import database_manager as db
# ==========================================================================================================================================================
# Queries
# ==========================================================================================================================================================
sql_adjsut_date_trigger = '''
	CREATE TRIGGER IF NOT EXISTS adjust_date
	AFTER UPDATE ON Questions
	FOR EACH ROW 
	BEGIN
		UPDATE Therapies 
		SET duration_days = ( 
			Select Cast ((JulianDay('now') - JulianDay(kick_off_date)) As Integer)
			FROM Therapies);
	END;'''

sql_pastimes_trigger = '''
	CREATE TRIGGER IF NOT EXISTS pastime_trigger_ent
	AFTER INSERT ON Treatments_Entertainments
	FOR EACH ROW 
	BEGIN
		INSERT INTO Treatments_Pastimes (treatment_id, pastime_id)
		SELECT treatment_id, entertainment_id as pastime_id 
		FROM Treatments_Entertainments
		WHERE rowid = (SELECT MAX(rowid) FROM Treatments_Entertainments);
	END;'''

sql_pastimes_trigger_2 = '''
	CREATE TRIGGER IF NOT EXISTS pastime_trigger_ses
	AFTER INSERT ON Treatments_Sessions
	FOR EACH ROW 
	BEGIN
		INSERT INTO Treatments_Pastimes (treatment_id, pastime_id)
		SELECT treatment_id, session_id
		FROM Treatments_Sessions 
		WHERE rowid = (SELECT MAX(rowid) FROM Treatments_Sessions);
	END;'''

sql_kind_games_trigger = '''
	CREATE TRIGGER IF NOT EXISTS complete_kind_trigger
	AFTER INSERT ON Kind_games
	FOR EACH ROW
	BEGIN
		UPDATE Kind_games
		SET details_kind = CASE
				WHEN kind_of_game = '1F' THEN 'game_Find'
				WHEN kind_of_game = '2L' THEN 'game_ColorLed'
				WHEN kind_of_game = '3S' THEN 'game_Same'
				WHEN kind_of_game = '4P' THEN 'game_Pair_as'
				WHEN kind_of_game = '5K' THEN 'game_Knowledge'
				WHEN kind_of_game = '6I' THEN 'game_Intruder' 
				WHEN kind_of_game = '7O' THEN 'game_Order'
				WHEN kind_of_game = '8Q' THEN 'game_Quiz'
				WHEN kind_of_game = '9C' THEN 'game_Count'
				WHEN kind_of_game = '10A' THEN 'game_Miscellanous'
			END
		WHERE kind_of_game IN ('1F','2L','3S','4P','5K','6I','7O','8Q','9C','10A');
	END;'''	

sql_patches_trigger = '''
	CREATE TRIGGER IF NOT EXISTS complete_patches_info_trigger
	AFTER INSERT ON Patches
	FOR EACH ROW
	BEGIN
		UPDATE Patches
			SET genre = CASE
				WHEN element = 'Plain' THEN 'Solid_color'
				WHEN element = 'Lion' THEN 'Animal' 
				WHEN element = 'Monkey' THEN 'Animal' 
				WHEN element = 'Parrot' THEN 'Animal' 
				WHEN element = 'Octopus' THEN 'Animal' 
				WHEN element = 'Bicycle' THEN 'Thing'
				WHEN element = 'Push_scooter' THEN 'Thing'
				WHEN element = 'Umbrella' THEN 'Thing'
				WHEN element = 'Glasses' THEN 'Thing'
				WHEN element = 'Girl' THEN 'People'
				WHEN element = 'Icecream' THEN 'Food'
			END
		WHERE element IN ('Plain', 'Lion', 'Monkey', 'Parrot', 'Octopus', 'Bicycle', 'Push_scooter', 'Girl', 'Glasses', 'Icecream', 'Umbrella');
	END;'''

sql_free_trigger = '''
	CREATE TRIGGER IF NOT EXISTS complete_free_category_trigger
	AFTER INSERT ON Entertainments
	FOR EACH ROW
	BEGIN
		UPDATE Entertainments
			SET category_of_game = CASE
				WHEN class_of_game = 'OnlyManual' THEN 'test'
				WHEN class_of_game = 'OnlyAuto' THEN 'test' 
				WHEN class_of_game = 'ApproachMe' THEN 'familiarization' 
				WHEN class_of_game = 'WakeMeUp' THEN 'familiarization' 
				WHEN class_of_game = 'DontWakeMeUp' THEN 'familiarization' 
				WHEN class_of_game = 'PutPatches' THEN 'familiarization' 
				WHEN class_of_game = 'ImitateMe' THEN 'socialization' 
				WHEN class_of_game = 'GuessMyFeeling' THEN 'socialization'
				WHEN class_of_game = 'DanceWithMe' THEN 'socialization'
				WHEN class_of_game = 'AutonomousMe' THEN 'locomotion'
				WHEN class_of_game = 'FollowingYou' THEN 'locomotion'
				WHEN class_of_game = 'RunningChasingYou' THEN 'locomotion'
				WHEN class_of_game = 'DriveMe' THEN 'locomotion'
				WHEN class_of_game = 'TouchMe' THEN 'touch'
			END
		WHERE class_of_game IN ('OnlyManual', 'OnlyAuto', 'ApproachMe', 'WakeMeUp', 'DontWakeMeUp', 'PutPatches', 
		'ImitateMe', 'GuessMyFeeling', 'DanceWithMe', 'AutonomousMe', 'FollowingYou', 'RunningChasingYou', 'DriveMe', 'TouchMe');
	END;'''

sql_needs_trigger_1 = '''
	CREATE TRIGGER IF NOT EXISTS set_null_matches_to_invalid
	AFTER INSERT ON Needs
	FOR EACH ROW
	WHEN (NEW.match_scope isNull)
	BEGIN
	END;
	'''
sql_needs_trigger_2 = '''
	CREATE TRIGGER IF NOT EXISTS set_null_games_to_invalid
	AFTER INSERT ON Needs
	FOR EACH ROW
	WHEN (NEW.game_scope isNull)
	BEGIN
	END;
	'''
sql_needs_trigger_3 = '''
	CREATE TRIGGER IF NOT EXISTS set_null_question_to_invalid
	AFTER INSERT ON Needs
	FOR EACH ROW
	WHEN (NEW.question_scope isNull)
	BEGIN
	END;
	'''

sql_disposable_trigger_11 = '''
	CREATE TRIGGER IF NOT EXISTS after_ok_disposable_update_in_Session
	AFTER UPDATE ON Sessions
	FOR EACH ROW 
	BEGIN
		UPDATE Matches
		SET disposable = 1
		WHERE match_id IN (
		SELECT Matches.match_id
		FROM Matches LEFT JOIN	(Sessions_Matches LEFT JOIN Sessions USING (session_id))
		ON Matches.match_id = Sessions_Matches.match_id
		WHERE Sessions.disposable = 1
		GROUP BY Matches.match_id
		);
	END;'''
sql_disposable_trigger_12 = '''
	CREATE TRIGGER IF NOT EXISTS after_ko_disposable_update_in_Session
	AFTER UPDATE ON Sessions
	FOR EACH ROW 
	BEGIN
		UPDATE Matches
		SET disposable = 0
		WHERE match_id IN (
		SELECT Matches.match_id
		FROM Matches LEFT JOIN	(Sessions_Matches LEFT JOIN Sessions USING (session_id))
		ON Matches.match_id = Sessions_Matches.match_id
		WHERE Sessions.disposable = 0 
		GROUP BY Matches.match_id
		);
	END;'''
sql_disposable_trigger_21 = '''
	CREATE TRIGGER IF NOT EXISTS after_ok_disposable_update_in_Matches
	AFTER UPDATE ON Matches
	FOR EACH ROW 
	BEGIN
		UPDATE Games
		SET disposable = 1
		WHERE game_id IN (
		SELECT Games.game_id
		FROM Games LEFT JOIN  (Matches_Games LEFT JOIN Matches USING (match_id))
		ON Games.game_id = Matches_Games.game_id
		WHERE Matches.disposable = 1 
		GROUP BY Games.game_id
		);
	END;'''
sql_disposable_trigger_22 = '''
	CREATE TRIGGER IF NOT EXISTS after_ko_disposable_update_in_Matches
	AFTER UPDATE ON Matches
	FOR EACH ROW 
	BEGIN
		UPDATE Games
		SET disposable = 0
		WHERE game_id IN (
		SELECT Games.game_id
		FROM Games LEFT JOIN  (Matches_Games LEFT JOIN Matches USING (match_id))
		ON Games.game_id = Matches_Games.game_id
		WHERE Matches.disposable = 0
		GROUP BY Games.game_id
		);
	END;'''

sql_trigger_summary_insert = '''
	CREATE TRIGGER IF NOT EXISTS update_score_ques
	AFTER INSERT on Full_played_summary
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalQuestion SET tot_errors = tot_errors + (NEW.tot_errors - OLD.tot_errors)
		WHERE question_id in (
		SELECT question_id 
		FROM Full_played_summary INNER JOIN Questions USING (question_id));
	END;
	'''
#ERR GAME ###############################################################################################################################################################################
sql_err_game_ans_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_answers_game
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalGame
		SET tot_answers_game = tot_answers_game + (NEW.tot_answers - OLD.tot_answers)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_game_corr_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_corrects_game
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalGame 
		SET tot_corrects_game = tot_corrects_game + (NEW.tot_corrects - OLD.tot_corrects)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_game_errs_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_errors_game
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalGame 
		SET tot_errors_game = tot_errors_game + (NEW.tot_errors - OLD.tot_errors)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_game_ind_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_indecisions_game
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalGame 
		SET tot_indecisions_game = tot_indecisions_game + (NEW.tot_indecisions - OLD.tot_indecisions)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_game_already_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_already_game
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalGame 
		SET tot_already_game = tot_already_game + (NEW.tot_already - OLD.tot_already)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_game_ans_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_answers_game_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalGame 
		SET tot_answers_game = tot_answers_game + (NEW.tot_answers - OLD.tot_answers)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_game_corr_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_corrects_game_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalGame 
		SET tot_corrects_game = tot_corrects_game + (NEW.tot_corrects - OLD.tot_corrects)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_game_errs_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_errors_game_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalGame 
		SET tot_errors_game = tot_errors_game + (NEW.tot_errors - OLD.tot_errors)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
sql_err_game_ind_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_indecisions_game_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalGame 
		SET tot_indecisions_game = tot_indecisions_game + (NEW.tot_indecisions - OLD.tot_indecisions)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_game_already_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_already_game_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalGame 
		SET tot_already_game = tot_already_game + (NEW.tot_already - OLD.tot_already)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
#ERR MATCH ###############################################################################################################################################################################
sql_err_match_ans_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_answers_match
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalMatch SET tot_answers_match = tot_answers_match + (NEW.tot_answers - OLD.tot_answers)
		WHERE question_id in (
		SELECT question_id 
		FROM Matches JOIN 
			Matches_Games JOIN 
			Games JOIN 
			Questions JOIN ScoresTotalQuestion (question_id))
		);
	END;
	'''
sql_err_match_corr_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_corrects_match
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalMatch SET tot_corrects_match = tot_corrects_match + (NEW.tot_corrects - OLD.tot_corrects)
		WHERE question_id in (
		SELECT question_id 
		FROM Matches JOIN 
			Matches_Games JOIN 
			Games JOIN 
			Questions JOIN ScoresTotalQuestion (question_id))
		);
	END;
	'''
sql_err_match_errs_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_errors_match
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalMatch SET tot_errors_match = tot_errors_match + (NEW.tot_errors - OLD.tot_errors)
		WHERE question_id in (
		SELECT question_id 
		FROM Matches JOIN 
			Matches_Games JOIN 
			Games JOIN 
			Questions JOIN ScoresTotalQuestion (question_id))
		);
	END;
	'''
sql_err_match_ind_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_indecisions_match
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalMatch SET tot_indecisions_match = tot_indecisions_match + (NEW.tot_indecisions - OLD.tot_indecisions)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_match_already_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_already_match
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalMatch SET tot_already_match = tot_already_match + (NEW.tot_already - OLD.tot_already)
		WHERE question_id in (
		SELECT question_id 
		FROM Matches JOIN 
			Matches_Games JOIN 
			Games JOIN 
			Questions JOIN ScoresTotalQuestion (question_id))
		);
	END;
	'''	
sql_err_match_ans_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_answers_match_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalMatch SET tot_answers_match = tot_answers_match + (NEW.tot_answers - OLD.tot_answers)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_match_corr_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_corrects_match_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalMatch SET tot_corrects_match = tot_corrects_match + (NEW.tot_corrects - OLD.tot_corrects)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_match_errs_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_errors_match_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalMatch SET tot_errors_match = tot_errors_match + (NEW.tot_errors - OLD.tot_errors)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_match_already_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_already_match_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalMatch SET tot_already_match = tot_already_match + (NEW.tot_already - OLD.tot_already)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''		
sql_err_match_ind_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_indecisions_match_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalMatch SET tot_indecisions_match = tot_indecisions_match + (NEW.tot_indecisions - OLD.tot_indecisions)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''		
#ERR SESSION ###############################################################################################################################################################################
sql_err_sess_ans_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_answers_sess
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalSession SET tot_answers_session = tot_answers_session + (NEW.tot_answers - OLD.tot_answers)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_sess_corr_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_corrects_sess
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalSession SET tot_corrects_session = tot_corrects_session + (NEW.tot_corrects - OLD.tot_corrects)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_sess_errs_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_errors_sess
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalSession SET tot_errors_session = tot_errors_session + (NEW.tot_errors - OLD.tot_errors)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_sess_ind_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_indecisions_sess
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalSession SET tot_indecisions_session = tot_indecisions_session + (NEW.tot_indecisions - OLD.tot_indecisions)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_sess_already_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_already_sess
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalSession SET tot_already_session = tot_already_session + (NEW.tot_already - OLD.tot_already)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
sql_err_sess_ans_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_ans_sess_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalSession SET tot_answers_session = tot_answers_session + (NEW.tot_answers - OLD.tot_answers)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
sql_err_sess_corr_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_corrects_sess_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalSession SET tot_corrects_session = tot_corrects_session + (NEW.tot_corrects - OLD.tot_corrects)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
sql_err_sess_errs_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_errors_sess_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalSession SET tot_errors_session = tot_errors_session + (NEW.tot_errors - OLD.tot_errors)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_sess_ind_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_indecisions_sess_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalSession SET tot_indecisions_session = tot_indecisions_session + (NEW.tot_indecisions - OLD.tot_indecisions)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_err_sess_already_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_already_sess_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalSession SET tot_already_session = tot_already_session + (NEW.tot_already - OLD.tot_already)
		WHERE question_id in (
		SELECT question_id 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''   
#TYPE ############################################################################################################################################################################### 
sql_type_ans_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_answers_typegame
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalTypeOfGame SET tot_answers_type = tot_answers_type + (NEW.tot_answers - OLD.tot_answers)
		WHERE type_of_game in (
		SELECT type_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
sql_type_corr_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_corrects_typegame
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalTypeOfGame SET tot_corrects_type = tot_corrects_type + (NEW.tot_corrects - OLD.tot_corrects)
		WHERE type_of_game in (
		SELECT type_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_type_errs_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_errors_typegame
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalTypeOfGame SET tot_errors_type = tot_errors_type + (NEW.tot_errors - OLD.tot_errors)
		WHERE type_of_game in (
		SELECT type_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_type_ind_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_indecisions_typegame
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalTypeOfGame SET tot_indecisions_type = tot_indecisions_type + (NEW.tot_indecisions - OLD.tot_indecisions)
		WHERE type_of_game in (
		SELECT type_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_type_already_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_already_typegame
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalTypeOfGame SET tot_already_type = tot_already_type + (NEW.tot_already - OLD.tot_already)
		WHERE type_of_game in (
		SELECT type_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
sql_type_ans_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_answers_typegame_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalTypeOfGame SET tot_answers_type = tot_answers_type + (NEW.tot_answers - OLD.tot_answers)
		WHERE type_of_game in (
		SELECT type_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
sql_type_corr_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_corrects_typegame_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalTypeOfGame SET tot_corrects_type = tot_corrects_type + (NEW.tot_corrects - OLD.tot_corrects)
		WHERE type_of_game in (
		SELECT type_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
sql_type_errs_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_errors_typegame_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalTypeOfGame SET tot_errors_type = tot_errors_type + (NEW.tot_errors - OLD.tot_errors)
		WHERE type_of_game in (
		SELECT type_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_type_ind_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_indecisions_typegame_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalTypeOfGame SET tot_indecisions_type = tot_indecisions_type + (NEW.tot_indecisions - OLD.tot_indecisions)
		WHERE type_of_game in (
		SELECT type_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_type_already_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_already_typegame_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalTypeOfGame SET tot_already_type = tot_already_type + (NEW.tot_already - OLD.tot_already)
		WHERE type_of_game in (
		SELECT type_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''		
#KIND ############################################################################################################################################################################### 
sql_kind_ans_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_answers_kindgame
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalKindOfGame SET tot_answers_kind = tot_answers_kind + (NEW.tot_answers - OLD.tot_answers)
		WHERE kind_of_game in (
		SELECT kind_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_kind_corr_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_corrects_kindgame
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalKindOfGame SET tot_corrects_kind = tot_corrects_kind + (NEW.tot_corrects - OLD.tot_corrects)
		WHERE kind_of_game in (
		SELECT kind_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
sql_kind_already_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_already_kindgame
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalKindOfGame SET tot_already_kind = tot_already_kind + (NEW.tot_already - OLD.tot_already)
		WHERE kind_of_game in (
		SELECT kind_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_kind_errs_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_errors_kindgame
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalKindOfGame SET tot_errors_kind = tot_errors_kind + (NEW.tot_errors - OLD.tot_errors)
		WHERE kind_of_game in (
		SELECT kind_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_kind_ind_trigger_after_update = '''
	CREATE TRIGGER IF NOT EXISTS update_indecisions_kindgame
	AFTER UPDATE ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalKindOfGame SET tot_indecisions_kind = tot_indecisions_kind + (NEW.tot_indecisions - OLD.tot_indecisions)
		WHERE kind_of_game in (
		SELECT kind_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_kind_ans_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_answers_kindgame_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalKindOfGame SET tot_answers_kind = tot_answers_kind + (NEW.tot_answers - OLD.tot_answers)
		WHERE kind_of_game in (
		SELECT kind_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_kind_corr_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_corrects_kindgame_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalKindOfGame SET tot_corrects_kind = tot_corrects_kind + (NEW.tot_corrects - OLD.tot_corrects)
		WHERE kind_of_game in (
		SELECT kind_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_kind_errs_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_errors_kindgame_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalKindOfGame SET tot_errors_kind = tot_errors_kind + (NEW.tot_errors - OLD.tot_errors)
		WHERE kind_of_game in (
		SELECT kind_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
sql_kind_ind_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_indecisions_kindgame_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalKindOfGame SET tot_indecisions_kind = tot_indecisions_kind + (NEW.tot_indecisions - OLD.tot_indecisions)
		WHERE kind_of_game in (
		SELECT kind_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''
sql_kind_already_trigger_1_after_insert = '''
	CREATE TRIGGER IF NOT EXISTS insert_already_kindgame_1
	AFTER INSERT ON ScoresTotalQuestion
	WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
	BEGIN
		UPDATE ScoresTotalKindOfGame SET tot_already_kind = tot_already_kind + (NEW.tot_already - OLD.tot_already)
		WHERE kind_of_game in (
		SELECT kind_of_game 
		FROM ScoresTotalQuestion INNER JOIN Questions USING (question_id));
	END;
	'''	
########################################################################################################################################################
sql_deletion_ques_trigger = ''' 
	CREATE TRIGGER IF NOT EXISTS after_question_deletion 
	AFTER delete ON Questions
	BEGIN
		DELETE FROM Questions_Patches WHERE question_id isNull;
		DELETE FROM Games_Questions WHERE question_id isNull;
		DELETE FROM ScoresTotalQuestion WHERE question_id isNull;
		DELETE FROM Scenarios_Questions WHERE question_id isNull;
	END; '''
sql_deletion_audios_trigger = ''' 
	CREATE TRIGGER IF NOT EXISTS after_audio_deletion 
	AFTER delete ON Audios
	BEGIN
		DELETE FROM Questions WHERE audio_id isNull;
	END; '''
sql_deletion_kid_trigger_1 = ''' 
	CREATE TRIGGER IF NOT EXISTS after_kid_deletion_1 
	AFTER delete ON Kids
	WHEN 1 = (SELECT count(*) FROM sqlite_master WHERE type='table' AND name='ScoresTotalQuestion')
	BEGIN
		DELETE FROM ScoresTotalQuestion WHERE question_id isNull;
	END; '''
#WHEN 1 = (SELECT val FROM OimiSettings WHERE keyp = 'fireTrigger')
sql_deletion_kid_trigger_2 = ''' 
	CREATE TRIGGER IF NOT EXISTS after_kid_deletion_2 
	AFTER delete ON Kids
	WHEN 1 = (SELECT count(*) FROM sqlite_master WHERE type='table' AND name='Kids_Sessions')
	BEGIN
		DELETE FROM Kids_Sessions WHERE kid_id isNull;
	END; '''
sql_deletion_kid_trigger_3 = ''' 
	CREATE TRIGGER IF NOT EXISTS after_kid_deletion_3
	AFTER delete ON Kids
	WHEN 1 = (SELECT count(*) FROM sqlite_master WHERE type='table' AND name='Kids_Achievements')
	BEGIN
		DELETE FROM Kids_Achievements WHERE kid_id isNull;
	END; '''
sql_deletion_kid_trigger_4 = ''' 
	CREATE TRIGGER IF NOT EXISTS after_kid_deletion_4
	AFTER delete ON Kids
	WHEN 1 = (SELECT count(*) FROM sqlite_master WHERE type='table' AND name='Kids_Impositions')
	BEGIN
		DELETE FROM Kids_Impositions WHERE kid_id isNull;
	END; '''
sql_deletion_kid_trigger_5 = ''' 
	CREATE TRIGGER IF NOT EXISTS after_kid_deletion_5
	AFTER delete ON Kids
	WHEN 1 = (SELECT count(*) FROM sqlite_master WHERE type='table' AND name='Kids_Symptoms')
	BEGIN
		DELETE FROM Kids_Symptoms WHERE kid_id isNull;
	END; '''
sql_deletion_kid_trigger_6 = ''' 
	CREATE TRIGGER IF NOT EXISTS after_kid_deletion_6
	AFTER delete ON Kids
	WHEN 1 = (SELECT count(*) FROM sqlite_master WHERE type='table' AND name='Kids_Matches')
	BEGIN
		DELETE FROM Kids_Matches WHERE kid_id isNull;
	END; '''
sql_deletion_kid_trigger_7 = ''' 
	CREATE TRIGGER IF NOT EXISTS after_kid_deletion_7
	AFTER delete ON Kids
	WHEN 1 = (SELECT count(*) FROM sqlite_master WHERE type='table' AND name='Kids_Games')
	BEGIN
		DELETE FROM Kids_Games WHERE kid_id isNull;
	END; '''

sql_deletion_kid_achievement_trigger = ''' 
	CREATE TRIGGER IF NOT EXISTS after_achievement
	AFTER delete ON Achievements
	WHEN 1 = (SELECT count(*) FROM sqlite_master WHERE type='table' AND name='Kids_Achievements') 
	BEGIN
		DELETE FROM Kids_Achievements WHERE achievement_name isNull; 
	END; '''
sql_deletion_kid_imposition_trigger = ''' 
	CREATE TRIGGER IF NOT EXISTS after_imposition
	AFTER delete ON Impositions
	WHEN 1 = (SELECT count(*) FROM sqlite_master WHERE type='table' AND name='Kids_Impositions') 
	BEGIN
		DELETE FROM Kids_Impositions WHERE imposition_name isNull; 
	END; '''
sql_deletion_kid_symptom_trigger = ''' 
	CREATE TRIGGER IF NOT EXISTS after_symptom
	AFTER delete ON Symptoms
	WHEN 1 = (SELECT count(*) FROM sqlite_master WHERE type='table' AND name='Kids_Symptoms')
	BEGIN
		DELETE FROM Kids_Symptoms WHERE symptom_name isNull; 
	END; '''
