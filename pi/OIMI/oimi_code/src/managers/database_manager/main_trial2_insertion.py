import sys
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
import database_manager as db
import audio_blob_conversion as abc

if __name__ == '__main__':	
	kid_id = 1000
	############################################################################################# session 2005
	"""
	session_id = 2005
	the_session_feeds = ['very','Bad',0.5]
	matches_feeds = [['match','Awful',0.5],['match','Awful',0.5],['match','Awful',0.5]]
	games_feeds = [['game','Awful',0.5],['game','Bad',0.5],['game','Bad',0.5],
	['game','Bad',0.5],['game','Bad',0.5],['game','Bad',0.5],
	['game','Bad',0.5],['game','Bad',0.5],['game','Bad',0.5],['game','Bad',0.5]]
	questions_feeds = [
	['very','Awful',0.5],['very','Bad',0.6],['very','Good',0.7],
	['very','Awful',0.5],['very','Bad',0.6],['very','Good',0.7],
	['very','Awful',0.5],['very','Bad',0.6],['very','Good',0.7],
	['very','Awful',0.5],['very','Bad',0.6],['very','Good',0.7],
	['very','Awful',0.5],['very','Bad',0.6],['very','Good',0.7],
	['very','Awful',0.5],['very','Bad',0.6],['very','Good',0.7],
	['very','Awful',0.5],['very','Bad',0.6],['very','Good',0.7],
	['very','Awful',0.5],['very','Bad',0.6],['very','Good',0.7],['very','Excellent',0.5]]

	
	time_sess = 31.22
	matches_results = [[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123]]
	games_results = [[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123]]
	questions_results = [
	[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],
	[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],
	[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],
	[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],
	[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],
	[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],
	[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],
	[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123],[1,2,3,4,5,100.123]]
	"""
	############################################################################################# session 2004
	session_id = 2004
	the_session_feeds = ['very','Bad',0.51]
	matches_feeds = [['match','Awful',0.41],['match','Awful',0.41],['match','Awful',0.41]]
	games_feeds = [['game','Awful',0.3],['game','Bad',0.3],['game','Bad',0.3],
	['game','Bad',0.3],['game','Bad',0.3],['game','Bad',0.3],
	['game','Bad',0.3],['game','Bad',0.3]]
	questions_feeds = [
	['very','Awful',0.51],['very','Bad',0.61],['very','Good',0.71],
	['very','Awful',0.51],['very','Bad',0.61],['very','Good',0.71],
	['very','Awful',0.51],['very','Bad',0.61],['very','Good',0.71],
	['very','Awful',0.51],['very','Bad',0.61],['very','Good',0.71],['very','Excellent',0.81]]

	#qq = '''UPDATE Kids_Session SET
	#quantity_feedback_session = ?, grade_feedback_session = ?, value_feedback_session = ?
	#WHERE kid_id = 1000 AND session_id = 2005'''
	#par = (the_session_feeds[0],the_session_feeds[1],the_session_feeds[2])

	time_sess = 54.54
	matches_results = [[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],[11,22,33,44,55,200.1]]
	games_results = [[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],
	[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],[11,22,33,44,55,200.1]]
	questions_results = [
	[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],
	[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],
	[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],
	[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],[11,22,33,44,55,200.1],[11,22,33,44,55,200.1]]
	
	# now the costructor is changed !!! with argument adjust_dates yes, no
	imidb = db.DatabaseManager() 
	with db.DatabaseManager():
		#oimidb.include_all_feeds_in_once_given_session(kid_id, session_id, questions_feeds, the_session_feeds, matches_feeds, games_feeds)
		#oimidb.include_all_results_in_once_given_session(kid_id, session_id, time_sess, questions_results, matches_results, games_results)
		
		#oimidb.include_all_feeds_in_once_given_session(kid_id, session_id, questions_feeds, the_session_feeds, matches_feeds, games_feeds)
		#oimidb.include_all_results_in_once_given_session(kid_id, session_id, time_sess, questions_results, matches_results, games_results)

		#oimidb.execute_param_query(qq,par)
		#oimidb.generic_commit()
		#oimidb.store_scores_results_offline()

		#max 6 impo....
		customized = []

		impo_mix = 'mixed'
		impo_never = 'question_never_asked'
		impo_diff_m_1 = 'create Easy_1 match'
		impo_diff_m_2 = 'create Easy_1 match'
		impo_diff_m_3 = 'create Normal_1 match'
		impo_diff_m_4 = 'create Normal_2 match'
		impo_diff_m_5 = 'create Medium_1 match'
		impo_diff_m_6 = 'create Medium_2 match'
		impo_diff_m_7 = 'create Hard_1 match'
		impo_diff_m_8 = 'create Hard_2 match'
		impo_diff_g_1 = 'create Easy_1 game'
		impo_diff_g_2 = 'create Easy_1 game'
		impo_diff_g_3 = 'create Normal_1 game'
		impo_diff_g_4 = 'create Normal_2 game'
		impo_diff_g_5 = 'create Medium_1 game'
		impo_diff_g_6 = 'create Medium_2 game'
		impo_diff_g_7 = 'create Hard_1 game'
		impo_diff_g_8 = 'create Hard_2 game'

		impo_compl_1 = 'complex_easy'
		impo_compl_2 = 'complex_normal'
		impo_compl_3 = 'complex_medium'
		impo_compl_4 = 'complex_hard'

		impo_type_1f_1 = 'type_game_1FCACO'
		impo_type_1f_2 = 'type_game_1FCACOA'
		impo_type_1f_3 = 'type_game_1FCOD'
		impo_type_1f_4 = 'type_game_1FCODT'
		impo_type_1f_5 = 'type_game_1FCOS'
		impo_type_1f_6 = 'type_game_1FGCA'
		impo_type_1f_7 = 'type_game_1FSUCOD'
		impo_type_1f_8 = 'type_game_1FSUCODT'
		impo_type_1f_9 = 'type_game_1FSUCOS'
		impo_type_1f_10 = 'type_game_1FSUD'
		impo_type_1f_11 = 'type_game_1FSUDT'
		impo_type_1f_12 = 'type_game_1FSUS'
		impo_type_2l_1 = 'type_game_2LDBOB'
		impo_type_2l_2 = 'type_game_2LDBOC'
		impo_type_2l_3 = 'type_game_2LDSU'
		impo_type_2l_4 = 'type_game_2LDSU2'
		impo_type_2l_5 = 'type_game_2LDSUOB'
		impo_type_2l_6 = 'type_game_2LDSUOC'
		impo_type_2l_7 = 'type_game_2LST'
		impo_type_2l_8 = 'type_game_2LSU'
		impo_type_2l_9 = 'type_game_2LTWO'
		impo_type_3s_1 = 'type_game_3SCO'
		impo_type_3s_2 = 'type_game_3SSU'
		impo_type_4p_1 = 'type_game_4PCO'
		impo_type_4p_2 = 'type_game_4PSU'
		impo_type_5k_1 = 'type_game_5KD'
		impo_type_5k_2 = 'type_game_5KS'
		impo_type_6I = 'type_game_6I'
		impo_type_7o_1 = 'type_game_7OCO2'
		impo_type_7o_2 = 'type_game_7OCO3'
		impo_type_7o_3 = 'type_game_7OCO4'
		impo_type_7o_4 = 'type_game_7OL2'
		impo_type_7o_5 = 'type_game_7OL3'
		impo_type_7o_6 = 'type_game_7OSU2'
		impo_type_7o_7 = 'type_game_7OSU3'
		impo_type_7o_8 = 'type_game_7OSU4'
		impo_type_8q_1 = 'type_game_8QTCODB1'
		impo_type_8q_2 = 'type_game_8QTCODB2'
		impo_type_8q_3 = 'type_game_8QTCODC1'
		impo_type_8q_4 = 'type_game_8QTCODC2'
		impo_type_8q_5 = 'type_game_8QTCOSB1'
		impo_type_8q_6 = 'type_game_8QTCOSB2'
		impo_type_8q_7 = 'type_game_8QTCOSC1'
		impo_type_8q_8 = 'type_game_8QTCOSC2'
		impo_type_8q_9 = 'type_game_8QTKNDC'
		impo_type_8q_10 = 'type_game_8QTKNSB'
		impo_type_8q_11 = 'type_game_8QTKNSC'
		impo_type_8q_12 = 'type_game_8QTPRSB'
		impo_type_8q_13 = 'type_game_8QTPRSC'
		impo_type_8q_14 = 'type_game_8QTSUDB1'
		impo_type_8q_15 = 'type_game_8QTSUDB2'
		impo_type_8q_16 = 'type_game_8QTSUDC1'
		impo_type_8q_17 = 'type_game_8QTSUDC2'
		impo_type_8q_18 = 'type_game_8QTSUSB1'
		impo_type_8q_19 = 'type_game_8QTSUSB2'
		impo_type_8q_20 = 'type_game_8QTSUSC1'
		impo_type_8q_21 = 'type_game_8QTSUSC2'
		impo_type_8q_22 = 'type_game_8QTSUSCOC1'
		impo_type_9c_1 = 'type_game_9CNCO'
		impo_type_9c_2 = 'type_game_9CNSPA'
		impo_type_9c_3 = 'type_game_9CNSPX'
		impo_type_9c_4 = 'type_game_9CNSSU'
		impo_type_9c_5 = 'type_game_9CNSU'

		impo_kind_1 = 'kind_game_1F'
		impo_kind_2 = 'kind_game_2L'
		impo_kind_3 = 'kind_game_3S'
		impo_kind_4 = 'kind_game_4P'
		impo_kind_5 = 'kind_game_5K'
		impo_kind_6 = 'kind_game_6I'
		impo_kind_7 = 'kind_game_7O'
		impo_kind_8 = 'kind_game_8Q'
		impo_kind_9 = 'kind_game_9C'

		impo_num_matches_1 = 'choose one_match'
		impo_num_matches_2 = 'choose two_matches'
		impo_num_matches_3 = 'choose three_matches'
		impo_num_matches_4 = 'choose four_matches'
		impo_num_games_1 = 'choose one_game'
		impo_num_games_2 = 'choose two_games'
		impo_num_games_3 = 'choose three_games'
		impo_num_games_4 = 'choose four_games'
		impo_num_questions_1 = 'choose one_question'	
		impo_num_questions_2 = 'choose two_questions'	
		impo_num_questions_3 = 'choose three_questions'	
		impo_num_questions_4 = 'choose four_questions'	

		imporedo = 'redo'

		impo_ask1 = 'ask_quest_3394'
		impo_ask2 = 'ask_quest_655'
		
		customized.append(impo_num_matches_3)
		customized.append(impo_num_games_2)
		customized.append(impo_ask2)
		customized.append(impo_kind_2)
		customized.append(impo_type_9c_1)
		customized.append(imporedo)

		oimidb.add_new_Session(0,0,0,customized)