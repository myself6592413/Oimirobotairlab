"""Info:
Notes:
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Documentation
# ==========================================================================================================================================================
##### PRELIMINARY
def database_class_doc():
	""" Info
			Contains all methods that involves the use of oimi database (oimi_database.db)
			Used 
				online => stores games results, games issues, kids improvements/stats/issues
				offline => creates reports, update tables, cretion predefinite elements 
		Details
			SUMMARY of tables:
			#Necessary data 
				Audios					All wav file for all oimi sounds/speeches.
				Kids					General useful information of all children that played with oimi.
			#Playing game 
				Sessions				Container of matches with one purpose in common and a unique difficulty, with a predeterminated time.
										(list of all sessions ever created (some predefinite, some created realtime, some disposable).
				Matches					Set of games that shares a certain difficulty.
										(list of all sessions ever created (some predefinite, some created realtime, some disposable). 
				Games					Series of questions asked during the game, with a certain difficulty and a type in common.
										(all games ever created (some predefinite, some created realtime, some disposable, some not).
				Available_games			All possible kind of games.			
				Questions				List of all available question with their immutable difficulty and type of game of reference.
				Patches					List of all available patches [subject,color,shape,genre].
				Scenarios 				List of scenarios (game environment)
			# Playing stats
				ScoresQuestion			Summary of all errors + indecisions + time ever done by one kid for each question, during all his/her completed sessions.
				ScoresKindOfGame		Summary of all errors + indecisions + time ever done by one kid for each type of game, during all completed sessions.
				ScoresGame				Errors + indecisions + time done in a certain game in one new entire session.
				ScoresMatch				Errors + indecisions + time done in a match in one new entire session.
				ScoresSession			Errors + indecisions + time done in a new session.
				Kids_played_recap		Complete resume of all games history for all kids.
				Kids_full_played_recap	Resume of all game history just for a specific kid, for real-time match.
			# Features 
			# of a spefic kid until now, gained during previous experience with oimi, specified before playing, 
			# used for creation of sessions-matches--games and finally questions.s
				Achievements			Catalogue of games goals, reached objectives.
				Symptoms				Catalogue of known issues, deficiencies.
				Needs					Catalogue of lackness that need to be improved.
				Impositions				Catalogue of arbitrary fixed setting(s) to follow during game.
			# Bridges 
				Kids_Achievements
				Kids_Needs
				Kids_Symptoms
				Kids_Impositions
				Kids_Sessions
				Sessions_Matches
				Matches_Games
				Games_Questions
				Questions_Patches
				Scenarios_Patches
		Attributes
			-deletions_list	
				Lists of lists of ids (one for each table that contain an autoincremented integer ID)
					--> [Kids, Sessions, Matches, Games, Questions, Needs, Scenarios], + Kids_Impositions + Kids_Achievements + Kids_Symptoms + Session_Impositions (ok are 9!!!)
					(NEW!! ADDED the new 3 class) 
					[Kids, Therapy, Treatment, Sessions, Matches, Games, Questions, Needs, Scenarios, Entertainment],
					(NEW!! ) [0, 1, 2, 3, 4, 5, 6, 7, 8],
					for fixing pending ids of removed rows changing the proper entrance in the (hidden/config) sqlite_sequence table 
					(that retain the counter of ids for those tables).
					Updated after every deletion of an element in those tables occurs, 
					with the aim of recording that id (the one of removed entity), and assign it later to the next created element of same class type.
					In this way the count order of ID is preserved.
			-last_words
				List of all last entries for each table, used when a query is automatically changed adding a constraint, 
					when is necessary to update oimi_queries.py file.
					Namely, for handling the issue of removing the comma in new insered line in the case of addition / removal of last constraint of the query.
		Methods
			+setup_database
				Checks sqlite options.
				Creates or Starts link connetion with oimi database and define all settings (eg. foreign key usability).
		Notes
			# For supplementary info about tables, look at oimi_games structure documentation 
				eg cannato!! creo un alias per farlo!! richiedo documentazione ... e la trova stampata su file! 
				faccio script comando da terminal per creare pdf 
				metto qui anche il comando da terminale print(database_class_doc.__doc__) ??? 
				metto anche have a look to uml file
		Author 
			Colombo Giacomo Airlab 2020
	"""
def database_setup_doc():
	""" Info
			Checks sqlite options.
			Creates or Starts link connetion with oimi database and define all settings (eg. foreign key usability).
		Attributes
			-conn
			-curs
		Methods 
			+lite.connect()
			+conn.cursor()																		
			+run()
			+curs.execute()
			+curs.execute()
			+curs.fetchone()
		Details
			=> conn 
				connection => Object that represents the database.
				If URI filenames are recognized when the database connection is originally opened, then URI filenames will also be recognized on ATTACH statements. 
				Similarly, if URI filenames are not recognized when the database connection is first opened, they will not be recognized by ATTACH.
				Specify the Uniform Resource Identifiers! connects to db with SQLITE_OPEN_READWRITE (but not SQLITE_OPEN_CREATE) 
				level of isolation in case two sources read same tables at the same time 
					IMMEDIATE attempts to acquire a reserved lock immediately. If it succeeds,
					it guarantees the write locks will be available to the transaction when they are needed, 
					but still allows other clients to continue to access the database for read-only operations.
					BEGIN IMMEDIATE = command goes ahead and starts a write transaction, and thus blocks all other writers.
			=> curs
				Returns an instance of Cursor that allows the creation of all methos pointing to the right db.
			=> run()
				Calls bash command to create database if not already exists! 
				_shell=True => means executing the code through the shell. It causes subprocess to spawn an intermediate shell process, and tell it to run the command.
					Is Necessary or Popen(*popenargs, **kwargs) will fail as process.
				_VACUUM => Creates a valid empty SQLite database file, including the root database page and the database header.
					The VACUUM command...
						*Rebuilds the database file, repacking it into a minimal amount of disk space.
						*Doesn't change the content of the database except the rowid values. 
						*Using = INTEGER PRIMARY KEY column, the VACUUM does not change those values. 
							Except using analiased rowid, the VACUUM command will reset/change the rowid values. 
						*Builds the rowid index from scratch.
						*Works only on the main database.
						*Useful especially when you delete large tables or indexes from a database.
			=> curs.connection
				(Read-only) provides the SQLite database Connection used by the Cursor object, 
				for comparing it to proper connection in order to confirm success/failure of connection.
			=> vers
				Container for bash command output (capture_output=True) 
				stdout[:7] = slices on byte objects taking only relevant info (instances of parent types).
				decode() = decode result from bytes to string:
					_encoding argument 'UTF-8'. 
					_ error handling 'strict', meaning that encoding errors raise a UnicodeError.
			=> PRAGMA foreign_keys = ON =>			
				1)The PRAGMA statement is an SQL extension specific to SQLite and used to modify the operation.
					of the SQLite library or to query the SQLite library for internal data (not table!).
				2)Allow the enforcement of foreign key constraints (used to enforce existance relationships between tables) --> set off by Default 
			=> execute()
				Executes an SQL statement.
			=> executemany()
				Executes an SQL command against all parameter sequences or mappings found in the sequence seq_of_parameters.
			=> fetchone()
				Fetches the next row of a query result set, returning a single sequence (or None when no more data is available).	
		Notes
			#cimport of relative pxd file.
			#cdef are allowed cause is called internall by __cinit__ 
	"""
def database_drop_all_doc():
	""" Info
			Clears and clean completely oimi database.
		Details
			=> PRAGMA writable_schema = ON, now the sqlite_schema table can be changed using ordinary UPDATE, INSERT, and DELETE statements.
			The PRAGMA statement is an SQL extension specific to SQLite and used to modify the operation.
			of the SQLite library or to query the SQLite library for internal data (not table!). 
			=> The deletion is done with "brute force" from sqlite_master controller.
			=> VACUUM is called to recover the deleted space.
			=> The integrity check pragma tests for unused sessions and formatting and consistency of tables, constraints and entries. 
			=> The foreign_key_check pragma returns one row output for each foreign key violation.
		Notes 
			Called as first method to reset database, before building and populate the entire db from scratch.
	"""
def execute_a_query_doc():
	""" Info
			Accomplish a precise sql instruction which involves the database
		Details
			Uses (one or more) received parameter(s) if is not equal to zero
			Returns the result 
			 => self.curs.fetchall()
	"""
def execute_another_query_doc():
	""" Info
			Accomplish a precise sql instruction which involves the database
		Details
			For insert a single parameter in a selection
			Uses (one or more) received parameter(s) if is not equal to zero
			Returns the result 
			 => self.curs.fetchall()
	"""
##### BUILDING DB
def build_OimiSettings_doc():
	""" Info
			Creates an ausiliary table in which a value is set to one or zero,
			for deciding if a certain sql trigger can start or not. 
	"""

def build_Scenarios_doc():
	""" Info
			Creates table with all Scenarios.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table if exists.
			Creates the table. 
			Inserts all audios created so far. 
			Allocates all modifications into db.
				=> commit()
	"""	
def build_Kids_doc():
	""" Info
			Creates table with all Kids, with their basic info, that have ever used oimi robot.
			Inserts all data coming from default lists.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
				=> reload(dbdl)		   
			Reads the query from oimi_queries.py storing the proper portion of text that contains it, from begin to end
				=> lines = fp.readlines() 
				=> chunk = lines[begin:end]
			Cleans creation_query for reading 
				Extracts string from list
					=> chunk.pop(0) 
				Remove all \n newlines
					=> s.strip()
				Binds all lines in one
					=> join()
				Removes any ''' chars by regexp
					=> re.sub()
			If the table of Kids already exists (maybe for some previous errros) is cancelled and created again.
			In order to customize the ID AUTOINCREMENT integer value, the first kid row is added separatedly from others, the table must contain at least one row. 
				So the entire lists of Kids is copied (into all_kids), for inserting only the first element of the list initially.
				=> self.curs.execute()
			At this point the SQLITE_SEQUENCE (sqlite hidden table) is modified, for placing the desired value of ID for the Kids 
				(starts from 1000 instead of 1).			
			Then all others kids (of the pre-filled list) are inserted into the table. 
				=> self.curs.executemany()
			Allocates all modifications into db.
				=> commit()
		Notes
			One single commit is enough 
	"""
def build_Achievements_doc():
	""" Info
			Creates the table whit the cataloque of all existing achievements.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table if exists.
			Creates the table. 
			Inserts all audios created so far.
				=> self.curs.execute() 
			Allocates all modifications into db 
				=> commit()
		Notes
			The comma in (item,) is fundamental 
			The primary unique keys is the string name of the achievement.
			The insertion is done with execute() not executemany() method
	"""	
def build_Symptoms_doc():
	""" Info
			Creates the table whit the cataloque of all possible symptoms.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Removes old version of the table if exists.
			Creates the table. 
			Inserts all audios created so far. 
				=> self.curs.execute()			  
			Allocates all modifications into db.
				=> commit()		
		Notes
			The primary unique keys is the string name of the symptom.
			The insertion is done with execute() not executemany() method
	"""	
def build_Impositions_doc():
	""" Info
			Creates the table whit the cataloque of all possible impositions.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)	  
			Removes old version of the table if exists.
			Creates the table. 
			Inserts all audios created so far. 
			Allocates all modifications into db.
				=> commit()
		Notes
			The primary unique keys is the string name of the imposition.
			The insertion is done with execute() not executemany() method
	"""	
def build_Available_games_doc():
	""" Info
			Creates the table whit the cataloque of all available kind of games.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)	  
			Removes old version of the table if exists.
			Creates the table. 
			Inserts all audios created so far.
				=> self.curs.execute()
			Allocates all modifications into db.
				=> commit()
		Notes
			The primary unique keys is the string name of the kind of games
			The insertion is done with execute() not executemany() method
	"""	
def build_Kind_games_doc():
	""" Info
			Creates the table whit all type (macro-set) of games.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)	  
			Removes old version of the table if exists.
			Creates the table. 
			Inserts all audios created so far.
				=> self.curs.execute()
			Allocates all modifications into db.
				=> commit()
		Notes
			The primary unique keys is the string name of the Kind of games
			The insertion is done with execute() not executemany() method
	"""	
def build_Needs_doc():
	""" Info
			Creates the table where Needs will be stored. Fills it with the predefinite Needs from db_default_lists.py
			Needs focuses:    
			    'focus_on speed',                 # improve time for solve questions, games, matches, session, start audio "mettici meno tempo"
		        'focus_on concentration',         # reduce number of indecisions, increase feedback, audio rules, start audio "concentrati"
		        'focus_on_error',                 # improve scores in genearl or in a certain questions, start audio "meno errori stavolta", repeate a game/question in which result are bad
		        'focus_on comprehension',         # repeat or not again rules, repeat same game with no rules, choose more difficult knowledge games, start audio "concentrati"
		        'focus_on resistance',            # improve num of questions, games, matches, start audio "resisti manca poco"
		        'focus_on achievements',          # resolve one / more task in the achievements list 
		        'focus_on automation',    		  # reduce audio dialogue during game that guide kid in execution start audio, repeat same game this time with no audio between answers "ricordati come hai fatto prima"
		        'focus_on rush'                   # increase time before receiving the ans, start audio "fai piano"
		        'focus_on self_esteem'            # reduce exhortation and continuos feedback

		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)	  
			If (for some previous errros) the table Needs already exists, is cancelled and created again.
			If the table of Needs already exists (maybe for some previous errros) is cancelled and created again.
			In order to customize the ID AUTOINCREMENT integer value, the first kid row is added separatedly from others, the table must contain at least one row. 
				So the entire lists of Needs is copied (into all_needs), for inserting only the first element of the list initially.
				=> self.curs.execute()
			At this point the SQLITE_SEQUENCE (sqlite hidden table) is modified, for placing the desired value of ID for the Needs (from 7000).			
			Then also all others Needs of the pre-filled list are inserted into the table. 
				=> self.curs.executemany()
			Allocates all modifications into db.
				=> commit()				   
		Notes 
			One single commit is enough 
	"""
def build_Audios_doc():
	""" Info
			Creates the table where all oimi speeches (wav format) are stored. 
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table if exists.
			Creates the new table.
			Inserts all audios created so far. 
			Allocates all modifications into db.
				=> commit()
		Notes
			The audio integer ID correspondes to the real name of the audio, not to a random (or autoincremented) primary key.
	"""    
def build_Patches_doc():
	""" Info
			Creates the table of all patches available for oimi games
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Cancels table if already exists.
			Builds new table of Patches.
		Notes
			The insertions in this case is done separately, for enabling the trigger of genre creation.
	"""
def build_Questions_doc():
	""" Info
			Creates the table where Questions will be stored. Fills it with the predefinite Session list of db_default_lists.py
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			If (for some previous errros) the table Questions already exists, is cancelled and created again.
			In order to customize the ID AUTOINCREMENT integer value, the first kid row is added separatedly from others, the table must contain at least one row. 
				So the entire lists of questions is copied (into all_questions), for inserting only the first element of the list initially.
				=> self.curs.execute()
			At this point the SQLITE_SEQUENCE (sqlite hidden table) is modified, for placing the desired value of ID for the Questions (from 5000).			
			Then all others Questions of the pre-filled list are inserted into the table.
				=> self.curs.executemany()
			Allocates all modifications into db. 
				=> commit()				   
	"""
def build_Games_doc():
	""" Info
			Creates the table where Games will be stored. Fills it with the predefinite Session list of db_default_lists.py
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			If (for some previous errros) the table Games already exists, is cancelled and created again.
			In order to customize the ID AUTOINCREMENT integer value, the table must contain at least one row.
			Thus the entire lists of Games is copied (into all_games), for inserting only the first element of the list initially. 
				=> self.curs.execute()
			At this point the SQLITE_SEQUENCE (sqlite hidden table) is modified, for placing the desired value of ID for the Games (from 4000).			
			Then all others Games of the pre-filled list are inserted into the table.
				=> self.curs.executemany()
			Allocates all modifications into db. 
				=> commit()				   
	"""
def build_Matches_doc():
	""" Info
			Creates the table where Matches will be stored. Fills it with the predefinite Session list of db_default_lists.py
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			If (for some previous errros) the table Matches already exists, is cancelled and created again.
			In order to customize the ID AUTOINCREMENT integer value, the table must contain at least one row.
			Thus the entire lists of Matches is copied (into all_matches), for inserting only the first element of the list initially.
				=> self.curs.execute()
			At thiss point the SQLITE_SEQUENCE (sqlite hidden table) is modified, for placing the desired value of ID for the Matches (from 3000).			
			Then all others Matches of the pre-filled list are inserted into the table. 
				=> self.curs.executemany()
			Allocates all modifications into db.
				=> commit()
	"""
def build_Sessions_doc():
	""" Info
			Creates the table where Sessions will be stored. Fills it with the predefinite Session list of db_default_lists.py
				METTO IN DOCUMENTAZIONE OIMI DATABASE... E SCRIVO QUI DI RIFERIRSI A QUELLA NEL CASO...metto qualcosa anche in questo file ma nel doc di ...in "addition of a new table"
				> mandatory_imposition or mandatory_need = [0,1] represent the bint value that indicates to build the session by looking at the needs and/or
				the impositions of the kid...previously inserted. It differs from the demands (impositions_lists)
				> demand lists indicate the constraints imposed by the user at the creation, considered when BY THE ALG during the addition of a new table 
				(that basically correspond to Impositions and Needs) in the sessions_demands table. --> metto in user guide i possibili comandi (le stringhe giuste...da terminale)

		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			If (for some previous errros) the table Sessions already exists, is cancelled and created again.
			In order to customize the ID AUTOINCREMENT integer value, the table must contain at least one row,
			thus the entire lists of sessions is copied (into all_sessions), for inserting only the first element of the list initially. [self.curs.execute()]
			At this point the SQLITE_SEQUENCE (sqlite hidden table) is modified, for placing the desired value of ID for the Sessions (from 2000).			
			Then all others sessions of the pre-filled list are inserted into the table. 
				=> self.curs.executemany()
			Finally the current datetime is created automatically for each entry.
			Allocates all modifications into db.
				=> commit()
	"""

def build_ScenarioPatch_doc():	
	""" Info
			Creates table with patches of all Scenarios.
		Details
			Loads last versions of oimi_queries.py and db_default_lists.
				=> reload(oq)
				=> reload(dbdl)		
			Removes old version of the table. 
			Creates new version of the table. 
			Inserts all scenarios and patches from dbdl.				
			Allocates modifications into db.
				=> commit()
	"""
def build_KidSession_doc():
	""" Info
			Creates the table with all sessions played by each kids.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table. 
			Creates new version of the table. 
			Inserts all kids and their session.				
			Allocates modifications into db.
				=> commit()
	"""
def build_SessionMatch_doc():
	""" Info
			Creates the table with all matches for each session.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table. 
			Creates new version of the table.
			Inserts all session and their match. 
			Allocates modifications into db.
				=> commit()
		Notes 
			Int primary keys for speed up computations.
	"""
def build_MatchGame_doc():
	""" Info
			Creates the table with all matches and their games.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table. 
			Creates new version of the table.
			Inserts all matches and their games. 
			Allocates modifications into db.
		Notes 
			Int primary keys for speed up computations.
	"""
def build_GameQuestion_doc():
	""" Info
			Creates the table with all questions contained in a game.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table. 
			Creates new version of the table.
			Inserts all questions for each game. 
			Allocates modifications into db.
		Notes 
			Int primary keys for speed up computations.
	"""
def build_QuestionPatch_doc():
	""" Info
			Creates the table with all patches needed for playing a game.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table. 
			Creates new version of the table.
			Inserts all patches involved in a question. 
			Allocates modifications into db.
		Notes 
			Int primary keys for speed up computations.
	"""

def build_KidsImpositions_doc():
	""" Info
			Creates the table with all kids impositions
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table. 
			Creates new version of the table.
			Inserts all impositions. 
			Allocates modifications into db.
		Notes 
			Text primary keys.
	"""
def build_KidsAchievements_doc():
	""" Info
			Creates the table with all kids achievements
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table. 
			Creates new version of the table.
			Inserts all achievements. 
			Allocates modifications into db.
		Notes 
			Text primary keys.
	"""
def build_KidsSymptoms_doc():
	""" Info
			Creates the table with all kids symptoms
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table. 
			Creates new version of the table.
			Inserts all symptoms. 
			Allocates modifications into db.
		Notes 
			Text primary keys.
	"""	
def build_KidsNeeds_doc():
	""" Info
			Creates the table with all kids needs
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table. 
			Creates new version of the table.
			Inserts all needs. 
			Allocates modifications into db.
		Notes 
			Text primary keys.
	"""
def build_KindKind_doc():
	""" Info
			Creates the table with all Typologies and their subset of kind
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)
			Removes old version of the table. 
			Creates new version of the table.
			Inserts all needs. 
			Allocates modifications into db.
		Notes 
			Text primary keys.
	"""
def build_ScoresQuestion_doc():
	""" Info
			Creates the table of the sum of all errors (mistakes, indecisions, time issues) ever made from a specific kid answering to a question.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Cancels table if already exists.
			Builds new table from scratch.
	"""
def build_ScoresKindOfGame_doc():
	""" Info
			Creates the table of the sum all errors (mistakes, indecisions, time issues) ever made from a specific kid in a game.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Cancels table if already exists.
			Builds new table from scratch.
	"""
def build_ScoresKindOfGameNew_doc():
	""" Info
			Fix the Kind of Game table of all errors, with the aim of conserve all foreigns key constraints,
			with sqlite the only way is delete and rebuild again.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Cancels table if already exists.
			Copies the values of ScoresKindOfGame table.
			Deletes old ScoresKindOfGame table,
			Rename the new table with correct name.
			Allocates all modifications into db.
				=> commit()				   
	"""
def fix_ScoresKindOfGame_new_table_doc():
	""" Info
			Updates again total errors for each game after a deletion on errors question.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Removes old version of the tables.
			Creates the table. 
			Inserts all games errors of old table.
				=> self.curs.execute()
			Rename the table.
			Allocates all modifications into db.
				=> commit()		
	"""		
def build_ScoresGame_doc():
	""" Info
			Creates the table of total game errors (mistakes, indecisions, time issues) made from a specific kid during the current session.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Cancels table if already exists.
			Builds new table from scratch.
	"""
def build_ScoresMatch_doc():
	""" Info
			Creates the table of total match errors (mistakes, indecisions, time issues) made from a specific kid during the current session.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Cancels table if already exists.
			Builds new table from scratch.
	"""
def build_ScoresSession_doc():
	""" Info
			Creates the table of total session errors (mistakes, indecisions, time issues) made from a specific kid during the current session.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Cancels table if already exists.
			Builds new table from scratch.		  
	"""

def build_KidsPlayedRecap_doc():
	""" Info
			Creates the table of the summary of all playing activity for all kids.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Cancels table if already exists.
			Builds recap table from query in oq. 
	"""
def build_KidsFullPlayedRecap():
	""" Info
			Creates the table of the history all playing activity for all kids, including results, errors
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Cancels table if already exists.
			Builds recap table from query in oq.
			Add errors, indecisions and time.	  
	"""
##### PLACEMENT AT THE START
def place_patches_doc():
	""" Info
			Creates the table of the history all playing activity for all kids, including results, errors
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)		 
			Cancels table if already exists.
			Builds recap table from query in oq.
			Add errors, indecisions and time.	  
	"""	
def insertion_errors_indecision_session_doc():
	""" Info
			Inserts errors, indecisions, time for all questions given the corresponding session,match and game.
		Details
			Update Kids_full_played_recap setting errors, indecision, taken time columns
		Allocates modifications into db.
				=> commit()
		Notes
			Called realtime during a play session, after one game is completed 
	"""	
def patches_insertion_doc():
	""" Info
			Populates Patches table with all existing Patches from db_default_lists.py.
		Details
			Loads last versions of oimi_queries.py.
				=> reload(oq)	  
			If (for some previous errros) the table Patches already exists, is cancelled and created again.
			If the table of Patches already exists (maybe for some previous errros) is cancelled and created again.
			In order to customize the ID AUTOINCREMENT integer value, the first kid row is added separatedly from others, the table must contain at least one row. 
				So the entire lists of Patches is copied (into all_patches), for inserting only the first element of the list initially.
				=> self.curs.execute()
			At this point the SQLITE_SEQUENCE (sqlite hidden table) is modified, for placing the desired value of ID for the Patches (from 6000).			
			Then also all others Patches of the pre-filled list are inserted into the table. 
				=> self.curs.executemany()
			Allocates all modifications into db.
				=> commit()		   
		Notes 
			One single commit is enough 
	"""

def place_patches_scenario_doc():
	""" Info
			Insert all existing scenario from list to Scenarios tale.
		Details
			In order to customize the ID AUTOINCREMENT integer value, the first kid row is added separatedly from others, the table must contain at least one row. 
			So the entire lists of Scenarios is copied (into all_scenarios), for inserting only the first element of the list initially.
				=> self.curs.execute()
			At this point the SQLITE_SEQUENCE (sqlite hidden table) is modified, for placing the desired value of ID for the Scenario 
			(starts from 8000 instead of 1).
			Then also all others Scenarios of the pre-filled list are inserted into the table. 
				=> self.curs.execute()
			Allocates all modifications into db.
				=> commit()				 			
		Notes
			The comma is fundamental
				=>(item,)
		Two commits are necessary				
	"""

def place_new_patch_scenario_doc():
	""" Info
			Insert 
		Details
			Saves name in a list to pass as parameter in the selection query, for placeholder (?) subtitution.
				=> selections = [scenario]
				=> place_holders = ",".join("?"*len(selections))
			Reads and constructs the true selection query from db_extern_methods, removing spurious parts,
				for selecting the row for checking its existance
				=> gather_line
			Replaces special characters and place_holders 
				=> query_1 = qq.replace(".format(place_holders)", "")
				=> query_1 = query_1.replace("\'\'\'", "")
			Checks if scenario exists in database
				=> if not return_que:
			Adds all patches to table scenarios_patch				
			Allocates all modifications into db.
				=> commit()				 			
	"""

def include_new_Scenario_doc():
	""" Info
			Add a new scenario into Scenarios table.
		Details
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (encoding is necessary).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If the data is unknow:
				Modifies the lists of all Scenario for future use with new added
					=> modify_Scenario_lists(new_scenario_name)
			Allocates all modifications into db.
				=> commit()
			Scenarios has an AUTOINCREMENT ID, so is necessary to control if there is a pending position for setting the new id, from another previous one delete,
			consulting the deletion list and updating the new ID with the proper value, belonging before to a deleted need	
			Finds the new element to modify through the SQLITE_SEQUENCE table.
			Resets to zero the 'Scenarios' position in the deletion list.	
		Notes 
			One single commit is enough 
	"""
##### MODIFICATION OF DBDL
def modify_Scenario_lists_doc():
	""" Info
			Modifies rows of default_lists file, preparing data of Scenarios for future use with the new added.
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			Searches in readlines for proper position of right query, where file must be written. 
				=> add_string
				=> pos
				Finds the index_for_lastest counting number of elements in the table
				The right position in this case is under place line 
					=> pos[j] = pos[j] + 1 
			Changes query adding to lines new restriction 
				=> lines.insert(pos[-j], add_string)
			Overwrites all lines into file 
				=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			new_scenario_name
		Notes
			Called by include_new_Scenario_table()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
	"""	
def modify_Audios_list_doc():
	""" Info
			Modifies rows of default_lists file, preparing data of all Audios for future use.
		Details
			Defines the two new lines to add into dbdl.
				=> line_to_include_1 = convert_to_binary line
				=> line_to_include_2 = audio id line 
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			#Searches in readlines for proper position of right query, where file must be written, 
				=> place.
				In this case 2 lines above "last_words = ["
					=> place[j] = place[j] -1
			Finds the index_for_lastest counting number of elements in the table.
				Selects count query 
				=> fetchone()
			Changes query adding to lines new restriction.
				=> lines.insert(place[-j], line_to_include_1)
			Overwrites all lines into file. 
				=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
					=> 'w' option will overwrite any existing content
					=> -2 buffering
						Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
						Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
						getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
						A negative buffering means to use the system default, which is usually line buffered.
					=> fp.write(''.join(lines)
			Repeats same process for line_to_include_2 from step #.
				The right position in this case is under place line 
					=> place[j] = place[j] + 1 + index_for_lastest
		Parameters
			audio_id
			str audio_path
		Notes
			Called by insert_new_into_Audios_table()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
			No mix of {1} or {} in formatting, python cannot switch from automatic field numbering to manual field specification
	"""		
def modify_Needs_list_doc():
	""" Info
			Modifies rows of default_lists file, preparing data of all Needs for future use with the new added.
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			Searches in readlines for proper position of right query, where file must be written 
				=> pos
				The right position is under "all_needs = [\n"
					=> pos[j] = pos[j] + 1
			Defines depending on the scope (match, game or question).
				=> add_str 
			Changes query adding to lines new restriction.. 
				=> lines.insert(pos[-j], add_str)
			Overwrites all lines into file. 
				=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
					=> 'w' option will overwrite any existing content
					=> -2 buffering
						Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
						Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
						getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
						A negative buffering means to use the system default, which is usually line buffered.
					=> fp.write(''.join(lines)		
		Parameters
			str focus_contraint
			str need_constraint
			str scope
		Notes
			Called by insert_into_new_Needs_table()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
			New need is added on top of the list
	"""	
def modify_Patches_list_doc():
	""" Info
			Modifies rows of default_lists file, add new patch data of all Patches for successively operation in oimi_database.
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			Searches in readlines for proper position of right query, where file must be written.
				=> pos
				The right position is after "all_patches = [\n"
					=> pos[j] = pos[j] + 1
			Defines depending on the scope (match, game or question).
				=> add_str 
			Changes query adding to lines new restriction.. 
				=> lines.insert(pos[-j], add_str)
			Overwrites all lines into file. 
				=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
					=> 'w' option will overwrite any existing content
					=> -2 buffering
						Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
						Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
						getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
						A negative buffering means to use the system default, which is usually line buffered.
					=> fp.write(''.join(lines)		
		Parameters
			str new_subject
			str new_color
			str new_shape
		Notes
			Called by insert_into_new_Patch_table()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
			New patch is added on top of the list
	"""	
def modify_Kids_list_doc():
	""" Info
			Modifies rows of default_lists file, add new patch data of Kids for successively operation in oimi_database
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			#Searches in readlines for proper position of right query, where file must be written, three different positions
				=> put_str_1
				=> put_str_2
				=> put_str_3
				=> locs
				=> locs_1
				Finds the index_for_lastest counting number of elements in the table.
				The right position in this case is under locs line 
					=> locs[j] = locs[j] + 2 
			Changes query adding to lines new restriction 
				=> lines.insert(locs[-j], search_str)
			Overwrites all lines into file 
			=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
			Repeats the routine for other two adjustments from #
				=> locs[j] = locs[j] + 4 for taking into account that the file at this point is two lines longer
		Parameters
			str name
			str surname 
			int age
			str level 
			str image_path
			str details_path
		Notes
			Called by add_new_Kid_table()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
	"""	 

def modify_Sessions_list_doc():
	""" Info
			Modifies rows of default_lists file, preparing data of Sessions for future use with the new added.
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			Searches in readlines for proper position of right query, where file must be written. 
				=> add_this 
				=> place
				Finds the index_for_lastest counting number of elements in the table
				The right position in this case is under place line 
					=> place[j] = place[j] + 1 
			Changes query adding to lines new restriction 
				=> lines.insert(place[-j], add_this)
			Overwrites all lines into file 
				=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			int num_of_matches
			str purpose
			str difficulty
			int disposable
		Notes
			Called by add_new_Session_table()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
	"""	
def modify_Matches_list_doc():
	""" Info
			Modifies rows of default_lists file, preparing data of all Matches for future use with the new added.
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			Searches in readlines for proper position of right query, where file must be written.
				=> add_this 
				=> place
				Finds the index_for_lastest counting number of elements in the table
				The right position in this case is immediately under place line 
					=> place[j] = place[j] + 1 
			Changes query adding to lines new restriction 
				=> lines.insert(place[-j], add_this)
			Overwrites all lines into file 
				=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			int num_of_games
			str nickname
			str difficulty
			int disposable
		Notes
			Called by add_new_Match_table()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
	"""		
def modify_Games_list_doc():
	""" Info
			Modifies rows of default_lists file, preparing data of all Games for future use with the new added.
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			Searches in readlines for proper position of right query, where file must be written.
				=> add_this 
				=> place
				Finds the index_for_lastest counting number of elements in the table
				The right position in this case is immediately under place line 
					=> place[j] = place[j] + 1 
			Changes query adding to lines new restriction 
				=> lines.insert(place[-j], add_this)
			Overwrites all lines into file 
				=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			str nickname
			str type_of_game
			int num_of_questions
			str difficulty
			int disposable
		Notes
			Called by add_new_Match_table()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
	"""	
def modify_Questions_list_doc():
	""" Info
			Modifies rows of default_lists file, preparing data of all Questions for future use with the new added.
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			Searches in readlines for proper position of right query, where file must be written. 
				=> add_this 
				=> place
				Finds the index_for_lastest counting number of elements in the table.
			The right position in this case is under place line 
				If parameter position != 0 means that I want to change some values of an old line, is the rowid of Questions table
					Subtraction of 5000 is necessary
						=> place[j] = place[j] + position - 5000 +1
					=> place[j] = place[j] + 1 
			Changes query adding to lines new restriction 
				=> lines.insert(place[-j], add_this)
			Overwrites all lines into file 
			=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			int audio_id
			str type_of_game
			str difficulty
			str description
			int position
		Notes
			Called by add_new_Questions_table()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
	"""
	
def modify_default_list_bridges_2_doc():
	""" Info
			Modifies the lists of for future use of Kids_Sessions, Sessions_Matches, Matches_Games, Games_Questions, Questions_Patches, Kids_Needs.
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			Searches in readlines for proper position of right query, where file must be written.
				=> insert_str
				=> found 
				Finds the index_for_lastest counting number of elements in the table
				The right position in this case is immediately under found line 
					=> found[j] = found[j] + 1 
			Changes query adding to lines new restriction 
				=> lines.insert(found[-j], insert_str)
			Overwrites all lines into file 
				=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			int first_id
			int second_id
			str table
		Notes
			Called by add_new_Kids_Sessions_table(), add_new_Sessions_Matches_table(), add_new_Matches_Games(), add_new_Games_Questions(), add_new_Questions_Patches(), add_new_Kids_Needs()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
	"""		
def modify_default_list_bridges_doc():
	""" Info
			Modifies the lists of for future use of Kids_Impositions, Kids_Achievements, Kids_Symptoms.
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			Searches in readlines for proper position of right query, where file must be written.
				=> insert_str
				=> found 
				Finds the index_for_lastest counting number of elements in the table
				The right position in this case is immediately under found line 
					=> found[j] = found[j] + 1 
			Changes query adding to lines new restriction 
				=> lines.insert(found[-j], insert_str)
			Overwrites all lines into file 
				=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			int first_id
			int second_id
			str table
		Notes
			Called by add_new_KidImposition(), add_new_KidAchievement(), add_new_KidSymptom_table()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
	"""	  
def modify_default_list_doc():
	""" Info
			Modifies the lists of for future use of Impositions, Achievements, Symptoms, Available_games
		Details
			Opens db_default_lists file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/db_default_lists.py',"r") 
			Searches in readlines for proper position of right query, where file must be written.
				=> insert_str 
				=> found
				Finds the index_for_lastest counting number of elements in the table
				The right position in this case is immediately under found line 
					=> found[j] = found[j] + 1 
			Changes query adding to lines new restriction 
				=> lines.insert(found[-j], insert_str)
			Overwrites all lines into file 
				=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			str constraint_str
			str table
		Notes
			Called by insert_new_into_Impositions(), insert_new_into_Achievements(), insert_new_into_Symptoms(), insert_new_into_Available_games()
			All spaces end of line \_' matters during search
			Using the 'with' keyword, file will be closed automatically
	"""	 
##### UPDATES
def update_Audios_constraints_doc():
	""" Info
			Writes into oimi_queries.py the new query for Audios, with the check of the new constraints after an insertion of a new row in Audios.
		Details
			Opens oq file, reads and stores everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") 
			Searches in readlines for proper position of right query, where file must be written. 
				=> reached
				The right position in this case is under locs line 
					=> locs[j] = locs[j] + 1
			Changes query adding to lines new restriction 
				=> lines.insert(locs[-j], update_with)
			Overwrites all lines into file 
			=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			int constraint_audio
		Notes
			Returns lines list before modification, in order to returns to initial status, if something will going wrong later.
			Called by insert_new_into_Audios()
			Using the 'with' keyword, file will be closed automatically
			Is better to use a random order for check lines in the audio query. 
				Adding +5 to count and removing comma, for avoiding "the comma issue" of last word has to be fixed, complicating thing.
	"""
def update_Needs_constraints_doc():
	""" Info
			Writes into oimi_queries.py the new query for Needs table, with the check of the new constraints after an insertion of a new row in Needs.
			Adds the focus and the need if are both new, or add just the new need if foucs already exists.
		Details
			Opens oq file, read and store everything in readlines.
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") 
			Searches in readlines for two positions, where new constraints must be added to file 
				update_with = "\t\t'{0}',\n".format(focus_contr)
				update_with_1 = "\t\t'{}',\n".format(need_contr)
				=> reached_focus.
				=> reached
			The right position in this case is under reached_focus line 
				=> reached_focus[j] = reached_focus[j] + 1 + index_for_lastest
			Changes query adding to lines new restriction 
				=> lines.insert(reached_focus[-j], update_with)
			Overwrites all lines into file.
			Search also in database the correct scope of second constraint.
			Repeat the same process for second constraint. 
			=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
				Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
				Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
				getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
				A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			str focus_contr = focus 
			str need_contr = need
			str position = scope 
		Notes
			Called by insert_new_need() , where there's the control that the couple focus+need doesn't exists 
			Using the 'with' keyword, file will be closed automatically
	"""	
def update_Patches_genre_doc():	
	''' Info
			Writes into oimi_queries.py the genre for new lines in Patches table.
		Details
			Removes old line that starts with "WHERE element.." in	oq, inside trigger genrepatch using bash sed (inplace deletion).
				=> txt_to_remove_1 = "sed -i '/{}*/d' oimi_queries.py".format(to_canc)
			Converts a List of Tuples to a string to build update_with_2
				=> elems = '\', \''.join([str(x) for t in elements_now for x in t])
			Loads last versions of oimi_queries.py that changed.
				=> reload(oq)
			Opens oq file, read and store everything in readlines.
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") 
			Searches in readlines for two positions, where new constraints must be added to file.
				=> update_with_1 
				=> update_with_2 
				=> reached
			The right position in this case is under reached_focus line 
				=> reached_focus[j] = reached_focus[j] + 1
			Changes query adding to lines new restriction 
				=> lines.insert(reached_focus[-j], update_with)
			Overwrites all lines into file.
			Search also in database the correct scope of second constraint.
			Repeat the same process for second constraint. 
			=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
				Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
				Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
				getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
				A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			str new_subject
			str new_genre
		Notes
			Returns lines list before modification, in order to returns to initial status, if something will going wrong later.
			Called by insert_new_Patches_table()
			Using the 'with' keyword, file will be closed automatically
	'''
def update_Patches_constraints_doc():
	''' Info
			Writes into oimi_queries.py the new query for Patches, with the check of the new constraints after an insertion of a new row in Patches.
		Details
			Removes in trigger genrepatch
				=> txt_to_remove_1 = "grep -v \"WHERE element IN (\'Nothing\',\" oimi_queries.py > tmpfile_6 && mv tmpfile_6 oimi_queries.py"
			#Loads last versions of oimi_queries.py that changed.
				=> reload(oq)
			Opens oq file, reads and stores everything in readlines.
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") 
			Finds out subject was already present into database (trough query)
			Searches in readlines for three positions, where new constraints must be added to file 
				=> update_with_1 = for element
				=> update_with_2 = for color 
				=> update_with_3 = for shape
				=> reached_1
				=> reached_2
				=> reached_3
			The right position in this case is below reached line 
				=> reached[j] = reached[j] + 1 
			Changes query adding to lines new restriction
				=> lines.insert(reached[-j], update_with)
			Overwrites all lines into file.
			Search also in database the correct scope of second constraint.
			
			=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
				Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
				Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
				getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
				A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
			Repeats same process for other two params from step # , reaload oq everytime.
		Parameters
			str new_subject
			str new_color
			str new_shape 
			str new_genre 
		Notes
			Returns
				if the genre must be added or not 
				backup of oimi_queries before changes for turn back to previous file, if some error happens
				lines list before modification, in order to returns to initial status, if something will going wrong later.
			Using the 'with' keyword, file will be closed automatically
			Called by insert_new_Patches_table()
	'''
def update_constraint_of_doc():
	""" Info
			Writes into oimi_queries.py the new query, with the check of the new constraints after an insertion of a new row in:
			(Achievements, Impositions, Symptoms, Available_games)
		Details
			Opens oq file, read and store everything in readlines. 
				=>	with open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py',"r") 
			Searches in readlines for proper position of right query, where file must be written => reached.
				The right position in this case is under reached line 
					=> reached[j] = reached[j] + 1
			Changes query adding to lines new restriction 
				=> lines.insert(reached[-j], update_with)
			Overwrites all lines into file 
			=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
		Parameters
			str constraint_str
			str table		
		Notes
			Returns lines list before modification, in order to returns to initial status, if something will going wrong later.
			Using the 'with' keyword, file will be closed automatically
			Called by
				insert_new_Impositions()
				insert_new_Symptoms()
				insert_new_Achievements()
				insert_new_Available_games()
	"""
##### INSERTIONS
def insert_new_Audio_doc():
	""" Info
			Inserts a new (wav format) audio file in Audios table, with its univoque index that corresponds to its name 
		Details
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (encoding is necessary).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If the data is unknow:
				Modifies the query for future use, add constrain 
					=> update_Audios_constraints(constraint_audio)
				Modifies the lists of all audios for future use 
					=> modify_Audios_list(constraint_audio, audio_path)
				Selects the Question linked to the audio, in case the new audio is a substitution of an old one
					(before dropping all audios for building again the table).
				Removes old version of the table if exists.
				Creates the new table.
				Loads last versions of oimi_queries.py and db_default_lists.
					Having last version of imported module is fundamental to insert also last value added.
					=> reload(oq)	
					=> reload(dbdl)
				Inserts all audios created so far.
				Allocates all modifications into db => [commit()].

				if is_a_substitution true
					Finds the correct position with rowid 
						=> row_n
					Update old audios with a new one 
						=> position != 0	
						=> modify_Questions_list(constraint_audio, kind, diff, desc, row_n) for removing 
						
					Redo all the tables in which questions are involved 
						+Questions_table()
						+ScoresQuestion()
						+ScoresKindOfGame_table()		 
						+ScoresKindOfGame_new_table()
						+QuestionPatch()
			If exception is raised, undo the modification in oq file.
				=> fp.write(''.join(old_lines_queries))
		Parameters
			-int constraint_audio = new audio to add
			-str audio_path = link real path of the wav file 
			-int is_a_substitution = states the new data will stole the id of the last old audio removed immediately before insertion.
				Otherwise a new line is added. So is always set to 0 when is called outside cancel_from_audios()
		Notes
			Insert new values updating both constraint and lists
			No need to check if there is a pending position in deletion_lists for setting the new id like another previus one deleted, 
				the primary key ID is the number of the audios
			Essential initialization for dontContinue = 0
	"""		
def insert_new_Achievement_doc():
	""" Info
			Inserts a new achievement in the Achievements table
		Details
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (encoding is necessary).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If the data is unknow:
				Modifies the query for future use, add constrain 
					=> update_constraint_of(constraint_str, 'Achievements')
				Reads the query from oimi_queries.py storing the proper portion of text, from begin to end.
					=> lines = fp.readlines() 
					=> chunk = lines[begin:end]
				Cleans insertion query for reading 
					Extracts string from list
						=> chunk.pop(0) 
					Remove all \n newlines
						=> s.strip()
					Binds all lines in one
						=> join()
					Removes any ''' chars by regexp
						=> re.sub()
				Copy all Achievements' rows. 
					=> SELECT sql FROM sqlite_master
				Create Achi_tmp, an empty table equal to Achievements and copy also Achievemnts existing values. 
				Drops old Achievements.
				Changes the name of the table into Achievements.
				Finally stores the new achievement.
				Modifies the lists of all achievements for future use 
					=> modify_default_list(constraint_str, 'Achievements')
			If exception is raised, undo the modification in oq file.
				=> fp.write(''.join(old_lines_queries))
		Parameters
			-str constraint_str = the new achievement
		Notes
			Insert new values updating both constraint and lists
			Using the 'with' keyword, file will be closed automatically
			No need to check if there is a pending position in deletion_lists for setting the new id like another previus one deleted, 
			the primary key ID is a TEXT
	"""
def insert_new_Need_doc():	
	""" Info
			Insert a new need in Needs table.
		Details
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Search for three different lines (use Invalid)
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system.
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If the data is unknow:
				Modifies the query for future use, add the constrain => update_Needs_constraints(constraint_str_1, constraint_str_2, scope).
				Reads the query from oimi_queries.py storing the proper portion of text, from begin to end.
					=> lines = fp.readlines() 
					=> chunk = lines[begin:end]
				Cleans insertion query for reading.
					Extracts string from list
						=> chunk.pop(0) 
					Remove all \n newlines
						=> s.strip()
					Binds all lines in one
						=> join()
					Removes any ''' chars by regexp
						=> re.sub()
				Copy all Needs' rows => SELECT sql FROM sqlite_master.
				Create Achi_tmp, an empty table equal to Needs and copy also Needs existing values.
				Drops old Needs.
				Changes the name of the table into Needs.
				Finally stores the new need according to the scope.
				Modifies the lists of all Needs for future use 
					=> modify_default_list(constraint_str, 'Needs')

				Looses trigger that now are enabled for fill the column with the 'Invalid' key => enable_needs_trigger_*
				Looks at newest changes in oimi_queries.py and in db_default_lists
					=> reload(oq)
					=> reload(dbdl)
				Modifies the lists of all needs for future use
					=> modify_Needs_list(constraint_need, need_path)

				Needs has an AUTOINCREMENT ID, so is necessary to control if there is a pending position for setting the new id, from another previous one delete,
				consulting the deletion list and updating the new ID with the proper value, belonging before to a deleted need	
				Finds the new element to modify through the SQLITE_SEQUENCE table.
				Resets to zero the 'Needs' position in the deletion list 
			If exception is raised, undo the modification in oq file.
				=> fp.write(''.join(old_lines_queries))
		Parameters
			str constraint_str_1 = focus
			str constraint_str_2 = achievement name 
			str scope = match, game or question
		Notes
			Insert new values updating both constraint and lists
			Using the 'with' keyword, file will be closed automatically
			Multiple commits are essentials
	"""	
def insert_new_Symptom_doc():	
	""" Info
			Insert a new symptom in Symptoms table.
		Details
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Search for three different lines (use Invalid)
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (encoding is necessary).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If the data is unknow:
				Modifies the query for future use, add the constrain => update_Needs_constraints(constraint_str)
				Reads the query from oimi_queries.py storing the proper portion of text, from begin to end
					=> lines = fp.readlines() 
					=> chunk = lines[begin:end]
				Cleans insertion query for reading.
					Extracts string from list
						=> chunk.pop(0) 
					Remove all \n newlines
						=> s.strip()
					Binds all lines in one
						=> join()
					Removes any ''' chars by regexp
						=> re.sub()					
				Drops Symptoms table.
				Creates again the Symptoms table.
				Inserts all values created so far. 
				Finally adds the new symptom.
				Modifies the lists of all needs for future use 
					=> modify_default_list(constraint_str, 'Symptoms')				
			If exception is raised, undo the modification in oq file.
				=> fp.write(''.join(old_lines_queries))
		Parameters
			str constraint_str = symptom name
		Notes
			Insert new values updating both constraint and lists
			Using the 'with' keyword, file will be closed automatically
			Multiple commits are essentials
			No need to check if there is a pending position in deletion_lists for setting the new id like another previus one deleted, 
			the primary key ID is a TEXT
	"""	
def insert_new_Impositions_doc():
	""" Info
			Insert a new imposition in Impositions table.
		Details
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Search for three different lines (use Invalid)
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (encoding is necessary).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If the data is unknow:
				Modifies the query for future use, add the constrain => update_constraint_of(constraint_str, 'Impositions')
				Reads the query from oimi_queries.py storing the proper portion of text, from begin to end
					=> lines = fp.readlines() 
					=> chunk = lines[begin:end]
				Cleans insertion query for reading.
					Extracts string from list
						=> chunk.pop(0) 
					Remove all \n newlines
						=> s.strip()
					Binds all lines in one
						=> join()
					Removes any ''' chars by regexp
						=> re.sub()							
				Drops Impositions table.
				Creates again the Impositions table.
				Inserts all values created so far.
				Finally adds the new imposition.
				Modifies the lists of all needs for future use 
					=> modify_default_list(constraint_str, 'Impositions')		
			If exception is raised, undo the modification in oq file.
				=> fp.write(''.join(old_lines_queries))
		Parameters
			str constraint_str = imposition name
		Notes
			Insert new values updating both constraint and lists
			Using the 'with' keyword, file will be closed automatically
			Multiple commits are essentials
			No need to check if there is a pending position in deletion_lists for setting the new id like another previus one deleted, 
			the primary key ID is a TEXT
	"""	
def insert_new_Available_games_doc():
	""" Info
			Insert a new available_game in Available_games table
		Details
			No deletion for kind_of_games
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (encoding is necessary).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If the data is unknow:
				Modifies the query for future use, add the constrain 
					=>	update_constraint_of(constraint_str, 'Available_games')
				Reads the query from oimi_queries.py storing the proper portion of text, from begin to end
				=> lines = fp.readlines() 
				=> chunk = lines[begin:end]
				Cleans insertion query for reading.
					Extracts string from list
						=> chunk.pop(0) 
					Remove all \n newlines
						=> s.strip()
					Binds all lines in one
						=> join()
					Removes any ''' chars by regexp
						=> re.sub()		
				Drops Available_games table.
				Creates again the Available_games table.
				Inserts all values created so far. 
				Finally adds the new available_game.
				Modifies the lists of all needs for future use 
					=> modify_default_list(constraint_str, 'Available_games')		
			If exception is raised, undo the modification in oq file.
				=> fp.write(''.join(old_lines_queries))
		Parameters
			str constraint_str = available_game name
		Notes
			Insert new values updating both constraint and lists
			Using the 'with' keyword, file will be closed automatically
			Multiple commits are essentials
			No need to check if there is a pending position in deletion_lists for setting the new id like another previus one deleted, 
			the primary key ID is a TEXT
	"""
def insert_new_Kind_games_doc():
	""" Info
			Insert a new Kind_game in Kind_games table
		Details
			No deletion for kind_of_game
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (encoding is necessary).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If the data is unknow:
				Modifies the query for future use, add the constrain 
					=>	update_constraint_of(constraint_str, 'Kind_games')
				Reads the query from oimi_queries.py storing the proper portion of text, from begin to end
				=> lines = fp.readlines() 
				=> chunk = lines[begin:end]
				Cleans insertion query for reading.
					Extracts string from list
						=> chunk.pop(0) 
					Remove all \n newlines
						=> s.strip()
					Binds all lines in one
						=> join()
					Removes any ''' chars by regexp
						=> re.sub()		
				Drops Kind_games table.
				Creates again the Kind_games table.
				Inserts all values created so far. 
				Finally adds the new available_game.
				Modifies the lists of all needs for future use 
					=> modify_default_list(constraint_str, 'Kind_games')		
			If exception is raised, undo the modification in oq file.
				=> fp.write(''.join(old_lines_queries))
		Parameters
			str constraint_str = available_game name
		Notes
			Insert new values updating both constraint and lists
			Using the 'with' keyword, file will be closed automatically
			Multiple commits are essentials
			No need to check if there is a pending position in deletion_lists for setting the new id like another previus one deleted, 
			the primary key ID is a TEXT
	"""
def insert_new_Patch_doc():
	''' Info
			Insert a new patch in Patches table
		Details
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Search for three different lines (use Invalid)
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (encoding is necessary).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Modifies the query for future use, add the constrain
					=> update_Patches_constraints(constraint_str_1, constraint_str_2, scope)
				Reads the query from oimi_queries.py storing the proper portion of text, from begin to end
					=> lines = fp.readlines() 
					=> chunk = lines[begin:end]
				Copy all Patches' rows 
					=> SELECT sql FROM sqlite_master
				Create Achi_tmp, an empty table equal to Patches and copy also Patches existing values 
				Drops old Patches
				Changes the name of the table into Patches
				Finally stores the new need according to the shape??? (try_catch)
				Modifies the lists of all Patches for future use 
					=> modify_default_list(constraint_str, 'Patches'
				Looses trigger that now are enabled for fill the column with the 'Invalid' key => enable_Patches_trigger_*
				Looks at newest changes in oimi_queries.py and db_default_lists.
					=> reload(oq)
					=> reload(dbdl)
				Modifies the lists of all Patches for future use => modify_Audios_list(constraint_audio, audio_path)
				Undo modifications in oimi_queries file if exception is raised
					=> fp.seek(last_pos, os.SEEK_SET)
					=> fp.truncate(last_pos)
			If exception is raised, undo the modification in oq file.
				=> fp.write(''.join(old_lines_queries))
		Parameters
			str new_subject
			str new_color
			str new_shape
			str new_genre
		Notes
			Insert new values updating both constraint and lists
			Using the 'with' keyword, file will be closed automatically
			Multiple commits are essentials
			Patches has an AUTOINCREMENT ID, but a Patch cannot be eliminated by construction, no deletion list
		'''
##### ADDITIONS
def add_new_Kid_doc():
	""" Info
			Inserts all info of a new kid in Kids table.
		Details
			Builds search_str from input parameters with formatting.s
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries.
					=> reload(oq)
				Stores the proper kid's image (from extern methods). 
					=> photo_to_insert = dbem.convert_to_binary(image_path)
				Stores the proper kid's file (from extern methods).
					=> doc_to_insert = dbem.convert_to_binary(details_path)
				Modifies the lists of all kids info for future use.
					=> modify_Kids_list(name, surname, age, level, image_path, details_path)
				Effectively add the new instance in the table trough insertion query.

				Kids has an AUTOINCREMENT ID, so is necessary to control if there is a pending position for setting the new id, from another previous one delete,
				consulting the deletion list and updating the new ID with the proper value, belonging before to a deleted need.
				Finds the new element to modify through the SQLITE_SEQUENCE table.
				Resets to zero the 'Kids' position in the deletion list (position zero).
				Allocates all modifications into db. 
					=> commit()				
		Parameters
			str name
			str surname
			int age
			str level
			str image_path
			str details_path
		Notes
			Add also details_ and photo_
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
			new_del boolean purpose is to ensure that index_to_replace and index_corrected to be reference only after their assignment
	""" 
def add_new_Session_doc():
	""" Info
			Inserts a new session in Sessions table.
		Details
			Builds search_str from input parameters with formatting.
			Check if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries file.
					=> reload(oq)
				Creates date and time for the new session.
				Modifies the lists of all Sessions for future use. 
					=> modify_Sessions_list(name, surname, age, level, image_path, details_path)
				Effectively add the new instance in the table trough insertion query.

				Sessions has an AUTOINCREMENT ID, so is necessary to control if there is a pending position for setting the new id, 
				from another previous one delete, consulting the deletion list and updating the new ID with the proper value, 
				belonging to a previously deleted need.
				Finds the new element to modify through the SQLITE_SEQUENCE table.
				Resets to zero the 'Sessions' position in the deletion list (position one).
				Allocates all modifications into db
					=> commit()				
		Parameters
			int num_of_matches
			str purpose 
			str difficulty 
			int disposable
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
			new_del boolean purpose is to ensure that index_to_replace and index_corrected to be reference only after their assignment
	"""
def add_new_Match_doc():
	""" Info
			Inserts a new match in Matches table.
		Details
			Builds search_str from input parameters with formatting.
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping loads the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries.
					=> reload(oq)
				Modifies the lists of all Matches for future use.
					=> modify_Matches_list(num_of_games, nickname, difficulty, disposable)
				Effectively add the new instance in the table trough insertion query.

				Matches has an AUTOINCREMENT ID, so is necessary to control if there is a pending position for setting the new id, from another previous one delete,
				consulting the deletion list and updating the new ID with the proper value, belonging before to a deleted need	
				Finds the new element to modify through the SQLITE_SEQUENCE table.
				Resets to zero the 'Matches' position in the deletion list (position two).
				Allocates all modifications into db 
					=> commit()
		Parameters
			int num_of_games
			str nickname
			str difficulty
			int disposable
		Notes
			Adds new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
			new_del boolean purpose is to ensure that index_to_replace and index_corrected to be reference only after their assignment
	"""
def add_new_Game_doc():
	""" Info
			Inserts a new game in Games table.
		Details		
			Builds search_str from input parameters with formatting.
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping loads the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)

			If the new data is unknow:
				Looks at the new version of oq file. 
					=> reload(oq)
				Modifies the lists of all Games for future use. 
					=> modify_Games_list(nickname, type_of_game, num_of_questions, difficulty, disposable)
				Effectively add the new instance in the table trough insertion query.

				Games has an AUTOINCREMENT ID, so is necessary to control if there is a pending position for setting the new id, from another previous one delete,
				consulting the deletion list and updating the new ID with the proper value, belonging before to a deleted need.
				Finds the new element to modify through the SQLITE_SEQUENCE table.
				Resets to zero the 'Games' position in the deletion list (position three).
				Allocates all modifications into db.
					=> commit()
		Parameters
			str nickname
			str type_of_game
			int num_of_questions
			str difficulty
			int disposable
		Notes
			Adds new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
			new_del boolean purpose is to ensure that index_to_replace and index_corrected to be reference only after their assignment
	"""
def add_new_Question_doc():
	""" Info
			Inserts a new question in Questions table.
		Details
			Builds search_str from input parameters with formatting.
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping loads the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries.
					=> reload(oq)
				Modifies the lists of all Questions for future use. 
					=> modify_Questions_list(audio_id, type_of_game, difficulty, description, 0) 
					#N.B 0 is the position 
				Effectively add the new instance in the table trough insertion query.
				
				Questions has an AUTOINCREMENT ID, so is necessary to control if there is a pending position for setting the new id, from another previous one delete,
				consulting the deletion list and updating the new ID with the proper value, belonging before to a deleted need.
				Finds the new element to modify through the SQLITE_SEQUENCE table.
				Resets to zero the 'Questions' position in the deletion list (position four).
				Allocates all modifications into db.
					=> commit()
		Parameters
			int audio_id
			str type_of_game
			str difficulty
			str description
		Notes
			Adds new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
			new_del boolean purpose is to ensure that index_to_replace and index_corrected to be reference only after their assignment
	"""
def add_new_KidSession_doc():
	""" Info
			Inserts a new session for one kid in Kids_Sessions table
		Details
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping loads the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If the new data is unknow:
				Looks at the new version of queries file 
					=> reload(oq)
				Modifies the lists of all Kids_Sessions info for future use 
					=> modify_default_list_bridges(kid_id, session_id, 'Kids_Sessions')
				Effectively add the new instance in the table trough insertion query.

				Updates OimiSettings fireTrigger flag value, avoiding starting the trigger and raise and exception 
					=> UPDATE OimiSettings SET val = 1
				Creates temp table Kids_Sessions_tmp with same characteristics of Kids_Sessions.
				Copies rows from Kids_Sessions and order values in ascending order.
				Cancels old Kids_Sessions
				Changes name to new table with Kids_Sessions
				Creates index for Kids_Sessions 
				=> CREATE INDEX kid_se_index
					Indexes are special lookup tables that the database search engine can use to speed up data retrieval,
					in this case the goal is to speed up table sorting (ASC indicates ascending order).
					Whenever you create an index, SQLite creates a B-tree structure to hold the index data.
				Reset OimiSettings fireTrigger flag value, enabling the trigger 
					=> UPDATE OimiSettings SET val = 1
				Allocates all modifications into db.
					=> commit()
		Parameters
			int kid_id
			int session_id
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion.
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
	"""
def add_new_SessionMatch_doc():
	""" Info
			Inserts a new match for one session in Sessions_Matches table
		Details
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping loads the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries file => reload(oq)
				Modifies the lists of all Sessions_Matches info for future use 
					=> modify_default_list_bridges(session_id, match_id, 'Sessions_Matches')
				Effectively add the new instance in the table trough insertion query.

				Creates temp table Sessions_Matches_tmp with same characteristics of Sessions_Matches.
				Copies rows from Sessions_Matches and order values in ascending order.
				Cancels old Sessions_Matches
				Changes name to new table with Sessions_Matches
				Creates index for Sessions_Matches
					=> CREATE INDEX session_index
					Indexes are special lookup tables that the database search engine can use to speed up data retrieval,
					in this case the goal is to speed up table sorting (ASC indicates ascending order).
					Whenever you create an index, SQLite creates a B-tree structure to hold the index data.
				Allocates all modifications into db.
					=> commit()
		Parameters
			int session_id
			int match_id
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion.
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
	"""
def add_new_MatchGame_doc():
	""" Info
			Inserts a new game for one match in Matches_Games table
		Details
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping loads the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries file 
					=> reload(oq)
				Modifies the lists of all Matches_Games info for future use 
					=> modify_default_list_bridges(match_id, game_id, 'Matches_Games')
				Effectively add the new instance in the table trough insertion query.

				Creates temp table Matches_Games_tmp with same characteristics of Matches_Games.
				Copies rows from Matches_Games and order values in ascending order.
				Cancels old Matches_Games
				Changes name to new table with Matches_Games
				Creates index for Matches_Games 
					=> CREATE INDEX match_index
					Indexes are special lookup tables that the database search engine can use to speed up data retrieval,
					in this case the goal is to speed up table sorting (ASC indicates ascending order).
					Whenever you create an index, SQLite creates a B-tree structure to hold the index data.
				Allocates all modifications into db.
					=> commit()
		Parameters
			int match_id
			int game_id
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion.
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
	"""
def add_new_GameQuestion_doc():
	""" Info
			Inserts a new question for one game in Games_Questions table
		Details
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries file 
					=> reload(oq)
				Modifies the lists of all Games_Questions info for future use => modify_default_list_bridges(game_id, question_id, 'Games_Questions')
				Effectively add the new instance in the table trough insertion query.

				Creates temp table Games_Questions_tmp with same characteristics of Games_Questions.
				Copies rows from Games_Questions and order values in ascending order.
				Cancels old Games_Questions
				Changes name to new table with Games_Questions
				Creates index for Games_Questions 
					=> CREATE INDEX game_index
					Indexes are special lookup tables that the database search engine can use to speed up data retrieval,
					in this case the goal is to speed up table sorting (ASC indicates ascending order).
					Whenever you create an index, SQLite creates a B-tree structure to hold the index data.
				Allocates all modifications into db.
					=> commit()
		Parameters
			int game_id
			int question_id
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion.
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
	"""
def add_new_QuestionPatch_doc():
	""" Info
			Inserts a new patch for one question in Questions_Patches table
		Details
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries file 
					=> reload(oq)
				Modifies the lists of all Questions_Patches info for future use => modify_default_list_bridges(question_id, patch_id, 'Questions_Patches')
				Effectively add the new instance in the table trough insertion query.

				Creates temp table Questions_Patches_tmp with same characteristics of Questions_Patches.
				Copies rows from Questions_Patches and order values in ascending order.
				Cancels old Questions_Patches
				Changes name to new table with Questions_Patches
				Creates index for Questions_Patches 
					=> CREATE INDEX question_pa_index
					Indexes are special lookup tables that the database search engine can use to speed up data retrieval,
					in this case the goal is to speed up table sorting (ASC indicates ascending order).
					Whenever you create an index, SQLite creates a B-tree structure to hold the index data.
				Allocates all modifications into db.
					=> commit()
		Parameters
			int question_id
			int patch_id
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion.
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
	"""
def add_new_KidNeed_doc():
	""" Info
			Inserts a new need for one kid in Kids_Needs table
		Details
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries file 
					=> reload(oq)
				Modifies the lists of all Kids_Needs info for future use => modify_default_list_bridges(kid_id, need_id, 'Kids_Needs')
				Effectively add the new instance in the table trough insertion query.

				Creates temp table Kids_Needs_tmp with same characteristics of Kids_Needs.
				Copies rows from Kids_Needs and order values in ascending order.
				Cancels old Kids_Needs
				Changes name to new table with Kids_Needs
				Creates index for Kids_Needs 
					=> CREATE INDEX kid_ne_index
					Indexes are special lookup tables that the database search engine can use to speed up data retrieval,
					in this case the goal is to speed up table sorting (ASC indicates ascending order).
					Whenever you create an index, SQLite creates a B-tree structure to hold the index data.
				Allocates all modifications into db.
					=> commit()
		Parameters
			int kid_id
			int need_id
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion.
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
	"""
def add_new_KidImposition_doc():
	""" Info
			Inserts a new imposition for one kid in Kids_Impositions table
		Details
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries file 
					=> reload(oq)
				Modifies the lists of all Kids_Impositions info for future use => modify_default_list_bridges_2(kid_id, imposition_name, 'Kids_Impositions')
				Effectively add the new instance in the table trough insertion query.

				Creates temp table Kids_Impositions_tmp with same characteristics of Kids_Impositions.
				Copies rows from Kids_Impositions and order values in ascending order.
				Cancels old Kids_Impositions
				Changes name to new table with Kids_Impositions
				Creates index for Kids_Impositions 
					=> CREATE INDEX kid_imp_index
					Indexes are special lookup tables that the database search engine can use to speed up data retrieval,
					in this case the goal is to speed up table sorting (ASC indicates ascending order).
					Whenever you create an index, SQLite creates a B-tree structure to hold the index data.
				Allocates all modifications into db.
					=> commit()
		Parameters
			int kid_id
			str imposition_name
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
	"""
def add_new_KidAchievement_doc():
	""" Info
			Inserts a new achievement for one kid in Kids_Achievements table
		Details
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries file 
					=> reload(oq)
				Modifies the lists of all Kids_Achievements info for future use => modify_default_list_bridges_2(kid_id, achievement_name, 'Kids_Achievements')
				Effectively add the new instance in the table trough insertion query.

				Creates temp table Kids_Achievements_tmp with same characteristics of Kids_Achievements.
				Copies rows from Kids_Achievements and order values in ascending order.
				Cancels old Kids_Achievements
				Changes name to new table with Kids_Achievements
				Creates index for Kids_Achievements 
					=> CREATE INDEX kid_ac_index
					Indexes are special lookup tables that the database search engine can use to speed up data retrieval,
					in this case the goal is to speed up table sorting (ASC indicates ascending order).
					Whenever you create an index, SQLite creates a B-tree structure to hold the index data.
				Allocates all modifications into db.
					=> commit()
		Parameters
			int kid_id
			str achievement_name
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
	"""
def add_new_KidSymptom_doc():
	""" Info
			Inserts a new symptom for one kid in Kids_Symptoms table
		Details
			Checks if the new element to insert already exists in database, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			
			If the new data is unknow:
				Looks at the new version of queries file 
					=> reload(oq)
				Modifies the lists of all Kids_Symptoms info for future use => modify_default_list_bridges_2(kid_id, symptom_name, 'Kids_Symptoms')
				Effectively add the new instance in the table trough insertion query.

				Creates temp table Kids_Symptoms_tmp with same characteristics of Kids_Symptoms.
				Copies rows from Kids_Symptoms and order values in ascending order.
				Cancels old Kids_Symptoms
				Changes name to new table with Kids_Symptoms
				Creates index for Kids_Symptoms 
					=> CREATE INDEX kid_sy_index
					Indexes are special lookup tables that the database search engine can use to speed up data retrieval,
					in this case the goal is to speed up table sorting (ASC indicates ascending order).
					Whenever you create an index, SQLite creates a B-tree structure to hold the index data.
				Allocates all modifications into db.
					=> commit()
		Parameters
			int kid_id
			str symptom_name
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
	"""
def add_new_ScoresQuestion_doc():
	""" Info
			Inserts a new error instance in ScoresQuestion table
		Details
			Checks if the new error is referred to a question already coupled with a kid, or need to be created, looking at db_default_lists.
			Searches for a specific string (complete line).
			Using Memory mapping can load the file directly into computer memory, improving search's performance.
			fileno() returns the integer file descriptor of a requested I/O operations from the operating system (need encoding).
				=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If the kid has answered to that question previously:
				=> flag_createOrmodify = 1 # modify
				Looks at the new version of queries file
					=> reload(oq)
				set flag to pass to modify_ScoresQuestion_list, to specify that an update will require a deletion first.
				Modifies the lists of all Kids_Symptoms info for future use 
					=> modify_ScoresQuestion_list(question_id, kid_id, tot_num_errors, tot_num_indecisions, flag_createOrmodify)
				Effectively refreshes the table trough update using sum query.
					=> UPDATE ScoresQuestion SET 
				Calls fix_ScoresKindOfGame_new_table() to update again number of total errors (of current kid) regarding the kind of game to which the question belongs 
				Allocates all modifications into db.
					=> commit()

			If the kid has never answered to that question:
				=> flag_createOrmodify = 1 # modify
				Looks at the new version of queries file
					=> reload(oq)
				Modifies the lists of all Kids_Symptoms info for future use 
					=>	modify_ScoresQuestion_list(question_id, kid_id, tot_num_errors, tot_num_indecisions, flag_createOrmodify)
				Effectively add the new instance in the table trough insertion query.
				Calls fix_ScoresKindOfGame_new_table() to update again number of total errors (of current kid) regarding the kind of game to which the question belongs 
				Allocates all modifications into db.
					=> commit()
		Parameters
			int question_id
			int kid_id
			int tot_num_errors
			int tot_num_indecisions
			int tot_time
		Notes
			Add new row, no changes in oimi query constraints, only in whole list for future uses in case of deletion
			Addition into table can be done immediately, before modification of db_default_lists, in case of errors nothing is written on dbdl file
	"""	
##### CHANGES
def alter_Kids_doc():
	''' Info
			Chages one of the key of Kids table.
		Details
			Prepares info for subtitution of text of the second selection query.
				=> which_id_1 = which_id_1.replace("\'", "")
			Selects kid's old info thanks to extern method query_string_4 replace special characters.
				=> data6 = data5.replace('£', which_surname)
			Stores selection's results
				=> return_que = self.curs.fetchall() 
			Extracts from db_default_lists the exact link of the photo and document,
				searching for the line that start with the name of the kids
			Transform the list of image into a string,
				for preparing the string that contain the link to be used as parameter for add function
				=>e2 = dbem.replace_all(d2, dic)
			Repeats same routine for the link of kids'details.
			Deletes old kid row (both from db and list).
			Inserts a new row with corrected new value.
		Parameters
			str name
			str surname
			str change_this ('name','surname','age','current_level','photo','details') 
			str new_value
		Notes 
			Pay attention when called immediately after another 
			Also age param is required in str format 
	'''	
def change_disposable_doc():
	''' Info
			Changes disposable value for a Session/Match/Games.
		Details
			Enables disposable triggers, for automatically set all elements of the same set with same value of disposable
			(eg. all games of a not_disposable session).
			Discovers the desired table to change from received id, extracting the first digit of the id.
				(Sessions,Matches,Games)
				=> total_digits = dbem.count_number(id_to_remove).
				=> digit_1 = dbem.digit_selector(id_to_remove, 1,total_digits).
			Controls if the disposable flag is already set with desired value.
				Quit, nothing to do 
				=> self.curs.execute("SELECT disposable..
				=> [str(id_row)]) is used for avoiding unsupported type error
			Otherwise change the value of disposable into the proper table.
				=> self.curs.execute("UPDATE..
			Allocates all modifications into db.
				=> commit()
			Selects with query Sessions keys + Matches keys + Games keys
				Later also matches and games linked to Sessions need to change in dbdl (all disposable 0 turns to 1 and viceversa)
				Stores also row number for leaving all changed line in same place
				=> rowid_num = output_q[0][0]
				Uses 
					total_put_match = []
					total_put_game = []
					total_remove_me_match = []
					total_remove_me_game = []
				for saving lines to substitute also for matches and game, even is the session that is changing
				(same holds for changing games linked to a match, when the match disposable is changing, but usually is the session that changes and thus match and game)
				Removes duplicates from list:
					=> correct_list_1=list(set(arr))
					=> correct_list_2=list(set(arr2))
			Removes also from db_default_lists trough bash grep command, the line with the question (linked to the audio input parameter) from Questions
				(creating a new copy of the file without lines matchings)
				=> txt_to_remove = "grep -v
			Searches in readlines for proper position of right query, where file must be written, according to which table need an update 
				=> search_str 
				=> locs
				=> gap = for put again line in right position in subset of (all_sessions -2000 /all_matches -3000  /all_games -4000 )
				Finds the index_for_lastest counting number of elements in the table
				The right position in this case is under locs line 
					=> locs[j] = locs[j] + 1 
			Changes query adding to lines new restriction 
				=> lines.insert(locs[-j], search_str)
			Overwrites all lines into file 
			=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)
							
		Parameters
			int id_row
			int new_val
		Notes
			Replacing using Sed is not working here, grep is instead used (removal) + inseriment 
			Works only when instance already exist in ScoresQuestion
	'''	
def change_difficulty_q_doc():
	''' Info
			Changes difficulty value for a Session/Match/Games.
		Details
			Controls if the difficulty is already set with desired value.
				Quit, nothing to do 
				=> self.curs.execute("SELECT difficulty..
				=> [str(id_row)]) is used for avoiding unsupported type error
			Otherwise change the value of difficulty.
				=> self.curs.execute("UPDATE..
			Allocates all modifications into db.
				=> commit()
			Removes also from db_default_lists trough bash grep command, the line with the question 
				(creating a new copy of the file without lines matchings)
				=> txt_to_remove = "grep -v
			Searches in readlines for proper position of right query, where file must be written, according to which table need an update 
				=> search_str 
				=> locs
				Finds the index_for_lastest counting number of elements in the table
				The right position in this case is under locs line 
					=> locs[j] = locs[j] + 1 
			Changes query adding to lines new restriction 
				=> lines.insert(locs[-j], search_str)
			Overwrites all lines into file 
			=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
				=> 'w' option will overwrite any existing content
				=> -2 buffering
					Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
					Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
					getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
					A negative buffering means to use the system default, which is usually line buffered.
				=> fp.write(''.join(lines)						
		Parameters
			int id_quest
			int new_diff
		Notes
			Replacing using Sed is not working here, grep is instead used (removal) + inseriment 
			Works only when instance already exist in ScoresQuestion
	'''	
##### DELETIONS
def delete_from_id_doc():
	''' Info
			Deletes row in specified table, given the ID, for those tables with AUTOINCREMENTED integer ID 
			(Kids, Sessions, Matches, Games, Questions, Patches, Needs).
			Is the only way to remove a single Sessions, Matches, Games, Questions, Patches directly.
		Details
			0 ind_del is the index for the deletions_lists of the DatabaseManager class object, (0 for ....)
			For clarity reasons, There is one specific number of index for all Tables of this module, even if methods are different 
			1 Discovers the table from received id, extracting the first digit of the id + the second one + get the length of the ID
				=> total_digits = dbem.count_number(id_to_remove).
				=> digit_1 = dbem.digit_selector(id_to_remove, 1,total_digits).
			2 Saves the ID in a list to pass as parameter in the selection query, for placeholder (?) subtitution.
			 	=> selections = [id_to_remove]
				=> place_holders = ",".join("?"*len(selections))
			3 Reads and constructs the true query from db_extern_methods, removing spurious parts, for selecting the row with the goal of checking its existance
				=> leggiline = dbem.get_line_selection()
				=> data.replace()
			4 Controls that the element to eliminate really exists:
				If the row to remove is present:
					If the element is a Need:
						Selects parameters for calling cancel_from_needs() that will remove the db tuple
						Updates needs occurrence in the deletions_list, for successive insertions
						Removes also from kids-needs from db_default_lists using grep bash command 
						txt_to_remove_1 = "grep -v \" {}),\" db_default_lists.py > tmpfile_66 && mv tmpfile_66 db_default_lists.py".format(id_to_remove) 
						run(txt_to_remove_1, shell=True)
						Quit.
							=> return()
							In case of a need everything is done in this branch 
				If the element is a Question:
					Effectively cancels the question from database (DANGEROUS!!!)
					Calls methods for fixing tables involving the question.
						=> self.build_ScoresKindOfGame_table()
						=> self.build_ScoresKindOfGame_new_table()
				If the elements is part of the therapies conducted by patients... [Therapy, Treatments, Session, Match, Game, Entertainments]	
					A) 
					Retrieves columns key for creating the deletion query, according to which table, and detect if a session/match/game is disposable or not!
					(different case for find minimum distinguishable elements to insert in the query). 
					The following values are taken to compose the line for the removal of the line in the dbdl file with grep
						=> first_column = return_que[0][0]
						=> second_column = return_que[0][1]
						=> is_disposable_or_not = return_que[0][2]
					N.B > return_queue is the row to cancel founded with the constructed ad-hoc query

					Session case:

					Matches case:
					B)
					Finally if the removal is allowed:
						Erases from table the indicted tuple.
						If the object to remove belong to a class that incorporate other subclasses (Sessions and Matches) all rows on the bridge table Ù
						that contains that ID must be removed to with the series of queries...
						=> e.g., ######################################################################################################
						db_self.curs.execute("DELETE FROM Games WHERE disposable = 1 AND game_id IN (
						SELECT Games.game_id FROM Games JOIN (Sessions_Matches JOIN Matches_Games USING (match_id)) USING (game_id)
						WHERE session_id = ?)",(id_to_remove,))
						###############################################################################################################
						
						Removes also from db_default_lists (dbdl) trough bash grep command.
							_the line correspondent to the subject of the deletion, the one with all the attributes (all_sessions_list =[) 
							_the line with the id_to_remove at the beginning for all kind of lists (of dbdl) that contains the link to that id
								example session >> (20021,
							_the line with the id_to_remove at the end for lists that contains the link to that id
								example session >> 20021),
					C) 
					Updates the global deletion lists of databaseManager() object , appending the id of eliminated instance.
					Using the ind_del var set before, it is possible to retrieve the right list (of the proper type) inside the deletions_list! (e.g. session is the number 3)
						=> deletions_list[ind_del].pop(0)
						=> deletions_list[ind_del].append(d) 
					Erases from db all rows in all other tables involved, (recap + summary) depending on the principal table of the id_to_remove.
					Allocates all modifications into db.
						=> commit()
		Parameters
			 int id_to_remove
		Notes
			For all tables that contains ID Kids, Sessions, Match, Games, Needs (1000---,2000---,3000---,4000---,5000, 7000)
			Patches cannot be removed. (no 7000 id)
			Needs table also requires an update of oimi_queries file [cancel_from_needs()], for others only in db_default_lists is changed
	'''
def delete_row_from_table_2_doc():
	''' Info
			Deletes row in specified table, given two parameters 
			Works for Kids (name, surname)
		Details
			Controls that the element to eliminate really exists.
			If true:
				Saves id in a list to pass as parameter in the selection query, for placeholder (?) subtitution.
					=> delete_this = [result]
					=> place_holders = ",".join("?"*len(delete_this))
				Updates the deletions lists, appending the id of eliminated instance.
					=> deletions_list[ind_del].pop(0)
					=> deletions_list[ind_del].append(d) 
				Enables deletion's triggers.
					must be placed here, not in main file, for avoiding trigger errors every time the table will be temporary removed (for eg. after a sort)
					=> self.enable_deletion_trigger_kid_1() 
					=> self.enable_deletion_trigger_kid_2()
					=> self.enable_deletion_trigger_kid_3()
					=> self.enable_deletion_trigger_kid_4()
					=> self.enable_deletion_trigger_kid_5()
				Reads and constructs the true query from db_extern_methods, removing spurious parts,
					for selecting the row for checking its existance
					=> leggiline = dbem.get_line_selection()
					=> data.replace()
				Effectively cancels the tuple from database. 
				Removes also from db_default_lists trough bash grep command.
					=> txt_to_remove = "grep -v \"\'{}\', \'{}\'\" db_default_lists.py > tmpfile_1 && mv tmpfile_1 db_default_lists.py".format(first_param, second_param)
				Allocates all modifications into db.
					=> commit()
		Parameters
			str which_table
			str first_param
			str second_param = if true also original instance in table must be eliminated, not just bridge 
		Notes
			Essential initialization for dontContinue = 0 
	'''
def delete_row_from_bridge_doc():
	''' Info
			Deletes row in specified table, given two parameters.
			Works for bridges tables with double integer (Kids_Sessions, Kids_Needs, Sessions_Matches, Matches_Games, Games_Questions, Questions_Patches)
		Details
			Controls that the element to eliminate really exists.
			If true:
				Reads and constructs the true query from db_extern_methods, removing spurious parts,
				for deleting the row for checking its existance, replacing ! ? $ .. symbols
					=> leggiline = dbem.get_line_selection()
					=> data.replace()
				Allocates all modifications into db => [commit()].
				Effectively cancels the tuple from database. 
				Removes also from db_default_lists trough bash grep command.
					=> txt_to_remove = "grep -v 
			must_eliminate:
				ind_del mi serve per questo!!!
				if must_eliminate is set, also original elements of second column of table must be eliminate 
				but not in the case of Questions or Patches! ??? perchè no question se dall'altra parte avevo deciso (metodo precedente) avevo deciso di non eliminarle??
			Finds the name of the table from which the second column came from, with split (max of 1 result imposed)
				=> remove_also_from_table = which_table.split("_", 1)[1].split("/n", 1)[0]
			Updates the deletions lists, appending the id of eliminated instance.
				=> deletions_list[ind_del].pop(0)
				=> deletions_list[ind_del].append(d) 
			Effectively cancels the tuple from database. 
			Removes also from db_default_lists trough bash grep command.
				=> txt_to_remove = "grep -v					
		Parameters
			str which_table
			int first_param
			int second_param
			int must_eliminate = if true also original instance in table must be eliminated, not just bridge 
		Notes
			Essential initialization for dontContinue = 0 
			Using the 'with' keyword, file will be closed automatically
	'''
def delete_row_from_bridge_str_doc():
	''' Info
			Deletes row in specified table, given two parameters.
			Works for bridges tables with name as prmary key (Kids_Impositions / Kids_Achievements / Kids_Symptoms)
		Details
			Controls that the element to eliminate really exists.
			If true:
				Reads and constructs the true query from db_extern_methods, removing spurious parts,
				for deleting the row for checking its existance, replacing ! ? $ .. symbols
					=> leggiline = dbem.get_line_selection()
					=> data.replace()
				Allocates all modifications into db => [commit()].
				Effectively cancels the tuple from database. 
				Removes also from db_default_lists trough bash grep command.
					=> txt_to_remove = "grep -v 
			if must_eliminate is set, also original elements of second column of table must be eliminate 
			but not in the case of Questions or Patches! ??? perchè no question se dall'altra parte avevo deciso (metodo precedente) avevo deciso di non eliminarle??
				Finds the name of the table from which the second column came from, with split (max of 1 result imposed)
					=> remove_also_from_table = which_table.split("_", 1)[1].split("/n", 1)[0]
				Updates the deletions lists, appending the id of eliminated instance.
					=> deletions_list[ind_del].pop(0)
					=> deletions_list[ind_del].append(d) 
				Effectively cancels the tuple from database. 
				Removes also from db_default_lists trough bash grep command.
					=> txt_to_remove = "grep -v					
		Parameters
			str which_table
			int first_param
			str second_param
			int must_eliminate = if true also original instance in table must be eliminated, not just bridge 
		Notes
			Essential initialization for dontContinue = 0 
			Using the 'with' keyword, file will be closed automatically
				#eliminate single row with the two meaningful values
	'''
def delete_all_from_bridge_first_doc():
	''' Info
			Deletes all rows in specified principal table, starting from a given param that represent a value for first column of a bridge table.
			Works for bridges tables with name as prmary key (Kids_Sessions, Sessions_Matches, Games_Questions, Questions_Patches)
		Details
			Controls that the element to eliminate really exists.
			If true:
				Reads and constructs the true query from db_extern_methods, removing spurious parts,
				for deleting the row, replacing ! ? $ .. special symbols
					=> leggiline = dbem.get_line_selection()
					=> data.replace()
				Allocates all modifications into db => [commit()].
				Effectively cancels the tuple from database.
			if must_eliminate is set, also original elements of second column of table must be eliminate 
			but not in the case of Questions or Patches! ??? perchè no question se dall'altra parte avevo deciso (metodo precedente) avevo deciso di non eliminarle??
				Finds the name of the table from which the second column came from, with split (max of 1 result imposed)
					=> remove_also_from_table = which_table.split("_", 1)[1].split("/n", 1)[0]
				Updates the deletions lists, appending the id of eliminated instance.
					=> deletions_list[ind_del].pop(0)
					=> deletions_list[ind_del].append(d) 
				Effectively cancels the tuple from database. 
				Removes also from db_default_lists trough bash grep command, all lines that contains the removed id (cames from second_column)
					=> txt_to_remove = "grep -v					
		Parameters
			str which_table
			int first_param
			int must_eliminate = if true also original instance in table must be eliminated, not just bridge 
		Notes
			Essential initialization for dontContinue = 0 
			Using the 'with' keyword, file will be closed automatically
	'''
def delete_all_from_bridge_second_doc():
	''' Info
			Deletes all rows in specified principal table, starting from a given param that represent a value for second column of a bridge table.
			Works for bridges tables with name as prmary key (Kids_Sessions, Sessions_Matches, Games_Questions, Questions_Patches)
			Don't remove also principal foreign key. Just the link between two table
		Details
			Controls that the element to eliminate really exists.
			If true:
				Reads and constructs the true query from db_extern_methods, removing spurious parts,
				for deleting the row, replacing ! ? $ .. special symbols
					=> leggiline = dbem.get_line_selection()
					=> data.replace()
				Allocates all modifications into db => [commit()].
				Effectively cancels the tuple from database. 
				Removes also from db_default_lists trough bash grep command, the line with the removed id (cames from second_column)
					=> txt_to_remove = "grep -v 
		Parameters
			str which_table
			int second_param
			int must_eliminate = if true also original instance in table must be eliminated, not just bridge 
		Notes
			Essential initialization for dontContinue = 0 
			Using the 'with' keyword, file will be closed automatically
			No removal of original
	'''
def cancel_need_doc():
	''' Info 
			Deletes a need from Need table
		Details
			Controls that the element to eliminate really exists.
			If true:
				Detects scope
				Search for three different lines (use Invalid)
				Using Memory mapping can load the file directly into computer memory, improving search's performance.
				fileno() returns the integer file descriptor of a requested I/O operations from the operating system.
				(An encoding is mandatory)
					=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
				# non posso fare direttamente fetchone()[0] pwerché se risutla errore non è sucettibile a [] None!!
				'code' string #need for avoiding that python interprete this as an object = need add'' and then remove '' --> successive replace
				Reads and constructs the true query from db_extern_methods, removing spurious parts,
				for deleting the row, replacing ! ? $ .. symbols
				Removes also from db_default_lists trough bash grep command, the line with the scope
					=> txt_to_remove = "grep -v 
				Removes also from oimi_queries trough bash grep command, the line with the scope
					=> txt_to_remove = "grep -v 
				Handles the case of remotion of the last element in the list, then need a further modification in oimi_queries file (comma issue)
					Searches in readlines for proper position of value to remove, where file must be written => locs.
					Adds the penultimate line (the one immediately above) without comma, it will be the new last element of the list.
					The right position in this case is under locs line => locs[j] = locs[j] + 1 + index_for_lastest
					Changes query adding to lines new restriction => lines.insert(locs[-j], search_str)
					Overwrites all lines into file 
						=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w',-2) 
						=> 'w' option will overwrite any existing content
						=> -2 buffering
							Enabling buffering means that you're not directly interfacing with the OS's representation of a file.
							Instead, a chunk of data is read from the raw OS filestream into a buffer until it is consumed,
							getting a BufferedIOBase object, wrapping an underlying RawIOBase (which represents the raw file stream).
							A negative buffering means to use the system default, which is usually line buffered.
						=> fp.write(''.join(lines)
					Removes also from db_default_lists trough bash grep command, the line with penultimate value, that now needs the "," comma.
						=> txt_to_remove = "grep -v 
					Removes also from db_default_lists trough bash grep command, the last element that has to be erased.
						=> txt_to_remove = "grep -v												
		Parameters
			str which_purpose
			str which_scope
		Notes	
			Using the 'with' keyword, file will be closed automatically
			Called by delete_from_id()
			Cancels both from oimi_queries and db_default_lists, but only the scope! not the focus
			The focus itself canno be removed
	'''	
def cancel_from_doc():
	''' Info 
			Deletes a need from tables that have text name as primary id.
			(Achievements, Impositions, Symptoms, Available_games)
		Details
			Retrieves element for building selection query and for checking if current element to remove is the last for according to given table
				=> code = column name 
				=> choose = for successive if-condition
				=> last_to_change = self.last_words..
				=> index_for_subs = for jumping (summing to locs line) the correct number of lines for adding new line in correct place in db_default_lists
				Start the right trigger for removing all instance in bridge table, that are 
					=> self.enable_deletion_trigger_kid_achievement()
			Controls that the element to eliminate really exists.
			If true:
				Search for proper line' position 
				Using Memory mapping can load the file directly into computer memory, improving search's performance.
				fileno() returns the integer file descriptor of a requested I/O operations from the operating system.
				(An encoding is mandatory)
					=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
				In this case check existance also in real database if received which_name corresponds to right table.
				'code' string #need for avoiding that python interprete this as an object = need add'' and then remove '' --> successive replace
				Reads and constructs the true query from db_extern_methods, removing spurious parts,
				for deleting the row, replacing ! ? $ .. symbols
				Removes also from db_default_lists trough bash grep command, the line with the scope
					=> txt_to_remove = "grep -v 
				Removes also from oimi_queries trough bash grep command, the line with the scope
					=> txt_to_remove = "grep -v 
				Handles the case of remotion of the last element in the list, then need a further modification in oimi_queries file (comma issue)
					
					Detects line above the line that contains the tuple to erase 
					This time the removal from oimi_queries is not done trough bash grep command, 
					but
						reading entire file, not all lines 
							=> input_stream=f.read()
						splitting the file into \n line
							=> input_stream_lines=input_stream.split("\n")
						creating a new file equal to original one, withuot undesired line (the one immediately above the line to remove)
						(in this case is better to close the file manually)
							=> output_stream=output_stream+line+"\n"
					For each id create the locs, to avoid possible unpredictable failures of formatting \t during creation of locs.
					Searches in readlines for proper position of value to remove, where file must be written => locs.
					Adds the penultimate line (the one immediately above) without comma, it will be the new last element of the list.
					The right position in this case is under locs line => locs[j] = locs[j] + 1 + index_for_lastest
					Changes query adding to lines new restriction => lines.insert(locs[-j], search_str)
					Overwrites all lines into file 
						=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w') 
						=> g.write(output_stream)
						=> 'w' option will overwrite any existing content
						=> no buffering provision
					Removes also from db_default_lists trough bash grep command
						=> txt_to_remove = "grep -v							
		Parameters
			str which_table
			str which_name
		Notes
			Deletions both in oimi_queries and db_default_lists
			Using the 'with' keyword, file will be closed automatically
			Cancels both from oimi_queries and db_default_lists, but only the scope! not the focus
			The focus itself canno be removed
	'''	
def cancel_audio_doc():
	''' Info 
			Deletes an audio from Audios table, can remove also the question related to the audio.
		Details
			Enables sqlite deletion triggers
			Controls that the element to eliminate really exists.
				Search for proper line position.
				Using Memory mapping can load the file directly into computer memory, improving search's performance.
				fileno() returns the integer file descriptor of a requested I/O operations from the operating system.
				(An encoding is mandatory)
					=> mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ)
			If true:
				Retrieves question_id of current audio, even if is useful later only if element to remove is last of the table
					=> what_was_question_id
				Reads and constructs the true query from db_extern_methods, removing spurious parts,
				for deleting the row, replacing ! ? $ .. special symbols
					=> leggiline = dbem.get_line_selection()
					=> data.replace()
				Allocates all modifications into db => [commit()].
				Effectively cancels the tuple from database. 
				Removes also from db_default_lists trough bash grep command, the line with the element to remove
					=> txt_to_remove = "grep -v 
				Removes also from oimi_queries trough bash grep command, the line with the element to remove
					(creating a new copy of the file without lines matchings)
					=> txt_to_remove = "grep -v 
				Handles the case of remotion of the last element in the list, then need a further modification in oimi_queries file (comma issue)
					Detects line above the line that contains the tuple to erase 
					This time the removal from oimi_queries is not done trough bash grep command, 
					but
						reading entire file, not all lines 
							=> input_stream=f.read()
						splitting the file into \n line
							=> input_stream_lines=input_stream.split("\n")
						creating a new file equal to original one, withuot undesired line (the one immediately above the line to remove)
						(in this case is better to close the file manually)
							=> output_stream=output_stream+line+"\n"
					Searches in readlines for proper position of value to remove, where file must be written => locs.
					Adds the penultimate line (the one immediately above) without comma, it will be the new last element of the list.
					The right position in this case is under locs line => locs[j] = locs[j] + 1 + index_for_lastest
					Changes query adding to lines new restriction => lines.insert(locs[-j], search_str)
					Overwrites all lines into file 
						=> open('/home/pi/OIMI/oimi_code/src/managers/database_manager/oimi_queries.py', 'w') 
						=> g.write(output_stream)
						=> 'w' option will overwrite any existing content
						=> no buffering provision
					Removes also from db_default_lists trough bash grep command, the line with the question (linked to the audio input parameter) from Questions
						(creating a new copy of the file without lines matchings)
						=> txt_to_remove = "grep -v		
					Removes also from db_default_lists trough bash grep command, the line with the question (linked to the audio input parameter) from Questions_Patches
						(creating a new copy of the file without lines matchings)
						=> txt_to_remove = "grep -v		
					Removes also from db_default_lists trough bash grep command, the line with the question (linked to the audio input parameter) from Games_Questions
						(creating a new copy of the file without lines matchings)
						=> txt_to_remove = "grep -v 
					Calls delete_from_id(what_was_question_id) in order to effectively remove all rows and tables involving the current question and set the deletetions_list 
				Calls insert_new_Audios_table()
					If the question linked to the deleted audio remains the same, we have to insert immediately the new id and link for the replacement.
		Parameters
			int which_audio
			int cancel_question = if true remove also the linked questions and all occurrence of the question in all tables
			int new_candidate
			str its_path 
		Notes
			No autoincremented integer id here, there's no need to use the deletion lists for 
			Deletions both in oimi_queries and db_default_lists
			Using the 'with' keyword, file will be closed automatically
			Called by delete_from_id()
			Cancels both from oimi_queries and db_default_lists, but only the scope, not the focus
	'''	


if __name__ == '__main__':
    help(cancel_from_doc)
    help(cancel_need_doc)
    help(cancel_audio_doc)
