"""Info:
	Oimi robot database_manager update achievements
	it's in a separated module for avoiding the warning variable tracking size limit exceeded +
	and also the method is splitted in three parts for update all lines on kids_achievements table, splitted for compilation problems
	renew_Kids_Achievements_1
	renew_Kids_Achievements_2
	renew_Kids_Achievements_3
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os
if '/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/extras_database_manager')
#sys.path.insert(1, os.path.join(sys.path[0], '.'))
import re
import mmap
from importlib import reload
from subprocess import run 
from typing import Optional
import oimi_queries as oq
import db_default_lists as dbdl
import db_extern_methods as dbem
import audio_blob_conversion as abc
cimport modify_default_lists as mdl
cimport database_controller_build as acb
cimport database_controller_add_into as dca
cimport database_controller_check_mixed as dcm
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
# ----------------------------------------------------------------------------------------------------------------------------------------
# Renew
# ----------------------------------------------------------------------------------------------------------------------------------------
cpdef renew_Kids_Achievements_table(db_self, int which_kid):
	renew_Kids_Achievements_table_1(db_self, which_kid)
	renew_Kids_Achievements_table_2(db_self, which_kid)
	renew_Kids_Achievements_table_3(db_self, which_kid)
	renew_Kids_Achievements_table_4(db_self, which_kid)

cdef renew_Kids_Achievements_table_1(db_self, int which_kid):
	try:
		db_self.curs.execute("SELECT COUNT(session_id) FROM Full_played_Recap_giga WHERE kid_id=? AND complexity='Easy'",(which_kid,))
		res00 = db_self.curs.fetchone()
		print("res00 eccola ", res00)
		if res00:
			res00 = res00[0]
			print("Num of easy session done ",res00)
			param_achi = (res00,"Num of easy session done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(session_id) FROM  Full_played_Recap_giga WHERE kid_id=? AND complexity='Normal'",(which_kid,))
		res01 = db_self.curs.fetchone()
		print("res01 eccola ", res01)
		if res01:
			res01 = res01[0]
			print("Num of normal session done ",res01)
			param_achi = (res01,"Num of normal session done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(session_id) FROM Full_played_Recap_giga WHERE kid_id=? AND complexity='Medium'",(which_kid,))
		res02 = db_self.curs.fetchone()
		print("res02 eccola ", res02)
		if res02:
			res02 = res02[0]
			print("Num of medium session done ",res02)
			param_achi = (res02,"Num of medium session done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(session_id) FROM Full_played_Recap_giga WHERE kid_id=? AND complexity='Hard'",(which_kid,))
		res03 = db_self.curs.fetchone()
		print("res03 eccola ", res03)
		if res03:
			res03 = res03[0]
			print("Num of hard session done ",res03)
			param_achi = (res03,"Num of hard session done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND m_difficulty='Easy_1'",(which_kid,))
		res04 = db_self.curs.fetchone()
		print("res04 eccola ", res04)
		if res04:
			res04 = res04[0]
			print("Num of easy_1 matches done ",res04)
			param_achi = (res04,"Num of easy_1 matches done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND m_difficulty='Easy_2'",(which_kid,))
		res05 = db_self.curs.fetchone()
		print("res05 eccola ", res05)
		if res05:
			res05 = res05[0]
			print("Num of easy_2 matches done ",res05)
			param_achi = (res05,"Num of easy_2 matches done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND m_difficulty='Normal_1'",(which_kid,))
		res06 = db_self.curs.fetchone()
		print("res06 eccola ", res06)
		if res06:
			res06 = res06[0]
			print("Num of normal_1 matches done ",res06)
			param_achi = (res06,"Num of normal_1 matches done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND m_difficulty='Normal_2'",(which_kid,))
		res07 = db_self.curs.fetchone()
		print("res07 eccola ", res07)
		if res07:
			res07 = res07[0]
			print("Num of normal_2 matches done ",res07)
			param_achi = (res07,"Num of normal_2 matches done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND m_difficulty='Medium_1'",(which_kid,))
		res08 = db_self.curs.fetchone()
		print("res08 eccola ", res08)
		if res08:
			res08 = res08[0]
			print("Num of medium_1 matches done ",res08)
			param_achi = (res08,"Num of medium_1 matches done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND m_difficulty='Medium_2'",(which_kid,))
		res09 = db_self.curs.fetchone()
		print("res09 eccola ", res09)
		if res09:
			res09 = res09[0]
			print("Num of medium_2 matches done ",res09)
			param_achi = (res09,"Num of medium_2 matches done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND m_difficulty='Hard_1'",(which_kid,))
		res10 = db_self.curs.fetchone()
		print("res10 eccola ", res10)
		if res10:
			res10 = res10[0]
			print("Num of hard_1 matches done ",res10)
			param_achi = (res10,"Num of hard_1 matches done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND m_difficulty='Hard_2'",(which_kid,))
		res11 = db_self.curs.fetchone()
		print("res11 eccola ", res11)
		if res11:
			res11 = res11[0]
			print("Num of hard_2 matches done ",res11)
			param_achi = (res11,"Num of hard_2 matches done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND g_difficulty='Easy_1'",(which_kid,))
		res12 = db_self.curs.fetchone()
		print("res12 eccola ", res12)
		if res12:
			res12 = res12[0]
			print("Num of easy_1 games done ",res12)
			param_achi = (res12,"Num of easy_1 games done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND g_difficulty='Easy_2'",(which_kid,))
		res13 = db_self.curs.fetchone()
		print("res13 eccola ", res13)
		if res13:
			res13 = res13[0]
			print("Num of easy_2 games done ",res13)
			param_achi = (res13,"Num of easy_2 games done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND g_difficulty='Normal_1'",(which_kid,))
		res14 = db_self.curs.fetchone()
		print("res14 eccola ", res14)
		if res14:
			res14 = res14[0]
			print("Num of normal_1 games done ",res14)
			param_achi = (res14,"Num of normal_1 games done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND g_difficulty='Normal_2'",(which_kid,))
		res15 = db_self.curs.fetchone()
		print("res15 eccola ", res15)
		if res15:
			res15 = res15[0]
			print("Num of normal_2 games done ",res15)
			param_achi = (res15,"Num of normal_2 games done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND g_difficulty='Medium_1'",(which_kid,))
		res16 = db_self.curs.fetchone()
		print("res16 eccola ", res16)
		if res16:
			res16 = res16[0]
			print("Num of medium_1 games done ",res16)
			param_achi = (res16,"Num of medium_1 games done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND g_difficulty='Medium_2'",(which_kid,))
		res17 = db_self.curs.fetchone()
		print("res17 eccola ", res17)
		if res17:
			res17 = res17[0]
			print("Num of medium_2 games done ",res17)
			param_achi = (res17,"Num of medium_2 games done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND g_difficulty='Hard_1'",(which_kid,))
		res18 = db_self.curs.fetchone()
		print("res18 eccola ", res18)
		if res18:
			res18 = res18[0]
			print("Num of hard_1 games done ",res18)
			param_achi = (res18,"Num of hard_1 games done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND g_difficulty='Hard_2'",(which_kid,))
		res19 = db_self.curs.fetchone()
		print("res19 eccola ", res19)
		if res19:
			res19 = res19[0]
			print("Num of hard_2 games done ",res19)
			param_achi = (res19,"Num of hard_2 games done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
	
		db_self.conn.commit()
	
	except Exception as e:
		print("--> An error occurred in renew 1")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef renew_Kids_Achievements_table_2(db_self, int which_kid):
	try:
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND kind='1F'",(which_kid,))
		res20 = db_self.curs.fetchone()
		print("res20 eccola ", res20)
		if res20:
			res20 = res20[0]
			print("Kind 1F done ",res20)
			param_achi = (res20,"Kind 1F done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND kind='2L'",(which_kid,))
		res21 = db_self.curs.fetchone()
		print("res21 eccola ", res21)
		if res21:
			res21 = res21[0]
			print("Kind 2L done ",res21)
			param_achi = (res21,"Kind 2L done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND kind='3S'",(which_kid,))
		res22 = db_self.curs.fetchone()
		print("res22 eccola ", res22)
		if res22:
			res22 = res22[0]
			print("Kind 3S done ",res22)
			param_achi = (res22,"Kind 3S done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND kind='4P'",(which_kid,))
		res23 = db_self.curs.fetchone()
		print("res23 eccola ", res23)
		if res23:
			res23 = res23[0]
			print("Kind 4P done ",res23)
			param_achi = (res23,"Kind 4P done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND kind='5K'",(which_kid,))
		res24 = db_self.curs.fetchone()
		print("res24 eccola ", res24)
		if res24:
			res24 = res24[0]
			print("Kind 5K done ",res24)
			param_achi = (res24,"Kind 5K done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND kind='6I'",(which_kid,))
		res25 = db_self.curs.fetchone()
		if res25:
			print("res25 eccola ", res25)
			res25 = res25[0]
			print("Kind 6I done ",res25)
			param_achi = (res25,"Kind 6I done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND kind='7O'",(which_kid,))
		res26 = db_self.curs.fetchone()
		print("res26 eccola ", res26)
		if res26:
			res26 = res26[0]
			print("Kind 7O done ",res26)
			param_achi = (res26,"Kind 7O done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND kind='8Q'",(which_kid,))
		res27 = db_self.curs.fetchone()
		print("res27 eccola ", res27)
		if res27:
			res27 = res27[0]
			print("Kind 8Q done ",res27)
			param_achi = (res27,"Kind 8Q done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Full_played_Recap_giga WHERE kid_id=? AND kind='9C'",(which_kid,))
		res28 = db_self.curs.fetchone()
		print("res28 eccola ", res28)
		if res28:
			res28 = res28[0]
			print("Kind 9C done ",res28)
			param_achi = (res28,"Kind 9C done",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(session_id) FROM Sessions JOIN Full_played_Recap_giga pk USING(session_id) WHERE kid_id = ? AND pk.order_difficulty_matches='same' GROUP BY match_id",(which_kid,))
		res29 = db_self.curs.fetchone()
		print("res29 eccola ", res29)
		if res29:
			res29 = res29[0]
		else:	
			res29 = 0
		print("Num of sessions with matches in SAME order of difficulty ",res29)
		param_achi = (res29,"Num of sessions with matches in SAME order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(session_id) FROM Sessions JOIN Full_played_Recap_giga pk USING(session_id) WHERE kid_id = ? AND pk.order_difficulty_matches='casu' GROUP BY match_id",(which_kid,))
		res30 = db_self.curs.fetchone()
		print("res30 eccola ", res30)
		if res30:
			res30 = res30[0]
		else:
			res30 = 0
		print("Num of sessions with matches in CASU order of difficulty ",res30)
		param_achi = (res30,"Num of sessions with matches in CASU order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(session_id) FROM Sessions JOIN Full_played_Recap_giga pk USING(session_id) WHERE kid_id = ? AND pk.order_difficulty_matches='asc' GROUP BY match_id",(which_kid,))
		res31 = db_self.curs.fetchone()
		print("res31 eccola ", res31)
		if res31:
			res31 = res31[0]
		else:
			res31 = 0
		print("Num of sessions with matches in ASC order of difficulty ",res31)
		param_achi = (res31,"Num of sessions with matches in ASC order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(session_id) FROM Sessions JOIN Full_played_Recap_giga pk USING(session_id) WHERE kid_id = ? AND pk.order_difficulty_matches='desc' GROUP BY match_id",(which_kid,))
		res32 = db_self.curs.fetchone()
		print("res32 eccola ", res32)
		if res32:
			res32 = res32[0]
		else:
			res32 = 0
		print("Num of sessions with matches in DESC order of difficulty" ,res32)
		param_achi = (res32,"Num of sessions with matches in DESC order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(session_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_games='same' GROUP BY match_id",(which_kid,))
		res33 = db_self.curs.fetchone()
		print("res33 eccola ", res33)
		if res33:
			res33 = res33[0]
		else:
			res33 = 0
		print("Num of sessions with matches with SAME quantity of games ",res33)
		param_achi = (res33,"Num of sessions with matches with SAME quantity of games",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(session_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_games='casu' GROUP BY match_id",(which_kid,))
		res34 = db_self.curs.fetchone()
		print("res34 eccola ", res34)
		if res34:
			res34 = res34[0]
		else:			
			res34 = 0
		print("Num of sessions with matches with CASU quantity of games ",res34)
		param_achi = (res34,"Num of sessions with matches with CASU quantity of games",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(session_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_games='asc' GROUP BY match_id",(which_kid,))
		res35 = db_self.curs.fetchone()
		print("res35 eccola ", res35)
		if res35:
			res35 = res35[0]
		else:	
			res35 = 0
		print("Num of sessions with matches with ASC quantity of games ",res35)
		param_achi = (res35,"Num of sessions with matches with ASC quantity of games",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(session_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_games='desc' GROUP BY match_id",(which_kid,))
		res36 = db_self.curs.fetchone()
		print("res36 eccola ", res36)
		if res36:
			res36 = res36[0]
		else:
			res36 = 0
		print("Num of sessions with matches with DESC quantity of games ",res36)
		param_achi = (res36,"Num of sessions with matches with DESC quantity of games",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_games='same' GROUP BY game_id",(which_kid,))
		res37 = db_self.curs.fetchone()
		print("res37 eccola ", res37)
		if res37:
			res37 = res37[0]
		else:
			res37 = 0
		print("Num of matches with games in SAME order of difficulty ",res37)
		param_achi = (res37,"Num of matches with games in SAME order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_games='casu' GROUP BY game_id",(which_kid,))
		res38 = db_self.curs.fetchone()
		print("res38 eccola ", res38)
		if res38:
			res38 = res38[0]
		else:		
			res38 = 0
		print("Num of matches with games in CASU order of difficulty ",res38)
		param_achi = (res38,"Num of matches with games in CASU order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_games='asc' GROUP BY game_id",(which_kid,))
		res39 = db_self.curs.fetchone()
		print("res39 eccola ", res39)
		if res39:
			res39 = res39[0]
		else:
			res39 = 0
		print("Num of matches with games in ASC order of difficulty ",res39)
		param_achi = (res39,"Num of matches with games in ASC order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_games='desc' GROUP BY game_id",(which_kid,))
		res40 = db_self.curs.fetchone()
		print("res40 eccola ", res40)
		if res40:
			res40 = res40[0]
		else:
			res40 = 0
		print("Num of matches with games in DESC order of difficulty ",res40)
		param_achi = (res40,"Num of matches with games in DESC order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(match_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_questions='same' GROUP BY game_id",(which_kid,))
		res41 = db_self.curs.fetchone()
		print("res41 eccola ", res41)
		if res41:
			res41 = res41[0]
		else:
			res41 = 0
		print("Num of matches with games with SAME quantity of questions ",res41)
		param_achi = (res41,"Num of matches with games with SAME quantity of questions",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_questions='casu' GROUP BY game_id",(which_kid,))
		res42 = db_self.curs.fetchone()
		print("res42 eccola ", res42)
		if res42:
			res42 = res42[0]
		else:
			res42 = 0
		print("Num of matches with games with CASU quantity of questions ",res42)
		param_achi = (res42,"Num of matches with games with CASU quantity of questions",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_questions='asc' GROUP BY game_id",(which_kid,))
		res43 = db_self.curs.fetchone()
		print("res43 eccola ", res43)
		if res43:
			res43 = res43[0]
		else:
			res43 = 0
		print("Num of matches with games with ASC quantity of questions ",res43)
		param_achi = (res43,"Num of matches with games with ASC quantity of questions",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(match_id) FROM Matches JOIN Full_played_Recap_giga pk USING(match_id) WHERE kid_id = ? AND pk.order_quantity_questions='desc' GROUP BY game_id",(which_kid,))
		res44 = db_self.curs.fetchone()
		print("res44 eccola ", res44)
		if res44:
			res44 = res44[0]
		else:
			res44 = 0
		print("Num of matches with games with DESC quantity of questions ",res44)
		param_achi = (res44,"Num of matches with games with DESC quantity of questions",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT COUNT(game_id) FROM Games JOIN Full_played_Recap_giga pk USING(game_id) WHERE kid_id = ? AND pk.order_difficulty_questions='same' GROUP BY game_id",(which_kid,))
		res45 = db_self.curs.fetchone()
		print("res45 eccola ", res45)
		if res45:
			res45 = res45[0]
		else:
			res45 = 0
		print("Num of games with questions in SAME order of difficulty ",res45)
		param_achi = (res45,"Num of games with questions in SAME order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(game_id) FROM Games JOIN Full_played_Recap_giga pk USING(game_id) WHERE kid_id = ? AND pk.order_difficulty_questions='casu' GROUP BY game_id",(which_kid,))
		res46 = db_self.curs.fetchone()
		print("res46 eccola ", res46)
		if res46:
			res46 = res46[0]
		else:
			res46 = 0
		print("Num of games with questions in CASU order of difficulty ",res46)
		param_achi = (res46,"Num of games with questions in CASU order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(game_id) FROM Games JOIN Full_played_Recap_giga pk USING(game_id) WHERE kid_id = ? AND pk.order_difficulty_questions='asc' GROUP BY game_id",(which_kid,))
		res47 = db_self.curs.fetchone()
		print("res47 eccola ", res47)
		if res47:
			res47 = res47[0]
		else:
			res47 = 0
		print("Num of games with questions in ASC order of difficulty ",res47)
		param_achi = (res47,"Num of games with questions in ASC order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(game_id) FROM Games JOIN Full_played_Recap_giga pk USING(game_id) WHERE kid_id = ? AND pk.order_difficulty_questions='desc' GROUP BY game_id",(which_kid,))
		res48 = db_self.curs.fetchone()
		print("res48 eccola ", res48)
		if res48:
			res48 = res48[0]
		else:
			res48 = 0
		print("Num of games with questions in DESC order of difficulty, ", res48)
		param_achi = (res48,"Num of games with questions in DESC order of difficulty",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
	
		db_self.conn.commit()
	
	except Exception as e:
		print("--> An error occurred in renew 2")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)

cdef renew_Kids_Achievements_table_3(db_self, int which_kid):
	try:
		db_self.curs.execute("SELECT COUNT(DISTINCT session_id) FROM Full_played_Recap_giga pk WHERE kid_id = ?",(which_kid,))
		res49 = db_self.curs.fetchone()
		print("res49 eccola ", res49)
		if res49:
			res49 = res49[0]
			print("Total session completed ",res49)
			param_achi = (res49,"Total session completed",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(DISTINCT game_id) FROM Full_played_Recap_giga pk WHERE kid_id = ?",(which_kid,))
		res50 = db_self.curs.fetchone()
		print("res50 eccola ", res50)
		if res50:
			res50 = res50[0]
			print("Total game solved ",res50)
			param_achi = (res50,"Total game solved",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT COUNT(DISTINCT question_id) FROM Full_played_Recap_giga pk WHERE kid_id = ?",(which_kid,))
		res51 = db_self.curs.fetchone()
		print("res51 eccola ", res51)
		if res51:
			res51 = res51[0]
			print("tot ques answered ", res51)
			param_achi = (res51,"Total questions answered",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.curs.execute("SELECT MAX(s.num_of_matches) FROM Sessions s JOIN Full_played_Recap_giga pk USING(session_id) WHERE pk.kid_id = ?",(which_kid,))
		res52 = db_self.curs.fetchone()
		print("res52 eccola ", res52)
		if res52:
			print("max is res", res52)
			res52 = res52[0]
			print("max num of matches in a sessio nis res final ", res52)
			param_achi = (res52,"Max num of matches in a session",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT MAX(m.num_of_games) FROM Matches m JOIN Full_played_Recap_giga pk USING(match_id) WHERE pk.kid_id = ?",(which_kid,))
		res53 = db_self.curs.fetchone()
		print("res53 eccola ", res53)
		if res53:
			print("max is res", res53)
			res53 = res53[0]
			print("max num of games in a match is res final ", res53)
			param_achi = (res53,"Max num of games in a match",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		
		db_self.curs.execute("SELECT MAX(g.num_questions) FROM Games g JOIN Full_played_Recap_giga pk USING(game_id) WHERE pk.kid_id = ?",(which_kid,))
		res54 = db_self.curs.fetchone()
		print("res54 eccola ", res54)
		if res54:
			print("max is res", res54)
			res54 = res54[0]
			print("max num of questions in a game is res final ", res54)
			param_achi = (res54,"Max num of questions in a game",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		#########################################
		que_1 = "SELECT grade_feedback_session FROM Kids_Sessions ks WHERE ks.kid_id = ?"
		param_1 = (which_kid)
		res55 = db_self.execute_param_query(que_1,param_1)
		print("res55" ,res55)
		#res_1 = [item for sublist in resp for item in sublist]
		if res55[0]:
			if 'Perfect' in res55:
				res_gras = 'Perfect'
			elif 'Excellent' in res55:
				res_gras = 'Excellent'
			elif 'Good' in res55:
				res_gras = 'Good'
			elif 'Bad' in res55:
				res_gras = 'Bad'
			elif 'Awful' in res55:
				res_gras = 'Awful'
			else:
				res_gras = 'Nothing'
		else:
			res_gras = 'Awful'
		print("res of max score reached_question ==>", res_gras)	
		param_achi = (res_gras,"Max score reached_session",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		######################################### param query always return a list, at least with a None inside ==> if res: is always True! uso if res[0]
		que_1 = "SELECT grade_feedback_match FROM Kids_Matches WHERE kid_id = ?"
		param_1 = (which_kid)
		res56 = db_self.execute_param_query(que_1,param_1)
		print("res56" ,res56)
		#res_1 = [item for sublist in resp for item in sublist]
		if res56[0]:
			if 'Perfect' in res56:
				res_gram = 'Perfect'
			elif 'Excellent' in res56:
				res_gram = 'Excellent'
			elif 'Good' in res56:
				res_gram = 'Good'
			elif 'Bad' in res56:
				res_gram = 'Bad'
			elif 'Awful' in res56:
				res_gram = 'Awful'
			else:
				res_gram = 'Nothing'
		else:
			res_gram = 'Awful'
		print("res of max score reached_question ==>", res_gram)
		param_achi = (res_gram,"Max score reached_match",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		#########################################
		que_1 = "SELECT grade_feedback_game FROM Kids_Games WHERE kid_id = ?"
		param_1 = (which_kid)
		res57 = db_self.execute_param_query(que_1,param_1)
		print("res57" ,res57)
		#res_1 = [item for sublist in resp for item in sublist]
		if res57[0]:
			if 'Perfect' in res57:
				res_grag = 'Perfect'
			if 'Excellent' in res57:
				res_grag = 'Excellent'
			elif 'Good' in res57:
				res_grag = 'Good'
			elif 'Bad' in res57:
				res_grag = 'Bad'
			elif 'Awful' in res57:
				res_grag = 'Awful'
			else:
				res_grag = 'Nothing'
		else:
			res_grag = 'Awful'
		print("res of max score reached_question ==>", res_grag)
		param_achi = (res_grag,"Max score reached_game",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		##############################################
		#qui vedo se invece andarli a prendere da scores_question!! 
		que_1 = "SELECT grade_feedback_question FROM Kids_Questions pk WHERE kid_id = ?"
		param_1 = (which_kid)
		res58 = db_self.execute_param_query(que_1,param_1)
		print("res58" ,res58)
		#res_1 = [item for sublist in resp for item in sublist]
		if res58[0]:
			if 'Perfect' in res58:
				res_graq = 'Perfect'
			elif 'Excellent' in res58:
				res_graq = 'Excellent'
			elif 'Good' in res58:
				res_graq = 'Good'
			elif 'Bad' in res58:
				res_graq = 'Bad'
			elif 'Awful' in res58:
				res_graq = 'Awful'
			else:
				res_graq = 'Nothing'
		else:
			res_graq = 'Awful'
		print("res of max score reached_question ==>", res_graq)
		param_achi = (res_graq,"Max score reached_question",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
		######################################### param query always return a list, at least with a None inside ==> if res: is always True! uso if res[0]
		que_1 = "SELECT MAX(best_value_feedback_kind) FROM ScoresTotalKindOfGame WHERE kid_id = ? AND kind = '1F'"
		param_1 = (which_kid)
		res59 = db_self.execute_param_query(que_1,param_1)
		print("res59 eccola ", res59)
		if res59[0]:
			res59 = res59[0]
			print("max score 1F is => ", res59)
			param_achi = (res59,"Kind 1F max score",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MAX(best_value_feedback_kind) FROM ScoresTotalKindOfGame WHERE kid_id = ? AND kind = '2L'"
		param_1 = (which_kid)
		res60 = db_self.execute_param_query(que_1,param_1)
		print("res60 eccola ", res60)
		if res60[0]:
			res60 = res60[0]
			print("max score 2l is => ", res60)
			param_achi = (res60,"Kind 2L max score",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MAX(best_value_feedback_kind) FROM ScoresTotalKindOfGame WHERE kid_id = ? AND kind = '3S'"
		param_1 = (which_kid)
		res61 = db_self.execute_param_query(que_1,param_1)
		print("res61 eccola ", res61)
		if res61[0]:
			res61 = res61[0]
			print("max score 3s is => ", res61)
			param_achi = (res61,"Kind 3S max score",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MAX(best_value_feedback_kind) FROM ScoresTotalKindOfGame WHERE kid_id = ? AND kind = '4P'"
		param_1 = (which_kid)
		res62 = db_self.execute_param_query(que_1,param_1)
		print("res62 eccola ", res62)
		if res62[0]:
			res62 = res62[0]
			print("max score 4p is => ", res62)
			param_achi = (res62,"Kind 4P max score",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MAX(best_value_feedback_kind) FROM ScoresTotalKindOfGame WHERE kid_id = ? AND kind = '5K'"
		param_1 = (which_kid)
		res63 = db_self.execute_param_query(que_1,param_1)
		print("res63 eccola ", res63)
		if res63[0]:
			res63 = res63[0]
			print("max score 5k is => ", res63)
			param_achi = (res63,"Kind 5K max score",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MAX(best_value_feedback_kind) FROM ScoresTotalKindOfGame WHERE kid_id = ? AND kind = '6I'"
		param_1 = (which_kid)
		res64 = db_self.execute_param_query(que_1,param_1)
		print("res64 eccola ", res64)
		if res64[0]:
			res64 = res64[0]
			print("max score 6i is => ", res64)
			param_achi = (res64,"Kind 6I max score",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MAX(best_value_feedback_kind) FROM ScoresTotalKindOfGame WHERE kid_id = ? AND kind = '7O'"
		param_1 = (which_kid)
		res65 = db_self.execute_param_query(que_1,param_1)
		print("res65 eccola ", res65)
		if res65[0]:
			res65 = res65[0]
			print("max score 7o is => ", res65)
			param_achi = (res65,"Kind 7O max score",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MAX(best_value_feedback_kind) FROM ScoresTotalKindOfGame WHERE kid_id = ? AND kind = '8Q'"
		param_1 = (which_kid)
		res66 = db_self.execute_param_query(que_1,param_1)
		print("res66 eccola ", res66)
		if res66[0]:
			res66 = res66[0]
			print("max score 8q is => ", res66)
			param_achi = (res66,"Kind 8Q max score",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MAX(best_value_feedback_kind) FROM ScoresTotalKindOfGame WHERE kid_id = ? AND kind = '9C'"
		param_1 = (which_kid)
		res67 = db_self.execute_param_query(que_1,param_1)
		print("res67 eccola ", res67)
		if res67[0]:
			res67 = res67[0]
			print("max score 9c is => ", res67)
			param_achi = (res67,"Kind 9C max score",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MAX(tot_errors) FROM ScoresTotalQuestion WHERE kid_id = ?"
		param_1 = (which_kid)
		res68 = db_self.execute_param_query(que_1,param_1)
		print("res68 eccola ", res68)
		if res68[0]:
			res68 = res68[0]
			print("max num err in a question is :: ", res68)
			param_achi = (res68,"Max num errors in a question",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MAX(tot_indecisions) FROM ScoresTotalQuestion WHERE kid_id = ?"
		param_1 = (which_kid)
		res69 = db_self.execute_param_query(que_1,param_1)
		print("res69 eccola ", res69)
		if res69[0]:
			res69 = res69[0]
			print("max num INDECIS in a question is :: ", res69)
			param_achi = (res69,"Max num indecisions in a question",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MIN(best_time) FROM ScoresTotalQuestion WHERE kid_id = ?"
		param_1 = (which_kid)
		res70 = db_self.execute_param_query(que_1,param_1)
		print("res70 eccola ", res70)
		if res70[0]:
			res70 = res70[0]
			print("MIN TIME in a question is :: ", res70)
			param_achi = (res70,"Min time question",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MIN(time_of_session) FROM Kids_Sessions WHERE kid_id = ?"
		param_1 = (which_kid)
		res71 = db_self.execute_param_query(que_1,param_1)
		print("res71 eccola ", res71)
		if res71[0]:
			res71 = res71[0]
			print("MIN TIME TOTAL SESSION is :: ", res71)
			param_achi = (res71,"Min time session",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MIN(time_of_match) FROM Kids_Matches WHERE kid_id = ?"
		param_1 = (which_kid)
		res72 = db_self.execute_param_query(que_1,param_1)
		print("res72 eccola ", res72)
		if res72[0]:
			res72 = res72[0]
			print("MIN TIME match is :: ", res72)
			param_achi = (res72,"Min time match",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		que_1 = "SELECT MIN(time_of_game) FROM Kids_Games WHERE kid_id = ?"
		param_1 = (which_kid)
		res73 = db_self.execute_param_query(que_1,param_1)
		print("res73 eccola ", res73)
		if res73[0]:
			res73 = res73[0]
			print("min time game is :: ", res73)
			param_achi = (res73,"Min time game",which_kid)
			db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)
	
		#mi mancano!! # solo mixed!!!!
		#'Num of Mixed match tried',
	
		db_self.conn.commit()
	
	except Exception as e:
		print("--> An error occurred in renew 3")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)


cdef renew_Kids_Achievements_table_4(db_self, int which_kid):
	try:
		db_self.curs.execute("SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1",(which_kid,))
		which_sess = db_self.curs.fetchone()[0]		
		mixed_or_not = dcm.check_session_is_mixed(db_self, which_sess)
		print("resp mixed_or_not of renew = ", mixed_or_not)
		param_achi = (mixed_or_not,"Num of Mixed match tried",which_kid)
		db_self.execute_param_query(oq.sql_update_kids_achi,param_achi)

		db_self.conn.commit()

	except Exception as e:
		print("--> An error occurred in renew 3")
		print("--> Namely")
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
