""" Oimi robot database_manager, nuovo_modulo_salvataggio
	Notes:
		For other detailed info, look at dabatase_doc

	Created by Colombo Giacomo, Politecnico di Milano 2020 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys 
import random
cimport add_prediction_5 as adp5
cimport add_prediction_6 as adp6
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
cpdef extract_pred_info(db_self):
	if db_self.num_of_matches==3 or db_self.num_of_matches==4:
		db_self.extra_time_tolerance = 1
	else:
		db_self.extra_time_tolerance = 0		
	if 'motorial impairment' in db_self.kids_syms:
		db_self.movements_allowed = 0
	else:
		db_self.movements_allowed = 1
	if 'slowness' in db_self.kids_syms:
		db_self.extra_time_tolerance = 1
	else:
		db_self.extra_time_tolerance = 0
	if 'eccessive shyness' in db_self.kids_syms:
		db_self.body_enabled = 0
		db_self.exhortations = 1
	else:
		db_self.body_enabled = 1
	if 'understanding' in db_self.kids_syms:
		db_self.rules_explication = 1
		db_self.repeat_question = 1
	else:
		db_self.rules_explication = 1
	if 'noise hyperactivity' in db_self.kids_syms:
		db_self.led_brightness = 'low'
	else:
		db_self.led_brightness = 'std'
	stress_flag = 0
	if 'memoria deficit' in db_self.kids_syms:
		db_self.repeat_intro = 1
	else:	
		db_self.repeat_intro = 0
	if ('slowness','respect_rules','eccessive_shyness')	in db_self.kids_syms:	
		db_self.exhortations = 1	
	
	if db_self.complexity in ['Easy','Normal']:
		db_self.exhortations = random.randint(0,1)
		db_self.repeat_intro = 1
		db_self.repeat_question
	else:
		db_self.exhortations = random.randint(0,1)			
		db_self.repeat_intro = 0
		db_self.repeat_question

	search_str = "{}, '{}', '{}', {}, {}".format(db_self.num_of_matches, db_self.complexity, db_self.status, db_self.mandatory_imposition, db_self.mandatory_need)
	new_del = 0

	return search_str

cpdef complete_prediction_info(db_self):
	db_self.status = 'to_do'
	db_self.disposable = 0 
	scen = []
	
	if db_self.num_of_matches==1:
		db_self.advisable_time_m1 = 480
		if db_self.games_m1==1:
			db_self.necessary_time_g1_m1 = 120
		if db_self.games_m1==2:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
		if db_self.games_m1==3:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
			db_self.necessary_time_g3_m1 = 120
		if db_self.games_m1==4:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
			db_self.necessary_time_g3_m1 = 120
			db_self.necessary_time_g4_m1 = 120
	if db_self.num_of_matches==2:
		db_self.advisable_time_m1 = 480
		db_self.advisable_time_m2 = 480
		if db_self.games_m1==1:
			db_self.necessary_time_g1_m1 = 120
		if db_self.games_m1==2:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
		if db_self.games_m1==3:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
			db_self.necessary_time_g3_m1 = 120
		if db_self.games_m1==4:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
			db_self.necessary_time_g3_m1 = 120
			db_self.necessary_time_g4_m1 = 120
		if db_self.games_m2==1:
			db_self.necessary_time_g1_m2 = 120
		if db_self.games_m2==2:
			db_self.necessary_time_g1_m2 = 120
			db_self.necessary_time_g2_m2 = 120
		if db_self.games_m2==3:
			db_self.necessary_time_g1_m2 = 120
			db_self.necessary_time_g2_m2 = 120
			db_self.necessary_time_g3_m2 = 120
		if db_self.games_m2==4:
			db_self.necessary_time_g1_m2 = 120
			db_self.necessary_time_g2_m2 = 120
			db_self.necessary_time_g3_m2 = 120
			db_self.necessary_time_g4_m2 = 120
	if db_self.num_of_matches==3:
		db_self.advisable_time_m1 = 480
		db_self.advisable_time_m2 = 480
		db_self.advisable_time_m3 = 480
		if db_self.games_m1==1:
			db_self.necessary_time_g1_m1 = 120
		if db_self.games_m1==2:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
		if db_self.games_m1==3:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
			db_self.necessary_time_g3_m1 = 120
		if db_self.games_m1==4:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
			db_self.necessary_time_g3_m1 = 120
			db_self.necessary_time_g4_m1 = 120
		if db_self.games_m2==1:
			db_self.necessary_time_g1_m2 = 120
		if db_self.games_m2==2:
			db_self.necessary_time_g1_m2 = 120
			db_self.necessary_time_g2_m2 = 120
		if db_self.games_m2==3:
			db_self.necessary_time_g1_m2 = 120
			db_self.necessary_time_g2_m2 = 120
			db_self.necessary_time_g3_m2 = 120
		if db_self.games_m2==4:
			db_self.necessary_time_g1_m2 = 120
			db_self.necessary_time_g2_m2 = 120
			db_self.necessary_time_g3_m2 = 120
			db_self.necessary_time_g4_m2 = 120
		if db_self.games_m3==1:
			db_self.necessary_time_g1_m3 = 120
		if db_self.games_m3==2:
			db_self.necessary_time_g1_m3 = 120
			db_self.necessary_time_g2_m3 = 120
		if db_self.games_m3==3:
			db_self.necessary_time_g1_m3 = 120
			db_self.necessary_time_g2_m3 = 120
			db_self.necessary_time_g3_m3 = 120
		if db_self.games_m3==4:
			db_self.necessary_time_g1_m3 = 120
			db_self.necessary_time_g2_m3 = 120
			db_self.necessary_time_g3_m3 = 120
			db_self.necessary_time_g4_m3 = 120
	if db_self.num_of_matches==4:
		db_self.advisable_time_m1 = 480
		db_self.advisable_time_m2 = 480
		db_self.advisable_time_m3 = 480
		db_self.advisable_time_m4 = 480
		if db_self.games_m1==1:
			db_self.necessary_time_g1_m1 = 120
		if db_self.games_m1==2:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
		if db_self.games_m1==3:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
			db_self.necessary_time_g3_m1 = 120
		if db_self.games_m1==4:
			db_self.necessary_time_g1_m1 = 120
			db_self.necessary_time_g2_m1 = 120
			db_self.necessary_time_g3_m1 = 120
			db_self.necessary_time_g4_m1 = 120
		if db_self.games_m2==1:
			db_self.necessary_time_g1_m2 = 120
		if db_self.games_m2==2:
			db_self.necessary_time_g1_m2 = 120
			db_self.necessary_time_g2_m2 = 120
		if db_self.games_m2==3:
			db_self.necessary_time_g1_m2 = 120
			db_self.necessary_time_g2_m2 = 120
			db_self.necessary_time_g3_m2 = 120
		if db_self.games_m2==4:
			db_self.necessary_time_g1_m2 = 120
			db_self.necessary_time_g2_m2 = 120
			db_self.necessary_time_g3_m2 = 120
			db_self.necessary_time_g4_m2 = 120
		if db_self.games_m3==1:
			db_self.necessary_time_g1_m3 = 120
		if db_self.games_m3==2:
			db_self.necessary_time_g1_m3 = 120
			db_self.necessary_time_g2_m3 = 120
		if db_self.games_m3==3:
			db_self.necessary_time_g1_m3 = 120
			db_self.necessary_time_g2_m3 = 120
			db_self.necessary_time_g3_m3 = 120
		if db_self.games_m3==4:
			db_self.necessary_time_g1_m3 = 120
			db_self.necessary_time_g2_m3 = 120
			db_self.necessary_time_g3_m3 = 120
			db_self.necessary_time_g4_m3 = 120
		if db_self.games_m4==1:
			db_self.necessary_time_g1_m4 = 120
		if db_self.games_m4==2:
			db_self.necessary_time_g1_m4 = 120
			db_self.necessary_time_g2_m4 = 120
		if db_self.games_m4==3:
			db_self.necessary_time_g1_m4 = 120
			db_self.necessary_time_g2_m4 = 120
			db_self.necessary_time_g3_m4 = 120
		if db_self.games_m4==4:
			db_self.necessary_time_g1_m4 = 120
			db_self.necessary_time_g2_m4 = 120
			db_self.necessary_time_g3_m4 = 120
			db_self.necessary_time_g4_m4 = 120

	adp5.pick_scenario(db_self)
	adp6.addition_to_add_predic(db_self, scen)

