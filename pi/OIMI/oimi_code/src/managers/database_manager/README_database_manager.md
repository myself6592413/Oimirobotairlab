# **DATABASE MANAGER**

## Description
Contains all methods for oimi database creation, runtime modifications and maintainance.  
- .py --> Python principal file.  
- .pyx --> Cython source file.  
- .pxd --> Cython file for declarations and addictions also for C-compiler, which is usually included by its corresponding Cython module.   

## Overview

├── add_customized_prediction.cpp
├── add_customized_prediction.cpython-38-x86_64-linux-gnu.so
├── add_customized_prediction.pxd 
├── add_customized_prediction.pyx
├── add_customized_prediction_second_half.cpp
├── add_customized_prediction_second_half.cpython-38-x86_64-linux-gnu.so
├── add_customized_prediction_second_half.pxd
├── add_customized_prediction_second_half.pyx
├── add_prediction_2.cpp
├── add_prediction_2.cpython-38-x86_64-linux-gnu.so
├── add_prediction_2.pxd
├── add_prediction_2.pyx
├── add_prediction_3.cpp
├── add_prediction_3.cpython-38-x86_64-linux-gnu.so
├── add_prediction_3.pxd
├── add_prediction_3.pyx
├── add_prediction_4.cpp
├── add_prediction_4.cpython-38-x86_64-linux-gnu.so
├── add_prediction_4.pxd
├── add_prediction_4.pyx
├── add_prediction_5.cpp
├── add_prediction_5.cpython-38-x86_64-linux-gnu.so
├── add_prediction_5.pxd
├── add_prediction_5.pyx
├── add_prediction_6.cpp
├── add_prediction_6.cpython-38-x86_64-linux-gnu.so
├── add_prediction_6.pxd
├── add_prediction_6.pyx
├── add_prediction.cpp
├── add_prediction.cpython-38-x86_64-linux-gnu.so
├── add_prediction.pxd
├── add_prediction.pyx
├── add_prediction_second_half.cpp
├── add_prediction_second_half.cpython-38-x86_64-linux-gnu.so
├── add_prediction_second_half.pxd
├── add_prediction_second_half.pyx
├── build
│         └── temp.linux-x86_64-3.8
│             ├── add_customized_prediction.o
│             ├── add_customized_prediction_second_half.o
│             ├── add_prediction_2.o
│             ├── add_prediction_3.o
│             ├── add_prediction_4.o
│             ├── add_prediction_5.o
│             ├── add_prediction_6.o
│             ├── add_prediction.o
│             ├── add_prediction_second_half.o
│             ├── database_controller_add_into.o
│             ├── database_controller_add_into_part_2_1.o
│             ├── database_controller_add_into_part_2.o
│             ├── database_controller_add_into_part_3.o
│             ├── database_controller_add_into_part_4.o
│             ├── database_controller_add_into_part_5.o
│             ├── database_controller_add_into_part_6.o
│             ├── database_controller_build.o
│             ├── database_controller_check_mixed.o
│             ├── database_controller_delete.o
│             ├── database_controller_fix_regular_match.o
│             ├── database_controller_generate_online.o
│             ├── database_controller_populate.o
│             ├── database_controller_renew.o
│             ├── database_controller_save_to_files.o
│             ├── database_controller_take.o
│             ├── database_controller_update.o
│             ├── database_controller_update_second.o
│             ├── database_manager.o
│             ├── database_triggers.o
│             ├── modify_default_lists.o
│             └── security_prediction.o
├── database_controller_add_into.cpp
├── database_controller_add_into.cpython-38-x86_64-linux-gnu.so
├── database_controller_add_into_part_2_1.cpp
├── database_controller_add_into_part_2_1.cpython-38-x86_64-linux-gnu.so
├── database_controller_add_into_part_2_1.pxd
├── database_controller_add_into_part_2_1.pyx
├── database_controller_add_into_part_2.cpp
├── database_controller_add_into_part_2.cpython-38-x86_64-linux-gnu.so
├── database_controller_add_into_part_2.pxd
├── database_controller_add_into_part_2.pyx
├── database_controller_add_into_part_3.cpp
├── database_controller_add_into_part_3.cpython-38-x86_64-linux-gnu.so
├── database_controller_add_into_part_3.pxd
├── database_controller_add_into_part_3.pyx
├── database_controller_add_into_part_4.cpp
├── database_controller_add_into_part_4.cpython-38-x86_64-linux-gnu.so
├── database_controller_add_into_part_4.pxd
├── database_controller_add_into_part_4.pyx
├── database_controller_add_into_part_5.cpp
├── database_controller_add_into_part_5.cpython-38-x86_64-linux-gnu.so
├── database_controller_add_into_part_5.pxd
├── database_controller_add_into_part_5.pyx
├── database_controller_add_into_part_6.cpp
├── database_controller_add_into_part_6.cpython-38-x86_64-linux-gnu.so
├── database_controller_add_into_part_6.pxd
├── database_controller_add_into_part_6.pyx
├── database_controller_add_into.pxd
├── database_controller_add_into.pyx
├── database_controller_build.cpp
├── database_controller_build.cpython-38-x86_64-linux-gnu.so
├── database_controller_build.pxd
├── database_controller_build.pyx
├── database_controller_check_mixed.cpp
├── database_controller_check_mixed.cpython-38-x86_64-linux-gnu.so
├── database_controller_check_mixed.pxd
├── database_controller_check_mixed.pyx
├── database_controller_delete.cpp
├── database_controller_delete.cpython-38-x86_64-linux-gnu.so
├── database_controller_delete.pxd
├── database_controller_delete.pyx
├── database_controller_fix_regular_match.cpp
├── database_controller_fix_regular_match.cpython-38-x86_64-linux-gnu.so
├── database_controller_fix_regular_match.pxd
├── database_controller_fix_regular_match.pyx
├── database_controller_generate_online.cpp
├── database_controller_generate_online.cpython-38-x86_64-linux-gnu.so
├── database_controller_generate_online.pxd
├── database_controller_generate_online.pyx
├── database_controller_populate.cpp
├── database_controller_populate.cpython-38-x86_64-linux-gnu.so
├── database_controller_populate.pxd
├── database_controller_populate.pyx
├── database_controller.pxd
├── database_controller_renew.cpp
├── database_controller_renew.cpython-38-x86_64-linux-gnu.so
├── database_controller_renew.pxd
├── database_controller_renew.pyx
├── database_controller_save_to_files.cpp
├── database_controller_save_to_files.cpython-38-x86_64-linux-gnu.so
├── database_controller_save_to_files.pxd
├── database_controller_save_to_files.pyx
├── database_controller_take.cpp
├── database_controller_take.cpython-38-x86_64-linux-gnu.so
├── database_controller_take.pxd
├── database_controller_take.pyx
├── database_controller_update.cpp
├── database_controller_update.cpython-38-x86_64-linux-gnu.so
├── database_controller_update.pxd
├── database_controller_update.pyx
├── database_controller_update_second.cpp
├── database_controller_update_second.cpython-38-x86_64-linux-gnu.so
├── database_controller_update_second.pxd
├── database_controller_update_second.pyx
├── database_manager.cpp
├── database_manager.cpython-38-x86_64-linux-gnu.so
├── database_manager.pxd
├── database_manager.pyx
├── database_triggers.cpp
├── database_triggers.cpython-38-x86_64-linux-gnu.so
├── database_triggers.pxd
├── database_triggers.pyx
├── db_default_lists_2.py
├── db_default_lists_3.py
├── db_default_lists_4.py
├── db_default_lists.py
├── db_extern_methods.py
├── db_extern_methods_two.py
├── extras_database_manager
│             ├── audio_blob_conversion.py
│             ├── backup_db_defaults
│             │             ├── db_default_lists_2.py
│             │             ├── db_default_lists_3.py
│             │             ├── db_default_lists_4.py
│             │             └── db_default_lists.py
│             ├── creation_files
│             │             ├── 4psu_4pco_creation_of_all_csv
│             │             │             ├── definitive_4psu_4pco_scen_ques_list
│             │             │             │             ├── final_complete_4pco_ok.csv
│             │             │             │             └── final_complete_4psu_ok.csv
│             │             │             ├── scrap_separated_csvs
│             │             │             │             ├── all_separated_csv_4pco
│             │             │             │             │             ├── blue_4pco_1_sinistra.csv
│             │             │             │             │             ├── blue_4pco_2_sinistra.csv
│             │             │             │             │             ├── blue_4pco_3_destra.csv
│             │             │             │             │             ├── blue_4pco_4_destra.csv
│             │             │             │             │             ├── blue_total_4pco_scen_ques.csv
│             │             │             │             │             ├── green_4pco_1_sinistra.csv
│             │             │             │             │             ├── green_4pco_2_sinistra.csv
│             │             │             │             │             ├── green_4pco_3_destra.csv
│             │             │             │             │             ├── green_4pco_4_destra.csv
│             │             │             │             │             ├── green_total_4pco_scen_ques.csv
│             │             │             │             │             ├── red_4pco_1_sinistra.csv
│             │             │             │             │             ├── red_4pco_2_sinistra.csv
│             │             │             │             │             ├── red_4pco_3_destra.csv
│             │             │             │             │             ├── red_4pco_4_destra.csv
│             │             │             │             │             ├── red_total_4pco_scen_ques.csv
│             │             │             │             │             ├── yellow_4pco_1_sinistra.csv
│             │             │             │             │             ├── yellow_4pco_2_sinistra.csv
│             │             │             │             │             ├── yellow_4pco_3_destra.csv
│             │             │             │             │             ├── yellow_4pco_4_destra.csv
│             │             │             │             │             └── yellow_total_4pco_scen_ques.csv
│             │             │             │             └── all_seprated_csv_4psu
│             │             │             │                 ├── lion_4psu_1_sinistra.csv
│             │             │             │                 ├── lion_4psu_2_sinistra.csv
│             │             │             │                 ├── lion_4psu_3_destra.csv
│             │             │             │                 ├── lion_4psu_4_destra.csv
│             │             │             │                 ├── lion_total_4psu.csv
│             │             │             │                 ├── monkey_4psu_1_sinistra.csv
│             │             │             │                 ├── monkey_4psu_2_sinistra.csv
│             │             │             │                 ├── monkey_4psu_3_destra.csv
│             │             │             │                 ├── monkey_4psu_4_destra.csv
│             │             │             │                 ├── monkey_total_4psu.csv
│             │             │             │                 ├── octopus_4psu_1_sinistra.csv
│             │             │             │                 ├── octopus_4psu_2_sinistra.csv
│             │             │             │                 ├── octopus_4psu_3_destra.csv
│             │             │             │                 ├── octopus_4psu_4_destra.csv
│             │             │             │                 ├── octopus_total_4psu.csv
│             │             │             │                 ├── parrot_4psu_1_sinistra.csv
│             │             │             │                 ├── parrot_4psu_2_sinistra.csv
│             │             │             │                 ├── parrot_4psu_3_destra.csv
│             │             │             │                 ├── parrot_4psu_4_destra.csv
│             │             │             │                 └── parrot_total_4psu.csv
│             │             │             └── sqlite_queries_create_4psu_4pco
│             │             │                 ├── colors_4pco_sqlite_query_to_use_blue.py
│             │             │                 ├── colors_4pco_sqlite_query_to_use_green.py
│             │             │                 ├── colors_4pco_sqlite_query_to_use_red.py
│             │             │                 ├── colors_4pco_sqlite_query_to_use_yellow.py
│             │             │                 ├── subjects_4psu_sqlite_query_to_use_lion.py
│             │             │                 ├── subjects_4psu_sqlite_query_to_use_monkey.py
│             │             │                 ├── subjects_4psu_sqlite_query_to_use_octopus.py
│             │             │                 └── subjects_4psu_sqlite_query_to_use_parrot.py
│             │             ├── create_scen_kinds_test.py
│             │             └── create_scen_types_test.py
│             ├── def_list_all_questions.csv
│             ├── def_list_all_scenarios.csv
│             ├── how_to_create_ques_scen_table.py
│             ├── kids_docs
│             │             ├── docu_prova_blob.pdf
│             │             └── image_prova_blob.jpeg
│             ├── list_scen_ques_1.csv
│             ├── list_scen_ques_2.csv
│             ├── list_scen_ques_3.csv
│             ├── list_scen_ques_4.csv
│             ├── list_scen_ques_5.csv
│             ├── list_scen_ques_6.csv
│             ├── pickle_deletions_list.pk
│             ├── pickle_first_time.pk
│             ├── pickle_game_after_change.pk
│             ├── __pycache__
│             │             └── audio_blob_conversion.cpython-38.pyc
│             │             └── audio_blob_conversion.cpython-38.pyc
│             ├── Saved
│             │         ├── kids_games_feedbacks.csv
│             │         ├── kids_matches_feedbacks.csv
│             │         ├── kids_questions_feedbacks.csv
│             │         └── kids_sessions_feedbacks.csv
│             ├── scen_kind_populate.csv
│             └── scen_type_populate.csv
│
├── modify_default_lists.cpp
├── modify_default_lists.cpython-38-x86_64-linux-gnu.so
├── modify_default_lists.pxd
├── modify_default_lists.pyx
├── oimi_queries_triggers.py
├── security_prediction.cpp
├── security_prediction.cpython-38-x86_64-linux-gnu.so
├── security_prediction.pxd
├── security_prediction.pyx
├── oimi_queries.py
├── script_clean_all_database.sh
├── README_database_manager.md
├── main.py
└── setup.py


## Summary
- mains_testing_database_manager
    - various main files for testing singular functionalities
- extra_database_manager
    - auxiliary files used during execution
- code_docs_of_database_manager
    - accurate explanation of methods and classes
1. PY
    - add_custom_pred_2.py
        * include all shared py methods regarding games execution.
    - db_default_lists.py
    - db_default_lists_2.py
    - db_default_lists_3.py
    - db_default_lists_4.py
        * exact and up-to-date copy of the database tables, with all tuples of values used for filling the database the first time or save all updates. \
        It is splitted in many files for readability. \
        It is fundamental for build again the full database in case of deletions.
    - db_extern_methods.py
        * full of simple methods called multiple times by several modules.
    - oimi_queries.py
        * with all paramount sqlite3 queries, triggers and constraints used for database creation from scratch, addition, deletions or updates.
    - setup.py
        * pivotal script for building all modules using the Distutils utility. Includes compiled setuptools extensions for Cython, passing a .pyx file.
2. PYX 
    - add_custom_prediction.pyx

    - add_custom_prediction_second_part.pyx
    - add_prediction_2.pyx
    - add_prediction_3.pyx
    - add_prediction_3_second_half.pyx
    - add_prediction_4.pyx
    - add_prediction_5.pyx
    - add_prediction.pyx
    - database_manager.pyx
    - extension_dbman.pyx
    - modify_default_list.pyx
3. PXD 
    - add_custom_prediction.pxd
    - add_custom_prediction_second_part.pxd
    - add_prediction_2.pxd
    - add_prediction_3.pxd
    - add_prediction_3_second_half.pxd
    - add_prediction_4.pxd
    - add_prediction_5.pxd
    - add_prediction.pxd
    - database_manager.pxd
    - extension_dbman.pxd
    - modify_default_list.pxd

-----------------------------------------------------------------------------------------------------------------------------------------------------------------

### Details
    Consult the complete documentation docs_database_manager for further details ---> ./code_docs_of_database_manager
    Check the project on github at <ciaopep.it>


### Recap Usage
> Show this Readme on terminal
    $ mdv README_database_manager.dm
> Get docs on terminal
    $ python db_extern_methods.py db_extern_methods_doc
    (needs this:
        if __name__ == '__main__':
            globals()[sys.argv[1]]())

    $ python db_extern_methods.py digit_selector_doc
    
    or (remove .py extensions!!!)
    $ pydoc db_extern_methods 
> Print docs on file
    $ python db_extern_methods.py db_extern_methods_doc > db_documentation.txt
    
    or 
    
    $ pandoc --pdf-engine=xelatex README_database_manager.md -o Readread.pdf
> How build and compile after modifications
    $ python setup.py build_ext --inplace 
    ...or 
    $ setta
> To get cool info about modules online in local 
    $pydoc -b db_extern_methods.py