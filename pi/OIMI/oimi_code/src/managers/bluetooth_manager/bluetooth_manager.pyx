#distutils language=c++
#cython: language_level=3
"""Bluetooth manager
"""
# ===============================================================================================================
#  Imports
# ===============================================================================================================
from bluetooth import *
import time,sys
from multiprocessing import current_process
#import oimi_logging.oimi_logger as olog
from collections import defaultdict
import subprocess
import sys
sys.path.append('/home/pi/OIMI/oimi_code/src/shared')
import shared as sh
import custom_exceptions as ce
# ===============================================================================================================
#  Objects
# ===============================================================================================================
#log = olog.logger
# ===============================================================================================================
#  Methods
# ===============================================================================================================
def setup_bluetooth():
	cdef:
		str name = "OimiServer"
		str uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
	server_socket=BluetoothSocket( RFCOMM )
	#server_socket.bind(("",PORT_ANY)) #if switch to port 2...error!!
	
	server_socket.bind(("",1))
	server_socket.listen(1)
	cdef int port = server_socket.getsockname()[1]
	blue = myBluetooth(name,server_socket,uuid,port)
	return blue
# ===============================================================================================================
#  Class
# ===============================================================================================================	
cdef class myBluetooth():
	cdef public:
		str name
		str uuid
		int port
		server_socket
		_events 
	def __cinit__(self, name, server_socket, uuid, port):
		self.name = name
		self.server_socket = server_socket
		self.uuid = uuid
		self.port = port
		self.advertise()
		self._events = defaultdict()
	# ----------------------------------------------------------------------------
	# Class Methods
	# ----------------------------------------------------------------------------		
	cdef advertise(self):
		""" Broadcasts bluetooth server signal for pairing """
		advertise_service(self.server_socket, 
			self.name, service_id = self.uuid, 
			service_classes = [ self.uuid, SERIAL_PORT_CLASS ], 
			profiles = [ SERIAL_PORT_PROFILE ],
			protocols = [ OBEX_UUID ],
		)
	
	cdef set_event(self, event):
		event.set()
	cdef clear_event(self, event):
		event.clear()
	cdef default_val(self):
		return "Not Present"
	
	cpdef create_events_dictionary(self, stopInterrupt, event_fun3, event_fun4, event_fun5, event_fun6, event_fun7, event_fun8,
									event_startFamModality, event_startGameModality, event_startTestModality, event_startInsertModality, event_feeling, 
									event_reacting):
		self._events["moving"] = stopInterrupt
		self._events["playing1"] = event_fun3
		self._events["playing2"] = event_fun4
		self._events["testing1"] = event_fun5
		self._events["testing2"] = event_fun6
		self._events["inserting1"] = event_fun7
		self._events["inserting2"] = event_fun8
		self._events["familiarization"] = event_startFamModality
		self._events["feeling"] = event_feeling
		self._events["reacting"] = event_reacting
		self._events["game"] = event_startGameModality
		self._events["experience"] = event_startTestModality
		self._events["inseriment"] = event_startInsertModality

	cdef set_specific_event(self, str name):
		print("ecco specific name to set! {}".format(name))
		self.set_event(self._events[name]) #che sarebbe global status	
	
	cdef clear_specific_event(self, str name):
		print("ecco specific name to clear! {}".format(name))
		self.clear_event(self._events[name]) #che sarebbe global status	
	
	cdef run_bluetooth(self, sub_connection, event_blueIsReady, blue_command):
		print("enter run bluetooth")
		cdef:
			bytes data_bytes = b''
			str data = ''
		while True:
			""" Waiting for client's requests, then connect to clients """
			print("Waiting for connection on RFCOMM channel number: %d" % self.port)
			#while True:
			client_socket, client_info = self.server_socket.accept()
			print("Accepted connection from ", client_info)
			try:
				while True:
					data_bytes = client_socket.recv(1024)
					if len(data_bytes) == 0: break
					print("bluetooth has received [%s]" % data_bytes)
					data = data_bytes.decode('UTF-8', 'strict')
					blue_command.put(data)
					event_blueIsReady.set()
					time.sleep(2)
					#event_blueIsReady.clear()	
					sub_connection.send(['begin_jobs'])
			except IOError:
				print("Closed connection {} {}".format(self.port,self.name))
				#log.error('System Exit raised!', exc_info=True)
				pass

	def dispatch_msgs(self, global_status, stopInterrupt, event_blueIsReady, event_startFamModality, event_startGameModality, event_startTestModality, event_startInsertModality,
						event_feeling, event_reacting, event_autoMove, event_folloMove, event_patternMove, event_Body, event_fun3, event_fun4, event_fun5, event_fun6, event_fun7, event_fun8,
						blue_command, fami_option, game_option, expe_option, insert_option, primary_lock, queue_lock, current_modality):
		""" Launches proper method, according to blueetooth message"""
		cdef bint onlyone = True
		print("enter in disparch msg ble")
		while True:
			print("Bluetooth is listening")
			event_blueIsReady.wait()
			print("ok bluetooth event is ready ! ")
			event_blueIsReady.clear()	
			print("Here ther order for Oimi:")
			command = blue_command.get()
			print("The command is {}".format(command))
			#0
			if command == 'quit': #exit from ALL also quit from main 
				glo = global_status.get()
				global_status.put(glo)
				print(glo.get_globalstatus())
				if glo.get_globalstatus() != 'doing_nothing' and glo.get_globalstatus() != 'beginning':
					if not queue_lock.empty(): #fundamental for not getting stuck in if or queue get
						q = queue_lock.get()
						if q:
							print("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]stop also Lock that is active")
							stopInterrupt.set()				
							glo = global_status.get()
							print("The global_status is {}".format(glo.get_globalstatus()))
							self.set_specific_event(glo.get_globalstatus()) #settare i singoli eventi fun equivale a fermare...
							fami_option.put('dont_move')
							game_option.put(60)
							expe_option.put(60)
							insert_option.put(60)
							#########Next instruction is dangerous since if the current_modality is empty the subprocess get stuck
							#mode_event = current_modality.get()
							#self.clear_specific_event(mode_event)
							current_modality.clear()
							#########Next instructions can be done but it useless to close everything 
							#event_startFamModality.clear()
							#event_startGameModality.clear()
							#event_startTestModality.clear()
							#event_startInsertModality.clear()
							print("DONE WITH ALL STUFF TO CLOSE")
							qu = queue_lock.get()
							while qu:
								print("waiting for quitting safely...")
					else:
						glo = global_status.get()
						print("The global_status is {}".format(glo.get_globalstatus()))
						self.set_specific_event(glo.get_globalstatus())
						fami_option.put('dont_move')
						game_option.put(60)
						expe_option.put(60)
						insert_option.put(60)
						#mode_event = current_modality.get()
						#self.clear_specific_event(mode_event)
						current_modality.clear()
						#event_fun3.set()
						#event_fun4.set()
				else: 
					print("I'm doing nothing! No active process to quit.")
				print("I'm quitting the main program!")
				subprocess.call("kill $(ps -aux | grep 'main_oimi.py' | awk '{print $2}')", shell=True)
			#1
			elif command == 'hard_stop_exit': #stop nomatter what, exit also from modality, even if there s a primary_lock 
				glo = global_status.get()
				global_status.put(glo)
				print(glo.get_globalstatus())
				if glo.get_globalstatus() != 'doing_nothing' and glo.get_globalstatus() != 'beginning':
					if not queue_lock.empty():
						q = queue_lock.get()
						if q:
							print("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]stop also Lock that is active")
							stopInterrupt.set()				
						glo = global_status.get()
						print("glo all interno di quit è {}".format(glo.get_globalstatus()))
						self.set_specific_event(glo.get_globalstatus())
						fami_option.put('dont_move')
						game_option.put(60)
						expe_option.put(60)
						insert_option.put(60)
						#if not current_modality.empty(): #otherwise get stuck here trying to get empty 
						#	mode_event = current_modality.get()
						#	rint("mode event to clear   ",mode_event)
						#self.clear_specific_event()
						current_modality.clear()
						#event_startFamModality.clear()
						#event_startGameModality.clear()
						#event_startTestModality.clear()
						#event_startInsertModality.clear()
						#stopInterrupt.set() #primary_lock event
						print("Hard unstoppable stop confirmed")
					else:
						glo = global_status.get()
						print("glo all interno di quit è {}".format(glo.get_globalstatus()))
						self.set_specific_event(glo.get_globalstatus()) 
						fami_option.put('dont_move')
						game_option.put(60)
						expe_option.put(60)
						insert_option.put(60)
						if glo.get_globalstatus() in ['moving', 'feeling', 'reacting']:
							print("ok glo = familiarization!!")
						#	mode_event = current_modality.get()
						#	print("mode event to clear   ",mode_event)
							self.clear_specific_event('familiarization')
						if glo.get_globalstatus() in ['playing1','playing2']:
							print("ok glo = playing!!")
						#	mode_event = current_modality.get()
						#	print("mode event to clear   ",mode_event)
							self.clear_specific_event('game')
						current_modality.clear()
						#game_option.put(60)
						#event_fun3.set()
						#event_fun4.set()
					status = sh.PyGlobalStatus(b'doing_nothing')
					print("status is {}".format(status.get_globalstatus()))
					global_status.put(status)
				else:
					print("already stopped doing nothing!")
					print("current modality is {}".format(current_modality))
					#if not current_modality.empty():
					#	print("current_modality not empty!!")
					#	mode_event = current_modality.get()
					#	print("modality is   ", mode_event)
					#	self.clear_specific_event(mode_event)
					glo = global_status.get()
					print("status in doing_nothing--- ", glo.get_globalstatus())
					if glo.get_globalstatus() in ['moving', 'feeling', 'reacting']:
						self.clear_specific_event('familiarization')
					elif glo.get_globalstatus() in ['playing1','playing2']:
						self.clear_specific_event('game')
					#if not  current_modality.empty():
					#	mode_event = current_modality.get()
					#	print("modality after is   ", mode_event)
					current_modality.clear()
					
			#2
			elif command == 'soft_stop_exit': #stop gracefully, even if there s a log and also exit from modality
				""" qui do per scontato che ci sia il lock ogni volta che c'è un audio"""
				print("softtt stop exitttt!")
				glo = global_status.get()
				global_status.put(glo)
				print(glo.get_globalstatus())
				if glo.get_globalstatus() != 'doing_nothing' and glo.get_globalstatus() != 'beginning':
					if not queue_lock.empty(): #fondamentale per nn fermarsi dentro if q o queue get ...oppure se metto no wait??
						q = queue_lock.get()
						if q:
							print(" q is True so a]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]stop also Lock that is active, but gracefully")
							while True:
								script_output = subprocess.check_output("./check_if_audio_is_running.sh '%s'", shell=True)
								print("script_output is {}".format(script_output))
								std_output = script_output.decode().rstrip()
								print("STD_output is {}".format(std_output))
								try:
									if std_output != 'audio player is still running':
										#self.set_specific_event(global_status) #se esco da modalituy non mi serve vero?
										fami_option.put('dont_move')
										game_option.put(60)
										expe_option.put(60)
										insert_option.put(60)
										#mode_event = current_modality.get()
										#self.clear_specific_event(mode_event)
										current_modality.clear()
										#event_startFamModality.clear()
										#event_startGameModality.clear()
										#event_startTestModality.clear()
										#event_startInsertModality.clear()
										stopInterrupt.set()
										glo = global_status.get()
										self.set_specific_event(glo.get_globalstatus())
										break
									else:
										raise ce.BlueException
								except ce.BlueException:
									print("blue exception raised!!!")
									continue			
					else:
						#self.set_specific_event(global_status) #se esco da modalituy non mi serve vero?
						fami_option.put('dont_move')
						game_option.put(60)
						expe_option.put(60)
						insert_option.put(60)
						#mode_event = current_modality.get()
						#self.clear_specific_event(mode_event)
						current_modality.clear()
						glo = global_status.get()
						self.set_specific_event(glo.get_globalstatus())
						#event_startFamModality.clear()
						#event_startGameModality.clear()
						#event_startTestModality.clear()
						#event_startInsertModality.clear()
						status = sh.PyGlobalStatus(b'doing_nothing')
						print("status is {}".format(status.get_globalstatus()))
						global_status.put(status)
						#game_option.put(60)
						#event_fun3.set()
						#event_fun4.set()
					status = sh.PyGlobalStatus(b'doing_nothing')
					print("status is {}".format(status.get_globalstatus()))
					global_status.put(status)

				else:
					print("already stopped doing soft stop nothing!")
					print("current modality is {}".format(current_modality))
					#mode_event = current_modality.get()
					#print("mode event to clear   ",mode_event)
					#self.clear_specific_event(mode_event)
					current_modality.clear()
					#if not  current_modality.empty():
					#	mode_event = current_modality.get()
					#	print("modality after is   ", mode_event)
			#3		
			elif command == 'hard_stop': #stop nomatter what, dont exit from modality
				#glo = global_status.get()
				#self.set_specific_event(glo.get_globalstatus())
				#print("entro inside inside in stop blueee")
				glo = global_status.get()
				print("global status di hard_Stop eccolo {} ".format(glo.get_globalstatus()))
				global_status.put(glo)
				if glo.get_globalstatus() != 'doing_nothing' or glo.get_globalstatus() != 'beginning':
					print("hard stoppppp ok enter in globalstatus correctly")
					#non esco anche dalla modalità, posso invocare ogni modality, devo rimettere Ok senno fun1 non andrà
					#mi serve sicuramente uscire dai singoli eventi essendoci il while true che rompe
					if not queue_lock.empty(): #fondamentale per nn fermarsi dentro if q o queue get ...oppure se metto no wait??
						print("entro TRUE  di final stop --- lock is not EMPTYYYY")
						q = queue_lock.get()
						print("Q of queue is {}".format(q))
						if q:
							print(" q is True so a]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]stop also Lock that is active")
							stopInterrupt.set()
						#per moving is simply stopInterrupt l'evento di fun specifico, ma se commento le prossime due righe fa piu fatica la seconda volta a fermarsi
						glo = global_status.get()
						print("gloglo   ", glo)
						self.set_specific_event(glo.get_globalstatus())
						fami_option.put('dont_move')
						game_option.put(60)
						expe_option.put(60)
						insert_option.put(60)
						#if glo.get_globalstatus() in ['moving']:
						#	print("ok glo = playing!!")
						#	mode_event = current_modality.get()
						#	print("mode event to clear   ",mode_event)
						#	self.clear_specific_event('familirization')						
						print("!!!!!!!!!!!!!!!!!!hard stop confirmed!!!!!!!!!!!!!!!!!!!!!")
					else:
						print("entro nell else di final stop")
						print("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[")
						glo = global_status.get()
						print("gloglo   ", glo)
						self.set_specific_event(glo.get_globalstatus())
						fami_option.put('dont_move')
						game_option.put(60)
						expe_option.put(60)
						insert_option.put(60)
						#if glo.get_globalstatus() in ['moving']:
						#	print("ok glo = playing!!")
						#	mode_event = current_modality.get()
						#	print("mode event to clear   ",mode_event)
						#	self.clear_specific_event('familirization')						
						#game_option.put(60)
						#event_fun3.set()
						#event_fun4.set()
					status = sh.PyGlobalStatus(b'doing_nothing')
					print("status is {}".format(status.get_globalstatus()))
					global_status.put(status)
				
				else: print("already stopped doing nothing!")
			#4
			elif command == 'soft_stop': #stop gracefully dont exit from modality
				print("softtt stoppppp")
				# non esco da modality rimango in quella corrente per ogni job, non devo rimettere Ok per far andare fun1
				# cerca evento in corso e pulisco i suoi eventi attivi e le code attive ma non l'evento "start"! 
				# se sta ancora andando una fun...devo settare evento delle fun
				# con questi funziona al contrario che vanno settati anzichè chiusi

				# nn so se il processo che vado a stoppare c'è il primary_lock o meno ,,,controllo e distinguo
				# get / read / put again
				glo = global_status.get()
				print(glo.get_globalstatus())
				global_status.put(glo)
				if glo.get_globalstatus() != 'doing_nothing' or glo.get_globalstatus() != 'beginning':
					print("POLITECNICO MILANO SABATO 5 DICEMBRE")
					if not queue_lock.empty(): #fondamentale per nn fermarsi dentro if q o queue get ...oppure se metto no wait??
						q = queue_lock.get()
						print("lock is not EMPTYYYYYYYYYYYYYYYY")
						print("Q IS {}". format(q))
						if q:
							print("stop also Lock that is active but graecefully")
							while True:
								script_output = subprocess.check_output("./check_if_audio_is_running.sh '%s'", shell=True)
								std_output = script_output.decode().rstrip()
								print("try try try inside soft_stop moving")
								if std_output == 'pygame audio is running':
									continue
								stopInterrupt.set()
								glo = global_status.get()
								print("again global status = ", glo.get_globalstatus())
								self.set_specific_event(glo.get_globalstatus())
								#if glo.get_globalstatus() in ['moving']:
								#	print("ok glo = playing!!")
								#	mode_event = current_modality.get()
								#	print("mode event to clear   ",mode_event)
								#	self.clear_specific_event('familirization')										
								fami_option.put('dont_move')
								game_option.put(60)
								expe_option.put(60)
								insert_option.put(60)
								print("soft normal con q True stop confirmed")
								break
					else:
						print("Lock is VERYYYYYYYYYYYYYYY empty")
						#game_option.put(60)
						#event_fun3.set()
						#event_fun4.set()
						glo = global_status.get()
						print("IN ELSE!!!!!!!!! the STADIO IS {}".format(glo.get_globalstatus()))
						self.set_specific_event(glo.get_globalstatus())
						#if glo.get_globalstatus() in ['moving']:
						#	print("ok glo = playing!!")
						#	mode_event = current_modality.get()
						#	print("mode event to clear   ",mode_event)
						#	self.clear_specific_event('familirization')							
						fami_option.put('dont_move')
						game_option.put(60)
						expe_option.put(60)
						insert_option.put(60)
					status = sh.PyGlobalStatus(b'doing_nothing')
					print("status is {}".format(status.get_globalstatus()))
					global_status.put(status)
					print("finally soft stop confirmed")
				else: print("---> already stopped doing nothing!")
			elif command=='fmode':
				if current_modality.empty():
					stopInterrupt.clear()
					event_startFamModality.set()
					print("----------family modality founded")
				else: print("there s another modality in execution")
			
			elif command=='gmode':
				if not current_modality.empty():
					print("current_mod not empty")	
					mode_event = current_modality.get()
					print("perché non setto current modality che è    ",mode_event)
					print("there s another modality in execution")
				else:
					#stopInterrupt.clear()
					print("currrent_mode_empy")
					event_startGameModality.set()
					print("game modality founded")
			
			elif command=='emode':
				if current_modality.empty():
					stopInterrupt.clear()
					event_startTestModality.set()
					print("test modality founded")
				else: print("there s another modality in execution")
			
			elif command=='imode':
				if current_modality.empty():
					stopInterrupt.clear()
					event_startInsertModality.set()
					print("insert modality founded")
				else: print("there s another modality in execution")
			
			elif command=='feel':
				print("FEEEL FEEEL ")
				stopInterrupt.clear() #if is already clear doesn t change nor set
				#event_autoMove.clear()
				if event_startFamModality.is_set():
					#event_folloMove.set()
					#event_patternMove.set()
					print("I have to show my feelings")
					fami_option.put('feel')
			elif command=='approachme':
				stopInterrupt.clear() #if is already clear doesn t change nor set
				#event_autoMove.clear()
				if event_startFamModality.is_set():
					#event_folloMove.set()
					#event_patternMove.set()
					print("entro in approachme approachme")
					fami_option.put('approachme')
			elif command=='acquire_touch':
				stopInterrupt.clear() #if is already clear doesn t change nor set
				#event_autoMove.clear()
				if event_startFamModality.is_set():
					#event_folloMove.set()
					#event_patternMove.set()
					print("entro in touacquire_touch dataset composition")
					fami_option.put('acquire_touch')
			elif command=='touch':
				print("touch touch ")
				stopInterrupt.clear() #if is already clear doesn t change nor set
				#event_autoMove.clear()
				if event_startFamModality.is_set():
					#event_folloMove.set()
					#event_patternMove.set()
					print("entro in touchme gameeeeeeeeeeeeeeeeee")
					fami_option.put('touch')
			elif command=='react':
				print("REACT REACT ")
				stopInterrupt.clear() #if is already clear doesn t change nor set
				#event_autoMove.clear()
				if event_startFamModality.is_set():
					#event_folloMove.set()
					#event_patternMove.set()
					print("I must react")
					fami_option.put('react')
			elif command=='go_auto':
				print("GO GO GO GO GO GO GO GO GO GO GO GO GO GO GO GO GORANNNNNNNNNNNNNNNNNNNNNN")
				stopInterrupt.clear() #if is already clear doesn t change nor set
				#event_autoMove.clear()
				if event_startFamModality.is_set():
					#event_folloMove.set()
					#event_patternMove.set()
					print("go auto founded")
					fami_option.put('auto_move')
			elif command=='go_follo':
				stopInterrupt.clear() #if is already clear deosn t change nor set
				#event_folloMove.clear()
				if event_startFamModality.is_set():	
					print("go follo founded")
					#event_autoMove.set()
					#event_patternMove.set()
					fami_option.put('follo_move')
			#non metto gopatt ma metto direttamente sad,happy etc etc 
			elif command=='driveme':
				stopInterrupt.clear() #if is already clear deosn t change nor set
				#event_folloMove.clear()
				if event_startFamModality.is_set():	
					print("driveme founded")
					#event_autoMove.set()
					#event_patternMove.set()
					fami_option.put('driveme')
			elif command=='joystickme':
				stopInterrupt.clear() #if is already clear deosn t change nor set
				#event_folloMove.clear()
				if event_startFamModality.is_set():	
					print("go follo founded")
					#event_autoMove.set()
					#event_patternMove.set()
					fami_option.put('joystickme')
			elif command=='go_patt':
				print("sono appena appena entrato in gopatt")
				#stopInterrupt.clear() #if is already clear doesn t change nor set
				####event_patternMove.clear()
				if event_startFamModality.is_set():	
					print("go patt is founded ")
					#event_autoMove.set()
					#event_folloMove.set()
					fami_option.put('pattern_move')
			elif command=='fun3':
				print("enter in fun3 of bluetooth")
				event_fun3.clear()
				if event_startGameModality.is_set():
					event_fun4.set()
					print("ECCO FUN3")
					game_option.put(20)
				else : print("event unfortunately")
	
			elif command=='fun4':
				print("enter in fun4 of bluett")
				event_fun4.clear()
				if event_startGameModality.is_set():
					event_fun3.set()
					print("ECCO FUN4")
					game_option.put(22)
				else : print("event unfortunately")
			
			elif command=='fun5':
				event_fun5.clear()
				if event_startTestModality.is_set():
					event_fun6.set()
					print("ECCO FUN6")
					game_option.put(30)
			
			elif command=='fun6':
				event_fun6.clear()
				if event_startTestModality.is_set():
					event_fun5.set()
					print("ECCO FUN6")
					game_option.put(32)
			
			elif command=='fun7':
				event_fun7.clear()
				if event_startInsertModality.is_set():
					event_fun8.set()
					print("ECCO FUN7")
					game_option.put(40)
			
			elif command=='fun8':
				event_fun8.clear()
				if event_startInsertModality.is_set():
					event_fun7.set()
					print("ECCO FUN8")
					game_option.put(42)
			'''elif command == 'soft_stop': #stop gracefully dont exit from modality
				print("softtt stoppppp")
				# non esco da modality rimango in quella corrente per ogni job, non devo rimettere Ok per far andare fun1
				# cerca evento in corso e pulisco i suoi eventi attivi e le code attive ma non l'evento "start"! 
				# se sta ancora andando una fun...devo settare evento delle fun
				# con questi funziona al contrario che vanno settati anzichè chiusi

				# nn so se il processo che vado a stoppare c'è il primary_lock o meno ,,,controllo e distinguo
				glo = global_status.get()
				print(glo.get_globalstatus())
				global_status.put(glo)
				if glo.get_globalstatus() != 'doing_nothing' or glo.get_globalstatus() != 'beginning':
					print("POLITECNICO MILANO SABATO 5 DICEMBRE")
					if not queue_lock.empty(): #fondamentale per nn fermarsi dentro if q o queue get ...oppure se metto no wait??
						q = queue_lock.get()
						print("lock is not EMPTYYYYYYYYYYYYYYYY")
						print("Q IS {}". format(q))
						if q:
							print("stop also Lock that is active but graecefully")
							while True:
								script_output = subprocess.check_output("./oimiscript.sh '%s'", shell=True)
								std_output = script_output.decode().rstrip()
								try:
									print("try try try inside soft_stop moving")
									if std_output != 'omxplayer is running':
										fami_option.put('dont_move')
										game_option.put(60)
										expe_option.put(60)
										insert_option.put(60)
										stopInterrupt.set()
										glo = global_status.get()
										self.set_specific_event(glo.get_globalstatus())
										break
									else:
										raise ce.BlueException
								except ce.BlueException:
									if onlyone:
										print("blue exception raised!!!")
										onlyone = False
									continue				
							print("soft normal con q True stop confirmed")
					else:
						print("Lock is VERYYYYYYYYYYYYYYY empty")
						#game_option.put(60)
						#event_fun3.set()
						#event_fun4.set()
						glo = global_status.get()
						print("IN ELSE!!!!!!!!! the STADIO IS {}".format(glo.get_globalstatus()))
						self.set_specific_event(glo.get_globalstatus())
						fami_option.put('dont_move')
						game_option.put(60)
						expe_option.put(60)
						insert_option.put(60)
					status = sh.PyGlobalStatus(b'doing_nothing')
					print("status is {}".format(status.get_globalstatus()))
					global_status.put(status)
					print("finally soft stop confirmed")
				else: print("---> already stopped doing nothing!")'''

			'''elif command=='go_pattern':
				event_folloMove.clear()
				if event_startFamModality.is_set():	
					print("FUN2 founded")
					fami_option.put('follo_move')
					event_autoMove.set()
			elif command=='body':
				event_folloMove.clear()
				if event_startFamModality.is_set():	
					print("FUN2 founded")
					fami_option.put('follo_move')
					event_autoMove.set()'''
	# ----------------------------------------------------------------------------
	# Wrappers
	# ----------------------------------------------------------------------------
	def connect_bluetooth(self, sub_connection, event_blueIsReady, blue_command):
		self.run_bluetooth(sub_connection, event_blueIsReady, blue_command)
