# ===================================== # ===================================== #
# External Imports
# ===================================== # ===================================== #
cdef extern from "Python.h":
	object PyList(float *s, Py_ssize_t leng)

cdef extern from "c_functions.h":
	char* compareMsg(char * name)
	char * createMsg(char * comm, float strafe, float forward, float angular)
# ===============================================================================================================
#  Class:
# ===============================================================================================================	 
cdef class SerialManager:
	cdef:
		str name
		int start_marker  
		int end_marker
		ser
	# -----------------------------------------------------------------------------------------------------------
	# Basic
	# -----------------------------------------------------------------------------------------------------------
	cdef send_to_arduino(self, char* sendString)
	cdef char* receive_from_arduino(self)
	cdef wait_arduino(self)
	cdef wait_arduino_special(self)
	cdef wait_arduino_nano1(self)
	cdef wait_arduino_nano2(self)
	cdef wait_arduino_mega(self)
	cdef wait_end_of_a_reaction(self)
	cpdef close(self)
	cpdef inWaiting(self)
	cpdef transmit(self, char * command)
	cpdef transmit_with_no_answer(self, char * command)
	cpdef transmit_until_its_ok(self, char * command)
	cpdef transmit_until_its_ok_NUOVO(self, char * command)
	cdef send_led_animation_tentiamo(self, char* led_comm)
	cpdef get_dataReceived(self)
	# -----------------------------------------------------------------------------------------------------------
	# Complete
	# -----------------------------------------------------------------------------------------------------------
	cdef char* pickup_fromMega_confirm(self)
	cdef char* pickup_fromNano1_confirm(self)
	# -----------------------------------------------------------------------------------------------------------
	# Control
	# -----------------------------------------------------------------------------------------------------------
	cdef tell_to_reset(self)
	cdef tell_to_switch(self,val)
	cdef tell_to_pause(self)
	cpdef flushInput(self)
	cpdef flushOutput(self)
	# -----------------------------------------------------------------------------------------------------------
	# Reactions
	# ----------------------------------------------------------------------------------------------------------- 
	cdef tell_to_react(self, command)
	cdef emotion_reaction(self,move_comm, led_col, animation, for_speed, ang_speed)
	cdef start_reaction(self)
	cdef send_led_animation(self, char* led_comm)
	cdef csendledcommand(self, int conferma)
	cdef csendSpecificLED(self, str pattern_and_color)
	cdef send_led_animation_while_moving(self, char* led_comm)
	cdef stop_led(self)
	# -----------------------------------------------------------------------------------------------------------
	# Pressures Arduino nano_2
	# -----------------------------------------------------------------------------------------------------------	
	cdef take_front_pressure(self)
	cdef take_one_scan_pressure(self, float mp0, float mp1, float mp2, float mp3, float mp4)
	
	cpdef take_proper_msg_body_touch_reaction(self, perceived)
	
	cpdef take_10_pressures(self)
	#cpdef take_pressures_forever(self, event_continue_take_press, event_continue_take_press22222222, lifo_queue)
	cpdef take_pressures_forever_vecchia(self, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press)
	cpdef take_pressures_forever(self, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_queue)
	cpdef take_10_pressures_and_mpus(self, mpu1_left, mpu2_right)
	cpdef wait_pressurestop_msg(self)
	# -----------------------------------------------------------------------------------------------------------
	# Navigation
	# ----------------------------------------------------------------------------------------------------------- 
	cdef tell_to_stop(self)
	cdef movement_command(self, command)
	cdef tell_to_move(self, command)
	cdef tell_to_move2(self, command)
	cdef tell_to_move_reaction(self, command)

	cpdef sendreset(self)
	cpdef open(self)
	
	cpdef takeoneMpxScan(self)
	

	cdef cmon_go(self,val)
	cdef cprova1(self)
	cdef cgamepad(self, char* command, float strafe, float forward, float angular)