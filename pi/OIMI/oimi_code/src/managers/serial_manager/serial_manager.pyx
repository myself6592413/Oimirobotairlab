""" Serial Manager responsible of controlling the serial communication with the three Arduinos """
# ===============================================================================================================
# Cython directives
# ===============================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ===============================================================================================================
#  Imports:
# ===============================================================================================================
import sys
if '/home/pi/OIMI/oimi_code/src/managers/interrupts_manager/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/interrupts_manager/')    
if '/home/pi/OIMI/oimi_code/src/shared/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/shared/')
if '/home/pi/OIMI/oimi_code/src/managers/serial_manager/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/serial_manager/')
if '/home/pi/OIMI/oimi_code/src/managers/playing/free_games/touchMe_entertainment_body_contact/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/playing/free_games/touchMe_entertainment_body_contact/')
import re
import time
from random import randint
import serial
import signal
import math
import custom_exceptions as ce
import shared as shar
cimport serial_manager as serr
import body_create_sample as bcs
from subprocess import run
from posix.time cimport clock_gettime, timespec, CLOCK_REALTIME
from press_support import rebuild
from libc.stdlib cimport malloc, free
from libc.string cimport strcpy, strlen

cdef extern from "Python.h":
    object PyList(float *s, Py_ssize_t leng)
# ===============================================================================================================
#  Elements:
# ===============================================================================================================
signal.signal(signal.SIGALRM, ce.time_handler)
signal.alarm(5)
# ===============================================================================================================
#  Methods:
# ===============================================================================================================
def setup_serial():
    cdef:
        str name1 = "MegaSerial"
        str name2 = "Nano1Serial"
        str name3 = "Nano2Serial"
        str serial_port1 = "/dev/ttyACM0"
        str serial_port2 = "/dev/ttyUSB1"
        str serial_port3 = "/dev/ttyUSB0"
        int baud_rate = 115200
        int start_marker = 60
        int end_marker = 62
    ser_m = serial.Serial(serial_port1, baud_rate, timeout=1)
    ser_n1 = serial.Serial(serial_port2, baud_rate, timeout=1)    
    ser_n2 = serial.Serial(serial_port3, baud_rate, timeout=1)    
    sermega = SerialManager(name1, start_marker, end_marker, ser_m)
    sernano1 = SerialManager(name2, start_marker, end_marker, ser_n1)
    sernano2 = SerialManager(name3, start_marker, end_marker, ser_n2)
    return sermega, sernano1, sernano2

def setup_seria_mega():
    cdef:
        str name1 = "MegaSerial"
        str serial_port1 = "/dev/ttyACM0"
        int baud_rate = 115200
        int start_marker = 60
        int end_marker = 62
    ser_m = serial.Serial(serial_port1, baud_rate, timeout=1)
    sermega = SerialManager(name1, start_marker, end_marker, ser_m)
    return sermega

def setup_serial_nano1():
    cdef:
        str name2 = "Nano1Serial"
        str serial_port2 = "/dev/ttyUSB1"
        int baud_rate = 115200
        int start_marker = 60
        int end_marker = 62
    ser_n1 = serial.Serial(serial_port2, baud_rate, timeout=1)    
    sernano1 = SerialManager(name2, start_marker, end_marker, ser_n1)
    return sernano1

def setup_serial_nano2():
    cdef:
        str name3 = "Nano2Serial"
        str serial_port3 = "/dev/ttyUSB0"
        int baud_rate = 115200
        int start_marker = 60
        int end_marker = 62
    ser_n2 = serial.Serial(serial_port3, baud_rate, timeout=1)    
    sernano2 = SerialManager(name3, start_marker, end_marker, ser_n2)
    return sernano2    
# ===============================================================================================================
#  Class:
# ===============================================================================================================
cdef class SerialManager:
    #left as example, if not putted into .pxd files
    #cdef public:
    #    str name
    #    int start_marker  
    #    int end_marker
    #    ser
    def __cinit__(self, str name, int start_marker, int end_marker, ser):
        self.name = name
        self.start_marker = start_marker
        self.end_marker = end_marker
        self.ser = ser

    def __reduce__(self): 
        '''for handling with success reduction when dumping or call object from Pool --> thanks to simply rebuild method in press_support_reduction module'''
        return rebuild, (self.name, self.start_marker, self.end_marker, self.ser,)
    # -----------------------------------------------------------------------------------------------------------
    # Attributes getters,setters 
    # -----------------------------------------------------------------------------------------------------------
    @property
    def name(self):
        return self.name
    @name.setter
    def name(self, value):
        self.name = value    
    @property
    def start_marker(self):
        return self.start_marker
    @start_marker.setter
    def start_marker(self, value):
        self.start_marker = value
    @property
    def end_marker(self):
        return self.end_marker
    @end_marker.setter
    def end_marker(self, value):
        self.end_marker = value                    
    # -----------------------------------------------------------------------------------------------------------
    # Basic
    # -----------------------------------------------------------------------------------------------------------
    cdef send_to_arduino(self, char* sendString):
        """ Writes a command (the given string) on serial """
        self.ser.write(sendString)
        
    cdef char* receive_from_arduino(self):
        """ Reads on serial the correct string inside <> special characters. 
        The first element contains the number of bytes that the Arduino said it included in
        msg, for checking that the full msg was received. 
        The second element contains the msg as a string. 
        Waiting until the start marker is detected saving all subsequent bytes until the end marker is detected. 
        """        
        cdef char* z = 'z'                        # any value different from < or >                           
        cdef char* ch = ''                        # empty msg at the beginning
        cdef bytes put = z                        
        cha = ch.decode('UTF-8', 'strict')        # converts from bytes to string (encoding argument, error type)
        try:
            while ord(put) = self.start_marker:    
                put = self.ser.read()                
            while ord(put) = self.end_marker:        # saves data until the end marker is found
                if ord(put) = self.start_marker:
                    val = put.decode('UTF-8', 'strict') 
                    cha = '{}{}'.format(cha,val)
                put = self.ser.read() 
        except TypeError:
            print("type error from receive arduino basic")
            pass
        cc = cha.encode('UTF-8', 'strict')        # returns a bytes representation  
        return cc

    cdef wait_arduino_nano1(self):
        """ Ensures connection with Arduino Nano_1 is fine.
        Provides that all bytes left over from a previous message are discarded. 
        """            
        cdef char* msg = ''
        cdef char* right_msg = 'Nano1 is ready'
        cdef timespec ts
        cdef double current_t
        cdef double first_current_t
        cdef int i = 0          
        while msg.find(right_msg) == -1:
            while self.ser.inWaiting() == 0:  #if number of characters ready to be read
                #pass
                #print("giro i ", i)
                #print()
                clock_gettime(CLOCK_REALTIME, &ts)
                current_t = ts.tv_sec + (ts.tv_nsec / 1000000000.)
                #current_t = (ts.tv_sec + ts.tv_nsec) / 1000000000.
                current_t_ok = math.trunc(current_t)
                #print("current_t current_t ", current_t)
                #print("current_t current_t ", current_t_ok)
                if i == 0: 
                    first_current_t = current_t_ok
                    print("first_current_t {}".format(first_current_t))
                else:
                    if current_t_ok - first_current_t > 1:
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        self.sendreset()
                        break
                i = i + 1 
            msg = self.receive_from_arduino()
            print(msg)

    cdef wait_arduino_nano2(self):
        """ Ensures connection with Arduino Nano_2 is fine.
        Provides that all bytes left over from a previous message are discarded. 
        """            
        cdef char* msg = ''
        cdef char* right_msg = 'Nano2 is ready'
        cdef timespec ts
        cdef double current_t
        cdef double first_current_t
        cdef int i = 0          
        while msg.find(right_msg) == -1:
            while self.ser.inWaiting() == 0:  #if number of characters on the serial buffer is more than zero, the guffer is ready to be read
                #pass
                #print("giro i ", i)
                #print()
                clock_gettime(CLOCK_REALTIME, &ts)
                current_t = ts.tv_sec + (ts.tv_nsec / 1000000000.)
                #current_t = (ts.tv_sec + ts.tv_nsec) / 1000000000.
                current_t_ok = math.trunc(current_t)
                #print("current_t current_t ", current_t)
                #print("current_t current_t ", current_t_ok)
                if i == 0: 
                    first_current_t = current_t_ok
                    print("first_current_t {}".format(first_current_t))
                else:
                    if current_t_ok - first_current_t > 1:
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        self.sendreset()
                        break
                i = i + 1 
            msg = self.receive_from_arduino()
            print(msg)

    cdef wait_arduino_mega(self):
        """ Ensures connection with Arduino Mega is fine. 
        Provides that all bytes left over from a previous message are discarded. 
        """ 
        cdef char* msg = ''
        cdef char* right_msg = 'Mega is ready'
        cdef timespec ts
        cdef double current_t
        cdef double first_current_t
        cdef int i = 0
        while msg.find(right_msg) == -1:
            #pass #old
            while self.ser.inWaiting() == 0:
                #print("giro i ", i)
                #print()
                clock_gettime(CLOCK_REALTIME, &ts)
                current_t = ts.tv_sec + (ts.tv_nsec / 1000000000.)
                #current_t = (ts.tv_sec + ts.tv_nsec) / 1000000000.
                current_t_ok = math.trunc(current_t)
                #print("current_t current_t ", current_t)
                #print("current_t current_t ", current_t_ok)
                if i == 0: 
                    first_current_t = current_t_ok
                    print("first_current_t {}".format(first_current_t))
                else:
                    if current_t_ok - first_current_t > 1:
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        print("enter in the case")
                        self.sendreset()
                        break
                i = i + 1 
            msg = self.receive_from_arduino()
            print(msg)

    cdef wait_arduino(self):
        """ Ensures connection with Arduino Mega is fine. 
        Provides that all bytes left over from a previous message are discarded. 
        """ 
        cdef char* msg = ''
        cdef char* right_msg
        cdef timespec ts
        cdef double current_t
        cdef double first_current_t
        cdef int i = 0
        if self.name=='MegaSerial':
            right_msg = 'Mega is ready'
        elif self.name=='Nano1Serial':
            right_msg = 'Nano1 is ready'
        elif self.name=='Nano2Serial':
            right_msg = 'Nano2 is ready'

        while msg.find(right_msg) == -1:
            print("right_msg expected is =   ", right_msg)
            #pass #old
            while self.ser.inWaiting() == 0:
                #print("giro i ", i)
                #print()
                clock_gettime(CLOCK_REALTIME, &ts)
                current_t = ts.tv_sec + (ts.tv_nsec / 1000000000.)
                #current_t = (ts.tv_sec + ts.tv_nsec) / 1000000000.
                current_t_ok = math.trunc(current_t)
                #print("current_t current_t ", current_t)
                #print("current_t current_t ", current_t_ok)
                if i == 0: 
                    first_current_t = current_t_ok
                    print("first_current_t {}".format(first_current_t))
                else:
                    if current_t_ok - first_current_t > 1.5:
                        print("too much time has passed, enter in the case we need to send a reset to blocked process")
                        print("proceeding with the reset of the following board = {}".format(self.name))
                        self.sendreset()
                        break #need the while not if
                i = i + 1 
            msg = self.receive_from_arduino()
            print(msg)

    cdef wait_arduino_special(self):
        """ Ensures connection with Arduino Mega is fine. 
        Provides that all bytes left over from a previous message are discarded. 
        """ 
        cdef char* msg = ''
        cdef char* right_msg
        cdef timespec ts
        cdef double current_t
        cdef double first_current_t
        cdef int i = 0
        if    self.name=='MegaSerial':
            right_msg = 'Mega is ready'
        elif  self.name=='Nano1Serial':
            right_msg = 'Nano1 is ready'
        elif  self.name=='Nano2Serial':
            right_msg = 'Nano2 is ready'

        while msg.find(right_msg) == -1:
            #pass #old
            while self.ser.inWaiting() == 0:
                #print("giro i ", i)
                #print()
                clock_gettime(CLOCK_REALTIME, &ts)
                current_t = ts.tv_sec + (ts.tv_nsec / 1000000000.)
                #current_t = (ts.tv_sec + ts.tv_nsec) / 1000000000.
                current_t_ok = math.trunc(current_t)
                #print("current_t current_t ", current_t)
                #print("current_t current_t ", current_t_ok)
                if i == 0: 
                    first_current_t = current_t_ok
                    print("first_current_t {}".format(first_current_t))
                else:
                    if current_t_ok - first_current_t > 1:
                        print("enter in the case")
                        self.sendreset()
                        break
                i = i + 1 
            msg = self.receive_from_arduino()
            print("msg   ", msg)
        return msg

    cdef wait_end_of_a_reaction(self):    
        """ Ensures connection with Arduino Mega is fine. 
        Provides that all bytes left over from a previous message are discarded. 
        """ 
        print("Im currently in wait_end_reaction")
        cdef char* msg = ''
        cdef char* right_msg = 'finished'
        ok_end = 0
        try:
            while msg.find(right_msg)==-1:
                while self.ser.inWaiting()==0:
                    pass
                msg = self.receive_from_arduino()
                print("messaggio ricevuto da mega è:")
                print(msg)
        except Exception:
            print("@@@@@@@@@@@@@@@ eccezionale veramente")

        #while ok_end<1:
        #    try:
        #        while msg.find(right_msg)==-1:
        #            while self.ser.inWaiting()==0:
        #                msg = self.receive_from_arduino()
        #                print("messaggio ricevuto da mega è:")
        #                print(msg)
        #                if msg.decode() in ['finished']:
        #                    ok_end = 1
        #    except Exception:
        #        print("@@@@@@@@@@@@@@@ eccezionale veramente")

    #old
    ''' 
    cdef wait_end_of_a_reaction(self):    
        """ Ensures connection with Arduino Mega is fine. 
        Provides that all bytes left over from a previous message are discarded. 
        """ 
        print("Im currently in wait_end_reaction")
        cdef char* msg = ''
        cdef char* right_msg = 'finished'
        try:
            while msg.find(right_msg)==-1:
                while self.ser.inWaiting()==0:
                    pass
                msg = self.receive_from_arduino()
                print("messaggio ricevuto da mega è:")
                print(msg)
        except Exception:
            print("@@@@@@@@@@@@@@@ eccezionale veramente")
    '''
    cpdef close(self):
        """  Closes serial port correctly. """ 
        print("close connectionclose {} connectionclose {} connectionclose {}".format(self.name, self.name, self.name))
        self.ser.close()
    cpdef open(self):
        print("TI APRO IN 2")
        self.ser.open()

    cpdef inWaiting(self):
        """  Waits until serial port is connected. """ 
        self.ser.inWaiting()

    cpdef transmit_with_no_answer(self, char * command):
        print("son son son transmit_with_no_answertransmit_with_no_answer transmit_with_no_answer")
        cdef:
            bint waitingForReply = False
            char* dataReceived = ''
            float startTime = time.time() #??
        if waitingForReply == False:
            self.send_to_arduino(command)
            print('Sent from RASPI {} = {}'.format(self.name, command.decode('UTF-8', 'strict')))

    cpdef transmit(self, char * command):  #need immediate response from Mega 
        cdef:
            bint waitingForReply = False
            char* dataReceived = ''
            float startTime = time.time() #??
        if waitingForReply == False:
            self.send_to_arduino(command)
            print('Sent from RASPI {} = {}'.format(self.name, command.decode('UTF-8', 'strict')))
            waitingForReply = True
        if waitingForReply == True:
            while self.ser.inWaiting() == 0:    ##Continue getting the number of bytes in the input buffer if there is none
                pass    
            dataReceived = self.receive_from_arduino()
            print('Reply Received from Arduino through {} = {}'.format(self.name, dataReceived.decode('UTF-8', 'strict')))

    cpdef transmit_until_its_ok_NUOVO(self, char * command):
        cdef:
            int sgnl = 0
            int i = 0
            int restart = 0
            int flag = 0
            cdef timespec ts
            cdef double current_t
            cdef double first_current_t            
            bint waitingForReply = False
            char* dataReceived = ''
            char * command_old_repair = command;
        while sgnl < 1:
            if not waitingForReply:
                self.send_to_arduino(command)
                print('Sent from Raspi {} = {} command'.format(self.name, command))
                waitingForReply = True
            if waitingForReply:
                while self.ser.inWaiting() == 0:    ##Continue getting the number of bytes in the input buffer if there is none
                    #pass
                    clock_gettime(CLOCK_REALTIME, &ts)
                    current_t = ts.tv_sec + (ts.tv_nsec / 1000000000.)
                    #current_t = (ts.tv_sec + ts.tv_nsec) / 1000000000.
                    current_t_ok = math.trunc(current_t)
                    #print("current_t current_t ", current_t)
                    #print("current_t current_t ", current_t_ok)
                    if i == 0: 
                        first_current_t = current_t_ok
                        print("first_current_t {}".format(first_current_t))
                    else:
                        if current_t_ok - first_current_t > 1:
                            restart += 1
                            print("nuovo enter in the case nuovo")
                            print("nuovo enter in the case nuovo")
                            print("nuovo enter in the case nuovo")
                            print("nuovo enter in the case nuovo")
                            print("nuovo enter in the case nuovo")
                            print("nuovo enter in the case nuovo")
                            print("nuovo enter in the case nuovo")
                            print("nuovo enter in the case nuovo")
                            print("nuovo enter in the case nuovo")
                            waitingForReply = False
                            if restart>3:
                                #self.sendreset2(command_old_repair)
                                self.sendreset()
                                self.transmit_until_its_ok_NUOVO(command)
                                restart=0 #useless
                            break
                    i = i + 1 

                dataReceived = self.receive_from_arduino()
                print('Reply Received from Arduino through {} = {}'.format(self.name, dataReceived.decode('UTF-8', 'strict')))
                if len(dataReceived.decode())==0 or dataReceived.decode() in ["I'm stopped","finished","I'm reset"]:
                    print("anger INCORRETTO INCORRETTO")
                    sgnl = 0
                    waitingForReply = False
                else:
                    print("anger ricevuto correttamente")
                    sgnl = 1

    cpdef transmit_until_its_ok(self, char * command):
        cdef:
            int sgnl = 0
            int flag = 0
            bint waitingForReply = False
            char* dataReceived = ''
        while sgnl < 1:
            if not waitingForReply:
                self.send_to_arduino(command)
                print('Sent from Raspi {} = {} command'.format(self.name, command))
                waitingForReply = True
            if waitingForReply:
                while self.ser.inWaiting() == 0:    ##Continue getting the number of bytes in the input buffer if there is none
                    pass
                dataReceived = self.receive_from_arduino()
                print('Reply Received from Arduino through {} = {}'.format(self.name, dataReceived.decode('UTF-8', 'strict')))
                if dataReceived.decode() in [" ", "I'm stopped","finished"]:
                    print("anger INCORRETTO INCORRETTO")
                    sgnl = 0
                    waitingForReply = False #no
                else:
                    print("anger ricevuto correttamente")
                    sgnl = 1

    cpdef get_dataReceived(self):
        return self.receive_from_arduino()
    # -----------------------------------------------------------------------------------------------------------
    # Complete
    # -----------------------------------------------------------------------------------------------------------
    cdef char* pickup_fromMega_confirm(self):
        """Gathers data from Arduino Mega during robot movement. 
        If corresponds to a known msg, otherwise... exception?? """ 
        cdef:
            int confirm = 0
            char* msg = b''
            char* feedback = b''
        while True:
            try:
                #while self.ser.inWaiting() == 0:
                #    pass
                msg = self.receive_from_arduino()
                if msg= b'': #mi serve per velocizzare autonomous 
                    feedback = serr.compareMsg(msg)
                else: feedback = b'notFound'
                print("siamo inside pickup from Mega e feedback is {}".format(feedback))
                #if feedback = b'notFound':
                print("msg is {}".format(msg))
                print("confirm is {}".format(confirm))    
                return feedback
                #else:
                #    confirm = 1
                #    print(confirm)
                #    raise ce.ErrorSerialException
            except ce.ErrorSerialException:
                print("pickupeRrrorerrr")
                continue

    cdef char* pickup_fromNano1_confirm(self):
        """  Gathers data from Arduino Nano1. """ 
        cdef:
            int verify = 0
            char* msg = ''
            char* right_auto_msg = 'goauto'
            char* right_follow_msg = 'gofollo'
            char* right_action_msg = 'goaction'
            char* auto_confirm = 'autonomous'
            char* follo_confirm = 'following'
            char* sadness_conf = 'following'
        while self.ser.inWaiting() == 0:
            pass
        msg = self.receive_from_arduino()
        if msg.find(right_auto_msg) == 0:
            print(msg)
            print("verify value is {}".format(verify))
            return auto_confirm
        elif msg.find(right_follow_msg) == 0:
            print(msg)
            print("verify value is {}".format(verify))
            return follo_confirm

    # -----------------------------------------------------------------------------------------------------------
    # Control
    # -----------------------------------------------------------------------------------------------------------
    cdef tell_to_reset(self):
        cdef: 
            bint waitingForReply = False
            char* result = b'<reset>'
            char* dataReceived
        startledtime = time.time()
        if waitingForReply == False:
            self.send_to_arduino(result)
            print('Sent from RASPI {} = {}'.format(self.name, result))
            waitingForReply = True
        if waitingForReply == True:
            while self.ser.inWaiting() == 0:
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply led Received{}'.format(dataReceived))
    
    cdef tell_to_switch(self,val):
        cdef:
            char* command0 = '<switchMod0>'
            char* command1 = '<switchMod1>'
            char* command2 = '<switchMod2>'
            char* command3 = '<switchMod3>'
            char* dataReceived = ''
            bint waiting_for_reply = False
        if not waiting_for_reply:
            if val==0:
                self.send_to_arduino(command0)
                print('Sent from Raspi {} = {} '.format(self.name, command0))
            elif val==1:
                self.send_to_arduino(command1)
                print('Sent from Raspi {} = {} '.format(self.name, command1))
            elif val==2:
                self.send_to_arduino(command2)
                print('Sent from Raspi {} = {} '.format(self.name, command2))
            elif val==3:
                self.send_to_arduino(command3)
                print('Sent from Raspi {} = {}' .format(self.name, command3))
            waiting_for_reply = True
        if waiting_for_reply:
            while self.ser.inWaiting() == 0:            
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply Received take one {}'.format(dataReceived))

    cdef tell_to_pause(self):
        cdef:
            bint waitingForReply = False
            char* dataReceived = ''
            char* result = b'<pause>' #change command here
        startledtime = time.time()
        if waitingForReply == False:
            self.ser.reset_input_buffer()
            self.ser.reset_output_buffer()
            self.send_to_arduino(result)
            print('Sent from RASPI {} = {}'.format(self.name, result.decode('UTF-8', 'strict')))
            waitingForReply = True
        if waitingForReply == True:
            while self.ser.inWaiting() == 0:
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply led Received {}'.format(dataReceived.decode('UTF-8', 'strict')))

    # -----------------------------------------------------------------------------------------------------------
    # Reactions
    # ----------------------------------------------------------------------------------------------------------- 
    cdef tell_to_react(self, command):
        """ 
        """
        cdef:
            bint waiting_for_reply = False
            char* dataReceived = ''
        if not waiting_for_reply:
            self.send_to_arduino(command)
            print('Sent from Raspi {} = {} TEST PRESS'.format(self.name, command))
            waiting_for_reply = True
        if waiting_for_reply:
            while self.ser.inWaiting() == 0:            
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply Received from arduino {}'.format(dataReceived))
            res = dataReceived.split()
            ans1 = float(res[2].decode('UTF-8', 'strict'))
            ans2 = float(res[6].decode('UTF-8', 'strict'))
            ans3 = res[10].decode('UTF-8', 'strict')
            ans4 = res[13].decode('UTF-8', 'strict')
            ans5 = res[16].decode('UTF-8', 'strict')
        return ans1,ans2,ans3,ans4,ans5

    cdef emotion_reaction(self, move_comm, led_col, animation, for_speed, ang_speed):
        """ 
        """ 
        end = '>'
        if move_comm == "sad":
            command_feeling = '<sad'
        elif move_comm == "normal":
            command_feeling = '<normal'
        elif move_comm == "happy":
            command_feeling = '<happy'
        elif move_comm == "enthusiast":
            command_feeling = '<enthusiast'

        values = [command_feeling, led_col, animation, for_speed, ang_speed]
        strin = ','.join(map(str, values))
        addend = [strin,end]
        command_to_send = ''.join(map(str, addend))
        print("command to send is {}".format(command_to_send))
        command_to_send = command_to_send.encode('UTF-8', 'strict')
        #cdef char* command = command_to_send #per passare char faccio prova coi cdef
        
        #gare.play_audio(choosen_audio)
        
        ans1, ans2, ans3, ans4, ans5 = self.tell_to_react(command_to_send)
        print("ans1 is {}".format(ans1))
        print("ans2 is {}".format(ans2))
        print("ans3 is {}".format(ans3))
        print("ans4 is {}".format(ans4))
        print("ans5 is {}".format(ans5))


    cdef start_reaction(self):
        print("startprova333")
        cdef:
            float for_speed
            float ang_speed
            str led_col
            str animation
            str move_comma
        for_speed = 20
        ang_speed = 1.5
        led_col = "red"
        animation = "stable"
        move_comma = "sad"                                                                         
        self.emotion_reaction(move_comma, led_col, animation, for_speed, ang_speed)

    cdef send_led_animation(self, char* led_comm):
        signal.signal(signal.SIGALRM, ce.time_handler)
        signal.alarm(5)
        print("enter in led anuimationnn")
        self.send_to_arduino(led_comm)
        print('Sent from RASPI led animation {} = {}'.format(self.name, led_comm.decode('UTF-8', 'strict')))
        waitingForReply = True
        if waitingForReply == True:
            while True:
                try:
                    while self.ser.inWaiting()==0:
                        pass
                    dataReceived = self.receive_from_arduino()
                    print('Reply of LED Received arduino - {}'.format(dataReceived.decode('UTF-8', 'strict')))
                    #signal.alarm(0)
                    #break
                except ce.TimeOutException:
                    print("takes too long time")
                    signal.alarm(5)
                    continue
                finally: 
                    signal.alarm(0)
                    break

    cdef send_led_animation_while_moving(self, char* led_comm):
        signal.signal(signal.SIGALRM, ce.time_handler)
        signal.alarm(5)
        waitingForReply = True
        print("enter in led animation")
        while True:
            if waitingForReply == True:
                self.send_to_arduino(led_comm)
                print('Sent from RASPI led animation while moving {} = {}'.format(self.name, led_comm.decode('UTF-8', 'strict')))
                waitingForReply = False
            else:
                try:
                    while self.ser.inWaiting()==0:
                        pass
                    dataReceived = self.receive_from_arduino()
                    print('Reply of LED Received arduino {} ---> {}'.format(self.name, dataReceived.decode('UTF-8', 'strict')))
                    #signal.alarm(0)
                    #break
                    if dataReceived.decode() not in ["left1",'front1','right1','back1']:
                        print("LED found LED found LED found INCORREctINCORREct INCORREctINCORREctINCORREct")
                        waitingForReply = True #no
                        continue
                    else:
                        print("led command ricevuto correttamente")
                        #break #already in finallyy
                        signal.alarm(0)
                        break

                except ce.TimeOutException:
                    print("takes too long time LED animation")
                    signal.alarm(5)
                    waitingForReply = True #no
                    continue #abort instead of continue??
                #finally: 
                #    signal.alarm(0)
                #    break

    cdef send_led_animation_tentiamo(self, char* led_comm):
        too_much_exceptions = 0
        signal.signal(signal.SIGALRM, ce.time_handler)
        signal.alarm(5)
        waitingForReply = True
        print("enter in led anuimationnn")
        while True:
            if waitingForReply == True:
                self.send_to_arduino(led_comm)
                print('Sent from RASPI led animation tentiamo {} = {}'.format(self.name, led_comm.decode('UTF-8', 'strict')))
                waitingForReply = False
            else:
                try:
                    while self.ser.inWaiting()==0:
                        pass
                    dataReceived = self.receive_from_arduino()
                    print('Reply of LED Received arduino {} ---> {}'.format(self.name, dataReceived.decode('UTF-8', 'strict')))
                    #print('Reply of LED Received arduino {} ---> {}'.format(self.name, dataReceived))
                    #signal.alarm(0)
                    #break
                    if dataReceived.decode() not in ["caress_ok", "touch_ok","shove_ok","hit_ok","quiet_ok","choke_ok","squeeze_ok","hug_ok",
                        "hit_left_ok","hit_front_ok","hit_right_ok","hit_back_ok", "reacting_pos","reacting_neg","reward_ok_ok",
                        "reward_ko_ok","reward_sess_ok","reward_match_ok","reward_game_ok","reward_enter_ok",
                        "prompt_atte_ok","prompt_encourage_ok","prompt_start_ok","prompt_end_ok", "correct_ans_ok", "wrong_ans_ok"]:
                        print("LED found LED found LED found INCORREctINCORREct INCORREctINCORREctINCORREct")
                        waitingForReply = True #no
                        continue
                    else:
                        print("led command ricevuto correttamente")
                        #break #already in finallyy
                        signal.alarm(0) #necessary 
                        break
                except ce.TimeOutException:
                    too_much_exceptions+=1
                    print("There is Something wrong. It is taking too much time in trying to send the right LED animation command")
                    signal.alarm(0) #necessary  also here to restart counter
                    waitingForReply = True #no
                    if too_much_exceptions<3:
                        continue
                    else:
                        signal.alarm(0)
                        break
                #finally: #no
                #    signal.alarm(0)
                #    break

    cdef csendSpecificLED(self, str pattern_and_color):
        cdef bint waitingForReply = False
        cdef char* dataReceived = ''
        if waitingForReply == False:
            #separating the string, composign the msg and using switch_mod1 don't slow down the reaction 
            #therefore ...it is not necessary for now to divide before and send 2 separated string or to prepare a series of right msg before...
            separator = '_'
            list_paco = pattern_and_color.split (separator)
            print(type(list_paco))
            print(list_paco)
            LED_msg = list_paco[0] + ", " + list_paco[1]
            msg_tmp = "<" + "reacthis, " + LED_msg + ">"
            print(msg_tmp)
            #msg_to_send = msg_tmp.encode()
            msg_to_send = msg_tmp.encode('UTF-8', 'strict')
            print(msg_to_send)
            print(type(msg_to_send))
            #switchalert = b'<switchMod1>'
            #self.transmit(switchalert)
            #time.sleep(1)
            self.send_to_arduino(msg_to_send)
            print('Sent from RASPI {} = {}'.format(self.name, msg_to_send.decode('UTF-8', 'strict')))
            waitingForReply = True
        if waitingForReply == True:
            while self.ser.inWaiting()==0:
                pass
            dataReceived = self.receive_from_arduino()
            print('csendSpecificLED Reply led Received arduino - {}'.format(dataReceived.decode('UTF-8', 'strict')))


    cdef csendledcommand(self, int conferma):
        cdef bint waitingForReply = False
        cdef char* dataReceived = ''
        cdef char* resultstop = b'<q>' #change command here
        cdef char* resultleft = b'<w>' #change command here
        cdef char* resultfront = b'<x>' #change command here
        cdef char* resultright = b'<y>' #change command here
        cdef char* resultback = b'<z>' #change command here
        cdef char* resultturn = b'<t>' #change command here
        cdef char* resulttran = b'<k>' #change command here
        cdef char* resultno = b'<m>' #change command here
        cdef char* resultfeel = b'<kk>' #change command here
        if conferma==1:
            result = resultleft
        elif conferma==2:
            result = resultfront
        elif conferma==3:
            result = resultright
        elif conferma==4:
            result = resultback
        elif conferma==5:
            result = resultturn
        elif conferma==6:
            result = resultstop
        elif conferma==7:
            result = resulttran
        elif conferma==8:
            result = resultno
        elif conferma==9:
            result = resultfeel
        while True:
            if waitingForReply == False:
                self.send_to_arduino(result)
                print('Sent from RASPI {} = {}'.format(self.name, result.decode('UTF-8', 'strict')))
                waitingForReply = True
            if waitingForReply==True:
                while self.ser.inWaiting()==0:
                    continue
                dataReceived = self.receive_from_arduino()
                if len(dataReceived.decode())==0:
                    print("led command non è non è non è non è non è non è ricevuto correttamente")
                    continue
                else:
                    print("led command ricevuto correttamente")
                    print('Reply csendledcommand csendledcommand led Received arduino {} - {}'.format(self.name, dataReceived.decode('UTF-8', 'strict')))
                    break
            ''' OLD with no check
            self.send_to_arduino(result)
            print('Sent from RASPI {} = {}'.format(self.name, result.decode('UTF-8', 'strict')))
            waitingForReply = True
        if waitingForReply == True:
            while self.ser.inWaiting()==0:
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply csendledcommand csendledcommand led Received arduino - {}'.format(dataReceived.decode('UTF-8', 'strict')))
            '''

    cpdef sendreset(self):
        cdef:
            bint waitingForReply = False
            char* dataReceived2
            char* result = b'<reset>'
        if waitingForReply == False:
            self.send_to_arduino(result)
            print('Sent from RASPI reset method with {} = {} string'.format(self.name, result.decode('UTF-8', 'strict')))
            waitingForReply = True
        if waitingForReply == True:
            while self.ser.inWaiting()==0:
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply led Received arduino on {} = {}'.format(self.name, dataReceived.decode('UTF-8', 'strict')))

    def sendled(self, conferma):
        self.csendledcommand(conferma)

    def sendSpecLED(self, pattern_and_color):
        self.csendSpecificLED(pattern_and_color)

    cdef stop_led(self):
        cdef:
            bint waitingForReply = False
            char* dataReceived2
            char* result = b'<stopcolor>'
        startledtime = time.time()
        if waitingForReply == False:
            self.send_to_arduino(result)
            print('Sent from RASPI stop led method {} = {}'.format(self.name, result.decode('UTF-8', 'strict')))
            waitingForReply = True
    # -----------------------------------------------------------------------------------------------------------
    # Pressures Arduino nano_2
    # -----------------------------------------------------------------------------------------------------------    
    cdef take_front_pressure(self):
        cdef:
            char* command = '<ppf>'
            char* dataReceived = ''
            bint waiting_for_reply = False
        if not waiting_for_reply:
            self.send_to_arduino(command)
            print('Sent from Raspi {} = {} take front'.format(self.name, command))
            waiting_for_reply = True
        if waiting_for_reply:
            while self.ser.inWaiting() == 0:            
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply Received take one {}'.format(dataReceived))
            res = dataReceived.split()
            ans = float(res[3].decode())
        return ans

    cdef take_one_scan_pressure(self, float mp0, float mp1, float mp2, float mp3, float mp4):
        cdef:
            char* command = '<ppos>'
            char* dataReceived = ''
            bint waiting_for_reply = False
        if not waiting_for_reply:
            self.send_to_arduino(command)
            print('Sent from Raspi {}={} TEST PRESS 1 scan'.format(self.name, command))
            waiting_for_reply = True
        if waiting_for_reply:
            while self.ser.inWaiting() == 0:
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply Received take one {}'.format(dataReceived))
            res = dataReceived.split()
            mp0 = float(res[2].decode())
            mp1 = float(res[3].decode())
            mp2 = float(res[4].decode())
            mp3 = float(res[5].decode())
            mp4 = float(res[6].decode())
        return mp0,mp1,mp2,mp3,mp4

    cpdef take_proper_msg_body_touch_reaction(self, perceived):
        if perceived not in ["hitf", "hitb", "hitl", "hitr"]:
            last_char = perceived[len(perceived)-1:]
            msg_start = perceived[:len(perceived)-1]
            msg_tmp = "<" + msg_start + ", " + last_char + ">"
        else:
            msg_tmp = "<" + perceived + ">"

        #mess = "<pratte,3>"
        commandcom = msg_tmp.encode('UTF-8', 'strict')

        print("siamo in take_proper_msg_body_touch_reaction msg_tmp  ::  ", msg_tmp)
        print("siamo in take_proper_msg_body_touch_reaction msg_tmp  ::  ", msg_tmp)
        print("siamo in take_proper_msg_body_touch_reaction msg_tmp  ::  ", msg_tmp)
        print("siamo in take_proper_msg_body_touch_reaction msg_tmp  ::  ", msg_tmp)
        print("siamo in take_proper_msg_body_touch_reaction commandcom == ", commandcom)
        print("siamo in take_proper_msg_body_touch_reaction commandcom == ", commandcom)
        print("siamo in take_proper_msg_body_touch_reaction commandcom == ", commandcom)
        print("siamo in take_proper_msg_body_touch_reaction commandcom == ", commandcom)
        return commandcom        

    cpdef take_10_pressures_and_mpus(self, mpu1_left, mpu2_right):
        #deque [float] mmpp
        #print(mmpp)
        cdef:
            char* command = '<press10>'
            char* dataReceived = ''
            Py_ssize_t i = 0
            Py_ssize_t leng = 10
            bint waiting_for_reply = False
        mpx0,mpx1,mpx2,mpx3,mpx4 = [],[],[],[],[]
        gyroxl,gyrozl,gyroxr,gyrozr = [],[],[],[]
        for i in range(leng):
            gxl, gzl, gxr, gr = bcs.scan_MPUs(mpu1_left, mpu2_right)
            gyroxl.append(gxl)
            gyrozl.append(gzl)
            gyroxr.append(gxr)
            gyrozr.append(gr)
            if not waiting_for_reply:
                self.send_to_arduino(command)
                print('Sent from Raspi {} {} TEST PRESS 10 scans'.format(self.name, command))
                waiting_for_reply = True
                dataReceived = self.receive_from_arduino()
                #dataRec = str(dataReceived,'utf-8')
                print('Received pressures {} '.format(dataReceived))
                res = dataReceived.split()
                mpx0.append(res[2].decode())
                mpx1.append(res[3].decode())
                mpx2.append(res[4].decode())
                mpx3.append(res[5].decode())
                mpx4.append(res[6].decode())
            else:
                #while ser.inWaiting() == 0:
                #    pass
                dataReceived = self.receive_from_arduino()
                #dataRec = str(dataReceived,'utf-8')
                print('Received pressures {} '.format(dataReceived))
                res = dataReceived.split()
                mpx0.append(res[2].decode())
                mpx1.append(res[3].decode())
                mpx2.append(res[4].decode())
                mpx3.append(res[5].decode())
                mpx4.append(res[6].decode())
            #time.sleep(0.3)
        return mpx0,mpx1,mpx2,mpx3,mpx4,gyroxl,gyrozl,gyroxr,gyrozr

    cpdef take_10_pressures(self):
        #deque [float] mmpp
        #print(mmpp)
        cdef:
            char* command = '<press10>'
            char* dataReceived = ''
            Py_ssize_t i = 0
            Py_ssize_t leng = 11
            bint waiting_for_reply = False
        mpx0,mpx1,mpx2,mpx3,mpx4 = [],[],[],[],[]
        for i in range(leng):
            if not waiting_for_reply:
                self.send_to_arduino(command)
                print('Sent from Raspi {} {} TEST PRESS 10 scans'.format(self.name, command))
                waiting_for_reply = True
            else:
                #while ser.inWaiting() == 0:
                #    pass
                dataReceived = self.receive_from_arduino()
                #dataRec = str(dataReceived,'utf-8')
                #print('Received from Following pressures {} '.format(dataReceived))
                res = dataReceived.split()
                mpx0.append(res[2].decode())
                mpx1.append(res[3].decode())
                mpx2.append(res[4].decode())
                mpx3.append(res[5].decode())
                mpx4.append(res[6].decode())
        return mpx0,mpx1,mpx2,mpx3,mpx4

    cpdef take_pressures_forever_vecchia(self, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press):
        cdef:
            char* command = '<press>'
            char* dataReceived = ''
            Py_ssize_t i = 0
            Py_ssize_t leng = 10            
            bint waiting_for_reply = False
            bint completed = False
        mpx0,mpx1,mpx2,mpx3,mpx4= [],[],[],[],[]
        #or try this:
        #while event_continue_take_press.is_set():
        while True:
            if event_continue_take_press.is_set():
                #print(">>>>>>>>>>>>>>>>>>> serial  serial  serial  serial  event_continue_take_press is set entro nel while")
                if not waiting_for_reply:
                    self.send_to_arduino(command)
                    print('Sent from Raspi {} {} TEST PRESS scans'.format(self.name, command))
                    waiting_for_reply = True
                else:
                    #print("sono in else di take pressures forever")
                    for i in range(leng):
                        while self.ser.inWaiting() == 0:
                            pass
                        dataReceived = self.receive_from_arduino()
                        #dataRec = str(dataReceived,'utf-8')
                        #print('Received from Following pressures {} '.format(dataReceived))
                        res = dataReceived.split()
                        mpx0.append(res[2].decode())
                        mpx1.append(res[3].decode())
                        mpx2.append(res[4].decode())
                        mpx3.append(res[5].decode())
                        mpx4.append(res[6].decode())
                    lis = [mpx0, mpx1, mpx2, mpx3, mpx4]
                    lifo_press.put([mpx0, mpx1, mpx2, mpx3, mpx4])
                    event_lifo_press_filled.set()
                    #lifo_press.put(lis)
                    #lifo_press.put(mpx0)
            if event_continue_take_press22222222.is_set():
                print("exit serial  exit")
                #self.transmit(b'<presstop>')
                self.wait_pressurestop_msg()
                break

    cpdef take_pressures_forever(self, event_continue_take_press, event_continue_take_press22222222, event_lifo_press_filled, lifo_press):
        cdef:
            char* command = '<press>'
            char* dataReceived = ''
            Py_ssize_t i = 0
            Py_ssize_t leng = 10            
            bint waiting_for_reply = False
            bint completed = False
        mpx0,mpx1,mpx2,mpx3,mpx4= [],[],[],[],[]
        #or try this:
        #while event_continue_take_press.is_set():
        while True:
            if event_continue_take_press.is_set():
                #print(">>>>>>>>>>>>>>>>>>> serial  serial  serial  serial  event_continue_take_press is set entro nel while")
                if not waiting_for_reply:
                    self.send_to_arduino(command)
                    print('Sent from Raspi {} {} TEST PRESS scans'.format(self.name, command))
                    waiting_for_reply = True
                else:
                    #print("sono in else di take pressures forever")
                    while self.ser.inWaiting() == 0:
                        pass
                    dataReceived = self.receive_from_arduino()
                    #dataRec = str(dataReceived,'utf-8')
                    #print('Received from Following pressures {} '.format(dataReceived))
                    res = dataReceived.split()
                    mpx0.append(res[2].decode())
                    mpx1.append(res[3].decode())
                    mpx2.append(res[4].decode())
                    mpx3.append(res[5].decode())
                    mpx4.append(res[6].decode())
                    if len(mpx0)==10:
                        lifo_press.put([mpx0, mpx1, mpx2, mpx3, mpx4])
                        mpx0,mpx1,mpx2,mpx3,mpx4= [],[],[],[],[]
                        event_lifo_press_filled.set()
                    #lifo_press.put([mpx0, mpx1, mpx2, mpx3, mpx4])                    
                    #if lifo_press.qsize() > 1:


            if event_continue_take_press22222222.is_set():
                print("exit serial  exit")
                #self.transmit(b'<presstop>')
                self.wait_pressurestop_msg()
                break

    cpdef wait_pressurestop_msg(self): 
        cdef:
            char* msg = ''
            cdef char* stopcom    = b'<presstop>'
            bint waitingForReply = False
            char* dataReceived = ''
            cdef char* right_msg = 'press_stopped'
        if waitingForReply == False:
            self.send_to_arduino(stopcom)
            print('Sent from RASPI {} = {}'.format(self.name, stopcom.decode('UTF-8', 'strict')))
            waitingForReply = True
        if waitingForReply == True:
            #while self.ser.inWaiting() == 0:
            #    pass
            #dataReceived = self.receive_from_arduino()
            while dataReceived.find(right_msg) == -1:
                self.transmit(b'<presstop>')
                dataReceived = self.receive_from_arduino()
                print('Reply Received wait_pressurestop_msg wait_pressurestop_msg {} = {}'.format(self.name, dataReceived.decode('UTF-8', 'strict')))
                print(dataReceived)
    # -----------------------------------------------------------------------------------------------------------
    # Navigation
    # ----------------------------------------------------------------------------------------------------------- 
    cdef tell_to_stop(self):
        cdef:
            int sgnl = 0
            int flag = 0
            bint waiting_for_reply = False
            cdef char* stopcom    = b'<stop>'
            char* dataReceived = ''
        while sgnl < 1:
            try:
                if not waiting_for_reply:
                    self.send_to_arduino(stopcom)
                    print('Sent from Raspi {} = {} stop'.format(self.name, stopcom))
                    waiting_for_reply = True
                if waiting_for_reply:
                    #if self.ser.inWaiting() > 0:
                    #    sgnl = 1
                    #else: continue
                    print("sgnl is {}".format(sgnl))
                    #time.sleep(1)
                    dataReceived = self.receive_from_arduino()
                    print('Reply Received stop method => {}'.format(dataReceived))
                    if dataReceived.decode() == "I'm stopped":
                        print("stop ricevuto correttamente")
                        sgnl = 1
                    else: 
                        print("stop ricevuto INCORRETTO")
                        waiting_for_reply = False
                        flag = flag + 1
                        print("dfasddddddddddddddddddddddddddddddddddd ricomincio giro con flag = ", flag)
                    #res = dataReceived.split()
                    #ans = float(res[3].decode())
            except serial.serialutil.SerialException:
                print("type_error sono qui in tell to stop")
                continue

    cdef movement_command(self, command):
        cdef:
            bint waiting_for_reply = False
            char* dataReceived = ''
        if not waiting_for_reply:
            self.send_to_arduino(command)
            print('Sent from Raspi {} = {} TEST PRESS'.format(self.name, command))
            waiting_for_reply = True
        if waiting_for_reply:
            while self.ser.inWaiting() == 0:            
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply Received take one {}'.format(dataReceived))
            res = dataReceived.split()
            strafe = float(res[3].decode())
            forward = float(res[7].decode())
            angular = float(res[11].decode())
        return strafe,forward,angular
    
    cdef tell_to_move(self, command):
        ''' Sends joypad input as command, receives back strafe, forward and angular speeds'''
        cdef:
            bint waiting_for_reply = False
            char* dataReceived = ''
            float speed1
            float speed2
            float speed3
        if not waiting_for_reply:
            self.send_to_arduino(command)
            print('Sent from Raspi {} = {}'.format(self.name, command))
            waiting_for_reply = True
        if waiting_for_reply:
            while self.ser.inWaiting() == 0:            
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply Received {}'.format(dataReceived))
            res = dataReceived.split()
            speed1 = float(res[3].decode())
            speed2 = float(res[7].decode())
            speed3 = float(res[11].decode())
        return speed1, speed2, speed3

    cdef tell_to_move2(self, command):
        ''' Sends joypad input as command, receives back strafe and forward speeds'''
        cdef:
            bint waiting_for_reply = False
            char* dataReceived = ''
            float speed1
            float speed2
        if not waiting_for_reply:
            self.send_to_arduino(command)
            print('Sent from Raspi {} = {} TEST PRESS'.format(self.name, command))
            waiting_for_reply = True
        if waiting_for_reply:
            while self.ser.inWaiting() == 0:            
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply Received take one {}'.format(dataReceived))
            res = dataReceived.split()
            speed1 = float(res[3].decode())
            speed2 = float(res[7].decode())
        return speed1, speed2

    cdef tell_to_move_reaction(self, command):
        ''' Reaction'''
        cdef:
            bint waiting_for_reply = False
            char* dataReceived = ''
            float speed1
            float speed2
            float speed3
        if not waiting_for_reply:
            self.send_to_arduino(command)
            print('Sent from Raspi {} = {} tell to move reaction:'.format(self.name, command))
            waiting_for_reply = True
        if waiting_for_reply:
            while self.ser.inWaiting() == 0:            
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply Received from Arduino Mega is {}'.format(dataReceived))
            res = dataReceived.split()
            print("res is =>> after split ===>  ", res)
            speed1 = float(res[3].decode())
            speed2 = float(res[7].decode())
            speed3 = float(res[11].decode())
        return speed1, speed2, speed3
    # ===============================================================================================================
    #  Wrappers:
    # ===============================================================================================================        
    def await_arduinospecial(self):
        a = self.wait_arduino_special()
        return a

    def await_arduinoMega(self):
        self.wait_arduino_mega()
    
    def await_arduinoNano1(self):
        self.wait_arduino_nano1()

    def await_arduinoNano2(self):
        self.wait_arduino_nano2()

    def await_arduino(self):
        """single method for all arduinos...actually, is the one in use"""
        self.wait_arduino()

    def await_end(self):
        self.wait_end_of_a_reaction()
                
    def pickup_fromNano1(self):
        print("enter in pickupfromNANo")
        cdef char* validation = self.pickup_fromNano1_confirm()
        print("Confirmation is valid, {} it is ok".format(validation))
        return validation
    
    def pickup_fromMega(self):
        print("enter in pickupfromMEGA")
        cdef char* conf = self.pickup_fromMega_confirm()
        print("Confirmation is valid, {} it accepted".format(conf))
        return conf
    
    def takeMpxPressure(self):
        feedback = self.take_front_pressure()
        print('The pressures are: {}'.format(feedback))
        return feedback 

    cpdef takeoneMpxScan(self):
        cdef:
            float mp0=0.0
            float mp1=0.0
            float mp2=0.0
            float mp3=0.0
            float mp4=0.0
        mp0,mp1,mp2,mp3,mp4 = self.take_one_scan_pressure(mp0,mp1,mp2,mp3,mp4)
        return mp0,mp1,mp2,mp3,mp4

    def send_led_anim(self, led_comm):
        #self.send_led_animation(led_comm);
        self.send_led_animation_tentiamo(led_comm);
    
    def send_led_anim_moving(self, led_comm):
        #self.send_led_animation(led_comm);
        self.send_led_animation_while_moving(led_comm);

    def send_stop(self):
        self.tell_to_stop()
    
    def send_pause(self):
        res = self.tell_to_stop()
        return res
            
    def send_reset(self):
        self.tell_to_reset()

    def switch_modality(self, val):
        self.tell_to_switch(val)
        
    def startMoving_order(self, move_comm): #need mov2
        cdef str command_mov = move_comm
        cdef char* command
        ####change transforming others that are equal...or reduce length of msg command to send
        if command_mov == "following":
            command = b'<follo>'
        elif command_mov == "autonomous":
            command = b'<auto>'
        elif command_mov == "pattern":
            command = b'<pattern>'
        elif command_mov == "agree":
            command = b'<agree>'
        elif command_mov == "disagree":
            command = b'<disagree>'
        elif command_mov == "joypad":
            command = b'<joypad>'
        elif command_mov == "sadness":
            command = b'<sadness>'
        elif command_mov == "anger":
            command = b'<anger>'
        elif command_mov == "scare":
            command = b'<scare>'
        elif command_mov == "enthusiast":
            command = b'<enthu>'
        elif command_mov == "surprised_1":
            command = b'<surpr,1>'
        elif command_mov == "quiet":
            command = b'<quiet_nothing>'

        elif command_mov=="anger_mov1":
            command = b'<anger,1>'
        elif command_mov=="anger_mov2":
            command = b'<anger,2>'
        elif command_mov=="anger_mov3":
            command = b'<anger,3>'
        elif command_mov=="enthu_mov1":
            command = b'<enthu,1>'
        elif command_mov=="enthu_mov2":
            command = b'<enthu,2>'
        elif command_mov=="enthu_mov3":
            command = b'<enthu,3>'
        elif command_mov=="happy_mov1":
            command = b'<happy,1>'
        elif command_mov=="happy_mov2":
            command = b'<happy,2>'
        elif command_mov=="happy_mov3":
            command = b'<happy,3>'
        elif command_mov=="sad_mov1":
            command = b'<sad,1>'
        elif command_mov=="sad_mov2":
            command = b'<sad,2>'
        elif command_mov=="sad_mov3":
            command = b'<sad,3>'
        elif command_mov=="surp_mov1":
            command = b'<surp,1>'
        elif command_mov=="surp_mov2":
            command = b'<surp,2>'
        elif command_mov=="surp_mov3":
            command = b'<surp,3>'
        elif command_mov=="scare_mov1":
            command = b'<scare,1>'
        elif command_mov=="scare_mov2":
            command = b'<scare,2>'
        elif command_mov=="scare_mov3":
            command = b'<scare,3>'

        elif command_mov == "reactst_pos_1":
            command = b'<reactst_pos,1>'
        elif command_mov == "reactst_pos_2":
            command = b'<reactst_pos,2>'
        elif command_mov == "reactst_pos_3":
            command = b'<reactst_pos,3>'
        elif command_mov == "reactst_pos_4":
            command = b'<reactst_pos,4>'
        elif command_mov == "reactst_pos_5":
            command = b'<reactst_pos,5>'
        elif command_mov == "reactst_pos_6":
            command = b'<reactst_pos,6>'

        elif command_mov == "reactst_neg_1":
            command = b'<reactst_neg,1>'
        elif command_mov == "reactst_neg_2":
            command = b'<reactst_neg,2>'
        elif command_mov == "reactst_neg_3":
            command = b'<reactst_neg,3>'
        elif command_mov == "reactst_neg_4":
            command = b'<reactst_neg,4>'
        elif command_mov == "reactst_neg_5":
            command = b'<reactst_neg,5>'
        elif command_mov == "reactst_neg_6":
            command = b'<reactst_neg,6>'

        elif command_mov == "attention_mov1":
            command = b'<prompt_attention,1>'
        elif command_mov == "attention_mov2":
            command = b'<prompt_attention,2>'
        elif command_mov == "attention_mov3":
            command = b'<prompt_attention,3>'
        elif command_mov == "encourage_mov1":
            command = b'<prompt_encourage,1>'
        elif command_mov == "encourage_mov2":
            command = b'<prompt_encourage,2>'
        elif command_mov == "encourage_mov3":
            command = b'<prompt_encourage,3>'
        elif command_mov == "greetingst_mov1":
            command = b'<prompt_start_game,1>'
        elif command_mov == "greetingst_mov2":
            command = b'<prompt_start_game,2>'
        elif command_mov == "greetingst_mov3":
            command = b'<prompt_start_game,3>'
        elif command_mov == "greetingen_mov1":
            command = b'<prompt_endgame,1>'
        elif command_mov == "greetingen_mov2":
            command = b'<prompt_endgame,2>'
        elif command_mov == "greetingen_mov3":
            command = b'<prompt_endgame,3>'       

        elif command_mov == "rewardright_mov1":
            command = b'<reward_right,1>'
        elif command_mov == "rewardright_mov2":
            command = b'<reward_right,2>'
        elif command_mov == "rewardright_mov3":
            command = b'<reward_right,3>'
        elif command_mov == "rewardwrong_mov1":
            command = b'<reward_wrong,1>'
        elif command_mov == "rewardwrong_mov2":
            command = b'<reward_wrong,2>'
        elif command_mov == "rewardwrong_mov3":
            command = b'<reward_wrong,3>'
        elif command_mov == "rewardgame_mov1":
            command = b'<reward_game,1>'
        elif command_mov == "rewardgame_mov2":
            command = b'<reward_game,2>'
        elif command_mov == "rewardgame_mov3":
            command = b'<reward_game,3>'
        elif command_mov == "rewardmatch_mov1":
            command = b'<reward_match,1>'
        elif command_mov == "rewardmatch_mov2":
            command = b'<reward_match,2>'
        elif command_mov == "rewardmatch_mov3":
            command = b'<reward_match,3>'
        elif command_mov == "rewardsess_mov1":
            command = b'<reward_session,1>'
        elif command_mov == "rewardsess_mov2":
            command = b'<reward_session,2>'
        elif command_mov == "rewardsess_mov3":
            command = b'<reward_session,3>'

        #num_led = self.transmit(command) why returning a value??? 
        #self.transmit(command)
        #self.transmit_until_its_ok(command)
        self.transmit_until_its_ok_NUOVO(command)
        #ans1,ans2,ans3,ans4 = self.tell_to_move3(command)
        #print("{},{},{},{}".format(ans1,ans2,ans3,ans4))

    ################# no driveMe, no moveLoop but direct commands for movement
    def react_to_game(self, move_comm, stra_speed, for_speed, ang_speed): #custom speeds ? need mov3
        end = '>'
        if move_comm == "happy":
            command_feeling = '<happy_old'
        elif move_comm == "normal":
            command_feeling = '<normal_old'

        values = [command_feeling,stra_speed,for_speed,ang_speed]
        strin = ','.join(map(str, values))
        addend = [strin,end]
        command_to_send = ''.join(map(str, addend))
        print("command to send is {}".format(command_to_send))
        command_to_send = command_to_send.encode('UTF-8', 'strict')
        #gare.play_audio(choosen_audio)
        ans1, ans2, ans3 = self.tell_to_move_reaction(command_to_send)
        print("ans1 is {}".format(ans1))
        print("ans2 is {}".format(ans2))
        print("ans3 is {}".format(ans3))


    def gamepad_order(self, move_comm, strafe, forward, angular):
        end = '>'
        if move_comm == "gostra":
            move_comm = '<gostra'
        elif move_comm == "golef":
            move_comm = '<golef'
        elif move_comm == "gorig":
            move_comm = '<gorig'
        elif move_comm == "gobac":
            move_comm = '<gobac'
        elif move_comm == "joystop":
            move_comm = '<joystop'
        elif move_comm == "simplygo":
            move_comm = '<simplygo'

        values = [move_comm,strafe,forward, angular]
        strin = ','.join(map(str, values))
        addend = [strin,end]
        command_to_send = ''.join(map(str, addend))
        print("command to send is {}".format(command_to_send))
        command_to_send = command_to_send.encode('UTF-8', 'strict')
        #gare.play_audio(choosen_audio)
        ans1, ans2, ans3 = self.tell_to_move(command_to_send)
        print("ans1 is {}".format(ans1))
        print("ans2 is {}".format(ans2))
        print("ans3 is {}".format(ans3))

    def gamepad_order2(self, move_comm, strafe, forward):
        '''Special case without angular speed'''
        print("---> ENTER GAMEPAD2")
        end = '>'
        if move_comm == "gostra":
            move_comm = '<gostra'
        elif move_comm == "golef":
            move_comm = '<golef'
        elif move_comm == "gorig":
            move_comm = '<gorig'
        elif move_comm == "gobac":
            move_comm = '<gobac'
        elif move_comm == "joystop":
            move_comm = '<joystop'
        elif move_comm == "simplygo2":
            move_comm = '<simplygo2'

        values = [move_comm,strafe,forward]
        strin = ','.join(map(str, values))
        addend = [strin,end]
        command_to_send = ''.join(map(str, addend))
        print("The command to send is {}".format(command_to_send))
        command_to_send = command_to_send.encode('UTF-8', 'strict')
        #gare.play_audio(choosen_audio)
        speed1, speed2 = self.tell_to_move2(command_to_send)
        print("The speed1 is {}".format(speed1))
        print("The speed2 is {}".format(speed2))
    
    def send_pause(self):
        res = self.tell_to_stop()
        return res
            
    def send_reset(self):
        self.tell_to_reset()            
    
    cpdef flushInput(self):
        self.ser.flushInput()
    cpdef flushOutput(self):
        self.ser.flushOutput()


    cdef cgamepad(self, char* command, float strafe, float forward, float angular):     
        cdef char* dataReceived = ''
        cdef bint waiting_for_reply = False
        comma = serr.createMsg(command, strafe, forward, angular);
        cdef char * command_to_send= <char * >malloc(60)
        strcpy(command_to_send,comma)
        print("command to send for gamepad is {}".format(command_to_send))
        print("command has been send comma is {}".format(comma))
        print("command again  is {}".format(command_to_send))
        #cdef char* command = command_to_send #per passare char faccio prova coi cdef
        #gare.play_audio(choosen_audio)
        #command_to_send = comm.encode('UTF-8', 'strict') #already bytes no??
        #ans1, ans2, ans3 = self.tell_to_move(command_to_send)
        #print("ans1 is {}".format(ans1))
        #print("ans2 is {}".format(ans2))
        #print("ans3 is {}".format(ans3))
        if not waiting_for_reply:
            self.send_to_arduino(command_to_send)
            print('Sent from Raspi cgamepad {}'.format(command_to_send))
            waiting_for_reply = True
        if waiting_for_reply:
            while self.ser.inWaiting() == 0:
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply Received cgamepad {}'.format(dataReceived))
            res = dataReceived.split()
            ans1 = float(res[3].decode())
            ans2 = float(res[7].decode())
            ans3 = float(res[11].decode())
        #return ans1,ans2,ans3
            print("ans1 is {}".format(ans1))
            print("ans2 is {}".format(ans2))
            print("ans3 is {}".format(ans3))
        free(command_to_send)
        
    def gamepad(self,command,strafe_speed,forward_speed,angular_speed):
        order = command.encode('UTF-8', 'strict')
        cdef:
            char* move_comm = order
            float strafe = strafe_speed
            float forward = forward_speed
            float angular = angular_speed
        #move_comm = serr.fromStringToBytes(command)
        self.cgamepad(move_comm,strafe,forward,angular)

    cdef cprova1(self):
        cdef bint waitingForReply = False
        cdef char* dataReceived
        cdef char* result = b'<control>'
        if waitingForReply == False:
            self.send_to_arduino(result)
            print('Sent from RASPI begin to use joypad info {}'.format(result))
            waitingForReply = True
        if waitingForReply == True:
            while self.ser.inWaiting() == 0:
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply control Received{}'.format(dataReceived))

    def prova1(self):
        self.cprova1();

    cdef cmon_go(self,val):
        cdef char* command0 = '<switchMod0>'
        cdef char* command1 = '<switchMod1>'
        cdef char* command2 = '<switchMod2>'
        cdef char* command3 = '<switchMod3>'
        cdef char* dataReceived = ''
        cdef bint waiting_for_reply = False
        if not waiting_for_reply:
            if val==0:
                self.send_to_arduino(command0)
                print('Sent from Raspi cmon_gotutta {} '.format(command0))
            elif val==1:
                self.send_to_arduino(command1)
                print('Sent from Raspi cmon_gotutta {} '.format(command1))
            elif val==2:
                self.send_to_arduino(command2)
                print('Sent from Raspi cmon_gotutta {} '.format(command2))
            elif val==3:
                self.send_to_arduino(command3)
                print('Sent from Raspi cmon_gotutta {}' .format(command3))
            waiting_for_reply = True
        if waiting_for_reply:
            while self.ser.inWaiting() == 0:            
                pass
            dataReceived = self.receive_from_arduino()
            print('Reply Received take one cmon_gotutta {}'.format(dataReceived))

    def cmon_gotutta(self,val):
        self.cmon_go(val)