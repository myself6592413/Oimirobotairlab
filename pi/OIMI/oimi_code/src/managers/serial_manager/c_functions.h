#include <iostream>
#include <stdio.h>
#include <string.h>
#include <cstring>
#include <cstdlib>
#include <bits/stdc++.h>

using namespace std;

char* compareMsg(char* name){
    char * res = new char;
    if(strcmp(name,"turnmov")==0)
        res = "turn1";
    else if(strcmp(name,"leftmov")==0)
        res = "left1";
    else if(strcmp(name,"leftmov2")==0)
        res = "left2";
    else if(strcmp(name,"rightmov")==0)
        res = "right1";
    else if(strcmp(name,"rightmov2")==0)
        res = "right2";
    else if(strcmp(name,"frontmov")==0)
        res = "front1";
    else if(strcmp(name,"backmov")==0)
        res = "back1";
    else if(strcmp(name,"nopat")==0)
        res = "nopat";
    else if(strcmp(name,"transpat")==0)
        res = "trapat";
    else if(strcmp(name,"blopat")==0)
        res = "blo";
    else if(strcmp(name,"alpat")==0)
        res = "alpa";
    else if(strcmp(name,"albapat")==0)
        res = "albpa";
    else if(strcmp(name,"changepat")==0)
        res = "cpat";
    else if(strcmp(name,"leftpat")==0)
        res = "lpat";
    else if(strcmp(name,"frontpat")==0)
        res = "fpat";
    else if(strcmp(name,"rightpat")==0)
        res = "rpat";
    else if(strcmp(name,"backpat")==0)
        res = "bpat";
    else res = "notFound";

    return res;
}

char * createMsg(char * comm, float strafe,float forward, float angular){
    string str_str = std::to_string(strafe);
    string for_str = std::to_string(forward);
    string ang_str = std::to_string(angular);
    string resp = comm;
    string res;
    res.append("<").append(resp).append(",");
    res.append(str_str).append(",").append(for_str);
    res.append(",").append(ang_str).append(">");

    int n = res.length();

    char char_array[n + 1];     // declaring character array
    //char * result;
    //result = new char;
    char * result = new char;
    // copying the contents of the
    // string to char array
    strcpy(char_array, res.c_str());
    result = char_array;
    return result;
}

char * fromStringToBytes(string str){
    int n = str.length();

    char char_array[n + 1];     // declaring character array
    char * result;
    result = new char;
    // copying the contents of the
    // string to char array
    strcpy(char_array, str.c_str());
    result = char_array;
    return result;
}