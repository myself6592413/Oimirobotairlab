using namespace std;
//the order matters!
//enum class global_status {beginning, busy, waiting, moving, playing1, playing2, testing1, inserting1, playing2, testing2, inserting2, doing_nothing, feeling, reacting};
enum class global_status {beginning, busy, waiting, moving, 
	playing1, playing2, testing1, testing2, inserting1, inserting2, feeling, reacting, doing_nothing};