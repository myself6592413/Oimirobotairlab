# ===============================================================================================================
# Cython directives
# ===============================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ===============================================================================================================
#  Imports:
# ===============================================================================================================
from shared cimport *

led_dict = {"joystick": b'<a>', "autonomous": b'<a>', "following": b'<f>', "turn1": b'<t>', "nopat": b'<m>', "noled": b'<m>',
            "happiness":b'<c7>',"agree":b'<c1>',"disagree":b'<c2>',"sadness":b'<c3>',"angriness":b'<c4>',"enthusiasm":b'<c5>',"scare": b'<c6>',
            "left1": b'<le>', "front1": b'<fr>', "right1":b'<ri>',"back1": b'<ba>',
            "leftmov": b'<le>', "frontmov": b'<fr>', "rightmov":b'<ri>',"backmov": b'<ba>',
            "blo":b'<p>',"alpa": b'<u>', "alba": b'<k>', "cpat":b'<cp>', 
            "lpat":b'<lp>',"rpat": b'<rp>', "trapat": b'<tp>', "fpat":b'<fp>', 
            "bpat": b'<bp>',"stop_game":b'<q>',"pattern":b'<c>'}

def get_key_from_elem(val): 
    for key, value in led_dict.items(): 
        if val == value:
            return key 
# ===============================================================================================================
#  Classes:
# ===============================================================================================================
cdef class PyGlobalStatus:
    cdef global_status oimiglobalstatus
    #def __cinit__(self, int val):
    def __cinit__(self, char* valchar):
        if valchar.decode()=="beginning":
            val = 0
        elif valchar.decode()=="busy":
            val = 1
        elif valchar.decode()=="waiting":
            val = 2
        elif valchar.decode()=="moving":
            val = 3
        elif valchar.decode()=="playing1":
            val = 4
        elif valchar.decode()=="playing2":
            val = 5
        elif valchar.decode()=="testing1":
            val = 6
        elif valchar.decode()=="testing2":
            val = 7
        elif valchar.decode()=="inserting1":
            val = 8
        elif valchar.decode()=="inserting2":
            val = 9
        elif valchar.decode()=="feeling":
            val = 10
        elif valchar.decode()=="reacting":
            val = 11
        elif valchar.decode()=="doing_nothing":
            val = 12
        self.oimiglobalstatus = <global_status> val

    def get_globalstatus(self):
        cdef sta = {<int>beginning:"beginning", <int>busy:"busy",
                    <int>waiting:"waiting", <int>moving:"moving",
                    <int>playing1:"playing1", <int> testing1:"playing2",
                    <int>playing2:"testing1", <int> testing2:"testing2",
                    <int>inserting1:"inserting1", <int>inserting2:"inserting2",
                    <int>feeling:"feeling", <int>reacting:"reacting",                    
                    <int>doing_nothing:"doing_nothing"}
        return sta[<int>self.oimiglobalstatus]

    def __reduce__(self):
        data = self.get_globalstatus()
        return (rebuild, (data,))
# ===============================================================================================================
#  Methods:
# ===============================================================================================================
cpdef object rebuild(data): #for multiprocessing pickle global status object
    data = data.encode()
    c = PyGlobalStatus(data)
    return c