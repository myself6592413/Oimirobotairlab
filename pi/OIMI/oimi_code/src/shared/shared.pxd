# avoid to put pyglobalstatus here
# the order is fundamental!!
cdef extern from "c_status.h":
	cdef cppclass global_status:
		pass
cdef extern from "c_status.h" namespace "global_status":
	cdef global_status beginning
	cdef global_status busy
	cdef global_status waiting
	cdef global_status moving 				#ok just one cause there s lock unique event 
	cdef global_status playing1
	cdef global_status playing2 			# in others without lock I need to distinguish between single functions, we will see in future
	cdef global_status testing1
	cdef global_status testing2
	cdef global_status inserting1
	cdef global_status inserting2
	cdef global_status feeling
	cdef global_status reacting
	cdef global_status doing_nothing

cpdef object rebuild(data)
