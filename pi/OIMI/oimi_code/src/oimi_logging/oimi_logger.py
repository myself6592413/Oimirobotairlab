#!/phome/pi/oimi-ven/bin/python3.5
# -*- coding: utf-8 -*-
"""Oimi Logger:
	
	Three distinct loggers
		-for ordinary info to display on terminal  
		-for python modules (error,info,warning) for both terminal and logfile
		-for cython modules to retrieve subprocess name (with extra keyword)
"""
# ===============================================================================================================
#  Imports:
# ===============================================================================================================
import os 
import datetime
import simplelogging
import logging
from pathlib import Path
from pythonjsonlogger import jsonlogger
# ===============================================================================================================
#  Log objects:
# ===============================================================================================================
CY_FORMAT = '%(asctime)s %(module)s %(process)d %(levelname)s:%(message)s'  # Defines Formatters with most useful info, for specifying the layout of log records
PY_FORMAT = "%(process)d - %(threadName)s - %(asctime)s - %(filename)s:%(lineno)s %(funcName)s %(levelname)s:%(message)s"
CONSOLE_FORMAT = ("%(log_color)s%(asctime)s [%(levelname)-2s] %(filename)20s(%(lineno)3s):%(funcName)-5s %(message)s%(reset)s")

simlog = simplelogging.get_logger(console_format=CONSOLE_FORMAT, console_level=simplelogging.INFO) 	# Defines a logger for most important info that use highlighted msgs 

BASE_DIR = os.path.abspath(os.getcwd()) 									# Returns the current working directory of a process.
file_json = os.path.join(BASE_DIR, 'oimi-log.json')

logging.root.setLevel(logging.NOTSET) 										# Sets the root level to None, instead of default one that is 'WARNING', 
																			# ...otherwise some msgs might be lost 
logger = logging.getLogger('error-log')
logger2 = logging.getLogger('oimi-log2') 									# Defines two records' objects, hence a different name is needed

logHandler = logging.StreamHandler()										# An Handler sends log records to the appropriate destination
log2Handler = logging.StreamHandler()										# Creates stream for standard output (terminal) 
jsonHandler = logging.FileHandler(filename=file_json)						# Creates another stream just for the logfile

logHandler.setLevel(logging.INFO)											# Logging mgs (of same logger obj) less severe than ERRROR will be ignored
log2Handler.setLevel(logging.INFO) 											# Puts the threshold for this logger to INFO
jsonHandler.setLevel(logging.ERROR)											# Puts the threshold for this logger to ERROR
jsonHandler.setLevel(logging.WARNING)										# Puts the threshold for this logger to WARNING

formatter = jsonlogger.JsonFormatter(PY_FORMAT)								# Returns a new instance of the Json Formatter, specifying desired format
formatter2 = jsonlogger.JsonFormatter(PY_FORMAT)
formatterJ = jsonlogger.JsonFormatter()

logHandler.setFormatter(formatterJ)											# Establishes the proper fomatter for a specific handler 
jsonHandler.setFormatter(formatter)
log2Handler.setFormatter(formatter2)

logger.addHandler(logHandler)												# Includes the specified handler to logger for printing both on terminal and logfile
logger.addHandler(jsonHandler)
logger2.addHandler(log2Handler) 											# Includes the specified handler to logger for printing only in terminal
