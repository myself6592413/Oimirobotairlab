"""
Oimi robot create report (one for any kid) that contains all kid informations, stats and game results. 
"""
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os
import pickle
import re 
import itertools
from webcolors import name_to_rgb
from datetime import datetime
import pandas as pd
import numpy as np
from numpy import nan
import sqlite3
import matplotlib.pyplot as plt
from fpdf import FPDF
from math import pi, sin, cos, floor, ceil
from pylab import title, figure, xlabel, ylabel, xticks, bar, legend, axis, savefig
from matplotlib.ticker import MaxNLocator
import generate_report_tables as grt
if '/home/pi/OIMI/oimi_code//src/managers/database_manager/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code//src/managers/database_manager/')
import database_manager as db
import warnings
# ==========================================================================================================================================================
# Variables
# ==========================================================================================================================================================
warnings.filterwarnings("ignore")
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
def replace_every_nth_occurrance(ss, sub, repl, nth):
    find = ss.find(sub)
    i = 1                                            
    while find = -1:                                   # loop util we find no match
        if i == nth:                                    # if i  is equal to nth we found nth matches so replace
            ss = ss[:find]+repl+ss[find + len(sub):]
            i = 0
        find = ss.find(sub, find + len(sub) + 1)        # find + len(sub) + 1 means we start after the last match  
        i += 1
    return ss

def percentage(part, whole):
    if whole==0:
        return 0
    else:
        return round(part/whole,2)
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
class PDF(FPDF):
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Pivotal
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    __instance = None
    def __new__(cls):
        if cls.__instance is None:
            cls.__instance = super(PDF,cls).__new__(cls)
            cls.__instance.__initialized = False
        return cls.__instance

    def __init__(self):
        if(self.__initialized): 
            return
        self.__initialized = True        
        super(PDF,self).__init__()

        self.db_conn = db.DatabaseManager()
        child = self.db_conn.execute_a_query("SELECT kid_id, name, surname FROM Kids JOIN Kids_Sessions USING(kid_id) ORDER BY session_id DESC LIMIT 1")
        self.id_child = child[0]
        self.name_child = child[1]
        self.surname_child = child[2]

        self.legends=''
        self.wLegend=''
        self.summation=0
        self.NbVal=0

    def __enter__(self):
        print("Creating Report for current kid...")
        print("Extraction all necessary kid data from db...")
        print("Setting photo and notes...")
        print("Witing all games' results...")
        print("Generating tables and graphs...")
        self.set_author('Oimi')

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("OK Done.")
        print("Report PDF doc created successfully.")
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Shared
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    def header(self):
        title = "Complete Report of {} {}".format(self.name_child, self.surname_child)
        self.tit = title
        self.set_title(self.tit)
        self.add_font('Recursive_VF_1.078', '', './fonts/Recursive_VF_1.078.ttf', uni=True)     #add and use a new font
        self.set_font('Recursive_VF_1.078', '', 12)
        wi = self.get_string_width(title) + 15                                                  #calculate title width and position
        self.set_x((210 - wi) / 2)                                                              #set abscissa pos looking at title length
        self.set_draw_color(*name_to_rgb('DarkSlateGray'))                                      #frame color
        self.set_fill_color(*name_to_rgb('LightCyan'))                                          #background color 
        self.set_text_color(*name_to_rgb('darkblue'))                                           #text color
        self.set_line_width(1)                                                                  #specify the thickness of frame (1 mm)
        self.cell(wi, 9, title, 1, 1, 'C', 1)                                                   #create container title 'C' = center text alignment
        self.ln(10)                                                                             #set line break 10

    def footer(self):
        self.set_y(-15)                                                         #set the position from bottom --> 1.5 cm 
        self.set_font('Helvetica', 'I', 8)                                      #choose font,format,size for next cell = Helvetica italic 8
        self.set_text_color(128)                                                #define text color in gray
        self.cell(0, 10, 'Page {}'.format(str(self.page_no())) , 0, 0, 'C')     #set dynamic page number
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Special
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    def sector(self, xc, yc, r, a, b, style='FD', cw=True, o=90):    
        d0 = a - b
        if cw:
            d = b
            b = o - a
            a = o - d
        else:
            b += o
            a += o
        
        while a<0:
            a += 360
        while a>360:
            a -= 360
        while b<0:
            b += 360
        while b>360:
            b -= 360
        if a > b:
            b += 360
        b = b/360*2*pi
        a = a/360*2*pi
        d = b - a
        if d == 0 and d0 = 0:
            d = 2*pi
        k = self.k
        hp = self.h
        if sin(d/2):
            myArc = 4/3*(1-cos(d/2))/sin(d/2)*r
        else:
            myArc = 0
        #first put the center
        self._out('%.2F %.2F m' % ((xc)*k,(hp-yc)*k))
        #put the first point
        self._out('%.2F %.2F l' % ((xc+r*cos(a))*k,((hp-(yc-r*sin(a)))*k)))
        #draw the arc
        if d < pi/2:
            self.sector_arc(xc+r*cos(a)+myArc*cos(pi/2+a),
                        yc-r*sin(a)-myArc*sin(pi/2+a),
                        xc+r*cos(b)+myArc*cos(b-pi/2),
                        yc-r*sin(b)-myArc*sin(b-pi/2),
                        xc+r*cos(b),
                        yc-r*sin(b)
                        )
        else:
            b = a + d/4
            myArc = 4/3*(1-cos(d/8))/sin(d/8)*r
            self.sector_arc(xc+r*cos(a)+myArc*cos(pi/2+a),
                        yc-r*sin(a)-myArc*sin(pi/2+a),
                        xc+r*cos(b)+myArc*cos(b-pi/2),
                        yc-r*sin(b)-myArc*sin(b-pi/2),
                        xc+r*cos(b),
                        yc-r*sin(b)
                        )
            a = b
            b = a + d/4
            self.sector_arc(xc+r*cos(a)+myArc*cos(pi/2+a),
                        yc-r*sin(a)-myArc*sin(pi/2+a),
                        xc+r*cos(b)+myArc*cos(b-pi/2),
                        yc-r*sin(b)-myArc*sin(b-pi/2),
                        xc+r*cos(b),
                        yc-r*sin(b)
                        )
            a = b
            b = a + d/4
            self.sector_arc(xc+r*cos(a)+myArc*cos(pi/2+a),
                        yc-r*sin(a)-myArc*sin(pi/2+a),
                        xc+r*cos(b)+myArc*cos(b-pi/2),
                        yc-r*sin(b)-myArc*sin(b-pi/2),
                        xc+r*cos(b),
                        yc-r*sin(b)
                        )
            a = b
            b = a + d/4
            self.sector_arc(xc+r*cos(a)+myArc*cos(pi/2+a),
                        yc-r*sin(a)-myArc*sin(pi/2+a),
                        xc+r*cos(b)+myArc*cos(b-pi/2),
                        yc-r*sin(b)-myArc*sin(b-pi/2),
                        xc+r*cos(b),
                        yc-r*sin(b)
                        )
        #terminate drawing
        if style=='F':
            op='f'
        elif style=='FD' or style=='DF':
            op='b'
        else:
            op='s'
        self._out(op)
    
    def sector_arc(self, x1, y1, x2, y2, x3, y3 ):
        h = self.h
        self._out('%.2F %.2F %.2F %.2F %.2F %.2F c' %
            (x1*self.k,
            (h-y1)*self.k,
            x2*self.k,
            (h-y2)*self.k,
            x3*self.k,
            (h-y3)*self.k))
    
    def pie_chart(self, w, h, data, format, colors=None):
        self.set_font('Helvetica', '', 10)
        self.set_legends(data,format)

        xPage = self.get_x()
        yPage = self.get_y()
        margin = 2
        hLegend = 5
        radius = min(w - margin * 4 - hLegend - self.wLegend, h - margin * 2)
        radius = floor(radius / 2)
        xDiag = xPage + margin + radius
        yDiag = yPage + margin + radius
        if colors == None: 
            for i in range(0,self.NbVal):
                gray = i * int(255 / self.NbVal)
                colors[i] = (gray,gray,gray)
            
        #sectors
        self.set_line_width(0.2)
        angleStart = 0
        angleEnd = 0
        i = 0
        for val in data.values(): 
            angle = (val * 360) / float(self.summation)
            if angle = 0: 
                angleEnd = angleStart + angle
                self.set_fill_color(colors[i][0],colors[i][1],colors[i][2])
                self.sector(xDiag, yDiag, radius, angleStart, angleEnd)
                angleStart += angle
            
            i+=1

        #Legends
        self.set_font('Helvetica', '', 10)
        x1 = xPage + 2 * radius + 4 * margin
        x2 = x1 + hLegend + margin
        y1 = yDiag - radius + (2 * radius - self.NbVal*(hLegend + margin)) / 2
        for i in range (0, self.NbVal):
            self.set_fill_color(colors[i][0],colors[i][1],colors[i][2])
            self.rect(x1, y1, hLegend, hLegend, 'DF')
            self.set_xy(x2,y1)
            self.cell(0,hLegend,self.legends[i])
            y1+=hLegend + margin
          
    def bar_diagram(self, w, h, data, format, color=None, maxVal=0, nbDiv=4):
        self.set_font('Helvetica', '', 10)
        self.set_legends(data,format)

        xPage = self.get_x()
        yPage = self.get_y()
        margin = 2
        yDiag = yPage + margin
        hDiag = floor(h - margin * 2)
        xDiag = xPage + margin * 2 + self.wLegend
        lDiag = floor(w - margin * 3 - self.wLegend)
        if color == None:
            color=(155,155,155)
        if maxVal == 0: 
            maxVal = max(data.values())
        
        valIndRepere = ceil(float(maxVal) / float(nbDiv))
        maxVal = valIndRepere * nbDiv
        lRepere = floor(lDiag / nbDiv)
        lDiag = lRepere * nbDiv
        unit = lDiag / maxVal
        hBar = floor(hDiag / (self.NbVal + 1))
        hDiag = hBar * (self.NbVal + 1)
        eBaton = floor(hBar * 80 / 100)

        self.set_line_width(0.2)
        self.rect(xDiag, yDiag, lDiag, hDiag)

        self.set_font('Helvetica', '', 10)
        self.set_fill_color(color[0],color[1],color[2])
        i=0
        for val in data.values(): 
            #Bar
            xval = xDiag
            lval = int(val * unit)
            yval = yDiag + (i + 1) * hBar - eBaton / 2
            hval = eBaton
            self.rect(xval, yval, lval, hval, 'DF')
            #Legend
            self.set_xy(0, yval)
            self.cell(xval - margin, hval, self.legends[i],0,0,'R')
            i+=1
        
        #Scales
        for i in range(0, nbDiv): 
            xpos = xDiag + lRepere * i
            self.line(xpos, yDiag, xpos, yDiag + hDiag)
            val = i * valIndRepere
            xpos = xDiag + lRepere * i - self.get_string_width(str(val)) / 2
            ypos = yDiag + hDiag - margin
            self.text(xpos, ypos, str(val))
            
    def set_legends(self, data, format):
        self.legends=[]
        self.wLegend=0
        self.summation=sum(data.values())
        self.NbVal=len(data)
        for l, val in zip(data.keys(),data.values()):
            p=('%.2f' % (val/self.summation*100)) + '%'
            legend=format.replace('%l',str(l)).replace('%v', str(val)).replace('%p', str(p))
            self.legends.append(legend)
            self.wLegend=max(self.get_string_width(legend),self.wLegend)
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Chapters
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    def make_chapter_title(self, num, chap_title):
        self.set_font('Helvetica', '', 12)                                      #choose font,format,size for next cell = Helvetica None 12
        self.set_fill_color(200, 220, 255)                                      
        self.cell(0, 6, 'Chapter %d : %s' % (num, chap_title), 0, 0, 'L', 1)    #create chap title
        self.ln(0)

    def make_chapter_body_first(self, file_d, file_n):
        self.ln(3)
        with open(file_d, 'rb') as fh:                      #open, read kid file data
            txt = fh.read().decode('utf-8')     
        with open(file_n, 'rb') as fh:                      #open, read kid file notes
            txt1 = fh.read().decode('utf-8') 
        self.set_font('Courier', '', 12)                    #choose font,format,size for next cell = Times None 12
        self.multi_cell(0, 5, txt, 0)                       #justify output text (no border)
        self.ln()# Line break
    
        prof_img_path = './pics/{}_{}_profile.png'.format(self.name_child,self.surname_child)
        if os.path.exists(prof_img_path):
            self.image(prof_img_path,160,40,40)    #(img file path; abscissa ; ordinate ; size)
        else:
            self.image('./pics/empty_profile.png',160,40,40)    #(img file path; abscissa ; ordinate ; size)
        
        sym_kid = self.db_conn.execute_new_query("SELECT symptom_name FROM Kids_Symptoms WHERE kid_id = ?",(self.id_child))
        need_kid = self.db_conn.execute_param_query("SELECT * FROM Kids_Needs JOIN Needs USING(need_id) WHERE kid_id = ?",(self.id_child))
        impo_kid = self.db_conn.execute_param_query("SELECT imposition_name FROM Kids_Impositions WHERE kid_id = ?",(self.id_child))
        impo_tmp = [item for sublist in impo_kid for item in sublist]
        if not impo_tmp:
            impos = 'None'
        else:
            impos = ' , '.join(impo_tmp)
        try:
            needs_ok = [[j for j in i if j='Invalid'] for i in need_kid]
            for sublist in needs_ok:
                del sublist[0]
                del sublist[0]
            resp = [item for sublist in needs_ok for item in sublist]
            it = iter(resp)
            need_def_tmp = [' : '.join(s) for s in zip(it,it)]
            need_def = ' ; '.join(need_def_tmp)
        except:
            needs_ok = [i for i in need_kid if i='Invalid']
            del needs_ok[0]
            del needs_ok[0]
            if 'no_focus' in needs_ok:
                need_def = 'No focus in particular'
            else:
                need_def = ' : '.join(needs_ok)
        
        info_kids_from_db = """Details:\n  - Usual symptoms and bad behaviours = {} \n  - Possible focus and hints for next sessions: \n    {} \n  - Suggested imposition for next sessions: \n    {} """.format(' - '.join(sym_kid),need_def, impos)
        self.multi_cell(0,  6, info_kids_from_db, 1, 0)  #(length, height, txt, border, ln = pose of next : 0 = cell to the right (if ln=1 remove subsequent line ln() ))

        self.ln(0)                                          #with argument = 0 is placed linked with previous table
        self.add_font('DejaVuSans', '', './fonts/DejaVuSans.ttf', uni=True)
        self.set_font('DejaVuSans', '',10)                            #choose font,format,size for next cell= Italics None 
        self.set_fill_color(173, 216, 230)
        self.multi_cell(0, 10, txt1, 1, 0, fill=True)        #fill = consider background color
        self.ln()                                           #if called without argument next cell is insert automaticatilly using current ln (one of the multicell in this case)
        pdf_path = './kids_final_reports/Report_{}_{}.pdf'.format(self.name_child,self.surname_child)
        if os.path.exists(pdf_path):                 #check if report already exists
            siz = os.stat(pdf_path).st_size          #calculate size of file
            siz = siz >> 10
        else:
            siz = 4
        today = datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")              #get current datetime
        self.set_font('Times', 'I', 8)                                       #choose font,format,size for next cell= Italics None , if font isnt specified use current in use
        self.cell(0, 0, 
        '[Last update: {} {} File_size: {} Kb]'.format(today, 
        '                                                                                                                                                                         ', siz))
        self.ln(50)
        self.set_font('ZapfDingbats', '', 12)  #choose font,format,size for next cell = Times None 12
        self.cell(0,0,"QWERTYUIOPASDFGHJKLZXCVBNM")
    
    def make_chapter_body_second(self):
        self.set_font('', 'I',8) #choose font,format,size for next cell= Italics None 
        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? ORDER BY kid_id ASC LIMIT 18",(self.id_child))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd

        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")
        self.ln()                                   #if called without argument next cell is insert automaticatilly immediately after the previuos one
        self.set_font('', 'B',8)                    #choose font,format,size for next cell= Italics None 
        self.multi_cell(0,  5, result, 1, 'L', 0)   #(length, height, txt, border, ln = pose of next : 0 = cell to the right (if ln=1 remove subsequent line ln() ))        
        ##################################################################################################################################################################
        a1 = 'Max num of matches in a session'
        a2 = 'Max num of questions in a game'
        a3 = 'Max num of games in a match'
        a4 = 'Max score reached_game'
        a5 = 'Max score reached_match'
        a6 = 'Max score reached_question'
        a7 = 'Max score reached_session'

        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?,?,?,?,?) ",(self.id_child,a1,a2,a3,a4,a5,a6,a7))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd

        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")
        index = result.find(' Max score reached_game')
        result = result[:index] + '\n' + result[index:]

        self.ln(0)                             
        self.set_font('', 'B',8)          
        self.multi_cell(0,  5, result, 0, 'L', 0)
        ##################################################################################################################################################################
        a1 = 'Min time question'
        a2 = 'Min time game'
        a3 = 'Min time match'
        a4 = 'Min time session'

        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?,?) ",(self.id_child,a1,a2,a3,a4))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        
        achi_whole[-1] = int(achi_whole[-1])
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        
        res = replace_every_nth_occurrance(dd, ':', ';', 2)
        ee = res.replace(";"," s;")
        result = ee.replace(":","==>")

        self.ln(0)
        self.set_font('', 'B',8)
        self.multi_cell(0,  4, result, 1, 'L', 0)
        ##################################################################################################################################################################
        a1 = 'Num of easy session done'
        a2 = 'Num of hard session done'
        a3 = 'Num of medium session done'
        a4 = 'Num of normal session done'

        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?,?) ",(self.id_child,a1,a2,a3,a4))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd

        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")
        
        index = result.find(' Num of medium session done')
        result = result[:index] + '\n' + result[index:]

        self.ln(0)
        self.set_font('', 'B',8)
        self.multi_cell(0,  6, result, 0, 'L', 0)
        ##################################################################################################################################################################
        a0 = 'Num of easy_1 games done'
        a1 = 'Num of easy_2 games done'
        a2 = 'Num of hard_1 games done'
        a3 = 'Num of hard_2 games done'
        a4 = 'Num of medium_1 games done'
        a5 = 'Num of medium_2 games done'
        a6 = 'Num of normal_1 games done'
        a7 = 'Num of normal_2 games done'

        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?,?,?,?,?,?) ",(self.id_child,a0,a1,a2,a3,a4,a5,a6,a7))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd

        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")
        index = result.find(' Num of hard_2 games done')
        index1 = result.find(' Num of normal_2 games done')
        result = result[:index] + '\n' + result[index:]
        result = result[:index1] + '\n' + result[index1:]

        self.ln(0)
        self.set_font('', 'B',8)
        self.multi_cell(0,  6, result, 0, 'L', 0)
        ##################################################################################################################################################################
        a0 = 'Num of easy_1 matches done'
        a1 = 'Num of easy_2 matches done'
        a2 = 'Num of hard_1 matches done'
        a3 = 'Num of hard_2 matches done'
        a4 = 'Num of medium_1 matches done'
        a5 = 'Num of medium_2 matches done'
        a6 = 'Num of normal_1 matches done'
        a7 = 'Num of normal_2 matches done'

        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?,?,?,?,?,?) ",(self.id_child,a0,a1,a2,a3,a4,a5,a6,a7))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd

        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")
        index = result.find(' Num of hard_2 matches done ==>')
        index1 = result.find('Num of normal_2 matches done ==>')
        result = result[:index] + '\n' + result[index:]
        result = result[:index1] + '\n' + result[index1:]

        self.ln(0)
        self.set_font('', 'B',8)
        self.multi_cell(0,  6, result, 0, 'L', 0)
        ##################################################################################################################################################################
        a0 = 'Num of games with questions in ASC order of difficulty'
        a1 = 'Num of games with questions in CASU order of difficulty'
        a2 = 'Num of games with questions in DESC order of difficulty'
        a3 = 'Num of games with questions in SAME order of difficulty'

        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?,?) ",(self.id_child,a0,a1,a2,a3))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd
        
        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")
        index = result.find(' Num of games with questions in DESC order of difficulty')
        result = result[:index] + '\n' + result[index:]

        self.ln(0)                                           
        self.set_font('', 'B',8)                            
        self.multi_cell(0,  6, result, 1, 'L', 0)
        ##################################################################################################################################################################
        a0 = 'Num of matches with games in ASC order of difficulty'
        a1 = 'Num of matches with games in CASU order of difficulty'
        a2 = 'Num of matches with games in DESC order of difficulty'
        a3 = 'Num of matches with games in SAME order of difficulty'
        
        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?,?) ",(self.id_child,a0,a1,a2,a3))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd
        
        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")



        index = result.find(' Num of matches with games in DESC order of difficulty')
        result = result[:index] + '\n' + result[index:]

        self.ln(0)    
        self.set_font('', 'B',8)
        self.multi_cell(0,  6, result, 1, 'L', 0)
        ##################################################################################################################################################################        
        a0 = 'Num of sessions with matches in ASC order of difficulty'
        a1 = 'Num of sessions with matches in CASU order of difficulty'
        a2 = 'Num of sessions with matches in DESC order of difficulty'
        a3 = 'Num of sessions with matches in SAME order of difficulty'
        
        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?,?) ",(self.id_child,a0,a1,a2,a3))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd
        
        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")
        index = result.find(' Num of sessions with matches in DESC order of difficulty')
        result = result[:index] + '\n' + result[index:]


        self.ln(0)                             
        self.set_font('', 'B',8)         
        self.multi_cell(0,  6, result, 1, 'L', 0)
        ##################################################################################################################################################################
        a0 = 'Num of matches with games with ASC quantity of questions'
        a1 = 'Num of matches with games with CASU quantity of questions'
        a2 = 'Num of matches with games with DESC quantity of questions'
        a3 = 'Num of matches with games with SAME quantity of questions'
        
        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?,?) ",(self.id_child,a0,a1,a2,a3))
        
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 10
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd
        
        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")
        index = result.find(' Num of matches with games with DESC quantity of questions')
        result = result[:index] + '\n' + result[index:]
        
        self.ln(0)
        self.set_font('', 'B',8)
        self.multi_cell(0,  6, result, 0, 'L', 0)
        ##################################################################################################################################################################        
        ##################################################################################################################################################################
        a0 = 'Num of sessions with matches with ASC quantity of games'
        a1 = 'Num of sessions with matches with CASU quantity of games'
        a2 = 'Num of sessions with matches with DESC quantity of games'
        a3 = 'Num of sessions with matches with SAME quantity of games'
        a4 = 'Num of Mixed match tried'
        
        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?,?,?) ",(self.id_child,a0,a1,a2,a3,a4))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]
        
        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st
        
        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd
    
        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")
        index = result.find(' Num of sessions with matches with ASC quantity of games')
        result = result[:index] + '\n' + result[index:]
        index = result.find(' Num of sessions with matches with CASU quantity of games')
        result = result[:index] + '\n' + result[index:]
        index = result.find(' Num of sessions with matches with DESC quantity of games')
        result = result[:index] + '\n' + result[index:]

      
        self.ln(0)
        self.set_font('', 'B',8)
        self.multi_cell(0,  6, result, 0, 'L', 0)
        ##################################################################################################################################################################
        a0 = 'Total game solved'
        a1 = 'Total questions answered'
        a2 = 'Total session completed'
        
        achi_kid = self.db_conn.execute_last_query("SELECT achievement_name, achievement_content FROM Kids_Achievements WHERE kid_id = ? AND achievement_name in (?,?,?) ",(self.id_child,a0,a1,a2))
        achi_whole = [item for sublist in achi_kid for item in sublist]
        list_size = 8
        achi_kid_def = [achi_whole[i:i+list_size] for i in range(0, len(achi_whole), list_size)]

        total_st = ''
        for i in achi_kid_def:
            the_st = '{}: \n' .format(i)
            total_st = total_st + the_st

        aa = total_st.replace("[","")
        bb = aa.replace("]","")
        cc = bb.replace(",",":")
        dd = cc.replace("'"," ")
        ee = dd
        
        res = replace_every_nth_occurrance(ee, ':', ';', 2)
        result = res.replace(":","==>")

        self.ln(0)                                           
        self.set_font('', 'B',8)                            
        self.multi_cell(0,  6, result, 1, 'L', 0)  

    def make_chapter_body_third(self):
        last_kid_sess = self.db_conn.execute_last_query("""SELECT session_id FROM Kids_Sessions WHERE kid_id = ? ORDER BY session_id DESC LIMIT 1""",(self.id_child))       
        last_kid_sess = last_kid_sess[0]

        query_take = """SELECT session_id,date_time,complexity,num_of_matches,order_difficulty_matches,order_quantity_games
            FROM Full_played_recap_giga WHERE kid_id = {} ORDER BY session_id DESC LIMIT 1""".format(self.id_child)
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        
        #df['session_id'] = np.floor(pd.to_numeric(df['session_id'],errors='coerce')).astype('Int64')
        #df['complexity'] = np.floor(pd.to_numeric(df['complexity'],errors='coerce')).astype('Int64')
        #df['num_of_matches'] = np.floor(pd.to_numeric(df['num_of_matches'],errors='coerce')).astype('Int64')
        #df['match_id'] = np.floor(pd.to_numeric(df['match_id'],errors='coerce')).astype('Int64')
        #df['m_difficulty'] = np.floor(pd.to_numeric(df['m_difficulty'],errors='coerce')).astype('Int64')
        #df['game_id'] = np.floor(pd.to_numeric(df['game_id'],errors='coerce')).astype('Int64')
        #df['g_difficulty'] = np.floor(pd.to_numeric(df['g_difficulty'],errors='coerce')).astype('Int64')
        #df['num_questions'] = np.floor(pd.to_numeric(df['num_questions'],errors='coerce')).astype('Int64')
        #df['question_id'] = np.floor(pd.to_numeric(df['question_id'],errors='coerce')).astype('Int64')

        df = df.applymap(str)
        
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][2] = 'session complexity'
        lol[0][3] = 'num of matches'
        lol[0][4] = 'order diff matches'
        lol[0][5] = 'order quan games'

        data = tuple(map(tuple, lol))

        self.set_font("Times", size=8)
        line_height = self.font_size * 5
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 7
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)
        ##################################################################################################################################
        query_take = """SELECT mandatory_imposition,mandatory_need,extra_time_tolerance,movements_allowed,body_enabled,stress_flag,led_brightness
            FROM Full_played_recap_giga WHERE kid_id = {} ORDER BY session_id DESC LIMIT 1""".format(self.id_child)
        df = pd.read_sql_query(query_take, self.db_conn.conn)

        df = df.applymap(str)
        
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][0] = 'mandatory impo'
        lol[0][1] = 'mandatory need'
        lol[0][2] = 'extra time tolerance'
        lol[0][3] = 'movements allowed'
        lol[0][4] = 'stress flag'
        lol[0][5] = 'led brightness'
        
        data = tuple(map(tuple, lol))

        self.set_font("Times", size=8)
        line_height = self.font_size * 2.5
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 7
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)
        ##################################################################################################################################
        query_take = """SELECT exhortations,repeat_question,repeat_intro,desiderable_time
            FROM Full_played_recap_giga WHERE kid_id = {} ORDER BY session_id DESC LIMIT 1""".format(self.id_child)
        df = pd.read_sql_query(query_take, self.db_conn.conn)

        df = df.applymap(str)
        
        
        
        lol = [df.columns.tolist()] + df.values.tolist()

        lol[0][1] = 'repeat question'
        lol[0][2] = 'repeat intro'
        lol[0][3] = 'desiderable time'
        
        data = tuple(map(tuple, lol))

        self.set_font("Times", size=8)
        line_height = self.font_size * 2.5
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 7
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)            
        ##################################################################################################################################
        query_take = """SELECT match_id,num_of_games,m_difficulty,order_difficulty_games,order_quantity_questions
            FROM Full_played_recap_giga WHERE kid_id = {} AND session_id = {}""".format(self.id_child,last_kid_sess)

        df = pd.read_sql_query(query_take, self.db_conn.conn)

        df = df.drop_duplicates()
        df = df.applymap(str)
        
        
        
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][1] = 'num of games'
        lol[0][2] = 'match difficulty'
        lol[0][3] = 'order diff games'
        lol[0][4] = 'order quan questions'

        data = tuple(map(tuple, lol))


        self.set_font("Times", size=8)
        line_height = self.font_size * 2.5
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 7
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)

        ##################################################################################################################################
        query_take = """SELECT game_id,g_difficulty,num_questions,kind,type,question_id,order_difficulty_questions
            FROM Full_played_recap_giga WHERE kid_id = {} AND session_id = {}""".format(self.id_child,last_kid_sess)
        df = pd.read_sql_query(query_take, self.db_conn.conn)

        df = df.applymap(str)
        
        
        
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][1] = 'game difficulty'
        lol[0][3] = 'game kind'
        lol[0][4] = 'game type'
        lol[0][6] = 'order diff questions'
        
        data = tuple(map(tuple, lol))

        self.set_font("Times", size=8)
        line_height = self.font_size * 2.5
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 7
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)    
        ########################################################################################################################################################
        self.ln()
        query_take = """SELECT rowid,match_id,num_answers_match,num_corrects_match,num_errors_match,num_indecisions_match,num_already_match,time_of_match
            FROM Full_played_summary WHERE kid_id = {} AND session_id = {}""".format(self.id_child,last_kid_sess)
        
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        
        df['rowid'] = np.floor(pd.to_numeric(df['rowid'],errors='coerce')).astype('Int64')
        df['match_id'] = np.floor(pd.to_numeric(df['match_id'],errors='coerce')).astype('Int64')
        df['num_answers_match'] = np.floor(pd.to_numeric(df['num_answers_match'],errors='coerce')).astype('Int64')
        df['num_corrects_match'] = np.floor(pd.to_numeric(df['num_corrects_match'],errors='coerce')).astype('Int64')
        df['num_errors_match'] = np.floor(pd.to_numeric(df['num_errors_match'],errors='coerce')).astype('Int64')
        df['num_indecisions_match'] = np.floor(pd.to_numeric(df['num_indecisions_match'],errors='coerce')).astype('Int64')
        df['num_already_match'] = np.floor(pd.to_numeric(df['num_already_match'],errors='coerce')).astype('Int64')
        df['time_of_match'] = np.floor(pd.to_numeric(df['time_of_match'],errors='coerce')).astype('Int64')

        df = df.applymap(str)
        df2 = df.sort_values(by=['rowid'])
        df = df.drop('rowid', 1)
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][1] = 'answers'
        lol[0][2] = 'corrects'
        lol[0][3] = 'errors'
        lol[0][4] = 'indecisions'
        lol[0][5] = 'already'
        lol[0][6] = 'time'
        
        data = tuple(map(tuple, lol))

        self.set_font("Times", size=10)
        line_height = self.font_size * 2.5
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 7
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)
        ########################################################################################################################################################
        self.ln()
        query_take = """SELECT rowid,game_id,num_answers_game,num_corrects_game,num_errors_game,num_indecisions_game,num_already_game,time_of_game
            FROM Full_played_summary WHERE kid_id = {} AND session_id = {}""".format(self.id_child,last_kid_sess)
        
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        
        df['rowid'] = np.floor(pd.to_numeric(df['rowid'],errors='coerce')).astype('Int64')
        df['game_id'] = np.floor(pd.to_numeric(df['game_id'],errors='coerce')).astype('Int64')
        df['num_answers_game'] = np.floor(pd.to_numeric(df['num_answers_game'],errors='coerce')).astype('Int64')
        df['num_corrects_game'] = np.floor(pd.to_numeric(df['num_corrects_game'],errors='coerce')).astype('Int64')
        df['num_errors_game'] = np.floor(pd.to_numeric(df['num_errors_game'],errors='coerce')).astype('Int64')
        df['num_indecisions_game'] = np.floor(pd.to_numeric(df['num_indecisions_game'],errors='coerce')).astype('Int64')
        df['num_already_game'] = np.floor(pd.to_numeric(df['num_already_game'],errors='coerce')).astype('Int64')
        df['time_of_game'] = np.floor(pd.to_numeric(df['time_of_game'],errors='coerce')).astype('Int64')

        df = df.applymap(str)
        df2 = df.sort_values(by=['rowid'])
        df = df.drop('rowid', 1)
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][1] = 'answers'
        lol[0][2] = 'corrects'
        lol[0][3] = 'errors'
        lol[0][4] = 'indecisions'
        lol[0][5] = 'already'
        lol[0][6] = 'time'
        
        data = tuple(map(tuple, lol))

        self.set_font("Times", size=10)
        line_height = self.font_size * 2.5
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 7
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)
        ########################################################################################################################################################
        self.ln()
        query_take = """SELECT rowid,question_id,num_answers_question,num_corrects_question,num_errors_question,num_indecisions_question,
        num_already_question, time_of_question 
            FROM Full_played_summary WHERE kid_id = {} AND session_id = {}""".format(self.id_child,last_kid_sess)
        
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        
        df['rowid'] = np.floor(pd.to_numeric(df['rowid'],errors='coerce')).astype('Int64')
        df['question_id'] = np.floor(pd.to_numeric(df['question_id'],errors='coerce')).astype('Int64')
        df['num_answers_question'] = np.floor(pd.to_numeric(df['num_answers_question'],errors='coerce')).astype('Int64')
        df['num_corrects_question'] = np.floor(pd.to_numeric(df['num_corrects_question'],errors='coerce')).astype('Int64')
        df['num_errors_question'] = np.floor(pd.to_numeric(df['num_errors_question'],errors='coerce')).astype('Int64')
        df['num_indecisions_question'] = np.floor(pd.to_numeric(df['num_indecisions_question'],errors='coerce')).astype('Int64')
        df['num_already_question'] = np.floor(pd.to_numeric(df['num_already_question'],errors='coerce')).astype('Int64')
        df['time_of_question'] = np.floor(pd.to_numeric(df['time_of_question'],errors='coerce')).astype('Int64')

        df = df.applymap(str)
        #df2 = df.sort_values(by=['rowid'])
        df = df.drop('rowid', 1)
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][1] = 'answers'
        lol[0][2] = 'corrects'
        lol[0][3] = 'errors'
        lol[0][4] = 'indecisions'
        lol[0][5] = 'already'
        lol[0][6] = 'time'
        
        data = tuple(map(tuple, lol))

        self.set_font("Times", size=10)
        line_height = self.font_size * 2.5
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 7
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)
        ##### feedbacks
        ########################################################################################################################################################        
        self.ln()
        query_take = """SELECT rowid,quantity_feedback_session,grade_feedback_session,value_feedback_session
            FROM Full_played_summary WHERE kid_id = {} AND session_id = {}""".format(self.id_child,last_kid_sess)
        
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        
        df['rowid'] = np.floor(pd.to_numeric(df['rowid'],errors='coerce')).astype('Int64')
        df['value_feedback_session'] = np.floor(pd.to_numeric(df['value_feedback_session'],errors='coerce')).astype('Int64')

        df = df.applymap(str)
        #df2 = df.sort_values(by=['rowid'])
        df = df.drop('rowid', 1)
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][0] = 'quantity feedback session'
        lol[0][1] = 'grade feedback session'
        lol[0][2] = 'value feedback session,'

        data = tuple(map(tuple, lol))

        self.set_font("Times", size=10)
        line_height = self.font_size * 3
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 4
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)

        #### put real value for choosing if ok or not
        if lol[1][0]=='None':
            self.image('./pics/red_emoticon.png',155,195,10)
        #if lol[1][1]=='None':
        #    self.image('./pics/yellow_emoticon.png',155,195,10)
        #if lol[1][2]=='<NA>':
        #    self.image('./pics/green_emoticon.png',155,195,10)

        ########################################################################################################################################################        
        self.ln()
        query_take = """SELECT rowid,quantity_feedback_match,grade_feedback_match,value_feedback_match
            FROM Full_played_summary WHERE kid_id = {} AND session_id = {}""".format(self.id_child,last_kid_sess)
        
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        
        df['rowid'] = np.floor(pd.to_numeric(df['rowid'],errors='coerce')).astype('Int64')
        df['value_feedback_match'] = np.floor(pd.to_numeric(df['value_feedback_match'],errors='coerce')).astype('Int64')

        df = df.applymap(str)
        #df2 = df.sort_values(by=['rowid'])
        df = df.drop('rowid', 1)
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][0] = 'quantity feedback match'
        lol[0][1] = 'grade feedback match'
        lol[0][2] = 'value feedback match,'

        data = tuple(map(tuple, lol))

        self.set_font("Times", size=10)
        line_height = self.font_size * 3
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 5
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)

        #### put real value for choosing if ok or not
        if lol[1][0]=='None':
            self.image('./pics/red_emoticon.png',130,222,10)
        #if lol[1][1]=='None':
        #    self.image('./pics/yellow_emoticon.png',140,222,10)
        #if lol[1][2]=='<NA>':
        #    self.image('./pics/green_emoticon.png',140,222,10)

        ########################################################################################################################################################        
        self.ln()
        query_take = """SELECT rowid,quantity_feedback_game,grade_feedback_game,value_feedback_game
            FROM Full_played_summary WHERE kid_id = {} AND session_id = {}""".format(self.id_child,last_kid_sess)
        
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        
        df['rowid'] = np.floor(pd.to_numeric(df['rowid'],errors='coerce')).astype('Int64')
        df['value_feedback_game'] = np.floor(pd.to_numeric(df['value_feedback_game'],errors='coerce')).astype('Int64')

        df = df.applymap(str)
        #df2 = df.sort_values(by=['rowid'])
        df = df.drop('rowid', 1)
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][0] = 'quantity feedback game'
        lol[0][1] = 'grade feedback game'
        lol[0][2] = 'value feedback game'

        data = tuple(map(tuple, lol))

        self.set_font("Times", size=10)
        line_height = self.font_size * 3
        lh_list = [] #list with proper line_height of each row
        col_width = self.epw / 5
        use_default_height = 0
        #create a list with size equal to rows 
        for row in data:
            for datum in row:
                word_list = datum.split()
                number_of_words = len(word_list) #count words
                if number_of_words>2: # name and city formed by 2 words like Los Angeles are ok)
                    use_default_height = 1
                    new_line_height = self.font_size * (number_of_words/2) # set a new bigger height that variate according to num of words 
            if not use_default_height:
                lh_list.append(line_height)
            else:
                lh_list.append(new_line_height)
                use_default_height = 0
        #create fpdf table passing also max_line_height
        for j,row in enumerate(data): 
            for datum in row:
                if j==0:
                    line_height = lh_list[j] #choose right height for current row
                    #pdf.set_fill_color(0, 0, 255)
                    #pdf.set_fill_color(*name_to_rgb('cadetblue'))
                    pdf.set_fill_color(*name_to_rgb('lightslategray'))
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size, fill=True)
                else:                    
                    line_height = lh_list[j] #choose right height for current row
                    self.multi_cell(col_width, line_height, datum, border=1,align='C',ln=3, 
                    max_line_height=self.font_size,)
            self.ln(line_height)

        #### put real value for choosing if ok or not
        if lol[1][0]=='None':
            self.image('./pics/red_emoticon.png',130,247,10)
        #if lol[1][1]=='None':
        #    self.image('./pics/yellow_emoticon.png',200,240,10)
        #if lol[1][2]=='<NA>':
        #    self.image('./pics/green_emoticon.png',200,240,10)

    def make_chapter_body_fourth(self):
        query_take = """SELECT rowid,num_answers_question,num_corrects_question,num_errors_question,num_indecisions_question,num_already_question,time_of_question
        FROM Full_played_summary WHERE kid_id = {} ORDER BY session_id ASC""".format(self.id_child)
        df = pd.read_sql_query(query_take, self.db_conn.conn)

        df['rowid'] = np.floor(pd.to_numeric(df['rowid'],errors='coerce')).astype('Int64')
        df['num_answers_question'] = np.floor(pd.to_numeric(df['num_answers_question'],errors='coerce')).astype('Int64')
        df['num_corrects_question'] = np.floor(pd.to_numeric(df['num_corrects_question'],errors='coerce')).astype('Int64')
        df['num_errors_question'] = np.floor(pd.to_numeric(df['num_errors_question'],errors='coerce')).astype('Int64')
        df['num_indecisions_question'] = np.floor(pd.to_numeric(df['num_indecisions_question'],errors='coerce')).astype('Int64')
        df['num_already_question'] = np.floor(pd.to_numeric(df['num_already_question'],errors='coerce')).astype('Int64')
        df['time_of_question'] = np.floor(pd.to_numeric(df['time_of_question'],errors='coerce')).astype('Int64')

        df = df.dropna() #drop rows with NAN values
        df2 = df.sort_values(by=['rowid'])
        lol = [df.columns.tolist()] + df.values.tolist()
        
        lol[0][1] = 'answers'
        lol[0][2] = 'corrects'
        lol[0][3] = 'errors'
        lol[0][4] = 'indecisions'
        lol[0][5] = 'already'
        lol[0][6] = 'time'

        data = tuple(map(tuple, lol))
        
        numplist = df2.to_numpy()
        tot_ques_done = list(range(1,len(numplist)+1))
        
        minimum_ele = min(tot_ques_done)
        maximum_ele = max(tot_ques_done)
        
        ######################################################################################################################################### 1
        yyy = [item[1] for item in numplist] #list ok
        
        ypoints = yyy
        xpoints = tot_ques_done

        #tranform tedious float values on axis with integer ---> FINALLY
        ax = plt.figure().gca()
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        ax = plt.figure().gca()
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))

        ax.set_facecolor('silver')
        
        plt.xlabel("Questions ")
        plt.ylabel("Given ans ")
        #ax.set_title(nome_kid,color='C0')
        ax.grid(True)
        ax.plot(xpoints,ypoints,label='answers question',linewidth=4.0,color = "b")
        ax.set_ylim(ymin=0) #always after the plot if placed before set the object in range (0,1)
        plt.legend()
        leg = plt.legend()
        # get the lines and texts inside legend box
        leg_lines = leg.get_lines()
        leg_texts = leg.get_texts()
        # bulk-set the properties of all lines and texts
        plt.setp(leg_lines, linewidth=4)
        plt.setp(leg_texts, fontsize='x-large')
        kid_first_graph_path = './pics/num_ans_{}_{}.png'.format(self.name_child,self.surname_child)
        savefig(kid_first_graph_path)
        pdf.image(kid_first_graph_path,5,36,80) #(name ; abscissa ; ordinate ; size)
        ######################################################################################################################################### 2
        perce = []
        coco = [item[2] for item in numplist]
        erer = [item[3] for item in numplist]
        for i in range(len(coco)):
            denominator = coco[i]+erer[i]
            perper = percentage(coco[i], denominator)
            perce.append(perper)

        yyy = perce 
        
        ypoints = yyy
        xpoints = tot_ques_done

        #tranform tedious float values on axis with integer ---> FINALLY
        ax = plt.figure().gca()
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        ax = plt.figure().gca()
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))

        ax.set_facecolor('silver')
        
        plt.xlabel("Questions ")
        plt.ylabel("Corresct ans ")
        #ax.set_title(nome_kid,color='C0')
        ax.grid(True)
        ax.plot(xpoints,ypoints,label='correct questions',linewidth=4.0,color = "g")
        ax.set_ylim(ymin=0)
        plt.legend()
        leg = plt.legend()
        # get the lines and texts inside legend box
        leg_lines = leg.get_lines()
        leg_texts = leg.get_texts()
        # bulk-set the properties of all lines and texts
        plt.setp(leg_lines, linewidth=4)
        plt.setp(leg_texts, fontsize='x-large')
        kid_second_graph_path = './pics/num_corr_{}_{}.png'.format(self.name_child,self.surname_child)
        savefig(kid_second_graph_path)
        pdf.image(kid_second_graph_path,95,36,80) #(name ; abscissa ; ordinate ; size)
        
        #########################################################################################################################################
        yyy = [item[3] for item in numplist] #list ok
        
        ypoints = yyy
        xpoints = tot_ques_done

        #tranform tedious float values on axis with integer ---> FINALLY
        ax = plt.figure().gca()
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        ax = plt.figure().gca()
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))

        ax.set_facecolor('silver')
        plt.xlabel("Questions ")
        plt.ylabel("Errors ")
        #ax.set_title(nome_kid,color='C0')
        ax.grid(True)
        ax.plot(xpoints,ypoints,label='wrong_questions',linewidth=4.0,color = "r")
        
        plt.legend()
        leg = plt.legend()
        # get the lines and texts inside legend box
        leg_lines = leg.get_lines()
        leg_texts = leg.get_texts()
        # bulk-set the properties of all lines and texts
        plt.setp(leg_lines, linewidth=4)
        plt.setp(leg_texts, fontsize='x-large')
        kid_third_graph_path = './pics/errors_done_{}_{}.png'.format(self.name_child,self.surname_child)
        savefig(kid_third_graph_path)
        pdf.image(kid_third_graph_path,5,100,80) #(name ; abscissa ; ordinate ; size)
        #########################################################################################################################################
        yyy = [item[4] for item in numplist] #list ok
        
        ypoints = yyy
        xpoints = tot_ques_done

        #tranform tedious float values on axis with integer ---> FINALLY
        ax = plt.figure().gca()
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        ax = plt.figure().gca()
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))

        ax.set_facecolor('silver')
        
        plt.xlabel("Questions ")
        plt.ylabel("Indecisions ")
        #ax.set_title(nome_kid,color='C0')
        ax.grid(True)
        ax.plot(xpoints,ypoints,label='indecisions',linewidth=4.0,color = "m")
        plt.legend()
        leg = plt.legend()
        # get the lines and texts inside legend box
        leg_lines = leg.get_lines()
        leg_texts = leg.get_texts()
        # bulk-set the properties of all lines and texts
        plt.setp(leg_lines, linewidth=4)
        plt.setp(leg_texts, fontsize='x-large')
        kid_fourth_graph_path = './pics/indecisions_{}_{}.png'.format(self.name_child,self.surname_child)
        savefig(kid_fourth_graph_path)
        pdf.image(kid_fourth_graph_path,95,100,80) #(name ; abscissa ; ordinate ; size)                

    def make_chapter_body_sixth(self):
        query_take = "SELECT * FROM ScoresTotalQuestion WHERE kid_id={}".format(self.id_child)
        df = pd.read_sql_query(query_take, self.db_conn.conn)

        df = df.applymap(str)
        df = df.dropna()

        df.drop(df.index[df['tot_answers'] == '<NA>'], inplace = True)
        df.drop(df.index[df['tot_corrects'] == '<NA>'], inplace = True)
        df.drop(df.index[df['tot_errors'] == '<NA>'], inplace = True)
        df.drop(df.index[df['tot_indecisions'] == '<NA>'], inplace = True)
        df.drop(df.index[df['tot_already'] == '<NA>'], inplace = True)
        df.drop(df.index[df['best_time'] == '<NA>'], inplace = True)
        df.drop(df.index[df['worst_time'] == '<NA>'], inplace = True)
        
        df['tot_answers'] = np.floor(pd.to_numeric(df['tot_answers'],errors='coerce')).astype('Int64')
        df['tot_corrects'] = np.floor(pd.to_numeric(df['tot_corrects'],errors='coerce')).astype('Int64')
        df['tot_errors'] = np.floor(pd.to_numeric(df['tot_errors'],errors='coerce')).astype('Int64')
        df['tot_indecisions'] = np.floor(pd.to_numeric(df['tot_indecisions'],errors='coerce')).astype('Int64')
        df['tot_already'] = np.floor(pd.to_numeric(df['tot_already'],errors='coerce')).astype('Int64')
        df['best_time'] = np.floor(pd.to_numeric(df['best_time'],errors='coerce')).astype('Int64')
        df['worst_time'] = np.floor(pd.to_numeric(df['worst_time'],errors='coerce')).astype('Int64')

        summa = []
        print("df")
        print(df)        
        total = df['tot_corrects'].sum()
        print("sum  ", total)
        summa.append(total)
        total = df['tot_errors'].sum()
        print("sum  ", total)
        summa.append(total)
        total = df['tot_indecisions'].sum()
        print("sum  ", total)
        summa.append(total)
        print("summa ", summa)
        
        bb = ['tot_corrects','tot_errors','tot_indecisions']
        wanted_lol = dict(zip(bb,summa))
        print("wanted_lol ", wanted_lol)

        #Pie chart
        self.set_font('Helvetica', 'BIU', 12)
        self.cell(0, 5, '1 - Pie chart', 0, 1)
        self.ln(8)

        self.set_font('Helvetica', '', 10)
        valX = self.get_x()
        valY = self.get_y()

        self.cell(30, 5, 'tot_corrects:')
        self.cell(15, 5, str(wanted_lol['tot_corrects']), 0, 0, 'R')
        self.ln()
        self.cell(30, 5, 'tot_errors:')
        self.cell(15, 5, str(wanted_lol['tot_errors']), 0, 0, 'R')
        self.ln()
        self.cell(30, 5, 'tot_indecisions:')
        self.cell(15, 5, str(wanted_lol['tot_indecisions']), 0, 0, 'R')
        self.ln()
        self.ln(8)

        self.set_xy(90, valY)
        col1=(100,100,255)
        col2=(255,100,100)
        col3=(255,255,100)
        self.pie_chart(100, 35, wanted_lol, '%l (%p)', (col1,col2,col3))
        self.set_xy(valX, valY + 40)


    def get_tot_scores_match(self):
        query_take = "SELECT * FROM ScoresTotalMatch WHERE kid_id={}".format(self.id_child)
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        df['tot_answers_match'] = np.floor(pd.to_numeric(df['tot_answers_match'],errors='coerce')).astype('Int64')
        df['tot_corrects_match'] = np.floor(pd.to_numeric(df['tot_corrects_match'],errors='coerce')).astype('Int64')
        df['tot_errors_match'] = np.floor(pd.to_numeric(df['tot_errors_match'],errors='coerce')).astype('Int64')
        df['tot_indecisions_match'] = np.floor(pd.to_numeric(df['tot_indecisions_match'],errors='coerce')).astype('Int64')
        df['tot_already_match'] = np.floor(pd.to_numeric(df['tot_already_match'],errors='coerce')).astype('Int64')
        df['tot_time_match'] = np.floor(pd.to_numeric(df['tot_time_match'],errors='coerce')).astype('Int64')
        df = df.applymap(str)

        data_in_lol = [df.columns.tolist()] + df.values.tolist()

        data_in_lol[0][2] = 'tot answers'
        data_in_lol[0][3] = 'tot corrects'
        data_in_lol[0][4] = 'tot errors'
        data_in_lol[0][5] = 'tot indecisions'
        data_in_lol[0][6] = 'tot already'
        data_in_lol[0][7] = 'tot time'

        return data_in_lol

    def get_tot_scores_kind_game(self):
        query_take = "SELECT * FROM ScoresTotalKindOfGame WHERE kid_id={}".format(self.id_child)
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        df['tot_answers_kind'] = np.floor(pd.to_numeric(df['tot_answers_kind'],errors='coerce')).astype('Int64')
        df['tot_corrects_kind'] = np.floor(pd.to_numeric(df['tot_corrects_kind'],errors='coerce')).astype('Int64')
        df['tot_errors_kind'] = np.floor(pd.to_numeric(df['tot_errors_kind'],errors='coerce')).astype('Int64')
        df['tot_indecisions_kind'] = np.floor(pd.to_numeric(df['tot_indecisions_kind'],errors='coerce')).astype('Int64')
        df['tot_already_kind'] = np.floor(pd.to_numeric(df['tot_already_kind'],errors='coerce')).astype('Int64')
        df['tot_max_time_kind'] = np.floor(pd.to_numeric(df['tot_max_time_kind'],errors='coerce')).astype('Int64')
        df['tot_min_time_kind'] = np.floor(pd.to_numeric(df['tot_min_time_kind'],errors='coerce')).astype('Int64')
        df = df.applymap(str)

        data_in_lol = [df.columns.tolist()] + df.values.tolist()

        data_in_lol[0][2] = 'tot answers'
        data_in_lol[0][3] = 'tot corrects'
        data_in_lol[0][4] = 'tot errors'
        data_in_lol[0][5] = 'tot indecisions'
        data_in_lol[0][6] = 'tot already'
        data_in_lol[0][7] = 'tot max time'
        data_in_lol[0][7] = 'tot min time'

        return data_in_lol

    def get_tot_scores_game(self):
        query_take = "SELECT * FROM ScoresTotalGame WHERE kid_id={}".format(self.id_child)
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        df['tot_answers_game'] = np.floor(pd.to_numeric(df['tot_answers_game'],errors='coerce')).astype('Int64')
        df['tot_corrects_game'] = np.floor(pd.to_numeric(df['tot_corrects_game'],errors='coerce')).astype('Int64')
        df['tot_errors_game'] = np.floor(pd.to_numeric(df['tot_errors_game'],errors='coerce')).astype('Int64')
        df['tot_indecisions_game'] = np.floor(pd.to_numeric(df['tot_indecisions_game'],errors='coerce')).astype('Int64')
        df['tot_already_game'] = np.floor(pd.to_numeric(df['tot_already_game'],errors='coerce')).astype('Int64')
        df['tot_time_game'] = np.floor(pd.to_numeric(df['tot_time_game'],errors='coerce')).astype('Int64')
        df = df.applymap(str)

        data_in_lol = [df.columns.tolist()] + df.values.tolist()

        data_in_lol[0][2] = 'tot answers'
        data_in_lol[0][3] = 'tot corrects'
        data_in_lol[0][4] = 'tot errors'
        data_in_lol[0][5] = 'tot indecisions'
        data_in_lol[0][6] = 'tot already'
        data_in_lol[0][7] = 'tot time'
        data_in_lol[0][8] = 'tot interruptions'

        return data_in_lol

    def get_tot_scores_question(self):
        query_take = "SELECT * FROM ScoresTotalQuestion WHERE kid_id={}".format(self.id_child)
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        df['tot_answers'] = np.floor(pd.to_numeric(df['tot_answers'],errors='coerce')).astype('Int64')
        df['tot_corrects'] = np.floor(pd.to_numeric(df['tot_corrects'],errors='coerce')).astype('Int64')
        df['tot_errors'] = np.floor(pd.to_numeric(df['tot_errors'],errors='coerce')).astype('Int64')
        df['tot_indecisions'] = np.floor(pd.to_numeric(df['tot_indecisions'],errors='coerce')).astype('Int64')
        df['tot_already'] = np.floor(pd.to_numeric(df['tot_already'],errors='coerce')).astype('Int64')
        df['best_time'] = np.floor(pd.to_numeric(df['best_time'],errors='coerce')).astype('Int64')
        df['worst_time'] = np.floor(pd.to_numeric(df['worst_time'],errors='coerce')).astype('Int64')
        df = df.applymap(str)

        data_in_lol = [df.columns.tolist()] + df.values.tolist()

        data_in_lol[0][2] = 'tot answers'
        data_in_lol[0][3] = 'tot corrects'
        data_in_lol[0][4] = 'tot errors'
        data_in_lol[0][5] = 'tot indecisions'
        data_in_lol[0][6] = 'tot already'
        data_in_lol[0][7] = 'tot time'

        return data_in_lol

    def get_tot_scores_session(self):
        query_take = "SELECT * FROM ScoresTotalSession WHERE kid_id={}".format(self.id_child)
        df = pd.read_sql_query(query_take, self.db_conn.conn)
        df['tot_answers_session'] = np.floor(pd.to_numeric(df['tot_answers_session'],errors='coerce')).astype('Int64')
        df['tot_corrects_session'] = np.floor(pd.to_numeric(df['tot_corrects_session'],errors='coerce')).astype('Int64')
        df['tot_errors_session'] = np.floor(pd.to_numeric(df['tot_errors_session'],errors='coerce')).astype('Int64')
        df['tot_indecisions_session'] = np.floor(pd.to_numeric(df['tot_indecisions_session'],errors='coerce')).astype('Int64')
        df['tot_already_session'] = np.floor(pd.to_numeric(df['tot_already_session'],errors='coerce')).astype('Int64')
        df['tot_time_session'] = np.floor(pd.to_numeric(df['tot_time_session'],errors='coerce')).astype('Int64')
        df = df.applymap(str)

        data_in_lol = [df.columns.tolist()] + df.values.tolist()

        data_in_lol[0][2] = 'tot answers'
        data_in_lol[0][3] = 'tot corrects'
        data_in_lol[0][4] = 'tot errors'
        data_in_lol[0][5] = 'tot indecisions'
        data_in_lol[0][6] = 'tot already'
        data_in_lol[0][7] = 'tot time'

        return data_in_lol           
    # ---------------------------------------------------------------------------------------------------------------------------------------------------------
    # Pages 
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    def publish_first_chapter(self, num, title, file0, file1):
        self.add_page()                         #add another page in the doc for this chapter
        self.make_chapter_title(num, title)     #put new chapter title
        self.make_chapter_body_first(file0, file1)    #put the content of chapter

    def publish_second_chapter(self, num, title):
        self.add_page()                         
        self.make_chapter_title(num, title)     
        self.ln(0)
        self.make_chapter_body_second()          

    def publish_third_chapter(self, num, title):
        self.add_page()                         
        self.make_chapter_title(num, title)
        self.ln(10)
        self.make_chapter_body_third()          

    def publish_fourth_chapter(self, num, title):
        self.add_page()                         
        self.make_chapter_title(num, title)
        self.ln(10)
        self.make_chapter_body_fourth()

    def publish_sixth(self, num, title):
        self.add_page()                         
        self.make_chapter_title(num, title)
        self.ln(10)
        self.make_chapter_body_sixth()
# ==========================================================================================================================================================
# Main
# ==========================================================================================================================================================
if __name__=="__main__":
    pdf = PDF()
    with PDF():
        pdf.publish_first_chapter(1, 'Overview', './kids_docs/Camillo_Golgi_data.txt','./kids_docs/Camillo_Golgi_notes.txt')
        pdf.publish_second_chapter(2, 'Kids Achievements')
        pdf.publish_third_chapter(3, 'Last Session Results')
        pdf.publish_fourth_chapter(4, 'Kid Session History and Improvements')
        ####################################################################### page 5
        pdf.add_page()
        pdf.make_chapter_title(5, 'Recap Kid games')
        ################### table 1
        pdf.ln()
        pdf.set_font("Times", size=10)
        lol = pdf.get_tot_scores_session()
        wanted_lol = {k: v for k, *v in zip(*lol)}

        grt.create_table(pdf=pdf, table_data = wanted_lol,title='Total Scores for every Sesssion played',align_header='L', align_data='L', cell_width=[15,15,15,15,15,20,15,13], emphasize_data=['1','3012'], emphasize_style='BIU',emphasize_color=(255,0,0))
        ################### table 2
        pdf.ln()
        pdf.set_font("Times", size=10)
        lol = pdf.get_tot_scores_match()
        wanted_lol = {k: v for k, *v in zip(*lol)}

        grt.create_table(pdf=pdf, table_data = wanted_lol,title='Total Scores for every Match played',align_header='L', align_data='L', cell_width=[15,15,15,15,15,20,15,13], emphasize_data=['1','3012'], emphasize_style='BIU',emphasize_color=(255,0,0))
        
        ####################################################################### page 6
        pdf.add_page()
        pdf.make_chapter_title(6, 'Recap Kid games')
        ################### table 3
        pdf.ln()
        pdf.set_font("Times", size=10)
        lol = pdf.get_tot_scores_game()
        wanted_lol = {k: v for k, *v in zip(*lol)}        

        grt.create_table(pdf=pdf, table_data = wanted_lol,title='Total Scores for every Game played',align_header='L', align_data='L', cell_width=[15,15,15,15,15,20,15,13,20], emphasize_data=['1','4000'], emphasize_style='BIU',emphasize_color=(255,0,0))
        ####################################################################### page 7
        pdf.add_page()
        pdf.make_chapter_title(7, 'Recap Kid games')
        ################### table 4
        pdf.ln()
        pdf.set_font("Times", size=10)
        lol = pdf.get_tot_scores_game()
        wanted_lol = {k: v for k, *v in zip(*lol)}        

        grt.create_table(pdf=pdf, table_data = wanted_lol,title='Total Scores for every Question played',align_header='L', align_data='L', cell_width=[15,15,15,15,15,20,15,13,20,15,16], emphasize_data=['1','10'], emphasize_style='BIU',emphasize_color=(255,0,0))
        
        ####################################################################### page 8
        pdf.add_page()
        pdf.make_chapter_title(8, 'Recap Kid games')
        ################### table 3
        pdf.ln()
        pdf.set_font("Times", size=10)
        lol = pdf.get_tot_scores_game()
        wanted_lol = {k: v for k, *v in zip(*lol)}        

        grt.create_table(pdf=pdf, table_data = wanted_lol,title='Total Scores for every Kind Game done',align_header='L', align_data='L', cell_width=[15,15,15,15,15,20,15,13,20,15,15], emphasize_data=['1','5'], emphasize_style='BIU',emphasize_color=(255,0,0))
        ####################################################################### page 9
        pdf.add_page()
        pdf.make_chapter_title(9, 'Recap Kid games')

        ################### table 4
        pdf.ln()
        pdf.set_font("Times", size=10)
        lol = pdf.get_tot_scores_game()
        wanted_lol = {k: v for k, *v in zip(*lol)}        

        grt.create_table(pdf=pdf, table_data = wanted_lol,title='Total Scores for every Type Game done',align_header='L', align_data='L', cell_width=[15,15,15,15,15,20,15,13,20,15,15], emphasize_data=['1','6'], emphasize_style='BIU',emphasize_color=(255,0,0))
      
        pdf.publish_sixth(6, 'Pie example to fix')

        pdf.output('./kids_final_reports/Report_Camillo_Golgi.pdf', 'F')