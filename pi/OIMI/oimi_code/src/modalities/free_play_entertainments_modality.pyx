#distutils: language=c++
#cython: language_level=3
"""Familiariz 
    metto global sstatus piu volte perché finche non gli arriva il comando preciso anche se sono in family mode non mi muovo cmq
    wait_ba and wait_bo: time that reaction go_led methods must wait before sending the "stop led" commands to Nano
    
"""
# ==================================================================================================================================================================
#  Imports:
# ==================================================================================================================================================================
#remove???
import asyncio 

import sys
if '/home/pi/OIMI/oimi_code/src/interacting/sound_playback/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/interacting/sounds_playback/')
if '/home/pi/OIMI/oimi_code/src/shared/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/shared/')
if '/home/pi/OIMI/oimi_code/src/managers/interrupts_manager/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/interrupts_manager/')
if '/home/pi/OIMI/oimi_code/src/playing/free_games/touchMe_entertainment_body_contact/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/playing/free_games/touchMe_entertainment_body_contact/')
if '/home/pi/OIMI/oimi_code/src/playing/free_games/approachMe_entertainment/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/playing/free_games/approachMe_entertainment/')
if '/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/data_acquisition/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/data_acquisition/')

#if '/home/pi/OIMI/oimi_code/src/managers/database_manager/' not in sys.path:
#    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/database_manager/')
#

import sounds_playback as sp
import shared as shar
import custom_exceptions as ce  
import moving.std_navigation.std_navigation as move
import serial_manager as ser
from subprocess import run
import subprocess
import reactions as rea
import time, random
import contact_manager as cm
import approachMe as am
import database_manager as db
import data_acquisition_mapping as dam
from oimi_process import OimiProcess, OimiStartProcess, OimiQueue, OimiManager
from multiprocessing import Process, Event
import serial_manager as serr
#import navigation.joystick_navigation.joystick_movement as jmove

# ==================================================================================================================================================================
#  Methods:
# ==================================================================================================================================================================
cdef extract_all_possible_substages(int kid_id, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb):
    #for now... until are not in the right place into the DATABASE ... then add 3 lines for extract id from db
    specific_quiet_Reaction_1 = 9056
    specific_quiet_Reaction_2 = 9056
    specific_touch_Reaction_1 = 9056
    
    if not kid_id:
        #with oimidb:
        #if random... metto in self!!!
        #oimidb.execute_a_query("DROP TABLE IF EXISTS Matches_Games") 
        Prompt_st_1 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "group_1_attention"''')
        Prompt_st_2 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "group_1_encourage"''')
        Prompt_st_3 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "group_1_greeting_start"''')
        Prompt_st_4 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "group_1_greeting_end"''')
        Reward_st_1 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "extent_1_positive"''')
        Reward_st_2 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "extent_1_negative"''')
        Reward_st_3 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "extent_1_game"''')
        Reward_st_4 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "extent_1_match"''')
        Reward_st_5 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "extent_1_session"''')
        Mood_st_1 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "emo_level_1_anger"''')
        Mood_st_2 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "emo_level_1_excitement"''')
        Mood_st_3 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "emo_level_1_happiness"''')
        Mood_st_4 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "emo_level_1_sadness"''')
        Mood_st_5 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "emo_level_1_surprise"''')
        Mood_st_6 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "emo_level_1_fear"''')
        do_Reactpos_st_1 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subpositive_1"''')
        do_Reactpos_st_2 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subpositive_2"''')
        do_Reactpos_st_3 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subpositive_3"''')
        do_Reactpos_st_4 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subpositive_4"''')
        do_Reactpos_st_5 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subpositive_5"''')
        do_Reactpos_st_6 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subpositive_6"''')
        do_Reactneg_st_1 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subnegative_1"''')
        do_Reactneg_st_2 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subnegative_2"''')
        do_Reactneg_st_3 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subnegative_3"''')
        do_Reactneg_st_4 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subnegative_4"''')
        do_Reactneg_st_5 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subnegative_5"''')
        do_Reactneg_st_6 = oimidb.execute_a_query('''SELECT substage_id FROM Substages WHERE sub_name = "subnegative_6"''')
    
        stage_quiet = rea.SubStage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        decision = random.randint(0,2)
        it_can_be1 = random.randint(0,4)
        or_it_can_be2 = random.randint(0,3)
        if decision==0:
            if or_it_can_be2==0:
                command_led_1 = "quiet1"
                command_led_2 = "quiet2"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==1:
                command_led_1 = "quiet2"
                command_led_2 = "quiet1"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==2:
                command_led_1 = "quiet2"
                command_led_2 = "quiet3"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==3:
                command_led_1 = "quiet3"
                command_led_2 = "quiet2"
                wait_ba = 5
                wait_bo = 5
        else:
            if it_can_be1==2:
                command_led_1 = "quiet2"
                command_led_2 = "quiet2"
                wait_ba = 5
                wait_bo = 5
            elif it_can_be1==3:
                command_led_1 = "quiet3"
                command_led_2 = "quiet3"
                wait_ba = 5
                wait_bo = 5
            else:
                command_led_1 = "quiet1"
                command_led_2 = "quiet1"
                wait_ba = 5
                wait_bo = 5
        #movem_to_do = "scare"
        movem_to_do = "quiet"
        audio_in_question = "old_quiet_hum_1"
        print()
        stage_quiet.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
    
        stage_caress = rea.SubStage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        decision = random.randint(0,2)
        it_can_be1 = random.randint(0,4)
        or_it_can_be2 = random.randint(0,3)
        if decision==0:
            if or_it_can_be2==0:
                command_led_1 = "caress1"
                command_led_2 = "caress2"
                wait_ba = 5
                wait_bo = 5                
            elif or_it_can_be2==1:
                command_led_1 = "caress2"
                command_led_2 = "caress1"
                wait_ba = 5
                wait_bo = 5                
            elif or_it_can_be2==2:
                command_led_1 = "caress2"
                command_led_2 = "caress3"
                wait_ba = 5
                wait_bo = 5                
            elif or_it_can_be2==3:
                command_led_1 = "caress3"
                command_led_2 = "caress2"
                wait_ba = 5
                wait_bo = 5                
            audio_in_question = "happy_celebration_1" 
            movem_to_do = "happy"
        else:
            if it_can_be1==2:
                command_led_1 = "caress2"
                command_led_2 = "caress2"
                wait_ba = 5
                wait_bo = 5
            elif it_can_be1==3:
                command_led_1 = "caress3"
                command_led_2 = "caress3"
                wait_ba = 5
                wait_bo = 5
            else:
                command_led_1 = "caress1"
                command_led_2 = "caress1"
                wait_ba = 5
                wait_bo = 5

            audio_in_question = "happy_celebration_2" 
            movem_to_do = "happy"
            #movem_to_do = "reactst_pos_2"

        
        stage_caress.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)

        stage_touch = rea.SubStage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        decision = random.randint(0,2)
        it_can_be1 = random.randint(0,4)
        or_it_can_be2 = random.randint(0,3)
        if decision==0:
            if or_it_can_be2==0:
                command_led_1 = "touch1"
                command_led_2 = "touch2"
                movem_to_do = "surp_mov1"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==1:
                command_led_1 = "touch2"
                command_led_2 = "touch1"
                movem_to_do = "surp_mov1"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==2:
                command_led_1 = "touch2"
                command_led_2 = "touch3"
                movem_to_do = "surp_mov2"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==3:
                command_led_1 = "touch3"
                command_led_2 = "touch2"
                movem_to_do = "surp_mov3"
                wait_ba = 5
                wait_bo = 5
            audio_in_question = "surpise_mood_2"    
        else:
            if it_can_be1==2:
                command_led_1 = "touch2"
                command_led_2 = "touch2"
                wait_ba = 5
                wait_bo = 5
            elif it_can_be1==3:
                command_led_1 = "touch3"
                command_led_2 = "touch3"
                wait_ba = 5
                wait_bo = 5
            else:
                command_led_1 = "touch1"
                command_led_2 = "touch1"
                wait_ba = 5
                wait_bo = 5
            audio_in_question = "surpise_mood_3" 
        stage_touch.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        
        stage_squeeze = rea.SubStage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        decision = random.randint(0,2)
        it_can_be1 = random.randint(0,4)
        or_it_can_be2 = random.randint(0,3)
        if decision==0:
            if or_it_can_be2==0:
                command_led_1 = "squeeze1"
                command_led_2 = "squeeze2"
                movem_to_do = "happy_mov1"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==1:
                command_led_1 = "squeeze2"
                command_led_2 = "squeeze1"
                movem_to_do = "happy_mov2"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==2:
                command_led_1 = "squeeze2"
                command_led_2 = "squeeze3"
                movem_to_do = "happy_mov2"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==3:
                command_led_1 = "squeeze3"
                command_led_2 = "squeeze2"
                movem_to_do = "happy_mov3"
                wait_ba = 5
                wait_bo = 5
            audio_in_question = "pleased_squeeze_3"
        else:
            if it_can_be1==2:
                command_led_1 = "squeeze2"
                command_led_2 = "squeeze2"
                wait_ba = 5
                wait_bo = 5
            elif it_can_be1==3:
                command_led_1 = "squeeze3"
                command_led_2 = "squeeze3"
                wait_ba = 5
                wait_bo = 5
            else:
                command_led_1 = "squeeze1"
                command_led_2 = "squeeze1"
                wait_ba = 5
                wait_bo = 5
            movem_to_do = "reactst_pos_5"
            audio_in_question = "happy_celebration_5"
        stage_squeeze.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)

        stage_choke = rea.SubStage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        decision = random.randint(0,2)
        it_can_be1 = random.randint(0,4)
        or_it_can_be2 = random.randint(0,3)
        if decision==0:
            if or_it_can_be2==0:
                command_led_1 = "choke1"
                command_led_2 = "choke2"
                movem_to_do = "sad_mov1"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==1:
                command_led_1 = "choke2"
                command_led_2 = "choke1"
                movem_to_do = "sad_mov2"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==2:
                command_led_1 = "choke2"
                command_led_2 = "choke3"
                movem_to_do = "sad_mov2"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==3:
                command_led_1 = "choke3"
                command_led_2 = "choke2"
                movem_to_do = "sad_mov3"
                wait_ba = 5
                wait_bo = 5
            audio_in_question = "scold_sad_hit_2" 
        else:
            if it_can_be1==2:
                command_led_1 = "choke2"
                command_led_2 = "choke2"
                wait_ba = 5
                wait_bo = 5
            elif it_can_be1==3:
                command_led_1 = "choke3"
                command_led_2 = "choke3"
                wait_ba = 5
                wait_bo = 5
            else:
                command_led_1 = "choke1"
                command_led_2 = "choke1"
                wait_ba = 5
                wait_bo = 5
            movem_to_do = "reactst_neg_1"
            audio_in_question = "scold_sad_hit_1"   
        stage_choke.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)

        stage_hug = rea.SubStage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        decision = random.randint(0,2)
        it_can_be1 = random.randint(0,4)
        or_it_can_be2 = random.randint(0,3)
        if decision==0:
            if or_it_can_be2==0:
                command_led_1 = "hug1"
                command_led_2 = "hug2"
                movem_to_do = "enthu_mov1"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==1:
                command_led_1 = "hug2"
                command_led_2 = "hug1"
                movem_to_do = "happy_mov1"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==2:
                command_led_1 = "hug2"
                command_led_2 = "hug3"
                movem_to_do = "happy_mov2"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==3:
                command_led_1 = "hug3"
                command_led_2 = "hug2"
                wait_ba = 5
                wait_bo = 5
                movem_to_do = "enthu_mov2"
            audio_in_question = "old_pleased_hug_2" 
        else:
            if it_can_be1==2:
                command_led_1 = "hug2"
                command_led_2 = "hug2"
                wait_ba = 5
                wait_bo = 5
            elif it_can_be1==3:
                command_led_1 = "hug3"
                command_led_2 = "hug3"
                wait_ba = 5
                wait_bo = 5
            else:
                command_led_1 = "hug1"
                command_led_2 = "hug1"
                wait_ba = 5
                wait_bo = 5
            movem_to_do = "reactst_pos_4"
            audio_in_question = "happy_celebration_4" #fix!!
        stage_hug.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)

        stage_shove = rea.SubStage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        #add type???
        decision = random.randint(0,2)
        it_can_be1 = random.randint(0,4)
        or_it_can_be2 = random.randint(0,3)
        if decision==0:
            if or_it_can_be2==0:
                command_led_1 = "shove1"
                command_led_2 = "shove2"
                wait_ba = 5
                wait_bo = 5
                movem_to_do = "sad_mov1"
            elif or_it_can_be2==1:
                command_led_1 = "shove2"
                command_led_2 = "shove1"
                wait_ba = 5
                wait_bo = 5
                movem_to_do = "sad_mov2"
            elif or_it_can_be2==2:
                command_led_1 = "shove2"
                command_led_2 = "shove3"
                wait_ba = 5
                wait_bo = 5
                movem_to_do = "sad_mov3"
            elif or_it_can_be2==3:
                command_led_1 = "shove3"
                command_led_2 = "shove2"
                wait_ba = 5
                wait_bo = 5
                movem_to_do = "anger_mov2"
            audio_in_question = "old_angry_moan_1" 
        else:
            if it_can_be1==2:
                command_led_1 = "shove2"
                command_led_2 = "shove2"
                wait_ba = 5
                wait_bo = 5
            elif it_can_be1==3:
                command_led_1 = "shove3"
                command_led_2 = "shove3"
                wait_ba = 5
                wait_bo = 5
            else:
                command_led_1 = "shove1"
                command_led_2 = "shove1"
                wait_ba = 5
                wait_bo = 5
            movem_to_do = "reactst_neg_4"
            audio_in_question = "scold_sad_hit_3" 
        stage_shove.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)

        stage_hit = rea.SubStage(1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        decision = random.randint(0,2)
        it_can_be1 = random.randint(0,4)
        or_it_can_be2 = random.randint(0,3)
        if decision==0:
            if or_it_can_be2==0:
                command_led_1 = "hit1"
                command_led_2 = "hit2"
                movem_to_do = "sad_mov1"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==1:
                command_led_1 = "hit2"
                command_led_2 = "hit1"
                movem_to_do = "anger_mov1"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==2:
                command_led_1 = "hit2"
                command_led_2 = "hit3"
                movem_to_do = "anger_mov2"
                wait_ba = 5
                wait_bo = 5
            elif or_it_can_be2==3:
                command_led_1 = "hit3"
                command_led_2 = "hit2"
                movem_to_do = "anger_mov3"
            audio_in_question = "scold_sad_hit_3" 
        else:
            if it_can_be1==2:
                command_led_1 = "hit2"
                command_led_2 = "hit2"
                wait_ba = 5
                wait_bo = 5
            elif it_can_be1==3:
                command_led_1 = "hit3"
                command_led_2 = "hit3"
                wait_ba = 5
                wait_bo = 5
            else:
                command_led_1 = "hit1"
                command_led_2 = "hit1"
                wait_ba = 5
                wait_bo = 5
            movem_to_do = "reactst_neg_2"
            audio_in_question = "moan_hit_1" 
        stage_hit.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)


        stage_Prompt_st_1 = rea.SubStage(Prompt_st_1[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Prompt_st_2 = rea.SubStage(Prompt_st_2[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Prompt_st_3 = rea.SubStage(Prompt_st_3[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Prompt_st_4 = rea.SubStage(Prompt_st_4[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reward_st_1 = rea.SubStage(Reward_st_1[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reward_st_2 = rea.SubStage(Reward_st_2[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reward_st_3 = rea.SubStage(Reward_st_3[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reward_st_4 = rea.SubStage(Reward_st_4[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reward_st_5 = rea.SubStage(Reward_st_5[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_1 = rea.SubStage(Mood_st_1[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_2 = rea.SubStage(Mood_st_2[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_3 = rea.SubStage(Mood_st_3[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_4 = rea.SubStage(Mood_st_4[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_5 = rea.SubStage(Mood_st_5[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_6 = rea.SubStage(Mood_st_6[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_1 = rea.SubStage(do_Reactpos_st_1[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_2 = rea.SubStage(do_Reactpos_st_2[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_3 = rea.SubStage(do_Reactpos_st_3[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_4 = rea.SubStage(do_Reactpos_st_4[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_5 = rea.SubStage(do_Reactpos_st_5[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_6 = rea.SubStage(do_Reactpos_st_6[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_1 = rea.SubStage(do_Reactneg_st_1[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_2 = rea.SubStage(do_Reactneg_st_2[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_3 = rea.SubStage(do_Reactneg_st_3[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_4 = rea.SubStage(do_Reactneg_st_4[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_5 = rea.SubStage(do_Reactneg_st_5[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_6 = rea.SubStage(do_Reactneg_st_6[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_quiet_Reaction_1 = rea.SubStage(specific_quiet_Reaction_1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_quiet_Reaction_2 = rea.SubStage(specific_quiet_Reaction_2, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_touch_Reaction_1 = rea.SubStage(specific_touch_Reaction_1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)

        stage_quiet_Reaction_1.subtype = "specific_quiet1"
        stage_quiet_Reaction_2.subtype = "specific_quiet2"
        stage_touch_Reaction_1.subtype = "specific_touch1"


        ##################################################################################################
        decision = random.randint(0,2)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "pratte"
            command_led_2 = "pratte"
            movem_to_do = "attention_mov1"
            wait_ba = 4
            wait_bo = 4
            if depends:
                audio_in_question = "old_quiet_hum_1" 
            else:
                audio_in_question = "old_quiet_hum_2" 
        elif decision==1:
            command_led_1 = "pratte2"
            command_led_2 = "pratte2"
            wait_ba = 8
            wait_bo = 8         
            movem_to_do = "attention_mov2"
            if depends:
                audio_in_question = "old_quiet_hum_1" 
            else:
                audio_in_question = "surprise_mood_2" 
        else:
            command_led_1 = "pratte3"
            command_led_2 = "pratte3"
            wait_ba = 12
            wait_bo = 12
            movem_to_do = "attention_mov3"
            if depends:
                audio_in_question = "old_pleased_touch_2" 
            else:
                audio_in_question = "surprise_mood_3"
        stage_Prompt_st_1.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        ###############################
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "prenco1"
            command_led_2 = "prenco1"
            wait_ba = 5
            wait_bo = 5            
            movem_to_do = "encourage_mov1"
            if depends:
                audio_in_question = "prompt_game_1"
            else:
                audio_in_question = "cheers_startgame_1" 
        if decision==1:
            command_led_1 = "prenco2"
            command_led_2 = "prenco2"
            wait_ba = 5
            wait_bo = 5            
            movem_to_do = "encourage_mov2"
            if depends:
                audio_in_question = "prompt_game_2" 
            else:
                audio_in_question = "cheers_startgame_1"                 
        else:
            command_led_1 = "prenco3"
            command_led_2 = "prenco3"            
            wait_ba = 5
            wait_bo = 5            
            movem_to_do = "encourage_mov3"
            if depends:
                audio_in_question = "prompt_game_1"
            else:
                audio_in_question = "congratulations_3" 
        stage_Prompt_st_2.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "prsta1"
            command_led_2 = "prsta1"
            wait_ba = 5
            wait_bo = 5            
            movem_to_do = "greetingst_mov1"
            if depends:
                audio_in_question = "greeting_start_2"
            else:
                audio_in_question = "cheers_startgame_2" 
        if decision==1:
            command_led_1 = "prsta2"
            command_led_2 = "prsta2"
            wait_ba = 5
            wait_bo = 5            
            movem_to_do = "greetingst_mov2"
            if depends:
                audio_in_question = "greeting_start_1"
            else:
                audio_in_question = "cheers_startgame_2" 
        else:
            command_led_1 = "prsta3"
            command_led_2 = "prsta3"
            wait_ba = 5
            wait_bo = 5            
            movem_to_do = "greetingst_mov3"
            if depends:
                audio_in_question = "old_cheers_startsession_2"
            else:
                audio_in_question = "cheers_startsession_3"      
        stage_Prompt_st_3.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "prend2"
            command_led_2 = "prend2"
            wait_ba = 5
            wait_bo = 5            
            movem_to_do = "greetingen_mov1"
            if depends:
                audio_in_question = "greeting_end_1"
            else:
                audio_in_question = "cheers_endgame_1"             
        if decision==1:
            command_led_1 = "prend2"
            command_led_2 = "prend2"
            wait_ba = 5
            wait_bo = 5            
            movem_to_do = "greetingen_mov2"
            if depends:
                audio_in_question = "greeting_end_1"
            else:
                audio_in_question = "cheers_endgame_1"             
        else:
            command_led_1 = "prend3"
            command_led_2 = "prend3"            
            wait_ba = 5
            wait_bo = 5            
            movem_to_do = "greetingen_mov3"
            if depends:
                audio_in_question = "greeting_end_3"
            else:
                audio_in_question = "greeting_end_4" 
        stage_Prompt_st_4.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        ##################################################################################################
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "correct1" 
            command_led_2 = "correct1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardright_mov1"
            if depends:
                audio_in_question = "congratulations_1"
            else:
                audio_in_question = "congratulations_2" 
        if decision==1:
            command_led_1 = "correct2" 
            command_led_2 = "correct2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardright_mov2"
            if depends:
                audio_in_question = "prompt_game_1"
            else:
                audio_in_question = "old_game_right_2" 
        else:            
            command_led_1 = "correct3" 
            command_led_2 = "correct3"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardright_mov3"
            if depends:
                audio_in_question = "old_game_right_4"
            else:
                audio_in_question = "old_game_right_3" 
        stage_Reward_st_1.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "wrong1"
            command_led_2 = "wrong1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardwrong_mov1"
            if depends:
                audio_in_question = "game_error_1"
            else:
                audio_in_question = "game_error_2" 
        if decision==1:
            command_led_1 = "wrong2"
            command_led_2 = "wrong2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardwrong_mov2"
            if depends:
                audio_in_question = "old_game_error_7"
            else:
                audio_in_question = "old_game_error_8" 
        else:            
            command_led_1 = "wrong3"
            command_led_2 = "wrong3"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardwrong_mov3"
            if depends:
                audio_in_question = "old_game_error_9"
            else:
                audio_in_question = "old_game_error_4" 
        stage_Reward_st_2.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "stgam1"
            command_led_2 = "stgam1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardgame_mov1"
            if depends:
                audio_in_question = "prompt_game_1"
            else:
                audio_in_question = "prompt_game_3" 
        if decision==1:
            command_led_1 = "stgam2"
            command_led_2 = "stgam2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardgame_mov2"
            if depends:
                audio_in_question = "prompt_game_2"
            else:
                audio_in_question = "prompt_game_1" 
        else:            
            command_led_1 = "stgam3"
            command_led_2 = "stgam3"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardgame_mov3"
            if depends:
                audio_in_question = "old_game_right_2"
            else:
                audio_in_question = "old_game_right_5" 
        stage_Reward_st_3.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "stmat1"
            command_led_2 = "stmat1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardmatch_mov1"
            if depends:
                audio_in_question = "old_cheers_endmatch_1"
            else:
                audio_in_question = "old_game_right_3" 
        if decision==1:
            command_led_1 = "stmat2"
            command_led_2 = "stmat2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardmatch_mov2"
            if depends:
                audio_in_question = "compliments_3"
            else:
                audio_in_question = "compliments_2" 
        else:            
            command_led_1 = "stmat3"
            command_led_2 = "stmat3"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardmatch_mov3"
            if depends:
                audio_in_question = "old_game_right_3"
            else:
                audio_in_question = "old_cheers_startmatch_1" 
        stage_Reward_st_4.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "stsess1"
            command_led_2 = "stsess1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardsess_mov1"
            if depends:
                audio_in_question = "old_cheers_endmatch_1"
            else:
                audio_in_question = "cheers_startsession_3" 
        if decision==1:
            command_led_1 = "stsess2"
            command_led_2 = "stsess2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardsess_mov2"
            if depends:
                audio_in_question = "old_game_right_2"
            else:
                audio_in_question = "old_greeting_onboot_2" 
        else:            
            command_led_1 = "stsess3"
            command_led_2 = "stsess3"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "rewardsess_mov3"
            if depends:
                audio_in_question = "old_greeting_onboot_2"
            else:
                audio_in_question = "cheers_endgame_1" 
        stage_Reward_st_5.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        ##################################################################################################
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "moodandgry1"
            command_led_2 = "moodandgry1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "anger_mov1"
            if depends:
                audio_in_question = "angry_mood_1"
            else:
                audio_in_question = "angry_mood_2"             
        if decision==1:
            command_led_1 = "moodandgry2"
            command_led_2 = "moodandgry2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "anger_mov2"
            if depends:
                audio_in_question = "angry_mood_2"
            else:
                audio_in_question = "angry_mood_4" 
        else:            
            command_led_1 = "moodandgry3"
            command_led_2 = "moodandgry3"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "anger_mov3"
            if depends:
                audio_in_question = "angry_mood_4"
            else:
                audio_in_question = "angry_mood_1" 
        stage_Mood_st_1.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "moodexcit1"
            command_led_2 = "moodexcit1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "enthu_mov1"
            if depends:
                audio_in_question = "happy_celebration_7"
            else:
                audio_in_question = "happy_celebration_2" 

        if decision==1:
            command_led_1 = "moodexcit2"
            command_led_2 = "moodexcit2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "enthu_mov2"
            if depends:
                audio_in_question = "happy_celebration_2"
            else:
                audio_in_question = "happy_celebration_3" 

        else:
            command_led_1 = "moodexcit3"
            command_led_2 = "moodexcit3"            
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "enthu_mov3"
            if depends:
                audio_in_question = "happy_celebration_5"
            else:
                audio_in_question = "happy_celebration_6" 

        stage_Mood_st_2.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "moodhappy1"
            command_led_2 = "moodhappy1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "happy_mov1"
            if depends:
                audio_in_question = "happy_celebration_1"
            else:
                audio_in_question = "old_pleased_hug_2" 
        if decision==1:
            command_led_1 = "moodhappy2"
            command_led_2 = "moodhappy2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "happy_mov2"
            if depends:
                audio_in_question = "happy_celebration_2"
            else:
                audio_in_question = "enthusiast_mood_1" 
        else:            
            command_led_1 = "moodhappy3"
            command_led_2 = "moodhappy3"            
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "happy_mov3"
            if depends:
                audio_in_question = "old_quiet_hum_1"
            else:
                audio_in_question = "pleased_mood_hug_3" 
        stage_Mood_st_3.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        
        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "moodsad1"
            command_led_2 = "moodsad1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "sad_mov1"
            if depends:
                audio_in_question = "scold_sad_hit_1"
            else:
                audio_in_question = "scold_sad_hit_2" 
        if decision==1:
            command_led_1 = "moodsad2"
            command_led_2 = "moodsad2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "sad_mov2"
            if depends:
                audio_in_question = "scold_sad_hit_3"
            else:
                audio_in_question = "sad_mood_1" 
        else:            
            command_led_1 = "moodsad3"
            command_led_2 = "moodsad3"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "sad_mov2"
            if depends:
                audio_in_question = "sad_mood_2"
            else:
                audio_in_question = "scold_sad_hit_3" 
        stage_Mood_st_4.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)

        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "moodsurp1"
            command_led_2 = "moodsurp1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "surp_mov1"
            if depends:
                audio_in_question = "surpise_mood_1"
            else:
                audio_in_question = "surpise_mood_2" 
        if decision==1:
            command_led_1 = "moodsurp2"
            command_led_2 = "moodsurp2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "surp_mov2"
            if depends:
                audio_in_question = "surpise_mood_1"
            else:
                audio_in_question = "surpise_mood_2" 
        else:            
            command_led_1 = "moodsurp3"
            command_led_2 = "moodsurp3"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "surp_mov3"
            if depends:
                audio_in_question = "surpise_mood_1"
            else:
                audio_in_question = "surpise_mood_2" 
        stage_Mood_st_5.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)

        decision = random.randint(0,3)
        depends = random.randint(0,1)
        if decision==0:
            command_led_1 = "moodscare1"
            command_led_2 = "moodscare1"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "scare_mov1"
            if depends:
                audio_in_question = "fear_mood_1"
            else:
                audio_in_question = "fear_mood_2" 
        if decision==1:
            command_led_1 = "moodscare2"
            command_led_2 = "moodscare2"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "scare_mov2"
            if depends:
                audio_in_question = "fear_mood_1"
            else:
                audio_in_question = "fear_mood_2" 
        else:            
            command_led_1 = "moodscare3"
            command_led_2 = "moodscare3"
            wait_ba = 5
            wait_bo = 5
            movem_to_do = "scare_mov3"
            if depends:
                audio_in_question = "fear_mood_1"
            else:
                audio_in_question = "fear_mood_2" 
        stage_Mood_st_6.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        #################################################################################################
        depends = random.randint(0,1)
        command_led_1 = "posreac1"
        command_led_2 = "posreac1"
        wait_ba = 5
        wait_bo = 5
        if depends:
            movem_to_do = "reactst_pos_1"
            audio_in_question = "old_pleased_touch_2"
        else:
            movem_to_do = "scare_mov3"
            audio_in_question = "surpise_mood_3" 
        stage_Reactpos_st_1.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)
        command_led_1 = "posreac2"
        command_led_2 = "posreac2"
        wait_ba = 5
        wait_bo = 5
        movem_to_do = "reactst_pos_2"
        movem_to_do = "scare_mov3"
        if depends:
            audio_in_question = "old_pleased_hug_1"
        else:
            audio_in_question = "happy_celebration_2" 
        stage_Reactpos_st_2.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)
        command_led_1 = "posreac3"
        command_led_2 = "posreac3"
        wait_ba = 5
        wait_bo = 5
        if depends:
            audio_in_question = "compliments_1"
            movem_to_do = "reactst_pos_3"
        else:
            audio_in_question = "old_pleased_hug_1" 
            movem_to_do = "scare_mov3"
        stage_Reactpos_st_3.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)
        command_led_1 = "posreac4"
        command_led_2 = "posreac4"
        wait_ba = 5
        wait_bo = 5
        if depends:
            audio_in_question = "pleased_caress_2"
            movem_to_do = "reactst_pos_4"
        else:
            audio_in_question = "old_pleased_caress_1" 
            movem_to_do = "scare_mov3"
        stage_Reactpos_st_4.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)
        command_led_1 = "posreac5"
        command_led_2 = "posreac5"
        wait_ba = 5
        wait_bo = 5
        if depends:
            audio_in_question = "pleased_squeeze_3"
            movem_to_do = "reactst_pos_5"
        else:
            audio_in_question = "happy_celebration_6" 
            movem_to_do = "scare_mov3"
        stage_Reactpos_st_5.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)
        command_led_1 = "posreac6"
        command_led_2 = "posreac6"
        wait_ba = 5
        wait_bo = 5
        if depends:
            audio_in_question = "pleased_mood_hug_3"
            movem_to_do = "scare_mov3"
        else:
            audio_in_question = "old_pleased_hug_1" 
            movem_to_do = "reactst_pos_6"
        stage_Reactpos_st_6.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)
        command_led_1 = "negreac1"
        command_led_2 = "negreac1"
        wait_ba = 5
        wait_bo = 5
        if depends:
            audio_in_question = "old_scold_sad_hit_4"
            movem_to_do = "reactst_neg_1"
        else:
            audio_in_question = "scold_sad_hit_1" 
            movem_to_do = "scare_mov3"
        stage_Reactneg_st_1.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)        
        command_led_1 = "negreac2"
        command_led_2 = "negreac2"
        wait_ba = 5
        wait_bo = 5
        if depends:
            audio_in_question = "old_scold_sad_hit_4"
            movem_to_do = "scare_mov3"
        else:
            audio_in_question = "scold_sad_hit_1"    
            movem_to_do = "reactst_neg_2"
        stage_Reactneg_st_2.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)
        command_led_1 = "negreac3"
        command_led_2 = "negreac3"
        wait_ba = 5
        wait_bo = 5
        if depends:
            audio_in_question = "old_scold_sad_hit_4"
            movem_to_do = "scare_mov3"
        else:
            audio_in_question = "scold_sad_hit_2" 
            movem_to_do = "reactst_neg_3"
        stage_Reactneg_st_3.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)
        command_led_1 = "negreac4"
        command_led_2 = "negreac4"
        wait_ba = 5
        wait_bo = 5
        if depends:
            audio_in_question = "scold_sad_hit_2"
            movem_to_do = "scare_mov3"
        else:
            audio_in_question = "scold_sad_hit_3" 
            movem_to_do = "reactst_neg_4"
        stage_Reactneg_st_4.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)
        command_led_1 = "negreac5"
        command_led_2 = "negreac5"
        wait_ba = 5
        wait_bo = 5
        if depends:
            audio_in_question = "scold_sad_hit_1"
            movem_to_do = "scare_mov3"
        else:
            audio_in_question = "old_scold_sad_hit_4" 
            movem_to_do = "reactst_neg_5"
        stage_Reactneg_st_5.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)
        depends = random.randint(0,1)
        command_led_1 = "negreac6"
        command_led_2 = "negreac6"
        wait_ba = 5
        wait_bo = 5
        if depends:
            audio_in_question = "scold_sad_hit_3"
            movem_to_do = "scare_mov3"
        else:
            audio_in_question = "scold_sad_hit_2" 
            movem_to_do = "reactst_neg_6"
        stage_Reactneg_st_6.set_substage_attr(command_led_1, command_led_2, movem_to_do, audio_in_question, wait_ba, wait_bo)

        stage_quiet_Reaction_1.initialize_substage(oimidb)
        stage_quiet_Reaction_2.initialize_substage(oimidb)
        stage_touch_Reaction_1.initialize_substage(oimidb)
        
        #0 stage_Prompt_st_1,
        #1 stage_Prompt_st_2,
        #2 stage_Prompt_st_3,
        #3 stage_Prompt_st_4,
        #4 stage_Reward_st_1,
        #5 stage_Reward_st_2,
        #6 stage_Reward_st_3,
        #7 stage_Reward_st_4,
        #8 stage_Reward_st_5,
        #9 stage_Mood_st_1,
        #10 stage_Mood_st_2,
        #11 stage_Mood_st_3,
        #12 stage_Mood_st_4,
        #13 stage_Mood_st_5,
        #14 stage_Mood_st_6,
        #15 stage_Reactpos_st_1,
        #16 stage_Reactpos_st_2,
        #17 stage_Reactpos_st_3,
        #18 stage_Reactpos_st_4,
        #19 stage_Reactpos_st_5,
        #20 stage_Reactpos_st_6,
        #21 stage_Reactneg_st_1,
        #22 stage_Reactneg_st_2,
        #23 stage_Reactneg_st_3,
        #24 stage_Reactneg_st_4,
        #25 stage_Reactneg_st_5,
        #26 stage_Reactneg_st_6
        #27 stage_quiet_Reaction_1
        #28 stage_quiet_Reaction_2
        #29 stage_touch_Reaction_1
    
        #30 stage_quiet
        #31 stage_touch
        #32 stage_caress
        #33 stage_squeeze
        #34 stage_hug
        #35 stage_hit
        #36 stage_shove
        #37 stage_choke

        substage_lists = [
            stage_Prompt_st_1,
            stage_Prompt_st_2,
            stage_Prompt_st_3,
            stage_Prompt_st_4,
            stage_Reward_st_1,
            stage_Reward_st_2,
            stage_Reward_st_3,
            stage_Reward_st_4,
            stage_Reward_st_5,
            stage_Mood_st_1,
            stage_Mood_st_2,
            stage_Mood_st_3,
            stage_Mood_st_4,
            stage_Mood_st_5,
            stage_Mood_st_6,
            stage_Reactpos_st_1,
            stage_Reactpos_st_2,
            stage_Reactpos_st_3,
            stage_Reactpos_st_4,
            stage_Reactpos_st_5,
            stage_Reactpos_st_6,
            stage_Reactneg_st_1,
            stage_Reactneg_st_2,
            stage_Reactneg_st_3,
            stage_Reactneg_st_4,
            stage_Reactneg_st_5,
            stage_Reactneg_st_6,
            stage_quiet_Reaction_1,
            stage_quiet_Reaction_2,
            stage_touch_Reaction_1,
            stage_quiet,
            stage_touch,
            stage_caress,
            stage_squeeze,
            stage_hug,
            stage_hit,
            stage_shove,
            stage_choke]

    else:
        #with oimidb:
        Prompt_st_1 = oimidb.execute_param_query('''SELECT permission_flag_Pr_st_1 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Prompt_st_2 = oimidb.execute_param_query('''SELECT permission_flag_Pr_st_2 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Prompt_st_3 = oimidb.execute_param_query('''SELECT permission_flag_Pr_st_3 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Prompt_st_4 = oimidb.execute_param_query('''SELECT permission_flag_Pr_st_4 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Reward_st_1 = oimidb.execute_param_query('''SELECT permission_flag_Rw_st_1 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Reward_st_2 = oimidb.execute_param_query('''SELECT permission_flag_Rw_st_2 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Reward_st_3 = oimidb.execute_param_query('''SELECT permission_flag_Rw_st_3 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Reward_st_4 = oimidb.execute_param_query('''SELECT permission_flag_Rw_st_4 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Reward_st_5 = oimidb.execute_param_query('''SELECT permission_flag_Rw_st_5 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Mood_st_1 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_1 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Mood_st_2 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_2 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Mood_st_3 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_3 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Mood_st_4 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_4 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Mood_st_5 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_5 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        Mood_st_6 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_6 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactpos_st_1 = oimidb.execute_param_query('''SELECT do_Rep_st_1 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactpos_st_2 = oimidb.execute_param_query('''SELECT do_Rep_st_2 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactpos_st_3 = oimidb.execute_param_query('''SELECT do_Rep_st_3 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactpos_st_4 = oimidb.execute_param_query('''SELECT do_Rep_st_4 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactpos_st_5 = oimidb.execute_param_query('''SELECT do_Rep_st_5 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactpos_st_6 = oimidb.execute_param_query('''SELECT do_Rep_st_6 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactneg_st_1 = oimidb.execute_param_query('''SELECT do_Ren_st_1 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactneg_st_2 = oimidb.execute_param_query('''SELECT do_Ren_st_2 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactneg_st_3 = oimidb.execute_param_query('''SELECT do_Ren_st_3 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactneg_st_4 = oimidb.execute_param_query('''SELECT do_Ren_st_4 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactneg_st_5 = oimidb.execute_param_query('''SELECT do_Ren_st_5 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))
        do_Reactneg_st_6 = oimidb.execute_param_query('''SELECT do_Ren_st_6 FROM Childre_Substages WHERE kid_id = ?''',(kid_id))

        #print("stampo trovati da database Childre_Substages!!!! ")
        #print("Prompt_st_1 ", Prompt_st_1)
        #print("Prompt_st_2 ", Prompt_st_2)
        #print("Prompt_st_3 ", Prompt_st_3)
        #print("Prompt_st_4 ", Prompt_st_4)
        #print("Reward_st_1 ", Reward_st_1)
        #print("Reward_st_2 ", Reward_st_2)
        #print("Reward_st_3 ", Reward_st_3)
        #print("Reward_st_4 ", Reward_st_4)
        #print("Reward_st_5 ", Reward_st_5)
        #print("Mood_st_1 ", Mood_st_1)
        #print("Mood_st_2 ", Mood_st_2)
        #print("Mood_st_3 ", Mood_st_3)
        #print("Mood_st_4 ", Mood_st_4)
        #print("Mood_st_5 ", Mood_st_5)
        #print("Mood_st_6 ", Mood_st_6)
        #print("do_Reactpos_st_1 ", do_Reactpos_st_1)
        #print("do_Reactpos_st_2 ", do_Reactpos_st_2)
        #print("do_Reactpos_st_3 ", do_Reactpos_st_3)
        #print("do_Reactpos_st_4 ", do_Reactpos_st_4)
        #print("do_Reactpos_st_5 ", do_Reactpos_st_5)
        #print("do_Reactpos_st_6 ", do_Reactpos_st_6)
        #print("do_Reactneg_st_1 ", do_Reactneg_st_1)
        #print("do_Reactneg_st_2 ", do_Reactneg_st_2)
        #print("do_Reactneg_st_3 ", do_Reactneg_st_3)
        #print("do_Reactneg_st_4 ", do_Reactneg_st_4)
        #print("do_Reactneg_st_5 ", do_Reactneg_st_5)
        #print("do_Reactneg_st_6 ", do_Reactneg_st_6)
        #print("tipooo do_Reactneg_st_6 ", print(type(do_Reactneg_st_6)))
        #print("tipooo do_Reactneg_st_6 ", print(type(do_Reactneg_st_6[0])))


        stage_Prompt_st_1 = rea.SubStage(Prompt_st_1[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Prompt_st_2 = rea.SubStage(Prompt_st_2[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Prompt_st_3 = rea.SubStage(Prompt_st_3[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Prompt_st_4 = rea.SubStage(Prompt_st_4[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reward_st_1 = rea.SubStage(Reward_st_1[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reward_st_2 = rea.SubStage(Reward_st_2[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reward_st_3 = rea.SubStage(Reward_st_3[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reward_st_4 = rea.SubStage(Reward_st_4[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reward_st_5 = rea.SubStage(Reward_st_5[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_1 = rea.SubStage(Mood_st_1[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_2 = rea.SubStage(Mood_st_2[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_3 = rea.SubStage(Mood_st_3[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_4 = rea.SubStage(Mood_st_4[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_5 = rea.SubStage(Mood_st_5[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Mood_st_6 = rea.SubStage(Mood_st_6[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_1 = rea.SubStage(do_Reactpos_st_1[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_2 = rea.SubStage(do_Reactpos_st_2[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_3 = rea.SubStage(do_Reactpos_st_3[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_4 = rea.SubStage(do_Reactpos_st_4[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_5 = rea.SubStage(do_Reactpos_st_5[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactpos_st_6 = rea.SubStage(do_Reactpos_st_6[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_1 = rea.SubStage(do_Reactneg_st_1[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_2 = rea.SubStage(do_Reactneg_st_2[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_3 = rea.SubStage(do_Reactneg_st_3[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_4 = rea.SubStage(do_Reactneg_st_4[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_5 = rea.SubStage(do_Reactneg_st_5[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_Reactneg_st_6 = rea.SubStage(do_Reactneg_st_6[0], event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_quiet_Reaction_1 = rea.SubStage(specific_quiet_Reaction_1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_quiet_Reaction_2 = rea.SubStage(specific_quiet_Reaction_2, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
        stage_touch_Reaction_1 = rea.SubStage(specific_touch_Reaction_1, event_main, qu_reaction, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)

        stage_quiet_Reaction_1.subtype = "specific_quiet1"
        stage_quiet_Reaction_2.subtype = "specific_quiet2"
        stage_touch_Reaction_1.subtype = "specific_touch1"

        stage_Prompt_st_1.initialize_substage(oimidb)
        stage_Prompt_st_2.initialize_substage(oimidb)
        stage_Prompt_st_3.initialize_substage(oimidb)
        stage_Prompt_st_4.initialize_substage(oimidb)
        stage_Reward_st_1.initialize_substage(oimidb)
        stage_Reward_st_2.initialize_substage(oimidb)
        stage_Reward_st_3.initialize_substage(oimidb)
        stage_Reward_st_4.initialize_substage(oimidb)
        stage_Reward_st_5.initialize_substage(oimidb)
        stage_Mood_st_1.initialize_substage(oimidb)
        stage_Mood_st_2.initialize_substage(oimidb)
        stage_Mood_st_3.initialize_substage(oimidb)
        stage_Mood_st_4.initialize_substage(oimidb)
        stage_Mood_st_5.initialize_substage(oimidb)
        stage_Mood_st_6.initialize_substage(oimidb)
        stage_Reactpos_st_1.initialize_substage(oimidb)
        stage_Reactpos_st_2.initialize_substage(oimidb)
        stage_Reactpos_st_3.initialize_substage(oimidb)
        stage_Reactpos_st_4.initialize_substage(oimidb)
        stage_Reactpos_st_5.initialize_substage(oimidb)
        stage_Reactpos_st_6.initialize_substage(oimidb)
        stage_Reactneg_st_1.initialize_substage(oimidb)
        stage_Reactneg_st_2.initialize_substage(oimidb)
        stage_Reactneg_st_3.initialize_substage(oimidb)
        stage_Reactneg_st_4.initialize_substage(oimidb)
        stage_Reactneg_st_5.initialize_substage(oimidb)
        stage_Reactneg_st_6.initialize_substage(oimidb)
        stage_quiet_Reaction_1.initialize_substage(oimidb)
        stage_quiet_Reaction_2.initialize_substage(oimidb)
        stage_touch_Reaction_1.initialize_substage(oimidb)
        
        #0 stage_Prompt_st_1,
        #1 stage_Prompt_st_2,
        #2 stage_Prompt_st_3,
        #3 stage_Prompt_st_4,
        #4 stage_Reward_st_1,
        #5 stage_Reward_st_2,
        #6 stage_Reward_st_3,
        #7 stage_Reward_st_4,
        #8 stage_Reward_st_5,
        #9 stage_Mood_st_1,
        #10 stage_Mood_st_2,
        #11 stage_Mood_st_3,
        #12 stage_Mood_st_4,
        #13 stage_Mood_st_5,
        #14 stage_Mood_st_6,
        #15 stage_Reactpos_st_1,
        #16 stage_Reactpos_st_2,
        #17 stage_Reactpos_st_3,
        #18 stage_Reactpos_st_4,
        #19 stage_Reactpos_st_5,
        #20 stage_Reactpos_st_6,
        #21 stage_Reactneg_st_1,
        #22 stage_Reactneg_st_2,
        #23 stage_Reactneg_st_3,
        #24 stage_Reactneg_st_4,
        #25 stage_Reactneg_st_5,
        #26 stage_Reactneg_st_6
        #27 stage_quiet_Reaction_1
        #28 stage_quiet_Reaction_2
        #29 stage_touch_Reaction_1
    
        substage_lists = [
        stage_Prompt_st_1,
        stage_Prompt_st_2,
        stage_Prompt_st_3,
        stage_Prompt_st_4,
        stage_Reward_st_1,
        stage_Reward_st_2,
        stage_Reward_st_3,
        stage_Reward_st_4,
        stage_Reward_st_5,
        stage_Mood_st_1,
        stage_Mood_st_2,
        stage_Mood_st_3,
        stage_Mood_st_4,
        stage_Mood_st_5,
        stage_Mood_st_6,
        stage_Reactpos_st_1,
        stage_Reactpos_st_2,
        stage_Reactpos_st_3,
        stage_Reactpos_st_4,
        stage_Reactpos_st_5,
        stage_Reactpos_st_6,
        stage_Reactneg_st_1,
        stage_Reactneg_st_2,
        stage_Reactneg_st_3,
        stage_Reactneg_st_4,
        stage_Reactneg_st_5,
        stage_Reactneg_st_6,
        stage_quiet_Reaction_1,
        stage_quiet_Reaction_2,
        stage_touch_Reaction_1]

    return substage_lists

def start_familiarize(global_status, event_startFamModality, event_feeling, event_reacting, event_autoMove, event_folloMove, event_Body, block_movement, stopInterrupt, fami_option, primary_lock,queue_lock, current_modality):
    """ Start familiarization mode"""
    #print("wewewewewWelcome to Familiarization mode")
    while True:
        event_startFamModality.wait()
        print("-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>start Familiarization begin")
        current_modality.put("familiarization")
        
        #sp.play_audio(b'bella')
        playthis = sp.sounds_dict.get("greeting_start_3","This sound doesn't exist!")
        print("playthis playthis playthis playthis reaction = = = ", playthis)
        print("playthis playthis playthis playthis reaction tipo tipo tipo = = = ", type(playthis))
        sp.play_audio(playthis)
        sp.play_audio(b'ti_aspettavo')
        kid_id = 0 ##change later...pass kid_id as parameter
        dispatcher(global_status, block_movement, event_feeling, event_reacting, event_autoMove, event_folloMove, event_Body, stopInterrupt, fami_option,  primary_lock, queue_lock)

cdef dispatcher(global_status, block_movement, event_feeling, event_reacting, event_autoMove, event_folloMove, event_Body, stopInterrupt, fami_option, primary_lock, queue_lock):
    cdef:
        char* lightthis = b''
    """ Dispatcher of familiarization mode """
    while True:
        next_command = fami_option.get()
        print("fami option is for now {}".format(next_command))
        '''era qui
        self.ser1, self.ser2, self.ser3 = serr.setup_serial()
        #ser1.await_arduinoMega()
        #ser2.await_arduinoNano1()
        #ser3.await_arduinoNano2()
        print("comincio ad aspettare seriali vari")
        self.ser1.await_arduino()
        self.ser2.await_arduino()
        self.ser3.await_arduino()        
        '''
        if next_command=='feel': #test singular reaction pattern
            coda = OimiQueue("Fifo coda")
            kid_id = 0
            event_main = Event()
            event_main.set()
            print("ho cliccato feel!!!")
            print("ho cliccato feel!!!")
            print("ho cliccato feel!!!")
            print("ho cliccato feel!!!")
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()            
            switchalert = b'<switchMod2>'
            ser2.transmit(switchalert) 
            ser3.transmit(switchalert)  
            oimidb = db.DatabaseManager()
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)

            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))

            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with reaction1:
            #    print("uga chaca")
            #old way: __enter__ with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            
            #with list of predefined stages
            right_reaction = exact_stages_list_required[0]
            right_reaction.stage_exhibition(stopInterrupt, primary_lock, queue_lock)
            #run("grab_arduino_mega_msg -c '<reset>' > /dev/null 2>&1", shell=True)
            ###veccchio superato
            #reaction1 = rea.Stage(1)
            #with reaction1:
            #    reaction1.stage_start_animation("prova")
            
            ##WRONGG
            """
            serMega, serNano, serNano2 = ser.setup_serial()
            serNano2.await_arduinoNano2()
            print("ok connection established")
            print("Enter in express feeling mode")
            move_type = "none"
            status = shar.PyGlobalStatus(b'feeling')
            print("status is {}".format(status.get_globalstatus()))
            global_status.put(status)
            #fun1(e3)
            #fun1_lock(primary_lock)
            lightthis = b'<hh>'
            #playThis = playthis
            serNano2.send_led(lightthis)
            playThis = sp.sounds_dict.get("no","This sound doesn't exists!")
            print("ìììììììììììììììììììììììììsiamo qui in movement patterni e playthis is {}".format(playThis))
            sp.play_audio(playThis)         
            """
            #aa = b'bella'
            #sp.play_audio(aa)
            
            #print("Enter in pattern_move!!")
            ##fun1(e5)
            ##move_type = "sadness"
            #move_type = "agree"
            ##move_type = "disagree"
            #status = shar.PyGlobalStatus(b'moving')
            #print("status is {}".format(status.get_globalstatus()))
            #global_status.put(status)
            ##fun1_lock(primary_lock)
            #move.move_pattern(global_status, stopInterrupt, move_type, primary_lock, queue_lock)
        elif next_command=='acquire_touch':
            status = shar.PyGlobalStatus(b'reacting')
            print("status is {}".format(status.get_globalstatus()))
            global_status.put(status)
            #cambio!!!
            #da rimettere main giusto!!!!
            #cm.contact_manager_main(stopInterrupt, primary_lock, queue_lock)
            #da togliere prova acquisizione!!!!
            
            #asyncio.run(cm.contact_manager_for_data_acquisition())
            
            #cm.contact_manager_for_data_acquisition() ok ! or otherwise,,,choosing csv to fill
            #dam.start_acquisition()
            dam.start_acquire_for_given_csv('hug')
            print("sono tornato in dispatcher of fmode di acquire_touch!!!")
        elif next_command=='autonomousme':
            kid_id = 0
            print("sono in approachme approachme approachme dopo avere estratto tutti gli stage")
            coda = OimiQueue("Fifo coda")
            event_main = Event()
            event_main.set()
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            switchalert = b'<switchMod1>'
            ser1.transmit(switchalert)
            #ser2.transmit(switchalert) #remove for now! 
            #ser3.transmit(switchalert)            
            adjust_dates = 0
            #oimidb = db.DatabaseManager(adjust_dates) #no!!! use the other constructor!!!!!
            oimidb = db.DatabaseManager()
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)
            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            #stage_main in __cinit__!!
            #with reaction1:
            #    print("uga chaca fami option")
            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))
            
            am.approachMe_main(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, exact_stages_list_required)
            print("sono tornato in dispatcher of fmode di approachme approachme!!!")
        elif next_command=='followme':
            kid_id = 0
            print("sono in approachme approachme approachme dopo avere estratto tutti gli stage")
            coda = OimiQueue("Fifo coda")
            event_main = Event()
            event_main.set()
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            switchalert = b'<switchMod1>'
            ser1.transmit(switchalert)
            #ser2.transmit(switchalert) #remove for now! 
            #ser3.transmit(switchalert)            
            adjust_dates = 0
            #oimidb = db.DatabaseManager(adjust_dates) #no!!! use the other constructor!!!!!
            oimidb = db.DatabaseManager()
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)
            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            #stage_main in __cinit__!!
            #with reaction1:
            #    print("uga chaca fami option")
            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))
            
            am.approachMe_main(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, exact_stages_list_required)
            print("sono tornato in dispatcher of fmode di approachme approachme!!!")
        elif next_command=='putpatches':
            kid_id = 0
            print("sono in approachme approachme approachme dopo avere estratto tutti gli stage")
            coda = OimiQueue("Fifo coda")
            event_main = Event()
            event_main.set()
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            switchalert = b'<switchMod1>'
            ser1.transmit(switchalert)
            #ser2.transmit(switchalert) #remove for now! 
            #ser3.transmit(switchalert)            
            adjust_dates = 0
            #oimidb = db.DatabaseManager(adjust_dates) #no!!! use the other constructor!!!!!
            oimidb = db.DatabaseManager()
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)
            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            #stage_main in __cinit__!!
            #with reaction1:
            #    print("uga chaca fami option")
            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))
            
            am.approachMe_main(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, exact_stages_list_required)
            print("sono tornato in dispatcher of fmode di approachme approachme!!!")
        elif next_command=='runningchasingyou':
            kid_id = 0
            print("sono in approachme approachme approachme dopo avere estratto tutti gli stage")
            coda = OimiQueue("Fifo coda")
            event_main = Event()
            event_main.set()
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            switchalert = b'<switchMod1>'
            ser1.transmit(switchalert)
            #ser2.transmit(switchalert) #remove for now! 
            #ser3.transmit(switchalert)            
            adjust_dates = 0
            #oimidb = db.DatabaseManager(adjust_dates) #no!!! use the other constructor!!!!!
            oimidb = db.DatabaseManager()
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)
            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            #stage_main in __cinit__!!
            #with reaction1:
            #    print("uga chaca fami option")
            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))
            
            am.approachMe_main(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, exact_stages_list_required)
            print("sono tornato in dispatcher of fmode di approachme approachme!!!")
        elif next_command=='imitateme':
            kid_id = 0
            print("sono in approachme approachme approachme dopo avere estratto tutti gli stage")
            coda = OimiQueue("Fifo coda")
            event_main = Event()
            event_main.set()
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            switchalert = b'<switchMod1>'
            ser1.transmit(switchalert)
            #ser2.transmit(switchalert) #remove for now! 
            #ser3.transmit(switchalert)            
            adjust_dates = 0
            #oimidb = db.DatabaseManager(adjust_dates) #no!!! use the other constructor!!!!!
            oimidb = db.DatabaseManager()
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)
            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            #stage_main in __cinit__!!
            #with reaction1:
            #    print("uga chaca fami option")
            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))
            
            am.approachMe_main(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, exact_stages_list_required)
            print("sono tornato in dispatcher of fmode di approachme approachme!!!")
        elif next_command=='dancewithme':
            kid_id = 0
            print("sono in approachme approachme approachme dopo avere estratto tutti gli stage")
            coda = OimiQueue("Fifo coda")
            event_main = Event()
            event_main.set()
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            switchalert = b'<switchMod1>'
            ser1.transmit(switchalert)
            #ser2.transmit(switchalert) #remove for now! 
            #ser3.transmit(switchalert)            
            adjust_dates = 0
            #oimidb = db.DatabaseManager(adjust_dates) #no!!! use the other constructor!!!!!
            oimidb = db.DatabaseManager()
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)
            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            #stage_main in __cinit__!!
            #with reaction1:
            #    print("uga chaca fami option")
            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))
            
            am.approachMe_main(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, exact_stages_list_required)
            print("sono tornato in dispatcher of fmode di approachme approachme!!!")
        elif next_command=='wakemeup':
            kid_id = 0
            print("sono in approachme approachme approachme dopo avere estratto tutti gli stage")
            coda = OimiQueue("Fifo coda")
            event_main = Event()
            event_main.set()
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            switchalert = b'<switchMod1>'
            ser1.transmit(switchalert)
            #ser2.transmit(switchalert) #remove for now! 
            #ser3.transmit(switchalert)            
            adjust_dates = 0
            #oimidb = db.DatabaseManager(adjust_dates) #no!!! use the other constructor!!!!!
            oimidb = db.DatabaseManager()
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)
            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            #stage_main in __cinit__!!
            #with reaction1:
            #    print("uga chaca fami option")
            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))
            
            am.approachMe_main(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, exact_stages_list_required)
            print("sono tornato in dispatcher of fmode di approachme approachme!!!")    
        elif next_command=='dontwakemeup':
            kid_id = 0
            print("sono in approachme approachme approachme dopo avere estratto tutti gli stage")
            coda = OimiQueue("Fifo coda")
            event_main = Event()
            event_main.set()
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            switchalert = b'<switchMod1>'
            ser1.transmit(switchalert)
            #ser2.transmit(switchalert) #remove for now! 
            #ser3.transmit(switchalert)            
            adjust_dates = 0
            #oimidb = db.DatabaseManager(adjust_dates) #no!!! use the other constructor!!!!!
            oimidb = db.DatabaseManager()
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)
            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            #stage_main in __cinit__!!
            #with reaction1:
            #    print("uga chaca fami option")
            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))
            
            am.approachMe_main(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, exact_stages_list_required)
            print("sono tornato in dispatcher of fmode di approachme approachme!!!")                         
        elif next_command=='approachme':
            kid_id = 0
            print("sono in approachme approachme approachme dopo avere estratto tutti gli stage")
            coda = OimiQueue("Fifo coda")
            event_main = Event()
            event_main.set()
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            switchalert = b'<switchMod1>'
            ser1.transmit(switchalert)
            #ser2.transmit(switchalert) #remove for now! 
            #ser3.transmit(switchalert)            
            adjust_dates = 0
            #oimidb = db.DatabaseManager(adjust_dates) #no!!! use the other constructor!!!!!
            oimidb = db.DatabaseManager()
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)
            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            #stage_main in __cinit__!!
            #with reaction1:
            #    print("uga chaca fami option")
            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))
            
            am.approachMe_main(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, exact_stages_list_required)
            print("sono tornato in dispatcher of fmode di approachme approachme!!!")

        elif next_command=='touch':
            kid_id = 0
            
            print("sono in touch dopo avere estratto tutti gli stage")
            #vecchio funzionante!!!
            coda = OimiQueue("Fifo coda")
            event_main = Event()
            event_main.set()
            ser1, ser2, ser3 = serr.setup_serial()
            #ser1.await_arduinoMega()
            #ser2.await_arduinoNano1()
            #ser3.await_arduinoNano2()
            print("comincio ad aspettare seriali vari")
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            switchalert = b'<switchMod1>'
            ser2.transmit(switchalert) #remove for now!
            ser3.transmit(switchalert)            
            adjust_dates = 0
            #oimidb = db.DatabaseManager(adjust_dates) #no!!! use the other constructor!!!!!
            oimidb = db.DatabaseManager()
            #choose here stage!!!!from kid_id or randomly!!!! choose id...
            exact_stages_list_required = extract_all_possible_substages(kid_id, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3, oimidb)
            #reaction1 = rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            #stage_main in __cinit__!!
            #with reaction1:
            #    print("uga chaca fami option")
            status = shar.PyGlobalStatus(b'reacting')
            global_status.put(status)
            print("status is {}".format(status.get_globalstatus()))
            cm.contact_manager_main(stopInterrupt, primary_lock, queue_lock, ser3, exact_stages_list_required)
            ''' nuovo multiprocessing no!
            ser1, ser2, ser3 = serr.setup_serial()
            #old!!!
            #ser1.await_arduinoMega()
            #ser2.await_arduinoNano1()
            #ser3.await_arduinoNano2()
            print("comincio ad aspettare seriali vari")
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()  
            #with rea.Stage(1, event_main, coda, stopInterrupt, primary_lock, queue_lock) as reaction1:
            status = shar.PyGlobalStatus(b'reacting')
            print("status is {}".format(status.get_globalstatus()))
            global_status.put(status)
            cm.contact_main_new_multiproc(stopInterrupt, primary_lock, queue_lock, ser1, ser2, ser3)
            '''
            print("sono tornato in dispatcher of fmode di touch!!!")

        elif next_command=='react':
            print("Enter in autonomous_move!!")
            move_type = "none"
            status = shar.PyGlobalStatus(b'reacting')
            print("status is {}".format(status.get_globalstatus()))
            global_status.put(status)
        elif next_command=='auto_move':
            print("Enter in autonomous_move!!")
            move_type = "autonomous"
            status = shar.PyGlobalStatus(b'moving')
            print("status is {}".format(status.get_globalstatus()))
            global_status.put(status)
            move.move_robot(stopInterrupt, move_type, primary_lock,queue_lock)
        elif next_command=='follo_move':
            move_type = "following"
            print("Enter in following_move!!")
            status = shar.PyGlobalStatus(b'moving')
            print("status is {}".format(status.get_globalstatus()))
            global_status.put(status)
            move.move_robot(stopInterrupt, move_type, primary_lock, queue_lock)
        elif next_command=='driveme':
        # open a file for writing the output
            with open('output_driveme.txt', 'w') as outfile:
                joy = run("/home/pi/oimi-venv/bin/python3 /home/pi/2Gennaio_gamepad/py_main_movement.py", shell=True, stdout=outfile)    
            '''
            #joy = run("/home/pi/oimi-venv/bin/python3 /home/pi/OIMI/oimi_code/src/main_joypad_movements.py", shell=True, stdout=outfile)
            print("next command DRIVE DRIVE DRIVE DRIVE ")
            #joy = run("/home/pi/oimi-venv/bin/python3 /home/pi/2Gennaio_gamepad/py_main_movement.py", capture_output=True, shell=True)
            cmd = ['/home/pi/oimi-venv/bin/python3', '/home/pi/2Gennaio_gamepad/py_main_movement.py']
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

            while process.poll() is None:
                # read output from stdout and stderr
                stdout_data = process.stdout.readline().decode('utf-8')
                stderr_data = process.stderr.readline().decode('utf-8')
                if stdout_data:
                    print(stdout_data, end='')
                if stderr_data:
                    print(stderr_data, end='')

            # read any remaining output
            stdout_data, stderr_data = process.communicate()
            if stdout_data:
                print(stdout_data.decode('utf-8'))
            if stderr_data:
                print(stderr_data.decode('utf-8'))
            #process = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            #output = process.stdout.decode('utf-8')
            #error = process.stderr.decode('utf-8')
            #print(output)
            #print(error)

            #joy = run("/home/pi/oimi-venv/bin/python3 /home/pi/OIMI/oimi_code/src/moving/joypad_navigation/main_joypad_movements.py", shell=True)
            #joy = run("/home/pi/oimi-venv/bin/python3 /home/pi/OIMI/oimi_code/src/main_joypad_movements.py", shell=True)
            print("sono tornato in dispatcher of fmode dopo driveMe_maindriveMe_maindriveMe_main driveMe_main!!!")
            '''
        elif next_command=='joystickme':
            move_type = "control"
            print("Enter in joystick_move!!")
            status = shar.PyGlobalStatus(b'moving')
            print("status is {}".format(status.get_globalstatus()))
            
            global_status.put(status)
            ser1, ser2, ser3 = serr.setup_serial()
            ser1.await_arduino()
            ser2.await_arduino()
            ser3.await_arduino()
            #switchalert = b'<switchMod1>' 
            #ser1.transmit(switchalert)
            #ser2.transmit(switchalert) #remove for now!
            #ser3.transmit(switchalert)                 
            #fun1_lock(primary_lock)
            ##########joy = run("/home/pi/oimi-venv/bin/python3 /home/pi/2Gennaio_gamepad/py_main_movement.py", shell=True)
            move.move_robot_joypad(global_status, stopInterrupt, move_type, primary_lock, queue_lock, ser1, ser2, ser3)
            #joy = run("python /home/pi/temporary/Projects/PERFECT_WORKING/16Ottobre_gamepad/py_main_movement.py", shell=True)
            #joy = subprocess.check_call("/home/pi/nuovoscript.sh '%s'", shell=True)
            #move.move_robot(stopInterrupt, move_type, primary_lock) 
            print("sono tornato in dispatcher of fmode dopo driveMe_maindriveMe_maindriveMe_main driveMe_main!!!")
        #-- per adesso lo metto, poi in teoria non ci vorrebbe in familiy mode 
        elif next_command=='pattern_move': #?? ne metto uno per ogni specifica emozione
            print("Enter in pattern_move!!")
            #fun1(e5)
            move_type = "sadness"
            #move_type = "agree"
            #move_type = "disagree"
            status = shar.PyGlobalStatus(b'moving')
            print("status is {}".format(status.get_globalstatus()))
            global_status.put(status)
            #fun1_lock(primary_lock)
            #move.move_pattern(global_status, stopInterrupt, move_type, primary_lock, queue_lock)
            move.move_robot2(stopInterrupt, move_type, primary_lock, queue_lock)
            #move.move_pattern(global_status, stopInterrupt, move_type, primary_lock, queue_lock)

        elif next_command=='dont_move':
            return
        #elif next_command=='body':
        #   print("ENTER IN giochiamo ad abbracciarci fisical interaction!!!!!!!!!!!")
        #   print("ret_value of fun2 is {}".format(ret_value))
        #   #fun2(e6)
        #   #fun2_lock(primary_lock)