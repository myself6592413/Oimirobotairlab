# ==========================================================================================================================================================
#  Imports:
# ==========================================================================================================================================================
# Fixed Screen size as android screen
# remove both line when build App
import oimi_robot_send_email as orsm
import time
import re
import random
import os
from kivy.config import Config
Config.set('graphics', 'width', '360')
Config.set('graphics', 'height', '740')
Config.set('graphics', 'resizable', True)

from os.path import exists
import json

from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.screenmanager import ScreenManager
from kivy.uix.screenmanager import NoTransition,CardTransition,SlideTransition,SwapTransition
from kivy.uix.screenmanager import RiseInTransition,FadeTransition,WipeTransition,FallOutTransition 

from kivy.uix.floatlayout import FloatLayout
from kivymd.uix.tab import MDTabsBase
from kivy.properties import ObjectProperty, StringProperty, ListProperty, NumericProperty

from kivymd.uix.navigationdrawer import MDNavigationLayout
from kivymd.uix.navigationdrawer import MDNavigationDrawer
from kivy.clock import Clock
from kivy.uix.image import Image
from kivymd.uix.boxlayout import MDBoxLayout

from kivy.lang import Builder

from kivymd.theming import ThemableBehavior
from kivymd.uix.toolbar import MDToolbar
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.list import OneLineIconListItem, MDList, IRightBodyTouch, OneLineAvatarIconListItem
from kivymd.icon_definitions import md_icons
from kivymd.uix.button import MDFlatButton
from kivymd.uix.button import MDRaisedButton

from kivymd.uix.button import MDFloatingActionButton

from kivymd.uix.dialog import MDDialog
from kivymd.uix.textfield import MDTextField
from kivymd.uix.textfield import MDTextField,MDTextFieldRound
from kivy.properties import BooleanProperty
from kivy.metrics import dp
from functools import partial

from kivy.uix.behaviors import TouchRippleBehavior, ButtonBehavior
from kivy.lang import Builder
from kivy.utils import get_color_from_hex

from kivymd.uix.menu import MDDropdownMenu

from os import listdir
from os.path import isfile, join, isdir
import kivy_object1 as ko1
import kivy_object2 as ko2

from kivymd.uix.datatables import MDDataTable
from kivymd.uix.screen import MDScreen
# ==========================================================================================================================================================
#  Methods:
# ==========================================================================================================================================================
def n_len_rand(lun, floor=1):
    '''create_random_number of lun digits. Need to stay here, outside any class to avoid a formatting error in return '''
    top = 10**lun
    if floor > top:
        raise ValueError(f"Floor '{floor}' must be less than requested top '{top}'")
    return f'{random.randrange(floor, top):0{lun}}'
# ==========================================================================================================================================================
#  Classes:
# ==========================================================================================================================================================
class Tabs(FloatLayout, MDTabsBase):
    '''Class implementing content for a tab on home screen'''

class MyMDToolbar(MDToolbar):
    '''NOthing'''    
class ContentNavigationDrawer(MDBoxLayout):
    nav_drawer = ObjectProperty()  
    #pass

class ItemDrawer(OneLineIconListItem):
    icon = StringProperty()
    text_color = ListProperty((0, 0, 0, 1))

class MyMDFlatButton(MDFlatButton):
    '''ciao'''

class MyMDTextField(MDTextField):
    password_mode = BooleanProperty(True)

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            if self.icon_right:
                if not self.password_mode:
                    self.icon_right = 'eye-off'
                    self.password_mode = True
                    self.password = self.password_mode
                else:
                    self.icon_right = 'eye'
                    self.password_mode = False
                    self.password = self.password_mode
                # try to adjust cursor position
                cursor = self.cursor
                self.cursor = (0,0)
                Clock.schedule_once(partial(self.set_cursor, cursor))
        return super(MyMDTextField, self).on_touch_down(touch)

    def set_cursor(self, pos, dt):
        self.cursor = pos

class DrawerList(ThemableBehavior, MDList):
    def set_color_item(self, instance_item):
        """Called when tap on a menu item."""

        # Set the color of the icon and text for the menu item.
        for item in self.children:
            if item.text_color == self.theme_cls.primary_color:
                item.text_color = self.theme_cls.text_color
                break
        instance_item.text_color = self.theme_cls.primary_color

class MDRoundFlatIconButton(TouchRippleBehavior):
    primary_color = get_color_from_hex("#EB8933")

    def on_touch_down(self, touch):
        collide_point = self.collide_point(touch.x, touch.y)
        if collide_point:
            touch.grab(self)
            self.ripple_show(touch)
            return True
        return False

    def on_touch_up(self, touch):
        if touch.grab_current is self:
            touch.ungrab(self)
            self.ripple_fade()
            return True
        return False

class IconListItem(OneLineIconListItem):
    icon = StringProperty()

class IconListItem1(OneLineIconListItem):
    icon = StringProperty()

class IconListItem2(OneLineIconListItem):
    icon = StringProperty()

class IconListItem3(OneLineIconListItem):
    icon = StringProperty()

class IconListItem4(OneLineIconListItem):
    icon = StringProperty()

class IconListItem5(OneLineIconListItem):
    icon = StringProperty()

class IconListItem6(OneLineIconListItem):
    icon = StringProperty()

class RightContentCls(IRightBodyTouch, MDBoxLayout):
    icon = StringProperty()
    text = StringProperty()

class MDRaisedButton(MDFlatButton):
    pass

class Item(OneLineAvatarIconListItem):
    left_icon = StringProperty()
    right_icon = StringProperty()
    right_text = StringProperty()


class ButtonsApp(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(ButtonsApp, self).__init__(**kwargs)
        self.source = 'scre2.jpg'

    def on_press(self):
        pass
        #self.source = 'scre2.jpg'
        #self.source = 'atlas://data/images/defaulttheme/checkbox_on'

    def on_release(self):
        pass
        #self.source = 'scre2.jpg'
        #self.source = 'atlas://data/images/defaulttheme/checkbox_off'

class MyFloatLayout(FloatLayout):
    pass

class MDData(MDScreen):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data = [
            ["Carlo Pascal", "80013", "cognitive_skills", ("alert", [255 / 256, 165 / 256, 0, 1], "Only one Session")],
            ["Carlo Pascal", "80013", "emotion_recognition", ("checkbox-marked-circle", [39 / 256, 174 / 256, 96 / 256, 1],"Good therapist_grade")],
            ["Carlo Pascal", "80013", "emotion_expression", ("alert-circle", [1, 0, 0, 1], "Offline")],
            ["Carlo Pascal", "80013", "imitation", ("magnify", [255 / 256, 165 / 256, 0, 1], "No imitation entertainment")],
            ["Carlo Pascal", "80013", "creativity", ("pencil", [102, 153, 255, 1], "Offline")],
            ["Carlo Pascal", "80013", "compliance_rules", ("magnify", [1, 0, 0, 1], "Offline")],


            ["Carlo Pascal", "80013", "joint_attention", ("pencil", get_color_from_hex("#ffff99"), "locomotion stimulative found in notes")],
            ["Carlo Pascal", "80013", "eye_contact", ("eye", [102, 255, 153, 1], "pay attention found in notes")],
            ["Carlo Pascal", "80013", "turn_taking", ("alert-circle", [255, 153, 102, 1], "3 indecisions in second Session")],
            ["Carlo Pascal", "80013", "self_inititiation", ("alert", [255 / 256, 165 / 256, 0, 1], "self-management Issue")],
            ["Carlo Pascal", "80013", "vocalization", ("vanish", get_color_from_hex("#ffffff"), "Nothing to report")],
            ["Carlo Pascal", "80013", "triadic_interaction", ("account-supervisor-circle", get_color_from_hex("#4dd2ff"), "adults involved a lot found in notes")],
            ["Carlo Pascal", "80013", "theory_of_mind",  ("check-decagram-outline", [102, 255, 153], "Particular interest found in notes")],

            ["Giacomo Venezian", "80021", "cognitive_skills", ("head-snowflake", [255 / 256, 165 / 256, 0, 1], "Only 2 error in 8 games")],
            ["Giacomo Venezian", "80021", "emotion_recognition", ("checkbox-marked-circle", [39 / 256, 174 / 256, 96 / 256, 1],"Good therapist_grade")],
            ["Giacomo Venezian", "80021", "emotion_expression", ("emoticon-happy-outline", get_color_from_hex("#ffffff"), "happy found in notes")],
            ["Giacomo Venezian", "80021", "imitation", ("magnify",  get_color_from_hex("#ffccff"), "No imitation entertainment")],
            ["Giacomo Venezian", "80021", "creativity", ("alert", [255 / 256, 165 / 256, 0, 1], "Only one Session")],
            ["Giacomo Venezian", "80021", "compliance_rules", ("alert-circle", [1, 0, 0, 1], "Rules of 5th game repeated 3 times")],
            ["Giacomo Venezian", "80021", "joint_attention", ("checkbox-marked-circle", [39 / 256, 174 / 256, 96 / 256, 1],"Attention to details Strength")],
            ["Giacomo Venezian", "80021", "eye_contact", ("alert", [255 / 256, 165 / 256, 0, 1], "Distraction Symptom")],

            ["Giacomo Venezian", "80021", "turn_taking", ("alert-circle", [1, 0, 0, 1], "Offline")],
            ["Giacomo Venezian", "80021", "self_inititiation", ("alert-circle", [1, 0, 0, 1], "Offline")],
            ["Giacomo Venezian", "80021", "vocalization", ("alert-circle", [1, 0, 0, 1], "Offline")],
            ["Giacomo Venezian", "80021", "triadic_interaction", ("alert-circle", [1, 0, 0, 1], "Offline")],
            ["Giacomo Venezian", "80021", "theory_of_mind",  ("checkbox-marked-circle", [39 / 256, 174 / 256, 96 / 256, 1],"Online")],
        ]
        self.data_tables = MDDataTable(
            use_pagination=True,
            check=True,
            rows_num=26,
            #size_hint=(0.9, 0.6),
            column_data=[
                ("Kid", dp(50)),
                ("Treatment_id", dp(30)),
                ("Target_behaviour", dp(50)),
                ("Hints", dp(70)),
            ]
        )
        self.data_tables.row_data = self.data
        self.add_widget(self.data_tables)


    def delete_checked_rows(self):
        def deselect_rows(*args):
            self.data_tables.table_data.select_all("normal")

        for data in self.data_tables.get_row_checks():
            self.data_tables.remove_row(data)

        Clock.schedule_once(deselect_rows)

################################################################################################################
class OimiApp(MDApp):
    def __init__(self, *args, **kwargs):
        #super().__init__(**kwargs)
        MDApp.__init__(self, *args, **kwargs)
        global screen_manager
        screen_manager = ScreenManager(transition=FadeTransition())
        screen_manager.size_hint = (1, 1)
        self.screen = screen_manager
        # Gets current python dir then add the KV dir
        kv_path = os.getcwd() + '/kv/'
        kv_load_list = [f for f in listdir(kv_path) if isfile(join(kv_path, f))]

        # Loads all KV file
        for file in kv_load_list:
            if file.endswith('.kv'):
                Builder.load_file(kv_path + file)
        #screen_manager = Builder.load_string(KV)
        #self.menu = MDDropdownMenu(
        #    caller=self.screen.ids.field,
        #    items=menu_items,
        #    position="bottom",
        #    width_mult=4,
        #)

    #def __init__(self, **kwargs):
    #    super().__init__(**kwargs)

        #Clock.schedule_once(self.on_start)
        #self._on_enter_trig = trig = 
        #Clock.schedule_once(self._my_on_enter, 0)
        ##self.bind(on_enter=self._my_on_enter)
        
        self._on_enter_trig = trig = Clock.create_trigger(self._my_on_enter)
        self.bind(on_enter=trig)

        #Clock.schedule_once(self.change_screen("home"), 3)

    email = StringProperty()
    password = StringProperty()
    username = StringProperty()
    fullname = StringProperty()
    int_code = StringProperty()
    dialog = None
    #email2 = StringProperty()
    #im1 = Image(source="image1.png")
    
    def set_credentials(self, uname, pwd):
        self.username = uname
        self.password = pwd

    def create_user_data(self, ema, uname, fname, pwd):
        self.email = ema
        self.username = uname
        self.fullname = fname
        self.password = pwd

    def set_usermail(self, ema):
        self.email = ema
        self.check_logged_forgot(self.email)
        print("sono in set_mail in main e quella che sto leggendo è: {}".format(self.email))
    
    def signup_user(self):
        print("email is {} and pass is {}".format(self.email, self.password))
        print("username is {} and fullname is {}".format(self.username, self.fullname))
        print("tipi {} {} ".format(type(self.email),type(self.password)))
        # Converting string to dictionary
        stri1 = "{}={}".format(self.email,self.username)
        stri2 = "{}={}".format(self.email,self.password)
        stri3 = "{}={}".format(self.username,self.password)
        stri4 = "{}={}".format(self.email,self.fullname)
        print("stri1 ",stri1)
        print("stri2 ",stri2)
        print("stri3 ",stri3)
        print("stri4 ",stri4)
        dictionary1 = dict(subString.split("=") for subString in stri1.split(";"))
        dictionary2 = dict(subString.split("=") for subString in stri2.split(";"))
        dictionary3 = dict(subString.split("=") for subString in stri3.split(";"))
        dictionary4 = dict(subString.split("=") for subString in stri4.split(";"))
        print("data dizionario creato1 ", dictionary1)
        print("data dizionario creato2 ", dictionary2)
        print("data dizionario creato3 ", dictionary3)
        print("data dizionario creato4 ", dictionary4)
        
        if os.stat("mail_pass.json").st_size!=0:
            print("file_vuoto")
            data1 = self.get_data_mail_user()
            print("data used1 ", data1)
            if data1:
                dictionary1 = {**data1, **dictionary1}
                print("merged dictionary1 ",dictionary1)

        if os.stat("user_pass.json").st_size!=0:
            data2 = self.get_data_mail_pass()
            print("data used2 ", data2)
            if data2:
                dictionary2 = {**data2, **dictionary2}
                print("merged dictionary2 ",dictionary2)

        if os.stat("mail_user.json").st_size!=0:
            data3 = self.get_data_user_pass()
            print("data used3 ", data3)
            if data3:
                dictionary3 = {**data3, **dictionary3}
                print("merged dictionary3 ",dictionary3)

        if os.stat("mail_full.json").st_size!=0:
            data4 = self.get_data_mail_user()
            print("data used3 ", data4)
            if data4:
                dictionary4 = {**data4, **dictionary4}
                print("merged dictionary4 ",dictionary4)
        
        with open("mail_user.json", "wb") as f:
            f.write(json.dumps(dictionary1).encode())
        with open("mail_pass.json", "wb") as f:
            f.write(json.dumps(dictionary2).encode())
        with open("user_pass.json", "wb") as f:
            f.write(json.dumps(dictionary3).encode())
        with open("mail_full.json", "wb") as f:
            f.write(json.dumps(dictionary4).encode())

        self.send_verification_code(self.email)
        self.change_screen("verification")

    def loggin_user(self):
        rett = self.check_pass()
        print("rett rett is ",rett)
        if rett:
        #if self.email == 'a' and self.password == 'a':
            screen_manager.transition = NoTransition()
            screen_manager.current = "home"
            screen_manager.transition = SlideTransition()
            ####old self.change_screen("verification")
            self.set_logged("logged", "True")
        else:
            print("ERror. email is {} and pass is {}".format(self.email, self.password))

    def logout(self):
        self.change_screen("home")
        self.set_logged("logged", "False")

    def get_logged(self):
        with open("data_log.json", "rb") as f:
            f_data = f.read().decode()
            data = json.loads(f_data)
        return data

    def get_data_mail_user(self):
        with open("mail_user.json", "rb") as f:
            f_data = f.read().decode()
            data = json.loads(f_data)
        return data
    def get_data_mail_pass(self):
        with open("mail_pass.json", "rb") as f:
            f_data = f.read().decode()
            data = json.loads(f_data)
        return data
    def get_data_user_pass(self):
        with open("user_pass.json", "rb") as f:
            f_data = f.read().decode()
            data = json.loads(f_data)
        return data
    def get_data_mail_full(self):
        with open("mail_full.json", "rb") as f:
            f_data = f.read().decode()
            data = json.loads(f_data)
        return data

    def set_logged(self, key, value):
        file_exists = exists("data_log.json")
        if not file_exists:
            os.mknod("data_log.json") #create file without opening
        data = self.get_logged()
        data[key] = value
        with open("data_log.json", "wb") as f:
            f.write(json.dumps(data).encode())

    ##################################################### old one
    def check_logged(self, *args):
        print("received mail add in check_logged!!! " ,args)
        data = self.get_logged()
        print("data recievide from getdata ", data)
        if data['logged'] == "True":
            screen_manager.transition = NoTransition()
            screen_manager.current = "verification"
            screen_manager.transition = SlideTransition()
    ##################################################### right one
    def check_logged_forgot(self, *args):
        print("received mail add in check_logged!!! " ,args)
        data = self.get_logged()
        print("data recievide from getdata ", data)
        if data['logged'] == "True":
            return 1
        else:
            return 0

    def check_pass(self, *args):
        print("received mail add in check pass!!! " ,args)
        data = self.get_data_user_pass()
        print("data recievide from get_data_user_pass in checkpass", data)
        #if data['logged'] == "True":
        if data[self.username] == self.password:
            return 1
        else:
            return 0
            #screen_manager.transition = NoTransition()
            #screen_manager.current = "home"
            #screen_manager.transition = SlideTransition()

    ##################################################### navigate = change screen of kv pages called with on_release 
    def change_screen(self, name):
        time.sleep(0.5)
        screen_manager.current = name

    def change_screen_1(self, name):
        screen_manager.current = "login"
        screen_manager.transition = SlideTransition(direction='right')

    def change_screen_2(self, inst):
        self.closeDialog(inst)
        screen_manager.current = "home"
        #screen_manager.transition = SlideTransition(direction='right')

    def change_screen_3(self, inst):
        self.closeDialog(inst)
        screen_manager.current = "forgot"
        #screen_manager.transition = SlideTransition(direction='right')


    def change_screen5(self, name):
        time.sleep(0.5)
        screen_manager.current =  "prova2"
        #screen_manager.transition = SlideTransition(direction='right')

    def _my_on_enter(self, *args):
        print("ciao!!!!!!!!!!!!!!!!!!!!, ",self.root.ids)

        self.drawitems = ContentNavigationDrawer()
        icons_item = {
            "folder": "My files",
            "account-multiple": "Shared with me",
            "star": "Starred",
            "history": "Recent",
            "checkbox-marked": "Shared with me",
            "upload": "Upload",
        }
        #for icon_name in icons_item.keys():
        #    self.drawitems.add_widget(ItemDrawer(icon=icon_name, text=icons_item[icon_name]))

        for icon_name in icons_item.keys():
            self.root.ids.content_drawer.ids.md_list.add_widget(ItemDrawer(icon=icon_name, text=icons_item[icon_name]))

    def send_mail_forgot(self, mail_add):
        send_ok = 0
        #receive mail_add from inputtxt...
        print("received mail add ",mail_add)
        print("received mail add ",mail_add)
        print("received mail add ",mail_add)
        print("received mail add ",mail_add)
        if os.stat("mail_pass.json").st_size!=0:
            data1 = self.get_data_mail_pass()
            print("data1 in use ", data1)
            print("no mail given error you need to sign up!!!!!!!!!!!!!!!!!!!!!!!!!!!!") #qui metto popup!!!
            send_ok = 1
        if os.stat("mail_user.json").st_size!=0:
            data2 = self.get_data_mail_user()
            print("data2 in use ", data2)
        if os.stat("mail_full.json").st_size!=0:
            data3 = self.get_data_mail_full()
            print("data3 in use ", data3)
        if send_ok:
            try:
                if data1[mail_add]:
                    if mail_add=='oimirobotairlab@gmail.com':
                        print("mail giusta!!!")
                        #orsm.send_mail(to_email=[mail_add], subject='Oimi Robot retrieve password', message='Hi {},\n Your username is: \'{}\'.\n Your password is: \'{} \'!'.format(data3[mail_add],data2[mail_add],data1[mail_add]))
                        self.show_alert_dialog_forgot_ok()
                    else:
                        print("sono nel prmo caso eccezionale")
                        raise Exception
                        #self.show_alert_dialog_forgot_ko()
            except Exception:
                print("CIAO CIAO FORGOT eccezione!!")
                self.show_alert_dialog_forgot_ko()
        else:
            print("CIAO CIAO FORGOT caso esterno se files vuoti!!")
            self.show_alert_dialog_forgot_ko()

    def send_verification_code(self, mail_add):
        #receive mail_add from inputtxt...
        print("received mail add ",mail_add)
        print("received mail add ",mail_add)
        print("received mail add ",mail_add)
        code = n_len_rand(4)
        print("code int = ", code)
        self.int_code = '{}'.format(code)
        print("generated CODE INTERO!!! ",self.int_code)
        if os.stat("mail_full.json").st_size!=0:
            data = self.get_data_mail_full()
            print("data fullname in use ", data)
        if data[mail_add]:
            if mail_add=='oimirobotairlab@gmail.com':
                print("mail giusta!!!")
                #orsm.send_mail(to_email=[mail_add], subject='Oimi Robot account verification', message='Hi {},\n Your verification code is: \'{} \'!'.format(data[mail_add], self.int_code))

    def send_again_verification_code(self, inst):
        self.closeDialog(inst)
        print("redo last mail ",self.email)
        print("redo last code ",self.int_code)
        if os.stat("mail_full.json").st_size!=0:
            data = self.get_data_mail_full()
            print("data fullname in use ", data)
        if self.email=='oimirobotairlab@gmail.com':
            print("mail giusta!!!")
            #orsm.send_mail(to_email=[mail_add], subject='Oimi Robot account verification', message='Hi {},\n Your verification code is: \'{} \'!'.format(data[mail_add], self.int_code))

    def check_id_submitted(self, code):
        print("check_id_submitted ricevuto == {} ".format(code))
        print("check_id_submitted quello salvato self == {} ".format(self.int_code))
        print(type(code))
        print(type(self.int_code))
        if code==self.int_code:
            print("bella ok codi uguali!!!!")
            self.change_screen("home")
        else:
            self.show_alert_dialog_verification()    

    def show_alert_dialog_verification(self):
        if not self.dialog:
            self.dialog = MDDialog(
                text="Your verification code is wrong!",
                buttons=[
                    MDFlatButton(
                        text="Try again",
                        theme_text_color="Custom",
                        text_color=self.theme_cls.primary_color,
                        on_release= self.closeDialog
                    ),
                    MDFlatButton(
                        text="Resend code",
                        theme_text_color="Custom",
                        text_color=self.theme_cls.primary_color,
                        on_release=self.send_again_verification_code
                    ),
                ],
            )
        self.dialog.open()

    def show_alert_dialog_forgot_ok(self):
        if not self.dialog:
            self.dialog = MDDialog(
                text="Ok. We have send your code to your email."
            )
        self.dialog.open()
        Clock.schedule_once(self.closeDialog, 2) #per chiuderlo subito

    def show_alert_dialog_forgot_ko(self):
        if not self.dialog:
            self.dialog = MDDialog(
                text="Your are not registered yet",
                buttons=[
                    MDFlatButton(
                        text="Sign up",
                        theme_text_color="Custom",
                        text_color=self.theme_cls.primary_color,
                        on_release=self.change_screen_2
                    ),
                    MDFlatButton(
                        text="Turn back",
                        theme_text_color="Custom",
                        text_color=self.theme_cls.primary_color,
                        on_release=self.change_screen_3
                    ),                    
                ],
            )
        self.dialog.open()

    def closeDialog(self, inst): #inst ovunque ---> mi serve perche show_aler passa sempre due parametri on_release anche se non specificati
        self.dialog.dismiss()


    def switch_theme_style(self):
        self.theme_cls.primary_palette = (
            "Blue" if self.theme_cls.primary_palette == "Blue" else "Blue"
        )
        self.theme_cls.theme_style = (
            "Dark" if self.theme_cls.theme_style == "Light" else "Light"
        )

    def set_item(self, text__item):
        self.screen.ids.field.text = text__item
        self.menu.dismiss()

    def menu_callback(self, text_item):
        print(text_item)



    ############################################# MAIN #############################################
    ############################################# main #############################################
    def build(self): 
        self.icon = 'polimi.png'
        '''main function that build all existing pages'''
        #global screen_manager
        #screen_manager = ScreenManager(transition=FadeTransition())
        #screen_manager.size_hint = (1, 1)
        #self.screen = screen_manager
        screen_manager.add_widget(Builder.load_file("intro.kv"))
        screen_manager.add_widget(Builder.load_file("login.kv"))  
        
        self.screen = Builder.load_string(ko1.KV1)
        menu_items2 = [
            {
                "viewclass": "IconListItem",
                "icon": "git",
                "height": dp(56),
                "width": dp(20),
                "text": f"Item {i}",
                "on_release": lambda x=f"Item {i}": self.set_item(x),
            } for i in range(5)]
        #############################################################################
        menu_items = [
            {
                "viewclass": "IconListItem",
                "icon": "gamepad-down",
                "height": dp(56),
                "width": dp(20),
                "text": f"2LDBOB",
                "on_release": lambda x=f"Item ciao": self.set_item(x),
            },
            {
                "viewclass": "IconListItem",
                "icon": "gamepad-round",
                "height": dp(56),
                "width": dp(20),
                "text": f"2LDBOC",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            },
            {
                "viewclass": "IconListItem",
                "icon": "gamepad-round-outline",
                "height": dp(56),
                "width": dp(20),
                "text": f"2LDSU",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            },
            {
                "viewclass": "IconListItem",
                "icon": "gamepad-round-up",
                "height": dp(56),
                "width": dp(20),
                "text": f"2LDSU2",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            },
            {
                "viewclass": "IconListItem",
                "icon": "gamepad-round-left",
                "height": dp(56),
                "width": dp(20),
                "text": f"2LDSUOB",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            },
            {
                "viewclass": "IconListItem",
                "icon": "gamepad-up",
                "height": dp(56),
                "width": dp(20),
                "text": f"2LDSUOC",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            },
            {
                "viewclass": "IconListItem",
                "icon": "gamepad-round-down",
                "height": dp(56),
                "width": dp(20),
                "text": f"2LST",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            },
            {
                "viewclass": "IconListItem",
                "icon": "gamepad-outline",
                "height": dp(56),
                "width": dp(20),
                "text": f"2LSU",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            },
            #{
            #    "viewclass": "IconListItem",
            #    "icon": "gampad-round-right",
            #    "height": dp(56),
            #    "width": dp(20),
            #    "text": f"2LTWO",
            #    #"on_release": lambda x=f"Item {i}": self.set_item(x),
            #}
            ]


        self.menu = MDDropdownMenu(
            caller=self.screen.ids.field,
            items=menu_items,
            position="bottom",
            width_mult=3,
            pos_hint= {'center_x': 0.5, 'center_y': 0.1},
            #dropdown_bg=[204, 230, 255,1]
            #max_height=dp(112),
        )
        #############################################################################
        menu_items1 = [
            {
                "viewclass": "IconListItem1",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"primaprima",
                #"on_release": lambda x=f"Item ciao": self.set_item(x),
            },
            {
                "viewclass": "IconListItem1",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"pencil",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            }]

        self.menu1 = MDDropdownMenu(
            caller=self.screen.ids.field1,
            items=menu_items1,
            position="bottom",
            width_mult=3,
            #max_height=dp(112),
        )
        #############################################################################
        menu_items2 = [
            {
                "viewclass": "IconListItem2",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"primaprima",
                #"on_release": lambda x=f"Item ciao": self.set_item(x),
            },
            {
                "viewclass": "IconListItem2",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"pencil",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            }]

        self.menu2 = MDDropdownMenu(
            caller=self.screen.ids.field2,
            items=menu_items2,
            position="bottom",
            width_mult=3,
            #max_height=dp(112),
        )
        #############################################################################
        menu_items3 = [
            {
                "viewclass": "IconListItem3",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"primaprima",
                #"on_release": lambda x=f"Item ciao": self.set_item(x),
            },
            {
                "viewclass": "IconListItem3",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"pencil",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            }]

        self.menu3 = MDDropdownMenu(
            caller=self.screen.ids.field3,
            items=menu_items3,
            position="bottom",
            width_mult=3,
            #max_height=dp(112),
        )
        #############################################################################
        #############################################################################
        menu_items4 = [
            {
                "viewclass": "IconListItem4",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"ciaone",
                #"on_release": lambda x=f"Item ciao": self.set_item(x),
            },
            {
                "viewclass": "IconListItem4",
                "icon": "alert",
                "height": dp(56),
                "width": dp(20),
                "text": f"pencil",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            }]

        self.menu4 = MDDropdownMenu(
            caller=self.screen.ids.field4,
            items=menu_items4,
            position="bottom",
            width_mult=3,
            #max_height=dp(112),
        )
        #############################################################################
        #############################################################################
        menu_items5 = [
            {
                "viewclass": "IconListItem5",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"primaprima",
                #"on_release": lambda x=f"Item ciao": self.set_item(x),
            },
            {
                "viewclass": "IconListItem5",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"5453lkjh",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            }]

        self.menu5 = MDDropdownMenu(
            caller=self.screen.ids.field5,
            items=menu_items5,
            position="bottom",
            width_mult=3,
            #max_height=dp(112),
        )
        #############################################################################
        #############################################################################
        menu_items6 = [
            {
                "viewclass": "IconListItem6",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"eccads",
                #"on_release": lambda x=f"Item ciao": self.set_item(x),
            },
            {
                "viewclass": "IconListItem6",
                "icon": "pencil",
                "height": dp(56),
                "width": dp(20),
                "text": f"432dsrr",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            }]

        self.menu6 = MDDropdownMenu(
            caller=self.screen.ids.field6,
            items=menu_items6,
            position="bottom",
            width_mult=3,
            #max_height=dp(112),
        )
        #############################################################################
        self.screen = Builder.load_string(ko2.KV2)
        menu_item2 = [
            #    {
            #        "text": f"Item {i}",
            #        "right_text": f"R+{i}",
            #        "right_icon": "apple-keyboard-command",
            #        "left_icon": "git",
            #        "viewclass": "Item",
            #        "height": dp(54),
            #        "on_release": lambda x=f"Item {i}": self.menu_callback(x),
            #    } for i in range(5)
            #]
            {
                "viewclass": "Item",
                "left_icon": "pencil",
                "height": dp(50),
                "width": dp(20),
                "text": f"low",
                #"on_release": lambda x=f"Item ciao": self.set_item(x),
            },
            {
                "viewclass": "Item",
                "left_icon": "pencil",
                "height": dp(50),
                "width": dp(20),
                "text": f"std",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            },
            {
                "viewclass": "Item",
                "left_icon": "pencil",
                "height": dp(50),
                "width": dp(20),
                "text": f"high",
                #"on_release": lambda x=f"Item {i}": self.set_item(x),
            }            
            ]

        self.menu2 = MDDropdownMenu(
            caller=self.screen.ids.button,
            items=menu_item2,
            width_mult=3,
            pos_hint={'top': 1}
            #elevation=38,
            #position="top",
        )
        #self.load_kv("prova2.kv")

        #text = self.root.ids.write.ids.input
        screen_manager.add_widget(Builder.load_file("home.kv"))
        screen_manager.add_widget(Builder.load_file("verification.kv"))
        screen_manager.add_widget(Builder.load_file("familiarization_mode.kv"))
        screen_manager.add_widget(Builder.load_file("login_222.kv"))
        screen_manager.add_widget(Builder.load_file("table_page.kv"))
        screen_manager.add_widget(Builder.load_file("loading_page.kv"))
        screen_manager.add_widget(Builder.load_file("prova2.kv"))
        screen_manager.add_widget(Builder.load_file("prova3.kv"))
        screen_manager.add_widget(Builder.load_file("pagina_prima_prova.kv"))
        screen_manager.add_widget(Builder.load_file("game_mode.kv"))
        screen_manager.add_widget(Builder.load_file("test_mode.kv"))
        screen_manager.add_widget(Builder.load_file("signup.kv"))
        screen_manager.add_widget(Builder.load_file("forgot.kv"))
        Clock.schedule_once(self.change_screen_1, 20)
        self.theme_cls.theme_style_switch_animation = True
        self.theme_cls.theme_style = "Dark" 
        self.theme_cls.material_style = "M2" #M3 problem!!!, but brings to errors and basically there is no documentation

        return screen_manager
        
if __name__ == "__main__":
    OimiApp().run()