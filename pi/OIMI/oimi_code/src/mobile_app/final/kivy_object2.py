KV2 = '''
<RightContentCls>
    disabled: True
    adaptive_size: True
    pos_hint: {"center_y": .5}

    MDIconButton:
        icon: root.icon
        user_font_size: "16sp"
        md_bg_color_disabled: 0, 0, 0, 0

    MDLabel:
        text: root.text
        font_style: "Caption"
        adaptive_size: True
        pos_hint: {"center_y": .5}


<Item>

    IconLeftWidget:
        icon: root.left_icon

    RightContentCls:
        id: container
        icon: root.right_icon
        text: root.right_text

MDScreen:

    MDTextField:
        id: field
        pos_hint: {'center_x': 2.4, 'center_y': 5}
        size_hint_x: 0.5
        width: "100dp"
        #hint_text: "Add_kid"
        on_focus: if self.focus: app.menu.open()

    MDTextField:
        id: field1
        pos_hint: {'center_x': 2.4, 'center_y': 5}
        size_hint_x: 0.5
        width: "100dp"
        #hint_text: "Add_kid"
        on_focus: if self.focus: app.menunew.open()


    MDTextField:
        id: field2
        pos_hint: {'center_x': 2.4, 'center_y': 5}
        size_hint_x: 0.5
        width: "100dp"
        #hint_text: "Add_kid"
        on_focus: if self.focus: app.menunew.open()        


    MDTextField:
        id: field3
        pos_hint: {'center_x': 2.4, 'center_y': 5}
        size_hint_x: 0.5
        width: "100dp"
        #hint_text: "Add_kid"
        on_focus: if self.focus: app.menunew.open()        

    MDTextField:
        id: field4
        pos_hint: {'center_x': 2.4, 'center_y': 5}
        size_hint_x: 0.5
        width: "100dp"
        #hint_text: "Add_kid"
        on_focus: if self.focus: app.menunew.open()        

    MDTextField:
        id: field5
        pos_hint: {'center_x': 2.4, 'center_y': 5}
        size_hint_x: 0.5
        width: "100dp"
        #hint_text: "Add_kid"
        on_focus: if self.focus: app.menunew.open()        

    MDTextField:
        id: field6
        pos_hint: {'center_x': 2.4, 'center_y': 5}
        size_hint_x: 0.5
        width: "100dp"
        #hint_text: "Add_kid"
        on_focus: if self.focus: app.menunew.open()  
        
    MDRaisedButton:
        id: button
        text: "PRESS ME"
        pos_hint: {"center_x": .5, "center_y": .5}
        on_release: app.menu2.open()
'''