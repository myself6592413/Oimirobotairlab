"""Welcome to Oimi Robot project main

The multiprocessing module provides various ways to manage parallel execution of code, including:

    Process: Represents a separate process that can run concurrently with the main process.
    Pool: A convenient way to run a number of functions in parallel using a pool of worker processes.
    Pipe: Two-way communication between processes.
    Event: A simple way to communicate between processes, used for signaling.
    Lock: A synchronization primitive that can be used to ensure that only one process accesses a resource at a time.
    Value: A synchronized value shared between processes.

    6 jobs running of type OimiProcess
    waiting for bluetooth command
    act 

    Notes 
        to look at elements of fifo queues without get():
            queue_content = list(global_status.queue)
            print("queue_content", queue_content)

        On the contrary...
        It is not possible to retrieve the elements in a LifoQueue object without removing them, 
        as the LifoQueue class does not provide a method or property for accessing its elements without removing them. 
        To access the elements in a LifoQueue object without removing them, you would need to:
        1 create a separate data structure to store the elements, such as a list, and add to it each time an element is added to the LifoQueue object.
        2 get, read and put again the element on the LifoQueue.
"""
# ===============================================================================================================
#  Imports
# ===============================================================================================================
import sys
if '/home/pi/OIMI/oimi_code/src/managers/interrupts_manager/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/interrupts_manager/')
import asyncio
import os
import toml
import time, timedelta
import signal
import serial
import subprocess
import oimi_config     #useless???? toml???
import oimi_logging.oimi_logger as olog

from pyfiglet import Figlet
from functools import partial
import multiprocessing
from multiprocessing import Process, Pool, Pipe, Event, Lock, Value
import pyximport; pyximport.install() #useless????

#import managers.interrupts_manager.custom_exceptions as ce
import custom_exceptions as ce
import shared as shar
import bluetooth_manager as bman
import serial_manager as serm
import std_navigation as navi
import joystick_movement as jnavi
import free_play_entertainments_modality as fami
import methodical_games_modality as game
import experience_mode_test_modality as expe
import insert_mode as inse # with inside update or create send data ... 2 methods inside 
from oimi_process import OimiProcess, OimiStartProcess, OimiQueue, OimiManager
from queue import LifoQueue
# ===============================================================================================================
#  Elements
# ===============================================================================================================
#configuration = toml.load("oimi_config/config.toml")
log = olog.logger
log2 = olog.logger2
simplelog = olog.simlog
#intro_fig = Figlet(font='larry3d')
# ===============================================================================================================
#  Methods
# ===============================================================================================================
def main():
    # Configlogger
    #shut down previous main process not ended properly,
    #to avoid error on bluetooth port if server closed before client
    pid_curr = os.getpid()
    pid_old = subprocess.run("ps -A | grep python | awk '{print $1}' ", shell=True)
    if pid_curr!=pid_old:
        ss = str(pid_old)
        str_comm = "kill -9" + " " + ss
        subprocess.run("kill -9 $(ps -A | grep python | awk '{print $1}')", shell=True)
    
    print(intro_fig.renderText('Oimi-Robot'))
    signal.signal(signal.SIGTERM, partial(ce.sigterm_main_handler, 1))
    #enable bluetooth handler automatically launching advertiser script
    subprocess.run(". /home/pi/bluetooth_adv > /dev/null 2>&1", shell=True)
    sudo_password = 'oimi4'
    command = 'chmod o+rw /var/run/sdp'
    command = command.split()
    cmd1 = subprocess.Popen(['echo',sudo_password], stdout=subprocess.PIPE)
    cmd2 = subprocess.Popen(['sudo','-S'] + command, stdin=cmd1.stdout, stdout=subprocess.PIPE)
    output = cmd2.stdout.read().decode() 
    #Manager, Queues, and Events
    goAhead = False
    blue = bman.setup_bluetooth()
    parent_id = os.getpid()
    main_connection, sub_connection = Pipe()
    blue_command = OimiQueue("Fifo bluetooth")
    fami_option = OimiQueue("Fifo familiarization")
    game_option = OimiQueue("Fifo game")
    expe_option = OimiQueue("Fifo experience")
    insert_option = OimiQueue("Fifo insert")
    current_modality = OimiQueue("Fifo mode")
    primary_lock = Lock()
    manager = OimiManager()
    OimiManager.register('LifoQueue', LifoQueue)
    manager.start()
    global_status = manager.LifoQueue()
    status = shar.PyGlobalStatus(b'beginning')
    global_status.put(status)

    queue_lock = OimiQueue("lock queue")
    stopInterrupt = Event() # event for lock
    block_movement = Event() # equivalent to event_fun3..or event_fun4... for movements se metto lock non serve giusto?
    event_blueIsReady = Event()
    event_startFamModality = Event()
    event_startGameModality = Event()
    event_startTestModality = Event()
    event_startInsertModality = Event()
    event_feeling = Event()
    event_reacting = Event()
    event_autoMove = Event()
    event_folloMove = Event()
    event_patternMove = Event()
    event_Body = Event()
    event_fun3 = Event()
    event_fun4 = Event()
    event_fun5 = Event()
    event_fun6 = Event()
    event_fun7 = Event()
    event_fun8 = Event()
    blue.create_events_dictionary(stopInterrupt,event_fun3, event_fun4, event_fun5, event_fun6, event_fun7, event_fun8,
                                event_startFamModality, event_startGameModality, event_startTestModality, event_startInsertModality, 
                                event_feeling, event_reacting)

    bluetooth_listener = OimiStartProcess(name = 'bluetooth_listener',
                                            sub_connection = sub_connection,
                                            global_status = global_status,
                                            parent_id = parent_id,
                                            execute = blue.connect_bluetooth,
                                            daemon = False,
                                            event_blueIsReady = event_blueIsReady,
                                            blue_command = blue_command)
    bluetooth_listener1 = OimiStartProcess(name = 'bluetooth_listener',
                                            sub_connection = sub_connection,
                                            global_status = global_status,
                                            parent_id = parent_id,
                                            execute = blue.connect_bluetooth,
                                            daemon = False,
                                            event_blueIsReady = event_blueIsReady,
                                            blue_command = blue_command)
    msgs_dispatcher = OimiProcess(name='msgs_dispatcher',
                                            global_status = global_status,
                                            parent_id = parent_id,
                                            execute = blue.dispatch_msgs,
                                            daemon = False,
                                            stopInterrupt=stopInterrupt,
                                            event_blueIsReady=event_blueIsReady,
                                            event_startFamModality = event_startFamModality,
                                            event_startGameModality = event_startGameModality,
                                            event_startTestModality = event_startTestModality,
                                            event_startInsertModality = event_startInsertModality,
                                            event_feeling = event_feeling,
                                            event_reacting = event_reacting,
                                            event_autoMove = event_autoMove,
                                            event_folloMove = event_folloMove,
                                            event_patternMove = event_patternMove,
                                            event_Body = event_Body,
                                            event_fun3 = event_fun3,
                                            event_fun4 = event_fun4,
                                            event_fun5 = event_fun5,
                                            event_fun6 = event_fun6,
                                            event_fun7 = event_fun7,
                                            event_fun8 = event_fun8,
                                            blue_command = blue_command,
                                            fami_option = fami_option,
                                            game_option = game_option,
                                            expe_option = expe_option,
                                            insert_option = insert_option,
                                            primary_lock = primary_lock,
                                            queue_lock = queue_lock,
                                            current_modality = current_modality)
    familiarization = OimiProcess(name='familiarization modality',
                                            global_status = global_status,
                                            parent_id = parent_id,
                                            execute = fami.start_familiarize,
                                            daemon = False,
                                            event_startFamModality = event_startFamModality,
                                            event_feeling = event_feeling,
                                            event_reacting = event_reacting,                                            
                                            event_autoMove = event_autoMove,
                                            event_folloMove = event_folloMove,
                                            event_Body=event_Body,
                                            block_movement = block_movement,
                                            stopInterrupt = stopInterrupt,
                                            fami_option = fami_option,
                                            primary_lock = primary_lock,
                                            queue_lock = queue_lock,
                                            current_modality = current_modality)
    playgame = OimiProcess(name='game modality',
                                            global_status = global_status,
                                            parent_id= parent_id,
                                            execute = game.games_main,
                                            daemon = False,
                                            event_startGameModality = event_startGameModality,
                                            event_fun3 = event_fun3,
                                            event_fun4 = event_fun4,
                                            stopInterrupt = stopInterrupt,
                                            game_option = game_option,
                                            primary_lock = primary_lock,
                                            current_modality = current_modality)
    experience = OimiProcess(name='test modality',
                                            global_status = global_status,
                                            parent_id = parent_id,
                                            execute = expe.trials_main,
                                            daemon = False,
                                            event_startTestModality = event_startTestModality,
                                            event_fun5 = event_fun5,
                                            event_fun6 = event_fun6,
                                            stopInterrupt = stopInterrupt,
                                            expe_option = expe_option,
                                            primary_lock = primary_lock,
                                            current_modality = current_modality)
    insert_database = OimiProcess(name='use_database',
                                            global_status = global_status,
                                            parent_id = parent_id,
                                            execute = inse.inse_main,
                                            daemon = False,
                                            event_startInsertModality = event_startInsertModality,
                                            event_fun7 = event_fun7,
                                            event_fun8 = event_fun8,
                                            stopInterrupt = stopInterrupt,
                                            insert_option = insert_option,
                                            primary_lock = primary_lock,
                                            current_modality = current_modality)

    try:
        bluetooth_listener.start()
        #bluetooth_listener1.start()
        msgs_dispatcher.start()
        while not goAhead:
            pipe_elem = main_connection.recv()
            if pipe_elem[0] == "begin_jobs":
                goAhead = True
                familiarization.start()
                playgame.start()
                experience.start()
        bluetooth_listener.join()
        #bluetooth_listener1.join()
        msgs_dispatcher.join()
        familiarization.join()
        playgame.join()
        experience.join()
    except SystemExit:
        log.error('System Exit raised!', exc_info=True)
        pass
# ===============================================================================================================
#  Main
# ===============================================================================================================
if __name__ == '__main__':
    main()