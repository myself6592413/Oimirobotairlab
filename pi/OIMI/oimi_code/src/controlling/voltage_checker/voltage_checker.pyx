""" Oimi get battery current voltage from Arduino Mega"""
# ===============================================================================================================
# Cython directives
# ===============================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ================================================================================================================
# Imports
# ================================================================================================================
cdef extract_min_voltage(voltages_arr):
	provolon = np.asarray(voltages_arr, dtype=np.float32)
	cdef float[:] provolon_view = provolon	
	min_vol = cmin(provolon_view)
	return min_vol

cpdef battery_current_voltage(serMega):
	cdef char* comm_volt = b'<volt>'
	voltages_arr = []
	
	serMega.transmit(comm_volt)
	res = serMega.get_dataReceived()
	volt = res.split()
	current_volt = float(volt[2].decode())
	voltages_arr.append(current_volt)

	min_vol = extract_min_voltage(voltages_arr)
	if(min_vol < 11.5):
		print("battery low, STOP now... the current voltage is exactly {} ".format(min_vol))
		#playsong(comm)???
	else:
		print("battery is ok, there still time, the voltage is {}".format(min_vol))