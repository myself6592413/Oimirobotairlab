# !/bin/bash
output=$(ps -aux | pgrep python)
echo $output

if [[ -z $output ]];
then
    echo "no pending python process to close"
else
    kill -9 $(ps -aux | pgrep python)
fi

echo '' > captured_output.txt
#/home/pi/oimi-venv/bin/python3.9 -Wignore main_oimi.py
#/home/pi/oimi-venv/bin/python3.9 -Wignore main_oimi.py > captured_output.txt
#/home/pi/oimi-venv/bin/python3.9 -u -Wignore main_oimi.py 
/home/pi/oimi-venv/bin/python3.9 -u -Wignore main_oimi.py | tee captured_output.txt
