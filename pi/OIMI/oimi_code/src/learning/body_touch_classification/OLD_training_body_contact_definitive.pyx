"""Info:
    Oimi robot
    # logistic_regression_test_body test gennaio new dataset
Notes:
    The "classification_report" tool provided by metrics library is not working anymore in scikit-learn last version (the one installed on raspi which is the version 1.2).
    To the metrics use another terminal/environment where a version lower than 1 of scikit-learn is installed.
    In my PC I use the version 0.24.2
    For detailed info, look at ./code_docs_of_dabatase_manager """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import numpy as np
cimport numpy as np
import joblib
import copy
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale
from sklearn.preprocessing import StandardScaler
from sklearn.exceptions import ConvergenceWarning
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import train_test_split as tts

from sklearn.metrics import hamming_loss
#from sklearn.metrics import classification_report #see notes
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, multilabel_confusion_matrix

try:
    from sklearn.utils._testing import ignore_warnings
except ImportError:
   from sklearn.utils.testing import ignore_warnings
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class training:
    #@ignore_warnings(category=ConvergenceWarning)
    def __cinit__(self, name):
        self.name = name
        self.class_weights = {'caress':1,'touch':1, 'hug':1,'squeeze':1, 'choke':1}
        self.target_names = ["caress", "touch", "hug", "squeeze", "choke"]
        #self.logreg = LogisticRegression(C=1e4, solver='lbfgs', warm_start=False, max_iter=100000000000000, 
                                            #tol=0.00001, multi_class='multinomial',class_weight=self.class_weights)

        self.logreg = LogisticRegression(C=1e4, solver='newton-cg', warm_start=True, max_iter=2500000000000000, 
                                    tol=0.00001, multi_class='multinomial',class_weight=self.class_weights)


    #@ignore_warnings(category=ConvergenceWarning)
    cpdef training_logistic_regression(self):
        self.my_dataset = pd.read_csv("/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/17genn_2023_dataset_touches.csv", skiprows=None, engine='python')
        self.Y = self.my_dataset.pop('Type_of_contact')
        dat = copy.copy(self.my_dataset)
        self.X_train, self.X_test, self.Y_train, self.Y_test = tts(self.my_dataset, self.Y, test_size=0.15)
        self.logreg.fit(self.X_train, self.Y_train)
        print("ok, training done")
        print()
        print(self.logreg)
        filename = '/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/training_body_17genn_2023.sav'
        joblib.dump(self.logreg, filename)

    cpdef get_logreg(self):
        return self.logreg

    cpdef lookfor_hyperparameters(self):
        #max_iter = range(100, 500)
        solver = ['lbfgs', 'newton-cg', 'liblinear']
        warm_start = [True, False]
        C = [0.0001,0.001,0.01,0.1,1,10,100,1000]
        tol = [0.0001,0.001,0.01,0.1,1,10,100,1000]
        random_grid ={
            'warm_start' : warm_start,
            'solver' : solver,
            'C' : C,
            'tol':tol,
        }
        estimator =LogisticRegression()
        random_estimator = RandomizedSearchCV(estimator = estimator, param_distributions = random_grid, n_iter = 100, 
                                            scoring = 'accuracy',n_jobs = -1, verbose = 1, random_state = 1,)
        random_estimator.fit(self.X_train, self.Y_train)
        #clf = RandomizedSearchCV(logreg, self.my_dataset, random_state=0)

        print(random_estimator.best_params_)

    
    cdef ccalc_metrics(self):
        yhat = self.logreg.predict(self.X_test)

        accuracy_score(self.Y_test, yhat)
    
        #as stated in notes ... classification_report is not existing anymore with new version of sklearn...
        #cr_y1 = classification_report(self.Y_test, yhat, self.target_names)
        #print(cr_y1)

        hamm = hamming_loss(self.Y_test, yhat)
        print("HAMMING LOSS")
        print(hamm)

        cm = confusion_matrix(self.Y_test, yhat, labels=self.target_names)

        disp = ConfusionMatrixDisplay(confusion_matrix=cm,display_labels=self.target_names)

        fig, ax = plt.subplots(figsize=(10,10))
        print(cm)
        disp.plot(ax=ax,xticks_rotation=45)

    @ignore_warnings(category=ConvergenceWarning)
    def calc_metrics(self):
        self.ccalc_metrics()
