"""Info:
	Oimi data acquisition sending press10 msg 
	-Max
Notes:
	For detailed info, look at ./code_docs_of_dabatase_manager
	Pressure signals of 10 / 12 / 15 samples ?

	Final sav file imported = > created by "only_training_logistic_regression.py" file
Author:
	Created by Colombo Giacomo, Politecnico di Milano 2021 """
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys
import os
if '/home/pi/OIMI/oimi_code/src/playing/touchMe_entertainment_body_contact/' not in sys.path:
	sys.path.append('/home/pi/OIMI/oimi_code/src/playing/touchMe_entertainment_body_contact/')
import re
import random
#import threading, timeit
import time
import logging
import sched
import csv
#import IMU_sensor as bimu
import contact_manager as cm

device_Address_l = 0x69   # MPU6050 device address 1
device_Address_r = 0x68	  # MPU6050 device address 2
# ==========================================================================================================================================================
# Auxiliary methods
# ==========================================================================================================================================================
def quietfile():
	rightcsv = '/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/data_acquisition/sensor_outputs_to_learn/quiet1.csv' 
	return rightcsv
def caressfile():
	rightcsv = '/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/data_acquisition/sensor_outputs_to_learn/caress2.csv' 
	return rightcsv
def touchfile():
	rightcsv = '/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/data_acquisition/sensor_outputs_to_learn/touch_original_to_refine.csv' 
	return rightcsv
def hugfile():
	rightcsv = '/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/data_acquisition/sensor_outputs_to_learn/hug1.csv' 
	return rightcsv
def squeezefile():
	rightcsv = '/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/data_acquisition/sensor_outputs_to_learn/squeeze_original_to_refine.csv' 
	return rightcsv
def chokefile():
	rightcsv = '/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/data_acquisition/sensor_outputs_to_learn/choke2.csv' 
	return rightcsv
def shovefile():
	rightcsv = '/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/data_acquisition/sensor_outputs_to_learn/shove1.csv' 
	return rightcsv

def askmeCsv():
	inter = {
		'qu': quietfile(),
		'ca': caressfile(),
		'to': touchfile(),
		'sq': squeezefile(),
		'hug': hugfile(),
		'sho': shovefile(),
		'cho': chokefile()
		}
	inp = input('Choose the interaction type of contact you want to acquire: ')
	rightcsv = inter.get(inp, -1)
	print("ok the right csv to fill is: ", rightcsv)
	return rightcsv

def choose_csv(type_t):
	if type_t=='quiet':
		rightcsv = quietfile()
	if type_t=='caress':
		rightcsv = caressfile()
	if type_t=='touch':
		rightcsv = touchfile()
	if type_t=='hug':
		rightcsv = hugfile()
	if type_t=='squeeze':
		rightcsv = squeezefile()
	if type_t=='shove':
		rightcsv = shovefile()
	if type_t=='choke':
		rightcsv = chokefile()

	print("ok the right csv to fill is: ", rightcsv)
	return rightcsv

def restart_program():
	python = sys.executable
	os.execl(python, python, * sys.argv)

# ==========================================================================================================================================================
# Main
# ==========================================================================================================================================================
def start_acquisition():
	rightcsv = askmeCsv()
	cm.contact_manager_for_data_acquisition(rightcsv)

def start_acquire_for_given_csv(type_t):
	rightcsv = choose_csv(type_t)
	cm.contact_manager_for_data_acquisition(rightcsv)

