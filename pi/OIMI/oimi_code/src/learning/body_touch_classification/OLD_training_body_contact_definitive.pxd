"""Info:
    Oimi robot
    # logistic_regression_test_body test gennaio new dataset
Notes:
    For detailed info, look at ./code_docs_of_dabatase_manager
"""
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import numpy as np
cimport numpy as np
import joblib
import copy
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale
from sklearn.preprocessing import StandardScaler
from sklearn.exceptions import ConvergenceWarning
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import train_test_split as tts

from sklearn.metrics import hamming_loss
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, multilabel_confusion_matrix
try:
    from sklearn.utils._testing import ignore_warnings
except ImportError:
    from sklearn.utils.testing import ignore_warnings
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class training():
        cdef: 
            name
            target_names
            class_weights
            Y
            logreg
            X_train
            Y_train
            X_test
            Y_test
            my_dataset
        cpdef training_logistic_regression(self)
        cpdef get_logreg(self)
        cpdef lookfor_hyperparameters(self)
        cdef ccalc_metrics(self)