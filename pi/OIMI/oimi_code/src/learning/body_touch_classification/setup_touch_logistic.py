"""
Logistic Regrassion setup file to speed up setup, better run few extensions at time, commenting the others
"""
# ==========================================================================================================================================================
#  Imports:
# ==========================================================================================================================================================
import os
import path
import io
import sys
import shutil
from distutils.core import setup
import setuptools
from distutils.extension import Extension
from Cython.Build import build_ext
from Cython.Build import cythonize
import numpy
# ==========================================================================================================================================================
#  Modules:
# ==========================================================================================================================================================
#dont comment with """, it will not work! use only # 
# non mettere in cartelle separate nulla!
ext_modules = [
 	Extension("training_body_contact_definitive", ["training_body_contact_definitive.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native','--verbose'],
		include_dirs=[numpy.get_include()],
		language='c++'),
	Extension("prediction_body_contact_definitive", ["prediction_body_contact_definitive.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native', '--verbose'],
		include_dirs=[numpy.get_include()],
		language='c++'),
	]
# ==========================================================================================================================================================
#  Class:
# ==========================================================================================================================================================
class BuildExt(build_ext):
	""" Extend Cyhon extension builder, necessary for removing annoying warnings"""
	def build_extensions(self):
		print("Building package package")
		if '-Wstrict-prototypes' in self.compiler.compiler_so:
			self.compiler.compiler_so.remove('-Wstrict-prototypes')
		super().build_extensions()
# ==========================================================================================================================================================
#  Main:
# ==========================================================================================================================================================
if sys.version_info < (3, 5):
	sys.exit("Have you activated the corrent python environment? oimi doesnt support a py version < 3.7")
	
#clean()

#for e in ext_modules:
#	e.cython_directives = {'embedsignature': True, 'boundscheck': False, 'wraparound': False, 'linetrace': True, 'language_level': "3"}

for e in ext_modules:
    e.cython_directives = {'embedsignature': True, 'boundscheck': False, 'wraparound': False, 'linetrace': True, 'language_level': "3"}


setup(
	name='oimi-robot source compilation',
	version='2.1.0',
	author='Colombo Giacomo',
	ext_modules=ext_modules,
	cmdclass = {'build_ext': BuildExt},
	)
