""" Oimi robot create session with prediction,
	code used for determine the best session suitable for one 
	Usage: 
	all paths for dataset are contained in:
	'/home/pi/OIMI/oimi_code/src/learning/games_classification/All_paths.txt'
	Notes:
		Used by database_manager.pyx
		For detailed info, look at classification_doc
		table_1, table_2, table_3, table_4, table_5 --> input => 5 learning to step to define sessions entirely """

# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import pandas as pd
import numpy as np
import joblib
import multiprocessing as mp
from multiprocessing import Pool
import pickle
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import sklearn.preprocessing as pre
from multiprocessing import Process, Pool, Queue, Manager
try:
    from sklearn.utils._testing import ignore_warnings
except ImportError:
    from sklearn.utils.testing import ignore_warnings
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.model_selection import train_test_split as tts
from sklearn.preprocessing import scale, normalize
from sklearn.exceptions import ConvergenceWarning
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_multilabel_classification
from sklearn.multioutput import MultiOutputClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_validate
from sklearn.metrics import recall_score
from sklearn.utils import shuffle
from sklearn.model_selection import cross_validate
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score, classification_report, f1_score, hamming_loss, confusion_matrix, plot_confusion_matrix
from sklearn.datasets import load_digits
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit
# ==========================================================================================================================================================
# Classes
# ==========================================================================================================================================================
class RandomForest_classification:
	def __init__(self, name, right_path_to_table, weights_flag, num_outputs_prev):
		self.name = name
		self.weights_flag = weights_flag
		self.right_path_to_table = right_path_to_table
		self.current_dataset = pd.read_csv(self.right_path_to_table, skiprows=None, engine='python', encoding="utf-8")
		# remove python engine param to find exact errors of csv, when there is something wrong
		#self.current_dataset = pd.read_csv(self.right_path_to_table, skiprows=None, encoding="utf-8", low_memory=False) 
		self.num_outputs_prev = num_outputs_prev
		self.setup_classificators()
		self.kid_settings()

	def which_class(self):
		print(self.__class__)
		print("Current subclass is: {}".format(self.getClass()))

	def getClass(self):
		#return self.__class__.__module__
		return self.__class__.__name__
	
	def get_name(self):
		self.get_the_name()
	
	def get_the_name(self):
		print("forest father name is: {}".format(self.name))
		return self.name

	def setup_classificators(self):
		if self.weights_flag:
			self.forest = RandomForestClassifier()
			self.clf = MultiOutputClassifier(self.forest)
		else:
			self.forest = RandomForestClassifier()
			#self.forest = RandomForestClassifier(random_state = 0, max_features = "auto", class_weight = "balanced", min_samples_leaf=5, max_depth = 10)	
			self.clf = MultiOutputClassifier(self.forest)
	
	def kid_settings(self):
		substring_56 = "456"	
		substring_78  = "789"
		substring_910 = "101112"
		fullstring = self.right_path_to_table
		self.kid_age="456"
		if fullstring.find(substring_56) != -1:
			self.kid_age="456"
		if fullstring.find(substring_78) != -1:
			self.kid_age="456"
		if fullstring.find(substring_910) != -1:
			self.kid_age="456"

	####################################################
	def get_classifier(self):
		with pd.option_context('display.max_rows', None, 'display.max_columns', None):	# more options can be specified also
			print(self.current_dataset)
		return self.clf

	def prepare_dataset(self):
		print("converting and shuffling")
		self.converto_to_string()
		# Shuffling the data
		self.current_dataset = shuffle(self.current_dataset, random_state=3) 
	
	def converto_to_string(self):
		'''Actual method depend on subclass override method '''
		print("Converting features...")

	def choose_correct_sav_file_training(self):
		'''Actual method depend on subclass override method '''
		print("Choosing the proper sav files for storing training results")

	def choose_correct_sav_file_training_for_table_3(self):
		'''Actual method depend on subclass override method '''
		print("Choosing the proper sav files for storing training results just for table_3")

	def training_singleoutput_classification_for_validation(self):
		try:
			#first output
			X_train, X_val, X_test = np.split(self.current_dataset.sample(frac=1), [int(.6*len(self.current_dataset)), int(.8*len(self.current_dataset))])
			#X_train, X_test, Y_train, Y_test = tts(self.current_dataset, self.Y, test_size=0.15)
			Y_1_train, Y_1_val, Y_1_test = np.split(self.Y_1.sample(frac=1), [int(.6*len(self.Y_1)), int(.8*len(self.Y_1))])

			#scaler = pre.StandardScaler().fit(X_train)
			#X_train = scaler.transform(X_train
			self.forest.fit(X_train, Y_1_train)

			self.X_train = X_train
			self.Y_1_train = Y_1_train
			self.X_test = X_test
			self.Y_1_test = Y_1_test
			self.X_val = X_val
			self.Y_1_val = Y_1_val	  
			scores_1 = self.forest.score(X_train, Y_1_train)
			scores_test_1 = self.forest.score(X_test, Y_1_test)
			cross_scores_tra_1 = cross_val_score(self.forest, X_train, Y_1_train, cv=5) 
			cross_scores_val_1 = cross_val_score(self.forest, X_val, Y_1_val, cv=5)
			cross_scores_test_1 = cross_val_score(self.forest, X_test, Y_1_test, cv=5)
			print("SCORE:")
			print(scores_1)
			print("SCORE test:")
			print(scores_test_1)

			print("CROSSSCORE val:")
			print(cross_scores_val_1)
			print("%0.2f accuracy with a standard deviation of %0.2f" % (cross_scores_val_1.mean(), cross_scores_val_1.std()))
			print("CROSSSCORE tra:")
			print(cross_scores_tra_1)
			print("%0.2f accuracy with a standard deviation of %0.2f" % (cross_scores_tra_1.mean(), cross_scores_tra_1.std()))
			print("CROSSSCORE test:")
			print(cross_scores_test_1)
			print("%0.2f accuracy with a standard deviation of %0.2f" % (cross_scores_test_1.mean(), cross_scores_test_1.std()))

			#print(len(self.current_dataset.columns))
			print("ok, singleoutput 1 fit done")
			print()
			#second output
			#X_train, X_test, Y_train, Y_test = tts(self.current_dataset, self.Y, test_size=0.15)
			Y_2_train, Y_2_val, Y_2_test = np.split(self.Y_2.sample(frac=1), [int(.6*len(self.Y_2)), int(.8*len(self.Y_2))])

			#scaler = pre.StandardScaler().fit(X_train)
			#X_train = scaler.transform(X_train
			self.forest.fit(X_train, Y_2_train)

			self.X_train = X_train
			self.Y_2_train = Y_2_train
			self.X_test = X_test
			self.Y_2_test = Y_2_test
			self.X_val = X_val
			self.Y_2_val = Y_2_val	  
			scores_2 = self.forest.score(X_train, Y_2_train)
			scores_test_2 = self.forest.score(X_test, Y_2_test)
			cross_scores_tra_2 = cross_val_score(self.forest, X_train, Y_2_train, cv=5) 
			cross_scores_val_2 = cross_val_score(self.forest, X_val, Y_2_val, cv=5)
			cross_scores_test_2 = cross_val_score(self.forest, X_test, Y_2_test, cv=5)
			print("SCORE:")
			print(scores_2)
			print("SCORE test:")
			print(scores_test_2)

			print("CROSSSCORE val:")
			print(cross_scores_val_2)
			print("%0.2f accuracy with a standard deviation of %0.2f" % (cross_scores_val_2.mean(), cross_scores_val_2.std()))
			print("CROSSSCORE tra:")
			print(cross_scores_tra_2)
			print("%0.2f accuracy with a standard deviation of %0.2f" % (cross_scores_tra_2.mean(), cross_scores_tra_2.std()))
			print("CROSSSCORE test:")
			print(cross_scores_test_2)
			print("%0.2f accuracy with a standard deviation of %0.2f" % (cross_scores_test_2.mean(), cross_scores_test_2.std()))

			#print(len(self.current_dataset.columns))
			print("ok, singleoutput 2 fit done")
			print()

			#third output
			#X_train, X_test, Y_train, Y_test = tts(self.current_dataset, self.Y, test_size=0.15)
			Y_3_train, Y_3_val, Y_3_test = np.split(self.Y_3.sample(frac=1), [int(.6*len(self.Y_3)), int(.8*len(self.Y_3))])

			#scaler = pre.StandardScaler().fit(X_train)
			#X_train = scaler.transform(X_train
			self.forest.fit(X_train, Y_3_train)

			self.X_train = X_train
			self.Y_3_train = Y_3_train
			self.X_test = X_test
			self.Y_3_test = Y_3_test
			self.X_val = X_val
			self.Y_3_val = Y_3_val	  
			scores_3 = self.forest.score(X_train, Y_3_train)
			scores_test_3 = self.forest.score(X_test, Y_3_test)
			cross_scores_tra_3 = cross_val_score(self.forest, X_train, Y_3_train, cv=5) 
			cross_scores_val_3 = cross_val_score(self.forest, X_val, Y_3_val, cv=5)
			cross_scores_test_3 = cross_val_score(self.forest, X_test, Y_3_test, cv=5)
			print("SCORE:")
			print(scores_3)
			print("SCORE test:")
			print(scores_test_3)

			print("CROSSSCORE val:")
			print(cross_scores_val_3)
			print("%0.3f accuracy with a standard deviation of %0.3f" % (cross_scores_val_3.mean(), cross_scores_val_3.std()))
			print("CROSSSCORE tra:")
			print(cross_scores_tra_3)
			print("%0.3f accuracy with a standard deviation of %0.3f" % (cross_scores_tra_3.mean(), cross_scores_tra_3.std()))
			print("CROSSSCORE test:")
			print(cross_scores_test_3)
			print("%0.3f accuracy with a standard deviation of %0.3f" % (cross_scores_test_3.mean(), cross_scores_test_3.std()))

			#print(len(self.current_dataset.columns))
			print("ok, singleoutput 3 fit done")
			print() 

			#fourth output
			#X_train, X_test, Y_train, Y_test = tts(self.current_dataset, self.Y, test_size=0.15)
			Y_4_train, Y_4_val, Y_4_test = np.split(self.Y_4.sample(frac=1), [int(.6*len(self.Y_4)), int(.8*len(self.Y_4))])

			#scaler = pre.StandardScaler().fit(X_train)
			#X_train = scaler.transform(X_train
			self.forest.fit(X_train, Y_4_train)

			self.X_train = X_train
			self.Y_4_train = Y_4_train
			self.X_test = X_test
			self.Y_4_test = Y_4_test
			self.X_val = X_val
			self.Y_4_val = Y_4_val	  
			scores_4 = self.forest.score(X_train, Y_4_train)
			scores_test_4 = self.forest.score(X_test, Y_4_test)
			cross_scores_tra_4 = cross_val_score(self.forest, X_train, Y_4_train, cv=5) 
			cross_scores_val_4 = cross_val_score(self.forest, X_val, Y_4_val, cv=5)
			cross_scores_test_4 = cross_val_score(self.forest, X_test, Y_4_test, cv=5)
			print("SCORE:")
			print(scores_4)
			print("SCORE test:")
			print(scores_test_4)

			print("CROSSSCORE val:")
			print(cross_scores_val_4)
			print("%0.4f accuracy with a standard deviation of %0.4f" % (cross_scores_val_4.mean(), cross_scores_val_4.std()))
			print("CROSSSCORE tra:")
			print(cross_scores_tra_4)
			print("%0.4f accuracy with a standard deviation of %0.4f" % (cross_scores_tra_4.mean(), cross_scores_tra_4.std()))
			print("CROSSSCORE test:")
			print(cross_scores_test_4)
			print("%0.4f accuracy with a standard deviation of %0.4f" % (cross_scores_test_4.mean(), cross_scores_test_4.std()))

			#print(len(self.current_dataset.columns))
			print("ok, singleoutput 4 fit done")
			print()		
							
		except Exception:
			print("Single output prediction score finished")

			#fifth output
			#X_train, X_test, Y_train, Y_test = tts(self.current_dataset, self.Y, test_size=0.15)
			Y_5_train, Y_5_val, Y_5_test = np.split(self.Y_5.sample(frac=1), [int(.6*len(self.Y_5)), int(.8*len(self.Y_5))])

			#scaler = pre.StandardScaler().fit(X_train)
			#X_train = scaler.transform(X_train
			self.forest.fit(X_train, Y_5_train)

			self.X_train = X_train
			self.Y_5_train = Y_5_train
			self.X_test = X_test
			self.Y_5_test = Y_5_test
			self.X_val = X_val
			self.Y_5_val = Y_5_val	  
			scores_5 = self.forest.score(X_train, Y_5_train)
			scores_test_5 = self.forest.score(X_test, Y_5_test)
			cross_scores_tra_5 = cross_val_score(self.forest, X_train, Y_5_train, cv=5) 
			cross_scores_val_5 = cross_val_score(self.forest, X_val, Y_5_val, cv=5)
			cross_scores_test_5 = cross_val_score(self.forest, X_test, Y_5_test, cv=5)
			print("SCORE:")
			print(scores_5)
			print("SCORE test:")
			print(scores_test_5)

			print("CROSSSCORE val:")
			print(cross_scores_val_5)
			print("%0.5f accuracy with a standard deviation of %0.5f" % (cross_scores_val_5.mean(), cross_scores_val_5.std()))
			print("CROSSSCORE tra:")
			print(cross_scores_tra_5)
			print("%0.5f accuracy with a standard deviation of %0.5f" % (cross_scores_tra_5.mean(), cross_scores_tra_5.std()))
			print("CROSSSCORE test:")
			print(cross_scores_test_5)
			print("%0.5f accuracy with a standard deviation of %0.5f" % (cross_scores_test_5.mean(), cross_scores_test_5.std()))

			#print(len(self.current_dataset.columns))
			print("ok, singleoutput 5 fit done")
			print() 

	def training_multioutput_classification(self):
		X_train, X_val, X_test = np.split(self.current_dataset.sample(frac=1), [int(.6*len(self.current_dataset)), int(.8*len(self.current_dataset))])
		Y_train, Y_val, Y_test = np.split(self.Y.sample(frac=1), [int(.6*len(self.Y)), int(.8*len(self.Y))])

		#X_train, X_test, Y_train, Y_test = tts(self.current_dataset, self.Y, test_size=0.15)

		#scaler = pre.StandardScaler().fit(X_train)
		#X_train = scaler.transform(X_train)
		
		self.clf.fit(X_train, Y_train)

		self.X_train = X_train
		self.Y_train = Y_train
		self.X_test = X_test
		self.Y_test = Y_test
		self.X_val = X_val
		self.Y_val = Y_val
		
		# --------------------- calc scores --------------------------- #
		#scores = self.clf.score(X_train, Y_train)
		#scores_test = self.clf.score(X_test, Y_test)
		#print("SCORE:")
		#print(scores)
		#print("SCORE test:")
		#print(scores_test)
		#print("ok, multioutup fit done")
		#print()
		
		# --------------------- If it is needed to further split the dataset into others sets/folders like complexity --------------------------- #
		#which_subclass = self.getClass()
		#if which_subclass=="RandomForest_clf_table3":
		#	print("which_subclass ok")
		#	self.check_data_for_table_3()
		#	self.choose_correct_sav_table_3()
		#	print("table_3_kid_level")
		#	print(self.table_3_kid_level)
		#	print("table_3_sess_complexity")
		#	print(self.table_3_sess_complexity)
		#	print("table_3_match_1_difficulty")
		#	print(self.table_3_match_1_difficulty)
		#else:
		#	self.choose_correct_sav_file_training()
		# --------------------- --------------------------- --------------------------- --------------------------- --------------------------- #
		self.choose_correct_sav_file_training()
		joblib.dump(self.clf, self.filename_sav)

	def predict_whole(self):
		print("Enter in whole prediction:")
		self.final_pred= self.forest.predict(self.X_test)

	def predict_instance(self):
		#which_subclass = self.getClass()
		#if which_subclass=="RandomForest_clf_table1":
		#	print("which_subclass ok")
		#	self.check_data_for_table_3()
		#	self.choose_correct_sav_table_3()
		#	print("table_3_kid_level")
		#	print(self.table_3_kid_level)
		#	print("kid_age")
		#	print(self.kid_age)
		#	print("table_3_sess_complexity")
		#	print(self.table_3_sess_complexity)
		#	print("table_3_match_1_difficulty")
		#	print(self.table_3_match_1_difficulty)
		#else:
		#	self.choose_correct_sav_file_training()
		
		print("Enter in single prediction:")
		self.choose_correct_sav_file_training()
		loaded_model = joblib.load(self.filename_sav)
		
		#self.table_predictions = pd.Series(loaded_model.predict(self.arra))
		self.table_predictions = loaded_model.predict(self.arra)
		print("table_predictions is:")
		print(self.table_predictions)
		return self.table_predictions

		'''
		n_jobs=10
		self.choose_correct_sav_file_training()
		loaded_model = joblib.load(self.filename_sav)
		pool = mp.Pool(processes=n_jobs)
		#arraok = np.asarray(self.arra).reshape(-1, 1) # Reshape input array
		arraok = np.asarray(self.arra).reshape(1, -10) # Reshape input array
		self.table_predictions = pool.map(loaded_model.predict, arraok)
		pool.close()
		pool.join()
		#self.table_predictions = pd.Series(loaded_model.predict(self.arra))
		#self.table_predictions = loaded_model.predict(self.arra)
		print("table_predictions is:")
		print(self.table_predictions)
		return self.table_predictions
		'''
		'''
		arraok = np.asarray(self.arra)
		self.num_samples, self.num_features = arraok.shape
		n_jobs=10
		self.choose_correct_sav_file_training()
		loaded_model = joblib.load(self.filename_sav)
		pool = mp.Pool(processes=n_jobs)
		inputs = [arraok[:, i].reshape(-1, 1) for i in range(self.num_features)]
		self.table_predictions = pool.map(loaded_model.predict, inputs)
		pool.close()
		pool.join()
		print("table_predictions is:")
		print(self.table_predictions)
		return self.table_predictions
		'''

	# ----------------------------------- metrics
	def metric_1(self):
		#jupyter
		cr_y1 = classification_report(self.Y_val,self.predicted_outputs)
		print(cr_y1)
	def metric_2(self):
		hamm = hamming_loss(self.Y_val,self.predicted_outputs)
		print(hamm)   
	
	def compare(self):
		print("ok,sistemodopo")
	##############################
	def score_clf(self):
		print("Scoring classificators goodness:")
		try:
			self.acc_1 = accuracy_score(self.Y_1_test, self.final_pred)
			print(self.acc_1)
			print("-----REPORT-----")
			target_names_1 = ['Easy', 'Normal', 'Medium','Hard']
			print(classification_report(self.Y_1_test, self.final_pred, target_names=target_names))
			print("--------------------------- F1_SCORE RESULT ----------------------------------------------")
			#metrics.f1_score(self.Y_test, self.Y_test, labels=np.unique(self.final_pred))
			f1_def_res_1 = f1_score(self.Y_1_test, self.final_pred, average='weighted', labels=np.unique(self.final_pred))
			print(f1_def_res_1)
			
			self.acc_2 = accuracy_score(self.Y_2_test, self.final_pred)
			print(self.acc_2)
			print("-----REPORT-----")
			target_names_2 = ['Easy', 'Normal', 'Medium','Hard']
			print(classification_report(self.Y_2_test, self.final_pred, target_names=target_names))
			print("--------------------------- F2_SCORE RESULT ----------------------------------------------")
			#metrics.f2_score(self.Y_test, self.Y_test, labels=np.unique(self.final_pred))
			f2_def_res_2 = f2_score(self.Y_2_test, self.final_pred, average='weighted', labels=np.unique(self.final_pred))
			print(f2_def_res_2)
			
			self.acc_3 = accuracy_score(self.Y_3_test, self.final_pred)
			print(self.acc_3)
			print("-----REPORT-----")
			target_names_3 = ['Easy', 'Normal', 'Medium','Hard']
			print(classification_report(self.Y_3_test, self.final_pred, target_names=target_names))
			print("--------------------------- F3_SCORE RESULT ----------------------------------------------")
			#metrics.f3_score(self.Y_test, self.Y_test, labels=np.unique(self.final_pred))
			f3_def_res_3 = f3_score(self.Y_3_test, self.final_pred, average='weighted', labels=np.unique(self.final_pred))
			print(f3_def_res_3)

			self.acc_4 = accuracy_score(self.Y_4_test, self.final_pred)
			print(self.acc_4)
			print("-----REPORT-----")
			target_names_4 = ['Easy', 'Normal', 'Medium','Hard']
			print(classification_report(self.Y_4_test, self.final_pred, target_names=target_names))
			print("--------------------------- F4_SCORE RESULT ----------------------------------------------")
			#metrics.f4_score(self.Y_test, self.Y_test, labels=np.unique(self.final_pred))
			f4_def_res_4 = f4_score(self.Y_4_test, self.final_pred, average='weighted', labels=np.unique(self.final_pred))
			print(f4_def_res_4)

			self.acc_5 = accuracy_score(self.Y_5_test, self.final_pred)
			print(self.acc_5)
			print("-----REPORT-----")
			target_names_5 = ['Easy', 'Normal', 'Medium','Hard']
			print(classification_report(self.Y_5_test, self.final_pred, target_names=target_names))
			print("--------------------------- F5_SCORE RESULT ----------------------------------------------")
			#metrics.f5_score(self.Y_test, self.Y_test, labels=np.unique(self.final_pred))
			f5_def_res_5 = f5_score(self.Y_5_test, self.final_pred, average='weighted', labels=np.unique(self.final_pred))
			print(f5_def_res_5)
		except Exception:
			print("Report concluded.")

	def calculate_loss(self):
		try:
			hamm_1 = hamming_loss(self.Y_1_test, self.final_pred)
			print("HAMMING LOSS {}".format(hamm_1))
			hamm_2 = hamming_loss(self.Y_1_test, self.final_pred)
			print("HAMMING LOSS {}".format(hamm_2))
			hamm_3 = hamming_loss(self.Y_1_test, self.final_pred)
			print("HAMMING LOSS {}".format(hamm_3))
			hamm_4 = hamming_loss(self.Y_1_test, self.final_pred)
			print("HAMMING LOSS {}".format(hamm_4))
			hamm_5 = hamming_loss(self.Y_1_test, self.final_pred)
			print("HAMMING LOSS {}".format(hamm_5))
		except Exception:
			print("Loss calcuolated.")

	def define_conf_matrix_1(self):
		#conf = confusion_matrix(self.X_test, self.Y_test, normalize='all')
		#print("confusion_matrix_2")
		#print(conf)
		try:
			plot_confusion_matrix(self.forest, self.X_test, self.Y_1_test)
			print("First matrix")
			plt.show()
			plot_confusion_matrix(self.forest, self.X_test, self.Y_2_test)
			print("Second matrix")
			plt.show()
			plot_confusion_matrix(self.forest, self.X_test, self.Y_3_test)
			print("Third matrix")
			plt.show()
			plot_confusion_matrix(self.forest, self.X_test, self.Y_4_test)
			print("Fourth matrix")
			plt.show()
			plot_confusion_matrix(self.forest, self.X_test, self.Y_5_test)
			print("Fifth matrix")
			plt.show()
		except Exception:
			print("Here's the confusion_matrix")

# ------------------------------------------------------------------------------------------------------------------------
# Dataset 1st step
# ------------------------------------------------------------------------------------------------------------------------
class RandomForest_clf_table1(RandomForest_classification): 

	def converto_to_string(self):
		# Transforming int features in strings, these columns are not removed from the table later..
		#self.current_dataset['A3']= self.current_dataset['A3'].apply(str)
		self.current_dataset['A3 max num of matches in a session']= self.current_dataset['A3 max num of matches in a session'].apply(str)
		self.current_dataset['Dummy']= self.current_dataset['Dummy'].apply(str)

	def transform_dataset(self):
		jobs_e0 = LabelEncoder()
		jobs_e0.fit(self.current_dataset['Level'])
		jobs_e1 = LabelEncoder()
		jobs_e1.fit(self.current_dataset['SY1'])
		jobs_e2 = LabelEncoder()
		jobs_e2.fit(self.current_dataset['SY2'])
		jobs_e3 = LabelEncoder()
		jobs_e3.fit(self.current_dataset['A1 range num of easy / normal / medium / hard session done'])
		jobs_e4 = LabelEncoder()
		jobs_e4.fit(self.current_dataset['A2 range num of sessions with matches in SAME/CASUAL/ASCENDENT/DESCENDENT order of difficulty'])

		self.arra_job = [jobs_e0,jobs_e1,jobs_e2,jobs_e3,jobs_e4]
		self.store_jobs()

		trans0 = jobs_e0.transform(self.current_dataset['Level'])
		trans1 = jobs_e1.transform(self.current_dataset['SY1'])
		trans2 = jobs_e2.transform(self.current_dataset['SY2'])
		trans3 = jobs_e3.transform(self.current_dataset['A1 range num of easy / normal / medium / hard session done'])
		trans4 = jobs_e4.transform(self.current_dataset['A2 range num of sessions with matches in SAME/CASUAL/ASCENDENT/DESCENDENT order of difficulty'])

		removed_ones = pd.DataFrame([self.current_dataset.pop(x) for x in ['Level', 'SY1', 'SY2', 'A1 range num of easy / normal / medium / hard session done',
			'A2 range num of sessions with matches in SAME/CASUAL/ASCENDENT/DESCENDENT order of difficulty']]).T
		
		self.current_dataset['Level']=trans0.tolist()
		self.current_dataset['SY1']=trans1.tolist()
		self.current_dataset['SY2']=trans2.tolist()
		self.current_dataset['A1 range num of easy / normal / medium / hard session done']=trans3.tolist()
		self.current_dataset['A2 range num of sessions with matches in SAME/CASUAL/ASCENDENT/DESCENDENT order of difficulty']=trans4.tolist()
		
		with pd.option_context('display.max_columns', None):
			print(self.current_dataset)

		column_1 = self.current_dataset.pop('Level')
		column_2 = self.current_dataset.pop('SY1')
		column_3 = self.current_dataset.pop('SY2')
		column_4 = self.current_dataset.pop('A1 range num of easy / normal / medium / hard session done')
		column_5 = self.current_dataset.pop('A2 range num of sessions with matches in SAME/CASUAL/ASCENDENT/DESCENDENT order of difficulty')

		# Insert column using insert(position,column_name,first_column) BACKWARD mode
		self.current_dataset.insert(0, 'A2 range num of sessions with matches in SAME/CASUAL/ASCENDENT/DESCENDENT order of difficulty', column_5)
		self.current_dataset.insert(0, 'A1 range num of easy / normal / medium / hard session done', column_4)
		self.current_dataset.insert(0, 'SY2', column_3)
		self.current_dataset.insert(0, 'SY1', column_2)
		self.current_dataset.insert(0, 'Level', column_1)
		
		self.current_dataset = shuffle(self.current_dataset, random_state=3)
		self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['Complexity', 'num of matches', 'Order difficulty Matches']]).T 
		
		self.Y['num of matches']= self.Y['num of matches'].apply(str)
		print()
		print(self.Y)
		self.Y_1 = self.Y['Complexity']
		self.Y_2 = self.Y['num of matches']
		self.Y_3 = self.Y['Order difficulty Matches']

	def choose_correct_sav_file_training(self):
		if self.kid_age=="456":
			self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table1_training.sav'
		if self.kid_age=="789":
			self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table1_training.sav'
		if self.kid_age=="101112":
			self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table1_training.sav'

	def prepare_array_for_prediction(self, dict_arra):
		filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_1.pk'

		with open(filename_pk, 'rb') as fi:
			arra_job_main = pickle.load(fi)

		df_sample = pd.DataFrame(data=dict_arra)
		print(df_sample)

		trans0 = arra_job_main[0].transform(df_sample['col1'])
		ohe0 = pd.DataFrame(trans0)
		trans1 = arra_job_main[1].transform(df_sample['col2'])
		ohe1 = pd.DataFrame(trans1)
		trans2 = arra_job_main[2].transform(df_sample['col3'])
		ohe2 = pd.DataFrame(trans2)
		trans3 = arra_job_main[3].transform(df_sample['col4'])
		ohe3 = pd.DataFrame(trans3)
		trans4 = arra_job_main[4].transform(df_sample['col5'])
		ohe4 = pd.DataFrame(trans4)

		removed_features = pd.DataFrame([df_sample.pop(x) for x in ['col1','col2','col3','col4','col5']]).T

		self.arra = pd.concat([ohe0,
						ohe1,
						ohe2,
						ohe3,
						ohe4,
						df_sample], axis=1)

	def define_class_weights(self):
		self.class_weights = {'m1 ord quest':0.5,
							  'm1 ord games diff':0.5,
							  'm1 num games':0.5,
							  'm2 ord quest':0.5,
							  'm2 ord games diff':0.5,
							  'm2 num games':0.5,
							  'm3 ord quest':0.5,
							  'm3 ord games diff':0.5,
							  'm3 num games':0.5,
							  'm4 ord quest':0.5,
							  'm4 ord games diff':0.5,
							  'm4 num games':0.5,
							  'm5 ord quest':0.5,
							  'm5 ord games diff':0.5,
							  'm5 num games':0.5}
		self.weights_flag = 1

	def store_jobs(self):
		filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/oimipickle_table_1.pk'
		with open(filename_pk, 'wb') as fi:
		# save result of training into the proper sav file
			pickle.dump(self.arra_job, fi)

# ------------------------------------------------------------------------------------------------------------------------
# Dataset 2nd step
# ------------------------------------------------------------------------------------------------------------------------
class RandomForest_clf_table2(RandomForest_classification):
	def converto_to_string(self):
		print("Nothing to convert for dataset2")

	def transform_dataset(self):
		jobs_e0 = LabelEncoder()
		jobs_e0.fit(self.current_dataset['Level'])
		jobs_e1 = LabelEncoder()
		jobs_e1.fit(self.current_dataset['Complexity'])
		jobs_e2 = LabelEncoder()
		jobs_e2.fit(self.current_dataset['Order difficulty Matches'])
		jobs_e3 = LabelEncoder()
		jobs_e3.fit(self.current_dataset['IS1'])
		jobs_e4 = LabelEncoder()
		jobs_e4.fit(self.current_dataset['N1'])
		jobs_e5 = LabelEncoder()
		jobs_e5.fit(self.current_dataset['SY3'])
		jobs_e6 = LabelEncoder()
		jobs_e6.fit(self.current_dataset['SY4'])
		jobs_e7 = LabelEncoder()
		jobs_e7.fit(self.current_dataset['A4 num of e1 / e2 / n1 / n2 / m1 / m2 / h1 / h2 matches done'])
		jobs_e8 = LabelEncoder()
		jobs_e8.fit(self.current_dataset['A5 num of matches with games with SAME / CASU / ASC / DESC quantity of questions'])
		jobs_e9 = LabelEncoder()
		jobs_e9.fit(self.current_dataset['A6 num of sessions with matches with SAME / CASU / ASC / DESC quantity of games'])

		self.arra_job = [jobs_e0,jobs_e1,jobs_e2,jobs_e3,jobs_e4,jobs_e5,jobs_e6,jobs_e7,jobs_e8,jobs_e9]
		self.store_jobs()

		trans0 = jobs_e0.transform(self.current_dataset['Level'])
		trans1 = jobs_e1.transform(self.current_dataset["Complexity"])
		trans2 = jobs_e2.transform(self.current_dataset["Order difficulty Matches"])
		trans3 = jobs_e3.transform(self.current_dataset["IS1"])
		trans4 = jobs_e4.transform(self.current_dataset["N1"])
		trans5 = jobs_e5.transform(self.current_dataset["SY3"])
		trans6 = jobs_e6.transform(self.current_dataset["SY4"])
		trans7 = jobs_e7.transform(self.current_dataset['A4 num of e1 / e2 / n1 / n2 / m1 / m2 / h1 / h2 matches done'])
		trans8 = jobs_e8.transform(self.current_dataset['A5 num of matches with games with SAME / CASU / ASC / DESC quantity of questions'])
		trans9 = jobs_e9.transform(self.current_dataset['A6 num of sessions with matches with SAME / CASU / ASC / DESC quantity of games'])

		removed_ones = pd.DataFrame([self.current_dataset.pop(x) for x in ['Level', 'Complexity', 'Order difficulty Matches',
			'IS1', 'N1', 'SY3', 'SY4', 'A4 num of e1 / e2 / n1 / n2 / m1 / m2 / h1 / h2 matches done', 
			'A5 num of matches with games with SAME / CASU / ASC / DESC quantity of questions',
			'A6 num of sessions with matches with SAME / CASU / ASC / DESC quantity of games']]).T
		
		self.current_dataset['Level']=trans0.tolist()
		self.current_dataset['Complexity']=trans1.tolist()
		self.current_dataset['Order difficulty Matches']=trans2.tolist()
		self.current_dataset['IS1']=trans3.tolist()
		self.current_dataset['N1']=trans4.tolist()
		self.current_dataset['SY3']=trans5.tolist()
		self.current_dataset['SY4']=trans6.tolist()
		self.current_dataset['A4 num of e1 / e2 / n1 / n2 / m1 / m2 / h1 / h2 matches done']=trans7.tolist()
		self.current_dataset['A5 num of matches with games with SAME / CASU / ASC / DESC quantity of questions']=trans8.tolist()
		self.current_dataset['A6 num of sessions with matches with SAME / CASU / ASC / DESC quantity of games']=trans9.tolist()

		column_1 = self.current_dataset.pop('Level')
		column_2 = self.current_dataset.pop('Complexity')
		column_3 = self.current_dataset.pop('Order difficulty Matches')
		column_4 = self.current_dataset.pop('IS1')
		column_5 = self.current_dataset.pop('N1')
		column_6 = self.current_dataset.pop('SY3')
		column_7 = self.current_dataset.pop('SY4')
		column_8 = self.current_dataset.pop('A4 num of e1 / e2 / n1 / n2 / m1 / m2 / h1 / h2 matches done')
		column_9 = self.current_dataset.pop('A5 num of matches with games with SAME / CASU / ASC / DESC quantity of questions')
		column_10 = self.current_dataset.pop('A6 num of sessions with matches with SAME / CASU / ASC / DESC quantity of games')

		#insert column using insert(position,column_name,first_column) function BACKWARD
		self.current_dataset.insert(0, 'A6 num of sessions with matches with SAME / CASU / ASC / DESC quantity of games', column_10)
		self.current_dataset.insert(0, 'A5 num of matches with games with SAME / CASU / ASC / DESC quantity of questions', column_9)
		self.current_dataset.insert(0, 'A4 num of e1 / e2 / n1 / n2 / m1 / m2 / h1 / h2 matches done', column_8)
		self.current_dataset.insert(0, 'SY4', column_7)
		self.current_dataset.insert(0, 'SY3', column_6)
		self.current_dataset.insert(0, 'N1', column_5)
		self.current_dataset.insert(0, 'IS1', column_4)
		self.current_dataset.insert(0, 'Order difficulty Matches', column_3)
		self.current_dataset.insert(0, 'Complexity', column_2)
		self.current_dataset.insert(0, 'Level', column_1)
		
		self.current_dataset = shuffle(self.current_dataset, random_state=3)
		
		if self.num_outputs_prev==1:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['match_1 difficulty', 'order quantity games']]).T 
			self.Y_1 = self.Y['match_1 difficulty']
			self.Y_2 = self.Y['order quantity games']
		if self.num_outputs_prev==2:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['match_1 difficulty', 'match_2 difficulty', 'order quantity games']]).T 
			self.Y_1 = self.Y['match_1 difficulty']
			self.Y_2 = self.Y['match_2 difficulty']
			self.Y_3 = self.Y['order quantity games']
		if self.num_outputs_prev==3:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['match_1 difficulty', 'match_2 difficulty', 'match_3 difficulty', 'order quantity games']]).T 
			self.Y_1 = self.Y['match_1 difficulty']
			self.Y_2 = self.Y['match_2 difficulty']
			self.Y_3 = self.Y['match_3 difficulty']
			self.Y_4 = self.Y['order quantity games']
		if self.num_outputs_prev==4:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['match_1 difficulty', 'match_2 difficulty', 'match_3 difficulty', 'match_4 difficulty', 'order quantity games']]).T 
			self.Y_1 = self.Y['match_1 difficulty']
			self.Y_2 = self.Y['match_2 difficulty']
			self.Y_3 = self.Y['match_3 difficulty']
			self.Y_4 = self.Y['match_4 difficulty']
			self.Y_5 = self.Y['order quantity games']

	def choose_correct_sav_file_training(self):
		if self.kid_age=="456":
			print("sono in 456") 
			if self.num_outputs_prev==1:
				print("sono in 1")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_2_training_1.sav'
			if self.num_outputs_prev==2:
				print("sono in 2")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_2_training_2.sav'
			if self.num_outputs_prev==3:
				print("sono in 3")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_2_training_3.sav'
			if self.num_outputs_prev==4:
				print("sono in 4")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_2_training_4.sav'
		if self.kid_age=="78":
			if self.num_outputs_prev==1:
				print("sono in 1")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_2_training_1.sav'
			if self.num_outputs_prev==2:
				print("sono in 2")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_2_training_2.sav'
			if self.num_outputs_prev==3:
				print("sono in 3")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_2_training_3.sav'
			if self.num_outputs_prev==4:
				print("sono in 4")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_2_training_4.sav'			
		if self.kid_age=="910":
			if self.num_outputs_prev==1:
				print("sono in 1")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_2_training_1.sav'
			if self.num_outputs_prev==2:
				print("sono in 2")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_2_training_2.sav'
			if self.num_outputs_prev==3:
				print("sono in 3")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_2_training_3.sav'
			if self.num_outputs_prev==4:
				print("sono in 4")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_2_training_4.sav'

	def prepare_array_for_prediction(self, dict_arra):
		if self.num_outputs_prev==1:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_21.pk'
		elif self.num_outputs_prev==2:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_22.pk'
		elif self.num_outputs_prev==3:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_23.pk'
		elif self.num_outputs_prev==4:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_24.pk'

		with open(filename_pk, 'rb') as fi:
			arra_job_main = pickle.load(fi)

		sample = pd.DataFrame(data=dict_arra)
		print(sample)

		trans0 = arra_job_main[0].transform(sample['col1'])
		ohe0 = pd.DataFrame(trans0)
		trans1 = arra_job_main[1].transform(sample['col2'])
		ohe1 = pd.DataFrame(trans1)
		trans2 = arra_job_main[2].transform(sample['col3'])
		ohe2 = pd.DataFrame(trans2)
		trans3 = arra_job_main[3].transform(sample['col4'])
		ohe3 = pd.DataFrame(trans3)
		trans4 = arra_job_main[4].transform(sample['col5'])
		ohe4 = pd.DataFrame(trans4)
		trans5 = arra_job_main[5].transform(sample['col6'])
		ohe5 = pd.DataFrame(trans5)
		trans6 = arra_job_main[6].transform(sample['col7'])
		ohe6 = pd.DataFrame(trans6)
		trans7 = arra_job_main[7].transform(sample['col8'])
		ohe7 = pd.DataFrame(trans7)
		trans8 = arra_job_main[8].transform(sample['col9'])
		ohe8 = pd.DataFrame(trans8)
		trans9 = arra_job_main[9].transform(sample['col10'])
		ohe9 = pd.DataFrame(trans9)

		removed_features = pd.DataFrame([sample.pop(x) for x in ['col1','col2','col3','col4','col5','col6','col7','col8','col9','col10']]).T

		self.arra = pd.concat([ohe0,
						ohe1,
						ohe2,
						ohe3,
						ohe4,
						ohe5,
						ohe6,
						ohe7,
						ohe8,
						ohe9,
						sample], axis=1)

		with pd.option_context('display.max_rows', None, 'display.max_columns', None):
			print(self.arra)

	def define_class_weights(self):
		if self.num_outputs_prev==1:
			self.class_weights = {'match_1 difficulty':0.5,'match order quantity games':0.5}
		elif self.num_outputs_prev==1:
			self.class_weights = {'match_1 difficulty':0.5,'match_2 difficulty':0.5,'match order quantity games':0.5}
		self.weights_flag = 1

	#store num_outputes of current table = Y for next table inputs + choose correct table
	def store_jobs(self):
		if self.num_outputs_prev==1:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_21.pk'
		elif self.num_outputs_prev==2:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_22.pk'
		elif self.num_outputs_prev==3:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_23.pk'
		elif self.num_outputs_prev==4:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_24.pk'		
		with open(filename_pk, 'wb') as fi:
			pickle.dump(self.arra_job, fi)

# ------------------------------------------------------------------------------------------------------------------------
# Dataset 3th step
# ------------------------------------------------------------------------------------------------------------------------
class RandomForest_clf_table3(RandomForest_classification):
	def __init__(self, name, right_path_to_table, cur_lev, weights_flag, num_outputs_prev):
		self.name = name
		self.weights_flag = weights_flag
		self.right_path_to_table = right_path_to_table
		#self.current_dataset = pd.read_csv(self.right_path_to_table, skiprows=None, engine='python', encoding="utf-8", chunksize=10)
		self.current_dataset = pd.read_csv(self.right_path_to_table, skiprows=None, engine='python', encoding="utf-8")
		self.cur_lev = cur_lev
		self.num_outputs_prev = num_outputs_prev
		self.setup_classificators()
		self.kid_settings()

	def converto_to_string(self):
		self.current_dataset['A8 max num of games in a match']= self.current_dataset['A8 max num of games in a match'].apply(str)

	def transform_dataset(self):
		if self.num_outputs_prev==1:
			jobs_e0 = LabelEncoder()
			jobs_e0.fit(self.current_dataset['Level'])
			jobs_e1 = LabelEncoder()
			jobs_e1.fit(self.current_dataset['Complexity'])
			jobs_e2 = LabelEncoder()
			jobs_e2.fit(self.current_dataset['IS2'])
			jobs_e3 = LabelEncoder()
			jobs_e3.fit(self.current_dataset['N3'])
			jobs_e4 = LabelEncoder()
			jobs_e4.fit(self.current_dataset['N4'])
			jobs_e5 = LabelEncoder()
			jobs_e5.fit(self.current_dataset['SY5'])
			jobs_e6 = LabelEncoder()
			jobs_e6.fit(self.current_dataset['SY6'])
			jobs_e7 = LabelEncoder()
			jobs_e7.fit(self.current_dataset['ST1'])
			jobs_e8 = LabelEncoder()
			jobs_e8.fit(self.current_dataset['M1 difficulty'])
			jobs_e9 = LabelEncoder()
			jobs_e9.fit(self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty'])
			#jobs_e10 = LabelEncoder()
			#jobs_e10.fit(self.current_dataset['A8 max num of games in a match'])

			#self.arra_job = [jobs_e0,jobs_e1,jobs_e2,jobs_e3,jobs_e4,jobs_e5,jobs_e6,jobs_e7,jobs_e8,jobs_e9,jobs_e10]
			self.arra_job = [jobs_e0,jobs_e1,jobs_e2,jobs_e3,jobs_e4,jobs_e5,jobs_e6,jobs_e7,jobs_e8,jobs_e9]
			self.store_jobs()

			trans0 = jobs_e0.transform(self.current_dataset['Level'])
			trans1 = jobs_e1.transform(self.current_dataset['Complexity'])
			trans2 = jobs_e2.transform(self.current_dataset['IS2'])
			trans3 = jobs_e3.transform(self.current_dataset['N3'])
			trans4 = jobs_e4.transform(self.current_dataset['N4'])
			trans5 = jobs_e5.transform(self.current_dataset['SY5'])
			trans6 = jobs_e6.transform(self.current_dataset['SY6'])
			trans7 = jobs_e7.transform(self.current_dataset['ST1'])
			trans8 = jobs_e8.transform(self.current_dataset['M1 difficulty'])
			trans9 = jobs_e9.transform(self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty'])
			#trans10 = jobs_e10.transform(self.current_dataset['A8 max num of games in a match'])

			#removed_ones = pd.DataFrame([self.current_dataset.pop(x) for x in ['Level','Complexity','IS2','N3','N4','SY5','SY6','ST1','M1 difficulty',
			#'A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty','A8 max num of games in a match']]).T

			removed_ones = pd.DataFrame([self.current_dataset.pop(x) for x in ['Level','Complexity','IS2','N3','N4','SY5','SY6','ST1','M1 difficulty',
			'A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty']]).T
		
			self.current_dataset['Level']=trans0.tolist()
			self.current_dataset['Complexity']=trans1.tolist()
			self.current_dataset['IS2']=trans2.tolist()
			self.current_dataset['N3']=trans3.tolist()
			self.current_dataset['N4']=trans4.tolist()
			self.current_dataset['SY5']=trans5.tolist()
			self.current_dataset['SY6']=trans6.tolist()
			self.current_dataset['ST1']=trans7.tolist()
			self.current_dataset['M1 difficulty']=trans8.tolist()
			self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty']=trans9.tolist()
			#self.current_dataset['A8 max num of games in a match']=trans10.tolist()

			column_1 = self.current_dataset.pop('Level')
			column_2 = self.current_dataset.pop('Complexity')
			column_3 = self.current_dataset.pop('IS2')
			column_4 = self.current_dataset.pop('N3')
			column_5 = self.current_dataset.pop('N4')
			column_6 = self.current_dataset.pop('SY5')
			column_7 = self.current_dataset.pop('SY6')
			column_8 = self.current_dataset.pop('ST1')
			column_9 = self.current_dataset.pop('M1 difficulty')
			column_10 = self.current_dataset.pop('A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty')
			#column_11 = self.current_dataset.pop('A8 max num of games in a match')


			#self.current_dataset.insert(0, 'A8 max num of games in a match', column_11)
			self.current_dataset.insert(0, 'A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty', column_10)
			self.current_dataset.insert(0, 'M1 difficulty', column_9)
			self.current_dataset.insert(0, 'ST1', column_8)
			self.current_dataset.insert(0, 'SY6', column_7)
			self.current_dataset.insert(0, 'SY5', column_6)
			self.current_dataset.insert(0, 'N4', column_5)
			self.current_dataset.insert(0, 'N3', column_4)
			self.current_dataset.insert(0, 'IS2', column_3)
			self.current_dataset.insert(0, 'Complexity', column_2)
			self.current_dataset.insert(0, 'Level', column_1)
			
			self.current_dataset = shuffle(self.current_dataset, random_state=3)
			
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['match_1 order quantity questions','match_1 order games difficulty','match_1 num of games']]).T 
			self.Y['match_1 num of games']= self.Y['match_1 num of games'].apply(str)
			self.Y_1 = self.Y['match_1 order quantity questions']
			self.Y_2 = self.Y['match_1 order games difficulty']
			self.Y_3 = self.Y['match_1 num of games']

		elif self.num_outputs_prev==2:
			jobs_e0 = LabelEncoder()
			jobs_e0.fit(self.current_dataset['Level'])
			jobs_e1 = LabelEncoder()
			jobs_e1.fit(self.current_dataset['Complexity'])
			jobs_e2 = LabelEncoder()
			jobs_e2.fit(self.current_dataset['IS2'])
			jobs_e3 = LabelEncoder()
			jobs_e3.fit(self.current_dataset['N3'])
			jobs_e4 = LabelEncoder()
			jobs_e4.fit(self.current_dataset['N4'])
			jobs_e5 = LabelEncoder()
			jobs_e5.fit(self.current_dataset['SY5'])
			jobs_e6 = LabelEncoder()
			jobs_e6.fit(self.current_dataset['SY6'])
			jobs_e7 = LabelEncoder()
			jobs_e7.fit(self.current_dataset['ST1'])
			jobs_e8 = LabelEncoder()
			jobs_e8.fit(self.current_dataset['M1 difficulty'])
			jobs_e9 = LabelEncoder()
			jobs_e9.fit(self.current_dataset['M2 difficulty'])
			jobs_e10 = LabelEncoder()
			jobs_e10.fit(self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty'])

			self.arra_job = [jobs_e0,jobs_e1,jobs_e2,jobs_e3,jobs_e4,jobs_e5,jobs_e6,jobs_e7,jobs_e8,jobs_e9,jobs_e10]
			self.store_jobs()

			trans0 = jobs_e0.transform(self.current_dataset['Level'])
			trans1 = jobs_e1.transform(self.current_dataset['Complexity'])
			trans2 = jobs_e2.transform(self.current_dataset['IS2'])
			trans3 = jobs_e3.transform(self.current_dataset['N3'])
			trans4 = jobs_e4.transform(self.current_dataset['N4'])
			trans5 = jobs_e5.transform(self.current_dataset['SY5'])
			trans6 = jobs_e6.transform(self.current_dataset['SY6'])
			trans7 = jobs_e7.transform(self.current_dataset['ST1'])
			trans8 = jobs_e8.transform(self.current_dataset['M1 difficulty'])
			trans9 = jobs_e9.transform(self.current_dataset['M2 difficulty'])
			trans10 = jobs_e10.transform(self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty'])

			removed_ones = pd.DataFrame([self.current_dataset.pop(x) for x in ['Level','Complexity','IS2','N3','N4','SY5','SY6','ST1','M1 difficulty','M2 difficulty',
			'A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty']]).T
		
			self.current_dataset['Level']=trans0.tolist()
			self.current_dataset['Complexity']=trans1.tolist()
			self.current_dataset['IS2']=trans2.tolist()
			self.current_dataset['N3']=trans3.tolist()
			self.current_dataset['N4']=trans4.tolist()
			self.current_dataset['SY5']=trans5.tolist()
			self.current_dataset['SY6']=trans6.tolist()
			self.current_dataset['ST1']=trans7.tolist()
			self.current_dataset['M1 difficulty']=trans8.tolist()
			self.current_dataset['M2 difficulty']=trans9.tolist()
			self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty']=trans10.tolist()

			column_1 = self.current_dataset.pop('Level')
			column_2 = self.current_dataset.pop('Complexity')
			column_3 = self.current_dataset.pop('IS2')
			column_4 = self.current_dataset.pop('N3')
			column_5 = self.current_dataset.pop('N4')
			column_6 = self.current_dataset.pop('SY5')
			column_7 = self.current_dataset.pop('SY6')
			column_8 = self.current_dataset.pop('ST1')
			column_9 = self.current_dataset.pop('M1 difficulty')
			column_10 = self.current_dataset.pop('M2 difficulty')
			column_11 = self.current_dataset.pop('A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty')

			self.current_dataset.insert(0, 'A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty', column_11)
			self.current_dataset.insert(0, 'M2 difficulty', column_10)
			self.current_dataset.insert(0, 'M1 difficulty', column_9)
			self.current_dataset.insert(0, 'ST1', column_8)
			self.current_dataset.insert(0, 'SY6', column_7)
			self.current_dataset.insert(0, 'SY5', column_6)
			self.current_dataset.insert(0, 'N4', column_5)
			self.current_dataset.insert(0, 'N3', column_4)
			self.current_dataset.insert(0, 'IS2', column_3)
			self.current_dataset.insert(0, 'Complexity', column_2)
			self.current_dataset.insert(0, 'Level', column_1)
			
			self.current_dataset = shuffle(self.current_dataset, random_state=3)
			
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['match_1 order quantity questions','match_1 order games difficulty',
				'match_1 num of games','match_2 order quantity questions','match_2 order games difficulty',
				'match_2 num of games']]).T
			self.Y['match_1 num of games']= self.Y['match_1 num of games'].apply(str)
			self.Y['match_2 num of games']= self.Y['match_2 num of games'].apply(str)
			self.Y_1 = self.Y['match_1 order quantity questions']
			self.Y_2 = self.Y['match_1 order games difficulty']
			self.Y_3 = self.Y['match_1 num of games']
			self.Y_4 = self.Y['match_2 order quantity questions']
			self.Y_5 = self.Y['match_2 order games difficulty']
			self.Y_6 = self.Y['match_2 num of games']		

		elif self.num_outputs_prev==3:
			jobs_e0 = LabelEncoder()
			jobs_e0.fit(self.current_dataset['Complexity'])
			jobs_e1 = LabelEncoder()
			jobs_e1.fit(self.current_dataset['IS2'])
			jobs_e2 = LabelEncoder()
			jobs_e2.fit(self.current_dataset['N3'])
			jobs_e3 = LabelEncoder()
			jobs_e3.fit(self.current_dataset['N4'])
			jobs_e4 = LabelEncoder()
			jobs_e4.fit(self.current_dataset['SY5'])
			jobs_e5 = LabelEncoder()
			jobs_e5.fit(self.current_dataset['SY6'])
			jobs_e6 = LabelEncoder()
			jobs_e6.fit(self.current_dataset['ST1'])
			jobs_e7 = LabelEncoder()
			jobs_e7.fit(self.current_dataset['M1 difficulty'])
			jobs_e8 = LabelEncoder()
			jobs_e8.fit(self.current_dataset['M2 difficulty'])
			jobs_e9 = LabelEncoder()
			jobs_e9.fit(self.current_dataset['M3 difficulty'])
			jobs_e10 = LabelEncoder()
			jobs_e10.fit(self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty'])

			self.arra_job = [jobs_e0,jobs_e1,jobs_e2,jobs_e3,jobs_e4,jobs_e5,jobs_e6,jobs_e7,jobs_e8,jobs_e9,jobs_e10]
			self.store_jobs()

			trans0 = jobs_e0.transform(self.current_dataset['Complexity'])
			trans1 = jobs_e1.transform(self.current_dataset['IS2'])
			trans2 = jobs_e2.transform(self.current_dataset['N3'])
			trans3 = jobs_e3.transform(self.current_dataset['N4'])
			trans4 = jobs_e4.transform(self.current_dataset['SY5'])
			trans5 = jobs_e5.transform(self.current_dataset['SY6'])
			trans6 = jobs_e6.transform(self.current_dataset['ST1'])
			trans7 = jobs_e7.transform(self.current_dataset['M1 difficulty'])
			trans8 = jobs_e8.transform(self.current_dataset['M2 difficulty'])
			trans9 = jobs_e9.transform(self.current_dataset['M3 difficulty'])
			trans10 = jobs_e10.transform(self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty'])

			removed_ones = pd.DataFrame([self.current_dataset.pop(x) for x in ['Complexity','IS2','N3','N4','SY5','SY6','ST1','M1 difficulty','M2 difficulty',
				'M3 difficulty','A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty']]).T
		
			self.current_dataset['Complexity']=trans0.tolist()
			self.current_dataset['IS2']=trans1.tolist()
			self.current_dataset['N3']=trans2.tolist()
			self.current_dataset['N4']=trans3.tolist()
			self.current_dataset['SY5']=trans4.tolist()
			self.current_dataset['SY6']=trans5.tolist()
			self.current_dataset['ST1']=trans6.tolist()
			self.current_dataset['M1 difficulty']=trans7.tolist()
			self.current_dataset['M2 difficulty']=trans8.tolist()
			self.current_dataset['M3 difficulty']=trans9.tolist()
			self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty']=trans10.tolist()

			column_1 = self.current_dataset.pop('Complexity')
			column_2 = self.current_dataset.pop('IS2')
			column_3 = self.current_dataset.pop('N3')
			column_4 = self.current_dataset.pop('N4')
			column_5 = self.current_dataset.pop('SY5')
			column_6 = self.current_dataset.pop('SY6')
			column_7 = self.current_dataset.pop('ST1')
			column_8 = self.current_dataset.pop('M1 difficulty')
			column_9 = self.current_dataset.pop('M2 difficulty')
			column_10 = self.current_dataset.pop('M3 difficulty')
			column_11 = self.current_dataset.pop('A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty')

			self.current_dataset.insert(0, 'A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty', column_11)
			self.current_dataset.insert(0, 'M3 difficulty', column_10)
			self.current_dataset.insert(0, 'M2 difficulty', column_9)
			self.current_dataset.insert(0, 'M1 difficulty', column_8)
			self.current_dataset.insert(0, 'ST1', column_7)
			self.current_dataset.insert(0, 'SY6', column_6)
			self.current_dataset.insert(0, 'SY5', column_5)
			self.current_dataset.insert(0, 'N4', column_4)
			self.current_dataset.insert(0, 'N3', column_3)
			self.current_dataset.insert(0, 'IS2', column_2)
			self.current_dataset.insert(0, 'Complexity', column_1)
			
			self.current_dataset = shuffle(self.current_dataset, random_state=3)
			
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['match_1 order quantity questions','match_1 order games difficulty',
				'match_1 num of games','match_2 order quantity questions','match_2 order games difficulty',
				'match_2 num of games','match_3 order quantity questions','match_3 order games difficulty',
				'match_3 num of games']]).T
			self.Y['match_1 num of games']= self.Y['match_1 num of games'].apply(str)
			self.Y['match_2 num of games']= self.Y['match_2 num of games'].apply(str)
			self.Y['match_3 num of games']= self.Y['match_3 num of games'].apply(str)

			self.Y_1 = self.Y['match_1 order quantity questions']
			self.Y_2 = self.Y['match_1 order games difficulty']
			self.Y_3 = self.Y['match_1 num of games']
			self.Y_4 = self.Y['match_2 order quantity questions']
			self.Y_5 = self.Y['match_2 order games difficulty']
			self.Y_6 = self.Y['match_2 num of games']			
			self.Y_7 = self.Y['match_3 order quantity questions']
			self.Y_8 = self.Y['match_3 order games difficulty']
			self.Y_9 = self.Y['match_3 num of games']	

		elif self.num_outputs_prev==4:
			jobs_e0 = LabelEncoder()
			jobs_e0.fit(self.current_dataset['Complexity'])
			jobs_e1 = LabelEncoder()
			jobs_e1.fit(self.current_dataset['IS2'])
			jobs_e2 = LabelEncoder()
			jobs_e2.fit(self.current_dataset['N3'])
			jobs_e3 = LabelEncoder()
			jobs_e3.fit(self.current_dataset['N4'])
			jobs_e4 = LabelEncoder()
			jobs_e4.fit(self.current_dataset['SY5'])
			jobs_e5 = LabelEncoder()
			jobs_e5.fit(self.current_dataset['SY6'])
			jobs_e6 = LabelEncoder()
			jobs_e6.fit(self.current_dataset['ST1'])
			jobs_e7 = LabelEncoder()
			jobs_e7.fit(self.current_dataset['M1 difficulty'])
			jobs_e8 = LabelEncoder()
			jobs_e8.fit(self.current_dataset['M2 difficulty'])
			jobs_e9 = LabelEncoder()
			jobs_e9.fit(self.current_dataset['M3 difficulty'])
			jobs_e10 = LabelEncoder()
			jobs_e10.fit(self.current_dataset['M4 difficulty'])
			jobs_e11 = LabelEncoder()
			jobs_e11.fit(self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty'])

			self.arra_job = [jobs_e0,jobs_e1,jobs_e2,jobs_e3,jobs_e4,jobs_e5,jobs_e6,jobs_e7,jobs_e8,jobs_e9,jobs_e10,jobs_e11]
			self.store_jobs()

			trans0 = jobs_e0.transform(self.current_dataset['Complexity'])
			trans1 = jobs_e1.transform(self.current_dataset['IS2'])
			trans2 = jobs_e2.transform(self.current_dataset['N3'])
			trans3 = jobs_e3.transform(self.current_dataset['N4'])
			trans4 = jobs_e4.transform(self.current_dataset['SY5'])
			trans5 = jobs_e5.transform(self.current_dataset['SY6'])
			trans6 = jobs_e6.transform(self.current_dataset['ST1'])
			trans7 = jobs_e7.transform(self.current_dataset['M1 difficulty'])
			trans8 = jobs_e8.transform(self.current_dataset['M2 difficulty'])
			trans9 = jobs_e9.transform(self.current_dataset['M3 difficulty'])
			trans10 = jobs_e10.transform(self.current_dataset['M4 difficulty'])
			trans11 = jobs_e11.transform(self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty'])

			removed_ones = pd.DataFrame([self.current_dataset.pop(x) for x in ['Complexity','IS2','N3','N4','SY5','SY6','ST1','M1 difficulty','M2 difficulty',
				'M3 difficulty','M4 difficulty','A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty']]).T
		
			self.current_dataset['Complexity']=trans0.tolist()
			self.current_dataset['IS2']=trans1.tolist()
			self.current_dataset['N3']=trans2.tolist()
			self.current_dataset['N4']=trans3.tolist()
			self.current_dataset['SY5']=trans4.tolist()
			self.current_dataset['SY6']=trans5.tolist()
			self.current_dataset['ST1']=trans6.tolist()
			self.current_dataset['M1 difficulty']=trans7.tolist()
			self.current_dataset['M2 difficulty']=trans8.tolist()
			self.current_dataset['M3 difficulty']=trans9.tolist()
			self.current_dataset['M4 difficulty']=trans10.tolist()
			self.current_dataset['A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty']=trans11.tolist()

			column_1 = self.current_dataset.pop('Complexity')
			column_2 = self.current_dataset.pop('IS2')
			column_3 = self.current_dataset.pop('N3')
			column_4 = self.current_dataset.pop('N4')
			column_5 = self.current_dataset.pop('SY5')
			column_6 = self.current_dataset.pop('SY6')
			column_7 = self.current_dataset.pop('ST1')
			column_8 = self.current_dataset.pop('M1 difficulty')
			column_9 = self.current_dataset.pop('M2 difficulty')
			column_10 = self.current_dataset.pop('M3 difficulty')
			column_11 = self.current_dataset.pop('M4 difficulty')
			column_12 = self.current_dataset.pop('A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty')

			self.current_dataset.insert(0, 'A7 range num of matches with games in SAME / CASU / ASC / DESC order of difficulty', column_12)
			self.current_dataset.insert(0, 'M4 difficulty', column_11)
			self.current_dataset.insert(0, 'M3 difficulty', column_10)
			self.current_dataset.insert(0, 'M2 difficulty', column_9)
			self.current_dataset.insert(0, 'M1 difficulty', column_8)
			self.current_dataset.insert(0, 'ST1', column_7)
			self.current_dataset.insert(0, 'SY6', column_6)
			self.current_dataset.insert(0, 'SY5', column_5)
			self.current_dataset.insert(0, 'N4', column_4)
			self.current_dataset.insert(0, 'N3', column_3)
			self.current_dataset.insert(0, 'IS2', column_2)
			self.current_dataset.insert(0, 'Complexity', column_1)
			
			self.current_dataset = shuffle(self.current_dataset, random_state=3)
			
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['match_1 order quantity questions','match_1 order games difficulty','match_1 num of games',
				'match_2 order quantity questions','match_2 order games difficulty','match_2 num of games',
				'match_3 order quantity questions','match_3 order games difficulty','match_3 num of games',
				'match_4 order quantity questions','match_4 order games difficulty','match_4 num of games']]).T
			self.Y['match_1 num of games']= self.Y['match_1 num of games'].apply(str)
			self.Y['match_2 num of games']= self.Y['match_2 num of games'].apply(str)
			self.Y['match_3 num of games']= self.Y['match_3 num of games'].apply(str)
			self.Y['match_4 num of games']= self.Y['match_4 num of games'].apply(str)

			self.Y_1 = self.Y['match_1 order quantity questions']
			self.Y_2 = self.Y['match_1 order games difficulty']
			self.Y_3 = self.Y['match_1 num of games']
			self.Y_4 = self.Y['match_2 order quantity questions']
			self.Y_5 = self.Y['match_2 order games difficulty']
			self.Y_6 = self.Y['match_2 num of games']			
			self.Y_7 = self.Y['match_3 order quantity questions']
			self.Y_8 = self.Y['match_3 order games difficulty']
			self.Y_9 = self.Y['match_3 num of games']	
			self.Y_10 = self.Y['match_4 order quantity questions']
			self.Y_11 = self.Y['match_4 order games difficulty']
			self.Y_12 = self.Y['match_4 num of games']	

	def choose_correct_sav_file_training(self):
		if self.kid_age=="456":
			print("sono in 456") 
			if self.num_outputs_prev==1:
				print("sono in 1")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_3_training_1.sav'
			if self.num_outputs_prev==2:
				print("sono in 2")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_3_training_2.sav'
			if self.num_outputs_prev==3:
				if self.cur_lev=="beg":
					self.table_3_kid_level = 'beginner'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_3_training_3B.sav'
				if self.cur_lev=="ele":
					self.table_3_kid_level = 'elementary'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_3_training_3E.sav'
				if self.cur_lev=="int":
					self.table_3_kid_level = 'intermediate'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_3_training_3I.sav'
				if self.cur_lev=="adv":
					self.table_3_kid_level = 'advanced'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_3_training_3A.sav'
			if self.num_outputs_prev==4:
				print("sono in 4")
				if self.cur_lev=="beg":
					self.table_3_kid_level = 'beginner'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_3_training_4B.sav'
				if self.cur_lev=="ele":
					self.table_3_kid_level = 'elementary'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_3_training_4E.sav'
				if self.cur_lev=="int":
					self.table_3_kid_level = 'intermediate'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_3_training_4I.sav'
				if self.cur_lev=="adv":
					self.table_3_kid_level = 'advanced'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_3_training_4A.sav'
		elif self.kid_age=="789":
			if self.num_outputs_prev==1:
				print("sono in 1")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_3_training_1.sav'
			if self.num_outputs_prev==2:
				print("sono in 2")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_3_training_2.sav'
			if self.num_outputs_prev==3:
				if self.cur_lev=="beg":
					self.table_3_kid_level = 'beginner'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_3_training_3B.sav'
				if self.cur_lev=="ele":
					self.table_3_kid_level = 'elementary'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_3_training_3E.sav'
				if self.cur_lev=="int":
					self.table_3_kid_level = 'intermediate'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_3_training_3I.sav'
				if self.cur_lev=="adv":
					self.table_3_kid_level = 'advanced'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_3_training_3A.sav'
			if self.num_outputs_prev==4:
				print("sono in 4")
				if self.cur_lev=="beg":
					self.table_3_kid_level = 'beginner'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_3_training_4B.sav'
				if self.cur_lev=="ele":
					self.table_3_kid_level = 'elementary'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_3_training_4E.sav'
				if self.cur_lev=="int":
					self.table_3_kid_level = 'intermediate'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_3_training_4I.sav'
				if self.cur_lev=="adv":
					self.table_3_kid_level = 'advanced'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_3_training_4A.sav'
		elif self.kid_age=="101112":
			if self.num_outputs_prev==1:
				print("sono in 1")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_3_training_1.sav'
			if self.num_outputs_prev==2:
				print("sono in 2")
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_3_training_2.sav'
			if self.num_outputs_prev==3:
				if self.cur_lev=="beg":
					self.table_3_kid_level = 'beginner'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_3_training_3B.sav'
				if self.cur_lev=="ele":
					self.table_3_kid_level = 'elementary'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_3_training_3E.sav'
				if self.cur_lev=="int":
					self.table_3_kid_level = 'intermediate'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_3_training_3I.sav'
				if self.cur_lev=="adv":
					self.table_3_kid_level = 'advanced'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_3_training_3A.sav'
			if self.num_outputs_prev==4:
				print("sono in 4")
				if self.cur_lev=="beg":
					self.table_3_kid_level = 'beginner'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_3_training_4B.sav'
				elif self.cur_lev=="ele":
					self.table_3_kid_level = 'elementary'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_3_training_4E.sav'
				elif self.cur_lev=="int":
					self.table_3_kid_level = 'intermediate'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_3_training_4I.sav'
				elif self.cur_lev=="adv":
					self.table_3_kid_level = 'advanced'
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_3_training_4A.sav'

	def prepare_array_for_prediction(self, dict_arra):
		if self.num_outputs_prev==1:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_31.pk'

			with open(filename_pk, 'rb') as fi:
				arra_job_main = pickle.load(fi)

			arra = pd.DataFrame(data=dict_arra)
			print(arra)

			trans0 = arra_job_main[0].transform(arra['col1'])
			ohe0 = pd.DataFrame(trans0)
			trans1 = arra_job_main[1].transform(arra['col2'])
			ohe1 = pd.DataFrame(trans1)
			trans2 = arra_job_main[2].transform(arra['col3'])
			ohe2 = pd.DataFrame(trans2)
			trans3 = arra_job_main[3].transform(arra['col4'])
			ohe3 = pd.DataFrame(trans3)
			trans4 = arra_job_main[4].transform(arra['col5'])
			ohe4 = pd.DataFrame(trans4)
			trans5 = arra_job_main[5].transform(arra['col6'])
			ohe5 = pd.DataFrame(trans5)
			trans6 = arra_job_main[6].transform(arra['col7'])
			ohe6 = pd.DataFrame(trans6)
			trans7 = arra_job_main[7].transform(arra['col8'])
			ohe7 = pd.DataFrame(trans7)
			trans8 = arra_job_main[8].transform(arra['col9'])
			ohe8 = pd.DataFrame(trans8)
			trans9 = arra_job_main[9].transform(arra['col10'])
			ohe9 = pd.DataFrame(trans9)
			#trans10 = arra_job_main[10].transform(arra['col11'])
			#ohe10 = pd.DataFrame(trans10)

			#removed_features = pd.DataFrame([arra.pop(x) for x in ['col1','col2','col3','col4','col5','col6','col7','col8','col9','col10','col11']]).T
			removed_features = pd.DataFrame([arra.pop(x) for x in ['col1','col2','col3','col4','col5','col6','col7','col8','col9','col10']]).T

			self.arra = pd.concat([ohe0,
							ohe1,
							ohe2,
							ohe3,
							ohe4,
							ohe5,
							ohe6,
							ohe7,
							ohe8,
							ohe9,
							arra], axis=1)

		elif self.num_outputs_prev==2:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_32.pk'

			with open(filename_pk, 'rb') as fi:
				arra_job_main = pickle.load(fi)
			arra = pd.DataFrame(data=dict_arra)

			trans0 = arra_job_main[0].transform(arra['col1'])
			ohe0 = pd.DataFrame(trans0)
			trans1 = arra_job_main[1].transform(arra['col2'])
			ohe1 = pd.DataFrame(trans1)
			trans2 = arra_job_main[2].transform(arra['col3'])
			ohe2 = pd.DataFrame(trans2)
			trans3 = arra_job_main[3].transform(arra['col4'])
			ohe3 = pd.DataFrame(trans3)
			trans4 = arra_job_main[4].transform(arra['col5'])
			ohe4 = pd.DataFrame(trans4)
			trans5 = arra_job_main[5].transform(arra['col6'])
			ohe5 = pd.DataFrame(trans5)
			trans6 = arra_job_main[6].transform(arra['col7'])
			ohe6 = pd.DataFrame(trans6)
			trans7 = arra_job_main[7].transform(arra['col8'])
			ohe7 = pd.DataFrame(trans7)
			trans8 = arra_job_main[8].transform(arra['col9'])
			ohe8 = pd.DataFrame(trans8)
			trans9 = arra_job_main[9].transform(arra['col10'])
			ohe9 = pd.DataFrame(trans9)
			trans10 = arra_job_main[10].transform(arra['col11'])
			ohe10 = pd.DataFrame(trans10)
			
			removed_features = pd.DataFrame([arra.pop(x) for x in ['col1','col2','col3','col4','col5','col6','col7','col8','col9','col10','col11']]).T

			self.arra = pd.concat([ohe0,
							ohe1,
							ohe2,
							ohe3,
							ohe4,
							ohe5,
							ohe6,
							ohe7,
							ohe8,
							ohe9,
							ohe10,
							arra], axis=1)
		
		elif self.num_outputs_prev==3:
			if self.cur_lev=="beg":			
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_33B.pk'
			elif self.cur_lev=="ele":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_33E.pk'
			elif self.cur_lev=="int":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_33I.pk'
			elif self.cur_lev=="adv":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_33A.pk'

			with open(filename_pk, 'rb') as fi:
				arra_job_main = pickle.load(fi)
			arra = pd.DataFrame(data=dict_arra)
				
			trans0 = arra_job_main[0].transform(arra['col1'])
			ohe0 = pd.DataFrame(trans0)
			trans1 = arra_job_main[1].transform(arra['col2'])
			ohe1 = pd.DataFrame(trans1)
			trans2 = arra_job_main[2].transform(arra['col3'])
			ohe2 = pd.DataFrame(trans2)
			trans3 = arra_job_main[3].transform(arra['col4'])
			ohe3 = pd.DataFrame(trans3)
			trans4 = arra_job_main[4].transform(arra['col5'])
			ohe4 = pd.DataFrame(trans4)
			trans5 = arra_job_main[5].transform(arra['col6'])
			ohe5 = pd.DataFrame(trans5)
			trans6 = arra_job_main[6].transform(arra['col7'])
			ohe6 = pd.DataFrame(trans6)
			trans7 = arra_job_main[7].transform(arra['col8'])
			ohe7 = pd.DataFrame(trans7)
			trans8 = arra_job_main[8].transform(arra['col9'])
			ohe8 = pd.DataFrame(trans8)
			trans9 = arra_job_main[9].transform(arra['col10'])
			ohe9 = pd.DataFrame(trans9)
			trans10 = arra_job_main[10].transform(arra['col11'])
			ohe10 = pd.DataFrame(trans10)

			removed_features = pd.DataFrame([arra.pop(x) for x in ['col1','col2','col3','col4','col5','col6','col7','col8','col9','col10','col11']]).T

			self.arra = pd.concat([ohe0,
							ohe1,
							ohe2,
							ohe3,
							ohe4,
							ohe5,
							ohe6,
							ohe7,
							ohe8,
							ohe9,
							ohe10,
							arra], axis=1)				

		elif self.num_outputs_prev==4:
			if self.cur_lev=="beg":			
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_34B.pk'
			elif self.cur_lev=="ele":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_34E.pk'
			elif self.cur_lev=="int":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_34I.pk'
			elif self.cur_lev=="adv":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_34A.pk'	
		
			
			with open(filename_pk, 'rb') as fi:
				arra_job_main = pickle.load(fi)
			arra = pd.DataFrame(data=dict_arra)

			trans0 = arra_job_main[0].transform(arra['col1'])
			ohe0 = pd.DataFrame(trans0)
			trans1 = arra_job_main[1].transform(arra['col2'])
			ohe1 = pd.DataFrame(trans1)
			trans2 = arra_job_main[2].transform(arra['col3'])
			ohe2 = pd.DataFrame(trans2)
			trans3 = arra_job_main[3].transform(arra['col4'])
			ohe3 = pd.DataFrame(trans3)
			trans4 = arra_job_main[4].transform(arra['col5'])
			ohe4 = pd.DataFrame(trans4)
			trans5 = arra_job_main[5].transform(arra['col6'])
			ohe5 = pd.DataFrame(trans5)
			trans6 = arra_job_main[6].transform(arra['col7'])
			ohe6 = pd.DataFrame(trans6)
			trans7 = arra_job_main[7].transform(arra['col8'])
			ohe7 = pd.DataFrame(trans7)
			trans8 = arra_job_main[8].transform(arra['col9'])
			ohe8 = pd.DataFrame(trans8)
			trans9 = arra_job_main[9].transform(arra['col10'])
			ohe9 = pd.DataFrame(trans9)
			trans10 = arra_job_main[10].transform(arra['col11'])
			ohe10 = pd.DataFrame(trans10)
			trans11 = arra_job_main[11].transform(arra['col12'])
			ohe11 = pd.DataFrame(trans11)

			removed_features = pd.DataFrame([arra.pop(x) for x in ['col1','col2','col3','col4','col5','col6','col7','col8','col9','col10','col11','col12']]).T

			self.arra = pd.concat([ohe0,
							ohe1,
							ohe2,
							ohe3,
							ohe4,
							ohe5,
							ohe6,
							ohe7,
							ohe8,
							ohe9,
							ohe10,
							ohe11,
							arra], axis=1)				
		
		with pd.option_context('display.max_rows', None, 'display.max_columns', None):	# more options can be specified also
			print(self.arra)

	def define_class_weights(self):
		if self.num_outputs_prev==1:
			self.class_weights = {
							'match_1 order quantity questions':0.5,
							'match_1 order games difficulty':0.5,
							'match_1 num of games':0.5,
							}
		if self.num_outputs_prev==2:
			self.class_weights = {
							'match_1 order quantity questions':0.5,
							'match_1 order games difficulty':0.5,
							'match_1 num of games':0.5,
							'match_2 order quantity questions':0.5,
							'match_2 order games difficulty':0.5,
							'match_2 num of games':0.5,
							}
		if self.num_outputs_prev==3:
			self.class_weights = {
							'match_1 order quantity questions':0.5,
							'match_1 order games difficulty':0.5,
							'match_1 num of games':0.5,
							'match_2 order quantity questions':0.5,
							'match_2 order games difficulty':0.5,
							'match_2 num of games':0.5,
							'match_3 order quantity questions':0.5,
							'match_3 order games difficulty':0.5,
							'match_3 num of games':0.5,
							}
		if self.num_outputs_prev==4:
			self.class_weights = {
							'match_1 order quantity questions':0.5,
							'match_1 order games difficulty':0.5,
							'match_1 num of games':0.5,
							'match_2 order quantity questions':0.5,
							'match_2 order games difficulty':0.5,
							'match_2 num of games':0.5,
							'match_3 order quantity questions':0.5,
							'match_3 order games difficulty':0.5,
							'match_3 num of games':0.5,
							'match_4 order quantity questions':0.5,
							'match_4 order games difficulty':0.5,
							'match_4 num of games':0.5,
							}
		self.weights_flag = 1

	def store_jobs(self):
		if self.num_outputs_prev==1:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_31.pk'
		elif self.num_outputs_prev==2:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_32.pk'
		elif self.num_outputs_prev==3:
			if self.cur_lev=="beg":			
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_33B.pk'
			elif self.cur_lev=="ele":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_33E.pk'
			elif self.cur_lev=="int":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_33I.pk'
			elif self.cur_lev=="adv":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_33A.pk'
		elif self.num_outputs_prev==4:
			if self.cur_lev=="beg":			
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_34B.pk'
			elif self.cur_lev=="ele":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_34E.pk'
			elif self.cur_lev=="int":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_34I.pk'
			elif self.cur_lev=="adv":
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_34A.pk'
		with open(filename_pk, 'wb') as fi:
			pickle.dump(self.arra_job, fi)

# ------------------------------------------------------------------------------------------------------------------------
# Dataset 4th step
# ------------------------------------------------------------------------------------------------------------------------
class RandomForest_clf_table4(RandomForest_classification):
	def print_test_ok(self):
		#with pd.option_context('display.max_rows', None, 'display.max_columns', None): # more options can be specified also
		with pd.option_context('display.max_columns', None):
			print(self.current_dataset)

	def converto_to_string(self):
		self.current_dataset['A9 max num of questions in a game']= self.current_dataset['A9 max num of questions in a game'].apply(str) #transforming int feature in strings

	def transform_dataset(self):
		jobs_e0 = LabelEncoder()
		jobs_e0.fit(self.current_dataset['Level'])
		jobs_e1 = LabelEncoder()
		jobs_e1.fit(self.current_dataset['Match difficulty'])
		jobs_e2 = LabelEncoder()
		jobs_e2.fit(self.current_dataset['IS3'])
		jobs_e3 = LabelEncoder()
		jobs_e3.fit(self.current_dataset['N5'])
		jobs_e4 = LabelEncoder()
		jobs_e4.fit(self.current_dataset['N6'])
		jobs_e5 = LabelEncoder()
		jobs_e5.fit(self.current_dataset['SY7'])
		jobs_e6 = LabelEncoder()
		jobs_e6.fit(self.current_dataset['SY8'])
		jobs_e7 = LabelEncoder()
		jobs_e7.fit(self.current_dataset['ST2'])
		jobs_e8 = LabelEncoder()
		jobs_e8.fit(self.current_dataset['Match order quantity of questions'])
		jobs_e9 = LabelEncoder()
		jobs_e9.fit(self.current_dataset['Match order difficulty games'])

		self.arra_job = [jobs_e0,jobs_e1,jobs_e2,jobs_e3,jobs_e4,jobs_e5,jobs_e6,jobs_e7,jobs_e8,jobs_e9]
		self.store_jobs()

		trans0 = jobs_e0.transform(self.current_dataset['Level'])
		trans1 = jobs_e1.transform(self.current_dataset['Match difficulty'])
		trans2 = jobs_e2.transform(self.current_dataset['IS3'])
		trans3 = jobs_e3.transform(self.current_dataset['N5'])
		trans4 = jobs_e4.transform(self.current_dataset['N6'])
		trans5 = jobs_e5.transform(self.current_dataset['SY7'])
		trans6 = jobs_e6.transform(self.current_dataset['SY8'])
		trans7 = jobs_e7.transform(self.current_dataset['ST2'])
		trans8 = jobs_e8.transform(self.current_dataset['Match order quantity of questions'])
		trans9 = jobs_e9.transform(self.current_dataset['Match order difficulty games'])

		removed_ones = pd.DataFrame([self.current_dataset.pop(x) for x in ['Level','Match difficulty','IS3','N5','N6','SY7','SY8','ST2',
			'Match order quantity of questions','Match order difficulty games']]).T

		self.current_dataset['Level']=trans0.tolist()
		self.current_dataset['Match difficulty']=trans1.tolist()
		self.current_dataset['IS3']=trans2.tolist()
		self.current_dataset['N5']=trans3.tolist()
		self.current_dataset['N6']=trans4.tolist()
		self.current_dataset['SY7']=trans5.tolist()
		self.current_dataset['SY8']=trans6.tolist()
		self.current_dataset['ST2']=trans7.tolist()
		self.current_dataset['Match order quantity of questions']=trans8.tolist()
		self.current_dataset['Match order difficulty games']=trans9.tolist()
		
		column_1 = self.current_dataset.pop('Level')
		column_2 = self.current_dataset.pop('Match difficulty')
		column_3 = self.current_dataset.pop('IS3')
		column_4 = self.current_dataset.pop('N5')
		column_5 = self.current_dataset.pop('N6')
		column_6 = self.current_dataset.pop('SY7')
		column_7 = self.current_dataset.pop('SY8')
		column_8 = self.current_dataset.pop('ST2')
		column_9 = self.current_dataset.pop('Match order quantity of questions')
		column_10 = self.current_dataset.pop('Match order difficulty games')

		self.current_dataset.insert(0, 'Match order difficulty games', column_10)
		self.current_dataset.insert(0, 'Match order quantity of questions', column_9)
		self.current_dataset.insert(0, 'ST2', column_8)
		self.current_dataset.insert(0, 'SY8', column_7)
		self.current_dataset.insert(0, 'SY7', column_6)
		self.current_dataset.insert(0, 'N6', column_5)
		self.current_dataset.insert(0, 'N5', column_4)
		self.current_dataset.insert(0, 'IS3', column_3)
		self.current_dataset.insert(0, 'Match difficulty', column_2)
		self.current_dataset.insert(0, 'Level', column_1)
		
		with pd.option_context('display.max_rows', None, 'display.max_columns', None):	
			print(self.current_dataset)

		self.current_dataset = shuffle(self.current_dataset, random_state=3)
			
		if self.num_outputs_prev==1:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['game_1 difficulty','game_1 kind of game','game_1 num of questions','game_1 order difficulty questions']]).T 
			self.Y['game_1 num of questions']= self.Y['game_1 num of questions'].apply(str)
			self.Y_1 = self.Y['game_1 difficulty']
			self.Y_2 = self.Y['game_1 kind of game']
			self.Y_3 = self.Y['game_1 num of questions']
			self.Y_4 = self.Y['game_1 order difficulty questions']
		if self.num_outputs_prev==2:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['game_1 difficulty','game_1 kind of game','game_1 num of questions','game_1 order difficulty questions',
				'game_2 difficulty','game_2 kind of game','game_2 num of questions','game_2 order difficulty questions']]).T 
			self.Y['game_1 num of questions']= self.Y['game_1 num of questions'].apply(str)
			self.Y['game_2 num of questions']= self.Y['game_2 num of questions'].apply(str)
			self.Y_1 = self.Y['game_1 difficulty']
			self.Y_2 = self.Y['game_1 kind of game']
			self.Y_3 = self.Y['game_1 num of questions']
			self.Y_4 = self.Y['game_1 order difficulty questions']
			self.Y_5 = self.Y['game_2 difficulty']
			self.Y_6 = self.Y['game_2 kind of game']
			self.Y_7 = self.Y['game_2 num of questions']
			self.Y_8 = self.Y['game_2 order difficulty questions']
		if self.num_outputs_prev==3:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['game_1 difficulty','game_1 kind of game','game_1 num of questions','game_1 order difficulty questions',
				'game_2 difficulty','game_2 kind of game','game_2 num of questions','game_2 order difficulty questions',
				'game_3 difficulty','game_3 kind of game','game_3 num of questions','game_3 order difficulty questions']]).T 
			self.Y['game_1 num of questions']= self.Y['game_1 num of questions'].apply(str)
			self.Y['game_2 num of questions']= self.Y['game_2 num of questions'].apply(str)
			self.Y['game_3 num of questions']= self.Y['game_3 num of questions'].apply(str)
			self.Y_1 = self.Y['game_1 difficulty']
			self.Y_2 = self.Y['game_1 kind of game']
			self.Y_3 = self.Y['game_1 num of questions']
			self.Y_4 = self.Y['game_1 order difficulty questions']
			self.Y_5 = self.Y['game_2 difficulty']
			self.Y_6 = self.Y['game_2 kind of game']
			self.Y_7 = self.Y['game_2 num of questions']
			self.Y_8 = self.Y['game_2 order difficulty questions']
			self.Y_9 = self.Y['game_3 difficulty']
			self.Y_10 = self.Y['game_3 kind of game']
			self.Y_11 = self.Y['game_3 num of questions']
			self.Y_12 = self.Y['game_3 order difficulty questions']
		if self.num_outputs_prev==4:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['game_1 difficulty','game_1 kind of game','game_1 num of questions','game_1 order difficulty questions',
				'game_2 difficulty','game_2 kind of game','game_2 num of questions','game_2 order difficulty questions',
				'game_3 difficulty','game_3 kind of game','game_3 num of questions','game_3 order difficulty questions',
				'game_4 difficulty','game_4 kind of game','game_4 num of questions','game_4 order difficulty questions']]).T 
			self.Y['game_1 num of questions']= self.Y['game_1 num of questions'].apply(str)
			self.Y['game_2 num of questions']= self.Y['game_2 num of questions'].apply(str)
			self.Y['game_3 num of questions']= self.Y['game_3 num of questions'].apply(str)
			self.Y['game_4 num of questions']= self.Y['game_4 num of questions'].apply(str)
			self.Y_1 = self.Y['game_1 difficulty']
			self.Y_2 = self.Y['game_1 kind of game']
			self.Y_3 = self.Y['game_1 num of questions']
			self.Y_4 = self.Y['game_1 order difficulty questions']
			self.Y_5 = self.Y['game_2 difficulty']
			self.Y_6 = self.Y['game_2 kind of game']
			self.Y_7 = self.Y['game_2 num of questions']
			self.Y_8 = self.Y['game_2 order difficulty questions']
			self.Y_9 = self.Y['game_3 difficulty']
			self.Y_10 = self.Y['game_3 kind of game']
			self.Y_11 = self.Y['game_3 num of questions']
			self.Y_12 = self.Y['game_3 order difficulty questions']
			self.Y_13 = self.Y['game_4 difficulty']
			self.Y_14 = self.Y['game_4 kind of game']
			self.Y_15 = self.Y['game_4 num of questions']
			self.Y_16 = self.Y['game_4 order difficulty questions']
	
	def choose_correct_sav_file_training(self):
		if self.kid_age=="456":
			if self.num_outputs_prev==1:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_4_training_1.sav'
			elif self.num_outputs_prev==2:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_4_training_2.sav'
			elif self.num_outputs_prev==3:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_4_training_3.sav'
			elif self.num_outputs_prev==4:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_4_training_4.sav'
		elif self.kid_age=="789":
			if self.num_outputs_prev==1:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_4_training_1.sav'
			elif self.num_outputs_prev==2:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_4_training_2.sav'
			elif self.num_outputs_prev==3:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_4_training_3.sav'
			elif self.num_outputs_prev==4:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/789years_old/table_4_training_4.sav'
		elif self.kid_age=="101112":
			if self.num_outputs_prev==1:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_4_training_1.sav'
			elif self.num_outputs_prev==2:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_4_training_2.sav'
			elif self.num_outputs_prev==3:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_4_training_3.sav'
			elif self.num_outputs_prev==4:
				self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/101112years_old/table_4_training_4.sav'

	def prepare_array_for_prediction(self, dict_arra):
		if self.kid_age=="456":
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_41.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_42.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_43.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_44.pk'
		if self.kid_age=="789":
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/789years_old/oimipickle_table_41.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/789years_old/oimipickle_table_42.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/789years_old/oimipickle_table_43.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/789years_old/oimipickle_table_44.pk'
		if self.kid_age=="101112":
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/101112years_old/oimipickle_table_41.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/101112years_old/oimipickle_table_42.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/101112years_old/oimipickle_table_43.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/101112years_old/oimipickle_table_44.pk'

		with open(filename_pk, 'rb') as fi:
			arra_job_main = pickle.load(fi)

		arra = pd.DataFrame(data=dict_arra)
		print(arra)

		trans0 = arra_job_main[0].transform(arra['col1'])
		ohe0 = pd.DataFrame(trans0)
		trans1 = arra_job_main[1].transform(arra['col2'])
		ohe1 = pd.DataFrame(trans1)
		trans2 = arra_job_main[2].transform(arra['col3'])
		ohe2 = pd.DataFrame(trans2)
		trans3 = arra_job_main[3].transform(arra['col4'])
		ohe3 = pd.DataFrame(trans3)
		trans4 = arra_job_main[4].transform(arra['col5'])
		ohe4 = pd.DataFrame(trans4)
		trans5 = arra_job_main[5].transform(arra['col6'])
		ohe5 = pd.DataFrame(trans5)
		trans6 = arra_job_main[6].transform(arra['col7'])
		ohe6 = pd.DataFrame(trans6)
		trans7 = arra_job_main[7].transform(arra['col8'])
		ohe7 = pd.DataFrame(trans7)
		trans8 = arra_job_main[8].transform(arra['col9'])
		ohe8 = pd.DataFrame(trans8)
		trans9 = arra_job_main[9].transform(arra['col10'])
		ohe9 = pd.DataFrame(trans9)

		removed_features = pd.DataFrame([arra.pop(x) for x in ['col1','col2','col3','col4','col5','col6','col7','col8','col9','col10']]).T

		self.arra = pd.concat([ohe0,
						ohe1,
						ohe2,
						ohe3,
						ohe4,
						ohe5,
						ohe6,
						ohe7,
						ohe8,
						ohe9,
						arra], axis=1)		

	def define_class_weights(self):
		if self.num_outputs_prev==1: 
			self.class_weights = {
							'game_1 difficulty':0.5,
							'game_1 kind of game':0.5,
							'game_1 num of questions':0.5,
							'game_1 order difficulty questions':0.5
							}
		if self.num_outputs_prev==2: 
			self.class_weights = {
							'game_1 difficulty':0.5,
							'game_1 kind of game':0.5,
							'game_1 num of questions':0.5,
							'game_1 order difficulty questions':0.5,
							'game_2 difficulty':0.5,
							'game_2 kind of game':0.5,
							'game_2 num of questions':0.5,
							'game_2 order difficulty questions':0.5
							}
		if self.num_outputs_prev==3: 
			self.class_weights = {
							'game_1 difficulty':0.5,
							'game_1 kind of game':0.5,
							'game_1 num of questions':0.5,
							'game_1 order difficulty questions':0.5,
							'game_2 difficulty':0.5,
							'game_2 kind of game':0.5,
							'game_2 num of questions':0.5,
							'game_2 order difficulty questions':0.5,
							'game_3 difficulty':0.5,
							'game_3 kind of game':0.5,
							'game_3 num of questions':0.5,
							'game_3 order difficulty questions':0.5
							}
		if self.num_outputs_prev==4: 
			self.class_weights = {
							'game_1 difficulty':0.5,
							'game_1 kind of game':0.5,
							'game_1 num of questions':0.5,
							'game_1 order difficulty questions':0.5,
							'game_2 difficulty':0.5,
							'game_2 kind of game':0.5,
							'game_2 num of questions':0.5,
							'game_2 order difficulty questions':0.5,
							'game_3 difficulty':0.5,
							'game_3 kind of game':0.5,
							'game_3 num of questions':0.5,
							'game_3 order difficulty questions':0.5,
							'game_4 difficulty':0.5,
							'game_4 kind of game':0.5,
							'game_4 num of questions':0.5,
							'game_4 order difficulty questions':0.5
							}
		self.weights_flag = 1

	def store_jobs(self):
		if self.num_outputs_prev==1:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_41.pk'
		elif self.num_outputs_prev==2:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_42.pk'
		elif self.num_outputs_prev==3:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_43.pk'
		elif self.num_outputs_prev==4:
			filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_44.pk'
		with open(filename_pk, 'wb') as fi:
			pickle.dump(self.arra_job, fi)

# ------------------------------------------------------------------------------------------------------------------------
# Dataset 5th step
# ------------------------------------------------------------------------------------------------------------------------
class RandomForest_clf_table5(RandomForest_classification):
	def __init__(self, name, right_path_to_table, kind, weights_flag, num_outputs_prev):
		self.name = name
		self.weights_flag = weights_flag
		self.right_path_to_table = right_path_to_table
		self.current_dataset = pd.read_csv(self.right_path_to_table, skiprows=None, engine='python', encoding="utf-8")
		self.num_outputs_prev = num_outputs_prev
		self.kind = kind
		self.setup_classificators()
		self.kid_settings()

	def converto_to_string(self):
		self.current_dataset['Dummy'] = self.current_dataset['Dummy'].apply(str)

	def transform_dataset(self):
		jobs_e0 = LabelEncoder()
		jobs_e0.fit(self.current_dataset['Level'])
		jobs_e1 = LabelEncoder()
		jobs_e1.fit(self.current_dataset['Game difficulty'])
		jobs_e2 = LabelEncoder()
		jobs_e2.fit(self.current_dataset['Order difficulty questions'])
		jobs_e3 = LabelEncoder()
		jobs_e3.fit(self.current_dataset['N7'])
		jobs_e4 = LabelEncoder()
		jobs_e4.fit(self.current_dataset['N8'])
		jobs_e5 = LabelEncoder()
		jobs_e5.fit(self.current_dataset['SY9'])
		jobs_e6 = LabelEncoder()
		jobs_e6.fit(self.current_dataset['SY10'])
		jobs_e7 = LabelEncoder()
		jobs_e7.fit(self.current_dataset['ST3'])

		self.arra_job = [jobs_e0,jobs_e1,jobs_e2,jobs_e3,jobs_e4,jobs_e5,jobs_e6,jobs_e7]
		self.store_jobs()

		trans0 = jobs_e0.transform(self.current_dataset['Level'])
		trans1 = jobs_e1.transform(self.current_dataset['Game difficulty'])
		trans2 = jobs_e2.transform(self.current_dataset['Order difficulty questions'])
		trans3 = jobs_e3.transform(self.current_dataset['N7'])
		trans4 = jobs_e4.transform(self.current_dataset['N8'])
		trans5 = jobs_e5.transform(self.current_dataset['SY9'])
		trans6 = jobs_e6.transform(self.current_dataset['SY10'])
		trans7 = jobs_e7.transform(self.current_dataset['ST3'])

		removed_ones = pd.DataFrame([self.current_dataset.pop(x) for x in ['Level','Game difficulty', 
		'Order difficulty questions','N7','N8','SY9','SY10','ST3']]).T
		
		self.current_dataset['Level']=trans0.tolist()
		self.current_dataset['Game difficulty']=trans1.tolist()
		self.current_dataset['Order difficulty questions']=trans2.tolist()
		self.current_dataset['N7']=trans3.tolist()
		self.current_dataset['N8']=trans4.tolist()
		self.current_dataset['SY9']=trans5.tolist()
		self.current_dataset['SY10']=trans6.tolist()
		self.current_dataset['ST3']=trans7.tolist()
		
		column_1 = self.current_dataset.pop('Level')
		column_2 = self.current_dataset.pop('Game difficulty')
		column_3 = self.current_dataset.pop('Order difficulty questions')
		column_4 = self.current_dataset.pop('N7')
		column_5 = self.current_dataset.pop('N8')
		column_6 = self.current_dataset.pop('SY9')
		column_7 = self.current_dataset.pop('SY10')
		column_8 = self.current_dataset.pop('ST3')

		self.current_dataset.insert(0, 'ST3', column_8)
		self.current_dataset.insert(0, 'SY10', column_7)
		self.current_dataset.insert(0, 'SY9', column_6)
		self.current_dataset.insert(0, 'N8', column_5)
		self.current_dataset.insert(0, 'N7', column_4)
		self.current_dataset.insert(0, 'Order difficulty questions', column_3)
		self.current_dataset.insert(0, 'Game difficulty', column_2)
		self.current_dataset.insert(0, 'Level', column_1)
		
		self.current_dataset = shuffle(self.current_dataset, random_state=3)
		#self.current_dataset = shuffle(self.current_dataset, random_state=3)
		
		if self.num_outputs_prev==1:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['question_1 type','question_1 value']]).T 
			self.Y['question_1 value'] = self.Y['question_1 value'].apply(str) 
			self.Y_1 = self.Y['question_1 type']
			self.Y_2 = self.Y['question_1 value']
		elif self.num_outputs_prev==2:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['question_1 type','question_1 value','question_2 type','question_2 value']]).T 
			self.Y['question_1 value'] = self.Y['question_1 value'].apply(str) 
			self.Y['question_2 value'] = self.Y['question_2 value'].apply(str) 
			self.Y_1 = self.Y['question_1 type']
			self.Y_2 = self.Y['question_1 value']
			self.Y_3 = self.Y['question_2 type']
			self.Y_4 = self.Y['question_2 value']
		elif self.num_outputs_prev==3:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['question_1 type','question_1 value','question_2 type','question_2 value',
				'question_3 type','question_3 value']]).T 
			self.Y['question_1 value'] = self.Y['question_1 value'].apply(str) 
			self.Y['question_2 value'] = self.Y['question_2 value'].apply(str) 
			self.Y['question_3 value'] = self.Y['question_3 value'].apply(str) 
			self.Y_1 = self.Y['question_1 type']
			self.Y_2 = self.Y['question_1 value']
			self.Y_3 = self.Y['question_2 type']
			self.Y_4 = self.Y['question_2 value']
			self.Y_5 = self.Y['question_3 type']
			self.Y_6 = self.Y['question_3 value']
		elif self.num_outputs_prev==4:
			self.Y = pd.DataFrame([self.current_dataset.pop(x) for x in ['question_1 type','question_1 value','question_2 type','question_2 value',
				'question_3 type','question_3 value','question_4 type','question_4 value']]).T 
			self.Y['question_1 value'] = self.Y['question_1 value'].apply(str) 
			self.Y['question_2 value'] = self.Y['question_2 value'].apply(str) 
			self.Y['question_3 value'] = self.Y['question_3 value'].apply(str) 
			self.Y['question_4 value'] = self.Y['question_4 value'].apply(str) 
			self.Y_1 = self.Y['question_1 type']
			self.Y_2 = self.Y['question_1 value']
			self.Y_3 = self.Y['question_2 type']
			self.Y_4 = self.Y['question_2 value']
			self.Y_5 = self.Y['question_3 type']
			self.Y_6 = self.Y['question_3 value']
			self.Y_7 = self.Y['question_4 type']
			self.Y_8 = self.Y['question_4 value']
	
	
	def choose_correct_sav_file_training(self):
		if self.kid_age=="456":
			if self.kind=='1f':
				print("kind 1f!")
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f4.sav'
			elif self.kind=='2l':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l4.sav'			
			elif self.kind=='3s':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s4.sav'
			elif self.kind=='4p':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p4.sav'
			elif self.kind=='5k':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k4.sav'
			elif self.kind=='7o':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o4.sav'
			elif self.kind=='8q':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q4.sav'
			elif self.kind=='9c':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c4.sav'
		if self.kid_age=="78":
			if self.kind=='1f':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f4.sav'
			elif self.kind=='2l':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l4.sav'			
			elif self.kind=='3s':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s4.sav'
			elif self.kind=='4p':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p4.sav'
			elif self.kind=='5k':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k4.sav'
			elif self.kind=='7o':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o4.sav'
			elif self.kind=='8q':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q4.sav'
			elif self.kind=='9c':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c4.sav'
		if self.kid_age=="910":
			if self.kind=='1f':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training1f4.sav'
			elif self.kind=='2l':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training2l4.sav'			
			elif self.kind=='3s':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training3s4.sav'
			elif self.kind=='4p':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training4p4.sav'
			elif self.kind=='5k':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training5k4.sav'
			elif self.kind=='7o':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training7o4.sav'
			elif self.kind=='8q':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training8q4.sav'
			elif self.kind=='9c':
				if self.num_outputs_prev==1:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c1.sav'
				elif self.num_outputs_prev==2:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c2.sav'
				elif self.num_outputs_prev==3:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c3.sav'
				elif self.num_outputs_prev==4:
					self.filename_sav = '/home/pi/OIMI/oimi_code/src/learning/new_training_sav23/456years_old/table_5_training9c4.sav'

	def prepare_array_for_prediction(self, dict_arra):
		if self.kind=='1f':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_51f1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_51f2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_51f3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_51f4.pk'
		elif self.kind=='2l':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_52l1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_52l2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_52l3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_52l4.pk'			
		elif self.kind=='3s':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_53s1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_53s2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_53s3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_53s4.pk'
		elif self.kind=='4p':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_54p1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_54p2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_54p3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_54p4.pk'
		elif self.kind=='5k':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_55k1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_55k2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_55k3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_55k4.pk'
		elif self.kind=='7o':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_57o1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_57o2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_57o3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_57o4.pk'
		elif self.kind=='8q':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_58q1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_58q2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_58q3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_58q4.pk'
		elif self.kind=='9c':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_59c1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_59c2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_59c3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_59c4.pk'

		with open(filename_pk, 'rb') as fi:
			arra_job_main = pickle.load(fi)

		arra = pd.DataFrame(data=dict_arra)

		trans0 = arra_job_main[0].transform(arra['col1'])
		ohe0 = pd.DataFrame(trans0)
		trans1 = arra_job_main[1].transform(arra['col2'])
		ohe1 = pd.DataFrame(trans1)
		trans2 = arra_job_main[2].transform(arra['col3'])
		ohe2 = pd.DataFrame(trans2)
		trans3 = arra_job_main[3].transform(arra['col4'])
		ohe3 = pd.DataFrame(trans3)
		trans4 = arra_job_main[4].transform(arra['col5'])
		ohe4 = pd.DataFrame(trans4)
		trans5 = arra_job_main[5].transform(arra['col6'])
		ohe5 = pd.DataFrame(trans5)
		trans6 = arra_job_main[6].transform(arra['col7'])
		ohe6 = pd.DataFrame(trans6)
		trans7 = arra_job_main[7].transform(arra['col8'])
		ohe7 = pd.DataFrame(trans7)

		removed_features = pd.DataFrame([arra.pop(x) for x in ['col1','col2','col3','col4','col5','col6','col7','col8']]).T

		self.arra = pd.concat([ohe0,
						ohe1,
						ohe2,
						ohe3,
						ohe4,
						ohe5,
						ohe6,
						ohe7,
						arra], axis=1)		

	def define_class_weights(self):
		if self.num_outputs_prev==1:
			self.class_weights = {'question_1 type':0.5}
		elif self.num_outputs_prev==2:
			self.class_weights = {'question_1 type':0.5,
								  'question_2 type':0.5}
		elif self.num_outputs_prev==3:
			self.class_weights = {'question_1 type':0.5,
								  'question_2 type':0.5,
								  'question_3 type':0.5}
		elif self.num_outputs_prev==4:
				self.class_weights = {'question_1 type':0.5,
								  'question_2 type':0.5,
								  'question_3 type':0.5,
								  'question_4 type':0.5}
				self.weights_flag = 1

	def store_jobs(self):
		if self.kind=='1f':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_51f1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_51f2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_51f3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_51f4.pk'
		elif self.kind=='2l':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_52l1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_52l2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_52l3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_52l4.pk'			
		elif self.kind=='3s':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_53s1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_53s2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_53s3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_53s4.pk'
		elif self.kind=='4p':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_54p1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_54p2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_54p3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_54p4.pk'
		elif self.kind=='5k':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_55k1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_55k2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_55k3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_55k4.pk'
		elif self.kind=='7o':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_57o1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_57o2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_57o3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_57o4.pk'
		elif self.kind=='8q':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_58q1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_58q2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_58q3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_58q4.pk'
		elif self.kind=='9c':
			if self.num_outputs_prev==1:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_59c1.pk'
			elif self.num_outputs_prev==2:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_59c2.pk'
			elif self.num_outputs_prev==3:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_59c3.pk'
			elif self.num_outputs_prev==4:
				filename_pk = '/home/pi/OIMI/oimi_code/src/learning/new_pickles23/456years_old/oimipickle_table_59c4.pk'

		with open(filename_pk, 'wb') as fi:
			pickle.dump(self.arra_job, fi)
# ---------------------------------------------------------------------------------------------------
# Main
# ---------------------------------------------------------------------------------------------------
if __name__=='__main__':
	with open('/home/pi/OIMI/oimi_code/src/learning/games_classification/All_paths/complete_456_csv_patchs.txt') as f:
		lines = f.read().splitlines()
	# Training 
	#classificator = RandomForest_clf_table1(name='table_1', right_path_to_table = lines[0], num_outputs_prev = 0, weights_flag=0)
	#classificator = RandomForest_clf_table2(name='table_2_1m', right_path_to_table = lines[1], num_outputs_prev = 1, weights_flag=0)
	#classificator = RandomForest_clf_table2(name='table_2_2m', right_path_to_table = lines[2], num_outputs_prev = 2, weights_flag=0)
	#classificator = RandomForest_clf_table2(name='table_2_3m', right_path_to_table = lines[3], num_outputs_prev = 3, weights_flag=0)
	#classificator = RandomForest_clf_table2(name='table_2_4m', right_path_to_table = lines[4], num_outputs_prev = 4, weights_flag=0)
	#classificator = RandomForest_clf_table3(name='table_3_1m', right_path_to_table = lines[5], num_outputs_prev = 1, cur_lev = 'whatever', weights_flag=0)
	#classificator = RandomForest_clf_table3(name='table_3_2m', right_path_to_table = lines[6], num_outputs_prev = 2, cur_lev = 'whatever', weights_flag=0)
	#classificator = RandomForest_clf_table3(name='table_3_3m_b', right_path_to_table = lines[7], num_outputs_prev = 3, cur_lev = 'beg', weights_flag=0)
	#classificator = RandomForest_clf_table3(name='table_3_3m_e', right_path_to_table = lines[8], num_outputs_prev = 3, cur_lev = 'ele', weights_flag=0)
	#classificator = RandomForest_clf_table3(name='table_3_3m_i', right_path_to_table = lines[9], num_outputs_prev = 3, cur_lev = 'int', weights_flag=0)
	#classificator = RandomForest_clf_table3(name='table_3_3m_a', right_path_to_table = lines[10], num_outputs_prev = 3, cur_lev = 'adv', weights_flag=0)
	#classificator = RandomForest_clf_table3(name='table_3_4m_b', right_path_to_table = lines[11], num_outputs_prev = 4, cur_lev = 'beg', weights_flag=0)
	#classificator = RandomForest_clf_table3(name='table_3_4m_e', right_path_to_table = lines[12], num_outputs_prev = 4, cur_lev = 'ele', weights_flag=0)
	# classificator = RandomForest_clf_table3(name='table_3_4m_i', right_path_to_table = lines[13], num_outputs_prev = 4, cur_lev = 'int', weights_flag=0)
	# classificator = RandomForest_clf_table3(name='table_3_4m_a', right_path_to_table = lines[14], num_outputs_prev = 4, cur_lev = 'adv', weights_flag=0)
	# classificator = RandomForest_clf_table4(name='table_4_4g_b', right_path_to_table = lines[15], num_outputs_prev = 1, weights_flag=0)
	# classificator = RandomForest_clf_table4(name='table_4_4g_e', right_path_to_table = lines[16], num_outputs_prev = 2, weights_flag=0)
	# classificator = RandomForest_clf_table4(name='table_4_4g_i', right_path_to_table = lines[17], num_outputs_prev = 3, weights_flag=0)
	# classificator = RandomForest_clf_table4(name='table_4_4g_a', right_path_to_table = lines[18], num_outputs_prev = 4, weights_flag=0)

	#classificator = RandomForest_clf_table5(name='table_5_1q_1f', right_path_to_table = lines[19], num_outputs_prev = 1, kind = '1f', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_2q_1f', right_path_to_table = lines[20], num_outputs_prev = 2, kind = '1f', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_3q_1f', right_path_to_table = lines[21], num_outputs_prev = 3, kind = '1f', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_4q_1f', right_path_to_table = lines[22], num_outputs_prev = 4, kind = '1f', weights_flag=0)
	#classificator = RandomForest_clf_table5(name='table_5_1q_2l', right_path_to_table = lines[23], num_outputs_prev = 1, kind = '2l', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_2q_2l', right_path_to_table = lines[24], num_outputs_prev = 2, kind = '2l', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_3q_2l', right_path_to_table = lines[25], num_outputs_prev = 3, kind = '2l', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_4q_2l', right_path_to_table = lines[26], num_outputs_prev = 4, kind = '2l', weights_flag=0)
	
	# classificator = RandomForest_clf_table5(name='table_5_1q_3s', right_path_to_table = lines[27], num_outputs_prev = 1, kind = '3s', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_2q_3s', right_path_to_table = lines[28], num_outputs_prev = 2, kind = '3s', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_3q_3s', right_path_to_table = lines[29], num_outputs_prev = 3, kind = '3s', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_4q_3s', right_path_to_table = lines[30], num_outputs_prev = 4, kind = '3s', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_1q_4p', right_path_to_table = lines[31], num_outputs_prev = 1, kind = '4p', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_2q_4p', right_path_to_table = lines[32], num_outputs_prev = 2, kind = '4p', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_3q_4p', right_path_to_table = lines[33], num_outputs_prev = 3, kind = '4p', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_4q_4p', right_path_to_table = lines[34], num_outputs_prev = 4, kind = '4p', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_1q_5k', right_path_to_table = lines[35], num_outputs_prev = 1, kind = '5k', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_2q_5k', right_path_to_table = lines[36], num_outputs_prev = 2, kind = '5k', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_3q_5k', right_path_to_table = lines[37], num_outputs_prev = 3, kind = '5k', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_4q_5k', right_path_to_table = lines[38], num_outputs_prev = 4, kind = '5k', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_1q_7o', right_path_to_table = lines[39], num_outputs_prev = 1, kind = '7o', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_2q_7o', right_path_to_table = lines[40], num_outputs_prev = 2, kind = '7o', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_3q_7o', right_path_to_table = lines[41], num_outputs_prev = 3, kind = '7o', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_4q_7o', right_path_to_table = lines[42], num_outputs_prev = 4, kind = '7o', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_1q_8q', right_path_to_table = lines[43], num_outputs_prev = 1, kind = '8q', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_2q_8q', right_path_to_table = lines[44], num_outputs_prev = 2, kind = '8q', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_3q_8q', right_path_to_table = lines[45], num_outputs_prev = 3, kind = '8q', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_4q_8q', right_path_to_table = lines[46], num_outputs_prev = 4, kind = '8q', weights_flag=0)
	#classificator = RandomForest_clf_table5(name='table_5_1q_9c', right_path_to_table = lines[47], num_outputs_prev = 1, kind = '9c', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_2q_9c', right_path_to_table = lines[48], num_outputs_prev = 2, kind = '9c', weights_flag=0)
	# classificator = RandomForest_clf_table5(name='table_5_3q_9c', right_path_to_table = lines[49], num_outputs_prev = 3, kind = '9c', weights_flag=0)
	#classificator = RandomForest_clf_table5(name='table_5_4q_9c', right_path_to_table = lines[50], num_outputs_prev = 4, kind = '9c', weights_flag=0)



	classificator.prepare_dataset()
	classificator.transform_dataset()
	classificator.training_multioutput_classification()
	"""
	"""

	# Prediction trials
	############################################################ dict 1
	dict_for_table_1 = {'col1': ['andvanced'],
	 'col2': ['diversion'],
	 'col3': ['endurance'],
	 'col4': ['AAAA'],
	 'col5': ['AAAA'],
	 'col6': [0],
	 'col7': [0]}
	############################################################ dict 2
	dict_for_table_2 = {'col1': ['beginner'],
		 'col2': ['Easy'],
		 'col3': ['same'],
		 'col4': ['play_leisure_skills'],
		 'col5': ['focus_on concentration'],
		 'col6': ['listening'],
		 'col7': ['none'],
		 'col8': ['CACABBAA'],
		 'col9': ['AAAA'],
		 'col10': ['ABACACBA']}
	############################################################ dict 31
	dict_for_table_31 = {
		'col1': ['beginner'],
		'col2': ['Easy'],
		'col3': ['tolerance'],
		'col4': ['focus_automation'],
		'col5': ['focus_concentration'],
		'col6': ['restriction'],
		'col7': ['rules'],
		'col8': ['comprehension'],
		'col9': ['Normal_1'],
		'col10': ['AAAA'],
		'col11': [0]}
	############################################################ dict 32
	dict_for_table_32 = {		
		'col1': ['beginner'],
		'col2': ['Easy'],
		'col3': ['none'],
		'col4': ['focus_concentration'],
		'col5': ['none'],
		'col6': ['none'],
		'col7': ['rules'],
		'col8': ['comprehension'],
		'col9': ['Normal_1'],
		'col10': ['Normal_2'],
		'col11': ['AAAA'],
		'col12': [0]}
	############################################################ dict 33
	dict_for_table_33 = {
		'col1': ['Easy'],
		'col2': ['none'],
		'col3': ['focus_concentration'],
		'col4': ['none'],
		'col5': ['none'],
		'col6': ['rules'],
		'col7': ['comprehension'],
		'col8': ['Normal_1'],
		'col9': ['Normal_2'],
		'col10': ['Medium_1'],
		'col11': ['AAAA'],
		'col12': [0]}
	############################################################ dict 34
	dict_for_table_34 = {
		'col1': ['Easy'],
		'col2': ['none'],
		'col3': ['focus_concentration'],
		'col4': ['none'],
		'col5': ['none'],
		'col6': ['rules'],
		'col7': ['comprehension'],
		'col8': ['Normal_1'],
		'col9': ['Medium_2'],
		'col10': ['Hard_1'],
		'col11': ['Hard_2'],
		'col12': ['AAAA'],
		'col13': [0]}
	############################################################ dict 4
	dict_for_table_4 = {
		'col1': ['beginner'],
		'col2': ['Hard_2'],
		'col3': ['cognitive'],
		'col4': ['focus_automation'],
		'col5': ['focus_change'],
		'col6': ['none'],
		'col7': ['calculate'],
		'col8': ['memory'],
		'col9': ['asc'],
		'col10': ['desc'],
		'col11': [2]}
	############################################################ dict 5
	dict_for_table_5 = {
		'col1': ['advanced'],
		'col2': ['Hard_2'],
		'col3': ['asc'],
		'col4': ['focus_automation'],
		'col5': ['focus_error'],
		'col6': ['restriction'],
		'col7': ['none'],
		'col8': ['none'],
		'col9': [2]}

	classificator.prepare_array_for_prediction(dict_for_table_5)
	result_class = classificator.predict_instance()
	print(result_class[0][0])
	print(result_class[0][1])
