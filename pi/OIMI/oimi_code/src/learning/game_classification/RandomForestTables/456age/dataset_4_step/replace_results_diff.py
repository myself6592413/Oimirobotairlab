import csv
import random

#mapping_set = {"Easy_1", "Easy_2", "Normal_1", "Normal_2", "Medium_1", "Medium_2", "Hard_1", "Hard_2"}
#mapping_set = {"1F", "2L", "3S", "4P", "5K", "6I", "7O", "8Q", "9C"}
#mapping_set = {1,2,3,4}
mapping_set = {"same", "casu", "asc", "desc"}
#mapping_set = {"piopio","baubau"}

# Open the input and output files
with open('eccolo_42.csv', 'r') as infile, open('eccolo_43.csv', 'w', newline='') as outfile:
    reader = csv.reader(infile)
    writer = csv.writer(outfile, quoting=csv.QUOTE_ALL)
    # Iterate over each row in the input file
    for row in reader:
        # Check if a random value is less than or equal to 0.4 (40% chance)
        #if random.random() <= 0.4:
        # Choose a new value from the mapping set
        non_overlapping_elements = mapping_set - set(row)
        if non_overlapping_elements:
            new_value = random.choice(list(non_overlapping_elements))
        else:
            new_value = random.choice(list(mapping_set))  # Handle the case where there are no non-overlapping elements
        # Replace the old value with the new value
        row[-1] = new_value
        # Write the modified row to the output file
        writer.writerow(row)