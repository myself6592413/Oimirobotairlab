import sys
import pandas as pd
import numpy as np
import random
import csv
from contextlib import redirect_stdout


listone_papabili_beg = ['ApproachMe', 'PutPatches', 'GuessMyFeeling', 'TouchMe', 'AutonomousMe']
listone_papabili_ele = ['DanceWithMe','ApproachMe', 'PutPatches', 'WakeMeUp', 'ImitateMe', 'GuessMyFeeling', 'TouchMe', 'AutonomousMe', 'FollowingYou']
listone_papabili_int = ['ApproachMe', 'PutPatches', 'WakeMeUp', 'ImitateMe', 'GuessMyFeeling', 'TouchMe', 'AutonomousMe', 'FollowingYou', 'RunningChasingYou']
listone_papabili_adv = ['ApproachMe', 'PutPatches', 'WakeMeUp', 'DontWakeMeUp', 'ImitateMe', 'GuessMyFeeling', 'TouchMe', 'AutonomousMe', 'FollowingYou', 'RunningChasingYou', 'DriveMe']

listone_locomotion_b = ['AutonomousMe']
listone_locomotion_e = ['AutonomousMe', 'FollowingYou']
listone_locomotion_i = ['AutonomousMe', 'FollowingYou', 'RunningChasingYou']
listone_locomotion_a = ['AutonomousMe', 'FollowingYou', 'RunningChasingYou', 'DriveMe']

listone_socialization_b = ['GuessMyFeeling']
listone_socialization_e = ['ImitateMe', 'GuessMyFeeling']
listone_socialization_i = ['ImitateMe', 'GuessMyFeeling', 'DanceWithMe']
listone_socialization_a = ['ImitateMe', 'GuessMyFeeling']

listone_familiarization_b = ['ApproachMe', 'PutPatches']
listone_familiarization_e = ['ApproachMe', 'PutPatches', 'WakeMeUp']
listone_familiarization_i = ['ApproachMe', 'PutPatches', 'WakeMeUp']
listone_familiarization_a = ['ApproachMe', 'PutPatches', 'WakeMeUp', 'DontWakeMeUp']

listone_papabili_sicurezza_b = ['ApproachMe', 'PutPatches', 'GuessMyFeeling', 'TouchMe']
listone_papabili_sicurezza_e = ['ApproachMe', 'PutPatches', 'WakeMeUp', 'ImitateMe', 'GuessMyFeeling', 'TouchMe']
listone_papabili_sicurezza_i = ['ApproachMe', 'PutPatches', 'WakeMeUp', 'ImitateMe', 'GuessMyFeeling', 'TouchMe']
listone_papabili_sicurezza_a = ['ApproachMe', 'PutPatches', 'WakeMeUp', 'DontWakeMeUp', 'ImitateMe', 'GuessMyFeeling', 'TouchMe']

def funzia(x,param):
	#if pd.isnull(x['free_game']):
	#	if x['Level']=='beginner':
	#		x['free_game'] = random.choice(listone_papabili_beg)
	##	elif x['Level']=='elementary':
	##		x['free_game'] = random.choice(listone_papabili_ele)
	#	elif x['Level']=='intermediate':
	##		x['free_game'] = random.choice(listone_papabili_int)
	#	elif x['Level']=='advanced':
	#		x['free_game'] = random.choice(listone_papabili_adv)
	
	#@########@#@#@###############@#@@@#############@########@#@#@###############@#@@@############
	if x['Level']=='beginner':
		if x['perc_fam']==1 and x['perc_tou']==0 and x['perc_soc']==0 and x['perc_loc']==0:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_locomotion_b)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_socialization_b)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'
		if x['perc_fam']==0 and x['perc_tou']==0 and x['perc_soc']==0 and x['perc_loc']==1:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_familiarization_b)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_socialization_b)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'
		if x['perc_fam']==0 and x['perc_tou']==0 and x['perc_soc']==1 and x['perc_loc']==0:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_locomotion_b)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_familiarization_b)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'		
		if x['perc_fam']==0 and x['perc_tou']==1 and x['perc_soc']==0 and x['perc_loc']==0:
			ran = random.randint(1,3)
			if ran==1:
				chosen_game = random.choice(listone_locomotion_b)
			elif ran==2:
				chosen_game = random.choice(listone_familiarization_b)
			elif ran==3:
				chosen_game = random.choice(listone_socialization_b)
			x['free_game'] = chosen_game
		###########################################################################################################		
		if (x['perc_fam'] != 1 and ['perc_tou'] != 1 and x['perc_soc'] != 1 and x['perc_loc']!=1)
		and (x['perc_fam'] <= x['perc_tou'] <= x['perc_soc'] <= x['perc_loc']==0):
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1:
					x['free_game'] = 'TouchMe'
				elif ran2==3 and ran2==3:
					chosen_game = random.choice(listone_socialization_b)
					x['free_game'] = chosen_game
			#if ran=2=:
			#	ran2 = random.randint(1,6)
			#	if ran2==1:
			#		chosen_game = random.choice(listone_familiarization_b)
			#	elif ran2==2 and ran2==3 and ran2==4:
			#		chosen_game = random.choice(listone_socialization_b)
			#	elif ran2==6 and ran2==7:
			#		chosen_game = random.choice(listone_locomotion_b)
			#	x['free_game'] = chosen_game
		if (x['perc_fam'] != 1 and ['perc_tou'] != 1 and x['perc_soc'] != 1 and x['perc_loc']!=1)
		and (x['perc_tou'] <= x['perc_fam'] <= x['perc_soc'] <= x['perc_loc']==0):
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,5)
				if ran2==1 and ran2==2:
					x['free_game'] = 'TouchMe'
				elif ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_b)
					x['free_game'] = chosen_game
				elif ran2==5:
					chosen_game = random.choice(listone_familiarization_b)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,3)
				if ran2==1:
					chosen_game = random.choice(listone_locomotion_b)
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_b)
				x['free_game'] = chosen_game
		if (x['perc_fam'] != 1 and ['perc_tou'] != 1 and x['perc_soc'] != 1 and x['perc_loc']!=1)
		and (x['perc_soc'] <= x['perc_fam'] <= x['perc_tou'] <= x['perc_loc']==0):
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1:
					x['free_game'] = 'TouchMe'
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_b)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,6)
				if ran2==1 and ran2==6:
					chosen_game = random.choice(listone_familiarization_b)
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_b)
				elif ran2==7:
					chosen_game = random.choice(listone_locomotion_b)
				x['free_game'] = chosen_game				
		if (x['perc_fam'] != 1 and ['perc_tou'] != 1 and x['perc_soc'] != 1 and x['perc_loc']!=1)
		and (x['perc_soc'] <= x['perc_fam'] <= x['perc_loc'] <= x['perc_tou']==0):
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1 and ran2==2:
					chosen_game = random.choice(listone_socialization_b)
				elif ran2==3:
					chosen_game = random.choice(listone_familiarization_b)
				x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,4)
				if ran2==1:
					x['free_game'] = 'TouchMe'
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_b)
					x['free_game'] = chosen_game
		if (x['perc_fam'] != 1 and ['perc_tou'] != 1 and x['perc_soc'] != 1 and x['perc_loc']!=1 
		and (x['perc_loc'] <= x['perc_fam'] <= x['perc_soc'] <= x['perc_tou']==0):
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1 and ran2==2:
					chosen_game = random.choice(listone_locomotion_b)
				elif ran2==3:
					chosen_game = random.choice(listone_familiarization_b)
				x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,3)
				if ran2==1:
					chosen_game = random.choice(listone_locomotion_b)
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_b)
				x['free_game'] = chosen_game
		
		#ran3 = random.randint(1,3)
		#if ran3==1:
		###############################################################################
		if x['Lowest_grade']=='touch' and (x['S1']!='anger' or x['I1']!='tolerance'):
			x['free_game'] = 'TouchMe'			
		elif x['Lowest_grade']=='familiarization' and (x['S1']!='anger' or x['I1']!='tolerance'):
			chosen_game = random.choice(listone_familiarization_b)
			x['free_game'] = chosen_game
		elif x['Lowest_grade']=='socialization' and (x['S1']!='anger' or x['I1']!='tolerance'):
			chosen_game = random.choice(listone_socialization_b)
			x['free_game'] = chosen_game
		elif x['Lowest_grade']=='locomotion' and (x['S1']!='anger' or x['I1']!='tolerance'):
			chosen_game = random.choice(listone_locomotion_b)
			x['free_game'] = chosen_game


		ran11 = random.randint(1,6)
		if ran11==1:
			if x['S1'] in ['shyness','anger']:
				chosen_game = random.choice(listone_socialization_b)
				x['free_game'] = chosen_game
		if ran11==2:
			if x['S1'] in ['shyness','anger','meltdown']:
				chosen_game = random.choice(listone_socialization_b)
				x['free_game'] = chosen_game
			if x['S1'] in ['fearful', 'slowness','distraction', 'communication']:
				chosen_game = random.choice(listone_familiarization_b)
				x['free_game'] = chosen_game
			if x['S1'] in ['hyperactivity'] or x['S1'] in ['hyperactivity']:
				chosen_game = random.choice(listone_locomotion_b)
				x['free_game'] = chosen_game
		if ran11==3:
			if x['S1'] in ['communication','distraction','respect rules'] or x['S1'] in ['communication','distraction','respect rules']:
				x['free_game'] = 'TouchMe'
			if x['S1'] in ['slowness'] or x['S1'] in ['slowness']:
				chosen_game = random.choice(listone_familiarization_b)
				x['free_game'] = chosen_game
			if x['S1'] in ['understanding'] or x['S1'] in ['understanding']:
				chosen_game = random.choice(listone_socialization_b)
				x['free_game'] = chosen_game
			if x['S1'] in ['hyperactivity','endurance'] or x['S1'] in ['hyperactivity','endurance']:
				chosen_game = random.choice(listone_locomotion_b)
				x['free_game'] = chosen_game

		ran12 = random.randint(1,8)
		if ran12==1:
			if x['I1'] in ['emotional development','adaptability','language deficits']:
				chosen_game = random.choice(listone_socialization_b)
				x['free_game'] = chosen_game
			if x['I1'] in ['language deficits']:
				chosen_game = random.choice(listone_familiarization_b)
				x['free_game'] = chosen_game			
		if ran12==2:
			if x['I1'] in ['learning','self-management']:
				chosen_game = random.choice(listone_socialization_b)
				x['free_game'] = chosen_game
			if x['I1'] in ['language deficits', 'play and leisure skills']:
				chosen_game = random.choice(listone_familiarization_b)
				x['free_game'] = chosen_game
			if x['I1'] in ['']:
				chosen_game = random.choice(listone_locomotion_b)
				x['free_game'] = chosen_game
		if ran12==4:
			if x['I1'] in ['self-management','social relation','play and leisure skills']:
				x['free_game'] = 'TouchMe'
			if x['I1'] in ['']:
				chosen_game = random.choice(listone_familiarization_b)
				x['free_game'] = chosen_game
			if x['I1'] in ['self-management','social relation']:
				chosen_game = random.choice(listone_socialization_b)
				x['free_game'] = chosen_game
			if x['I1'] in ['tolerance','play and leisure skills']:
				chosen_game = random.choice(listone_locomotion_b)
				x['free_game'] = chosen_game


		if x['I1']=='motor impairments' and (x['free_game'] in ('AutonomousMe', 'FollowingYou', 'ChasingFollowingYou', 'DriveMe', 'DanceWithMe')):
			random_free_last = random.choice(listone_papabili_sicurezza_b)
			x['free_game'] = random_free_last

		if x['I1']=='motor impairments' and (x['free_game'] in ('AutonomousMe', 'FollowingYou', 'ChasingFollowingYou', 'DriveMe', 'DanceWithMe')):
			random_free_last = random.choice(listone_papabili_sicurezza_b)
			x['free_game'] = random_free_last


		if x['free_game'] == 'DriveMe':
			chosen_game = random.choice(listone_locomotion_b)
			x['free_game'] = chosen_game

	#@########@#@#@###############@#@@@#############@########@#@#@###############@#@@@############
	elif x['Level']=='elementary':
		if x['perc_fam']==1 and x['perc_tou']==0 and x['perc_soc']==0 and x['perc_loc']==0:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_locomotion_e)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'

		if x['perc_fam']==0 and x['perc_tou']==0 and x['perc_soc']==0 and x['perc_loc']==1:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_familiarization_e)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'

		if x['perc_fam']==0 and x['perc_tou']==0 and x['perc_soc']==1 and x['perc_loc']==0:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_locomotion_e)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_familiarization_e)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'		

		if x['perc_fam']==0 and x['perc_tou']==1 and x['perc_soc']==0 and x['perc_loc']==0:
			ran = random.randint(1,3)
			if ran==1:
				chosen_game = random.choice(listone_locomotion_e)
			elif ran==2:
				chosen_game = random.choice(listone_familiarization_e)
			elif ran==3:
				chosen_game = random.choice(listone_socialization_e)
			x['free_game'] = chosen_game
		###########################################################################################################		
		if x['perc_fam'] <= x['perc_tou'] <= x['perc_soc'] <= x['perc_loc']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,5)
				if ran2==1 and ran2==2:
					x['free_game'] = 'TouchMe'
				elif ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_e)
					x['free_game'] = chosen_game
				elif ran2==5:
					chosen_game = random.choice(listone_locomotion_e)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,6)
				if ran2==1:
					chosen_game = random.choice(listone_familiarization_e)
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_e)
				elif ran2==6 and ran2==7:
					chosen_game = random.choice(listone_locomotion_e)
				x['free_game'] = chosen_game
		if x['perc_tou'] <= x['perc_fam'] <= x['perc_soc'] <= x['perc_loc']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,5)
				if ran2==1 and ran2==2:
					x['free_game'] = 'TouchMe'
				elif ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_e)
					x['free_game'] = chosen_game
				elif ran2==5:
					chosen_game = random.choice(listone_familiarization_e)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,3)
				if ran2==1:
					chosen_game = random.choice(listone_locomotion_e)
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game

		if x['perc_soc'] <= x['perc_fam'] <= x['perc_tou'] <= x['perc_loc']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1:
					x['free_game'] = 'TouchMe'
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_e)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,6)
				if ran2==1 and ran2==6:
					chosen_game = random.choice(listone_familiarization_e)
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_e)
				elif ran2==7:
					chosen_game = random.choice(listone_locomotion_e)
				x['free_game'] = chosen_game				
		if x['perc_soc'] <= x['perc_fam'] <= x['perc_loc'] <= x['perc_tou']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1 and ran2==2:
					chosen_game = random.choice(listone_socialization_e)
				elif ran2==3:
					chosen_game = random.choice(listone_familiarization_e)
				x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,4)
				if ran2==1:
					x['free_game'] = 'TouchMe'
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_e)
					x['free_game'] = chosen_game
		if x['perc_loc'] <= x['perc_fam'] <= x['perc_soc'] <= x['perc_tou']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1 and ran2==2:
					chosen_game = random.choice(listone_locomotion_e)
				elif ran2==3:
					chosen_game = random.choice(listone_familiarization_e)
				x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,3)
				if ran2==1:
					chosen_game = random.choice(listone_locomotion_e)
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game

		ran3 = random.randint(1,3)
		if ran3==1:
			if x['Lowest_grade']=='touch':
				x['free_game'] = 'TouchMe'			
			elif x['Lowest_grade']=='familiarization':
				chosen_game = random.choice(listone_familiarization_e)
				x['free_game'] = chosen_game
			elif x['Lowest_grade']=='socialization':
				chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game
			elif x['Lowest_grade']=='locomotion':
				chosen_game = random.choice(listone_locomotion_e)
				x['free_game'] = chosen_game


		ran11 = random.randint(1,6)
		if ran11==1:
			if x['S1'] in ['shyness','anger']:
				chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game
		if ran11==2:
			if x['S1'] in ['shyness','anger','meltdown']:
				chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game
			if x['S1'] in ['fearful', 'slowness','distraction', 'communication']:
				chosen_game = random.choice(listone_familiarization_e)
				x['free_game'] = chosen_game
			if x['S1'] in ['hyperactivity'] or x['S1'] in ['hyperactivity']:
				chosen_game = random.choice(listone_locomotion_e)
				x['free_game'] = chosen_game
		if ran11==3:
			if x['S1'] in ['communication','distraction','respect rules'] or x['S1'] in ['communication','distraction','respect rules']:
				x['free_game'] = 'TouchMe'
			if x['S1'] in ['slowness'] or x['S1'] in ['slowness']:
				chosen_game = random.choice(listone_familiarization_e)
				x['free_game'] = chosen_game
			if x['S1'] in ['understanding'] or x['S1'] in ['understanding']:
				chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game
			if x['S1'] in ['hyperactivity','endurance'] or x['S1'] in ['hyperactivity','endurance']:
				chosen_game = random.choice(listone_locomotion_e)
				x['free_game'] = chosen_game

		ran12 = random.randint(1,8)
		if ran12==1:
			if x['I1'] in ['emotional development','adaptability','language deficits']:
				chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game
			if x['I1'] in ['language deficits']:
				chosen_game = random.choice(listone_familiarization_e)
				x['free_game'] = chosen_game			
		if ran12==2:
			if x['I1'] in ['learning','self-management']:
				chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game
			if x['I1'] in ['language deficits', '','', '']:
				chosen_game = random.choice(listone_familiarization_e)
				x['free_game'] = chosen_game
			if x['I1'] in ['']:
				chosen_game = random.choice(listone_locomotion_e)
				x['free_game'] = chosen_game
		if ran12==4:
			if x['I1'] in ['self-management','social relation','play and leisure skills']:
				x['free_game'] = 'TouchMe'
			if x['I1'] in ['']:
				chosen_game = random.choice(listone_familiarization_e)
				x['free_game'] = chosen_game
			if x['I1'] in ['self-management','social relation']:
				chosen_game = random.choice(listone_socialization_e)
				x['free_game'] = chosen_game
			if x['I1'] in ['tolerance','play and leisure skills']:
				chosen_game = random.choice(listone_locomotion_e)
				x['free_game'] = chosen_game


		if x['I1']=='motor impairments' and (x['free_game'] in ('AutonomousMe', 'FollowingYou', 'ChasingFollowingYou', 'DriveMe', 'DanceWithMe')):
			random_free_last = random.choice(listone_papabili_sicurezza_e)
			x['free_game'] = random_free_last

		if x['I1']=='motor impairments' and (x['free_game'] in ('AutonomousMe', 'FollowingYou', 'ChasingFollowingYou', 'DriveMe', 'DanceWithMe')):
			random_free_last = random.choice(listone_papabili_sicurezza_e)
			x['free_game'] = random_free_last


		if x['free_game'] == 'DriveMe':
			chosen_game = random.choice(listone_locomotion_e)
			x['free_game'] = chosen_game

	#@########@#@#@###############@#@@@#############@########@#@#@###############@#@@@############
	elif x['Level']=='elementary':
		if x['perc_fam']==1 and x['perc_tou']==0 and x['perc_soc']==0 and x['perc_loc']==0:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_locomotion_i)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'


		if x['perc_fam']==0 and x['perc_tou']==0 and x['perc_soc']==0 and x['perc_loc']==1:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_familiarization_i)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'

		if x['perc_fam']==0 and x['perc_tou']==0 and x['perc_soc']==1 and x['perc_loc']==0:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_locomotion_i)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_familiarization_i)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'		

		if x['perc_fam']==0 and x['perc_tou']==1 and x['perc_soc']==0 and x['perc_loc']==0:
			ran = random.randint(1,3)
			if ran==1:
				chosen_game = random.choice(listone_locomotion_i)
			elif ran==2:
				chosen_game = random.choice(listone_familiarization_i)
			elif ran==3:
				chosen_game = random.choice(listone_socialization_i)
			x['free_game'] = chosen_game
		###########################################################################################################		
		if x['perc_fam'] <= x['perc_tou'] <= x['perc_soc'] <= x['perc_loc']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,5)
				if ran2==1 and ran2==2:
					x['free_game'] = 'TouchMe'
				elif ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_i)
					x['free_game'] = chosen_game
				elif ran2==5:
					chosen_game = random.choice(listone_locomotion_i)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,6)
				if ran2==1:
					chosen_game = random.choice(listone_familiarization_i)
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_i)
				elif ran2==6 and ran2==7:
					chosen_game = random.choice(listone_locomotion_i)
				x['free_game'] = chosen_game
		if x['perc_tou'] <= x['perc_fam'] <= x['perc_soc'] <= x['perc_loc']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,5)
				if ran2==1 and ran2==2:
					x['free_game'] = 'TouchMe'
				elif ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_i)
					x['free_game'] = chosen_game
				elif ran2==5:
					chosen_game = random.choice(listone_familiarization_i)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,3)
				if ran2==1:
					chosen_game = random.choice(listone_locomotion_i)
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game

		if x['perc_soc'] <= x['perc_fam'] <= x['perc_tou'] <= x['perc_loc']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1:
					x['free_game'] = 'TouchMe'
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_i)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,6)
				if ran2==1 and ran2==6:
					chosen_game = random.choice(listone_familiarization_i)
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_i)
				elif ran2==7:
					chosen_game = random.choice(listone_locomotion_i)
				x['free_game'] = chosen_game				
		if x['perc_soc'] <= x['perc_fam'] <= x['perc_loc'] <= x['perc_tou']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1 and ran2==2:
					chosen_game = random.choice(listone_socialization_i)
				elif ran2==3:
					chosen_game = random.choice(listone_familiarization_i)
				x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,4)
				if ran2==1:
					x['free_game'] = 'TouchMe'
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_i)
					x['free_game'] = chosen_game
		if x['perc_loc'] <= x['perc_fam'] <= x['perc_soc'] <= x['perc_tou']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1 and ran2==2:
					chosen_game = random.choice(listone_locomotion_i)
				elif ran2==3:
					chosen_game = random.choice(listone_familiarization_i)
				x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,3)
				if ran2==1:
					chosen_game = random.choice(listone_locomotion_i)
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game

		ran3 = random.randint(1,3)
		if ran3==1:
			if x['Lowest_grade']=='touch':
				x['free_game'] = 'TouchMe'			
			elif x['Lowest_grade']=='familiarization':
				chosen_game = random.choice(listone_familiarization_i)
				x['free_game'] = chosen_game
			elif x['Lowest_grade']=='socialization':
				chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game
			elif x['Lowest_grade']=='locomotion':
				chosen_game = random.choice(listone_locomotion_i)
				x['free_game'] = chosen_game


		ran11 = random.randint(1,6)
		if ran11==1:
			if x['S1'] in ['shyness','anger']:
				chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game
		if ran11==2:
			if x['S1'] in ['shyness','anger','meltdown']:
				chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game
			if x['S1'] in ['fearful', 'slowness','distraction', 'communication']:
				chosen_game = random.choice(listone_familiarization_i)
				x['free_game'] = chosen_game
			if x['S1'] in ['hyperactivity'] or x['S1'] in ['hyperactivity']:
				chosen_game = random.choice(listone_locomotion_i)
				x['free_game'] = chosen_game
		if ran11==3:
			if x['S1'] in ['communication','distraction','respect rules'] or x['S1'] in ['communication','distraction','respect rules']:
				x['free_game'] = 'TouchMe'
			if x['S1'] in ['slowness'] or x['S1'] in ['slowness']:
				chosen_game = random.choice(listone_familiarization_i)
				x['free_game'] = chosen_game
			if x['S1'] in ['understanding'] or x['S1'] in ['understanding']:
				chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game
			if x['S1'] in ['hyperactivity','endurance'] or x['S1'] in ['hyperactivity','endurance']:
				chosen_game = random.choice(listone_locomotion_i)
				x['free_game'] = chosen_game

		ran12 = random.randint(1,8)
		if ran12==1:
			if x['I1'] in ['emotional development','adaptability','language deficits']:
				chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game
			if x['I1'] in ['language deficits']:
				chosen_game = random.choice(listone_familiarization_i)
				x['free_game'] = chosen_game			
		if ran12==2:
			if x['I1'] in ['learning','self-management']:
				chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game
			if x['I1'] in ['language deficits', '','', '']:
				chosen_game = random.choice(listone_familiarization_i)
				x['free_game'] = chosen_game
			if x['I1'] in ['']:
				chosen_game = random.choice(listone_locomotion_i)
				x['free_game'] = chosen_game
		if ran12==4:
			if x['I1'] in ['self-management','social relation','play and leisure skills']:
				x['free_game'] = 'TouchMe'
			if x['I1'] in ['']:
				chosen_game = random.choice(listone_familiarization_i)
				x['free_game'] = chosen_game
			if x['I1'] in ['self-management','social relation']:
				chosen_game = random.choice(listone_socialization_i)
				x['free_game'] = chosen_game
			if x['I1'] in ['tolerance','play and leisure skills']:
				chosen_game = random.choice(listone_locomotion_i)
				x['free_game'] = chosen_game


		if x['I1']=='motor impairments' and (x['free_game'] in ('AutonomousMe', 'FollowingYou', 'ChasingFollowingYou', 'DriveMe', 'DanceWithMe')):
			random_free_last = random.choice(listone_papabili_sicurezza_i)
			x['free_game'] = random_free_last

		if x['I1']=='motor impairments' and (x['free_game'] in ('AutonomousMe', 'FollowingYou', 'ChasingFollowingYou', 'DriveMe', 'DanceWithMe')):
			random_free_last = random.choice(listone_papabili_sicurezza_i)
			x['free_game'] = random_free_last


		if x['free_game'] == 'DriveMe':
			chosen_game = random.choice(listone_locomotion_i)
			x['free_game'] = chosen_game

	#@########@#@#@###############@#@@@#############@########@#@#@###############@#@@@############
	elif x['Level']=='advanced':
		if x['perc_fam']==1 and x['perc_tou']==0 and x['perc_soc']==0 and x['perc_loc']==0:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_locomotion_a)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'


		if x['perc_fam']==0 and x['perc_tou']==0 and x['perc_soc']==0 and x['perc_loc']==1:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_familiarization_a)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'

		if x['perc_fam']==0 and x['perc_tou']==0 and x['perc_soc']==1 and x['perc_loc']==0:
			ran = random.randint(1,5)
			if ran==1 and ran==2:
				chosen_game = random.choice(listone_locomotion_a)
				x['free_game'] = chosen_game
			elif ran==3 and ran==4:
				chosen_game = random.choice(listone_familiarization_a)
				x['free_game'] = chosen_game
			elif ran==5:
				x['free_game'] = 'TouchMe'		

		if x['perc_fam']==0 and x['perc_tou']==1 and x['perc_soc']==0 and x['perc_loc']==0:
			ran = random.randint(1,3)
			if ran==1:
				chosen_game = random.choice(listone_locomotion_a)
			elif ran==2:
				chosen_game = random.choice(listone_familiarization_a)
			elif ran==3:
				chosen_game = random.choice(listone_socialization_a)
			x['free_game'] = chosen_game
		###########################################################################################################		
		if x['perc_fam'] <= x['perc_tou'] <= x['perc_soc'] <= x['perc_loc']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,5)
				if ran2==1 and ran2==2:
					x['free_game'] = 'TouchMe'
				elif ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_a)
					x['free_game'] = chosen_game
				elif ran2==5:
					chosen_game = random.choice(listone_locomotion_a)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,6)
				if ran2==1:
					chosen_game = random.choice(listone_familiarization_a)
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_a)
				elif ran2==6 and ran2==7:
					chosen_game = random.choice(listone_locomotion_a)
				x['free_game'] = chosen_game
		if x['perc_tou'] <= x['perc_fam'] <= x['perc_soc'] <= x['perc_loc']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,5)
				if ran2==1 and ran2==2:
					x['free_game'] = 'TouchMe'
				elif ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_a)
					x['free_game'] = chosen_game
				elif ran2==5:
					chosen_game = random.choice(listone_familiarization_a)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,3)
				if ran2==1:
					chosen_game = random.choice(listone_locomotion_a)
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game

		if x['perc_soc'] <= x['perc_fam'] <= x['perc_tou'] <= x['perc_loc']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1:
					x['free_game'] = 'TouchMe'
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_a)
					x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,6)
				if ran2==1 and ran2==6:
					chosen_game = random.choice(listone_familiarization_a)
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_a)
				elif ran2==7:
					chosen_game = random.choice(listone_locomotion_a)
				x['free_game'] = chosen_game				
		if x['perc_soc'] <= x['perc_fam'] <= x['perc_loc'] <= x['perc_tou']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1 and ran2==2:
					chosen_game = random.choice(listone_socialization_a)
				elif ran2==3:
					chosen_game = random.choice(listone_familiarization_a)
				x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,4)
				if ran2==1:
					x['free_game'] = 'TouchMe'
				elif ran2==2 and ran2==3 and ran2==4:
					chosen_game = random.choice(listone_socialization_a)
					x['free_game'] = chosen_game
		if x['perc_loc'] <= x['perc_fam'] <= x['perc_soc'] <= x['perc_tou']==0:
			ran = random.randint(1,2)
			if ran==1:
				ran2 = random.randint(1,3)
				if ran2==1 and ran2==2:
					chosen_game = random.choice(listone_locomotion_a)
				elif ran2==3:
					chosen_game = random.choice(listone_familiarization_a)
				x['free_game'] = chosen_game
			if ran==2:
				ran2 = random.randint(1,3)
				if ran2==1:
					chosen_game = random.choice(listone_locomotion_a)
				elif ran2==2 and ran2==3:
					chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game

		ran3 = random.randint(1,3)
		if ran3==1:
			if x['Lowest_grade']=='touch':
				x['free_game'] = 'TouchMe'			
			elif x['Lowest_grade']=='familiarization':
				chosen_game = random.choice(listone_familiarization_a)
				x['free_game'] = chosen_game
			elif x['Lowest_grade']=='socialization':
				chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game
			elif x['Lowest_grade']=='locomotion':
				chosen_game = random.choice(listone_locomotion_a)
				x['free_game'] = chosen_game


		ran11 = random.randint(1,6)
		if ran11==1:
			if x['S1'] in ['shyness','anger']:
				chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game
		if ran11==2:
			if x['S1'] in ['shyness','anger','meltdown']:
				chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game
			if x['S1'] in ['fearful', 'slowness','distraction', 'communication']:
				chosen_game = random.choice(listone_familiarization_a)
				x['free_game'] = chosen_game
			if x['S1'] in ['hyperactivity'] or x['S1'] in ['hyperactivity']:
				chosen_game = random.choice(listone_locomotion_a)
				x['free_game'] = chosen_game
		if ran11==3:
			if x['S1'] in ['communication','distraction','respect rules'] or x['S1'] in ['communication','distraction','respect rules']:
				x['free_game'] = 'TouchMe'
			if x['S1'] in ['slowness'] or x['S1'] in ['slowness']:
				chosen_game = random.choice(listone_familiarization_a)
				x['free_game'] = chosen_game
			if x['S1'] in ['understanding'] or x['S1'] in ['understanding']:
				chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game
			if x['S1'] in ['hyperactivity','endurance'] or x['S1'] in ['hyperactivity','endurance']:
				chosen_game = random.choice(listone_locomotion_a)
				x['free_game'] = chosen_game

		ran12 = random.randint(1,8)
		if ran12==1:
			if x['I1'] in ['emotional development','adaptability','language deficits']:
				chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game
			if x['I1'] in ['language deficits']:
				chosen_game = random.choice(listone_familiarization_a)
				x['free_game'] = chosen_game			
		if ran12==2:
			if x['I1'] in ['learning','self-management']:
				chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game
			if x['I1'] in ['language deficits', '','', '']:
				chosen_game = random.choice(listone_familiarization_a)
				x['free_game'] = chosen_game
			if x['I1'] in ['']:
				chosen_game = random.choice(listone_locomotion_a)
				x['free_game'] = chosen_game
		if ran12==4:
			if x['I1'] in ['self-management','social relation','play and leisure skills']:
				x['free_game'] = 'TouchMe'
			if x['I1'] in ['']:
				chosen_game = random.choice(listone_familiarization_a)
				x['free_game'] = chosen_game
			if x['I1'] in ['self-management','social relation']:
				chosen_game = random.choice(listone_socialization_a)
				x['free_game'] = chosen_game
			if x['I1'] in ['tolerance','play and leisure skills']:
				chosen_game = random.choice(listone_locomotion_a)
				x['free_game'] = chosen_game


		if x['I1']=='motor impairments' and (x['free_game'] in ('AutonomousMe', 'FollowingYou', 'ChasingFollowingYou', 'DriveMe', 'DanceWithMe')):
			random_free_last = random.choice(listone_papabili_sicurezza_a)
			x['free_game'] = random_free_last

		if x['I1']=='motor impairments' and (x['free_game'] in ('AutonomousMe', 'FollowingYou', 'ChasingFollowingYou', 'DriveMe', 'DanceWithMe')):
			random_free_last = random.choice(listone_papabili_sicurezza_a)
			x['free_game'] = random_free_last


		if x['free_game'] == 'DriveMe':
			chosen_game = random.choice(listone_locomotion_a)
			x['free_game'] = chosen_game
		
	
	return x

def funziatest(x,param):
	if x['Level']=='beginner':
		if x['perc_fam']==1 and x['perc_tou']==0 and x['perc_soc']==0 and x['perc_loc']==0:
			x['free_game'] = 'ok'


	#if x['I1']=='motor impairments' and (x['free_game'] in ('AutonomousMe', 'FollowingYou', 'ChasingFollowingYou', 'DriveMe', 'DanceWithMe')):
	#	random_free_last = random.choice(listone_papabili_sicurezza)
	#	x['free_game'] = random_free_last

	return x

def funzia2(x,param):
	if pd.isnull(x['free_game']):
		if x['Level']=='beginner':
			x['free_game'] = random.choice(listone_papabili_beg)
		elif x['Level']=='elementary':
			x['free_game'] = random.choice(listone_papabili_ele)
		elif x['Level']=='intermediate':
			x['free_game'] = random.choice(listone_papabili_int)
		elif x['Level']=='advanced':
			x['free_game'] = random.choice(listone_papabili_adv)
	return x	
########################################################################################################################
if __name__=="__main__":
	games_attr = sys.argv
	params = [games_attr[1]]
	if int(params[0])==0:
		df = pd.read_csv('/home/notto4/Desktop/tabellona.csv')

	elif int(params[0])==17:
		df = pd.read_csv('/home/notto4/Desktop/beginn_vediamo.csv')
	elif int(params[0])==18:
		df = pd.read_csv('/home/notto4/Desktop/element_vediamo.csv')
	elif int(params[0])==19:
		df = pd.read_csv('/home/notto4/Desktop/interm_vediamo.csv')
	elif int(params[0])==20:
		df = pd.read_csv('/home/notto4/Desktop/adavanc_vediamo.csv')

	elif int(params[0])==1:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/begin_free/dataset_free_games_1.csv')
	elif int(params[0])==2:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/begin_free/dataset_free_games_2.csv')
	elif int(params[0])==3:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/begin_free/dataset_free_games_3.csv')
	elif int(params[0])==4:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/begin_free/dataset_free_games_4.csv')
	elif int(params[0])==5:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/elemen_free/dataset_free_games_1.csv')
	elif int(params[0])==6:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/elemen_free/dataset_free_games_2.csv')
	elif int(params[0])==7:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/elemen_free/dataset_free_games_3.csv')
	elif int(params[0])==8:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/elemen_free/dataset_free_games_4.csv')
	elif int(params[0])==9:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/inter_free/dataset_free_games_1.csv')
	elif int(params[0])==10:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/inter_free/dataset_free_games_2.csv')
	elif int(params[0])==11:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/inter_free/dataset_free_games_3.csv')
	elif int(params[0])==12:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/inter_free/dataset_free_games_4.csv')
	elif int(params[0])==13:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/advan_free/dataset_free_games_1.csv')
	elif int(params[0])==14:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/advan_free/dataset_free_games_2.csv')
	elif int(params[0])==15:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/advan_free/dataset_free_games_3.csv')
	elif int(params[0])==16:
		df = pd.read_csv('/home/notto4/Desktop/dataset_FREE/advan_free/dataset_free_games_4.csv')

	df = df.apply(funzia, axis=1, param=params[0])
	df = df.apply(funzia2, axis=1, param=params[0])

	if int(params[0])==0:
		df.to_csv(r'/home/notto4/Desktop/tabellone_pronto.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	
	elif int(params[0])==17:
		df.to_csv(r'/home/notto4/Desktop/beg_pronto.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==18:
		df.to_csv(r'/home/notto4/Desktop/ele_pronto.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)	
	elif int(params[0])==19:
		df.to_csv(r'/home/notto4/Desktop/int_pronto.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==20:
		df.to_csv(r'/home/notto4/Desktop/adv_pronto.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)		

	elif int(params[0])==1:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/begin_free/dataset_free_games_1.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==2:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/begin_free/dataset_free_games_2.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==3:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/begin_free/dataset_free_games_3.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==4:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/begin_free/dataset_free_games_4.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)	
	elif int(params[0])==5:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/elemen_free/dataset_free_games_1.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==6:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/elemen_free/dataset_free_games_2.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==7:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/elemen_free/dataset_free_games_3.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==8:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/elemen_free/dataset_free_games_4.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)		
	elif int(params[0])==9:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/inter_free/dataset_free_games_1.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==10:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/inter_free/dataset_free_games_2.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==11:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/inter_free/dataset_free_games_3.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==12:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/inter_free/dataset_free_games_4.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)		
	elif int(params[0])==13:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/advan_free/dataset_free_games_1.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==14:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/advan_free/dataset_free_games_2.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==15:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/advan_free/dataset_free_games_3.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)
	elif int(params[0])==16:
		df.to_csv(r'/home/notto4/Desktop/dataset_FREE/advan_free/dataset_free_games_4.csv', index=False, quotechar='"',
                      quoting=csv.QUOTE_NONNUMERIC)


	with pd.option_context('display.max_rows', None, 'display.max_columns', None):
		print(df)
		
