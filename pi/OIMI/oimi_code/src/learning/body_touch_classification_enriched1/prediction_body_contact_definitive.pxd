from cpython cimport array

cdef class prediction():
    cdef: 
        name
        perceived_interaction
    
    cdef check_gyrox_stdev(self, int[:] std_g_l, int[:] std_g_r)
    cdef float cmin(self, float[:] a)
    cdef float cmax(self, float[:] a)
    cdef float cavg(self, float[:] a)
    cdef float cstdDev(self, float[:] a)
    cdef dict cplat(self, float[:] b)
    cdef array.array counterpeak(self, dict urca)
    cdef conv(self, dict di)
    cdef float crange(self, float[:] numbers, int amount)
    cdef dict cplat_mpu(self, int[:] b)
    cdef float cavg2(self, int[:] a)
    cdef float cstdDev2(self, int[:] a)
    cdef calculate_single_sample_new(self, new1,new2,new3,new4,new5,giro_l,giro_r)
    cdef predict_current_sample_new(self, ddp2)