"""Info:
    Oimi robot
    Logistic_regression_prediction for a sigle sample, real time test!
    Create the appropriate numpy array with all measures that correspond to the features of the dataset
    Take sav file that contain the result of the training previously done
Notes:
    For detailed info, look at ./code_docs_of_dabatase_manager
Author:
Created by Colombo Giacomo, Politecnico di Milano 2022 """
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language = c++
#cython: language_level = 3
#cython: boundscheck = False
#cython: wraparound = False
#cython: cdivision = True
#cython: nonecheck = False
#cython: binding = True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import numpy as np
cimport numpy as np
import joblib
import pandas as pd
from cpython cimport array
cimport cython
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split as tts
from sklearn.preprocessing import scale
from sklearn.model_selection import RandomizedSearchCV
from sklearn.exceptions import ConvergenceWarning
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import hamming_loss
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, multilabel_confusion_matrix

cdef extern from"Python.h":
    object PyList(float *s, Py_ssize_t leng)
cdef extern from "math.h":
    float sqrt(float m)
    float fabs(float m)

try:
    from sklearn.utils._testing import ignore_warnings
except ImportError:
    from sklearn.utils.testing import ignore_warnings
# ==========================================================================================================================================================
# Class
# ==========================================================================================================================================================
cdef class prediction:
    def __cinit__(self, name):
        self.name = name

    ##########################################
    cdef check_gyrox_stdev(self, int[:] std_g_l, int[:] std_g_r):
        cdef Py_ssize_t j = 6
        cdef Py_ssize_t lenl = len(std_g_l)
        cdef Py_ssize_t lenr = len(std_g_r)
        cdef bint flag_ok = 0
        if ((143<abs(std_g_r[5])<=159) and (38<=std_g_l[5]<=60)):
            flag_ok = 1
        for j in range(j,lenl):
            if ((143<abs(std_g_r[j])<=159) and (38<std_g_l[j]<=60) and flag_ok==1):
                flag_ok = 1
            else:
                flag_ok = 0 
        return flag_ok
    ##########################################
    cdef float cmin(self, float[:] a):
        cdef:
            int c
            float mini
            Py_ssize_t la = len(a)
        mini = a[0]
        with nogil:
            for c in range(1,la):
                if (a[c] < mini): 
                    mini = a[c]
        return mini
    ##########################################
    cdef float cmax(self, float[:] a):
        cdef:
            int c 
            float maxi
            Py_ssize_t la = len(a)
        maxi = a[0]
        with nogil:
            for c in range(1,la):
                if (a[c] > maxi):
                    maxi = a[c]
        return maxi
    ##########################################
    cdef float cavg(self, float[:] a):
        cdef:
            float sum = 0
            int N = len(a)
        with nogil:
            for i in range(N):
                sum += a[i]
        return sum/N
    ##########################################
    cdef float cstdDev(self, float[:] a):
        cdef:
            Py_ssize_t i
            Py_ssize_t n = len(a)
            float m = 0.0
            float v = 0.0
        with nogil:
            for i in range(n):
                m+=a[i]
        m /= n
        with nogil:
            for i in range(n):
                v += (a[i] - m)**2
        return sqrt(v / n)
    ##########################################
    cdef dict cplat(self, float[:] b):
        cdef:
            Py_ssize_t siz = len(b)
            int i = 0
            int j = 0  
            int k = 0  
            int cont = 0  
            bint up = 0
            dict res = dict()
            float prev
            float val
        for j in range(1, siz):
            prev, val = b[j - 1], b[j]
            if up and val < prev:
                res[k]=cont
                k+=1
                up = 0
                cont = 0
            if (val > prev and fabs(val - prev) >= 0.2 and cont==0) or \
                (val > prev and up) or (val == prev and cont != 0):
                cont += 1
                i = j
                up = 1
            if j == siz - 1:
                if up:
                    res[k]=cont
                if not res:
                    res[k]=cont
                return res      
    ##########################################
    cdef array.array counterpeak(self, dict urca):
        cdef array.array num_peaks = array.array('i', [0,0,0,0,0])
        for k in urca.keys():
            if urca[k]==1:
                num_peaks[0]+=1
            elif urca[k]==2:
                num_peaks[1]+=1
            elif urca[k]==3: 
                num_peaks[2]+=1
            elif urca[k]==4:
                num_peaks[3]+=1
            elif urca[k]>=5:
                num_peaks[4]+=1
        return num_peaks
    ##########################################
    cdef conv(self, dict di):
        cdef bint fla= 0
        d = dict({0: 0})
        if di is None:
            fla = 1 
        if fla == 1: 
            return fla, d
        if fla == 0:
            return fla, di  
    ##########################################
    cdef float crange(self, float[:] numbers, int amount):
        cdef int i = 1
        cdef float mini=0.0
        cdef maxi=0.0
        cdef diff=0.0 
        mini = numbers[0]
        maxi = numbers[0]
        for i in range(amount):
            if numbers[i] < mini:
                mini = numbers[i]
            if maxi < numbers[i]:
                maxi = numbers[i]
        diff = maxi - mini
        return diff
    #########################################
    cdef dict cplat_mpu(self, int[:] b):
        cdef Py_ssize_t siz = len(b)
        cdef int i = 0
        cdef int j = 0  
        cdef int k = 0  
        cdef int cont = 0  
        cdef bint up = 0
        cdef dict res = dict()
        cdef float prev
        cdef float val  
        for j in range(1, siz):
            prev, val = b[j - 1], b[j]
            if up and val < prev:
                res[k]=cont
                k+=1
                up = 0
                cont = 0
            if (val > prev and fabs(val - prev) >= 50 and cont==0) or \
                (val > prev and up) or (val == prev and cont != 0):
                cont += 1
                i = j
                up = 1
            if j == siz - 1:
                if up:
                    res[k]=cont 
                if not res:
                    res[k]=cont
        return res
    ##########################################
    cdef float cavg2(self, int[:] a):
        cdef int sum = 0
        cdef int N = len(a)
        with nogil:
            for i in range(N):
                sum += a[i]
        return sum/N
    ##########################################        
    cdef float cstdDev2(self, int[:] a):
        cdef Py_ssize_t i
        cdef Py_ssize_t n = len(a)
        cdef int m = 0
        cdef int v = 0
        with nogil:
            for i in range(n):
                m+=a[i]
        m /= n    
        with nogil:
            for i in range(n):
                v += (a[i] - m)**2
        return sqrt(v / n)
    ##########################################
    def dtw(s, t): #dynamic time warping
        n = len(s)
        m = len(t)
        dtw_matrix = np.zeros((n+1, m+1))
        for i in range(n+1):
            for j in range(m+1):
                dtw_matrix[i, j] = np.inf
        dtw_matrix[0, 0] = 0
        
        for i in range(1, n+1):
            for j in range(1, m+1):
                cost = fabs(s[i-1] - t[j-1])
                # take last min from a square box
                last_min = np.min([dtw_matrix[i-1, j], dtw_matrix[i, j-1], dtw_matrix[i-1, j-1]])
                dtw_matrix[i, j] = cost + last_min
        return dtw_matrix

    def warp(s,t):
        ma = dtw(s,t)
        return ma[len(ma)-1][len(ma)-1]
    ##########################################
    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def ccmax(self, aa):
        res = self.cmax(aa)
        return res
    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def ccmin(self, list_min):
        res = self.cmin(list_min)
        return res
    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def ccavg(self, list_avg):
        res = self.cavg(list_avg)
        return res
    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def ccstdev(self, list_stdev):
        res = self.cstdDev(list_stdev)
        return res
    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def ccplat(self, list_plat):
        res = self.cplat(list_plat)
        return res
    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def ccounter(self, dict_counter):
        res = self.counterpeak(dict_counter)
        return res
    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def cconv(self, dict_conv):
        res1,res2 = self.conv(dict_conv)
        return res1,res2
    @cython.boundscheck(False)
    @cython.wraparound(False)
    def ccavg2(self, list_avg2):
        res = self.cavg2(list_avg2)
        return res
    @cython.boundscheck(False)  # Deactivate bounds checking
    @cython.wraparound(False)   # Deactivate negative indexing.
    def ccstd2(self, list_std2):
        res = self.cstdDev2(list_std2)
        return res

    cdef calculate_single_sample_new(self, new1,new2,new3,new4,new5,giro_l,giro_r):
        print("SONO DENTRO calculate_single_sample_new")
        girox_l = np.asarray(giro_l, dtype=np.int32)
        girox_r = np.asarray(giro_r, dtype=np.int32)
        cdef int[:] std_g_l=girox_l
        cdef int[:] std_g_r=girox_r 
        prot0 = np.asarray(new1, dtype=np.float32)
        prot1 = np.asarray(new2, dtype=np.float32)
        prot2 = np.asarray(new3, dtype=np.float32)
        prot3 = np.asarray(new4, dtype=np.float32)
        prot4 = np.asarray(new5, dtype=np.float32)

        # a bytes-like object is required, not 'list'
        cdef float[:] prot0_view = prot0
        cdef float[:] prot1_view = prot1
        cdef float[:] prot2_view = prot2
        cdef float[:] prot3_view = prot3
        cdef float[:] prot4_view = prot4
        min0,min1,min2,min3,min4 = self.cmin(prot0_view), self.cmin(prot1_view), self.cmin(prot2_view), self.cmin(prot3_view), self.cmin(prot4_view)
        max0,max1,max2,max3,max4 = self.cmax(prot0_view), self.cmax(prot1_view), self.cmax(prot2_view), self.cmax(prot3_view), self.cmax(prot4_view)
        avg0,avg1,avg2,avg3,avg4 = self.cavg(prot0_view), self.cavg(prot1_view), self.cavg(prot2_view), self.cavg(prot3_view), self.cavg(prot4_view) 
        std0,std1,std2,std3,std4 = self.cstdDev(prot0_view), self.cstdDev(prot1_view), self.cstdDev(prot2_view), self.cstdDev(prot3_view), self.cstdDev(prot4_view)
        range0,range1,range2,range3,range4 = self.crange(prot0_view,10),self.crange(prot1_view,10),self.crange(prot2_view,10),self.crange(prot3_view,10),self.crange(prot4_view,10)  
        perc0,perc1,perc2,perc3,perc4 = np.percentile(prot0_view,97),np.percentile(prot1_view,97),np.percentile(prot2_view,97),np.percentile(prot3_view,97),np.percentile(prot4_view,97),


        pla0, pla1, pla2, pla3, pla4 = self.cplat(prot0_view), self.cplat(prot1_view), self.cplat(prot2_view) , self.cplat(prot3_view), self.cplat(prot4_view)
        pla_left = self.cplat_mpu(std_g_l)
        pla_right = self.cplat_mpu(std_g_r)

        resp_left, da_left = self.conv(pla_left)
        resp_right, da_right = self.conv(pla_right)

        num_peaks_l = self.counterpeak(da_left)
        num_peaks_r = self.counterpeak(da_right)

        resp0,da0 = self.conv(pla0)
        resp1,da1 = self.conv(pla1)
        resp2,da2 = self.conv(pla2)
        resp3,da3 = self.conv(pla3)
        resp4,da4 = self.conv(pla4)

        num_peaks0 = self.counterpeak(da0) 
        num_peaks1 = self.counterpeak(da1) 
        num_peaks2 = self.counterpeak(da2)
        num_peaks3 = self.counterpeak(da3)
        num_peaks4 = self.counterpeak(da4)

        '''ddp2 = np.array([[max0,max1,max2,max3,max4,avg0,avg1,avg2,avg3,avg4,std0,std1,std2,std3,std4,
         num_peaks0[0],num_peaks0[1],num_peaks0[2],num_peaks0[3],num_peaks0[4], 
         num_peaks1[0],num_peaks1[1],num_peaks1[2],num_peaks1[3],num_peaks1[4], 
         num_peaks2[0],num_peaks2[1],num_peaks2[2],num_peaks2[3],num_peaks2[4], 
         num_peaks3[0],num_peaks3[1],num_peaks3[2],num_peaks3[3],num_peaks3[4], 
         num_peaks4[0],num_peaks4[1],num_peaks4[2],num_peaks4[3],num_peaks4[4]]])'''

        #then add warp!!! also in making_new
        tw0=warp(sample_quiet0, prot0_view)
        tw1=warp(sample_quiet1, prot1_view)
        tw2=warp(sample_quiet2, prot2_view)
        tw3=warp(sample_quiet3, prot3_view)
        tw4=warp(sample_quiet4, prot4_view)

        std_gir_l = self.cstdDev2(girox_l)
        std_gir_r = self.cstdDev2(girox_r)
        avg_gir_l = self.cavg2(girox_l)
        avg_gir_r = self.cavg2(girox_r)

        np.set_printoptions(formatter={'float_kind':'{:f}'.format})
        ####################################################################################################################################################
        #ddp2 = np.array([[max0,max1,max2,max3,max4,avg0,avg1,avg2,avg3,avg4,std0,std1,std2,std3,std4]])

        '''ddp2 = np.array([[max0,max1,max2,max3,max4,avg0,avg1,avg2,avg3,avg4,
        std0,std1,std2,std3,std4,std_gir_l,std_gir_r,range0,range1,range2,range3,range4,
        tw0,tw1,tw2,tw3,tw4,num_peaks_l[0],num_peaks_r[0],
        perc0,perc1,perc2,perc3,perc4]])'''

        '''
        ddp2 = np.array([[
            max0,max1,max2,max3,max4,
            avg0,avg1,avg2,avg3,avg4,
            std0,std1,std2,std3,std4,
            num_peaks0[0],num_peaks0[1],
            num_peaks1[0],num_peaks1[1],
            num_peaks2[0],num_peaks2[1],
            num_peaks3[0],num_peaks3[1],
            num_peaks4[0],num_peaks4[1],
            range0,range1,range2,range3,range4,
            num_peaks_l[0],num_peaks_r[0],
         
        std_gir_l, std_gir_r
        #tw0,tw1,tw2,tw3,tw4,
        #num_peaks0[0],#num_peaks0[4],
        #num_peaks1[0],#num_peaks1[4],
        #num_peaks2[0],#num_peaks2[4],
        #num_peaks3[0],#num_peaks3[4],
        #num_peaks4[0]#num_peaks4[4],
         ]])

        #ddp2 = np.array([[max0,max1,max2,max3,max4,avg0,avg1,avg2,avg3,avg4,
        #std0,std1,std2,std3,std4,
        #num_peaks0[0],num_peaks0[4],
        #num_peaks1[0],num_peaks1[4],
        #num_peaks2[0],num_peaks2[4],
        #num_peaks3[0],num_peaks3[4],
        #num_peaks4[0],num_peaks4[4],
        #std_gir_l, std_gir_r]])
        '''
        ''' this is standard:
        ddp2 = np.array([[
            min0,min1,min2,min3,min4,
            max0,max1,max2,max3,max4,
            avg0,avg1,avg2,avg3,avg4,
            std0,std1,std2,std3,std4,
            range0,range1,range2,range3,range4,
            num_peaks0[0],num_peaks0[1],
            num_peaks1[0],num_peaks1[1],
            num_peaks2[0],num_peaks2[1],
            num_peaks3[0],num_peaks3[1],
            num_peaks4[0],num_peaks4[1],     
            avg_gir_l, avg_gir_r,
            std_gir_l, std_gir_r
            ]])
        ''' 
        #this is a new tentative 
        ddp2 = np.array([[
            min0,min1,min2,min3,min4,
            max0,max1,max2,max3,max4,
            avg0,avg1,avg2,avg3,avg4,
            std0,std1,std2,std3,std4,
            range0,range1,range2,range3,range4,
            tw0,tw1,tw2,tw3,tw4,
            num_peaks0[0],num_peaks0[1],
            num_peaks1[0],num_peaks1[1],
            num_peaks2[0],num_peaks2[1],
            num_peaks3[0],num_peaks3[1],
            num_peaks4[0],num_peaks4[1],     
            avg_gir_l, avg_gir_r,
            std_gir_l, std_gir_r
            ]])
        print("Im calculating ddp2 ddp2")
        print(ddp2)
        print()
        flag_discrimination_1 = self.check_gyrox_stdev(std_g_l, std_g_r)
        print(flag_discrimination_1)
        return ddp2, flag_discrimination_1

    cdef predict_current_sample_new(self, ddp2):
        print("SONO DENTRO predict_current_sample_new")
        loaded_model = joblib.load("/home/pi/OIMI/oimi_code/src/learning/body_touch_classification/training_body_17genn_2023.sav")
        self.perceived_interaction = loaded_model.predict(ddp2)
        print("self.perceived_interaction!")
        print(self.perceived_interaction)
        return self.perceived_interaction

    def calculate_single_sample(self, new1,new2,new3,new4,new5,giro_l,giro_r):
        res = self.calculate_single_sample_new(new1,new2,new3,new4,new5,giro_l,giro_r)
        return res

    def predict_current_sample(self, ddp2):
        res = self.predict_current_sample_new(ddp2)
        return res