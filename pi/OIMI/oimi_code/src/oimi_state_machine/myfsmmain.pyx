"""
Info:
    State machine implemented in Python using the State, StateMachine and fsm_event modules, to manage the flow of a game. 
    The code defines three classes, each of which implements a State of the game: PlaySessionState, PlayMatchState and PlayGameState.
    The update_state_fsm method of each class is responsible for handling transitions between states based on events received. 
    The state transitions are determined by the target of the received event, as indicated by the Target(evt.target).name string. 
    Each transition also optionally executes a task specified in the received event's user data.
    The enter_state_fsm and exit_state_fsm methods of each class are responsible for executing logic when entering and exiting a state, respectively. 
    PlaySessionState, PlayMatchState, and PlayGameState, that inherit from the State class from the state module. 
    These classes represent the different states in a game-playing scenario.
"""
# ===========================================================================================================================
# Imports
# ===========================================================================================================================
from fsm import StateMachine
from state import State
import fsm_event
from fsm_event import Trigger, Target
import time
from typing import Optional
from game import Game
# ===========================================================================================================================
# Methods
# ===========================================================================================================================
class PlaySessionState(State):
    def __init__(self, state_name: str):
        super().__init__(state_name)

    def update_state_fsm(self, evt):
        print("I'exiting from state {}".format(self.state_name))
        super().update_state_fsm(evt)
        print("In State: {} \u2014 Event<target={}>".format(self.state_name, str(Target(evt.target).name)))

        if str(Target(evt.target).name) == 'BEGIN_SES':
            task = (
            evt.user_data["task_preli"]()
            if "task_preli" in evt.user_data
            else None)      
            return "start"
        if str(Target(evt.target).name) == 'GO_TO_MIDDLE_S':
            task = (
            evt.user_data["task_begin_play"]()
            if "task_begin_play" in evt.user_data
            else None)      
            return "middle"
        if str(Target(evt.target).name) == 'GO_TO_LAST_S':
            task = (
            evt.user_data["task_finals"]()
            if "task_finals" in evt.user_data
            else None)
            return "last"      
        #if task is not None and task == 'ask':
        #    return "middle2"
        
    def enter_state_fsm(self) -> None:
        print(" Entered State is {}".format(self.state_name))
        #if self.state_name == 'start':
        #    x = 5 
        #    print(" res is {}".format(x))
        #if self.state_name == 'middle2':
        #    x = 4        
        #    print(" res is {}".format(x))    
        return None

    def exit_state_fsm(self) -> None:
        print(" Exited State is {}".format(self.state_name))
        return None

class PlayMatchState(State):
    def __init__(self, state_name: str):
        super().__init__(state_name)

    def update_state_fsm(self, evt):
        print("I'm exiting from state {}".format(self.state_name))
        super().update_state_fsm(evt)
        print("In State: {} \u2014 Event<target={}>".format(self.state_name, str(Target(evt.target).name)))
        #task = (
        #    evt.user_data["task"]
        #    if "task" in evt.user_data
        #    else None
        #)
        if str(Target(evt.target).name) == 'BEGIN_MAT':
            task = (
            evt.user_data["task_preli"]()
            if "task_preli" in evt.user_data
            else None
            )            
            return "start_m"

        if str(Target(evt.target).name) == 'GO_TO_MIDDLE_M':
            task = (
            evt.user_data["task_begin_play"]()
            if "task_begin_play" in evt.user_data
            else None
            )            
            return "middle_m"

        if str(Target(evt.target).name) == 'GO_TO_LAST_M':
            #nothing for now....we will se
            return "last_m"      
        #if task is not None and task == 'ask':
        #    return "middle2"
        
    def enter_state_fsm(self)-> None:
        print(" Entered State is {}".format(self.state_name))
        
        if self.state_name == 'start_m':
            print("ecco che comincio match_sono nel primo stato")   	
        #    print(" res is {}".format(x))
        #if self.state_name == 'middle2':
        #    x = 4        
        #    print(" res is {}".format(x))    
        return None

    def exit_state_fsm(self) -> None:
        print(" Exited State is {}".format(self.state_name))
        return None

class PlayGameState(State):
    def __init__(self, state_name: str):
        super().__init__(state_name)

    def update_state_fsm(self, evt):
        print("I'm exiting from state {}".format(self.state_name))
        super().update_state_fsm(evt)
        print("In State: {} \u2014 Event<target={}>".format(self.state_name, str(Target(evt.target).name)))

        if str(Target(evt.target).name) == 'BEGIN_GAM':
            task = (
            evt.user_data["task"]()
            if "task" in evt.user_data
            else None)            
            return "start_g"
        if str(Target(evt.target).name) == 'GO_TO_MIDDLE_G':
            task = (
            evt.user_data["check1"]()
            if "check1" in evt.user_data
            else None)
            return "middle_g"

        if str(Target(evt.target).name) == 'GO_TO_MIDDLE2_G':
            task = (
            evt.user_data["task_save_results"]()
            if "task_save_results" in evt.user_data
            else None)
            return "middle2_g"

        if str(Target(evt.target).name) == 'GO_TO_LAST_G':
            task = (
            evt.user_data["check2"]()
            if "check2" in evt.user_data
            else None)
            return "last_g" 
        #if task is not None and task == 'ask':
        #    return "middle2"
        
    def enter_state_fsm(self)-> None:
        print(" Entered State is {}".format(self.state_name))
        #if self.state_name == 'start':
        #    print(" res is {}".format(x))
        #if self.state_name == 'middle2':
        #    x = 4        
        #    print(" res is {}".format(x))    
        #return None

    def exit_state_fsm(self) -> None:
        print(" Exited State is {}".format(self.state_name))
        return None        
#===============================================================#===============================================================
class TestFSM(StateMachine):
    def __init__(self):
        self.add_state(PlaySessionState, "start")
        self.add_state(PlaySessionState, "middle")
        self.add_state(PlaySessionState, "last")
        self.entry_state = "start"

class TestFSMAT(StateMachine):
    def __init__(self):
        self.add_state(PlayMatchState, "start_m")
        self.add_state(PlayMatchState, "middle_m")
        self.add_state(PlayMatchState, "last_m")
        self.entry_state = "start_m"
class TestFSMGA(StateMachine):
    def __init__(self):
        self.add_state(PlayGameState, "start_g")
        self.add_state(PlayGameState, "middle_g")
        self.add_state(PlayGameState, "last_g")
        self.entry_state = "start_g"
#===============================================================#===============================================================
class OimiFSMSession(StateMachine):
    def __init__(self):
        self.add_state(PlaySessionState, "start")
        self.add_state(PlaySessionState, "middle")
        self.add_state(PlaySessionState, "last")
        self.entry_state = "start"
class OimiFSMMatch(StateMachine):
    def __init__(self):
        self.add_state(PlayMatchState, "start_m")
        self.add_state(PlayMatchState, "middle_m")
        self.add_state(PlayMatchState, "last_m")
        self.entry_state = "start_m"
class OimiFSMGame(StateMachine):
    def __init__(self):
        self.add_state(PlayGameState, "start_g")
        self.add_state(PlayGameState, "middle_g")
        self.add_state(PlayGameState, "middle2_g")
        self.add_state(PlayGameState, "last_g")
        self.entry_state = "start_g"