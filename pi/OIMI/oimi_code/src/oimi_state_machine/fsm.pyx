# ===========================================================================================================================
# Cython directives
# ===========================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
# ===========================================================================================================================
# Imports
# ===========================================================================================================================
from state cimport State
from fsm_event cimport GameEvent
# ===========================================================================================================================
# Class
# ===========================================================================================================================
cdef class StateMachine:
    cdef public:
        dict all_states
        str ingress_state
        str actual_state
        bint is_the_entrance
    
    def __cinit__(self):
        self.all_states = {}
        self.ingress_state = ""
        self.actual_state = ""
        self.is_the_entrance = False

    cpdef to_entry(self):
        self.current_state = self.entry_state
        self.is_the_entrance = True

    cdef State get_state(self, str state_name):
        if self.has_state(state_name):
            return self.all_states[state_name]
        return None

    def __getitem__(self, str state_name not None):
        return self.get_state(state_name)

    cpdef State add_state(self, state_type, str state_name):
        cdef State state
        if state_name not in self.all_states:
            state = state_type(state_name)
            self.all_states[state_name] = state
            return state
        return None

    @property
    def current_state(self):
        return self.actual_state
    @current_state.setter
    def current_state(self, new_current: str) -> None:
        if self.has_state(new_current):
            self.actual_state = new_current
    @property
    def entry_state(self) -> str:
        return self.ingress_state
    @entry_state.setter
    def entry_state(self, new_entry: str) -> None:
        if self.has_state(new_entry):
            self.ingress_state = new_entry
            
    cpdef bint has_state(self, str state_name):
        return state_name in self.all_states

    @property
    def is_started(self):
        return bool(self.actual_state)

    cdef State ongoing_state(self):
        if not self.is_started:
            self.is_the_entrance = True
            if not self.has_state(self.ingress_state):
                raise RuntimeError("Error! Couldn't find a state for given entry state name!")
            else:
                self.current_state = self.ingress_state
        if not self.has_state(self.actual_state):
            raise RuntimeError("Error! Couldn't find a state for given entry state name!")
        return self.all_states[self.current_state]

    cpdef update_state_fsm(self, GameEvent evt):
        cdef  next_fsm_state
        cdef str next_state_name
        cdef State fsm_state = self.ongoing_state()

        if evt is None:
            fsm_state.exit_state_fsm()
            return

        if fsm_state is None:
            raise TypeError("Something wrong happened")

        if self.is_the_entrance:
            self.is_the_entrance = False
            fsm_state.enter_state_fsm()
        next_state_name = fsm_state.update_state_fsm(evt)
        if next_state_name is not None and self.has_state(next_state_name):
            fsm_state.exit_state_fsm()
            next_fsm_state = self.get_state(next_state_name)
            if isinstance(next_fsm_state, State):
                next_fsm_state.enter_state_fsm()
                self.current_state = next_state_name