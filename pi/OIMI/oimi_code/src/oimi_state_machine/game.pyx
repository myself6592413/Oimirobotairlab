# ===========================================================================================================================
# Cython directives
# ===========================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
# ===========================================================================================================================
# Imports
# ===========================================================================================================================
import examples as ex 
import myfsmmain as my
import fsm_event
from fsm_event import Trigger, Target
import time
from typing import Optional
# ===========================================================================================================================
# Class
# ===========================================================================================================================
cdef class Game:
	cdef public:
		str id_game
		int kind_of_game 
		int audio_intro_kind
		int turn
		user_dict
		user_dict_m
		user_dict_g
	def __cinit__(self, str id_game, int kind_of_game):
		self.id_game = id_game
		self.kind_of_game = kind_of_game
		self.turn = 1
		self.user_dict={"check":self.check_res, "task": self.play_2}
		self.user_dict_m={"check":self.check_res, "task": self.play_3}
		self.user_dict_g={"check":self.check_res, "task": self.play, "task_1": self.play, "task_2": self.play_long}

	def preli(self):
		print("--> preliminals")

	def play(self):
		if(self.turn==1):
			print("turn = 1")
			self.turn+=1 
		if(self.turn==2):
			print("turn = 2")
			self.turn+=1 
		if(self.turn==3):
			print("turn = 3")
			self.turn+=1 
		if(self.turn==4):
			print("turn = 4")
			self.turn=1

	def play_long(self):
		time.sleep(5)
	
	def play_2(self):
		state0 = my.PlayMatchState("play_match_inside")
		evt1 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.START_M, user_data=self.user_dict_m,)
		mainmafsm = my.TestFSMAT()
		mainmafsm.update_state_fsm(evt1)

	def play_3(self):
		state0 = my.PlayGameState("play_game_inside")
		evt1 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.START_G, user_data=self.user_dict_g,)
		evt2 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.MIDDLE_G, user_data=self.user_dict_g,)
		evt3 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.LAST_G, user_data=self.user_dict_g,)
		maingafsm = my.TestFSMGA()
		maingafsm.update_state_fsm(evt1)
		maingafsm.update_state_fsm(evt2)
		maingafsm.update_state_fsm(evt3)

	def check_res(self):	
		ex.check(1)

	def runme(self):
		state0 = my.PlaySessionState("play_session")
		evt1 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.MIDDLE, user_data=self.user_dict,)
		evt2 = fsm_event.GameEvent(delta_seconds=1 / 60.0, target=fsm_event.Target.LAST, user_data=self.user_dict,)
		mainfsm = my.TestFSM()
		mainfsm.update_state_fsm(evt1)
		mainfsm.update_state_fsm(evt2)