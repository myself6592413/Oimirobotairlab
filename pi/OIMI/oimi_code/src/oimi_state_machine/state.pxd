from fsm_event cimport GameEvent

cdef class State:
    cdef readonly str state_name

    cpdef update_state_fsm(self, GameEvent evt)
    cpdef enter_state_fsm(self)
    cpdef exit_state_fsm(self)
