""" Oimi setup file that compiles all cython source files
    
    Usage:
        python setup_state_machine.py build_ext --inplace 
"""
# ============================================================================================
#  Imports:
# ============================================================================================
try:
    from setuptools import setup
    from setuptools import Extension
except ImportError:
    from distutils.core import setup

import sys
import setuptools
from distutils.extension import Extension
from Cython.Build import build_ext
from Cython.Build import cythonize
import numpy
import os.path
import io
import shutil
# ===========================================================================================
#  Elements:
# ===========================================================================================
ext_modules = [
    Extension("fsm", ["fsm.pyx"],
        extra_compile_args=['-O3','-w','-fopenmp'],
        extra_link_args=['-fopenmp','-ffast-math','-march=native'],
        include_dirs=[numpy.get_include()],
        language='c++'),
    Extension("state", ["state.pyx"],
        extra_compile_args=['-O3','-w','-fopenmp'],
        extra_link_args=['-fopenmp','-ffast-math','-march=native'],
        include_dirs=[numpy.get_include()],
        language='c++'),
    Extension("fsm_event", ["fsm_event.pyx"],
        extra_compile_args=['-O3','-w','-fopenmp'],
        extra_link_args=['-fopenmp','-ffast-math','-march=native'],
        include_dirs=[numpy.get_include()],
        language='c++'),
    Extension("game", ["game.pyx"],
        extra_compile_args=['-O3','-w','-fopenmp'],
        extra_link_args=['-fopenmp','-ffast-math','-march=native'],
        include_dirs=[numpy.get_include()],
        language='c++'),
    Extension("examples", ["examples.pyx"],
        extra_compile_args=['-O3','-w','-fopenmp'],
        extra_link_args=['-fopenmp','-ffast-math','-march=native'],
        include_dirs=[numpy.get_include()],
        language='c++'),
    Extension("myfsmmain", ["myfsmmain.pyx"],
        extra_compile_args=['-O3','-w','-fopenmp'],
        extra_link_args=['-fopenmp','-ffast-math','-march=native'],
        include_dirs=[numpy.get_include()],
        language='c++'),
    ]
# =======================================================================
#  Class:
# =======================================================================
class BuildExt(build_ext):
    """ Extend Ctyhon build_ext for removing annoying warnings"""
    def build_extensions(self):
        if '-Wstrict-prototypes' in self.compiler.compiler_so:
            self.compiler.compiler_so.remove('-Wstrict-prototypes')
        super().build_extensions()
# =====================================================================================================================================
#  Main:
# =====================================================================================================================================
if sys.version_info < (3, 5):
    sys.exit("Have you activated the corrent python environment? oimi doesnt support a py version < 3.7")

for e in ext_modules:
    e.cython_directives = {'embedsignature': True,'boundscheck': False,'wraparound': False,'linetrace': True, 'language_level': "3"}

setup(
    name='oimi-robot source compilation',
    version='0.1.0',
    author='Colombo Giacomo',
    ext_modules=ext_modules,
    cmdclass = {'build_ext': BuildExt},
    )