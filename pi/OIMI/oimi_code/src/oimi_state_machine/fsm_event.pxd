from libcpp.map cimport map

cpdef enum Trigger:
    CUSTOM = 1
    UPDATE = 2

cpdef enum Target:
    STANDARD = 1
    BEGIN_SES = 2
    GO_TO_MIDDLE_S = 3
    GO_TO_LAST_S = 4
    BEGIN_MAT = 5
    GO_TO_MIDDLE_M = 6
    GO_TO_LAST_M = 7
    BEGIN_GAM = 8
    GO_TO_MIDDLE_G = 9
    GO_TO_MIDDLE2_G = 10
    GO_TO_LAST_G = 11

cdef class EventBase:
    cdef readonly Trigger trigger
    cdef readonly Target target
    cdef readonly dict user_data

cdef class GameEvent(EventBase):
    cdef readonly float delta_seconds
