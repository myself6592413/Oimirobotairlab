# ===========================================================================================================================
# Cython directives
# ===========================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
# ===========================================================================================================================
# Imports
# ===========================================================================================================================
from fsm_event import GameEvent
from fsm_event import Trigger, Target
# ===========================================================================================================================
# Class
# ===========================================================================================================================
cdef class State:
    def __init__(self, str state_name):
        self.state_name = state_name

    cpdef update_state_fsm(self, GameEvent evt):
        pass
    cpdef enter_state_fsm(self):
        pass
    cpdef exit_state_fsm(self):
        pass

