# ===========================================================================================================================
# Cython directives
# ===========================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
# ===========================================================================================================================
# Imports
# ===========================================================================================================================
from fsm_event cimport Trigger, Target
# ===========================================================================================================================
# Class
# ===========================================================================================================================
cdef class EventBase:
    def __cinit__(self, Trigger trigger=Trigger.CUSTOM, Target target=Target.STANDARD, dict user_data=None, **kwargs):
        self.target = target
        self.trigger = trigger
        self.user_data = user_data

cdef class GameEvent(EventBase):
    def __cinit__(self, float delta_seconds=-1.0, Target target=Target.STANDARD, dict user_data=None, **kwargs):
        self.target = target
        self.trigger = Trigger.UPDATE
        self.user_data = user_data
        self.delta_seconds = delta_seconds if delta_seconds >= 0.0 else 0.0