""" Oimi Patient class for kid that contains info of kids extracted from database
    Created by Colombo Giacomo Airlab Polimi 2022
"""
# ==========================================================================================================================================================
# Cython directives
# ==========================================================================================================================================================
#distutils: language=c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: nonecheck=False
#cython: binding=True
# ==========================================================================================================================================================
# Imports
# ==========================================================================================================================================================
import sys, os
#import sqlite3 as lite useless no??? is inside db
import database_manager as db
# ==========================================================================================================================================================
# Classes
# ==========================================================================================================================================================
cdef class Kid:
    def __cinit__(self, kid_id):
        self.kid_id = kid_id
        self.setup_kid(stopInterrupt, primary_lock, queue_lock)
    
    def __enter__(self):
        print("Initializing the kid...")

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("The stage have been created successfully.")
        print("Im exiting from stage")

    cdef setup_kid(self):
        with db.DatabaseManager() as oimidb:
            self.permission_flag_Pr_st_1 = oimidb.execute_param_query('''SELECT permission_flag_Pr_st_1 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Pr_st_2 = oimidb.execute_param_query('''SELECT permission_flag_Pr_st_2 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Pr_st_3 = oimidb.execute_param_query('''SELECT permission_flag_Pr_st_3 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Pr_st_4 = oimidb.execute_param_query('''SELECT permission_flag_Pr_st_4 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Rw_st_1 = oimidb.execute_param_query('''SELECT permission_flag_Rw_st_1 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Rw_st_2 = oimidb.execute_param_query('''SELECT permission_flag_Rw_st_2 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Rw_st_3 = oimidb.execute_param_query('''SELECT permission_flag_Rw_st_3 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Rw_st_4 = oimidb.execute_param_query('''SELECT permission_flag_Rw_st_4 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Rw_st_5 = oimidb.execute_param_query('''SELECT permission_flag_Rw_st_5 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Mo_st_1 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_1 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Mo_st_2 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_2 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Mo_st_3 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_3 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Mo_st_4 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_4 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Mo_st_5 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_5 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.permission_flag_Mo_st_6 = oimidb.execute_param_query('''SELECT permission_flag_Mo_st_6 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Rep_st_1 = oimidb.execute_param_query('''SELECT do_Rep_st_1 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Rep_st_2 = oimidb.execute_param_query('''SELECT do_Rep_st_2 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Rep_st_3 = oimidb.execute_param_query('''SELECT do_Rep_st_3 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Rep_st_4 = oimidb.execute_param_query('''SELECT do_Rep_st_4 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Rep_st_5 = oimidb.execute_param_query('''SELECT do_Rep_st_5 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Rep_st_6 = oimidb.execute_param_query('''SELECT do_Rep_st_6 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Ren_st_1 = oimidb.execute_param_query('''SELECT do_Ren_st_1 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Ren_st_2 = oimidb.execute_param_query('''SELECT do_Ren_st_2 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Ren_st_3 = oimidb.execute_param_query('''SELECT do_Ren_st_3 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Ren_st_4 = oimidb.execute_param_query('''SELECT do_Ren_st_4 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Ren_st_5 = oimidb.execute_param_query('''SELECT do_Ren_st_5 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))
            self.do_Ren_st_6 = oimidb.execute_param_query('''SELECT do_Ren_st_6 FROM Childre_Substages WHERE kid_id = ?)''',(self.kid_id))