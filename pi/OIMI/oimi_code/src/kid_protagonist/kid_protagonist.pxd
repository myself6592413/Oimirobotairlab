cdef class Kid:
	cdef:
		int kid_id
		int permission_flag_Pr_st_1
		int permission_flag_Pr_st_2
		int permission_flag_Pr_st_3
		int permission_flag_Pr_st_4
		int permission_flag_Rw_st_1
		int permission_flag_Rw_st_2
		int permission_flag_Rw_st_3
		int permission_flag_Rw_st_4
		int permission_flag_Rw_st_5
		int permission_flag_Mo_st_1
		int permission_flag_Mo_st_2
		int permission_flag_Mo_st_3
		int permission_flag_Mo_st_4
		int permission_flag_Mo_st_5
		int permission_flag_Mo_st_6
		int do_Rep_st_1
		int do_Rep_st_2
		int do_Rep_st_3
		int do_Rep_st_4
		int do_Rep_st_5
		int do_Rep_st_6
		int do_Ren_st_1
		int do_Ren_st_2
		int do_Ren_st_3
		int do_Ren_st_4
		int do_Ren_st_5
		int do_Ren_st_6

	cdef setup_kid(self)