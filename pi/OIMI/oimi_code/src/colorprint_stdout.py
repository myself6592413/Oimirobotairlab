def prRed(prt):
    print(f"\033[91m{prt}\033[00m")
def prGreen(prt):
    print(f"\033[92m{prt}\033[00m")
def prYellow(prt):
    print(f"\033[93m{prt}\033[00m")
def prLightPurple(prt):
    print(f"\033[94m{prt}\033[00m")
def prPurple(prt):
    print(f"\033[95m{prt}\033[00m")
def prCyan(prt):
    print(f"\033[96m{prt}\033[00m")
def prLightGray(prt):
    print(f"\033[97m{prt}\033[00m")
def prBlack(prt):
    print(f"\033[98m{prt}\033[00m")
def prReset(prt):
    print(f"\033[0m{prt}\033[00m")
def prPure_red(prt):
    print(f"\033[0;31m{prt}\033[00m")
def prDark_green(prt):
    print(f"\033[0;32m{prt}\033[00m")
def prOrange(prt):
    print(f"\033[0;33m{prt}\033[00m")
def prDark_blue(prt):
    print(f"\033[0;34m{prt}\033[00m")
def prBright_purple(prt):
    print(f"\033[0;35m{prt}\033[00m")
def prDark_cyan(prt):
    print(f"\033[0;36m{prt}\033[00m")
def prDull_white(prt):
    print(f"\033[0;37m{prt}\033[00m")
def prPure_black(prt):
    print(f"\033[0;30m{prt}\033[00m")
def prBright_red(prt):
    print(f"\033[0;91m{prt}\033[00m")
def prLight_green(prt):
    print(f"\033[0;92m{prt}\033[00m")
def prYellow(prt):
    print(f"\033[0;93m{prt}\033[00m")
def prBright_blue(prt):
    print(f"\033[0;94m{prt}\033[00m")
def prMagenta(prt):
    print(f"\033[0;95m{prt}\033[00m")
def prLight_cyan(prt):
    print(f"\033[0;96m{prt}\033[00m")
def prBright_black(prt):
    print(f"\033[0;90m{prt}\033[00m")
def prBright_white(prt):
    print(f"\033[0;97m{prt}\033[00m")
def prCyan_back(prt):
    print(f"\033[0;46m{prt}\033[00m")
def prPurple_back(prt):
    print(f"\033[0;45m{prt}\033[00m")
def prWhite_back(prt):
    print(f"\033[0;47m{prt}\033[00m")
def prBlue_back(prt):
    print(f"\033[0;44m{prt}\033[00m")
def prOrange_back(prt):
    print(f"\033[0;43m{prt}\033[00m")
def prGreen_back(prt):
    print(f"\033[0;42m{prt}\033[00m")
def prPink_back(prt):
    print(f"\033[0;41m{prt}\033[00m")
def prGrey_back(prt):
    print(f"\033[0;40m{prt}\033[00m")
def prGrey(prt):
    print(f"\033[38;4;236m'{prt}\033[00m")
def prBold(prt):
    print(f"\033[1m{prt}\033[00m")
def prUnderline(prt):
    print(f"\033[4m{prt}\033[00m")
def prItalic(prt):
    print(f"\033[3m{prt}\033[00m")
def prDarken(prt):
    print(f"\033[2m{prt}\033[00m")
def prInvisible(prt):
    print(f"'\033[08m{prt}\033[00m")
def prReverse_colour(prt):
    print(f"\033[07m{prt}\033[00m")
def prReset_colour(prt):
    print(f"\033[0m{prt}\033[00m")
def prGrey(prt):
    print(f"\x1b[90m{prt}\033[00m")