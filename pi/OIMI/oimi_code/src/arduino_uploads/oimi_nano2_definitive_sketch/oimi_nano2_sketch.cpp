/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Final Arduino Nano sketch for Oimi Robot,
* sonars control 
* body LEDs animation
*
* Notes:
DONT INCLUDE to reset board
//#include <avr/io.h> dangerous!!! 
//#include <avr/wdt.h>
In this way it will restart avr and without resetting the board manually with the reset button placed on the Arduino 
it will be impossible to upload other sketches
*
*
* Created by Giacomo Colombo, Airlab Politecnico di Milano 2021
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include "StreamPrint.h"
#include "SoftwareSerial.h"
#include "StandardCplusplus.h"
#include <vector>
#include <queue>
#include "Mpx_5010.h"
#include "Adafruit_NeoPixel.h"
//#include "WS2812FX.h"
#include "SerialComm_2.h"
#include "SerialReplies.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
//Analog pins for mpx_5010 pressure sensors
#define a0 A0
#define a1 A1
#define a2 A2
#define a3 A3
#define a4 A4
//Digital pin for the Adafruit ledstrip
#define LED_PIN 4
// Number of leds of the ledstrip
#define LED_COUNT 60
// Timers
#define MAX_ITERATIONS 5000
#define MAX_TIME_ALLOWED 17000
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Global variables */
const int COUNT_ADDR1 = 0;
const int COUNT_ADDR2 = 1;

std::queue<int> q;				// queue of ints, for prepare right pattern during following movement
//Indices
int press_index;
int mpx_index;
int currParser;					// how catch info on serial, can change is value according to new data read
bool var_setup_repeat_msg;
bool global_event_colours;
bool check_global;
int modality;					// 0 if autonomous, 1 if following, 2 if single action, 5 if joypad
char* bufferMsg;				// where commands from raspi are stored
char* LedMsg;		    		// ledstrip color indication
char* patternMsg;   			// ledstrip pattern indication
char* LedMsg2;					// fixed without spaces
boolean taken; 					// true if modality has been received correctly from Mega
boolean just_happened;			// true if a command has been received now
boolean scanModality;			// to prepare Arduino to receive the right number of separated msgs and their types
boolean isStarted;				// check is started
boolean isPushed;				// for body contact during games
boolean isStillReading;			// for reading whole msg
// sublevel from rasPi, indicating the substage

int rando;

int group;						
int intensity;					
int resp_type;					
// Timers
unsigned long startMillis, currentMillis;                       // for body contact during games
unsigned long animationBeginMillis, animationElapsedMillis;     // for led time
unsigned long replyinterval;			                        // response pause
unsigned long colorDelay; 				                        // led pattern delay (used in games)
unsigned long resetDelay;			                            // reset delay
unsigned long pressureDelay;			                        // body pressure pause interval
unsigned long pauseDelay;			                            // for Arduino pause
unsigned long stopDelay;										// for extending duration of RED led during movement with gamepad
// Pressure mpx_5010
float press0,press1,press2,press3,press4;
bool stop_send_press;
bool finito;
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructors */
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
//WS2812FX ws2812fx = WS2812FX(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
//(la chiamo nano su raspi!)
SerialComm_2 sc = SerialComm_2(); //mod0 = without parser =no modality parameter to pass 
/*
currParser = sc.getCurrParser();
sc.acquireDataFromRaspi(currParser);
*/
// Mpxs
Mpx_5010 mpx_5010 = Mpx_5010(a0, a1, a2, a3, a4);
// Serial prints
SerialReplies sr = SerialReplies();
// Led predefinite colors (cambio nomi!)
uint32_t pink = strip.Color(255,102,255);  
uint32_t red = strip.Color(255,0,0);  	 
uint32_t green = strip.Color(0,255,0);		 
uint32_t blue = strip.Color(0,0,255);		 
uint32_t yellow = strip.Color(255,255,0);	 
uint32_t dark_green = strip.Color(102,102,0);	 
uint32_t light_blue = strip.Color(102,255,255);	 
uint32_t orange = strip.Color(255,102,0);	 
uint32_t color8 = strip.Color(0, 153, 51);	 
uint32_t violet = strip.Color(102, 0, 204);	 
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Methods */
void(* Reset_to_setup)(void) = 0;
//#define Reset_AVR() wdt_enable(WDTO_30MS); while(1) {} //definizione della funzione reset --> dangerous!! see avr comment above

void alertSwitch(int modality){
/*
Sets new parsing modality, letting Arduino to be prepared to read properly data on serial
*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sc.setCurrParser(modality);
		sr.alertSwitch(modality);
	}
}
char* removeSpaces(char* source){
	/*remove space from strings preceded by space ('<wipe, red>'), useless for integer submsgs*/ 
    char* token = strtok(source, " ");
    char* res = token;
    while (token != NULL) {
      token = strtok(NULL, " ");
      if (token != NULL) {
        strcat(res, token);
      }
    }
    return res;
}
////////////////////////////////////////////////////////////////////////////////////////////  LEDs
void set_queue_colors(){
    //Serial.println("enter in set_queue_colors");
	int t1 = 0;
	int t2 = 1;
	int t3 = 2;
	int t4 = 3;
	int t5 = 4;
	int t6 = 5;
	
	q.push(t1);
	q.push(t2);
	q.push(t3);
	q.push(t4);
	q.push(t5);
	q.push(t6);
}

void brightall(){
	Serial.println("enter in brightall");
	while (!q.empty()){
		if(!check_global){
			int a  = q.front();
			if (a == 1){
				strip.middleFill(red,50);
				Serial.print("size:");
				Serial.println(q.size());
				q.pop();
			}
			if (a == 2){
				strip.touchPattern_body(30,1);
				Serial.print("size:");
				Serial.println(q.size());
				q.pop();
			}
			if (a == 3){
				strip.randomPositionFill(blue,50);
				//fun1();
				Serial.print("size:");
				Serial.println(q.size());
			}
			if (a == 4)
			strip.shovePattern_body(30,2);
			}
		else{
			while(!q.empty()){
				q.pop();
				Serial.print("size_finale:");
				Serial.println(q.size());
			}
		}
	}
}

void reply_press_stop(){
    if(sc.getOkNewData()){
         sc.setOkNewData(false);
         if(just_happened){
				just_happened = 0;
	         	Serial.print("<press_stopped>");
     		}
     }
}
/////////////////////////////////////////////////////////////// 
bool controlstopduringpress(){
	int i = 0;
	int rr = 1;
	//need necessarily the while...doing many tentatives 5 tentatiteves are ALWAYS enough?????
	while(i<=5){
		sc.acquireDataFromRaspi();  			// starts catching data from serial
		bufferMsg = sc.getBufferMsg();			// takes command
		just_happened = sc.getJustChanged();	// receive confirm of msg received

		if(strcmp(bufferMsg, "presstop")==0){ 
			stop_send_press = false;
			reply_press_stop();
			rr = 0;
		}
		i = i + 1;
	}
	if(rr==0)
		return false;
	if(rr==1)
		return true;
	//else:
	//	sendPressures(1);					// echo previous pressures
}
/****************************************************************************************** Body pressures */
void grabPressures(){
	/*
	Stores current pressures
	*/
	mpx_5010.detectPressures();
	press0 = mpx_5010.getPressFrontLeft();
	press1 = mpx_5010.getPressFrontRight();
	press2 = mpx_5010.getPressRight();
	press3 = mpx_5010.getPressBack();
	press4 = mpx_5010.getPressLeft();
}

void sendPressures(int scans){
	/*
	Returns singleeeean of mpxs
		print("FINITA PRIMA PARTE ACQUISITZIONE")
	case 0:
		Sends a single scan, pressure already taken by loop independently??
		Used by raspi to discover new contacts
	case 1:
		Returns 10 scan of mpxs
		Used by raspi classification alg. in order to discover current interaction
	*/
	if(sc.getOkNewData()){
		int i = 0;
		sc.setOkNewData(false);
		if(!scans){
			grabPressures(); //to remove??? and leave in the loop??
			sr.echoPressures(press0, press1, press2, press3, press4);
		}
		else if(scans==1){
			while(press_index<=10){
				grabPressures();
				sr.echoPressures(press0, press1, press2, press3, press4, press_index);
				pressureDelay = millis();
				while (millis() < pressureDelay + 200) {} // waiting for 300 ms
				press_index = press_index + 1;
			}
			press_index=1;
			finito = true;
		}
		else if(scans==2){ //error!! non ne esco piu è !!!
			//while(stop_send_press==true){
			//	grabPressures();
			//	sr.echoPressures(press0, press1, press2, press3, press4, press_index);
			//	pressureDelay = millis();
			//	while (millis() < pressureDelay + 400) {} // waiting for 400 ms
			while(stop_send_press==true){
				while(press_index<=10){
					grabPressures();
					sr.echoPressures(press0, press1, press2, press3, press4, press_index);
					pressureDelay = millis();
					while (millis() < pressureDelay + 300) {} // pause for 200 ms
					press_index = press_index + 1;
				}
				press_index=1;
				finito = true;
				stop_send_press = controlstopduringpress();
			}
		}
	}
}
void checkPush(boolean isPushed){
	/* Returns true if body has been touched
	Used for body contact during games */
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.respondePush(isPushed);
	}
}
//################################################################################################################################################
//################################################################################################################################################
void reactBodyTouching(char* interaction){
	if(strcmp(interaction, "quiet")==0){
		//sr.printBody(0);
		Serialprint("<quiet_found>");
		strip.quietPattern_base(50, group);
	}
	else if (strcmp(interaction, "touch")==0){
		Serialprint("<touch_found>");
		rando = random(2);
		if(rando)
			strip.reactions_touch_noise_positive_or_negative(1, 1, 1); //(resp_type,negative,body)
		else
			strip.touchPattern_base(50, group);
	}
	else if (strcmp(interaction, "caress")==0){
		Serialprint("<caress_found>");
		rando = random(3);
		if(rando==0)
			strip.reactions_touch_noise_positive_or_negative(2, 1, 1); //(resp_type,negative,body)
		if(rando==1)
			strip.reactions_touch_noise_positive_or_negative(4, 1, 1); //(resp_type,negative,body)		
		else
			strip.caressPattern_base(50, group);
	}
	else if (strcmp(interaction, "choke")==0){
		Serialprint("<choke_found>");
		rando = random(2);
		if(rando)
			strip.chokePattern_base(50, group);
		else
			strip.reactions_touch_noise_positive_or_negative(1, 0, 1); //(resp_type,negative,body)
	}
	else if (strcmp(interaction, "squeeze")==0){
		Serialprint("<squeeze_found>");
		rando = random(2);
		if(rando)
			strip.squeezePattern_base(50, group);
		else		
			strip.reactions_touch_noise_positive_or_negative(5, 1, 1); //(resp_type,negative,body)
	}
	else if (strcmp(interaction, "shove")==0){
		Serialprint("<shove_found>");
		rando = random(2);
		if(rando)
			strip.shovePattern_base(50, group);
		else
			strip.reactions_touch_noise_positive_or_negative(2, 0, 1); //(resp_type,negative,body)
	}
	else if ((strcmp(interaction, "hug")==0) || (strcmp(interaction, "moodexcit")==0)){
		Serialprint("<hug_found>");
		rando = random(3);
		if(rando==0)
			strip.reactions_touch_noise_positive_or_negative(4, 1, 1); //(resp_type,negative,body)
		if(rando==1)		
			strip.reactions_touch_noise_positive_or_negative(2, 0, 1); //(resp_type,negative,body)
		else
			strip.hugPattern_base(10, group);
	}
	else if (strcmp(interaction, "hit")==0){
		Serialprint("<hit_found>");
		rando = random(2);
		if(rando)
			strip.hitPattern_base(50, group);
		else
			strip.reactions_touch_noise_positive_or_negative(4, 0, 1); //(resp_type,negative,body)
	}
	else if (strcmp(interaction, "hitleft")==0){  
		Serialprint("<hit_found>");
		strip.reactions_touch_noise_positive_or_negative(4, 0, 0);
	}
	else if (strcmp(interaction, "hitfront")==0){ 
		Serialprint("<hit_found>"); 
		strip.reactions_touch_noise_positive_or_negative(4, 0, 0);
	}
	else if (strcmp(interaction, "hitright")==0){ 
		Serialprint("<hit_found>"); 
		strip.reactions_touch_noise_positive_or_negative(4, 0, 0);
	}
	else if (strcmp(interaction, "hitback")==0){  
		Serialprint("<hit_found>"); 
		strip.reactions_touch_noise_positive_or_negative(4, 0, 0);
	}
}
void playPredictedInteraction(char* interaction){
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		group = sc.getSublevel();
		reactBodyTouching(interaction);
	}
}
void playSimpleReaction(int pos_or_neg){
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		if(pos_or_neg)
			Serialprint("<reacting_pos>");
		else
			Serialprint("<reacting_neg>");
		resp_type = sc.getSublevel();
		strip.reactions_touch_noise_positive_or_negative(resp_type, pos_or_neg, 0); //(resp_type, negative, base)
	}
}
void RespondWithLEDAnimantion(int result_obtained){
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		intensity = sc.getSublevel();
		switch(result_obtained){
			case 0: //ans to simple exercize is wrong
				sr.answerEx(result_obtained);
				strip.exercizeAnimation(result_obtained, 1); //1=body
				break;
			case 1: //ans to simple exercize is correct
				sr.answerEx(result_obtained);
				strip.exercizeAnimation(result_obtained, 1);
				break;
			case 2: //react to results achieved, celebrate
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 1); //..intensity, body)
				break;
			case 3: //react to results achieved, console
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 1);
				break;				
			case 4: //start animation for Sessions
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 1);
				break;
			case 5: //start animation for Matches
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 1);
				break;
			case 6: //start animation for Games 
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 1);
				break;
			case 7: //start animation for Entertainments 
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 1); 
				break;
			case 8: //inform that the answer is already given
				sr.respondeAlreadyGiven();
				strip.alreadyGivenAnimation(1, 2); //body/sophistication
				break;				
			case 9: //check time elapsed time from the beginning of LED animation
				sr.printTimeElapsed();
				strip.elapsedTimeAnimation(0);
				break;
			case 10: //prompt attention
				sr.printReaction(result_obtained);
				colorDelay = millis();
				strip.setInsideLEDmillis(colorDelay);
				//Serialprint("<prompt_atte>");
				//Serial.print(F("<prompt_atte>"));
				strip.promptAnimation(result_obtained, intensity, 1); //last 1 = body
				break;				
			case 11: //prompt encourage
				sr.printReaction(result_obtained);
				strip.promptAnimation(result_obtained, intensity, 1);
				break;				
			case 12: //prompt start
				sr.printReaction(result_obtained);
				strip.promptAnimation(result_obtained, intensity, 1);
				break;				
			case 13: //prompt end
				sr.printReaction(result_obtained);
				strip.promptAnimation(result_obtained, intensity, 1);
				break;				

		}
	}
}
//################################################################################################################################################
//################################################################################################################################################
void ordersDispatcher(){
	/*
	* Select proper instructions according to command received from raspi.
	* BufferMsg for starting led while robot is moving need a command the shortest as possible (one char).
	* Replies are just text (to be embraced into <>)
	* Same names of led_dict name
	*/
	sc.acquireDataFromRaspi();  			// starts catching data from serial
	bufferMsg = sc.getBufferMsg();			// takes command
	just_happened = sc.getJustChanged();	// receive confirm of msg received

	if(strcmp(bufferMsg, "switchMod0")==0){
		//sc.setCurrParser(0);
		alertSwitch(0);
	}
	else if(strcmp(bufferMsg, "switchMod1")==0){
		//sc.setCurrParser(1);
		alertSwitch(1);
	}
	else if(strcmp(bufferMsg, "switchMod2")==0){
		//sc.setCurrParser(2);
		alertSwitch(2);
	}	
	else if((strcmp(bufferMsg, "reset")==0)){	//reset
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				sr.printReset();
				resetDelay = millis();
				while(millis() < resetDelay + 500){}
				Reset_to_setup(); 
			}
		}
	}
	else if(strcmp(bufferMsg, "wrong")==0)  RespondWithLEDAnimantion(0);
	else if(strcmp(bufferMsg, "correct")==0)  RespondWithLEDAnimantion(1);
	else if(strcmp(bufferMsg, "celeb")==0)  RespondWithLEDAnimantion(2);
	else if(strcmp(bufferMsg, "conso")==0)  RespondWithLEDAnimantion(3);
	else if(strcmp(bufferMsg, "stsess")==0)  RespondWithLEDAnimantion(4);
	else if(strcmp(bufferMsg, "stmat")==0)  RespondWithLEDAnimantion(5);
	else if(strcmp(bufferMsg, "stgam")==0)  RespondWithLEDAnimantion(6);
	else if(strcmp(bufferMsg, "stent")==0)  RespondWithLEDAnimantion(7);
	else if(strcmp(bufferMsg, "already")==0)  RespondWithLEDAnimantion(8);
	else if(strcmp(bufferMsg, "elaps")==0)  RespondWithLEDAnimantion(9);
	else if(strcmp(bufferMsg, "pratte")==0) RespondWithLEDAnimantion(10);
	else if(strcmp(bufferMsg, "prenco")==0)  RespondWithLEDAnimantion(11);
	else if(strcmp(bufferMsg, "prsta")==0)  RespondWithLEDAnimantion(12);
	else if(strcmp(bufferMsg, "prend")==0)  RespondWithLEDAnimantion(13);
	else if(strcmp(bufferMsg, "moodandgry")==0) playPredictedInteraction('hit');
	else if(strcmp(bufferMsg, "moodexcit")==0) playPredictedInteraction(bufferMsg);
	else if(strcmp(bufferMsg, "moodhappy")==0) playPredictedInteraction('caress');
	else if(strcmp(bufferMsg, "moodsad")==0) playPredictedInteraction('shove');
	else if(strcmp(bufferMsg, "moodsurp")==0) playPredictedInteraction('touch');
	else if(strcmp(bufferMsg, "moodscare")==0) playPredictedInteraction('choke');
	else if(strcmp(bufferMsg, "negreac")==0) playSimpleReaction(0);
	else if(strcmp(bufferMsg, "posreac")==0) playSimpleReaction(1);

	else if((strcmp(bufferMsg, "quiet")==0) || (strcmp(bufferMsg, "caress")==0) || (strcmp(bufferMsg, "touch")==0) 
	|| (strcmp(bufferMsg, "squeeze")==0) ||	(strcmp(bufferMsg, "hug")==0) || (strcmp(bufferMsg, "choke")==0) || 
	(strcmp(bufferMsg, "shove")==0) || (strcmp(bufferMsg, "hit")==0)|| (strcmp(bufferMsg, "hitf")==0) || 
	(strcmp(bufferMsg, "hitr")==0) || (strcmp(bufferMsg, "hitl")==0) || (strcmp(bufferMsg, "hitb")==0)){ 
		playPredictedInteraction(bufferMsg); }	
	/*################################################################################################################################################*/
	else if((strcmp(bufferMsg, "reacthis")==0)){	// reaction with switchMod1 
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				patternMsg = sc.getMsgSecond();
				LedMsg = sc.getMsgThird();
				//Serial.print(F("<"));
				Serial.print(F("<custom reaction nano2: "));
				Serial.print(patternMsg);
				Serial.print(", ");
				Serial.print(LedMsg);
				Serial.println(F(">"));
			    LedMsg2 = removeSpaces(LedMsg);
				//strip.noColor(); //not here!!!
				if((strcmp(patternMsg, " stable")==0)){
					strip.colorStableAll(LedMsg2);
				}
				else if((strcmp(patternMsg, " wipe")==0)){
					strip.colorWipe(LedMsg2, 50); 
				}
				else if((strcmp(patternMsg, " blink")==0)){
					strip.middleFill(LedMsg2, 50); 
				}				
				//strip.noColor();
			}
		}
	}
	else if((strcmp(bufferMsg, "hh")==0)){ 	// happiness
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<happiness_ok>");
				strip.colorStable(blue);
				delay(5000);
				strip.noColor();
				strip.colorWipe(dark_green, 40);
				delay(5000);
				strip.noColor();
				strip.colorWipe(yellow, 40);
				delay(5000);
				strip.noColor();
				}
			}
		}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 Movements
	// Autonomous
	else if((strcmp(bufferMsg, "a")==0)){		// Autonomous begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<autonomous>");
				strip.colorStable(pink);
			}
		}
	}
	else if((strcmp(bufferMsg, "le")==0)){ 	// left autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<left1>");
				strip.colorStable(pink);
			}
		}
	}
	else if((strcmp(bufferMsg, "fr")==0)){	// front autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<front1>");
				strip.colorStable(green);
			}
		}
	}
	else if((strcmp(bufferMsg, "ri")==0)){	// right autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<right1>");
				strip.colorStable(yellow);
			}
		}
	}
	else if((strcmp(bufferMsg, "ba")==0)){	// back autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<back1>");
				strip.colorStable(red);
			}
		}
	}
	else if((strcmp(bufferMsg, "t")==0)){	// turn autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<turn1>");
				//strip.noColor();
				strip.colorStableAll(blue);
				//strip.colorStableInverse(blue);
			}
		}
	}
	//stop
	else if((strcmp(bufferMsg, "m")==0)){	// switch off led following
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				global_event_colours = 0; 
				check_global=1;
				just_happened = 0;
				Serialprint("<noled>");
				strip.noColor();
			}
		}
	}
	// Following
	else if((strcmp(bufferMsg, "f")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<following>");
				strip.colorStable(blue);
			}
		}
	}
	else if((strcmp(bufferMsg, "lp")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<leftpat>");
				strip.colorStable(dark_green);
			}
		}
	}
	else if((strcmp(bufferMsg, "rp")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<rightpat>");
				strip.colorWipe(light_blue, 30);
			}
		}
	}
	else if((strcmp(bufferMsg, "fp")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<frontpat>");
				strip.colorStable(orange);
			}
		}
	}
	// Joystick
	else if((strcmp(bufferMsg, "w")==0)){	// left gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<left-joystick>");
				//strip.noColor();
				strip.colorStableAll(green); //green
			}
		}
	}
	else if((strcmp(bufferMsg, "x")==0)){ 	// front gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<front-joystick>");
				//strip.noColor();
				strip.colorStableAll(light_blue); //light_blue
			    //delay(2000);
                //strip.randomPositionFill(light_blue, 30);
			    //strip.noColor();
    			//randomPositionFill(Color(0, 51, 0), 30);
				//strip.colorWipe(blue,40);
				//strip.noColor();
				//strip.colorWipe(blue,40);
				//strip.noColor();
				}
			}
		}
	else if((strcmp(bufferMsg, "y")==0)){	// right gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				Serialprint("<right-joystick>");
				//strip.noColor();
				strip.colorStableAll(yellow); //yellow
				//delay(2000);
                //strip.noColor();
			}
		}
	}
	else if((strcmp(bufferMsg, "z")==0)){	// back gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<back-joystick>");
				//strip.noColor();
				strip.colorStableAll(pink); //pink
                //delay(1000);
                //strip.colorStable(blue);
				//strip.noColor();

			}
		}
	}
	else if((strcmp(bufferMsg, "q")==0)){	// stop gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				stopDelay = millis();
				just_happened = 0;
				Serialprint("<stop-led>");
				//strip.noColor();
				strip.colorStableAll(red); //red
				while (millis() < stopDelay+300){}
				//strip.colorStable(red);
				//#delay(1000);
				//strip.colorStable(red);
				//delay(1000);
				//strip.colorStable(red);
				//delay(1000);
				//strip.colorStable(red);
				strip.noColor();
			}
		}
	}
	else if((strcmp(bufferMsg, "k")==0)){	//translate gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<trans-game>");
				//strip.noColor();
				strip.colorStableAll(red);
				//strip.randomPositionFill(pink,40);
			}
		}
	}
	else if((strcmp(bufferMsg, "kk")==0)){	//feel color 
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<feel-game>");
				strip.colorStableAll(violet); //violet
				//delay(3000);
				//strip.randomPositionFill(pink,40);
			}
		}
	}
	else if((strcmp(bufferMsg, "c")==0)){
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<pattern>");
				global_event_colours=true;
			}
		}
	}

}
/*#########################################################################################################################*/
void lightLED(){
	if(!q.empty()){
		if(!check_global){
			ordersDispatcher(); 			//still scan serial for external msgs
			int i = q.front();
			if(i==0){
				strip.middleFill(red,50);
				strip.noColor();
				q.pop();
			}
			else if(i==1){
				strip.middleFill(green,50);
				strip.noColor();
				q.pop();
			}
			else if(i==2){
				strip.middleFill(blue,50);
				strip.noColor();
				q.pop();
			}
			else if(i==3){
				strip.middleFill(yellow,50);
				strip.noColor();
				q.pop();
			}
			else if(i==4){
				strip.middleFill(red,50);
				strip.noColor();
				q.pop();
			}
			else if(i==5){
				strip.middleFill(green,50);
				strip.noColor();
				q.pop();
			}
		}
		else{
			global_event_colours=false;
			strip.noColor();
			while(!q.empty())
				q.pop();
			Serial.print("size_finale:");
			Serial.println(q.size());
		}
	}
}
/*#########################################################################################################################
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Main */
void setup(){
	Serial.begin(115200);
	Serialprint("<Nano2 is ready>");
	Serial.flush();
	strip.begin();
	strip.show();
	strip.setBrightness(100);
	set_queue_colors();
	// Indices
	press_index = 1;
	mpx_index = 0;
	// Timers
	replyinterval = 50; //ms
	colorDelay = 0;
	resetDelay = 0;
	startMillis = millis();
	animationBeginMillis = millis();
	animationElapsedMillis = millis();
	var_setup_repeat_msg = 0;
	// Flags
	isPushed = false;
	isStillReading = false;
	isStarted = true;
	scanModality = 0;
	strip.noColor();
	finito = false;
}
void loop(){
	/*
	modality == 0,1 --> need sonars, autonomous,following move
	modality == 2 --> single action
	modality == 3 --> joystick move
	*/
	currentMillis = millis();
    //grabPressures();
	ordersDispatcher(); // gets command from raspi if any
	//if(global_event_colours){
	//	lightLED();
	//}
	//if (var_setup_repeat_msg) {
	//	Serialprint("<Nano2 is ready>");
	//}
}