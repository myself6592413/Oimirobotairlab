/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Final Arduino sketch  for the Nano1 board on Oimi.
* It's responsabilities is to control LEDs_base and sonars 
* 
Notes: 
	"modality" in the loop has the role of activating subroutines, it had nothing to do with serial parser modality, 

	Removed parts: LEDS other libraries
	library ws2812fx and FASTLED works well, but it was removed due to limit code size 
	//#include "WS2812FX.h"
	//ws2812fx.init();
	//ws2812fx.setBrightness(200);
	//ws2812fx.setSpeed(100);
	//ws2812fx.start();

	//Number of leds of ledstrip Fastled
	//#define NUM_LEDS 60
	//#define BRIGHTNESS 64
	//#define LED_TYPE    WS2811
	//#define COLOR_ORDER GRB
	//CRGB leds[NUM_LEDS];
	//#define UPDATES_PER_SECOND 100

*
* Created by Giacomo Colombo, Airlab Politecnico di Milano 2021
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
#include "StreamPrint.h"
#include "SoftwareSerial.h"
#include "StandardCplusplus.h"
#include "Sonars_EZ1.h"
#include "Adafruit_NeoPixel.h"
#include "SerialReplies.h"
#include "SerialComm_2.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
// Analog Pins for sonars
#define _ANA1 0    //back
#define _ANA2 1    //left
#define _ANA3 2    //front
#define _ANA4 3    //right
// Digital PWM Pins for sonars
#define _TRIG1 6   //back
#define _TRIG2 11  //left
#define _TRIG3 10  //front
#define _TRIG4 9   //right
//Digital pin for the base ledstrip
#define LED_PIN 5
// Number of leds of the Adafruit ledstrip 
#define LED_COUNT 60
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Global variables */
const int COUNT_ADDR1 = 0;
const int COUNT_ADDR2 = 1;

Obs current_env;  						// extern sonars environment
int currParser;							// how catch info on serial, can change is value according to new data read
int typeOfMovement;     				// for sending to raspi the right current movement received from Mega 
int dist1,dist2,dist3,dist4;			// sonars detections [inch]
int modality;							// 0 if autonomous, 1 if following, 2 if single action, 5 if joypad
char* bufferMsg;						// where commands from raspi are stored
char* LedMsg;		    				// ledstrip color indication
char* patternMsg;   					// ledstrip pattern indication
char* LedMsg2;		    		
char* patternMsg2;   		
int group;								// to get the secondMsg from rasPi (a number in this case), indicating the substage
int inte;								// same as above
int intensity;							// same as above
int resp_type;							// same as above
boolean taken; 							// true if modality has been received correctly from Mega
boolean just_happened;					// true if a command has been received now


int rando;
// Timers
unsigned long stopDelay;				// for extending duration of RED led during movement with gamepad
unsigned long resetDelay;				// for extending duration of RED led during movement with gamepad
unsigned long colorDelay; 				                     //led pattern delay (used in games)
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructors */
SoftwareSerial sonarSerial(12,8);  //rx,tx (12-8 / 8-12 pins connection--invert pin of master)
Sonars_EZ1 ez1 = Sonars_EZ1(_ANA1, _ANA2, _ANA3, _ANA4, _TRIG1, _TRIG2, _TRIG3, _TRIG4);
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
//WS2812FX ws2812fx = WS2812FX(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
//(la chiamo nano su raspi!)
SerialComm_2 sc = SerialComm_2();
// Serial prints
SerialReplies sr = SerialReplies();

// Predefinite colors (for "reacthis")
uint32_t orange = strip.Color(255,102,0);
uint32_t red = strip.Color(255,0, 0);  	 
uint32_t green = strip.Color(0,255,0);		 
uint32_t blue = strip.Color(0, 0,255);		 
uint32_t yellow = strip.Color(255,255,0);	 
uint32_t dark_green = strip.Color(102,102,0);
uint32_t light_blue = strip.Color(102,255,255);
uint32_t pink = strip.Color(255,102,255);	 
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Methods */
void(* Reset_to_setup)(void) = 0;

void alertSwitch(int rightconf){
	/* Sets new parsing rightconf, letting Arduino to be prepared to read properly data on serial */
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sc.setCurrParser(rightconf);
		sr.alertSwitch(rightconf);
	}
}

/*
char* removeSpaces(char* source){
    if (source == NULL || *source == '\0' || strchr(source, ' ') == NULL) { //NULL, empty or contains no spaces
        // Return an empty string or a string with a single space if input is empty or contains no spaces
        return strdup(*source == '\0' ? "" : " "); //using the strdup function to allocate memory for the new string
    }

    char* token = strtok(source, " ");
    char* res = token;
    while (token != NULL) {
      token = strtok(NULL, " ");
      if (token != NULL) {
        strcat(res, token);
      }
    }
    return res;
}
*/
/*
char* removeSpaces(char* source){
		//After the result is computed, we create a copy of the result using strdup, free the memory allocated for source_copy, and return the result. 
		//This updated implementation should handle the case where the input string needs to be preserved and return a valid result without modifying 
		//the original input string

    if (source == NULL || *source == '\0' || strchr(source, ' ') == NULL) {
        // Return an empty string or a string with a single space if input is empty or contains no spaces
        return strdup(*source == '\0' ? "" : " ");
    }

    char* source_copy = strdup(source);
    char* token = strtok(source_copy, " ");
    char* res = token;
    while (token != NULL) {
      token = strtok(NULL, " ");
      if (token != NULL) {
        strcat(res, token);
      }
    }
    char* result = strdup(res);
    free(source_copy);
    return result;
}
*/
char* removeSpaces(char* source){
	/* Removes all spaces from a given string by iterating through the tokens of the string 
	(separated by spaces) and concatenating them without spaces.*/ 
    char* token = strtok(source, " ");
    char* res = token;
    while (token != NULL) {
      token = strtok(NULL, " ");
      if (token != NULL) {
        strcat(res, token);
      }
    }
    return res;
}

void sonarsCatch(){
	/* Scans sonars */
	ez1.maxsonarsLoop(); //read the distances 
	dist1 = ez1.getSonarLeft();
	dist2 = ez1.getSonarFront();
	dist3 = ez1.getSonarRight();
	dist4 = ez1.getSonarBack();
}
void sonarsPrint(){
	/* Sends sonars values to Mega */
	sonarSerial.print(dist1);
	sonarSerial.print(",");
	sonarSerial.print(dist2);
	sonarSerial.print(",");
	sonarSerial.print(dist3);
	sonarSerial.print(",");
	sonarSerial.print(dist4);
	sonarSerial.print('\n');
}
void reactBodyTouching(char* interaction){
	// don't stop to look at sonars
	sonarsCatch();
	sonarsPrint();

	if(strcmp(interaction, "quiet")==0){
		//sr.printBody(0);
		Serialprint("<quiet_ok>");
		strip.quietPattern_base(50, group);
	}
	else if (strcmp(interaction, "touch")==0){
		Serialprint("<touch_ok>");
		rando = random(2);
		if(rando)
			strip.reactions_touch_noise_positive_or_negative(1, 1,0); //(resp_type,negative,base)
		else
			strip.touchPattern_base(50, group);
	}
	else if (strcmp(interaction, "caress")==0){
		Serialprint("<caress_ok>");
		rando = random(3);
		if(rando==0)
			strip.reactions_touch_noise_positive_or_negative(2, 1,0); //(resp_type,negative,base)
		if(rando==1)
			strip.reactions_touch_noise_positive_or_negative(4, 1,0); //(resp_type,negative,base)		
		else
			strip.caressPattern_base(50, group);
	}
	else if (strcmp(interaction, "choke")==0){
		Serialprint("<choke_ok>");
		rando = random(2);
		if(rando)
			strip.chokePattern_base(50, group);
		else
			strip.reactions_touch_noise_positive_or_negative(1, 0, 0); //(resp_type,negative,base)
	}
	else if (strcmp(interaction, "squeeze")==0){
		Serialprint("<squeeze_ok>");
		rando = random(2);
		if(rando)
			strip.squeezePattern_base(50, group);
		else		
			strip.reactions_touch_noise_positive_or_negative(5, 1,0); //(resp_type,negative,base)
	}
	else if (strcmp(interaction, "shove")==0){
		Serialprint("<shove_ok>");
		rando = random(2);
		if(rando)
			strip.shovePattern_base(50, group);
		else
			strip.reactions_touch_noise_positive_or_negative(2, 0, 0); //(resp_type,negative,base)
	}
	else if ((strcmp(interaction, "hug")==0) || (strcmp(interaction, "moodexcit")==0)){
		Serialprint("<hug_ok>");
		rando = random(3);
		if(rando==0)
			strip.reactions_touch_noise_positive_or_negative(4, 1,0); //(resp_type,negative,base)
		if(rando==1)		
			strip.reactions_touch_noise_positive_or_negative(2, 0, 0); //(resp_type,negative,base)
		else
			strip.hugPattern_base(10, group);
	}
	else if (strcmp(interaction, "hit")==0){
		Serialprint("<hit_ok>");
		rando = random(2);
		if(rando)
			strip.hitPattern_base(50, group);
		else
			strip.reactions_touch_noise_positive_or_negative(4, 0, 0); //(resp_type,negative,base)
	}
	else if (strcmp(interaction, "hitleft")==0){  
		Serialprint("<hit_ok>");
		strip.reactions_touch_noise_positive_or_negative(4, 0, 0);
	}
	else if (strcmp(interaction, "hitfront")==0){ 
		Serialprint("<hit_ok>"); 
		strip.reactions_touch_noise_positive_or_negative(4, 0, 0);
	}
	else if (strcmp(interaction, "hitright")==0){ 
		Serialprint("<hit_ok>"); 
		strip.reactions_touch_noise_positive_or_negative(4, 0, 0);
	}
	else if (strcmp(interaction, "hitback")==0){  
		Serialprint("<hit_ok>");
		strip.reactions_touch_noise_positive_or_negative(4, 0, 0);
	}
}

void playPredictedInteraction(char* interaction){
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		group = sc.getSublevel();
		reactBodyTouching(interaction);
	}
}
void playSimpleReaction(int pos_or_neg){
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		if(pos_or_neg)
			Serialprint("<reacting_pos>");
		else
			Serialprint("<reacting_neg>");
		resp_type = sc.getSublevel();
		strip.reactions_touch_noise_positive_or_negative(resp_type, pos_or_neg, 0); //(resp_type,negative,base)
	}
}
void RespondWithLEDAnimantion(int result_obtained){
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		intensity = sc.getSublevel();
		switch(result_obtained){
			case 0: //ans to simple exercize is wrong
				sr.answerEx(result_obtained);
				strip.exercizeAnimation(result_obtained,0); //0=base
				break;
			case 1: //ans to simple exercize is correct
				sr.answerEx(result_obtained);
				strip.exercizeAnimation(result_obtained,0);
				break;
			case 2: //react to results achieved, celebrate
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 0); //..intensity, base)
				break;
			case 3: //react to results achieved, console
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 0);
				break;				
			case 4: //start animation for Sessions
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 0);
				break;
			case 5: //start animation for Matches
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 0);
				break;
			case 6: //start animation for Games 
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 0);
				break;
			case 7: //start animation for Entertainments 
				sr.printReaction(result_obtained);
				strip.rewardAnimation(result_obtained, intensity, 0); 
				break;
			case 8: //inform that the answer is already given
				sr.respondeAlreadyGiven();
				strip.alreadyGivenAnimation(0, 2);
				break;
			case 9: //check time elapsed time from the beginning of LED animation
				sr.printTimeElapsed();
				strip.elapsedTimeAnimation(0);
				break;
			case 10: //prompt attention ---> intensity??
				sr.printReaction(result_obtained);
				colorDelay = millis();
				strip.setInsideLEDmillis(colorDelay);
				strip.promptAnimation(result_obtained, intensity, 0);
				//Serial.print(F("<prompt_attention_ok>"));
				//Serial.print(F("<prompt_atte>"));
				break;			
			case 11: //prompt encourage
				sr.printReaction(result_obtained);
				strip.promptAnimation(result_obtained, intensity, 0);
				break;			
			case 12: //prompt start
				sr.printReaction(result_obtained);
				strip.promptAnimation(result_obtained, intensity, 0);
				break;			
			case 13: //prompt end
				sr.printReaction(result_obtained);
				strip.promptAnimation(result_obtained, intensity, 0);
				break;
		}
	}
}
/****************************************************************************************** Games reactions */

/****************************************************************************************** Physical contacts reactions*/
/**
/****************************************************************************************** Inspector of serial msgs */
void ordersDispatcher(){
	/*
	* Select proper instructions according to command received from raspi.
	* BufferMsg for starting led while robot is moving need a command the shortest as possible (one char).
	* Replies are just text (to be embraced into <>)
	* Same names of led_dict name
	*/
	sc.acquireDataFromRaspi();  // starts catching data from serial
	bufferMsg = sc.getBufferMsg();			// takes command
	just_happened = sc.getJustChanged();	// receive confirm of msg completely received

	if(strcmp(bufferMsg, "switchMod0")==0)
		alertSwitch(0);
	else if(strcmp(bufferMsg, "switchMod1")==0)
		alertSwitch(1);
	else if(strcmp(bufferMsg, "switchMod2")==0)
		alertSwitch(2);
	else if((strcmp(bufferMsg, "reset")==0)){	//reset to setup 
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				sr.printReset();
				resetDelay = millis();
				while(millis() < resetDelay + 500){}
				//Serialprint("<Nano1 is ready>"); no repetition needed
				Reset_to_setup();
			}
		}
	}
	else if(strcmp(bufferMsg, "wrong")==0)  RespondWithLEDAnimantion(0);
	else if(strcmp(bufferMsg, "correct")==0)  RespondWithLEDAnimantion(1);
	else if(strcmp(bufferMsg, "celeb")==0)  RespondWithLEDAnimantion(2);
	else if(strcmp(bufferMsg, "conso")==0)  RespondWithLEDAnimantion(3);
	else if(strcmp(bufferMsg, "stsess")==0)  RespondWithLEDAnimantion(4);
	else if(strcmp(bufferMsg, "stmat")==0)  RespondWithLEDAnimantion(5);
	else if(strcmp(bufferMsg, "stgam")==0)  RespondWithLEDAnimantion(6);
	else if(strcmp(bufferMsg, "stent")==0)  RespondWithLEDAnimantion(7);
	else if(strcmp(bufferMsg, "already")==0)  RespondWithLEDAnimantion(8);
	else if(strcmp(bufferMsg, "elaps")==0)  RespondWithLEDAnimantion(9);
	else if(strcmp(bufferMsg, "pratte")==0) RespondWithLEDAnimantion(10);
	else if(strcmp(bufferMsg, "prenco")==0)  RespondWithLEDAnimantion(11);
	else if(strcmp(bufferMsg, "prsta")==0)  RespondWithLEDAnimantion(12);
	else if(strcmp(bufferMsg, "prend")==0)  RespondWithLEDAnimantion(13);	
	else if(strcmp(bufferMsg, "moodandgry")==0) playPredictedInteraction('hit');
	else if(strcmp(bufferMsg, "moodexcit")==0) playPredictedInteraction(bufferMsg);
	else if(strcmp(bufferMsg, "moodhappy")==0) playPredictedInteraction('caress');
	else if(strcmp(bufferMsg, "moodsad")==0) playPredictedInteraction('shove');
	else if(strcmp(bufferMsg, "moodsurp")==0) playPredictedInteraction('touch');
	else if(strcmp(bufferMsg, "moodscare")==0) playPredictedInteraction('choke');
	else if(strcmp(bufferMsg, "negreac")==0) playSimpleReaction(0);
	else if(strcmp(bufferMsg, "posreac")==0) playSimpleReaction(1);

	else if((strcmp(bufferMsg, "quiet")==0) || (strcmp(bufferMsg, "caress")==0) || (strcmp(bufferMsg, "touch")==0) 
	|| (strcmp(bufferMsg, "squeeze")==0) ||	(strcmp(bufferMsg, "hug")==0) || (strcmp(bufferMsg, "choke")==0) || 
	(strcmp(bufferMsg, "shove")==0) || (strcmp(bufferMsg, "hit")==0)|| (strcmp(bufferMsg, "hitf")==0) || 
	(strcmp(bufferMsg, "hitr")==0) || (strcmp(bufferMsg, "hitl")==0) || (strcmp(bufferMsg, "hitb")==0)){ 
		playPredictedInteraction(bufferMsg); }

	//############################################################################################################
	else if((strcmp(bufferMsg, "reacthis")==0)){	// reaction with switchMod1 
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if (modality == 4 && taken){
				sonarsCatch();
				sonarsPrint();
			}
			if(just_happened){ //if data_completely received
				just_happened = 0; //rest received....only locally!!! on on justchanged in SerialComm2 library
				patternMsg = sc.getMsgSecond();
				LedMsg = sc.getMsgThird();
				Serialprint("<nano1: ");
				Serial.print(patternMsg);
				Serialprint("; ");
				Serial.print(LedMsg); //no serialprint library!!!! it is not a string!
				Serial.println(">");
			    patternMsg2 = removeSpaces(patternMsg);
			    LedMsg2 = removeSpaces(LedMsg);
				//strip.noColor(); //not here!!!
				if((strcmp(patternMsg2, "stable")==0)){
					strip.colorStableAll(LedMsg2);
				}
				else if((strcmp(patternMsg2, "wipe")==0)){
					strip.colorWipe(LedMsg2, 50); 
				}
				else if((strcmp(patternMsg2, "blink")==0)){
					strip.middleFill(LedMsg2, 50); 
				}
			}
		}
	}
	//############################################################################################################ movements
	// Autonomous
	else if((strcmp(bufferMsg, "a")==0)){		// Autonomous begins 
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<autonomous>");
				strip.colorWipe(blue,50);
				//strip.colorStable(orange);
			}
		}
	}
	else if((strcmp(bufferMsg, "le")==0)){ 	// left autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<left1>");
				strip.colorStable(orange);
			}
		}
	}
	else if((strcmp(bufferMsg, "fr")==0)){	// front autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<front1>");
				strip.colorStable(green);
			}
		}
	}
	else if((strcmp(bufferMsg, "ri")==0)){	// right autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<right1>");
				strip.colorStable(yellow);
			}
		}
	}
	else if((strcmp(bufferMsg, "ba")==0)){	// back autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<back1>");
				strip.colorStable(red);
			}
		}
	}
	else if((strcmp(bufferMsg, "t")==0)){	// turn autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<turn1>");
				//strip.noColor();
				strip.colorStable(blue);
				//strip.colorStableInverse(blue);
			}
		}
	}
	else if((strcmp(bufferMsg, "m")==0)){	// switch off led following
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<noled>");
				strip.noColor();
			}
		}
	}
	// Following
	else if((strcmp(bufferMsg, "f")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<following>");
				strip.colorStable(blue);
			}
		}
	}
	else if((strcmp(bufferMsg, "lp")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<leftpat>");
				strip.colorStable(dark_green);
			}
		}
	}
	else if((strcmp(bufferMsg, "rp")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<rightpat>");
				strip.colorStable(light_blue);
			}
		}
	}
	else if((strcmp(bufferMsg, "fp")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<frontpat>");
				strip.colorStable(pink);
			}
		}
	}
	// Joystick
	else if((strcmp(bufferMsg, "w")==0)){	// left gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<left-joystick>");
				strip.colorStable(green);
			}
		}
	}
	else if((strcmp(bufferMsg, "x")==0)){ 	// front gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<front-joystick>");
				strip.colorStable(blue);
				//strip.colorWipe_noFor(blue,40);
				//strip.noColor();
				//strip.colorWipe(blue,40);
				//strip.noColor();
			}
		}
	}
	else if((strcmp(bufferMsg, "y")==0)){	// right gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				typeOfMovement=0;
				Serialprint("<right-joystick>");
				strip.colorStable(yellow);
			}
		}
	}
	else if((strcmp(bufferMsg, "z")==0)){	// back gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<back_joystick>");
				strip.colorStable(dark_green);
			}
		}
	}
	else if((strcmp(bufferMsg, "q")==0)){	// stop gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				stopDelay = millis();
				just_happened = 0;
				Serialprint("<stop-game>");
				strip.noColor();
				strip.colorStable(red);
				while (millis() < stopDelay+500){}
				//strip.colorStable(red);
				//#delay(1000);
				//strip.colorStable(red);
				//delay(1000);
				//strip.colorStable(red);
				//delay(1000);
				//strip.colorStable(red);
				strip.noColor();
			}
		}
	}
	else if((strcmp(bufferMsg, "k")==0)){	//translate gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<trans-game>");
				strip.noColor();
				strip.colorStable(orange);
				//strip.randomPositionFill(orange,40);
			}
		}
	}
	else if((strcmp(bufferMsg, "c")==0)){
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<pattern>");
			}
		}
	}

	else if((strcmp(bufferMsg, "reset")==0)){	//reset
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				sr.printReset();
				resetDelay = millis();
				while(millis() < resetDelay + 500){}
				//var_setup_repeat_msg = 1;
				//Serialprint("<Nano1 is ready>");
				Reset_to_setup();
				//Reset_AVR();
			}
		}
	}	
}
/*##################################################################################################*/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Main */
void setup(){
	sonarSerial.begin(9600);
	sonarSerial.flush();
	Serial.begin(115200);
	Serialprint("<Nano1 is ready>");
	Serial.flush();
	strip.begin();
	strip.show();
	strip.setBrightness(200);
	resetDelay = 0;
}
void loop(){
	/*
	modality == 0, 1 --> sonars, autonomous, following move
	modality == 2 --> single action pattern with predefinite speeds in Movements library
	modality == 3 --> joystick movements commands
	modality == 4 --> pattern but using sonars
	modality == 5 --> stop taking sonars
	*/
	ordersDispatcher(); // gets command from raspi if any 

	if(sonarSerial.available()){ // gets command from Mega if any
		int mod = sonarSerial.parseInt();
		if(sonarSerial.read() == '\n'){
			modality = mod;
			modality==0 ? typeOfMovement = 2 : modality==1 ? typeOfMovement = 1 :
			modality==2 ? typeOfMovement = 3 : modality==4 ? typeOfMovement = 4 : 
			modality==5 ? typeOfMovement = 5 : typeOfMovement = 0;
			taken = true;
		}
	}
	if((modality != 2 && modality != 3) && taken){  // when joypad is active (modality is 3) sonars aren't necessary! also for some patterns of movement
	//if((modality != 2 && modality != 3 && modality != 4) && taken){  // when joypad is active (modality is 3) sonars aren't necessary! also for some patterns of movement
		sonarsCatch();
		sonarsPrint();
	}
	if(taken){
		if(typeOfMovement){ //to speedup and jump immediately after first cycle
			if(typeOfMovement==1){
				//Serialprint("<goauto>");
				typeOfMovement = 0;
			}
			else if(typeOfMovement==2){
				//Serialprint("<gofollo>");
				typeOfMovement = 0;
			}
			else if(typeOfMovement==3){
				//Serialprint("<goaction>");
				typeOfMovement = 0;
			}
			if(typeOfMovement==4){
				//Serialprint("<go_pattern_sensor>");
				typeOfMovement = 0;
			}
			if(typeOfMovement==5){
				//Serialprint("<closesonars>");
				taken = false;
				typeOfMovement = 0;
			}		
		}
	}
}