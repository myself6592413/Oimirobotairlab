/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Oimi-Robot definitive sketch for Arduino Mega
* Controls pressure sensors, motors, leds
* --notes
* 	--loop
		N.B. --see below--  no pidLoop here,
		to avoid micro-movements instead of being completely still
*
* Created by Giacomo Colombo, Airlab Politecnico di Milano 2020
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdlib.h>
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Libraries */
#include "SoftwareSerial.h"
#include "StreamPrint.h"
#include "StandardCplusplus.h"
#include <vector>
#include "CytronDriver_C10.h"
#include "Encoder.h"
#include "OimiPid.h"
#include "Sonars_EZ1.h"
#include "Mpx_5010.h"
#include "Adafruit_NeoPixel.h"
#include "SerialComm.h"
#include "SerialReplies.h"
#include "Movements.h"
#include "Voltage.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
//Analog pins for mpx_5010 pressure sensors
#define a0 A0
#define a1 A1
#define a2 A2
#define a3 A3
#define a4 A4
//Analog pins for maxsonars distance sensors
//#define _ANA1 12 //back
//#define _ANA2 8  //left
//#define _ANA3 11 //front
//#define _ANA4 9  //right
//Digital pin for maxsonars distance sensors
//#define _TRIG 7

//Digital pins for motors
#define _DIR1 9
#define _DIR2 11
#define _DIR3 10
#define _PWM1 6
#define _PWM2 5
#define _PWM3 4
//Digital pins for encoders' channels
#define _HA1 20
#define _HA2 18
#define _HA3 3
#define _HB1 21
#define _HB2 19
#define _HB3 2
//Digital pin for ledstrip
#define LED_PIN 53
// Number of leds of ledstrip
#define LED_COUNT 60
//Analog pin for voltage checker
#define VOLT_PIN A5
// Timers
#define MAX_ITERATIONS 5000
#define MAX_TIME_ALLOWED 17000
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * Global variables */
//Indices
int further_check;
int emo_level;
int rea;
int group;
int extent;
int forpause;
int son1,son2,son3,son4;									// to store sonars detection by Nano
int modnano;
int scanProcess;												// to send modality of scanning sonars to Nano
int press_index;												// for looping pressures
int mpx_index;													// for pressure
int stopscan; 													// single predifinite movements that ends, no more necessary to tell to nano1 to scan sonar
int ended;
// Buffers
int currParser;							               // how catch info on serial, can change is value according to new data read
//useless!!! //const byte buffSize = 200; 			// predefinite buffer dimension 200 is the right choice, don't reduce
char* bufferMsg;      										// where first and main command is stored, initializes first element to zero
char* ledMsg;		    										// ledstrip color command
char* patternMsg;   											// ledstrip pattern command
// Flags
boolean very_first_loop_reaction;
boolean entered_in_move;
boolean letsStart;
boolean tellNano;												// true when is time to send data to Nano in next cycle
boolean toldNano;												// cicle tellnano ended ...ok to scan sensors
boolean isReady;												// handle correct working of loops starting movement with correct sonars values
boolean just_happened;					  					// for emotion reactions
boolean isStarted;						  					// for ???
boolean isPushed;						  					   // for body contact during games
boolean isStillReading;					  					// for reading whole msg
boolean okNewData;						  					// if ok looks for data on serial
boolean isSingleCustom;										// custom movement with precise speeds (no need to multiply)
// Timers
unsigned long timeSetup;
unsigned long startAllTime, loopTime;              // for body contact during games
unsigned long replyinterval;			               // response pause
unsigned long colorDelay; 				               // led pattern delay (used in games)
unsigned long resetDelay;			                  // reset delay
unsigned long pressureDelay;			               // body pressure pause interval
unsigned long pauseDelay;			                  // for Arduino pause
// Pressure mpx_5010
float press0,press1,press2,press3,press4;
// Speeds gamepad, calculated from direction versors
float forwSpeedOrder  = 0.0;				  			   // forward speed
float straSpeedOrder = 0.0;				  			   // strafe speed
float anguSpeedOrder  = 0.0;				  			   // angular speed
// Movement
unsigned char last_movement;
// Objects
Obs current_env;
// Battery
float power;							  					   // battery status [volt]
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructors */
// Serial between Mega and Nano
SoftwareSerial sonarSerial(12,8); //rx,tx (12-8 / 8-12 pins connection-- N.B: invert pin of master)
// Motor drivers
CytronDriver_C10 m1(_DIR1,_PWM1);
CytronDriver_C10 m2(_DIR2,_PWM2);
CytronDriver_C10 m3(_DIR3,_PWM3);
// Encoders
Encoder e1(_HA1, _HB1);
Encoder e2(_HA2, _HB2);
Encoder e3(_HA3, _HB3);
// OimiPid
OimiPid oimiPid(m1, m2, m3, e1, e2, e3);
// Leds
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
//Sonars
Sonars_EZ1 ez1 = Sonars_EZ1();
//Movements
Movements mov(oimiPid, ez1, strip);
// Pressures
Mpx_5010 mpx_5010(a0, a1, a2, a3, a4);
// Voltage
Voltage volt(VOLT_PIN);
// Serial communication
SerialComm sc = SerialComm();
// Serial prints
SerialReplies sr = SerialReplies();
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Functions */
/************************************************************************************** Sonars */
void sonarLoop(){
	if(sonarSerial.available()){
		//Serial.print("entro in sonarloop!!!");
		int son1 = sonarSerial.parseInt();
		int son2 = sonarSerial.parseInt();
		int son3 = sonarSerial.parseInt();
		int son4 = sonarSerial.parseInt();
	   if (sonarSerial.read() == '\n') {
			//Serial.print("--->");
			//Serial.print(son1);
			//Serial.print(",");
			//Serial.print(son2);
			// Serial.print(",");
			//Serial.print(son3);
			//Serial.print(",");
			//Serial.println(son4);
			//Serial.print("<---");
			ez1.putSonars(son1, son2, son3, son4);
			//ez1.maxsonarsLoop(0);
			ez1.maxsonarsLoop(scanProcess);
		}
	}
}
/************************************************************************************** Arduino control */
void emptyArduno(){
	while(Serial.available())
		byte temnp = Serial.read();

}
void(* resetFunc)(void) = 0; /*Safe reset after */

void pauseArduino(){
/* Puts Arduino in sleeping mode, ready for a new instruction
   After a single movement instruction
   After a emotional behaviour
   After ends of movement
*/
	pauseDelay = millis();
	while (millis() < pauseDelay + 1000) {}  // waits 1 second
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);     // sets sleep mode power consumption to 0.36 mA
	cli();	// Disable interrupts            // disables interrupts
	sleep_mode();                            // puts AVR in sleep mode
}

void reportReset(){
/* Receive resetting order from raspi*/
	if (sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.printReset();
		resetDelay = millis();
		while(millis() < resetDelay + 500){}
		mov.setIsEnded(true);
		resetFunc();
	}
}
void findSwitch(int modality){
/* Sets new parsing modality, letting Arduino to be prepared to read properly data on serial */
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sc.setCurrParser(modality);
		sr.alertSwitch(modality);
	}
}
void prepareForNano(int modality){
	/* Sets modality for scanning distances, to send to Nano, not for pattern but for auto,follo,gamepad*/
	tellNano = true;
	modnano = modality; //nb the modality matters for starting to use oimi_nano1 readings of sonards ez1
	//in any case sonarloop is avoided with gamepad.. with pattern game single emotion??
	if(!modality) //if modality = 0 ..case of following for sonars ... scanProcess how to select scans methods
		scanProcess = 0;
	else
		scanProcess = 1;
}
/****************************************************************************************** Battery */
void showVoltage(){
/*
 Shows internal battery status
*/
	if (sc.getOkNewData()){
		sc.setOkNewData(false);
		power = volt.findVoltage();
		sr.printBatteryStatus(power);
	}
}
/****************************************************************************************** Wrappers */
void pidLoop(){
	oimiPid.PIDLoop();
}
void moveLoop(){
	mov.driveMe();
}
/****************************************************************************************** Sonars replies*/
void publishSonars(){
//	SonarLoop();
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.publishSonars(ez1.getSonarLeft(),ez1.getSonarFront(),ez1.getSonarRight(),ez1.getSonarBack());
	}
}
/****************************************************************************************** Movements */
void inputGamepad(){
/*
??
*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.replySpeeds(mov.getStrafe() * MAX_FORWARD_CONTROL,
					   mov.getForward() * MAX_FORWARD_CONTROL,
					   mov.getAngular() * MAX_ANGULAR_CONTROL);
	}
}
void sendPreciseSpeeds(){
	/*
	new for all kind of speeds...strafe, forward, angular same as gamepad...
	*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.replySpeeds(mov.getStrafe(), mov.getForward(), mov.getAngular());
	}
}
void sendCustomSpeeds(){
/*sending custom angle size and speed cancel??? speeds??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		press_index=1;
		sr.replySpeeds(mov.getCustomAngle(), mov.getCustomAngle(), mov.getCustomSpeed());
	}
}
void reportMovement(char* movetype){
/*??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		if(strcmp(movetype, "auto")==0){
			prepareForNano(1);
			letsStart = true;
			mov.setOkLeds(true);
			mov.setActualMovement(autonomous);
			sr.publishMovement(movetype);
		}
		else if(strcmp(movetype, "follo")==0){
			prepareForNano(0);
			letsStart = true;
			mov.setOkLeds(true);
			mov.setActualMovement(following);
			sr.publishMovement(movetype);
		}
		else if(strcmp(movetype, "control")==0){
			prepareForNano(3);
			letsStart = true;
			//mov.setOkLeds(false);
			Serial.print("<control_ok>");
			//sr.publishMovement(movetype);
		}
		else if(strcmp(movetype, "pattern")==0){
			prepareForNano(3);
			letsStart = true;
			mov.setOkLeds(false);
			sr.publishMovement(movetype);
		}
		else if(strcmp(movetype, "predefinite")==0){
			prepareForNano(4);
			letsStart = true;
			//very_first_loop_reaction = true;
			mov.setOkLeds(false);
			sr.publishMovement(movetype); //remove to print something else at deeper level
		}
		else if(strcmp(movetype, "closesonar")==0){
			prepareForNano(5);
			letsStart = true;
			//mov.setOkLeds(false);
			sr.publishMovement(movetype);
		}
	}
}
void reportStop(){
/*??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.printStop();
		//Serial.println();
		//Serial.write(13);
		//Serial.flush();
	}
}
void reportPause(){
/*??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.printPause();
		//delay(4000);
		mov.setIsEnded(true);
		//pauseArduino();
	}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Serial listener, creator of msgs */
void commandDispatcher(){
/*
* Select proper instruction ....
* Free at the end error
* real current currparser is in serialcomm library! lookatit
*/
	currParser = sc.getCurrParser();
	sc.acquireDataFromRaspi(currParser);
	bufferMsg = sc.getBufferMsg();

	if(strcmp(bufferMsg, "idle")==0){
		further_check = 0;
		sc.setJustChanged(1);
		//Serial.write(13);
		//Serial.println();
		//Serial.println();
	}
	
	else if(strcmp(bufferMsg, "switchMod0")==0){
		sc.setCurrParser(0);
		findSwitch(0);
	}
	else if(strcmp(bufferMsg, "switchMod1")==0){
		sc.setCurrParser(1);
		findSwitch(1);
	}
	else if(strcmp(bufferMsg, "switchMod2")==0){
		sc.setCurrParser(2);
		findSwitch(2);
	}
	else if(strcmp(bufferMsg, "switchMod3")==0){
		sc.setCurrParser(3);
		findSwitch(3);
	}
	else if(strcmp(bufferMsg, "pause")==0){
		reportPause();
	}
	else if(strcmp(bufferMsg, "stop")==0){
		mov.setOkLeds(false);
		mov.stopMoving();
		mov.setActualMovement(no_movement);
		//mov.setIsMoving(false); now it is just really!!
		mov.setIsReallyMoving(false);
		mov.setIsControlled(false);
		mov.setIsEnded(true);
		reportStop();
	}
	else if(strcmp(bufferMsg, "joystop")==0){
		mov.setIsControlled(false);
		inputGamepad();
		sc.setJustChanged(false);
		mov.setIsMoving(false);
		mov.setIsReallyMoving(false);
	}
	else if((strcmp(bufferMsg, "simplygo")==0) && (sc.getJustChanged() == true)){
		straSpeedOrder = sc.getFirstFloatVar();
		forwSpeedOrder = sc.getSecondFloatVar();
		anguSpeedOrder = sc.getThirdFloatVar();
		mov.setIsControlled(true);
		mov.setStrafeSpeed(straSpeedOrder);
		mov.setForwardSpeed(forwSpeedOrder);
		mov.setAngularSpeed(anguSpeedOrder);
		mov.setIsMoving(false); //no need to go in driveMe loop, just call pid controller with right info
		mov.setIsReallyMoving(false); //same as above 
		inputGamepad();
		sc.setJustChanged(false);
	}
	//why is necessary?? select start
	else if((strcmp(bufferMsg, "simplygo2")==0) && (sc.getJustChanged() == true)){
		straSpeedOrder = sc.getFirstFloatVar();
		forwSpeedOrder = sc.getSecondFloatVar();
		mov.setIsControlled(true);
		mov.setStrafeSpeed(straSpeedOrder);
		mov.setForwardSpeed(forwSpeedOrder);
		mov.setIsMoving(false);
		mov.setIsReallyMoving(false);
		inputGamepad();
		sc.setJustChanged(false);
	}
	else if((strcmp(bufferMsg, "simplygo")==0) && (sc.getJustChanged() == false)){
		mov.setIsControlled(true);
		inputGamepad();
	}
	else if((strcmp(bufferMsg, "simplygo2")==0) && (sc.getJustChanged() == false)){
		mov.setIsControlled(true);
		inputGamepad();
	}
	else if(strcmp(bufferMsg, "auto")==0){
		//if(just_happened == 0){
			mov.setIsMoving(true);
			mov.setIsReallyMoving(true);
			reportMovement(bufferMsg);
			just_happened = 1;
			//tellNano = true;
		//}
	}
	else if(strcmp(bufferMsg, "control")==0){
		if(just_happened == 0){
			mov.setIsMoving(false);
			mov.setIsReallyMoving(false);
			mov.setIsControlled(true);
			reportMovement(bufferMsg);
			just_happened = 1;
			//tellNano = true;
		}
	}
	else if(strcmp(bufferMsg, "follo")==0){
		if(just_happened == 0){
			mov.setIsMoving(true);
			mov.setIsReallyMoving(true);
			reportMovement(bufferMsg);
			just_happened = 1;
			//tellNano = true;
		}
	}
	else if(strcmp(bufferMsg, "pattern")==0){
		if(just_happened == 0){
			mov.setIsMoving(true);
			mov.setIsReallyMoving(true);
			reportMovement(bufferMsg);
			just_happened = 1;
			//tellNano = false; undo and put false???
		}
	}
	else if((strcmp(bufferMsg, "sadness_old")==0)){
		if(just_happened==0){
			//ledMsg = sc.getMsgSecond();
			//patternMsg = sc.getMsgThird();
			//mov.setIsMoving(true); //new here or there inside mov.actSad
			//forwSpeedOrder = sc.getFirstFloatVar();
			//anguSpeedOrder = sc.getSecondFloatVar();
			//mov.setCustomSpeed(forwSpeedOrder);
			//mov.setCustomAngle(anguSpeedOrder);
			//mov.setCurrentAnimation(patternMsg);
			//mov.setCurrentColor(ledMsg);
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			//reportMovement("pattern");
			mov.setActualMovement(sadness_old);
			
			just_happened = 1;
		}
	}
	else if((strcmp(bufferMsg, "anger_old")==0)){
		if(sc.getJustChanged() and not further_check){
		further_check = 1;
		//if(just_happened==0){
			//ledMsg = sc.getMsgSecond();
			//patternMsg = sc.getMsgThird();
			//mov.setIsMoving(true); //new here or there inside mov.actSad
			//forwSpeedOrder = sc.getFirstFloatVar();
			//anguSpeedOrder = sc.getSecondFloatVar();
			//mov.setCustomSpeed(forwSpeedOrder);
			//mov.setCustomAngle(anguSpeedOrder);
			//mov.setCurrentAnimation(patternMsg);
			//mov.setCurrentColor(ledMsg);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(anger_old);
			sc.setJustChanged(0);
		}
	}
	else if((strcmp(bufferMsg, "scare_old")==0)){
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {

		if(sc.getJustChanged() and not further_check){
		further_check = 1;
		//if(just_happened==0){
			//ledMsg = sc.getMsgSecond();
			//patternMsg = sc.getMsgThird();
			//mov.setIsMoving(true); //new here or there inside mov.actSad
			//forwSpeedOrder = sc.getFirstFloatVar();
			//anguSpeedOrder = sc.getSecondFloatVar();
			//mov.setCustomSpeed(forwSpeedOrder);
			//mov.setCustomAngle(anguSpeedOrder);
			//mov.setCurrentAnimation(patternMsg);
			//mov.setCurrentColor(ledMsg);
			
			reportMovement("predefinite"); 			
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(scare_old);
			sc.setJustChanged(0);
		}
	}
//}
else if((strcmp(bufferMsg, "enthu_old")==0)){
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
		further_check = 1;
		//if(just_happened==0){
			//ledMsg = sc.getMsgSecond();
			//patternMsg = sc.getMsgThird();
			//mov.setIsMoving(true); //new here or there inside mov.actSad
			//forwSpeedOrder = sc.getFirstFloatVar();
			//anguSpeedOrder = sc.getSecondFloatVar();
			//mov.setCustomSpeed(forwSpeedOrder);
			//mov.setCustomAngle(anguSpeedOrder);
			//mov.setCurrentAnimation(patternMsg);
			//mov.setCurrentColor(ledMsg);
			
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(enthusiasm_old); //NB not 'enthusiast'!
			sc.setJustChanged(0);
		}
	}		
	//else if(strcmp(bufferMsg, "happy")==0){ && (sc.getJustChanged() == true))
	else if(strcmp(bufferMsg, "happy_old")==0){ //always precedeed by a call of switchMod1!!
		//Serial.print(F("<entroinhappy>")); NO!!!!!!!
		if(just_happened==0){
			straSpeedOrder = sc.getFirstFloatVar();
			forwSpeedOrder = sc.getSecondFloatVar();
			anguSpeedOrder = sc.getThirdFloatVar();
			// already done in serial replies library no?? Serial.print(F("<straSpeedOrder: "));
			// already done in serial replies library no?? Serial.print(forwSpeedOrder);
			// already done in serial replies library no?? Serial.print(F(">"));
			// already done in serial replies library no?? Serial.print(F("<forwSpeedOrder: "));
			// already done in serial replies library no?? Serial.print(forwSpeedOrder);
			// already done in serial replies library no?? Serial.print(F(">"));
			// already done in serial replies library no?? Serial.print(F("<anguSpeedOrder: "));
			// already done in serial replies library no?? Serial.print(anguSpeedOrder);
			// already done in serial replies library no?? Serial.print(F(">"));
			mov.setIsControlled(true);
			isSingleCustom = true;
			mov.setStrafeSpeed(straSpeedOrder);
			mov.setForwardSpeed(forwSpeedOrder);
			mov.setAngularSpeed(anguSpeedOrder);
			mov.setIsMoving(false); //?? the movement library it is not used... not pidLoop and moveLoop to do!!! but directly holonomicMove!
			mov.setIsReallyMoving(false); //?? the movement library it is not used... not pidLoop and moveLoop to do!!! but directly holonomicMove!
			sendPreciseSpeeds();
			sc.setJustChanged(false); //??? the movement library it is not used...
			//mov.setActualMovement(happiness); not needed!!!! if mov library is not used!!! the robot is moved in the main loop direct movement with customs speeds!!
			just_happened = 1;
		}
	}
	else if((strcmp(bufferMsg, "normal_old")==0)){
		if(just_happened==0){
			forwSpeedOrder = sc.getFirstFloatVar();
			anguSpeedOrder = sc.getSecondFloatVar();
			ledMsg = sc.getMsgSecond();
			patternMsg = sc.getMsgThird();
			mov.setCustomSpeed(forwSpeedOrder);
			mov.setCustomAngle(anguSpeedOrder);
			mov.setCurrentAnimation(patternMsg);
			mov.setCurrentColor(ledMsg);
			sendCustomSpeeds();
			mov.lightUpLeds(ledMsg,patternMsg);
			mov.setActualMovement(normality);
			just_happened = 1;
		}
	}
	else if((strcmp(bufferMsg, "quiet_nothing")==0)){ //need switchmode0 ...but work with switchMode0
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			mov.setGroupPrompt(group);
			reportMovement("predefinite");
			mov.chooseQuietDisplay();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(attention);
			sc.setJustChanged(0);
		}
	}
	else if((strcmp(bufferMsg, "surpr")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
		further_check = 1;
		emo_level = sc.getFirstFloatVar();
		mov.setEmoLevel(emo_level);
		reportMovement("predefinite");
		mov.setupTranslateTime();
		mov.setJustCalled(1); //fundamental to stop running 
		mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
		mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
		mov.setActualMovement(surprise);
		sc.setJustChanged(0);
		}
	}		
	else if((strcmp(bufferMsg, "sad")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
		further_check = 1;
		emo_level = sc.getFirstFloatVar();
		mov.setEmoLevel(emo_level);
		reportMovement("predefinite");
		mov.setupTranslateTime();
		mov.setJustCalled(1); //fundamental to stop running 
		mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
		mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
		mov.setActualMovement(sadness);
		sc.setJustChanged(0);
		}
	}
	else if((strcmp(bufferMsg, "happy")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
		further_check = 1;
		emo_level = sc.getFirstFloatVar();
		mov.setEmoLevel(emo_level);
		reportMovement("predefinite");
		mov.setupTranslateTime();
		mov.setJustCalled(1); //fundamental to stop running 
		mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
		mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
		mov.setActualMovement(happiness);
		sc.setJustChanged(0);
		}
	}
	else if((strcmp(bufferMsg, "scare")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
		further_check = 1;
		emo_level = sc.getFirstFloatVar();
		mov.setEmoLevel(emo_level);
		reportMovement("predefinite");
		mov.setupTranslateTime();
		mov.setJustCalled(1); //fundamental to stop running 
		mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
		mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
		mov.setActualMovement(scare);
		sc.setJustChanged(0);
		}
	}	
	else if((strcmp(bufferMsg, "anger")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
		further_check = 1;
		emo_level = sc.getFirstFloatVar();
		mov.setEmoLevel(emo_level);
		reportMovement("predefinite");
		mov.setupTranslateTime();
		mov.setJustCalled(1); //fundamental to stop running 
		mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
		mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
		mov.setActualMovement(anger);
		sc.setJustChanged(0);
		}
	}		
	else if((strcmp(bufferMsg, "enthu")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		//if(sc.getJustChanged() and not further_check){
		if(sc.getJustChanged()){
			further_check = 1;
			emo_level = sc.getFirstFloatVar();
			mov.setEmoLevel(emo_level);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(no_movement);
			//mov.setActualMovement(enthusiasm);
			sc.setJustChanged(0);
		}
	}			

	else if((strcmp(bufferMsg, "reactst_pos")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			rea = sc.getFirstFloatVar();
			mov.setKindReactionPos(rea);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(reaction_pos);
			sc.setJustChanged(0);
		}
	}	
	else if((strcmp(bufferMsg, "reactst_neg")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			rea = sc.getFirstFloatVar();
			mov.setKindReactionNeg(rea);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(reaction_neg);
			sc.setJustChanged(0);
		}
	}		
	else if((strcmp(bufferMsg, "prompt_attention")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			group = sc.getFirstFloatVar();
			mov.setGroupPrompt(group);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(attention);
			sc.setJustChanged(0);
		}
	}	
	else if((strcmp(bufferMsg, "prompt_encourage")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			group = sc.getFirstFloatVar();
			mov.setGroupPrompt(group);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(encourage);
			sc.setJustChanged(0);
		}
	}	
	else if((strcmp(bufferMsg, "prompt_startgame")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			group = sc.getFirstFloatVar();
			mov.setGroupPrompt(group);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(greeting_start);
			sc.setJustChanged(0);
		}
	}	
	else if((strcmp(bufferMsg, "prompt_endgame")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			group = sc.getFirstFloatVar();
			mov.setGroupPrompt(group);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(greeting_end);
			sc.setJustChanged(0);
		}
	}	
	else if((strcmp(bufferMsg, "reward_right")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			extent = sc.getFirstFloatVar();
			mov.setExtentReward(extent);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(reward_positive);
			sc.setJustChanged(0);
		}
	}	
	else if((strcmp(bufferMsg, "reward_wrong")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			extent = sc.getFirstFloatVar();
			mov.setExtentReward(extent);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(reward_negative);
			sc.setJustChanged(0);
		}
	}
	else if((strcmp(bufferMsg, "reward_game")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			extent = sc.getFirstFloatVar();
			mov.setExtentReward(extent);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(reward_game);
			sc.setJustChanged(0);
		}
	}
	else if((strcmp(bufferMsg, "reward_match")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			extent = sc.getFirstFloatVar();
			mov.setExtentReward(extent);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(reward_match);
			sc.setJustChanged(0);
		}
	}
	else if((strcmp(bufferMsg, "reward_session")==0)){ //need switchmode1 ...it works even if the number of elements that SerialComm::catchFromSerial are more than 1 in <switchmod1>
		//if(very_first_loop_reaction){		// when command = predefinite pattern of movement for reaction stage ... 
		//											//it should be do nothing for 1 loop, ...no sonar detected here!! cannot move if it is not set to false
		//	reportMovement("predefinite");
		//	very_first_loop_reaction=false;
		//}
		//else {
		if(sc.getJustChanged() and not further_check){
			further_check = 1;
			extent = sc.getFirstFloatVar();
			mov.setExtentReward(extent);
			reportMovement("predefinite");
			mov.setupTranslateTime();
			mov.setJustCalled(1); //fundamental to stop running 
			mov.setIsMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setIsReallyMoving(true); //in theory is already done in movements library!!! check if works without it
			mov.setActualMovement(reward_session);
			sc.setJustChanged(0);
		}
	}		
	/*
	else if((strcmp(bufferMsg, "enthusiast")==0)){
		if(just_happened==0){
			forwSpeedOrder = sc.getFirstFloatVar();
			anguSpeedOrder = sc.getSecondFloatVar();
			ledMsg = sc.getMsgSecond();
			patternMsg = sc.getMsgThird();
			mov.setCustomSpeed(forwSpeedOrder);
			mov.setCustomAngle(anguSpeedOrder);
			mov.setCurrentAnimation(patternMsg);
			mov.setCurrentColor(ledMsg);
			sendCustomSpeeds();
			mov.lightUpLeds(ledMsg,patternMsg);
			mov.setActualMovement(enthusiasm);
			just_happened = 1;
		}
	}
	*/
	else if(strcmp(bufferMsg, "reset")==0)			 reportReset();
	else if(strcmp(bufferMsg, "volt")==0)		    showVoltage();

}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Main */
void setup(){
	// Serial
	Serial.begin(115200);				  	// open serial port, establishing the baud data rate in bits per second [bps] for data transmission
	Serial.print(F("<Mega is ready>"));	// communicates immediately to raspi for first time with a short msg
	Serial.flush();							//...waits data to be transmitted completely. Rather than discarding the incoming data it allows it to wait
													//...so once the data inside the buffer is transmitted completely then the Serial buffer can receive the new data.
													//...open software serial port, establishing the baud
	sonarSerial.begin(9600);			  	
	// Leds
	strip.begin();			    			// setups ledstrip for working correctly
	strip.show();			    			// turns off ledstrip pixels, starts to send data
	strip.setBrightness(200);   			// tunes ledstrip brightness (max is 255) to remove!!!
	// Indices
	press_index = 1;
	mpx_index 	= 0;
	currParser 	= 0;
	// Timers
	colorDelay = 0;
	resetDelay = 0;
	replyinterval = 50; //ms
	startAllTime  = millis();
	// Flags
	isStarted = true;
	// Movements
	last_movement = no_movement;
	forwSpeedOrder  = 0.0;
	straSpeedOrder = 0.0;
	anguSpeedOrder  = 0.0;
	mov.setupMovement();
	timeSetup = 0;
	further_check = 0; 								// to enter in body of function only one time ---> not only just_happened
	//tellNano = true; 						//could be useful in case: set to true at the beginning for testing procedure of movement without rasPi
	//modnano = 0;	
	very_first_loop_reaction = true;
}

void loop(){
/*
ledstart non serve per far partire dritto da subito...
*/
//	emptyArduino();
	loopTime = millis();		 			// first start time useful in game modality
	commandDispatcher();         		// listens for new command at every loop is always prepared to receive from raspi
	if(tellNano){							// afterward a command of changing scan modality is received from raspi
		sonarSerial.print(modnano);	// sends to Nano sonars modality
		sonarSerial.print('\n');		// sends last character
		sonarSerial.flush();				// waits for the transmission of outgoing serial data to complete
		tellNano = false;					// sets flag to false for don't disturbing Nano loop for no reason
		toldNano = true;
	}
	//if((!very_first_loop_reaction) && (toldNano)){
	//	sonarLoop();						// receives data from Nano and performe sonars operations
	//	toldNano = false;
	//}
	
	if(mov.getIsMoving() && mov.getIsReallyMoving()){ // only if robot is ready to move
	//		if(millis() > timeSetup+3000){
				entered_in_move = true;
				sonarLoop();		// receives data from Nano and performe sonars operations
				pidLoop();			// activates PID (Proportional–Integral–Derivative) controller
				moveLoop();       // performs the correct movement
			}
	else if(mov.getIsMoving() && !mov.getIsReallyMoving()){ // only if robot is moving
	//		if(millis() > timeSetup+3000){
				entered_in_move = true;
				sonarLoop();		// receives data from Nano and performe sonars operations
				moveLoop();       // performs the correct movement
			}

	

	//		else sonarLoop();
	

	//	}
		else{ //no move pre-established pattern, just direct order giving the speeds
			if(mov.getIsControlled()){	/* only when robot is in manual config instead of go through methods of Movem library...*/
				if(isSingleCustom){
					oimiPid.holonomicRun(mov.getStrafe(), mov.getForward(), mov.getAngular()); // navigates with current speeds
				}
				else{
					oimiPid.holonomicRun(mov.getStrafe() * MAX_FORWARD_CONTROL,
					mov.getForward() * MAX_FORWARD_CONTROL,
					mov.getAngular() * MAX_ANGULAR_CONTROL); 					
				}
					pidLoop();		    								// starts/continues Pid controller cycle
				}
			else
				oimiPid.simpleStop();  										// stops without resetting Iterm
				//pidLoop(); 													// N.B error to put here!!

				//oimiPid.holonomicRun(mov.getStrafe() * MAX_FORWARD_CONTROL,
				//mov.getForward() * MAX_FORWARD_CONTROL,
				//mov.getAngular() * MAX_ANGULAR_CONTROL);  					// navigates with current speeds
				//	pidLoop();														// starts/continues Pid controller cycle
				//}
				//else
				//	oimiPid.simpleStop();  											// stops without resetting Iterm
				//	//pidLoop(); 													// N.B error to put here!!
		}
		just_happened = 0;                                          //the reaction is ended...just_happended is reset
		if(mov.getIsEnded() && (!mov.getIsMoving()) && entered_in_move){  							// after one routine/task is complete..
			reportMovement("closesonar");
			entered_in_move = false;
	}
}
	/*
	if(mov.getIsEnded() && (!mov.getIsMoving())){  							// after one routine/task is complete..
		reportMovement("closesonar");
		if(sr.getMandato())                  //fondamentale che prima ritorni il messaggio al raspi senno va in pausa prima 
			pauseArduino();						//e nn risponde e tutto si blocca!! quindi pause funziona benissimo
		//if(forpause==5){
		//	forpause = 0;
													// ..leaves Arduino in pause waiting for next thing to do
		//}
		//else
		//	forpause++;
		//isReady = false;													// necessary resets
	}
	*/