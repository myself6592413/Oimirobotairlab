import sys

class TimeOutException(Exception):
	print("TimeOutException!")

class StopAllException(Exception):
	print("simpleStopALLException!")

class StopAllException1(Exception):
	print("simpleStopALLException1!")

class StopAllException2(Exception):
	def __init__(self, *args):
		if args:
			self.message = args[0]
		else:
			self.message = None
	
	def __str__(self):
		print('calling str')
		if self.message:
			return 'StopAllException, {0} '.format(self.message)
		else:
			return 'StopAllException  has been raised'
	
def timeHandler(signum, frame):
	raise TimeOutException

def sigterm_handler(elem, _signo, _stack_frame):
	#funz9(0)
	#print("SIGTERM ARRIVED!!")
	raise StopAllException #raise qui è fondamentale  per far fermare bene tutto
	#sys.exit()

def sigint_handler(elem, _signo, _stack_frame):
	#funz9(0)
	#print("SIGTERM ARRIVED!!") #nn mettere nulla qui è meglio eccezione reetrant
	#raise StopAllException1 #raise qui è fondamentale  per far fermare bene tutto
	print("quitting, you pressed crtl-c! ")
	sys.exit()
