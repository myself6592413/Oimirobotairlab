
#cython: language_level=3

# ===============================================================================================================
#    Imports:
# ===============================================================================================================
import sys
import os 
#BASE_DIR = os.path.abspath(os.getcwd())
#print("BASE_DIR ", BASE_DIR)
#sounds_folder = os.path.join(BASE_DIR, '/interacting/sound_playback')
#print("sounds_folder ", sounds_folder)
#if '/home/pi/OIMI/oimi_code/src/interacting/sound_playback/' not in sys.path:
    #sys.path.append(sounds_folder)
#sys.path.append('/home/pi/OIMI/oimi_code/src/interacting/sound_playback/')
if '/home/pi/OIMI/oimi_code/src/shared/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/shared/')

if '/home/pi/OIMI/oimi_code/src/moving/joypad_navigation' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/moving/joypad_navigation/')
    
import serial
import time
from datetime import datetime, timedelta
import random 
import struct
import math
import signal
import sounds_playback as sp
import custom_exceptions as ce
import serial_manager as ser
import shared as sh
from libc.stdlib cimport malloc, free


import asyncio
from signal import SIGINT, SIGTERM
import joypad_controller as jc
# ===============================================================================================================
#    Elements:
# ===============================================================================================================
#def time_handler(signum, frame):
#    raise ce.TimeOutException

signal.signal(signal.SIGALRM, ce.time_handler)
signal.alarm(10005)

def fun55(al):
    print("enter in fun55")
    print(al*7)    

# ===============================================================================================================
#    Methods:
# ===============================================================================================================    
cdef move_pattern_oimi(global_status, stopInterrupt, move_type, primary_lock, queue_lock):
    # non devo mandargli led ma fa tutto lui da solo con arduino nano ??? no gli mando il c ...per settare colora in arduino senno non sarebbe settato, 
    # mi serve in loop arduino!!! quindi si gli manda il comando  ma solo una volta 
    # per adesso nn gli mando startmoving order con pattern (anche perchè non sarebbe pronto a riverlo arduino com msg) 
    # e non aspetto che mi mandi il "gopattern come gli altri tipi di movimenti"
    # se va tutto bene si ferma da solo non c'è bisogno di mandargli stop ...quindi forse finally va cambiato
    cdef:
        char* lightThis = b''
        bint cont = False    
        bint interrupted  = False    
        #char* validation
     #--sounds depends on movetype simply
    serMega, serNano, serNano2 = ser.setup_serial() 
    serMega.await_arduinoMega()
    serNano.await_arduinoNano1()
    serNano2.await_arduinoNano2()
    #serMega.startMoving_order("pattern")
    #pyvalidation = serNano.pickup_fromNano()
    #validation = pyvalidation
    #print("ecco la validazione del PATTERN!!!! {}".format(validation))
    print("ok ready to start pattern!")
    queue_lock.clear()
    primary_lock.acquire()
    queue_lock.put(True)
    try:
        while True:
            if not stopInterrupt.is_set():
                if cont is False:
                    serMega.startMoving_order(move_type)
                    playThis = sp.sounds_dict.get(move_type,"This sound doesn't exists!")
                    print("ìììììììììììììììììììììììììsiamo qui in movement patterni e playthis is{}".format(playThis))
                    lightthis = sh.led_dict.get(move_type,"This led pattern doesn't exists!")
                    #playThis = playthis
                    lightThis = lightthis
                    serNano.send_led(lightThis)
                    serNano2.send_led(lightThis)
                    sp.play_audio(playThis)
                    cont = True
                    #else:
                    #    print("sono in else e mi tocca attendere finished")
                    #    #serMega.await_end()
                    #    #finished = True
                    #    # non c'è stato uno stop vero e proprio quindi devo cambiare anche qui global status
                    #    #status = sh.PyGlobalStatus(b'doing_nothing')
                    #    #print("status is {}".format(status.get_globalstatus()))
                    #    #global_status.put(status)
                    #    #break
            else:
                interrupted = True
                raise ce.MovementsError
    except ce.MovementsError:
        print("errore orrore")
    finally:
        primary_lock.release()
        queue_lock.put(False)
        #serMega.ser.reset_input_buffer()                    
        #serMega.ser.reset_output_buffer()            
        if interrupted == True:
            serMega.send_stop()
        else:
            status = sh.PyGlobalStatus(b'doing_nothing')
            print("status is {}".format(status.get_globalstatus()))
            global_status.put(status)
        lightthis = sh.led_dict.get("noled","This led pattern doesn't exists!")
        lightThis = lightthis
        serNano.send_led(lightThis)
        serMega.send_pause()
        serMega.close()
        serNano.close()
        print("FINALMENTE 2!!")
    '''try:
        while True:
            if not stopInterrupt.is_set():
                if cont is False:
                    serMega.startMoving_order(move_type)
                    playThis = sounds.sounds_dict.get(move_type,"This sound doesn't exists!")
                    print("ìììììììììììììììììììììììììsiamo qui in movement patterni e playthis is{}".format(playThis))
                    lightthis = sh.led_dict.get(move_type,"This led pattern doesn't exists!")
                    #playThis = playthis
                    lightThis = lightthis
                    serNano.send_led(lightThis)
                    sounds.play_audio(playThis)
                    cont = True
                else:
                    print("sono in else e mi tocca attendere finished")
                    serMega.await_end()
                    finished = True
                    # non c'è stato uno stop vero e proprio quindi devo cambiare anche qui global status
                    status = sh.PyGlobalStatus(b'doing_nothing')
                    print("status is {}".format(status.get_globalstatus()))
                    global_status.put(status)
                    break
            else:
                raise ce.MovementsError
    except ce.MovementsError:
        print("errore orrore")
    finally:
        primary_lock.release()
        queue_lock.put(False)
        serMega.ser.reset_input_buffer()                    
        serMega.ser.reset_output_buffer()            
        if finished == False:
            serMega.send_stop()
        lightthis = sh.led_dict.get("noled","This led pattern doesn't exists!")
        lightThis = lightthis
        serNano.send_led(lightThis)
        serMega.send_pause()
        serMega.close()
        serNano.close()
        print("FINALMENTE 2!!")'''
        
cdef move_robot_oimi(stopInterrupt, move_type, primary_lock, queue_lock): #autonomous????
    #sounds at start depends on modality
    #sounds depends on what happens
    before_time = datetime.now()
    cdef:
        char* feedback
        char* validation
        char* old_feedback = b'old'
        int enableAudio = 0
    serMega, serNano, serNano2 = ser.setup_serial()
    ##signal.signal(signal.SIGALRM, partial(timeHandler1))
    serMega.await_arduinoMega()
    serNano.await_arduinoNano1()
    print("ok ready to start!")
    serMega.startMoving_order(move_type)
    print("Movetype to pass to dict {}".format(move_type))
    playthis = sp.sounds_dict.get(move_type,"This sound doesn't exist!")
    cdef char * playThis = playthis
    cdef char * speech
    print("playThis1111 is {}".format(playthis))
    print("playThis2222 is {}".format(playThis))
    print("LOCK ACQUIRED")
    queue_lock.clear()
    primary_lock.acquire()
    queue_lock.put(True)
    #led and audio of autonomous/following
    #take goauto/go_follo
    pyvalidation = serNano.pickup_fromNano1()
    validation = pyvalidation
    print("ecco la validazione!!!! {}".format(validation))
    lightThis = sh.led_dict.get(move_type,"This led pattern doesn't exists!")
    print("lightThis is {}".format(lightThis))
    tipella = type(lightThis)
    print("tipo di lightTThis è {}".format(tipella))
    if lightThis.decode() == '<u>':
        print("uuuuuuuuu uuuuuuuuuuuuu uuuuuuuuuuuuu non mando niente da illuminare")
    else:
        serNano.send_led(lightThis)
    sp.play_audio(playThis)
    #start moving and play audio / led according to movement
    try:
        while True:
            print("---------------------------------------------------------------------------- NAVIGATION VERO INIZIO!!!")
            current_time = datetime.now()
            if current_time > before_time+timedelta(seconds=6000):
                before_time = datetime.now()
                raise ce.TimeElapsed('Time elapsed')
            if not stopInterrupt.is_set():
                try:
                    print("---> navigation inizio : enter in try bellali:")
                    a = queue_lock.get()
                    print("in this try queulock value is {}".format(a))
                    queue_lock.put(a)
                    pyfeedback = serMega.pickup_fromMega()
                    feedback = pyfeedback
                    print(" navigation: serialMega coferma is {}".format(feedback))
                    while True:
                        try:
                            if feedback is not None:
                                if feedback != b'initial' and feedback != b'notFound' and feedback != old_feedback:
                                #if feedback != b'initial' and feedback != old_feedback:
                                    tipo = type(feedback)
                                    print("siamo in while stdnavigation e feedback is {}".format(feedback))
                                    print("siamo sempre qui e tipo feedback is {}".format(tipo))
                                    lightThis = sh.led_dict.get(feedback.decode(),"This led pattern doesn't exists!")
                                    print("!!!lightthis is    {} ".format(lightThis))
                                    serNano.send_led(lightThis)
                                    old_feedback = feedback
                                    print("il vecchio feeedback in caso normaleè !!! ", old_feedback)
                                    res = sp.sounds_dict.get(feedback.decode(),"This sound doesnt exist!")
                                    print("feeback audio inside while {}".format(feedback))
                                    speech = res
                                    if enableAudio==5:
                                        print("enable == 5 ok")
                                        print("speech name speech {} ".format(speech))
                                        print("speech name playthis {} ".format(playThis))
                                        sp.play_audio(speech)
                                        enableAudio=0
                                        break
                                    else:
                                        print("enamble != 5 ko")
                                        enableAudio+=1
                                        print("speech name speech {} ".format(speech))
                                        print("speech name playthis {} ".format(playThis))
                                        break
                                elif feedback == b'notFound' or feedback == old_feedback:
                                    if feedback == old_feedback:
                                        print("TI HO SGAMATO, FEEDBACK == OLD!")
                                    print("exit from navigation true inner cause feedback = not found")
                                    #enableAudio+=1 #??? pensaci
                                    old_feedback = feedback
                                    print("il vecchio feeedback in caso di notFound è !!! ", old_feedback)    
                                    break
                        except ce.TimeOutException:
                            continue
                        else:
                            signal.alarm(0)
                except Exception as msg:
                    print("Eccezione during movement occurred!!!")
                    print(msg)
            else:
                print("7777777777777777777777777777777777777777777777777777 stop interrupt is set!!")
                #serMega.ser.reset_input_buffer()                    
                #break
                print("raise eccezione movement!")
                raise ce.MovementsError
    except ce.TimeElapsed:
        print("time elapsed!!! 13212rewrtertert345678743t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    except ce.MovementsError:
        fun55(1) #gli faccio far qualcosa qui
        print("dio caro!!!!!")
    finally:
        print("enter in finally!!!")
        primary_lock.release()
        queue_lock.put(False)
        #serMega.ser.reset_input_buffer()
        #serMega.ser.reset_output_buffer()
        serMega.send_stop()
        lightThis = sh.led_dict.get("noled","This led pattern doesn't exists!")
        print("lightThis finally is {} ".format(lightThis))
        serNano.send_led(lightThis)
        serMega.close()
        serNano.close()
        print("FINALMENTEEE")
        
cdef move_robot_oimi2(stopInterrupt, serMega, move_type, primary_lock, queue_lock):
    #sounds at start depends on modality
    #sounds depends on what happens
    before_time = datetime.now()
    cdef:
        char* feedback
        char* validation
        char* old_feedback = b'old'
        int enableAudio = 0
    #serMega, serNano, serNano2 = ser.setup_serial()
    ##signal.signal(signal.SIGALRM, partial(timeHandler1))
    #serMega.await_arduinoMega()
    #serNano.await_arduinoNano1()
    #serNano2.await_arduinoNano2()
    #lightthis = b'<hh>'
    #playThis = playthis
    #serNano2.send_led(lightthis)
    #print("ok ready to start!")
    print("Movetype to pass to dict {}".format(move_type))
    print("LOCK ACQUIRED")
    queue_lock.clear()
    primary_lock.acquire()
    queue_lock.put(True)
    serMega.startMoving_order(move_type)
    serMega.await_end()
    serMega.transmit_with_no_answer(b'<idle>')
    serMega.flushInput()
    serMega.flushOutput()
    #led and audio of autonomous/following
    #playThis = sp.sounds_dict.get("no","This sound doesn't exists!")
    #print("ìììììììììììììììììììììììììsiamo qui in movement patterni e playthis is {}".format(playThis))
    #sp.play_audio(playThis)       
    #print("enter in finally!!!")
    primary_lock.release()
    queue_lock.put(False)
    #serMega.ser.reset_input_buffer()
    #serMega.ser.reset_output_buffer()
    time.sleep(3)
    serMega.send_stop()
    serMega.flushInput()
    serMega.flushOutput()
    serMega.transmit_with_no_answer(b'<idle>')
    #lightThis = sh.led_dict.get("noled","This led pattern doesn't exists!")
    #print("lightThis finally is {} ".format(lightThis))
    #serNano.send_led(lightThis)
    #serMega.close()
    print("FINALMENTEEE si muove!!!!!")

cdef move_robot_oimi22(stopInterrupt, serMega, move_type, primary_lock, queue_lock):
    #sounds at start depends on modality
    #sounds depends on what happens
    before_time = datetime.now()
    cdef:
        char* feedback
        char* validation
        char* old_feedback = b'old'
        int enableAudio = 0
    #serMega, serNano, serNano2 = ser.setup_serial()
    ##signal.signal(signal.SIGALRM, partial(timeHandler1))
    #serMega.await_arduinoMega()
    #serNano.await_arduinoNano1()
    #serNano2.await_arduinoNano2()
    #lightthis = b'<hh>'
    #playThis = playthis
    #serNano2.send_led(lightthis)
    #print("ok ready to start!")
    print("Movetype to pass to dict {}".format(move_type))
    print("LOCK ACQUIRED")
    queue_lock.clear()
    primary_lock.acquire()
    queue_lock.put(True)
    serMega.startMoving_order(move_type)
    serMega.await_end()
    serMega.transmit_with_no_answer(b'<idle>')
    serMega.flushInput()
    serMega.flushOutput()
    #led and audio of autonomous/following
    #playThis = sp.sounds_dict.get("no","This sound doesn't exists!")
    #print("ìììììììììììììììììììììììììsiamo qui in movement patterni e playthis is {}".format(playThis))
    #sp.play_audio(playThis)       
    #print("enter in finally!!!")
    primary_lock.release()
    queue_lock.put(False)
    #serMega.ser.reset_input_buffer()
    #serMega.ser.reset_output_buffer()
    
    #time.sleep(3)

    #serMega.send_stop()
    serMega.flushInput()
    serMega.flushOutput()
    serMega.transmit_with_no_answer(b'<idle>')
    #lightThis = sh.led_dict.get("noled","This led pattern doesn't exists!")
    #print("lightThis finally is {} ".format(lightThis))
    #serNano.send_led(lightThis)
    #serMega.close()
    print("FINALMENTEEE si muove!!!!!")
        
cdef move_robot_oimi3(stopInterrupt, serMega, move_type, primary_lock, queue_lock, stra_speed, for_speed, ang_speed):
    #sounds at start depends on modality
    #sounds depends on what happens
    before_time = datetime.now()
    cdef:
        char* feedback
        char* validation
        char* old_feedback = b'old'
        int enableAudio = 0
    #serMega, serNano, serNano2 = ser.setup_serial()
    ##signal.signal(signal.SIGALRM, partial(timeHandler1))
    #serMega.await_arduinoMega()
    #serNano.await_arduinoNano1()
    #serNano2.await_arduinoNano2()
    #lightthis = b'<hh>'
    #playThis = playthis
    #serNano2.send_led(lightthis)
    #print("ok ready to start!")
    print("Movetype to pass to dict {}".format(move_type))
    print("LOCK ACQUIRED")
    queue_lock.clear()
    primary_lock.acquire()
    queue_lock.put(True)
    switchalert = b'<switchMod1>'
    serMega.transmit(switchalert)
    serMega.react_to_game(move_type, stra_speed, for_speed, ang_speed)
    #led and audio of autonomous/following
    #playThis = sp.sounds_dict.get("no","This sound doesn't exists!")
    #print("ìììììììììììììììììììììììììsiamo qui in movement patterni e playthis is {}".format(playThis))
    #sp.play_audio(playThis)       
    #print("enter in finally!!!")
    primary_lock.release()
    queue_lock.put(False)
    time.sleep(1)
    #serMega.ser.reset_input_buffer()
    #serMega.ser.reset_output_buffer()
    serMega.send_stop()
    #lightThis = sh.led_dict.get("noled","This led pattern doesn't exists!")
    #print("lightThis finally is {} ".format(lightThis))
    #serNano.send_led(lightThis)
    #serMega.close()
    print("FINALMENTEEE si muove!!!!!")



cdef move_robot_oimi4(stopInterrupt, serMega, move_type, primary_lock, queue_lock):
    before_time = datetime.now()
    cdef:
        char* feedback
        char* validation
        char* old_feedback = b'old'
        int enableAudio = 0
    #serMega, serNano, serNano2 = ser.setup_serial()
    ##signal.signal(signal.SIGALRM, partial(timeHandler1))
    #serMega.await_arduinoMega()
    #serNano.await_arduinoNano1()
    #serNano2.await_arduinoNano2()
    #lightthis = b'<hh>'
    #playThis = playthis
    #serNano2.send_led(lightthis)
    #print("ok ready to start!")
    print("Movetype to pass to dict {}".format(move_type))
    print("LOCK ACQUIRED")
    queue_lock.clear()
    primary_lock.acquire()
    queue_lock.put(True)
    switchalert = b'<switchMod1>'
    serMega.transmit(switchalert)
    serMega.startMoving_order(move_type)
    serMega.await_end()
    serMega.transmit_with_no_answer(b'<idle>')
    serMega.flushInput()
    serMega.flushOutput()
    #led and audio of autonomous/following
    #playThis = sp.sounds_dict.get("no","This sound doesn't exists!")
    #print("ìììììììììììììììììììììììììsiamo qui in movement patterni e playthis is {}".format(playThis))
    #sp.play_audio(playThis)       
    #print("enter in finally!!!")
    primary_lock.release()
    queue_lock.put(False)
    #serMega.ser.reset_input_buffer()
    #serMega.ser.reset_output_buffer()
    
    #time.sleep(3)

    #serMega.send_stop()
    serMega.flushInput()
    serMega.flushOutput()
    serMega.transmit_with_no_answer(b'<idle>')
    switchalert = b'<switchMod0>'
    serMega.transmit(switchalert)  
    #lightThis = sh.led_dict.get("noled","This led pattern doesn't exists!")
    #print("lightThis finally is {} ".format(lightThis))
    #serNano.send_led(lightThis)
    #serMega.close()
    print("FINALMENTEEE si muove!!!!!")


cdef move_robot_oimi_auto_free_game(stopInterrupt, move_type, primary_lock, queue_lock, serMega, serNano, serNano2): 
    #sounds at start depends on modality are not managed from extern but directly here since lights depends on movements 
    #sounds depends on what happens
    before_time = datetime.now()
    cdef:
        char* feedback
        char* validation
        char* old_feedback = b'old'
        int enableAudio = 0
    ###serMega, serNano, serNano2 = ser.setup_serial()
    ##signal.signal(signal.SIGALRM, partial(timeHandler1))
    print("ok ready to start autonomous!")
    serMega.startMoving_order(move_type)
    #serMega.flushInput()
    #serMega.flushOutput()    
    print("Movetype to pass to dict {}".format(move_type))
    playthis = sp.sounds_dict.get(move_type,"This sound doesn't exist!")
    cdef char * playThis = playthis
    cdef char * speech
    print("playThis1111 is {}".format(playthis))
    print("playThis2222 is {}".format(playThis))
    print("LOCK ACQUIRED")
    queue_lock.clear()
    primary_lock.acquire()
    queue_lock.put(True)


    print("Movetype to pass to dict {}".format(move_type))
    ##switchmode?? /pickup?
    #led and audio of autonomous/following
    pyvalidation = serNano.pickup_fromNano1()
    validation = pyvalidation
    print("ecco la validazione!!!! {}".format(validation))
    lightThis = sh.led_dict.get(move_type,"This led pattern doesn't exists!")
    print("lightThis is {}".format(lightThis))
    tipella = type(lightThis)
    print("tipo di lightTThis è {}".format(tipella))
    if lightThis.decode() == '<u>':
        print("uuuuuuuuu uuuuuuuuuuuuu uuuuuuuuuuuuu non mando niente da illuminare")
    else:
        serNano.send_led(lightThis)
    sp.play_audio(playThis)
    #time.sleep(2)
    #lightThis = sh.led_dict.get("nopat","This led pattern doesn't exists!")
    #start moving and play audio / led according to movement
    try:
        while True:
            print("---------------------------------------------------------------------------- NAVIGATION VERO INIZIO!!!")
            current_time = datetime.now()
            if current_time > before_time+timedelta(seconds=6000):
                before_time = datetime.now()
                raise ce.TimeElapsed('Time elapsed')
            if not stopInterrupt.is_set():
                try:
                    print("---> navigation inizio : enter in try bellali:")
                    a = queue_lock.get()
                    print("in this try queulock value is {}".format(a))
                    queue_lock.put(a)
                    pyfeedback = serMega.pickup_fromMega()
                    feedback = pyfeedback
                    print(" navigation: serialMega coferma is {}".format(feedback))
                    while True:
                        try:
                            if feedback is not None:
                                if feedback != b'initial' and feedback != b'notFound' and feedback != old_feedback:
                                #if feedback != b'initial' and feedback != old_feedback:
                                    tipo = type(feedback)
                                    print("siamo in while stdnavigation e feedback is {}".format(feedback))
                                    print("siamo sempre qui e tipo feedback is {}".format(tipo))
                                    lightThis = sh.led_dict.get(feedback.decode(),"This led pattern doesn't exists!")
                                    print("!!!lightthis is    {} ".format(lightThis))
                                    serNano.send_led_anim_moving(lightThis)
                                    serNano2.send_led_anim_moving(lightThis)

                                    old_feedback = feedback
                                    print("il vecchio feeedback in caso normaleè !!! ", old_feedback)
                                    res = sp.sounds_dict.get(feedback.decode(),"This sound doesnt exist!")
                                    print("feeback audio inside while {}".format(feedback))
                                    speech = res
                                    if enableAudio==5:
                                        print("enable == 5 ok")
                                        print("speech name speech {} ".format(speech))
                                        print("speech name playthis {} ".format(playThis))
                                        sp.play_audio(speech)
                                        enableAudio=0
                                        break
                                    else:
                                        print("enamble != 5 ko")
                                        enableAudio+=1
                                        print("speech name speech {} ".format(speech))
                                        print("speech name playthis {} ".format(playThis))
                                        break
                                elif feedback == b'notFound' or feedback == old_feedback:
                                    if feedback == old_feedback:
                                        print("TI HO SGAMATO, FEEDBACK == OLD!")
                                    print("exit from navigation true inner cause feedback = not found")
                                    #enableAudio+=1 #??? pensaci
                                    old_feedback = feedback
                                    print("il vecchio feeedback in caso di notFound è !!! ", old_feedback)    
                                    break
                        except ce.TimeOutException:
                            continue
                        else:
                            signal.alarm(0)
                except Exception as msg:
                    print("Eccezione during movement occurred!!!")
                    print(msg)
            else:
                print("7777777777777777777777777777777777777777777777777777 stop interrupt is set!!")
                #serMega.ser.reset_input_buffer()                    
                #break
                print("raise eccezione movement!")
                raise ce.MovementsError
    except ce.TimeElapsed:
        print("time elapsed!!! 13212rewrtertert345678743t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    except ce.MovementsError:
        fun55(1) #gli faccio far qualcosa qui
        print("dio caro!!!!!")
    finally:
        print("enter in finally!!!")
        primary_lock.release()
        queue_lock.put(False)
        #serMega.ser.reset_input_buffer()
        #serMega.ser.reset_output_buffer()
        serMega.send_stop()
        lightThis = sh.led_dict.get("noled","This led pattern doesn't exists!")
        print("lightThis finally is {} ".format(lightThis))
        serNano.send_led(lightThis)
        serMega.close()
        serNano.close()
        print("FINALMENTEEE")        

cdef move_robot_oimi_joypad(global_status, stopInterrupt, move_type, primary_lock, queue_lock, serMega, serNano, serNano2): 
    before_time = datetime.now()
    cdef:
        char* feedback
        char* validation
        char* old_feedback = b'old'
        int enableAudio = 0
    ###serMega, serNano, serNano2 = ser.setup_serial()
    ##signal.signal(signal.SIGALRM, partial(timeHandler1))
    print("ok ready to start autonomous!")
    #serMega.startMoving_order(move_type)
    #serMega.flushInput()
    #serMega.flushOutput()
    playthis = sp.sounds_dict.get('joypad',"This sound doesn't exist!")
    cdef char * playThis = playthis
    cdef char * speech
    print("playThis1111 is {}".format(playthis))
    print("playThis2222 is {}".format(playThis))
    print("LOCK ACQUIRED")
    queue_lock.clear()
    primary_lock.acquire()
    queue_lock.put(True)
    ##switchmode?? /pickup?
    #led and audio of autonomous/following
    #pyvalidation = serNano.pickup_fromNano1() useless ?? there is already the Mega that tells to the nano how to direct its loop
    #validation = pyvalidation
    #print("ecco la validazione!!!! {}".format(validation))
    lightThis = sh.led_dict.get("joystick","This led pattern doesn't exists!")
    print("lightThis is {}".format(lightThis))
    switchalert = b'<switchMod1>'
    serNano.transmit(switchalert)
    serNano.send_led_anim(b'<moodexcit>')
    sp.play_audio(playThis)
    #time.sleep(2)
    #lightThis = sh.led_dict.get("nopat","This led pattern doesn't exists!")
    #start moving and play audio / led according to movement
    try:
        while True:
            print("--------------------------------------------------- NAVIGATION REAL BEGINNING")
            current_time = datetime.now()
            if current_time > before_time+timedelta(seconds=6000):
                before_time = datetime.now()
                raise ce.TimeElapsed('Time elapsed')
            if not stopInterrupt.is_set():
            #try:
                print("---> navigation start : enter in try bellali:")
                a = queue_lock.get()
                print("in this try queulock value is {}".format(a))
                queue_lock.put(a)
                #pyfeedback = serMega.pickup_fromMega() #finally take "Controlled enable msg" (no control)
                #feedback = pyfeedback
                #print(" navigation: serialMega coferma is {}".format(feedback))
                loop = asyncio.get_event_loop()
                main_task = asyncio.ensure_future(main_gamepad(stopInterrupt, primary_lock, queue_lock, global_status, serMega, serNano, serNano2))

                #loop.add_signal_handler(SIGINT, ctrlc)
                loop.add_signal_handler(SIGINT, main_task.cancel)
                loop.add_signal_handler(SIGTERM, main_task.cancel)
                try:
                    loop.run_until_complete(main_task)
                finally:
                    loop.close()
                    quit()
            #    except Exception as msg:
            #        print("Eccezione during movement occurred!!!")
            #        print(msg)
            else:
                print("7777777777777777777777777777777777777777777777777777 stop interrupt is set!!")
                #serMega.ser.reset_input_buffer()                    
                #break
                print("raise eccezione movement!")
                raise ce.MovementsError
    except ce.MovementsError:
        fun55(1) #gli faccio far qualcosa qui
        #finally: //cannot be here...asyncio need to last until interrupt msg or ctrl-canc arrive
        print("enter in exception!!!")
        primary_lock.release()
        queue_lock.put(False)
        #serMega.ser.reset_input_buffer()
        #serMega.ser.reset_output_buffer()
        serMega.send_stop()
        serNano.send_led_anim(b'<moodexcit>')
        #lightThis = sh.led_dict.get("noled","This led pattern doesn't exists!")
        #print("lightThis finally is {} ".format(lightThis))
        #serNano.send_led(lightThis)
        serMega.close()
        serNano.close()
        print("finally the joystick is closed")
        
        
def move_robot(stopInterrupt, move_type, primary_lock,queue_lock):
    print("enter moving!!")
    move_robot_oimi(stopInterrupt, move_type, primary_lock,queue_lock)
def move_robot2(stopInterrupt, serMega, move_type, primary_lock,queue_lock):
    print("move_robot2 move_robot2 move_robot2 move_robot2 move_robot2 move_robot2 move_robot2 enter moving!!")
    move_robot_oimi2(stopInterrupt, serMega, move_type, primary_lock,queue_lock)
def move_robot22(stopInterrupt, serMega, move_type, primary_lock,queue_lock):
    print("move_robot222222222!")
    move_robot_oimi22(stopInterrupt, serMega, move_type, primary_lock,queue_lock)
def move_pattern(global_status,stopInterrupt, move_type, primary_lock, queue_lock):
    print("enter moving!!")
    move_pattern_oimi(global_status,stopInterrupt,move_type, primary_lock, queue_lock)
def move_robot3(stopInterrupt, serMega, move_type, primary_lock, queue_lock, stra_speed, for_speed, ang_speed):
    print("move_robot3 move_robot3move_robot3move_robot3move_robot3move_robot3move_robot3move_robot3 move_robot3!!")
    move_robot_oimi3(stopInterrupt, serMega, move_type, primary_lock, queue_lock, stra_speed, for_speed, ang_speed)
def move_robot4(stopInterrupt, serMega, move_type, primary_lock,queue_lock):
    print("move_robot44 444 444 444 444 444 444 444 444 !")
    move_robot_oimi4(stopInterrupt, serMega, move_type, primary_lock, queue_lock)

def move_robot_oimi_auto(stopInterrupt, move_type, primary_lock,queue_lock, serMega, serNano, serNano2):
    print("move_robot auto auto auto auto !!!!")
    move_robot_oimi_auto_free_game(stopInterrupt, move_type, primary_lock, queue_lock, serMega, serNano, serNano2)


def move_robot_joypad(global_status, stopInterrupt, move_type, primary_lock,queue_lock, serMega, serNano, serNano2):
    print("move_robot joypad joypad !!!!")
    move_robot_oimi_joypad(global_status, stopInterrupt, move_type, primary_lock, queue_lock, serMega, serNano, serNano2)

async def main_gamepad(stopInterrupt, primary_lock, queue_lock, global_status, ser1, ser2, ser3):
    try:
        #await awaitable()
        print("entro in main gamepad!!")
        print("entro in main gamepad!!")
        print("entro in main gamepad!!")
        jc.joypad_movement_logic(stopInterrupt, primary_lock, queue_lock, global_status, ser1, ser2, ser3)
    except asyncio.CancelledError:
        print("Create an exception here?")
