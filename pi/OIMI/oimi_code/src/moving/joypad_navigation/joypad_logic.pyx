#distutils: language=c++
#cython: language_level=3

import serial, time, random, struct, math, os
cimport joypad_logic
from select import select
from evdev import InputDevice, categorize, ecodes, KeyEvent
from subprocess import run
import shared as shar
import multiprocessing
from multiprocessing import Pool
from oimi_process import OimiProcess, OimiStartProcess, OimiQueue, OimiManager
from multiprocessing import Process, Pool, Pipe, Event, Lock, Value
from queue import LifoQueue
from enum import Enum, unique
import sys
if '/home/pi/OIMI/oimi_code/src/managers/serial_manager/' not in sys.path:
    sys.path.append('/home/pi/OIMI/oimi_code/src/managers/serial_manager/')
import serial_manager as serr

#inerith from class
@unique
class Joystick(Enum):
    LEFT = 0
    FRONT = 1
    RIGHT = 2
    BACK = 3
    LEFT_FRONT = 4
    LEFT_BACK = 5 
    RIGHT_FRONT = 6
    RIGHT_BACK = 7
    NONE = 8
    START = 9
    TURN = 10
    STOP = 11    
# Open the device
gamepad = InputDevice('/dev/input/event2')

last = Joystick.START
#startTime = time.time()
def setup_serial():
    cdef:
        str name1 = "Serial1"
        str name2 = "Serial2"
        str name3 = "Serial3"
        str serial_port1 = "/dev/ttyACM0"
        str serial_port2 = "/dev/ttyUSB1"
        str serial_port3 = "/dev/ttyUSB0"
        int baud_rate = 115200
        int start_marker = 60
        int end_marker = 62
    ser1 = serial.Serial(serial_port1,baud_rate,timeout=5)
    ser2 = serial.Serial(serial_port2,baud_rate,timeout=5)
    ser3 = serial.Serial(serial_port3,baud_rate,timeout=5)
    sermega = serr.SerialManager(name1,start_marker,end_marker,ser1)
    sernano = serr.SerialManager(name2,start_marker,end_marker,ser2)
    sernano2 = serr.SerialManager(name3,start_marker,end_marker,ser3)
    return sermega, sernano, sernano2

cdef convert_to_python(float *ptr, int n):
    cdef int i = 0
    lst=[]
    for i in range(n):
        lst.append(ptr[i])
    return lst

def calcola(module, xb, yb):
    cdef float strafe = 0
    cdef float forward = 0
    cdef float angular = 0 
    
    if xb == 0 and yb == 0:
        strafe  = 0
        forward = 0
        
    else:
        angle = math.atan2(yb,xb)
        print("calcolca angle is {}".format(angle))
        strafe = math.cos(angle)*module  
        print("calcolca strafe is {}".format(strafe))
        forward = math.sin(angle)*module
        print("calcolca forward is {}".format(forward))
    
    return strafe,forward,angular
    
#def controller(ser1, ser2, ser3, event_led_body, led_command):
#tutti i metodi hanno bisogno di global status....
def controller(global_status, ser1, ser2, ser3, event_led, led_command):
    print("enter_controller!!!")
    cdef str move_comma
    cdef float multiplier = 1
    cdef float stra = 0
    cdef float forw = 0
    cdef float angu = 0
    cdef int contnone = 0
    cdef float[3] speeds
    prevsec = 0
    last_value = 0
    last_result = 'ABS'
    last = Joystick.START
    for event in gamepad.read_loop():
        #if last is Joystick.NONE:
            #contnone += 1
            #if contnone==5:
            #if event.sec - prevsec > 5:
            #ser2.sendled(8)
            #last = Joystick.NONE #useless
            #contnone=0
        if event.code==40:
            print("Your controller went to sleep")
            while os.path.exists('/dev/input/event2') != True:
                time.sleep(1)
        if last is Joystick.LEFT:
            print("go_left!!")
            #stra,forw,angu = calcola(0.4*multiplier,-1,0)
            speeds = setStrafeForward(0.4*multiplier,-1,0)
            stra = speeds[0]
            forw = speeds[1]
            angu = speeds[2]
            move_comma = "simplygo"
            ser1.gamepad(move_comma,stra,forw,angu)
            led_command.put(1)
            event_led.clear()
            #ser2.sendled(1)
            #ser3.sendled(1)
            last = Joystick.LEFT
        if last is Joystick.RIGHT:
            print("go_right!!!")
            #stra,forw,angu = calcola(0.4*multiplier,1,0)
            speeds = setStrafeForward(0.4*multiplier,1,0)
            stra = speeds[0]
            forw = speeds[1]
            angu = speeds[2]
            move_comma = "simplygo"
            ser1.gamepad(move_comma,stra,forw,angu)
            led_command.put(3)
            event_led.clear()
            #ser2.sendled(3)
            #ser3.sendled(3)
            last = Joystick.RIGHT        
        if last is Joystick.FRONT:
            print("go_straight!!!")
            #stra,forw,angu = calcola(0.4*multiplier,0,1)
            led_command.put(2)
            event_led.clear()
            speeds = setStrafeForward(0.4*multiplier,0,1)
            stra = speeds[0]
            forw = speeds[1]
            angu = speeds[2]
            move_comma = "simplygo"
            ser1.gamepad(move_comma,stra,forw,angu)
            #ser2.sendled(2)
            #ser3.sendled(2)
            last = Joystick.FRONT
        if last is Joystick.BACK:
            print("go_back!!!")
            #stra,forw,angu = calcola(0.4*multiplier,0,-1)
            led_command.put(4)
            event_led.clear()                        
            speeds = setStrafeForward(0.4*multiplier,0,-1)
            stra = speeds[0]
            forw = speeds[1]
            angu = speeds[2]
            move_comma = "simplygo"
            ser1.gamepad(move_comma,stra,forw,angu)
            #ser2.sendled(4)
            last = Joystick.BACK
        if last is Joystick.RIGHT_FRONT:
            print("go_front-right!!!")
            #stra,forw,angu = calcola(0.4*multiplier,0.5,0.5)
            speeds = setStrafeForward(0.4*multiplier,0.5,0.5)
            stra = speeds[0]
            forw = speeds[1]
            angu = speeds[2]
            move_comma = "simplygo"
            ser1.gamepad(move_comma,stra,forw,angu)
            led_command.put(7)
            event_led.clear()
            #ser2.sendled(7)
            last = Joystick.RIGHT_FRONT
        if last is Joystick.RIGHT_BACK:
            #stra,forw,angu = calcola(0.4*multiplier,0.5,-0.5)
            speeds = setStrafeForward(0.4*multiplier,0.5,-0.5)
            stra = speeds[0]
            forw = speeds[1]
            angu = speeds[2]
            move_comma = "simplygo"
            ser1.gamepad(move_comma,stra,forw,angu)
            led_command.put(7)
            event_led.clear() 
            #ser2.sendled(7)
            last = Joystick.RIGHT_BACK        
        if last is Joystick.LEFT_FRONT:
            print("go_front-left")
            #stra,forw,angu = calcola(0.4*multiplier,-0.5,0.5)
            speeds = setStrafeForward(0.4*multiplier,-0.5,0.5)
            stra = speeds[0]
            forw = speeds[1]
            angu = speeds[2]
            move_comma = "simplygo"
            ser1.gamepad(move_comma,stra,forw,angu)
            led_command.put(7)
            event_led.clear() 
            #ser2.sendled(7)
            last = Joystick.LEFT_FRONT
        if last is Joystick.LEFT_BACK:
            print("go_back-left")
            #stra,forw,angu = calcola(0.4*multiplier,-0.5,-0.5)
            speeds = setStrafeForward(0.4*multiplier,-0.5,-0.5)
            stra = speeds[0]
            forw = speeds[1]
            angu = speeds[2]
            move_comma = "simplygo"
            ser1.gamepad(move_comma,stra,forw,angu)
            led_command.put(7)
            event_led.clear() 
            #ser2.sendled(7)
            last = Joystick.LEFT_BACK
                                        
        if event.type == ecodes.EV_KEY:
            keyevent = categorize(event)
            if keyevent.keystate == KeyEvent.key_down:
                # BTN_A comes in a tuple
                #if keyevent.keycode[0] == 'BTN_A': 
                #    print ("Acceso")
                #elif keyevent.keycode[0] == 'BTN_B': 
                #    print ("FERMA TUTTO btn b")
                #    move_comma = "joystop"
                #    ser.gamepad(move_comma,stra,forw,angu)
                if keyevent.keycode  == 'BTN_TL': 
                    print("TL TL TL stopall!")
                    move_comma = "joystop"
                    ser1.gamepad(move_comma,stra,forw,angu)
                    led_command.put(6)
                    event_led.clear() 
                    #ser2.sendled(6)
                    last = Joystick.NONE
                elif keyevent.keycode  == 'BTN_THUMBL': 
                    print("BTN_THUMBL stopall!")
                    move_comma = "joystop"
                    ser1.gamepad(move_comma,stra,forw,angu)
                    led_command.put(6)
                    event_led.clear() 
                    #ser2.sendled(6)
                    #last = Joystick.NONE
                    #prevsec = event.sec
                elif keyevent.keycode  == 'BTN_THUMBR': 
                    print("BTN_THUMBR stopall!")
                    move_comma = "joystop"
                    ser1.gamepad(move_comma,stra,forw,angu)
                    led_command.put(6)
                    event_led.clear() 
                    #ser2.sendled(6)
                    #last = Joystick.NONE
                    #prevsec = event.sec
                elif keyevent.keycode[0] == 'BTN_A': 
                    print("switchToZero")
                    ser1.cmon_gotutta(0)
                elif keyevent.keycode[0] == 'BTN_B': 
                    print("switchToOne")
                    ser1.cmon_gotutta(1)
                elif keyevent.keycode[0] == 'BTN_WEST': 
                    print("switchToTwo")
                    ser1.cmon_gotutta(2)                    
                elif keyevent.keycode[0] == 'BTN_NORTH': 
                    print("switchToThree")
                    ser1.cmon_gotutta(3)                    
                #elif keyevent.keycode[0] == 'BTN_WEST': 
                #    print("cmon_gotutta2")
                #    comm = "autonomous"
                #    ser.move_order(comm)
                #elif keyevent.keycode[0] == 'BTN_NORTH': 
                #    print("cmon_gotutta22")
                #    ser.cmon_gotutta(0)
                elif keyevent.keycode == 'BTN_SELECT': 
                    print("exit and close")
                    move_comma = "joystop"
                    ser1.gamepad(move_comma,stra,forw,angu)
                    led_command.put(8)
                    event_led.clear() 
                    #ser2.sendled(8)
                    last = Joystick.NONE
                    gamepad.close()
                    quit()
                elif keyevent.keycode == 'BTN_START':  #senza [0]
                    print("start!!!!")
                    run("python /home/pi/Scripts/python_tests/play_audio_with_pygame/play_oimi_audio.py '/home/pi/audio_files_volume/dammi_una_carezza.mp3' > /dev/null 2>&1", shell=True)
                    time.sleep(1);
                    run("python /home/pi/Scripts/python_tests/play_audio_with_pygame/play_oimi_audio.py '/home/pi/audio_files_volume/che_bello_evviva.mp3' > /dev/null 2>&1", shell=True)
                    #print("select")
                    #move_comma = "simplygo2"
                    #stra = 0.5
                    #forw = 10
                    #ser1.gamepad2(move_comma,stra,forw)
                    #ser2.sendled(3)
                                
        elif event.type == ecodes.EV_ABS:
            absevent = categorize(event)
            result = ecodes.bytype[absevent.event.type][absevent.event.code]
            value = absevent.event.value
            print("result 1 joypad is: {}".format(result))
            print("value 2 joypad is: {}".format(value))
    
            if result == 'ABS_RZ'and value > 250:
                multiplier = value/100
            elif result == 'ABS_RZ'and value == 0:
                multiplier = 1.5             

            elif (result == 'ABS_HAT0X' and value == 0) or (result == 'ABS_HAT0Y' and value == 0):
                last_result = result
                last_value = 0
                print("stop all")
                move_comma = "joystop"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(6)
                event_led.clear() 
                #ser2.sendled(6)
                last = Joystick.NONE
                prevsec = event.sec
            elif result == 'ABS_HAT0X' and value == 1 and last_value == 0:
                last_result = result
                last_value = value 
                print("go_right")
                #stra,forw,angu = calcola(0.4*multiplier,1,0)
                speeds = setStrafeForward(0.4*multiplier,1,0)
                stra = speeds[0]
                forw = speeds[1]
                angu = speeds[2]
                move_comma = "simplygo"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(3)
                event_led.clear()         
                #ser2.sendled(3)
                last = Joystick.RIGHT
            elif result == 'ABS_HAT0X' and value == -1 and last_value == 0:
                last_result = result
                last_value = value 
                print("go_left")
                #stra,forw,angu = calcola(0.4*multiplier,-1,0)
                speeds = setStrafeForward(0.4*multiplier,-1,0)
                stra = speeds[0]
                forw = speeds[1]
                angu = speeds[2]
                move_comma = "simplygo"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(1)
                event_led.clear() 
                #ser2.sendled(1)
                last = Joystick.LEFT
            elif result == 'ABS_HAT0Y' and value == -1 and last_value == 0:
                last_result = result
                last_value = value 
                print("go_straight")
                #stra,forw,angu = calcola(0.4*multiplier,0,1)
                speeds = setStrafeForward(0.4*multiplier,0,1)
                stra = speeds[0]
                forw = speeds[1]
                angu = speeds[2]
                move_comma = "simplygo"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(2)
                event_led.clear() 
                #ser2.sendled(2)
                last = Joystick.FRONT
            elif result == 'ABS_HAT0Y' and value == 1 and last_value == 0:
                last_result = result
                last_value = value 
                print("go_back")
                #stra,forw,angu = calcola(0.4*multiplier,0,-1)
                speeds = setStrafeForward(0.4*multiplier,0,-1)
                stra = speeds[0]
                forw = speeds[1]
                angu = speeds[2]
                move_comma = "simplygo"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(4)
                event_led.clear() 
                #ser2.sendled(4)
                last = Joystick.BACK
            elif ((result == 'ABS_HAT0Y' and value == 1) and (last_result == 'ABS_HAT0X' and last_value == 1)):
                print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++ go_back-right")
                #stra,forw,angu = calcola(0.4*multiplier,0.5,-0.5)
                speeds = setStrafeForward(0.4*multiplier,0.5,-0.5)
                stra = speeds[0]
                forw = speeds[1]
                angu = speeds[2]
                move_comma = "simplygo"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(4)
                event_led.clear() 
                #ser2.sendled(7)
                last = Joystick.RIGHT_BACK
            elif ((result == 'ABS_HAT0Y' and value == -1) and (last_result == 'ABS_HAT0X' and last_value == 1)):
                print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++ go_front-right")
                #stra,forw,angu = calcola(0.4*multiplier,0.5,0.5)
                speeds = setStrafeForward(0.4*multiplier,0.5,0.5)
                stra = speeds[0]
                forw = speeds[1]
                angu = speeds[2]
                move_comma = "simplygo"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(2)
                event_led.clear() 
                #ser2.sendled(7)
                last = Joystick.RIGHT_FRONT
            elif ((result == 'ABS_HAT0X' and value == -1) and (last_result == 'ABS_HAT0Y' and last_value == -1)):
                print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++ go_front-left")
                #stra,forw,angu = calcola(0.4*multiplier,-0.5,0.5)
                speeds = setStrafeForward(0.4*multiplier,-0.5,0.5)
                stra = speeds[0]
                forw = speeds[1]
                angu = speeds[2]
                move_comma = "simplygo"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(2)
                event_led.clear() 
                #ser2.sendled(7)
                last = Joystick.LEFT_FRONT
            elif ((result == 'ABS_HAT0X' and value == -1) and (last_result == 'ABS_HAT0Y' and last_value == 1)):
                print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++ go_back-left")
                #stra,forw,angu = calcola(0.4*multiplier,-0.5,-0.5)
                speeds = setStrafeForward(0.4*multiplier,-0.5,-0.5)
                stra = speeds[0]
                forw = speeds[1]
                angu = speeds[2]
                move_comma = "simplygo"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(4)
                event_led.clear()                 
                #ser2.sendled(7)
                last = Joystick.LEFT_BACK
            
            elif value == 128 or value == -129 or value == -900 or value == 385 or value == -386 or value== -643:
                print("stop all")
                move_comma = "joystop"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(8)
                event_led.clear()                 
                #ser2.sendled(8) #MEGLIO NON METTERLO QUI CHE ROMPE E BASTA 
                last=Joystick.NONE
                prevsec = event.sec
            #elif result == 'ABS_RX' and value > 0 and value != 128:
                #print('turn right')
                #stra = 0 
                #forw = 0 
                #angu = 0.9 
                #move_comma = "simplygo"
                #ser1.gamepad(move_comma,stra,forw,angu)
                #ser2.sendled(5)
                #last=Joystick.TURN
            #elif result == 'ABS_RX' and value < 0 and value != 128:
                #print('turn left')
                #stra = 0 
                #forw = 0 
                #angu = -0.9 
                #move_comma = "simplygo"                
                #ser1.gamepad(move_comma,stra,forw,angu)                
                #ser2.sendled(5)
                #last=Joystick.TURN
              

            elif result == 'ABS_RY' and value > 0 and value not in [128,385]:
                print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> turn right')
                stra = 0 
                forw = 0 
                angu = 0.9 
                move_comma = "simplygo"                
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(5)
                event_led.clear()            
                #ser2.sendled(5)
                last=Joystick.TURN
            elif result == 'ABS_RY' and value < 0 and value not in [-129,-600,-386]:
            #elif result == 'ABS_RY' and value < 0 and value != -129:
                print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> turn left')
                stra = 0 
                forw = 0 
                angu = -0.9 
                move_comma = "simplygo"
                ser1.gamepad(move_comma,stra,forw,angu)
                led_command.put(5)
                event_led.clear() 
                #ser2.sendled(5)
                last=Joystick.TURN

            print("stra sent: ")
            print(stra)
            print("forw sent: ")
            print(forw)
            print("angu sent: ")
            print(angu)

        #elif not event.type and last==None:
        #    last = None
        #    print("nothing!!!!")
        #    led_command.put(8)
        #    event_led.clear() 


def goLED_body(event_led_body, ser, led_command):
    print("!!enter in goled_body!!")
    #while True:
        #try:
    if not event_led_body.is_set():
        print("event body not set, quindi posso entrare")
        #ser = led_command.get()
        #print("ser nella coda = ", ser)
        conferma = led_command.get()
        print("led numero nella coda = ", conferma)
        ser.sendled(conferma)
        #break
        #time.sleep(3)
        
        #ser.sendled(8)
        #event_led_body.set()
        
        #    else: raise Exception
        #except Exception as e:
        #    print("noncene")
        #    break
        aa = 'aa'
        return aa


def activateLED(global_status, event_startledanimation, event_led_body, stopInterrupt, ser, led_command):
    print("enter in led animation, waiting")
    while True:
        event_startledanimation.wait() #with set wait method works ; with not set better use if but not in this case
        print("!!event_startledanimation!!")
        aa = goLED_body(event_led_body, ser, led_command)
        if aa=='aa':
            print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
            event_startledanimation.clear()


def begin_ok(global_status, event_startledanimation, event_led, event_led_body, stopInterrupt, ser, led_command):
    while True:
        if not event_led.is_set():
            print("begin_ok_cmon_gotutta2")
            #time.sleep(5)
            #print("tempo_passato!!!")
            #led_command.put(1)
            event_startledanimation.set()
            event_led_body.clear()
            #led_command.put(ser)
            #controller(ser1,ser2,ser3)
            event_led.set()
            while not led_command.empty():
                led_command.get()
            print("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOK SVUOTATOOOOOOOOOOOOOOOO")
            #time.sleep(20)
            #led_command.put(2)
            #event_startledanimation.set()

def joypad_logic_main():
    parent_id = os.getpid()
    main_connection, sub_connection = Pipe()
    led_command = OimiQueue("Fifo led commands")
    event_startledanimation = Event()
    event_led = Event()
    event_led_body = Event()
    stopInterrupt = Event()
    manager = OimiManager()
    OimiManager.register('LifoQueue', LifoQueue)
    manager.start()
    global_status = manager.LifoQueue()
    status = shar.PyGlobalStatus(b'beginning')
    global_status.put(status)

    ser1, ser2, ser3 = serr.setup_serial()
    ser1.await_arduino()
    ser2.await_arduino()
    ser3.await_arduino()
    #ser1.waitard_mega()
    #ser2.waitard_nano()
    #ser3.waitard_nano2()
    ser1.prova1()
    #led_command.put(1)
    #ser3.sendled(1)
    #event_startledanimation.set()
    event_led_body.set()
    led_listener = OimiStartProcess(name = 'led_listener',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = activateLED,
                                    daemon = False,
                                    event_startledanimation = event_startledanimation,
                                    event_led_body = event_led_body, 
                                    stopInterrupt = stopInterrupt,
                                    ser = ser3,
                                    led_command = led_command)
    main_listener = OimiStartProcess(name = 'main_listener',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = begin_ok,
                                    daemon = False,
                                    event_startledanimation = event_startledanimation,
                                    event_led = event_led, 
                                    event_led_body = event_led_body, 
                                    stopInterrupt = stopInterrupt,
                                    ser = ser2,
                                    ser = ser3,
                                    led_command = led_command)
    joypad_listener = OimiStartProcess(name = 'joypad_listener',
                                    sub_connection = sub_connection,
                                    global_status = global_status,
                                    parent_id = parent_id,
                                    execute = controller,
                                    daemon = False,
                                    ser1 = ser1,
                                    ser2 = ser2,
                                    ser3 = ser3,
                                    event_led = event_led,                           
                                    led_command = led_command)
    led_listener.start()
    main_listener.start()
    joypad_listener.start()
    led_listener.join()
    main_listener.join()
    joypad_listener.join()
