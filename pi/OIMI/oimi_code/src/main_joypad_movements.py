# ==========================================================================================================
# Imports
# ==========================================================================================================
import asyncio
from signal import SIGINT, SIGTERM
import joy_manager as mm
# ==========================================================================================================
# Methods
# ==========================================================================================================
def ctrlc(sig,frame):
	raise KeyboardInterrupt("CTRL-C")

async def main_gamepad():
	try:
		#await awaitable()
		mm.manager_main()
	except asyncio.CancelledError:
		do_cleanup()
# ==========================================================================================================
# Main
# ==========================================================================================================
if __name__ == '__main__':
	loop = asyncio.get_event_loop()
	main_task = asyncio.ensure_future(main_gamepad())
	#loop.add_signal_handler(SIGINT, ctrlc)
	loop.add_signal_handler(SIGINT, main_task.cancel)
	loop.add_signal_handler(SIGTERM, main_task.cancel)
	try:
		loop.run_until_complete(main_task)
	finally:
		loop.close()
		quit()

