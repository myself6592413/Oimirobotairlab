"""Compiles all cython source file provided, using setuptools and Cython.
	Build modules to build and compile Cython extensions. converting the Cython code in .pyx files.
	The verbose option is not passed to the cythonize function, so the script runs without any output of the ongoing process.
	It then defines Cython extensions, with specific compiler and linker flags, and include the numpy headers.
	Rhe script calls the setup() function from setuptools to specify the package name and version, the author, and the Cython extensions to be built.
	The cython_directives attribute is used to specify some Cython compiler options. 
	For example, the option "embedsignature" is used to include the function signature in the generated C/C++ code;
	"boundscheck" and "wraparound" are used to turn off the bounds checking and negative indexing for faster performance.
 	The class BuildExt to the cmdclass attribute of the setup() function, so that the build_extensions() method of the custom class is used to build the extensions.

	The setup() function from setuptools is used to specify the package name, version, author, and extensions to be built.
	The ext_modules argument of the setup() function takes a list of Extension objects, and when the setup() function is run, 
	It's not necessary to call cythonize() method esplicitly, because the script will automatically call cythonize() on these 
	Extension objects telling distutils to cythonize the files by passing the Extension objects to the ext_modules argument. 

    Usage:
        python setup_oimi.py build_ext --inplace (or alias setta)
"""
# ============================================================================================
#  Imports:
# ============================================================================================
#try:
#    from setuptools import setup
#    from setuptools import Extension
#except ImportError:

from distutils.core import setup
import setuptools
from distutils.extension import Extension
from Cython.Build import build_ext
from Cython.Build import cythonize
import numpy
import os.path,io,sys,shutil
# ===========================================================================================
#  Elements:
# ===========================================================================================
#readme_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../README.md')

#included_path_0 = os.path.abspath('./managers/serial_manager')

#if 'SRC_PATH' not in os.environ:
#	os.environ['included_path_0'] = included_path_0
#else:
#	included_path_0 = os.environ['included_path_0']
# ===========================================================================================
#  Extension Modules:
# ===========================================================================================
ext_modules = [
	Extension("free_play_entertainments_modality", ["modalities/free_play_entertainments_modality.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include()],
		language='c++'),
    Extension("methodical_games_modality", ["modalities/methodical_games_modality.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include()],
		language='c++'),
	Extension("experience_mode_test_modality", ["modalities/experience_mode_test_modality.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include()],
		language='c++'),
	Extension("insert_mode", ["modalities/insert_mode.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
		extra_link_args=['-fopenmp','-ffast-math','-march=native'],
		include_dirs=[numpy.get_include()],
		language='c++'),
	]
# =======================================================================
#  Class:
# =======================================================================
class BuildExt(build_ext):
    """ Extend Ctyhon build_ext for removing annoying warnings"""
    def build_extensions(self):
        if '-Wstrict-prototypes' in self.compiler.compiler_so:
            self.compiler.compiler_so.remove('-Wstrict-prototypes')
        super().build_extensions()
# =====================================================================================================================================
#  Main:
# =====================================================================================================================================
#clean()
for e in ext_modules:
    e.cython_directives = {'embedsignature': True,'boundscheck': False,'wraparound': False,'linetrace': True, 'language_level': "3"}

setup(
    name='oimi-robot source compilation',
    version='0.1.0',
    author='Colombo Giacomo',
    #long_description=io.open(readme_file, 'rt', encoding='utf-8').read(),
    ext_modules=ext_modules,
    cmdclass = {'build_ext': BuildExt},
    )