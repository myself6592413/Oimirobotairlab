	/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Oimi-Robot definitive sketch for Arduino Mega
*
*
* Created by Giacomo Colombo, Airlab Politecnico di Milano 2020
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include "Mpx_5010.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
//Analog pins for mpx_5010 pressure sensors
#define a0 A0
#define a1 A1
#define a2 A2
#define a3 A3
#define a4 A4

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Global variables */
float press0,press1,press2,press3,press4;

Mpx_5010 mpx_5010 = Mpx_5010(a0, a1, a2, a3, a4);

void grabPressures(){
/*
Stores current pressures
*/
		mpx_5010.detectPressures();
		mpx_5010.convertPressures();
		mpx_5010.mappingPressures();
		mpx_5010.printPressureAll();
        
        //press0 = mpx_5010.getRawFrontLeft();
        //press1 = mpx_5010.getRawFrontRight();
		//press2 = mpx_5010.getRawRight();
		//press3 = mpx_5010.getRawBack();
		//press4 = mpx_5010.getRawLeft();
}

void setup(){
	Serial.begin(115200);
}
void loop(){
	grabPressures();
    delay(1000);
    //Serial.print("press0")
	//Serial.print(press0)
	//Serial.print("press1")
	//Serial.print(press1)
	//Serial.print("press2")
	//Serial.print(press2)
	//Serial.print("press3")
	//Serial.print(press3)
	//Serial.print("press4")
	//Serial.print(press4)
}
