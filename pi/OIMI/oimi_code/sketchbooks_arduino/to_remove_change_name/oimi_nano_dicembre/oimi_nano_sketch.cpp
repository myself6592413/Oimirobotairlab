/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Final Arduino Nano sketch for Oimi Robot,
* Leds and sonars control
*
*
* Created by Giacomo Colombo, Airlab Politecnico di Milano 2020
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
#include "StreamPrint.h"
#include "SoftwareSerial.h"
#include "StandardCplusplus.h"
#include <vector>
#include <queue>
#include "Sonars_EZ1.h"
#include "Adafruit_NeoPixel.h"
//#include "WS2812FX.h"
#include "SerialComm_2.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
#define _ANA1 0   //back
#define _ANA2 1   //left
#define _ANA3 2   //front
#define _ANA4 3   //right
#define _TRIG1 6  //back
#define _TRIG2 11  //left
#define _TRIG3 10  //front
#define _TRIG4 9  //right
//Digital pin for ledstrip
#define LED_PIN 5
// Number of leds of ledstrip Ada
#define LED_COUNT 60
// Number of leds of ledstrip Fastled
//#define NUM_LEDS 60
//#define BRIGHTNESS 64
//#define LED_TYPE    WS2811
//#define COLOR_ORDER GRB
//CRGB leds[NUM_LEDS];
//#define UPDATES_PER_SECOND 100
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Global variables */
const int COUNT_ADDR1 = 0;
const int COUNT_ADDR2 = 1;

std::queue<int> q;

bool globale;
bool variabileglobale;
Obs current_env;  				// extern sonars environment
int typeOfMovement;     		// for sending to raspi current movement received from Mega
int dist1,dist2,dist3,dist4;	// sonars detections [inch]
int modality;					// 0 if autonomous, 1 if following, 2 if single action, 5 if joypad
char* bufferMsg;				// where commands from raspi are stored
boolean taken; 					// true if modality has been received correctly from Mega
boolean just_happened;			// true if a command has been received now
unsigned long stopDelay;		// for extending duration of RED led during movement with gamepad
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructors */
SoftwareSerial sonarSerial(12,8);  //rx,tx (12-8 / 8-12 pins connection--invert pin of master)
Sonars_EZ1 ez1 = Sonars_EZ1(_ANA1, _ANA2, _ANA3, _ANA4, _TRIG1, _TRIG2, _TRIG3, _TRIG4);
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
//WS2812FX ws2812fx = WS2812FX(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
//(la chiamo nano su raspi!)
SerialComm_2 sc = SerialComm_2();
// Led predefinite colors (cambio nomi!)
uint32_t color0 = strip.Color(255,102,0);    //ORANGE
uint32_t color1 = strip.Color(255,0,0);  	 //RED
uint32_t color2 = strip.Color(0,255,0);		 //GREEN
uint32_t color3 = strip.Color(0,0,255);		 //BLUE
uint32_t color4 = strip.Color(255,255,0);	 //YELLOW
uint32_t color5 = strip.Color(102,102,0);	 //DARK GREEN
uint32_t color6 = strip.Color(102,255,255);	 //LIGHT BLUE
uint32_t color7 = strip.Color(255,102,0);	 //ORANGE
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Methods */
void sonarsCatch(){
/* Scans sonars */
	ez1.maxsonarsLoop();
	dist1 = ez1.getSonarLeft();
	dist2 = ez1.getSonarFront();
	dist3 = ez1.getSonarRight();
	dist4 = ez1.getSonarBack();
}
void sonarsPrint(){
/* Send sonars values to Mega */
	sonarSerial.print(dist1);
	sonarSerial.print(",");
	sonarSerial.print(dist2);
	sonarSerial.print(",");
	sonarSerial.print(dist3);
	sonarSerial.print(",");
	sonarSerial.print(dist4);
	sonarSerial.print('\n');
}

void settacolori(){
	Serial.println("entra in settacolori");
	int t1 = 0;
	int t2 = 1;
	int t3 = 2;
	int t4 = 3;
	int t5 = 4;
	int t6 = 5;
	
	q.push(t1);
	q.push(t2);
	q.push(t3);
	q.push(t4);
	q.push(t5);
	q.push(t6);
}

void coloratutto(){
	Serial.println("entra in coloratutto");
	while (!q.empty()){
		if(!variabileglobale){
			int a  = q.front();
			if (a == 1){
				strip.middleFill(color1,50);
				Serial.print("size:");
				Serial.println(q.size());
				q.pop();
			}
			if (a == 2){
				strip.contactPattern(50);
				Serial.print("size:");
				Serial.println(q.size());
				q.pop();
			}
			if (a == 3){
				strip.randomPositionFill(color3,50);
				//fun1();
				Serial.print("size:");
				Serial.println(q.size());
			}
			if (a == 4)
			strip.shovePattern(50);
			}
		else{
			while(!q.empty()){
				q.pop();
				Serial.print("size_finale:");
				Serial.println(q.size());
			}
		}
	}
}


/*removeee*/
void ordersDispatcher(){
/*
* Select proper instructions according to command received from raspi.
* BufferMsg for starting led while robot is moving need a command the shortest as possible (one char).
* Replies are just text (to be embraced into <>)
* Same names of led_dict name
*/
	sc.acquireDataFromRaspi();  			// starts catching data from serial
	bufferMsg = sc.getBufferMsg();			// takes command
	just_happened = sc.getJustChanged();	// receive confirm of msg received

	if((strcmp(bufferMsg, "s")==0)){
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<domenica>");
			}
		}
	}
	// Autonomous
	else if((strcmp(bufferMsg, "a")==0)){		// Autonomous begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<autonomous>");
				strip.colorStable(color0);
			}
		}
	}
	else if((strcmp(bufferMsg, "le")==0)){ 	// left autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<left1>");
				strip.colorStable(color0);
			}
		}
	}
	else if((strcmp(bufferMsg, "fr")==0)){	// front autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<front1>");
				strip.colorStable(color2);
			}
		}
	}
	else if((strcmp(bufferMsg, "ri")==0)){	// right autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<right1>");
				strip.colorStable(color4);
			}
		}
	}
	else if((strcmp(bufferMsg, "ba")==0)){	// back autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<back1>");
				strip.colorStable(color1);
			}
		}
	}
	else if((strcmp(bufferMsg, "t")==0)){	// turn autonomous
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<turn1>");
//				strip.noColor();
				strip.colorStable(color3);
//				strip.colorStableInverse(color3);
			}
		}
	}
	else if((strcmp(bufferMsg, "m")==0)){	// switch off led following
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				globale = 0; 
				variabileglobale=1;
				just_happened = 0;
				Serialprint("<noled>");
				strip.noColor();
			}
		}
	}
	// Following
	else if((strcmp(bufferMsg, "f")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<following>");
				strip.colorStable(color3);
			}
		}
	}
	else if((strcmp(bufferMsg, "lp")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<leftpat>");
				strip.colorStable(color5);
			}
		}
	}
	else if((strcmp(bufferMsg, "rp")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<rightpat>");
				strip.colorStable(color6);
			}
		}
	}
	else if((strcmp(bufferMsg, "fp")==0)){	// Following begins
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<frontpat>");
				strip.colorStable(color7);
			}
		}
	}
	// Joystick
	else if((strcmp(bufferMsg, "w")==0)){	// left gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<left-joystick>");
				strip.colorStable(color2);
			}
		}
	}
	else if((strcmp(bufferMsg, "x")==0)){ 	// front gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<front-joystick>");
				strip.colorStable(color3);
//				strip.colorWipe_noFor(color3,40);
//				strip.noColor();
//				strip.colorWipe(color3,40);
//				strip.noColor();
			}
		}
	}
	else if((strcmp(bufferMsg, "y")==0)){	// right gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				typeOfMovement=0;
				Serialprint("<right-joystick>");
				strip.colorStable(color4);
			}
		}
	}
	else if((strcmp(bufferMsg, "z")==0)){	// back gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<back_joystick>");
				strip.colorStable(color5);
			}
		}
	}
	else if((strcmp(bufferMsg, "q")==0)){	// stop gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				stopDelay = millis();
				just_happened = 0;
				Serialprint("<stop-game>");
				strip.noColor();
				strip.colorStable(color1);
				while (millis() < stopDelay+500){}
				//strip.colorStable(color1);
				//#delay(1000);
				//strip.colorStable(color1);
				//delay(1000);
				//strip.colorStable(color1);
				//delay(1000);
				//strip.colorStable(color1);
				strip.noColor();
			}
		}
	}
	else if((strcmp(bufferMsg, "k")==0)){	//translate gamepad
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<trans-game>");
				strip.noColor();
				strip.colorStable(color0);
//				strip.randomPositionFill(color0,40);
			}
		}
	}
	else if((strcmp(bufferMsg, "c")==0)){
		if(sc.getOkNewData()){
			sc.setOkNewData(false);
			if(just_happened){
				just_happened = 0;
				Serialprint("<pattern>");
				globale=true;
			}
		}
	}
}
void colora(){
	if(!q.empty()){
		if(!variabileglobale){
			ordersDispatcher();
			int i = q.front();
			if(i==0){
				strip.middleFill(color1,50);
				strip.noColor();
				q.pop();
			}
			else if(i==1){
				strip.middleFill(color2,50);
				strip.noColor();
				q.pop();
			}
			else if(i==2){
				strip.middleFill(color3,50);
				strip.noColor();
				q.pop();
			}
			else if(i==3){
				strip.middleFill(color4,50);
				strip.noColor();
				q.pop();
			}
			else if(i==4){
				strip.middleFill(color1,50);
				strip.noColor();
				q.pop();
			}
			else if(i==5){
				strip.middleFill(color2,50);
				strip.noColor();
				q.pop();
			}
		}
		else{
			globale=false;
			strip.noColor();
			while(!q.empty())
				q.pop();
			Serial.print("size_finale:");
			Serial.println(q.size());
		}
	}
}

/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Main */
void setup(){
	sonarSerial.begin(9600);
	sonarSerial.flush();
	Serial.begin(115200);
	Serialprint("<Nano is ready>");
	Serial.flush();
	strip.begin();
	strip.show();
	strip.setBrightness(50);
	settacolori();
//	ws2812fx.init();
//	ws2812fx.setBrightness(200);
//	ws2812fx.setSpeed(100);
//	ws2812fx.start();
}
void loop(){
/*
modality == 0,1 --> need sonars, autonomous,following move
modality == 2 --> single action
modality == 3 --> joystick move
*/
	ordersDispatcher();							// gets command from raspi if any
	if(sonarSerial.available()){				// gets command from Mega if any
		int mod = sonarSerial.parseInt();
		if(sonarSerial.read() == '\n'){
			modality = mod;
			modality==0 ? typeOfMovement = 2 : modality==1 ? typeOfMovement = 1 :
			modality==2 ? typeOfMovement = 3 : typeOfMovement = 0;
			taken = true;
		}
	}
	if((modality != 2 && modality != 3) && taken){  // when joypad is active sonars aren't necessary
		sonarsCatch();
		sonarsPrint();
	}
	if(typeOfMovement){ //to speedup and jump immediately after first cycle
		if(typeOfMovement==1){
			Serialprint("<goauto>");
			typeOfMovement = 0;
		}
		else if(typeOfMovement==2){
			Serialprint("<gofollo>");
			typeOfMovement = 0;
		}
		else if(typeOfMovement==3){
			Serialprint("<goaction>");
			typeOfMovement = 0;
		}
	}
	if(globale){
		colora();
	}
}
