/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
* Oimi-Robot definitive sketch for Arduino Mega
* Controls pressure sensors, motors, leds
* --notes
* 	--loop
		N.B. --see below--  no pidLoop here,
		to avoid micro-movements instead of being completely still
*
* Created by Giacomo Colombo, Airlab Politecnico di Milano 2020
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdlib.h>
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Libraries */
#include "SoftwareSerial.h"
#include "StreamPrint.h"
#include "StandardCplusplus.h"
#include <vector>
#include "CytronDriver_C10.h"
#include "Encoder.h"
#include "OimiPid.h"
#include "Sonars_EZ1.h"
#include "Mpx_5010.h"
#include "Adafruit_NeoPixel.h"
#include "SerialComm.h"
#include "SerialReplies.h"
#include "Movements.h"
#include "Voltage.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
//Analog pins for mpx_5010 pressure sensors
#define a0 A0
#define a1 A1
#define a2 A2
#define a3 A3
#define a4 A4
//Analog pins for maxsonars distance sensors
//#define _ANA1 12 //back
//#define _ANA2 8  //left
//#define _ANA3 11 //front
//#define _ANA4 9  //right
//Digital pin for maxsonars distance sensors
//#define _TRIG 7

//Digital pins for motors
#define _DIR1 9
#define _DIR2 11
#define _DIR3 10
#define _PWM1 6
#define _PWM2 5
#define _PWM3 4
//Digital pins for encoders' channels
#define _HA1 20
#define _HA2 18
#define _HA3 3
#define _HB1 21
#define _HB2 19
#define _HB3 2
//Digital pin for ledstrip
#define LED_PIN 53
// Number of leds of ledstrip
#define LED_COUNT 60
//Analog pin for voltage checker
#define VOLT_PIN A5
// Timers
#define MAX_ITERATIONS 5000
#define MAX_TIME_ALLOWED 17000
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Global variables */
//Indices
int forpause;
int son1,son2,son3,son4;										// to store sonars detection by Nano
int modnano;
int scanProcess;												// to send modality of scanning sonars to Nano
int press_index;												// for looping pressures
int mpx_index;													// for pressure
// Buffers
int currParser;							                        // how catch info on serial, can change is value according to new data read
const byte buffSize = 200; 			                            // predefinite buffer dimension 200 is the right choice, don't reduce
char* bufferMsg;      											// where first and main command is stored, initializes first element to zero
char* ledMsg;		    										// ledstrip color command
char* patternMsg;   											// ledstrip pattern command
// Flags
boolean letsStart;
boolean tellNano;												// true when is time to send data to Nano in next cycle
boolean isReady;												// handle correct working of loops starting movement with correct sonars values
boolean just_happened;					  					    // for emotion reactions
boolean isStarted;						  					    // for ???
boolean isPushed;						  					    // for body contact during games
boolean isStillReading;					  					    // for reading whole msg
boolean okNewData;						  					    // if ok looks for data on serial
// Timers
unsigned long timeSetup;
unsigned long startAllTime, loopTime;                           // for body contact during games
unsigned long replyinterval;			                        // response pause
unsigned long colorDelay; 				                        // led pattern delay (used in games)
unsigned long resetDelay;			                            // reset delay
unsigned long pressureDelay;			                        // body pressure pause interval
unsigned long pauseDelay;			                            // for Arduino pause
// Pressure mpx_5010
float press0,press1,press2,press3,press4;
// Speeds gamepad, calculated from direction versors
float forSpeedOrder  = 0.0;				  						// forward speed
float straSpeedOrder = 0.0;				  						// strafe speed
float angSpeedOrder  = 0.0;				  						// angular speed
// Movement
unsigned char last_movement;
// Objects
Obs current_env;
// Battery
float power;							  						// battery status [volt]
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructors */
// Serial between Mega and Nano
SoftwareSerial sonarSerial(12,8); //rx,tx (12-8 / 8-12 pins connection--invert pin of master)
// Motor drivers
CytronDriver_C10 m1(_DIR1,_PWM1);
CytronDriver_C10 m2(_DIR2,_PWM2);
CytronDriver_C10 m3(_DIR3,_PWM3);
// Encoders
Encoder e1(_HA1, _HB1);
Encoder e2(_HA2, _HB2);
Encoder e3(_HA3, _HB3);
// OimiPid
OimiPid oimiPid(m1, m2, m3, e1, e2, e3);
// Leds
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
//Sonars
Sonars_EZ1 ez1 = Sonars_EZ1();
//Movements
Movements mov(oimiPid, ez1, strip);
// Pressures
Mpx_5010 mpx_5010(a0, a1, a2, a3, a4);
// Voltage
Voltage volt(VOLT_PIN);
// Serial communication
SerialComm sc = SerialComm();
// Serial prints
SerialReplies sr = SerialReplies();
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Functions */
/************************************************************************************** Sonars */
void sonarLoop(){
	if(sonarSerial.available()){
//	    Serial.print("entro in sonarloop!!!");
	    int son1 = sonarSerial.parseInt();
		int son2 = sonarSerial.parseInt();
		int son3 = sonarSerial.parseInt();
		int son4 = sonarSerial.parseInt();
	    if (sonarSerial.read() == '\n') {
	        //Serial.print("--->");
	        //Serial.print(son1);
	        //Serial.print(",");
	        //Serial.print(son2);
	       // Serial.print(",");
	        //Serial.print(son3);
	        //Serial.print(",");
	        //Serial.println(son4);
	        //Serial.print("<---");
	        ez1.putSonars(son1, son2, son3, son4);
	        //ez1.maxsonarsLoop(0);
			ez1.maxsonarsLoop(scanProcess);
	    }
	}
}
/************************************************************************************** Arduino control */
void emptyArduno(){
	while(Serial.available())
		byte temnp = Serial.read();

}
void(* resetFunc)(void) = 0; /*Safe reset after */

void pauseArduino(){
/* Puts Arduino in sleeping mode, ready for a new instruction
   After a single movement instruction
   After a emotional behaviour
   After ends of movement
*/
	pauseDelay = millis();
	while (millis() < pauseDelay + 1000) {}  // waits 1 second
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);     // sets sleep mode power consumption to 0.36 mA
	cli();	// Disable interrupts            // disables interrupts
	sleep_mode();                            // puts AVR in sleep mode
}

void reportReset(){
/* Receive resetting order from raspi*/
	if (sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.printReset();
		resetDelay = millis();
		while(millis() < resetDelay + 500){}
		mov.setIsEnded(true);
		resetFunc();
	}
}
void findSwitch(int modality){
/* Sets new parsing modality, letting Arduino to be prepared to read properly data on serial */
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sc.setCurrParser(modality);
		sr.alertSwitch(modality);
	}
}
void prepareForNano(int modality){
/* Sets modality for scanning distances, to send to Nano, not for pattern but for auto,follo,gamepad*/
	tellNano = true;
	modnano = modality;
	//in any case sonarloop is avoided with gamepad.. with pattern game single emotion??
	if(!modality) //if modality = 0 ..case of following for sonars
		scanProcess = 0;
	else
		scanProcess = 1;
}
/****************************************************************************************** Battery */
void showVoltage(){
/*
 Shows internal battery status
*/
	if (sc.getOkNewData()){
		sc.setOkNewData(false);
		power = volt.findVoltage();
		sr.printBatteryStatus(power);
	}
}
/****************************************************************************************** Wrappers */
void pidLoop(){
	oimiPid.PIDLoop();
}
void moveLoop(){
	mov.driveMe();
}
/****************************************************************************************** Body pressures */
void grabPressures(){
/*
Stores current pressures
*/
		mpx_5010.detectPressures();
		press0 = mpx_5010.getRawFrontLeft();
		press1 = mpx_5010.getRawFrontRight();
		press2 = mpx_5010.getRawRight();
		press3 = mpx_5010.getRawBack();
		press4 = mpx_5010.getRawLeft();
}
void sendPressures(int scans){
/*
Returns single scan of mpxs
case 0:
	Sends a single scan, pressure already taken by loop independently??
	Used by raspi to discover new contacts
case 1:
	Returns 10 scan of mpxs
	Used by raspi classification alg. in order to discover current interaction
*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		if(!scans)
			sr.echoPressures(press0, press1, press2, press3, press4);
		else{
			while(press_index<=10){
				grabPressures();
				sr.echoPressures(press0, press1, press2, press3, press4, press_index);
				pressureDelay = millis();
				while (millis() < pressureDelay + 400) {} // waiting for 400 ms
			}
			press_index=1;
		}
	}
}
void checkPush(boolean isPushed){
/*
Returns true if body has been touched
Used for body contact during games
*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.respondePush(isPushed);
	}
}
/****************************************************************************************** Sonars replies*/
void publishSonars(){
//	SonarLoop();
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.publishSonars(ez1.getSonarLeft(),ez1.getSonarFront(),ez1.getSonarRight(),ez1.getSonarBack());
	}
}
/****************************************************************************************** Physical contacts reactions*/
void reactHit(int zone){
/*
Change also pattern cases?? or only audio ???
*/
	sr.printHit(zone);
	strip.hitPattern(50); //wipe
}
void reactBodyTouching(char* interaction){
/*
Try different interval e.g squeeze > caress !!
*/
	if(strcmp(interaction, "quiet")==0){
		sr.printBody(0);
		strip.quietPattern(50); //stable
	}
	else if (strcmp(interaction, "contact")==0){
		sr.printBody(1);
		strip.contactPattern(50); //stable
	}
	else if (strcmp(interaction, "caress")==0){
		sr.printBody(2);
		strip.caressPattern(50);
	}
	else if (strcmp(interaction, "touch")==0){
		sr.printBody(3);
		strip.touchPattern(50);
	}
	else if (strcmp(interaction, "squeeze")==0){
		sr.printBody(4);
		strip.squeezePattern(50);
	}
	else if (strcmp(interaction, "shove")==0){
		sr.printBody(5);
		strip.shovePattern(10);
	}
	else if (strcmp(interaction, "shake")==0){
		sr.printBody(6);
		strip.shakePattern(10);
	}
	else if (strcmp(interaction, "pat")==0){
		/*change me*/
		sr.printBody(7);
		strip.patPattern(50);
	}
	else if (strcmp(interaction, "hug")==0){
		sr.printBody(8);
		strip.hugPattern(30);
	}
	else if (strcmp(interaction, "hitleft")==0)  reactHit(0);
	else if (strcmp(interaction, "hitfront")==0) reactHit(1);
	else if (strcmp(interaction, "hitright")==0) reactHit(2);
	else if (strcmp(interaction, "hitback")==0)  reactHit(3);
}
void playPredictedInteraction(char* interaction){
/*??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		press_index=1;
		reactBodyTouching(interaction);
	}
}
/****************************************************************************************** Games reactions */
void answerToExercize(int result){
/*??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		press_index=1;
		sr.answerEx(result);
		strip.exercizeAnimation(result);
	}
}
void reactToResults(int result){
/*??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		press_index=1;
		strip.resultAnimation(result);
		sr.printReaction(result);
	}
}
void noticeTimeElapsed(){
/*??*/
	if (sc.getOkNewData()){
		sc.setOkNewData(false);
		press_index=1;
		sr.printTimeElapsed();
		strip.elapsedTimeAnimation();
	}
}
void beginMatch(){
/*??*/
	if (sc.getOkNewData()){
		sc.setOkNewData(false);
		press_index=1;
		strip.beginAnimation();
	}
}
void detectAlreadyGiven(){
/*??*/
	if (sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.respondeAlreadyGiven();
		press_index=1;
		strip.alreadyGivenAnimation();
	}
}
/****************************************************************************************** Behaviours */
void respondeEmotion(int kind){
/*
??
*/
	if (sc.getOkNewData()){
		sc.setOkNewData(false);
		press_index=1;
		uint32_t noColor = strip.Color(0, 0, 0);
		colorDelay = millis();
		sr.printEmotion(kind);
		strip.responseAnimation(kind);
		while(millis() < colorDelay + 2300){}  //ok delay???
		strip.colorWipeAll(noColor, 0); //off
	}
}
/****************************************************************************************** Movements */
void inputGamepad(){
/*
??
*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.replySpeeds(mov.getStrafe() * MAX_FORWARD_CONTROL,
					   mov.getForward() * MAX_FORWARD_CONTROL,
					   mov.getAngular() * MAX_ANGULAR_CONTROL);
	}
}
void sendCustomSpeeds(){
/*??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		press_index=1;
		sr.replyCustomSpeeds(mov.getCustomDistance(), mov.getCustomAngle(),
							 mov.getCustomSpeed(), mov.getCurrentColor(), mov.getCurrentAnimation());
		/*here is ok for adding a color to response of celebration but for now is removed
		cause the color is also in the movement reaction inside Movements.copp*/
		//strip.colorWipe(strip.Color(255, 102, 0), 50); //orange
		//strip.colorWipeAll(strip.Color(0, 0, 0), 0);
		//strip.colorWipe(strip.Color(255, 102, 0), 50); //orange
		//strip.colorWipeAll(strip.Color(0, 0, 0), 0);
		//strip.colorWipe(strip.Color(255, 102, 0), 50); //orange
		//strip.colorWipeAll(strip.Color(0, 0, 0), 0);
	}
}
void reportMovement(char* movetype){
/*??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		if(strcmp(movetype, "auto")==0){
			prepareForNano(1);
			letsStart = true;
			mov.setOkLeds(true);
			mov.setActualMovement(autonomous);
			sr.publishMovement(movetype);
		}
		else if(strcmp(movetype, "follo")==0){
			prepareForNano(0);
			letsStart = true;
			mov.setOkLeds(true);
			mov.setActualMovement(following);
			sr.publishMovement(movetype);
		}
		else if(strcmp(movetype, "control")==0){
			prepareForNano(3);
			letsStart = true;
			mov.setOkLeds(false);
			sr.publishMovement(movetype);
		}
		else if(strcmp(movetype, "pattern")==0){
			letsStart = true;
			mov.setOkLeds(false);
			sr.publishMovement(movetype);
		}
	}
}
void reportStop(){
/*??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.printStop();
	}
}
void reportPause(){
/*??*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.printPause();
		//delay(4000);
		mov.setIsEnded(true);
		//pauseArduino();
	}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Messages Manager */
void commandDispatcher(){
/*
* Select proper instruction ....
* Free at the end error
* real current currparser is in serialcomm library! lookatit
*/
	currParser = sc.getCurrParser();
	sc.acquireDataFromRaspi(currParser);
	bufferMsg = sc.getBufferMsg();
	if(strcmp(bufferMsg, "switchMod0")==0){
		sc.setCurrParser(0);
		findSwitch(0);
	}
	else if(strcmp(bufferMsg, "switchMod1")==0){
		sc.setCurrParser(1);
		findSwitch(1);
	}
	else if(strcmp(bufferMsg, "switchMod2")==0){
		sc.setCurrParser(2);
		findSwitch(2);
	}
	else if(strcmp(bufferMsg, "switchMod3")==0){
		sc.setCurrParser(3);
		findSwitch(3);
	}
	else if(strcmp(bufferMsg, "pause")==0){
		reportPause();
	}
	else if(strcmp(bufferMsg, "stop")==0){
		mov.setOkLeds(false);
		mov.stopMoving();
		mov.setActualMovement(no_movement);
		mov.setIsMoving(false);
		mov.setIsControlled(false);
		mov.setIsEnded(true);
		reportStop();
	}
	else if(strcmp(bufferMsg, "joystop")==0){
		mov.setIsControlled(false);
		inputGamepad();
		sc.setJustChanged(false);
		mov.setIsMoving(false);
	}
	else if((strcmp(bufferMsg, "simplygo")==0) && (sc.getJustChanged() == true)){
		straSpeedOrder = sc.getFirstFloatVar();
		forSpeedOrder = sc.getSecondFloatVar();
		angSpeedOrder = sc.getThirdFloatVar();
		mov.setIsControlled(true);
		mov.setStrafeSpeed(straSpeedOrder);
		mov.setForwardSpeed(forSpeedOrder);
		mov.setAngularSpeed(angSpeedOrder);
		mov.setIsMoving(false);
		inputGamepad();
		sc.setJustChanged(false);
	}
	//why is necessary?? select start
	else if((strcmp(bufferMsg, "simplygo2")==0) && (sc.getJustChanged() == true)){
		straSpeedOrder = sc.getFirstFloatVar();
		forSpeedOrder = sc.getSecondFloatVar();
		mov.setIsControlled(true);
		mov.setStrafeSpeed(straSpeedOrder);
		mov.setForwardSpeed(forSpeedOrder);
		mov.setIsMoving(false);
		inputGamepad();
		sc.setJustChanged(false);
	}
	else if((strcmp(bufferMsg, "simplygo")==0) && (sc.getJustChanged() == false)){
		mov.setIsControlled(true);
		inputGamepad();
	}
	else if((strcmp(bufferMsg, "simplygo2")==0) && (sc.getJustChanged() == false)){
		mov.setIsControlled(true);
		inputGamepad();
	}
	else if(strcmp(bufferMsg, "auto")==0){
		//if(just_happened == 0){
			mov.setIsMoving(true);
			reportMovement(bufferMsg);
			just_happened = 1;
			//tellNano = true;
		//}
	}
	else if(strcmp(bufferMsg, "control")==0){
		if(just_happened == 0){
			mov.setIsMoving(false);
			mov.setIsControlled(true);
			reportMovement(bufferMsg);
			just_happened = 1;
			//tellNano = true;
		}
	}
	else if(strcmp(bufferMsg, "follo")==0){
		if(just_happened == 0){
			mov.setIsMoving(true);
			reportMovement(bufferMsg);
			just_happened = 1;
			//tellNano = true;
		}
	}
	else if(strcmp(bufferMsg, "pattern")==0){
		if(just_happened == 0){
			mov.setIsMoving(true);
			reportMovement(bufferMsg);
			just_happened = 1;
			tellNano = false;
		}
	}
	else if((strcmp(bufferMsg, "sadness")==0)){
		if(just_happened==0){
			//ledMsg = sc.getMsgSecond();
			//patternMsg = sc.getMsgThird();
			//mov.setIsMoving(true); //new here or there inside mov.actSad
			//forSpeedOrder = sc.getFirstFloatVar();
			//angSpeedOrder = sc.getSecondFloatVar();
			//mov.setCustomSpeed(forSpeedOrder);
			//mov.setCustomAngle(angSpeedOrder);
			//mov.setCurrentAnimation(patternMsg);
			//mov.setCurrentColor(ledMsg);
			mov.setIsMoving(true);
			reportMovement("pattern");
			mov.setActualMovement(sadness);
			just_happened = 1;
		}
	}
	else if(strcmp(bufferMsg, "happy")==0){
		if(just_happened==0){
			forSpeedOrder = sc.getFirstFloatVar();
			angSpeedOrder = sc.getSecondFloatVar();
			ledMsg = sc.getMsgSecond();
			patternMsg = sc.getMsgThird();
			mov.setCustomSpeed(forSpeedOrder);
			mov.setCustomAngle(angSpeedOrder);
			mov.setCurrentAnimation(patternMsg);
			mov.setCurrentColor(ledMsg);
			sendCustomSpeeds();
			mov.lightUpLeds(ledMsg,patternMsg);
			mov.setActualMovement(happiness);
			just_happened = 1;
		}
	}
	else if((strcmp(bufferMsg, "normal")==0)){
		if(just_happened==0){
			forSpeedOrder = sc.getFirstFloatVar();
			angSpeedOrder = sc.getSecondFloatVar();
			ledMsg = sc.getMsgSecond();
			patternMsg = sc.getMsgThird();
			mov.setCustomSpeed(forSpeedOrder);
			mov.setCustomAngle(angSpeedOrder);
			mov.setCurrentAnimation(patternMsg);
			mov.setCurrentColor(ledMsg);
			sendCustomSpeeds();
			mov.lightUpLeds(ledMsg,patternMsg);
			mov.setActualMovement(normality);
			just_happened = 1;
		}
	}
	else if((strcmp(bufferMsg, "enthusiast")==0)){
		if(just_happened==0){
			forSpeedOrder = sc.getFirstFloatVar();
			angSpeedOrder = sc.getSecondFloatVar();
			ledMsg = sc.getMsgSecond();
			patternMsg = sc.getMsgThird();
			mov.setCustomSpeed(forSpeedOrder);
			mov.setCustomAngle(angSpeedOrder);
			mov.setCurrentAnimation(patternMsg);
			mov.setCurrentColor(ledMsg);
			sendCustomSpeeds();
			mov.lightUpLeds(ledMsg,patternMsg);
			mov.setActualMovement(enthusiasm);
			just_happened = 1;
		}
	}
	else if(strcmp(bufferMsg, "push")==0){
		/*CHANGE VALUES LATER ACCORDING TO DECISION*/
		if(loopTime - startAllTime <= 10000){
			grabPressures();
			mpx_index+=1;
			if((press2>0.34) || (press4>0.26)){
				isPushed=1;
				mpx_index=1001;
				checkPush(isPushed);
			}
		}
		else{
			startAllTime = millis();
			isPushed=0;
			mpx_index=1001;
			checkPush(isPushed);
		}
	}
	else if(strcmp(bufferMsg, "correct")==0)		answerToExercize(0);
	else if(strcmp(bufferMsg, "wrong")==0)			answerToExercize(1);
	else if(strcmp(bufferMsg, "cele")==0)			reactToResults(0);
	else if(strcmp(bufferMsg, "cons")==0)			reactToResults(1);
	else if(strcmp(bufferMsg, "elaps")==0)			noticeTimeElapsed();
	else if(strcmp(bufferMsg, "start_match")==0)	beginMatch();
	else if(strcmp(bufferMsg, "game_red")==0)		respondeEmotion(0);
	else if(strcmp(bufferMsg, "game_green")==0)		respondeEmotion(1);
	else if(strcmp(bufferMsg, "game_yellow")==0)	respondeEmotion(2);
	else if(strcmp(bufferMsg, "game_blue")==0)		respondeEmotion(3);
	else if(strcmp(bufferMsg, "already")==0)		detectAlreadyGiven();
	else if(strcmp(bufferMsg, "reset")==-1)			reportReset();
	else if(strcmp(bufferMsg, "voltage")==0)		showVoltage();

	else if((strcmp(bufferMsg, "quiet")==0) || (strcmp(bufferMsg, "caress")==0) || (strcmp(bufferMsg, "contact")==0) ||
	(strcmp(bufferMsg, "touch")==0) || (strcmp(bufferMsg, "pat")==0) || (strcmp(bufferMsg, "squeeze")==0) ||
	(strcmp(bufferMsg, "shake")==0) || (strcmp(bufferMsg, "shove")==0) || (strcmp(bufferMsg, "hug")==0) ||
	(strcmp(bufferMsg, "hitleft")==0) || (strcmp(bufferMsg, "hitfront")==0) || (strcmp(bufferMsg, "hitright")==0) ||
	(strcmp(bufferMsg, "hitback")==0)){
		playPredictedInteraction(bufferMsg);
	}

}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Main */
void setup(){
	// Serial
	Serial.begin(115200);				  	// open serial port, establishing the baud data rate in bits per second [bps] for data transmission
	Serial.print(F("<Mega is ready>"));		// communicates immediately to raspi for first time with a short msg
	Serial.flush();
	sonarSerial.begin(9600);			  	// open software serial port, establishing the baud
	// Leds
	strip.begin();			    			// setups ledstrip for working correctly
	strip.show();			    			// turns off ledstrip pixels, starts to send data
	strip.setBrightness(200);   			// tunes ledstrip brightness (max is 255)
	// Indices
	press_index = 1;
	mpx_index 	= 0;
	currParser 	= 0;
	// Timers
	colorDelay = 0;
	resetDelay = 0;
	replyinterval = 50; //ms
	startAllTime  = millis();
	// Flags
	isStarted = true;
	// Movements
	last_movement = no_movement;
	forSpeedOrder  = 0.0;
	straSpeedOrder = 0.0;
	angSpeedOrder  = 0.0;
	mov.setupMovement();
	timeSetup = 0;
	//tellNano = true; //se voglio provare senza raspi 
	//modnano = 0;
}

void loop(){
/*
ledstart non serve per far partire dritto da subito...
*/
//	emptyArduino();
	loopTime = millis();		 			// first start time useful in game modality
	commandDispatcher();         			// listens for new command at every loop is always prepared to receive from raspi
	if(tellNano){							// afterward a command of changing scan modality is received from raspi
		sonarSerial.print(modnano);			// sends to Nano sonars modality
		sonarSerial.print('\n');			// sends last character
		sonarSerial.flush();				// waits for the transmission of outgoing serial data to complete
		tellNano = false;					// sets flag to false for don't disturbing Nano loop for no reason
	}
	if(mov.getIsMoving()){ // only if robot is ready to move
//		if(millis() > timeSetup+3000){
			sonarLoop();		// receives data from Nano and performe sonars operations
			pidLoop();			// activates PID (Proportional–Integral–Derivative) controller
			moveLoop();         // performs the correct movement
		}
//		else sonarLoop();
//	}
	else{
		if(mov.getIsControlled()){ 											// only when robot is in manual config
			oimiPid.holonomicRun(mov.getStrafe() * MAX_FORWARD_CONTROL,
			mov.getForward() * MAX_FORWARD_CONTROL,
			mov.getAngular() * MAX_ANGULAR_CONTROL);  					// navigates with current speeds
				pidLoop();														// starts/continues Pid controller cycle
			}
			else
				oimiPid.simpleStop();  											// stops without resetting Iterm
				//pidLoop(); 													// N.B error to put here!!
	}
	if(mov.getIsEnded() && (!mov.getIsMoving())){  							// after one routine/task is complete..
		if(sr.getMandato()) //fondamentale che prima ritorni il messaggio al raspi senno va in pausa prima e nn risponde e tutto si blocca!! quindi pause funziona benissimo
		//if(forpause==5){
		//	forpause = 0;
			pauseArduino(); 													// ..leaves Arduino in pause waiting for next thing to do
		//}
		//else
		//	forpause++;
		//isReady = false;													// necessary resets
	}
}
