//Giacomo sonar multiple analog

#define MAXTIMEALLOWED 5200
#define MAXSCAN 15
int cont = 1;
//const int anPin1 = 12; //old//13 //back
//const int anPin2 = 8; //12 //left
//const int anPin3 = 11; //10 //center
//const int anPin4 = 9; //8 //right

const int anPin1 = 0; //old//13 //back
const int anPin2 = 1; //12 //left
const int anPin3 = 2; //10 //center
const int anPin4 = 3; //8 //right

int diss;
int dis1,dis2,dis3,dis4;
int triggerPin1 = 6;
int triggerPin2 = 11;
int triggerPin3 = 10;
int triggerPin4 = 9; //prima erano scambiati des e sin?? ok?? errore?
unsigned long triggerDelay;
unsigned long triggerDelay1;
unsigned long triggerDelay2;
unsigned long triggerDelay3;
unsigned long triggerDelay4;
//long distance1, distance2, distance3, distance4;
int distance1, distance2, distance3, distance4,real_dist1;
//long distanc1, distanc2, distanc3, distanc4;
int distanc1, distanc2, distanc3, distanc4;
unsigned long time;
unsigned long time1;
unsigned long time2;
unsigned long time3;
unsigned long time4;
unsigned long sonarDelay;
unsigned long sonarDelay2;
unsigned long sonarDelay3;
unsigned long startTime;
unsigned long caliendtime;

float dicm1, dicm2, dicm3, dicm4;
int rangevalue1[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0};
int rangevalue2[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0};
int rangevalue3[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0};
int rangevalue4[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0};
int arraysize = 9;
int modE1, modE2, modE3, modE4;

/*void start_sensor(){
  digitalWrite(triggerPin1,HIGH);
  digitalWrite(triggerPin2,HIGH);
  digitalWrite(triggerPin3,HIGH);
  digitalWrite(triggerPin4,HIGH);
  time = millis();
  while(millis() < time + 40) {}
  //delay(40);
  digitalWrite(triggerPin1,LOW);
  digitalWrite(triggerPin2,LOW);
  digitalWrite(triggerPin3,LOW);
  digitalWrite(triggerPin4,LOW);
}*/


/*######################################################################################################*/
uint16_t analogReadNreferenc(uint8_t pin, uint8_t bits){
     bits = constrain(bits, 10, 16) -10;
   
     int d = 1;
     for (int i=0; i<bits; i++) d *= 2;
     int samples = d*d;
  
    uint16_t sum=0;
    for (int i=0; i< samples; i++) sum += analogRead(pin);
    return sum / d;   // rounding => return (sum + d/2) / d;                             
  }
  
uint16_t analogReadN(uint8_t pin, uint8_t bits){
    bits = constrain(bits, 10, 16) -10;
  
    int samples = 1 << (bits << 1);
  
    uint32_t sum=0;
    for (int i=0; i< samples; i++) sum += analogRead(pin);
    return sum >> bits;
  }
    
/*#####################################################################################################*/
void start_sensor(){
  digitalWrite(triggerPin1,HIGH);
  time = millis();
  while(millis() < time + 50) {}
  digitalWrite(triggerPin1,LOW);
  digitalWrite(triggerPin2,HIGH);
  time = millis();
  while(millis() < time + 50) {}
  digitalWrite(triggerPin2,LOW);
  digitalWrite(triggerPin3,HIGH);
  time = millis();
  while(millis() < time + 50) {}
  digitalWrite(triggerPin3,LOW);
  digitalWrite(triggerPin4,HIGH);
  time = millis();
  while(millis() < time + 50) {}
  digitalWrite(triggerPin4,LOW);
}
/*void start_sensor(){
  digitalWrite(triggerPin1,LOW);
  if(millis() > time1 + 40){
  	digitalWrite(triggerPin1,HIGH);
	time1 = millis();
  }
  digitalWrite(triggerPin2,LOW);
  if(millis() > time2 + 40){
  	digitalWrite(triggerPin2,HIGH);
  	time2 = millis();
  }
  digitalWrite(triggerPin3,LOW);
  if(millis() > time3 + 40){
  	digitalWrite(triggerPin3,HIGH);
 	time3 = millis();
  }
  digitalWrite(triggerPin4,LOW);
  if(millis() > time4 + 40){
  	digitalWrite(triggerPin4,HIGH);
  	time4 = millis();
	}
}*/
/*void start_sensor(){
	if(millis() >= triggerDelay + 40){
		digitalWrite(triggerPin1,HIGH);
		triggerDelay = millis();
	}
	else if(millis()<triggerDelay+40)
		digitalWrite(triggerPin1,LOW);
	if(millis() >=  triggerDelay + 40){
		digitalWrite(triggerPin2,HIGH);
		triggerDelay = millis();
	}
 	else if(millis()<triggerDelay+40)
		digitalWrite(triggerPin2,LOW);
	if(millis() >= triggerDelay + 40){
		digitalWrite(triggerPin3,HIGH);
		triggerDelay = millis();
	}
	else if(millis()<triggerDelay+40)
		digitalWrite(triggerPin3,LOW);
	if(millis() >= triggerDelay + 40){
		digitalWrite(triggerPin4,HIGH);
		triggerDelay = millis();
	}
	else if(millis()<triggerDelay+40)
		digitalWrite(triggerPin4,LOW);
}*/
/*void start_sensor(){
	if(millis() >= triggerDelay1 + 40){
		digitalWrite(triggerPin1,HIGH);
		triggerDelay1 = millis();
	}
	else if(millis()<triggerDelay1+40)
		digitalWrite(triggerPin1,LOW);
	if(millis() >=  triggerDelay2 + 40){
		digitalWrite(triggerPin2,HIGH);
		triggerDelay2 = millis();
	}
 	else if(millis()<triggerDelay2+40)
		digitalWrite(triggerPin2,LOW);
	if(millis() >= triggerDelay3 + 40){
		digitalWrite(triggerPin3,HIGH);
		triggerDelay3 = millis();
	}
	else if(millis()<triggerDelay3+40)
		digitalWrite(triggerPin3,LOW);
	if(millis() >= triggerDelay4 + 40){
		digitalWrite(triggerPin4,HIGH);
		triggerDelay4 = millis();
	}
	else if(millis()<triggerDelay4+40)
		digitalWrite(triggerPin4,LOW);
}*/

void read_sensors(){
  /*
  Scale factor is (Vcc/512) per inch. A 5V supply yields ~9.8mV/in
  Arduino analog pin goes from 0 to 1024, so the value has to be divided by 2 to get the actual inches
  */
  distance1 = analogRead(anPin1)/2;
  //Serial.println(distance1);
  if(distance1<=7) {
  	real_dist1 = distance1 - 6;
    dicm1 = float(real_dist1)*2.54;
  }
  else{
  	real_dist1 = distance1 + 1;
  	dicm1 = float(real_dist1)*2.54;
  }
  distance2 = analogRead(anPin2)/2;
  //Serial.println(distance2);
  if(distance2<=10)  distance2 -= 10;
  else distance2+=2;
  dicm2 = float(distance2)*2.54;
  distance3 = analogRead(anPin3)/2;
  //Serial.println(distance3);
  if(distance3<=10)  distance3 -= 10;
  else distance2+=2;
  dicm3 = float(distance3)*2.54;
  distance4 = analogRead(anPin4)/2;
  //Serial.println(distance4);
  if(distance4<=10)  distance4 -= 10;
  else distance2+=2;
  dicm4 = float(distance4)*2.54;
}


void printArray(int *a, int n)
{
  for (int i = 0; i < n; i++)
  {
    Serial.print(a[i], DEC);
    Serial.print(' ');
  }
	Serial.println();
}

void print_all(){
	//Serial.print("Sensor1");
	//Serial.print(" ");
	Serial.print(distance2);
	//Serial.print("in");
	Serial.print(", ");
	Serial.print(dicm2); //left
	//Serial.print("cm");
	Serial.print("; ");
	Serial.print(distance3);
	//Serial.print("in");
	Serial.print(", ");
	Serial.print(dicm3); //front
	//Serial.print("cm");
	Serial.print("; ");
	Serial.print(distance4);
	//Serial.print("in");
	Serial.print(", ");
	Serial.print(dicm4); //right
	//Serial.print("cm");
	Serial.print("; ");
	Serial.print(distance1);
	//Serial.print("in");
	Serial.print(", ");
	Serial.print(real_dist1);
	Serial.print(", ");
	Serial.print(dicm1); //back
	//Serial.print("cm");
	//Serial.println("; ");
	Serial.println();

/*  Serial.print("S3");
  Serial.print(" ");
  Serial.print(distance3);
  Serial.print("in");
  Serial.print(" ");
  Serial.print(" ");
  Serial.print("S4");
  Serial.print(" ");
  Serial.print(distance4);
  Serial.print("in");
  Serial.println();
*/
}


void loopin_true(){
     time = millis();
     //Serial.print("tempo:");	
     //Serial.println(time); 
     if(time-startTime < MAXTIMEALLOWED){
       start_sensor();
       read_sensors();
       print_all();
       delay(200);
    }
     else {
        if (cont==1){
           Serial.println("fine1");
           Serial.print("tempo finale:");
           Serial.println(time);
           startTime = time;
           cont++;
    }
      else {
        Serial.println("fine_finale");
        Serial.print("tempo finale:");
        Serial.println(time);
        //time = 0;
        while(1);
    }
    }
}
void isort(int *a, int n)
//  *a is an array pointer function
{
  for (int i = 1; i < n; ++i)   {     int j = a[i];     int k;     for (k = i - 1; (k >= 0) && (j < a[k]); k--)
    {
      a[k + 1] = a[k];
    }
    a[k + 1] = j;
  }
}

int calcMode(int *array, int size){
	int counter = 1;
	int max = 0;
	int mode = array[0];
	for (int pass = 0; pass < size - 1; pass++){
	   if ( array[pass] == array[pass+1] ){
	      counter++;
	      if ( counter > max){
		  max = counter;
		  mode = array[pass];
	      }
	   } else
	      counter = 1; // reset counter.
	}
        return mode;
}


void looping(){
  start_sensor();
  sonarDelay2 = 0;
  sonarDelay3 = 0;
  for(int i = 0; i < arraysize; i++){
     rangevalue1[i] = analogRead(anPin1)/2;
     rangevalue2[i] = analogRead(anPin2)/2;
     rangevalue3[i] = analogRead(anPin3)/2;
     rangevalue4[i] = analogRead(anPin4)/2;
     while (millis() < sonarDelay2 + 250){}
  }
  Serial.print("Unsorted: ");
  printArray(rangevalue1,arraysize);
  printArray(rangevalue2,arraysize);
  printArray(rangevalue3,arraysize);
  printArray(rangevalue4,arraysize);
  Serial.println();

  isort(rangevalue1,arraysize);
  isort(rangevalue2,arraysize);
  isort(rangevalue3,arraysize);
  isort(rangevalue4,arraysize);
  Serial.print("Sorted: ");
  printArray(rangevalue1,arraysize);
  printArray(rangevalue2,arraysize);
  printArray(rangevalue3,arraysize);
  printArray(rangevalue4,arraysize);
  Serial.println();
  
  modE1 = calcMode(rangevalue1,arraysize);
  modE2 = calcMode(rangevalue2,arraysize);
  modE3 = calcMode(rangevalue3,arraysize);
  modE4 = calcMode(rangevalue4,arraysize);

  Serial.print("The mode is: ");
  Serial.print(modE1);
  Serial.print(','); 
  Serial.print(modE2);
  Serial.print(',');
  Serial.print(modE3);
  Serial.print(',');
  Serial.print(modE4);
  Serial.println();
  while (millis() < sonarDelay3 + 1000){}
} 

void prova_Read(){
 	distance1 = analogRead(anPin1)/2;
	Serial.print(distance1);
	Serial.print(", ");
	distance2 = analogRead(anPin2)/2;
	Serial.print(distance2);
	Serial.print(", ");
	distance3 = analogRead(anPin3)/2;
	Serial.print(distance3);
	Serial.print(", ");
	distance4 = analogRead(anPin4)/2;
	Serial.println(distance4);
}

void prova_Read_better(){
  unsigned int distance1_better = analogReadN(anPin1, 10);
  Serial.print(distance1_better/2, DEC);
  Serial.print(", ");
  unsigned int distance2_better = analogReadN(anPin2, 10);
  Serial.print(distance2_better/2, DEC);
  Serial.print(", ");
  unsigned int distance3_better = analogReadN(anPin3, 10);
  Serial.print(distance3_better/2, DEC);
  Serial.print(", ");
  unsigned int distance4_better = analogReadN(anPin4, 10);
  Serial.println(distance4_better/2);
}

void setup() {
  //Serial.begin(115200);
  Serial.begin(9600);
  pinMode(triggerPin1, OUTPUT);
  pinMode(triggerPin2, OUTPUT);
  pinMode(triggerPin3, OUTPUT);
  pinMode(triggerPin4, OUTPUT);
  startTime = millis();
  Serial.print("startTimeis:");
  Serial.println(startTime);
  sonarDelay=0;
	time1=0;
	time2=0;
	time3=0;
	time4=0;
}
void loop(){
   start_sensor();
   if(millis() >= sonarDelay + 250){
	   //prova_Read();
    prova_Read_better();
   	sonarDelay = millis();
   }
   //read_sensors();
   //print_all();
   //prova_Read();
   //delay(250);

}
void loop_test(){
	start_sensor();
	diss = analogRead(anPin4)/2;
	Serial.println(diss);
	delay(1000);
}
