//#include <Arduino.h> //useless no??

#include "SoftwareSerial.h"
#include "StandardCplusplus.h"
//#include "Sonars_EZ1_following_settembre.h"
#include "Sonars_EZ1.h"


#define _ANA1 0 //back
#define _ANA2 1  //left
#define _ANA3 2 //front
#define _ANA4 3  //right
#define _TRIG1 4
#define _TRIG2 7
#define _TRIG3 6
#define _TRIG4 5

bool scanModality;
int s1,s2,s3,s4;
int mod;

//Sonars_EZ1_following_settembre ez1 = Sonars_EZ1_following_settembre(_ANA1, _ANA2, _ANA3, _ANA4, _TRIG1, _TRIG2, _TRIG3, _TRIG4);
Sonars_EZ1 ez1 = Sonars_EZ1(_ANA1, _ANA2, _ANA3, _ANA4, _TRIG1, _TRIG2, _TRIG3, _TRIG4);
SoftwareSerial sonarSerial(12,8); //rx,tx (12-8 / 8-12 pins connection--invert pin of master)

void sonarLoopNew(){
	ez1.test_sonarsLoop_simple();
	s1 = ez1.getSonarLeft();
	s2 = ez1.getSonarFront();
	s3 = ez1.getSonarRight();
	s4 = ez1.getSonarBack();
}

//cambio poi creando unica funzione! con parametro sonarloop unica merge di questa e quella sopra
/*void sonarLoopNewFol(){
	int leftS[3],frontS[3],rightS[3],backS[3];
	int leftSo[3],frontSo[3],rightSo[3],backSo[3];
	//ez1.readInFollow();
	int iii=0;
	while(iii<3){
		ez1.maxsonarsLoop_simple();
		s1 = ez1.getSonarLeft();
		s2 = ez1.getSonarFront();
		s3 = ez1.getSonarRight();
		s4 = ez1.getSonarBack();
		leftS[iii]  = s1;
		frontS[iii] = s2;
		rightS[iii] = s3;
		backS[iii]  = s4;
		iii++;
	}
	ez1.sortSamples(leftS, leftSo, 3, false);
	ez1.sortSamples(frontS, frontSo, 3, false);
	ez1.sortSamples(rightS, rightSo, 3, false);
	ez1.sortSamples(backS, backSo, 3, false);
	s1 = ez1.Filter(leftSo,3);
	s2 = ez1.Filter(frontSo,3);
	s3 = ez1.Filter(rightSo,3);
	s4 = ez1.Filter(backSo,3);
}*/

void sonarPrintNew(){
	sonarSerial.print(s1);
	sonarSerial.print(",");
	sonarSerial.print(s2);
	sonarSerial.print(",");
	sonarSerial.print(s3);
	sonarSerial.print(",");
	sonarSerial.print(s4);
	sonarSerial.print('\n');
}

void receiveMod(int a){
	if(!a) mod =0;
	else mod=1;
}
void setup(){
	sonarSerial.begin(4800);
	Serial.begin(115200);
	sonarSerial.flush();
}
void mainfun(int mod){
	if(!mod){
	sonarLoopNew();
	sonarPrintNew();
	}
	else{
	//sonarLoopNewFol();
	sonarLoopNew();
	sonarPrintNew();
	}
}

void loop(){
	if(sonarSerial.available()){
		int mo = sonarSerial.parseInt();
		if(sonarSerial.read() == '\n')
			mod = mo;
	}
	mainfun(mod);
}

