#include <Arduino.h>

//tentative1
//void reset() { asm volatile ("jmp 0"); }
//tentative2
/*
void reset(){
  noInterrupts();
  __asm("wdr");
  WDTCSR = (1<<WDCE) | (1<<WDE);
  WDTCSR = (1<<WDE);
  for(;;){};
}


void setup(){

    reset();
}


void loop(){
    reset();
}
*/

//tentative3
#include <avr/wdt.h>

void reboot() {
  wdt_disable();
  wdt_enable(WDTO_15MS);
  while (1) {}
}

void setup() {
  pinMode(13, OUTPUT);
}


void loop() {
  // flash slowly a few times
  for (int i = 0; i < 10; i++) {
    digitalWrite(13, !digitalRead(13));
    delay(500);
  }
  // reset the Arduino
  delay(1000);
  reboot();
}

