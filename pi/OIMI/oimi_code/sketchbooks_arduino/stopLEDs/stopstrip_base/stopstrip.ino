#include "Adafruit_NeoPixel.h"
#define LED_PIN    5
#define LED_COUNT 60

#ifndef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

void setup(){
	Serial.begin(115200);
	//ledstripe.setupLedStripe();
    strip.begin();      
	strip.show();           
	strip.setBrightness(0);
	strip.colorWipeAll(strip.Color(0,   0,   0), 50); //White
}

void loop(){
    strip.colorWipeAll(strip.Color(0,   0,   0), 50);
}
