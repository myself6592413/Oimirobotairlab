/********************************************************************
* Oimi-Robot Sketch prova seriallibr
*
*********************************************************************/
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Imports */
#include <Arduino.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdlib.h>
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Libraries */
#include "Mpx_5010.h"
#include "Adafruit_NeoPixel.h"
#include "SerialComm.h"
#include "SerialReplies.h"
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Definitions */
//Analog pins for mpx_5010 pressure sensors
#define a0 A0
#define a1 A1
#define a2 A2
#define a3 A3
#define a4 A4
//Digital pin for ledstrip
#define LED_PIN 4
// Number of leds of ledstrip
#define LED_COUNT 60
// Timers
#define MAX_ITERATIONS 5000
#define MAX_TIME_ALLOWED 17000
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Global variables */
//Indices
int press_index;
int mpx_index;
// Buffers
int currParser;							                        // how catch info on serial, can change is value according to new data read
const byte buffSize = 200; 			                            // predefinite buffer dimension
char* bufferMsg;      											// where first and main command is stored, initializes first element to zero
char* LedMsg;		    										// ledstrip color command
char* patternMsg;   											// ledstrip pattern command
// Flags
//boolean just_changed; //ok useless ??	  					    // for manual movement
boolean scanModality;
boolean just_happened;					  					    // for emotion reactions
boolean isStarted;						  					    // for ??
boolean isPushed;						  					    // for body contact during games
boolean isStillReading;					  					    // for reading whole msg
boolean okNewData;						  					    // if ok looks for data on serial
// Timers
unsigned long startMillis,currentMillis;                        // for body contact during games
unsigned long replyinterval;			                        // response pause
unsigned long colorDelay; 				                        // led pattern delay (used in games)
unsigned long resetDelay;			                            // reset delay
unsigned long pressureDelay;			                        // body pressure pause interval
unsigned long pauseDelay;			                            // for Arduino pause
// Pressure mpx_5010
float press0,press1,press2,press3,press4;
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Constructors */
// Leds
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
// Mpxs
Mpx_5010 mpx_5010 = Mpx_5010(a0, a1, a2, a3, a4);
// Serial communication
SerialComm sc = SerialComm();
// Serial prints
SerialReplies sr = SerialReplies();
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Functions */
/**
**************************************************************************************** Arduino control */
void(* resetFunc)(void) = 0; /*Safe reset after */

void pauseArduino(){
/*
??
*/
	pauseDelay = millis();
	while (millis() < pauseDelay + 1000) {}  // waits 1 second
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);     // sets sleep mode power consumption to 0.36 mA
	cli();	// Disable interrupts            // disables interrupts
	sleep_mode();                            // puts AVR in sleep mode
}
void reportReset(){
/*
??
*/
	if (sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.printReset();
		resetDelay = millis();
		//strip.colorStable(strip.Color(0, 255, 0)); //green
		while(millis() < resetDelay + 500){}
		resetFunc();
	}
}
/****************************************************************************************** Switch */
void alertSwitch(int modality){
/*
Sets new parsing modality, letting Arduino to be prepared to read properly data on serial
*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sc.setCurrParser(modality);
		sr.alertSwitch(modality);
	}
}
/****************************************************************************************** Body pressures */
 void reply_quiet_1(){
    if(sc.getOkNewData()){
         sc.setOkNewData(false);
         Serial.print("<quiet1 found");
         Serial.println(">");
     }
}


void grabPressures(){
/*
Stores current pressures
*/
		mpx_5010.detectPressures();
		press0 = mpx_5010.getPressFrontLeft();
		press1 = mpx_5010.getPressFrontRight();
		press2 = mpx_5010.getPressRight();
		press3 = mpx_5010.getPressBack();
		press4 = mpx_5010.getPressLeft();
}


void sendPressures(int scans){
/*
Returns singleeeean of mpxs
	print("FINITA PRIMA PARTE ACQUISITZIONE")
case 0:
	Sends a single scan, pressure already taken by loop independently??
	Used by raspi to discover new contacts
case 1:
	Returns 10 scan of mpxs
	Used by raspi classification alg. in order to discover current interaction
*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		if(!scans)
			sr.echoPressures(press0, press1, press2, press3, press4);
		else{
			while(press_index<=10){
				grabPressures();
				sr.echoPressures(press0, press1, press2, press3, press4, press_index);
				pressureDelay = millis();
				while (millis() < pressureDelay + 400) {} // waiting for 400 ms
			}
			press_index=1;
		}
	}
}
void checkPush(boolean isPushed){
/*
Returns true if body has been touched
Used for body contact during games
*/
	if(sc.getOkNewData()){
		sc.setOkNewData(false);
		sr.respondePush(isPushed);
	}
}
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Messages Manager */
void serialDispatcher(){
/*
* Select proper instruction..
* Free at the end error
*/
	//free(bufferMsg);
	//bufferMsg = (char*)malloc(buffSize * sizeof(char*));
	LedMsg = (char*)malloc(buffSize * sizeof(char*));		// ledstrip color command
	patternMsg = (char*)malloc(buffSize * sizeof(char*));   // ledstrip pattern command
	currParser = sc.getCurrParser();
	sc.acquireDataFromRaspi(currParser);
	bufferMsg = sc.getBufferMsg();
	if(strcmp(bufferMsg, "t")==0){  //old "pressa"
		sendPressures(1);	
	}
	else if(strcmp(bufferMsg, "f")==0){
		sendPressures(0);	
	}
    else if(strcmp(bufferMsg, "qu1")==0)
             reply_quiet_1();
        else if(strcmp(bufferMsg, "qu2")==0)
             reply_quiet_1();
         else if(strcmp(bufferMsg, "co1")==0)
             reply_quiet_1();
         else if(strcmp(bufferMsg, "co2")==0)
             reply_quiet_1();
         else if(strcmp(bufferMsg, "ca1")==0)
             reply_quiet_1();
         else if(strcmp(bufferMsg, "ca2")==0)
             reply_quiet_1();
         else if(strcmp(bufferMsg, "sq1")==0)
             reply_quiet_1();


	else if(strcmp(bufferMsg, "push")==0){
		/*CHANGE VALUES LATER ACCORDING TO DECISION*/
		if(currentMillis - startMillis <= 10000){
			grabPressures();
			mpx_index+=1;
			if((press2>0.34) || (press4>0.26)){
				isPushed=1;
				mpx_index=1001;
				checkPush(isPushed);
			}
		}
		else{
			startMillis = millis();
			isPushed=0;
			mpx_index=1001;
			checkPush(isPushed);
		}
	}

	}
	// free(bufferMsg); error!!
/**
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Main */
void setup(){
/*??*/
	// Serial
	Serial.begin(115200);		// open serial port, establishing the baud data rate in bits per second [bps] for data transmission
	Serial.println(F("<hei>")); // communicates immediately to raspi for first time with a short msg
	// Led
	strip.begin();			    // setups ledstrip for working correctly
	strip.show();			    // turns off ledstrip pixels, starts to send data
	strip.setBrightness(200);   // tunes ledstrip brightness (max is 255)
	// Indices
	press_index = 1;
	mpx_index = 0;
	currParser = 0;
	// Timers
	replyinterval = 50; //ms
	colorDelay = 0;
	resetDelay = 0;
	startMillis = millis();
	// Flags
	isPushed = false;
	isStillReading = false;
	okNewData = false;
	isStarted = true;
	scanModality = 0;
	//just_changed = false;
	// Movements
	//DA RIMETTERE
	//mov.setupMovement();
	//DA TOGLIERE DEBUGGING???:
	//mov.setCustomSpeed(0.0);
	//mov.setCustomAngle(2.9);
	//mov.setupMovement(autonomous_movement);
}
void loop(){
	grabPressures();
	serialDispatcher();
}

/*TO REMOVE*/
//void loop_vecchio(){
//	sonarLoop();
//	delay(250);
	//pidLoop();
	//moveLoop();
//}

/*void loop() {
	currentMillis = millis();
	serialDispatcher();         // listens for new command
	if (isStarted){			    // only if Oimi is ready to move ?? change me
		if(mov.getIsMoving()){  // only if Oimi is in autonomous config
			sonarLoop();		// scans surrounding environment
			pidLoop();			// activates PID (Proportional–Integral–Derivative) controller
			moveLoop();         // performs correct movement
		}
		else{
			if(mov.getIsControlled()){ 											// only when Oimi is in manual config
				oimiPid.holonomicRun(mov.getStrafe() * MAX_FORWARD_CONTROL,
									 mov.getForward() * MAX_FORWARD_CONTROL,
									 mov.getAngular() * MAX_ANGULAR_CONTROL);  	// navigates with current speeds
				pidLoop();
			}
			else
				oimiPid.simpleStop();  											// stops without resetting Iterm
			//pidLoop(); qui non ci va metto nelle notes del loop perché se no continua a muoversi da solo poco poco e cambia posa    														// starts/continues Pid controller cycle
		}
	}
	if(mov.getIsEnded() && (!mov.getIsMoving()))  								// after one routine/task is complete..
		pauseArduino(); 						  								// ..leaves Arduino in pause waiting for next thing to do
}*/

