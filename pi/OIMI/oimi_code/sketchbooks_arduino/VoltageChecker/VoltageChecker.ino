#define NUM_SAMPLES 10
#define MAX_INDICATOR_VOLTAGE 12.5
#define MIN_INDICATOR_VOLTAGE 11.5
#define MAX_INDICATOR_VALUE 0.0
#define MIN_INDICATOR_VALUE 5.0
#define BATTERY_UPDATE_TIME 20000

#define VOLT_CHECKER_PIN A5

unsigned char sample_count=0;
int sum=0;
int cont;
float voltage = 0.0;
int battery_indicator=0;

float mapp;

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
 return (float)(x - in_min) * (out_max - out_min) / (float)(in_max - in_min) + out_min;
}

void setup(){
    Serial.begin(115200);
    cont=0;
    // take a number of analog samples and add them up
/*  while (sample_count < NUM_SAMPLES) {
        sum += analogRead(VOLT_CHECKER_PIN);
        sample_count++;
    }
    // calculate the voltage
    // use 5.0 for a 5.0V ADC reference voltage
    // 5.015V is the calibrated reference voltage
    voltage = (((float)sum / (float)NUM_SAMPLES * 5.005) / 1024.0)*4.013;
    mapp = mapfloat(voltage,MIN_INDICATOR_VOLTAGE,MAX_INDICATOR_VOLTAGE,MIN_INDICATOR_VALUE,MAX_INDICATOR_VALUE);
    battery_indicator=constrain(mapp,MIN_INDICATOR_VALUE,MAX_INDICATOR_VALUE);
    Serial.println("*B" + String(battery_indicator) + "*");
    lastBatteryUpdate=millis();
    // send voltage for display on Serial Monitor
    // voltage multiplied by 11 when using voltage divider that
    // divides by 11. 11.132 is the calibrated voltage divide
    // value
    sample_count = 0;
    sum = 0;
    if(voltage<=11.5){
      Serial.println("aiuto_scarica");
      //playS(BATTERIA_SCARICA_AUDIO);
      //interpreterState=discharge;
      lastWarning=millis();
    }
*/
}

void loop(){
	while (cont<5700){
    	// take a number of analog samples and add them up
    	while (sample_count < NUM_SAMPLES) {
        	sum += analogRead(VOLT_CHECKER_PIN);
        	sample_count++;
    	}
    	// calculate the voltage
    	// use 5.0 for a 5.0V ADC reference voltage
    	// 5.015V is the calibrated reference voltage
    	//voltage = (((float)sum / (float)NUM_SAMPLES * 5.005) / 1024.0)*3.51;
    	
		voltage = (((float)sum / (float)NUM_SAMPLES * 5.005) / 1024.0)*4.04;
    	//olds ---54?? 3.61/63 ?ma prima era ok con ?3.72 = 01112019 ricky pair ; 3.78 = oimi dicembre ; 3.915 = oimi pair 01112019;//4.013;
    	//other new battery was 3.915 instead of 3.715
    	// send voltage for display on Serial Monitor
    	// voltage multiplied by 11 when using voltage divider that
    	// divides by 11. 11.132 is the calibrated voltage divide
    	// value
		//NUVO =>>> 4.04 ...batteria seria
    	sample_count = 0;
    	sum = 0;
    	cont++;
    	if(voltage<=11.5){
			Serial.print("pericolo! stato batteria: ");
			Serial.println(voltage);
    	}
    	else{
			Serial.print("no è carica, precisamente è ");
        	Serial.println(voltage);
    	}
    	delay(1000);
	}
	return;
}
