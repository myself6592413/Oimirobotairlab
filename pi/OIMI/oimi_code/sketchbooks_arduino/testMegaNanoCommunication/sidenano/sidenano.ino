#include "SoftwareSerial.h"

const int anPin0 = 0;
int distance;

//SoftwareSerial mySerial(10, 11); // RX, TX
SoftwareSerial mySerial(12,8); // RX, TX

void setup(){
	//Serial.begin(4800); useless!
    //Serial.flush();
	mySerial.begin(4800);
    mySerial.flush();
}

void loop(){
	distance = analogRead(anPin0)/2;
	mySerial.print(distance);
	mySerial.print('\n');
	delay(200);
}
