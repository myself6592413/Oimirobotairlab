#include "SoftwareSerial.h"
#define pino 13

//SoftwareSerial mySerial(10, 11); // RX, TX
SoftwareSerial mySerial(12,8); // RX, TX

void setup(){
	Serial.begin(115200);
	Serial.flush();
	mySerial.begin(4800);
	mySerial.flush();
	pinMode(pino,OUTPUT);
}
void loop(){
	if(mySerial.available()){
		int son = mySerial.parseInt();
		if (mySerial.read() == '\n') {
			Serial.print(son);
			if(son<8){
				digitalWrite(pino,HIGH);
			}
			else digitalWrite(pino,LOW);
		}
	}
}
