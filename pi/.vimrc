set re=0
set backspace=indent,eol,start
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set number
set tabpagemax=500
syntax on 
filetype on
au BufNewFile,BufRead *.txt set filetype=txt
set ignorecase
set smartcase
set mouse-=r 
nnoremap <F2> :<C-U>setlocal lcs=tab:>-,trail:-,eol:$ list! list? <CR>
vnoremap <C-c> "*y
inoremap <C-v> <ESC>"+pa
vnoremap <C-c> "+y
vnoremap <C-d> "+d
set thesaurus+=/home/notto4/Documents/guide/thesaurusvim.txt
set maxmempattern=5000
hi link Repeat Statement
nmap <F5> <Esc>:w<CR>:!clear;python %<CR>
" underline some words
match Underlined /du -h/
set nocp
" enlarge vim last command history
set history=10000

" duplicate line in normal mode:
nnoremap <C-D> Yp
" duplicate line in insert mode:
inoremap <C-D> <Esc> Ypi
" avoid entering in Ex mode --> where i get stuck if i dont go out immediately with :visual
map Q <Nop>
" avoid redrawtime exceeded syntax highlighting disabled


silent! call plug#begin('~/.vim/plugged')
Plug 'junegunn/goyo.vim'

Plug 'mechatroner/rainbow_csv'
call plug#end()
