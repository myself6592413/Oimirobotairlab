## Oimi robot project
Development of a mobile autonomous robot addressed to autistic children at AIRLab lab at Politecnico di Milano
For more details about the project, look into: 
 ``` 
pi/OIMI/oimi_docs
```
The main folder with source code is in 
```
Oimirobotairlab/pi/OIMI/oimi_code/src
```

