import sys,random,time,smbus			#import SMBus module of I2C
from time import sleep			#import

round = 0
#some MPU6050 Registers and their Address
PWR_MGMT_1	 = 0x6B
SMPLRT_DIV	 = 0x19
CONFIG		 = 0x1A
GYRO_CONFIG  = 0x1B
INT_ENABLE	 = 0x38
ACCEL_XOUT_H = 0x3B
ACCEL_YOUT_H = 0x3D
ACCEL_ZOUT_H = 0x3F
GYRO_XOUT_H  = 0x43
GYRO_YOUT_H  = 0x45
GYRO_ZOUT_H  = 0x47

typing_speed = 250 #wpm
def sloprint(t):
	t = str(t)
	for l in t:
		sys.stdout.write(l)
		sys.stdout.flush()
		time.sleep(random.random()*5.0/typing_speed)
	print (' ')

def slonumprint(t):
	ti = str(t)
	sys.stdout.write(ti)
	sys.stdout.flush()
	time.sleep(random.random()*10.0/typing_speed)
	print ('')

def MPU_Init_right():
	#write to sample rate register
	bus.write_byte_data(Device_Address_r, SMPLRT_DIV, 7)

	#Write to power management register
	bus.write_byte_data(Device_Address_r, PWR_MGMT_1, 1)

	#Write to Configuration register
	bus.write_byte_data(Device_Address_r, CONFIG, 0)

	#Write to Gyro configuration register
	bus.write_byte_data(Device_Address_r, GYRO_CONFIG, 24)

	#Write to interrupt enable register
	bus.write_byte_data(Device_Address_r, INT_ENABLE, 1)

def read_raw_data_right(addr):
	#Accelero and Gyro value are 16-bit
		high = bus.read_byte_data(Device_Address_r, addr)
		low = bus.read_byte_data(Device_Address_r, addr+1)

		#concatenate higher and lower value
		value = ((high << 8) | low)

		#to get signed value from mpu6050
		if(value > 32768):
				value = value - 65536
		return value


def MPU_Init_left():
	#write to sample rate register
	bus.write_byte_data(Device_Address_l, SMPLRT_DIV, 7)

	#Write to power management register
	bus.write_byte_data(Device_Address_l, PWR_MGMT_1, 1)

	#Write to Configuration register
	bus.write_byte_data(Device_Address_l, CONFIG, 0)

	#Write to Gyro configuration register
	bus.write_byte_data(Device_Address_l, GYRO_CONFIG, 24)

	#Write to interrupt enable register
	bus.write_byte_data(Device_Address_l, INT_ENABLE, 1)

def read_raw_data_left(addr):
	#Accelero and Gyro value are 16-bit
		high = bus.read_byte_data(Device_Address_l, addr)
		low = bus.read_byte_data(Device_Address_l, addr+1)

		#concatenate higher and lower value
		value = ((high << 8) | low)

		#to get signed value from mpu6050
		if(value > 32768):
				value = value - 65536
		return value


bus = smbus.SMBus(1)	# or bus = smbus.SMBus(0) for older version boards
time.sleep(1)
Device_Address_r = 0x68   # MPU6050 device address
Device_Address_l = 0x69
MPU_Init_left()
MPU_Init_right()

#sloprint (" Reading Data of Gyroscope and Accelerometer left")
#sloprint (" Reading Data of Gyroscope and Accelerometer right")

out_file = open("/home/pi/Scripts/python_tests/oimi_mpu/secondtest_text.txt","w")
out_file.write("This Text is going to out file\nLook at it and see\n")
out_file.close()

#while True:
while round < 500:
	#Read Accelerometer raw value
	acc_x_r = read_raw_data_right(ACCEL_XOUT_H)
	acc_y_r = read_raw_data_right(ACCEL_YOUT_H)
	acc_z_r = read_raw_data_right(ACCEL_ZOUT_H)

	#Read Gyroscope raw value
	gyro_x_r = read_raw_data_right(GYRO_XOUT_H)
	gyro_y_r = read_raw_data_right(GYRO_YOUT_H)
	gyro_z_r = read_raw_data_right(GYRO_ZOUT_H)

	#Full scale range +/- 250 degree/C as per sensitivity scale factor
	Ax_r = acc_x_r/16384.0
	Ay_r = acc_y_r/16384.0
	Az_r = acc_z_r/16384.0

	Gx_r = gyro_x_r/131.0
	Gy_r = gyro_y_r/131.0
	Gz_r = gyro_z_r/131.0

	#Read Accelerometer raw value
	acc_x_l = read_raw_data_left(ACCEL_XOUT_H)
	acc_y_l = read_raw_data_left(ACCEL_YOUT_H)
	acc_z_l = read_raw_data_left(ACCEL_ZOUT_H)

	#Read Gyroscope raw value
	gyro_x_l = read_raw_data_left(GYRO_XOUT_H)
	gyro_y_l = read_raw_data_left(GYRO_YOUT_H)
	gyro_z_l = read_raw_data_left(GYRO_ZOUT_H)

	#Full scale range +/- 250 degree/C as per sensitivity scale factor
	Ax_l = acc_x_l/16384.0
	Ay_l = acc_y_l/16384.0
	Az_l = acc_z_l/16384.0

	Gx_l = gyro_x_l/131.0
	Gy_l = gyro_y_l/131.0
	Gz_l = gyro_z_l/131.0

	elle_giro = [gyro_x_l,gyro_y_l,gyro_z_l]
	erre_giro = [gyro_x_r,gyro_y_r,gyro_z_r]


	elle_giro_no_y = [gyro_x_l,gyro_z_l]
	erre_giro_no_y = [gyro_x_r,gyro_z_r]
	
	elle_acc = [acc_x_l, acc_y_l,acc_z_l]
	erre_acc = [acc_x_r, acc_y_r,acc_z_r]
	#sloprint(elle)
	#slonumprint(gyro_y_l)
	#slonumprint(gyro_z_l)
	#sloprint(erre)
	
	#print("--> acce: ")
	#print(erre_acc)
	#print(elle_acc)
	#print("---real acce real: ")
	#print(Ax_l,Ay_l,Az_l)
	#print(Ax_r,Ay_r,Az_r)

	print("--> gyros: ")
	print(erre_giro_no_y)
	#slonumprint(gyro_y_r)
	#slonumprint(gyro_z_r)
	print(elle_giro_no_y)
	print()
	#print ("Gx=%.2f" %Gx, u'\u00b0'+ "/s", "\tGy=%.2f" %Gy, u'\u00b0'+ "/s", "\tGz=%.2f" %Gz, u'\u00b0'+ "/s", "\tAx=%.2f g" %Ax, "\tAy=%.2f g" %Ay, "\tAz=%.2f g" %Az)
	sleep(0.3)
	round+=1

out_file = open("/home/pi/Scripts/python_tests/oimi_mpu/secondtest_text.txt","w")
out_file.write("I HVE FINISHED\n")
out_file.close()
exit()
