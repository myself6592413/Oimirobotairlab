import time, board, busio, adafruit_mpr121
i2c = busio.I2C(board.SCL, board.SDA)
from datetime import datetime, timedelta
# Create MPR121 object.
mpr121 = adafruit_mpr121.MPR121(i2c)
# ==========================================================================================================================================================
# Methods
# ==========================================================================================================================================================
def fun_capa(mpr121, sec):
	print("CAPA CAPA CAPA CAPA CAPA CAPA CAPA CAPA")
	#maxtime= datetime.now()+timedelta(seconds=sec)
	j = 0 
	n = 0
	#print("maxtime = ", maxtime)
	#while datetime.now()<maxtime:
	while n<100:
		n+=1
		beg_time = time.time()
		for i in range(4):
			# Call is_touched and pass it then number of the input.	If it's touched
			# it will return True, otherwise it will return False.
			if mpr121[i].value:
				print('Input CAPA CAPA CAPA CAPA CAPA CAPA {} touched!'.format(i))
				print(i)
				print(mpr121[i])
				#break 
				#return i
			time.sleep(0.25) # Small delay to keep from spamming output messages.
	return 5 
	
def fun_mpr121(sec):
	i = fun_capa(mpr121, sec)
	return i 

i = fun_mpr121(20)
print(i)
