import time
from datetime import datetime,timedelta
import board
import busio

# Import MPR121 module.
import adafruit_mpr121

# Create I2C bus.
i2c = busio.I2C(board.SCL, board.SDA)

# Create MPR121 object.
mpr121 = adafruit_mpr121.MPR121(i2c)

# Note you can optionally change the address of the device:
# mpr121 = adafruit_mpr121.MPR121(i2c, address=0x91)

# Loop forever testing each input and printing when they're touched.
begtime = datetime.now()
maxtime = datetime.now()+timedelta(seconds=50)

while datetime.now() < maxtime:
# Loop through all 12 inputs (0-11).
	print('tim {}'.format(begtime))
	for i in range(4):
	# Call is_touched and pass it then number of the input.  If it's touched
	# it will return True, otherwise it will return False.
		if mpr121[i].value:
			print("Input {} touched!".format(i))
			break
		else:
			print("no_touch")
	time.sleep(0.25)  # Small delay to keep from spamming output messages.
print("end")
