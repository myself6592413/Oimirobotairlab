#!/usr/bin/python
# OK WORKD SETTEMBRE 2020
##############################################################
from evdev import InputDevice, categorize, ecodes, KeyEvent, device, UInput, AbsInfo

cap = {
    ecodes.EV_KEY : [ecodes.KEY_A, ecodes.KEY_B],
    ecodes.EV_ABS : [
    (ecodes.ABS_X, AbsInfo(value=0, min=0, max=255,
    fuzz=0, flat=0, resolution=0)),
    (ecodes.ABS_Y, AbsInfo(0, 0, 255, 0, 0, 0)),
    (ecodes.ABS_MT_POSITION_X, (0, 128, 255, 0)) ]
}

# Open the device
gamepad = InputDevice('/dev/input/event0')

for event in gamepad.read_loop():
    print("enter in loop read")
    if event.type == ecodes.EV_KEY:
        keyevent = categorize(event)
        print("keyevent is = ", keyevent)
        if keyevent.keystate == KeyEvent.key_down:
            # BTN_A comes in a tuple
            if keyevent.keycode[0] == 'BTN_A':
                print ("Back")
            elif keyevent.keycode[0] == 'BTN_WEST':
                print ("Forward")
            elif keyevent.keycode[0] == 'BTN_B':
                print ("Right")
            elif keyevent.keycode[0] == 'BTN_NORTH':
                print ("Left")
            elif keyevent.keycode[0] == 'BTN_THUMBR' or keyevent.keycode == 'BTN_THUMBL':
                print ("Stop")
            elif keyevent.keycode    == 'BTN_TR':
                print ("Faster")
            elif keyevent.keycode    == 'BTN_TL':
                print ("Slower")
            elif keyevent.keycode == 'BTN_START':
                print ("start")
            elif keyevent.keycode == 'BTN_SELECT':
                print ("Select")
    if event.type == ecodes.EV_ABS:
        absevent = categorize(event)
        result = ecodes.bytype[absevent.event.type][absevent.event.code]
        value = absevent.event.value
        if result == 'ABS_RZ' and value > 250:
            print("R2 DESTRO!")
