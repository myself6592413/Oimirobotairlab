#!/usr/bin/python

from select import select
from evdev import InputDevice, categorize, ecodes, KeyEvent, device
n=0
lista = []
gamepad = InputDevice('/dev/input/event2')
result = None
for event in gamepad.read_loop():
    if n < 12229990000:
        if not event.type:
            print ("result is none?  ", result)
        if event.type == ecodes.EV_KEY:
            print("stavolta è key")
            keyevent = categorize(event)
            if keyevent.keystate == KeyEvent.key_down:
                print(keyevent.keycode)
                print(event.sec)
                print(event.timestamp)
        elif event.type == ecodes.EV_ABS:
            print("stavolta è abs")
            absevent = categorize(event)
            result = ecodes.bytype[absevent.event.type][absevent.event.code]
            value = absevent.event.value
            #print(ecodes.bytype[absevent.event.type][absevent.event.code], absevent.event.value)
            print(result)
            print(value)
            #print(len(result))
            lista.append(value)
            #print(device.AbsInfo.value)
            #print(lista)
            n+=1
            lista = []

## other possibility with asyncio 

'''import asyncio
from evdev import InputDevice, categorize, ecodes

dev = InputDevice('/dev/input/event0')

async def helper(dev):
    async for ev in dev.async_read_loop():
        print(repr(ev))

loop = asyncio.get_event_loop()
loop.run_until_complete(helper(dev))'''
