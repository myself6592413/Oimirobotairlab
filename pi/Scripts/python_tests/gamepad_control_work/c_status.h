using namespace std;

enum class global_status {beginning, busy, waiting, moving, playing1, testing1, inserting1, playing2, testing2, inserting2, feeling, reacting, doing_nothing};
