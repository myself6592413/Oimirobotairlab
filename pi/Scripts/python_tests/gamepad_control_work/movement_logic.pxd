# ===================================== # ===================================== #
# Extern Imports
# ===================================== # ===================================== #
cdef extern from "Python.h":
	object PyList(float *s, Py_ssize_t leng)

cdef extern from "c_speedVec.h":
	float* setStrafeForward(float module, float xp, float yp)
	float setAngular(float rot)
	unsigned char* transformBytes(float strafe,float forward,float angular)