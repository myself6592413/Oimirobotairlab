#include <vector>
#define MINSTRAFE 0.01
#define MINFORWARD 0.01
#define MINANGULAR 0.1

float* setStrafeForward(float module, float xp, float yp){
        float *arra = new float[3];
        float strafe = 0.0;
        float forward = 0.0;
        float angular = 0.0;

        if(xp==0 && yp==0){
            strafe = 0.0;
            forward =0.0;
            arra[0] = strafe;
            arra[1] = forward;
            arra[2] = angular;
        }
        float angle = atan2(yp, xp);

        strafe = cos(angle)*module;
        forward = sin(angle)*module;

        if(strafe < MINSTRAFE && strafe > -MINSTRAFE)
           strafe = 0.0;
        if(forward < MINFORWARD && forward > -MINFORWARD)
        	forward = 0.0;

        arra[0] = strafe;
        arra[1] = forward;
        arra[2] = angular;

        return arra;
    }

float setAngular(float rot){
   float angular = rot;
   if(angular < MINANGULAR && angular > -MINANGULAR)
      angular = 0;
    }

unsigned char* transformBytes(float strafe,float forward,float angular){
	unsigned char *byteArray = new unsigned char[4];
	byteArray[0]=(char)(floor(50*strafe)+50);
	byteArray[1]=(char)(floor(50*forward)+50);
	byteArray[2]=(char)(floor(50*angular)+50);
	byteArray[3]='n';

	return byteArray;
  }

/*
std::vector<uint8_t> transformBytes(float strafe,float forward,float angular){
	std::vector<uint8_t> byteArray;
	byteArray[0]=(char)(floor(50*strafe)+50);
	byteArray[1]=(char)(floor(50*forward)+50);
	byteArray[2]=(char)(floor(50*angular)+50);
	byteArray[3]='n';
   	return byteArray
}
*/
