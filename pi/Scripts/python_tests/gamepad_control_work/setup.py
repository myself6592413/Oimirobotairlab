from distutils.core import setup
import setuptools
from distutils.extension import Extension
from Cython.Build import build_ext
from Cython.Build import cythonize
import numpy,time

ext_modules = [
	Extension("movement_logic", ["movement_logic.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
        extra_link_args=['-fopenmp','-ffast-math','-march=native'],
        include_dirs=[numpy.get_include()],
        language='c++'),
	Extension("movement_manager", ["movement_manager.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
        extra_link_args=['-fopenmp','-ffast-math','-march=native'],
        include_dirs=[numpy.get_include()],
        language='c++'),
	Extension("shared", ["shared.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
        extra_link_args=['-fopenmp','-ffast-math','-march=native'],
        include_dirs=[numpy.get_include()],
        language='c++'),
	Extension("serialmanager", ["serialmanager.pyx"],
		extra_compile_args=['-O3','-w','-fopenmp'],
        extra_link_args=['-fopenmp','-ffast-math','-march=native'],
        include_dirs=[numpy.get_include()],
        language='c++'),
]

class BuildExt(build_ext):
    def build_extensions(self):
        if '-Wstrict-prototypes' in self.compiler.compiler_so:
            self.compiler.compiler_so.remove('-Wstrict-prototypes')
        super().build_extensions()

for e in ext_modules:
    e.cython_directives = {'embedsignature': True,'boundscheck': False,'wraparound': False,'linetrace': True, 'language_level': "3"}

setup(
    name='gamepad_manager',
    ext_modules=ext_modules,
    cmdclass = {'build_ext': BuildExt},
    )
