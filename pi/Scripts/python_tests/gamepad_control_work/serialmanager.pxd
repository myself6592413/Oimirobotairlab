# ===================================== # ===================================== #
# Extern Imports
# ===================================== # ===================================== #
cdef extern from "Python.h":
	object PyList(float *s, Py_ssize_t leng)

cdef extern from "c_methods.h":
	char * createMsg(char * comm, float strafe, float forward, float angular)
