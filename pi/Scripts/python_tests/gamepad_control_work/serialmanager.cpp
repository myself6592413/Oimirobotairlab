#ifndef PY_SSIZE_T_CLEAN
#define PY_SSIZE_T_CLEAN
#endif /* PY_SSIZE_T_CLEAN */
#include "Python.h"
#ifndef Py_PYTHON_H
    #error Python headers needed to compile C extensions, please install development version of Python.
#elif PY_VERSION_HEX < 0x02060000 || (0x03000000 <= PY_VERSION_HEX && PY_VERSION_HEX < 0x03030000)
    #error Cython requires Python 2.6+ or Python 3.3+.
#else
#define CYTHON_ABI "0_29_32"
#define CYTHON_HEX_VERSION 0x001D20F0
#define CYTHON_FUTURE_DIVISION 1
#include <stddef.h>
#ifndef offsetof
  #define offsetof(type, member) ( (size_t) & ((type*)0) -> member )
#endif
#if !defined(WIN32) && !defined(MS_WINDOWS)
  #ifndef __stdcall
    #define __stdcall
  #endif
  #ifndef __cdecl
    #define __cdecl
  #endif
  #ifndef __fastcall
    #define __fastcall
  #endif
#endif
#ifndef DL_IMPORT
  #define DL_IMPORT(t) t
#endif
#ifndef DL_EXPORT
  #define DL_EXPORT(t) t
#endif
#define __PYX_COMMA ,
#ifndef HAVE_LONG_LONG
  #if PY_VERSION_HEX >= 0x02070000
    #define HAVE_LONG_LONG
  #endif
#endif
#ifndef PY_LONG_LONG
  #define PY_LONG_LONG LONG_LONG
#endif
#ifndef Py_HUGE_VAL
  #define Py_HUGE_VAL HUGE_VAL
#endif
#ifdef PYPY_VERSION
  #define CYTHON_COMPILING_IN_PYPY 1
  #define CYTHON_COMPILING_IN_PYSTON 0
  #define CYTHON_COMPILING_IN_CPYTHON 0
  #define CYTHON_COMPILING_IN_NOGIL 0
  #undef CYTHON_USE_TYPE_SLOTS
  #define CYTHON_USE_TYPE_SLOTS 0
  #undef CYTHON_USE_PYTYPE_LOOKUP
  #define CYTHON_USE_PYTYPE_LOOKUP 0
  #if PY_VERSION_HEX < 0x03050000
    #undef CYTHON_USE_ASYNC_SLOTS
    #define CYTHON_USE_ASYNC_SLOTS 0
  #elif !defined(CYTHON_USE_ASYNC_SLOTS)
    #define CYTHON_USE_ASYNC_SLOTS 1
  #endif
  #undef CYTHON_USE_PYLIST_INTERNALS
  #define CYTHON_USE_PYLIST_INTERNALS 0
  #undef CYTHON_USE_UNICODE_INTERNALS
  #define CYTHON_USE_UNICODE_INTERNALS 0
  #undef CYTHON_USE_UNICODE_WRITER
  #define CYTHON_USE_UNICODE_WRITER 0
  #undef CYTHON_USE_PYLONG_INTERNALS
  #define CYTHON_USE_PYLONG_INTERNALS 0
  #undef CYTHON_AVOID_BORROWED_REFS
  #define CYTHON_AVOID_BORROWED_REFS 1
  #undef CYTHON_ASSUME_SAFE_MACROS
  #define CYTHON_ASSUME_SAFE_MACROS 0
  #undef CYTHON_UNPACK_METHODS
  #define CYTHON_UNPACK_METHODS 0
  #undef CYTHON_FAST_THREAD_STATE
  #define CYTHON_FAST_THREAD_STATE 0
  #undef CYTHON_FAST_PYCALL
  #define CYTHON_FAST_PYCALL 0
  #undef CYTHON_PEP489_MULTI_PHASE_INIT
  #define CYTHON_PEP489_MULTI_PHASE_INIT 0
  #undef CYTHON_USE_TP_FINALIZE
  #define CYTHON_USE_TP_FINALIZE 0
  #undef CYTHON_USE_DICT_VERSIONS
  #define CYTHON_USE_DICT_VERSIONS 0
  #undef CYTHON_USE_EXC_INFO_STACK
  #define CYTHON_USE_EXC_INFO_STACK 0
  #ifndef CYTHON_UPDATE_DESCRIPTOR_DOC
    #define CYTHON_UPDATE_DESCRIPTOR_DOC (PYPY_VERSION_HEX >= 0x07030900)
  #endif
#elif defined(PYSTON_VERSION)
  #define CYTHON_COMPILING_IN_PYPY 0
  #define CYTHON_COMPILING_IN_PYSTON 1
  #define CYTHON_COMPILING_IN_CPYTHON 0
  #define CYTHON_COMPILING_IN_NOGIL 0
  #ifndef CYTHON_USE_TYPE_SLOTS
    #define CYTHON_USE_TYPE_SLOTS 1
  #endif
  #undef CYTHON_USE_PYTYPE_LOOKUP
  #define CYTHON_USE_PYTYPE_LOOKUP 0
  #undef CYTHON_USE_ASYNC_SLOTS
  #define CYTHON_USE_ASYNC_SLOTS 0
  #undef CYTHON_USE_PYLIST_INTERNALS
  #define CYTHON_USE_PYLIST_INTERNALS 0
  #ifndef CYTHON_USE_UNICODE_INTERNALS
    #define CYTHON_USE_UNICODE_INTERNALS 1
  #endif
  #undef CYTHON_USE_UNICODE_WRITER
  #define CYTHON_USE_UNICODE_WRITER 0
  #undef CYTHON_USE_PYLONG_INTERNALS
  #define CYTHON_USE_PYLONG_INTERNALS 0
  #ifndef CYTHON_AVOID_BORROWED_REFS
    #define CYTHON_AVOID_BORROWED_REFS 0
  #endif
  #ifndef CYTHON_ASSUME_SAFE_MACROS
    #define CYTHON_ASSUME_SAFE_MACROS 1
  #endif
  #ifndef CYTHON_UNPACK_METHODS
    #define CYTHON_UNPACK_METHODS 1
  #endif
  #undef CYTHON_FAST_THREAD_STATE
  #define CYTHON_FAST_THREAD_STATE 0
  #undef CYTHON_FAST_PYCALL
  #define CYTHON_FAST_PYCALL 0
  #undef CYTHON_PEP489_MULTI_PHASE_INIT
  #define CYTHON_PEP489_MULTI_PHASE_INIT 0
  #undef CYTHON_USE_TP_FINALIZE
  #define CYTHON_USE_TP_FINALIZE 0
  #undef CYTHON_USE_DICT_VERSIONS
  #define CYTHON_USE_DICT_VERSIONS 0
  #undef CYTHON_USE_EXC_INFO_STACK
  #define CYTHON_USE_EXC_INFO_STACK 0
  #ifndef CYTHON_UPDATE_DESCRIPTOR_DOC
    #define CYTHON_UPDATE_DESCRIPTOR_DOC 0
  #endif
#elif defined(PY_NOGIL)
  #define CYTHON_COMPILING_IN_PYPY 0
  #define CYTHON_COMPILING_IN_PYSTON 0
  #define CYTHON_COMPILING_IN_CPYTHON 0
  #define CYTHON_COMPILING_IN_NOGIL 1
  #ifndef CYTHON_USE_TYPE_SLOTS
    #define CYTHON_USE_TYPE_SLOTS 1
  #endif
  #undef CYTHON_USE_PYTYPE_LOOKUP
  #define CYTHON_USE_PYTYPE_LOOKUP 0
  #ifndef CYTHON_USE_ASYNC_SLOTS
    #define CYTHON_USE_ASYNC_SLOTS 1
  #endif
  #undef CYTHON_USE_PYLIST_INTERNALS
  #define CYTHON_USE_PYLIST_INTERNALS 0
  #ifndef CYTHON_USE_UNICODE_INTERNALS
    #define CYTHON_USE_UNICODE_INTERNALS 1
  #endif
  #undef CYTHON_USE_UNICODE_WRITER
  #define CYTHON_USE_UNICODE_WRITER 0
  #undef CYTHON_USE_PYLONG_INTERNALS
  #define CYTHON_USE_PYLONG_INTERNALS 0
  #ifndef CYTHON_AVOID_BORROWED_REFS
    #define CYTHON_AVOID_BORROWED_REFS 0
  #endif
  #ifndef CYTHON_ASSUME_SAFE_MACROS
    #define CYTHON_ASSUME_SAFE_MACROS 1
  #endif
  #ifndef CYTHON_UNPACK_METHODS
    #define CYTHON_UNPACK_METHODS 1
  #endif
  #undef CYTHON_FAST_THREAD_STATE
  #define CYTHON_FAST_THREAD_STATE 0
  #undef CYTHON_FAST_PYCALL
  #define CYTHON_FAST_PYCALL 0
  #ifndef CYTHON_PEP489_MULTI_PHASE_INIT
    #define CYTHON_PEP489_MULTI_PHASE_INIT 1
  #endif
  #ifndef CYTHON_USE_TP_FINALIZE
    #define CYTHON_USE_TP_FINALIZE 1
  #endif
  #undef CYTHON_USE_DICT_VERSIONS
  #define CYTHON_USE_DICT_VERSIONS 0
  #undef CYTHON_USE_EXC_INFO_STACK
  #define CYTHON_USE_EXC_INFO_STACK 0
#else
  #define CYTHON_COMPILING_IN_PYPY 0
  #define CYTHON_COMPILING_IN_PYSTON 0
  #define CYTHON_COMPILING_IN_CPYTHON 1
  #define CYTHON_COMPILING_IN_NOGIL 0
  #ifndef CYTHON_USE_TYPE_SLOTS
    #define CYTHON_USE_TYPE_SLOTS 1
  #endif
  #if PY_VERSION_HEX < 0x02070000
    #undef CYTHON_USE_PYTYPE_LOOKUP
    #define CYTHON_USE_PYTYPE_LOOKUP 0
  #elif !defined(CYTHON_USE_PYTYPE_LOOKUP)
    #define CYTHON_USE_PYTYPE_LOOKUP 1
  #endif
  #if PY_MAJOR_VERSION < 3
    #undef CYTHON_USE_ASYNC_SLOTS
    #define CYTHON_USE_ASYNC_SLOTS 0
  #elif !defined(CYTHON_USE_ASYNC_SLOTS)
    #define CYTHON_USE_ASYNC_SLOTS 1
  #endif
  #if PY_VERSION_HEX < 0x02070000
    #undef CYTHON_USE_PYLONG_INTERNALS
    #define CYTHON_USE_PYLONG_INTERNALS 0
  #elif !defined(CYTHON_USE_PYLONG_INTERNALS)
    #define CYTHON_USE_PYLONG_INTERNALS 1
  #endif
  #ifndef CYTHON_USE_PYLIST_INTERNALS
    #define CYTHON_USE_PYLIST_INTERNALS 1
  #endif
  #ifndef CYTHON_USE_UNICODE_INTERNALS
    #define CYTHON_USE_UNICODE_INTERNALS 1
  #endif
  #if PY_VERSION_HEX < 0x030300F0 || PY_VERSION_HEX >= 0x030B00A2
    #undef CYTHON_USE_UNICODE_WRITER
    #define CYTHON_USE_UNICODE_WRITER 0
  #elif !defined(CYTHON_USE_UNICODE_WRITER)
    #define CYTHON_USE_UNICODE_WRITER 1
  #endif
  #ifndef CYTHON_AVOID_BORROWED_REFS
    #define CYTHON_AVOID_BORROWED_REFS 0
  #endif
  #ifndef CYTHON_ASSUME_SAFE_MACROS
    #define CYTHON_ASSUME_SAFE_MACROS 1
  #endif
  #ifndef CYTHON_UNPACK_METHODS
    #define CYTHON_UNPACK_METHODS 1
  #endif
  #if PY_VERSION_HEX >= 0x030B00A4
    #undef CYTHON_FAST_THREAD_STATE
    #define CYTHON_FAST_THREAD_STATE 0
  #elif !defined(CYTHON_FAST_THREAD_STATE)
    #define CYTHON_FAST_THREAD_STATE 1
  #endif
  #ifndef CYTHON_FAST_PYCALL
    #define CYTHON_FAST_PYCALL (PY_VERSION_HEX < 0x030A0000)
  #endif
  #ifndef CYTHON_PEP489_MULTI_PHASE_INIT
    #define CYTHON_PEP489_MULTI_PHASE_INIT (PY_VERSION_HEX >= 0x03050000)
  #endif
  #ifndef CYTHON_USE_TP_FINALIZE
    #define CYTHON_USE_TP_FINALIZE (PY_VERSION_HEX >= 0x030400a1)
  #endif
  #ifndef CYTHON_USE_DICT_VERSIONS
    #define CYTHON_USE_DICT_VERSIONS (PY_VERSION_HEX >= 0x030600B1)
  #endif
  #if PY_VERSION_HEX >= 0x030B00A4
    #undef CYTHON_USE_EXC_INFO_STACK
    #define CYTHON_USE_EXC_INFO_STACK 0
  #elif !defined(CYTHON_USE_EXC_INFO_STACK)
    #define CYTHON_USE_EXC_INFO_STACK (PY_VERSION_HEX >= 0x030700A3)
  #endif
  #ifndef CYTHON_UPDATE_DESCRIPTOR_DOC
    #define CYTHON_UPDATE_DESCRIPTOR_DOC 1
  #endif
#endif
#if !defined(CYTHON_FAST_PYCCALL)
#define CYTHON_FAST_PYCCALL  (CYTHON_FAST_PYCALL && PY_VERSION_HEX >= 0x030600B1)
#endif
#if CYTHON_USE_PYLONG_INTERNALS
  #if PY_MAJOR_VERSION < 3
    #include "longintrepr.h"
  #endif
  #undef SHIFT
  #undef BASE
  #undef MASK
  #ifdef SIZEOF_VOID_P
    enum { __pyx_check_sizeof_voidp = 1 / (int)(SIZEOF_VOID_P == sizeof(void*)) };
  #endif
#endif
#ifndef __has_attribute
  #define __has_attribute(x) 0
#endif
#ifndef __has_cpp_attribute
  #define __has_cpp_attribute(x) 0
#endif
#ifndef CYTHON_RESTRICT
  #if defined(__GNUC__)
    #define CYTHON_RESTRICT __restrict__
  #elif defined(_MSC_VER) && _MSC_VER >= 1400
    #define CYTHON_RESTRICT __restrict
  #elif defined (__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
    #define CYTHON_RESTRICT restrict
  #else
    #define CYTHON_RESTRICT
  #endif
#endif
#ifndef CYTHON_UNUSED
# if defined(__GNUC__)
#   if !(defined(__cplusplus)) || (__GNUC__ > 3 || (__GNUC__ == 3 && __GNUC_MINOR__ >= 4))
#     define CYTHON_UNUSED __attribute__ ((__unused__))
#   else
#     define CYTHON_UNUSED
#   endif
# elif defined(__ICC) || (defined(__INTEL_COMPILER) && !defined(_MSC_VER))
#   define CYTHON_UNUSED __attribute__ ((__unused__))
# else
#   define CYTHON_UNUSED
# endif
#endif
#ifndef CYTHON_MAYBE_UNUSED_VAR
#  if defined(__cplusplus)
     template<class T> void CYTHON_MAYBE_UNUSED_VAR( const T& ) { }
#  else
#    define CYTHON_MAYBE_UNUSED_VAR(x) (void)(x)
#  endif
#endif
#ifndef CYTHON_NCP_UNUSED
# if CYTHON_COMPILING_IN_CPYTHON
#  define CYTHON_NCP_UNUSED
# else
#  define CYTHON_NCP_UNUSED CYTHON_UNUSED
# endif
#endif
#define __Pyx_void_to_None(void_result) ((void)(void_result), Py_INCREF(Py_None), Py_None)
#ifdef _MSC_VER
    #ifndef _MSC_STDINT_H_
        #if _MSC_VER < 1300
           typedef unsigned char     uint8_t;
           typedef unsigned int      uint32_t;
        #else
           typedef unsigned __int8   uint8_t;
           typedef unsigned __int32  uint32_t;
        #endif
    #endif
#else
   #include <stdint.h>
#endif
#ifndef CYTHON_FALLTHROUGH
  #if defined(__cplusplus) && __cplusplus >= 201103L
    #if __has_cpp_attribute(fallthrough)
      #define CYTHON_FALLTHROUGH [[fallthrough]]
    #elif __has_cpp_attribute(clang::fallthrough)
      #define CYTHON_FALLTHROUGH [[clang::fallthrough]]
    #elif __has_cpp_attribute(gnu::fallthrough)
      #define CYTHON_FALLTHROUGH [[gnu::fallthrough]]
    #endif
  #endif
  #ifndef CYTHON_FALLTHROUGH
    #if __has_attribute(fallthrough)
      #define CYTHON_FALLTHROUGH __attribute__((fallthrough))
    #else
      #define CYTHON_FALLTHROUGH
    #endif
  #endif
  #if defined(__clang__ ) && defined(__apple_build_version__)
    #if __apple_build_version__ < 7000000
      #undef  CYTHON_FALLTHROUGH
      #define CYTHON_FALLTHROUGH
    #endif
  #endif
#endif

#ifndef __cplusplus
  #error "Cython files generated with the C++ option must be compiled with a C++ compiler."
#endif
#ifndef CYTHON_INLINE
  #if defined(__clang__)
    #define CYTHON_INLINE __inline__ __attribute__ ((__unused__))
  #else
    #define CYTHON_INLINE inline
  #endif
#endif
template<typename T>
void __Pyx_call_destructor(T& x) {
    x.~T();
}
template<typename T>
class __Pyx_FakeReference {
  public:
    __Pyx_FakeReference() : ptr(NULL) { }
    __Pyx_FakeReference(const T& ref) : ptr(const_cast<T*>(&ref)) { }
    T *operator->() { return ptr; }
    T *operator&() { return ptr; }
    operator T&() { return *ptr; }
    template<typename U> bool operator ==(U other) { return *ptr == other; }
    template<typename U> bool operator !=(U other) { return *ptr != other; }
  private:
    T *ptr;
};

#if CYTHON_COMPILING_IN_PYPY && PY_VERSION_HEX < 0x02070600 && !defined(Py_OptimizeFlag)
  #define Py_OptimizeFlag 0
#endif
#define __PYX_BUILD_PY_SSIZE_T "n"
#define CYTHON_FORMAT_SSIZE_T "z"
#if PY_MAJOR_VERSION < 3
  #define __Pyx_BUILTIN_MODULE_NAME "__builtin__"
  #define __Pyx_PyCode_New(a, k, l, s, f, code, c, n, v, fv, cell, fn, name, fline, lnos)\
          PyCode_New(a+k, l, s, f, code, c, n, v, fv, cell, fn, name, fline, lnos)
  #define __Pyx_DefaultClassType PyClass_Type
#else
  #define __Pyx_BUILTIN_MODULE_NAME "builtins"
  #define __Pyx_DefaultClassType PyType_Type
#if PY_VERSION_HEX >= 0x030B00A1
    static CYTHON_INLINE PyCodeObject* __Pyx_PyCode_New(int a, int k, int l, int s, int f,
                                                    PyObject *code, PyObject *c, PyObject* n, PyObject *v,
                                                    PyObject *fv, PyObject *cell, PyObject* fn,
                                                    PyObject *name, int fline, PyObject *lnos) {
        PyObject *kwds=NULL, *argcount=NULL, *posonlyargcount=NULL, *kwonlyargcount=NULL;
        PyObject *nlocals=NULL, *stacksize=NULL, *flags=NULL, *replace=NULL, *call_result=NULL, *empty=NULL;
        const char *fn_cstr=NULL;
        const char *name_cstr=NULL;
        PyCodeObject* co=NULL;
        PyObject *type, *value, *traceback;
        PyErr_Fetch(&type, &value, &traceback);
        if (!(kwds=PyDict_New())) goto end;
        if (!(argcount=PyLong_FromLong(a))) goto end;
        if (PyDict_SetItemString(kwds, "co_argcount", argcount) != 0) goto end;
        if (!(posonlyargcount=PyLong_FromLong(0))) goto end;
        if (PyDict_SetItemString(kwds, "co_posonlyargcount", posonlyargcount) != 0) goto end;
        if (!(kwonlyargcount=PyLong_FromLong(k))) goto end;
        if (PyDict_SetItemString(kwds, "co_kwonlyargcount", kwonlyargcount) != 0) goto end;
        if (!(nlocals=PyLong_FromLong(l))) goto end;
        if (PyDict_SetItemString(kwds, "co_nlocals", nlocals) != 0) goto end;
        if (!(stacksize=PyLong_FromLong(s))) goto end;
        if (PyDict_SetItemString(kwds, "co_stacksize", stacksize) != 0) goto end;
        if (!(flags=PyLong_FromLong(f))) goto end;
        if (PyDict_SetItemString(kwds, "co_flags", flags) != 0) goto end;
        if (PyDict_SetItemString(kwds, "co_code", code) != 0) goto end;
        if (PyDict_SetItemString(kwds, "co_consts", c) != 0) goto end;
        if (PyDict_SetItemString(kwds, "co_names", n) != 0) goto end;
        if (PyDict_SetItemString(kwds, "co_varnames", v) != 0) goto end;
        if (PyDict_SetItemString(kwds, "co_freevars", fv) != 0) goto end;
        if (PyDict_SetItemString(kwds, "co_cellvars", cell) != 0) goto end;
        if (PyDict_SetItemString(kwds, "co_linetable", lnos) != 0) goto end;
        if (!(fn_cstr=PyUnicode_AsUTF8AndSize(fn, NULL))) goto end;
        if (!(name_cstr=PyUnicode_AsUTF8AndSize(name, NULL))) goto end;
        if (!(co = PyCode_NewEmpty(fn_cstr, name_cstr, fline))) goto end;
        if (!(replace = PyObject_GetAttrString((PyObject*)co, "replace"))) goto cleanup_code_too;
        if (!(empty = PyTuple_New(0))) goto cleanup_code_too; // unfortunately __pyx_empty_tuple isn't available here
        if (!(call_result = PyObject_Call(replace, empty, kwds))) goto cleanup_code_too;
        Py_XDECREF((PyObject*)co);
        co = (PyCodeObject*)call_result;
        call_result = NULL;
        if (0) {
            cleanup_code_too:
            Py_XDECREF((PyObject*)co);
            co = NULL;
        }
        end:
        Py_XDECREF(kwds);
        Py_XDECREF(argcount);
        Py_XDECREF(posonlyargcount);
        Py_XDECREF(kwonlyargcount);
        Py_XDECREF(nlocals);
        Py_XDECREF(stacksize);
        Py_XDECREF(replace);
        Py_XDECREF(call_result);
        Py_XDECREF(empty);
        if (type) {
            PyErr_Restore(type, value, traceback);
        }
        return co;
    }
#else
  #define __Pyx_PyCode_New(a, k, l, s, f, code, c, n, v, fv, cell, fn, name, fline, lnos)\
          PyCode_New(a, k, l, s, f, code, c, n, v, fv, cell, fn, name, fline, lnos)
#endif
  #define __Pyx_DefaultClassType PyType_Type
#endif
#ifndef Py_TPFLAGS_CHECKTYPES
  #define Py_TPFLAGS_CHECKTYPES 0
#endif
#ifndef Py_TPFLAGS_HAVE_INDEX
  #define Py_TPFLAGS_HAVE_INDEX 0
#endif
#ifndef Py_TPFLAGS_HAVE_NEWBUFFER
  #define Py_TPFLAGS_HAVE_NEWBUFFER 0
#endif
#ifndef Py_TPFLAGS_HAVE_FINALIZE
  #define Py_TPFLAGS_HAVE_FINALIZE 0
#endif
#ifndef METH_STACKLESS
  #define METH_STACKLESS 0
#endif
#if PY_VERSION_HEX <= 0x030700A3 || !defined(METH_FASTCALL)
  #ifndef METH_FASTCALL
     #define METH_FASTCALL 0x80
  #endif
  typedef PyObject *(*__Pyx_PyCFunctionFast) (PyObject *self, PyObject *const *args, Py_ssize_t nargs);
  typedef PyObject *(*__Pyx_PyCFunctionFastWithKeywords) (PyObject *self, PyObject *const *args,
                                                          Py_ssize_t nargs, PyObject *kwnames);
#else
  #define __Pyx_PyCFunctionFast _PyCFunctionFast
  #define __Pyx_PyCFunctionFastWithKeywords _PyCFunctionFastWithKeywords
#endif
#if CYTHON_FAST_PYCCALL
#define __Pyx_PyFastCFunction_Check(func)\
    ((PyCFunction_Check(func) && (METH_FASTCALL == (PyCFunction_GET_FLAGS(func) & ~(METH_CLASS | METH_STATIC | METH_COEXIST | METH_KEYWORDS | METH_STACKLESS)))))
#else
#define __Pyx_PyFastCFunction_Check(func) 0
#endif
#if CYTHON_COMPILING_IN_PYPY && !defined(PyObject_Malloc)
  #define PyObject_Malloc(s)   PyMem_Malloc(s)
  #define PyObject_Free(p)     PyMem_Free(p)
  #define PyObject_Realloc(p)  PyMem_Realloc(p)
#endif
#if CYTHON_COMPILING_IN_CPYTHON && PY_VERSION_HEX < 0x030400A1
  #define PyMem_RawMalloc(n)           PyMem_Malloc(n)
  #define PyMem_RawRealloc(p, n)       PyMem_Realloc(p, n)
  #define PyMem_RawFree(p)             PyMem_Free(p)
#endif
#if CYTHON_COMPILING_IN_PYSTON
  #define __Pyx_PyCode_HasFreeVars(co)  PyCode_HasFreeVars(co)
  #define __Pyx_PyFrame_SetLineNumber(frame, lineno) PyFrame_SetLineNumber(frame, lineno)
#else
  #define __Pyx_PyCode_HasFreeVars(co)  (PyCode_GetNumFree(co) > 0)
  #define __Pyx_PyFrame_SetLineNumber(frame, lineno)  (frame)->f_lineno = (lineno)
#endif
#if !CYTHON_FAST_THREAD_STATE || PY_VERSION_HEX < 0x02070000
  #define __Pyx_PyThreadState_Current PyThreadState_GET()
#elif PY_VERSION_HEX >= 0x03060000
  #define __Pyx_PyThreadState_Current _PyThreadState_UncheckedGet()
#elif PY_VERSION_HEX >= 0x03000000
  #define __Pyx_PyThreadState_Current PyThreadState_GET()
#else
  #define __Pyx_PyThreadState_Current _PyThreadState_Current
#endif
#if PY_VERSION_HEX < 0x030700A2 && !defined(PyThread_tss_create) && !defined(Py_tss_NEEDS_INIT)
#include "pythread.h"
#define Py_tss_NEEDS_INIT 0
typedef int Py_tss_t;
static CYTHON_INLINE int PyThread_tss_create(Py_tss_t *key) {
  *key = PyThread_create_key();
  return 0;
}
static CYTHON_INLINE Py_tss_t * PyThread_tss_alloc(void) {
  Py_tss_t *key = (Py_tss_t *)PyObject_Malloc(sizeof(Py_tss_t));
  *key = Py_tss_NEEDS_INIT;
  return key;
}
static CYTHON_INLINE void PyThread_tss_free(Py_tss_t *key) {
  PyObject_Free(key);
}
static CYTHON_INLINE int PyThread_tss_is_created(Py_tss_t *key) {
  return *key != Py_tss_NEEDS_INIT;
}
static CYTHON_INLINE void PyThread_tss_delete(Py_tss_t *key) {
  PyThread_delete_key(*key);
  *key = Py_tss_NEEDS_INIT;
}
static CYTHON_INLINE int PyThread_tss_set(Py_tss_t *key, void *value) {
  return PyThread_set_key_value(*key, value);
}
static CYTHON_INLINE void * PyThread_tss_get(Py_tss_t *key) {
  return PyThread_get_key_value(*key);
}
#endif
#if CYTHON_COMPILING_IN_CPYTHON || defined(_PyDict_NewPresized)
#define __Pyx_PyDict_NewPresized(n)  ((n <= 8) ? PyDict_New() : _PyDict_NewPresized(n))
#else
#define __Pyx_PyDict_NewPresized(n)  PyDict_New()
#endif
#if PY_MAJOR_VERSION >= 3 || CYTHON_FUTURE_DIVISION
  #define __Pyx_PyNumber_Divide(x,y)         PyNumber_TrueDivide(x,y)
  #define __Pyx_PyNumber_InPlaceDivide(x,y)  PyNumber_InPlaceTrueDivide(x,y)
#else
  #define __Pyx_PyNumber_Divide(x,y)         PyNumber_Divide(x,y)
  #define __Pyx_PyNumber_InPlaceDivide(x,y)  PyNumber_InPlaceDivide(x,y)
#endif
#if CYTHON_COMPILING_IN_CPYTHON && PY_VERSION_HEX >= 0x030500A1 && CYTHON_USE_UNICODE_INTERNALS
#define __Pyx_PyDict_GetItemStr(dict, name)  _PyDict_GetItem_KnownHash(dict, name, ((PyASCIIObject *) name)->hash)
#else
#define __Pyx_PyDict_GetItemStr(dict, name)  PyDict_GetItem(dict, name)
#endif
#if PY_VERSION_HEX > 0x03030000 && defined(PyUnicode_KIND)
  #define CYTHON_PEP393_ENABLED 1
  #if defined(PyUnicode_IS_READY)
  #define __Pyx_PyUnicode_READY(op)       (likely(PyUnicode_IS_READY(op)) ?\
                                              0 : _PyUnicode_Ready((PyObject *)(op)))
  #else
  #define __Pyx_PyUnicode_READY(op)       (0)
  #endif
  #define __Pyx_PyUnicode_GET_LENGTH(u)   PyUnicode_GET_LENGTH(u)
  #define __Pyx_PyUnicode_READ_CHAR(u, i) PyUnicode_READ_CHAR(u, i)
  #define __Pyx_PyUnicode_MAX_CHAR_VALUE(u)   PyUnicode_MAX_CHAR_VALUE(u)
  #define __Pyx_PyUnicode_KIND(u)         PyUnicode_KIND(u)
  #define __Pyx_PyUnicode_DATA(u)         PyUnicode_DATA(u)
  #define __Pyx_PyUnicode_READ(k, d, i)   PyUnicode_READ(k, d, i)
  #define __Pyx_PyUnicode_WRITE(k, d, i, ch)  PyUnicode_WRITE(k, d, i, ch)
  #if defined(PyUnicode_IS_READY) && defined(PyUnicode_GET_SIZE)
  #if CYTHON_COMPILING_IN_CPYTHON && PY_VERSION_HEX >= 0x03090000
  #define __Pyx_PyUnicode_IS_TRUE(u)      (0 != (likely(PyUnicode_IS_READY(u)) ? PyUnicode_GET_LENGTH(u) : ((PyCompactUnicodeObject *)(u))->wstr_length))
  #else
  #define __Pyx_PyUnicode_IS_TRUE(u)      (0 != (likely(PyUnicode_IS_READY(u)) ? PyUnicode_GET_LENGTH(u) : PyUnicode_GET_SIZE(u)))
  #endif
  #else
  #define __Pyx_PyUnicode_IS_TRUE(u)      (0 != PyUnicode_GET_LENGTH(u))
  #endif
#else
  #define CYTHON_PEP393_ENABLED 0
  #define PyUnicode_1BYTE_KIND  1
  #define PyUnicode_2BYTE_KIND  2
  #define PyUnicode_4BYTE_KIND  4
  #define __Pyx_PyUnicode_READY(op)       (0)
  #define __Pyx_PyUnicode_GET_LENGTH(u)   PyUnicode_GET_SIZE(u)
  #define __Pyx_PyUnicode_READ_CHAR(u, i) ((Py_UCS4)(PyUnicode_AS_UNICODE(u)[i]))
  #define __Pyx_PyUnicode_MAX_CHAR_VALUE(u)   ((sizeof(Py_UNICODE) == 2) ? 65535 : 1114111)
  #define __Pyx_PyUnicode_KIND(u)         (sizeof(Py_UNICODE))
  #define __Pyx_PyUnicode_DATA(u)         ((void*)PyUnicode_AS_UNICODE(u))
  #define __Pyx_PyUnicode_READ(k, d, i)   ((void)(k), (Py_UCS4)(((Py_UNICODE*)d)[i]))
  #define __Pyx_PyUnicode_WRITE(k, d, i, ch)  (((void)(k)), ((Py_UNICODE*)d)[i] = ch)
  #define __Pyx_PyUnicode_IS_TRUE(u)      (0 != PyUnicode_GET_SIZE(u))
#endif
#if CYTHON_COMPILING_IN_PYPY
  #define __Pyx_PyUnicode_Concat(a, b)      PyNumber_Add(a, b)
  #define __Pyx_PyUnicode_ConcatSafe(a, b)  PyNumber_Add(a, b)
#else
  #define __Pyx_PyUnicode_Concat(a, b)      PyUnicode_Concat(a, b)
  #define __Pyx_PyUnicode_ConcatSafe(a, b)  ((unlikely((a) == Py_None) || unlikely((b) == Py_None)) ?\
      PyNumber_Add(a, b) : __Pyx_PyUnicode_Concat(a, b))
#endif
#if CYTHON_COMPILING_IN_PYPY && !defined(PyUnicode_Contains)
  #define PyUnicode_Contains(u, s)  PySequence_Contains(u, s)
#endif
#if CYTHON_COMPILING_IN_PYPY && !defined(PyByteArray_Check)
  #define PyByteArray_Check(obj)  PyObject_TypeCheck(obj, &PyByteArray_Type)
#endif
#if CYTHON_COMPILING_IN_PYPY && !defined(PyObject_Format)
  #define PyObject_Format(obj, fmt)  PyObject_CallMethod(obj, "__format__", "O", fmt)
#endif
#define __Pyx_PyString_FormatSafe(a, b)   ((unlikely((a) == Py_None || (PyString_Check(b) && !PyString_CheckExact(b)))) ? PyNumber_Remainder(a, b) : __Pyx_PyString_Format(a, b))
#define __Pyx_PyUnicode_FormatSafe(a, b)  ((unlikely((a) == Py_None || (PyUnicode_Check(b) && !PyUnicode_CheckExact(b)))) ? PyNumber_Remainder(a, b) : PyUnicode_Format(a, b))
#if PY_MAJOR_VERSION >= 3
  #define __Pyx_PyString_Format(a, b)  PyUnicode_Format(a, b)
#else
  #define __Pyx_PyString_Format(a, b)  PyString_Format(a, b)
#endif
#if PY_MAJOR_VERSION < 3 && !defined(PyObject_ASCII)
  #define PyObject_ASCII(o)            PyObject_Repr(o)
#endif
#if PY_MAJOR_VERSION >= 3
  #define PyBaseString_Type            PyUnicode_Type
  #define PyStringObject               PyUnicodeObject
  #define PyString_Type                PyUnicode_Type
  #define PyString_Check               PyUnicode_Check
  #define PyString_CheckExact          PyUnicode_CheckExact
#ifndef PyObject_Unicode
  #define PyObject_Unicode             PyObject_Str
#endif
#endif
#if PY_MAJOR_VERSION >= 3
  #define __Pyx_PyBaseString_Check(obj) PyUnicode_Check(obj)
  #define __Pyx_PyBaseString_CheckExact(obj) PyUnicode_CheckExact(obj)
#else
  #define __Pyx_PyBaseString_Check(obj) (PyString_Check(obj) || PyUnicode_Check(obj))
  #define __Pyx_PyBaseString_CheckExact(obj) (PyString_CheckExact(obj) || PyUnicode_CheckExact(obj))
#endif
#ifndef PySet_CheckExact
  #define PySet_CheckExact(obj)        (Py_TYPE(obj) == &PySet_Type)
#endif
#if PY_VERSION_HEX >= 0x030900A4
  #define __Pyx_SET_REFCNT(obj, refcnt) Py_SET_REFCNT(obj, refcnt)
  #define __Pyx_SET_SIZE(obj, size) Py_SET_SIZE(obj, size)
#else
  #define __Pyx_SET_REFCNT(obj, refcnt) Py_REFCNT(obj) = (refcnt)
  #define __Pyx_SET_SIZE(obj, size) Py_SIZE(obj) = (size)
#endif
#if CYTHON_ASSUME_SAFE_MACROS
  #define __Pyx_PySequence_SIZE(seq)  Py_SIZE(seq)
#else
  #define __Pyx_PySequence_SIZE(seq)  PySequence_Size(seq)
#endif
#if PY_MAJOR_VERSION >= 3
  #define PyIntObject                  PyLongObject
  #define PyInt_Type                   PyLong_Type
  #define PyInt_Check(op)              PyLong_Check(op)
  #define PyInt_CheckExact(op)         PyLong_CheckExact(op)
  #define PyInt_FromString             PyLong_FromString
  #define PyInt_FromUnicode            PyLong_FromUnicode
  #define PyInt_FromLong               PyLong_FromLong
  #define PyInt_FromSize_t             PyLong_FromSize_t
  #define PyInt_FromSsize_t            PyLong_FromSsize_t
  #define PyInt_AsLong                 PyLong_AsLong
  #define PyInt_AS_LONG                PyLong_AS_LONG
  #define PyInt_AsSsize_t              PyLong_AsSsize_t
  #define PyInt_AsUnsignedLongMask     PyLong_AsUnsignedLongMask
  #define PyInt_AsUnsignedLongLongMask PyLong_AsUnsignedLongLongMask
  #define PyNumber_Int                 PyNumber_Long
#endif
#if PY_MAJOR_VERSION >= 3
  #define PyBoolObject                 PyLongObject
#endif
#if PY_MAJOR_VERSION >= 3 && CYTHON_COMPILING_IN_PYPY
  #ifndef PyUnicode_InternFromString
    #define PyUnicode_InternFromString(s) PyUnicode_FromString(s)
  #endif
#endif
#if PY_VERSION_HEX < 0x030200A4
  typedef long Py_hash_t;
  #define __Pyx_PyInt_FromHash_t PyInt_FromLong
  #define __Pyx_PyInt_AsHash_t   __Pyx_PyIndex_AsHash_t
#else
  #define __Pyx_PyInt_FromHash_t PyInt_FromSsize_t
  #define __Pyx_PyInt_AsHash_t   __Pyx_PyIndex_AsSsize_t
#endif
#if PY_MAJOR_VERSION >= 3
  #define __Pyx_PyMethod_New(func, self, klass) ((self) ? ((void)(klass), PyMethod_New(func, self)) : __Pyx_NewRef(func))
#else
  #define __Pyx_PyMethod_New(func, self, klass) PyMethod_New(func, self, klass)
#endif
#if CYTHON_USE_ASYNC_SLOTS
  #if PY_VERSION_HEX >= 0x030500B1
    #define __Pyx_PyAsyncMethodsStruct PyAsyncMethods
    #define __Pyx_PyType_AsAsync(obj) (Py_TYPE(obj)->tp_as_async)
  #else
    #define __Pyx_PyType_AsAsync(obj) ((__Pyx_PyAsyncMethodsStruct*) (Py_TYPE(obj)->tp_reserved))
  #endif
#else
  #define __Pyx_PyType_AsAsync(obj) NULL
#endif
#ifndef __Pyx_PyAsyncMethodsStruct
    typedef struct {
        unaryfunc am_await;
        unaryfunc am_aiter;
        unaryfunc am_anext;
    } __Pyx_PyAsyncMethodsStruct;
#endif

#if defined(_WIN32) || defined(WIN32) || defined(MS_WINDOWS)
  #if !defined(_USE_MATH_DEFINES)
    #define _USE_MATH_DEFINES
  #endif
#endif
#include <math.h>
#ifdef NAN
#define __PYX_NAN() ((float) NAN)
#else
static CYTHON_INLINE float __PYX_NAN() {
  float value;
  memset(&value, 0xFF, sizeof(value));
  return value;
}
#endif
#if defined(__CYGWIN__) && defined(_LDBL_EQ_DBL)
#define __Pyx_truncl trunc
#else
#define __Pyx_truncl truncl
#endif

#define __PYX_MARK_ERR_POS(f_index, lineno) \
    { __pyx_filename = __pyx_f[f_index]; (void)__pyx_filename; __pyx_lineno = lineno; (void)__pyx_lineno; __pyx_clineno = __LINE__; (void)__pyx_clineno; }
#define __PYX_ERR(f_index, lineno, Ln_error) \
    { __PYX_MARK_ERR_POS(f_index, lineno) goto Ln_error; }

#ifndef __PYX_EXTERN_C
  #ifdef __cplusplus
    #define __PYX_EXTERN_C extern "C"
  #else
    #define __PYX_EXTERN_C extern
  #endif
#endif

#define __PYX_HAVE__serialmanager
#define __PYX_HAVE_API__serialmanager
/* Early includes */
#include "c_methods.h"
#include <string.h>
#include <stdlib.h>
#ifdef _OPENMP
#include <omp.h>
#endif /* _OPENMP */

#if defined(PYREX_WITHOUT_ASSERTIONS) && !defined(CYTHON_WITHOUT_ASSERTIONS)
#define CYTHON_WITHOUT_ASSERTIONS
#endif

typedef struct {PyObject **p; const char *s; const Py_ssize_t n; const char* encoding;
                const char is_unicode; const char is_str; const char intern; } __Pyx_StringTabEntry;

#define __PYX_DEFAULT_STRING_ENCODING_IS_ASCII 0
#define __PYX_DEFAULT_STRING_ENCODING_IS_UTF8 0
#define __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT (PY_MAJOR_VERSION >= 3 && __PYX_DEFAULT_STRING_ENCODING_IS_UTF8)
#define __PYX_DEFAULT_STRING_ENCODING ""
#define __Pyx_PyObject_FromString __Pyx_PyBytes_FromString
#define __Pyx_PyObject_FromStringAndSize __Pyx_PyBytes_FromStringAndSize
#define __Pyx_uchar_cast(c) ((unsigned char)c)
#define __Pyx_long_cast(x) ((long)x)
#define __Pyx_fits_Py_ssize_t(v, type, is_signed)  (\
    (sizeof(type) < sizeof(Py_ssize_t))  ||\
    (sizeof(type) > sizeof(Py_ssize_t) &&\
          likely(v < (type)PY_SSIZE_T_MAX ||\
                 v == (type)PY_SSIZE_T_MAX)  &&\
          (!is_signed || likely(v > (type)PY_SSIZE_T_MIN ||\
                                v == (type)PY_SSIZE_T_MIN)))  ||\
    (sizeof(type) == sizeof(Py_ssize_t) &&\
          (is_signed || likely(v < (type)PY_SSIZE_T_MAX ||\
                               v == (type)PY_SSIZE_T_MAX)))  )
static CYTHON_INLINE int __Pyx_is_valid_index(Py_ssize_t i, Py_ssize_t limit) {
    return (size_t) i < (size_t) limit;
}
#if defined (__cplusplus) && __cplusplus >= 201103L
    #include <cstdlib>
    #define __Pyx_sst_abs(value) std::abs(value)
#elif SIZEOF_INT >= SIZEOF_SIZE_T
    #define __Pyx_sst_abs(value) abs(value)
#elif SIZEOF_LONG >= SIZEOF_SIZE_T
    #define __Pyx_sst_abs(value) labs(value)
#elif defined (_MSC_VER)
    #define __Pyx_sst_abs(value) ((Py_ssize_t)_abs64(value))
#elif defined (__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
    #define __Pyx_sst_abs(value) llabs(value)
#elif defined (__GNUC__)
    #define __Pyx_sst_abs(value) __builtin_llabs(value)
#else
    #define __Pyx_sst_abs(value) ((value<0) ? -value : value)
#endif
static CYTHON_INLINE const char* __Pyx_PyObject_AsString(PyObject*);
static CYTHON_INLINE const char* __Pyx_PyObject_AsStringAndSize(PyObject*, Py_ssize_t* length);
#define __Pyx_PyByteArray_FromString(s) PyByteArray_FromStringAndSize((const char*)s, strlen((const char*)s))
#define __Pyx_PyByteArray_FromStringAndSize(s, l) PyByteArray_FromStringAndSize((const char*)s, l)
#define __Pyx_PyBytes_FromString        PyBytes_FromString
#define __Pyx_PyBytes_FromStringAndSize PyBytes_FromStringAndSize
static CYTHON_INLINE PyObject* __Pyx_PyUnicode_FromString(const char*);
#if PY_MAJOR_VERSION < 3
    #define __Pyx_PyStr_FromString        __Pyx_PyBytes_FromString
    #define __Pyx_PyStr_FromStringAndSize __Pyx_PyBytes_FromStringAndSize
#else
    #define __Pyx_PyStr_FromString        __Pyx_PyUnicode_FromString
    #define __Pyx_PyStr_FromStringAndSize __Pyx_PyUnicode_FromStringAndSize
#endif
#define __Pyx_PyBytes_AsWritableString(s)     ((char*) PyBytes_AS_STRING(s))
#define __Pyx_PyBytes_AsWritableSString(s)    ((signed char*) PyBytes_AS_STRING(s))
#define __Pyx_PyBytes_AsWritableUString(s)    ((unsigned char*) PyBytes_AS_STRING(s))
#define __Pyx_PyBytes_AsString(s)     ((const char*) PyBytes_AS_STRING(s))
#define __Pyx_PyBytes_AsSString(s)    ((const signed char*) PyBytes_AS_STRING(s))
#define __Pyx_PyBytes_AsUString(s)    ((const unsigned char*) PyBytes_AS_STRING(s))
#define __Pyx_PyObject_AsWritableString(s)    ((char*) __Pyx_PyObject_AsString(s))
#define __Pyx_PyObject_AsWritableSString(s)    ((signed char*) __Pyx_PyObject_AsString(s))
#define __Pyx_PyObject_AsWritableUString(s)    ((unsigned char*) __Pyx_PyObject_AsString(s))
#define __Pyx_PyObject_AsSString(s)    ((const signed char*) __Pyx_PyObject_AsString(s))
#define __Pyx_PyObject_AsUString(s)    ((const unsigned char*) __Pyx_PyObject_AsString(s))
#define __Pyx_PyObject_FromCString(s)  __Pyx_PyObject_FromString((const char*)s)
#define __Pyx_PyBytes_FromCString(s)   __Pyx_PyBytes_FromString((const char*)s)
#define __Pyx_PyByteArray_FromCString(s)   __Pyx_PyByteArray_FromString((const char*)s)
#define __Pyx_PyStr_FromCString(s)     __Pyx_PyStr_FromString((const char*)s)
#define __Pyx_PyUnicode_FromCString(s) __Pyx_PyUnicode_FromString((const char*)s)
static CYTHON_INLINE size_t __Pyx_Py_UNICODE_strlen(const Py_UNICODE *u) {
    const Py_UNICODE *u_end = u;
    while (*u_end++) ;
    return (size_t)(u_end - u - 1);
}
#define __Pyx_PyUnicode_FromUnicode(u)       PyUnicode_FromUnicode(u, __Pyx_Py_UNICODE_strlen(u))
#define __Pyx_PyUnicode_FromUnicodeAndLength PyUnicode_FromUnicode
#define __Pyx_PyUnicode_AsUnicode            PyUnicode_AsUnicode
#define __Pyx_NewRef(obj) (Py_INCREF(obj), obj)
#define __Pyx_Owned_Py_None(b) __Pyx_NewRef(Py_None)
static CYTHON_INLINE PyObject * __Pyx_PyBool_FromLong(long b);
static CYTHON_INLINE int __Pyx_PyObject_IsTrue(PyObject*);
static CYTHON_INLINE int __Pyx_PyObject_IsTrueAndDecref(PyObject*);
static CYTHON_INLINE PyObject* __Pyx_PyNumber_IntOrLong(PyObject* x);
#define __Pyx_PySequence_Tuple(obj)\
    (likely(PyTuple_CheckExact(obj)) ? __Pyx_NewRef(obj) : PySequence_Tuple(obj))
static CYTHON_INLINE Py_ssize_t __Pyx_PyIndex_AsSsize_t(PyObject*);
static CYTHON_INLINE PyObject * __Pyx_PyInt_FromSize_t(size_t);
static CYTHON_INLINE Py_hash_t __Pyx_PyIndex_AsHash_t(PyObject*);
#if CYTHON_ASSUME_SAFE_MACROS
#define __pyx_PyFloat_AsDouble(x) (PyFloat_CheckExact(x) ? PyFloat_AS_DOUBLE(x) : PyFloat_AsDouble(x))
#else
#define __pyx_PyFloat_AsDouble(x) PyFloat_AsDouble(x)
#endif
#define __pyx_PyFloat_AsFloat(x) ((float) __pyx_PyFloat_AsDouble(x))
#if PY_MAJOR_VERSION >= 3
#define __Pyx_PyNumber_Int(x) (PyLong_CheckExact(x) ? __Pyx_NewRef(x) : PyNumber_Long(x))
#else
#define __Pyx_PyNumber_Int(x) (PyInt_CheckExact(x) ? __Pyx_NewRef(x) : PyNumber_Int(x))
#endif
#define __Pyx_PyNumber_Float(x) (PyFloat_CheckExact(x) ? __Pyx_NewRef(x) : PyNumber_Float(x))
#if PY_MAJOR_VERSION < 3 && __PYX_DEFAULT_STRING_ENCODING_IS_ASCII
static int __Pyx_sys_getdefaultencoding_not_ascii;
static int __Pyx_init_sys_getdefaultencoding_params(void) {
    PyObject* sys;
    PyObject* default_encoding = NULL;
    PyObject* ascii_chars_u = NULL;
    PyObject* ascii_chars_b = NULL;
    const char* default_encoding_c;
    sys = PyImport_ImportModule("sys");
    if (!sys) goto bad;
    default_encoding = PyObject_CallMethod(sys, (char*) "getdefaultencoding", NULL);
    Py_DECREF(sys);
    if (!default_encoding) goto bad;
    default_encoding_c = PyBytes_AsString(default_encoding);
    if (!default_encoding_c) goto bad;
    if (strcmp(default_encoding_c, "ascii") == 0) {
        __Pyx_sys_getdefaultencoding_not_ascii = 0;
    } else {
        char ascii_chars[128];
        int c;
        for (c = 0; c < 128; c++) {
            ascii_chars[c] = c;
        }
        __Pyx_sys_getdefaultencoding_not_ascii = 1;
        ascii_chars_u = PyUnicode_DecodeASCII(ascii_chars, 128, NULL);
        if (!ascii_chars_u) goto bad;
        ascii_chars_b = PyUnicode_AsEncodedString(ascii_chars_u, default_encoding_c, NULL);
        if (!ascii_chars_b || !PyBytes_Check(ascii_chars_b) || memcmp(ascii_chars, PyBytes_AS_STRING(ascii_chars_b), 128) != 0) {
            PyErr_Format(
                PyExc_ValueError,
                "This module compiled with c_string_encoding=ascii, but default encoding '%.200s' is not a superset of ascii.",
                default_encoding_c);
            goto bad;
        }
        Py_DECREF(ascii_chars_u);
        Py_DECREF(ascii_chars_b);
    }
    Py_DECREF(default_encoding);
    return 0;
bad:
    Py_XDECREF(default_encoding);
    Py_XDECREF(ascii_chars_u);
    Py_XDECREF(ascii_chars_b);
    return -1;
}
#endif
#if __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT && PY_MAJOR_VERSION >= 3
#define __Pyx_PyUnicode_FromStringAndSize(c_str, size) PyUnicode_DecodeUTF8(c_str, size, NULL)
#else
#define __Pyx_PyUnicode_FromStringAndSize(c_str, size) PyUnicode_Decode(c_str, size, __PYX_DEFAULT_STRING_ENCODING, NULL)
#if __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT
static char* __PYX_DEFAULT_STRING_ENCODING;
static int __Pyx_init_sys_getdefaultencoding_params(void) {
    PyObject* sys;
    PyObject* default_encoding = NULL;
    char* default_encoding_c;
    sys = PyImport_ImportModule("sys");
    if (!sys) goto bad;
    default_encoding = PyObject_CallMethod(sys, (char*) (const char*) "getdefaultencoding", NULL);
    Py_DECREF(sys);
    if (!default_encoding) goto bad;
    default_encoding_c = PyBytes_AsString(default_encoding);
    if (!default_encoding_c) goto bad;
    __PYX_DEFAULT_STRING_ENCODING = (char*) malloc(strlen(default_encoding_c) + 1);
    if (!__PYX_DEFAULT_STRING_ENCODING) goto bad;
    strcpy(__PYX_DEFAULT_STRING_ENCODING, default_encoding_c);
    Py_DECREF(default_encoding);
    return 0;
bad:
    Py_XDECREF(default_encoding);
    return -1;
}
#endif
#endif


/* Test for GCC > 2.95 */
#if defined(__GNUC__)     && (__GNUC__ > 2 || (__GNUC__ == 2 && (__GNUC_MINOR__ > 95)))
  #define likely(x)   __builtin_expect(!!(x), 1)
  #define unlikely(x) __builtin_expect(!!(x), 0)
#else /* !__GNUC__ or GCC < 2.95 */
  #define likely(x)   (x)
  #define unlikely(x) (x)
#endif /* __GNUC__ */
static CYTHON_INLINE void __Pyx_pretend_to_initialize(void* ptr) { (void)ptr; }

static PyObject *__pyx_m = NULL;
static PyObject *__pyx_d;
static PyObject *__pyx_b;
static PyObject *__pyx_cython_runtime = NULL;
static PyObject *__pyx_empty_tuple;
static PyObject *__pyx_empty_bytes;
static PyObject *__pyx_empty_unicode;
static int __pyx_lineno;
static int __pyx_clineno = 0;
static const char * __pyx_cfilenm= __FILE__;
static const char *__pyx_filename;


static const char *__pyx_f[] = {
  "serialmanager.pyx",
  "stringsource",
};

/*--- Type declarations ---*/
struct __pyx_obj_13serialmanager_SerialManager;

/* "serialmanager.pyx":11
 * 
 * 
 * cdef class SerialManager:             # <<<<<<<<<<<<<<
 * 	cdef public:
 * 		str name
 */
struct __pyx_obj_13serialmanager_SerialManager {
  PyObject_HEAD
  struct __pyx_vtabstruct_13serialmanager_SerialManager *__pyx_vtab;
  PyObject *name;
  int start_marker;
  int end_marker;
  PyObject *ser;
};



struct __pyx_vtabstruct_13serialmanager_SerialManager {
  PyObject *(*send_to_arduino)(struct __pyx_obj_13serialmanager_SerialManager *, char *);
  char *(*receive_from_arduino)(struct __pyx_obj_13serialmanager_SerialManager *);
  PyObject *(*waiting_arduino)(struct __pyx_obj_13serialmanager_SerialManager *);
  PyObject *(*waiting_arduino_nano)(struct __pyx_obj_13serialmanager_SerialManager *);
  PyObject *(*waiting_arduino_nano2)(struct __pyx_obj_13serialmanager_SerialManager *);
  PyObject *(*waiting_arduino_mega)(struct __pyx_obj_13serialmanager_SerialManager *);
  PyObject *(*close)(struct __pyx_obj_13serialmanager_SerialManager *, int __pyx_skip_dispatch);
  PyObject *(*inWaiting)(struct __pyx_obj_13serialmanager_SerialManager *, int __pyx_skip_dispatch);
  PyObject *(*led_controller)(struct __pyx_obj_13serialmanager_SerialManager *, char *);
  PyObject *(*prendi)(struct __pyx_obj_13serialmanager_SerialManager *);
  PyObject *(*take_front)(struct __pyx_obj_13serialmanager_SerialManager *);
  PyObject *(*daje)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *);
  PyObject *(*tell_to_stop)(struct __pyx_obj_13serialmanager_SerialManager *, char *);
  PyObject *(*tell_to_move3)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *);
  PyObject *(*movement_command)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *);
  PyObject *(*tell_to_move)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *);
  PyObject *(*tell_to_move2)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *);
  PyObject *(*cgamepad)(struct __pyx_obj_13serialmanager_SerialManager *, char *, float, float, float);
  PyObject *(*cprova1)(struct __pyx_obj_13serialmanager_SerialManager *);
  PyObject *(*csendledcommand)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *);
};
static struct __pyx_vtabstruct_13serialmanager_SerialManager *__pyx_vtabptr_13serialmanager_SerialManager;

/* --- Runtime support code (head) --- */
/* Refnanny.proto */
#ifndef CYTHON_REFNANNY
  #define CYTHON_REFNANNY 0
#endif
#if CYTHON_REFNANNY
  typedef struct {
    void (*INCREF)(void*, PyObject*, int);
    void (*DECREF)(void*, PyObject*, int);
    void (*GOTREF)(void*, PyObject*, int);
    void (*GIVEREF)(void*, PyObject*, int);
    void* (*SetupContext)(const char*, int, const char*);
    void (*FinishContext)(void**);
  } __Pyx_RefNannyAPIStruct;
  static __Pyx_RefNannyAPIStruct *__Pyx_RefNanny = NULL;
  static __Pyx_RefNannyAPIStruct *__Pyx_RefNannyImportAPI(const char *modname);
  #define __Pyx_RefNannyDeclarations void *__pyx_refnanny = NULL;
#ifdef WITH_THREAD
  #define __Pyx_RefNannySetupContext(name, acquire_gil)\
          if (acquire_gil) {\
              PyGILState_STATE __pyx_gilstate_save = PyGILState_Ensure();\
              __pyx_refnanny = __Pyx_RefNanny->SetupContext((name), __LINE__, __FILE__);\
              PyGILState_Release(__pyx_gilstate_save);\
          } else {\
              __pyx_refnanny = __Pyx_RefNanny->SetupContext((name), __LINE__, __FILE__);\
          }
#else
  #define __Pyx_RefNannySetupContext(name, acquire_gil)\
          __pyx_refnanny = __Pyx_RefNanny->SetupContext((name), __LINE__, __FILE__)
#endif
  #define __Pyx_RefNannyFinishContext()\
          __Pyx_RefNanny->FinishContext(&__pyx_refnanny)
  #define __Pyx_INCREF(r)  __Pyx_RefNanny->INCREF(__pyx_refnanny, (PyObject *)(r), __LINE__)
  #define __Pyx_DECREF(r)  __Pyx_RefNanny->DECREF(__pyx_refnanny, (PyObject *)(r), __LINE__)
  #define __Pyx_GOTREF(r)  __Pyx_RefNanny->GOTREF(__pyx_refnanny, (PyObject *)(r), __LINE__)
  #define __Pyx_GIVEREF(r) __Pyx_RefNanny->GIVEREF(__pyx_refnanny, (PyObject *)(r), __LINE__)
  #define __Pyx_XINCREF(r)  do { if((r) != NULL) {__Pyx_INCREF(r); }} while(0)
  #define __Pyx_XDECREF(r)  do { if((r) != NULL) {__Pyx_DECREF(r); }} while(0)
  #define __Pyx_XGOTREF(r)  do { if((r) != NULL) {__Pyx_GOTREF(r); }} while(0)
  #define __Pyx_XGIVEREF(r) do { if((r) != NULL) {__Pyx_GIVEREF(r);}} while(0)
#else
  #define __Pyx_RefNannyDeclarations
  #define __Pyx_RefNannySetupContext(name, acquire_gil)
  #define __Pyx_RefNannyFinishContext()
  #define __Pyx_INCREF(r) Py_INCREF(r)
  #define __Pyx_DECREF(r) Py_DECREF(r)
  #define __Pyx_GOTREF(r)
  #define __Pyx_GIVEREF(r)
  #define __Pyx_XINCREF(r) Py_XINCREF(r)
  #define __Pyx_XDECREF(r) Py_XDECREF(r)
  #define __Pyx_XGOTREF(r)
  #define __Pyx_XGIVEREF(r)
#endif
#define __Pyx_XDECREF_SET(r, v) do {\
        PyObject *tmp = (PyObject *) r;\
        r = v; __Pyx_XDECREF(tmp);\
    } while (0)
#define __Pyx_DECREF_SET(r, v) do {\
        PyObject *tmp = (PyObject *) r;\
        r = v; __Pyx_DECREF(tmp);\
    } while (0)
#define __Pyx_CLEAR(r)    do { PyObject* tmp = ((PyObject*)(r)); r = NULL; __Pyx_DECREF(tmp);} while(0)
#define __Pyx_XCLEAR(r)   do { if((r) != NULL) {PyObject* tmp = ((PyObject*)(r)); r = NULL; __Pyx_DECREF(tmp);}} while(0)

/* PyObjectGetAttrStr.proto */
#if CYTHON_USE_TYPE_SLOTS
static CYTHON_INLINE PyObject* __Pyx_PyObject_GetAttrStr(PyObject* obj, PyObject* attr_name);
#else
#define __Pyx_PyObject_GetAttrStr(o,n) PyObject_GetAttr(o,n)
#endif

/* GetBuiltinName.proto */
static PyObject *__Pyx_GetBuiltinName(PyObject *name);

/* RaiseArgTupleInvalid.proto */
static void __Pyx_RaiseArgtupleInvalid(const char* func_name, int exact,
    Py_ssize_t num_min, Py_ssize_t num_max, Py_ssize_t num_found);

/* RaiseDoubleKeywords.proto */
static void __Pyx_RaiseDoubleKeywordsError(const char* func_name, PyObject* kw_name);

/* ParseKeywords.proto */
static int __Pyx_ParseOptionalKeywords(PyObject *kwds, PyObject **argnames[],\
    PyObject *kwds2, PyObject *values[], Py_ssize_t num_pos_args,\
    const char* function_name);

/* ArgTypeTest.proto */
#define __Pyx_ArgTypeTest(obj, type, none_allowed, name, exact)\
    ((likely((Py_TYPE(obj) == type) | (none_allowed && (obj == Py_None)))) ? 1 :\
        __Pyx__ArgTypeTest(obj, type, name, exact))
static int __Pyx__ArgTypeTest(PyObject *obj, PyTypeObject *type, const char *name, int exact);

/* PyThreadStateGet.proto */
#if CYTHON_FAST_THREAD_STATE
#define __Pyx_PyThreadState_declare  PyThreadState *__pyx_tstate;
#define __Pyx_PyThreadState_assign  __pyx_tstate = __Pyx_PyThreadState_Current;
#define __Pyx_PyErr_Occurred()  __pyx_tstate->curexc_type
#else
#define __Pyx_PyThreadState_declare
#define __Pyx_PyThreadState_assign
#define __Pyx_PyErr_Occurred()  PyErr_Occurred()
#endif

/* PyErrFetchRestore.proto */
#if CYTHON_FAST_THREAD_STATE
#define __Pyx_PyErr_Clear() __Pyx_ErrRestore(NULL, NULL, NULL)
#define __Pyx_ErrRestoreWithState(type, value, tb)  __Pyx_ErrRestoreInState(PyThreadState_GET(), type, value, tb)
#define __Pyx_ErrFetchWithState(type, value, tb)    __Pyx_ErrFetchInState(PyThreadState_GET(), type, value, tb)
#define __Pyx_ErrRestore(type, value, tb)  __Pyx_ErrRestoreInState(__pyx_tstate, type, value, tb)
#define __Pyx_ErrFetch(type, value, tb)    __Pyx_ErrFetchInState(__pyx_tstate, type, value, tb)
static CYTHON_INLINE void __Pyx_ErrRestoreInState(PyThreadState *tstate, PyObject *type, PyObject *value, PyObject *tb);
static CYTHON_INLINE void __Pyx_ErrFetchInState(PyThreadState *tstate, PyObject **type, PyObject **value, PyObject **tb);
#if CYTHON_COMPILING_IN_CPYTHON
#define __Pyx_PyErr_SetNone(exc) (Py_INCREF(exc), __Pyx_ErrRestore((exc), NULL, NULL))
#else
#define __Pyx_PyErr_SetNone(exc) PyErr_SetNone(exc)
#endif
#else
#define __Pyx_PyErr_Clear() PyErr_Clear()
#define __Pyx_PyErr_SetNone(exc) PyErr_SetNone(exc)
#define __Pyx_ErrRestoreWithState(type, value, tb)  PyErr_Restore(type, value, tb)
#define __Pyx_ErrFetchWithState(type, value, tb)  PyErr_Fetch(type, value, tb)
#define __Pyx_ErrRestoreInState(tstate, type, value, tb)  PyErr_Restore(type, value, tb)
#define __Pyx_ErrFetchInState(tstate, type, value, tb)  PyErr_Fetch(type, value, tb)
#define __Pyx_ErrRestore(type, value, tb)  PyErr_Restore(type, value, tb)
#define __Pyx_ErrFetch(type, value, tb)  PyErr_Fetch(type, value, tb)
#endif

/* Profile.proto */
#ifndef CYTHON_PROFILE
#if CYTHON_COMPILING_IN_PYPY || CYTHON_COMPILING_IN_PYSTON
  #define CYTHON_PROFILE 0
#else
  #define CYTHON_PROFILE 1
#endif
#endif
#ifndef CYTHON_TRACE_NOGIL
  #define CYTHON_TRACE_NOGIL 0
#else
  #if CYTHON_TRACE_NOGIL && !defined(CYTHON_TRACE)
    #define CYTHON_TRACE 1
  #endif
#endif
#ifndef CYTHON_TRACE
  #define CYTHON_TRACE 0
#endif
#if CYTHON_TRACE
  #undef CYTHON_PROFILE_REUSE_FRAME
#endif
#ifndef CYTHON_PROFILE_REUSE_FRAME
  #define CYTHON_PROFILE_REUSE_FRAME 0
#endif
#if CYTHON_PROFILE || CYTHON_TRACE
  #include "compile.h"
  #include "frameobject.h"
  #include "traceback.h"
#if PY_VERSION_HEX >= 0x030b00a6
  #ifndef Py_BUILD_CORE
    #define Py_BUILD_CORE 1
  #endif
  #include "internal/pycore_frame.h"
#endif
  #if CYTHON_PROFILE_REUSE_FRAME
    #define CYTHON_FRAME_MODIFIER static
    #define CYTHON_FRAME_DEL(frame)
  #else
    #define CYTHON_FRAME_MODIFIER
    #define CYTHON_FRAME_DEL(frame) Py_CLEAR(frame)
  #endif
  #define __Pyx_TraceDeclarations\
      static PyCodeObject *__pyx_frame_code = NULL;\
      CYTHON_FRAME_MODIFIER PyFrameObject *__pyx_frame = NULL;\
      int __Pyx_use_tracing = 0;
  #define __Pyx_TraceFrameInit(codeobj)\
      if (codeobj) __pyx_frame_code = (PyCodeObject*) codeobj;
#if PY_VERSION_HEX >= 0x030b00a2
  #define __Pyx_IsTracing(tstate, check_tracing, check_funcs)\
     (unlikely((tstate)->cframe->use_tracing) &&\
         (!(check_tracing) || !(tstate)->tracing) &&\
         (!(check_funcs) || (tstate)->c_profilefunc || (CYTHON_TRACE && (tstate)->c_tracefunc)))
  #define __Pyx_EnterTracing(tstate) PyThreadState_EnterTracing(tstate)
  #define __Pyx_LeaveTracing(tstate) PyThreadState_LeaveTracing(tstate)
#elif PY_VERSION_HEX >= 0x030a00b1
  #define __Pyx_IsTracing(tstate, check_tracing, check_funcs)\
     (unlikely((tstate)->cframe->use_tracing) &&\
         (!(check_tracing) || !(tstate)->tracing) &&\
         (!(check_funcs) || (tstate)->c_profilefunc || (CYTHON_TRACE && (tstate)->c_tracefunc)))
  #define __Pyx_EnterTracing(tstate)\
      do { tstate->tracing++; tstate->cframe->use_tracing = 0; } while (0)
  #define __Pyx_LeaveTracing(tstate)\
      do {\
          tstate->tracing--;\
          tstate->cframe->use_tracing = ((CYTHON_TRACE && tstate->c_tracefunc != NULL)\
                                 || tstate->c_profilefunc != NULL);\
      } while (0)
#else
  #define __Pyx_IsTracing(tstate, check_tracing, check_funcs)\
     (unlikely((tstate)->use_tracing) &&\
         (!(check_tracing) || !(tstate)->tracing) &&\
         (!(check_funcs) || (tstate)->c_profilefunc || (CYTHON_TRACE && (tstate)->c_tracefunc)))
  #define __Pyx_EnterTracing(tstate)\
      do { tstate->tracing++; tstate->use_tracing = 0; } while (0)
  #define __Pyx_LeaveTracing(tstate)\
      do {\
          tstate->tracing--;\
          tstate->use_tracing = ((CYTHON_TRACE && tstate->c_tracefunc != NULL)\
                                         || tstate->c_profilefunc != NULL);\
      } while (0)
#endif
  #ifdef WITH_THREAD
  #define __Pyx_TraceCall(funcname, srcfile, firstlineno, nogil, goto_error)\
  if (nogil) {\
      if (CYTHON_TRACE_NOGIL) {\
          PyThreadState *tstate;\
          PyGILState_STATE state = PyGILState_Ensure();\
          tstate = __Pyx_PyThreadState_Current;\
          if (__Pyx_IsTracing(tstate, 1, 1)) {\
              __Pyx_use_tracing = __Pyx_TraceSetupAndCall(&__pyx_frame_code, &__pyx_frame, tstate, funcname, srcfile, firstlineno);\
          }\
          PyGILState_Release(state);\
          if (unlikely(__Pyx_use_tracing < 0)) goto_error;\
      }\
  } else {\
      PyThreadState* tstate = PyThreadState_GET();\
      if (__Pyx_IsTracing(tstate, 1, 1)) {\
          __Pyx_use_tracing = __Pyx_TraceSetupAndCall(&__pyx_frame_code, &__pyx_frame, tstate, funcname, srcfile, firstlineno);\
          if (unlikely(__Pyx_use_tracing < 0)) goto_error;\
      }\
  }
  #else
  #define __Pyx_TraceCall(funcname, srcfile, firstlineno, nogil, goto_error)\
  {   PyThreadState* tstate = PyThreadState_GET();\
      if (__Pyx_IsTracing(tstate, 1, 1)) {\
          __Pyx_use_tracing = __Pyx_TraceSetupAndCall(&__pyx_frame_code, &__pyx_frame, tstate, funcname, srcfile, firstlineno);\
          if (unlikely(__Pyx_use_tracing < 0)) goto_error;\
      }\
  }
  #endif
  #define __Pyx_TraceException()\
  if (likely(!__Pyx_use_tracing)); else {\
      PyThreadState* tstate = __Pyx_PyThreadState_Current;\
      if (__Pyx_IsTracing(tstate, 0, 1)) {\
          __Pyx_EnterTracing(tstate);\
          PyObject *exc_info = __Pyx_GetExceptionTuple(tstate);\
          if (exc_info) {\
              if (CYTHON_TRACE && tstate->c_tracefunc)\
                  tstate->c_tracefunc(\
                      tstate->c_traceobj, __pyx_frame, PyTrace_EXCEPTION, exc_info);\
              tstate->c_profilefunc(\
                  tstate->c_profileobj, __pyx_frame, PyTrace_EXCEPTION, exc_info);\
              Py_DECREF(exc_info);\
          }\
          __Pyx_LeaveTracing(tstate);\
      }\
  }
  static void __Pyx_call_return_trace_func(PyThreadState *tstate, PyFrameObject *frame, PyObject *result) {
      PyObject *type, *value, *traceback;
      __Pyx_ErrFetchInState(tstate, &type, &value, &traceback);
      __Pyx_EnterTracing(tstate);
      if (CYTHON_TRACE && tstate->c_tracefunc)
          tstate->c_tracefunc(tstate->c_traceobj, frame, PyTrace_RETURN, result);
      if (tstate->c_profilefunc)
          tstate->c_profilefunc(tstate->c_profileobj, frame, PyTrace_RETURN, result);
      CYTHON_FRAME_DEL(frame);
      __Pyx_LeaveTracing(tstate);
      __Pyx_ErrRestoreInState(tstate, type, value, traceback);
  }
  #ifdef WITH_THREAD
  #define __Pyx_TraceReturn(result, nogil)\
  if (likely(!__Pyx_use_tracing)); else {\
      if (nogil) {\
          if (CYTHON_TRACE_NOGIL) {\
              PyThreadState *tstate;\
              PyGILState_STATE state = PyGILState_Ensure();\
              tstate = __Pyx_PyThreadState_Current;\
              if (__Pyx_IsTracing(tstate, 0, 0)) {\
                  __Pyx_call_return_trace_func(tstate, __pyx_frame, (PyObject*)result);\
              }\
              PyGILState_Release(state);\
          }\
      } else {\
          PyThreadState* tstate = __Pyx_PyThreadState_Current;\
          if (__Pyx_IsTracing(tstate, 0, 0)) {\
              __Pyx_call_return_trace_func(tstate, __pyx_frame, (PyObject*)result);\
          }\
      }\
  }
  #else
  #define __Pyx_TraceReturn(result, nogil)\
  if (likely(!__Pyx_use_tracing)); else {\
      PyThreadState* tstate = __Pyx_PyThreadState_Current;\
      if (__Pyx_IsTracing(tstate, 0, 0)) {\
          __Pyx_call_return_trace_func(tstate, __pyx_frame, (PyObject*)result);\
      }\
  }
  #endif
  static PyCodeObject *__Pyx_createFrameCodeObject(const char *funcname, const char *srcfile, int firstlineno);
  static int __Pyx_TraceSetupAndCall(PyCodeObject** code, PyFrameObject** frame, PyThreadState* tstate, const char *funcname, const char *srcfile, int firstlineno);
#else
  #define __Pyx_TraceDeclarations
  #define __Pyx_TraceFrameInit(codeobj)
  #define __Pyx_TraceCall(funcname, srcfile, firstlineno, nogil, goto_error)   if ((1)); else goto_error;
  #define __Pyx_TraceException()
  #define __Pyx_TraceReturn(result, nogil)
#endif
#if CYTHON_TRACE
  static int __Pyx_call_line_trace_func(PyThreadState *tstate, PyFrameObject *frame, int lineno) {
      int ret;
      PyObject *type, *value, *traceback;
      __Pyx_ErrFetchInState(tstate, &type, &value, &traceback);
      __Pyx_PyFrame_SetLineNumber(frame, lineno);
      __Pyx_EnterTracing(tstate);
      ret = tstate->c_tracefunc(tstate->c_traceobj, frame, PyTrace_LINE, NULL);
      __Pyx_LeaveTracing(tstate);
      if (likely(!ret)) {
          __Pyx_ErrRestoreInState(tstate, type, value, traceback);
      } else {
          Py_XDECREF(type);
          Py_XDECREF(value);
          Py_XDECREF(traceback);
      }
      return ret;
  }
  #ifdef WITH_THREAD
  #define __Pyx_TraceLine(lineno, nogil, goto_error)\
  if (likely(!__Pyx_use_tracing)); else {\
      if (nogil) {\
          if (CYTHON_TRACE_NOGIL) {\
              int ret = 0;\
              PyThreadState *tstate;\
              PyGILState_STATE state = PyGILState_Ensure();\
              tstate = __Pyx_PyThreadState_Current;\
              if (__Pyx_IsTracing(tstate, 0, 0) && tstate->c_tracefunc && __pyx_frame->f_trace) {\
                  ret = __Pyx_call_line_trace_func(tstate, __pyx_frame, lineno);\
              }\
              PyGILState_Release(state);\
              if (unlikely(ret)) goto_error;\
          }\
      } else {\
          PyThreadState* tstate = __Pyx_PyThreadState_Current;\
          if (__Pyx_IsTracing(tstate, 0, 0) && tstate->c_tracefunc && __pyx_frame->f_trace) {\
              int ret = __Pyx_call_line_trace_func(tstate, __pyx_frame, lineno);\
              if (unlikely(ret)) goto_error;\
          }\
      }\
  }
  #else
  #define __Pyx_TraceLine(lineno, nogil, goto_error)\
  if (likely(!__Pyx_use_tracing)); else {\
      PyThreadState* tstate = __Pyx_PyThreadState_Current;\
      if (__Pyx_IsTracing(tstate, 0, 0) && tstate->c_tracefunc && __pyx_frame->f_trace) {\
          int ret = __Pyx_call_line_trace_func(tstate, __pyx_frame, lineno);\
          if (unlikely(ret)) goto_error;\
      }\
  }
  #endif
#else
  #define __Pyx_TraceLine(lineno, nogil, goto_error)   if ((1)); else goto_error;
#endif

/* PyCFunctionFastCall.proto */
#if CYTHON_FAST_PYCCALL
static CYTHON_INLINE PyObject *__Pyx_PyCFunction_FastCall(PyObject *func, PyObject **args, Py_ssize_t nargs);
#else
#define __Pyx_PyCFunction_FastCall(func, args, nargs)  (assert(0), NULL)
#endif

/* PyFunctionFastCall.proto */
#if CYTHON_FAST_PYCALL
#define __Pyx_PyFunction_FastCall(func, args, nargs)\
    __Pyx_PyFunction_FastCallDict((func), (args), (nargs), NULL)
#if 1 || PY_VERSION_HEX < 0x030600B1
static PyObject *__Pyx_PyFunction_FastCallDict(PyObject *func, PyObject **args, Py_ssize_t nargs, PyObject *kwargs);
#else
#define __Pyx_PyFunction_FastCallDict(func, args, nargs, kwargs) _PyFunction_FastCallDict(func, args, nargs, kwargs)
#endif
#define __Pyx_BUILD_ASSERT_EXPR(cond)\
    (sizeof(char [1 - 2*!(cond)]) - 1)
#ifndef Py_MEMBER_SIZE
#define Py_MEMBER_SIZE(type, member) sizeof(((type *)0)->member)
#endif
#if CYTHON_FAST_PYCALL
  static size_t __pyx_pyframe_localsplus_offset = 0;
  #include "frameobject.h"
#if PY_VERSION_HEX >= 0x030b00a6
  #ifndef Py_BUILD_CORE
    #define Py_BUILD_CORE 1
  #endif
  #include "internal/pycore_frame.h"
#endif
  #define __Pxy_PyFrame_Initialize_Offsets()\
    ((void)__Pyx_BUILD_ASSERT_EXPR(sizeof(PyFrameObject) == offsetof(PyFrameObject, f_localsplus) + Py_MEMBER_SIZE(PyFrameObject, f_localsplus)),\
     (void)(__pyx_pyframe_localsplus_offset = ((size_t)PyFrame_Type.tp_basicsize) - Py_MEMBER_SIZE(PyFrameObject, f_localsplus)))
  #define __Pyx_PyFrame_GetLocalsplus(frame)\
    (assert(__pyx_pyframe_localsplus_offset), (PyObject **)(((char *)(frame)) + __pyx_pyframe_localsplus_offset))
#endif // CYTHON_FAST_PYCALL
#endif

/* PyObjectCall.proto */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_Call(PyObject *func, PyObject *arg, PyObject *kw);
#else
#define __Pyx_PyObject_Call(func, arg, kw) PyObject_Call(func, arg, kw)
#endif

/* PyObjectCall2Args.proto */
static CYTHON_UNUSED PyObject* __Pyx_PyObject_Call2Args(PyObject* function, PyObject* arg1, PyObject* arg2);

/* PyObjectCallMethO.proto */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallMethO(PyObject *func, PyObject *arg);
#endif

/* PyObjectCallOneArg.proto */
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallOneArg(PyObject *func, PyObject *arg);

/* IncludeStringH.proto */
#include <string.h>

/* decode_c_string_utf16.proto */
static CYTHON_INLINE PyObject *__Pyx_PyUnicode_DecodeUTF16(const char *s, Py_ssize_t size, const char *errors) {
    int byteorder = 0;
    return PyUnicode_DecodeUTF16(s, size, errors, &byteorder);
}
static CYTHON_INLINE PyObject *__Pyx_PyUnicode_DecodeUTF16LE(const char *s, Py_ssize_t size, const char *errors) {
    int byteorder = -1;
    return PyUnicode_DecodeUTF16(s, size, errors, &byteorder);
}
static CYTHON_INLINE PyObject *__Pyx_PyUnicode_DecodeUTF16BE(const char *s, Py_ssize_t size, const char *errors) {
    int byteorder = 1;
    return PyUnicode_DecodeUTF16(s, size, errors, &byteorder);
}

/* decode_c_string.proto */
static CYTHON_INLINE PyObject* __Pyx_decode_c_string(
         const char* cstring, Py_ssize_t start, Py_ssize_t stop,
         const char* encoding, const char* errors,
         PyObject* (*decode_func)(const char *s, Py_ssize_t size, const char *errors));

/* UnicodeAsUCS4.proto */
static CYTHON_INLINE Py_UCS4 __Pyx_PyUnicode_AsPy_UCS4(PyObject*);

/* object_ord.proto */
#if PY_MAJOR_VERSION >= 3
#define __Pyx_PyObject_Ord(c)\
    (likely(PyUnicode_Check(c)) ? (long)__Pyx_PyUnicode_AsPy_UCS4(c) : __Pyx__PyObject_Ord(c))
#else
#define __Pyx_PyObject_Ord(c) __Pyx__PyObject_Ord(c)
#endif
static long __Pyx__PyObject_Ord(PyObject* c);

/* PyObjectCallNoArg.proto */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallNoArg(PyObject *func);
#else
#define __Pyx_PyObject_CallNoArg(func) __Pyx_PyObject_Call(func, __pyx_empty_tuple, NULL)
#endif

/* decode_c_bytes.proto */
static CYTHON_INLINE PyObject* __Pyx_decode_c_bytes(
         const char* cstring, Py_ssize_t length, Py_ssize_t start, Py_ssize_t stop,
         const char* encoding, const char* errors,
         PyObject* (*decode_func)(const char *s, Py_ssize_t size, const char *errors));

/* decode_bytes.proto */
static CYTHON_INLINE PyObject* __Pyx_decode_bytes(
         PyObject* string, Py_ssize_t start, Py_ssize_t stop,
         const char* encoding, const char* errors,
         PyObject* (*decode_func)(const char *s, Py_ssize_t size, const char *errors)) {
    return __Pyx_decode_c_bytes(
        PyBytes_AS_STRING(string), PyBytes_GET_SIZE(string),
        start, stop, encoding, errors, decode_func);
}

/* GetTopmostException.proto */
#if CYTHON_USE_EXC_INFO_STACK
static _PyErr_StackItem * __Pyx_PyErr_GetTopmostException(PyThreadState *tstate);
#endif

/* SaveResetException.proto */
#if CYTHON_FAST_THREAD_STATE
#define __Pyx_ExceptionSave(type, value, tb)  __Pyx__ExceptionSave(__pyx_tstate, type, value, tb)
static CYTHON_INLINE void __Pyx__ExceptionSave(PyThreadState *tstate, PyObject **type, PyObject **value, PyObject **tb);
#define __Pyx_ExceptionReset(type, value, tb)  __Pyx__ExceptionReset(__pyx_tstate, type, value, tb)
static CYTHON_INLINE void __Pyx__ExceptionReset(PyThreadState *tstate, PyObject *type, PyObject *value, PyObject *tb);
#else
#define __Pyx_ExceptionSave(type, value, tb)   PyErr_GetExcInfo(type, value, tb)
#define __Pyx_ExceptionReset(type, value, tb)  PyErr_SetExcInfo(type, value, tb)
#endif

/* WriteUnraisableException.proto */
static void __Pyx_WriteUnraisable(const char *name, int clineno,
                                  int lineno, const char *filename,
                                  int full_traceback, int nogil);

/* PyIntCompare.proto */
static CYTHON_INLINE PyObject* __Pyx_PyInt_EqObjC(PyObject *op1, PyObject *op2, long intval, long inplace);

/* PyDictVersioning.proto */
#if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_TYPE_SLOTS
#define __PYX_DICT_VERSION_INIT  ((PY_UINT64_T) -1)
#define __PYX_GET_DICT_VERSION(dict)  (((PyDictObject*)(dict))->ma_version_tag)
#define __PYX_UPDATE_DICT_CACHE(dict, value, cache_var, version_var)\
    (version_var) = __PYX_GET_DICT_VERSION(dict);\
    (cache_var) = (value);
#define __PYX_PY_DICT_LOOKUP_IF_MODIFIED(VAR, DICT, LOOKUP) {\
    static PY_UINT64_T __pyx_dict_version = 0;\
    static PyObject *__pyx_dict_cached_value = NULL;\
    if (likely(__PYX_GET_DICT_VERSION(DICT) == __pyx_dict_version)) {\
        (VAR) = __pyx_dict_cached_value;\
    } else {\
        (VAR) = __pyx_dict_cached_value = (LOOKUP);\
        __pyx_dict_version = __PYX_GET_DICT_VERSION(DICT);\
    }\
}
static CYTHON_INLINE PY_UINT64_T __Pyx_get_tp_dict_version(PyObject *obj);
static CYTHON_INLINE PY_UINT64_T __Pyx_get_object_dict_version(PyObject *obj);
static CYTHON_INLINE int __Pyx_object_dict_version_matches(PyObject* obj, PY_UINT64_T tp_dict_version, PY_UINT64_T obj_dict_version);
#else
#define __PYX_GET_DICT_VERSION(dict)  (0)
#define __PYX_UPDATE_DICT_CACHE(dict, value, cache_var, version_var)
#define __PYX_PY_DICT_LOOKUP_IF_MODIFIED(VAR, DICT, LOOKUP)  (VAR) = (LOOKUP);
#endif

/* GetModuleGlobalName.proto */
#if CYTHON_USE_DICT_VERSIONS
#define __Pyx_GetModuleGlobalName(var, name)  {\
    static PY_UINT64_T __pyx_dict_version = 0;\
    static PyObject *__pyx_dict_cached_value = NULL;\
    (var) = (likely(__pyx_dict_version == __PYX_GET_DICT_VERSION(__pyx_d))) ?\
        (likely(__pyx_dict_cached_value) ? __Pyx_NewRef(__pyx_dict_cached_value) : __Pyx_GetBuiltinName(name)) :\
        __Pyx__GetModuleGlobalName(name, &__pyx_dict_version, &__pyx_dict_cached_value);\
}
#define __Pyx_GetModuleGlobalNameUncached(var, name)  {\
    PY_UINT64_T __pyx_dict_version;\
    PyObject *__pyx_dict_cached_value;\
    (var) = __Pyx__GetModuleGlobalName(name, &__pyx_dict_version, &__pyx_dict_cached_value);\
}
static PyObject *__Pyx__GetModuleGlobalName(PyObject *name, PY_UINT64_T *dict_version, PyObject **dict_cached_value);
#else
#define __Pyx_GetModuleGlobalName(var, name)  (var) = __Pyx__GetModuleGlobalName(name)
#define __Pyx_GetModuleGlobalNameUncached(var, name)  (var) = __Pyx__GetModuleGlobalName(name)
static CYTHON_INLINE PyObject *__Pyx__GetModuleGlobalName(PyObject *name);
#endif

/* pyobject_as_double.proto */
static double __Pyx__PyObject_AsDouble(PyObject* obj);
#if CYTHON_COMPILING_IN_PYPY
#define __Pyx_PyObject_AsDouble(obj)\
(likely(PyFloat_CheckExact(obj)) ? PyFloat_AS_DOUBLE(obj) :\
 likely(PyInt_CheckExact(obj)) ?\
 PyFloat_AsDouble(obj) : __Pyx__PyObject_AsDouble(obj))
#else
#define __Pyx_PyObject_AsDouble(obj)\
((likely(PyFloat_CheckExact(obj))) ?\
 PyFloat_AS_DOUBLE(obj) : __Pyx__PyObject_AsDouble(obj))
#endif

/* GetItemInt.proto */
#define __Pyx_GetItemInt(o, i, type, is_signed, to_py_func, is_list, wraparound, boundscheck)\
    (__Pyx_fits_Py_ssize_t(i, type, is_signed) ?\
    __Pyx_GetItemInt_Fast(o, (Py_ssize_t)i, is_list, wraparound, boundscheck) :\
    (is_list ? (PyErr_SetString(PyExc_IndexError, "list index out of range"), (PyObject*)NULL) :\
               __Pyx_GetItemInt_Generic(o, to_py_func(i))))
#define __Pyx_GetItemInt_List(o, i, type, is_signed, to_py_func, is_list, wraparound, boundscheck)\
    (__Pyx_fits_Py_ssize_t(i, type, is_signed) ?\
    __Pyx_GetItemInt_List_Fast(o, (Py_ssize_t)i, wraparound, boundscheck) :\
    (PyErr_SetString(PyExc_IndexError, "list index out of range"), (PyObject*)NULL))
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_List_Fast(PyObject *o, Py_ssize_t i,
                                                              int wraparound, int boundscheck);
#define __Pyx_GetItemInt_Tuple(o, i, type, is_signed, to_py_func, is_list, wraparound, boundscheck)\
    (__Pyx_fits_Py_ssize_t(i, type, is_signed) ?\
    __Pyx_GetItemInt_Tuple_Fast(o, (Py_ssize_t)i, wraparound, boundscheck) :\
    (PyErr_SetString(PyExc_IndexError, "tuple index out of range"), (PyObject*)NULL))
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_Tuple_Fast(PyObject *o, Py_ssize_t i,
                                                              int wraparound, int boundscheck);
static PyObject *__Pyx_GetItemInt_Generic(PyObject *o, PyObject* j);
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_Fast(PyObject *o, Py_ssize_t i,
                                                     int is_list, int wraparound, int boundscheck);

/* BytesEquals.proto */
static CYTHON_INLINE int __Pyx_PyBytes_Equals(PyObject* s1, PyObject* s2, int equals);

/* GetException.proto */
#if CYTHON_FAST_THREAD_STATE
#define __Pyx_GetException(type, value, tb)  __Pyx__GetException(__pyx_tstate, type, value, tb)
static int __Pyx__GetException(PyThreadState *tstate, PyObject **type, PyObject **value, PyObject **tb);
#else
static int __Pyx_GetException(PyObject **type, PyObject **value, PyObject **tb);
#endif

/* FastTypeChecks.proto */
#if CYTHON_COMPILING_IN_CPYTHON
#define __Pyx_TypeCheck(obj, type) __Pyx_IsSubtype(Py_TYPE(obj), (PyTypeObject *)type)
static CYTHON_INLINE int __Pyx_IsSubtype(PyTypeObject *a, PyTypeObject *b);
static CYTHON_INLINE int __Pyx_PyErr_GivenExceptionMatches(PyObject *err, PyObject *type);
static CYTHON_INLINE int __Pyx_PyErr_GivenExceptionMatches2(PyObject *err, PyObject *type1, PyObject *type2);
#else
#define __Pyx_TypeCheck(obj, type) PyObject_TypeCheck(obj, (PyTypeObject *)type)
#define __Pyx_PyErr_GivenExceptionMatches(err, type) PyErr_GivenExceptionMatches(err, type)
#define __Pyx_PyErr_GivenExceptionMatches2(err, type1, type2) (PyErr_GivenExceptionMatches(err, type1) || PyErr_GivenExceptionMatches(err, type2))
#endif
#define __Pyx_PyException_Check(obj) __Pyx_TypeCheck(obj, PyExc_Exception)

/* UnicodeEquals.proto */
static CYTHON_INLINE int __Pyx_PyUnicode_Equals(PyObject* s1, PyObject* s2, int equals);

/* None.proto */
static CYTHON_INLINE void __Pyx_RaiseUnboundLocalError(const char *varname);

/* RaiseTooManyValuesToUnpack.proto */
static CYTHON_INLINE void __Pyx_RaiseTooManyValuesError(Py_ssize_t expected);

/* RaiseNeedMoreValuesToUnpack.proto */
static CYTHON_INLINE void __Pyx_RaiseNeedMoreValuesError(Py_ssize_t index);

/* IterFinish.proto */
static CYTHON_INLINE int __Pyx_IterFinish(void);

/* UnpackItemEndCheck.proto */
static int __Pyx_IternextUnpackEndCheck(PyObject *retval, Py_ssize_t expected);

/* RaiseException.proto */
static void __Pyx_Raise(PyObject *type, PyObject *value, PyObject *tb, PyObject *cause);

/* PyObject_GenericGetAttrNoDict.proto */
#if CYTHON_USE_TYPE_SLOTS && CYTHON_USE_PYTYPE_LOOKUP && PY_VERSION_HEX < 0x03070000
static CYTHON_INLINE PyObject* __Pyx_PyObject_GenericGetAttrNoDict(PyObject* obj, PyObject* attr_name);
#else
#define __Pyx_PyObject_GenericGetAttrNoDict PyObject_GenericGetAttr
#endif

/* PyObject_GenericGetAttr.proto */
#if CYTHON_USE_TYPE_SLOTS && CYTHON_USE_PYTYPE_LOOKUP && PY_VERSION_HEX < 0x03070000
static PyObject* __Pyx_PyObject_GenericGetAttr(PyObject* obj, PyObject* attr_name);
#else
#define __Pyx_PyObject_GenericGetAttr PyObject_GenericGetAttr
#endif

/* SetVTable.proto */
static int __Pyx_SetVtable(PyObject *dict, void *vtable);

/* PyErrExceptionMatches.proto */
#if CYTHON_FAST_THREAD_STATE
#define __Pyx_PyErr_ExceptionMatches(err) __Pyx_PyErr_ExceptionMatchesInState(__pyx_tstate, err)
static CYTHON_INLINE int __Pyx_PyErr_ExceptionMatchesInState(PyThreadState* tstate, PyObject* err);
#else
#define __Pyx_PyErr_ExceptionMatches(err)  PyErr_ExceptionMatches(err)
#endif

/* PyObjectGetAttrStrNoError.proto */
static CYTHON_INLINE PyObject* __Pyx_PyObject_GetAttrStrNoError(PyObject* obj, PyObject* attr_name);

/* SetupReduce.proto */
static int __Pyx_setup_reduce(PyObject* type_obj);

/* Import.proto */
static PyObject *__Pyx_Import(PyObject *name, PyObject *from_list, int level);

/* ImportFrom.proto */
static PyObject* __Pyx_ImportFrom(PyObject* module, PyObject* name);

/* CLineInTraceback.proto */
#ifdef CYTHON_CLINE_IN_TRACEBACK
#define __Pyx_CLineForTraceback(tstate, c_line)  (((CYTHON_CLINE_IN_TRACEBACK)) ? c_line : 0)
#else
static int __Pyx_CLineForTraceback(PyThreadState *tstate, int c_line);
#endif

/* CodeObjectCache.proto */
typedef struct {
    PyCodeObject* code_object;
    int code_line;
} __Pyx_CodeObjectCacheEntry;
struct __Pyx_CodeObjectCache {
    int count;
    int max_count;
    __Pyx_CodeObjectCacheEntry* entries;
};
static struct __Pyx_CodeObjectCache __pyx_code_cache = {0,0,NULL};
static int __pyx_bisect_code_objects(__Pyx_CodeObjectCacheEntry* entries, int count, int code_line);
static PyCodeObject *__pyx_find_code_object(int code_line);
static void __pyx_insert_code_object(int code_line, PyCodeObject* code_object);

/* AddTraceback.proto */
static void __Pyx_AddTraceback(const char *funcname, int c_line,
                               int py_line, const char *filename);

/* GCCDiagnostics.proto */
#if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6))
#define __Pyx_HAS_GCC_DIAGNOSTIC
#endif

/* CIntFromPy.proto */
static CYTHON_INLINE int __Pyx_PyInt_As_int(PyObject *);

/* CIntToPy.proto */
static CYTHON_INLINE PyObject* __Pyx_PyInt_From_long(long value);

/* CIntToPy.proto */
static CYTHON_INLINE PyObject* __Pyx_PyInt_From_int(int value);

/* CIntFromPy.proto */
static CYTHON_INLINE long __Pyx_PyInt_As_long(PyObject *);

/* CheckBinaryVersion.proto */
static int __Pyx_check_binary_version(void);

/* InitStrings.proto */
static int __Pyx_InitStrings(__Pyx_StringTabEntry *t);

static PyObject *__pyx_f_13serialmanager_13SerialManager_send_to_arduino(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, char *__pyx_v_sendString); /* proto*/
static char *__pyx_f_13serialmanager_13SerialManager_receive_from_arduino(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_waiting_arduino(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_waiting_arduino_nano(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_waiting_arduino_nano2(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_waiting_arduino_mega(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_close(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, int __pyx_skip_dispatch); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_inWaiting(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, int __pyx_skip_dispatch); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_led_controller(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, char *__pyx_v_result); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_prendi(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_take_front(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_daje(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_val); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_tell_to_stop(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, char *__pyx_v_command); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_tell_to_move3(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_command); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_movement_command(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_command); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_tell_to_move(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_comm); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_tell_to_move2(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_command); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_cgamepad(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, char *__pyx_v_command, float __pyx_v_strafe, float __pyx_v_forward, float __pyx_v_angular); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_cprova1(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_csendledcommand(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_conferma); /* proto*/

/* Module declarations from 'libc.string' */

/* Module declarations from 'libc.stdlib' */

/* Module declarations from 'serialmanager' */
static PyTypeObject *__pyx_ptype_13serialmanager_SerialManager = 0;
#define __Pyx_MODULE_NAME "serialmanager"
extern int __pyx_module_is_main_serialmanager;
int __pyx_module_is_main_serialmanager = 0;

/* Implementation of 'serialmanager' */
static PyObject *__pyx_builtin_print;
static PyObject *__pyx_builtin_map;
static PyObject *__pyx_builtin_TypeError;
static const char __pyx_k_[] = "{}{}";
static const char __pyx_k__3[] = "";
static const char __pyx_k__4[] = "{},{},{},{}";
static const char __pyx_k__5[] = ">";
static const char __pyx_k__6[] = ",";
static const char __pyx_k_re[] = "re";
static const char __pyx_k_map[] = "map";
static const char __pyx_k_sad[] = "sad";
static const char __pyx_k_ser[] = "ser";
static const char __pyx_k_auto[] = "<auto>";
static const char __pyx_k_find[] = "find";
static const char __pyx_k_main[] = "__main__";
static const char __pyx_k_name[] = "name";
static const char __pyx_k_read[] = "read";
static const char __pyx_k_test[] = "__test__";
static const char __pyx_k_time[] = "time";
static const char __pyx_k_UTF_8[] = "UTF-8";
static const char __pyx_k_close[] = "close";
static const char __pyx_k_gobac[] = "gobac";
static const char __pyx_k_golef[] = "golef";
static const char __pyx_k_gorig[] = "gorig";
static const char __pyx_k_happy[] = "happy";
static const char __pyx_k_print[] = "print";
static const char __pyx_k_sad_2[] = "<sad";
static const char __pyx_k_sleep[] = "sleep";
static const char __pyx_k_split[] = "split";
static const char __pyx_k_write[] = "write";
static const char __pyx_k_decode[] = "decode";
static const char __pyx_k_encode[] = "encode";
static const char __pyx_k_follow[] = "follow";
static const char __pyx_k_format[] = "format";
static const char __pyx_k_gostra[] = "gostra";
static const char __pyx_k_import[] = "__import__";
static const char __pyx_k_name_2[] = "__name__";
static const char __pyx_k_normal[] = "normal";
static const char __pyx_k_reduce[] = "__reduce__";
static const char __pyx_k_serial[] = "serial";
static const char __pyx_k_strafe[] = "strafe";
static const char __pyx_k_strict[] = "strict";
static const char __pyx_k_ans1_is[] = "ans1 is {}";
static const char __pyx_k_ans2_is[] = "ans2 is {}";
static const char __pyx_k_ans3_is[] = "ans3 is {}";
static const char __pyx_k_ans4_is[] = "ans4 is {}";
static const char __pyx_k_command[] = "command";
static const char __pyx_k_forward[] = "forward";
static const char __pyx_k_gobac_2[] = "<gobac";
static const char __pyx_k_golef_2[] = "<golef";
static const char __pyx_k_gorig_2[] = "<gorig";
static const char __pyx_k_happy_2[] = "<happy";
static const char __pyx_k_joystop[] = "joystop";
static const char __pyx_k_led_col[] = "led_col";
static const char __pyx_k_ansel_is[] = "ansel is{}";
static const char __pyx_k_follow_2[] = "<follow>";
static const char __pyx_k_getstate[] = "__getstate__";
static const char __pyx_k_gostra_2[] = "<gostra";
static const char __pyx_k_normal_2[] = "<normal";
static const char __pyx_k_setstate[] = "__setstate__";
static const char __pyx_k_OimiQueue[] = "OimiQueue";
static const char __pyx_k_TypeError[] = "TypeError";
static const char __pyx_k_ang_speed[] = "ang_speed";
static const char __pyx_k_animation[] = "animation";
static const char __pyx_k_for_speed[] = "for_speed";
static const char __pyx_k_inWaiting[] = "inWaiting";
static const char __pyx_k_joystop_2[] = "<joystop";
static const char __pyx_k_move_comm[] = "move_comm";
static const char __pyx_k_reduce_ex[] = "__reduce_ex__";
static const char __pyx_k_simplygo2[] = "simplygo2";
static const char __pyx_k_autonomous[] = "autonomous";
static const char __pyx_k_end_marker[] = "end_marker";
static const char __pyx_k_enthusiast[] = "enthusiast";
static const char __pyx_k_pyx_vtable[] = "__pyx_vtable__";
static const char __pyx_k_serialutil[] = "serialutil";
static const char __pyx_k_OimiManager[] = "OimiManager";
static const char __pyx_k_OimiProcess[] = "OimiProcess";
static const char __pyx_k_simplygo2_2[] = "<simplygo2";
static const char __pyx_k_enthusiast_2[] = "<enthusiast";
static const char __pyx_k_oimi_process[] = "oimi_process";
static const char __pyx_k_start_marker[] = "start_marker";
static const char __pyx_k_strafe_speed[] = "strafe_speed";
static const char __pyx_k_SerialManager[] = "SerialManager";
static const char __pyx_k_angular_speed[] = "angular_speed";
static const char __pyx_k_choosen_audio[] = "choosen_audio";
static const char __pyx_k_forward_speed[] = "forward_speed";
static const char __pyx_k_reduce_cython[] = "__reduce_cython__";
static const char __pyx_k_ENTER_GAMEPAD2[] = "---> ENTER GAMEPAD2";
static const char __pyx_k_Reply_Received[] = "Reply Received {}";
static const char __pyx_k_Sent_from_RASPI[] = "Sent from RASPI {}";
static const char __pyx_k_Sent_from_Raspi[] = "Sent from Raspi {} ";
static const char __pyx_k_SerialException[] = "SerialException";
static const char __pyx_k_setstate_cython[] = "__setstate_cython__";
static const char __pyx_k_OimiStartProcess[] = "OimiStartProcess";
static const char __pyx_k_command_again_is[] = "command again  is {}";
static const char __pyx_k_Sent_from_Raspi_2[] = "Sent from Raspi {}";
static const char __pyx_k_Reply_led_Received[] = "Reply led Received{}";
static const char __pyx_k_cline_in_traceback[] = "cline_in_traceback";
static const char __pyx_k_command_to_send_is[] = "command to send is {}";
static const char __pyx_k_Sent_from_Raspi_stop[] = "Sent from Raspi {} stop!";
static const char __pyx_k_Reply_control_Received[] = "Reply control Received{}";
static const char __pyx_k_Reply_Received_take_one[] = "Reply Received take one {}";
static const char __pyx_k_Reply_Received_happiness[] = "Reply Received happiness {}";
static const char __pyx_k_Reply_led_Received_arduino[] = "Reply led Received arduino - {}";
static const char __pyx_k_Sent_from_Raspi_TEST_PRESS[] = "Sent from Raspi {} TEST PRESS";
static const char __pyx_k_Sent_from_Raspi_take_front[] = "Sent from Raspi {} take front";
static const char __pyx_k_command_has_been_send_comma_is[] = "command has been send comma is {}";
static const char __pyx_k_command_to_send_for_gamepad_is[] = "command to send for gamepad is {}";
static const char __pyx_k_no_default___reduce___due_to_non[] = "no default __reduce__ due to non-trivial __cinit__";
static PyObject *__pyx_kp_u_;
static PyObject *__pyx_kp_u_ENTER_GAMEPAD2;
static PyObject *__pyx_n_s_OimiManager;
static PyObject *__pyx_n_s_OimiProcess;
static PyObject *__pyx_n_s_OimiQueue;
static PyObject *__pyx_n_s_OimiStartProcess;
static PyObject *__pyx_kp_u_Reply_Received;
static PyObject *__pyx_kp_u_Reply_Received_happiness;
static PyObject *__pyx_kp_u_Reply_Received_take_one;
static PyObject *__pyx_kp_u_Reply_control_Received;
static PyObject *__pyx_kp_u_Reply_led_Received;
static PyObject *__pyx_kp_u_Reply_led_Received_arduino;
static PyObject *__pyx_kp_u_Sent_from_RASPI;
static PyObject *__pyx_kp_u_Sent_from_Raspi;
static PyObject *__pyx_kp_u_Sent_from_Raspi_2;
static PyObject *__pyx_kp_u_Sent_from_Raspi_TEST_PRESS;
static PyObject *__pyx_kp_u_Sent_from_Raspi_stop;
static PyObject *__pyx_kp_u_Sent_from_Raspi_take_front;
static PyObject *__pyx_n_s_SerialException;
static PyObject *__pyx_n_s_SerialManager;
static PyObject *__pyx_n_s_TypeError;
static PyObject *__pyx_kp_u_UTF_8;
static PyObject *__pyx_kp_b__3;
static PyObject *__pyx_kp_u__3;
static PyObject *__pyx_kp_u__4;
static PyObject *__pyx_kp_u__5;
static PyObject *__pyx_kp_u__6;
static PyObject *__pyx_n_s_ang_speed;
static PyObject *__pyx_n_s_angular_speed;
static PyObject *__pyx_n_s_animation;
static PyObject *__pyx_kp_u_ans1_is;
static PyObject *__pyx_kp_u_ans2_is;
static PyObject *__pyx_kp_u_ans3_is;
static PyObject *__pyx_kp_u_ans4_is;
static PyObject *__pyx_kp_u_ansel_is;
static PyObject *__pyx_kp_b_auto;
static PyObject *__pyx_kp_u_auto;
static PyObject *__pyx_n_u_autonomous;
static PyObject *__pyx_n_s_choosen_audio;
static PyObject *__pyx_n_s_cline_in_traceback;
static PyObject *__pyx_n_s_close;
static PyObject *__pyx_n_s_command;
static PyObject *__pyx_kp_u_command_again_is;
static PyObject *__pyx_kp_u_command_has_been_send_comma_is;
static PyObject *__pyx_kp_u_command_to_send_for_gamepad_is;
static PyObject *__pyx_kp_u_command_to_send_is;
static PyObject *__pyx_n_s_decode;
static PyObject *__pyx_n_s_encode;
static PyObject *__pyx_n_s_end_marker;
static PyObject *__pyx_n_u_enthusiast;
static PyObject *__pyx_kp_u_enthusiast_2;
static PyObject *__pyx_n_s_find;
static PyObject *__pyx_n_u_follow;
static PyObject *__pyx_kp_b_follow_2;
static PyObject *__pyx_n_s_for_speed;
static PyObject *__pyx_n_s_format;
static PyObject *__pyx_n_s_forward;
static PyObject *__pyx_n_s_forward_speed;
static PyObject *__pyx_n_s_getstate;
static PyObject *__pyx_n_u_gobac;
static PyObject *__pyx_kp_u_gobac_2;
static PyObject *__pyx_n_u_golef;
static PyObject *__pyx_kp_u_golef_2;
static PyObject *__pyx_n_u_gorig;
static PyObject *__pyx_kp_u_gorig_2;
static PyObject *__pyx_n_u_gostra;
static PyObject *__pyx_kp_u_gostra_2;
static PyObject *__pyx_n_u_happy;
static PyObject *__pyx_kp_u_happy_2;
static PyObject *__pyx_n_s_import;
static PyObject *__pyx_n_s_inWaiting;
static PyObject *__pyx_n_u_joystop;
static PyObject *__pyx_kp_u_joystop_2;
static PyObject *__pyx_n_s_led_col;
static PyObject *__pyx_n_s_main;
static PyObject *__pyx_n_s_map;
static PyObject *__pyx_n_s_move_comm;
static PyObject *__pyx_n_s_name;
static PyObject *__pyx_n_s_name_2;
static PyObject *__pyx_kp_s_no_default___reduce___due_to_non;
static PyObject *__pyx_n_u_normal;
static PyObject *__pyx_kp_u_normal_2;
static PyObject *__pyx_n_s_oimi_process;
static PyObject *__pyx_n_s_print;
static PyObject *__pyx_n_s_pyx_vtable;
static PyObject *__pyx_n_s_re;
static PyObject *__pyx_n_s_read;
static PyObject *__pyx_n_s_reduce;
static PyObject *__pyx_n_s_reduce_cython;
static PyObject *__pyx_n_s_reduce_ex;
static PyObject *__pyx_n_u_sad;
static PyObject *__pyx_kp_u_sad_2;
static PyObject *__pyx_n_s_ser;
static PyObject *__pyx_n_s_serial;
static PyObject *__pyx_n_s_serialutil;
static PyObject *__pyx_n_s_setstate;
static PyObject *__pyx_n_s_setstate_cython;
static PyObject *__pyx_n_u_simplygo2;
static PyObject *__pyx_kp_u_simplygo2_2;
static PyObject *__pyx_n_s_sleep;
static PyObject *__pyx_n_s_split;
static PyObject *__pyx_n_s_start_marker;
static PyObject *__pyx_n_s_strafe;
static PyObject *__pyx_n_s_strafe_speed;
static PyObject *__pyx_n_u_strict;
static PyObject *__pyx_n_s_test;
static PyObject *__pyx_n_s_time;
static PyObject *__pyx_n_s_write;
static int __pyx_pf_13serialmanager_13SerialManager___cinit__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_name, int __pyx_v_start_marker, int __pyx_v_end_marker, PyObject *__pyx_v_ser); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_2close(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_4inWaiting(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_6waitard(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_8waitard_nano(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_10waitard_nano2(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_12waitard_mega(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_14led_co(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_result); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_16takempx(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_18dajetutta(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_val); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_20move_order(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_move_comm); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_22react_results_final(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_move_comm, PyObject *__pyx_v_led_col, PyObject *__pyx_v_animation, PyObject *__pyx_v_for_speed, PyObject *__pyx_v_ang_speed, CYTHON_UNUSED PyObject *__pyx_v_choosen_audio); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_24test_for_movement(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_26test_for_stop(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_28prendiprendi(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_30send_reset(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_32gamepad2(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_move_comm, PyObject *__pyx_v_strafe, PyObject *__pyx_v_forward); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_34gamepad(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_command, PyObject *__pyx_v_strafe_speed, PyObject *__pyx_v_forward_speed, PyObject *__pyx_v_angular_speed); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_36prova1(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_38sendled(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_conferma); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_4name___get__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static int __pyx_pf_13serialmanager_13SerialManager_4name_2__set__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static int __pyx_pf_13serialmanager_13SerialManager_4name_4__del__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_12start_marker___get__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static int __pyx_pf_13serialmanager_13SerialManager_12start_marker_2__set__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_10end_marker___get__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static int __pyx_pf_13serialmanager_13SerialManager_10end_marker_2__set__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_3ser___get__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static int __pyx_pf_13serialmanager_13SerialManager_3ser_2__set__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_value); /* proto */
static int __pyx_pf_13serialmanager_13SerialManager_3ser_4__del__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_40__reduce_cython__(CYTHON_UNUSED struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self); /* proto */
static PyObject *__pyx_pf_13serialmanager_13SerialManager_42__setstate_cython__(CYTHON_UNUSED struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, CYTHON_UNUSED PyObject *__pyx_v___pyx_state); /* proto */
static PyObject *__pyx_tp_new_13serialmanager_SerialManager(PyTypeObject *t, PyObject *a, PyObject *k); /*proto*/
static PyObject *__pyx_int_0;
static PyObject *__pyx_int_1;
static PyObject *__pyx_int_2;
static PyObject *__pyx_int_3;
static PyObject *__pyx_int_4;
static PyObject *__pyx_int_5;
static PyObject *__pyx_int_6;
static PyObject *__pyx_int_7;
static PyObject *__pyx_int_8;
static PyObject *__pyx_int_neg_1;
static PyObject *__pyx_tuple__2;
static PyObject *__pyx_tuple__7;
static PyObject *__pyx_tuple__8;
static PyObject *__pyx_tuple__9;
/* Late includes */

/* "serialmanager.pyx":17
 * 		int end_marker
 * 		ser
 * 	def __cinit__(self, str name, int start_marker, int end_marker, ser):             # <<<<<<<<<<<<<<
 * 		self.name = name
 * 		self.start_marker = start_marker
 */

/* Python wrapper */
static int __pyx_pw_13serialmanager_13SerialManager_1__cinit__(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static int __pyx_pw_13serialmanager_13SerialManager_1__cinit__(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_v_name = 0;
  int __pyx_v_start_marker;
  int __pyx_v_end_marker;
  PyObject *__pyx_v_ser = 0;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__cinit__ (wrapper)", 0);
  {
    static PyObject **__pyx_pyargnames[] = {&__pyx_n_s_name,&__pyx_n_s_start_marker,&__pyx_n_s_end_marker,&__pyx_n_s_ser,0};
    PyObject* values[4] = {0,0,0,0};
    if (unlikely(__pyx_kwds)) {
      Py_ssize_t kw_args;
      const Py_ssize_t pos_args = PyTuple_GET_SIZE(__pyx_args);
      switch (pos_args) {
        case  4: values[3] = PyTuple_GET_ITEM(__pyx_args, 3);
        CYTHON_FALLTHROUGH;
        case  3: values[2] = PyTuple_GET_ITEM(__pyx_args, 2);
        CYTHON_FALLTHROUGH;
        case  2: values[1] = PyTuple_GET_ITEM(__pyx_args, 1);
        CYTHON_FALLTHROUGH;
        case  1: values[0] = PyTuple_GET_ITEM(__pyx_args, 0);
        CYTHON_FALLTHROUGH;
        case  0: break;
        default: goto __pyx_L5_argtuple_error;
      }
      kw_args = PyDict_Size(__pyx_kwds);
      switch (pos_args) {
        case  0:
        if (likely((values[0] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_name)) != 0)) kw_args--;
        else goto __pyx_L5_argtuple_error;
        CYTHON_FALLTHROUGH;
        case  1:
        if (likely((values[1] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_start_marker)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("__cinit__", 1, 4, 4, 1); __PYX_ERR(0, 17, __pyx_L3_error)
        }
        CYTHON_FALLTHROUGH;
        case  2:
        if (likely((values[2] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_end_marker)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("__cinit__", 1, 4, 4, 2); __PYX_ERR(0, 17, __pyx_L3_error)
        }
        CYTHON_FALLTHROUGH;
        case  3:
        if (likely((values[3] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_ser)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("__cinit__", 1, 4, 4, 3); __PYX_ERR(0, 17, __pyx_L3_error)
        }
      }
      if (unlikely(kw_args > 0)) {
        if (unlikely(__Pyx_ParseOptionalKeywords(__pyx_kwds, __pyx_pyargnames, 0, values, pos_args, "__cinit__") < 0)) __PYX_ERR(0, 17, __pyx_L3_error)
      }
    } else if (PyTuple_GET_SIZE(__pyx_args) != 4) {
      goto __pyx_L5_argtuple_error;
    } else {
      values[0] = PyTuple_GET_ITEM(__pyx_args, 0);
      values[1] = PyTuple_GET_ITEM(__pyx_args, 1);
      values[2] = PyTuple_GET_ITEM(__pyx_args, 2);
      values[3] = PyTuple_GET_ITEM(__pyx_args, 3);
    }
    __pyx_v_name = ((PyObject*)values[0]);
    __pyx_v_start_marker = __Pyx_PyInt_As_int(values[1]); if (unlikely((__pyx_v_start_marker == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 17, __pyx_L3_error)
    __pyx_v_end_marker = __Pyx_PyInt_As_int(values[2]); if (unlikely((__pyx_v_end_marker == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 17, __pyx_L3_error)
    __pyx_v_ser = values[3];
  }
  goto __pyx_L4_argument_unpacking_done;
  __pyx_L5_argtuple_error:;
  __Pyx_RaiseArgtupleInvalid("__cinit__", 1, 4, 4, PyTuple_GET_SIZE(__pyx_args)); __PYX_ERR(0, 17, __pyx_L3_error)
  __pyx_L3_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.__cinit__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __Pyx_RefNannyFinishContext();
  return -1;
  __pyx_L4_argument_unpacking_done:;
  if (unlikely(!__Pyx_ArgTypeTest(((PyObject *)__pyx_v_name), (&PyUnicode_Type), 1, "name", 1))) __PYX_ERR(0, 17, __pyx_L1_error)
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager___cinit__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), __pyx_v_name, __pyx_v_start_marker, __pyx_v_end_marker, __pyx_v_ser);

  /* function exit code */
  goto __pyx_L0;
  __pyx_L1_error:;
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_13serialmanager_13SerialManager___cinit__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_name, int __pyx_v_start_marker, int __pyx_v_end_marker, PyObject *__pyx_v_ser) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__cinit__", 0);
  __Pyx_TraceCall("__cinit__", __pyx_f[0], 17, 0, __PYX_ERR(0, 17, __pyx_L1_error));

  /* "serialmanager.pyx":18
 * 		ser
 * 	def __cinit__(self, str name, int start_marker, int end_marker, ser):
 * 		self.name = name             # <<<<<<<<<<<<<<
 * 		self.start_marker = start_marker
 * 		self.end_marker = end_marker
 */
  __Pyx_TraceLine(18,0,__PYX_ERR(0, 18, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_name);
  __Pyx_GIVEREF(__pyx_v_name);
  __Pyx_GOTREF(__pyx_v_self->name);
  __Pyx_DECREF(__pyx_v_self->name);
  __pyx_v_self->name = __pyx_v_name;

  /* "serialmanager.pyx":19
 * 	def __cinit__(self, str name, int start_marker, int end_marker, ser):
 * 		self.name = name
 * 		self.start_marker = start_marker             # <<<<<<<<<<<<<<
 * 		self.end_marker = end_marker
 * 		self.ser = ser
 */
  __Pyx_TraceLine(19,0,__PYX_ERR(0, 19, __pyx_L1_error))
  __pyx_v_self->start_marker = __pyx_v_start_marker;

  /* "serialmanager.pyx":20
 * 		self.name = name
 * 		self.start_marker = start_marker
 * 		self.end_marker = end_marker             # <<<<<<<<<<<<<<
 * 		self.ser = ser
 * 
 */
  __Pyx_TraceLine(20,0,__PYX_ERR(0, 20, __pyx_L1_error))
  __pyx_v_self->end_marker = __pyx_v_end_marker;

  /* "serialmanager.pyx":21
 * 		self.start_marker = start_marker
 * 		self.end_marker = end_marker
 * 		self.ser = ser             # <<<<<<<<<<<<<<
 * 
 * 	cdef send_to_arduino(self, char* sendString):
 */
  __Pyx_TraceLine(21,0,__PYX_ERR(0, 21, __pyx_L1_error))
  __Pyx_INCREF(__pyx_v_ser);
  __Pyx_GIVEREF(__pyx_v_ser);
  __Pyx_GOTREF(__pyx_v_self->ser);
  __Pyx_DECREF(__pyx_v_self->ser);
  __pyx_v_self->ser = __pyx_v_ser;

  /* "serialmanager.pyx":17
 * 		int end_marker
 * 		ser
 * 	def __cinit__(self, str name, int start_marker, int end_marker, ser):             # <<<<<<<<<<<<<<
 * 		self.name = name
 * 		self.start_marker = start_marker
 */

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.__cinit__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":23
 * 		self.ser = ser
 * 
 * 	cdef send_to_arduino(self, char* sendString):             # <<<<<<<<<<<<<<
 * 		#self.ser.write(bytes(sendString))
 * 		self.ser.write(sendString)
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_send_to_arduino(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, char *__pyx_v_sendString) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("send_to_arduino", 0);
  __Pyx_TraceCall("send_to_arduino", __pyx_f[0], 23, 0, __PYX_ERR(0, 23, __pyx_L1_error));

  /* "serialmanager.pyx":25
 * 	cdef send_to_arduino(self, char* sendString):
 * 		#self.ser.write(bytes(sendString))
 * 		self.ser.write(sendString)             # <<<<<<<<<<<<<<
 * 
 * 	'''read on serial correct string'''
 */
  __Pyx_TraceLine(25,0,__PYX_ERR(0, 25, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_write); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 25, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = __Pyx_PyBytes_FromString(__pyx_v_sendString); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 25, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_4 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
    __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_2);
    if (likely(__pyx_t_4)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
      __Pyx_INCREF(__pyx_t_4);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_2, function);
    }
  }
  __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_2, __pyx_t_4, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 25, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":23
 * 		self.ser = ser
 * 
 * 	cdef send_to_arduino(self, char* sendString):             # <<<<<<<<<<<<<<
 * 		#self.ser.write(bytes(sendString))
 * 		self.ser.write(sendString)
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_AddTraceback("serialmanager.SerialManager.send_to_arduino", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":28
 * 
 * 	'''read on serial correct string'''
 * 	cdef char* receive_from_arduino(self):             # <<<<<<<<<<<<<<
 * 		cdef char* z = 'z'
 * 		cdef char* ch = ''
 */

static char *__pyx_f_13serialmanager_13SerialManager_receive_from_arduino(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  char *__pyx_v_z;
  char *__pyx_v_ch;
  PyObject *__pyx_v_put = 0;
  PyObject *__pyx_v_cha = NULL;
  PyObject *__pyx_v_va = NULL;
  PyObject *__pyx_v_cc = NULL;
  char *__pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  long __pyx_t_5;
  int __pyx_t_6;
  PyObject *__pyx_t_7 = NULL;
  PyObject *__pyx_t_8 = NULL;
  int __pyx_t_9;
  PyObject *__pyx_t_10 = NULL;
  char *__pyx_t_11;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("receive_from_arduino", 0);
  __Pyx_TraceCall("receive_from_arduino", __pyx_f[0], 28, 0, __PYX_ERR(0, 28, __pyx_L1_error));

  /* "serialmanager.pyx":29
 * 	'''read on serial correct string'''
 * 	cdef char* receive_from_arduino(self):
 * 		cdef char* z = 'z'             # <<<<<<<<<<<<<<
 * 		cdef char* ch = ''
 * 		cdef bytes put = z
 */
  __Pyx_TraceLine(29,0,__PYX_ERR(0, 29, __pyx_L1_error))
  __pyx_v_z = ((char *)"z");

  /* "serialmanager.pyx":30
 * 	cdef char* receive_from_arduino(self):
 * 		cdef char* z = 'z'
 * 		cdef char* ch = ''             # <<<<<<<<<<<<<<
 * 		cdef bytes put = z
 * 		cha = ch.decode('UTF-8', 'strict')
 */
  __Pyx_TraceLine(30,0,__PYX_ERR(0, 30, __pyx_L1_error))
  __pyx_v_ch = ((char *)"");

  /* "serialmanager.pyx":31
 * 		cdef char* z = 'z'
 * 		cdef char* ch = ''
 * 		cdef bytes put = z             # <<<<<<<<<<<<<<
 * 		cha = ch.decode('UTF-8', 'strict')
 * 		try:
 */
  __Pyx_TraceLine(31,0,__PYX_ERR(0, 31, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyBytes_FromString(__pyx_v_z); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 31, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_v_put = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":32
 * 		cdef char* ch = ''
 * 		cdef bytes put = z
 * 		cha = ch.decode('UTF-8', 'strict')             # <<<<<<<<<<<<<<
 * 		try:
 * 			while ord(put) != self.start_marker:
 */
  __Pyx_TraceLine(32,0,__PYX_ERR(0, 32, __pyx_L1_error))
  __pyx_t_1 = __Pyx_decode_c_string(__pyx_v_ch, 0, strlen(__pyx_v_ch), NULL, NULL, PyUnicode_DecodeUTF8); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 32, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_v_cha = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":33
 * 		cdef bytes put = z
 * 		cha = ch.decode('UTF-8', 'strict')
 * 		try:             # <<<<<<<<<<<<<<
 * 			while ord(put) != self.start_marker:
 * 				put = self.ser.read()
 */
  __Pyx_TraceLine(33,0,__PYX_ERR(0, 33, __pyx_L1_error))
  {
    __Pyx_PyThreadState_declare
    __Pyx_PyThreadState_assign
    __Pyx_ExceptionSave(&__pyx_t_2, &__pyx_t_3, &__pyx_t_4);
    __Pyx_XGOTREF(__pyx_t_2);
    __Pyx_XGOTREF(__pyx_t_3);
    __Pyx_XGOTREF(__pyx_t_4);
    /*try:*/ {

      /* "serialmanager.pyx":34
 * 		cha = ch.decode('UTF-8', 'strict')
 * 		try:
 * 			while ord(put) != self.start_marker:             # <<<<<<<<<<<<<<
 * 				put = self.ser.read()
 * 			while ord(put) != self.end_marker:
 */
      __Pyx_TraceLine(34,0,__PYX_ERR(0, 34, __pyx_L3_error))
      while (1) {
        __pyx_t_5 = __Pyx_PyObject_Ord(__pyx_v_put); if (unlikely(__pyx_t_5 == ((long)(long)(Py_UCS4)-1))) __PYX_ERR(0, 34, __pyx_L3_error)
        __pyx_t_6 = ((__pyx_t_5 != __pyx_v_self->start_marker) != 0);
        if (!__pyx_t_6) break;

        /* "serialmanager.pyx":35
 * 		try:
 * 			while ord(put) != self.start_marker:
 * 				put = self.ser.read()             # <<<<<<<<<<<<<<
 * 			while ord(put) != self.end_marker:
 * 				if ord(put) != self.start_marker:
 */
        __Pyx_TraceLine(35,0,__PYX_ERR(0, 35, __pyx_L3_error))
        __pyx_t_7 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_read); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 35, __pyx_L3_error)
        __Pyx_GOTREF(__pyx_t_7);
        __pyx_t_8 = NULL;
        if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_7))) {
          __pyx_t_8 = PyMethod_GET_SELF(__pyx_t_7);
          if (likely(__pyx_t_8)) {
            PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_7);
            __Pyx_INCREF(__pyx_t_8);
            __Pyx_INCREF(function);
            __Pyx_DECREF_SET(__pyx_t_7, function);
          }
        }
        __pyx_t_1 = (__pyx_t_8) ? __Pyx_PyObject_CallOneArg(__pyx_t_7, __pyx_t_8) : __Pyx_PyObject_CallNoArg(__pyx_t_7);
        __Pyx_XDECREF(__pyx_t_8); __pyx_t_8 = 0;
        if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 35, __pyx_L3_error)
        __Pyx_GOTREF(__pyx_t_1);
        __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
        if (!(likely(PyBytes_CheckExact(__pyx_t_1))||((__pyx_t_1) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "bytes", Py_TYPE(__pyx_t_1)->tp_name), 0))) __PYX_ERR(0, 35, __pyx_L3_error)
        __Pyx_DECREF_SET(__pyx_v_put, ((PyObject*)__pyx_t_1));
        __pyx_t_1 = 0;
      }

      /* "serialmanager.pyx":36
 * 			while ord(put) != self.start_marker:
 * 				put = self.ser.read()
 * 			while ord(put) != self.end_marker:             # <<<<<<<<<<<<<<
 * 				if ord(put) != self.start_marker:
 * 					va = put.decode('UTF-8', 'strict')
 */
      __Pyx_TraceLine(36,0,__PYX_ERR(0, 36, __pyx_L3_error))
      while (1) {
        __pyx_t_5 = __Pyx_PyObject_Ord(__pyx_v_put); if (unlikely(__pyx_t_5 == ((long)(long)(Py_UCS4)-1))) __PYX_ERR(0, 36, __pyx_L3_error)
        __pyx_t_6 = ((__pyx_t_5 != __pyx_v_self->end_marker) != 0);
        if (!__pyx_t_6) break;

        /* "serialmanager.pyx":37
 * 				put = self.ser.read()
 * 			while ord(put) != self.end_marker:
 * 				if ord(put) != self.start_marker:             # <<<<<<<<<<<<<<
 * 					va = put.decode('UTF-8', 'strict')
 * 					cha = '{}{}'.format(cha,va)
 */
        __Pyx_TraceLine(37,0,__PYX_ERR(0, 37, __pyx_L3_error))
        __pyx_t_5 = __Pyx_PyObject_Ord(__pyx_v_put); if (unlikely(__pyx_t_5 == ((long)(long)(Py_UCS4)-1))) __PYX_ERR(0, 37, __pyx_L3_error)
        __pyx_t_6 = ((__pyx_t_5 != __pyx_v_self->start_marker) != 0);
        if (__pyx_t_6) {

          /* "serialmanager.pyx":38
 * 			while ord(put) != self.end_marker:
 * 				if ord(put) != self.start_marker:
 * 					va = put.decode('UTF-8', 'strict')             # <<<<<<<<<<<<<<
 * 					cha = '{}{}'.format(cha,va)
 * 				put = self.ser.read()
 */
          __Pyx_TraceLine(38,0,__PYX_ERR(0, 38, __pyx_L3_error))
          if (unlikely(__pyx_v_put == Py_None)) {
            PyErr_Format(PyExc_AttributeError, "'NoneType' object has no attribute '%.30s'", "decode");
            __PYX_ERR(0, 38, __pyx_L3_error)
          }
          __pyx_t_1 = __Pyx_decode_bytes(__pyx_v_put, 0, PY_SSIZE_T_MAX, NULL, NULL, PyUnicode_DecodeUTF8); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 38, __pyx_L3_error)
          __Pyx_GOTREF(__pyx_t_1);
          __Pyx_XDECREF_SET(__pyx_v_va, __pyx_t_1);
          __pyx_t_1 = 0;

          /* "serialmanager.pyx":39
 * 				if ord(put) != self.start_marker:
 * 					va = put.decode('UTF-8', 'strict')
 * 					cha = '{}{}'.format(cha,va)             # <<<<<<<<<<<<<<
 * 				put = self.ser.read()
 * 		except:
 */
          __Pyx_TraceLine(39,0,__PYX_ERR(0, 39, __pyx_L3_error))
          __pyx_t_7 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_, __pyx_n_s_format); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 39, __pyx_L3_error)
          __Pyx_GOTREF(__pyx_t_7);
          __pyx_t_8 = NULL;
          __pyx_t_9 = 0;
          if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_7))) {
            __pyx_t_8 = PyMethod_GET_SELF(__pyx_t_7);
            if (likely(__pyx_t_8)) {
              PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_7);
              __Pyx_INCREF(__pyx_t_8);
              __Pyx_INCREF(function);
              __Pyx_DECREF_SET(__pyx_t_7, function);
              __pyx_t_9 = 1;
            }
          }
          #if CYTHON_FAST_PYCALL
          if (PyFunction_Check(__pyx_t_7)) {
            PyObject *__pyx_temp[3] = {__pyx_t_8, __pyx_v_cha, __pyx_v_va};
            __pyx_t_1 = __Pyx_PyFunction_FastCall(__pyx_t_7, __pyx_temp+1-__pyx_t_9, 2+__pyx_t_9); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 39, __pyx_L3_error)
            __Pyx_XDECREF(__pyx_t_8); __pyx_t_8 = 0;
            __Pyx_GOTREF(__pyx_t_1);
          } else
          #endif
          #if CYTHON_FAST_PYCCALL
          if (__Pyx_PyFastCFunction_Check(__pyx_t_7)) {
            PyObject *__pyx_temp[3] = {__pyx_t_8, __pyx_v_cha, __pyx_v_va};
            __pyx_t_1 = __Pyx_PyCFunction_FastCall(__pyx_t_7, __pyx_temp+1-__pyx_t_9, 2+__pyx_t_9); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 39, __pyx_L3_error)
            __Pyx_XDECREF(__pyx_t_8); __pyx_t_8 = 0;
            __Pyx_GOTREF(__pyx_t_1);
          } else
          #endif
          {
            __pyx_t_10 = PyTuple_New(2+__pyx_t_9); if (unlikely(!__pyx_t_10)) __PYX_ERR(0, 39, __pyx_L3_error)
            __Pyx_GOTREF(__pyx_t_10);
            if (__pyx_t_8) {
              __Pyx_GIVEREF(__pyx_t_8); PyTuple_SET_ITEM(__pyx_t_10, 0, __pyx_t_8); __pyx_t_8 = NULL;
            }
            __Pyx_INCREF(__pyx_v_cha);
            __Pyx_GIVEREF(__pyx_v_cha);
            PyTuple_SET_ITEM(__pyx_t_10, 0+__pyx_t_9, __pyx_v_cha);
            __Pyx_INCREF(__pyx_v_va);
            __Pyx_GIVEREF(__pyx_v_va);
            PyTuple_SET_ITEM(__pyx_t_10, 1+__pyx_t_9, __pyx_v_va);
            __pyx_t_1 = __Pyx_PyObject_Call(__pyx_t_7, __pyx_t_10, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 39, __pyx_L3_error)
            __Pyx_GOTREF(__pyx_t_1);
            __Pyx_DECREF(__pyx_t_10); __pyx_t_10 = 0;
          }
          __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
          __Pyx_DECREF_SET(__pyx_v_cha, __pyx_t_1);
          __pyx_t_1 = 0;

          /* "serialmanager.pyx":37
 * 				put = self.ser.read()
 * 			while ord(put) != self.end_marker:
 * 				if ord(put) != self.start_marker:             # <<<<<<<<<<<<<<
 * 					va = put.decode('UTF-8', 'strict')
 * 					cha = '{}{}'.format(cha,va)
 */
        }

        /* "serialmanager.pyx":40
 * 					va = put.decode('UTF-8', 'strict')
 * 					cha = '{}{}'.format(cha,va)
 * 				put = self.ser.read()             # <<<<<<<<<<<<<<
 * 		except:
 * 			pass
 */
        __Pyx_TraceLine(40,0,__PYX_ERR(0, 40, __pyx_L3_error))
        __pyx_t_7 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_read); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 40, __pyx_L3_error)
        __Pyx_GOTREF(__pyx_t_7);
        __pyx_t_10 = NULL;
        if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_7))) {
          __pyx_t_10 = PyMethod_GET_SELF(__pyx_t_7);
          if (likely(__pyx_t_10)) {
            PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_7);
            __Pyx_INCREF(__pyx_t_10);
            __Pyx_INCREF(function);
            __Pyx_DECREF_SET(__pyx_t_7, function);
          }
        }
        __pyx_t_1 = (__pyx_t_10) ? __Pyx_PyObject_CallOneArg(__pyx_t_7, __pyx_t_10) : __Pyx_PyObject_CallNoArg(__pyx_t_7);
        __Pyx_XDECREF(__pyx_t_10); __pyx_t_10 = 0;
        if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 40, __pyx_L3_error)
        __Pyx_GOTREF(__pyx_t_1);
        __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
        if (!(likely(PyBytes_CheckExact(__pyx_t_1))||((__pyx_t_1) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "bytes", Py_TYPE(__pyx_t_1)->tp_name), 0))) __PYX_ERR(0, 40, __pyx_L3_error)
        __Pyx_DECREF_SET(__pyx_v_put, ((PyObject*)__pyx_t_1));
        __pyx_t_1 = 0;
      }

      /* "serialmanager.pyx":33
 * 		cdef bytes put = z
 * 		cha = ch.decode('UTF-8', 'strict')
 * 		try:             # <<<<<<<<<<<<<<
 * 			while ord(put) != self.start_marker:
 * 				put = self.ser.read()
 */
    }
    __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    goto __pyx_L8_try_end;
    __pyx_L3_error:;
    __Pyx_XDECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_XDECREF(__pyx_t_10); __pyx_t_10 = 0;
    __Pyx_XDECREF(__pyx_t_7); __pyx_t_7 = 0;
    __Pyx_XDECREF(__pyx_t_8); __pyx_t_8 = 0;

    /* "serialmanager.pyx":41
 * 					cha = '{}{}'.format(cha,va)
 * 				put = self.ser.read()
 * 		except:             # <<<<<<<<<<<<<<
 * 			pass
 * 		cc = cha.encode('UTF-8', 'strict')
 */
    __Pyx_TraceLine(41,0,__PYX_ERR(0, 41, __pyx_L5_except_error))
    /*except:*/ {
      __Pyx_ErrRestore(0,0,0);
      goto __pyx_L4_exception_handled;
    }
    __pyx_L5_except_error:;

    /* "serialmanager.pyx":33
 * 		cdef bytes put = z
 * 		cha = ch.decode('UTF-8', 'strict')
 * 		try:             # <<<<<<<<<<<<<<
 * 			while ord(put) != self.start_marker:
 * 				put = self.ser.read()
 */
    __Pyx_XGIVEREF(__pyx_t_2);
    __Pyx_XGIVEREF(__pyx_t_3);
    __Pyx_XGIVEREF(__pyx_t_4);
    __Pyx_ExceptionReset(__pyx_t_2, __pyx_t_3, __pyx_t_4);
    goto __pyx_L1_error;
    __pyx_L4_exception_handled:;
    __Pyx_XGIVEREF(__pyx_t_2);
    __Pyx_XGIVEREF(__pyx_t_3);
    __Pyx_XGIVEREF(__pyx_t_4);
    __Pyx_ExceptionReset(__pyx_t_2, __pyx_t_3, __pyx_t_4);
    __pyx_L8_try_end:;
  }

  /* "serialmanager.pyx":43
 * 		except:
 * 			pass
 * 		cc = cha.encode('UTF-8', 'strict')             # <<<<<<<<<<<<<<
 * 		return cc
 * 
 */
  __Pyx_TraceLine(43,0,__PYX_ERR(0, 43, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_v_cha, __pyx_n_s_encode); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 43, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_t_7 = __Pyx_PyObject_Call(__pyx_t_1, __pyx_tuple__2, NULL); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 43, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_7);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_v_cc = __pyx_t_7;
  __pyx_t_7 = 0;

  /* "serialmanager.pyx":44
 * 			pass
 * 		cc = cha.encode('UTF-8', 'strict')
 * 		return cc             # <<<<<<<<<<<<<<
 * 
 * 	'''connection boot/reboot test communication'''
 */
  __Pyx_TraceLine(44,0,__PYX_ERR(0, 44, __pyx_L1_error))
  __pyx_t_11 = __Pyx_PyObject_AsWritableString(__pyx_v_cc); if (unlikely((!__pyx_t_11) && PyErr_Occurred())) __PYX_ERR(0, 44, __pyx_L1_error)
  __pyx_r = __pyx_t_11;
  goto __pyx_L0;

  /* "serialmanager.pyx":28
 * 
 * 	'''read on serial correct string'''
 * 	cdef char* receive_from_arduino(self):             # <<<<<<<<<<<<<<
 * 		cdef char* z = 'z'
 * 		cdef char* ch = ''
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_7);
  __Pyx_XDECREF(__pyx_t_8);
  __Pyx_XDECREF(__pyx_t_10);
  __Pyx_WriteUnraisable("serialmanager.SerialManager.receive_from_arduino", __pyx_clineno, __pyx_lineno, __pyx_filename, 1, 0);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_put);
  __Pyx_XDECREF(__pyx_v_cha);
  __Pyx_XDECREF(__pyx_v_va);
  __Pyx_XDECREF(__pyx_v_cc);
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":47
 * 
 * 	'''connection boot/reboot test communication'''
 * 	cdef waiting_arduino(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'hei'
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_waiting_arduino(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  char *__pyx_v_msg;
  char *__pyx_v_right_msg;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  int __pyx_t_5;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("waiting_arduino", 0);
  __Pyx_TraceCall("waiting_arduino", __pyx_f[0], 47, 0, __PYX_ERR(0, 47, __pyx_L1_error));

  /* "serialmanager.pyx":48
 * 	'''connection boot/reboot test communication'''
 * 	cdef waiting_arduino(self):
 * 		cdef char* msg = ''             # <<<<<<<<<<<<<<
 * 		cdef char* right_msg = 'hei'
 * 		while msg.find(right_msg) == -1:
 */
  __Pyx_TraceLine(48,0,__PYX_ERR(0, 48, __pyx_L1_error))
  __pyx_v_msg = ((char *)"");

  /* "serialmanager.pyx":49
 * 	cdef waiting_arduino(self):
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'hei'             # <<<<<<<<<<<<<<
 * 		while msg.find(right_msg) == -1:
 * 			while self.ser.inWaiting() == 0:
 */
  __Pyx_TraceLine(49,0,__PYX_ERR(0, 49, __pyx_L1_error))
  __pyx_v_right_msg = ((char *)"hei");

  /* "serialmanager.pyx":50
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'hei'
 * 		while msg.find(right_msg) == -1:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(50,0,__PYX_ERR(0, 50, __pyx_L1_error))
  while (1) {
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_msg); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 50, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_find); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 50, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_right_msg); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 50, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_4 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_4)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_4);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_4, __pyx_t_2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2);
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 50, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_t_1, __pyx_int_neg_1, -1L, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 50, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_t_5 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_5 < 0)) __PYX_ERR(0, 50, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (!__pyx_t_5) break;

    /* "serialmanager.pyx":51
 * 		cdef char* right_msg = 'hei'
 * 		while msg.find(right_msg) == -1:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			msg = self.receive_from_arduino()
 */
    __Pyx_TraceLine(51,0,__PYX_ERR(0, 51, __pyx_L1_error))
    while (1) {
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 51, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __pyx_t_2 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_1))) {
        __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_1);
        if (likely(__pyx_t_2)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_1);
          __Pyx_INCREF(__pyx_t_2);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_1, function);
        }
      }
      __pyx_t_3 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_1, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_1);
      __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 51, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      __pyx_t_1 = __Pyx_PyInt_EqObjC(__pyx_t_3, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 51, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_5 = __Pyx_PyObject_IsTrue(__pyx_t_1); if (unlikely(__pyx_t_5 < 0)) __PYX_ERR(0, 51, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      if (!__pyx_t_5) break;
    }

    /* "serialmanager.pyx":53
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			msg = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print(msg)
 * 	cdef waiting_arduino_nano(self):
 */
    __Pyx_TraceLine(53,0,__PYX_ERR(0, 53, __pyx_L1_error))
    __pyx_v_msg = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":54
 * 				pass
 * 			msg = self.receive_from_arduino()
 * 			print(msg)             # <<<<<<<<<<<<<<
 * 	cdef waiting_arduino_nano(self):
 * 		cdef char* msg = ''
 */
    __Pyx_TraceLine(54,0,__PYX_ERR(0, 54, __pyx_L1_error))
    __pyx_t_1 = __Pyx_PyBytes_FromString(__pyx_v_msg); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 54, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 54, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  }

  /* "serialmanager.pyx":47
 * 
 * 	'''connection boot/reboot test communication'''
 * 	cdef waiting_arduino(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'hei'
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_AddTraceback("serialmanager.SerialManager.waiting_arduino", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":55
 * 			msg = self.receive_from_arduino()
 * 			print(msg)
 * 	cdef waiting_arduino_nano(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Nano1 is ready'
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_waiting_arduino_nano(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  char *__pyx_v_msg;
  char *__pyx_v_right_msg;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  int __pyx_t_5;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("waiting_arduino_nano", 0);
  __Pyx_TraceCall("waiting_arduino_nano", __pyx_f[0], 55, 0, __PYX_ERR(0, 55, __pyx_L1_error));

  /* "serialmanager.pyx":56
 * 			print(msg)
 * 	cdef waiting_arduino_nano(self):
 * 		cdef char* msg = ''             # <<<<<<<<<<<<<<
 * 		cdef char* right_msg = 'Nano1 is ready'
 * 		while msg.find(right_msg) == -1:
 */
  __Pyx_TraceLine(56,0,__PYX_ERR(0, 56, __pyx_L1_error))
  __pyx_v_msg = ((char *)"");

  /* "serialmanager.pyx":57
 * 	cdef waiting_arduino_nano(self):
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Nano1 is ready'             # <<<<<<<<<<<<<<
 * 		while msg.find(right_msg) == -1:
 * 			while self.ser.inWaiting() == 0:
 */
  __Pyx_TraceLine(57,0,__PYX_ERR(0, 57, __pyx_L1_error))
  __pyx_v_right_msg = ((char *)"Nano1 is ready");

  /* "serialmanager.pyx":58
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Nano1 is ready'
 * 		while msg.find(right_msg) == -1:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(58,0,__PYX_ERR(0, 58, __pyx_L1_error))
  while (1) {
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_msg); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 58, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_find); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 58, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_right_msg); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 58, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_4 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_4)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_4);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_4, __pyx_t_2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2);
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 58, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_t_1, __pyx_int_neg_1, -1L, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 58, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_t_5 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_5 < 0)) __PYX_ERR(0, 58, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (!__pyx_t_5) break;

    /* "serialmanager.pyx":59
 * 		cdef char* right_msg = 'Nano1 is ready'
 * 		while msg.find(right_msg) == -1:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			msg = self.receive_from_arduino()
 */
    __Pyx_TraceLine(59,0,__PYX_ERR(0, 59, __pyx_L1_error))
    while (1) {
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 59, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __pyx_t_2 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_1))) {
        __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_1);
        if (likely(__pyx_t_2)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_1);
          __Pyx_INCREF(__pyx_t_2);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_1, function);
        }
      }
      __pyx_t_3 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_1, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_1);
      __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 59, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      __pyx_t_1 = __Pyx_PyInt_EqObjC(__pyx_t_3, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 59, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_5 = __Pyx_PyObject_IsTrue(__pyx_t_1); if (unlikely(__pyx_t_5 < 0)) __PYX_ERR(0, 59, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      if (!__pyx_t_5) break;
    }

    /* "serialmanager.pyx":61
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			msg = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print(msg)
 * 	cdef waiting_arduino_nano2(self):
 */
    __Pyx_TraceLine(61,0,__PYX_ERR(0, 61, __pyx_L1_error))
    __pyx_v_msg = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":62
 * 				pass
 * 			msg = self.receive_from_arduino()
 * 			print(msg)             # <<<<<<<<<<<<<<
 * 	cdef waiting_arduino_nano2(self):
 * 		cdef char* msg = ''
 */
    __Pyx_TraceLine(62,0,__PYX_ERR(0, 62, __pyx_L1_error))
    __pyx_t_1 = __Pyx_PyBytes_FromString(__pyx_v_msg); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 62, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 62, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  }

  /* "serialmanager.pyx":55
 * 			msg = self.receive_from_arduino()
 * 			print(msg)
 * 	cdef waiting_arduino_nano(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Nano1 is ready'
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_AddTraceback("serialmanager.SerialManager.waiting_arduino_nano", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":63
 * 			msg = self.receive_from_arduino()
 * 			print(msg)
 * 	cdef waiting_arduino_nano2(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Nano2 is ready'
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_waiting_arduino_nano2(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  char *__pyx_v_msg;
  char *__pyx_v_right_msg;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  int __pyx_t_5;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("waiting_arduino_nano2", 0);
  __Pyx_TraceCall("waiting_arduino_nano2", __pyx_f[0], 63, 0, __PYX_ERR(0, 63, __pyx_L1_error));

  /* "serialmanager.pyx":64
 * 			print(msg)
 * 	cdef waiting_arduino_nano2(self):
 * 		cdef char* msg = ''             # <<<<<<<<<<<<<<
 * 		cdef char* right_msg = 'Nano2 is ready'
 * 		while msg.find(right_msg) == -1:
 */
  __Pyx_TraceLine(64,0,__PYX_ERR(0, 64, __pyx_L1_error))
  __pyx_v_msg = ((char *)"");

  /* "serialmanager.pyx":65
 * 	cdef waiting_arduino_nano2(self):
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Nano2 is ready'             # <<<<<<<<<<<<<<
 * 		while msg.find(right_msg) == -1:
 * 			while self.ser.inWaiting() == 0:
 */
  __Pyx_TraceLine(65,0,__PYX_ERR(0, 65, __pyx_L1_error))
  __pyx_v_right_msg = ((char *)"Nano2 is ready");

  /* "serialmanager.pyx":66
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Nano2 is ready'
 * 		while msg.find(right_msg) == -1:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(66,0,__PYX_ERR(0, 66, __pyx_L1_error))
  while (1) {
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_msg); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 66, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_find); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 66, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_right_msg); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 66, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_4 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_4)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_4);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_4, __pyx_t_2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2);
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 66, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_t_1, __pyx_int_neg_1, -1L, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 66, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_t_5 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_5 < 0)) __PYX_ERR(0, 66, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (!__pyx_t_5) break;

    /* "serialmanager.pyx":67
 * 		cdef char* right_msg = 'Nano2 is ready'
 * 		while msg.find(right_msg) == -1:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			msg = self.receive_from_arduino()
 */
    __Pyx_TraceLine(67,0,__PYX_ERR(0, 67, __pyx_L1_error))
    while (1) {
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 67, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __pyx_t_2 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_1))) {
        __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_1);
        if (likely(__pyx_t_2)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_1);
          __Pyx_INCREF(__pyx_t_2);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_1, function);
        }
      }
      __pyx_t_3 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_1, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_1);
      __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 67, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      __pyx_t_1 = __Pyx_PyInt_EqObjC(__pyx_t_3, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 67, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_5 = __Pyx_PyObject_IsTrue(__pyx_t_1); if (unlikely(__pyx_t_5 < 0)) __PYX_ERR(0, 67, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      if (!__pyx_t_5) break;
    }

    /* "serialmanager.pyx":69
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			msg = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print(msg)
 * 	cdef waiting_arduino_mega(self):
 */
    __Pyx_TraceLine(69,0,__PYX_ERR(0, 69, __pyx_L1_error))
    __pyx_v_msg = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":70
 * 				pass
 * 			msg = self.receive_from_arduino()
 * 			print(msg)             # <<<<<<<<<<<<<<
 * 	cdef waiting_arduino_mega(self):
 * 		cdef char* msg = ''
 */
    __Pyx_TraceLine(70,0,__PYX_ERR(0, 70, __pyx_L1_error))
    __pyx_t_1 = __Pyx_PyBytes_FromString(__pyx_v_msg); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 70, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 70, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  }

  /* "serialmanager.pyx":63
 * 			msg = self.receive_from_arduino()
 * 			print(msg)
 * 	cdef waiting_arduino_nano2(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Nano2 is ready'
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_AddTraceback("serialmanager.SerialManager.waiting_arduino_nano2", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":71
 * 			msg = self.receive_from_arduino()
 * 			print(msg)
 * 	cdef waiting_arduino_mega(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Mega is ready'
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_waiting_arduino_mega(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  char *__pyx_v_msg;
  char *__pyx_v_right_msg;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  int __pyx_t_5;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("waiting_arduino_mega", 0);
  __Pyx_TraceCall("waiting_arduino_mega", __pyx_f[0], 71, 0, __PYX_ERR(0, 71, __pyx_L1_error));

  /* "serialmanager.pyx":72
 * 			print(msg)
 * 	cdef waiting_arduino_mega(self):
 * 		cdef char* msg = ''             # <<<<<<<<<<<<<<
 * 		cdef char* right_msg = 'Mega is ready'
 * 		while msg.find(right_msg) == -1:
 */
  __Pyx_TraceLine(72,0,__PYX_ERR(0, 72, __pyx_L1_error))
  __pyx_v_msg = ((char *)"");

  /* "serialmanager.pyx":73
 * 	cdef waiting_arduino_mega(self):
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Mega is ready'             # <<<<<<<<<<<<<<
 * 		while msg.find(right_msg) == -1:
 * 			while self.ser.inWaiting() == 0:
 */
  __Pyx_TraceLine(73,0,__PYX_ERR(0, 73, __pyx_L1_error))
  __pyx_v_right_msg = ((char *)"Mega is ready");

  /* "serialmanager.pyx":74
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Mega is ready'
 * 		while msg.find(right_msg) == -1:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(74,0,__PYX_ERR(0, 74, __pyx_L1_error))
  while (1) {
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_msg); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 74, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_find); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 74, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_right_msg); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 74, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_4 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_4)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_4);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_4, __pyx_t_2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2);
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 74, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_t_1, __pyx_int_neg_1, -1L, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 74, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_t_5 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_5 < 0)) __PYX_ERR(0, 74, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (!__pyx_t_5) break;

    /* "serialmanager.pyx":75
 * 		cdef char* right_msg = 'Mega is ready'
 * 		while msg.find(right_msg) == -1:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			msg = self.receive_from_arduino()
 */
    __Pyx_TraceLine(75,0,__PYX_ERR(0, 75, __pyx_L1_error))
    while (1) {
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 75, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __pyx_t_2 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_1))) {
        __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_1);
        if (likely(__pyx_t_2)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_1);
          __Pyx_INCREF(__pyx_t_2);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_1, function);
        }
      }
      __pyx_t_3 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_1, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_1);
      __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 75, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      __pyx_t_1 = __Pyx_PyInt_EqObjC(__pyx_t_3, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 75, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_5 = __Pyx_PyObject_IsTrue(__pyx_t_1); if (unlikely(__pyx_t_5 < 0)) __PYX_ERR(0, 75, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      if (!__pyx_t_5) break;
    }

    /* "serialmanager.pyx":77
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			msg = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print(msg)
 * 
 */
    __Pyx_TraceLine(77,0,__PYX_ERR(0, 77, __pyx_L1_error))
    __pyx_v_msg = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":78
 * 				pass
 * 			msg = self.receive_from_arduino()
 * 			print(msg)             # <<<<<<<<<<<<<<
 * 
 * 	cpdef close(self):
 */
    __Pyx_TraceLine(78,0,__PYX_ERR(0, 78, __pyx_L1_error))
    __pyx_t_1 = __Pyx_PyBytes_FromString(__pyx_v_msg); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 78, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 78, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  }

  /* "serialmanager.pyx":71
 * 			msg = self.receive_from_arduino()
 * 			print(msg)
 * 	cdef waiting_arduino_mega(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Mega is ready'
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_AddTraceback("serialmanager.SerialManager.waiting_arduino_mega", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":80
 * 			print(msg)
 * 
 * 	cpdef close(self):             # <<<<<<<<<<<<<<
 * 		self.ser.close()
 * 
 */

static PyObject *__pyx_pw_13serialmanager_13SerialManager_3close(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_close(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, int __pyx_skip_dispatch) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("close", 0);
  __Pyx_TraceCall("close", __pyx_f[0], 80, 0, __PYX_ERR(0, 80, __pyx_L1_error));
  /* Check if called by wrapper */
  if (unlikely(__pyx_skip_dispatch)) ;
  /* Check if overridden in Python */
  else if (unlikely((Py_TYPE(((PyObject *)__pyx_v_self))->tp_dictoffset != 0) || (Py_TYPE(((PyObject *)__pyx_v_self))->tp_flags & (Py_TPFLAGS_IS_ABSTRACT | Py_TPFLAGS_HEAPTYPE)))) {
    #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    static PY_UINT64_T __pyx_tp_dict_version = __PYX_DICT_VERSION_INIT, __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
    if (unlikely(!__Pyx_object_dict_version_matches(((PyObject *)__pyx_v_self), __pyx_tp_dict_version, __pyx_obj_dict_version))) {
      PY_UINT64_T __pyx_type_dict_guard = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      #endif
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(((PyObject *)__pyx_v_self), __pyx_n_s_close); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 80, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      if (!PyCFunction_Check(__pyx_t_1) || (PyCFunction_GET_FUNCTION(__pyx_t_1) != (PyCFunction)(void*)__pyx_pw_13serialmanager_13SerialManager_3close)) {
        __Pyx_XDECREF(__pyx_r);
        __Pyx_INCREF(__pyx_t_1);
        __pyx_t_3 = __pyx_t_1; __pyx_t_4 = NULL;
        if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_3))) {
          __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
          if (likely(__pyx_t_4)) {
            PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
            __Pyx_INCREF(__pyx_t_4);
            __Pyx_INCREF(function);
            __Pyx_DECREF_SET(__pyx_t_3, function);
          }
        }
        __pyx_t_2 = (__pyx_t_4) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
        __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
        if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 80, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_2);
        __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
        __pyx_r = __pyx_t_2;
        __pyx_t_2 = 0;
        __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
        goto __pyx_L0;
      }
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
      __pyx_tp_dict_version = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      __pyx_obj_dict_version = __Pyx_get_object_dict_version(((PyObject *)__pyx_v_self));
      if (unlikely(__pyx_type_dict_guard != __pyx_tp_dict_version)) {
        __pyx_tp_dict_version = __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
      }
      #endif
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    }
    #endif
  }

  /* "serialmanager.pyx":81
 * 
 * 	cpdef close(self):
 * 		self.ser.close()             # <<<<<<<<<<<<<<
 * 
 * 	cpdef inWaiting(self):
 */
  __Pyx_TraceLine(81,0,__PYX_ERR(0, 81, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_close); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 81, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
    __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_2);
    if (likely(__pyx_t_3)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
      __Pyx_INCREF(__pyx_t_3);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_2, function);
    }
  }
  __pyx_t_1 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 81, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":80
 * 			print(msg)
 * 
 * 	cpdef close(self):             # <<<<<<<<<<<<<<
 * 		self.ser.close()
 * 
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_AddTraceback("serialmanager.SerialManager.close", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_3close(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_2close[] = "SerialManager.close(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_3close(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("close (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_2close(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_2close(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("close", 0);
  __Pyx_TraceCall("close (wrapper)", __pyx_f[0], 80, 0, __PYX_ERR(0, 80, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __pyx_f_13serialmanager_13SerialManager_close(__pyx_v_self, 1); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 80, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.close", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":83
 * 		self.ser.close()
 * 
 * 	cpdef inWaiting(self):             # <<<<<<<<<<<<<<
 * 		self.ser.inWaiting()
 * 
 */

static PyObject *__pyx_pw_13serialmanager_13SerialManager_5inWaiting(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static PyObject *__pyx_f_13serialmanager_13SerialManager_inWaiting(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, int __pyx_skip_dispatch) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("inWaiting", 0);
  __Pyx_TraceCall("inWaiting", __pyx_f[0], 83, 0, __PYX_ERR(0, 83, __pyx_L1_error));
  /* Check if called by wrapper */
  if (unlikely(__pyx_skip_dispatch)) ;
  /* Check if overridden in Python */
  else if (unlikely((Py_TYPE(((PyObject *)__pyx_v_self))->tp_dictoffset != 0) || (Py_TYPE(((PyObject *)__pyx_v_self))->tp_flags & (Py_TPFLAGS_IS_ABSTRACT | Py_TPFLAGS_HEAPTYPE)))) {
    #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    static PY_UINT64_T __pyx_tp_dict_version = __PYX_DICT_VERSION_INIT, __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
    if (unlikely(!__Pyx_object_dict_version_matches(((PyObject *)__pyx_v_self), __pyx_tp_dict_version, __pyx_obj_dict_version))) {
      PY_UINT64_T __pyx_type_dict_guard = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      #endif
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(((PyObject *)__pyx_v_self), __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 83, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      if (!PyCFunction_Check(__pyx_t_1) || (PyCFunction_GET_FUNCTION(__pyx_t_1) != (PyCFunction)(void*)__pyx_pw_13serialmanager_13SerialManager_5inWaiting)) {
        __Pyx_XDECREF(__pyx_r);
        __Pyx_INCREF(__pyx_t_1);
        __pyx_t_3 = __pyx_t_1; __pyx_t_4 = NULL;
        if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_3))) {
          __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
          if (likely(__pyx_t_4)) {
            PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
            __Pyx_INCREF(__pyx_t_4);
            __Pyx_INCREF(function);
            __Pyx_DECREF_SET(__pyx_t_3, function);
          }
        }
        __pyx_t_2 = (__pyx_t_4) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
        __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
        if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 83, __pyx_L1_error)
        __Pyx_GOTREF(__pyx_t_2);
        __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
        __pyx_r = __pyx_t_2;
        __pyx_t_2 = 0;
        __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
        goto __pyx_L0;
      }
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
      __pyx_tp_dict_version = __Pyx_get_tp_dict_version(((PyObject *)__pyx_v_self));
      __pyx_obj_dict_version = __Pyx_get_object_dict_version(((PyObject *)__pyx_v_self));
      if (unlikely(__pyx_type_dict_guard != __pyx_tp_dict_version)) {
        __pyx_tp_dict_version = __pyx_obj_dict_version = __PYX_DICT_VERSION_INIT;
      }
      #endif
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      #if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_PYTYPE_LOOKUP && CYTHON_USE_TYPE_SLOTS
    }
    #endif
  }

  /* "serialmanager.pyx":84
 * 
 * 	cpdef inWaiting(self):
 * 		self.ser.inWaiting()             # <<<<<<<<<<<<<<
 * 
 * 	cdef led_controller(self, char* result):
 */
  __Pyx_TraceLine(84,0,__PYX_ERR(0, 84, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 84, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
    __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_2);
    if (likely(__pyx_t_3)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
      __Pyx_INCREF(__pyx_t_3);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_2, function);
    }
  }
  __pyx_t_1 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 84, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":83
 * 		self.ser.close()
 * 
 * 	cpdef inWaiting(self):             # <<<<<<<<<<<<<<
 * 		self.ser.inWaiting()
 * 
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_AddTraceback("serialmanager.SerialManager.inWaiting", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_5inWaiting(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_4inWaiting[] = "SerialManager.inWaiting(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_5inWaiting(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("inWaiting (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_4inWaiting(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_4inWaiting(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("inWaiting", 0);
  __Pyx_TraceCall("inWaiting (wrapper)", __pyx_f[0], 83, 0, __PYX_ERR(0, 83, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __pyx_f_13serialmanager_13SerialManager_inWaiting(__pyx_v_self, 1); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 83, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.inWaiting", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":86
 * 		self.ser.inWaiting()
 * 
 * 	cdef led_controller(self, char* result):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_led_controller(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, char *__pyx_v_result) {
  int __pyx_v_waitingForReply;
  char *__pyx_v_dataReceived;
  CYTHON_UNUSED PyObject *__pyx_v_startledtime = NULL;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  int __pyx_t_4;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("led_controller", 0);
  __Pyx_TraceCall("led_controller", __pyx_f[0], 86, 0, __PYX_ERR(0, 86, __pyx_L1_error));

  /* "serialmanager.pyx":87
 * 
 * 	cdef led_controller(self, char* result):
 * 		cdef bint waitingForReply = False             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived
 * 		startledtime = time.time()
 */
  __Pyx_TraceLine(87,0,__PYX_ERR(0, 87, __pyx_L1_error))
  __pyx_v_waitingForReply = 0;

  /* "serialmanager.pyx":89
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived
 * 		startledtime = time.time()             # <<<<<<<<<<<<<<
 * 		if waitingForReply == False:
 * 			self.send_to_arduino(result)
 */
  __Pyx_TraceLine(89,0,__PYX_ERR(0, 89, __pyx_L1_error))
  __Pyx_GetModuleGlobalName(__pyx_t_2, __pyx_n_s_time); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 89, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_time); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 89, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_2 = NULL;
  if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_3))) {
    __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_3);
    if (likely(__pyx_t_2)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
      __Pyx_INCREF(__pyx_t_2);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_3, function);
    }
  }
  __pyx_t_1 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 89, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_v_startledtime = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":90
 * 		cdef char* dataReceived
 * 		startledtime = time.time()
 * 		if waitingForReply == False:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))
 */
  __Pyx_TraceLine(90,0,__PYX_ERR(0, 90, __pyx_L1_error))
  __pyx_t_4 = ((__pyx_v_waitingForReply == 0) != 0);
  if (__pyx_t_4) {

    /* "serialmanager.pyx":91
 * 		startledtime = time.time()
 * 		if waitingForReply == False:
 * 			self.send_to_arduino(result)             # <<<<<<<<<<<<<<
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True
 */
    __Pyx_TraceLine(91,0,__PYX_ERR(0, 91, __pyx_L1_error))
    __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_result); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 91, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

    /* "serialmanager.pyx":92
 * 		if waitingForReply == False:
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))             # <<<<<<<<<<<<<<
 * 			waitingForReply = True
 * 		if waitingForReply == True:
 */
    __Pyx_TraceLine(92,0,__PYX_ERR(0, 92, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_RASPI, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 92, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_result); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 92, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_1 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 92, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 92, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":93
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True             # <<<<<<<<<<<<<<
 * 		if waitingForReply == True:
 * 			while self.ser.inWaiting() == 0:
 */
    __Pyx_TraceLine(93,0,__PYX_ERR(0, 93, __pyx_L1_error))
    __pyx_v_waitingForReply = 1;

    /* "serialmanager.pyx":90
 * 		cdef char* dataReceived
 * 		startledtime = time.time()
 * 		if waitingForReply == False:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))
 */
  }

  /* "serialmanager.pyx":94
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True
 * 		if waitingForReply == True:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(94,0,__PYX_ERR(0, 94, __pyx_L1_error))
  __pyx_t_4 = ((__pyx_v_waitingForReply == 1) != 0);
  if (__pyx_t_4) {

    /* "serialmanager.pyx":95
 * 			waitingForReply = True
 * 		if waitingForReply == True:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(95,0,__PYX_ERR(0, 95, __pyx_L1_error))
    while (1) {
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 95, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __pyx_t_2 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_1))) {
        __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_1);
        if (likely(__pyx_t_2)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_1);
          __Pyx_INCREF(__pyx_t_2);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_1, function);
        }
      }
      __pyx_t_3 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_1, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_1);
      __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 95, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      __pyx_t_1 = __Pyx_PyInt_EqObjC(__pyx_t_3, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 95, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_1); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 95, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      if (!__pyx_t_4) break;
    }

    /* "serialmanager.pyx":97
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply led Received{}'.format(dataReceived))
 * 
 */
    __Pyx_TraceLine(97,0,__PYX_ERR(0, 97, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":98
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply led Received{}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 
 * 	cdef prendi(self):
 */
    __Pyx_TraceLine(98,0,__PYX_ERR(0, 98, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_led_Received, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 98, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 98, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_1 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 98, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 98, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":94
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True
 * 		if waitingForReply == True:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":86
 * 		self.ser.inWaiting()
 * 
 * 	cdef led_controller(self, char* result):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("serialmanager.SerialManager.led_controller", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_startledtime);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":100
 * 			print('Reply led Received{}'.format(dataReceived))
 * 
 * 	cdef prendi(self):             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived
 * 		startledtime = time.time()
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_prendi(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  char *__pyx_v_dataReceived;
  CYTHON_UNUSED PyObject *__pyx_v_startledtime = NULL;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("prendi", 0);
  __Pyx_TraceCall("prendi", __pyx_f[0], 100, 0, __PYX_ERR(0, 100, __pyx_L1_error));

  /* "serialmanager.pyx":102
 * 	cdef prendi(self):
 * 		cdef char* dataReceived
 * 		startledtime = time.time()             # <<<<<<<<<<<<<<
 * 		#while self.ser.inWaiting() == 0:
 * 		#	pass
 */
  __Pyx_TraceLine(102,0,__PYX_ERR(0, 102, __pyx_L1_error))
  __Pyx_GetModuleGlobalName(__pyx_t_2, __pyx_n_s_time); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 102, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_time); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 102, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_2 = NULL;
  if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_3))) {
    __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_3);
    if (likely(__pyx_t_2)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
      __Pyx_INCREF(__pyx_t_2);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_3, function);
    }
  }
  __pyx_t_1 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 102, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_v_startledtime = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":105
 * 		#while self.ser.inWaiting() == 0:
 * 		#	pass
 * 		dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 		print('Reply led Received{}'.format(dataReceived))
 * 
 */
  __Pyx_TraceLine(105,0,__PYX_ERR(0, 105, __pyx_L1_error))
  __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

  /* "serialmanager.pyx":106
 * 		#	pass
 * 		dataReceived = self.receive_from_arduino()
 * 		print('Reply led Received{}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 
 * 	cdef take_front(self):
 */
  __Pyx_TraceLine(106,0,__PYX_ERR(0, 106, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_led_Received, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 106, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 106, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_4 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
    __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
    if (likely(__pyx_t_4)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
      __Pyx_INCREF(__pyx_t_4);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_3, function);
    }
  }
  __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_4, __pyx_t_2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2);
  __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 106, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 106, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

  /* "serialmanager.pyx":100
 * 			print('Reply led Received{}'.format(dataReceived))
 * 
 * 	cdef prendi(self):             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived
 * 		startledtime = time.time()
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_AddTraceback("serialmanager.SerialManager.prendi", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_startledtime);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":108
 * 		print('Reply led Received{}'.format(dataReceived))
 * 
 * 	cdef take_front(self):             # <<<<<<<<<<<<<<
 * 		cdef char* command = '<pp>'
 * 		cdef char* dataReceived = ''
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_take_front(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  char *__pyx_v_command;
  char *__pyx_v_dataReceived;
  int __pyx_v_waiting_for_reply;
  PyObject *__pyx_v_res = NULL;
  double __pyx_v_ans;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  double __pyx_t_6;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("take_front", 0);
  __Pyx_TraceCall("take_front", __pyx_f[0], 108, 0, __PYX_ERR(0, 108, __pyx_L1_error));

  /* "serialmanager.pyx":109
 * 
 * 	cdef take_front(self):
 * 		cdef char* command = '<pp>'             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False
 */
  __Pyx_TraceLine(109,0,__PYX_ERR(0, 109, __pyx_L1_error))
  __pyx_v_command = ((char *)"<pp>");

  /* "serialmanager.pyx":110
 * 	cdef take_front(self):
 * 		cdef char* command = '<pp>'
 * 		cdef char* dataReceived = ''             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		if not waiting_for_reply:
 */
  __Pyx_TraceLine(110,0,__PYX_ERR(0, 110, __pyx_L1_error))
  __pyx_v_dataReceived = ((char *)"");

  /* "serialmanager.pyx":111
 * 		cdef char* command = '<pp>'
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False             # <<<<<<<<<<<<<<
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)
 */
  __Pyx_TraceLine(111,0,__PYX_ERR(0, 111, __pyx_L1_error))
  __pyx_v_waiting_for_reply = 0;

  /* "serialmanager.pyx":112
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} take front'.format(command))
 */
  __Pyx_TraceLine(112,0,__PYX_ERR(0, 112, __pyx_L1_error))
  __pyx_t_1 = ((!(__pyx_v_waiting_for_reply != 0)) != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":113
 * 		cdef bint waiting_for_reply = False
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)             # <<<<<<<<<<<<<<
 * 			print('Sent from Raspi {} take front'.format(command))
 * 			waiting_for_reply = True
 */
    __Pyx_TraceLine(113,0,__PYX_ERR(0, 113, __pyx_L1_error))
    __pyx_t_2 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_command); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 113, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

    /* "serialmanager.pyx":114
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} take front'.format(command))             # <<<<<<<<<<<<<<
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 */
    __Pyx_TraceLine(114,0,__PYX_ERR(0, 114, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi_take_front, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 114, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = __Pyx_PyBytes_FromString(__pyx_v_command); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 114, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 114, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 114, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":115
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} take front'.format(command))
 * 			waiting_for_reply = True             # <<<<<<<<<<<<<<
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:
 */
    __Pyx_TraceLine(115,0,__PYX_ERR(0, 115, __pyx_L1_error))
    __pyx_v_waiting_for_reply = 1;

    /* "serialmanager.pyx":112
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} take front'.format(command))
 */
  }

  /* "serialmanager.pyx":116
 * 			print('Sent from Raspi {} take front'.format(command))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(116,0,__PYX_ERR(0, 116, __pyx_L1_error))
  __pyx_t_1 = (__pyx_v_waiting_for_reply != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":117
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(117,0,__PYX_ERR(0, 117, __pyx_L1_error))
    while (1) {
      __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 117, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __pyx_t_4 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
        __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_2);
        if (likely(__pyx_t_4)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
          __Pyx_INCREF(__pyx_t_4);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_2, function);
        }
      }
      __pyx_t_3 = (__pyx_t_4) ? __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_4) : __Pyx_PyObject_CallNoArg(__pyx_t_2);
      __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 117, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_t_3, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 117, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 117, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      if (!__pyx_t_1) break;
    }

    /* "serialmanager.pyx":119
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply Received take one {}'.format(dataReceived))
 * 			res = dataReceived.split()
 */
    __Pyx_TraceLine(119,0,__PYX_ERR(0, 119, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":120
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received take one {}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 			res = dataReceived.split()
 * 			ans = float(res[3].decode())
 */
    __Pyx_TraceLine(120,0,__PYX_ERR(0, 120, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_Received_take_one, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 120, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 120, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 120, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 120, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":121
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received take one {}'.format(dataReceived))
 * 			res = dataReceived.split()             # <<<<<<<<<<<<<<
 * 			ans = float(res[3].decode())
 * 		return ans
 */
    __Pyx_TraceLine(121,0,__PYX_ERR(0, 121, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 121, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_4 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_split); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 121, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_2 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_4))) {
      __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_4);
      if (likely(__pyx_t_2)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
        __Pyx_INCREF(__pyx_t_2);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_4, function);
      }
    }
    __pyx_t_3 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_4);
    __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 121, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_v_res = __pyx_t_3;
    __pyx_t_3 = 0;

    /* "serialmanager.pyx":122
 * 			print('Reply Received take one {}'.format(dataReceived))
 * 			res = dataReceived.split()
 * 			ans = float(res[3].decode())             # <<<<<<<<<<<<<<
 * 		return ans
 * 
 */
    __Pyx_TraceLine(122,0,__PYX_ERR(0, 122, __pyx_L1_error))
    __pyx_t_4 = __Pyx_GetItemInt(__pyx_v_res, 3, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 122, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_t_4, __pyx_n_s_decode); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 122, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_t_4 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
      __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_2);
      if (likely(__pyx_t_4)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
        __Pyx_INCREF(__pyx_t_4);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_2, function);
      }
    }
    __pyx_t_3 = (__pyx_t_4) ? __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_4) : __Pyx_PyObject_CallNoArg(__pyx_t_2);
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 122, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_6 = __Pyx_PyObject_AsDouble(__pyx_t_3); if (unlikely(__pyx_t_6 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 122, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_v_ans = __pyx_t_6;

    /* "serialmanager.pyx":116
 * 			print('Sent from Raspi {} take front'.format(command))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":123
 * 			res = dataReceived.split()
 * 			ans = float(res[3].decode())
 * 		return ans             # <<<<<<<<<<<<<<
 * 
 * 	cdef daje(self,val):
 */
  __Pyx_TraceLine(123,0,__PYX_ERR(0, 123, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_3 = PyFloat_FromDouble(__pyx_v_ans); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 123, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_r = __pyx_t_3;
  __pyx_t_3 = 0;
  goto __pyx_L0;

  /* "serialmanager.pyx":108
 * 		print('Reply led Received{}'.format(dataReceived))
 * 
 * 	cdef take_front(self):             # <<<<<<<<<<<<<<
 * 		cdef char* command = '<pp>'
 * 		cdef char* dataReceived = ''
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("serialmanager.SerialManager.take_front", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_res);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":125
 * 		return ans
 * 
 * 	cdef daje(self,val):             # <<<<<<<<<<<<<<
 * 		cdef char* command0 = '<switchMod0>'
 * 		cdef char* command1 = '<switchMod1>'
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_daje(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_val) {
  char *__pyx_v_command0;
  char *__pyx_v_command1;
  char *__pyx_v_command2;
  char *__pyx_v_command3;
  char *__pyx_v_dataReceived;
  int __pyx_v_waiting_for_reply;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("daje", 0);
  __Pyx_TraceCall("daje", __pyx_f[0], 125, 0, __PYX_ERR(0, 125, __pyx_L1_error));

  /* "serialmanager.pyx":126
 * 
 * 	cdef daje(self,val):
 * 		cdef char* command0 = '<switchMod0>'             # <<<<<<<<<<<<<<
 * 		cdef char* command1 = '<switchMod1>'
 * 		cdef char* command2 = '<switchMod2>'
 */
  __Pyx_TraceLine(126,0,__PYX_ERR(0, 126, __pyx_L1_error))
  __pyx_v_command0 = ((char *)"<switchMod0>");

  /* "serialmanager.pyx":127
 * 	cdef daje(self,val):
 * 		cdef char* command0 = '<switchMod0>'
 * 		cdef char* command1 = '<switchMod1>'             # <<<<<<<<<<<<<<
 * 		cdef char* command2 = '<switchMod2>'
 * 		cdef char* command3 = '<switchMod3>'
 */
  __Pyx_TraceLine(127,0,__PYX_ERR(0, 127, __pyx_L1_error))
  __pyx_v_command1 = ((char *)"<switchMod1>");

  /* "serialmanager.pyx":128
 * 		cdef char* command0 = '<switchMod0>'
 * 		cdef char* command1 = '<switchMod1>'
 * 		cdef char* command2 = '<switchMod2>'             # <<<<<<<<<<<<<<
 * 		cdef char* command3 = '<switchMod3>'
 * 		cdef char* dataReceived = ''
 */
  __Pyx_TraceLine(128,0,__PYX_ERR(0, 128, __pyx_L1_error))
  __pyx_v_command2 = ((char *)"<switchMod2>");

  /* "serialmanager.pyx":129
 * 		cdef char* command1 = '<switchMod1>'
 * 		cdef char* command2 = '<switchMod2>'
 * 		cdef char* command3 = '<switchMod3>'             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False
 */
  __Pyx_TraceLine(129,0,__PYX_ERR(0, 129, __pyx_L1_error))
  __pyx_v_command3 = ((char *)"<switchMod3>");

  /* "serialmanager.pyx":130
 * 		cdef char* command2 = '<switchMod2>'
 * 		cdef char* command3 = '<switchMod3>'
 * 		cdef char* dataReceived = ''             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		if not waiting_for_reply:
 */
  __Pyx_TraceLine(130,0,__PYX_ERR(0, 130, __pyx_L1_error))
  __pyx_v_dataReceived = ((char *)"");

  /* "serialmanager.pyx":131
 * 		cdef char* command3 = '<switchMod3>'
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False             # <<<<<<<<<<<<<<
 * 		if not waiting_for_reply:
 * 			if val==0:
 */
  __Pyx_TraceLine(131,0,__PYX_ERR(0, 131, __pyx_L1_error))
  __pyx_v_waiting_for_reply = 0;

  /* "serialmanager.pyx":132
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			if val==0:
 * 				self.send_to_arduino(command0)
 */
  __Pyx_TraceLine(132,0,__PYX_ERR(0, 132, __pyx_L1_error))
  __pyx_t_1 = ((!(__pyx_v_waiting_for_reply != 0)) != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":133
 * 		cdef bint waiting_for_reply = False
 * 		if not waiting_for_reply:
 * 			if val==0:             # <<<<<<<<<<<<<<
 * 				self.send_to_arduino(command0)
 * 				print('Sent from Raspi {} '.format(command0))
 */
    __Pyx_TraceLine(133,0,__PYX_ERR(0, 133, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_v_val, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 133, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 133, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":134
 * 		if not waiting_for_reply:
 * 			if val==0:
 * 				self.send_to_arduino(command0)             # <<<<<<<<<<<<<<
 * 				print('Sent from Raspi {} '.format(command0))
 * 			elif val==1:
 */
      __Pyx_TraceLine(134,0,__PYX_ERR(0, 134, __pyx_L1_error))
      __pyx_t_2 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_command0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 134, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

      /* "serialmanager.pyx":135
 * 			if val==0:
 * 				self.send_to_arduino(command0)
 * 				print('Sent from Raspi {} '.format(command0))             # <<<<<<<<<<<<<<
 * 			elif val==1:
 * 				self.send_to_arduino(command1)
 */
      __Pyx_TraceLine(135,0,__PYX_ERR(0, 135, __pyx_L1_error))
      __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 135, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __pyx_t_4 = __Pyx_PyBytes_FromString(__pyx_v_command0); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 135, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_4);
      __pyx_t_5 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
        __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
        if (likely(__pyx_t_5)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
          __Pyx_INCREF(__pyx_t_5);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_3, function);
        }
      }
      __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4);
      __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
      __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
      if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 135, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 135, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

      /* "serialmanager.pyx":133
 * 		cdef bint waiting_for_reply = False
 * 		if not waiting_for_reply:
 * 			if val==0:             # <<<<<<<<<<<<<<
 * 				self.send_to_arduino(command0)
 * 				print('Sent from Raspi {} '.format(command0))
 */
      goto __pyx_L4;
    }

    /* "serialmanager.pyx":136
 * 				self.send_to_arduino(command0)
 * 				print('Sent from Raspi {} '.format(command0))
 * 			elif val==1:             # <<<<<<<<<<<<<<
 * 				self.send_to_arduino(command1)
 * 				print('Sent from Raspi {} '.format(command1))
 */
    __Pyx_TraceLine(136,0,__PYX_ERR(0, 136, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_v_val, __pyx_int_1, 1, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 136, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 136, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":137
 * 				print('Sent from Raspi {} '.format(command0))
 * 			elif val==1:
 * 				self.send_to_arduino(command1)             # <<<<<<<<<<<<<<
 * 				print('Sent from Raspi {} '.format(command1))
 * 			elif val==2:
 */
      __Pyx_TraceLine(137,0,__PYX_ERR(0, 137, __pyx_L1_error))
      __pyx_t_3 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_command1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 137, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

      /* "serialmanager.pyx":138
 * 			elif val==1:
 * 				self.send_to_arduino(command1)
 * 				print('Sent from Raspi {} '.format(command1))             # <<<<<<<<<<<<<<
 * 			elif val==2:
 * 				self.send_to_arduino(command2)
 */
      __Pyx_TraceLine(138,0,__PYX_ERR(0, 138, __pyx_L1_error))
      __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi, __pyx_n_s_format); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 138, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __pyx_t_4 = __Pyx_PyBytes_FromString(__pyx_v_command1); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 138, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_4);
      __pyx_t_5 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
        __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_2);
        if (likely(__pyx_t_5)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
          __Pyx_INCREF(__pyx_t_5);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_2, function);
        }
      }
      __pyx_t_3 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_2, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_4);
      __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
      __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 138, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      __pyx_t_2 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 138, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

      /* "serialmanager.pyx":136
 * 				self.send_to_arduino(command0)
 * 				print('Sent from Raspi {} '.format(command0))
 * 			elif val==1:             # <<<<<<<<<<<<<<
 * 				self.send_to_arduino(command1)
 * 				print('Sent from Raspi {} '.format(command1))
 */
      goto __pyx_L4;
    }

    /* "serialmanager.pyx":139
 * 				self.send_to_arduino(command1)
 * 				print('Sent from Raspi {} '.format(command1))
 * 			elif val==2:             # <<<<<<<<<<<<<<
 * 				self.send_to_arduino(command2)
 * 				print('Sent from Raspi {} '.format(command2))
 */
    __Pyx_TraceLine(139,0,__PYX_ERR(0, 139, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_v_val, __pyx_int_2, 2, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 139, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 139, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":140
 * 				print('Sent from Raspi {} '.format(command1))
 * 			elif val==2:
 * 				self.send_to_arduino(command2)             # <<<<<<<<<<<<<<
 * 				print('Sent from Raspi {} '.format(command2))
 * 			elif val==3:
 */
      __Pyx_TraceLine(140,0,__PYX_ERR(0, 140, __pyx_L1_error))
      __pyx_t_2 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_command2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 140, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

      /* "serialmanager.pyx":141
 * 			elif val==2:
 * 				self.send_to_arduino(command2)
 * 				print('Sent from Raspi {} '.format(command2))             # <<<<<<<<<<<<<<
 * 			elif val==3:
 * 				self.send_to_arduino(command3)
 */
      __Pyx_TraceLine(141,0,__PYX_ERR(0, 141, __pyx_L1_error))
      __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 141, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __pyx_t_4 = __Pyx_PyBytes_FromString(__pyx_v_command2); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 141, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_4);
      __pyx_t_5 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
        __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
        if (likely(__pyx_t_5)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
          __Pyx_INCREF(__pyx_t_5);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_3, function);
        }
      }
      __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4);
      __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
      __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
      if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 141, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 141, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

      /* "serialmanager.pyx":139
 * 				self.send_to_arduino(command1)
 * 				print('Sent from Raspi {} '.format(command1))
 * 			elif val==2:             # <<<<<<<<<<<<<<
 * 				self.send_to_arduino(command2)
 * 				print('Sent from Raspi {} '.format(command2))
 */
      goto __pyx_L4;
    }

    /* "serialmanager.pyx":142
 * 				self.send_to_arduino(command2)
 * 				print('Sent from Raspi {} '.format(command2))
 * 			elif val==3:             # <<<<<<<<<<<<<<
 * 				self.send_to_arduino(command3)
 * 				print('Sent from Raspi {}' .format(command3))
 */
    __Pyx_TraceLine(142,0,__PYX_ERR(0, 142, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_v_val, __pyx_int_3, 3, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 142, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 142, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":143
 * 				print('Sent from Raspi {} '.format(command2))
 * 			elif val==3:
 * 				self.send_to_arduino(command3)             # <<<<<<<<<<<<<<
 * 				print('Sent from Raspi {}' .format(command3))
 * 			waiting_for_reply = True
 */
      __Pyx_TraceLine(143,0,__PYX_ERR(0, 143, __pyx_L1_error))
      __pyx_t_3 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_command3); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 143, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

      /* "serialmanager.pyx":144
 * 			elif val==3:
 * 				self.send_to_arduino(command3)
 * 				print('Sent from Raspi {}' .format(command3))             # <<<<<<<<<<<<<<
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 */
      __Pyx_TraceLine(144,0,__PYX_ERR(0, 144, __pyx_L1_error))
      __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi_2, __pyx_n_s_format); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 144, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __pyx_t_4 = __Pyx_PyBytes_FromString(__pyx_v_command3); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 144, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_4);
      __pyx_t_5 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
        __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_2);
        if (likely(__pyx_t_5)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
          __Pyx_INCREF(__pyx_t_5);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_2, function);
        }
      }
      __pyx_t_3 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_2, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_4);
      __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
      __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 144, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      __pyx_t_2 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 144, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

      /* "serialmanager.pyx":142
 * 				self.send_to_arduino(command2)
 * 				print('Sent from Raspi {} '.format(command2))
 * 			elif val==3:             # <<<<<<<<<<<<<<
 * 				self.send_to_arduino(command3)
 * 				print('Sent from Raspi {}' .format(command3))
 */
    }
    __pyx_L4:;

    /* "serialmanager.pyx":145
 * 				self.send_to_arduino(command3)
 * 				print('Sent from Raspi {}' .format(command3))
 * 			waiting_for_reply = True             # <<<<<<<<<<<<<<
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:
 */
    __Pyx_TraceLine(145,0,__PYX_ERR(0, 145, __pyx_L1_error))
    __pyx_v_waiting_for_reply = 1;

    /* "serialmanager.pyx":132
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			if val==0:
 * 				self.send_to_arduino(command0)
 */
  }

  /* "serialmanager.pyx":146
 * 				print('Sent from Raspi {}' .format(command3))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(146,0,__PYX_ERR(0, 146, __pyx_L1_error))
  __pyx_t_1 = (__pyx_v_waiting_for_reply != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":147
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(147,0,__PYX_ERR(0, 147, __pyx_L1_error))
    while (1) {
      __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 147, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __pyx_t_4 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
        __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
        if (likely(__pyx_t_4)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
          __Pyx_INCREF(__pyx_t_4);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_3, function);
        }
      }
      __pyx_t_2 = (__pyx_t_4) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
      __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
      if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 147, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_t_2, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 147, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 147, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      if (!__pyx_t_1) break;
    }

    /* "serialmanager.pyx":149
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply Received take one {}'.format(dataReceived))
 * 
 */
    __Pyx_TraceLine(149,0,__PYX_ERR(0, 149, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":150
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received take one {}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 
 * 	'''cdef tell_to_move(self, char* command):
 */
    __Pyx_TraceLine(150,0,__PYX_ERR(0, 150, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_Received_take_one, __pyx_n_s_format); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 150, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_4 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 150, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_2);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_2, function);
      }
    }
    __pyx_t_3 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_2, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_4);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 150, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_2 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 150, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

    /* "serialmanager.pyx":146
 * 				print('Sent from Raspi {}' .format(command3))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":125
 * 		return ans
 * 
 * 	cdef daje(self,val):             # <<<<<<<<<<<<<<
 * 		cdef char* command0 = '<switchMod0>'
 * 		cdef char* command1 = '<switchMod1>'
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("serialmanager.SerialManager.daje", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":173
 * 		return'''
 * 
 * 	cdef tell_to_stop(self, char* command):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_tell_to_stop(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, char *__pyx_v_command) {
  int __pyx_v_waiting_for_reply;
  char *__pyx_v_dataReceived;
  int __pyx_v_ra;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  PyObject *__pyx_t_6 = NULL;
  PyObject *__pyx_t_7 = NULL;
  PyObject *__pyx_t_8 = NULL;
  PyObject *__pyx_t_9 = NULL;
  PyObject *__pyx_t_10 = NULL;
  PyObject *__pyx_t_11 = NULL;
  PyObject *__pyx_t_12 = NULL;
  int __pyx_t_13;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("tell_to_stop", 0);
  __Pyx_TraceCall("tell_to_stop", __pyx_f[0], 173, 0, __PYX_ERR(0, 173, __pyx_L1_error));

  /* "serialmanager.pyx":174
 * 
 * 	cdef tell_to_stop(self, char* command):
 * 		cdef bint waiting_for_reply = False             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		cdef int ra =0
 */
  __Pyx_TraceLine(174,0,__PYX_ERR(0, 174, __pyx_L1_error))
  __pyx_v_waiting_for_reply = 0;

  /* "serialmanager.pyx":175
 * 	cdef tell_to_stop(self, char* command):
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''             # <<<<<<<<<<<<<<
 * 		cdef int ra =0
 * 		while ra < 1:
 */
  __Pyx_TraceLine(175,0,__PYX_ERR(0, 175, __pyx_L1_error))
  __pyx_v_dataReceived = ((char *)"");

  /* "serialmanager.pyx":176
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 * 		cdef int ra =0             # <<<<<<<<<<<<<<
 * 		while ra < 1:
 * 			try:
 */
  __Pyx_TraceLine(176,0,__PYX_ERR(0, 176, __pyx_L1_error))
  __pyx_v_ra = 0;

  /* "serialmanager.pyx":177
 * 		cdef char* dataReceived = ''
 * 		cdef int ra =0
 * 		while ra < 1:             # <<<<<<<<<<<<<<
 * 			try:
 * 				if not waiting_for_reply:
 */
  __Pyx_TraceLine(177,0,__PYX_ERR(0, 177, __pyx_L1_error))
  while (1) {
    __pyx_t_1 = ((__pyx_v_ra < 1) != 0);
    if (!__pyx_t_1) break;

    /* "serialmanager.pyx":178
 * 		cdef int ra =0
 * 		while ra < 1:
 * 			try:             # <<<<<<<<<<<<<<
 * 				if not waiting_for_reply:
 * 					self.send_to_arduino(command)
 */
    __Pyx_TraceLine(178,0,__PYX_ERR(0, 178, __pyx_L1_error))
    {
      __Pyx_PyThreadState_declare
      __Pyx_PyThreadState_assign
      __Pyx_ExceptionSave(&__pyx_t_2, &__pyx_t_3, &__pyx_t_4);
      __Pyx_XGOTREF(__pyx_t_2);
      __Pyx_XGOTREF(__pyx_t_3);
      __Pyx_XGOTREF(__pyx_t_4);
      /*try:*/ {

        /* "serialmanager.pyx":179
 * 		while ra < 1:
 * 			try:
 * 				if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 					self.send_to_arduino(command)
 * 					print('Sent from Raspi {} stop!'.format(command))
 */
        __Pyx_TraceLine(179,0,__PYX_ERR(0, 179, __pyx_L5_error))
        __pyx_t_1 = ((!(__pyx_v_waiting_for_reply != 0)) != 0);
        if (__pyx_t_1) {

          /* "serialmanager.pyx":180
 * 			try:
 * 				if not waiting_for_reply:
 * 					self.send_to_arduino(command)             # <<<<<<<<<<<<<<
 * 					print('Sent from Raspi {} stop!'.format(command))
 * 					waiting_for_reply = True
 */
          __Pyx_TraceLine(180,0,__PYX_ERR(0, 180, __pyx_L5_error))
          __pyx_t_5 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_command); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 180, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_5);
          __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;

          /* "serialmanager.pyx":181
 * 				if not waiting_for_reply:
 * 					self.send_to_arduino(command)
 * 					print('Sent from Raspi {} stop!'.format(command))             # <<<<<<<<<<<<<<
 * 					waiting_for_reply = True
 * 				if waiting_for_reply:
 */
          __Pyx_TraceLine(181,0,__PYX_ERR(0, 181, __pyx_L5_error))
          __pyx_t_6 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi_stop, __pyx_n_s_format); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 181, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_6);
          __pyx_t_7 = __Pyx_PyBytes_FromString(__pyx_v_command); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 181, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_7);
          __pyx_t_8 = NULL;
          if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_6))) {
            __pyx_t_8 = PyMethod_GET_SELF(__pyx_t_6);
            if (likely(__pyx_t_8)) {
              PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_6);
              __Pyx_INCREF(__pyx_t_8);
              __Pyx_INCREF(function);
              __Pyx_DECREF_SET(__pyx_t_6, function);
            }
          }
          __pyx_t_5 = (__pyx_t_8) ? __Pyx_PyObject_Call2Args(__pyx_t_6, __pyx_t_8, __pyx_t_7) : __Pyx_PyObject_CallOneArg(__pyx_t_6, __pyx_t_7);
          __Pyx_XDECREF(__pyx_t_8); __pyx_t_8 = 0;
          __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
          if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 181, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_5);
          __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;
          __pyx_t_6 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_5); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 181, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_6);
          __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
          __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;

          /* "serialmanager.pyx":182
 * 					self.send_to_arduino(command)
 * 					print('Sent from Raspi {} stop!'.format(command))
 * 					waiting_for_reply = True             # <<<<<<<<<<<<<<
 * 				if waiting_for_reply:
 * 					if self.ser.inWaiting() > 0:
 */
          __Pyx_TraceLine(182,0,__PYX_ERR(0, 182, __pyx_L5_error))
          __pyx_v_waiting_for_reply = 1;

          /* "serialmanager.pyx":179
 * 		while ra < 1:
 * 			try:
 * 				if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 					self.send_to_arduino(command)
 * 					print('Sent from Raspi {} stop!'.format(command))
 */
        }

        /* "serialmanager.pyx":183
 * 					print('Sent from Raspi {} stop!'.format(command))
 * 					waiting_for_reply = True
 * 				if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 					if self.ser.inWaiting() > 0:
 * 						ra = 1
 */
        __Pyx_TraceLine(183,0,__PYX_ERR(0, 183, __pyx_L5_error))
        __pyx_t_1 = (__pyx_v_waiting_for_reply != 0);
        if (__pyx_t_1) {

          /* "serialmanager.pyx":184
 * 					waiting_for_reply = True
 * 				if waiting_for_reply:
 * 					if self.ser.inWaiting() > 0:             # <<<<<<<<<<<<<<
 * 						ra = 1
 * 					else: continue
 */
          __Pyx_TraceLine(184,0,__PYX_ERR(0, 184, __pyx_L5_error))
          __pyx_t_5 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 184, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_5);
          __pyx_t_7 = NULL;
          if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_5))) {
            __pyx_t_7 = PyMethod_GET_SELF(__pyx_t_5);
            if (likely(__pyx_t_7)) {
              PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_5);
              __Pyx_INCREF(__pyx_t_7);
              __Pyx_INCREF(function);
              __Pyx_DECREF_SET(__pyx_t_5, function);
            }
          }
          __pyx_t_6 = (__pyx_t_7) ? __Pyx_PyObject_CallOneArg(__pyx_t_5, __pyx_t_7) : __Pyx_PyObject_CallNoArg(__pyx_t_5);
          __Pyx_XDECREF(__pyx_t_7); __pyx_t_7 = 0;
          if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 184, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_6);
          __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
          __pyx_t_5 = PyObject_RichCompare(__pyx_t_6, __pyx_int_0, Py_GT); __Pyx_XGOTREF(__pyx_t_5); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 184, __pyx_L5_error)
          __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;
          __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_5); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 184, __pyx_L5_error)
          __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
          if (__pyx_t_1) {

            /* "serialmanager.pyx":185
 * 				if waiting_for_reply:
 * 					if self.ser.inWaiting() > 0:
 * 						ra = 1             # <<<<<<<<<<<<<<
 * 					else: continue
 * 					print(ra)
 */
            __Pyx_TraceLine(185,0,__PYX_ERR(0, 185, __pyx_L5_error))
            __pyx_v_ra = 1;

            /* "serialmanager.pyx":184
 * 					waiting_for_reply = True
 * 				if waiting_for_reply:
 * 					if self.ser.inWaiting() > 0:             # <<<<<<<<<<<<<<
 * 						ra = 1
 * 					else: continue
 */
            goto __pyx_L15;
          }

          /* "serialmanager.pyx":186
 * 					if self.ser.inWaiting() > 0:
 * 						ra = 1
 * 					else: continue             # <<<<<<<<<<<<<<
 * 					print(ra)
 * 					time.sleep(1)
 */
          __Pyx_TraceLine(186,0,__PYX_ERR(0, 186, __pyx_L5_error))
          /*else*/ {
            goto __pyx_L11_try_continue;
          }
          __pyx_L15:;

          /* "serialmanager.pyx":187
 * 						ra = 1
 * 					else: continue
 * 					print(ra)             # <<<<<<<<<<<<<<
 * 					time.sleep(1)
 * 					while dataReceived == b'':
 */
          __Pyx_TraceLine(187,0,__PYX_ERR(0, 187, __pyx_L5_error))
          __pyx_t_5 = __Pyx_PyInt_From_int(__pyx_v_ra); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 187, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_5);
          __pyx_t_6 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_5); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 187, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_6);
          __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
          __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;

          /* "serialmanager.pyx":188
 * 					else: continue
 * 					print(ra)
 * 					time.sleep(1)             # <<<<<<<<<<<<<<
 * 					while dataReceived == b'':
 * 						try:
 */
          __Pyx_TraceLine(188,0,__PYX_ERR(0, 188, __pyx_L5_error))
          __Pyx_GetModuleGlobalName(__pyx_t_5, __pyx_n_s_time); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 188, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_5);
          __pyx_t_7 = __Pyx_PyObject_GetAttrStr(__pyx_t_5, __pyx_n_s_sleep); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 188, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_7);
          __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
          __pyx_t_5 = NULL;
          if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_7))) {
            __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_7);
            if (likely(__pyx_t_5)) {
              PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_7);
              __Pyx_INCREF(__pyx_t_5);
              __Pyx_INCREF(function);
              __Pyx_DECREF_SET(__pyx_t_7, function);
            }
          }
          __pyx_t_6 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_7, __pyx_t_5, __pyx_int_1) : __Pyx_PyObject_CallOneArg(__pyx_t_7, __pyx_int_1);
          __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
          if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 188, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_6);
          __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
          __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;

          /* "serialmanager.pyx":189
 * 					print(ra)
 * 					time.sleep(1)
 * 					while dataReceived == b'':             # <<<<<<<<<<<<<<
 * 						try:
 * 							dataReceived = self.receive_from_arduino()
 */
          __Pyx_TraceLine(189,0,__PYX_ERR(0, 189, __pyx_L5_error))
          while (1) {
            __pyx_t_6 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 189, __pyx_L5_error)
            __Pyx_GOTREF(__pyx_t_6);
            __pyx_t_1 = (__Pyx_PyBytes_Equals(__pyx_t_6, __pyx_kp_b__3, Py_EQ)); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 189, __pyx_L5_error)
            __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;
            if (!__pyx_t_1) break;

            /* "serialmanager.pyx":190
 * 					time.sleep(1)
 * 					while dataReceived == b'':
 * 						try:             # <<<<<<<<<<<<<<
 * 							dataReceived = self.receive_from_arduino()
 * 						except:
 */
            __Pyx_TraceLine(190,0,__PYX_ERR(0, 190, __pyx_L5_error))
            {
              __Pyx_PyThreadState_declare
              __Pyx_PyThreadState_assign
              __Pyx_ExceptionSave(&__pyx_t_9, &__pyx_t_10, &__pyx_t_11);
              __Pyx_XGOTREF(__pyx_t_9);
              __Pyx_XGOTREF(__pyx_t_10);
              __Pyx_XGOTREF(__pyx_t_11);
              /*try:*/ {

                /* "serialmanager.pyx":191
 * 					while dataReceived == b'':
 * 						try:
 * 							dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 						except:
 * 							continue
 */
                __Pyx_TraceLine(191,0,__PYX_ERR(0, 191, __pyx_L18_error))
                __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

                /* "serialmanager.pyx":190
 * 					time.sleep(1)
 * 					while dataReceived == b'':
 * 						try:             # <<<<<<<<<<<<<<
 * 							dataReceived = self.receive_from_arduino()
 * 						except:
 */
              }
              __Pyx_XDECREF(__pyx_t_9); __pyx_t_9 = 0;
              __Pyx_XDECREF(__pyx_t_10); __pyx_t_10 = 0;
              __Pyx_XDECREF(__pyx_t_11); __pyx_t_11 = 0;
              goto __pyx_L25_try_end;
              __pyx_L18_error:;
              __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
              __Pyx_XDECREF(__pyx_t_6); __pyx_t_6 = 0;
              __Pyx_XDECREF(__pyx_t_7); __pyx_t_7 = 0;
              __Pyx_XDECREF(__pyx_t_8); __pyx_t_8 = 0;

              /* "serialmanager.pyx":192
 * 						try:
 * 							dataReceived = self.receive_from_arduino()
 * 						except:             # <<<<<<<<<<<<<<
 * 							continue
 * 					print('Reply Received take one {}'.format(dataReceived))
 */
              __Pyx_TraceLine(192,0,__PYX_ERR(0, 192, __pyx_L20_except_error))
              /*except:*/ {
                __Pyx_AddTraceback("serialmanager.SerialManager.tell_to_stop", __pyx_clineno, __pyx_lineno, __pyx_filename);
                if (__Pyx_GetException(&__pyx_t_6, &__pyx_t_7, &__pyx_t_5) < 0) __PYX_ERR(0, 192, __pyx_L20_except_error)
                __Pyx_GOTREF(__pyx_t_6);
                __Pyx_GOTREF(__pyx_t_7);
                __Pyx_GOTREF(__pyx_t_5);

                /* "serialmanager.pyx":193
 * 							dataReceived = self.receive_from_arduino()
 * 						except:
 * 							continue             # <<<<<<<<<<<<<<
 * 					print('Reply Received take one {}'.format(dataReceived))
 * 					#res = dataReceived.split()
 */
                __Pyx_TraceLine(193,0,__PYX_ERR(0, 193, __pyx_L20_except_error))
                goto __pyx_L27_except_continue;
                __pyx_L27_except_continue:;
                __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;
                __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
                __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
                goto __pyx_L24_try_continue;
              }
              __pyx_L20_except_error:;

              /* "serialmanager.pyx":190
 * 					time.sleep(1)
 * 					while dataReceived == b'':
 * 						try:             # <<<<<<<<<<<<<<
 * 							dataReceived = self.receive_from_arduino()
 * 						except:
 */
              __Pyx_XGIVEREF(__pyx_t_9);
              __Pyx_XGIVEREF(__pyx_t_10);
              __Pyx_XGIVEREF(__pyx_t_11);
              __Pyx_ExceptionReset(__pyx_t_9, __pyx_t_10, __pyx_t_11);
              goto __pyx_L5_error;
              __pyx_L24_try_continue:;
              __Pyx_XGIVEREF(__pyx_t_9);
              __Pyx_XGIVEREF(__pyx_t_10);
              __Pyx_XGIVEREF(__pyx_t_11);
              __Pyx_ExceptionReset(__pyx_t_9, __pyx_t_10, __pyx_t_11);
              goto __pyx_L16_continue;
              __pyx_L25_try_end:;
            }
            __pyx_L16_continue:;
          }

          /* "serialmanager.pyx":194
 * 						except:
 * 							continue
 * 					print('Reply Received take one {}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 					#res = dataReceived.split()
 * 					#ans = float(res[3].decode())
 */
          __Pyx_TraceLine(194,0,__PYX_ERR(0, 194, __pyx_L5_error))
          __pyx_t_7 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_Received_take_one, __pyx_n_s_format); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 194, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_7);
          __pyx_t_6 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 194, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_6);
          __pyx_t_8 = NULL;
          if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_7))) {
            __pyx_t_8 = PyMethod_GET_SELF(__pyx_t_7);
            if (likely(__pyx_t_8)) {
              PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_7);
              __Pyx_INCREF(__pyx_t_8);
              __Pyx_INCREF(function);
              __Pyx_DECREF_SET(__pyx_t_7, function);
            }
          }
          __pyx_t_5 = (__pyx_t_8) ? __Pyx_PyObject_Call2Args(__pyx_t_7, __pyx_t_8, __pyx_t_6) : __Pyx_PyObject_CallOneArg(__pyx_t_7, __pyx_t_6);
          __Pyx_XDECREF(__pyx_t_8); __pyx_t_8 = 0;
          __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;
          if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 194, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_5);
          __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
          __pyx_t_7 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_5); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 194, __pyx_L5_error)
          __Pyx_GOTREF(__pyx_t_7);
          __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
          __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;

          /* "serialmanager.pyx":183
 * 					print('Sent from Raspi {} stop!'.format(command))
 * 					waiting_for_reply = True
 * 				if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 					if self.ser.inWaiting() > 0:
 * 						ra = 1
 */
        }

        /* "serialmanager.pyx":178
 * 		cdef int ra =0
 * 		while ra < 1:
 * 			try:             # <<<<<<<<<<<<<<
 * 				if not waiting_for_reply:
 * 					self.send_to_arduino(command)
 */
      }
      __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
      __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
      __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
      goto __pyx_L12_try_end;
      __pyx_L5_error:;
      __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
      __Pyx_XDECREF(__pyx_t_6); __pyx_t_6 = 0;
      __Pyx_XDECREF(__pyx_t_7); __pyx_t_7 = 0;
      __Pyx_XDECREF(__pyx_t_8); __pyx_t_8 = 0;

      /* "serialmanager.pyx":197
 * 					#res = dataReceived.split()
 * 					#ans = float(res[3].decode())
 * 			except serial.serialutil.SerialException:             # <<<<<<<<<<<<<<
 * 				continue
 * 			break
 */
      __Pyx_TraceLine(197,0,__PYX_ERR(0, 197, __pyx_L7_except_error))
      __Pyx_ErrFetch(&__pyx_t_7, &__pyx_t_5, &__pyx_t_6);
      __Pyx_GetModuleGlobalName(__pyx_t_8, __pyx_n_s_serial); if (unlikely(!__pyx_t_8)) __PYX_ERR(0, 197, __pyx_L7_except_error)
      __Pyx_GOTREF(__pyx_t_8);
      __pyx_t_12 = __Pyx_PyObject_GetAttrStr(__pyx_t_8, __pyx_n_s_serialutil); if (unlikely(!__pyx_t_12)) __PYX_ERR(0, 197, __pyx_L7_except_error)
      __Pyx_GOTREF(__pyx_t_12);
      __Pyx_DECREF(__pyx_t_8); __pyx_t_8 = 0;
      __pyx_t_8 = __Pyx_PyObject_GetAttrStr(__pyx_t_12, __pyx_n_s_SerialException); if (unlikely(!__pyx_t_8)) __PYX_ERR(0, 197, __pyx_L7_except_error)
      __Pyx_GOTREF(__pyx_t_8);
      __Pyx_DECREF(__pyx_t_12); __pyx_t_12 = 0;
      __pyx_t_13 = __Pyx_PyErr_GivenExceptionMatches(__pyx_t_7, __pyx_t_8);
      __Pyx_DECREF(__pyx_t_8); __pyx_t_8 = 0;
      __Pyx_ErrRestore(__pyx_t_7, __pyx_t_5, __pyx_t_6);
      __pyx_t_7 = 0; __pyx_t_5 = 0; __pyx_t_6 = 0;
      if (__pyx_t_13) {
        __Pyx_AddTraceback("serialmanager.SerialManager.tell_to_stop", __pyx_clineno, __pyx_lineno, __pyx_filename);
        if (__Pyx_GetException(&__pyx_t_6, &__pyx_t_5, &__pyx_t_7) < 0) __PYX_ERR(0, 197, __pyx_L7_except_error)
        __Pyx_GOTREF(__pyx_t_6);
        __Pyx_GOTREF(__pyx_t_5);
        __Pyx_GOTREF(__pyx_t_7);

        /* "serialmanager.pyx":198
 * 					#ans = float(res[3].decode())
 * 			except serial.serialutil.SerialException:
 * 				continue             # <<<<<<<<<<<<<<
 * 			break
 * 		return 0
 */
        __Pyx_TraceLine(198,0,__PYX_ERR(0, 198, __pyx_L7_except_error))
        goto __pyx_L29_except_continue;
        __pyx_L29_except_continue:;
        __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;
        __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
        __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
        goto __pyx_L11_try_continue;
      }
      goto __pyx_L7_except_error;
      __pyx_L7_except_error:;

      /* "serialmanager.pyx":178
 * 		cdef int ra =0
 * 		while ra < 1:
 * 			try:             # <<<<<<<<<<<<<<
 * 				if not waiting_for_reply:
 * 					self.send_to_arduino(command)
 */
      __Pyx_XGIVEREF(__pyx_t_2);
      __Pyx_XGIVEREF(__pyx_t_3);
      __Pyx_XGIVEREF(__pyx_t_4);
      __Pyx_ExceptionReset(__pyx_t_2, __pyx_t_3, __pyx_t_4);
      goto __pyx_L1_error;
      __pyx_L11_try_continue:;
      __Pyx_XGIVEREF(__pyx_t_2);
      __Pyx_XGIVEREF(__pyx_t_3);
      __Pyx_XGIVEREF(__pyx_t_4);
      __Pyx_ExceptionReset(__pyx_t_2, __pyx_t_3, __pyx_t_4);
      goto __pyx_L3_continue;
      __pyx_L12_try_end:;
    }

    /* "serialmanager.pyx":199
 * 			except serial.serialutil.SerialException:
 * 				continue
 * 			break             # <<<<<<<<<<<<<<
 * 		return 0
 * 
 */
    __Pyx_TraceLine(199,0,__PYX_ERR(0, 199, __pyx_L1_error))
    goto __pyx_L4_break;
    __pyx_L3_continue:;
  }
  __pyx_L4_break:;

  /* "serialmanager.pyx":200
 * 				continue
 * 			break
 * 		return 0             # <<<<<<<<<<<<<<
 * 
 * 	cdef tell_to_move3(self, command):
 */
  __Pyx_TraceLine(200,0,__PYX_ERR(0, 200, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_int_0);
  __pyx_r = __pyx_int_0;
  goto __pyx_L0;

  /* "serialmanager.pyx":173
 * 		return'''
 * 
 * 	cdef tell_to_stop(self, char* command):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_XDECREF(__pyx_t_6);
  __Pyx_XDECREF(__pyx_t_7);
  __Pyx_XDECREF(__pyx_t_8);
  __Pyx_XDECREF(__pyx_t_12);
  __Pyx_AddTraceback("serialmanager.SerialManager.tell_to_stop", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":202
 * 		return 0
 * 
 * 	cdef tell_to_move3(self, command):             # <<<<<<<<<<<<<<
 * 		#cdef char* command = '<elaps>'
 * 		cdef bint waiting_for_reply = False
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_tell_to_move3(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_command) {
  int __pyx_v_waiting_for_reply;
  char *__pyx_v_dataReceived;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  char *__pyx_t_2;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  PyObject *__pyx_t_6 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("tell_to_move3", 0);
  __Pyx_TraceCall("tell_to_move3", __pyx_f[0], 202, 0, __PYX_ERR(0, 202, __pyx_L1_error));

  /* "serialmanager.pyx":204
 * 	cdef tell_to_move3(self, command):
 * 		#cdef char* command = '<elaps>'
 * 		cdef bint waiting_for_reply = False             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:
 */
  __Pyx_TraceLine(204,0,__PYX_ERR(0, 204, __pyx_L1_error))
  __pyx_v_waiting_for_reply = 0;

  /* "serialmanager.pyx":205
 * 		#cdef char* command = '<elaps>'
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''             # <<<<<<<<<<<<<<
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)
 */
  __Pyx_TraceLine(205,0,__PYX_ERR(0, 205, __pyx_L1_error))
  __pyx_v_dataReceived = ((char *)"");

  /* "serialmanager.pyx":206
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 */
  __Pyx_TraceLine(206,0,__PYX_ERR(0, 206, __pyx_L1_error))
  __pyx_t_1 = ((!(__pyx_v_waiting_for_reply != 0)) != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":207
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)             # <<<<<<<<<<<<<<
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True
 */
    __Pyx_TraceLine(207,0,__PYX_ERR(0, 207, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyObject_AsWritableString(__pyx_v_command); if (unlikely((!__pyx_t_2) && PyErr_Occurred())) __PYX_ERR(0, 207, __pyx_L1_error)
    __pyx_t_3 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 207, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":208
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))             # <<<<<<<<<<<<<<
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 */
    __Pyx_TraceLine(208,0,__PYX_ERR(0, 208, __pyx_L1_error))
    __pyx_t_4 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi_TEST_PRESS, __pyx_n_s_format); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 208, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_4))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_4);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_4, function);
      }
    }
    __pyx_t_3 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_5, __pyx_v_command) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_v_command);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 208, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_t_4 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 208, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;

    /* "serialmanager.pyx":209
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True             # <<<<<<<<<<<<<<
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:
 */
    __Pyx_TraceLine(209,0,__PYX_ERR(0, 209, __pyx_L1_error))
    __pyx_v_waiting_for_reply = 1;

    /* "serialmanager.pyx":206
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 */
  }

  /* "serialmanager.pyx":210
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(210,0,__PYX_ERR(0, 210, __pyx_L1_error))
  __pyx_t_1 = (__pyx_v_waiting_for_reply != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":211
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(211,0,__PYX_ERR(0, 211, __pyx_L1_error))
    while (1) {
      __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 211, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __pyx_t_5 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
        __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
        if (likely(__pyx_t_5)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
          __Pyx_INCREF(__pyx_t_5);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_3, function);
        }
      }
      __pyx_t_4 = (__pyx_t_5) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_5) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
      __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
      if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 211, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_4);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_t_4, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 211, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
      __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 211, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      if (!__pyx_t_1) break;
    }

    /* "serialmanager.pyx":213
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply Received happiness {}'.format(dataReceived))
 * 			#res = dataReceived.split()
 */
    __Pyx_TraceLine(213,0,__PYX_ERR(0, 213, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":214
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received happiness {}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 			#res = dataReceived.split()
 * 			#ans1 = float(res[3].decode())
 */
    __Pyx_TraceLine(214,0,__PYX_ERR(0, 214, __pyx_L1_error))
    __pyx_t_4 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_Received_happiness, __pyx_n_s_format); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 214, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 214, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __pyx_t_6 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_4))) {
      __pyx_t_6 = PyMethod_GET_SELF(__pyx_t_4);
      if (likely(__pyx_t_6)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
        __Pyx_INCREF(__pyx_t_6);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_4, function);
      }
    }
    __pyx_t_3 = (__pyx_t_6) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_6, __pyx_t_5) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_t_5);
    __Pyx_XDECREF(__pyx_t_6); __pyx_t_6 = 0;
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 214, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_t_4 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 214, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;

    /* "serialmanager.pyx":210
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":220
 * 			#ans3 = res[10].decode()
 * 			#ans4 = res[13].decode()
 * 		return #ans1,ans2,ans3,ans4             # <<<<<<<<<<<<<<
 * 
 * 	cdef movement_command(self, command):
 */
  __Pyx_TraceLine(220,0,__PYX_ERR(0, 220, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;

  /* "serialmanager.pyx":202
 * 		return 0
 * 
 * 	cdef tell_to_move3(self, command):             # <<<<<<<<<<<<<<
 * 		#cdef char* command = '<elaps>'
 * 		cdef bint waiting_for_reply = False
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_XDECREF(__pyx_t_6);
  __Pyx_AddTraceback("serialmanager.SerialManager.tell_to_move3", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":222
 * 		return #ans1,ans2,ans3,ans4
 * 
 * 	cdef movement_command(self, command):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_movement_command(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_command) {
  int __pyx_v_waiting_for_reply;
  char *__pyx_v_dataReceived;
  PyObject *__pyx_v_res = NULL;
  double __pyx_v_strafe;
  double __pyx_v_forward;
  double __pyx_v_angular;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  char *__pyx_t_2;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  PyObject *__pyx_t_6 = NULL;
  double __pyx_t_7;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("movement_command", 0);
  __Pyx_TraceCall("movement_command", __pyx_f[0], 222, 0, __PYX_ERR(0, 222, __pyx_L1_error));

  /* "serialmanager.pyx":223
 * 
 * 	cdef movement_command(self, command):
 * 		cdef bint waiting_for_reply = False             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:
 */
  __Pyx_TraceLine(223,0,__PYX_ERR(0, 223, __pyx_L1_error))
  __pyx_v_waiting_for_reply = 0;

  /* "serialmanager.pyx":224
 * 	cdef movement_command(self, command):
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''             # <<<<<<<<<<<<<<
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)
 */
  __Pyx_TraceLine(224,0,__PYX_ERR(0, 224, __pyx_L1_error))
  __pyx_v_dataReceived = ((char *)"");

  /* "serialmanager.pyx":225
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 */
  __Pyx_TraceLine(225,0,__PYX_ERR(0, 225, __pyx_L1_error))
  __pyx_t_1 = ((!(__pyx_v_waiting_for_reply != 0)) != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":226
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)             # <<<<<<<<<<<<<<
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True
 */
    __Pyx_TraceLine(226,0,__PYX_ERR(0, 226, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyObject_AsWritableString(__pyx_v_command); if (unlikely((!__pyx_t_2) && PyErr_Occurred())) __PYX_ERR(0, 226, __pyx_L1_error)
    __pyx_t_3 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 226, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":227
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))             # <<<<<<<<<<<<<<
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 */
    __Pyx_TraceLine(227,0,__PYX_ERR(0, 227, __pyx_L1_error))
    __pyx_t_4 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi_TEST_PRESS, __pyx_n_s_format); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 227, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_4))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_4);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_4, function);
      }
    }
    __pyx_t_3 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_5, __pyx_v_command) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_v_command);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 227, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_t_4 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 227, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;

    /* "serialmanager.pyx":228
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True             # <<<<<<<<<<<<<<
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:
 */
    __Pyx_TraceLine(228,0,__PYX_ERR(0, 228, __pyx_L1_error))
    __pyx_v_waiting_for_reply = 1;

    /* "serialmanager.pyx":225
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 */
  }

  /* "serialmanager.pyx":229
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(229,0,__PYX_ERR(0, 229, __pyx_L1_error))
  __pyx_t_1 = (__pyx_v_waiting_for_reply != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":230
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(230,0,__PYX_ERR(0, 230, __pyx_L1_error))
    while (1) {
      __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 230, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __pyx_t_5 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
        __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
        if (likely(__pyx_t_5)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
          __Pyx_INCREF(__pyx_t_5);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_3, function);
        }
      }
      __pyx_t_4 = (__pyx_t_5) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_5) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
      __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
      if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 230, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_4);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_t_4, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 230, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
      __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 230, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      if (!__pyx_t_1) break;
    }

    /* "serialmanager.pyx":232
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply Received take one {}'.format(dataReceived))
 * 			res = dataReceived.split()
 */
    __Pyx_TraceLine(232,0,__PYX_ERR(0, 232, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":233
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received take one {}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 			res = dataReceived.split()
 * 			strafe = float(res[3].decode())
 */
    __Pyx_TraceLine(233,0,__PYX_ERR(0, 233, __pyx_L1_error))
    __pyx_t_4 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_Received_take_one, __pyx_n_s_format); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 233, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 233, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __pyx_t_6 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_4))) {
      __pyx_t_6 = PyMethod_GET_SELF(__pyx_t_4);
      if (likely(__pyx_t_6)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
        __Pyx_INCREF(__pyx_t_6);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_4, function);
      }
    }
    __pyx_t_3 = (__pyx_t_6) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_6, __pyx_t_5) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_t_5);
    __Pyx_XDECREF(__pyx_t_6); __pyx_t_6 = 0;
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 233, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_t_4 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 233, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;

    /* "serialmanager.pyx":234
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received take one {}'.format(dataReceived))
 * 			res = dataReceived.split()             # <<<<<<<<<<<<<<
 * 			strafe = float(res[3].decode())
 * 			forward = float(res[7].decode())
 */
    __Pyx_TraceLine(234,0,__PYX_ERR(0, 234, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 234, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_5 = __Pyx_PyObject_GetAttrStr(__pyx_t_3, __pyx_n_s_split); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 234, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_5))) {
      __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_5);
      if (likely(__pyx_t_3)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_5);
        __Pyx_INCREF(__pyx_t_3);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_5, function);
      }
    }
    __pyx_t_4 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_5);
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 234, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_v_res = __pyx_t_4;
    __pyx_t_4 = 0;

    /* "serialmanager.pyx":235
 * 			print('Reply Received take one {}'.format(dataReceived))
 * 			res = dataReceived.split()
 * 			strafe = float(res[3].decode())             # <<<<<<<<<<<<<<
 * 			forward = float(res[7].decode())
 * 			angular = float(res[11].decode())
 */
    __Pyx_TraceLine(235,0,__PYX_ERR(0, 235, __pyx_L1_error))
    __pyx_t_5 = __Pyx_GetItemInt(__pyx_v_res, 3, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 235, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_5, __pyx_n_s_decode); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 235, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_4 = (__pyx_t_5) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_5) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 235, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_7 = __Pyx_PyObject_AsDouble(__pyx_t_4); if (unlikely(__pyx_t_7 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 235, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_v_strafe = __pyx_t_7;

    /* "serialmanager.pyx":236
 * 			res = dataReceived.split()
 * 			strafe = float(res[3].decode())
 * 			forward = float(res[7].decode())             # <<<<<<<<<<<<<<
 * 			angular = float(res[11].decode())
 * 		return strafe,forward,angular
 */
    __Pyx_TraceLine(236,0,__PYX_ERR(0, 236, __pyx_L1_error))
    __pyx_t_3 = __Pyx_GetItemInt(__pyx_v_res, 7, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 236, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_5 = __Pyx_PyObject_GetAttrStr(__pyx_t_3, __pyx_n_s_decode); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 236, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_5))) {
      __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_5);
      if (likely(__pyx_t_3)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_5);
        __Pyx_INCREF(__pyx_t_3);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_5, function);
      }
    }
    __pyx_t_4 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_5);
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 236, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_t_7 = __Pyx_PyObject_AsDouble(__pyx_t_4); if (unlikely(__pyx_t_7 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 236, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_v_forward = __pyx_t_7;

    /* "serialmanager.pyx":237
 * 			strafe = float(res[3].decode())
 * 			forward = float(res[7].decode())
 * 			angular = float(res[11].decode())             # <<<<<<<<<<<<<<
 * 		return strafe,forward,angular
 * 
 */
    __Pyx_TraceLine(237,0,__PYX_ERR(0, 237, __pyx_L1_error))
    __pyx_t_5 = __Pyx_GetItemInt(__pyx_v_res, 11, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 237, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_5, __pyx_n_s_decode); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 237, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_4 = (__pyx_t_5) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_5) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 237, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_7 = __Pyx_PyObject_AsDouble(__pyx_t_4); if (unlikely(__pyx_t_7 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 237, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_v_angular = __pyx_t_7;

    /* "serialmanager.pyx":229
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":238
 * 			forward = float(res[7].decode())
 * 			angular = float(res[11].decode())
 * 		return strafe,forward,angular             # <<<<<<<<<<<<<<
 * 
 * 	def waitard(self):
 */
  __Pyx_TraceLine(238,0,__PYX_ERR(0, 238, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_4 = PyFloat_FromDouble(__pyx_v_strafe); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 238, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_4);
  __pyx_t_3 = PyFloat_FromDouble(__pyx_v_forward); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 238, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_5 = PyFloat_FromDouble(__pyx_v_angular); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 238, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_5);
  __pyx_t_6 = PyTuple_New(3); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 238, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_6);
  __Pyx_GIVEREF(__pyx_t_4);
  PyTuple_SET_ITEM(__pyx_t_6, 0, __pyx_t_4);
  __Pyx_GIVEREF(__pyx_t_3);
  PyTuple_SET_ITEM(__pyx_t_6, 1, __pyx_t_3);
  __Pyx_GIVEREF(__pyx_t_5);
  PyTuple_SET_ITEM(__pyx_t_6, 2, __pyx_t_5);
  __pyx_t_4 = 0;
  __pyx_t_3 = 0;
  __pyx_t_5 = 0;
  __pyx_r = __pyx_t_6;
  __pyx_t_6 = 0;
  goto __pyx_L0;

  /* "serialmanager.pyx":222
 * 		return #ans1,ans2,ans3,ans4
 * 
 * 	cdef movement_command(self, command):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_XDECREF(__pyx_t_6);
  __Pyx_AddTraceback("serialmanager.SerialManager.movement_command", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_res);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":240
 * 		return strafe,forward,angular
 * 
 * 	def waitard(self):             # <<<<<<<<<<<<<<
 * 		self.waiting_arduino()
 * 	def waitard_nano(self):
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_7waitard(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_6waitard[] = "SerialManager.waitard(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_7waitard(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("waitard (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_6waitard(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_6waitard(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("waitard", 0);
  __Pyx_TraceCall("waitard", __pyx_f[0], 240, 0, __PYX_ERR(0, 240, __pyx_L1_error));

  /* "serialmanager.pyx":241
 * 
 * 	def waitard(self):
 * 		self.waiting_arduino()             # <<<<<<<<<<<<<<
 * 	def waitard_nano(self):
 * 		self.waiting_arduino_nano()
 */
  __Pyx_TraceLine(241,0,__PYX_ERR(0, 241, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->waiting_arduino(__pyx_v_self); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 241, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":240
 * 		return strafe,forward,angular
 * 
 * 	def waitard(self):             # <<<<<<<<<<<<<<
 * 		self.waiting_arduino()
 * 	def waitard_nano(self):
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.waitard", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":242
 * 	def waitard(self):
 * 		self.waiting_arduino()
 * 	def waitard_nano(self):             # <<<<<<<<<<<<<<
 * 		self.waiting_arduino_nano()
 * 	def waitard_nano2(self):
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_9waitard_nano(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_8waitard_nano[] = "SerialManager.waitard_nano(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_9waitard_nano(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("waitard_nano (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_8waitard_nano(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_8waitard_nano(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("waitard_nano", 0);
  __Pyx_TraceCall("waitard_nano", __pyx_f[0], 242, 0, __PYX_ERR(0, 242, __pyx_L1_error));

  /* "serialmanager.pyx":243
 * 		self.waiting_arduino()
 * 	def waitard_nano(self):
 * 		self.waiting_arduino_nano()             # <<<<<<<<<<<<<<
 * 	def waitard_nano2(self):
 * 		self.waiting_arduino_nano2()
 */
  __Pyx_TraceLine(243,0,__PYX_ERR(0, 243, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->waiting_arduino_nano(__pyx_v_self); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 243, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":242
 * 	def waitard(self):
 * 		self.waiting_arduino()
 * 	def waitard_nano(self):             # <<<<<<<<<<<<<<
 * 		self.waiting_arduino_nano()
 * 	def waitard_nano2(self):
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.waitard_nano", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":244
 * 	def waitard_nano(self):
 * 		self.waiting_arduino_nano()
 * 	def waitard_nano2(self):             # <<<<<<<<<<<<<<
 * 		self.waiting_arduino_nano2()
 * 	def waitard_mega(self):
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_11waitard_nano2(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_10waitard_nano2[] = "SerialManager.waitard_nano2(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_11waitard_nano2(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("waitard_nano2 (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_10waitard_nano2(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_10waitard_nano2(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("waitard_nano2", 0);
  __Pyx_TraceCall("waitard_nano2", __pyx_f[0], 244, 0, __PYX_ERR(0, 244, __pyx_L1_error));

  /* "serialmanager.pyx":245
 * 		self.waiting_arduino_nano()
 * 	def waitard_nano2(self):
 * 		self.waiting_arduino_nano2()             # <<<<<<<<<<<<<<
 * 	def waitard_mega(self):
 * 		self.waiting_arduino_mega()
 */
  __Pyx_TraceLine(245,0,__PYX_ERR(0, 245, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->waiting_arduino_nano2(__pyx_v_self); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 245, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":244
 * 	def waitard_nano(self):
 * 		self.waiting_arduino_nano()
 * 	def waitard_nano2(self):             # <<<<<<<<<<<<<<
 * 		self.waiting_arduino_nano2()
 * 	def waitard_mega(self):
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.waitard_nano2", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":246
 * 	def waitard_nano2(self):
 * 		self.waiting_arduino_nano2()
 * 	def waitard_mega(self):             # <<<<<<<<<<<<<<
 * 		self.waiting_arduino_mega()
 * 	def led_co(self,result):
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_13waitard_mega(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_12waitard_mega[] = "SerialManager.waitard_mega(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_13waitard_mega(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("waitard_mega (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_12waitard_mega(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_12waitard_mega(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("waitard_mega", 0);
  __Pyx_TraceCall("waitard_mega", __pyx_f[0], 246, 0, __PYX_ERR(0, 246, __pyx_L1_error));

  /* "serialmanager.pyx":247
 * 		self.waiting_arduino_nano2()
 * 	def waitard_mega(self):
 * 		self.waiting_arduino_mega()             # <<<<<<<<<<<<<<
 * 	def led_co(self,result):
 * 		cdef char* res = result
 */
  __Pyx_TraceLine(247,0,__PYX_ERR(0, 247, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->waiting_arduino_mega(__pyx_v_self); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 247, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":246
 * 	def waitard_nano2(self):
 * 		self.waiting_arduino_nano2()
 * 	def waitard_mega(self):             # <<<<<<<<<<<<<<
 * 		self.waiting_arduino_mega()
 * 	def led_co(self,result):
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.waitard_mega", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":248
 * 	def waitard_mega(self):
 * 		self.waiting_arduino_mega()
 * 	def led_co(self,result):             # <<<<<<<<<<<<<<
 * 		cdef char* res = result
 * 		self.led_controller(res)
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_15led_co(PyObject *__pyx_v_self, PyObject *__pyx_v_result); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_14led_co[] = "SerialManager.led_co(self, result)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_15led_co(PyObject *__pyx_v_self, PyObject *__pyx_v_result) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("led_co (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_14led_co(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), ((PyObject *)__pyx_v_result));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_14led_co(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_result) {
  char *__pyx_v_res;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  char *__pyx_t_1;
  PyObject *__pyx_t_2 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("led_co", 0);
  __Pyx_TraceCall("led_co", __pyx_f[0], 248, 0, __PYX_ERR(0, 248, __pyx_L1_error));

  /* "serialmanager.pyx":249
 * 		self.waiting_arduino_mega()
 * 	def led_co(self,result):
 * 		cdef char* res = result             # <<<<<<<<<<<<<<
 * 		self.led_controller(res)
 * 	def takempx(self):
 */
  __Pyx_TraceLine(249,0,__PYX_ERR(0, 249, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_AsWritableString(__pyx_v_result); if (unlikely((!__pyx_t_1) && PyErr_Occurred())) __PYX_ERR(0, 249, __pyx_L1_error)
  __pyx_v_res = __pyx_t_1;

  /* "serialmanager.pyx":250
 * 	def led_co(self,result):
 * 		cdef char* res = result
 * 		self.led_controller(res)             # <<<<<<<<<<<<<<
 * 	def takempx(self):
 * 		ans = self.take_front()
 */
  __Pyx_TraceLine(250,0,__PYX_ERR(0, 250, __pyx_L1_error))
  __pyx_t_2 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->led_controller(__pyx_v_self, __pyx_v_res); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 250, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

  /* "serialmanager.pyx":248
 * 	def waitard_mega(self):
 * 		self.waiting_arduino_mega()
 * 	def led_co(self,result):             # <<<<<<<<<<<<<<
 * 		cdef char* res = result
 * 		self.led_controller(res)
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_AddTraceback("serialmanager.SerialManager.led_co", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":251
 * 		cdef char* res = result
 * 		self.led_controller(res)
 * 	def takempx(self):             # <<<<<<<<<<<<<<
 * 		ans = self.take_front()
 * 		print('ansel is{}'.format(ans))
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_17takempx(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_16takempx[] = "SerialManager.takempx(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_17takempx(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("takempx (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_16takempx(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_16takempx(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_v_ans = NULL;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("takempx", 0);
  __Pyx_TraceCall("takempx", __pyx_f[0], 251, 0, __PYX_ERR(0, 251, __pyx_L1_error));

  /* "serialmanager.pyx":252
 * 		self.led_controller(res)
 * 	def takempx(self):
 * 		ans = self.take_front()             # <<<<<<<<<<<<<<
 * 		print('ansel is{}'.format(ans))
 * 		return ans
 */
  __Pyx_TraceLine(252,0,__PYX_ERR(0, 252, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->take_front(__pyx_v_self); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 252, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_v_ans = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":253
 * 	def takempx(self):
 * 		ans = self.take_front()
 * 		print('ansel is{}'.format(ans))             # <<<<<<<<<<<<<<
 * 		return ans
 * 	def dajetutta(self,val):
 */
  __Pyx_TraceLine(253,0,__PYX_ERR(0, 253, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_ansel_is, __pyx_n_s_format); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 253, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
    __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_2);
    if (likely(__pyx_t_3)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
      __Pyx_INCREF(__pyx_t_3);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_2, function);
    }
  }
  __pyx_t_1 = (__pyx_t_3) ? __Pyx_PyObject_Call2Args(__pyx_t_2, __pyx_t_3, __pyx_v_ans) : __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_v_ans);
  __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 253, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_2 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 253, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

  /* "serialmanager.pyx":254
 * 		ans = self.take_front()
 * 		print('ansel is{}'.format(ans))
 * 		return ans             # <<<<<<<<<<<<<<
 * 	def dajetutta(self,val):
 * 		self.daje(val)
 */
  __Pyx_TraceLine(254,0,__PYX_ERR(0, 254, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_ans);
  __pyx_r = __pyx_v_ans;
  goto __pyx_L0;

  /* "serialmanager.pyx":251
 * 		cdef char* res = result
 * 		self.led_controller(res)
 * 	def takempx(self):             # <<<<<<<<<<<<<<
 * 		ans = self.take_front()
 * 		print('ansel is{}'.format(ans))
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_AddTraceback("serialmanager.SerialManager.takempx", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_ans);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":255
 * 		print('ansel is{}'.format(ans))
 * 		return ans
 * 	def dajetutta(self,val):             # <<<<<<<<<<<<<<
 * 		self.daje(val)
 * 
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_19dajetutta(PyObject *__pyx_v_self, PyObject *__pyx_v_val); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_18dajetutta[] = "SerialManager.dajetutta(self, val)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_19dajetutta(PyObject *__pyx_v_self, PyObject *__pyx_v_val) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("dajetutta (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_18dajetutta(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), ((PyObject *)__pyx_v_val));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_18dajetutta(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_val) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("dajetutta", 0);
  __Pyx_TraceCall("dajetutta", __pyx_f[0], 255, 0, __PYX_ERR(0, 255, __pyx_L1_error));

  /* "serialmanager.pyx":256
 * 		return ans
 * 	def dajetutta(self,val):
 * 		self.daje(val)             # <<<<<<<<<<<<<<
 * 
 * 	def move_order(self,move_comm):
 */
  __Pyx_TraceLine(256,0,__PYX_ERR(0, 256, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->daje(__pyx_v_self, __pyx_v_val); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 256, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":255
 * 		print('ansel is{}'.format(ans))
 * 		return ans
 * 	def dajetutta(self,val):             # <<<<<<<<<<<<<<
 * 		self.daje(val)
 * 
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.dajetutta", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":258
 * 		self.daje(val)
 * 
 * 	def move_order(self,move_comm):             # <<<<<<<<<<<<<<
 * 		cdef str command_mov = move_comm
 * 		cdef char* command_feeling
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_21move_order(PyObject *__pyx_v_self, PyObject *__pyx_v_move_comm); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_20move_order[] = "SerialManager.move_order(self, move_comm)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_21move_order(PyObject *__pyx_v_self, PyObject *__pyx_v_move_comm) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("move_order (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_20move_order(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), ((PyObject *)__pyx_v_move_comm));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_20move_order(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_move_comm) {
  PyObject *__pyx_v_command_mov = 0;
  PyObject *__pyx_v_command = NULL;
  PyObject *__pyx_v_ans1 = NULL;
  PyObject *__pyx_v_ans2 = NULL;
  PyObject *__pyx_v_ans3 = NULL;
  PyObject *__pyx_v_ans4 = NULL;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_t_2;
  int __pyx_t_3;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  PyObject *__pyx_t_6 = NULL;
  PyObject *__pyx_t_7 = NULL;
  PyObject *__pyx_t_8 = NULL;
  PyObject *(*__pyx_t_9)(PyObject *);
  int __pyx_t_10;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("move_order", 0);
  __Pyx_TraceCall("move_order", __pyx_f[0], 258, 0, __PYX_ERR(0, 258, __pyx_L1_error));

  /* "serialmanager.pyx":259
 * 
 * 	def move_order(self,move_comm):
 * 		cdef str command_mov = move_comm             # <<<<<<<<<<<<<<
 * 		cdef char* command_feeling
 * 		if command_mov == "follow":
 */
  __Pyx_TraceLine(259,0,__PYX_ERR(0, 259, __pyx_L1_error))
  if (!(likely(PyUnicode_CheckExact(__pyx_v_move_comm))||((__pyx_v_move_comm) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "unicode", Py_TYPE(__pyx_v_move_comm)->tp_name), 0))) __PYX_ERR(0, 259, __pyx_L1_error)
  __pyx_t_1 = __pyx_v_move_comm;
  __Pyx_INCREF(__pyx_t_1);
  __pyx_v_command_mov = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":261
 * 		cdef str command_mov = move_comm
 * 		cdef char* command_feeling
 * 		if command_mov == "follow":             # <<<<<<<<<<<<<<
 * 			command = b'<follow>'
 * 		elif command_mov == "autonomous":
 */
  __Pyx_TraceLine(261,0,__PYX_ERR(0, 261, __pyx_L1_error))
  __pyx_t_2 = (__Pyx_PyUnicode_Equals(__pyx_v_command_mov, __pyx_n_u_follow, Py_EQ)); if (unlikely(__pyx_t_2 < 0)) __PYX_ERR(0, 261, __pyx_L1_error)
  __pyx_t_3 = (__pyx_t_2 != 0);
  if (__pyx_t_3) {

    /* "serialmanager.pyx":262
 * 		cdef char* command_feeling
 * 		if command_mov == "follow":
 * 			command = b'<follow>'             # <<<<<<<<<<<<<<
 * 		elif command_mov == "autonomous":
 * 			command = b'<auto>'
 */
    __Pyx_TraceLine(262,0,__PYX_ERR(0, 262, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_b_follow_2);
    __pyx_v_command = __pyx_kp_b_follow_2;

    /* "serialmanager.pyx":261
 * 		cdef str command_mov = move_comm
 * 		cdef char* command_feeling
 * 		if command_mov == "follow":             # <<<<<<<<<<<<<<
 * 			command = b'<follow>'
 * 		elif command_mov == "autonomous":
 */
    goto __pyx_L3;
  }

  /* "serialmanager.pyx":263
 * 		if command_mov == "follow":
 * 			command = b'<follow>'
 * 		elif command_mov == "autonomous":             # <<<<<<<<<<<<<<
 * 			command = b'<auto>'
 * 		ans1,ans2,ans3,ans4 = self.tell_to_move3(command)
 */
  __Pyx_TraceLine(263,0,__PYX_ERR(0, 263, __pyx_L1_error))
  __pyx_t_3 = (__Pyx_PyUnicode_Equals(__pyx_v_command_mov, __pyx_n_u_autonomous, Py_EQ)); if (unlikely(__pyx_t_3 < 0)) __PYX_ERR(0, 263, __pyx_L1_error)
  __pyx_t_2 = (__pyx_t_3 != 0);
  if (__pyx_t_2) {

    /* "serialmanager.pyx":264
 * 			command = b'<follow>'
 * 		elif command_mov == "autonomous":
 * 			command = b'<auto>'             # <<<<<<<<<<<<<<
 * 		ans1,ans2,ans3,ans4 = self.tell_to_move3(command)
 * 		print("{},{},{},{}".format(ans1,ans2,ans3,ans4))
 */
    __Pyx_TraceLine(264,0,__PYX_ERR(0, 264, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_b_auto);
    __pyx_v_command = __pyx_kp_b_auto;

    /* "serialmanager.pyx":263
 * 		if command_mov == "follow":
 * 			command = b'<follow>'
 * 		elif command_mov == "autonomous":             # <<<<<<<<<<<<<<
 * 			command = b'<auto>'
 * 		ans1,ans2,ans3,ans4 = self.tell_to_move3(command)
 */
  }
  __pyx_L3:;

  /* "serialmanager.pyx":265
 * 		elif command_mov == "autonomous":
 * 			command = b'<auto>'
 * 		ans1,ans2,ans3,ans4 = self.tell_to_move3(command)             # <<<<<<<<<<<<<<
 * 		print("{},{},{},{}".format(ans1,ans2,ans3,ans4))
 * 
 */
  __Pyx_TraceLine(265,0,__PYX_ERR(0, 265, __pyx_L1_error))
  if (unlikely(!__pyx_v_command)) { __Pyx_RaiseUnboundLocalError("command"); __PYX_ERR(0, 265, __pyx_L1_error) }
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->tell_to_move3(__pyx_v_self, __pyx_v_command); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 265, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if ((likely(PyTuple_CheckExact(__pyx_t_1))) || (PyList_CheckExact(__pyx_t_1))) {
    PyObject* sequence = __pyx_t_1;
    Py_ssize_t size = __Pyx_PySequence_SIZE(sequence);
    if (unlikely(size != 4)) {
      if (size > 4) __Pyx_RaiseTooManyValuesError(4);
      else if (size >= 0) __Pyx_RaiseNeedMoreValuesError(size);
      __PYX_ERR(0, 265, __pyx_L1_error)
    }
    #if CYTHON_ASSUME_SAFE_MACROS && !CYTHON_AVOID_BORROWED_REFS
    if (likely(PyTuple_CheckExact(sequence))) {
      __pyx_t_4 = PyTuple_GET_ITEM(sequence, 0); 
      __pyx_t_5 = PyTuple_GET_ITEM(sequence, 1); 
      __pyx_t_6 = PyTuple_GET_ITEM(sequence, 2); 
      __pyx_t_7 = PyTuple_GET_ITEM(sequence, 3); 
    } else {
      __pyx_t_4 = PyList_GET_ITEM(sequence, 0); 
      __pyx_t_5 = PyList_GET_ITEM(sequence, 1); 
      __pyx_t_6 = PyList_GET_ITEM(sequence, 2); 
      __pyx_t_7 = PyList_GET_ITEM(sequence, 3); 
    }
    __Pyx_INCREF(__pyx_t_4);
    __Pyx_INCREF(__pyx_t_5);
    __Pyx_INCREF(__pyx_t_6);
    __Pyx_INCREF(__pyx_t_7);
    #else
    {
      Py_ssize_t i;
      PyObject** temps[4] = {&__pyx_t_4,&__pyx_t_5,&__pyx_t_6,&__pyx_t_7};
      for (i=0; i < 4; i++) {
        PyObject* item = PySequence_ITEM(sequence, i); if (unlikely(!item)) __PYX_ERR(0, 265, __pyx_L1_error)
        __Pyx_GOTREF(item);
        *(temps[i]) = item;
      }
    }
    #endif
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  } else {
    Py_ssize_t index = -1;
    PyObject** temps[4] = {&__pyx_t_4,&__pyx_t_5,&__pyx_t_6,&__pyx_t_7};
    __pyx_t_8 = PyObject_GetIter(__pyx_t_1); if (unlikely(!__pyx_t_8)) __PYX_ERR(0, 265, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_8);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_t_9 = Py_TYPE(__pyx_t_8)->tp_iternext;
    for (index=0; index < 4; index++) {
      PyObject* item = __pyx_t_9(__pyx_t_8); if (unlikely(!item)) goto __pyx_L4_unpacking_failed;
      __Pyx_GOTREF(item);
      *(temps[index]) = item;
    }
    if (__Pyx_IternextUnpackEndCheck(__pyx_t_9(__pyx_t_8), 4) < 0) __PYX_ERR(0, 265, __pyx_L1_error)
    __pyx_t_9 = NULL;
    __Pyx_DECREF(__pyx_t_8); __pyx_t_8 = 0;
    goto __pyx_L5_unpacking_done;
    __pyx_L4_unpacking_failed:;
    __Pyx_DECREF(__pyx_t_8); __pyx_t_8 = 0;
    __pyx_t_9 = NULL;
    if (__Pyx_IterFinish() == 0) __Pyx_RaiseNeedMoreValuesError(index);
    __PYX_ERR(0, 265, __pyx_L1_error)
    __pyx_L5_unpacking_done:;
  }
  __pyx_v_ans1 = __pyx_t_4;
  __pyx_t_4 = 0;
  __pyx_v_ans2 = __pyx_t_5;
  __pyx_t_5 = 0;
  __pyx_v_ans3 = __pyx_t_6;
  __pyx_t_6 = 0;
  __pyx_v_ans4 = __pyx_t_7;
  __pyx_t_7 = 0;

  /* "serialmanager.pyx":266
 * 			command = b'<auto>'
 * 		ans1,ans2,ans3,ans4 = self.tell_to_move3(command)
 * 		print("{},{},{},{}".format(ans1,ans2,ans3,ans4))             # <<<<<<<<<<<<<<
 * 
 * 	def react_results_final(self,move_comm,led_col,animation,for_speed,ang_speed,choosen_audio):
 */
  __Pyx_TraceLine(266,0,__PYX_ERR(0, 266, __pyx_L1_error))
  __pyx_t_7 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u__4, __pyx_n_s_format); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 266, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_7);
  __pyx_t_6 = NULL;
  __pyx_t_10 = 0;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_7))) {
    __pyx_t_6 = PyMethod_GET_SELF(__pyx_t_7);
    if (likely(__pyx_t_6)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_7);
      __Pyx_INCREF(__pyx_t_6);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_7, function);
      __pyx_t_10 = 1;
    }
  }
  #if CYTHON_FAST_PYCALL
  if (PyFunction_Check(__pyx_t_7)) {
    PyObject *__pyx_temp[5] = {__pyx_t_6, __pyx_v_ans1, __pyx_v_ans2, __pyx_v_ans3, __pyx_v_ans4};
    __pyx_t_1 = __Pyx_PyFunction_FastCall(__pyx_t_7, __pyx_temp+1-__pyx_t_10, 4+__pyx_t_10); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 266, __pyx_L1_error)
    __Pyx_XDECREF(__pyx_t_6); __pyx_t_6 = 0;
    __Pyx_GOTREF(__pyx_t_1);
  } else
  #endif
  #if CYTHON_FAST_PYCCALL
  if (__Pyx_PyFastCFunction_Check(__pyx_t_7)) {
    PyObject *__pyx_temp[5] = {__pyx_t_6, __pyx_v_ans1, __pyx_v_ans2, __pyx_v_ans3, __pyx_v_ans4};
    __pyx_t_1 = __Pyx_PyCFunction_FastCall(__pyx_t_7, __pyx_temp+1-__pyx_t_10, 4+__pyx_t_10); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 266, __pyx_L1_error)
    __Pyx_XDECREF(__pyx_t_6); __pyx_t_6 = 0;
    __Pyx_GOTREF(__pyx_t_1);
  } else
  #endif
  {
    __pyx_t_5 = PyTuple_New(4+__pyx_t_10); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 266, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    if (__pyx_t_6) {
      __Pyx_GIVEREF(__pyx_t_6); PyTuple_SET_ITEM(__pyx_t_5, 0, __pyx_t_6); __pyx_t_6 = NULL;
    }
    __Pyx_INCREF(__pyx_v_ans1);
    __Pyx_GIVEREF(__pyx_v_ans1);
    PyTuple_SET_ITEM(__pyx_t_5, 0+__pyx_t_10, __pyx_v_ans1);
    __Pyx_INCREF(__pyx_v_ans2);
    __Pyx_GIVEREF(__pyx_v_ans2);
    PyTuple_SET_ITEM(__pyx_t_5, 1+__pyx_t_10, __pyx_v_ans2);
    __Pyx_INCREF(__pyx_v_ans3);
    __Pyx_GIVEREF(__pyx_v_ans3);
    PyTuple_SET_ITEM(__pyx_t_5, 2+__pyx_t_10, __pyx_v_ans3);
    __Pyx_INCREF(__pyx_v_ans4);
    __Pyx_GIVEREF(__pyx_v_ans4);
    PyTuple_SET_ITEM(__pyx_t_5, 3+__pyx_t_10, __pyx_v_ans4);
    __pyx_t_1 = __Pyx_PyObject_Call(__pyx_t_7, __pyx_t_5, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 266, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
  }
  __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
  __pyx_t_7 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 266, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_7);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;

  /* "serialmanager.pyx":258
 * 		self.daje(val)
 * 
 * 	def move_order(self,move_comm):             # <<<<<<<<<<<<<<
 * 		cdef str command_mov = move_comm
 * 		cdef char* command_feeling
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_XDECREF(__pyx_t_6);
  __Pyx_XDECREF(__pyx_t_7);
  __Pyx_XDECREF(__pyx_t_8);
  __Pyx_AddTraceback("serialmanager.SerialManager.move_order", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_command_mov);
  __Pyx_XDECREF(__pyx_v_command);
  __Pyx_XDECREF(__pyx_v_ans1);
  __Pyx_XDECREF(__pyx_v_ans2);
  __Pyx_XDECREF(__pyx_v_ans3);
  __Pyx_XDECREF(__pyx_v_ans4);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":268
 * 		print("{},{},{},{}".format(ans1,ans2,ans3,ans4))
 * 
 * 	def react_results_final(self,move_comm,led_col,animation,for_speed,ang_speed,choosen_audio):             # <<<<<<<<<<<<<<
 * 		end = '>'
 * 		if move_comm == "sad":
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_23react_results_final(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_22react_results_final[] = "SerialManager.react_results_final(self, move_comm, led_col, animation, for_speed, ang_speed, choosen_audio)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_23react_results_final(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_v_move_comm = 0;
  PyObject *__pyx_v_led_col = 0;
  PyObject *__pyx_v_animation = 0;
  PyObject *__pyx_v_for_speed = 0;
  PyObject *__pyx_v_ang_speed = 0;
  CYTHON_UNUSED PyObject *__pyx_v_choosen_audio = 0;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("react_results_final (wrapper)", 0);
  {
    static PyObject **__pyx_pyargnames[] = {&__pyx_n_s_move_comm,&__pyx_n_s_led_col,&__pyx_n_s_animation,&__pyx_n_s_for_speed,&__pyx_n_s_ang_speed,&__pyx_n_s_choosen_audio,0};
    PyObject* values[6] = {0,0,0,0,0,0};
    if (unlikely(__pyx_kwds)) {
      Py_ssize_t kw_args;
      const Py_ssize_t pos_args = PyTuple_GET_SIZE(__pyx_args);
      switch (pos_args) {
        case  6: values[5] = PyTuple_GET_ITEM(__pyx_args, 5);
        CYTHON_FALLTHROUGH;
        case  5: values[4] = PyTuple_GET_ITEM(__pyx_args, 4);
        CYTHON_FALLTHROUGH;
        case  4: values[3] = PyTuple_GET_ITEM(__pyx_args, 3);
        CYTHON_FALLTHROUGH;
        case  3: values[2] = PyTuple_GET_ITEM(__pyx_args, 2);
        CYTHON_FALLTHROUGH;
        case  2: values[1] = PyTuple_GET_ITEM(__pyx_args, 1);
        CYTHON_FALLTHROUGH;
        case  1: values[0] = PyTuple_GET_ITEM(__pyx_args, 0);
        CYTHON_FALLTHROUGH;
        case  0: break;
        default: goto __pyx_L5_argtuple_error;
      }
      kw_args = PyDict_Size(__pyx_kwds);
      switch (pos_args) {
        case  0:
        if (likely((values[0] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_move_comm)) != 0)) kw_args--;
        else goto __pyx_L5_argtuple_error;
        CYTHON_FALLTHROUGH;
        case  1:
        if (likely((values[1] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_led_col)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("react_results_final", 1, 6, 6, 1); __PYX_ERR(0, 268, __pyx_L3_error)
        }
        CYTHON_FALLTHROUGH;
        case  2:
        if (likely((values[2] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_animation)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("react_results_final", 1, 6, 6, 2); __PYX_ERR(0, 268, __pyx_L3_error)
        }
        CYTHON_FALLTHROUGH;
        case  3:
        if (likely((values[3] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_for_speed)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("react_results_final", 1, 6, 6, 3); __PYX_ERR(0, 268, __pyx_L3_error)
        }
        CYTHON_FALLTHROUGH;
        case  4:
        if (likely((values[4] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_ang_speed)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("react_results_final", 1, 6, 6, 4); __PYX_ERR(0, 268, __pyx_L3_error)
        }
        CYTHON_FALLTHROUGH;
        case  5:
        if (likely((values[5] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_choosen_audio)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("react_results_final", 1, 6, 6, 5); __PYX_ERR(0, 268, __pyx_L3_error)
        }
      }
      if (unlikely(kw_args > 0)) {
        if (unlikely(__Pyx_ParseOptionalKeywords(__pyx_kwds, __pyx_pyargnames, 0, values, pos_args, "react_results_final") < 0)) __PYX_ERR(0, 268, __pyx_L3_error)
      }
    } else if (PyTuple_GET_SIZE(__pyx_args) != 6) {
      goto __pyx_L5_argtuple_error;
    } else {
      values[0] = PyTuple_GET_ITEM(__pyx_args, 0);
      values[1] = PyTuple_GET_ITEM(__pyx_args, 1);
      values[2] = PyTuple_GET_ITEM(__pyx_args, 2);
      values[3] = PyTuple_GET_ITEM(__pyx_args, 3);
      values[4] = PyTuple_GET_ITEM(__pyx_args, 4);
      values[5] = PyTuple_GET_ITEM(__pyx_args, 5);
    }
    __pyx_v_move_comm = values[0];
    __pyx_v_led_col = values[1];
    __pyx_v_animation = values[2];
    __pyx_v_for_speed = values[3];
    __pyx_v_ang_speed = values[4];
    __pyx_v_choosen_audio = values[5];
  }
  goto __pyx_L4_argument_unpacking_done;
  __pyx_L5_argtuple_error:;
  __Pyx_RaiseArgtupleInvalid("react_results_final", 1, 6, 6, PyTuple_GET_SIZE(__pyx_args)); __PYX_ERR(0, 268, __pyx_L3_error)
  __pyx_L3_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.react_results_final", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __Pyx_RefNannyFinishContext();
  return NULL;
  __pyx_L4_argument_unpacking_done:;
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_22react_results_final(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), __pyx_v_move_comm, __pyx_v_led_col, __pyx_v_animation, __pyx_v_for_speed, __pyx_v_ang_speed, __pyx_v_choosen_audio);

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_22react_results_final(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_move_comm, PyObject *__pyx_v_led_col, PyObject *__pyx_v_animation, PyObject *__pyx_v_for_speed, PyObject *__pyx_v_ang_speed, CYTHON_UNUSED PyObject *__pyx_v_choosen_audio) {
  PyObject *__pyx_v_end = NULL;
  PyObject *__pyx_v_command_feeling = NULL;
  PyObject *__pyx_v_values = NULL;
  PyObject *__pyx_v_strin = NULL;
  PyObject *__pyx_v_addend = NULL;
  PyObject *__pyx_v_command_to_send = NULL;
  PyObject *__pyx_v_ans1 = NULL;
  PyObject *__pyx_v_ans2 = NULL;
  PyObject *__pyx_v_ans3 = NULL;
  PyObject *__pyx_v_ans4 = NULL;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  PyObject *__pyx_t_6 = NULL;
  PyObject *__pyx_t_7 = NULL;
  PyObject *(*__pyx_t_8)(PyObject *);
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("react_results_final", 0);
  __Pyx_TraceCall("react_results_final", __pyx_f[0], 268, 0, __PYX_ERR(0, 268, __pyx_L1_error));

  /* "serialmanager.pyx":269
 * 
 * 	def react_results_final(self,move_comm,led_col,animation,for_speed,ang_speed,choosen_audio):
 * 		end = '>'             # <<<<<<<<<<<<<<
 * 		if move_comm == "sad":
 * 			command_feeling = '<sad'
 */
  __Pyx_TraceLine(269,0,__PYX_ERR(0, 269, __pyx_L1_error))
  __Pyx_INCREF(__pyx_kp_u__5);
  __pyx_v_end = __pyx_kp_u__5;

  /* "serialmanager.pyx":270
 * 	def react_results_final(self,move_comm,led_col,animation,for_speed,ang_speed,choosen_audio):
 * 		end = '>'
 * 		if move_comm == "sad":             # <<<<<<<<<<<<<<
 * 			command_feeling = '<sad'
 * 		elif move_comm == "normal":
 */
  __Pyx_TraceLine(270,0,__PYX_ERR(0, 270, __pyx_L1_error))
  __pyx_t_1 = (__Pyx_PyUnicode_Equals(__pyx_v_move_comm, __pyx_n_u_sad, Py_EQ)); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 270, __pyx_L1_error)
  if (__pyx_t_1) {

    /* "serialmanager.pyx":271
 * 		end = '>'
 * 		if move_comm == "sad":
 * 			command_feeling = '<sad'             # <<<<<<<<<<<<<<
 * 		elif move_comm == "normal":
 * 			command_feeling = '<normal'
 */
    __Pyx_TraceLine(271,0,__PYX_ERR(0, 271, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_u_sad_2);
    __pyx_v_command_feeling = __pyx_kp_u_sad_2;

    /* "serialmanager.pyx":270
 * 	def react_results_final(self,move_comm,led_col,animation,for_speed,ang_speed,choosen_audio):
 * 		end = '>'
 * 		if move_comm == "sad":             # <<<<<<<<<<<<<<
 * 			command_feeling = '<sad'
 * 		elif move_comm == "normal":
 */
    goto __pyx_L3;
  }

  /* "serialmanager.pyx":272
 * 		if move_comm == "sad":
 * 			command_feeling = '<sad'
 * 		elif move_comm == "normal":             # <<<<<<<<<<<<<<
 * 			command_feeling = '<normal'
 * 		elif move_comm == "happy":
 */
  __Pyx_TraceLine(272,0,__PYX_ERR(0, 272, __pyx_L1_error))
  __pyx_t_1 = (__Pyx_PyUnicode_Equals(__pyx_v_move_comm, __pyx_n_u_normal, Py_EQ)); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 272, __pyx_L1_error)
  if (__pyx_t_1) {

    /* "serialmanager.pyx":273
 * 			command_feeling = '<sad'
 * 		elif move_comm == "normal":
 * 			command_feeling = '<normal'             # <<<<<<<<<<<<<<
 * 		elif move_comm == "happy":
 * 			command_feeling = '<happy'
 */
    __Pyx_TraceLine(273,0,__PYX_ERR(0, 273, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_u_normal_2);
    __pyx_v_command_feeling = __pyx_kp_u_normal_2;

    /* "serialmanager.pyx":272
 * 		if move_comm == "sad":
 * 			command_feeling = '<sad'
 * 		elif move_comm == "normal":             # <<<<<<<<<<<<<<
 * 			command_feeling = '<normal'
 * 		elif move_comm == "happy":
 */
    goto __pyx_L3;
  }

  /* "serialmanager.pyx":274
 * 		elif move_comm == "normal":
 * 			command_feeling = '<normal'
 * 		elif move_comm == "happy":             # <<<<<<<<<<<<<<
 * 			command_feeling = '<happy'
 * 		elif move_comm == "enthusiast":
 */
  __Pyx_TraceLine(274,0,__PYX_ERR(0, 274, __pyx_L1_error))
  __pyx_t_1 = (__Pyx_PyUnicode_Equals(__pyx_v_move_comm, __pyx_n_u_happy, Py_EQ)); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 274, __pyx_L1_error)
  if (__pyx_t_1) {

    /* "serialmanager.pyx":275
 * 			command_feeling = '<normal'
 * 		elif move_comm == "happy":
 * 			command_feeling = '<happy'             # <<<<<<<<<<<<<<
 * 		elif move_comm == "enthusiast":
 * 			command_feeling = '<enthusiast'
 */
    __Pyx_TraceLine(275,0,__PYX_ERR(0, 275, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_u_happy_2);
    __pyx_v_command_feeling = __pyx_kp_u_happy_2;

    /* "serialmanager.pyx":274
 * 		elif move_comm == "normal":
 * 			command_feeling = '<normal'
 * 		elif move_comm == "happy":             # <<<<<<<<<<<<<<
 * 			command_feeling = '<happy'
 * 		elif move_comm == "enthusiast":
 */
    goto __pyx_L3;
  }

  /* "serialmanager.pyx":276
 * 		elif move_comm == "happy":
 * 			command_feeling = '<happy'
 * 		elif move_comm == "enthusiast":             # <<<<<<<<<<<<<<
 * 			command_feeling = '<enthusiast'
 * 
 */
  __Pyx_TraceLine(276,0,__PYX_ERR(0, 276, __pyx_L1_error))
  __pyx_t_1 = (__Pyx_PyUnicode_Equals(__pyx_v_move_comm, __pyx_n_u_enthusiast, Py_EQ)); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 276, __pyx_L1_error)
  if (__pyx_t_1) {

    /* "serialmanager.pyx":277
 * 			command_feeling = '<happy'
 * 		elif move_comm == "enthusiast":
 * 			command_feeling = '<enthusiast'             # <<<<<<<<<<<<<<
 * 
 * 		values = [command_feeling,led_col,animation,for_speed,ang_speed]
 */
    __Pyx_TraceLine(277,0,__PYX_ERR(0, 277, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_u_enthusiast_2);
    __pyx_v_command_feeling = __pyx_kp_u_enthusiast_2;

    /* "serialmanager.pyx":276
 * 		elif move_comm == "happy":
 * 			command_feeling = '<happy'
 * 		elif move_comm == "enthusiast":             # <<<<<<<<<<<<<<
 * 			command_feeling = '<enthusiast'
 * 
 */
  }
  __pyx_L3:;

  /* "serialmanager.pyx":279
 * 			command_feeling = '<enthusiast'
 * 
 * 		values = [command_feeling,led_col,animation,for_speed,ang_speed]             # <<<<<<<<<<<<<<
 * 		strin = ','.join(map(str, values))
 * 		addend = [strin,end]
 */
  __Pyx_TraceLine(279,0,__PYX_ERR(0, 279, __pyx_L1_error))
  if (unlikely(!__pyx_v_command_feeling)) { __Pyx_RaiseUnboundLocalError("command_feeling"); __PYX_ERR(0, 279, __pyx_L1_error) }
  __pyx_t_2 = PyList_New(5); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 279, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_INCREF(__pyx_v_command_feeling);
  __Pyx_GIVEREF(__pyx_v_command_feeling);
  PyList_SET_ITEM(__pyx_t_2, 0, __pyx_v_command_feeling);
  __Pyx_INCREF(__pyx_v_led_col);
  __Pyx_GIVEREF(__pyx_v_led_col);
  PyList_SET_ITEM(__pyx_t_2, 1, __pyx_v_led_col);
  __Pyx_INCREF(__pyx_v_animation);
  __Pyx_GIVEREF(__pyx_v_animation);
  PyList_SET_ITEM(__pyx_t_2, 2, __pyx_v_animation);
  __Pyx_INCREF(__pyx_v_for_speed);
  __Pyx_GIVEREF(__pyx_v_for_speed);
  PyList_SET_ITEM(__pyx_t_2, 3, __pyx_v_for_speed);
  __Pyx_INCREF(__pyx_v_ang_speed);
  __Pyx_GIVEREF(__pyx_v_ang_speed);
  PyList_SET_ITEM(__pyx_t_2, 4, __pyx_v_ang_speed);
  __pyx_v_values = ((PyObject*)__pyx_t_2);
  __pyx_t_2 = 0;

  /* "serialmanager.pyx":280
 * 
 * 		values = [command_feeling,led_col,animation,for_speed,ang_speed]
 * 		strin = ','.join(map(str, values))             # <<<<<<<<<<<<<<
 * 		addend = [strin,end]
 * 		command_to_send = ''.join(map(str, addend))
 */
  __Pyx_TraceLine(280,0,__PYX_ERR(0, 280, __pyx_L1_error))
  __pyx_t_2 = PyTuple_New(2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 280, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_INCREF(((PyObject *)(&PyUnicode_Type)));
  __Pyx_GIVEREF(((PyObject *)(&PyUnicode_Type)));
  PyTuple_SET_ITEM(__pyx_t_2, 0, ((PyObject *)(&PyUnicode_Type)));
  __Pyx_INCREF(__pyx_v_values);
  __Pyx_GIVEREF(__pyx_v_values);
  PyTuple_SET_ITEM(__pyx_t_2, 1, __pyx_v_values);
  __pyx_t_3 = __Pyx_PyObject_Call(__pyx_builtin_map, __pyx_t_2, NULL); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 280, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_2 = PyUnicode_Join(__pyx_kp_u__6, __pyx_t_3); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 280, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_v_strin = ((PyObject*)__pyx_t_2);
  __pyx_t_2 = 0;

  /* "serialmanager.pyx":281
 * 		values = [command_feeling,led_col,animation,for_speed,ang_speed]
 * 		strin = ','.join(map(str, values))
 * 		addend = [strin,end]             # <<<<<<<<<<<<<<
 * 		command_to_send = ''.join(map(str, addend))
 * 		print("command to send is {}".format(command_to_send))
 */
  __Pyx_TraceLine(281,0,__PYX_ERR(0, 281, __pyx_L1_error))
  __pyx_t_2 = PyList_New(2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 281, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_INCREF(__pyx_v_strin);
  __Pyx_GIVEREF(__pyx_v_strin);
  PyList_SET_ITEM(__pyx_t_2, 0, __pyx_v_strin);
  __Pyx_INCREF(__pyx_v_end);
  __Pyx_GIVEREF(__pyx_v_end);
  PyList_SET_ITEM(__pyx_t_2, 1, __pyx_v_end);
  __pyx_v_addend = ((PyObject*)__pyx_t_2);
  __pyx_t_2 = 0;

  /* "serialmanager.pyx":282
 * 		strin = ','.join(map(str, values))
 * 		addend = [strin,end]
 * 		command_to_send = ''.join(map(str, addend))             # <<<<<<<<<<<<<<
 * 		print("command to send is {}".format(command_to_send))
 * 		command_to_send = command_to_send.encode('UTF-8', 'strict')
 */
  __Pyx_TraceLine(282,0,__PYX_ERR(0, 282, __pyx_L1_error))
  __pyx_t_2 = PyTuple_New(2); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 282, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_INCREF(((PyObject *)(&PyUnicode_Type)));
  __Pyx_GIVEREF(((PyObject *)(&PyUnicode_Type)));
  PyTuple_SET_ITEM(__pyx_t_2, 0, ((PyObject *)(&PyUnicode_Type)));
  __Pyx_INCREF(__pyx_v_addend);
  __Pyx_GIVEREF(__pyx_v_addend);
  PyTuple_SET_ITEM(__pyx_t_2, 1, __pyx_v_addend);
  __pyx_t_3 = __Pyx_PyObject_Call(__pyx_builtin_map, __pyx_t_2, NULL); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 282, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_2 = PyUnicode_Join(__pyx_kp_u__3, __pyx_t_3); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 282, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_v_command_to_send = __pyx_t_2;
  __pyx_t_2 = 0;

  /* "serialmanager.pyx":283
 * 		addend = [strin,end]
 * 		command_to_send = ''.join(map(str, addend))
 * 		print("command to send is {}".format(command_to_send))             # <<<<<<<<<<<<<<
 * 		command_to_send = command_to_send.encode('UTF-8', 'strict')
 * 		#cdef char* command = command_to_send #per passare char faccio prova coi cdef
 */
  __Pyx_TraceLine(283,0,__PYX_ERR(0, 283, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_command_to_send_is, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 283, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_4 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
    __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
    if (likely(__pyx_t_4)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
      __Pyx_INCREF(__pyx_t_4);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_3, function);
    }
  }
  __pyx_t_2 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_4, __pyx_v_command_to_send) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_v_command_to_send);
  __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
  if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 283, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 283, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

  /* "serialmanager.pyx":284
 * 		command_to_send = ''.join(map(str, addend))
 * 		print("command to send is {}".format(command_to_send))
 * 		command_to_send = command_to_send.encode('UTF-8', 'strict')             # <<<<<<<<<<<<<<
 * 		#cdef char* command = command_to_send #per passare char faccio prova coi cdef
 * 
 */
  __Pyx_TraceLine(284,0,__PYX_ERR(0, 284, __pyx_L1_error))
  if (unlikely(__pyx_v_command_to_send == Py_None)) {
    PyErr_Format(PyExc_AttributeError, "'NoneType' object has no attribute '%.30s'", "encode");
    __PYX_ERR(0, 284, __pyx_L1_error)
  }
  __pyx_t_3 = PyUnicode_AsUTF8String(__pyx_v_command_to_send); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 284, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF_SET(__pyx_v_command_to_send, __pyx_t_3);
  __pyx_t_3 = 0;

  /* "serialmanager.pyx":289
 * 		#gare.play_audio(choosen_audio)
 * 
 * 		ans1, ans2, ans3, ans4 = self.tell_to_move(command_to_send)             # <<<<<<<<<<<<<<
 * 		print("ans1 is {}".format(ans1))
 * 		print("ans2 is {}".format(ans2))
 */
  __Pyx_TraceLine(289,0,__PYX_ERR(0, 289, __pyx_L1_error))
  __pyx_t_3 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->tell_to_move(__pyx_v_self, __pyx_v_command_to_send); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 289, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  if ((likely(PyTuple_CheckExact(__pyx_t_3))) || (PyList_CheckExact(__pyx_t_3))) {
    PyObject* sequence = __pyx_t_3;
    Py_ssize_t size = __Pyx_PySequence_SIZE(sequence);
    if (unlikely(size != 4)) {
      if (size > 4) __Pyx_RaiseTooManyValuesError(4);
      else if (size >= 0) __Pyx_RaiseNeedMoreValuesError(size);
      __PYX_ERR(0, 289, __pyx_L1_error)
    }
    #if CYTHON_ASSUME_SAFE_MACROS && !CYTHON_AVOID_BORROWED_REFS
    if (likely(PyTuple_CheckExact(sequence))) {
      __pyx_t_2 = PyTuple_GET_ITEM(sequence, 0); 
      __pyx_t_4 = PyTuple_GET_ITEM(sequence, 1); 
      __pyx_t_5 = PyTuple_GET_ITEM(sequence, 2); 
      __pyx_t_6 = PyTuple_GET_ITEM(sequence, 3); 
    } else {
      __pyx_t_2 = PyList_GET_ITEM(sequence, 0); 
      __pyx_t_4 = PyList_GET_ITEM(sequence, 1); 
      __pyx_t_5 = PyList_GET_ITEM(sequence, 2); 
      __pyx_t_6 = PyList_GET_ITEM(sequence, 3); 
    }
    __Pyx_INCREF(__pyx_t_2);
    __Pyx_INCREF(__pyx_t_4);
    __Pyx_INCREF(__pyx_t_5);
    __Pyx_INCREF(__pyx_t_6);
    #else
    {
      Py_ssize_t i;
      PyObject** temps[4] = {&__pyx_t_2,&__pyx_t_4,&__pyx_t_5,&__pyx_t_6};
      for (i=0; i < 4; i++) {
        PyObject* item = PySequence_ITEM(sequence, i); if (unlikely(!item)) __PYX_ERR(0, 289, __pyx_L1_error)
        __Pyx_GOTREF(item);
        *(temps[i]) = item;
      }
    }
    #endif
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  } else {
    Py_ssize_t index = -1;
    PyObject** temps[4] = {&__pyx_t_2,&__pyx_t_4,&__pyx_t_5,&__pyx_t_6};
    __pyx_t_7 = PyObject_GetIter(__pyx_t_3); if (unlikely(!__pyx_t_7)) __PYX_ERR(0, 289, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_7);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_8 = Py_TYPE(__pyx_t_7)->tp_iternext;
    for (index=0; index < 4; index++) {
      PyObject* item = __pyx_t_8(__pyx_t_7); if (unlikely(!item)) goto __pyx_L4_unpacking_failed;
      __Pyx_GOTREF(item);
      *(temps[index]) = item;
    }
    if (__Pyx_IternextUnpackEndCheck(__pyx_t_8(__pyx_t_7), 4) < 0) __PYX_ERR(0, 289, __pyx_L1_error)
    __pyx_t_8 = NULL;
    __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
    goto __pyx_L5_unpacking_done;
    __pyx_L4_unpacking_failed:;
    __Pyx_DECREF(__pyx_t_7); __pyx_t_7 = 0;
    __pyx_t_8 = NULL;
    if (__Pyx_IterFinish() == 0) __Pyx_RaiseNeedMoreValuesError(index);
    __PYX_ERR(0, 289, __pyx_L1_error)
    __pyx_L5_unpacking_done:;
  }
  __pyx_v_ans1 = __pyx_t_2;
  __pyx_t_2 = 0;
  __pyx_v_ans2 = __pyx_t_4;
  __pyx_t_4 = 0;
  __pyx_v_ans3 = __pyx_t_5;
  __pyx_t_5 = 0;
  __pyx_v_ans4 = __pyx_t_6;
  __pyx_t_6 = 0;

  /* "serialmanager.pyx":290
 * 
 * 		ans1, ans2, ans3, ans4 = self.tell_to_move(command_to_send)
 * 		print("ans1 is {}".format(ans1))             # <<<<<<<<<<<<<<
 * 		print("ans2 is {}".format(ans2))
 * 		print("ans3 is {}".format(ans3))
 */
  __Pyx_TraceLine(290,0,__PYX_ERR(0, 290, __pyx_L1_error))
  __pyx_t_6 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_ans1_is, __pyx_n_s_format); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 290, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_6);
  __pyx_t_5 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_6))) {
    __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_6);
    if (likely(__pyx_t_5)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_6);
      __Pyx_INCREF(__pyx_t_5);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_6, function);
    }
  }
  __pyx_t_3 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_6, __pyx_t_5, __pyx_v_ans1) : __Pyx_PyObject_CallOneArg(__pyx_t_6, __pyx_v_ans1);
  __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
  if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 290, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;
  __pyx_t_6 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 290, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_6);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;

  /* "serialmanager.pyx":291
 * 		ans1, ans2, ans3, ans4 = self.tell_to_move(command_to_send)
 * 		print("ans1 is {}".format(ans1))
 * 		print("ans2 is {}".format(ans2))             # <<<<<<<<<<<<<<
 * 		print("ans3 is {}".format(ans3))
 * 		print("ans4 is {}".format(ans4))
 */
  __Pyx_TraceLine(291,0,__PYX_ERR(0, 291, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_ans2_is, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 291, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_5 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
    __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
    if (likely(__pyx_t_5)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
      __Pyx_INCREF(__pyx_t_5);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_3, function);
    }
  }
  __pyx_t_6 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_v_ans2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_v_ans2);
  __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
  if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 291, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_6);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_6); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 291, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

  /* "serialmanager.pyx":292
 * 		print("ans1 is {}".format(ans1))
 * 		print("ans2 is {}".format(ans2))
 * 		print("ans3 is {}".format(ans3))             # <<<<<<<<<<<<<<
 * 		print("ans4 is {}".format(ans4))
 * 
 */
  __Pyx_TraceLine(292,0,__PYX_ERR(0, 292, __pyx_L1_error))
  __pyx_t_6 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_ans3_is, __pyx_n_s_format); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 292, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_6);
  __pyx_t_5 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_6))) {
    __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_6);
    if (likely(__pyx_t_5)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_6);
      __Pyx_INCREF(__pyx_t_5);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_6, function);
    }
  }
  __pyx_t_3 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_6, __pyx_t_5, __pyx_v_ans3) : __Pyx_PyObject_CallOneArg(__pyx_t_6, __pyx_v_ans3);
  __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
  if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 292, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;
  __pyx_t_6 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 292, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_6);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;

  /* "serialmanager.pyx":293
 * 		print("ans2 is {}".format(ans2))
 * 		print("ans3 is {}".format(ans3))
 * 		print("ans4 is {}".format(ans4))             # <<<<<<<<<<<<<<
 * 
 * 	def test_for_movement(self):
 */
  __Pyx_TraceLine(293,0,__PYX_ERR(0, 293, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_ans4_is, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 293, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_5 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
    __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
    if (likely(__pyx_t_5)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
      __Pyx_INCREF(__pyx_t_5);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_3, function);
    }
  }
  __pyx_t_6 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_v_ans4) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_v_ans4);
  __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
  if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 293, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_6);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_6); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 293, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_6); __pyx_t_6 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

  /* "serialmanager.pyx":268
 * 		print("{},{},{},{}".format(ans1,ans2,ans3,ans4))
 * 
 * 	def react_results_final(self,move_comm,led_col,animation,for_speed,ang_speed,choosen_audio):             # <<<<<<<<<<<<<<
 * 		end = '>'
 * 		if move_comm == "sad":
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_XDECREF(__pyx_t_6);
  __Pyx_XDECREF(__pyx_t_7);
  __Pyx_AddTraceback("serialmanager.SerialManager.react_results_final", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_end);
  __Pyx_XDECREF(__pyx_v_command_feeling);
  __Pyx_XDECREF(__pyx_v_values);
  __Pyx_XDECREF(__pyx_v_strin);
  __Pyx_XDECREF(__pyx_v_addend);
  __Pyx_XDECREF(__pyx_v_command_to_send);
  __Pyx_XDECREF(__pyx_v_ans1);
  __Pyx_XDECREF(__pyx_v_ans2);
  __Pyx_XDECREF(__pyx_v_ans3);
  __Pyx_XDECREF(__pyx_v_ans4);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":295
 * 		print("ans4 is {}".format(ans4))
 * 
 * 	def test_for_movement(self):             # <<<<<<<<<<<<<<
 * 		cdef char* comma = b'<auto>'
 * 		commando = "<auto>"
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_25test_for_movement(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_24test_for_movement[] = "SerialManager.test_for_movement(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_25test_for_movement(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("test_for_movement (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_24test_for_movement(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_24test_for_movement(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  CYTHON_UNUSED char *__pyx_v_comma;
  PyObject *__pyx_v_commando = NULL;
  CYTHON_UNUSED int __pyx_v_maxtime;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("test_for_movement", 0);
  __Pyx_TraceCall("test_for_movement", __pyx_f[0], 295, 0, __PYX_ERR(0, 295, __pyx_L1_error));

  /* "serialmanager.pyx":296
 * 
 * 	def test_for_movement(self):
 * 		cdef char* comma = b'<auto>'             # <<<<<<<<<<<<<<
 * 		commando = "<auto>"
 * 		commando = commando.encode('UTF-8', 'strict')
 */
  __Pyx_TraceLine(296,0,__PYX_ERR(0, 296, __pyx_L1_error))
  __pyx_v_comma = ((char *)"<auto>");

  /* "serialmanager.pyx":297
 * 	def test_for_movement(self):
 * 		cdef char* comma = b'<auto>'
 * 		commando = "<auto>"             # <<<<<<<<<<<<<<
 * 		commando = commando.encode('UTF-8', 'strict')
 * 		cdef int maxtime = 3000
 */
  __Pyx_TraceLine(297,0,__PYX_ERR(0, 297, __pyx_L1_error))
  __Pyx_INCREF(__pyx_kp_u_auto);
  __pyx_v_commando = __pyx_kp_u_auto;

  /* "serialmanager.pyx":298
 * 		cdef char* comma = b'<auto>'
 * 		commando = "<auto>"
 * 		commando = commando.encode('UTF-8', 'strict')             # <<<<<<<<<<<<<<
 * 		cdef int maxtime = 3000
 * 		self.tell_to_move3(commando)
 */
  __Pyx_TraceLine(298,0,__PYX_ERR(0, 298, __pyx_L1_error))
  __pyx_t_1 = PyUnicode_AsUTF8String(__pyx_v_commando); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 298, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF_SET(__pyx_v_commando, __pyx_t_1);
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":299
 * 		commando = "<auto>"
 * 		commando = commando.encode('UTF-8', 'strict')
 * 		cdef int maxtime = 3000             # <<<<<<<<<<<<<<
 * 		self.tell_to_move3(commando)
 * #		res = capa.fun_mpr121(maxtime)
 */
  __Pyx_TraceLine(299,0,__PYX_ERR(0, 299, __pyx_L1_error))
  __pyx_v_maxtime = 0xBB8;

  /* "serialmanager.pyx":300
 * 		commando = commando.encode('UTF-8', 'strict')
 * 		cdef int maxtime = 3000
 * 		self.tell_to_move3(commando)             # <<<<<<<<<<<<<<
 * #		res = capa.fun_mpr121(maxtime)
 * #		if res==1:
 */
  __Pyx_TraceLine(300,0,__PYX_ERR(0, 300, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->tell_to_move3(__pyx_v_self, __pyx_v_commando); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 300, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":295
 * 		print("ans4 is {}".format(ans4))
 * 
 * 	def test_for_movement(self):             # <<<<<<<<<<<<<<
 * 		cdef char* comma = b'<auto>'
 * 		commando = "<auto>"
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.test_for_movement", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_commando);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":306
 * #			self.tell_to_stop(stopcom)
 * 
 * 	def test_for_stop(self):             # <<<<<<<<<<<<<<
 * 		cdef char* stopcom  = b'<stop>'
 * 		ret = self.tell_to_stop(stopcom)
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_27test_for_stop(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_26test_for_stop[] = "SerialManager.test_for_stop(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_27test_for_stop(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("test_for_stop (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_26test_for_stop(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_26test_for_stop(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  char *__pyx_v_stopcom;
  PyObject *__pyx_v_ret = NULL;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("test_for_stop", 0);
  __Pyx_TraceCall("test_for_stop", __pyx_f[0], 306, 0, __PYX_ERR(0, 306, __pyx_L1_error));

  /* "serialmanager.pyx":307
 * 
 * 	def test_for_stop(self):
 * 		cdef char* stopcom  = b'<stop>'             # <<<<<<<<<<<<<<
 * 		ret = self.tell_to_stop(stopcom)
 * 		return ret
 */
  __Pyx_TraceLine(307,0,__PYX_ERR(0, 307, __pyx_L1_error))
  __pyx_v_stopcom = ((char *)"<stop>");

  /* "serialmanager.pyx":308
 * 	def test_for_stop(self):
 * 		cdef char* stopcom  = b'<stop>'
 * 		ret = self.tell_to_stop(stopcom)             # <<<<<<<<<<<<<<
 * 		return ret
 * 
 */
  __Pyx_TraceLine(308,0,__PYX_ERR(0, 308, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->tell_to_stop(__pyx_v_self, __pyx_v_stopcom); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 308, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_v_ret = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":309
 * 		cdef char* stopcom  = b'<stop>'
 * 		ret = self.tell_to_stop(stopcom)
 * 		return ret             # <<<<<<<<<<<<<<
 * 
 * 	def prendiprendi(self):
 */
  __Pyx_TraceLine(309,0,__PYX_ERR(0, 309, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_ret);
  __pyx_r = __pyx_v_ret;
  goto __pyx_L0;

  /* "serialmanager.pyx":306
 * #			self.tell_to_stop(stopcom)
 * 
 * 	def test_for_stop(self):             # <<<<<<<<<<<<<<
 * 		cdef char* stopcom  = b'<stop>'
 * 		ret = self.tell_to_stop(stopcom)
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.test_for_stop", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_ret);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":311
 * 		return ret
 * 
 * 	def prendiprendi(self):             # <<<<<<<<<<<<<<
 * 		self.prendi()
 * 
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_29prendiprendi(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_28prendiprendi[] = "SerialManager.prendiprendi(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_29prendiprendi(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("prendiprendi (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_28prendiprendi(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_28prendiprendi(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("prendiprendi", 0);
  __Pyx_TraceCall("prendiprendi", __pyx_f[0], 311, 0, __PYX_ERR(0, 311, __pyx_L1_error));

  /* "serialmanager.pyx":312
 * 
 * 	def prendiprendi(self):
 * 		self.prendi()             # <<<<<<<<<<<<<<
 * 
 * 	def send_reset(self):
 */
  __Pyx_TraceLine(312,0,__PYX_ERR(0, 312, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->prendi(__pyx_v_self); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 312, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":311
 * 		return ret
 * 
 * 	def prendiprendi(self):             # <<<<<<<<<<<<<<
 * 		self.prendi()
 * 
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.prendiprendi", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":314
 * 		self.prendi()
 * 
 * 	def send_reset(self):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* result = b'<reset>'
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_31send_reset(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_30send_reset[] = "SerialManager.send_reset(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_31send_reset(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("send_reset (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_30send_reset(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_30send_reset(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  int __pyx_v_waitingForReply;
  char *__pyx_v_result;
  char *__pyx_v_dataReceived;
  CYTHON_UNUSED PyObject *__pyx_v_startledtime = NULL;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  int __pyx_t_4;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("send_reset", 0);
  __Pyx_TraceCall("send_reset", __pyx_f[0], 314, 0, __PYX_ERR(0, 314, __pyx_L1_error));

  /* "serialmanager.pyx":315
 * 
 * 	def send_reset(self):
 * 		cdef bint waitingForReply = False             # <<<<<<<<<<<<<<
 * 		cdef char* result = b'<reset>'
 * 		cdef char* dataReceived
 */
  __Pyx_TraceLine(315,0,__PYX_ERR(0, 315, __pyx_L1_error))
  __pyx_v_waitingForReply = 0;

  /* "serialmanager.pyx":316
 * 	def send_reset(self):
 * 		cdef bint waitingForReply = False
 * 		cdef char* result = b'<reset>'             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived
 * 		startledtime = time.time()
 */
  __Pyx_TraceLine(316,0,__PYX_ERR(0, 316, __pyx_L1_error))
  __pyx_v_result = ((char *)"<reset>");

  /* "serialmanager.pyx":318
 * 		cdef char* result = b'<reset>'
 * 		cdef char* dataReceived
 * 		startledtime = time.time()             # <<<<<<<<<<<<<<
 * 		if waitingForReply == False:
 * 			self.send_to_arduino(result)
 */
  __Pyx_TraceLine(318,0,__PYX_ERR(0, 318, __pyx_L1_error))
  __Pyx_GetModuleGlobalName(__pyx_t_2, __pyx_n_s_time); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 318, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_time); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 318, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_2 = NULL;
  if (CYTHON_UNPACK_METHODS && unlikely(PyMethod_Check(__pyx_t_3))) {
    __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_3);
    if (likely(__pyx_t_2)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
      __Pyx_INCREF(__pyx_t_2);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_3, function);
    }
  }
  __pyx_t_1 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 318, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_v_startledtime = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":319
 * 		cdef char* dataReceived
 * 		startledtime = time.time()
 * 		if waitingForReply == False:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))
 */
  __Pyx_TraceLine(319,0,__PYX_ERR(0, 319, __pyx_L1_error))
  __pyx_t_4 = ((__pyx_v_waitingForReply == 0) != 0);
  if (__pyx_t_4) {

    /* "serialmanager.pyx":320
 * 		startledtime = time.time()
 * 		if waitingForReply == False:
 * 			self.send_to_arduino(result)             # <<<<<<<<<<<<<<
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True
 */
    __Pyx_TraceLine(320,0,__PYX_ERR(0, 320, __pyx_L1_error))
    __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_result); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 320, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

    /* "serialmanager.pyx":321
 * 		if waitingForReply == False:
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))             # <<<<<<<<<<<<<<
 * 			waitingForReply = True
 * 		if waitingForReply == True:
 */
    __Pyx_TraceLine(321,0,__PYX_ERR(0, 321, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_RASPI, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 321, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_result); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 321, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_1 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 321, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 321, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":322
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True             # <<<<<<<<<<<<<<
 * 		if waitingForReply == True:
 * 			while self.ser.inWaiting() == 0:
 */
    __Pyx_TraceLine(322,0,__PYX_ERR(0, 322, __pyx_L1_error))
    __pyx_v_waitingForReply = 1;

    /* "serialmanager.pyx":319
 * 		cdef char* dataReceived
 * 		startledtime = time.time()
 * 		if waitingForReply == False:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))
 */
  }

  /* "serialmanager.pyx":323
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True
 * 		if waitingForReply == True:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(323,0,__PYX_ERR(0, 323, __pyx_L1_error))
  __pyx_t_4 = ((__pyx_v_waitingForReply == 1) != 0);
  if (__pyx_t_4) {

    /* "serialmanager.pyx":324
 * 			waitingForReply = True
 * 		if waitingForReply == True:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(324,0,__PYX_ERR(0, 324, __pyx_L1_error))
    while (1) {
      __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 324, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __pyx_t_2 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_1))) {
        __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_1);
        if (likely(__pyx_t_2)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_1);
          __Pyx_INCREF(__pyx_t_2);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_1, function);
        }
      }
      __pyx_t_3 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_1, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_1);
      __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 324, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      __pyx_t_1 = __Pyx_PyInt_EqObjC(__pyx_t_3, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 324, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_4 = __Pyx_PyObject_IsTrue(__pyx_t_1); if (unlikely(__pyx_t_4 < 0)) __PYX_ERR(0, 324, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      if (!__pyx_t_4) break;
    }

    /* "serialmanager.pyx":326
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply led Received{}'.format(dataReceived))
 * 
 */
    __Pyx_TraceLine(326,0,__PYX_ERR(0, 326, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":327
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply led Received{}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 
 * 	cdef tell_to_move(self, comm):
 */
    __Pyx_TraceLine(327,0,__PYX_ERR(0, 327, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_led_Received, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 327, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 327, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_1 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 327, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 327, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":323
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True
 * 		if waitingForReply == True:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":314
 * 		self.prendi()
 * 
 * 	def send_reset(self):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* result = b'<reset>'
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("serialmanager.SerialManager.send_reset", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_startledtime);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":329
 * 			print('Reply led Received{}'.format(dataReceived))
 * 
 * 	cdef tell_to_move(self, comm):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_tell_to_move(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_comm) {
  int __pyx_v_waiting_for_reply;
  char *__pyx_v_dataReceived;
  PyObject *__pyx_v_res = NULL;
  double __pyx_v_ans1;
  double __pyx_v_ans2;
  double __pyx_v_ans3;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  char *__pyx_t_2;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  PyObject *__pyx_t_6 = NULL;
  double __pyx_t_7;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("tell_to_move", 0);
  __Pyx_TraceCall("tell_to_move", __pyx_f[0], 329, 0, __PYX_ERR(0, 329, __pyx_L1_error));

  /* "serialmanager.pyx":330
 * 
 * 	cdef tell_to_move(self, comm):
 * 		cdef bint waiting_for_reply = False             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:
 */
  __Pyx_TraceLine(330,0,__PYX_ERR(0, 330, __pyx_L1_error))
  __pyx_v_waiting_for_reply = 0;

  /* "serialmanager.pyx":331
 * 	cdef tell_to_move(self, comm):
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''             # <<<<<<<<<<<<<<
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(comm)
 */
  __Pyx_TraceLine(331,0,__PYX_ERR(0, 331, __pyx_L1_error))
  __pyx_v_dataReceived = ((char *)"");

  /* "serialmanager.pyx":332
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(comm)
 * 			print('Sent from Raspi {}'.format(comm))
 */
  __Pyx_TraceLine(332,0,__PYX_ERR(0, 332, __pyx_L1_error))
  __pyx_t_1 = ((!(__pyx_v_waiting_for_reply != 0)) != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":333
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(comm)             # <<<<<<<<<<<<<<
 * 			print('Sent from Raspi {}'.format(comm))
 * 			waiting_for_reply = True
 */
    __Pyx_TraceLine(333,0,__PYX_ERR(0, 333, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyObject_AsWritableString(__pyx_v_comm); if (unlikely((!__pyx_t_2) && PyErr_Occurred())) __PYX_ERR(0, 333, __pyx_L1_error)
    __pyx_t_3 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 333, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":334
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(comm)
 * 			print('Sent from Raspi {}'.format(comm))             # <<<<<<<<<<<<<<
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 */
    __Pyx_TraceLine(334,0,__PYX_ERR(0, 334, __pyx_L1_error))
    __pyx_t_4 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi_2, __pyx_n_s_format); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 334, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_4))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_4);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_4, function);
      }
    }
    __pyx_t_3 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_5, __pyx_v_comm) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_v_comm);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 334, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_t_4 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 334, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;

    /* "serialmanager.pyx":335
 * 			self.send_to_arduino(comm)
 * 			print('Sent from Raspi {}'.format(comm))
 * 			waiting_for_reply = True             # <<<<<<<<<<<<<<
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:
 */
    __Pyx_TraceLine(335,0,__PYX_ERR(0, 335, __pyx_L1_error))
    __pyx_v_waiting_for_reply = 1;

    /* "serialmanager.pyx":332
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(comm)
 * 			print('Sent from Raspi {}'.format(comm))
 */
  }

  /* "serialmanager.pyx":336
 * 			print('Sent from Raspi {}'.format(comm))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(336,0,__PYX_ERR(0, 336, __pyx_L1_error))
  __pyx_t_1 = (__pyx_v_waiting_for_reply != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":337
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(337,0,__PYX_ERR(0, 337, __pyx_L1_error))
    while (1) {
      __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 337, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __pyx_t_5 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
        __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
        if (likely(__pyx_t_5)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
          __Pyx_INCREF(__pyx_t_5);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_3, function);
        }
      }
      __pyx_t_4 = (__pyx_t_5) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_5) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
      __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
      if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 337, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_4);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_t_4, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 337, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
      __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 337, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      if (!__pyx_t_1) break;
    }

    /* "serialmanager.pyx":339
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply Received {}'.format(dataReceived))
 * 			res = dataReceived.split()
 */
    __Pyx_TraceLine(339,0,__PYX_ERR(0, 339, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":340
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received {}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 			res = dataReceived.split()
 * 			ans1 = float(res[3].decode())
 */
    __Pyx_TraceLine(340,0,__PYX_ERR(0, 340, __pyx_L1_error))
    __pyx_t_4 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_Received, __pyx_n_s_format); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 340, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 340, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __pyx_t_6 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_4))) {
      __pyx_t_6 = PyMethod_GET_SELF(__pyx_t_4);
      if (likely(__pyx_t_6)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
        __Pyx_INCREF(__pyx_t_6);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_4, function);
      }
    }
    __pyx_t_3 = (__pyx_t_6) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_6, __pyx_t_5) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_t_5);
    __Pyx_XDECREF(__pyx_t_6); __pyx_t_6 = 0;
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 340, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_t_4 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 340, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;

    /* "serialmanager.pyx":341
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received {}'.format(dataReceived))
 * 			res = dataReceived.split()             # <<<<<<<<<<<<<<
 * 			ans1 = float(res[3].decode())
 * 			ans2 = float(res[7].decode())
 */
    __Pyx_TraceLine(341,0,__PYX_ERR(0, 341, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 341, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_5 = __Pyx_PyObject_GetAttrStr(__pyx_t_3, __pyx_n_s_split); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 341, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_5))) {
      __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_5);
      if (likely(__pyx_t_3)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_5);
        __Pyx_INCREF(__pyx_t_3);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_5, function);
      }
    }
    __pyx_t_4 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_5);
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 341, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_v_res = __pyx_t_4;
    __pyx_t_4 = 0;

    /* "serialmanager.pyx":342
 * 			print('Reply Received {}'.format(dataReceived))
 * 			res = dataReceived.split()
 * 			ans1 = float(res[3].decode())             # <<<<<<<<<<<<<<
 * 			ans2 = float(res[7].decode())
 * 			ans3 = float(res[11].decode())
 */
    __Pyx_TraceLine(342,0,__PYX_ERR(0, 342, __pyx_L1_error))
    __pyx_t_5 = __Pyx_GetItemInt(__pyx_v_res, 3, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 342, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_5, __pyx_n_s_decode); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 342, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_4 = (__pyx_t_5) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_5) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 342, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_7 = __Pyx_PyObject_AsDouble(__pyx_t_4); if (unlikely(__pyx_t_7 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 342, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_v_ans1 = __pyx_t_7;

    /* "serialmanager.pyx":343
 * 			res = dataReceived.split()
 * 			ans1 = float(res[3].decode())
 * 			ans2 = float(res[7].decode())             # <<<<<<<<<<<<<<
 * 			ans3 = float(res[11].decode())
 * 		return ans1,ans2,ans3
 */
    __Pyx_TraceLine(343,0,__PYX_ERR(0, 343, __pyx_L1_error))
    __pyx_t_3 = __Pyx_GetItemInt(__pyx_v_res, 7, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 343, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_5 = __Pyx_PyObject_GetAttrStr(__pyx_t_3, __pyx_n_s_decode); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 343, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_5))) {
      __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_5);
      if (likely(__pyx_t_3)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_5);
        __Pyx_INCREF(__pyx_t_3);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_5, function);
      }
    }
    __pyx_t_4 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_5);
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 343, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_t_7 = __Pyx_PyObject_AsDouble(__pyx_t_4); if (unlikely(__pyx_t_7 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 343, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_v_ans2 = __pyx_t_7;

    /* "serialmanager.pyx":344
 * 			ans1 = float(res[3].decode())
 * 			ans2 = float(res[7].decode())
 * 			ans3 = float(res[11].decode())             # <<<<<<<<<<<<<<
 * 		return ans1,ans2,ans3
 * 
 */
    __Pyx_TraceLine(344,0,__PYX_ERR(0, 344, __pyx_L1_error))
    __pyx_t_5 = __Pyx_GetItemInt(__pyx_v_res, 11, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 344, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_5, __pyx_n_s_decode); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 344, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_4 = (__pyx_t_5) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_5) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 344, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_7 = __Pyx_PyObject_AsDouble(__pyx_t_4); if (unlikely(__pyx_t_7 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 344, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_v_ans3 = __pyx_t_7;

    /* "serialmanager.pyx":336
 * 			print('Sent from Raspi {}'.format(comm))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":345
 * 			ans2 = float(res[7].decode())
 * 			ans3 = float(res[11].decode())
 * 		return ans1,ans2,ans3             # <<<<<<<<<<<<<<
 * 
 * 	cdef tell_to_move2(self, command):
 */
  __Pyx_TraceLine(345,0,__PYX_ERR(0, 345, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_4 = PyFloat_FromDouble(__pyx_v_ans1); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 345, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_4);
  __pyx_t_3 = PyFloat_FromDouble(__pyx_v_ans2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 345, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_5 = PyFloat_FromDouble(__pyx_v_ans3); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 345, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_5);
  __pyx_t_6 = PyTuple_New(3); if (unlikely(!__pyx_t_6)) __PYX_ERR(0, 345, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_6);
  __Pyx_GIVEREF(__pyx_t_4);
  PyTuple_SET_ITEM(__pyx_t_6, 0, __pyx_t_4);
  __Pyx_GIVEREF(__pyx_t_3);
  PyTuple_SET_ITEM(__pyx_t_6, 1, __pyx_t_3);
  __Pyx_GIVEREF(__pyx_t_5);
  PyTuple_SET_ITEM(__pyx_t_6, 2, __pyx_t_5);
  __pyx_t_4 = 0;
  __pyx_t_3 = 0;
  __pyx_t_5 = 0;
  __pyx_r = __pyx_t_6;
  __pyx_t_6 = 0;
  goto __pyx_L0;

  /* "serialmanager.pyx":329
 * 			print('Reply led Received{}'.format(dataReceived))
 * 
 * 	cdef tell_to_move(self, comm):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_XDECREF(__pyx_t_6);
  __Pyx_AddTraceback("serialmanager.SerialManager.tell_to_move", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_res);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":347
 * 		return ans1,ans2,ans3
 * 
 * 	cdef tell_to_move2(self, command):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_tell_to_move2(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_command) {
  int __pyx_v_waiting_for_reply;
  char *__pyx_v_dataReceived;
  PyObject *__pyx_v_res = NULL;
  double __pyx_v_ans1;
  double __pyx_v_ans2;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  char *__pyx_t_2;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  PyObject *__pyx_t_6 = NULL;
  double __pyx_t_7;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("tell_to_move2", 0);
  __Pyx_TraceCall("tell_to_move2", __pyx_f[0], 347, 0, __PYX_ERR(0, 347, __pyx_L1_error));

  /* "serialmanager.pyx":348
 * 
 * 	cdef tell_to_move2(self, command):
 * 		cdef bint waiting_for_reply = False             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:
 */
  __Pyx_TraceLine(348,0,__PYX_ERR(0, 348, __pyx_L1_error))
  __pyx_v_waiting_for_reply = 0;

  /* "serialmanager.pyx":349
 * 	cdef tell_to_move2(self, command):
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''             # <<<<<<<<<<<<<<
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)
 */
  __Pyx_TraceLine(349,0,__PYX_ERR(0, 349, __pyx_L1_error))
  __pyx_v_dataReceived = ((char *)"");

  /* "serialmanager.pyx":350
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 */
  __Pyx_TraceLine(350,0,__PYX_ERR(0, 350, __pyx_L1_error))
  __pyx_t_1 = ((!(__pyx_v_waiting_for_reply != 0)) != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":351
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)             # <<<<<<<<<<<<<<
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True
 */
    __Pyx_TraceLine(351,0,__PYX_ERR(0, 351, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyObject_AsWritableString(__pyx_v_command); if (unlikely((!__pyx_t_2) && PyErr_Occurred())) __PYX_ERR(0, 351, __pyx_L1_error)
    __pyx_t_3 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 351, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":352
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))             # <<<<<<<<<<<<<<
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 */
    __Pyx_TraceLine(352,0,__PYX_ERR(0, 352, __pyx_L1_error))
    __pyx_t_4 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi_TEST_PRESS, __pyx_n_s_format); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 352, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_4))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_4);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_4, function);
      }
    }
    __pyx_t_3 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_5, __pyx_v_command) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_v_command);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 352, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_t_4 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 352, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;

    /* "serialmanager.pyx":353
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True             # <<<<<<<<<<<<<<
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:
 */
    __Pyx_TraceLine(353,0,__PYX_ERR(0, 353, __pyx_L1_error))
    __pyx_v_waiting_for_reply = 1;

    /* "serialmanager.pyx":350
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(command)
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 */
  }

  /* "serialmanager.pyx":354
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(354,0,__PYX_ERR(0, 354, __pyx_L1_error))
  __pyx_t_1 = (__pyx_v_waiting_for_reply != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":355
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(355,0,__PYX_ERR(0, 355, __pyx_L1_error))
    while (1) {
      __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 355, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __pyx_t_5 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
        __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
        if (likely(__pyx_t_5)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
          __Pyx_INCREF(__pyx_t_5);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_3, function);
        }
      }
      __pyx_t_4 = (__pyx_t_5) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_5) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
      __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
      if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 355, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_4);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_3 = __Pyx_PyInt_EqObjC(__pyx_t_4, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 355, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
      __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_3); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 355, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      if (!__pyx_t_1) break;
    }

    /* "serialmanager.pyx":357
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply Received take one {}'.format(dataReceived))
 * 			res = dataReceived.split()
 */
    __Pyx_TraceLine(357,0,__PYX_ERR(0, 357, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":358
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received take one {}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 			res = dataReceived.split()
 * 			ans1 = float(res[3].decode())
 */
    __Pyx_TraceLine(358,0,__PYX_ERR(0, 358, __pyx_L1_error))
    __pyx_t_4 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_Received_take_one, __pyx_n_s_format); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 358, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 358, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __pyx_t_6 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_4))) {
      __pyx_t_6 = PyMethod_GET_SELF(__pyx_t_4);
      if (likely(__pyx_t_6)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
        __Pyx_INCREF(__pyx_t_6);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_4, function);
      }
    }
    __pyx_t_3 = (__pyx_t_6) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_6, __pyx_t_5) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_t_5);
    __Pyx_XDECREF(__pyx_t_6); __pyx_t_6 = 0;
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 358, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_t_4 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 358, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;

    /* "serialmanager.pyx":359
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received take one {}'.format(dataReceived))
 * 			res = dataReceived.split()             # <<<<<<<<<<<<<<
 * 			ans1 = float(res[3].decode())
 * 			ans2 = float(res[7].decode())
 */
    __Pyx_TraceLine(359,0,__PYX_ERR(0, 359, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 359, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_5 = __Pyx_PyObject_GetAttrStr(__pyx_t_3, __pyx_n_s_split); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 359, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_5))) {
      __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_5);
      if (likely(__pyx_t_3)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_5);
        __Pyx_INCREF(__pyx_t_3);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_5, function);
      }
    }
    __pyx_t_4 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_5);
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 359, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_v_res = __pyx_t_4;
    __pyx_t_4 = 0;

    /* "serialmanager.pyx":360
 * 			print('Reply Received take one {}'.format(dataReceived))
 * 			res = dataReceived.split()
 * 			ans1 = float(res[3].decode())             # <<<<<<<<<<<<<<
 * 			ans2 = float(res[7].decode())
 * 		return ans1,ans2
 */
    __Pyx_TraceLine(360,0,__PYX_ERR(0, 360, __pyx_L1_error))
    __pyx_t_5 = __Pyx_GetItemInt(__pyx_v_res, 3, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 360, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_5, __pyx_n_s_decode); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 360, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_4 = (__pyx_t_5) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_5) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 360, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_7 = __Pyx_PyObject_AsDouble(__pyx_t_4); if (unlikely(__pyx_t_7 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 360, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_v_ans1 = __pyx_t_7;

    /* "serialmanager.pyx":361
 * 			res = dataReceived.split()
 * 			ans1 = float(res[3].decode())
 * 			ans2 = float(res[7].decode())             # <<<<<<<<<<<<<<
 * 		return ans1,ans2
 * 
 */
    __Pyx_TraceLine(361,0,__PYX_ERR(0, 361, __pyx_L1_error))
    __pyx_t_3 = __Pyx_GetItemInt(__pyx_v_res, 7, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 361, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_5 = __Pyx_PyObject_GetAttrStr(__pyx_t_3, __pyx_n_s_decode); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 361, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_5))) {
      __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_5);
      if (likely(__pyx_t_3)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_5);
        __Pyx_INCREF(__pyx_t_3);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_5, function);
      }
    }
    __pyx_t_4 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_5, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_5);
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 361, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_t_7 = __Pyx_PyObject_AsDouble(__pyx_t_4); if (unlikely(__pyx_t_7 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 361, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    __pyx_v_ans2 = __pyx_t_7;

    /* "serialmanager.pyx":354
 * 			print('Sent from Raspi {} TEST PRESS'.format(command))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":362
 * 			ans1 = float(res[3].decode())
 * 			ans2 = float(res[7].decode())
 * 		return ans1,ans2             # <<<<<<<<<<<<<<
 * 
 * 	'''cdef tell_to_move3(self, command):
 */
  __Pyx_TraceLine(362,0,__PYX_ERR(0, 362, __pyx_L1_error))
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_4 = PyFloat_FromDouble(__pyx_v_ans1); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 362, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_4);
  __pyx_t_5 = PyFloat_FromDouble(__pyx_v_ans2); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 362, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_5);
  __pyx_t_3 = PyTuple_New(2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 362, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_GIVEREF(__pyx_t_4);
  PyTuple_SET_ITEM(__pyx_t_3, 0, __pyx_t_4);
  __Pyx_GIVEREF(__pyx_t_5);
  PyTuple_SET_ITEM(__pyx_t_3, 1, __pyx_t_5);
  __pyx_t_4 = 0;
  __pyx_t_5 = 0;
  __pyx_r = __pyx_t_3;
  __pyx_t_3 = 0;
  goto __pyx_L0;

  /* "serialmanager.pyx":347
 * 		return ans1,ans2,ans3
 * 
 * 	cdef tell_to_move2(self, command):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_XDECREF(__pyx_t_6);
  __Pyx_AddTraceback("serialmanager.SerialManager.tell_to_move2", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_res);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":389
 * 		return ans1,ans2,ans3,ans4'''
 * 
 * 	def gamepad2(self,move_comm,strafe,forward):             # <<<<<<<<<<<<<<
 * 		'''without angular'''
 * 		print("---> ENTER GAMEPAD2")
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_33gamepad2(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_32gamepad2[] = "SerialManager.gamepad2(self, move_comm, strafe, forward)\nwithout angular";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_33gamepad2(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_v_move_comm = 0;
  PyObject *__pyx_v_strafe = 0;
  PyObject *__pyx_v_forward = 0;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("gamepad2 (wrapper)", 0);
  {
    static PyObject **__pyx_pyargnames[] = {&__pyx_n_s_move_comm,&__pyx_n_s_strafe,&__pyx_n_s_forward,0};
    PyObject* values[3] = {0,0,0};
    if (unlikely(__pyx_kwds)) {
      Py_ssize_t kw_args;
      const Py_ssize_t pos_args = PyTuple_GET_SIZE(__pyx_args);
      switch (pos_args) {
        case  3: values[2] = PyTuple_GET_ITEM(__pyx_args, 2);
        CYTHON_FALLTHROUGH;
        case  2: values[1] = PyTuple_GET_ITEM(__pyx_args, 1);
        CYTHON_FALLTHROUGH;
        case  1: values[0] = PyTuple_GET_ITEM(__pyx_args, 0);
        CYTHON_FALLTHROUGH;
        case  0: break;
        default: goto __pyx_L5_argtuple_error;
      }
      kw_args = PyDict_Size(__pyx_kwds);
      switch (pos_args) {
        case  0:
        if (likely((values[0] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_move_comm)) != 0)) kw_args--;
        else goto __pyx_L5_argtuple_error;
        CYTHON_FALLTHROUGH;
        case  1:
        if (likely((values[1] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_strafe)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("gamepad2", 1, 3, 3, 1); __PYX_ERR(0, 389, __pyx_L3_error)
        }
        CYTHON_FALLTHROUGH;
        case  2:
        if (likely((values[2] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_forward)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("gamepad2", 1, 3, 3, 2); __PYX_ERR(0, 389, __pyx_L3_error)
        }
      }
      if (unlikely(kw_args > 0)) {
        if (unlikely(__Pyx_ParseOptionalKeywords(__pyx_kwds, __pyx_pyargnames, 0, values, pos_args, "gamepad2") < 0)) __PYX_ERR(0, 389, __pyx_L3_error)
      }
    } else if (PyTuple_GET_SIZE(__pyx_args) != 3) {
      goto __pyx_L5_argtuple_error;
    } else {
      values[0] = PyTuple_GET_ITEM(__pyx_args, 0);
      values[1] = PyTuple_GET_ITEM(__pyx_args, 1);
      values[2] = PyTuple_GET_ITEM(__pyx_args, 2);
    }
    __pyx_v_move_comm = values[0];
    __pyx_v_strafe = values[1];
    __pyx_v_forward = values[2];
  }
  goto __pyx_L4_argument_unpacking_done;
  __pyx_L5_argtuple_error:;
  __Pyx_RaiseArgtupleInvalid("gamepad2", 1, 3, 3, PyTuple_GET_SIZE(__pyx_args)); __PYX_ERR(0, 389, __pyx_L3_error)
  __pyx_L3_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.gamepad2", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __Pyx_RefNannyFinishContext();
  return NULL;
  __pyx_L4_argument_unpacking_done:;
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_32gamepad2(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), __pyx_v_move_comm, __pyx_v_strafe, __pyx_v_forward);

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_32gamepad2(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_move_comm, PyObject *__pyx_v_strafe, PyObject *__pyx_v_forward) {
  PyObject *__pyx_v_end = NULL;
  PyObject *__pyx_v_values = NULL;
  PyObject *__pyx_v_strin = NULL;
  PyObject *__pyx_v_addend = NULL;
  PyObject *__pyx_v_command_to_send = NULL;
  PyObject *__pyx_v_ans1 = NULL;
  PyObject *__pyx_v_ans2 = NULL;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_t_2;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  PyObject *(*__pyx_t_6)(PyObject *);
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("gamepad2", 0);
  __Pyx_TraceCall("gamepad2", __pyx_f[0], 389, 0, __PYX_ERR(0, 389, __pyx_L1_error));
  __Pyx_INCREF(__pyx_v_move_comm);

  /* "serialmanager.pyx":391
 * 	def gamepad2(self,move_comm,strafe,forward):
 * 		'''without angular'''
 * 		print("---> ENTER GAMEPAD2")             # <<<<<<<<<<<<<<
 * 		end = '>'
 * 		if move_comm == "gostra":
 */
  __Pyx_TraceLine(391,0,__PYX_ERR(0, 391, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_Call(__pyx_builtin_print, __pyx_tuple__7, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 391, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":392
 * 		'''without angular'''
 * 		print("---> ENTER GAMEPAD2")
 * 		end = '>'             # <<<<<<<<<<<<<<
 * 		if move_comm == "gostra":
 * 			move_comm = '<gostra'
 */
  __Pyx_TraceLine(392,0,__PYX_ERR(0, 392, __pyx_L1_error))
  __Pyx_INCREF(__pyx_kp_u__5);
  __pyx_v_end = __pyx_kp_u__5;

  /* "serialmanager.pyx":393
 * 		print("---> ENTER GAMEPAD2")
 * 		end = '>'
 * 		if move_comm == "gostra":             # <<<<<<<<<<<<<<
 * 			move_comm = '<gostra'
 * 		elif move_comm == "golef":
 */
  __Pyx_TraceLine(393,0,__PYX_ERR(0, 393, __pyx_L1_error))
  __pyx_t_2 = (__Pyx_PyUnicode_Equals(__pyx_v_move_comm, __pyx_n_u_gostra, Py_EQ)); if (unlikely(__pyx_t_2 < 0)) __PYX_ERR(0, 393, __pyx_L1_error)
  if (__pyx_t_2) {

    /* "serialmanager.pyx":394
 * 		end = '>'
 * 		if move_comm == "gostra":
 * 			move_comm = '<gostra'             # <<<<<<<<<<<<<<
 * 		elif move_comm == "golef":
 * 			move_comm = '<golef'
 */
    __Pyx_TraceLine(394,0,__PYX_ERR(0, 394, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_u_gostra_2);
    __Pyx_DECREF_SET(__pyx_v_move_comm, __pyx_kp_u_gostra_2);

    /* "serialmanager.pyx":393
 * 		print("---> ENTER GAMEPAD2")
 * 		end = '>'
 * 		if move_comm == "gostra":             # <<<<<<<<<<<<<<
 * 			move_comm = '<gostra'
 * 		elif move_comm == "golef":
 */
    goto __pyx_L3;
  }

  /* "serialmanager.pyx":395
 * 		if move_comm == "gostra":
 * 			move_comm = '<gostra'
 * 		elif move_comm == "golef":             # <<<<<<<<<<<<<<
 * 			move_comm = '<golef'
 * 		elif move_comm == "gorig":
 */
  __Pyx_TraceLine(395,0,__PYX_ERR(0, 395, __pyx_L1_error))
  __pyx_t_2 = (__Pyx_PyUnicode_Equals(__pyx_v_move_comm, __pyx_n_u_golef, Py_EQ)); if (unlikely(__pyx_t_2 < 0)) __PYX_ERR(0, 395, __pyx_L1_error)
  if (__pyx_t_2) {

    /* "serialmanager.pyx":396
 * 			move_comm = '<gostra'
 * 		elif move_comm == "golef":
 * 			move_comm = '<golef'             # <<<<<<<<<<<<<<
 * 		elif move_comm == "gorig":
 * 			move_comm = '<gorig'
 */
    __Pyx_TraceLine(396,0,__PYX_ERR(0, 396, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_u_golef_2);
    __Pyx_DECREF_SET(__pyx_v_move_comm, __pyx_kp_u_golef_2);

    /* "serialmanager.pyx":395
 * 		if move_comm == "gostra":
 * 			move_comm = '<gostra'
 * 		elif move_comm == "golef":             # <<<<<<<<<<<<<<
 * 			move_comm = '<golef'
 * 		elif move_comm == "gorig":
 */
    goto __pyx_L3;
  }

  /* "serialmanager.pyx":397
 * 		elif move_comm == "golef":
 * 			move_comm = '<golef'
 * 		elif move_comm == "gorig":             # <<<<<<<<<<<<<<
 * 			move_comm = '<gorig'
 * 		elif move_comm == "gobac":
 */
  __Pyx_TraceLine(397,0,__PYX_ERR(0, 397, __pyx_L1_error))
  __pyx_t_2 = (__Pyx_PyUnicode_Equals(__pyx_v_move_comm, __pyx_n_u_gorig, Py_EQ)); if (unlikely(__pyx_t_2 < 0)) __PYX_ERR(0, 397, __pyx_L1_error)
  if (__pyx_t_2) {

    /* "serialmanager.pyx":398
 * 			move_comm = '<golef'
 * 		elif move_comm == "gorig":
 * 			move_comm = '<gorig'             # <<<<<<<<<<<<<<
 * 		elif move_comm == "gobac":
 * 			move_comm = '<gobac'
 */
    __Pyx_TraceLine(398,0,__PYX_ERR(0, 398, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_u_gorig_2);
    __Pyx_DECREF_SET(__pyx_v_move_comm, __pyx_kp_u_gorig_2);

    /* "serialmanager.pyx":397
 * 		elif move_comm == "golef":
 * 			move_comm = '<golef'
 * 		elif move_comm == "gorig":             # <<<<<<<<<<<<<<
 * 			move_comm = '<gorig'
 * 		elif move_comm == "gobac":
 */
    goto __pyx_L3;
  }

  /* "serialmanager.pyx":399
 * 		elif move_comm == "gorig":
 * 			move_comm = '<gorig'
 * 		elif move_comm == "gobac":             # <<<<<<<<<<<<<<
 * 			move_comm = '<gobac'
 * 		elif move_comm == "joystop":
 */
  __Pyx_TraceLine(399,0,__PYX_ERR(0, 399, __pyx_L1_error))
  __pyx_t_2 = (__Pyx_PyUnicode_Equals(__pyx_v_move_comm, __pyx_n_u_gobac, Py_EQ)); if (unlikely(__pyx_t_2 < 0)) __PYX_ERR(0, 399, __pyx_L1_error)
  if (__pyx_t_2) {

    /* "serialmanager.pyx":400
 * 			move_comm = '<gorig'
 * 		elif move_comm == "gobac":
 * 			move_comm = '<gobac'             # <<<<<<<<<<<<<<
 * 		elif move_comm == "joystop":
 * 			move_comm = '<joystop'
 */
    __Pyx_TraceLine(400,0,__PYX_ERR(0, 400, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_u_gobac_2);
    __Pyx_DECREF_SET(__pyx_v_move_comm, __pyx_kp_u_gobac_2);

    /* "serialmanager.pyx":399
 * 		elif move_comm == "gorig":
 * 			move_comm = '<gorig'
 * 		elif move_comm == "gobac":             # <<<<<<<<<<<<<<
 * 			move_comm = '<gobac'
 * 		elif move_comm == "joystop":
 */
    goto __pyx_L3;
  }

  /* "serialmanager.pyx":401
 * 		elif move_comm == "gobac":
 * 			move_comm = '<gobac'
 * 		elif move_comm == "joystop":             # <<<<<<<<<<<<<<
 * 			move_comm = '<joystop'
 * 		elif move_comm == "simplygo2":
 */
  __Pyx_TraceLine(401,0,__PYX_ERR(0, 401, __pyx_L1_error))
  __pyx_t_2 = (__Pyx_PyUnicode_Equals(__pyx_v_move_comm, __pyx_n_u_joystop, Py_EQ)); if (unlikely(__pyx_t_2 < 0)) __PYX_ERR(0, 401, __pyx_L1_error)
  if (__pyx_t_2) {

    /* "serialmanager.pyx":402
 * 			move_comm = '<gobac'
 * 		elif move_comm == "joystop":
 * 			move_comm = '<joystop'             # <<<<<<<<<<<<<<
 * 		elif move_comm == "simplygo2":
 * 			move_comm = '<simplygo2'
 */
    __Pyx_TraceLine(402,0,__PYX_ERR(0, 402, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_u_joystop_2);
    __Pyx_DECREF_SET(__pyx_v_move_comm, __pyx_kp_u_joystop_2);

    /* "serialmanager.pyx":401
 * 		elif move_comm == "gobac":
 * 			move_comm = '<gobac'
 * 		elif move_comm == "joystop":             # <<<<<<<<<<<<<<
 * 			move_comm = '<joystop'
 * 		elif move_comm == "simplygo2":
 */
    goto __pyx_L3;
  }

  /* "serialmanager.pyx":403
 * 		elif move_comm == "joystop":
 * 			move_comm = '<joystop'
 * 		elif move_comm == "simplygo2":             # <<<<<<<<<<<<<<
 * 			move_comm = '<simplygo2'
 * 
 */
  __Pyx_TraceLine(403,0,__PYX_ERR(0, 403, __pyx_L1_error))
  __pyx_t_2 = (__Pyx_PyUnicode_Equals(__pyx_v_move_comm, __pyx_n_u_simplygo2, Py_EQ)); if (unlikely(__pyx_t_2 < 0)) __PYX_ERR(0, 403, __pyx_L1_error)
  if (__pyx_t_2) {

    /* "serialmanager.pyx":404
 * 			move_comm = '<joystop'
 * 		elif move_comm == "simplygo2":
 * 			move_comm = '<simplygo2'             # <<<<<<<<<<<<<<
 * 
 * 		values = [move_comm,strafe,forward]
 */
    __Pyx_TraceLine(404,0,__PYX_ERR(0, 404, __pyx_L1_error))
    __Pyx_INCREF(__pyx_kp_u_simplygo2_2);
    __Pyx_DECREF_SET(__pyx_v_move_comm, __pyx_kp_u_simplygo2_2);

    /* "serialmanager.pyx":403
 * 		elif move_comm == "joystop":
 * 			move_comm = '<joystop'
 * 		elif move_comm == "simplygo2":             # <<<<<<<<<<<<<<
 * 			move_comm = '<simplygo2'
 * 
 */
  }
  __pyx_L3:;

  /* "serialmanager.pyx":406
 * 			move_comm = '<simplygo2'
 * 
 * 		values = [move_comm,strafe,forward]             # <<<<<<<<<<<<<<
 * 		strin = ','.join(map(str, values))
 * 		addend = [strin,end]
 */
  __Pyx_TraceLine(406,0,__PYX_ERR(0, 406, __pyx_L1_error))
  __pyx_t_1 = PyList_New(3); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 406, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_INCREF(__pyx_v_move_comm);
  __Pyx_GIVEREF(__pyx_v_move_comm);
  PyList_SET_ITEM(__pyx_t_1, 0, __pyx_v_move_comm);
  __Pyx_INCREF(__pyx_v_strafe);
  __Pyx_GIVEREF(__pyx_v_strafe);
  PyList_SET_ITEM(__pyx_t_1, 1, __pyx_v_strafe);
  __Pyx_INCREF(__pyx_v_forward);
  __Pyx_GIVEREF(__pyx_v_forward);
  PyList_SET_ITEM(__pyx_t_1, 2, __pyx_v_forward);
  __pyx_v_values = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":407
 * 
 * 		values = [move_comm,strafe,forward]
 * 		strin = ','.join(map(str, values))             # <<<<<<<<<<<<<<
 * 		addend = [strin,end]
 * 		command_to_send = ''.join(map(str, addend))
 */
  __Pyx_TraceLine(407,0,__PYX_ERR(0, 407, __pyx_L1_error))
  __pyx_t_1 = PyTuple_New(2); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 407, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_INCREF(((PyObject *)(&PyUnicode_Type)));
  __Pyx_GIVEREF(((PyObject *)(&PyUnicode_Type)));
  PyTuple_SET_ITEM(__pyx_t_1, 0, ((PyObject *)(&PyUnicode_Type)));
  __Pyx_INCREF(__pyx_v_values);
  __Pyx_GIVEREF(__pyx_v_values);
  PyTuple_SET_ITEM(__pyx_t_1, 1, __pyx_v_values);
  __pyx_t_3 = __Pyx_PyObject_Call(__pyx_builtin_map, __pyx_t_1, NULL); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 407, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_1 = PyUnicode_Join(__pyx_kp_u__6, __pyx_t_3); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 407, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_v_strin = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":408
 * 		values = [move_comm,strafe,forward]
 * 		strin = ','.join(map(str, values))
 * 		addend = [strin,end]             # <<<<<<<<<<<<<<
 * 		command_to_send = ''.join(map(str, addend))
 * 		print("command to send is {}".format(command_to_send))
 */
  __Pyx_TraceLine(408,0,__PYX_ERR(0, 408, __pyx_L1_error))
  __pyx_t_1 = PyList_New(2); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 408, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_INCREF(__pyx_v_strin);
  __Pyx_GIVEREF(__pyx_v_strin);
  PyList_SET_ITEM(__pyx_t_1, 0, __pyx_v_strin);
  __Pyx_INCREF(__pyx_v_end);
  __Pyx_GIVEREF(__pyx_v_end);
  PyList_SET_ITEM(__pyx_t_1, 1, __pyx_v_end);
  __pyx_v_addend = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":409
 * 		strin = ','.join(map(str, values))
 * 		addend = [strin,end]
 * 		command_to_send = ''.join(map(str, addend))             # <<<<<<<<<<<<<<
 * 		print("command to send is {}".format(command_to_send))
 * 		command_to_send = command_to_send.encode('UTF-8', 'strict')
 */
  __Pyx_TraceLine(409,0,__PYX_ERR(0, 409, __pyx_L1_error))
  __pyx_t_1 = PyTuple_New(2); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 409, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_INCREF(((PyObject *)(&PyUnicode_Type)));
  __Pyx_GIVEREF(((PyObject *)(&PyUnicode_Type)));
  PyTuple_SET_ITEM(__pyx_t_1, 0, ((PyObject *)(&PyUnicode_Type)));
  __Pyx_INCREF(__pyx_v_addend);
  __Pyx_GIVEREF(__pyx_v_addend);
  PyTuple_SET_ITEM(__pyx_t_1, 1, __pyx_v_addend);
  __pyx_t_3 = __Pyx_PyObject_Call(__pyx_builtin_map, __pyx_t_1, NULL); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 409, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_1 = PyUnicode_Join(__pyx_kp_u__3, __pyx_t_3); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 409, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_v_command_to_send = __pyx_t_1;
  __pyx_t_1 = 0;

  /* "serialmanager.pyx":410
 * 		addend = [strin,end]
 * 		command_to_send = ''.join(map(str, addend))
 * 		print("command to send is {}".format(command_to_send))             # <<<<<<<<<<<<<<
 * 		command_to_send = command_to_send.encode('UTF-8', 'strict')
 * 		#cdef char* command = command_to_send #per passare char faccio prova coi cdef
 */
  __Pyx_TraceLine(410,0,__PYX_ERR(0, 410, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_command_to_send_is, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 410, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_4 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
    __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_3);
    if (likely(__pyx_t_4)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
      __Pyx_INCREF(__pyx_t_4);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_3, function);
    }
  }
  __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_4, __pyx_v_command_to_send) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_v_command_to_send);
  __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 410, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 410, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

  /* "serialmanager.pyx":411
 * 		command_to_send = ''.join(map(str, addend))
 * 		print("command to send is {}".format(command_to_send))
 * 		command_to_send = command_to_send.encode('UTF-8', 'strict')             # <<<<<<<<<<<<<<
 * 		#cdef char* command = command_to_send #per passare char faccio prova coi cdef
 * 
 */
  __Pyx_TraceLine(411,0,__PYX_ERR(0, 411, __pyx_L1_error))
  if (unlikely(__pyx_v_command_to_send == Py_None)) {
    PyErr_Format(PyExc_AttributeError, "'NoneType' object has no attribute '%.30s'", "encode");
    __PYX_ERR(0, 411, __pyx_L1_error)
  }
  __pyx_t_3 = PyUnicode_AsUTF8String(__pyx_v_command_to_send); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 411, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF_SET(__pyx_v_command_to_send, __pyx_t_3);
  __pyx_t_3 = 0;

  /* "serialmanager.pyx":416
 * 		#gare.play_audio(choosen_audio)
 * 
 * 		ans1, ans2 = self.tell_to_move2(command_to_send)             # <<<<<<<<<<<<<<
 * 		print("ans1 is {}".format(ans1))
 * 		print("ans2 is {}".format(ans2))
 */
  __Pyx_TraceLine(416,0,__PYX_ERR(0, 416, __pyx_L1_error))
  __pyx_t_3 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->tell_to_move2(__pyx_v_self, __pyx_v_command_to_send); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 416, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  if ((likely(PyTuple_CheckExact(__pyx_t_3))) || (PyList_CheckExact(__pyx_t_3))) {
    PyObject* sequence = __pyx_t_3;
    Py_ssize_t size = __Pyx_PySequence_SIZE(sequence);
    if (unlikely(size != 2)) {
      if (size > 2) __Pyx_RaiseTooManyValuesError(2);
      else if (size >= 0) __Pyx_RaiseNeedMoreValuesError(size);
      __PYX_ERR(0, 416, __pyx_L1_error)
    }
    #if CYTHON_ASSUME_SAFE_MACROS && !CYTHON_AVOID_BORROWED_REFS
    if (likely(PyTuple_CheckExact(sequence))) {
      __pyx_t_1 = PyTuple_GET_ITEM(sequence, 0); 
      __pyx_t_4 = PyTuple_GET_ITEM(sequence, 1); 
    } else {
      __pyx_t_1 = PyList_GET_ITEM(sequence, 0); 
      __pyx_t_4 = PyList_GET_ITEM(sequence, 1); 
    }
    __Pyx_INCREF(__pyx_t_1);
    __Pyx_INCREF(__pyx_t_4);
    #else
    __pyx_t_1 = PySequence_ITEM(sequence, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 416, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __pyx_t_4 = PySequence_ITEM(sequence, 1); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 416, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    #endif
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  } else {
    Py_ssize_t index = -1;
    __pyx_t_5 = PyObject_GetIter(__pyx_t_3); if (unlikely(!__pyx_t_5)) __PYX_ERR(0, 416, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_5);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_6 = Py_TYPE(__pyx_t_5)->tp_iternext;
    index = 0; __pyx_t_1 = __pyx_t_6(__pyx_t_5); if (unlikely(!__pyx_t_1)) goto __pyx_L4_unpacking_failed;
    __Pyx_GOTREF(__pyx_t_1);
    index = 1; __pyx_t_4 = __pyx_t_6(__pyx_t_5); if (unlikely(!__pyx_t_4)) goto __pyx_L4_unpacking_failed;
    __Pyx_GOTREF(__pyx_t_4);
    if (__Pyx_IternextUnpackEndCheck(__pyx_t_6(__pyx_t_5), 2) < 0) __PYX_ERR(0, 416, __pyx_L1_error)
    __pyx_t_6 = NULL;
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    goto __pyx_L5_unpacking_done;
    __pyx_L4_unpacking_failed:;
    __Pyx_DECREF(__pyx_t_5); __pyx_t_5 = 0;
    __pyx_t_6 = NULL;
    if (__Pyx_IterFinish() == 0) __Pyx_RaiseNeedMoreValuesError(index);
    __PYX_ERR(0, 416, __pyx_L1_error)
    __pyx_L5_unpacking_done:;
  }
  __pyx_v_ans1 = __pyx_t_1;
  __pyx_t_1 = 0;
  __pyx_v_ans2 = __pyx_t_4;
  __pyx_t_4 = 0;

  /* "serialmanager.pyx":417
 * 
 * 		ans1, ans2 = self.tell_to_move2(command_to_send)
 * 		print("ans1 is {}".format(ans1))             # <<<<<<<<<<<<<<
 * 		print("ans2 is {}".format(ans2))
 * 
 */
  __Pyx_TraceLine(417,0,__PYX_ERR(0, 417, __pyx_L1_error))
  __pyx_t_4 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_ans1_is, __pyx_n_s_format); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 417, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_4);
  __pyx_t_1 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_4))) {
    __pyx_t_1 = PyMethod_GET_SELF(__pyx_t_4);
    if (likely(__pyx_t_1)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_4);
      __Pyx_INCREF(__pyx_t_1);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_4, function);
    }
  }
  __pyx_t_3 = (__pyx_t_1) ? __Pyx_PyObject_Call2Args(__pyx_t_4, __pyx_t_1, __pyx_v_ans1) : __Pyx_PyObject_CallOneArg(__pyx_t_4, __pyx_v_ans1);
  __Pyx_XDECREF(__pyx_t_1); __pyx_t_1 = 0;
  if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 417, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
  __pyx_t_4 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_3); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 417, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_4);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;

  /* "serialmanager.pyx":418
 * 		ans1, ans2 = self.tell_to_move2(command_to_send)
 * 		print("ans1 is {}".format(ans1))
 * 		print("ans2 is {}".format(ans2))             # <<<<<<<<<<<<<<
 * 
 * 	cdef cgamepad(self, char* command, float strafe, float forward, float angular):
 */
  __Pyx_TraceLine(418,0,__PYX_ERR(0, 418, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_ans2_is, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 418, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_1 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
    __pyx_t_1 = PyMethod_GET_SELF(__pyx_t_3);
    if (likely(__pyx_t_1)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
      __Pyx_INCREF(__pyx_t_1);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_3, function);
    }
  }
  __pyx_t_4 = (__pyx_t_1) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_1, __pyx_v_ans2) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_v_ans2);
  __Pyx_XDECREF(__pyx_t_1); __pyx_t_1 = 0;
  if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 418, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_4);
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_4); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 418, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

  /* "serialmanager.pyx":389
 * 		return ans1,ans2,ans3,ans4'''
 * 
 * 	def gamepad2(self,move_comm,strafe,forward):             # <<<<<<<<<<<<<<
 * 		'''without angular'''
 * 		print("---> ENTER GAMEPAD2")
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("serialmanager.SerialManager.gamepad2", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_end);
  __Pyx_XDECREF(__pyx_v_values);
  __Pyx_XDECREF(__pyx_v_strin);
  __Pyx_XDECREF(__pyx_v_addend);
  __Pyx_XDECREF(__pyx_v_command_to_send);
  __Pyx_XDECREF(__pyx_v_ans1);
  __Pyx_XDECREF(__pyx_v_ans2);
  __Pyx_XDECREF(__pyx_v_move_comm);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":420
 * 		print("ans2 is {}".format(ans2))
 * 
 * 	cdef cgamepad(self, char* command, float strafe, float forward, float angular):             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_cgamepad(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, char *__pyx_v_command, float __pyx_v_strafe, float __pyx_v_forward, float __pyx_v_angular) {
  char *__pyx_v_dataReceived;
  int __pyx_v_waiting_for_reply;
  char *__pyx_v_comma;
  char *__pyx_v_command_to_send;
  PyObject *__pyx_v_res = NULL;
  double __pyx_v_ans1;
  double __pyx_v_ans2;
  double __pyx_v_ans3;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  int __pyx_t_5;
  double __pyx_t_6;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("cgamepad", 0);
  __Pyx_TraceCall("cgamepad", __pyx_f[0], 420, 0, __PYX_ERR(0, 420, __pyx_L1_error));

  /* "serialmanager.pyx":421
 * 
 * 	cdef cgamepad(self, char* command, float strafe, float forward, float angular):
 * 		cdef char* dataReceived = ''             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		comma = sm.createMsg(command, strafe, forward, angular);
 */
  __Pyx_TraceLine(421,0,__PYX_ERR(0, 421, __pyx_L1_error))
  __pyx_v_dataReceived = ((char *)"");

  /* "serialmanager.pyx":422
 * 	cdef cgamepad(self, char* command, float strafe, float forward, float angular):
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False             # <<<<<<<<<<<<<<
 * 		comma = sm.createMsg(command, strafe, forward, angular);
 * 		cdef char * command_to_send= <char * >malloc(60)
 */
  __Pyx_TraceLine(422,0,__PYX_ERR(0, 422, __pyx_L1_error))
  __pyx_v_waiting_for_reply = 0;

  /* "serialmanager.pyx":423
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False
 * 		comma = sm.createMsg(command, strafe, forward, angular);             # <<<<<<<<<<<<<<
 * 		cdef char * command_to_send= <char * >malloc(60)
 * 		strcpy(command_to_send,comma)
 */
  __Pyx_TraceLine(423,0,__PYX_ERR(0, 423, __pyx_L1_error))
  __pyx_v_comma = createMsg(__pyx_v_command, __pyx_v_strafe, __pyx_v_forward, __pyx_v_angular);

  /* "serialmanager.pyx":424
 * 		cdef bint waiting_for_reply = False
 * 		comma = sm.createMsg(command, strafe, forward, angular);
 * 		cdef char * command_to_send= <char * >malloc(60)             # <<<<<<<<<<<<<<
 * 		strcpy(command_to_send,comma)
 * 		print("command to send for gamepad is {}".format(command_to_send))
 */
  __Pyx_TraceLine(424,0,__PYX_ERR(0, 424, __pyx_L1_error))
  __pyx_v_command_to_send = ((char *)malloc(60));

  /* "serialmanager.pyx":425
 * 		comma = sm.createMsg(command, strafe, forward, angular);
 * 		cdef char * command_to_send= <char * >malloc(60)
 * 		strcpy(command_to_send,comma)             # <<<<<<<<<<<<<<
 * 		print("command to send for gamepad is {}".format(command_to_send))
 * 		print("command has been send comma is {}".format(comma))
 */
  __Pyx_TraceLine(425,0,__PYX_ERR(0, 425, __pyx_L1_error))
  (void)(strcpy(__pyx_v_command_to_send, __pyx_v_comma));

  /* "serialmanager.pyx":426
 * 		cdef char * command_to_send= <char * >malloc(60)
 * 		strcpy(command_to_send,comma)
 * 		print("command to send for gamepad is {}".format(command_to_send))             # <<<<<<<<<<<<<<
 * 		print("command has been send comma is {}".format(comma))
 * 		print("command again  is {}".format(command_to_send))
 */
  __Pyx_TraceLine(426,0,__PYX_ERR(0, 426, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_command_to_send_for_gamepad_is, __pyx_n_s_format); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 426, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = __Pyx_PyBytes_FromString(__pyx_v_command_to_send); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 426, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_4 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
    __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_2);
    if (likely(__pyx_t_4)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
      __Pyx_INCREF(__pyx_t_4);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_2, function);
    }
  }
  __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_2, __pyx_t_4, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 426, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_2 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 426, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

  /* "serialmanager.pyx":427
 * 		strcpy(command_to_send,comma)
 * 		print("command to send for gamepad is {}".format(command_to_send))
 * 		print("command has been send comma is {}".format(comma))             # <<<<<<<<<<<<<<
 * 		print("command again  is {}".format(command_to_send))
 * 		#cdef char* command = command_to_send #per passare char faccio prova coi cdef
 */
  __Pyx_TraceLine(427,0,__PYX_ERR(0, 427, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_command_has_been_send_comma_is, __pyx_n_s_format); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 427, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_t_3 = __Pyx_PyBytes_FromString(__pyx_v_comma); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 427, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_4 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_1))) {
    __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_1);
    if (likely(__pyx_t_4)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_1);
      __Pyx_INCREF(__pyx_t_4);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_1, function);
    }
  }
  __pyx_t_2 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_1, __pyx_t_4, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_1, __pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 427, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_1 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 427, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":428
 * 		print("command to send for gamepad is {}".format(command_to_send))
 * 		print("command has been send comma is {}".format(comma))
 * 		print("command again  is {}".format(command_to_send))             # <<<<<<<<<<<<<<
 * 		#cdef char* command = command_to_send #per passare char faccio prova coi cdef
 * 
 */
  __Pyx_TraceLine(428,0,__PYX_ERR(0, 428, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_command_again_is, __pyx_n_s_format); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 428, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __pyx_t_3 = __Pyx_PyBytes_FromString(__pyx_v_command_to_send); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 428, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_3);
  __pyx_t_4 = NULL;
  if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
    __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_2);
    if (likely(__pyx_t_4)) {
      PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
      __Pyx_INCREF(__pyx_t_4);
      __Pyx_INCREF(function);
      __Pyx_DECREF_SET(__pyx_t_2, function);
    }
  }
  __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_2, __pyx_t_4, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
  __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
  if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 428, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __pyx_t_2 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 428, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

  /* "serialmanager.pyx":437
 * 		#print("ans2 is {}".format(ans2))
 * 		#print("ans3 is {}".format(ans3))
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(command_to_send)
 * 			print('Sent from Raspi {}'.format(command_to_send))
 */
  __Pyx_TraceLine(437,0,__PYX_ERR(0, 437, __pyx_L1_error))
  __pyx_t_5 = ((!(__pyx_v_waiting_for_reply != 0)) != 0);
  if (__pyx_t_5) {

    /* "serialmanager.pyx":438
 * 		#print("ans3 is {}".format(ans3))
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command_to_send)             # <<<<<<<<<<<<<<
 * 			print('Sent from Raspi {}'.format(command_to_send))
 * 			waiting_for_reply = True
 */
    __Pyx_TraceLine(438,0,__PYX_ERR(0, 438, __pyx_L1_error))
    __pyx_t_2 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_command_to_send); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 438, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

    /* "serialmanager.pyx":439
 * 		if not waiting_for_reply:
 * 			self.send_to_arduino(command_to_send)
 * 			print('Sent from Raspi {}'.format(command_to_send))             # <<<<<<<<<<<<<<
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 */
    __Pyx_TraceLine(439,0,__PYX_ERR(0, 439, __pyx_L1_error))
    __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_Raspi_2, __pyx_n_s_format); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 439, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __pyx_t_3 = __Pyx_PyBytes_FromString(__pyx_v_command_to_send); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 439, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_1))) {
      __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_1);
      if (likely(__pyx_t_4)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_1);
        __Pyx_INCREF(__pyx_t_4);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_1, function);
      }
    }
    __pyx_t_2 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_1, __pyx_t_4, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_1, __pyx_t_3);
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 439, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_t_1 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 439, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

    /* "serialmanager.pyx":440
 * 			self.send_to_arduino(command_to_send)
 * 			print('Sent from Raspi {}'.format(command_to_send))
 * 			waiting_for_reply = True             # <<<<<<<<<<<<<<
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:
 */
    __Pyx_TraceLine(440,0,__PYX_ERR(0, 440, __pyx_L1_error))
    __pyx_v_waiting_for_reply = 1;

    /* "serialmanager.pyx":437
 * 		#print("ans2 is {}".format(ans2))
 * 		#print("ans3 is {}".format(ans3))
 * 		if not waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(command_to_send)
 * 			print('Sent from Raspi {}'.format(command_to_send))
 */
  }

  /* "serialmanager.pyx":441
 * 			print('Sent from Raspi {}'.format(command_to_send))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(441,0,__PYX_ERR(0, 441, __pyx_L1_error))
  __pyx_t_5 = (__pyx_v_waiting_for_reply != 0);
  if (__pyx_t_5) {

    /* "serialmanager.pyx":442
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(442,0,__PYX_ERR(0, 442, __pyx_L1_error))
    while (1) {
      __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 442, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __pyx_t_3 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
        __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_2);
        if (likely(__pyx_t_3)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
          __Pyx_INCREF(__pyx_t_3);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_2, function);
        }
      }
      __pyx_t_1 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_2);
      __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
      if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 442, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_1);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_t_1, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 442, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
      __pyx_t_5 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_5 < 0)) __PYX_ERR(0, 442, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      if (!__pyx_t_5) break;
    }

    /* "serialmanager.pyx":444
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply Received {}'.format(dataReceived))
 * 			res = dataReceived.split()
 */
    __Pyx_TraceLine(444,0,__PYX_ERR(0, 444, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":445
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received {}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 			res = dataReceived.split()
 * 			ans1 = float(res[3].decode())
 */
    __Pyx_TraceLine(445,0,__PYX_ERR(0, 445, __pyx_L1_error))
    __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_Received, __pyx_n_s_format); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 445, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __pyx_t_3 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 445, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_1))) {
      __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_1);
      if (likely(__pyx_t_4)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_1);
        __Pyx_INCREF(__pyx_t_4);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_1, function);
      }
    }
    __pyx_t_2 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_1, __pyx_t_4, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_1, __pyx_t_3);
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 445, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_t_1 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 445, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

    /* "serialmanager.pyx":446
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply Received {}'.format(dataReceived))
 * 			res = dataReceived.split()             # <<<<<<<<<<<<<<
 * 			ans1 = float(res[3].decode())
 * 			ans2 = float(res[7].decode())
 */
    __Pyx_TraceLine(446,0,__PYX_ERR(0, 446, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 446, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_split); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 446, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_2 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_2)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_2);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_1 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
    __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 446, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_v_res = __pyx_t_1;
    __pyx_t_1 = 0;

    /* "serialmanager.pyx":447
 * 			print('Reply Received {}'.format(dataReceived))
 * 			res = dataReceived.split()
 * 			ans1 = float(res[3].decode())             # <<<<<<<<<<<<<<
 * 			ans2 = float(res[7].decode())
 * 			ans3 = float(res[11].decode())
 */
    __Pyx_TraceLine(447,0,__PYX_ERR(0, 447, __pyx_L1_error))
    __pyx_t_3 = __Pyx_GetItemInt(__pyx_v_res, 3, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 447, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_t_3, __pyx_n_s_decode); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 447, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
      __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_2);
      if (likely(__pyx_t_3)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
        __Pyx_INCREF(__pyx_t_3);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_2, function);
      }
    }
    __pyx_t_1 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_2);
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 447, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_6 = __Pyx_PyObject_AsDouble(__pyx_t_1); if (unlikely(__pyx_t_6 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 447, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_v_ans1 = __pyx_t_6;

    /* "serialmanager.pyx":448
 * 			res = dataReceived.split()
 * 			ans1 = float(res[3].decode())
 * 			ans2 = float(res[7].decode())             # <<<<<<<<<<<<<<
 * 			ans3 = float(res[11].decode())
 * 		#return ans1,ans2,ans3
 */
    __Pyx_TraceLine(448,0,__PYX_ERR(0, 448, __pyx_L1_error))
    __pyx_t_2 = __Pyx_GetItemInt(__pyx_v_res, 7, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 448, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_t_2, __pyx_n_s_decode); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 448, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_2 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_2 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_2)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_2);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_1 = (__pyx_t_2) ? __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_2) : __Pyx_PyObject_CallNoArg(__pyx_t_3);
    __Pyx_XDECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 448, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_6 = __Pyx_PyObject_AsDouble(__pyx_t_1); if (unlikely(__pyx_t_6 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 448, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_v_ans2 = __pyx_t_6;

    /* "serialmanager.pyx":449
 * 			ans1 = float(res[3].decode())
 * 			ans2 = float(res[7].decode())
 * 			ans3 = float(res[11].decode())             # <<<<<<<<<<<<<<
 * 		#return ans1,ans2,ans3
 * 			print("ans1 is {}".format(ans1))
 */
    __Pyx_TraceLine(449,0,__PYX_ERR(0, 449, __pyx_L1_error))
    __pyx_t_3 = __Pyx_GetItemInt(__pyx_v_res, 11, long, 1, __Pyx_PyInt_From_long, 0, 0, 0); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 449, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_t_3, __pyx_n_s_decode); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 449, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
      __pyx_t_3 = PyMethod_GET_SELF(__pyx_t_2);
      if (likely(__pyx_t_3)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
        __Pyx_INCREF(__pyx_t_3);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_2, function);
      }
    }
    __pyx_t_1 = (__pyx_t_3) ? __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_3) : __Pyx_PyObject_CallNoArg(__pyx_t_2);
    __Pyx_XDECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 449, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_6 = __Pyx_PyObject_AsDouble(__pyx_t_1); if (unlikely(__pyx_t_6 == ((double)((double)-1)) && PyErr_Occurred())) __PYX_ERR(0, 449, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_v_ans3 = __pyx_t_6;

    /* "serialmanager.pyx":451
 * 			ans3 = float(res[11].decode())
 * 		#return ans1,ans2,ans3
 * 			print("ans1 is {}".format(ans1))             # <<<<<<<<<<<<<<
 * 			print("ans2 is {}".format(ans2))
 * 			print("ans3 is {}".format(ans3))
 */
    __Pyx_TraceLine(451,0,__PYX_ERR(0, 451, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_ans1_is, __pyx_n_s_format); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 451, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_3 = PyFloat_FromDouble(__pyx_v_ans1); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 451, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
      __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_2);
      if (likely(__pyx_t_4)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
        __Pyx_INCREF(__pyx_t_4);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_2, function);
      }
    }
    __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_2, __pyx_t_4, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_3);
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 451, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_2 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 451, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

    /* "serialmanager.pyx":452
 * 		#return ans1,ans2,ans3
 * 			print("ans1 is {}".format(ans1))
 * 			print("ans2 is {}".format(ans2))             # <<<<<<<<<<<<<<
 * 			print("ans3 is {}".format(ans3))
 * 		free(command_to_send)
 */
    __Pyx_TraceLine(452,0,__PYX_ERR(0, 452, __pyx_L1_error))
    __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_ans2_is, __pyx_n_s_format); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 452, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __pyx_t_3 = PyFloat_FromDouble(__pyx_v_ans2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 452, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_1))) {
      __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_1);
      if (likely(__pyx_t_4)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_1);
        __Pyx_INCREF(__pyx_t_4);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_1, function);
      }
    }
    __pyx_t_2 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_1, __pyx_t_4, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_1, __pyx_t_3);
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 452, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __pyx_t_1 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 452, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

    /* "serialmanager.pyx":453
 * 			print("ans1 is {}".format(ans1))
 * 			print("ans2 is {}".format(ans2))
 * 			print("ans3 is {}".format(ans3))             # <<<<<<<<<<<<<<
 * 		free(command_to_send)
 * 
 */
    __Pyx_TraceLine(453,0,__PYX_ERR(0, 453, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_ans3_is, __pyx_n_s_format); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 453, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_3 = PyFloat_FromDouble(__pyx_v_ans3); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 453, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
      __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_2);
      if (likely(__pyx_t_4)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
        __Pyx_INCREF(__pyx_t_4);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_2, function);
      }
    }
    __pyx_t_1 = (__pyx_t_4) ? __Pyx_PyObject_Call2Args(__pyx_t_2, __pyx_t_4, __pyx_t_3) : __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_3);
    __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 453, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_1);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __pyx_t_2 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_1); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 453, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

    /* "serialmanager.pyx":441
 * 			print('Sent from Raspi {}'.format(command_to_send))
 * 			waiting_for_reply = True
 * 		if waiting_for_reply:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":454
 * 			print("ans2 is {}".format(ans2))
 * 			print("ans3 is {}".format(ans3))
 * 		free(command_to_send)             # <<<<<<<<<<<<<<
 * 
 * 	#def ss(self, send):
 */
  __Pyx_TraceLine(454,0,__PYX_ERR(0, 454, __pyx_L1_error))
  free(__pyx_v_command_to_send);

  /* "serialmanager.pyx":420
 * 		print("ans2 is {}".format(ans2))
 * 
 * 	cdef cgamepad(self, char* command, float strafe, float forward, float angular):             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_AddTraceback("serialmanager.SerialManager.cgamepad", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_res);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":460
 * 	#	self.send_to_arduino(send)
 * 
 * 	def gamepad(self,command,strafe_speed,forward_speed,angular_speed):             # <<<<<<<<<<<<<<
 * 		order = command.encode('UTF-8', 'strict')
 * 		cdef:
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_35gamepad(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_34gamepad[] = "SerialManager.gamepad(self, command, strafe_speed, forward_speed, angular_speed)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_35gamepad(PyObject *__pyx_v_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_v_command = 0;
  PyObject *__pyx_v_strafe_speed = 0;
  PyObject *__pyx_v_forward_speed = 0;
  PyObject *__pyx_v_angular_speed = 0;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("gamepad (wrapper)", 0);
  {
    static PyObject **__pyx_pyargnames[] = {&__pyx_n_s_command,&__pyx_n_s_strafe_speed,&__pyx_n_s_forward_speed,&__pyx_n_s_angular_speed,0};
    PyObject* values[4] = {0,0,0,0};
    if (unlikely(__pyx_kwds)) {
      Py_ssize_t kw_args;
      const Py_ssize_t pos_args = PyTuple_GET_SIZE(__pyx_args);
      switch (pos_args) {
        case  4: values[3] = PyTuple_GET_ITEM(__pyx_args, 3);
        CYTHON_FALLTHROUGH;
        case  3: values[2] = PyTuple_GET_ITEM(__pyx_args, 2);
        CYTHON_FALLTHROUGH;
        case  2: values[1] = PyTuple_GET_ITEM(__pyx_args, 1);
        CYTHON_FALLTHROUGH;
        case  1: values[0] = PyTuple_GET_ITEM(__pyx_args, 0);
        CYTHON_FALLTHROUGH;
        case  0: break;
        default: goto __pyx_L5_argtuple_error;
      }
      kw_args = PyDict_Size(__pyx_kwds);
      switch (pos_args) {
        case  0:
        if (likely((values[0] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_command)) != 0)) kw_args--;
        else goto __pyx_L5_argtuple_error;
        CYTHON_FALLTHROUGH;
        case  1:
        if (likely((values[1] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_strafe_speed)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("gamepad", 1, 4, 4, 1); __PYX_ERR(0, 460, __pyx_L3_error)
        }
        CYTHON_FALLTHROUGH;
        case  2:
        if (likely((values[2] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_forward_speed)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("gamepad", 1, 4, 4, 2); __PYX_ERR(0, 460, __pyx_L3_error)
        }
        CYTHON_FALLTHROUGH;
        case  3:
        if (likely((values[3] = __Pyx_PyDict_GetItemStr(__pyx_kwds, __pyx_n_s_angular_speed)) != 0)) kw_args--;
        else {
          __Pyx_RaiseArgtupleInvalid("gamepad", 1, 4, 4, 3); __PYX_ERR(0, 460, __pyx_L3_error)
        }
      }
      if (unlikely(kw_args > 0)) {
        if (unlikely(__Pyx_ParseOptionalKeywords(__pyx_kwds, __pyx_pyargnames, 0, values, pos_args, "gamepad") < 0)) __PYX_ERR(0, 460, __pyx_L3_error)
      }
    } else if (PyTuple_GET_SIZE(__pyx_args) != 4) {
      goto __pyx_L5_argtuple_error;
    } else {
      values[0] = PyTuple_GET_ITEM(__pyx_args, 0);
      values[1] = PyTuple_GET_ITEM(__pyx_args, 1);
      values[2] = PyTuple_GET_ITEM(__pyx_args, 2);
      values[3] = PyTuple_GET_ITEM(__pyx_args, 3);
    }
    __pyx_v_command = values[0];
    __pyx_v_strafe_speed = values[1];
    __pyx_v_forward_speed = values[2];
    __pyx_v_angular_speed = values[3];
  }
  goto __pyx_L4_argument_unpacking_done;
  __pyx_L5_argtuple_error:;
  __Pyx_RaiseArgtupleInvalid("gamepad", 1, 4, 4, PyTuple_GET_SIZE(__pyx_args)); __PYX_ERR(0, 460, __pyx_L3_error)
  __pyx_L3_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.gamepad", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __Pyx_RefNannyFinishContext();
  return NULL;
  __pyx_L4_argument_unpacking_done:;
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_34gamepad(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), __pyx_v_command, __pyx_v_strafe_speed, __pyx_v_forward_speed, __pyx_v_angular_speed);

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_34gamepad(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_command, PyObject *__pyx_v_strafe_speed, PyObject *__pyx_v_forward_speed, PyObject *__pyx_v_angular_speed) {
  PyObject *__pyx_v_order = NULL;
  char *__pyx_v_move_comm;
  float __pyx_v_strafe;
  float __pyx_v_forward;
  float __pyx_v_angular;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  char *__pyx_t_3;
  float __pyx_t_4;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("gamepad", 0);
  __Pyx_TraceCall("gamepad", __pyx_f[0], 460, 0, __PYX_ERR(0, 460, __pyx_L1_error));

  /* "serialmanager.pyx":461
 * 
 * 	def gamepad(self,command,strafe_speed,forward_speed,angular_speed):
 * 		order = command.encode('UTF-8', 'strict')             # <<<<<<<<<<<<<<
 * 		cdef:
 * 			char* move_comm = order
 */
  __Pyx_TraceLine(461,0,__PYX_ERR(0, 461, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_GetAttrStr(__pyx_v_command, __pyx_n_s_encode); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 461, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_t_2 = __Pyx_PyObject_Call(__pyx_t_1, __pyx_tuple__2, NULL); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 461, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_v_order = __pyx_t_2;
  __pyx_t_2 = 0;

  /* "serialmanager.pyx":463
 * 		order = command.encode('UTF-8', 'strict')
 * 		cdef:
 * 			char* move_comm = order             # <<<<<<<<<<<<<<
 * 			float strafe = strafe_speed
 * 			float forward = forward_speed
 */
  __Pyx_TraceLine(463,0,__PYX_ERR(0, 463, __pyx_L1_error))
  __pyx_t_3 = __Pyx_PyObject_AsWritableString(__pyx_v_order); if (unlikely((!__pyx_t_3) && PyErr_Occurred())) __PYX_ERR(0, 463, __pyx_L1_error)
  __pyx_v_move_comm = __pyx_t_3;

  /* "serialmanager.pyx":464
 * 		cdef:
 * 			char* move_comm = order
 * 			float strafe = strafe_speed             # <<<<<<<<<<<<<<
 * 			float forward = forward_speed
 * 			float angular = angular_speed
 */
  __Pyx_TraceLine(464,0,__PYX_ERR(0, 464, __pyx_L1_error))
  __pyx_t_4 = __pyx_PyFloat_AsFloat(__pyx_v_strafe_speed); if (unlikely((__pyx_t_4 == (float)-1) && PyErr_Occurred())) __PYX_ERR(0, 464, __pyx_L1_error)
  __pyx_v_strafe = __pyx_t_4;

  /* "serialmanager.pyx":465
 * 			char* move_comm = order
 * 			float strafe = strafe_speed
 * 			float forward = forward_speed             # <<<<<<<<<<<<<<
 * 			float angular = angular_speed
 * 		#move_comm = sm.fromStringToBytes(command)
 */
  __Pyx_TraceLine(465,0,__PYX_ERR(0, 465, __pyx_L1_error))
  __pyx_t_4 = __pyx_PyFloat_AsFloat(__pyx_v_forward_speed); if (unlikely((__pyx_t_4 == (float)-1) && PyErr_Occurred())) __PYX_ERR(0, 465, __pyx_L1_error)
  __pyx_v_forward = __pyx_t_4;

  /* "serialmanager.pyx":466
 * 			float strafe = strafe_speed
 * 			float forward = forward_speed
 * 			float angular = angular_speed             # <<<<<<<<<<<<<<
 * 		#move_comm = sm.fromStringToBytes(command)
 * 		self.cgamepad(move_comm,strafe,forward,angular)
 */
  __Pyx_TraceLine(466,0,__PYX_ERR(0, 466, __pyx_L1_error))
  __pyx_t_4 = __pyx_PyFloat_AsFloat(__pyx_v_angular_speed); if (unlikely((__pyx_t_4 == (float)-1) && PyErr_Occurred())) __PYX_ERR(0, 466, __pyx_L1_error)
  __pyx_v_angular = __pyx_t_4;

  /* "serialmanager.pyx":468
 * 			float angular = angular_speed
 * 		#move_comm = sm.fromStringToBytes(command)
 * 		self.cgamepad(move_comm,strafe,forward,angular)             # <<<<<<<<<<<<<<
 * 
 * 	cdef cprova1(self):
 */
  __Pyx_TraceLine(468,0,__PYX_ERR(0, 468, __pyx_L1_error))
  __pyx_t_2 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->cgamepad(__pyx_v_self, __pyx_v_move_comm, __pyx_v_strafe, __pyx_v_forward, __pyx_v_angular); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 468, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

  /* "serialmanager.pyx":460
 * 	#	self.send_to_arduino(send)
 * 
 * 	def gamepad(self,command,strafe_speed,forward_speed,angular_speed):             # <<<<<<<<<<<<<<
 * 		order = command.encode('UTF-8', 'strict')
 * 		cdef:
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_AddTraceback("serialmanager.SerialManager.gamepad", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XDECREF(__pyx_v_order);
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":470
 * 		self.cgamepad(move_comm,strafe,forward,angular)
 * 
 * 	cdef cprova1(self):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_cprova1(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  int __pyx_v_waitingForReply;
  char *__pyx_v_dataReceived;
  char *__pyx_v_result;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("cprova1", 0);
  __Pyx_TraceCall("cprova1", __pyx_f[0], 470, 0, __PYX_ERR(0, 470, __pyx_L1_error));

  /* "serialmanager.pyx":471
 * 
 * 	cdef cprova1(self):
 * 		cdef bint waitingForReply = False             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived
 * 		cdef char* result = b'<control>'
 */
  __Pyx_TraceLine(471,0,__PYX_ERR(0, 471, __pyx_L1_error))
  __pyx_v_waitingForReply = 0;

  /* "serialmanager.pyx":473
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived
 * 		cdef char* result = b'<control>'             # <<<<<<<<<<<<<<
 * 		if waitingForReply == False:
 * 			self.send_to_arduino(result)
 */
  __Pyx_TraceLine(473,0,__PYX_ERR(0, 473, __pyx_L1_error))
  __pyx_v_result = ((char *)"<control>");

  /* "serialmanager.pyx":474
 * 		cdef char* dataReceived
 * 		cdef char* result = b'<control>'
 * 		if waitingForReply == False:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))
 */
  __Pyx_TraceLine(474,0,__PYX_ERR(0, 474, __pyx_L1_error))
  __pyx_t_1 = ((__pyx_v_waitingForReply == 0) != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":475
 * 		cdef char* result = b'<control>'
 * 		if waitingForReply == False:
 * 			self.send_to_arduino(result)             # <<<<<<<<<<<<<<
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True
 */
    __Pyx_TraceLine(475,0,__PYX_ERR(0, 475, __pyx_L1_error))
    __pyx_t_2 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_result); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 475, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

    /* "serialmanager.pyx":476
 * 		if waitingForReply == False:
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))             # <<<<<<<<<<<<<<
 * 			waitingForReply = True
 * 		if waitingForReply == True:
 */
    __Pyx_TraceLine(476,0,__PYX_ERR(0, 476, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_RASPI, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 476, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = __Pyx_PyBytes_FromString(__pyx_v_result); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 476, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 476, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 476, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":477
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True             # <<<<<<<<<<<<<<
 * 		if waitingForReply == True:
 * 			while self.ser.inWaiting() == 0:
 */
    __Pyx_TraceLine(477,0,__PYX_ERR(0, 477, __pyx_L1_error))
    __pyx_v_waitingForReply = 1;

    /* "serialmanager.pyx":474
 * 		cdef char* dataReceived
 * 		cdef char* result = b'<control>'
 * 		if waitingForReply == False:             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result))
 */
  }

  /* "serialmanager.pyx":478
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True
 * 		if waitingForReply == True:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  __Pyx_TraceLine(478,0,__PYX_ERR(0, 478, __pyx_L1_error))
  __pyx_t_1 = ((__pyx_v_waitingForReply == 1) != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":479
 * 			waitingForReply = True
 * 		if waitingForReply == True:
 * 			while self.ser.inWaiting() == 0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(479,0,__PYX_ERR(0, 479, __pyx_L1_error))
    while (1) {
      __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 479, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __pyx_t_4 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
        __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_2);
        if (likely(__pyx_t_4)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
          __Pyx_INCREF(__pyx_t_4);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_2, function);
        }
      }
      __pyx_t_3 = (__pyx_t_4) ? __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_4) : __Pyx_PyObject_CallNoArg(__pyx_t_2);
      __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 479, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_t_3, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 479, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 479, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      if (!__pyx_t_1) break;
    }

    /* "serialmanager.pyx":481
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply control Received{}'.format(dataReceived))
 * 
 */
    __Pyx_TraceLine(481,0,__PYX_ERR(0, 481, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":482
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply control Received{}'.format(dataReceived))             # <<<<<<<<<<<<<<
 * 
 * 	def prova1(self):
 */
    __Pyx_TraceLine(482,0,__PYX_ERR(0, 482, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_control_Received, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 482, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = __Pyx_PyBytes_FromString(__pyx_v_dataReceived); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 482, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 482, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 482, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":478
 * 			print('Sent from RASPI {}'.format(result))
 * 			waitingForReply = True
 * 		if waitingForReply == True:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting() == 0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":470
 * 		self.cgamepad(move_comm,strafe,forward,angular)
 * 
 * 	cdef cprova1(self):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("serialmanager.SerialManager.cprova1", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":484
 * 			print('Reply control Received{}'.format(dataReceived))
 * 
 * 	def prova1(self):             # <<<<<<<<<<<<<<
 * 		self.cprova1();
 * 
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_37prova1(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_36prova1[] = "SerialManager.prova1(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_37prova1(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("prova1 (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_36prova1(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_36prova1(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("prova1", 0);
  __Pyx_TraceCall("prova1", __pyx_f[0], 484, 0, __PYX_ERR(0, 484, __pyx_L1_error));

  /* "serialmanager.pyx":485
 * 
 * 	def prova1(self):
 * 		self.cprova1();             # <<<<<<<<<<<<<<
 * 
 * 	cdef csendledcommand(self, conferma):
 */
  __Pyx_TraceLine(485,0,__PYX_ERR(0, 485, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->cprova1(__pyx_v_self); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 485, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":484
 * 			print('Reply control Received{}'.format(dataReceived))
 * 
 * 	def prova1(self):             # <<<<<<<<<<<<<<
 * 		self.cprova1();
 * 
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.prova1", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":487
 * 		self.cprova1();
 * 
 * 	cdef csendledcommand(self, conferma):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived = ''
 */

static PyObject *__pyx_f_13serialmanager_13SerialManager_csendledcommand(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_conferma) {
  int __pyx_v_waitingForReply;
  char *__pyx_v_dataReceived;
  char *__pyx_v_resultstop;
  char *__pyx_v_resultleft;
  char *__pyx_v_resultfront;
  char *__pyx_v_resultright;
  char *__pyx_v_resultback;
  char *__pyx_v_resultturn;
  char *__pyx_v_resulttran;
  char *__pyx_v_resultno;
  char *__pyx_v_result;
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  PyObject *__pyx_t_2 = NULL;
  PyObject *__pyx_t_3 = NULL;
  PyObject *__pyx_t_4 = NULL;
  PyObject *__pyx_t_5 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("csendledcommand", 0);
  __Pyx_TraceCall("csendledcommand", __pyx_f[0], 487, 0, __PYX_ERR(0, 487, __pyx_L1_error));

  /* "serialmanager.pyx":488
 * 
 * 	cdef csendledcommand(self, conferma):
 * 		cdef bint waitingForReply = False             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		cdef char* resultstop = b'<q>' #change command here!!
 */
  __Pyx_TraceLine(488,0,__PYX_ERR(0, 488, __pyx_L1_error))
  __pyx_v_waitingForReply = 0;

  /* "serialmanager.pyx":489
 * 	cdef csendledcommand(self, conferma):
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived = ''             # <<<<<<<<<<<<<<
 * 		cdef char* resultstop = b'<q>' #change command here!!
 * 		cdef char* resultleft = b'<w>' #change command here!!
 */
  __Pyx_TraceLine(489,0,__PYX_ERR(0, 489, __pyx_L1_error))
  __pyx_v_dataReceived = ((char *)"");

  /* "serialmanager.pyx":490
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived = ''
 * 		cdef char* resultstop = b'<q>' #change command here!!             # <<<<<<<<<<<<<<
 * 		cdef char* resultleft = b'<w>' #change command here!!
 * 		cdef char* resultfront = b'<x>' #change command here!!
 */
  __Pyx_TraceLine(490,0,__PYX_ERR(0, 490, __pyx_L1_error))
  __pyx_v_resultstop = ((char *)"<q>");

  /* "serialmanager.pyx":491
 * 		cdef char* dataReceived = ''
 * 		cdef char* resultstop = b'<q>' #change command here!!
 * 		cdef char* resultleft = b'<w>' #change command here!!             # <<<<<<<<<<<<<<
 * 		cdef char* resultfront = b'<x>' #change command here!!
 * 		cdef char* resultright = b'<y>' #change command here!!
 */
  __Pyx_TraceLine(491,0,__PYX_ERR(0, 491, __pyx_L1_error))
  __pyx_v_resultleft = ((char *)"<w>");

  /* "serialmanager.pyx":492
 * 		cdef char* resultstop = b'<q>' #change command here!!
 * 		cdef char* resultleft = b'<w>' #change command here!!
 * 		cdef char* resultfront = b'<x>' #change command here!!             # <<<<<<<<<<<<<<
 * 		cdef char* resultright = b'<y>' #change command here!!
 * 		cdef char* resultback = b'<z>' #change command here!!
 */
  __Pyx_TraceLine(492,0,__PYX_ERR(0, 492, __pyx_L1_error))
  __pyx_v_resultfront = ((char *)"<x>");

  /* "serialmanager.pyx":493
 * 		cdef char* resultleft = b'<w>' #change command here!!
 * 		cdef char* resultfront = b'<x>' #change command here!!
 * 		cdef char* resultright = b'<y>' #change command here!!             # <<<<<<<<<<<<<<
 * 		cdef char* resultback = b'<z>' #change command here!!
 * 		cdef char* resultturn = b'<t>' #change command here!!
 */
  __Pyx_TraceLine(493,0,__PYX_ERR(0, 493, __pyx_L1_error))
  __pyx_v_resultright = ((char *)"<y>");

  /* "serialmanager.pyx":494
 * 		cdef char* resultfront = b'<x>' #change command here!!
 * 		cdef char* resultright = b'<y>' #change command here!!
 * 		cdef char* resultback = b'<z>' #change command here!!             # <<<<<<<<<<<<<<
 * 		cdef char* resultturn = b'<t>' #change command here!!
 * 		cdef char* resulttran = b'<k>' #change command here!!
 */
  __Pyx_TraceLine(494,0,__PYX_ERR(0, 494, __pyx_L1_error))
  __pyx_v_resultback = ((char *)"<z>");

  /* "serialmanager.pyx":495
 * 		cdef char* resultright = b'<y>' #change command here!!
 * 		cdef char* resultback = b'<z>' #change command here!!
 * 		cdef char* resultturn = b'<t>' #change command here!!             # <<<<<<<<<<<<<<
 * 		cdef char* resulttran = b'<k>' #change command here!!
 * 		cdef char* resultno = b'<m>' #change command here!!
 */
  __Pyx_TraceLine(495,0,__PYX_ERR(0, 495, __pyx_L1_error))
  __pyx_v_resultturn = ((char *)"<t>");

  /* "serialmanager.pyx":496
 * 		cdef char* resultback = b'<z>' #change command here!!
 * 		cdef char* resultturn = b'<t>' #change command here!!
 * 		cdef char* resulttran = b'<k>' #change command here!!             # <<<<<<<<<<<<<<
 * 		cdef char* resultno = b'<m>' #change command here!!
 * 
 */
  __Pyx_TraceLine(496,0,__PYX_ERR(0, 496, __pyx_L1_error))
  __pyx_v_resulttran = ((char *)"<k>");

  /* "serialmanager.pyx":497
 * 		cdef char* resultturn = b'<t>' #change command here!!
 * 		cdef char* resulttran = b'<k>' #change command here!!
 * 		cdef char* resultno = b'<m>' #change command here!!             # <<<<<<<<<<<<<<
 * 
 * 		if waitingForReply == False:
 */
  __Pyx_TraceLine(497,0,__PYX_ERR(0, 497, __pyx_L1_error))
  __pyx_v_resultno = ((char *)"<m>");

  /* "serialmanager.pyx":499
 * 		cdef char* resultno = b'<m>' #change command here!!
 * 
 * 		if waitingForReply == False:             # <<<<<<<<<<<<<<
 * 			if conferma==1:
 * 				result = resultleft
 */
  __Pyx_TraceLine(499,0,__PYX_ERR(0, 499, __pyx_L1_error))
  __pyx_t_1 = ((__pyx_v_waitingForReply == 0) != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":500
 * 
 * 		if waitingForReply == False:
 * 			if conferma==1:             # <<<<<<<<<<<<<<
 * 				result = resultleft
 * 			if conferma==2:
 */
    __Pyx_TraceLine(500,0,__PYX_ERR(0, 500, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_v_conferma, __pyx_int_1, 1, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 500, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 500, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":501
 * 		if waitingForReply == False:
 * 			if conferma==1:
 * 				result = resultleft             # <<<<<<<<<<<<<<
 * 			if conferma==2:
 * 				result = resultfront
 */
      __Pyx_TraceLine(501,0,__PYX_ERR(0, 501, __pyx_L1_error))
      __pyx_v_result = __pyx_v_resultleft;

      /* "serialmanager.pyx":500
 * 
 * 		if waitingForReply == False:
 * 			if conferma==1:             # <<<<<<<<<<<<<<
 * 				result = resultleft
 * 			if conferma==2:
 */
    }

    /* "serialmanager.pyx":502
 * 			if conferma==1:
 * 				result = resultleft
 * 			if conferma==2:             # <<<<<<<<<<<<<<
 * 				result = resultfront
 * 			if conferma==3:
 */
    __Pyx_TraceLine(502,0,__PYX_ERR(0, 502, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_v_conferma, __pyx_int_2, 2, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 502, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 502, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":503
 * 				result = resultleft
 * 			if conferma==2:
 * 				result = resultfront             # <<<<<<<<<<<<<<
 * 			if conferma==3:
 * 				result = resultright
 */
      __Pyx_TraceLine(503,0,__PYX_ERR(0, 503, __pyx_L1_error))
      __pyx_v_result = __pyx_v_resultfront;

      /* "serialmanager.pyx":502
 * 			if conferma==1:
 * 				result = resultleft
 * 			if conferma==2:             # <<<<<<<<<<<<<<
 * 				result = resultfront
 * 			if conferma==3:
 */
    }

    /* "serialmanager.pyx":504
 * 			if conferma==2:
 * 				result = resultfront
 * 			if conferma==3:             # <<<<<<<<<<<<<<
 * 				result = resultright
 * 			if conferma==4:
 */
    __Pyx_TraceLine(504,0,__PYX_ERR(0, 504, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_v_conferma, __pyx_int_3, 3, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 504, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 504, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":505
 * 				result = resultfront
 * 			if conferma==3:
 * 				result = resultright             # <<<<<<<<<<<<<<
 * 			if conferma==4:
 * 				result = resultback
 */
      __Pyx_TraceLine(505,0,__PYX_ERR(0, 505, __pyx_L1_error))
      __pyx_v_result = __pyx_v_resultright;

      /* "serialmanager.pyx":504
 * 			if conferma==2:
 * 				result = resultfront
 * 			if conferma==3:             # <<<<<<<<<<<<<<
 * 				result = resultright
 * 			if conferma==4:
 */
    }

    /* "serialmanager.pyx":506
 * 			if conferma==3:
 * 				result = resultright
 * 			if conferma==4:             # <<<<<<<<<<<<<<
 * 				result = resultback
 * 			if conferma==5:
 */
    __Pyx_TraceLine(506,0,__PYX_ERR(0, 506, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_v_conferma, __pyx_int_4, 4, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 506, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 506, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":507
 * 				result = resultright
 * 			if conferma==4:
 * 				result = resultback             # <<<<<<<<<<<<<<
 * 			if conferma==5:
 * 				result = resultturn
 */
      __Pyx_TraceLine(507,0,__PYX_ERR(0, 507, __pyx_L1_error))
      __pyx_v_result = __pyx_v_resultback;

      /* "serialmanager.pyx":506
 * 			if conferma==3:
 * 				result = resultright
 * 			if conferma==4:             # <<<<<<<<<<<<<<
 * 				result = resultback
 * 			if conferma==5:
 */
    }

    /* "serialmanager.pyx":508
 * 			if conferma==4:
 * 				result = resultback
 * 			if conferma==5:             # <<<<<<<<<<<<<<
 * 				result = resultturn
 * 			if conferma==6:
 */
    __Pyx_TraceLine(508,0,__PYX_ERR(0, 508, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_v_conferma, __pyx_int_5, 5, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 508, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 508, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":509
 * 				result = resultback
 * 			if conferma==5:
 * 				result = resultturn             # <<<<<<<<<<<<<<
 * 			if conferma==6:
 * 				result = resultstop
 */
      __Pyx_TraceLine(509,0,__PYX_ERR(0, 509, __pyx_L1_error))
      __pyx_v_result = __pyx_v_resultturn;

      /* "serialmanager.pyx":508
 * 			if conferma==4:
 * 				result = resultback
 * 			if conferma==5:             # <<<<<<<<<<<<<<
 * 				result = resultturn
 * 			if conferma==6:
 */
    }

    /* "serialmanager.pyx":510
 * 			if conferma==5:
 * 				result = resultturn
 * 			if conferma==6:             # <<<<<<<<<<<<<<
 * 				result = resultstop
 * 			if conferma==7:
 */
    __Pyx_TraceLine(510,0,__PYX_ERR(0, 510, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_v_conferma, __pyx_int_6, 6, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 510, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 510, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":511
 * 				result = resultturn
 * 			if conferma==6:
 * 				result = resultstop             # <<<<<<<<<<<<<<
 * 			if conferma==7:
 * 				result = resulttran
 */
      __Pyx_TraceLine(511,0,__PYX_ERR(0, 511, __pyx_L1_error))
      __pyx_v_result = __pyx_v_resultstop;

      /* "serialmanager.pyx":510
 * 			if conferma==5:
 * 				result = resultturn
 * 			if conferma==6:             # <<<<<<<<<<<<<<
 * 				result = resultstop
 * 			if conferma==7:
 */
    }

    /* "serialmanager.pyx":512
 * 			if conferma==6:
 * 				result = resultstop
 * 			if conferma==7:             # <<<<<<<<<<<<<<
 * 				result = resulttran
 * 			if conferma==8:
 */
    __Pyx_TraceLine(512,0,__PYX_ERR(0, 512, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_v_conferma, __pyx_int_7, 7, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 512, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 512, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":513
 * 				result = resultstop
 * 			if conferma==7:
 * 				result = resulttran             # <<<<<<<<<<<<<<
 * 			if conferma==8:
 * 				result = resultno
 */
      __Pyx_TraceLine(513,0,__PYX_ERR(0, 513, __pyx_L1_error))
      __pyx_v_result = __pyx_v_resulttran;

      /* "serialmanager.pyx":512
 * 			if conferma==6:
 * 				result = resultstop
 * 			if conferma==7:             # <<<<<<<<<<<<<<
 * 				result = resulttran
 * 			if conferma==8:
 */
    }

    /* "serialmanager.pyx":514
 * 			if conferma==7:
 * 				result = resulttran
 * 			if conferma==8:             # <<<<<<<<<<<<<<
 * 				result = resultno
 * 			self.send_to_arduino(result)
 */
    __Pyx_TraceLine(514,0,__PYX_ERR(0, 514, __pyx_L1_error))
    __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_v_conferma, __pyx_int_8, 8, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 514, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 514, __pyx_L1_error)
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    if (__pyx_t_1) {

      /* "serialmanager.pyx":515
 * 				result = resulttran
 * 			if conferma==8:
 * 				result = resultno             # <<<<<<<<<<<<<<
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result.decode('UTF-8', 'strict')))
 */
      __Pyx_TraceLine(515,0,__PYX_ERR(0, 515, __pyx_L1_error))
      __pyx_v_result = __pyx_v_resultno;

      /* "serialmanager.pyx":514
 * 			if conferma==7:
 * 				result = resulttran
 * 			if conferma==8:             # <<<<<<<<<<<<<<
 * 				result = resultno
 * 			self.send_to_arduino(result)
 */
    }

    /* "serialmanager.pyx":516
 * 			if conferma==8:
 * 				result = resultno
 * 			self.send_to_arduino(result)             # <<<<<<<<<<<<<<
 * 			print('Sent from RASPI {}'.format(result.decode('UTF-8', 'strict')))
 * 			waitingForReply = True
 */
    __Pyx_TraceLine(516,0,__PYX_ERR(0, 516, __pyx_L1_error))
    __pyx_t_2 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->send_to_arduino(__pyx_v_self, __pyx_v_result); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 516, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

    /* "serialmanager.pyx":517
 * 				result = resultno
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result.decode('UTF-8', 'strict')))             # <<<<<<<<<<<<<<
 * 			waitingForReply = True
 * 		if waitingForReply == True:
 */
    __Pyx_TraceLine(517,0,__PYX_ERR(0, 517, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Sent_from_RASPI, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 517, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = __Pyx_decode_c_string(__pyx_v_result, 0, strlen(__pyx_v_result), NULL, NULL, PyUnicode_DecodeUTF8); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 517, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 517, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 517, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":518
 * 			self.send_to_arduino(result)
 * 			print('Sent from RASPI {}'.format(result.decode('UTF-8', 'strict')))
 * 			waitingForReply = True             # <<<<<<<<<<<<<<
 * 		if waitingForReply == True:
 * 			while self.ser.inWaiting()==0:
 */
    __Pyx_TraceLine(518,0,__PYX_ERR(0, 518, __pyx_L1_error))
    __pyx_v_waitingForReply = 1;

    /* "serialmanager.pyx":499
 * 		cdef char* resultno = b'<m>' #change command here!!
 * 
 * 		if waitingForReply == False:             # <<<<<<<<<<<<<<
 * 			if conferma==1:
 * 				result = resultleft
 */
  }

  /* "serialmanager.pyx":519
 * 			print('Sent from RASPI {}'.format(result.decode('UTF-8', 'strict')))
 * 			waitingForReply = True
 * 		if waitingForReply == True:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting()==0:
 * 				pass
 */
  __Pyx_TraceLine(519,0,__PYX_ERR(0, 519, __pyx_L1_error))
  __pyx_t_1 = ((__pyx_v_waitingForReply == 1) != 0);
  if (__pyx_t_1) {

    /* "serialmanager.pyx":520
 * 			waitingForReply = True
 * 		if waitingForReply == True:
 * 			while self.ser.inWaiting()==0:             # <<<<<<<<<<<<<<
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 */
    __Pyx_TraceLine(520,0,__PYX_ERR(0, 520, __pyx_L1_error))
    while (1) {
      __pyx_t_2 = __Pyx_PyObject_GetAttrStr(__pyx_v_self->ser, __pyx_n_s_inWaiting); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 520, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __pyx_t_4 = NULL;
      if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_2))) {
        __pyx_t_4 = PyMethod_GET_SELF(__pyx_t_2);
        if (likely(__pyx_t_4)) {
          PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_2);
          __Pyx_INCREF(__pyx_t_4);
          __Pyx_INCREF(function);
          __Pyx_DECREF_SET(__pyx_t_2, function);
        }
      }
      __pyx_t_3 = (__pyx_t_4) ? __Pyx_PyObject_CallOneArg(__pyx_t_2, __pyx_t_4) : __Pyx_PyObject_CallNoArg(__pyx_t_2);
      __Pyx_XDECREF(__pyx_t_4); __pyx_t_4 = 0;
      if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 520, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_3);
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      __pyx_t_2 = __Pyx_PyInt_EqObjC(__pyx_t_3, __pyx_int_0, 0, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 520, __pyx_L1_error)
      __Pyx_GOTREF(__pyx_t_2);
      __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
      __pyx_t_1 = __Pyx_PyObject_IsTrue(__pyx_t_2); if (unlikely(__pyx_t_1 < 0)) __PYX_ERR(0, 520, __pyx_L1_error)
      __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
      if (!__pyx_t_1) break;
    }

    /* "serialmanager.pyx":522
 * 			while self.ser.inWaiting()==0:
 * 				pass
 * 			dataReceived = self.receive_from_arduino()             # <<<<<<<<<<<<<<
 * 			print('Reply led Received arduino - {}'.format(dataReceived.decode('UTF-8', 'strict')))
 * 
 */
    __Pyx_TraceLine(522,0,__PYX_ERR(0, 522, __pyx_L1_error))
    __pyx_v_dataReceived = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->receive_from_arduino(__pyx_v_self);

    /* "serialmanager.pyx":523
 * 				pass
 * 			dataReceived = self.receive_from_arduino()
 * 			print('Reply led Received arduino - {}'.format(dataReceived.decode('UTF-8', 'strict')))             # <<<<<<<<<<<<<<
 * 
 * 	def sendled(self,conferma):
 */
    __Pyx_TraceLine(523,0,__PYX_ERR(0, 523, __pyx_L1_error))
    __pyx_t_3 = __Pyx_PyObject_GetAttrStr(__pyx_kp_u_Reply_led_Received_arduino, __pyx_n_s_format); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 523, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __pyx_t_4 = __Pyx_decode_c_string(__pyx_v_dataReceived, 0, strlen(__pyx_v_dataReceived), NULL, NULL, PyUnicode_DecodeUTF8); if (unlikely(!__pyx_t_4)) __PYX_ERR(0, 523, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_4);
    __pyx_t_5 = NULL;
    if (CYTHON_UNPACK_METHODS && likely(PyMethod_Check(__pyx_t_3))) {
      __pyx_t_5 = PyMethod_GET_SELF(__pyx_t_3);
      if (likely(__pyx_t_5)) {
        PyObject* function = PyMethod_GET_FUNCTION(__pyx_t_3);
        __Pyx_INCREF(__pyx_t_5);
        __Pyx_INCREF(function);
        __Pyx_DECREF_SET(__pyx_t_3, function);
      }
    }
    __pyx_t_2 = (__pyx_t_5) ? __Pyx_PyObject_Call2Args(__pyx_t_3, __pyx_t_5, __pyx_t_4) : __Pyx_PyObject_CallOneArg(__pyx_t_3, __pyx_t_4);
    __Pyx_XDECREF(__pyx_t_5); __pyx_t_5 = 0;
    __Pyx_DECREF(__pyx_t_4); __pyx_t_4 = 0;
    if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 523, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_2);
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;
    __pyx_t_3 = __Pyx_PyObject_CallOneArg(__pyx_builtin_print, __pyx_t_2); if (unlikely(!__pyx_t_3)) __PYX_ERR(0, 523, __pyx_L1_error)
    __Pyx_GOTREF(__pyx_t_3);
    __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
    __Pyx_DECREF(__pyx_t_3); __pyx_t_3 = 0;

    /* "serialmanager.pyx":519
 * 			print('Sent from RASPI {}'.format(result.decode('UTF-8', 'strict')))
 * 			waitingForReply = True
 * 		if waitingForReply == True:             # <<<<<<<<<<<<<<
 * 			while self.ser.inWaiting()==0:
 * 				pass
 */
  }

  /* "serialmanager.pyx":487
 * 		self.cprova1();
 * 
 * 	cdef csendledcommand(self, conferma):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived = ''
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_2);
  __Pyx_XDECREF(__pyx_t_3);
  __Pyx_XDECREF(__pyx_t_4);
  __Pyx_XDECREF(__pyx_t_5);
  __Pyx_AddTraceback("serialmanager.SerialManager.csendledcommand", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = 0;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":525
 * 			print('Reply led Received arduino - {}'.format(dataReceived.decode('UTF-8', 'strict')))
 * 
 * 	def sendled(self,conferma):             # <<<<<<<<<<<<<<
 * 		self.csendledcommand(conferma)
 * 
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_39sendled(PyObject *__pyx_v_self, PyObject *__pyx_v_conferma); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_38sendled[] = "SerialManager.sendled(self, conferma)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_39sendled(PyObject *__pyx_v_self, PyObject *__pyx_v_conferma) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("sendled (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_38sendled(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), ((PyObject *)__pyx_v_conferma));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_38sendled(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_conferma) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("sendled", 0);
  __Pyx_TraceCall("sendled", __pyx_f[0], 525, 0, __PYX_ERR(0, 525, __pyx_L1_error));

  /* "serialmanager.pyx":526
 * 
 * 	def sendled(self,conferma):
 * 		self.csendledcommand(conferma)             # <<<<<<<<<<<<<<
 * 
 * 
 */
  __Pyx_TraceLine(526,0,__PYX_ERR(0, 526, __pyx_L1_error))
  __pyx_t_1 = ((struct __pyx_vtabstruct_13serialmanager_SerialManager *)__pyx_v_self->__pyx_vtab)->csendledcommand(__pyx_v_self, __pyx_v_conferma); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 526, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":525
 * 			print('Reply led Received arduino - {}'.format(dataReceived.decode('UTF-8', 'strict')))
 * 
 * 	def sendled(self,conferma):             # <<<<<<<<<<<<<<
 * 		self.csendledcommand(conferma)
 * 
 */

  /* function exit code */
  __pyx_r = Py_None; __Pyx_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.sendled", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":13
 * cdef class SerialManager:
 * 	cdef public:
 * 		str name             # <<<<<<<<<<<<<<
 * 		int start_marker
 * 		int end_marker
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_4name_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_13serialmanager_13SerialManager_4name_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_4name___get__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_4name___get__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 13, 0, __PYX_ERR(0, 13, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->name);
  __pyx_r = __pyx_v_self->name;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.name.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static int __pyx_pw_13serialmanager_13SerialManager_4name_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_13serialmanager_13SerialManager_4name_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_4name_2__set__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_13serialmanager_13SerialManager_4name_2__set__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 13, 0, __PYX_ERR(0, 13, __pyx_L1_error));
  if (!(likely(PyUnicode_CheckExact(__pyx_v_value))||((__pyx_v_value) == Py_None)||(PyErr_Format(PyExc_TypeError, "Expected %.16s, got %.200s", "unicode", Py_TYPE(__pyx_v_value)->tp_name), 0))) __PYX_ERR(0, 13, __pyx_L1_error)
  __pyx_t_1 = __pyx_v_value;
  __Pyx_INCREF(__pyx_t_1);
  __Pyx_GIVEREF(__pyx_t_1);
  __Pyx_GOTREF(__pyx_v_self->name);
  __Pyx_DECREF(__pyx_v_self->name);
  __pyx_v_self->name = ((PyObject*)__pyx_t_1);
  __pyx_t_1 = 0;

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.name.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static int __pyx_pw_13serialmanager_13SerialManager_4name_5__del__(PyObject *__pyx_v_self); /*proto*/
static int __pyx_pw_13serialmanager_13SerialManager_4name_5__del__(PyObject *__pyx_v_self) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__del__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_4name_4__del__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_13serialmanager_13SerialManager_4name_4__del__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__del__", 0);
  __Pyx_TraceCall("__del__", __pyx_f[0], 13, 0, __PYX_ERR(0, 13, __pyx_L1_error));
  __Pyx_INCREF(Py_None);
  __Pyx_GIVEREF(Py_None);
  __Pyx_GOTREF(__pyx_v_self->name);
  __Pyx_DECREF(__pyx_v_self->name);
  __pyx_v_self->name = ((PyObject*)Py_None);

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.name.__del__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":14
 * 	cdef public:
 * 		str name
 * 		int start_marker             # <<<<<<<<<<<<<<
 * 		int end_marker
 * 		ser
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_12start_marker_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_13serialmanager_13SerialManager_12start_marker_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_12start_marker___get__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_12start_marker___get__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 14, 0, __PYX_ERR(0, 14, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __Pyx_PyInt_From_int(__pyx_v_self->start_marker); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 14, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.start_marker.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static int __pyx_pw_13serialmanager_13SerialManager_12start_marker_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_13serialmanager_13SerialManager_12start_marker_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_12start_marker_2__set__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_13serialmanager_13SerialManager_12start_marker_2__set__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 14, 0, __PYX_ERR(0, 14, __pyx_L1_error));
  __pyx_t_1 = __Pyx_PyInt_As_int(__pyx_v_value); if (unlikely((__pyx_t_1 == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 14, __pyx_L1_error)
  __pyx_v_self->start_marker = __pyx_t_1;

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.start_marker.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":15
 * 		str name
 * 		int start_marker
 * 		int end_marker             # <<<<<<<<<<<<<<
 * 		ser
 * 	def __cinit__(self, str name, int start_marker, int end_marker, ser):
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_10end_marker_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_13serialmanager_13SerialManager_10end_marker_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_10end_marker___get__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_10end_marker___get__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 15, 0, __PYX_ERR(0, 15, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __pyx_t_1 = __Pyx_PyInt_From_int(__pyx_v_self->end_marker); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 15, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __pyx_r = __pyx_t_1;
  __pyx_t_1 = 0;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.end_marker.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static int __pyx_pw_13serialmanager_13SerialManager_10end_marker_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_13serialmanager_13SerialManager_10end_marker_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_10end_marker_2__set__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_13serialmanager_13SerialManager_10end_marker_2__set__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_t_1;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 15, 0, __PYX_ERR(0, 15, __pyx_L1_error));
  __pyx_t_1 = __Pyx_PyInt_As_int(__pyx_v_value); if (unlikely((__pyx_t_1 == (int)-1) && PyErr_Occurred())) __PYX_ERR(0, 15, __pyx_L1_error)
  __pyx_v_self->end_marker = __pyx_t_1;

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.end_marker.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "serialmanager.pyx":16
 * 		int start_marker
 * 		int end_marker
 * 		ser             # <<<<<<<<<<<<<<
 * 	def __cinit__(self, str name, int start_marker, int end_marker, ser):
 * 		self.name = name
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_3ser_1__get__(PyObject *__pyx_v_self); /*proto*/
static PyObject *__pyx_pw_13serialmanager_13SerialManager_3ser_1__get__(PyObject *__pyx_v_self) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__get__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_3ser___get__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_3ser___get__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__get__", 0);
  __Pyx_TraceCall("__get__", __pyx_f[0], 16, 0, __PYX_ERR(0, 16, __pyx_L1_error));
  __Pyx_XDECREF(__pyx_r);
  __Pyx_INCREF(__pyx_v_self->ser);
  __pyx_r = __pyx_v_self->ser;
  goto __pyx_L0;

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.ser.__get__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __pyx_L0:;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static int __pyx_pw_13serialmanager_13SerialManager_3ser_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value); /*proto*/
static int __pyx_pw_13serialmanager_13SerialManager_3ser_3__set__(PyObject *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__set__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_3ser_2__set__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), ((PyObject *)__pyx_v_value));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_13serialmanager_13SerialManager_3ser_2__set__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, PyObject *__pyx_v_value) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__set__", 0);
  __Pyx_TraceCall("__set__", __pyx_f[0], 16, 0, __PYX_ERR(0, 16, __pyx_L1_error));
  __Pyx_INCREF(__pyx_v_value);
  __Pyx_GIVEREF(__pyx_v_value);
  __Pyx_GOTREF(__pyx_v_self->ser);
  __Pyx_DECREF(__pyx_v_self->ser);
  __pyx_v_self->ser = __pyx_v_value;

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.ser.__set__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* Python wrapper */
static int __pyx_pw_13serialmanager_13SerialManager_3ser_5__del__(PyObject *__pyx_v_self); /*proto*/
static int __pyx_pw_13serialmanager_13SerialManager_3ser_5__del__(PyObject *__pyx_v_self) {
  int __pyx_r;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__del__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_3ser_4__del__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static int __pyx_pf_13serialmanager_13SerialManager_3ser_4__del__(struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  int __pyx_r;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__del__", 0);
  __Pyx_TraceCall("__del__", __pyx_f[0], 16, 0, __PYX_ERR(0, 16, __pyx_L1_error));
  __Pyx_INCREF(Py_None);
  __Pyx_GIVEREF(Py_None);
  __Pyx_GOTREF(__pyx_v_self->ser);
  __Pyx_DECREF(__pyx_v_self->ser);
  __pyx_v_self->ser = Py_None;

  /* function exit code */
  __pyx_r = 0;
  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_AddTraceback("serialmanager.SerialManager.ser.__del__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = -1;
  __pyx_L0:;
  __Pyx_TraceReturn(Py_None, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "(tree fragment)":1
 * def __reduce_cython__(self):             # <<<<<<<<<<<<<<
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_41__reduce_cython__(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_40__reduce_cython__[] = "SerialManager.__reduce_cython__(self)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_41__reduce_cython__(PyObject *__pyx_v_self, CYTHON_UNUSED PyObject *unused) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__reduce_cython__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_40__reduce_cython__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_40__reduce_cython__(CYTHON_UNUSED struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__reduce_cython__", 0);
  __Pyx_TraceCall("__reduce_cython__", __pyx_f[1], 1, 0, __PYX_ERR(1, 1, __pyx_L1_error));

  /* "(tree fragment)":2
 * def __reduce_cython__(self):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")             # <<<<<<<<<<<<<<
 * def __setstate_cython__(self, __pyx_state):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 */
  __Pyx_TraceLine(2,0,__PYX_ERR(1, 2, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_Call(__pyx_builtin_TypeError, __pyx_tuple__8, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(1, 2, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_Raise(__pyx_t_1, 0, 0, 0);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __PYX_ERR(1, 2, __pyx_L1_error)

  /* "(tree fragment)":1
 * def __reduce_cython__(self):             # <<<<<<<<<<<<<<
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.__reduce_cython__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

/* "(tree fragment)":3
 * def __reduce_cython__(self):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):             # <<<<<<<<<<<<<<
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 */

/* Python wrapper */
static PyObject *__pyx_pw_13serialmanager_13SerialManager_43__setstate_cython__(PyObject *__pyx_v_self, PyObject *__pyx_v___pyx_state); /*proto*/
static char __pyx_doc_13serialmanager_13SerialManager_42__setstate_cython__[] = "SerialManager.__setstate_cython__(self, __pyx_state)";
static PyObject *__pyx_pw_13serialmanager_13SerialManager_43__setstate_cython__(PyObject *__pyx_v_self, PyObject *__pyx_v___pyx_state) {
  PyObject *__pyx_r = 0;
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__setstate_cython__ (wrapper)", 0);
  __pyx_r = __pyx_pf_13serialmanager_13SerialManager_42__setstate_cython__(((struct __pyx_obj_13serialmanager_SerialManager *)__pyx_v_self), ((PyObject *)__pyx_v___pyx_state));

  /* function exit code */
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}

static PyObject *__pyx_pf_13serialmanager_13SerialManager_42__setstate_cython__(CYTHON_UNUSED struct __pyx_obj_13serialmanager_SerialManager *__pyx_v_self, CYTHON_UNUSED PyObject *__pyx_v___pyx_state) {
  PyObject *__pyx_r = NULL;
  __Pyx_TraceDeclarations
  __Pyx_RefNannyDeclarations
  PyObject *__pyx_t_1 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__setstate_cython__", 0);
  __Pyx_TraceCall("__setstate_cython__", __pyx_f[1], 3, 0, __PYX_ERR(1, 3, __pyx_L1_error));

  /* "(tree fragment)":4
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")             # <<<<<<<<<<<<<<
 */
  __Pyx_TraceLine(4,0,__PYX_ERR(1, 4, __pyx_L1_error))
  __pyx_t_1 = __Pyx_PyObject_Call(__pyx_builtin_TypeError, __pyx_tuple__9, NULL); if (unlikely(!__pyx_t_1)) __PYX_ERR(1, 4, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_Raise(__pyx_t_1, 0, 0, 0);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __PYX_ERR(1, 4, __pyx_L1_error)

  /* "(tree fragment)":3
 * def __reduce_cython__(self):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):             # <<<<<<<<<<<<<<
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 */

  /* function exit code */
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_AddTraceback("serialmanager.SerialManager.__setstate_cython__", __pyx_clineno, __pyx_lineno, __pyx_filename);
  __pyx_r = NULL;
  __Pyx_XGIVEREF(__pyx_r);
  __Pyx_TraceReturn(__pyx_r, 0);
  __Pyx_RefNannyFinishContext();
  return __pyx_r;
}
static struct __pyx_vtabstruct_13serialmanager_SerialManager __pyx_vtable_13serialmanager_SerialManager;

static PyObject *__pyx_tp_new_13serialmanager_SerialManager(PyTypeObject *t, PyObject *a, PyObject *k) {
  struct __pyx_obj_13serialmanager_SerialManager *p;
  PyObject *o;
  if (likely((t->tp_flags & Py_TPFLAGS_IS_ABSTRACT) == 0)) {
    o = (*t->tp_alloc)(t, 0);
  } else {
    o = (PyObject *) PyBaseObject_Type.tp_new(t, __pyx_empty_tuple, 0);
  }
  if (unlikely(!o)) return 0;
  p = ((struct __pyx_obj_13serialmanager_SerialManager *)o);
  p->__pyx_vtab = __pyx_vtabptr_13serialmanager_SerialManager;
  p->name = ((PyObject*)Py_None); Py_INCREF(Py_None);
  p->ser = Py_None; Py_INCREF(Py_None);
  if (unlikely(__pyx_pw_13serialmanager_13SerialManager_1__cinit__(o, a, k) < 0)) goto bad;
  return o;
  bad:
  Py_DECREF(o); o = 0;
  return NULL;
}

static void __pyx_tp_dealloc_13serialmanager_SerialManager(PyObject *o) {
  struct __pyx_obj_13serialmanager_SerialManager *p = (struct __pyx_obj_13serialmanager_SerialManager *)o;
  #if CYTHON_USE_TP_FINALIZE
  if (unlikely(PyType_HasFeature(Py_TYPE(o), Py_TPFLAGS_HAVE_FINALIZE) && Py_TYPE(o)->tp_finalize) && !_PyGC_FINALIZED(o)) {
    if (PyObject_CallFinalizerFromDealloc(o)) return;
  }
  #endif
  PyObject_GC_UnTrack(o);
  Py_CLEAR(p->name);
  Py_CLEAR(p->ser);
  (*Py_TYPE(o)->tp_free)(o);
}

static int __pyx_tp_traverse_13serialmanager_SerialManager(PyObject *o, visitproc v, void *a) {
  int e;
  struct __pyx_obj_13serialmanager_SerialManager *p = (struct __pyx_obj_13serialmanager_SerialManager *)o;
  if (p->ser) {
    e = (*v)(p->ser, a); if (e) return e;
  }
  return 0;
}

static int __pyx_tp_clear_13serialmanager_SerialManager(PyObject *o) {
  PyObject* tmp;
  struct __pyx_obj_13serialmanager_SerialManager *p = (struct __pyx_obj_13serialmanager_SerialManager *)o;
  tmp = ((PyObject*)p->ser);
  p->ser = Py_None; Py_INCREF(Py_None);
  Py_XDECREF(tmp);
  return 0;
}

static PyObject *__pyx_getprop_13serialmanager_13SerialManager_name(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_13serialmanager_13SerialManager_4name_1__get__(o);
}

static int __pyx_setprop_13serialmanager_13SerialManager_name(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_13serialmanager_13SerialManager_4name_3__set__(o, v);
  }
  else {
    return __pyx_pw_13serialmanager_13SerialManager_4name_5__del__(o);
  }
}

static PyObject *__pyx_getprop_13serialmanager_13SerialManager_start_marker(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_13serialmanager_13SerialManager_12start_marker_1__get__(o);
}

static int __pyx_setprop_13serialmanager_13SerialManager_start_marker(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_13serialmanager_13SerialManager_12start_marker_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_13serialmanager_13SerialManager_end_marker(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_13serialmanager_13SerialManager_10end_marker_1__get__(o);
}

static int __pyx_setprop_13serialmanager_13SerialManager_end_marker(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_13serialmanager_13SerialManager_10end_marker_3__set__(o, v);
  }
  else {
    PyErr_SetString(PyExc_NotImplementedError, "__del__");
    return -1;
  }
}

static PyObject *__pyx_getprop_13serialmanager_13SerialManager_ser(PyObject *o, CYTHON_UNUSED void *x) {
  return __pyx_pw_13serialmanager_13SerialManager_3ser_1__get__(o);
}

static int __pyx_setprop_13serialmanager_13SerialManager_ser(PyObject *o, PyObject *v, CYTHON_UNUSED void *x) {
  if (v) {
    return __pyx_pw_13serialmanager_13SerialManager_3ser_3__set__(o, v);
  }
  else {
    return __pyx_pw_13serialmanager_13SerialManager_3ser_5__del__(o);
  }
}

static PyMethodDef __pyx_methods_13serialmanager_SerialManager[] = {
  {"close", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_3close, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_2close},
  {"inWaiting", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_5inWaiting, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_4inWaiting},
  {"waitard", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_7waitard, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_6waitard},
  {"waitard_nano", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_9waitard_nano, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_8waitard_nano},
  {"waitard_nano2", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_11waitard_nano2, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_10waitard_nano2},
  {"waitard_mega", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_13waitard_mega, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_12waitard_mega},
  {"led_co", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_15led_co, METH_O, __pyx_doc_13serialmanager_13SerialManager_14led_co},
  {"takempx", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_17takempx, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_16takempx},
  {"dajetutta", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_19dajetutta, METH_O, __pyx_doc_13serialmanager_13SerialManager_18dajetutta},
  {"move_order", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_21move_order, METH_O, __pyx_doc_13serialmanager_13SerialManager_20move_order},
  {"react_results_final", (PyCFunction)(void*)(PyCFunctionWithKeywords)__pyx_pw_13serialmanager_13SerialManager_23react_results_final, METH_VARARGS|METH_KEYWORDS, __pyx_doc_13serialmanager_13SerialManager_22react_results_final},
  {"test_for_movement", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_25test_for_movement, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_24test_for_movement},
  {"test_for_stop", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_27test_for_stop, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_26test_for_stop},
  {"prendiprendi", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_29prendiprendi, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_28prendiprendi},
  {"send_reset", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_31send_reset, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_30send_reset},
  {"gamepad2", (PyCFunction)(void*)(PyCFunctionWithKeywords)__pyx_pw_13serialmanager_13SerialManager_33gamepad2, METH_VARARGS|METH_KEYWORDS, __pyx_doc_13serialmanager_13SerialManager_32gamepad2},
  {"gamepad", (PyCFunction)(void*)(PyCFunctionWithKeywords)__pyx_pw_13serialmanager_13SerialManager_35gamepad, METH_VARARGS|METH_KEYWORDS, __pyx_doc_13serialmanager_13SerialManager_34gamepad},
  {"prova1", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_37prova1, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_36prova1},
  {"sendled", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_39sendled, METH_O, __pyx_doc_13serialmanager_13SerialManager_38sendled},
  {"__reduce_cython__", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_41__reduce_cython__, METH_NOARGS, __pyx_doc_13serialmanager_13SerialManager_40__reduce_cython__},
  {"__setstate_cython__", (PyCFunction)__pyx_pw_13serialmanager_13SerialManager_43__setstate_cython__, METH_O, __pyx_doc_13serialmanager_13SerialManager_42__setstate_cython__},
  {0, 0, 0, 0}
};

static struct PyGetSetDef __pyx_getsets_13serialmanager_SerialManager[] = {
  {(char *)"name", __pyx_getprop_13serialmanager_13SerialManager_name, __pyx_setprop_13serialmanager_13SerialManager_name, (char *)"name: unicode", 0},
  {(char *)"start_marker", __pyx_getprop_13serialmanager_13SerialManager_start_marker, __pyx_setprop_13serialmanager_13SerialManager_start_marker, (char *)"start_marker: 'int'", 0},
  {(char *)"end_marker", __pyx_getprop_13serialmanager_13SerialManager_end_marker, __pyx_setprop_13serialmanager_13SerialManager_end_marker, (char *)"end_marker: 'int'", 0},
  {(char *)"ser", __pyx_getprop_13serialmanager_13SerialManager_ser, __pyx_setprop_13serialmanager_13SerialManager_ser, (char *)"ser: object", 0},
  {0, 0, 0, 0, 0}
};

static PyTypeObject __pyx_type_13serialmanager_SerialManager = {
  PyVarObject_HEAD_INIT(0, 0)
  "serialmanager.SerialManager", /*tp_name*/
  sizeof(struct __pyx_obj_13serialmanager_SerialManager), /*tp_basicsize*/
  0, /*tp_itemsize*/
  __pyx_tp_dealloc_13serialmanager_SerialManager, /*tp_dealloc*/
  #if PY_VERSION_HEX < 0x030800b4
  0, /*tp_print*/
  #endif
  #if PY_VERSION_HEX >= 0x030800b4
  0, /*tp_vectorcall_offset*/
  #endif
  0, /*tp_getattr*/
  0, /*tp_setattr*/
  #if PY_MAJOR_VERSION < 3
  0, /*tp_compare*/
  #endif
  #if PY_MAJOR_VERSION >= 3
  0, /*tp_as_async*/
  #endif
  0, /*tp_repr*/
  0, /*tp_as_number*/
  0, /*tp_as_sequence*/
  0, /*tp_as_mapping*/
  0, /*tp_hash*/
  0, /*tp_call*/
  0, /*tp_str*/
  0, /*tp_getattro*/
  0, /*tp_setattro*/
  0, /*tp_as_buffer*/
  Py_TPFLAGS_DEFAULT|Py_TPFLAGS_HAVE_VERSION_TAG|Py_TPFLAGS_CHECKTYPES|Py_TPFLAGS_HAVE_NEWBUFFER|Py_TPFLAGS_BASETYPE|Py_TPFLAGS_HAVE_GC, /*tp_flags*/
  0, /*tp_doc*/
  __pyx_tp_traverse_13serialmanager_SerialManager, /*tp_traverse*/
  __pyx_tp_clear_13serialmanager_SerialManager, /*tp_clear*/
  0, /*tp_richcompare*/
  0, /*tp_weaklistoffset*/
  0, /*tp_iter*/
  0, /*tp_iternext*/
  __pyx_methods_13serialmanager_SerialManager, /*tp_methods*/
  0, /*tp_members*/
  __pyx_getsets_13serialmanager_SerialManager, /*tp_getset*/
  0, /*tp_base*/
  0, /*tp_dict*/
  0, /*tp_descr_get*/
  0, /*tp_descr_set*/
  0, /*tp_dictoffset*/
  0, /*tp_init*/
  0, /*tp_alloc*/
  __pyx_tp_new_13serialmanager_SerialManager, /*tp_new*/
  0, /*tp_free*/
  0, /*tp_is_gc*/
  0, /*tp_bases*/
  0, /*tp_mro*/
  0, /*tp_cache*/
  0, /*tp_subclasses*/
  0, /*tp_weaklist*/
  0, /*tp_del*/
  0, /*tp_version_tag*/
  #if PY_VERSION_HEX >= 0x030400a1
  0, /*tp_finalize*/
  #endif
  #if PY_VERSION_HEX >= 0x030800b1 && (!CYTHON_COMPILING_IN_PYPY || PYPY_VERSION_NUM >= 0x07030800)
  0, /*tp_vectorcall*/
  #endif
  #if PY_VERSION_HEX >= 0x030800b4 && PY_VERSION_HEX < 0x03090000
  0, /*tp_print*/
  #endif
  #if CYTHON_COMPILING_IN_PYPY && PY_VERSION_HEX >= 0x03090000
  0, /*tp_pypy_flags*/
  #endif
};

static PyMethodDef __pyx_methods[] = {
  {0, 0, 0, 0}
};

#if PY_MAJOR_VERSION >= 3
#if CYTHON_PEP489_MULTI_PHASE_INIT
static PyObject* __pyx_pymod_create(PyObject *spec, PyModuleDef *def); /*proto*/
static int __pyx_pymod_exec_serialmanager(PyObject* module); /*proto*/
static PyModuleDef_Slot __pyx_moduledef_slots[] = {
  {Py_mod_create, (void*)__pyx_pymod_create},
  {Py_mod_exec, (void*)__pyx_pymod_exec_serialmanager},
  {0, NULL}
};
#endif

static struct PyModuleDef __pyx_moduledef = {
    PyModuleDef_HEAD_INIT,
    "serialmanager",
    0, /* m_doc */
  #if CYTHON_PEP489_MULTI_PHASE_INIT
    0, /* m_size */
  #else
    -1, /* m_size */
  #endif
    __pyx_methods /* m_methods */,
  #if CYTHON_PEP489_MULTI_PHASE_INIT
    __pyx_moduledef_slots, /* m_slots */
  #else
    NULL, /* m_reload */
  #endif
    NULL, /* m_traverse */
    NULL, /* m_clear */
    NULL /* m_free */
};
#endif
#ifndef CYTHON_SMALL_CODE
#if defined(__clang__)
    #define CYTHON_SMALL_CODE
#elif defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 3))
    #define CYTHON_SMALL_CODE __attribute__((cold))
#else
    #define CYTHON_SMALL_CODE
#endif
#endif

static __Pyx_StringTabEntry __pyx_string_tab[] = {
  {&__pyx_kp_u_, __pyx_k_, sizeof(__pyx_k_), 0, 1, 0, 0},
  {&__pyx_kp_u_ENTER_GAMEPAD2, __pyx_k_ENTER_GAMEPAD2, sizeof(__pyx_k_ENTER_GAMEPAD2), 0, 1, 0, 0},
  {&__pyx_n_s_OimiManager, __pyx_k_OimiManager, sizeof(__pyx_k_OimiManager), 0, 0, 1, 1},
  {&__pyx_n_s_OimiProcess, __pyx_k_OimiProcess, sizeof(__pyx_k_OimiProcess), 0, 0, 1, 1},
  {&__pyx_n_s_OimiQueue, __pyx_k_OimiQueue, sizeof(__pyx_k_OimiQueue), 0, 0, 1, 1},
  {&__pyx_n_s_OimiStartProcess, __pyx_k_OimiStartProcess, sizeof(__pyx_k_OimiStartProcess), 0, 0, 1, 1},
  {&__pyx_kp_u_Reply_Received, __pyx_k_Reply_Received, sizeof(__pyx_k_Reply_Received), 0, 1, 0, 0},
  {&__pyx_kp_u_Reply_Received_happiness, __pyx_k_Reply_Received_happiness, sizeof(__pyx_k_Reply_Received_happiness), 0, 1, 0, 0},
  {&__pyx_kp_u_Reply_Received_take_one, __pyx_k_Reply_Received_take_one, sizeof(__pyx_k_Reply_Received_take_one), 0, 1, 0, 0},
  {&__pyx_kp_u_Reply_control_Received, __pyx_k_Reply_control_Received, sizeof(__pyx_k_Reply_control_Received), 0, 1, 0, 0},
  {&__pyx_kp_u_Reply_led_Received, __pyx_k_Reply_led_Received, sizeof(__pyx_k_Reply_led_Received), 0, 1, 0, 0},
  {&__pyx_kp_u_Reply_led_Received_arduino, __pyx_k_Reply_led_Received_arduino, sizeof(__pyx_k_Reply_led_Received_arduino), 0, 1, 0, 0},
  {&__pyx_kp_u_Sent_from_RASPI, __pyx_k_Sent_from_RASPI, sizeof(__pyx_k_Sent_from_RASPI), 0, 1, 0, 0},
  {&__pyx_kp_u_Sent_from_Raspi, __pyx_k_Sent_from_Raspi, sizeof(__pyx_k_Sent_from_Raspi), 0, 1, 0, 0},
  {&__pyx_kp_u_Sent_from_Raspi_2, __pyx_k_Sent_from_Raspi_2, sizeof(__pyx_k_Sent_from_Raspi_2), 0, 1, 0, 0},
  {&__pyx_kp_u_Sent_from_Raspi_TEST_PRESS, __pyx_k_Sent_from_Raspi_TEST_PRESS, sizeof(__pyx_k_Sent_from_Raspi_TEST_PRESS), 0, 1, 0, 0},
  {&__pyx_kp_u_Sent_from_Raspi_stop, __pyx_k_Sent_from_Raspi_stop, sizeof(__pyx_k_Sent_from_Raspi_stop), 0, 1, 0, 0},
  {&__pyx_kp_u_Sent_from_Raspi_take_front, __pyx_k_Sent_from_Raspi_take_front, sizeof(__pyx_k_Sent_from_Raspi_take_front), 0, 1, 0, 0},
  {&__pyx_n_s_SerialException, __pyx_k_SerialException, sizeof(__pyx_k_SerialException), 0, 0, 1, 1},
  {&__pyx_n_s_SerialManager, __pyx_k_SerialManager, sizeof(__pyx_k_SerialManager), 0, 0, 1, 1},
  {&__pyx_n_s_TypeError, __pyx_k_TypeError, sizeof(__pyx_k_TypeError), 0, 0, 1, 1},
  {&__pyx_kp_u_UTF_8, __pyx_k_UTF_8, sizeof(__pyx_k_UTF_8), 0, 1, 0, 0},
  {&__pyx_kp_b__3, __pyx_k__3, sizeof(__pyx_k__3), 0, 0, 0, 0},
  {&__pyx_kp_u__3, __pyx_k__3, sizeof(__pyx_k__3), 0, 1, 0, 0},
  {&__pyx_kp_u__4, __pyx_k__4, sizeof(__pyx_k__4), 0, 1, 0, 0},
  {&__pyx_kp_u__5, __pyx_k__5, sizeof(__pyx_k__5), 0, 1, 0, 0},
  {&__pyx_kp_u__6, __pyx_k__6, sizeof(__pyx_k__6), 0, 1, 0, 0},
  {&__pyx_n_s_ang_speed, __pyx_k_ang_speed, sizeof(__pyx_k_ang_speed), 0, 0, 1, 1},
  {&__pyx_n_s_angular_speed, __pyx_k_angular_speed, sizeof(__pyx_k_angular_speed), 0, 0, 1, 1},
  {&__pyx_n_s_animation, __pyx_k_animation, sizeof(__pyx_k_animation), 0, 0, 1, 1},
  {&__pyx_kp_u_ans1_is, __pyx_k_ans1_is, sizeof(__pyx_k_ans1_is), 0, 1, 0, 0},
  {&__pyx_kp_u_ans2_is, __pyx_k_ans2_is, sizeof(__pyx_k_ans2_is), 0, 1, 0, 0},
  {&__pyx_kp_u_ans3_is, __pyx_k_ans3_is, sizeof(__pyx_k_ans3_is), 0, 1, 0, 0},
  {&__pyx_kp_u_ans4_is, __pyx_k_ans4_is, sizeof(__pyx_k_ans4_is), 0, 1, 0, 0},
  {&__pyx_kp_u_ansel_is, __pyx_k_ansel_is, sizeof(__pyx_k_ansel_is), 0, 1, 0, 0},
  {&__pyx_kp_b_auto, __pyx_k_auto, sizeof(__pyx_k_auto), 0, 0, 0, 0},
  {&__pyx_kp_u_auto, __pyx_k_auto, sizeof(__pyx_k_auto), 0, 1, 0, 0},
  {&__pyx_n_u_autonomous, __pyx_k_autonomous, sizeof(__pyx_k_autonomous), 0, 1, 0, 1},
  {&__pyx_n_s_choosen_audio, __pyx_k_choosen_audio, sizeof(__pyx_k_choosen_audio), 0, 0, 1, 1},
  {&__pyx_n_s_cline_in_traceback, __pyx_k_cline_in_traceback, sizeof(__pyx_k_cline_in_traceback), 0, 0, 1, 1},
  {&__pyx_n_s_close, __pyx_k_close, sizeof(__pyx_k_close), 0, 0, 1, 1},
  {&__pyx_n_s_command, __pyx_k_command, sizeof(__pyx_k_command), 0, 0, 1, 1},
  {&__pyx_kp_u_command_again_is, __pyx_k_command_again_is, sizeof(__pyx_k_command_again_is), 0, 1, 0, 0},
  {&__pyx_kp_u_command_has_been_send_comma_is, __pyx_k_command_has_been_send_comma_is, sizeof(__pyx_k_command_has_been_send_comma_is), 0, 1, 0, 0},
  {&__pyx_kp_u_command_to_send_for_gamepad_is, __pyx_k_command_to_send_for_gamepad_is, sizeof(__pyx_k_command_to_send_for_gamepad_is), 0, 1, 0, 0},
  {&__pyx_kp_u_command_to_send_is, __pyx_k_command_to_send_is, sizeof(__pyx_k_command_to_send_is), 0, 1, 0, 0},
  {&__pyx_n_s_decode, __pyx_k_decode, sizeof(__pyx_k_decode), 0, 0, 1, 1},
  {&__pyx_n_s_encode, __pyx_k_encode, sizeof(__pyx_k_encode), 0, 0, 1, 1},
  {&__pyx_n_s_end_marker, __pyx_k_end_marker, sizeof(__pyx_k_end_marker), 0, 0, 1, 1},
  {&__pyx_n_u_enthusiast, __pyx_k_enthusiast, sizeof(__pyx_k_enthusiast), 0, 1, 0, 1},
  {&__pyx_kp_u_enthusiast_2, __pyx_k_enthusiast_2, sizeof(__pyx_k_enthusiast_2), 0, 1, 0, 0},
  {&__pyx_n_s_find, __pyx_k_find, sizeof(__pyx_k_find), 0, 0, 1, 1},
  {&__pyx_n_u_follow, __pyx_k_follow, sizeof(__pyx_k_follow), 0, 1, 0, 1},
  {&__pyx_kp_b_follow_2, __pyx_k_follow_2, sizeof(__pyx_k_follow_2), 0, 0, 0, 0},
  {&__pyx_n_s_for_speed, __pyx_k_for_speed, sizeof(__pyx_k_for_speed), 0, 0, 1, 1},
  {&__pyx_n_s_format, __pyx_k_format, sizeof(__pyx_k_format), 0, 0, 1, 1},
  {&__pyx_n_s_forward, __pyx_k_forward, sizeof(__pyx_k_forward), 0, 0, 1, 1},
  {&__pyx_n_s_forward_speed, __pyx_k_forward_speed, sizeof(__pyx_k_forward_speed), 0, 0, 1, 1},
  {&__pyx_n_s_getstate, __pyx_k_getstate, sizeof(__pyx_k_getstate), 0, 0, 1, 1},
  {&__pyx_n_u_gobac, __pyx_k_gobac, sizeof(__pyx_k_gobac), 0, 1, 0, 1},
  {&__pyx_kp_u_gobac_2, __pyx_k_gobac_2, sizeof(__pyx_k_gobac_2), 0, 1, 0, 0},
  {&__pyx_n_u_golef, __pyx_k_golef, sizeof(__pyx_k_golef), 0, 1, 0, 1},
  {&__pyx_kp_u_golef_2, __pyx_k_golef_2, sizeof(__pyx_k_golef_2), 0, 1, 0, 0},
  {&__pyx_n_u_gorig, __pyx_k_gorig, sizeof(__pyx_k_gorig), 0, 1, 0, 1},
  {&__pyx_kp_u_gorig_2, __pyx_k_gorig_2, sizeof(__pyx_k_gorig_2), 0, 1, 0, 0},
  {&__pyx_n_u_gostra, __pyx_k_gostra, sizeof(__pyx_k_gostra), 0, 1, 0, 1},
  {&__pyx_kp_u_gostra_2, __pyx_k_gostra_2, sizeof(__pyx_k_gostra_2), 0, 1, 0, 0},
  {&__pyx_n_u_happy, __pyx_k_happy, sizeof(__pyx_k_happy), 0, 1, 0, 1},
  {&__pyx_kp_u_happy_2, __pyx_k_happy_2, sizeof(__pyx_k_happy_2), 0, 1, 0, 0},
  {&__pyx_n_s_import, __pyx_k_import, sizeof(__pyx_k_import), 0, 0, 1, 1},
  {&__pyx_n_s_inWaiting, __pyx_k_inWaiting, sizeof(__pyx_k_inWaiting), 0, 0, 1, 1},
  {&__pyx_n_u_joystop, __pyx_k_joystop, sizeof(__pyx_k_joystop), 0, 1, 0, 1},
  {&__pyx_kp_u_joystop_2, __pyx_k_joystop_2, sizeof(__pyx_k_joystop_2), 0, 1, 0, 0},
  {&__pyx_n_s_led_col, __pyx_k_led_col, sizeof(__pyx_k_led_col), 0, 0, 1, 1},
  {&__pyx_n_s_main, __pyx_k_main, sizeof(__pyx_k_main), 0, 0, 1, 1},
  {&__pyx_n_s_map, __pyx_k_map, sizeof(__pyx_k_map), 0, 0, 1, 1},
  {&__pyx_n_s_move_comm, __pyx_k_move_comm, sizeof(__pyx_k_move_comm), 0, 0, 1, 1},
  {&__pyx_n_s_name, __pyx_k_name, sizeof(__pyx_k_name), 0, 0, 1, 1},
  {&__pyx_n_s_name_2, __pyx_k_name_2, sizeof(__pyx_k_name_2), 0, 0, 1, 1},
  {&__pyx_kp_s_no_default___reduce___due_to_non, __pyx_k_no_default___reduce___due_to_non, sizeof(__pyx_k_no_default___reduce___due_to_non), 0, 0, 1, 0},
  {&__pyx_n_u_normal, __pyx_k_normal, sizeof(__pyx_k_normal), 0, 1, 0, 1},
  {&__pyx_kp_u_normal_2, __pyx_k_normal_2, sizeof(__pyx_k_normal_2), 0, 1, 0, 0},
  {&__pyx_n_s_oimi_process, __pyx_k_oimi_process, sizeof(__pyx_k_oimi_process), 0, 0, 1, 1},
  {&__pyx_n_s_print, __pyx_k_print, sizeof(__pyx_k_print), 0, 0, 1, 1},
  {&__pyx_n_s_pyx_vtable, __pyx_k_pyx_vtable, sizeof(__pyx_k_pyx_vtable), 0, 0, 1, 1},
  {&__pyx_n_s_re, __pyx_k_re, sizeof(__pyx_k_re), 0, 0, 1, 1},
  {&__pyx_n_s_read, __pyx_k_read, sizeof(__pyx_k_read), 0, 0, 1, 1},
  {&__pyx_n_s_reduce, __pyx_k_reduce, sizeof(__pyx_k_reduce), 0, 0, 1, 1},
  {&__pyx_n_s_reduce_cython, __pyx_k_reduce_cython, sizeof(__pyx_k_reduce_cython), 0, 0, 1, 1},
  {&__pyx_n_s_reduce_ex, __pyx_k_reduce_ex, sizeof(__pyx_k_reduce_ex), 0, 0, 1, 1},
  {&__pyx_n_u_sad, __pyx_k_sad, sizeof(__pyx_k_sad), 0, 1, 0, 1},
  {&__pyx_kp_u_sad_2, __pyx_k_sad_2, sizeof(__pyx_k_sad_2), 0, 1, 0, 0},
  {&__pyx_n_s_ser, __pyx_k_ser, sizeof(__pyx_k_ser), 0, 0, 1, 1},
  {&__pyx_n_s_serial, __pyx_k_serial, sizeof(__pyx_k_serial), 0, 0, 1, 1},
  {&__pyx_n_s_serialutil, __pyx_k_serialutil, sizeof(__pyx_k_serialutil), 0, 0, 1, 1},
  {&__pyx_n_s_setstate, __pyx_k_setstate, sizeof(__pyx_k_setstate), 0, 0, 1, 1},
  {&__pyx_n_s_setstate_cython, __pyx_k_setstate_cython, sizeof(__pyx_k_setstate_cython), 0, 0, 1, 1},
  {&__pyx_n_u_simplygo2, __pyx_k_simplygo2, sizeof(__pyx_k_simplygo2), 0, 1, 0, 1},
  {&__pyx_kp_u_simplygo2_2, __pyx_k_simplygo2_2, sizeof(__pyx_k_simplygo2_2), 0, 1, 0, 0},
  {&__pyx_n_s_sleep, __pyx_k_sleep, sizeof(__pyx_k_sleep), 0, 0, 1, 1},
  {&__pyx_n_s_split, __pyx_k_split, sizeof(__pyx_k_split), 0, 0, 1, 1},
  {&__pyx_n_s_start_marker, __pyx_k_start_marker, sizeof(__pyx_k_start_marker), 0, 0, 1, 1},
  {&__pyx_n_s_strafe, __pyx_k_strafe, sizeof(__pyx_k_strafe), 0, 0, 1, 1},
  {&__pyx_n_s_strafe_speed, __pyx_k_strafe_speed, sizeof(__pyx_k_strafe_speed), 0, 0, 1, 1},
  {&__pyx_n_u_strict, __pyx_k_strict, sizeof(__pyx_k_strict), 0, 1, 0, 1},
  {&__pyx_n_s_test, __pyx_k_test, sizeof(__pyx_k_test), 0, 0, 1, 1},
  {&__pyx_n_s_time, __pyx_k_time, sizeof(__pyx_k_time), 0, 0, 1, 1},
  {&__pyx_n_s_write, __pyx_k_write, sizeof(__pyx_k_write), 0, 0, 1, 1},
  {0, 0, 0, 0, 0, 0, 0}
};
static CYTHON_SMALL_CODE int __Pyx_InitCachedBuiltins(void) {
  __pyx_builtin_print = __Pyx_GetBuiltinName(__pyx_n_s_print); if (!__pyx_builtin_print) __PYX_ERR(0, 54, __pyx_L1_error)
  __pyx_builtin_map = __Pyx_GetBuiltinName(__pyx_n_s_map); if (!__pyx_builtin_map) __PYX_ERR(0, 280, __pyx_L1_error)
  __pyx_builtin_TypeError = __Pyx_GetBuiltinName(__pyx_n_s_TypeError); if (!__pyx_builtin_TypeError) __PYX_ERR(1, 2, __pyx_L1_error)
  return 0;
  __pyx_L1_error:;
  return -1;
}

static CYTHON_SMALL_CODE int __Pyx_InitCachedConstants(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_InitCachedConstants", 0);

  /* "serialmanager.pyx":43
 * 		except:
 * 			pass
 * 		cc = cha.encode('UTF-8', 'strict')             # <<<<<<<<<<<<<<
 * 		return cc
 * 
 */
  __pyx_tuple__2 = PyTuple_Pack(2, __pyx_kp_u_UTF_8, __pyx_n_u_strict); if (unlikely(!__pyx_tuple__2)) __PYX_ERR(0, 43, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_tuple__2);
  __Pyx_GIVEREF(__pyx_tuple__2);

  /* "serialmanager.pyx":391
 * 	def gamepad2(self,move_comm,strafe,forward):
 * 		'''without angular'''
 * 		print("---> ENTER GAMEPAD2")             # <<<<<<<<<<<<<<
 * 		end = '>'
 * 		if move_comm == "gostra":
 */
  __pyx_tuple__7 = PyTuple_Pack(1, __pyx_kp_u_ENTER_GAMEPAD2); if (unlikely(!__pyx_tuple__7)) __PYX_ERR(0, 391, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_tuple__7);
  __Pyx_GIVEREF(__pyx_tuple__7);

  /* "(tree fragment)":2
 * def __reduce_cython__(self):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")             # <<<<<<<<<<<<<<
 * def __setstate_cython__(self, __pyx_state):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 */
  __pyx_tuple__8 = PyTuple_Pack(1, __pyx_kp_s_no_default___reduce___due_to_non); if (unlikely(!__pyx_tuple__8)) __PYX_ERR(1, 2, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_tuple__8);
  __Pyx_GIVEREF(__pyx_tuple__8);

  /* "(tree fragment)":4
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")
 * def __setstate_cython__(self, __pyx_state):
 *     raise TypeError("no default __reduce__ due to non-trivial __cinit__")             # <<<<<<<<<<<<<<
 */
  __pyx_tuple__9 = PyTuple_Pack(1, __pyx_kp_s_no_default___reduce___due_to_non); if (unlikely(!__pyx_tuple__9)) __PYX_ERR(1, 4, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_tuple__9);
  __Pyx_GIVEREF(__pyx_tuple__9);
  __Pyx_RefNannyFinishContext();
  return 0;
  __pyx_L1_error:;
  __Pyx_RefNannyFinishContext();
  return -1;
}

static CYTHON_SMALL_CODE int __Pyx_InitGlobals(void) {
  if (__Pyx_InitStrings(__pyx_string_tab) < 0) __PYX_ERR(0, 1, __pyx_L1_error);
  __pyx_int_0 = PyInt_FromLong(0); if (unlikely(!__pyx_int_0)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_int_1 = PyInt_FromLong(1); if (unlikely(!__pyx_int_1)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_int_2 = PyInt_FromLong(2); if (unlikely(!__pyx_int_2)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_int_3 = PyInt_FromLong(3); if (unlikely(!__pyx_int_3)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_int_4 = PyInt_FromLong(4); if (unlikely(!__pyx_int_4)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_int_5 = PyInt_FromLong(5); if (unlikely(!__pyx_int_5)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_int_6 = PyInt_FromLong(6); if (unlikely(!__pyx_int_6)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_int_7 = PyInt_FromLong(7); if (unlikely(!__pyx_int_7)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_int_8 = PyInt_FromLong(8); if (unlikely(!__pyx_int_8)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_int_neg_1 = PyInt_FromLong(-1); if (unlikely(!__pyx_int_neg_1)) __PYX_ERR(0, 1, __pyx_L1_error)
  return 0;
  __pyx_L1_error:;
  return -1;
}

static CYTHON_SMALL_CODE int __Pyx_modinit_global_init_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_variable_export_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_function_export_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_type_init_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_type_import_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_variable_import_code(void); /*proto*/
static CYTHON_SMALL_CODE int __Pyx_modinit_function_import_code(void); /*proto*/

static int __Pyx_modinit_global_init_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_global_init_code", 0);
  /*--- Global init code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}

static int __Pyx_modinit_variable_export_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_variable_export_code", 0);
  /*--- Variable export code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}

static int __Pyx_modinit_function_export_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_function_export_code", 0);
  /*--- Function export code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}

static int __Pyx_modinit_type_init_code(void) {
  __Pyx_RefNannyDeclarations
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannySetupContext("__Pyx_modinit_type_init_code", 0);
  /*--- Type init code ---*/
  __pyx_vtabptr_13serialmanager_SerialManager = &__pyx_vtable_13serialmanager_SerialManager;
  __pyx_vtable_13serialmanager_SerialManager.send_to_arduino = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, char *))__pyx_f_13serialmanager_13SerialManager_send_to_arduino;
  __pyx_vtable_13serialmanager_SerialManager.receive_from_arduino = (char *(*)(struct __pyx_obj_13serialmanager_SerialManager *))__pyx_f_13serialmanager_13SerialManager_receive_from_arduino;
  __pyx_vtable_13serialmanager_SerialManager.waiting_arduino = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *))__pyx_f_13serialmanager_13SerialManager_waiting_arduino;
  __pyx_vtable_13serialmanager_SerialManager.waiting_arduino_nano = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *))__pyx_f_13serialmanager_13SerialManager_waiting_arduino_nano;
  __pyx_vtable_13serialmanager_SerialManager.waiting_arduino_nano2 = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *))__pyx_f_13serialmanager_13SerialManager_waiting_arduino_nano2;
  __pyx_vtable_13serialmanager_SerialManager.waiting_arduino_mega = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *))__pyx_f_13serialmanager_13SerialManager_waiting_arduino_mega;
  __pyx_vtable_13serialmanager_SerialManager.close = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, int __pyx_skip_dispatch))__pyx_f_13serialmanager_13SerialManager_close;
  __pyx_vtable_13serialmanager_SerialManager.inWaiting = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, int __pyx_skip_dispatch))__pyx_f_13serialmanager_13SerialManager_inWaiting;
  __pyx_vtable_13serialmanager_SerialManager.led_controller = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, char *))__pyx_f_13serialmanager_13SerialManager_led_controller;
  __pyx_vtable_13serialmanager_SerialManager.prendi = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *))__pyx_f_13serialmanager_13SerialManager_prendi;
  __pyx_vtable_13serialmanager_SerialManager.take_front = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *))__pyx_f_13serialmanager_13SerialManager_take_front;
  __pyx_vtable_13serialmanager_SerialManager.daje = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *))__pyx_f_13serialmanager_13SerialManager_daje;
  __pyx_vtable_13serialmanager_SerialManager.tell_to_stop = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, char *))__pyx_f_13serialmanager_13SerialManager_tell_to_stop;
  __pyx_vtable_13serialmanager_SerialManager.tell_to_move3 = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *))__pyx_f_13serialmanager_13SerialManager_tell_to_move3;
  __pyx_vtable_13serialmanager_SerialManager.movement_command = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *))__pyx_f_13serialmanager_13SerialManager_movement_command;
  __pyx_vtable_13serialmanager_SerialManager.tell_to_move = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *))__pyx_f_13serialmanager_13SerialManager_tell_to_move;
  __pyx_vtable_13serialmanager_SerialManager.tell_to_move2 = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *))__pyx_f_13serialmanager_13SerialManager_tell_to_move2;
  __pyx_vtable_13serialmanager_SerialManager.cgamepad = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, char *, float, float, float))__pyx_f_13serialmanager_13SerialManager_cgamepad;
  __pyx_vtable_13serialmanager_SerialManager.cprova1 = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *))__pyx_f_13serialmanager_13SerialManager_cprova1;
  __pyx_vtable_13serialmanager_SerialManager.csendledcommand = (PyObject *(*)(struct __pyx_obj_13serialmanager_SerialManager *, PyObject *))__pyx_f_13serialmanager_13SerialManager_csendledcommand;
  if (PyType_Ready(&__pyx_type_13serialmanager_SerialManager) < 0) __PYX_ERR(0, 11, __pyx_L1_error)
  #if PY_VERSION_HEX < 0x030800B1
  __pyx_type_13serialmanager_SerialManager.tp_print = 0;
  #endif
  if ((CYTHON_USE_TYPE_SLOTS && CYTHON_USE_PYTYPE_LOOKUP) && likely(!__pyx_type_13serialmanager_SerialManager.tp_dictoffset && __pyx_type_13serialmanager_SerialManager.tp_getattro == PyObject_GenericGetAttr)) {
    __pyx_type_13serialmanager_SerialManager.tp_getattro = __Pyx_PyObject_GenericGetAttr;
  }
  if (__Pyx_SetVtable(__pyx_type_13serialmanager_SerialManager.tp_dict, __pyx_vtabptr_13serialmanager_SerialManager) < 0) __PYX_ERR(0, 11, __pyx_L1_error)
  if (PyObject_SetAttr(__pyx_m, __pyx_n_s_SerialManager, (PyObject *)&__pyx_type_13serialmanager_SerialManager) < 0) __PYX_ERR(0, 11, __pyx_L1_error)
  if (__Pyx_setup_reduce((PyObject*)&__pyx_type_13serialmanager_SerialManager) < 0) __PYX_ERR(0, 11, __pyx_L1_error)
  __pyx_ptype_13serialmanager_SerialManager = &__pyx_type_13serialmanager_SerialManager;
  __Pyx_RefNannyFinishContext();
  return 0;
  __pyx_L1_error:;
  __Pyx_RefNannyFinishContext();
  return -1;
}

static int __Pyx_modinit_type_import_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_type_import_code", 0);
  /*--- Type import code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}

static int __Pyx_modinit_variable_import_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_variable_import_code", 0);
  /*--- Variable import code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}

static int __Pyx_modinit_function_import_code(void) {
  __Pyx_RefNannyDeclarations
  __Pyx_RefNannySetupContext("__Pyx_modinit_function_import_code", 0);
  /*--- Function import code ---*/
  __Pyx_RefNannyFinishContext();
  return 0;
}


#ifndef CYTHON_NO_PYINIT_EXPORT
#define __Pyx_PyMODINIT_FUNC PyMODINIT_FUNC
#elif PY_MAJOR_VERSION < 3
#ifdef __cplusplus
#define __Pyx_PyMODINIT_FUNC extern "C" void
#else
#define __Pyx_PyMODINIT_FUNC void
#endif
#else
#ifdef __cplusplus
#define __Pyx_PyMODINIT_FUNC extern "C" PyObject *
#else
#define __Pyx_PyMODINIT_FUNC PyObject *
#endif
#endif


#if PY_MAJOR_VERSION < 3
__Pyx_PyMODINIT_FUNC initserialmanager(void) CYTHON_SMALL_CODE; /*proto*/
__Pyx_PyMODINIT_FUNC initserialmanager(void)
#else
__Pyx_PyMODINIT_FUNC PyInit_serialmanager(void) CYTHON_SMALL_CODE; /*proto*/
__Pyx_PyMODINIT_FUNC PyInit_serialmanager(void)
#if CYTHON_PEP489_MULTI_PHASE_INIT
{
  return PyModuleDef_Init(&__pyx_moduledef);
}
static CYTHON_SMALL_CODE int __Pyx_check_single_interpreter(void) {
    #if PY_VERSION_HEX >= 0x030700A1
    static PY_INT64_T main_interpreter_id = -1;
    PY_INT64_T current_id = PyInterpreterState_GetID(PyThreadState_Get()->interp);
    if (main_interpreter_id == -1) {
        main_interpreter_id = current_id;
        return (unlikely(current_id == -1)) ? -1 : 0;
    } else if (unlikely(main_interpreter_id != current_id))
    #else
    static PyInterpreterState *main_interpreter = NULL;
    PyInterpreterState *current_interpreter = PyThreadState_Get()->interp;
    if (!main_interpreter) {
        main_interpreter = current_interpreter;
    } else if (unlikely(main_interpreter != current_interpreter))
    #endif
    {
        PyErr_SetString(
            PyExc_ImportError,
            "Interpreter change detected - this module can only be loaded into one interpreter per process.");
        return -1;
    }
    return 0;
}
static CYTHON_SMALL_CODE int __Pyx_copy_spec_to_module(PyObject *spec, PyObject *moddict, const char* from_name, const char* to_name, int allow_none) {
    PyObject *value = PyObject_GetAttrString(spec, from_name);
    int result = 0;
    if (likely(value)) {
        if (allow_none || value != Py_None) {
            result = PyDict_SetItemString(moddict, to_name, value);
        }
        Py_DECREF(value);
    } else if (PyErr_ExceptionMatches(PyExc_AttributeError)) {
        PyErr_Clear();
    } else {
        result = -1;
    }
    return result;
}
static CYTHON_SMALL_CODE PyObject* __pyx_pymod_create(PyObject *spec, CYTHON_UNUSED PyModuleDef *def) {
    PyObject *module = NULL, *moddict, *modname;
    if (__Pyx_check_single_interpreter())
        return NULL;
    if (__pyx_m)
        return __Pyx_NewRef(__pyx_m);
    modname = PyObject_GetAttrString(spec, "name");
    if (unlikely(!modname)) goto bad;
    module = PyModule_NewObject(modname);
    Py_DECREF(modname);
    if (unlikely(!module)) goto bad;
    moddict = PyModule_GetDict(module);
    if (unlikely(!moddict)) goto bad;
    if (unlikely(__Pyx_copy_spec_to_module(spec, moddict, "loader", "__loader__", 1) < 0)) goto bad;
    if (unlikely(__Pyx_copy_spec_to_module(spec, moddict, "origin", "__file__", 1) < 0)) goto bad;
    if (unlikely(__Pyx_copy_spec_to_module(spec, moddict, "parent", "__package__", 1) < 0)) goto bad;
    if (unlikely(__Pyx_copy_spec_to_module(spec, moddict, "submodule_search_locations", "__path__", 0) < 0)) goto bad;
    return module;
bad:
    Py_XDECREF(module);
    return NULL;
}


static CYTHON_SMALL_CODE int __pyx_pymod_exec_serialmanager(PyObject *__pyx_pyinit_module)
#endif
#endif
{
  __Pyx_TraceDeclarations
  PyObject *__pyx_t_1 = NULL;
  PyObject *__pyx_t_2 = NULL;
  int __pyx_lineno = 0;
  const char *__pyx_filename = NULL;
  int __pyx_clineno = 0;
  __Pyx_RefNannyDeclarations
  #if CYTHON_PEP489_MULTI_PHASE_INIT
  if (__pyx_m) {
    if (__pyx_m == __pyx_pyinit_module) return 0;
    PyErr_SetString(PyExc_RuntimeError, "Module 'serialmanager' has already been imported. Re-initialisation is not supported.");
    return -1;
  }
  #elif PY_MAJOR_VERSION >= 3
  if (__pyx_m) return __Pyx_NewRef(__pyx_m);
  #endif
  #if CYTHON_REFNANNY
__Pyx_RefNanny = __Pyx_RefNannyImportAPI("refnanny");
if (!__Pyx_RefNanny) {
  PyErr_Clear();
  __Pyx_RefNanny = __Pyx_RefNannyImportAPI("Cython.Runtime.refnanny");
  if (!__Pyx_RefNanny)
      Py_FatalError("failed to import 'refnanny' module");
}
#endif
  __Pyx_RefNannySetupContext("__Pyx_PyMODINIT_FUNC PyInit_serialmanager(void)", 0);
  if (__Pyx_check_binary_version() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #ifdef __Pxy_PyFrame_Initialize_Offsets
  __Pxy_PyFrame_Initialize_Offsets();
  #endif
  __pyx_empty_tuple = PyTuple_New(0); if (unlikely(!__pyx_empty_tuple)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_empty_bytes = PyBytes_FromStringAndSize("", 0); if (unlikely(!__pyx_empty_bytes)) __PYX_ERR(0, 1, __pyx_L1_error)
  __pyx_empty_unicode = PyUnicode_FromStringAndSize("", 0); if (unlikely(!__pyx_empty_unicode)) __PYX_ERR(0, 1, __pyx_L1_error)
  #ifdef __Pyx_CyFunction_USED
  if (__pyx_CyFunction_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  #ifdef __Pyx_FusedFunction_USED
  if (__pyx_FusedFunction_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  #ifdef __Pyx_Coroutine_USED
  if (__pyx_Coroutine_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  #ifdef __Pyx_Generator_USED
  if (__pyx_Generator_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  #ifdef __Pyx_AsyncGen_USED
  if (__pyx_AsyncGen_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  #ifdef __Pyx_StopAsyncIteration_USED
  if (__pyx_StopAsyncIteration_init() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  /*--- Library function declarations ---*/
  /*--- Threads initialization code ---*/
  #if defined(WITH_THREAD) && PY_VERSION_HEX < 0x030700F0 && defined(__PYX_FORCE_INIT_THREADS) && __PYX_FORCE_INIT_THREADS
  PyEval_InitThreads();
  #endif
  /*--- Module creation code ---*/
  #if CYTHON_PEP489_MULTI_PHASE_INIT
  __pyx_m = __pyx_pyinit_module;
  Py_INCREF(__pyx_m);
  #else
  #if PY_MAJOR_VERSION < 3
  __pyx_m = Py_InitModule4("serialmanager", __pyx_methods, 0, 0, PYTHON_API_VERSION); Py_XINCREF(__pyx_m);
  #else
  __pyx_m = PyModule_Create(&__pyx_moduledef);
  #endif
  if (unlikely(!__pyx_m)) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  __pyx_d = PyModule_GetDict(__pyx_m); if (unlikely(!__pyx_d)) __PYX_ERR(0, 1, __pyx_L1_error)
  Py_INCREF(__pyx_d);
  __pyx_b = PyImport_AddModule(__Pyx_BUILTIN_MODULE_NAME); if (unlikely(!__pyx_b)) __PYX_ERR(0, 1, __pyx_L1_error)
  Py_INCREF(__pyx_b);
  __pyx_cython_runtime = PyImport_AddModule((char *) "cython_runtime"); if (unlikely(!__pyx_cython_runtime)) __PYX_ERR(0, 1, __pyx_L1_error)
  Py_INCREF(__pyx_cython_runtime);
  if (PyObject_SetAttrString(__pyx_m, "__builtins__", __pyx_b) < 0) __PYX_ERR(0, 1, __pyx_L1_error);
  /*--- Initialize various global constants etc. ---*/
  if (__Pyx_InitGlobals() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #if PY_MAJOR_VERSION < 3 && (__PYX_DEFAULT_STRING_ENCODING_IS_ASCII || __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT)
  if (__Pyx_init_sys_getdefaultencoding_params() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  if (__pyx_module_is_main_serialmanager) {
    if (PyObject_SetAttr(__pyx_m, __pyx_n_s_name_2, __pyx_n_s_main) < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  }
  #if PY_MAJOR_VERSION >= 3
  {
    PyObject *modules = PyImport_GetModuleDict(); if (unlikely(!modules)) __PYX_ERR(0, 1, __pyx_L1_error)
    if (!PyDict_GetItemString(modules, "serialmanager")) {
      if (unlikely(PyDict_SetItemString(modules, "serialmanager", __pyx_m) < 0)) __PYX_ERR(0, 1, __pyx_L1_error)
    }
  }
  #endif
  /*--- Builtin init code ---*/
  if (__Pyx_InitCachedBuiltins() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  /*--- Constants init code ---*/
  if (__Pyx_InitCachedConstants() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  /*--- Global type/function init code ---*/
  (void)__Pyx_modinit_global_init_code();
  (void)__Pyx_modinit_variable_export_code();
  (void)__Pyx_modinit_function_export_code();
  if (unlikely(__Pyx_modinit_type_init_code() < 0)) __PYX_ERR(0, 1, __pyx_L1_error)
  (void)__Pyx_modinit_type_import_code();
  (void)__Pyx_modinit_variable_import_code();
  (void)__Pyx_modinit_function_import_code();
  /*--- Execution code ---*/
  #if defined(__Pyx_Generator_USED) || defined(__Pyx_Coroutine_USED)
  if (__Pyx_patch_abc() < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  #endif
  __Pyx_TraceCall("__Pyx_PyMODINIT_FUNC PyInit_serialmanager(void)", __pyx_f[0], 1, 0, __PYX_ERR(0, 1, __pyx_L1_error));

  /* "serialmanager.pyx":3
 * #distutils: language=c++ cython: language_level=3
 * #import pyximport; pyximport.install()
 * import re, time             # <<<<<<<<<<<<<<
 * import serial
 * cimport serialmanager as sm
 */
  __Pyx_TraceLine(3,0,__PYX_ERR(0, 3, __pyx_L1_error))
  __pyx_t_1 = __Pyx_Import(__pyx_n_s_re, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 3, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_re, __pyx_t_1) < 0) __PYX_ERR(0, 3, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_1 = __Pyx_Import(__pyx_n_s_time, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 3, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_time, __pyx_t_1) < 0) __PYX_ERR(0, 3, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":4
 * #import pyximport; pyximport.install()
 * import re, time
 * import serial             # <<<<<<<<<<<<<<
 * cimport serialmanager as sm
 * from libc.stdlib cimport malloc, free
 */
  __Pyx_TraceLine(4,0,__PYX_ERR(0, 4, __pyx_L1_error))
  __pyx_t_1 = __Pyx_Import(__pyx_n_s_serial, 0, 0); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 4, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_serial, __pyx_t_1) < 0) __PYX_ERR(0, 4, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;

  /* "serialmanager.pyx":8
 * from libc.stdlib cimport malloc, free
 * from libc.string cimport strcpy, strlen
 * from oimi_process import OimiProcess, OimiStartProcess, OimiQueue, OimiManager             # <<<<<<<<<<<<<<
 * 
 * 
 */
  __Pyx_TraceLine(8,0,__PYX_ERR(0, 8, __pyx_L1_error))
  __pyx_t_1 = PyList_New(4); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 8, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  __Pyx_INCREF(__pyx_n_s_OimiProcess);
  __Pyx_GIVEREF(__pyx_n_s_OimiProcess);
  PyList_SET_ITEM(__pyx_t_1, 0, __pyx_n_s_OimiProcess);
  __Pyx_INCREF(__pyx_n_s_OimiStartProcess);
  __Pyx_GIVEREF(__pyx_n_s_OimiStartProcess);
  PyList_SET_ITEM(__pyx_t_1, 1, __pyx_n_s_OimiStartProcess);
  __Pyx_INCREF(__pyx_n_s_OimiQueue);
  __Pyx_GIVEREF(__pyx_n_s_OimiQueue);
  PyList_SET_ITEM(__pyx_t_1, 2, __pyx_n_s_OimiQueue);
  __Pyx_INCREF(__pyx_n_s_OimiManager);
  __Pyx_GIVEREF(__pyx_n_s_OimiManager);
  PyList_SET_ITEM(__pyx_t_1, 3, __pyx_n_s_OimiManager);
  __pyx_t_2 = __Pyx_Import(__pyx_n_s_oimi_process, __pyx_t_1, 0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 8, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_1 = __Pyx_ImportFrom(__pyx_t_2, __pyx_n_s_OimiProcess); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 8, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_OimiProcess, __pyx_t_1) < 0) __PYX_ERR(0, 8, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_1 = __Pyx_ImportFrom(__pyx_t_2, __pyx_n_s_OimiStartProcess); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 8, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_OimiStartProcess, __pyx_t_1) < 0) __PYX_ERR(0, 8, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_1 = __Pyx_ImportFrom(__pyx_t_2, __pyx_n_s_OimiQueue); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 8, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_OimiQueue, __pyx_t_1) < 0) __PYX_ERR(0, 8, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __pyx_t_1 = __Pyx_ImportFrom(__pyx_t_2, __pyx_n_s_OimiManager); if (unlikely(!__pyx_t_1)) __PYX_ERR(0, 8, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_1);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_OimiManager, __pyx_t_1) < 0) __PYX_ERR(0, 8, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_1); __pyx_t_1 = 0;
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;

  /* "serialmanager.pyx":23
 * 		self.ser = ser
 * 
 * 	cdef send_to_arduino(self, char* sendString):             # <<<<<<<<<<<<<<
 * 		#self.ser.write(bytes(sendString))
 * 		self.ser.write(sendString)
 */
  __Pyx_TraceLine(23,0,__PYX_ERR(0, 23, __pyx_L1_error))


  /* "serialmanager.pyx":28
 * 
 * 	'''read on serial correct string'''
 * 	cdef char* receive_from_arduino(self):             # <<<<<<<<<<<<<<
 * 		cdef char* z = 'z'
 * 		cdef char* ch = ''
 */
  __Pyx_TraceLine(28,0,__PYX_ERR(0, 28, __pyx_L1_error))


  /* "serialmanager.pyx":47
 * 
 * 	'''connection boot/reboot test communication'''
 * 	cdef waiting_arduino(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'hei'
 */
  __Pyx_TraceLine(47,0,__PYX_ERR(0, 47, __pyx_L1_error))


  /* "serialmanager.pyx":55
 * 			msg = self.receive_from_arduino()
 * 			print(msg)
 * 	cdef waiting_arduino_nano(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Nano1 is ready'
 */
  __Pyx_TraceLine(55,0,__PYX_ERR(0, 55, __pyx_L1_error))


  /* "serialmanager.pyx":63
 * 			msg = self.receive_from_arduino()
 * 			print(msg)
 * 	cdef waiting_arduino_nano2(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Nano2 is ready'
 */
  __Pyx_TraceLine(63,0,__PYX_ERR(0, 63, __pyx_L1_error))


  /* "serialmanager.pyx":71
 * 			msg = self.receive_from_arduino()
 * 			print(msg)
 * 	cdef waiting_arduino_mega(self):             # <<<<<<<<<<<<<<
 * 		cdef char* msg = ''
 * 		cdef char* right_msg = 'Mega is ready'
 */
  __Pyx_TraceLine(71,0,__PYX_ERR(0, 71, __pyx_L1_error))


  /* "serialmanager.pyx":80
 * 			print(msg)
 * 
 * 	cpdef close(self):             # <<<<<<<<<<<<<<
 * 		self.ser.close()
 * 
 */
  __Pyx_TraceLine(80,0,__PYX_ERR(0, 80, __pyx_L1_error))


  /* "serialmanager.pyx":83
 * 		self.ser.close()
 * 
 * 	cpdef inWaiting(self):             # <<<<<<<<<<<<<<
 * 		self.ser.inWaiting()
 * 
 */
  __Pyx_TraceLine(83,0,__PYX_ERR(0, 83, __pyx_L1_error))


  /* "serialmanager.pyx":86
 * 		self.ser.inWaiting()
 * 
 * 	cdef led_controller(self, char* result):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived
 */
  __Pyx_TraceLine(86,0,__PYX_ERR(0, 86, __pyx_L1_error))


  /* "serialmanager.pyx":100
 * 			print('Reply led Received{}'.format(dataReceived))
 * 
 * 	cdef prendi(self):             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived
 * 		startledtime = time.time()
 */
  __Pyx_TraceLine(100,0,__PYX_ERR(0, 100, __pyx_L1_error))


  /* "serialmanager.pyx":108
 * 		print('Reply led Received{}'.format(dataReceived))
 * 
 * 	cdef take_front(self):             # <<<<<<<<<<<<<<
 * 		cdef char* command = '<pp>'
 * 		cdef char* dataReceived = ''
 */
  __Pyx_TraceLine(108,0,__PYX_ERR(0, 108, __pyx_L1_error))


  /* "serialmanager.pyx":125
 * 		return ans
 * 
 * 	cdef daje(self,val):             # <<<<<<<<<<<<<<
 * 		cdef char* command0 = '<switchMod0>'
 * 		cdef char* command1 = '<switchMod1>'
 */
  __Pyx_TraceLine(125,0,__PYX_ERR(0, 125, __pyx_L1_error))


  /* "serialmanager.pyx":173
 * 		return'''
 * 
 * 	cdef tell_to_stop(self, char* command):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */
  __Pyx_TraceLine(173,0,__PYX_ERR(0, 173, __pyx_L1_error))


  /* "serialmanager.pyx":202
 * 		return 0
 * 
 * 	cdef tell_to_move3(self, command):             # <<<<<<<<<<<<<<
 * 		#cdef char* command = '<elaps>'
 * 		cdef bint waiting_for_reply = False
 */
  __Pyx_TraceLine(202,0,__PYX_ERR(0, 202, __pyx_L1_error))


  /* "serialmanager.pyx":222
 * 		return #ans1,ans2,ans3,ans4
 * 
 * 	cdef movement_command(self, command):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */
  __Pyx_TraceLine(222,0,__PYX_ERR(0, 222, __pyx_L1_error))


  /* "serialmanager.pyx":329
 * 			print('Reply led Received{}'.format(dataReceived))
 * 
 * 	cdef tell_to_move(self, comm):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */
  __Pyx_TraceLine(329,0,__PYX_ERR(0, 329, __pyx_L1_error))


  /* "serialmanager.pyx":347
 * 		return ans1,ans2,ans3
 * 
 * 	cdef tell_to_move2(self, command):             # <<<<<<<<<<<<<<
 * 		cdef bint waiting_for_reply = False
 * 		cdef char* dataReceived = ''
 */
  __Pyx_TraceLine(347,0,__PYX_ERR(0, 347, __pyx_L1_error))


  /* "serialmanager.pyx":420
 * 		print("ans2 is {}".format(ans2))
 * 
 * 	cdef cgamepad(self, char* command, float strafe, float forward, float angular):             # <<<<<<<<<<<<<<
 * 		cdef char* dataReceived = ''
 * 		cdef bint waiting_for_reply = False
 */
  __Pyx_TraceLine(420,0,__PYX_ERR(0, 420, __pyx_L1_error))


  /* "serialmanager.pyx":470
 * 		self.cgamepad(move_comm,strafe,forward,angular)
 * 
 * 	cdef cprova1(self):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived
 */
  __Pyx_TraceLine(470,0,__PYX_ERR(0, 470, __pyx_L1_error))


  /* "serialmanager.pyx":487
 * 		self.cprova1();
 * 
 * 	cdef csendledcommand(self, conferma):             # <<<<<<<<<<<<<<
 * 		cdef bint waitingForReply = False
 * 		cdef char* dataReceived = ''
 */
  __Pyx_TraceLine(487,0,__PYX_ERR(0, 487, __pyx_L1_error))


  /* "serialmanager.pyx":1
 * #distutils: language=c++ cython: language_level=3             # <<<<<<<<<<<<<<
 * #import pyximport; pyximport.install()
 * import re, time
 */
  __Pyx_TraceLine(1,0,__PYX_ERR(0, 1, __pyx_L1_error))
  __pyx_t_2 = __Pyx_PyDict_NewPresized(0); if (unlikely(!__pyx_t_2)) __PYX_ERR(0, 1, __pyx_L1_error)
  __Pyx_GOTREF(__pyx_t_2);
  if (PyDict_SetItem(__pyx_d, __pyx_n_s_test, __pyx_t_2) < 0) __PYX_ERR(0, 1, __pyx_L1_error)
  __Pyx_DECREF(__pyx_t_2); __pyx_t_2 = 0;
  __Pyx_TraceReturn(Py_None, 0);

  /*--- Wrapped vars code ---*/

  goto __pyx_L0;
  __pyx_L1_error:;
  __Pyx_XDECREF(__pyx_t_1);
  __Pyx_XDECREF(__pyx_t_2);
  if (__pyx_m) {
    if (__pyx_d) {
      __Pyx_AddTraceback("init serialmanager", __pyx_clineno, __pyx_lineno, __pyx_filename);
    }
    Py_CLEAR(__pyx_m);
  } else if (!PyErr_Occurred()) {
    PyErr_SetString(PyExc_ImportError, "init serialmanager");
  }
  __pyx_L0:;
  __Pyx_RefNannyFinishContext();
  #if CYTHON_PEP489_MULTI_PHASE_INIT
  return (__pyx_m != NULL) ? 0 : -1;
  #elif PY_MAJOR_VERSION >= 3
  return __pyx_m;
  #else
  return;
  #endif
}

/* --- Runtime support code --- */
/* Refnanny */
#if CYTHON_REFNANNY
static __Pyx_RefNannyAPIStruct *__Pyx_RefNannyImportAPI(const char *modname) {
    PyObject *m = NULL, *p = NULL;
    void *r = NULL;
    m = PyImport_ImportModule(modname);
    if (!m) goto end;
    p = PyObject_GetAttrString(m, "RefNannyAPI");
    if (!p) goto end;
    r = PyLong_AsVoidPtr(p);
end:
    Py_XDECREF(p);
    Py_XDECREF(m);
    return (__Pyx_RefNannyAPIStruct *)r;
}
#endif

/* PyObjectGetAttrStr */
#if CYTHON_USE_TYPE_SLOTS
static CYTHON_INLINE PyObject* __Pyx_PyObject_GetAttrStr(PyObject* obj, PyObject* attr_name) {
    PyTypeObject* tp = Py_TYPE(obj);
    if (likely(tp->tp_getattro))
        return tp->tp_getattro(obj, attr_name);
#if PY_MAJOR_VERSION < 3
    if (likely(tp->tp_getattr))
        return tp->tp_getattr(obj, PyString_AS_STRING(attr_name));
#endif
    return PyObject_GetAttr(obj, attr_name);
}
#endif

/* GetBuiltinName */
static PyObject *__Pyx_GetBuiltinName(PyObject *name) {
    PyObject* result = __Pyx_PyObject_GetAttrStr(__pyx_b, name);
    if (unlikely(!result)) {
        PyErr_Format(PyExc_NameError,
#if PY_MAJOR_VERSION >= 3
            "name '%U' is not defined", name);
#else
            "name '%.200s' is not defined", PyString_AS_STRING(name));
#endif
    }
    return result;
}

/* RaiseArgTupleInvalid */
static void __Pyx_RaiseArgtupleInvalid(
    const char* func_name,
    int exact,
    Py_ssize_t num_min,
    Py_ssize_t num_max,
    Py_ssize_t num_found)
{
    Py_ssize_t num_expected;
    const char *more_or_less;
    if (num_found < num_min) {
        num_expected = num_min;
        more_or_less = "at least";
    } else {
        num_expected = num_max;
        more_or_less = "at most";
    }
    if (exact) {
        more_or_less = "exactly";
    }
    PyErr_Format(PyExc_TypeError,
                 "%.200s() takes %.8s %" CYTHON_FORMAT_SSIZE_T "d positional argument%.1s (%" CYTHON_FORMAT_SSIZE_T "d given)",
                 func_name, more_or_less, num_expected,
                 (num_expected == 1) ? "" : "s", num_found);
}

/* RaiseDoubleKeywords */
static void __Pyx_RaiseDoubleKeywordsError(
    const char* func_name,
    PyObject* kw_name)
{
    PyErr_Format(PyExc_TypeError,
        #if PY_MAJOR_VERSION >= 3
        "%s() got multiple values for keyword argument '%U'", func_name, kw_name);
        #else
        "%s() got multiple values for keyword argument '%s'", func_name,
        PyString_AsString(kw_name));
        #endif
}

/* ParseKeywords */
static int __Pyx_ParseOptionalKeywords(
    PyObject *kwds,
    PyObject **argnames[],
    PyObject *kwds2,
    PyObject *values[],
    Py_ssize_t num_pos_args,
    const char* function_name)
{
    PyObject *key = 0, *value = 0;
    Py_ssize_t pos = 0;
    PyObject*** name;
    PyObject*** first_kw_arg = argnames + num_pos_args;
    while (PyDict_Next(kwds, &pos, &key, &value)) {
        name = first_kw_arg;
        while (*name && (**name != key)) name++;
        if (*name) {
            values[name-argnames] = value;
            continue;
        }
        name = first_kw_arg;
        #if PY_MAJOR_VERSION < 3
        if (likely(PyString_Check(key))) {
            while (*name) {
                if ((CYTHON_COMPILING_IN_PYPY || PyString_GET_SIZE(**name) == PyString_GET_SIZE(key))
                        && _PyString_Eq(**name, key)) {
                    values[name-argnames] = value;
                    break;
                }
                name++;
            }
            if (*name) continue;
            else {
                PyObject*** argname = argnames;
                while (argname != first_kw_arg) {
                    if ((**argname == key) || (
                            (CYTHON_COMPILING_IN_PYPY || PyString_GET_SIZE(**argname) == PyString_GET_SIZE(key))
                             && _PyString_Eq(**argname, key))) {
                        goto arg_passed_twice;
                    }
                    argname++;
                }
            }
        } else
        #endif
        if (likely(PyUnicode_Check(key))) {
            while (*name) {
                int cmp = (**name == key) ? 0 :
                #if !CYTHON_COMPILING_IN_PYPY && PY_MAJOR_VERSION >= 3
                    (__Pyx_PyUnicode_GET_LENGTH(**name) != __Pyx_PyUnicode_GET_LENGTH(key)) ? 1 :
                #endif
                    PyUnicode_Compare(**name, key);
                if (cmp < 0 && unlikely(PyErr_Occurred())) goto bad;
                if (cmp == 0) {
                    values[name-argnames] = value;
                    break;
                }
                name++;
            }
            if (*name) continue;
            else {
                PyObject*** argname = argnames;
                while (argname != first_kw_arg) {
                    int cmp = (**argname == key) ? 0 :
                    #if !CYTHON_COMPILING_IN_PYPY && PY_MAJOR_VERSION >= 3
                        (__Pyx_PyUnicode_GET_LENGTH(**argname) != __Pyx_PyUnicode_GET_LENGTH(key)) ? 1 :
                    #endif
                        PyUnicode_Compare(**argname, key);
                    if (cmp < 0 && unlikely(PyErr_Occurred())) goto bad;
                    if (cmp == 0) goto arg_passed_twice;
                    argname++;
                }
            }
        } else
            goto invalid_keyword_type;
        if (kwds2) {
            if (unlikely(PyDict_SetItem(kwds2, key, value))) goto bad;
        } else {
            goto invalid_keyword;
        }
    }
    return 0;
arg_passed_twice:
    __Pyx_RaiseDoubleKeywordsError(function_name, key);
    goto bad;
invalid_keyword_type:
    PyErr_Format(PyExc_TypeError,
        "%.200s() keywords must be strings", function_name);
    goto bad;
invalid_keyword:
    PyErr_Format(PyExc_TypeError,
    #if PY_MAJOR_VERSION < 3
        "%.200s() got an unexpected keyword argument '%.200s'",
        function_name, PyString_AsString(key));
    #else
        "%s() got an unexpected keyword argument '%U'",
        function_name, key);
    #endif
bad:
    return -1;
}

/* ArgTypeTest */
static int __Pyx__ArgTypeTest(PyObject *obj, PyTypeObject *type, const char *name, int exact)
{
    if (unlikely(!type)) {
        PyErr_SetString(PyExc_SystemError, "Missing type object");
        return 0;
    }
    else if (exact) {
        #if PY_MAJOR_VERSION == 2
        if ((type == &PyBaseString_Type) && likely(__Pyx_PyBaseString_CheckExact(obj))) return 1;
        #endif
    }
    else {
        if (likely(__Pyx_TypeCheck(obj, type))) return 1;
    }
    PyErr_Format(PyExc_TypeError,
        "Argument '%.200s' has incorrect type (expected %.200s, got %.200s)",
        name, type->tp_name, Py_TYPE(obj)->tp_name);
    return 0;
}

/* PyErrFetchRestore */
#if CYTHON_FAST_THREAD_STATE
static CYTHON_INLINE void __Pyx_ErrRestoreInState(PyThreadState *tstate, PyObject *type, PyObject *value, PyObject *tb) {
    PyObject *tmp_type, *tmp_value, *tmp_tb;
    tmp_type = tstate->curexc_type;
    tmp_value = tstate->curexc_value;
    tmp_tb = tstate->curexc_traceback;
    tstate->curexc_type = type;
    tstate->curexc_value = value;
    tstate->curexc_traceback = tb;
    Py_XDECREF(tmp_type);
    Py_XDECREF(tmp_value);
    Py_XDECREF(tmp_tb);
}
static CYTHON_INLINE void __Pyx_ErrFetchInState(PyThreadState *tstate, PyObject **type, PyObject **value, PyObject **tb) {
    *type = tstate->curexc_type;
    *value = tstate->curexc_value;
    *tb = tstate->curexc_traceback;
    tstate->curexc_type = 0;
    tstate->curexc_value = 0;
    tstate->curexc_traceback = 0;
}
#endif

/* Profile */
#if CYTHON_PROFILE
static int __Pyx_TraceSetupAndCall(PyCodeObject** code,
                                   PyFrameObject** frame,
                                   PyThreadState* tstate,
                                   const char *funcname,
                                   const char *srcfile,
                                   int firstlineno) {
    PyObject *type, *value, *traceback;
    int retval;
    if (*frame == NULL || !CYTHON_PROFILE_REUSE_FRAME) {
        if (*code == NULL) {
            *code = __Pyx_createFrameCodeObject(funcname, srcfile, firstlineno);
            if (*code == NULL) return 0;
        }
        *frame = PyFrame_New(
            tstate,                          /*PyThreadState *tstate*/
            *code,                           /*PyCodeObject *code*/
            __pyx_d,                  /*PyObject *globals*/
            0                                /*PyObject *locals*/
        );
        if (*frame == NULL) return 0;
        if (CYTHON_TRACE && (*frame)->f_trace == NULL) {
            Py_INCREF(Py_None);
            (*frame)->f_trace = Py_None;
        }
#if PY_VERSION_HEX < 0x030400B1
    } else {
        (*frame)->f_tstate = tstate;
#endif
    }
    __Pyx_PyFrame_SetLineNumber(*frame, firstlineno);
    retval = 1;
    __Pyx_EnterTracing(tstate);
    __Pyx_ErrFetchInState(tstate, &type, &value, &traceback);
    #if CYTHON_TRACE
    if (tstate->c_tracefunc)
        retval = tstate->c_tracefunc(tstate->c_traceobj, *frame, PyTrace_CALL, NULL) == 0;
    if (retval && tstate->c_profilefunc)
    #endif
        retval = tstate->c_profilefunc(tstate->c_profileobj, *frame, PyTrace_CALL, NULL) == 0;
    __Pyx_LeaveTracing(tstate);
    if (retval) {
        __Pyx_ErrRestoreInState(tstate, type, value, traceback);
        return __Pyx_IsTracing(tstate, 0, 0) && retval;
    } else {
        Py_XDECREF(type);
        Py_XDECREF(value);
        Py_XDECREF(traceback);
        return -1;
    }
}
static PyCodeObject *__Pyx_createFrameCodeObject(const char *funcname, const char *srcfile, int firstlineno) {
    PyCodeObject *py_code = 0;
#if PY_MAJOR_VERSION >= 3
    py_code = PyCode_NewEmpty(srcfile, funcname, firstlineno);
    if (likely(py_code)) {
        py_code->co_flags |= CO_OPTIMIZED | CO_NEWLOCALS;
    }
#else
    PyObject *py_srcfile = 0;
    PyObject *py_funcname = 0;
    py_funcname = PyString_FromString(funcname);
    if (unlikely(!py_funcname)) goto bad;
    py_srcfile = PyString_FromString(srcfile);
    if (unlikely(!py_srcfile)) goto bad;
    py_code = PyCode_New(
        0,
        0,
        0,
        CO_OPTIMIZED | CO_NEWLOCALS,
        __pyx_empty_bytes,     /*PyObject *code,*/
        __pyx_empty_tuple,     /*PyObject *consts,*/
        __pyx_empty_tuple,     /*PyObject *names,*/
        __pyx_empty_tuple,     /*PyObject *varnames,*/
        __pyx_empty_tuple,     /*PyObject *freevars,*/
        __pyx_empty_tuple,     /*PyObject *cellvars,*/
        py_srcfile,       /*PyObject *filename,*/
        py_funcname,      /*PyObject *name,*/
        firstlineno,
        __pyx_empty_bytes      /*PyObject *lnotab*/
    );
bad:
    Py_XDECREF(py_srcfile);
    Py_XDECREF(py_funcname);
#endif
    return py_code;
}
#endif

/* PyCFunctionFastCall */
#if CYTHON_FAST_PYCCALL
static CYTHON_INLINE PyObject * __Pyx_PyCFunction_FastCall(PyObject *func_obj, PyObject **args, Py_ssize_t nargs) {
    PyCFunctionObject *func = (PyCFunctionObject*)func_obj;
    PyCFunction meth = PyCFunction_GET_FUNCTION(func);
    PyObject *self = PyCFunction_GET_SELF(func);
    int flags = PyCFunction_GET_FLAGS(func);
    assert(PyCFunction_Check(func));
    assert(METH_FASTCALL == (flags & ~(METH_CLASS | METH_STATIC | METH_COEXIST | METH_KEYWORDS | METH_STACKLESS)));
    assert(nargs >= 0);
    assert(nargs == 0 || args != NULL);
    /* _PyCFunction_FastCallDict() must not be called with an exception set,
       because it may clear it (directly or indirectly) and so the
       caller loses its exception */
    assert(!PyErr_Occurred());
    if ((PY_VERSION_HEX < 0x030700A0) || unlikely(flags & METH_KEYWORDS)) {
        return (*((__Pyx_PyCFunctionFastWithKeywords)(void*)meth)) (self, args, nargs, NULL);
    } else {
        return (*((__Pyx_PyCFunctionFast)(void*)meth)) (self, args, nargs);
    }
}
#endif

/* PyFunctionFastCall */
#if CYTHON_FAST_PYCALL
static PyObject* __Pyx_PyFunction_FastCallNoKw(PyCodeObject *co, PyObject **args, Py_ssize_t na,
                                               PyObject *globals) {
    PyFrameObject *f;
    PyThreadState *tstate = __Pyx_PyThreadState_Current;
    PyObject **fastlocals;
    Py_ssize_t i;
    PyObject *result;
    assert(globals != NULL);
    /* XXX Perhaps we should create a specialized
       PyFrame_New() that doesn't take locals, but does
       take builtins without sanity checking them.
       */
    assert(tstate != NULL);
    f = PyFrame_New(tstate, co, globals, NULL);
    if (f == NULL) {
        return NULL;
    }
    fastlocals = __Pyx_PyFrame_GetLocalsplus(f);
    for (i = 0; i < na; i++) {
        Py_INCREF(*args);
        fastlocals[i] = *args++;
    }
    result = PyEval_EvalFrameEx(f,0);
    ++tstate->recursion_depth;
    Py_DECREF(f);
    --tstate->recursion_depth;
    return result;
}
#if 1 || PY_VERSION_HEX < 0x030600B1
static PyObject *__Pyx_PyFunction_FastCallDict(PyObject *func, PyObject **args, Py_ssize_t nargs, PyObject *kwargs) {
    PyCodeObject *co = (PyCodeObject *)PyFunction_GET_CODE(func);
    PyObject *globals = PyFunction_GET_GLOBALS(func);
    PyObject *argdefs = PyFunction_GET_DEFAULTS(func);
    PyObject *closure;
#if PY_MAJOR_VERSION >= 3
    PyObject *kwdefs;
#endif
    PyObject *kwtuple, **k;
    PyObject **d;
    Py_ssize_t nd;
    Py_ssize_t nk;
    PyObject *result;
    assert(kwargs == NULL || PyDict_Check(kwargs));
    nk = kwargs ? PyDict_Size(kwargs) : 0;
    if (Py_EnterRecursiveCall((char*)" while calling a Python object")) {
        return NULL;
    }
    if (
#if PY_MAJOR_VERSION >= 3
            co->co_kwonlyargcount == 0 &&
#endif
            likely(kwargs == NULL || nk == 0) &&
            co->co_flags == (CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE)) {
        if (argdefs == NULL && co->co_argcount == nargs) {
            result = __Pyx_PyFunction_FastCallNoKw(co, args, nargs, globals);
            goto done;
        }
        else if (nargs == 0 && argdefs != NULL
                 && co->co_argcount == Py_SIZE(argdefs)) {
            /* function called with no arguments, but all parameters have
               a default value: use default values as arguments .*/
            args = &PyTuple_GET_ITEM(argdefs, 0);
            result =__Pyx_PyFunction_FastCallNoKw(co, args, Py_SIZE(argdefs), globals);
            goto done;
        }
    }
    if (kwargs != NULL) {
        Py_ssize_t pos, i;
        kwtuple = PyTuple_New(2 * nk);
        if (kwtuple == NULL) {
            result = NULL;
            goto done;
        }
        k = &PyTuple_GET_ITEM(kwtuple, 0);
        pos = i = 0;
        while (PyDict_Next(kwargs, &pos, &k[i], &k[i+1])) {
            Py_INCREF(k[i]);
            Py_INCREF(k[i+1]);
            i += 2;
        }
        nk = i / 2;
    }
    else {
        kwtuple = NULL;
        k = NULL;
    }
    closure = PyFunction_GET_CLOSURE(func);
#if PY_MAJOR_VERSION >= 3
    kwdefs = PyFunction_GET_KW_DEFAULTS(func);
#endif
    if (argdefs != NULL) {
        d = &PyTuple_GET_ITEM(argdefs, 0);
        nd = Py_SIZE(argdefs);
    }
    else {
        d = NULL;
        nd = 0;
    }
#if PY_MAJOR_VERSION >= 3
    result = PyEval_EvalCodeEx((PyObject*)co, globals, (PyObject *)NULL,
                               args, (int)nargs,
                               k, (int)nk,
                               d, (int)nd, kwdefs, closure);
#else
    result = PyEval_EvalCodeEx(co, globals, (PyObject *)NULL,
                               args, (int)nargs,
                               k, (int)nk,
                               d, (int)nd, closure);
#endif
    Py_XDECREF(kwtuple);
done:
    Py_LeaveRecursiveCall();
    return result;
}
#endif
#endif

/* PyObjectCall */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_Call(PyObject *func, PyObject *arg, PyObject *kw) {
    PyObject *result;
    ternaryfunc call = Py_TYPE(func)->tp_call;
    if (unlikely(!call))
        return PyObject_Call(func, arg, kw);
    if (unlikely(Py_EnterRecursiveCall((char*)" while calling a Python object")))
        return NULL;
    result = (*call)(func, arg, kw);
    Py_LeaveRecursiveCall();
    if (unlikely(!result) && unlikely(!PyErr_Occurred())) {
        PyErr_SetString(
            PyExc_SystemError,
            "NULL result without error in PyObject_Call");
    }
    return result;
}
#endif

/* PyObjectCall2Args */
static CYTHON_UNUSED PyObject* __Pyx_PyObject_Call2Args(PyObject* function, PyObject* arg1, PyObject* arg2) {
    PyObject *args, *result = NULL;
    #if CYTHON_FAST_PYCALL
    if (PyFunction_Check(function)) {
        PyObject *args[2] = {arg1, arg2};
        return __Pyx_PyFunction_FastCall(function, args, 2);
    }
    #endif
    #if CYTHON_FAST_PYCCALL
    if (__Pyx_PyFastCFunction_Check(function)) {
        PyObject *args[2] = {arg1, arg2};
        return __Pyx_PyCFunction_FastCall(function, args, 2);
    }
    #endif
    args = PyTuple_New(2);
    if (unlikely(!args)) goto done;
    Py_INCREF(arg1);
    PyTuple_SET_ITEM(args, 0, arg1);
    Py_INCREF(arg2);
    PyTuple_SET_ITEM(args, 1, arg2);
    Py_INCREF(function);
    result = __Pyx_PyObject_Call(function, args, NULL);
    Py_DECREF(args);
    Py_DECREF(function);
done:
    return result;
}

/* PyObjectCallMethO */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallMethO(PyObject *func, PyObject *arg) {
    PyObject *self, *result;
    PyCFunction cfunc;
    cfunc = PyCFunction_GET_FUNCTION(func);
    self = PyCFunction_GET_SELF(func);
    if (unlikely(Py_EnterRecursiveCall((char*)" while calling a Python object")))
        return NULL;
    result = cfunc(self, arg);
    Py_LeaveRecursiveCall();
    if (unlikely(!result) && unlikely(!PyErr_Occurred())) {
        PyErr_SetString(
            PyExc_SystemError,
            "NULL result without error in PyObject_Call");
    }
    return result;
}
#endif

/* PyObjectCallOneArg */
#if CYTHON_COMPILING_IN_CPYTHON
static PyObject* __Pyx__PyObject_CallOneArg(PyObject *func, PyObject *arg) {
    PyObject *result;
    PyObject *args = PyTuple_New(1);
    if (unlikely(!args)) return NULL;
    Py_INCREF(arg);
    PyTuple_SET_ITEM(args, 0, arg);
    result = __Pyx_PyObject_Call(func, args, NULL);
    Py_DECREF(args);
    return result;
}
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallOneArg(PyObject *func, PyObject *arg) {
#if CYTHON_FAST_PYCALL
    if (PyFunction_Check(func)) {
        return __Pyx_PyFunction_FastCall(func, &arg, 1);
    }
#endif
    if (likely(PyCFunction_Check(func))) {
        if (likely(PyCFunction_GET_FLAGS(func) & METH_O)) {
            return __Pyx_PyObject_CallMethO(func, arg);
#if CYTHON_FAST_PYCCALL
        } else if (__Pyx_PyFastCFunction_Check(func)) {
            return __Pyx_PyCFunction_FastCall(func, &arg, 1);
#endif
        }
    }
    return __Pyx__PyObject_CallOneArg(func, arg);
}
#else
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallOneArg(PyObject *func, PyObject *arg) {
    PyObject *result;
    PyObject *args = PyTuple_Pack(1, arg);
    if (unlikely(!args)) return NULL;
    result = __Pyx_PyObject_Call(func, args, NULL);
    Py_DECREF(args);
    return result;
}
#endif

/* decode_c_string */
static CYTHON_INLINE PyObject* __Pyx_decode_c_string(
         const char* cstring, Py_ssize_t start, Py_ssize_t stop,
         const char* encoding, const char* errors,
         PyObject* (*decode_func)(const char *s, Py_ssize_t size, const char *errors)) {
    Py_ssize_t length;
    if (unlikely((start < 0) | (stop < 0))) {
        size_t slen = strlen(cstring);
        if (unlikely(slen > (size_t) PY_SSIZE_T_MAX)) {
            PyErr_SetString(PyExc_OverflowError,
                            "c-string too long to convert to Python");
            return NULL;
        }
        length = (Py_ssize_t) slen;
        if (start < 0) {
            start += length;
            if (start < 0)
                start = 0;
        }
        if (stop < 0)
            stop += length;
    }
    if (unlikely(stop <= start))
        return __Pyx_NewRef(__pyx_empty_unicode);
    length = stop - start;
    cstring += start;
    if (decode_func) {
        return decode_func(cstring, length, errors);
    } else {
        return PyUnicode_Decode(cstring, length, encoding, errors);
    }
}

/* UnicodeAsUCS4 */
static CYTHON_INLINE Py_UCS4 __Pyx_PyUnicode_AsPy_UCS4(PyObject* x) {
   Py_ssize_t length;
   #if CYTHON_PEP393_ENABLED
   length = PyUnicode_GET_LENGTH(x);
   if (likely(length == 1)) {
       return PyUnicode_READ_CHAR(x, 0);
   }
   #else
   length = PyUnicode_GET_SIZE(x);
   if (likely(length == 1)) {
       return PyUnicode_AS_UNICODE(x)[0];
   }
   #if Py_UNICODE_SIZE == 2
   else if (PyUnicode_GET_SIZE(x) == 2) {
       Py_UCS4 high_val = PyUnicode_AS_UNICODE(x)[0];
       if (high_val >= 0xD800 && high_val <= 0xDBFF) {
           Py_UCS4 low_val = PyUnicode_AS_UNICODE(x)[1];
           if (low_val >= 0xDC00 && low_val <= 0xDFFF) {
               return 0x10000 + (((high_val & ((1<<10)-1)) << 10) | (low_val & ((1<<10)-1)));
           }
       }
   }
   #endif
   #endif
   PyErr_Format(PyExc_ValueError,
                "only single character unicode strings can be converted to Py_UCS4, "
                "got length %" CYTHON_FORMAT_SSIZE_T "d", length);
   return (Py_UCS4)-1;
}

/* object_ord */
static long __Pyx__PyObject_Ord(PyObject* c) {
    Py_ssize_t size;
    if (PyBytes_Check(c)) {
        size = PyBytes_GET_SIZE(c);
        if (likely(size == 1)) {
            return (unsigned char) PyBytes_AS_STRING(c)[0];
        }
#if PY_MAJOR_VERSION < 3
    } else if (PyUnicode_Check(c)) {
        return (long)__Pyx_PyUnicode_AsPy_UCS4(c);
#endif
#if (!CYTHON_COMPILING_IN_PYPY) || (defined(PyByteArray_AS_STRING) && defined(PyByteArray_GET_SIZE))
    } else if (PyByteArray_Check(c)) {
        size = PyByteArray_GET_SIZE(c);
        if (likely(size == 1)) {
            return (unsigned char) PyByteArray_AS_STRING(c)[0];
        }
#endif
    } else {
        PyErr_Format(PyExc_TypeError,
            "ord() expected string of length 1, but %.200s found", Py_TYPE(c)->tp_name);
        return (long)(Py_UCS4)-1;
    }
    PyErr_Format(PyExc_TypeError,
        "ord() expected a character, but string of length %zd found", size);
    return (long)(Py_UCS4)-1;
}

/* PyObjectCallNoArg */
#if CYTHON_COMPILING_IN_CPYTHON
static CYTHON_INLINE PyObject* __Pyx_PyObject_CallNoArg(PyObject *func) {
#if CYTHON_FAST_PYCALL
    if (PyFunction_Check(func)) {
        return __Pyx_PyFunction_FastCall(func, NULL, 0);
    }
#endif
#ifdef __Pyx_CyFunction_USED
    if (likely(PyCFunction_Check(func) || __Pyx_CyFunction_Check(func)))
#else
    if (likely(PyCFunction_Check(func)))
#endif
    {
        if (likely(PyCFunction_GET_FLAGS(func) & METH_NOARGS)) {
            return __Pyx_PyObject_CallMethO(func, NULL);
        }
    }
    return __Pyx_PyObject_Call(func, __pyx_empty_tuple, NULL);
}
#endif

/* decode_c_bytes */
static CYTHON_INLINE PyObject* __Pyx_decode_c_bytes(
         const char* cstring, Py_ssize_t length, Py_ssize_t start, Py_ssize_t stop,
         const char* encoding, const char* errors,
         PyObject* (*decode_func)(const char *s, Py_ssize_t size, const char *errors)) {
    if (unlikely((start < 0) | (stop < 0))) {
        if (start < 0) {
            start += length;
            if (start < 0)
                start = 0;
        }
        if (stop < 0)
            stop += length;
    }
    if (stop > length)
        stop = length;
    if (unlikely(stop <= start))
        return __Pyx_NewRef(__pyx_empty_unicode);
    length = stop - start;
    cstring += start;
    if (decode_func) {
        return decode_func(cstring, length, errors);
    } else {
        return PyUnicode_Decode(cstring, length, encoding, errors);
    }
}

/* GetTopmostException */
#if CYTHON_USE_EXC_INFO_STACK
static _PyErr_StackItem *
__Pyx_PyErr_GetTopmostException(PyThreadState *tstate)
{
    _PyErr_StackItem *exc_info = tstate->exc_info;
    while ((exc_info->exc_type == NULL || exc_info->exc_type == Py_None) &&
           exc_info->previous_item != NULL)
    {
        exc_info = exc_info->previous_item;
    }
    return exc_info;
}
#endif

/* SaveResetException */
#if CYTHON_FAST_THREAD_STATE
static CYTHON_INLINE void __Pyx__ExceptionSave(PyThreadState *tstate, PyObject **type, PyObject **value, PyObject **tb) {
    #if CYTHON_USE_EXC_INFO_STACK
    _PyErr_StackItem *exc_info = __Pyx_PyErr_GetTopmostException(tstate);
    *type = exc_info->exc_type;
    *value = exc_info->exc_value;
    *tb = exc_info->exc_traceback;
    #else
    *type = tstate->exc_type;
    *value = tstate->exc_value;
    *tb = tstate->exc_traceback;
    #endif
    Py_XINCREF(*type);
    Py_XINCREF(*value);
    Py_XINCREF(*tb);
}
static CYTHON_INLINE void __Pyx__ExceptionReset(PyThreadState *tstate, PyObject *type, PyObject *value, PyObject *tb) {
    PyObject *tmp_type, *tmp_value, *tmp_tb;
    #if CYTHON_USE_EXC_INFO_STACK
    _PyErr_StackItem *exc_info = tstate->exc_info;
    tmp_type = exc_info->exc_type;
    tmp_value = exc_info->exc_value;
    tmp_tb = exc_info->exc_traceback;
    exc_info->exc_type = type;
    exc_info->exc_value = value;
    exc_info->exc_traceback = tb;
    #else
    tmp_type = tstate->exc_type;
    tmp_value = tstate->exc_value;
    tmp_tb = tstate->exc_traceback;
    tstate->exc_type = type;
    tstate->exc_value = value;
    tstate->exc_traceback = tb;
    #endif
    Py_XDECREF(tmp_type);
    Py_XDECREF(tmp_value);
    Py_XDECREF(tmp_tb);
}
#endif

/* WriteUnraisableException */
static void __Pyx_WriteUnraisable(const char *name, CYTHON_UNUSED int clineno,
                                  CYTHON_UNUSED int lineno, CYTHON_UNUSED const char *filename,
                                  int full_traceback, CYTHON_UNUSED int nogil) {
    PyObject *old_exc, *old_val, *old_tb;
    PyObject *ctx;
    __Pyx_PyThreadState_declare
#ifdef WITH_THREAD
    PyGILState_STATE state;
    if (nogil)
        state = PyGILState_Ensure();
#ifdef _MSC_VER
    else state = (PyGILState_STATE)-1;
#endif
#endif
    __Pyx_PyThreadState_assign
    __Pyx_ErrFetch(&old_exc, &old_val, &old_tb);
    if (full_traceback) {
        Py_XINCREF(old_exc);
        Py_XINCREF(old_val);
        Py_XINCREF(old_tb);
        __Pyx_ErrRestore(old_exc, old_val, old_tb);
        PyErr_PrintEx(1);
    }
    #if PY_MAJOR_VERSION < 3
    ctx = PyString_FromString(name);
    #else
    ctx = PyUnicode_FromString(name);
    #endif
    __Pyx_ErrRestore(old_exc, old_val, old_tb);
    if (!ctx) {
        PyErr_WriteUnraisable(Py_None);
    } else {
        PyErr_WriteUnraisable(ctx);
        Py_DECREF(ctx);
    }
#ifdef WITH_THREAD
    if (nogil)
        PyGILState_Release(state);
#endif
}

/* PyIntCompare */
static CYTHON_INLINE PyObject* __Pyx_PyInt_EqObjC(PyObject *op1, PyObject *op2, CYTHON_UNUSED long intval, CYTHON_UNUSED long inplace) {
    if (op1 == op2) {
        Py_RETURN_TRUE;
    }
    #if PY_MAJOR_VERSION < 3
    if (likely(PyInt_CheckExact(op1))) {
        const long b = intval;
        long a = PyInt_AS_LONG(op1);
        if (a == b) Py_RETURN_TRUE; else Py_RETURN_FALSE;
    }
    #endif
    #if CYTHON_USE_PYLONG_INTERNALS
    if (likely(PyLong_CheckExact(op1))) {
        int unequal;
        unsigned long uintval;
        Py_ssize_t size = Py_SIZE(op1);
        const digit* digits = ((PyLongObject*)op1)->ob_digit;
        if (intval == 0) {
            if (size == 0) Py_RETURN_TRUE; else Py_RETURN_FALSE;
        } else if (intval < 0) {
            if (size >= 0)
                Py_RETURN_FALSE;
            intval = -intval;
            size = -size;
        } else {
            if (size <= 0)
                Py_RETURN_FALSE;
        }
        uintval = (unsigned long) intval;
#if PyLong_SHIFT * 4 < SIZEOF_LONG*8
        if (uintval >> (PyLong_SHIFT * 4)) {
            unequal = (size != 5) || (digits[0] != (uintval & (unsigned long) PyLong_MASK))
                 | (digits[1] != ((uintval >> (1 * PyLong_SHIFT)) & (unsigned long) PyLong_MASK)) | (digits[2] != ((uintval >> (2 * PyLong_SHIFT)) & (unsigned long) PyLong_MASK)) | (digits[3] != ((uintval >> (3 * PyLong_SHIFT)) & (unsigned long) PyLong_MASK)) | (digits[4] != ((uintval >> (4 * PyLong_SHIFT)) & (unsigned long) PyLong_MASK));
        } else
#endif
#if PyLong_SHIFT * 3 < SIZEOF_LONG*8
        if (uintval >> (PyLong_SHIFT * 3)) {
            unequal = (size != 4) || (digits[0] != (uintval & (unsigned long) PyLong_MASK))
                 | (digits[1] != ((uintval >> (1 * PyLong_SHIFT)) & (unsigned long) PyLong_MASK)) | (digits[2] != ((uintval >> (2 * PyLong_SHIFT)) & (unsigned long) PyLong_MASK)) | (digits[3] != ((uintval >> (3 * PyLong_SHIFT)) & (unsigned long) PyLong_MASK));
        } else
#endif
#if PyLong_SHIFT * 2 < SIZEOF_LONG*8
        if (uintval >> (PyLong_SHIFT * 2)) {
            unequal = (size != 3) || (digits[0] != (uintval & (unsigned long) PyLong_MASK))
                 | (digits[1] != ((uintval >> (1 * PyLong_SHIFT)) & (unsigned long) PyLong_MASK)) | (digits[2] != ((uintval >> (2 * PyLong_SHIFT)) & (unsigned long) PyLong_MASK));
        } else
#endif
#if PyLong_SHIFT * 1 < SIZEOF_LONG*8
        if (uintval >> (PyLong_SHIFT * 1)) {
            unequal = (size != 2) || (digits[0] != (uintval & (unsigned long) PyLong_MASK))
                 | (digits[1] != ((uintval >> (1 * PyLong_SHIFT)) & (unsigned long) PyLong_MASK));
        } else
#endif
            unequal = (size != 1) || (((unsigned long) digits[0]) != (uintval & (unsigned long) PyLong_MASK));
        if (unequal == 0) Py_RETURN_TRUE; else Py_RETURN_FALSE;
    }
    #endif
    if (PyFloat_CheckExact(op1)) {
        const long b = intval;
        double a = PyFloat_AS_DOUBLE(op1);
        if ((double)a == (double)b) Py_RETURN_TRUE; else Py_RETURN_FALSE;
    }
    return (
        PyObject_RichCompare(op1, op2, Py_EQ));
}

/* PyDictVersioning */
#if CYTHON_USE_DICT_VERSIONS && CYTHON_USE_TYPE_SLOTS
static CYTHON_INLINE PY_UINT64_T __Pyx_get_tp_dict_version(PyObject *obj) {
    PyObject *dict = Py_TYPE(obj)->tp_dict;
    return likely(dict) ? __PYX_GET_DICT_VERSION(dict) : 0;
}
static CYTHON_INLINE PY_UINT64_T __Pyx_get_object_dict_version(PyObject *obj) {
    PyObject **dictptr = NULL;
    Py_ssize_t offset = Py_TYPE(obj)->tp_dictoffset;
    if (offset) {
#if CYTHON_COMPILING_IN_CPYTHON
        dictptr = (likely(offset > 0)) ? (PyObject **) ((char *)obj + offset) : _PyObject_GetDictPtr(obj);
#else
        dictptr = _PyObject_GetDictPtr(obj);
#endif
    }
    return (dictptr && *dictptr) ? __PYX_GET_DICT_VERSION(*dictptr) : 0;
}
static CYTHON_INLINE int __Pyx_object_dict_version_matches(PyObject* obj, PY_UINT64_T tp_dict_version, PY_UINT64_T obj_dict_version) {
    PyObject *dict = Py_TYPE(obj)->tp_dict;
    if (unlikely(!dict) || unlikely(tp_dict_version != __PYX_GET_DICT_VERSION(dict)))
        return 0;
    return obj_dict_version == __Pyx_get_object_dict_version(obj);
}
#endif

/* GetModuleGlobalName */
#if CYTHON_USE_DICT_VERSIONS
static PyObject *__Pyx__GetModuleGlobalName(PyObject *name, PY_UINT64_T *dict_version, PyObject **dict_cached_value)
#else
static CYTHON_INLINE PyObject *__Pyx__GetModuleGlobalName(PyObject *name)
#endif
{
    PyObject *result;
#if !CYTHON_AVOID_BORROWED_REFS
#if CYTHON_COMPILING_IN_CPYTHON && PY_VERSION_HEX >= 0x030500A1
    result = _PyDict_GetItem_KnownHash(__pyx_d, name, ((PyASCIIObject *) name)->hash);
    __PYX_UPDATE_DICT_CACHE(__pyx_d, result, *dict_cached_value, *dict_version)
    if (likely(result)) {
        return __Pyx_NewRef(result);
    } else if (unlikely(PyErr_Occurred())) {
        return NULL;
    }
#else
    result = PyDict_GetItem(__pyx_d, name);
    __PYX_UPDATE_DICT_CACHE(__pyx_d, result, *dict_cached_value, *dict_version)
    if (likely(result)) {
        return __Pyx_NewRef(result);
    }
#endif
#else
    result = PyObject_GetItem(__pyx_d, name);
    __PYX_UPDATE_DICT_CACHE(__pyx_d, result, *dict_cached_value, *dict_version)
    if (likely(result)) {
        return __Pyx_NewRef(result);
    }
    PyErr_Clear();
#endif
    return __Pyx_GetBuiltinName(name);
}

/* pyobject_as_double */
static double __Pyx__PyObject_AsDouble(PyObject* obj) {
    PyObject* float_value;
#if !CYTHON_USE_TYPE_SLOTS
    float_value = PyNumber_Float(obj);  if ((0)) goto bad;
#else
    PyNumberMethods *nb = Py_TYPE(obj)->tp_as_number;
    if (likely(nb) && likely(nb->nb_float)) {
        float_value = nb->nb_float(obj);
        if (likely(float_value) && unlikely(!PyFloat_Check(float_value))) {
            PyErr_Format(PyExc_TypeError,
                "__float__ returned non-float (type %.200s)",
                Py_TYPE(float_value)->tp_name);
            Py_DECREF(float_value);
            goto bad;
        }
    } else if (PyUnicode_CheckExact(obj) || PyBytes_CheckExact(obj)) {
#if PY_MAJOR_VERSION >= 3
        float_value = PyFloat_FromString(obj);
#else
        float_value = PyFloat_FromString(obj, 0);
#endif
    } else {
        PyObject* args = PyTuple_New(1);
        if (unlikely(!args)) goto bad;
        PyTuple_SET_ITEM(args, 0, obj);
        float_value = PyObject_Call((PyObject*)&PyFloat_Type, args, 0);
        PyTuple_SET_ITEM(args, 0, 0);
        Py_DECREF(args);
    }
#endif
    if (likely(float_value)) {
        double value = PyFloat_AS_DOUBLE(float_value);
        Py_DECREF(float_value);
        return value;
    }
bad:
    return (double)-1;
}

/* GetItemInt */
static PyObject *__Pyx_GetItemInt_Generic(PyObject *o, PyObject* j) {
    PyObject *r;
    if (!j) return NULL;
    r = PyObject_GetItem(o, j);
    Py_DECREF(j);
    return r;
}
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_List_Fast(PyObject *o, Py_ssize_t i,
                                                              CYTHON_NCP_UNUSED int wraparound,
                                                              CYTHON_NCP_UNUSED int boundscheck) {
#if CYTHON_ASSUME_SAFE_MACROS && !CYTHON_AVOID_BORROWED_REFS
    Py_ssize_t wrapped_i = i;
    if (wraparound & unlikely(i < 0)) {
        wrapped_i += PyList_GET_SIZE(o);
    }
    if ((!boundscheck) || likely(__Pyx_is_valid_index(wrapped_i, PyList_GET_SIZE(o)))) {
        PyObject *r = PyList_GET_ITEM(o, wrapped_i);
        Py_INCREF(r);
        return r;
    }
    return __Pyx_GetItemInt_Generic(o, PyInt_FromSsize_t(i));
#else
    return PySequence_GetItem(o, i);
#endif
}
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_Tuple_Fast(PyObject *o, Py_ssize_t i,
                                                              CYTHON_NCP_UNUSED int wraparound,
                                                              CYTHON_NCP_UNUSED int boundscheck) {
#if CYTHON_ASSUME_SAFE_MACROS && !CYTHON_AVOID_BORROWED_REFS
    Py_ssize_t wrapped_i = i;
    if (wraparound & unlikely(i < 0)) {
        wrapped_i += PyTuple_GET_SIZE(o);
    }
    if ((!boundscheck) || likely(__Pyx_is_valid_index(wrapped_i, PyTuple_GET_SIZE(o)))) {
        PyObject *r = PyTuple_GET_ITEM(o, wrapped_i);
        Py_INCREF(r);
        return r;
    }
    return __Pyx_GetItemInt_Generic(o, PyInt_FromSsize_t(i));
#else
    return PySequence_GetItem(o, i);
#endif
}
static CYTHON_INLINE PyObject *__Pyx_GetItemInt_Fast(PyObject *o, Py_ssize_t i, int is_list,
                                                     CYTHON_NCP_UNUSED int wraparound,
                                                     CYTHON_NCP_UNUSED int boundscheck) {
#if CYTHON_ASSUME_SAFE_MACROS && !CYTHON_AVOID_BORROWED_REFS && CYTHON_USE_TYPE_SLOTS
    if (is_list || PyList_CheckExact(o)) {
        Py_ssize_t n = ((!wraparound) | likely(i >= 0)) ? i : i + PyList_GET_SIZE(o);
        if ((!boundscheck) || (likely(__Pyx_is_valid_index(n, PyList_GET_SIZE(o))))) {
            PyObject *r = PyList_GET_ITEM(o, n);
            Py_INCREF(r);
            return r;
        }
    }
    else if (PyTuple_CheckExact(o)) {
        Py_ssize_t n = ((!wraparound) | likely(i >= 0)) ? i : i + PyTuple_GET_SIZE(o);
        if ((!boundscheck) || likely(__Pyx_is_valid_index(n, PyTuple_GET_SIZE(o)))) {
            PyObject *r = PyTuple_GET_ITEM(o, n);
            Py_INCREF(r);
            return r;
        }
    } else {
        PySequenceMethods *m = Py_TYPE(o)->tp_as_sequence;
        if (likely(m && m->sq_item)) {
            if (wraparound && unlikely(i < 0) && likely(m->sq_length)) {
                Py_ssize_t l = m->sq_length(o);
                if (likely(l >= 0)) {
                    i += l;
                } else {
                    if (!PyErr_ExceptionMatches(PyExc_OverflowError))
                        return NULL;
                    PyErr_Clear();
                }
            }
            return m->sq_item(o, i);
        }
    }
#else
    if (is_list || PySequence_Check(o)) {
        return PySequence_GetItem(o, i);
    }
#endif
    return __Pyx_GetItemInt_Generic(o, PyInt_FromSsize_t(i));
}

/* BytesEquals */
static CYTHON_INLINE int __Pyx_PyBytes_Equals(PyObject* s1, PyObject* s2, int equals) {
#if CYTHON_COMPILING_IN_PYPY
    return PyObject_RichCompareBool(s1, s2, equals);
#else
    if (s1 == s2) {
        return (equals == Py_EQ);
    } else if (PyBytes_CheckExact(s1) & PyBytes_CheckExact(s2)) {
        const char *ps1, *ps2;
        Py_ssize_t length = PyBytes_GET_SIZE(s1);
        if (length != PyBytes_GET_SIZE(s2))
            return (equals == Py_NE);
        ps1 = PyBytes_AS_STRING(s1);
        ps2 = PyBytes_AS_STRING(s2);
        if (ps1[0] != ps2[0]) {
            return (equals == Py_NE);
        } else if (length == 1) {
            return (equals == Py_EQ);
        } else {
            int result;
#if CYTHON_USE_UNICODE_INTERNALS && (PY_VERSION_HEX < 0x030B0000)
            Py_hash_t hash1, hash2;
            hash1 = ((PyBytesObject*)s1)->ob_shash;
            hash2 = ((PyBytesObject*)s2)->ob_shash;
            if (hash1 != hash2 && hash1 != -1 && hash2 != -1) {
                return (equals == Py_NE);
            }
#endif
            result = memcmp(ps1, ps2, (size_t)length);
            return (equals == Py_EQ) ? (result == 0) : (result != 0);
        }
    } else if ((s1 == Py_None) & PyBytes_CheckExact(s2)) {
        return (equals == Py_NE);
    } else if ((s2 == Py_None) & PyBytes_CheckExact(s1)) {
        return (equals == Py_NE);
    } else {
        int result;
        PyObject* py_result = PyObject_RichCompare(s1, s2, equals);
        if (!py_result)
            return -1;
        result = __Pyx_PyObject_IsTrue(py_result);
        Py_DECREF(py_result);
        return result;
    }
#endif
}

/* GetException */
#if CYTHON_FAST_THREAD_STATE
static int __Pyx__GetException(PyThreadState *tstate, PyObject **type, PyObject **value, PyObject **tb)
#else
static int __Pyx_GetException(PyObject **type, PyObject **value, PyObject **tb)
#endif
{
    PyObject *local_type, *local_value, *local_tb;
#if CYTHON_FAST_THREAD_STATE
    PyObject *tmp_type, *tmp_value, *tmp_tb;
    local_type = tstate->curexc_type;
    local_value = tstate->curexc_value;
    local_tb = tstate->curexc_traceback;
    tstate->curexc_type = 0;
    tstate->curexc_value = 0;
    tstate->curexc_traceback = 0;
#else
    PyErr_Fetch(&local_type, &local_value, &local_tb);
#endif
    PyErr_NormalizeException(&local_type, &local_value, &local_tb);
#if CYTHON_FAST_THREAD_STATE
    if (unlikely(tstate->curexc_type))
#else
    if (unlikely(PyErr_Occurred()))
#endif
        goto bad;
    #if PY_MAJOR_VERSION >= 3
    if (local_tb) {
        if (unlikely(PyException_SetTraceback(local_value, local_tb) < 0))
            goto bad;
    }
    #endif
    Py_XINCREF(local_tb);
    Py_XINCREF(local_type);
    Py_XINCREF(local_value);
    *type = local_type;
    *value = local_value;
    *tb = local_tb;
#if CYTHON_FAST_THREAD_STATE
    #if CYTHON_USE_EXC_INFO_STACK
    {
        _PyErr_StackItem *exc_info = tstate->exc_info;
        tmp_type = exc_info->exc_type;
        tmp_value = exc_info->exc_value;
        tmp_tb = exc_info->exc_traceback;
        exc_info->exc_type = local_type;
        exc_info->exc_value = local_value;
        exc_info->exc_traceback = local_tb;
    }
    #else
    tmp_type = tstate->exc_type;
    tmp_value = tstate->exc_value;
    tmp_tb = tstate->exc_traceback;
    tstate->exc_type = local_type;
    tstate->exc_value = local_value;
    tstate->exc_traceback = local_tb;
    #endif
    Py_XDECREF(tmp_type);
    Py_XDECREF(tmp_value);
    Py_XDECREF(tmp_tb);
#else
    PyErr_SetExcInfo(local_type, local_value, local_tb);
#endif
    return 0;
bad:
    *type = 0;
    *value = 0;
    *tb = 0;
    Py_XDECREF(local_type);
    Py_XDECREF(local_value);
    Py_XDECREF(local_tb);
    return -1;
}

/* FastTypeChecks */
#if CYTHON_COMPILING_IN_CPYTHON
static int __Pyx_InBases(PyTypeObject *a, PyTypeObject *b) {
    while (a) {
        a = a->tp_base;
        if (a == b)
            return 1;
    }
    return b == &PyBaseObject_Type;
}
static CYTHON_INLINE int __Pyx_IsSubtype(PyTypeObject *a, PyTypeObject *b) {
    PyObject *mro;
    if (a == b) return 1;
    mro = a->tp_mro;
    if (likely(mro)) {
        Py_ssize_t i, n;
        n = PyTuple_GET_SIZE(mro);
        for (i = 0; i < n; i++) {
            if (PyTuple_GET_ITEM(mro, i) == (PyObject *)b)
                return 1;
        }
        return 0;
    }
    return __Pyx_InBases(a, b);
}
#if PY_MAJOR_VERSION == 2
static int __Pyx_inner_PyErr_GivenExceptionMatches2(PyObject *err, PyObject* exc_type1, PyObject* exc_type2) {
    PyObject *exception, *value, *tb;
    int res;
    __Pyx_PyThreadState_declare
    __Pyx_PyThreadState_assign
    __Pyx_ErrFetch(&exception, &value, &tb);
    res = exc_type1 ? PyObject_IsSubclass(err, exc_type1) : 0;
    if (unlikely(res == -1)) {
        PyErr_WriteUnraisable(err);
        res = 0;
    }
    if (!res) {
        res = PyObject_IsSubclass(err, exc_type2);
        if (unlikely(res == -1)) {
            PyErr_WriteUnraisable(err);
            res = 0;
        }
    }
    __Pyx_ErrRestore(exception, value, tb);
    return res;
}
#else
static CYTHON_INLINE int __Pyx_inner_PyErr_GivenExceptionMatches2(PyObject *err, PyObject* exc_type1, PyObject *exc_type2) {
    int res = exc_type1 ? __Pyx_IsSubtype((PyTypeObject*)err, (PyTypeObject*)exc_type1) : 0;
    if (!res) {
        res = __Pyx_IsSubtype((PyTypeObject*)err, (PyTypeObject*)exc_type2);
    }
    return res;
}
#endif
static int __Pyx_PyErr_GivenExceptionMatchesTuple(PyObject *exc_type, PyObject *tuple) {
    Py_ssize_t i, n;
    assert(PyExceptionClass_Check(exc_type));
    n = PyTuple_GET_SIZE(tuple);
#if PY_MAJOR_VERSION >= 3
    for (i=0; i<n; i++) {
        if (exc_type == PyTuple_GET_ITEM(tuple, i)) return 1;
    }
#endif
    for (i=0; i<n; i++) {
        PyObject *t = PyTuple_GET_ITEM(tuple, i);
        #if PY_MAJOR_VERSION < 3
        if (likely(exc_type == t)) return 1;
        #endif
        if (likely(PyExceptionClass_Check(t))) {
            if (__Pyx_inner_PyErr_GivenExceptionMatches2(exc_type, NULL, t)) return 1;
        } else {
        }
    }
    return 0;
}
static CYTHON_INLINE int __Pyx_PyErr_GivenExceptionMatches(PyObject *err, PyObject* exc_type) {
    if (likely(err == exc_type)) return 1;
    if (likely(PyExceptionClass_Check(err))) {
        if (likely(PyExceptionClass_Check(exc_type))) {
            return __Pyx_inner_PyErr_GivenExceptionMatches2(err, NULL, exc_type);
        } else if (likely(PyTuple_Check(exc_type))) {
            return __Pyx_PyErr_GivenExceptionMatchesTuple(err, exc_type);
        } else {
        }
    }
    return PyErr_GivenExceptionMatches(err, exc_type);
}
static CYTHON_INLINE int __Pyx_PyErr_GivenExceptionMatches2(PyObject *err, PyObject *exc_type1, PyObject *exc_type2) {
    assert(PyExceptionClass_Check(exc_type1));
    assert(PyExceptionClass_Check(exc_type2));
    if (likely(err == exc_type1 || err == exc_type2)) return 1;
    if (likely(PyExceptionClass_Check(err))) {
        return __Pyx_inner_PyErr_GivenExceptionMatches2(err, exc_type1, exc_type2);
    }
    return (PyErr_GivenExceptionMatches(err, exc_type1) || PyErr_GivenExceptionMatches(err, exc_type2));
}
#endif

/* UnicodeEquals */
static CYTHON_INLINE int __Pyx_PyUnicode_Equals(PyObject* s1, PyObject* s2, int equals) {
#if CYTHON_COMPILING_IN_PYPY
    return PyObject_RichCompareBool(s1, s2, equals);
#else
#if PY_MAJOR_VERSION < 3
    PyObject* owned_ref = NULL;
#endif
    int s1_is_unicode, s2_is_unicode;
    if (s1 == s2) {
        goto return_eq;
    }
    s1_is_unicode = PyUnicode_CheckExact(s1);
    s2_is_unicode = PyUnicode_CheckExact(s2);
#if PY_MAJOR_VERSION < 3
    if ((s1_is_unicode & (!s2_is_unicode)) && PyString_CheckExact(s2)) {
        owned_ref = PyUnicode_FromObject(s2);
        if (unlikely(!owned_ref))
            return -1;
        s2 = owned_ref;
        s2_is_unicode = 1;
    } else if ((s2_is_unicode & (!s1_is_unicode)) && PyString_CheckExact(s1)) {
        owned_ref = PyUnicode_FromObject(s1);
        if (unlikely(!owned_ref))
            return -1;
        s1 = owned_ref;
        s1_is_unicode = 1;
    } else if (((!s2_is_unicode) & (!s1_is_unicode))) {
        return __Pyx_PyBytes_Equals(s1, s2, equals);
    }
#endif
    if (s1_is_unicode & s2_is_unicode) {
        Py_ssize_t length;
        int kind;
        void *data1, *data2;
        if (unlikely(__Pyx_PyUnicode_READY(s1) < 0) || unlikely(__Pyx_PyUnicode_READY(s2) < 0))
            return -1;
        length = __Pyx_PyUnicode_GET_LENGTH(s1);
        if (length != __Pyx_PyUnicode_GET_LENGTH(s2)) {
            goto return_ne;
        }
#if CYTHON_USE_UNICODE_INTERNALS
        {
            Py_hash_t hash1, hash2;
        #if CYTHON_PEP393_ENABLED
            hash1 = ((PyASCIIObject*)s1)->hash;
            hash2 = ((PyASCIIObject*)s2)->hash;
        #else
            hash1 = ((PyUnicodeObject*)s1)->hash;
            hash2 = ((PyUnicodeObject*)s2)->hash;
        #endif
            if (hash1 != hash2 && hash1 != -1 && hash2 != -1) {
                goto return_ne;
            }
        }
#endif
        kind = __Pyx_PyUnicode_KIND(s1);
        if (kind != __Pyx_PyUnicode_KIND(s2)) {
            goto return_ne;
        }
        data1 = __Pyx_PyUnicode_DATA(s1);
        data2 = __Pyx_PyUnicode_DATA(s2);
        if (__Pyx_PyUnicode_READ(kind, data1, 0) != __Pyx_PyUnicode_READ(kind, data2, 0)) {
            goto return_ne;
        } else if (length == 1) {
            goto return_eq;
        } else {
            int result = memcmp(data1, data2, (size_t)(length * kind));
            #if PY_MAJOR_VERSION < 3
            Py_XDECREF(owned_ref);
            #endif
            return (equals == Py_EQ) ? (result == 0) : (result != 0);
        }
    } else if ((s1 == Py_None) & s2_is_unicode) {
        goto return_ne;
    } else if ((s2 == Py_None) & s1_is_unicode) {
        goto return_ne;
    } else {
        int result;
        PyObject* py_result = PyObject_RichCompare(s1, s2, equals);
        #if PY_MAJOR_VERSION < 3
        Py_XDECREF(owned_ref);
        #endif
        if (!py_result)
            return -1;
        result = __Pyx_PyObject_IsTrue(py_result);
        Py_DECREF(py_result);
        return result;
    }
return_eq:
    #if PY_MAJOR_VERSION < 3
    Py_XDECREF(owned_ref);
    #endif
    return (equals == Py_EQ);
return_ne:
    #if PY_MAJOR_VERSION < 3
    Py_XDECREF(owned_ref);
    #endif
    return (equals == Py_NE);
#endif
}

/* None */
static CYTHON_INLINE void __Pyx_RaiseUnboundLocalError(const char *varname) {
    PyErr_Format(PyExc_UnboundLocalError, "local variable '%s' referenced before assignment", varname);
}

/* RaiseTooManyValuesToUnpack */
static CYTHON_INLINE void __Pyx_RaiseTooManyValuesError(Py_ssize_t expected) {
    PyErr_Format(PyExc_ValueError,
                 "too many values to unpack (expected %" CYTHON_FORMAT_SSIZE_T "d)", expected);
}

/* RaiseNeedMoreValuesToUnpack */
static CYTHON_INLINE void __Pyx_RaiseNeedMoreValuesError(Py_ssize_t index) {
    PyErr_Format(PyExc_ValueError,
                 "need more than %" CYTHON_FORMAT_SSIZE_T "d value%.1s to unpack",
                 index, (index == 1) ? "" : "s");
}

/* IterFinish */
static CYTHON_INLINE int __Pyx_IterFinish(void) {
#if CYTHON_FAST_THREAD_STATE
    PyThreadState *tstate = __Pyx_PyThreadState_Current;
    PyObject* exc_type = tstate->curexc_type;
    if (unlikely(exc_type)) {
        if (likely(__Pyx_PyErr_GivenExceptionMatches(exc_type, PyExc_StopIteration))) {
            PyObject *exc_value, *exc_tb;
            exc_value = tstate->curexc_value;
            exc_tb = tstate->curexc_traceback;
            tstate->curexc_type = 0;
            tstate->curexc_value = 0;
            tstate->curexc_traceback = 0;
            Py_DECREF(exc_type);
            Py_XDECREF(exc_value);
            Py_XDECREF(exc_tb);
            return 0;
        } else {
            return -1;
        }
    }
    return 0;
#else
    if (unlikely(PyErr_Occurred())) {
        if (likely(PyErr_ExceptionMatches(PyExc_StopIteration))) {
            PyErr_Clear();
            return 0;
        } else {
            return -1;
        }
    }
    return 0;
#endif
}

/* UnpackItemEndCheck */
static int __Pyx_IternextUnpackEndCheck(PyObject *retval, Py_ssize_t expected) {
    if (unlikely(retval)) {
        Py_DECREF(retval);
        __Pyx_RaiseTooManyValuesError(expected);
        return -1;
    } else {
        return __Pyx_IterFinish();
    }
    return 0;
}

/* RaiseException */
#if PY_MAJOR_VERSION < 3
static void __Pyx_Raise(PyObject *type, PyObject *value, PyObject *tb,
                        CYTHON_UNUSED PyObject *cause) {
    __Pyx_PyThreadState_declare
    Py_XINCREF(type);
    if (!value || value == Py_None)
        value = NULL;
    else
        Py_INCREF(value);
    if (!tb || tb == Py_None)
        tb = NULL;
    else {
        Py_INCREF(tb);
        if (!PyTraceBack_Check(tb)) {
            PyErr_SetString(PyExc_TypeError,
                "raise: arg 3 must be a traceback or None");
            goto raise_error;
        }
    }
    if (PyType_Check(type)) {
#if CYTHON_COMPILING_IN_PYPY
        if (!value) {
            Py_INCREF(Py_None);
            value = Py_None;
        }
#endif
        PyErr_NormalizeException(&type, &value, &tb);
    } else {
        if (value) {
            PyErr_SetString(PyExc_TypeError,
                "instance exception may not have a separate value");
            goto raise_error;
        }
        value = type;
        type = (PyObject*) Py_TYPE(type);
        Py_INCREF(type);
        if (!PyType_IsSubtype((PyTypeObject *)type, (PyTypeObject *)PyExc_BaseException)) {
            PyErr_SetString(PyExc_TypeError,
                "raise: exception class must be a subclass of BaseException");
            goto raise_error;
        }
    }
    __Pyx_PyThreadState_assign
    __Pyx_ErrRestore(type, value, tb);
    return;
raise_error:
    Py_XDECREF(value);
    Py_XDECREF(type);
    Py_XDECREF(tb);
    return;
}
#else
static void __Pyx_Raise(PyObject *type, PyObject *value, PyObject *tb, PyObject *cause) {
    PyObject* owned_instance = NULL;
    if (tb == Py_None) {
        tb = 0;
    } else if (tb && !PyTraceBack_Check(tb)) {
        PyErr_SetString(PyExc_TypeError,
            "raise: arg 3 must be a traceback or None");
        goto bad;
    }
    if (value == Py_None)
        value = 0;
    if (PyExceptionInstance_Check(type)) {
        if (value) {
            PyErr_SetString(PyExc_TypeError,
                "instance exception may not have a separate value");
            goto bad;
        }
        value = type;
        type = (PyObject*) Py_TYPE(value);
    } else if (PyExceptionClass_Check(type)) {
        PyObject *instance_class = NULL;
        if (value && PyExceptionInstance_Check(value)) {
            instance_class = (PyObject*) Py_TYPE(value);
            if (instance_class != type) {
                int is_subclass = PyObject_IsSubclass(instance_class, type);
                if (!is_subclass) {
                    instance_class = NULL;
                } else if (unlikely(is_subclass == -1)) {
                    goto bad;
                } else {
                    type = instance_class;
                }
            }
        }
        if (!instance_class) {
            PyObject *args;
            if (!value)
                args = PyTuple_New(0);
            else if (PyTuple_Check(value)) {
                Py_INCREF(value);
                args = value;
            } else
                args = PyTuple_Pack(1, value);
            if (!args)
                goto bad;
            owned_instance = PyObject_Call(type, args, NULL);
            Py_DECREF(args);
            if (!owned_instance)
                goto bad;
            value = owned_instance;
            if (!PyExceptionInstance_Check(value)) {
                PyErr_Format(PyExc_TypeError,
                             "calling %R should have returned an instance of "
                             "BaseException, not %R",
                             type, Py_TYPE(value));
                goto bad;
            }
        }
    } else {
        PyErr_SetString(PyExc_TypeError,
            "raise: exception class must be a subclass of BaseException");
        goto bad;
    }
    if (cause) {
        PyObject *fixed_cause;
        if (cause == Py_None) {
            fixed_cause = NULL;
        } else if (PyExceptionClass_Check(cause)) {
            fixed_cause = PyObject_CallObject(cause, NULL);
            if (fixed_cause == NULL)
                goto bad;
        } else if (PyExceptionInstance_Check(cause)) {
            fixed_cause = cause;
            Py_INCREF(fixed_cause);
        } else {
            PyErr_SetString(PyExc_TypeError,
                            "exception causes must derive from "
                            "BaseException");
            goto bad;
        }
        PyException_SetCause(value, fixed_cause);
    }
    PyErr_SetObject(type, value);
    if (tb) {
#if CYTHON_COMPILING_IN_PYPY
        PyObject *tmp_type, *tmp_value, *tmp_tb;
        PyErr_Fetch(&tmp_type, &tmp_value, &tmp_tb);
        Py_INCREF(tb);
        PyErr_Restore(tmp_type, tmp_value, tb);
        Py_XDECREF(tmp_tb);
#else
        PyThreadState *tstate = __Pyx_PyThreadState_Current;
        PyObject* tmp_tb = tstate->curexc_traceback;
        if (tb != tmp_tb) {
            Py_INCREF(tb);
            tstate->curexc_traceback = tb;
            Py_XDECREF(tmp_tb);
        }
#endif
    }
bad:
    Py_XDECREF(owned_instance);
    return;
}
#endif

/* PyObject_GenericGetAttrNoDict */
#if CYTHON_USE_TYPE_SLOTS && CYTHON_USE_PYTYPE_LOOKUP && PY_VERSION_HEX < 0x03070000
static PyObject *__Pyx_RaiseGenericGetAttributeError(PyTypeObject *tp, PyObject *attr_name) {
    PyErr_Format(PyExc_AttributeError,
#if PY_MAJOR_VERSION >= 3
                 "'%.50s' object has no attribute '%U'",
                 tp->tp_name, attr_name);
#else
                 "'%.50s' object has no attribute '%.400s'",
                 tp->tp_name, PyString_AS_STRING(attr_name));
#endif
    return NULL;
}
static CYTHON_INLINE PyObject* __Pyx_PyObject_GenericGetAttrNoDict(PyObject* obj, PyObject* attr_name) {
    PyObject *descr;
    PyTypeObject *tp = Py_TYPE(obj);
    if (unlikely(!PyString_Check(attr_name))) {
        return PyObject_GenericGetAttr(obj, attr_name);
    }
    assert(!tp->tp_dictoffset);
    descr = _PyType_Lookup(tp, attr_name);
    if (unlikely(!descr)) {
        return __Pyx_RaiseGenericGetAttributeError(tp, attr_name);
    }
    Py_INCREF(descr);
    #if PY_MAJOR_VERSION < 3
    if (likely(PyType_HasFeature(Py_TYPE(descr), Py_TPFLAGS_HAVE_CLASS)))
    #endif
    {
        descrgetfunc f = Py_TYPE(descr)->tp_descr_get;
        if (unlikely(f)) {
            PyObject *res = f(descr, obj, (PyObject *)tp);
            Py_DECREF(descr);
            return res;
        }
    }
    return descr;
}
#endif

/* PyObject_GenericGetAttr */
#if CYTHON_USE_TYPE_SLOTS && CYTHON_USE_PYTYPE_LOOKUP && PY_VERSION_HEX < 0x03070000
static PyObject* __Pyx_PyObject_GenericGetAttr(PyObject* obj, PyObject* attr_name) {
    if (unlikely(Py_TYPE(obj)->tp_dictoffset)) {
        return PyObject_GenericGetAttr(obj, attr_name);
    }
    return __Pyx_PyObject_GenericGetAttrNoDict(obj, attr_name);
}
#endif

/* SetVTable */
static int __Pyx_SetVtable(PyObject *dict, void *vtable) {
#if PY_VERSION_HEX >= 0x02070000
    PyObject *ob = PyCapsule_New(vtable, 0, 0);
#else
    PyObject *ob = PyCObject_FromVoidPtr(vtable, 0);
#endif
    if (!ob)
        goto bad;
    if (PyDict_SetItem(dict, __pyx_n_s_pyx_vtable, ob) < 0)
        goto bad;
    Py_DECREF(ob);
    return 0;
bad:
    Py_XDECREF(ob);
    return -1;
}

/* PyErrExceptionMatches */
#if CYTHON_FAST_THREAD_STATE
static int __Pyx_PyErr_ExceptionMatchesTuple(PyObject *exc_type, PyObject *tuple) {
    Py_ssize_t i, n;
    n = PyTuple_GET_SIZE(tuple);
#if PY_MAJOR_VERSION >= 3
    for (i=0; i<n; i++) {
        if (exc_type == PyTuple_GET_ITEM(tuple, i)) return 1;
    }
#endif
    for (i=0; i<n; i++) {
        if (__Pyx_PyErr_GivenExceptionMatches(exc_type, PyTuple_GET_ITEM(tuple, i))) return 1;
    }
    return 0;
}
static CYTHON_INLINE int __Pyx_PyErr_ExceptionMatchesInState(PyThreadState* tstate, PyObject* err) {
    PyObject *exc_type = tstate->curexc_type;
    if (exc_type == err) return 1;
    if (unlikely(!exc_type)) return 0;
    if (unlikely(PyTuple_Check(err)))
        return __Pyx_PyErr_ExceptionMatchesTuple(exc_type, err);
    return __Pyx_PyErr_GivenExceptionMatches(exc_type, err);
}
#endif

/* PyObjectGetAttrStrNoError */
static void __Pyx_PyObject_GetAttrStr_ClearAttributeError(void) {
    __Pyx_PyThreadState_declare
    __Pyx_PyThreadState_assign
    if (likely(__Pyx_PyErr_ExceptionMatches(PyExc_AttributeError)))
        __Pyx_PyErr_Clear();
}
static CYTHON_INLINE PyObject* __Pyx_PyObject_GetAttrStrNoError(PyObject* obj, PyObject* attr_name) {
    PyObject *result;
#if CYTHON_COMPILING_IN_CPYTHON && CYTHON_USE_TYPE_SLOTS && PY_VERSION_HEX >= 0x030700B1
    PyTypeObject* tp = Py_TYPE(obj);
    if (likely(tp->tp_getattro == PyObject_GenericGetAttr)) {
        return _PyObject_GenericGetAttrWithDict(obj, attr_name, NULL, 1);
    }
#endif
    result = __Pyx_PyObject_GetAttrStr(obj, attr_name);
    if (unlikely(!result)) {
        __Pyx_PyObject_GetAttrStr_ClearAttributeError();
    }
    return result;
}

/* SetupReduce */
static int __Pyx_setup_reduce_is_named(PyObject* meth, PyObject* name) {
  int ret;
  PyObject *name_attr;
  name_attr = __Pyx_PyObject_GetAttrStr(meth, __pyx_n_s_name_2);
  if (likely(name_attr)) {
      ret = PyObject_RichCompareBool(name_attr, name, Py_EQ);
  } else {
      ret = -1;
  }
  if (unlikely(ret < 0)) {
      PyErr_Clear();
      ret = 0;
  }
  Py_XDECREF(name_attr);
  return ret;
}
static int __Pyx_setup_reduce(PyObject* type_obj) {
    int ret = 0;
    PyObject *object_reduce = NULL;
    PyObject *object_getstate = NULL;
    PyObject *object_reduce_ex = NULL;
    PyObject *reduce = NULL;
    PyObject *reduce_ex = NULL;
    PyObject *reduce_cython = NULL;
    PyObject *setstate = NULL;
    PyObject *setstate_cython = NULL;
    PyObject *getstate = NULL;
#if CYTHON_USE_PYTYPE_LOOKUP
    getstate = _PyType_Lookup((PyTypeObject*)type_obj, __pyx_n_s_getstate);
#else
    getstate = __Pyx_PyObject_GetAttrStrNoError(type_obj, __pyx_n_s_getstate);
    if (!getstate && PyErr_Occurred()) {
        goto __PYX_BAD;
    }
#endif
    if (getstate) {
#if CYTHON_USE_PYTYPE_LOOKUP
        object_getstate = _PyType_Lookup(&PyBaseObject_Type, __pyx_n_s_getstate);
#else
        object_getstate = __Pyx_PyObject_GetAttrStrNoError((PyObject*)&PyBaseObject_Type, __pyx_n_s_getstate);
        if (!object_getstate && PyErr_Occurred()) {
            goto __PYX_BAD;
        }
#endif
        if (object_getstate != getstate) {
            goto __PYX_GOOD;
        }
    }
#if CYTHON_USE_PYTYPE_LOOKUP
    object_reduce_ex = _PyType_Lookup(&PyBaseObject_Type, __pyx_n_s_reduce_ex); if (!object_reduce_ex) goto __PYX_BAD;
#else
    object_reduce_ex = __Pyx_PyObject_GetAttrStr((PyObject*)&PyBaseObject_Type, __pyx_n_s_reduce_ex); if (!object_reduce_ex) goto __PYX_BAD;
#endif
    reduce_ex = __Pyx_PyObject_GetAttrStr(type_obj, __pyx_n_s_reduce_ex); if (unlikely(!reduce_ex)) goto __PYX_BAD;
    if (reduce_ex == object_reduce_ex) {
#if CYTHON_USE_PYTYPE_LOOKUP
        object_reduce = _PyType_Lookup(&PyBaseObject_Type, __pyx_n_s_reduce); if (!object_reduce) goto __PYX_BAD;
#else
        object_reduce = __Pyx_PyObject_GetAttrStr((PyObject*)&PyBaseObject_Type, __pyx_n_s_reduce); if (!object_reduce) goto __PYX_BAD;
#endif
        reduce = __Pyx_PyObject_GetAttrStr(type_obj, __pyx_n_s_reduce); if (unlikely(!reduce)) goto __PYX_BAD;
        if (reduce == object_reduce || __Pyx_setup_reduce_is_named(reduce, __pyx_n_s_reduce_cython)) {
            reduce_cython = __Pyx_PyObject_GetAttrStrNoError(type_obj, __pyx_n_s_reduce_cython);
            if (likely(reduce_cython)) {
                ret = PyDict_SetItem(((PyTypeObject*)type_obj)->tp_dict, __pyx_n_s_reduce, reduce_cython); if (unlikely(ret < 0)) goto __PYX_BAD;
                ret = PyDict_DelItem(((PyTypeObject*)type_obj)->tp_dict, __pyx_n_s_reduce_cython); if (unlikely(ret < 0)) goto __PYX_BAD;
            } else if (reduce == object_reduce || PyErr_Occurred()) {
                goto __PYX_BAD;
            }
            setstate = __Pyx_PyObject_GetAttrStr(type_obj, __pyx_n_s_setstate);
            if (!setstate) PyErr_Clear();
            if (!setstate || __Pyx_setup_reduce_is_named(setstate, __pyx_n_s_setstate_cython)) {
                setstate_cython = __Pyx_PyObject_GetAttrStrNoError(type_obj, __pyx_n_s_setstate_cython);
                if (likely(setstate_cython)) {
                    ret = PyDict_SetItem(((PyTypeObject*)type_obj)->tp_dict, __pyx_n_s_setstate, setstate_cython); if (unlikely(ret < 0)) goto __PYX_BAD;
                    ret = PyDict_DelItem(((PyTypeObject*)type_obj)->tp_dict, __pyx_n_s_setstate_cython); if (unlikely(ret < 0)) goto __PYX_BAD;
                } else if (!setstate || PyErr_Occurred()) {
                    goto __PYX_BAD;
                }
            }
            PyType_Modified((PyTypeObject*)type_obj);
        }
    }
    goto __PYX_GOOD;
__PYX_BAD:
    if (!PyErr_Occurred())
        PyErr_Format(PyExc_RuntimeError, "Unable to initialize pickling for %s", ((PyTypeObject*)type_obj)->tp_name);
    ret = -1;
__PYX_GOOD:
#if !CYTHON_USE_PYTYPE_LOOKUP
    Py_XDECREF(object_reduce);
    Py_XDECREF(object_reduce_ex);
    Py_XDECREF(object_getstate);
    Py_XDECREF(getstate);
#endif
    Py_XDECREF(reduce);
    Py_XDECREF(reduce_ex);
    Py_XDECREF(reduce_cython);
    Py_XDECREF(setstate);
    Py_XDECREF(setstate_cython);
    return ret;
}

/* Import */
static PyObject *__Pyx_Import(PyObject *name, PyObject *from_list, int level) {
    PyObject *empty_list = 0;
    PyObject *module = 0;
    PyObject *global_dict = 0;
    PyObject *empty_dict = 0;
    PyObject *list;
    #if PY_MAJOR_VERSION < 3
    PyObject *py_import;
    py_import = __Pyx_PyObject_GetAttrStr(__pyx_b, __pyx_n_s_import);
    if (!py_import)
        goto bad;
    #endif
    if (from_list)
        list = from_list;
    else {
        empty_list = PyList_New(0);
        if (!empty_list)
            goto bad;
        list = empty_list;
    }
    global_dict = PyModule_GetDict(__pyx_m);
    if (!global_dict)
        goto bad;
    empty_dict = PyDict_New();
    if (!empty_dict)
        goto bad;
    {
        #if PY_MAJOR_VERSION >= 3
        if (level == -1) {
            if ((1) && (strchr(__Pyx_MODULE_NAME, '.'))) {
                module = PyImport_ImportModuleLevelObject(
                    name, global_dict, empty_dict, list, 1);
                if (!module) {
                    if (!PyErr_ExceptionMatches(PyExc_ImportError))
                        goto bad;
                    PyErr_Clear();
                }
            }
            level = 0;
        }
        #endif
        if (!module) {
            #if PY_MAJOR_VERSION < 3
            PyObject *py_level = PyInt_FromLong(level);
            if (!py_level)
                goto bad;
            module = PyObject_CallFunctionObjArgs(py_import,
                name, global_dict, empty_dict, list, py_level, (PyObject *)NULL);
            Py_DECREF(py_level);
            #else
            module = PyImport_ImportModuleLevelObject(
                name, global_dict, empty_dict, list, level);
            #endif
        }
    }
bad:
    #if PY_MAJOR_VERSION < 3
    Py_XDECREF(py_import);
    #endif
    Py_XDECREF(empty_list);
    Py_XDECREF(empty_dict);
    return module;
}

/* ImportFrom */
static PyObject* __Pyx_ImportFrom(PyObject* module, PyObject* name) {
    PyObject* value = __Pyx_PyObject_GetAttrStr(module, name);
    if (unlikely(!value) && PyErr_ExceptionMatches(PyExc_AttributeError)) {
        PyErr_Format(PyExc_ImportError,
        #if PY_MAJOR_VERSION < 3
            "cannot import name %.230s", PyString_AS_STRING(name));
        #else
            "cannot import name %S", name);
        #endif
    }
    return value;
}

/* CLineInTraceback */
#ifndef CYTHON_CLINE_IN_TRACEBACK
static int __Pyx_CLineForTraceback(CYTHON_NCP_UNUSED PyThreadState *tstate, int c_line) {
    PyObject *use_cline;
    PyObject *ptype, *pvalue, *ptraceback;
#if CYTHON_COMPILING_IN_CPYTHON
    PyObject **cython_runtime_dict;
#endif
    if (unlikely(!__pyx_cython_runtime)) {
        return c_line;
    }
    __Pyx_ErrFetchInState(tstate, &ptype, &pvalue, &ptraceback);
#if CYTHON_COMPILING_IN_CPYTHON
    cython_runtime_dict = _PyObject_GetDictPtr(__pyx_cython_runtime);
    if (likely(cython_runtime_dict)) {
        __PYX_PY_DICT_LOOKUP_IF_MODIFIED(
            use_cline, *cython_runtime_dict,
            __Pyx_PyDict_GetItemStr(*cython_runtime_dict, __pyx_n_s_cline_in_traceback))
    } else
#endif
    {
      PyObject *use_cline_obj = __Pyx_PyObject_GetAttrStr(__pyx_cython_runtime, __pyx_n_s_cline_in_traceback);
      if (use_cline_obj) {
        use_cline = PyObject_Not(use_cline_obj) ? Py_False : Py_True;
        Py_DECREF(use_cline_obj);
      } else {
        PyErr_Clear();
        use_cline = NULL;
      }
    }
    if (!use_cline) {
        c_line = 0;
        (void) PyObject_SetAttr(__pyx_cython_runtime, __pyx_n_s_cline_in_traceback, Py_False);
    }
    else if (use_cline == Py_False || (use_cline != Py_True && PyObject_Not(use_cline) != 0)) {
        c_line = 0;
    }
    __Pyx_ErrRestoreInState(tstate, ptype, pvalue, ptraceback);
    return c_line;
}
#endif

/* CodeObjectCache */
static int __pyx_bisect_code_objects(__Pyx_CodeObjectCacheEntry* entries, int count, int code_line) {
    int start = 0, mid = 0, end = count - 1;
    if (end >= 0 && code_line > entries[end].code_line) {
        return count;
    }
    while (start < end) {
        mid = start + (end - start) / 2;
        if (code_line < entries[mid].code_line) {
            end = mid;
        } else if (code_line > entries[mid].code_line) {
             start = mid + 1;
        } else {
            return mid;
        }
    }
    if (code_line <= entries[mid].code_line) {
        return mid;
    } else {
        return mid + 1;
    }
}
static PyCodeObject *__pyx_find_code_object(int code_line) {
    PyCodeObject* code_object;
    int pos;
    if (unlikely(!code_line) || unlikely(!__pyx_code_cache.entries)) {
        return NULL;
    }
    pos = __pyx_bisect_code_objects(__pyx_code_cache.entries, __pyx_code_cache.count, code_line);
    if (unlikely(pos >= __pyx_code_cache.count) || unlikely(__pyx_code_cache.entries[pos].code_line != code_line)) {
        return NULL;
    }
    code_object = __pyx_code_cache.entries[pos].code_object;
    Py_INCREF(code_object);
    return code_object;
}
static void __pyx_insert_code_object(int code_line, PyCodeObject* code_object) {
    int pos, i;
    __Pyx_CodeObjectCacheEntry* entries = __pyx_code_cache.entries;
    if (unlikely(!code_line)) {
        return;
    }
    if (unlikely(!entries)) {
        entries = (__Pyx_CodeObjectCacheEntry*)PyMem_Malloc(64*sizeof(__Pyx_CodeObjectCacheEntry));
        if (likely(entries)) {
            __pyx_code_cache.entries = entries;
            __pyx_code_cache.max_count = 64;
            __pyx_code_cache.count = 1;
            entries[0].code_line = code_line;
            entries[0].code_object = code_object;
            Py_INCREF(code_object);
        }
        return;
    }
    pos = __pyx_bisect_code_objects(__pyx_code_cache.entries, __pyx_code_cache.count, code_line);
    if ((pos < __pyx_code_cache.count) && unlikely(__pyx_code_cache.entries[pos].code_line == code_line)) {
        PyCodeObject* tmp = entries[pos].code_object;
        entries[pos].code_object = code_object;
        Py_DECREF(tmp);
        return;
    }
    if (__pyx_code_cache.count == __pyx_code_cache.max_count) {
        int new_max = __pyx_code_cache.max_count + 64;
        entries = (__Pyx_CodeObjectCacheEntry*)PyMem_Realloc(
            __pyx_code_cache.entries, ((size_t)new_max) * sizeof(__Pyx_CodeObjectCacheEntry));
        if (unlikely(!entries)) {
            return;
        }
        __pyx_code_cache.entries = entries;
        __pyx_code_cache.max_count = new_max;
    }
    for (i=__pyx_code_cache.count; i>pos; i--) {
        entries[i] = entries[i-1];
    }
    entries[pos].code_line = code_line;
    entries[pos].code_object = code_object;
    __pyx_code_cache.count++;
    Py_INCREF(code_object);
}

/* AddTraceback */
#include "compile.h"
#include "frameobject.h"
#include "traceback.h"
#if PY_VERSION_HEX >= 0x030b00a6
  #ifndef Py_BUILD_CORE
    #define Py_BUILD_CORE 1
  #endif
  #include "internal/pycore_frame.h"
#endif
static PyCodeObject* __Pyx_CreateCodeObjectForTraceback(
            const char *funcname, int c_line,
            int py_line, const char *filename) {
    PyCodeObject *py_code = NULL;
    PyObject *py_funcname = NULL;
    #if PY_MAJOR_VERSION < 3
    PyObject *py_srcfile = NULL;
    py_srcfile = PyString_FromString(filename);
    if (!py_srcfile) goto bad;
    #endif
    if (c_line) {
        #if PY_MAJOR_VERSION < 3
        py_funcname = PyString_FromFormat( "%s (%s:%d)", funcname, __pyx_cfilenm, c_line);
        if (!py_funcname) goto bad;
        #else
        py_funcname = PyUnicode_FromFormat( "%s (%s:%d)", funcname, __pyx_cfilenm, c_line);
        if (!py_funcname) goto bad;
        funcname = PyUnicode_AsUTF8(py_funcname);
        if (!funcname) goto bad;
        #endif
    }
    else {
        #if PY_MAJOR_VERSION < 3
        py_funcname = PyString_FromString(funcname);
        if (!py_funcname) goto bad;
        #endif
    }
    #if PY_MAJOR_VERSION < 3
    py_code = __Pyx_PyCode_New(
        0,
        0,
        0,
        0,
        0,
        __pyx_empty_bytes, /*PyObject *code,*/
        __pyx_empty_tuple, /*PyObject *consts,*/
        __pyx_empty_tuple, /*PyObject *names,*/
        __pyx_empty_tuple, /*PyObject *varnames,*/
        __pyx_empty_tuple, /*PyObject *freevars,*/
        __pyx_empty_tuple, /*PyObject *cellvars,*/
        py_srcfile,   /*PyObject *filename,*/
        py_funcname,  /*PyObject *name,*/
        py_line,
        __pyx_empty_bytes  /*PyObject *lnotab*/
    );
    Py_DECREF(py_srcfile);
    #else
    py_code = PyCode_NewEmpty(filename, funcname, py_line);
    #endif
    Py_XDECREF(py_funcname);  // XDECREF since it's only set on Py3 if cline
    return py_code;
bad:
    Py_XDECREF(py_funcname);
    #if PY_MAJOR_VERSION < 3
    Py_XDECREF(py_srcfile);
    #endif
    return NULL;
}
static void __Pyx_AddTraceback(const char *funcname, int c_line,
                               int py_line, const char *filename) {
    PyCodeObject *py_code = 0;
    PyFrameObject *py_frame = 0;
    PyThreadState *tstate = __Pyx_PyThreadState_Current;
    PyObject *ptype, *pvalue, *ptraceback;
    if (c_line) {
        c_line = __Pyx_CLineForTraceback(tstate, c_line);
    }
    py_code = __pyx_find_code_object(c_line ? -c_line : py_line);
    if (!py_code) {
        __Pyx_ErrFetchInState(tstate, &ptype, &pvalue, &ptraceback);
        py_code = __Pyx_CreateCodeObjectForTraceback(
            funcname, c_line, py_line, filename);
        if (!py_code) {
            /* If the code object creation fails, then we should clear the
               fetched exception references and propagate the new exception */
            Py_XDECREF(ptype);
            Py_XDECREF(pvalue);
            Py_XDECREF(ptraceback);
            goto bad;
        }
        __Pyx_ErrRestoreInState(tstate, ptype, pvalue, ptraceback);
        __pyx_insert_code_object(c_line ? -c_line : py_line, py_code);
    }
    py_frame = PyFrame_New(
        tstate,            /*PyThreadState *tstate,*/
        py_code,           /*PyCodeObject *code,*/
        __pyx_d,    /*PyObject *globals,*/
        0                  /*PyObject *locals*/
    );
    if (!py_frame) goto bad;
    __Pyx_PyFrame_SetLineNumber(py_frame, py_line);
    PyTraceBack_Here(py_frame);
bad:
    Py_XDECREF(py_code);
    Py_XDECREF(py_frame);
}

/* CIntFromPyVerify */
#define __PYX_VERIFY_RETURN_INT(target_type, func_type, func_value)\
    __PYX__VERIFY_RETURN_INT(target_type, func_type, func_value, 0)
#define __PYX_VERIFY_RETURN_INT_EXC(target_type, func_type, func_value)\
    __PYX__VERIFY_RETURN_INT(target_type, func_type, func_value, 1)
#define __PYX__VERIFY_RETURN_INT(target_type, func_type, func_value, exc)\
    {\
        func_type value = func_value;\
        if (sizeof(target_type) < sizeof(func_type)) {\
            if (unlikely(value != (func_type) (target_type) value)) {\
                func_type zero = 0;\
                if (exc && unlikely(value == (func_type)-1 && PyErr_Occurred()))\
                    return (target_type) -1;\
                if (is_unsigned && unlikely(value < zero))\
                    goto raise_neg_overflow;\
                else\
                    goto raise_overflow;\
            }\
        }\
        return (target_type) value;\
    }

/* CIntFromPy */
static CYTHON_INLINE int __Pyx_PyInt_As_int(PyObject *x) {
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#endif
    const int neg_one = (int) -1, const_zero = (int) 0;
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic pop
#endif
    const int is_unsigned = neg_one > const_zero;
#if PY_MAJOR_VERSION < 3
    if (likely(PyInt_Check(x))) {
        if (sizeof(int) < sizeof(long)) {
            __PYX_VERIFY_RETURN_INT(int, long, PyInt_AS_LONG(x))
        } else {
            long val = PyInt_AS_LONG(x);
            if (is_unsigned && unlikely(val < 0)) {
                goto raise_neg_overflow;
            }
            return (int) val;
        }
    } else
#endif
    if (likely(PyLong_Check(x))) {
        if (is_unsigned) {
#if CYTHON_USE_PYLONG_INTERNALS
            const digit* digits = ((PyLongObject*)x)->ob_digit;
            switch (Py_SIZE(x)) {
                case  0: return (int) 0;
                case  1: __PYX_VERIFY_RETURN_INT(int, digit, digits[0])
                case 2:
                    if (8 * sizeof(int) > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) >= 2 * PyLong_SHIFT) {
                            return (int) (((((int)digits[1]) << PyLong_SHIFT) | (int)digits[0]));
                        }
                    }
                    break;
                case 3:
                    if (8 * sizeof(int) > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) >= 3 * PyLong_SHIFT) {
                            return (int) (((((((int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0]));
                        }
                    }
                    break;
                case 4:
                    if (8 * sizeof(int) > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) >= 4 * PyLong_SHIFT) {
                            return (int) (((((((((int)digits[3]) << PyLong_SHIFT) | (int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0]));
                        }
                    }
                    break;
            }
#endif
#if CYTHON_COMPILING_IN_CPYTHON
            if (unlikely(Py_SIZE(x) < 0)) {
                goto raise_neg_overflow;
            }
#else
            {
                int result = PyObject_RichCompareBool(x, Py_False, Py_LT);
                if (unlikely(result < 0))
                    return (int) -1;
                if (unlikely(result == 1))
                    goto raise_neg_overflow;
            }
#endif
            if (sizeof(int) <= sizeof(unsigned long)) {
                __PYX_VERIFY_RETURN_INT_EXC(int, unsigned long, PyLong_AsUnsignedLong(x))
#ifdef HAVE_LONG_LONG
            } else if (sizeof(int) <= sizeof(unsigned PY_LONG_LONG)) {
                __PYX_VERIFY_RETURN_INT_EXC(int, unsigned PY_LONG_LONG, PyLong_AsUnsignedLongLong(x))
#endif
            }
        } else {
#if CYTHON_USE_PYLONG_INTERNALS
            const digit* digits = ((PyLongObject*)x)->ob_digit;
            switch (Py_SIZE(x)) {
                case  0: return (int) 0;
                case -1: __PYX_VERIFY_RETURN_INT(int, sdigit, (sdigit) (-(sdigit)digits[0]))
                case  1: __PYX_VERIFY_RETURN_INT(int,  digit, +digits[0])
                case -2:
                    if (8 * sizeof(int) - 1 > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, long, -(long) (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 2 * PyLong_SHIFT) {
                            return (int) (((int)-1)*(((((int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
                case 2:
                    if (8 * sizeof(int) > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 2 * PyLong_SHIFT) {
                            return (int) ((((((int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
                case -3:
                    if (8 * sizeof(int) - 1 > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, long, -(long) (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 3 * PyLong_SHIFT) {
                            return (int) (((int)-1)*(((((((int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
                case 3:
                    if (8 * sizeof(int) > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 3 * PyLong_SHIFT) {
                            return (int) ((((((((int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
                case -4:
                    if (8 * sizeof(int) - 1 > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, long, -(long) (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 4 * PyLong_SHIFT) {
                            return (int) (((int)-1)*(((((((((int)digits[3]) << PyLong_SHIFT) | (int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
                case 4:
                    if (8 * sizeof(int) > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(int, unsigned long, (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(int) - 1 > 4 * PyLong_SHIFT) {
                            return (int) ((((((((((int)digits[3]) << PyLong_SHIFT) | (int)digits[2]) << PyLong_SHIFT) | (int)digits[1]) << PyLong_SHIFT) | (int)digits[0])));
                        }
                    }
                    break;
            }
#endif
            if (sizeof(int) <= sizeof(long)) {
                __PYX_VERIFY_RETURN_INT_EXC(int, long, PyLong_AsLong(x))
#ifdef HAVE_LONG_LONG
            } else if (sizeof(int) <= sizeof(PY_LONG_LONG)) {
                __PYX_VERIFY_RETURN_INT_EXC(int, PY_LONG_LONG, PyLong_AsLongLong(x))
#endif
            }
        }
        {
#if CYTHON_COMPILING_IN_PYPY && !defined(_PyLong_AsByteArray)
            PyErr_SetString(PyExc_RuntimeError,
                            "_PyLong_AsByteArray() not available in PyPy, cannot convert large numbers");
#else
            int val;
            PyObject *v = __Pyx_PyNumber_IntOrLong(x);
 #if PY_MAJOR_VERSION < 3
            if (likely(v) && !PyLong_Check(v)) {
                PyObject *tmp = v;
                v = PyNumber_Long(tmp);
                Py_DECREF(tmp);
            }
 #endif
            if (likely(v)) {
                int one = 1; int is_little = (int)*(unsigned char *)&one;
                unsigned char *bytes = (unsigned char *)&val;
                int ret = _PyLong_AsByteArray((PyLongObject *)v,
                                              bytes, sizeof(val),
                                              is_little, !is_unsigned);
                Py_DECREF(v);
                if (likely(!ret))
                    return val;
            }
#endif
            return (int) -1;
        }
    } else {
        int val;
        PyObject *tmp = __Pyx_PyNumber_IntOrLong(x);
        if (!tmp) return (int) -1;
        val = __Pyx_PyInt_As_int(tmp);
        Py_DECREF(tmp);
        return val;
    }
raise_overflow:
    PyErr_SetString(PyExc_OverflowError,
        "value too large to convert to int");
    return (int) -1;
raise_neg_overflow:
    PyErr_SetString(PyExc_OverflowError,
        "can't convert negative value to int");
    return (int) -1;
}

/* CIntToPy */
static CYTHON_INLINE PyObject* __Pyx_PyInt_From_long(long value) {
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#endif
    const long neg_one = (long) -1, const_zero = (long) 0;
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic pop
#endif
    const int is_unsigned = neg_one > const_zero;
    if (is_unsigned) {
        if (sizeof(long) < sizeof(long)) {
            return PyInt_FromLong((long) value);
        } else if (sizeof(long) <= sizeof(unsigned long)) {
            return PyLong_FromUnsignedLong((unsigned long) value);
#ifdef HAVE_LONG_LONG
        } else if (sizeof(long) <= sizeof(unsigned PY_LONG_LONG)) {
            return PyLong_FromUnsignedLongLong((unsigned PY_LONG_LONG) value);
#endif
        }
    } else {
        if (sizeof(long) <= sizeof(long)) {
            return PyInt_FromLong((long) value);
#ifdef HAVE_LONG_LONG
        } else if (sizeof(long) <= sizeof(PY_LONG_LONG)) {
            return PyLong_FromLongLong((PY_LONG_LONG) value);
#endif
        }
    }
    {
        int one = 1; int little = (int)*(unsigned char *)&one;
        unsigned char *bytes = (unsigned char *)&value;
        return _PyLong_FromByteArray(bytes, sizeof(long),
                                     little, !is_unsigned);
    }
}

/* CIntToPy */
static CYTHON_INLINE PyObject* __Pyx_PyInt_From_int(int value) {
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#endif
    const int neg_one = (int) -1, const_zero = (int) 0;
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic pop
#endif
    const int is_unsigned = neg_one > const_zero;
    if (is_unsigned) {
        if (sizeof(int) < sizeof(long)) {
            return PyInt_FromLong((long) value);
        } else if (sizeof(int) <= sizeof(unsigned long)) {
            return PyLong_FromUnsignedLong((unsigned long) value);
#ifdef HAVE_LONG_LONG
        } else if (sizeof(int) <= sizeof(unsigned PY_LONG_LONG)) {
            return PyLong_FromUnsignedLongLong((unsigned PY_LONG_LONG) value);
#endif
        }
    } else {
        if (sizeof(int) <= sizeof(long)) {
            return PyInt_FromLong((long) value);
#ifdef HAVE_LONG_LONG
        } else if (sizeof(int) <= sizeof(PY_LONG_LONG)) {
            return PyLong_FromLongLong((PY_LONG_LONG) value);
#endif
        }
    }
    {
        int one = 1; int little = (int)*(unsigned char *)&one;
        unsigned char *bytes = (unsigned char *)&value;
        return _PyLong_FromByteArray(bytes, sizeof(int),
                                     little, !is_unsigned);
    }
}

/* CIntFromPy */
static CYTHON_INLINE long __Pyx_PyInt_As_long(PyObject *x) {
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#endif
    const long neg_one = (long) -1, const_zero = (long) 0;
#ifdef __Pyx_HAS_GCC_DIAGNOSTIC
#pragma GCC diagnostic pop
#endif
    const int is_unsigned = neg_one > const_zero;
#if PY_MAJOR_VERSION < 3
    if (likely(PyInt_Check(x))) {
        if (sizeof(long) < sizeof(long)) {
            __PYX_VERIFY_RETURN_INT(long, long, PyInt_AS_LONG(x))
        } else {
            long val = PyInt_AS_LONG(x);
            if (is_unsigned && unlikely(val < 0)) {
                goto raise_neg_overflow;
            }
            return (long) val;
        }
    } else
#endif
    if (likely(PyLong_Check(x))) {
        if (is_unsigned) {
#if CYTHON_USE_PYLONG_INTERNALS
            const digit* digits = ((PyLongObject*)x)->ob_digit;
            switch (Py_SIZE(x)) {
                case  0: return (long) 0;
                case  1: __PYX_VERIFY_RETURN_INT(long, digit, digits[0])
                case 2:
                    if (8 * sizeof(long) > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) >= 2 * PyLong_SHIFT) {
                            return (long) (((((long)digits[1]) << PyLong_SHIFT) | (long)digits[0]));
                        }
                    }
                    break;
                case 3:
                    if (8 * sizeof(long) > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) >= 3 * PyLong_SHIFT) {
                            return (long) (((((((long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0]));
                        }
                    }
                    break;
                case 4:
                    if (8 * sizeof(long) > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) >= 4 * PyLong_SHIFT) {
                            return (long) (((((((((long)digits[3]) << PyLong_SHIFT) | (long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0]));
                        }
                    }
                    break;
            }
#endif
#if CYTHON_COMPILING_IN_CPYTHON
            if (unlikely(Py_SIZE(x) < 0)) {
                goto raise_neg_overflow;
            }
#else
            {
                int result = PyObject_RichCompareBool(x, Py_False, Py_LT);
                if (unlikely(result < 0))
                    return (long) -1;
                if (unlikely(result == 1))
                    goto raise_neg_overflow;
            }
#endif
            if (sizeof(long) <= sizeof(unsigned long)) {
                __PYX_VERIFY_RETURN_INT_EXC(long, unsigned long, PyLong_AsUnsignedLong(x))
#ifdef HAVE_LONG_LONG
            } else if (sizeof(long) <= sizeof(unsigned PY_LONG_LONG)) {
                __PYX_VERIFY_RETURN_INT_EXC(long, unsigned PY_LONG_LONG, PyLong_AsUnsignedLongLong(x))
#endif
            }
        } else {
#if CYTHON_USE_PYLONG_INTERNALS
            const digit* digits = ((PyLongObject*)x)->ob_digit;
            switch (Py_SIZE(x)) {
                case  0: return (long) 0;
                case -1: __PYX_VERIFY_RETURN_INT(long, sdigit, (sdigit) (-(sdigit)digits[0]))
                case  1: __PYX_VERIFY_RETURN_INT(long,  digit, +digits[0])
                case -2:
                    if (8 * sizeof(long) - 1 > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, long, -(long) (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 2 * PyLong_SHIFT) {
                            return (long) (((long)-1)*(((((long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
                case 2:
                    if (8 * sizeof(long) > 1 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 2 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 2 * PyLong_SHIFT) {
                            return (long) ((((((long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
                case -3:
                    if (8 * sizeof(long) - 1 > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, long, -(long) (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 3 * PyLong_SHIFT) {
                            return (long) (((long)-1)*(((((((long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
                case 3:
                    if (8 * sizeof(long) > 2 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 3 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((((unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 3 * PyLong_SHIFT) {
                            return (long) ((((((((long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
                case -4:
                    if (8 * sizeof(long) - 1 > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, long, -(long) (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 4 * PyLong_SHIFT) {
                            return (long) (((long)-1)*(((((((((long)digits[3]) << PyLong_SHIFT) | (long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
                case 4:
                    if (8 * sizeof(long) > 3 * PyLong_SHIFT) {
                        if (8 * sizeof(unsigned long) > 4 * PyLong_SHIFT) {
                            __PYX_VERIFY_RETURN_INT(long, unsigned long, (((((((((unsigned long)digits[3]) << PyLong_SHIFT) | (unsigned long)digits[2]) << PyLong_SHIFT) | (unsigned long)digits[1]) << PyLong_SHIFT) | (unsigned long)digits[0])))
                        } else if (8 * sizeof(long) - 1 > 4 * PyLong_SHIFT) {
                            return (long) ((((((((((long)digits[3]) << PyLong_SHIFT) | (long)digits[2]) << PyLong_SHIFT) | (long)digits[1]) << PyLong_SHIFT) | (long)digits[0])));
                        }
                    }
                    break;
            }
#endif
            if (sizeof(long) <= sizeof(long)) {
                __PYX_VERIFY_RETURN_INT_EXC(long, long, PyLong_AsLong(x))
#ifdef HAVE_LONG_LONG
            } else if (sizeof(long) <= sizeof(PY_LONG_LONG)) {
                __PYX_VERIFY_RETURN_INT_EXC(long, PY_LONG_LONG, PyLong_AsLongLong(x))
#endif
            }
        }
        {
#if CYTHON_COMPILING_IN_PYPY && !defined(_PyLong_AsByteArray)
            PyErr_SetString(PyExc_RuntimeError,
                            "_PyLong_AsByteArray() not available in PyPy, cannot convert large numbers");
#else
            long val;
            PyObject *v = __Pyx_PyNumber_IntOrLong(x);
 #if PY_MAJOR_VERSION < 3
            if (likely(v) && !PyLong_Check(v)) {
                PyObject *tmp = v;
                v = PyNumber_Long(tmp);
                Py_DECREF(tmp);
            }
 #endif
            if (likely(v)) {
                int one = 1; int is_little = (int)*(unsigned char *)&one;
                unsigned char *bytes = (unsigned char *)&val;
                int ret = _PyLong_AsByteArray((PyLongObject *)v,
                                              bytes, sizeof(val),
                                              is_little, !is_unsigned);
                Py_DECREF(v);
                if (likely(!ret))
                    return val;
            }
#endif
            return (long) -1;
        }
    } else {
        long val;
        PyObject *tmp = __Pyx_PyNumber_IntOrLong(x);
        if (!tmp) return (long) -1;
        val = __Pyx_PyInt_As_long(tmp);
        Py_DECREF(tmp);
        return val;
    }
raise_overflow:
    PyErr_SetString(PyExc_OverflowError,
        "value too large to convert to long");
    return (long) -1;
raise_neg_overflow:
    PyErr_SetString(PyExc_OverflowError,
        "can't convert negative value to long");
    return (long) -1;
}

/* CheckBinaryVersion */
static int __Pyx_check_binary_version(void) {
    char ctversion[5];
    int same=1, i, found_dot;
    const char* rt_from_call = Py_GetVersion();
    PyOS_snprintf(ctversion, 5, "%d.%d", PY_MAJOR_VERSION, PY_MINOR_VERSION);
    found_dot = 0;
    for (i = 0; i < 4; i++) {
        if (!ctversion[i]) {
            same = (rt_from_call[i] < '0' || rt_from_call[i] > '9');
            break;
        }
        if (rt_from_call[i] != ctversion[i]) {
            same = 0;
            break;
        }
    }
    if (!same) {
        char rtversion[5] = {'\0'};
        char message[200];
        for (i=0; i<4; ++i) {
            if (rt_from_call[i] == '.') {
                if (found_dot) break;
                found_dot = 1;
            } else if (rt_from_call[i] < '0' || rt_from_call[i] > '9') {
                break;
            }
            rtversion[i] = rt_from_call[i];
        }
        PyOS_snprintf(message, sizeof(message),
                      "compiletime version %s of module '%.100s' "
                      "does not match runtime version %s",
                      ctversion, __Pyx_MODULE_NAME, rtversion);
        return PyErr_WarnEx(NULL, message, 1);
    }
    return 0;
}

/* InitStrings */
static int __Pyx_InitStrings(__Pyx_StringTabEntry *t) {
    while (t->p) {
        #if PY_MAJOR_VERSION < 3
        if (t->is_unicode) {
            *t->p = PyUnicode_DecodeUTF8(t->s, t->n - 1, NULL);
        } else if (t->intern) {
            *t->p = PyString_InternFromString(t->s);
        } else {
            *t->p = PyString_FromStringAndSize(t->s, t->n - 1);
        }
        #else
        if (t->is_unicode | t->is_str) {
            if (t->intern) {
                *t->p = PyUnicode_InternFromString(t->s);
            } else if (t->encoding) {
                *t->p = PyUnicode_Decode(t->s, t->n - 1, t->encoding, NULL);
            } else {
                *t->p = PyUnicode_FromStringAndSize(t->s, t->n - 1);
            }
        } else {
            *t->p = PyBytes_FromStringAndSize(t->s, t->n - 1);
        }
        #endif
        if (!*t->p)
            return -1;
        if (PyObject_Hash(*t->p) == -1)
            return -1;
        ++t;
    }
    return 0;
}

static CYTHON_INLINE PyObject* __Pyx_PyUnicode_FromString(const char* c_str) {
    return __Pyx_PyUnicode_FromStringAndSize(c_str, (Py_ssize_t)strlen(c_str));
}
static CYTHON_INLINE const char* __Pyx_PyObject_AsString(PyObject* o) {
    Py_ssize_t ignore;
    return __Pyx_PyObject_AsStringAndSize(o, &ignore);
}
#if __PYX_DEFAULT_STRING_ENCODING_IS_ASCII || __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT
#if !CYTHON_PEP393_ENABLED
static const char* __Pyx_PyUnicode_AsStringAndSize(PyObject* o, Py_ssize_t *length) {
    char* defenc_c;
    PyObject* defenc = _PyUnicode_AsDefaultEncodedString(o, NULL);
    if (!defenc) return NULL;
    defenc_c = PyBytes_AS_STRING(defenc);
#if __PYX_DEFAULT_STRING_ENCODING_IS_ASCII
    {
        char* end = defenc_c + PyBytes_GET_SIZE(defenc);
        char* c;
        for (c = defenc_c; c < end; c++) {
            if ((unsigned char) (*c) >= 128) {
                PyUnicode_AsASCIIString(o);
                return NULL;
            }
        }
    }
#endif
    *length = PyBytes_GET_SIZE(defenc);
    return defenc_c;
}
#else
static CYTHON_INLINE const char* __Pyx_PyUnicode_AsStringAndSize(PyObject* o, Py_ssize_t *length) {
    if (unlikely(__Pyx_PyUnicode_READY(o) == -1)) return NULL;
#if __PYX_DEFAULT_STRING_ENCODING_IS_ASCII
    if (likely(PyUnicode_IS_ASCII(o))) {
        *length = PyUnicode_GET_LENGTH(o);
        return PyUnicode_AsUTF8(o);
    } else {
        PyUnicode_AsASCIIString(o);
        return NULL;
    }
#else
    return PyUnicode_AsUTF8AndSize(o, length);
#endif
}
#endif
#endif
static CYTHON_INLINE const char* __Pyx_PyObject_AsStringAndSize(PyObject* o, Py_ssize_t *length) {
#if __PYX_DEFAULT_STRING_ENCODING_IS_ASCII || __PYX_DEFAULT_STRING_ENCODING_IS_DEFAULT
    if (
#if PY_MAJOR_VERSION < 3 && __PYX_DEFAULT_STRING_ENCODING_IS_ASCII
            __Pyx_sys_getdefaultencoding_not_ascii &&
#endif
            PyUnicode_Check(o)) {
        return __Pyx_PyUnicode_AsStringAndSize(o, length);
    } else
#endif
#if (!CYTHON_COMPILING_IN_PYPY) || (defined(PyByteArray_AS_STRING) && defined(PyByteArray_GET_SIZE))
    if (PyByteArray_Check(o)) {
        *length = PyByteArray_GET_SIZE(o);
        return PyByteArray_AS_STRING(o);
    } else
#endif
    {
        char* result;
        int r = PyBytes_AsStringAndSize(o, &result, length);
        if (unlikely(r < 0)) {
            return NULL;
        } else {
            return result;
        }
    }
}
static CYTHON_INLINE int __Pyx_PyObject_IsTrue(PyObject* x) {
   int is_true = x == Py_True;
   if (is_true | (x == Py_False) | (x == Py_None)) return is_true;
   else return PyObject_IsTrue(x);
}
static CYTHON_INLINE int __Pyx_PyObject_IsTrueAndDecref(PyObject* x) {
    int retval;
    if (unlikely(!x)) return -1;
    retval = __Pyx_PyObject_IsTrue(x);
    Py_DECREF(x);
    return retval;
}
static PyObject* __Pyx_PyNumber_IntOrLongWrongResultType(PyObject* result, const char* type_name) {
#if PY_MAJOR_VERSION >= 3
    if (PyLong_Check(result)) {
        if (PyErr_WarnFormat(PyExc_DeprecationWarning, 1,
                "__int__ returned non-int (type %.200s).  "
                "The ability to return an instance of a strict subclass of int "
                "is deprecated, and may be removed in a future version of Python.",
                Py_TYPE(result)->tp_name)) {
            Py_DECREF(result);
            return NULL;
        }
        return result;
    }
#endif
    PyErr_Format(PyExc_TypeError,
                 "__%.4s__ returned non-%.4s (type %.200s)",
                 type_name, type_name, Py_TYPE(result)->tp_name);
    Py_DECREF(result);
    return NULL;
}
static CYTHON_INLINE PyObject* __Pyx_PyNumber_IntOrLong(PyObject* x) {
#if CYTHON_USE_TYPE_SLOTS
  PyNumberMethods *m;
#endif
  const char *name = NULL;
  PyObject *res = NULL;
#if PY_MAJOR_VERSION < 3
  if (likely(PyInt_Check(x) || PyLong_Check(x)))
#else
  if (likely(PyLong_Check(x)))
#endif
    return __Pyx_NewRef(x);
#if CYTHON_USE_TYPE_SLOTS
  m = Py_TYPE(x)->tp_as_number;
  #if PY_MAJOR_VERSION < 3
  if (m && m->nb_int) {
    name = "int";
    res = m->nb_int(x);
  }
  else if (m && m->nb_long) {
    name = "long";
    res = m->nb_long(x);
  }
  #else
  if (likely(m && m->nb_int)) {
    name = "int";
    res = m->nb_int(x);
  }
  #endif
#else
  if (!PyBytes_CheckExact(x) && !PyUnicode_CheckExact(x)) {
    res = PyNumber_Int(x);
  }
#endif
  if (likely(res)) {
#if PY_MAJOR_VERSION < 3
    if (unlikely(!PyInt_Check(res) && !PyLong_Check(res))) {
#else
    if (unlikely(!PyLong_CheckExact(res))) {
#endif
        return __Pyx_PyNumber_IntOrLongWrongResultType(res, name);
    }
  }
  else if (!PyErr_Occurred()) {
    PyErr_SetString(PyExc_TypeError,
                    "an integer is required");
  }
  return res;
}
static CYTHON_INLINE Py_ssize_t __Pyx_PyIndex_AsSsize_t(PyObject* b) {
  Py_ssize_t ival;
  PyObject *x;
#if PY_MAJOR_VERSION < 3
  if (likely(PyInt_CheckExact(b))) {
    if (sizeof(Py_ssize_t) >= sizeof(long))
        return PyInt_AS_LONG(b);
    else
        return PyInt_AsSsize_t(b);
  }
#endif
  if (likely(PyLong_CheckExact(b))) {
    #if CYTHON_USE_PYLONG_INTERNALS
    const digit* digits = ((PyLongObject*)b)->ob_digit;
    const Py_ssize_t size = Py_SIZE(b);
    if (likely(__Pyx_sst_abs(size) <= 1)) {
        ival = likely(size) ? digits[0] : 0;
        if (size == -1) ival = -ival;
        return ival;
    } else {
      switch (size) {
         case 2:
           if (8 * sizeof(Py_ssize_t) > 2 * PyLong_SHIFT) {
             return (Py_ssize_t) (((((size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
         case -2:
           if (8 * sizeof(Py_ssize_t) > 2 * PyLong_SHIFT) {
             return -(Py_ssize_t) (((((size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
         case 3:
           if (8 * sizeof(Py_ssize_t) > 3 * PyLong_SHIFT) {
             return (Py_ssize_t) (((((((size_t)digits[2]) << PyLong_SHIFT) | (size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
         case -3:
           if (8 * sizeof(Py_ssize_t) > 3 * PyLong_SHIFT) {
             return -(Py_ssize_t) (((((((size_t)digits[2]) << PyLong_SHIFT) | (size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
         case 4:
           if (8 * sizeof(Py_ssize_t) > 4 * PyLong_SHIFT) {
             return (Py_ssize_t) (((((((((size_t)digits[3]) << PyLong_SHIFT) | (size_t)digits[2]) << PyLong_SHIFT) | (size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
         case -4:
           if (8 * sizeof(Py_ssize_t) > 4 * PyLong_SHIFT) {
             return -(Py_ssize_t) (((((((((size_t)digits[3]) << PyLong_SHIFT) | (size_t)digits[2]) << PyLong_SHIFT) | (size_t)digits[1]) << PyLong_SHIFT) | (size_t)digits[0]));
           }
           break;
      }
    }
    #endif
    return PyLong_AsSsize_t(b);
  }
  x = PyNumber_Index(b);
  if (!x) return -1;
  ival = PyInt_AsSsize_t(x);
  Py_DECREF(x);
  return ival;
}
static CYTHON_INLINE Py_hash_t __Pyx_PyIndex_AsHash_t(PyObject* o) {
  if (sizeof(Py_hash_t) == sizeof(Py_ssize_t)) {
    return (Py_hash_t) __Pyx_PyIndex_AsSsize_t(o);
#if PY_MAJOR_VERSION < 3
  } else if (likely(PyInt_CheckExact(o))) {
    return PyInt_AS_LONG(o);
#endif
  } else {
    Py_ssize_t ival;
    PyObject *x;
    x = PyNumber_Index(o);
    if (!x) return -1;
    ival = PyInt_AsLong(x);
    Py_DECREF(x);
    return ival;
  }
}
static CYTHON_INLINE PyObject * __Pyx_PyBool_FromLong(long b) {
  return b ? __Pyx_NewRef(Py_True) : __Pyx_NewRef(Py_False);
}
static CYTHON_INLINE PyObject * __Pyx_PyInt_FromSize_t(size_t ival) {
    return PyInt_FromSize_t(ival);
}


#endif /* Py_PYTHON_H */
