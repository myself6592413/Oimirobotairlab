"""Oimi Processes:
	Copy of the real oimiprocess 
	Definition of global processes, queues and events.
	Queues are used to communicate between processes in different modules and subprocess, 
	by leaving and retrieving elements stored inside
	Signal defined and controlled directly inside subprocess
	Custom exception are defined for proper exiting from subprocess and then from main process
"""
# ===============================================================================================================
#  Imports:
# ===============================================================================================================
import sys,os
import signal
import psutil
import traceback
import multiprocessing
from multiprocessing import Process, Pool, Pipe, Event, Lock, Value, current_process
from multiprocessing.queues import Queue
from multiprocessing.managers import BaseManager
from queue import LifoQueue
from functools import partial
#import oimi_logging.oimi_logger as olog
import asyncio
#log = olog.logger
# ===============================================================================================================
#  Classes:
# ===============================================================================================================
class OimiQueue(Queue):
	""" OimiQueue class extend classic queue 
	_block=True means that the call to put() will be forbidden if the queue is full. 
	_no timeout expiration for adding elements 
	"""
	def __init__(self, name, maxsize=-1, block=True, timeout=None):
		self.name = name
		self.block = block
		self.timeout = timeout
		super().__init__(maxsize, ctx=multiprocessing.get_context())
	def clear(self):
		while not self.empty():
			self.get_nowait()

class OimiProcess(Process):
	""" OimiProcess template: 
		_global_status of oimi (check shared folder)
		_more args and kwargs depending on the process
		_A Pipe is a communication channel with only two endpoints (father, child)
		_signalint = handle an interruption/attention request
		_signalterm = handle a termination request
	"""
	def __init__(self, name, global_status, parent_id, execute, daemon, *args, **kwargs):
		Process.__init__(self)
		#super(OimiProcess, self).__init__()
		self.name = name
		self.global_status = global_status
		self.parent_id = parent_id
		self.execute = execute
		self.daemon = daemon
		self.args = args
		self.kwargs = kwargs
		self._strongStop_event = Event() 
		self._return = None
		self._parent_conn, self._child_conn = Pipe()
		self._exception = None
		self.signalint = signal.signal(signal.SIGINT, partial(self.sig_int_handler,self.parent_id))
		self.signalterm = signal.signal(signal.SIGTERM, partial(self.sig_term_handler,self.parent_id))
		#self.simplelog = olog.simplelog

	def run(self):
		try:
			if self.execute is not None:
				print("SUTIL {}".format(os.getpid()))
				print("enter in try ok")
				self._return = self.execute(self.global_status, *self.args, **self.kwargs)
				self._child_conn.send(None)
		except Exception as exc:
			print("arriva eccezione 1")
			log.error('eccezione process raised!', exc_info=True)
			print(exc)
			trab = traceback.format_exc()
			self._child_conn.send((exc, trab))

	@property
	def exception(self):
		if self._parent_conn.poll():
			self._exception = self._parent_conn.recv()
			return self._exception

	@classmethod
	def shutdown():
		print("Shutdown daje")
		_StopStrong_Event.set()

	@classmethod
	def reput():
		print("Reput daje")
		_strongStop_event.clear()

	def sig_int_handler(self, elem, signal_num, frame):
		signal.signal(signal_num, signal.SIG_IGN)
		print('signal num: %s' % signal_num)
		#log.info('signal num')
		parent = psutil.Process(self.parent_id)
		for child in parent.children():
			if child.pid != os.getpid():
				print("killing child: {} {} ".format(child.pid, current_process().name))
				child.kill()
		print("killing parent: {} {}".format(self.parent_id, current_process().name))
		parent.kill()
		print("suicide: %s" % os.getpid())
		psutil.Process(os.getpid()).kill()


	def sig_term_handler(self, elem, signal_num, frame):
		print('signal is : %s' % signal_num)
		parent = psutil.Process(self.parent_id)
		for son in parent.children():
			if son.pid != os.getpid():
				print("killing child %s" % son.pid)
				son.kill()
		print("killing parent %s" % self.parent_id)
		parent.kill()
		print("%s Turned off" % os.getpid())
		psutil.Process(os.getpid()).kill()
		#system.exit(1)


class OimiStartProcess(OimiProcess):
	"""SubProcesses that are really executed in parallel
	"""
	def __init__(self, name, sub_connection, global_status, parent_id, execute, daemon, *args, **kwargs):
		self.sub_connection = sub_connection
		super(OimiStartProcess, self).__init__(name, global_status, parent_id, execute, daemon, *args, **kwargs)
		#super().__init__(name, global_status, parent_id, execute, daemon, *args, **kwargs)

	def run(self):
		#if self.execute is not None:
		#	self._return = self.execute(self.sub_connection, *self.args, **self.kwargs)
		try:
			print("begin to run subprocess")
			if self.execute is not None:
				print("not none")
				self._return = self.execute(self.sub_connection, *self.args, **self.kwargs)
				#self._return = self.execute(self.global_status, self.sub_connection, *self.args, **self.kwargs)
				self._child_conn.send(None)
		except Exception as exc:
			print("arriva eccezione child")
			trab = traceback.format_exc()
			self._child_conn.send((exc, trab))
			print("##############################################################################################################")
			exc_type, exc_obj, exc_tb = sys.exc_info()
			fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			msg = "Error! An Exception of type {0} occurred.\nNamely: \n{1!r}"
			message1 = msg.format(type(exc).__name__, exc.args)
			print(message1)
			print("The detected error is at line {} of file: {}".format(exc_tb.tb_lineno,fname))
			error_is = traceback.format_exc().splitlines()
			bb = ''.join(error_is[2].split())
			print("The culprit is: {}".format(bb))
			print("##############################################################################################################")


class OimiManager(BaseManager):
	"""Need to be called in main with: OimiManager.register('LifoQueue', LifoQueue)"""
	pass