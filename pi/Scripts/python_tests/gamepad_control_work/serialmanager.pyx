"""Little serialmanager copy for test gamepad with only methods that are useful here"""
#distutils: language=c++ cython: language_level=3
#import pyximport; pyximport.install()
import re, time
import serial
cimport serialmanager as sm 
from libc.stdlib cimport malloc, free
from libc.string cimport strcpy, strlen
from oimi_process import OimiProcess, OimiStartProcess, OimiQueue, OimiManager


cdef class SerialManager:
	cdef public:
		str name
		int start_marker  
		int end_marker
		ser
	def __cinit__(self, str name, int start_marker, int end_marker, ser):
		self.name = name
		self.start_marker = start_marker
		self.end_marker = end_marker
		self.ser = ser

	cdef send_to_arduino(self, char* sendString):
		#self.ser.write(bytes(sendString))
		self.ser.write(sendString)
		
	'''read on serial correct string'''
	cdef char* receive_from_arduino(self):
		cdef char* z = 'z'
		cdef char* ch = ''
		cdef bytes put = z
		cha = ch.decode('UTF-8', 'strict')
		try:
			while ord(put) != self.start_marker:	
				put = self.ser.read()
			while ord(put) != self.end_marker:	
				if ord(put) != self.start_marker:
					va = put.decode('UTF-8', 'strict')	
					cha = '{}{}'.format(cha,va)
				put = self.ser.read() 
		except: 
			pass
		cc = cha.encode('UTF-8', 'strict')
		return cc

	'''connection boot/reboot test communication'''
	cdef waiting_arduino(self):
		cdef char* msg = ''
		cdef char* right_msg = 'hei'
		while msg.find(right_msg) == -1:
			while self.ser.inWaiting() == 0:
				pass
			msg = self.receive_from_arduino()
			print(msg)
	cdef waiting_arduino_nano(self):
		cdef char* msg = ''
		cdef char* right_msg = 'Nano1 is ready'
		while msg.find(right_msg) == -1:
			while self.ser.inWaiting() == 0:
				pass
			msg = self.receive_from_arduino()
			print(msg)
	cdef waiting_arduino_nano2(self):
		cdef char* msg = ''
		cdef char* right_msg = 'Nano2 is ready'
		while msg.find(right_msg) == -1:
			while self.ser.inWaiting() == 0:
				pass
			msg = self.receive_from_arduino()
			print(msg)			
	cdef waiting_arduino_mega(self):
		cdef char* msg = ''
		cdef char* right_msg = 'Mega is ready'
		while msg.find(right_msg) == -1:
			while self.ser.inWaiting() == 0:
				pass
			msg = self.receive_from_arduino()
			print(msg)

	cpdef close(self):
		self.ser.close()

	cpdef inWaiting(self):
		self.ser.inWaiting()

	cdef led_controller(self, char* result):
		cdef bint waitingForReply = False
		cdef char* dataReceived
		startledtime = time.time()
		if waitingForReply == False:
			self.send_to_arduino(result)
			print('Sent from RASPI {}'.format(result))
			waitingForReply = True
		if waitingForReply == True:
			while self.ser.inWaiting() == 0:
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply led Received{}'.format(dataReceived))

	cdef prendi(self):
		cdef char* dataReceived
		startledtime = time.time()
		#while self.ser.inWaiting() == 0:
		#	pass
		dataReceived = self.receive_from_arduino()
		print('Reply led Received{}'.format(dataReceived))
	
	cdef take_front(self):
		cdef char* command = '<pp>'
		cdef char* dataReceived = ''
		cdef bint waiting_for_reply = False
		if not waiting_for_reply:
			self.send_to_arduino(command)
			print('Sent from Raspi {} take front'.format(command))
			waiting_for_reply = True
		if waiting_for_reply:
			while self.ser.inWaiting() == 0:			
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply Received take one {}'.format(dataReceived))
			res = dataReceived.split()
			ans = float(res[3].decode())
		return ans
		
	cdef daje(self,val):
		cdef char* command0 = '<switchMod0>'
		cdef char* command1 = '<switchMod1>'
		cdef char* command2 = '<switchMod2>'
		cdef char* command3 = '<switchMod3>'
		cdef char* dataReceived = ''
		cdef bint waiting_for_reply = False
		if not waiting_for_reply:
			if val==0:
				self.send_to_arduino(command0)
				print('Sent from Raspi {} '.format(command0))
			elif val==1:
				self.send_to_arduino(command1)
				print('Sent from Raspi {} '.format(command1))
			elif val==2:
				self.send_to_arduino(command2)
				print('Sent from Raspi {} '.format(command2))
			elif val==3:
				self.send_to_arduino(command3)
				print('Sent from Raspi {}' .format(command3))
			waiting_for_reply = True
		if waiting_for_reply:
			while self.ser.inWaiting() == 0:			
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply Received take one {}'.format(dataReceived))

	'''cdef tell_to_move(self, char* command):
		cdef bint waiting_for_reply = False
		cdef char* dataReceived = ''
		while True:
			try:
				if not waiting_for_reply:
					self.send_to_arduino(command)
					print('Sent from Raspi {} autonomous movement'.format(command))
					waiting_for_reply = True
				if waiting_for_reply:
					while self.ser.inWaiting() == 0:			
						pass
					dataReceived = self.receive_from_arduino()
					print('Reply Received from ARDUIN {}'.format(dataReceived))
			except Exception:
				continue
					#res = dataReceived.split()
					#ans = float(res[3].decode())
			break #return ans
		return'''
	
	cdef tell_to_stop(self, char* command):
		cdef bint waiting_for_reply = False
		cdef char* dataReceived = ''
		cdef int ra =0
		while ra < 1:
			try:
				if not waiting_for_reply:
					self.send_to_arduino(command)
					print('Sent from Raspi {} stop!'.format(command))
					waiting_for_reply = True
				if waiting_for_reply:
					if self.ser.inWaiting() > 0:
						ra = 1
					else: continue
					print(ra)
					time.sleep(1)
					while dataReceived == b'': 
						try:
							dataReceived = self.receive_from_arduino()
						except:
							continue
					print('Reply Received take one {}'.format(dataReceived))
					#res = dataReceived.split()
					#ans = float(res[3].decode())
			except serial.serialutil.SerialException:
				continue
			break
		return 0
		
	cdef tell_to_move3(self, command):
		#cdef char* command = '<elaps>'
		cdef bint waiting_for_reply = False
		cdef char* dataReceived = ''
		if not waiting_for_reply:
			self.send_to_arduino(command)
			print('Sent from Raspi {} TEST PRESS'.format(command))
			waiting_for_reply = True
		if waiting_for_reply:
			while self.ser.inWaiting() == 0:			
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply Received happiness {}'.format(dataReceived))
			#res = dataReceived.split()
			#ans1 = float(res[3].decode())
			#ans2 = float(res[7].decode())
			#ans3 = res[10].decode()
			#ans4 = res[13].decode()
		return #ans1,ans2,ans3,ans4
	
	cdef movement_command(self, command):
		cdef bint waiting_for_reply = False
		cdef char* dataReceived = ''
		if not waiting_for_reply:
			self.send_to_arduino(command)
			print('Sent from Raspi {} TEST PRESS'.format(command))
			waiting_for_reply = True
		if waiting_for_reply:
			while self.ser.inWaiting() == 0:			
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply Received take one {}'.format(dataReceived))
			res = dataReceived.split()
			strafe = float(res[3].decode())
			forward = float(res[7].decode())
			angular = float(res[11].decode())
		return strafe,forward,angular
	
	def waitard(self):
		self.waiting_arduino()
	def waitard_nano(self):
		self.waiting_arduino_nano()
	def waitard_nano2(self):
		self.waiting_arduino_nano2()
	def waitard_mega(self):
		self.waiting_arduino_mega()
	def led_co(self,result):
		cdef char* res = result
		self.led_controller(res)
	def takempx(self):
		ans = self.take_front()
		print('ansel is{}'.format(ans))
		return ans
	def dajetutta(self,val):
		self.daje(val)
		
	def move_order(self,move_comm):
		cdef str command_mov = move_comm
		cdef char* command_feeling
		if command_mov == "follow":
			command = b'<follow>'
		elif command_mov == "autonomous":
			command = b'<auto>'
		ans1,ans2,ans3,ans4 = self.tell_to_move3(command)
		print("{},{},{},{}".format(ans1,ans2,ans3,ans4))
		
	def react_results_final(self,move_comm,led_col,animation,for_speed,ang_speed,choosen_audio):
		end = '>'
		if move_comm == "sad":
			command_feeling = '<sad'
		elif move_comm == "normal":
			command_feeling = '<normal'
		elif move_comm == "happy":
			command_feeling = '<happy'
		elif move_comm == "enthusiast":
			command_feeling = '<enthusiast'

		values = [command_feeling,led_col,animation,for_speed,ang_speed]
		strin = ','.join(map(str, values))
		addend = [strin,end]
		command_to_send = ''.join(map(str, addend))
		print("command to send is {}".format(command_to_send))
		command_to_send = command_to_send.encode('UTF-8', 'strict')
		#cdef char* command = command_to_send #per passare char faccio prova coi cdef
		
		#gare.play_audio(choosen_audio)
		
		ans1, ans2, ans3, ans4 = self.tell_to_move(command_to_send)
		print("ans1 is {}".format(ans1))
		print("ans2 is {}".format(ans2))
		print("ans3 is {}".format(ans3))
		print("ans4 is {}".format(ans4))
		
	def test_for_movement(self):
		cdef char* comma = b'<auto>'
		commando = "<auto>"
		commando = commando.encode('UTF-8', 'strict')		
		cdef int maxtime = 3000
		self.tell_to_move3(commando)
#		res = capa.fun_mpr121(maxtime)
#		if res==1:
#			cdef char* stopcom = b'<stop>'		
#			self.tell_to_stop(stopcom)

	def test_for_stop(self):
		cdef char* stopcom  = b'<stop>'
		ret = self.tell_to_stop(stopcom)
		return ret 
		
	def prendiprendi(self):
		self.prendi()
	
	def send_reset(self):
		cdef bint waitingForReply = False
		cdef char* result = b'<reset>'
		cdef char* dataReceived
		startledtime = time.time()
		if waitingForReply == False:
			self.send_to_arduino(result)
			print('Sent from RASPI {}'.format(result))
			waitingForReply = True
		if waitingForReply == True:
			while self.ser.inWaiting() == 0:
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply led Received{}'.format(dataReceived))

	cdef tell_to_move(self, comm):
		cdef bint waiting_for_reply = False
		cdef char* dataReceived = ''
		if not waiting_for_reply:
			self.send_to_arduino(comm)
			print('Sent from Raspi {}'.format(comm))
			waiting_for_reply = True
		if waiting_for_reply:
			while self.ser.inWaiting() == 0:			
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply Received {}'.format(dataReceived))
			res = dataReceived.split()
			ans1 = float(res[3].decode())
			ans2 = float(res[7].decode())
			ans3 = float(res[11].decode())
		return ans1,ans2,ans3

	cdef tell_to_move2(self, command):
		cdef bint waiting_for_reply = False
		cdef char* dataReceived = ''
		if not waiting_for_reply:
			self.send_to_arduino(command)
			print('Sent from Raspi {} TEST PRESS'.format(command))
			waiting_for_reply = True
		if waiting_for_reply:
			while self.ser.inWaiting() == 0:			
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply Received take one {}'.format(dataReceived))
			res = dataReceived.split()
			ans1 = float(res[3].decode())
			ans2 = float(res[7].decode())
		return ans1,ans2

	'''cdef tell_to_move3(self, command):
		#cdef char* command = '<elaps>'
		cdef bint waiting_for_reply = False
		cdef char* dataReceived = ''
		while True:
			try:
				if not waiting_for_reply:
					self.send_to_arduino(command)
					print('Sent from Raspi {} TEST PRESS'.format(command))
					waiting_for_reply = True
				if waiting_for_reply:
					while self.ser.inWaiting() == 0:			
						pass
					dataReceived = self.receive_from_arduino()
					print('Reply Received take one {}'.format(dataReceived))
					res = dataReceived.split()
					ans1 = float(res[3].decode())
					ans2 = float(res[7].decode())
					ans3 = res[10].decode()
					ans4 = res[13].decode()
			except Exception:
				continue
			break 
		return ans1,ans2,ans3,ans4'''

	def gamepad2(self,move_comm,strafe,forward):
		'''without angular'''
		print("---> ENTER GAMEPAD2")
		end = '>'
		if move_comm == "gostra":
			move_comm = '<gostra'
		elif move_comm == "golef":
			move_comm = '<golef'
		elif move_comm == "gorig":
			move_comm = '<gorig'
		elif move_comm == "gobac":
			move_comm = '<gobac'
		elif move_comm == "joystop":
			move_comm = '<joystop'
		elif move_comm == "simplygo2":
			move_comm = '<simplygo2'

		values = [move_comm,strafe,forward]
		strin = ','.join(map(str, values))
		addend = [strin,end]
		command_to_send = ''.join(map(str, addend))
		print("command to send is {}".format(command_to_send))
		command_to_send = command_to_send.encode('UTF-8', 'strict')
		#cdef char* command = command_to_send #per passare char faccio prova coi cdef
		
		#gare.play_audio(choosen_audio)
		
		ans1, ans2 = self.tell_to_move2(command_to_send)
		print("ans1 is {}".format(ans1))
		print("ans2 is {}".format(ans2))

	cdef cgamepad(self, char* command, float strafe, float forward, float angular):		
		cdef char* dataReceived = ''
		cdef bint waiting_for_reply = False
		comma = sm.createMsg(command, strafe, forward, angular);
		cdef char * command_to_send= <char * >malloc(60)
		strcpy(command_to_send,comma)
		print("command to send for gamepad is {}".format(command_to_send))
		print("command has been send comma is {}".format(comma))
		print("command again  is {}".format(command_to_send))
		#cdef char* command = command_to_send #per passare char faccio prova coi cdef
		
		#gare.play_audio(choosen_audio)
		#command_to_send = comm.encode('UTF-8', 'strict') #already bytes no??
		#ans1, ans2, ans3 = self.tell_to_move(command_to_send)
		#print("ans1 is {}".format(ans1))
		#print("ans2 is {}".format(ans2))
		#print("ans3 is {}".format(ans3))
		if not waiting_for_reply:
			self.send_to_arduino(command_to_send)
			print('Sent from Raspi cgamepad {}'.format(command_to_send))
			waiting_for_reply = True
		if waiting_for_reply:
			while self.ser.inWaiting() == 0:			
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply Received cgamepad {}'.format(dataReceived))
			res = dataReceived.split()
			ans1 = float(res[3].decode())
			ans2 = float(res[7].decode())
			ans3 = float(res[11].decode())
		#return ans1,ans2,ans3
			print("ans1 is {}".format(ans1))
			print("ans2 is {}".format(ans2))
			print("ans3 is {}".format(ans3))
		free(command_to_send)

	#def ss(self, send):
	#	print("sending msg {} to arduino".format(send))
	#	self.send_to_arduino(send)

	def gamepad(self,command,strafe_speed,forward_speed,angular_speed):
		order = command.encode('UTF-8', 'strict')
		cdef:
			char* move_comm = order
			float strafe = strafe_speed
			float forward = forward_speed
			float angular = angular_speed
		#move_comm = sm.fromStringToBytes(command)
		self.cgamepad(move_comm,strafe,forward,angular)

	cdef cprova1(self):
		cdef bint waitingForReply = False
		cdef char* dataReceived
		cdef char* result = b'<control>'
		if waitingForReply == False:
			self.send_to_arduino(result)
			print('Sent from RASPI {}'.format(result))
			waitingForReply = True
		if waitingForReply == True:
			while self.ser.inWaiting() == 0:
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply control Received{}'.format(dataReceived))

	def prova1(self):
		self.cprova1();
	
	cdef csendledcommand(self, conferma):
		cdef bint waitingForReply = False
		cdef char* dataReceived = ''
		cdef char* resultstop = b'<q>' #change command here!!
		cdef char* resultleft = b'<w>' #change command here!!
		cdef char* resultfront = b'<x>' #change command here!!
		cdef char* resultright = b'<y>' #change command here!!
		cdef char* resultback = b'<z>' #change command here!!
		cdef char* resultturn = b'<t>' #change command here!!
		cdef char* resulttran = b'<k>' #change command here!!
		cdef char* resultno = b'<m>' #change command here!!
		
		if waitingForReply == False:
			if conferma==1:
				result = resultleft
			if conferma==2:
				result = resultfront
			if conferma==3:
				result = resultright
			if conferma==4:
				result = resultback
			if conferma==5:
				result = resultturn
			if conferma==6:
				result = resultstop
			if conferma==7:
				result = resulttran
			if conferma==8:
				result = resultno
			self.send_to_arduino(result)
			print('Sent from RASPI {}'.format(result.decode('UTF-8', 'strict')))
			waitingForReply = True
		if waitingForReply == True:
			while self.ser.inWaiting()==0:
				pass
			dataReceived = self.receive_from_arduino()
			print('Reply led Received arduino - {}'.format(dataReceived.decode('UTF-8', 'strict')))

	def sendled(self,conferma):
		self.csendledcommand(conferma)

	
	'''def gamepad(self,move_comm,strafe,forward,angular):
		#with angular also
		end = '>'
		if move_comm == "gostra":
			move_comm = '<gostra'
		elif move_comm == "golef":
			move_comm = '<golef'
		elif move_comm == "gorig":
			move_comm = '<gorig'
		elif move_comm == "gobac":
			move_comm = '<gobac'
		elif move_comm == "joystop":
			move_comm = '<joystop'
		elif move_comm == "simplygo":
			move_comm = '<simplygo'

		values = [move_comm,strafe,forward, angular]
		strin = ','.join(map(str, values))
		addend = [strin,end]
		command_to_send = ''.join(map(str, addend))
		print("command to send is {}".format(command_to_send))
		command_to_send = command_to_send.encode('UTF-8', 'strict')
		#cdef char* command = command_to_send #per passare char faccio prova coi cdef
		
		#gare.play_audio(choosen_audio)
		
		ans1, ans2, ans3 = self.tell_to_move(command_to_send)
		print("ans1 is {}".format(ans1))
		print("ans2 is {}".format(ans2))
		print("ans3 is {}".format(ans3))
	'''
		
