#!/usr/bin/env python3
import board
import busio
from RPi import GPIO
import time

i2c = busio.I2C(board.SCL, board.SDA)

import adafruit_ads1x15.ads1115 as ADS
ads = ADS.ADS1115(i2c)

# Create the I2C bus
i2c = busio.I2C(board.SCL, board.SDA)

# Create the ADC object using the I2C bus
#ads = ADS.ADS1115(i2c)

# Create single-ended input on channel 0
chan = AnalogIn(ads, ADS.P0)

# Create differential input between channel 0 and 1
#chan = AnalogIn(ads, ADS.P0, ADS.P1)

print("{:>5}\t{:>5}".format('raw', 'v'))

while True:
    print("{:>5}\t{:>5.3f}".format(chan.value, chan.voltage))
    time.sleep(0.5)

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(17,GPIO.OUT) # pin 17 (LED) impostato OUT
GPIO.output(17,GPIO.LOW)

def richiamo_pulsante(channel):
    print('Pulsante premuto!')
    if(GPIO.input(17) == GPIO.LOW):
        GPIO.output(17,GPIO.HIGH)
        print('LED acceso!')
    else:
        GPIO.output(17,GPIO.LOW)
        print('LED spento!')

# Pin BCM 15 come INPUT e
# definirlo come PULL-DOWN = OFF = 0
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

# Creazione di un EVENTO (INTERRUPT) per il pin BCM 15
GPIO.add_event_detect(17, GPIO.RISING, callback=richiamo_pulsante)

#message = input('Premi INVIO per uscire\n\n')

GPIO.cleanup()
