import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
def my_callback(channel):
	print "falling edge detected on 17"

raw_input("Press Enter when ready\n>")
GPIO.add_event_detect(17, GPIO.FALLING, callback=my_callback, bouncetime=300)
try:
	print "Waiting for rising edge on port 17"
	GPIO.wait_for_edge(17, GPIO.RISING)
	print "Rising edge detected on port 17. Here endeth the third lesson."
except KeyboardInterrupt:
	GPIO.cleanup()

