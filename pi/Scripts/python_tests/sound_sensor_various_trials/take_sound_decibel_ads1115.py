import time
import board
import busio
import math
import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

startime = time.time()
# Create the I2C bus
i2c = busio.I2C(board.SCL, board.SDA)

# Create the ADC object using the I2C bus
ads = ADS.ADS1015(i2c)

# Create single-ended input on channel 0

# Create differential input between channel 0 and 1
#chan = AnalogIn(ads, ADS.P0, ADS.P1)


def metodo():
	chan = AnalogIn(ads, ADS.P0)
	
	print("{:>5}\t{:>5}".format('raw', 'v'))

	while True:
		print("{:>5}\t{:>5.3f}".format(chan.value, chan.voltage))
		time.sleep(0.5)

		val1 = 16.801*math.log(math.e,chan.value/1023) + 9.872
		val2 = 20.0  * math.log10 (chan.value) +9+1.76
		val3 = 20.0 * math.log10(chan.value)
		val4 = 10*math.log10(10/6)+20.0 * math.log10(chan.value)
		dB = (chan.value + 83.2073) / 11.003
		#print(val1)
		#print(val2)
		#print("val3 eccolo{}".format(val3))
		print("val4 == ", val4) #sono i decibel!!!!!!!!
		print("decibel == ", dB) #sono i decibel!!!!!!!!
		time.sleep(.2)
		timenow = time.time()
		diff=timenow-startime
		if(diff>50):
			break
			print("stop")
			print(diff)
			ads.stop_adc()

metodo()
"""
import time
import board
import busio
import Adafruit_ADS1x15
#import adafruit_ads1x15.ads1115 as ADS
import math


i2c = busio.I2C(board.SCL, board.SDA)
adc = Adafruit_ADS1x15.ADS1115(i2)
#adc = ADS.ADS1115(i2c)
gain = 1
startime = time.time()

while True:
	val=adc.read_adc(0,gain)
	print(val)
	val1 = 16.801*math.log(math.e,val/1023) + 9.872
	#dB = (val + 83.2073) / 11.003;
	val2 = 20.0  * math.log10 (val) +9+1.76
	val3 = 20.0 * math.log10(val)
	val4 = 10*math.log10(10/6)+20.0 * math.log10(val)
	print(val1)
	#print(val2)
	#print("val3 eccolo{}".format(val3))
	print(val4) #sono i decibel!!!!!!!!
	time.sleep(.2)
	timenow = time.time()
	diff=timenow-startime
	if(diff>50):
		break
		print("stop")
		print(diff)
		adc.stop_adc()

"""
"""
import time
import board
import busio
import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

# Create the I2C bus
i2c = busio.I2C(board.SCL, board.SDA)

# Create the ADC object using the I2C bus
ads = ADS.ADS1015(i2c)

# Create single-ended input on channel 0
chan = AnalogIn(ads, ADS.P0)

# Create differential input between channel 0 and 1
#chan = AnalogIn(ads, ADS.P0, ADS.P1)

print("{:>5}\t{:>5}".format('raw', 'v'))

while True:
	print("{:>5}\t{:>5.3f}".format(chan.value, chan.voltage))
	time.sleep(0.5)
"""    


